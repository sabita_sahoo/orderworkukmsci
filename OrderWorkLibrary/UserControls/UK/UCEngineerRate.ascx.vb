﻿Imports System.Web
Public Class UCEngineerRate
    Inherits System.Web.UI.UserControl
    Public Property ContactID() As Integer
        Get
            If Not IsNothing(ViewState("ContactID")) Then
                Return ViewState("ContactID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ContactID") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Session("EnggRate") = Nothing
            PopulateData()
        End If
    End Sub
    Public Sub PopulateData()
        Dim ds As DataSet = OrderWorkLibrary.DBWorkOrder.GetEngineerRateInfo(ContactID)
        If ds.Tables.Count > 0 Then
            Session("EnggRate") = ds
            If ds.Tables(0).Rows.Count > 0 Then
                PopulateEnggRate()
            End If
        End If
    End Sub
    Private Sub lnkAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddNew.Click
        UpdateEnggRate()
        
        Dim ds As New DataSet
        ds = CType(Session("EnggRate"), DataSet)

        Dim dvEnggRate As New DataView
        dvEnggRate = ds.Tables(0).DefaultView

        If Not IsNothing(rptEnggRate) Then
            dvEnggRate.RowFilter = "StandardId = 0"

            If dvEnggRate.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "js", "alert('Please select the rate to proceeed!');", True)
            Else
                AddEnggRate()
            End If

            dvEnggRate.RowFilter = ""
        End If

    End Sub
    Public Sub AddEnggRate()

        UpdateEnggRate()

        Dim ds As New DataSet
        ds = CType(Session("EnggRate"), DataSet)

        Dim MaxEnggRateId As Integer
        Dim RowNum As Integer
        RowNum = 1

        For Each drowEnggRate As DataRow In ds.Tables(0).Rows
            MaxEnggRateId = drowEnggRate.Item("MaxEnggRateId") + 1
            drowEnggRate.Item("MaxEnggRateId") = MaxEnggRateId
        Next

        ds.AcceptChanges()
        Session("EnggRate") = ds


        Dim dv As New DataView
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                dv = ds.Tables(0).DefaultView
                RowNum = RowNum + dv.ToTable.Rows.Count
            End If
        End If


        Dim dt As New DataTable

        dt = ds.Tables(0)

        Dim drow As DataRow
        drow = dt.NewRow()

        drow("EnggRateId") = MaxEnggRateId
        drow("EnggRate") = 0
        drow("StandardId") = 0
        drow("MaxEnggRateId") = MaxEnggRateId
        drow("RowNum") = RowNum

        dt.Rows.Add(drow)
        dt.AcceptChanges()

        ds.AcceptChanges()

        Session("EnggRate") = ds

        ds.AcceptChanges()
        Session("EnggRate") = ds

        PopulateEnggRate()

    End Sub

    Protected Sub PopulateEnggRate()
        Dim dsEnggRate As DataSet
        dsEnggRate = CType(Session("EnggRate"), DataSet)
        lblSelectedEnggRates.Text = ""
        If dsEnggRate.Tables.Count > 0 Then
            If dsEnggRate.Tables(0).Rows.Count > 0 Then
                Dim dvEnggRate As New DataView
                dvEnggRate = dsEnggRate.Tables(0).DefaultView
                rptEnggRate.Visible = True
                rptEnggRate.DataSource = dvEnggRate.ToTable.DefaultView
                rptEnggRate.DataBind()
                lblSelectedEnggRates.Text = dsEnggRate.Tables(0).Rows(0)("EngineerRateInfo").ToString
            Else
                rptEnggRate.Visible = False
                rptEnggRate.DataSource = Nothing
                rptEnggRate.DataBind()
            End If
        End If
    End Sub
    Public Sub populateStandardListDD(ByVal drpdwnStandard As DropDownList, ByVal SelectedStandard As Integer)
        Dim ds As DataSet
        ds = CommonFunctions.GetStandards(Page, "Engineer Rates")
        If ds.Tables(0).Rows.Count > 0 Then
            drpdwnStandard.Visible = True
            Dim liProducts As New ListItem
            liProducts = New ListItem
            liProducts.Text = "Select Rate"
            liProducts.Value = 0
            drpdwnStandard.DataSource = ds.Tables(0)
            drpdwnStandard.DataTextField = "StandardValue"
            drpdwnStandard.DataValueField = "StandardId"
            drpdwnStandard.DataBind()
            drpdwnStandard.Items.Insert(0, liProducts)
        Else
            drpdwnStandard.Visible = False
        End If
        

        If Not IsNothing(rptEnggRate) Then
            Dim hdnSelectedStandardId As HtmlInputHidden
            Dim ddListItem As ListItem
            For Each row As RepeaterItem In rptEnggRate.Items
                hdnSelectedStandardId = CType(row.FindControl("hdnSelectedStandardId"), HtmlInputHidden)
                ddListItem = drpdwnStandard.Items.FindByValue(hdnSelectedStandardId.Value)
                If Not ddListItem Is Nothing Then
                    If (hdnSelectedStandardId.Value <> 0) Then
                        drpdwnStandard.Items.Remove(drpdwnStandard.Items.FindByValue(hdnSelectedStandardId.Value))
                    End If
                End If
            Next
            If (drpdwnStandard.Items.Count = 2) Then
                lnkAddNew.Visible = False
            Else
                lnkAddNew.Visible = True
            End If
        End If

        Dim vListItem As ListItem = drpdwnStandard.Items.FindByValue(SelectedStandard)

        If Not vListItem Is Nothing Then
            drpdwnStandard.SelectedValue = SelectedStandard
        End If
        If (drpdwnStandard.SelectedValue <> 0) Then
            drpdwnStandard.Enabled = False
        End If
    End Sub
    Private Sub rptEnggRate_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptEnggRate.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim hdnSelectedStandardId As HtmlInputHidden = CType(e.Item.FindControl("hdnSelectedStandardId"), HtmlInputHidden)
            Dim drpdwnStandard As DropDownList = CType(e.Item.FindControl("drpdwnStandard"), DropDownList)
            populateStandardListDD(drpdwnStandard, hdnSelectedStandardId.Value)
        End If
    End Sub
    Private Sub rptEnggRate_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptEnggRate.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "RemoveRate") Then
                Dim hdnEnggRateId As HtmlInputHidden = CType(e.Item.FindControl("hdnEnggRateId"), HtmlInputHidden)

                UpdateEnggRate()

                RemoveEnggRate(hdnEnggRateId.Value.ToString)

            End If
        End If
    End Sub
    Public Sub RemoveEnggRate(ByVal EnggRateId As String)
        Dim ds As New DataSet
        ds = CType(Session("EnggRate"), DataSet)

        Dim i As Integer
        i = 0
        For Each drow As DataRow In ds.Tables(0).Rows
            If drow.Item("EnggRateId") <> EnggRateId Then
                i = i + 1
                drow.Item("RowNum") = i
            End If
        Next
        ds.Tables(0).AcceptChanges()
        Session("EnggRate") = ds

        Dim dtEnggRate As DataTable = ds.Tables(0)
        Dim dvEnggRate As New DataView
        dvEnggRate = dtEnggRate.Copy.DefaultView

        'add primary key as EnggRateId 
        Dim keysEnggRate(1) As DataColumn
        keysEnggRate(0) = dtEnggRate.Columns("EnggRateId")

        Dim foundrowEnggRate As DataRow() = dtEnggRate.Select("EnggRateId = '" & EnggRateId & "'")
        If Not (foundrowEnggRate Is Nothing) Then
            dtEnggRate.Rows.Remove(foundrowEnggRate(0))
            ds.Tables(0).AcceptChanges()
            Session("EnggRate") = ds
        End If

        PopulateEnggRate()
    End Sub
    Public Sub UpdateEnggRate()

        Dim hdnEnggRateId As HtmlInputHidden

        Dim ds As New DataSet
        ds = CType(Session("EnggRate"), DataSet)

        Dim dvEnggRate As New DataView
        dvEnggRate = ds.Tables(0).DefaultView

        If Not IsNothing(rptEnggRate) Then
            For Each row As RepeaterItem In rptEnggRate.Items
                hdnEnggRateId = CType(row.FindControl("hdnEnggRateId"), HtmlInputHidden)

                dvEnggRate.RowFilter = "EnggRateId = " & hdnEnggRateId.Value

                If dvEnggRate.Count > 0 Then

                    If (CType(row.FindControl("txtEnggRate"), TextBox).Text <> "") Then
                        If (IsNumeric(CType(row.FindControl("txtEnggRate"), TextBox).Text.Trim)) Then
                            dvEnggRate.Item(0).Item("EnggRate") = CType(row.FindControl("txtEnggRate"), TextBox).Text
                        End If
                    Else
                        dvEnggRate.Item(0).Item("EnggRate") = 0
                    End If
                    dvEnggRate.Item(0).Item("StandardId") = CType(row.FindControl("drpdwnStandard"), DropDownList).SelectedValue

                End If

                dvEnggRate.RowFilter = ""
                ds.Tables(0).AcceptChanges()
                Session("EnggRate") = ds
            Next
        End If
    End Sub

    Public Sub UpdateEnginerRates()
        UpdateEnggRate()
        Dim dsEnggRate As DataSet
        dsEnggRate = CType(Session("EnggRate"), DataSet)
          Dim xmlEngineerRateInfo As String = dsEnggRate.GetXml
        Dim ds As DataSet = OrderWorkLibrary.DBWorkOrder.SaveEngineerRateInfo(xmlEngineerRateInfo, ContactID)
    End Sub


End Class