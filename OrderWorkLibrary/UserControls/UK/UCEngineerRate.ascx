﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCEngineerRate.ascx.vb" Inherits="OrderWorkLibrary.UCEngineerRate" %>
<script type="text/javascript">
    function CheckIsDecimal(id) {
        if (document.getElementById(id).value != "") {
            if (IsNumeric(document.getElementById(id).value) == false) {
                document.getElementById(id).value = "0";
                alert("Please enter positive value.");
            }
        }
    }
    function IsNumeric(sText) {

        var IsNumber
        var isInteger_re = /^-?\d*(\.\d+)?$/;
        if (sText.match(isInteger_re)) {
            IsNumber = true;
        }
        else {
            IsNumber = false;

        }

        return IsNumber;
    }  
</script>
<style>
.divOuterpnlTimeBuilderQuestions {
    border: 1px solid #E2E2E2;
    float: left;
    padding: 15px;
}
</style>
 <asp:UpdatePanel ID="UpdatePanelEnggRates" runat="server">
 <ContentTemplate>
 <table cellspacing="0" cellpadding="0" width="100%" border="0" id="Table2" runat="server">
        <tr valign="top">
            <td align="left">
                <div id="div1" class="divAttachment" runat="server">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="padRight20Left20 formTxt">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">	
                                    <tr>
				                    <td>				       				       
				                        <div id = "divOuterpnlTimeBuilderRedirection" class="divOuterpnlTimeBuilderQuestions">
                                        <div id="divOuterRedirection" runat="server">
                                            <table width="100%">
                                        <tr>
                                        <td>&nbsp;</td>
                                        <td>                           
                                            <TABLE cellSpacing="0" cellPadding="0" width="92" border="0" style="float:right;">
		                                        <TR>
			                                        
			                                        <TD>
				                                        <asp:Linkbutton id="lnkAddNew"  runat="server" CausesValidation="false" CssClass="txtListing">
                                                        <asp:Image ID="imgAdd" runat="server" ToolTip="Add"  ImageUrl="~/OWLibrary/Images/Add_btn.gif" />
                                                        </asp:LinkButton>

                                                        
			                                        </TD>
			                                        
		                                        </TR>
	                                        </TABLE>
                                        </td>
                                        </tr>
                                        <tr><td><asp:Label ID="lblSelectedEnggRates" Visible = "false" runat="server"></asp:Label></td></tr>
                                        </table>
                                            <asp:Repeater ID="rptEnggRate" runat="server" >
                                            <ItemTemplate>
                                                    <table style="padding:5px;margin-top:10px;border:solid 1px #E2E2E2;">
                                                    <tr>
                                                    <td>                             
                                                    <input type="hidden" id="hdnEnggRateId" runat="server" value=<%#Container.dataitem("EnggRateId")%> />       
                                                    <input type="hidden" id="hdnSelectedStandardId" runat="server" value=<%#Container.dataitem("StandardId")%> />                                                                                  
                                                    <asp:DropDownList id="drpdwnStandard" runat="server" CssClass="formFieldGrey" Width="220"  Height="22"></asp:DropDownList>    
                                                    &nbsp;&nbsp;£&nbsp;<asp:TextBox ID="txtEnggRate" runat="server" onblur="javascript:CheckIsDecimal(this.id);" CssClass="formField" Width="70" Height="20" Text=<%#Container.dataitem("EnggRate")%>></asp:TextBox>                                                                                         
                                                <TABLE cellSpacing="0" cellPadding="0" width="70" border="0" style="float:right;margin-left:20px;">
		                                        <TR>
			                                         <td>
				                                        <asp:Linkbutton id="lnkBtnRemove"  runat="server" CausesValidation="false" CssClass="txtListing" CommandName="RemoveRate" CommandArgument='<%#Container.DataItem("EnggRateId") %>'>
                                                            <asp:Image ID="imgRemove" runat="server" ToolTip="Remove"  ImageUrl="~/OWLibrary/Images/KnowhowHarrods/delete.png" />
                                                        </asp:LinkButton>
			                                         </td>
		                                        </TR>
	                                        </TABLE>
                                                    </td>
					                            </tr>                                                         
				                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        </div>                           
                                        </div>
				                    </td>
				                    </tr>				      
                                    </table>		
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
 </table>
  </ContentTemplate>
 </asp:UpdatePanel>