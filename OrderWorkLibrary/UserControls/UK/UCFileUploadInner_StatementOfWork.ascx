<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCFileUpload_Inner.ascx.vb" Inherits="OrderWorkLibrary.UCFileUpload_Inner" %>
<style>
.clsButtonGreyFileUpload {
    background-image: url("../OWLibrary/Images/OrderWorkUkRedesign/Save_Btn.png");
    cursor: pointer;
    height: 20px;
    padding-top: 5px;
    text-align: center;
    width: 70px;
     font-family: Verdana;
    font-size: 12px;
}
.textDecorationNone {
    color: #000000;
    text-decoration: none;
}
.textDecorationNone:hover {
    color: #C54043;
    text-decoration: none;
}
</style>

 <table width="100%" id="tblFileAttachments" border="0" cellspacing="0" cellpadding="0" runat="server"  >
      <tr>
        <td width="220px" valign="top">
	        <asp:FileUpload ID="FileUpload1" CssClass="formFieldGrey" width="160px" runat="server" />	  
        </td>
        <td width="20x" valign="top">
            <div  class="clsButtonGreyFileUpload"><asp:linkbutton CausesValidation="false" CssClass="textDecorationNone" id="Linkbutton1" runat="server" OnClick="btnUpload_click" >Upload</asp:linkbutton></div>
        </td>
       
      </tr>
      <tr>
         <td height="18" align="left" valign="bottom" width="220px">
            <asp:label id="lblMsg" runat="server" class="formTxt"/>
        </td>
        <td width="20x">&nbsp;</td>
      </tr>      

</table>



