<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCFileUpload_Inner.ascx.vb" Inherits="OrderWorkLibrary.UCFileUpload_Inner" %>
<style>
.clsButtonGreyFileUpload {
    background-image: url("../OWLibrary/Images/OrderWorkUkRedesign/Save_Btn.png");
    cursor: pointer;
    height: 20px;
    padding-top: 5px;
    text-align: center;
    width: 70px;
     font-family: Verdana;
    font-size: 12px;
}
.textDecorationNone {
    color: #000000;
    text-decoration: none;
}
.textDecorationNone:hover {
    color: #C54043;
    text-decoration: none;
}
</style>
 <table width="100%" id="tblFileAttachments" border="0" cellspacing="0" cellpadding="0" runat="server"  >
      <tr >
        <td >
	        <asp:FileUpload ID="FileUpload1" CssClass="formFieldGrey width140" runat="server" />
        </td>
        <td>
            <div class="clsButtonGreyFileUpload"><asp:linkbutton CssClass="textDecorationNone" CausesValidation="false" id="btnUpload" runat="server">Upload</asp:linkbutton></div>
        </td>
      </tr>
      
      
      <tr align="left" valign="bottom">
        <td height="18" >
            <asp:label id="lblMsg" runat="server" class="formTxt"/>
        </td>
      </tr>     
    
</table>

