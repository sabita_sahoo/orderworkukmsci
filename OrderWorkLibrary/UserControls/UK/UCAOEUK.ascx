
<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCAOE.ascx.vb" Inherits="OrderWorkLibrary.UCAOE" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel2" runat="server"  >
 <ContentTemplate>
	<table><tr>
    <td  ><asp:Label ID=lblMessage  CssClass="errMsgAOE" runat=server></asp:Label></td>
  </tr></table>
<asp:Panel ID="pnlAOEListBoxes" runat=server >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  
  <tr>
    <td><table   border="0" cellspacing="0" cellpadding="0" class="tblSelAOE">
      <tr>
        <td class="formtxt" width="240px">Main-Category<input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" /></td>
        <td class="formtxt"  width="285">Sub-Category<asp:Label runat="server" ID="lblNoteMultiSel" >(use CTRL key to make multiple selections)</asp:Label> </td> 
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <asp:ListBox ID="LBMainCategory"  AutoPostBack=true  class="formtxt ListBoxAoeMainCat"  DataTextField="Name" DataValueField="MainCatID" runat="server"></asp:ListBox>
        </td>
        <td>
          <asp:ListBox ID="LBSubCategory"  class="formtxt ListBoxAoeSubCat"  SelectionMode="multiple" DataTextField="Name" DataValueField="CombID" runat="server"></asp:ListBox>
       </td>
        <td align="left" valign="bottom" ><TABLE cellSpacing="0" cellPadding="0"  class="tblAoeBtnSubmit"  border="0">
								<TR>
									<TD height="18" class="imgAOEBtnLeft">&nbsp;</TD>
									<TD><asp:linkbutton   class="txtButtonNoArrow" id="lnkbtnSelect" CausesValidation=false runat="server">Submit</asp:linkbutton></TD>
									<TD class="imgAOEBtnRight">&nbsp;</TD>
								</TR>
							</TABLE></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td valign=top height=20px class="formtxt"><em>Note: Please press Ctrl+ for multiple selection for Sub-Category </em></td>
    
  </tr>
</table>
</asp:Panel>
<asp:Panel ID="pnlSelectedAOE" runat=server Visible=false>
<table  border="0" cellspacing="0" cellpadding="0" class="tblAOEListOuter">
  <tr>
    <td height="25" class="formtxt">List of selected Areas of Expertise </td>
  </tr>
  <tr>
    <td><table class="tblBorder tblAOEListHeading"  bgcolor="#A0A0A0" border="0" cellspacing="0" cellpadding="2">
      <tr >
        <td width="25" align="center" class="formtxt"><input id='chkAll' onClick=CheckAll(this,'CheckAOE') type='checkbox' name='chkAll' /></td>
        <td class="txtButtonNoArrowHeading tdAOEListMainCat">Main Category </td>
        <td class="txtButtonNoArrowHeading">Sub Category</td>
      </tr>
    </table>
      <div id="divAOE" runat="server" class="treeControl">
      <asp:GridView ID="gvSelectedAOE" CellPadding=2 CellSpacing=0 runat="server" AutoGenerateColumns="False" BorderWidth="1px" BorderColor="#EDEDED" CssClass="gvTreeControl">
               <Columns>  
                   <asp:TemplateField ItemStyle-CssClass="gridRowAOE" ItemStyle-Width="15px" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="center"   >
						<ItemTemplate >
							 <asp:CheckBox ID="CheckAOE" Runat="server" ></asp:CheckBox> <input type="hidden" Runat="server" id="hdnCombId" value=<%# databinder.eval(container.dataitem, "CombID") %>/>
						</ItemTemplate>
						<ItemStyle Wrap=true HorizontalAlign=center  Width="15px"  />
					</asp:TemplateField>
	                <asp:BoundField ItemStyle-CssClass="gridTextNew tdAOEListMainCat" ItemStyle-HorizontalAlign=left  DataField="MainCatName"   />            
    	            <asp:BoundField ItemStyle-CssClass="gridTextNew" ItemStyle-HorizontalAlign=left  DataField="SubCatName"  />                        
                </Columns>
                 <AlternatingRowStyle  BackColor="#E7E7E7" />
                 <RowStyle CssClass=gridRow />                            
               </asp:GridView>
               <div id="divClearBoth"></div>               
               </div>
               <div id="divClearBoth"></div>
               </td>
  </tr>
  
   <tr>
    <td height="40" valign="middle" class="formTxt"> <TABLE cellSpacing="0" cellPadding="0" class="tblAoeBtnDel" border="0">
								<TR>
									<TD height="18" class="imgAOEBtnLeft">&nbsp;</TD>
									<TD><asp:linkbutton   class="txtButtonNoArrow" CausesValidation=false id="lnkbtnRemove" runat="server">Delete Selected</asp:linkbutton></TD>
									<TD class="imgAOEBtnRight">&nbsp;</TD>
								</TR>
							</TABLE> </td>
  </tr>
</table>
</asp:Panel>

</ContentTemplate>   
</asp:UpdatePanel> 