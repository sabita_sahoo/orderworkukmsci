<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCFileUpload_Outer.ascx.vb" Inherits="OrderWorkLibrary.UCFileUpload_Outer" %>
<asp:UpdatePanel ID="UpdatePnlCompanyProfile" runat="server"  UpdateMode="Conditional" RenderMode=Inline> <contenttemplate>
 
<!--<div class="divAttachment" runat=server style="width:100%; "  bgcolor="#F4F5EF" >
	<div class="AttachtopVal "><img src="OWLibrary/Images/Curves/Attach-TL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
	-->
<asp:Button id="btnJavaClickToPopulateAttachFile" onClick="btnJavaClickToPopulateAttachFile_Click" runat="server" causesValidation=False  CssClass="hdnClass" ></asp:Button>
<input type=button id="btnFocusAttach" runat=server class="hdnClass"/>

<div id="divAttachments" runat="server" class="floatleft"> <%--divAttachmentstyle removed--%>			
                            
			                <div style="padding-left:10px; margin-bottom:15px; margin-top:15px;" id="divAttachTitle" runat="server">
			                <div id="divAttachmentTopBand"></div>
                            <div id="divAttachmentMiddleBand" style="border:1px solid #d2d2d2;width:463px;border-bottom:0px;">
                         			<asp:Panel ID="pnlAttachments" runat="server" >
                                    	  <asp:datalist runat="server" ID="dlAttList"   Visible="true"  ItemStyle-BackColor="#FFFFFF" ItemStyle-Width="465px" OnItemCommand="Button_ItemCommand">
                                              <itemtemplate>
                                                <div style="width:100%; height:25px; padding-top:4px; padding-bottom:4px;" class="borderBottom">
                                                    <input type=hidden runat=server id="attachID"  value='<%#Container.DataItem("AttachmentID")%>'/>
                                                    <input type="hidden" runat="server" id="filePath"  value='<%#Container.DataItem("FilePath")%>'/>
                                                    <div class="txtGreySmallSelected" style="float:left; vertical-align:middle; width:235px;color:Black;text-decoration:none;margin-left:5px;">
                                                          <a class='anchorhover' href='<%# getlink(Container.DataItem("FilePath")) %>' target='_blank'><%# Container.DataItem("Name")%></a>
                                                    </div>
                                                    <div style="margin-right:15px; float:left; width:155px;"><img id="imgFileIcon" src='<%# OrderWorkLibrary.CommonFunctions.GetFileIcon(Container.DataItem("Name"))  %>' /></div>
                                                    
                                                    <div style="float:left; text-align:left;"><asp:linkbutton id="btnRemove" causesvalidation="false"  CommandName="Remove"   width="50px" runat="server" class="footerTxtSelected"><img style="margin-top:2px;"   height="14" width="14" src="../OWLibrary/Images/OrderWorkUkRedesign/Delete-icon.gif" title="Delete Icon" alt="Delete Icon"  border="0" /></asp:LinkButton></div>  
                                                </div>                                              
                                        
                                               
                                              </itemtemplate>
                                            </asp:datalist>
            						</asp:Panel>
            						</div>
            						<div id="divAttachmentBtmBand"></div>
            						</div>
            						
            						
            				
                 </div>

<div class="floatLeft marginTop10" id="divCompanyProfileWidth">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="paddingT5">
    <tr id="trTitle" runat="server">
        <td width="12px">&nbsp;</td>
        <td height="14" class="formLabelGrey" width="185px" id="tdExistTitle" runat="server">Select and attach existing file: </td>
        
        <td height="14" class="formLabelGrey" width="250px" id="tdUploadTitle"  runat="server">Select file to upload:</td>       
    </tr>
    <tr>
        <td width="12px">&nbsp;</td>
        <td id="tdExist" runat="server" valign="top">
               <asp:Repeater runat="server" ID="rptExistingAttList" Visible="true">
                      <itemtemplate>
                        <input type="checkbox" id="chkExistAtt"  runat="server"/>
                        <input type=hidden runat=server id="hdnExistAttachID"  value='<%#Container.DataItem("AttachmentID")%>'/>
                        <input type=hidden runat=server id="hdnExistFilePath"  value='<%#Container.DataItem("FilePath")%>'/>
                        
                        <span class='txtGreySmallSelected'>
                              <a class='footerTxtSelected' href='<%# getlink(Container.DataItem("FilePath")) %>' target='_blank'>
                                                            <%# Container.DataItem("Name")%></a>
                        </span><br>
                      </itemtemplate>
                    </asp:Repeater>
                              
                    <TABLE cellSpacing="0"  cellPadding="0" width="70"  border="0">
                        <tr>
                            <td>&nbsp;</td>
                        <tr>
			            <TR bgColor="#a0a0a0">
				            <TD height="18"><IMG height="18" src="~/OWLibrary/Images/Curves/Grey-BtnLeft.gif" runat="server" id="imgbtnGreyLeft" width="5"></TD>
				            <TD>
				                <asp:linkbutton id="btnAttach" class="txtButtonNoArrow" causesvalidation="false"  runat="server" >Attach</asp:linkbutton>
				            </TD>
				            <TD width="5"><IMG height="18" src="~/OWLibrary/Images/Curves/Grey-BtnRight.gif" runat="server" id="imgbtnGreyRight" width="5"></TD>
			            </TR>
		            </TABLE>
         </td>
        
        <td id="tdUpload"  runat="server" valign="top">
           <iframe src="FileAttach.aspx?MODE=IFRAME" id="frameUpload" style="width:100%;height:60px;border-width:0;" runat="server" frameborder="0">
			            Your browser does not support iframes
                </iframe>
        </td>
    </tr>
</table>

</div>
<!--
	<div class="AttachbottomVal "><img src="OWLibrary/Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
</div>
-->

   


</contenttemplate>
  
  
   <triggers>
    <asp:AsyncPostBackTrigger ControlID="btnAttach" EventName="Click" /> 


    

    </triggers> </asp:UpdatePanel>

