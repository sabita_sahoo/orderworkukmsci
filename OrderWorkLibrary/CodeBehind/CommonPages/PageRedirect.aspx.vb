Public Partial Class PageRedirect
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strQS As String = ""
        Dim strURL As String = ""
        Dim strFileName As String = ""
        Dim strQSType As String = ""
        Dim strAreaQS As String = ""

        If Not Request.Url.Query Is Nothing Then
            If Request.Url.Query.Trim <> "" Then
                strURL = Request.Url.Query.ToString.Trim.ToLower
                strQS = Request.Url.Query.Substring(strURL.LastIndexOf("/") + "/".Length).Replace("/", "").Trim
            End If
        End If

        If Not Request.Url.AbsolutePath Is Nothing Then
            If Request.Url.AbsolutePath <> "" Then
                strFileName = Request.Url.AbsolutePath.ToString.Substring(Request.Url.AbsolutePath.ToString.LastIndexOf("/") + "/".Length).Replace("/", "").Trim
            End If
        End If

        JumpFromOldToNewURL(strFileName, strQS, strAreaQS)

    End Sub

    Private Sub JumpFromOldToNewURL(ByVal strFileName As String, ByVal strQS As String, ByVal strAreaQS As String)
        Select Case strQS.ToLower
            Case "home"
                Server.Transfer("~/Home.aspx")

            Case "retailers"
                Server.Transfer("~/Retailers.aspx")
            Case "retailers-whatwedo"
                Server.Transfer("~/Retailers.aspx?tab=WhatWeDoRetailers")
            Case "retailers-benifits"
                Server.Transfer("~/Retailers.aspx?tab=BenifitsRetailers")
            Case "retailers-casestudies"
                Server.Transfer("~/Retailers.aspx?tab=CaseStudiesRetailers")
            Case "retailers-casestudies-mesh"
                Server.Transfer("~/Retailers.aspx?tab=CaseStudy1Retailers")
            Case "retailers-casestudies-nationwideretailer"
                Server.Transfer("~/Retailers.aspx?tab=CaseStudy2Retailers")
            Case "itfirms"
                Server.Transfer("~/ITFirms.aspx")
            Case "itfirms-whatwedo"
                Server.Transfer("~/ITFirms.aspx?tab=WhatWeDoITFirms")
            Case "itfirms-benifits"
                Server.Transfer("~/ITFirms.aspx?tab=BenifitsITFirms")
            Case "itfirms-securityvetting"
                Server.Transfer("~/ITFirms.aspx?tab=securityvettingITFirms")
            Case "itfirms-casestudies"
                Server.Transfer("~/ITFirms.aspx?tab=CaseStudiesITFirms")
            Case "itfirms-casestudy-eposprovider"
                Server.Transfer("~/ITFirms.aspx?tab=CaseStudy1ITFirms")
            Case "itfirms-casesstudy-educationservices"
                Server.Transfer("~/ITFirms.aspx?tab=CaseStudy2ITFirms")
            Case "itprojects"
                Server.Transfer("~/Appscon.aspx")
            Case "itprojects-whatwedo"
                Server.Transfer("~/Appscon.aspx?tab=WhatWeDoITStaffing")
            Case "itprojects-benifits"
                Server.Transfer("~/Appscon.aspx?tab=BenifitsITStaffing")
            Case "itprojects-casestudies"
                Server.Transfer("~/Appscon.aspx?tab=CaseStudiesITStaffing")
            Case "itprojects-casestudies-lda"
                Server.Transfer("~/Appscon.aspx?tab=CaseStudy1ITStaffing")
            Case "itprojects-casestudies-softwaredevhouse"
                Server.Transfer("~/Appscon.aspx?tab=CaseStudy2ITStaffing")
            Case "itstaffing"
                Server.Transfer("~/Appscon.aspx")
            Case "itstaffing-whatwedo"
                Server.Transfer("~/Appscon.aspx?tab=WhatWeDoITStaffing")
            Case "itstaffing-benifits"
                Server.Transfer("~/Appscon.aspx?tab=BenifitsITStaffing")
            Case "itstaffing-casestudies"
                Server.Transfer("~/Appscon.aspx?tab=CaseStudiesITStaffing")
            Case "itstaffing-casestudies-lda"
                Server.Transfer("~/Appscon.aspx?tab=CaseStudy1ITStaffing")
            Case "itstaffing-casestudies-softwaredevhouse"
                Server.Transfer("~/Appscon.aspx?tab=CaseStudy2ITStaffing")

            Case "howwework"
                Server.Transfer("~/Clients.aspx")
            Case "clients"
                Server.Transfer("~/Clients.aspx")
            Case "servicepartners"
                Server.Transfer("~/ServicePartners.aspx")

            Case "aboutus"
                Server.Transfer("~/Overview.aspx")
            Case "companyoverview"
                Server.Transfer("~/Overview.aspx")
            Case "managementteam"
                Server.Transfer("~/MgmtTeam.aspx")
            Case "pressrelease"
                Server.Transfer("~/PressRelease.aspx")
            Case "contactus"
                Server.Transfer("~/ContactUs.aspx")

            Case "register"
                Server.Transfer("~/Register.aspx")
            Case "registerasclient"
                Server.Transfer("~/RegisterAsBuyer.aspx")
            Case "registerasservicepartner"
                Server.Transfer("~/RegisterAsSupplier.aspx")

            Case "login"
                Response.Redirect(OrderWorkLibrary.ApplicationSettings.OrderWorkMyUKURL & "SecureLogin.aspx")

            Case "blogs"
                Response.Redirect(OrderWorkLibrary.ApplicationSettings.BlogsPath)

            Case "privacypolicy"
                Server.Transfer("~/PrivacyPolicy.aspx")
            Case "qualityprocedure"
                Server.Transfer("~/PrivacyPolicy.aspx?tab=qualityprocedure")
            Case "terms"
                Server.Transfer("~/TermsNConditions.aspx")

            Case "whitepaper-retailers"
                Server.Transfer("~/whitePaperDownload.aspx?from=yousellit")
            Case "whitepaper-flexibleresourcing"
                Server.Transfer("~/whitePaperDownload.aspx?from=flexibleresourcing")

            Case "careerdetails-accountimplementer"
                Server.Transfer("~/CareerDetails.aspx?job=strategicAccountsImplementer")
            Case "careerdetails-accountmanager"
                Server.Transfer("~/CareerDetails.aspx?job=accountManagerRole")
            Case "careerdetails-servicedeliveryspecialist"
                Server.Transfer("~/CareerDetails.aspx?job=DeliverySpecialist")
            Case "careerdetails-accountsimplementationmanager"
                Server.Transfer("~/CareerDetails.aspx?job=AccountsImplementationManager")
            Case "careerdetails"
                Server.Transfer("~/CareerDetails.aspx")
            Case "careerlisting"
                Server.Transfer("~/CareerListing.aspx")
            Case "mdblog"
                Response.Redirect(OrderWorkLibrary.ApplicationSettings.OrderWorkBlogURL)
            Case "spblog"
                Response.Redirect(OrderWorkLibrary.ApplicationSettings.ServicePartnerBlogURL)
            Case "blogspage"
                Server.Transfer("~/Blogs.aspx")

            Case "directors"
                Server.Transfer("~/Directors.aspx")

            Case Else
                ShowError()
        End Select
    End Sub

    Private Sub btnError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnError.Click
        lblError.Visible = True
    End Sub

    Private Sub ShowError()
        'FOR AJAX ERRORS

        If Not Page.IsPostBack Then



            If Context.Request("message") <> "" Then
                Dim ajaxErrorMessage As String = ""
                ajaxErrorMessage = Context.Request("message")
                If Not Session("PortalCompanyId") Is Nothing Then
                    'SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "<p> Company Id = " & Session("PortalCompanyId") & " ContactId = " & Session("PortalUserId") & "</p>" + ajaxErrorMessage, "Error in Orderwork: " & Context.Request("url") & " at " & Now())
                    'SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "<p> Company Id = " & Session("PortalCompanyId") & " ContactId = " & Session("PortalUserId") & "</p>" + ajaxErrorMessage, "Error in Orderwork: " & Context.Request("url") & " at " & Now())
                Else
                    'SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, ajaxErrorMessage, "Error in Orderwork: " & Context.Request("url") & " at " & Now())
                    'SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, ajaxErrorMessage, "Error in Orderwork: " & Context.Request("url") & " at " & Now())
                End If
                Return
            End If
            'FOR SERVER SIDE ERRORS
            If Not Session("err") Is Nothing Then
                Dim exDetails As New ExceptionDetails(Session("err"), "", "An error has occurred. If this problem persists, please contact OrderWork.")
                lblText.Text = "<p class='BodyTextLarge'>The resource cannot be found.</br></br>The resource you are looking for is temporarily unavailable. Please review the URL and make sure that it is spelled correctly. </p>" 'exDetails.DisplayMessage
                Dim errDetails As New StringBuilder
                errDetails.Append(exDetails.ToString)
                'Condition is added for checking whether the URL Referrer is nothing
                If Not IsNothing(Context.Request.UrlReferrer) Then
                    errDetails.Append(" Previous URL : " & Context.Request.UrlReferrer.AbsoluteUri & "Present Raw URL : " & Context.Request.RawUrl)
                Else
                    errDetails.Append("Present Raw URL : " & Context.Request.RawUrl)
                End If

                lblError.Text = errDetails.ToString
                lblError.Visible = False
                If Not Session("PortalCompanyId") Is Nothing Then
                    'SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "<p> Company Id = " & Session("PortalCompanyId") & " ContactId = " & Session("PortalUserId") & "</p>" + errDetails.ToString, "Error in Orderwork: Page not found " & Context.Request.Url.AbsoluteUri & " at " & Now())
                    'SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "<p> Company Id = " & Session("PortalCompanyId") & " ContactId = " & Session("PortalUserId") & "</p>" + errDetails.ToString, "Error in Orderwork: Page not found " & Context.Request.Url.AbsoluteUri & " at " & Now())
                Else
                    'SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, errDetails.ToString, "Error in Orderwork: Page not found" & Context.Request.Url.AbsoluteUri & " at " & Now())
                    'SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, errDetails.ToString, "Error in Orderwork: Page not found" & Context.Request.Url.AbsoluteUri & " at " & Now())
                End If
                If ApplicationSettings.ShowDetailedError = "1" Then
                    btnError.Visible = True
                ElseIf Context.Request("ShowError") <> "" Then
                    btnError.Visible = True
                Else
                    btnError.Visible = False
                End If
            Else
                ' Dim exDetails As New PIMAExceptionDetails(, "", "An error has occurred. If this problem persists, please contact OrderWork.")
                lblText.Text = "<p class='BodyTextLarge'>The resource cannot be found.</br></br>The resource you are looking for is temporarily unavailable. Please review the URL and make sure that it is spelled correctly.  </p>" 'exDetails.DisplayMessage
                lblError.Text = ""
                lblError.Visible = False

                'SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error 404: The requested page or file was not found.", "Error in Orderwork: Page not found " & Context.Request.Url.AbsoluteUri & " at " & Now())
                'SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "Error 404: The requested page or file was not found.", "Error in Orderwork: Page not found " & Context.Request.Url.AbsoluteUri & " at " & Now())
            End If
            ' Uncomment below 2 lines if you want to user log out after error
            ' Response.Cache.SetNoStore()
            'Session.Clear()   
        End If
    End Sub

End Class