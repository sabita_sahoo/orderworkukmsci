Public Partial Class errorpage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'FOR AJAX ERRORS

        If Not Page.IsPostBack Then



            If Context.Request("message") <> "" Then
                Dim ajaxErrorMessage As String = ""
                ajaxErrorMessage = Context.Request("message")
                If Not Session("PortalCompanyId") Is Nothing Then
                    SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "<p> Company Id = " & Session("PortalCompanyId") & " ContactId = " & Session("PortalUserId") & "</p>" + ajaxErrorMessage, "Error in Orderwork: " & Context.Request("url") & " at " & Now())
                    SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "<p> Company Id = " & Session("PortalCompanyId") & " ContactId = " & Session("PortalUserId") & "</p>" + ajaxErrorMessage, "Error in Orderwork: " & Context.Request("url") & " at " & Now())
                Else
                    SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, ajaxErrorMessage, "Error in Orderwork: " & Context.Request("url") & " at " & Now())
                    SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, ajaxErrorMessage, "Error in Orderwork: " & Context.Request("url") & " at " & Now())
                End If
                Return
            End If
            'FOR SERVER SIDE ERRORS
            If Not Session("err") Is Nothing Then
                Dim exDetails As New ExceptionDetails(Session("err"), "", "An error has occurred. If this problem persists, please contact OrderWork.")
                lblText.Text = "<p class='BodyTextLarge'>We�re sorry but an error has occurred with the action you were trying to carry out.</br></br>Our development team have been notified of this error message and will be working on resolving this issue shortly.</p>" 'exDetails.DisplayMessage
                Dim errDetails As New StringBuilder
                errDetails.Append(exDetails.ToString)
                'Added By Pankaj Malav on 20 Feb 2009
                'Condition is added for checking whether the URL Referrer is nothing
                If Not IsNothing(Context.Request.UrlReferrer) Then
                    errDetails.Append(" Previous URL : " & Context.Request.UrlReferrer.AbsoluteUri & "Present Raw URL : " & Context.Request.RawUrl)
                Else
                    errDetails.Append("Present Raw URL : " & Context.Request.RawUrl)
                End If

                lblError.Text = errDetails.ToString
                lblError.Visible = False
                If Not Session("PortalCompanyId") Is Nothing Then
                    SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "<p> Company Id = " & Session("PortalCompanyId") & " ContactId = " & Session("PortalUserId") & "</p>" + errDetails.ToString, "Error in Orderwork: " & Context.Request.Url.AbsoluteUri & " at " & Now())
                    SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "<p> Company Id = " & Session("PortalCompanyId") & " ContactId = " & Session("PortalUserId") & "</p>" + errDetails.ToString, "Error in Orderwork: " & Context.Request.Url.AbsoluteUri & " at " & Now())
                Else
                    SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, errDetails.ToString, "Error in Orderwork: " & Context.Request.Url.AbsoluteUri & " at " & Now())
                    SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, errDetails.ToString, "Error in Orderwork: " & Context.Request.Url.AbsoluteUri & " at " & Now())
                End If
                If ApplicationSettings.ShowDetailedError = "1" Then
                    btnError.Visible = True
                ElseIf Context.Request("ShowError") <> "" Then
                    btnError.Visible = True
                Else
                    btnError.Visible = False
                End If
            Else
                ' Dim exDetails As New PIMAExceptionDetails(, "", "An error has occurred. If this problem persists, please contact OrderWork.")
                lblText.Text = "<p class='BodyTextLarge'>We�re sorry but an error has occurred with the action you were trying to carry out.</br></br>Our development team have been notified of this error message and will be working on resolving this issue shortly. </p>" 'exDetails.DisplayMessage
                lblError.Text = ""
                lblError.Visible = False

                SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "No exception occurred but error page was accessed. Please check if error page was hit directly.", "Error in Orderwork: " & Context.Request.Url.AbsoluteUri & " at " & Now())
                SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "No exception occurred but error page was accessed. Please check if error page was hit directly.", "Error in Orderwork: " & Context.Request.Url.AbsoluteUri & " at " & Now())
            End If
            ' Uncomment belove 2 lines if you want to user log out after error
            ' Response.Cache.SetNoStore()
            'Session.Clear()   
        End If
    End Sub
    Private Sub btnError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnError.Click
        lblError.Visible = True
    End Sub
End Class