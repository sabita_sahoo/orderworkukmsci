Public Partial Class UCAOE
    Inherits System.Web.UI.UserControl
    Public Property BizDivId() As Integer
        Get
            If Not IsNothing(ViewState("AOEBizDivId")) Then
                Return ViewState("AOEBizDivId")
            Else
                Return ApplicationSettings.BizDivId
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("AOEBizDivId") = value
        End Set
    End Property
    Public Property CombIds() As String
        Get
            If Not IsNothing(ViewState("AOECombIds")) Then
                Return ViewState("AOECombIds")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("AOECombIds") = value
        End Set
    End Property
    Private _showMultiSelMsg As Boolean = True
    Public Property showMultiSelMsg() As Boolean
        Get
            Return _showMultiSelMsg
        End Get
        Set(ByVal value As Boolean)
            _showMultiSelMsg = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            PopulateAOE()
        End If
    End Sub
    Public Function PopulateAOE()
        PopulateMainCategory(True)
        PopulateSubCategory(0, True)
        PopulateSelectedCombIds()
        If showMultiSelMsg = False Then
            lblNoteMultiSel.Visible = False
        Else
            lblNoteMultiSel.Visible = True
        End If
        Return Nothing
    End Function

    Public Function GetSelectedCombIds(ByVal action As String, ByVal newDS As DataSet, Optional ByVal killCache As Boolean = False) As Object
        Dim ds As DataSet
        Dim CommaSeperatedCombIds As String = ""
        Dim CacheKey As String = "ContactsCategories" + Session.SessionID + CStr(BizDivId)
        If killCache Then
            Page.Cache.Remove(CacheKey)
        End If
        Select Case action
            Case "get"
                If Page.Cache(CacheKey) Is Nothing Then
                    ds = CommonFunctions.GetContactsCategories(Page, CombIds, BizDivId).Copy
                    ds.Tables(0).DefaultView.RowFilter = ""
                    Page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
                Else
                    ds = CType(Page.Cache(CacheKey), DataSet)
                    ds.Tables(0).DefaultView.RowFilter = ""
                End If
                Return ds
            Case "set"
                Page.Cache.Remove(CacheKey)
                Page.Cache.Add(CacheKey, newDS, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
                Return Nothing
            Case "getCommaSeperatedCombIds"
                If Page.Cache(CacheKey) Is Nothing Then
                    ds = CommonFunctions.GetContactsCategories(Page, CombIds, BizDivId).Copy
                    Page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
                Else
                    ds = CType(Page.Cache(CacheKey), DataSet)
                End If

                For Each drow As DataRow In ds.Tables("tblCombIds").Rows
                    If CommaSeperatedCombIds = "" Then
                        CommaSeperatedCombIds &= drow("Combid")
                    Else
                        CommaSeperatedCombIds &= "," & drow("Combid")
                    End If
                Next
                Return CommaSeperatedCombIds
        End Select
        Return Nothing
    End Function
    Public Function PopulateMainCategory(Optional ByVal killCache As Boolean = False)
       
        Dim dsSelectedCombIds As DataSet
        Dim dsMainCat As DataSet
        Dim CacheKey As String = "UnSelectedMainCatIds" & Session.SessionID & BizDivId
        If killCache Then
            Page.Cache.Remove(CacheKey)
        End If
        Dim dsSelectedMainCatIds As DataSet = CType(Page.Cache(CacheKey), DataSet)


        If Not IsNothing(dsSelectedMainCatIds) And killCache = False Then

            dsSelectedCombIds = GetSelectedCombIds("get", Nothing, killCache)
            Dim dvSelectedCombIds As DataView = CType(dsSelectedCombIds, DataSet).Tables(0).DefaultView

enumerateMainCat1:
            For Each drow As DataRow In dsSelectedMainCatIds.Tables(0).Rows
                dvSelectedCombIds.RowFilter = "MainCatID=" & drow("MainCatID")
                If dvSelectedCombIds.Count = drow("SubCatCount") Then
                    dsSelectedMainCatIds.Tables(0).Rows.Remove(drow)
                    GoTo enumerateMainCat1
                End If
            Next
            CType(dsSelectedCombIds, DataSet).Tables(0).DefaultView.RowFilter = ""
            If (dsSelectedMainCatIds.Tables(0).Rows.Count < 1) Then
                pnlAOEListBoxes.Visible = False
            Else
                pnlAOEListBoxes.Visible = True
            End If
            Page.Cache.Remove(CacheKey)
            Page.Cache.Add(CacheKey, dsSelectedMainCatIds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            LBMainCategory.DataSource = dsSelectedMainCatIds
            LBMainCategory.DataBind()
        Else
            dsSelectedCombIds = GetSelectedCombIds("get", Nothing, killCache)
            Dim dvSelectedCombIds As DataView = CType(dsSelectedCombIds, DataSet).Tables(0).DefaultView
            dsMainCat = CommonFunctions.GetMainCategoriesTypes(Page, BizDivId).Copy
enumerateMainCat2:
            For Each drow As DataRow In dsMainCat.Tables(0).Rows
                dvSelectedCombIds.RowFilter = "MainCatID=" & drow("MainCatID")
                If dvSelectedCombIds.Count = drow("SubCatCount") Then
                    dsMainCat.Tables(0).Rows.Remove(drow)
                    GoTo enumerateMainCat2
                End If
            Next
            CType(dsSelectedCombIds, DataSet).Tables(0).DefaultView.RowFilter = ""
            If (dsMainCat.Tables(0).Rows.Count < 1) Then
                pnlAOEListBoxes.Visible = False
            Else
                pnlAOEListBoxes.Visible = True
            End If
            LBMainCategory.DataSource = dsMainCat
            LBMainCategory.DataBind()
            Page.Cache.Remove(CacheKey)
            Page.Cache.Add(CacheKey, dsMainCat, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        End If

        Return Nothing
    End Function
    Public Function PopulateSubCategory(ByVal MainCatId As String, Optional ByVal killCache As Boolean = False)
       
        Dim CacheKey As String = "UnSelectedSubCatIds" & Session.SessionID & BizDivId
        If killCache Then
            Page.Cache.Remove(CacheKey)
        End If
        Dim dsSelectedCombIds As DataSet
        Dim dsSubCat As DataSet
        Dim dsSelectedSubCatIds As DataSet = CType(Page.Cache(CacheKey), DataSet)
        If Not IsNothing(dsSelectedSubCatIds) And killCache = False Then
            dsSelectedCombIds = GetSelectedCombIds("get", Nothing, killCache)
            Dim dvSelectedCombIds As DataView = CType(dsSelectedCombIds, DataSet).Tables(0).DefaultView
enumerateSubCat1:

            For Each dRow As DataRow In dsSelectedSubCatIds.Tables(0).Rows
                dvSelectedCombIds.RowFilter = "SubCatID=" & dRow("SubCatID") & "AND CombID=" & dRow("CombID")
                If dvSelectedCombIds.Count > 0 Then
                    dsSelectedSubCatIds.Tables(0).Rows.Remove(dRow)
                    GoTo enumerateSubCat1
                End If
            Next
            CType(dsSelectedCombIds, DataSet).Tables(0).DefaultView.RowFilter = ""
            Page.Cache.Remove(CacheKey)
            Page.Cache.Add(CacheKey, dsSelectedSubCatIds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))

        Else
            dsSelectedCombIds = GetSelectedCombIds("get", Nothing, killCache)
            Dim dvSelectedCombIds As DataView = CType(dsSelectedCombIds, DataSet).Tables(0).DefaultView
            dsSubCat = CommonFunctions.GetSubCategoriesTypes(Page, 0, BizDivId).Copy

enumerateSubCat2:

            For Each dRow As DataRow In dsSubCat.Tables(0).Rows
                dvSelectedCombIds.RowFilter = "SubCatID=" & dRow("SubCatID") & "AND CombID=" & dRow("CombID")
                If dvSelectedCombIds.Count > 0 Then
                    dsSubCat.Tables(0).Rows.Remove(dRow)
                    GoTo enumerateSubCat2
                End If
            Next
            CType(dsSelectedCombIds, DataSet).Tables(0).DefaultView.RowFilter = ""
            Page.Cache.Remove(CacheKey)
            Page.Cache.Add(CacheKey, dsSubCat, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        End If
        Dim ds As DataSet = CType(Page.Cache(CacheKey), DataSet)

        If MainCatId = 0 Then
            Dim dsMainCat As DataSet = CType(Page.Cache("UnSelectedMainCatIds" & Session.SessionID & BizDivId), DataSet)
            If Not IsNothing(dsMainCat) Then
                If dsMainCat.Tables(0).Rows.Count > 0 Then
                    MainCatId = dsMainCat.Tables(0).Rows(0)("MainCatID")
                Else
                    MainCatId = -1
                End If
            Else
                MainCatId = -1
            End If

        End If
        If MainCatId <> -1 Then            
            LBMainCategory.SelectedValue = MainCatId
        Else
        End If
        dsSelectedSubCatIds = CType(Page.Cache(CacheKey), DataSet)
        Dim dv As DataView = dsSelectedSubCatIds.Tables(0).DefaultView
        dv.RowFilter = "MainCatID=" & MainCatId
        LBSubCategory.DataSource = dv
        LBSubCategory.DataBind()
        dsSelectedSubCatIds.Tables(0).DefaultView.RowFilter = ""
        Return Nothing
    End Function
    Public Function PopulateSelectedCombIds()
        Dim dsSelectedSubCatIds As DataSet = GetSelectedCombIds("get", Nothing)
        If Not IsDBNull(dsSelectedSubCatIds) Then
            If dsSelectedSubCatIds.Tables(0).Rows.Count > 0 Then
                pnlSelectedAOE.Visible = True
                gvSelectedAOE.DataSource = dsSelectedSubCatIds
                gvSelectedAOE.DataBind()
                If dsSelectedSubCatIds.Tables(0).DefaultView.Count > 10 Then
                    divAOE.Attributes.Add("style", "height:208px")
                Else
                    divAOE.Attributes.Remove("style") '.Add("style", "min-height:208px")
                End If
            Else
                pnlSelectedAOE.Visible = False
            End If
        End If
        Return Nothing
    End Function

   
    Public Sub Select_AOE(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnSelect.Click
        Dim dsSelectedCombIds As DataSet
        If LBSubCategory.SelectedIndex < 0 Then
            lblMessage.Text = ResourceMessageText.GetString("SelectCategoryToSubmit")        '   "Please select at least one sub-category in order to submit."
            Return
        Else
            lblMessage.Text = ""
            dsSelectedCombIds = GetSelectedCombIds("get", Nothing)
            For Each lItem As ListItem In LBSubCategory.Items
                If lItem.Selected Then
                    Dim dRow As DataRow = dsSelectedCombIds.Tables(0).NewRow
                    Dim dvCombId As DataView = CommonFunctions.GetCategoryCombDetails(Page, lItem.Value, BizDivId)
                    dRow("CombId") = dvCombId(0)("CombID")
                    dRow("MainCatID") = dvCombId(0)("MainCatID")
                    dRow("SubCatID") = dvCombId(0)("SubCatID")
                    dRow("MainCatName") = dvCombId(0)("MainCatName")
                    dRow("SubCatName") = dvCombId(0)("SubCatName")
                    dsSelectedCombIds.Tables(0).Rows.Add(dRow)
                End If
            Next
        End If
        GetSelectedCombIds("set", dsSelectedCombIds)
        PopulateMainCategory()
        PopulateSubCategory(0)
        PopulateSelectedCombIds()
    End Sub

    Private Sub LBMainCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBMainCategory.SelectedIndexChanged
        PopulateSubCategory(LBMainCategory.SelectedValue)
    End Sub
    Public Sub Remove_AOE(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnRemove.Click

        Dim dsSelectedCombIds As DataSet
        dsSelectedCombIds = GetSelectedCombIds("get", Nothing)
        Dim flagCheck As Boolean = False
        'add primary key as combid 
        Dim keys(1) As DataColumn
        keys(0) = dsSelectedCombIds.Tables(0).Columns("CombID")

        'Sub cat vars
        Dim CacheKey As String = "UnSelectedSubCatIds" & Session.SessionID & BizDivId
        Dim dsSelectedSubCatIds As DataSet = CType(Page.Cache(CacheKey), DataSet)


        'Iteration through grid
        For Each row As GridViewRow In gvSelectedAOE.Rows
            Dim chkBox As CheckBox = CType(row.FindControl("CheckAOE"), CheckBox)
            If chkBox.Checked Then
                dsSelectedCombIds.Tables(0).PrimaryKey = keys
                flagCheck = True
                If dsSelectedCombIds.Tables(0).Rows.Count > 0 Then
                    dsSelectedCombIds.Tables(0).Rows.Remove(dsSelectedCombIds.Tables(0).Rows.Find(CType(row.FindControl("hdnCombId"), HtmlInputHidden).Value))
                    'Add to Unselected sub cats       
                    If Not IsNothing(dsSelectedSubCatIds) Then
                        Dim dRow As DataRow = dsSelectedSubCatIds.Tables(0).NewRow
                        Dim dvCombId As DataView = CommonFunctions.GetCategoryCombDetails(Page, CType(row.FindControl("hdnCombId"), HtmlInputHidden).Value, BizDivId)
                        dRow("CombId") = dvCombId(0)("CombID")
                        dRow("MainCatID") = dvCombId(0)("MainCatID")
                        dRow("SubCatID") = dvCombId(0)("SubCatID")
                        dRow("Name") = dvCombId(0)("SubCatName")
                        dsSelectedSubCatIds.Tables(0).Rows.Add(dRow)
                    End If
                End If

            End If
        Next
        'Add cache for subcats
        Page.Cache.Remove(CacheKey)
        Page.Cache.Add(CacheKey, dsSelectedSubCatIds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))

        'Show validation message
        If flagCheck Then
            lblMessage.Text = ""
        Else
            lblMessage.Text = ResourceMessageText.GetString("SelectCategoryTodelete")   '   "Please select atleast one category to delete."
        End If
        GetSelectedCombIds("set", dsSelectedCombIds)
        AdjustMainCats()
        PopulateMainCategory()
        PopulateSubCategory(0)
        PopulateSelectedCombIds()
    End Sub
    Public Sub AdjustMainCats()
        Dim dsSelectedCombIds As DataSet
        Dim dsMainCat As DataSet
        Dim CacheKey As String = "UnSelectedMainCatIds" & Session.SessionID & BizDivId
        Dim dsSelectedMainCatIds As DataSet = CType(Page.Cache(CacheKey), DataSet)

        dsSelectedCombIds = GetSelectedCombIds("get", Nothing)
        Dim dvSelectedCombIds As DataView = CType(dsSelectedCombIds, DataSet).Tables(0).DefaultView
        dsMainCat = CommonFunctions.GetMainCategoriesTypes(Page, BizDivId).Copy

        For Each drow As DataRow In dsMainCat.Tables(0).Rows
            dvSelectedCombIds.RowFilter = "MainCatID=" & drow("MainCatID")
            If dvSelectedCombIds.Count < drow("SubCatCount") Then
                dsSelectedMainCatIds.Tables(0).DefaultView.RowFilter = "MainCatID=" & drow("MainCatID")
                If dsSelectedMainCatIds.Tables(0).DefaultView.Count < 1 Then
                    Dim newRow As DataRow = dsSelectedMainCatIds.Tables(0).NewRow
                    newRow("MainCatID") = drow("MainCatID")
                    newRow("Name") = drow("Name")
                    newRow("SubCatCount") = drow("SubCatCount")
                    dsSelectedMainCatIds.Tables(0).Rows.Add(newRow)
                End If
            End If
        Next
        CType(dsSelectedCombIds, DataSet).Tables(0).DefaultView.RowFilter = ""
        Page.Cache.Remove(CacheKey)
        Page.Cache.Add(CacheKey, dsSelectedMainCatIds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
    End Sub

    Private Sub gvSelectedAOE_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSelectedAOE.PreRender
        gvSelectedAOE.HeaderRow.Visible = False
    End Sub
#Region "cache"
    Public Sub RemoveCallback(ByVal tag As String, ByVal obj As Object, ByVal reason As CacheItemRemovedReason)
        If (CacheItemRemovedReason.Underused = reason) Then
            Cache.Remove(tag)
            If Not IsNothing(obj) Then
                Cache.Insert(tag, obj, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            End If
        Else
            Cache.Remove(tag)
        End If
    End Sub
#End Region
End Class