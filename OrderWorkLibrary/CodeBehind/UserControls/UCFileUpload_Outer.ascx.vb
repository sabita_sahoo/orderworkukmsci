Imports System.IO

Partial Public Class UCFileUpload_Outer
    Inherits System.Web.UI.UserControl


    Protected WithEvents lnkFileName As System.Web.UI.HtmlControls.HtmlAnchor
    ''' <summary>
    ''' To show/hide Existing File Regions
    ''' </summary>
    ''' <remarks></remarks>
    Private _showExistAttachment As Boolean
    Public Property ShowExistAttach() As Boolean
        Get
            Return _showExistAttachment
        End Get
        Set(ByVal value As Boolean)
            _showExistAttachment = value
        End Set
    End Property

    ''' <summary>
    ''' To show/hide attached file region
    ''' </summary>
    ''' <remarks></remarks>
    Private _showNewAttachment As Boolean
    Public Property ShowNewAttach() As Boolean
        Get
            Return _showNewAttachment
        End Get
        Set(ByVal value As Boolean)
            _showNewAttachment = value
        End Set
    End Property

    ''' <summary>
    ''' To show/hide Upload file region
    ''' </summary>
    ''' <remarks></remarks>
    Private _showUpload As Boolean
    Public Property ShowUpload() As Boolean
        Get
            Return _showUpload
        End Get
        Set(ByVal value As Boolean)
            _showUpload = value
        End Set
    End Property


    ''' <summary>
    ''' To show type of upload, which determines where it is stored on the server which are Company, Insurance , WorkOrder
    ''' </summary>
    ''' <remarks></remarks>
    Private _type As String
    Public Property Type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    ''' <summary>
    ''' Link ID to store the attached file in database
    ''' </summary>
    ''' <remarks></remarks>
    Private _existAttachmentSourceID As Integer
    Public Property ExistAttachmentSourceID() As Integer
        Get
            Return _existAttachmentSourceID
        End Get
        Set(ByVal value As Integer)
            _existAttachmentSourceID = value
        End Set
    End Property

    Private _existAttachmentSource As String
    Public Property ExistAttachmentSource() As String
        Get
            Return _existAttachmentSource
        End Get
        Set(ByVal value As String)
            _existAttachmentSource = value
        End Set
    End Property


    Private _attachmentForID As Integer
    Public Property AttachmentForID() As Integer
        Get
            Return _attachmentForID
        End Get
        Set(ByVal value As Integer)
            _attachmentForID = value
        End Set
    End Property

    Private _attachmentForSource As String
    Public Property AttachmentForSource() As String
        Get
            Return _attachmentForSource
        End Get
        Set(ByVal value As String)
            _attachmentForSource = value
        End Set
    End Property

    Private _maxFile As String
    Public Property MaxFiles() As String
        Get
            Return _maxFile
        End Get
        Set(ByVal value As String)
            _maxFile = value
        End Set
    End Property

    Private _control As String
    Public Property Control() As String
        Get
            Return _control
        End Get
        Set(ByVal value As String)
            _control = value
        End Set
    End Property

    Private _bgColor As String
    Public Property BgColor() As String
        Get
            Return _bgColor
        End Get
        Set(ByVal value As String)
            _bgColor = value
        End Set
    End Property

    Private _companyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property


    Private _bizdivid As Integer
    Public Property BizDivID() As Integer
        Get
            Return _bizdivid
        End Get
        Set(ByVal value As Integer)
            _bizdivid = value
        End Set
    End Property

    Private _MultiAttachFileSizeRestrict As Boolean
    Public Property MultiAttachFileSizeRestrict() As Boolean
        Get
            Return _MultiAttachFileSizeRestrict
        End Get
        Set(ByVal value As Boolean)
            _MultiAttachFileSizeRestrict = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ViewState("ExistAttachmentSourceID") = _existAttachmentSourceID
            ViewState("AttachmentForID") = _attachmentForID
            ViewState("CompanyID") = _companyID
            ViewState("BizDivID") = _bizdivid
            PopulateAttachedFiles()
            PopulateExistingFiles()
            setFileUpload()
        End If
        If _existAttachmentSourceID = 0 Then
            _existAttachmentSourceID = ViewState("ExistAttachmentSourceID")
        Else
            ViewState("ExistAttachmentSourceID") = _existAttachmentSourceID
        End If
        If _attachmentForID = 0 Then
            _attachmentForID = ViewState("AttachmentForID")
        Else
            ViewState("AttachmentForID") = _attachmentForID
        End If
        _companyID = ViewState("CompanyID")
        _bizdivid = ViewState("BizDivID")

        'Preparing information to be send to the inner file upload UC
        Dim cachekey As String = "AttachedFiles-" & _attachmentForID & "-" & _attachmentForSource & "-" & Session.SessionID
        Dim fileStart As String = ""
        Select Case _attachmentForSource
            Case "WorkOrder", "WOComplete"
                fileStart = _companyID & "_" & _attachmentForSource
            Case Else
                fileStart = _attachmentForID
        End Select

        Dim parentname As String = ""
        If Not IsNothing(Me.Parent.ID) Then
            If Me.Parent.ID.ToLower = "pnlswbuyeracceptform" Then
                parentname = "BuyerAcceptForm"
            End If
        End If

        If HttpContext.Current.Request.RawUrl.ToLower.Contains("bookingforms") Then
            frameUpload.Attributes("src") = "~/SecurePages/FileAttach.aspx?id=" & _attachmentForID & "&AttachLinkSource=" & _attachmentForSource & "&Type=" & _type & "&Control=" & _control & "&cachekey=" & cachekey & "&companyid=" & OrderWorkLibrary.Encryption.Encrypt(_companyID) & "&fileStart=" & fileStart & "&bizdivid=" & _bizdivid & "&parentname=" & parentname & "&MultiAttachFileSizeRestrict=" & MultiAttachFileSizeRestrict
        Else
            frameUpload.Attributes("src") = "~/SecurePages/FileAttach.aspx?id=" & _attachmentForID & "&AttachLinkSource=" & _attachmentForSource & "&Type=" & _type & "&Control=" & _control & "&cachekey=" & cachekey & "&companyid=" & OrderWorkLibrary.Encryption.Encrypt(_companyID) & "&fileStart=" & fileStart & "&bizdivid=" & _bizdivid & "&parentname=" & parentname & "&MultiAttachFileSizeRestrict=" & MultiAttachFileSizeRestrict
        End If
    End Sub

    Private Sub setFileUpload()
        If AttachmentForSource = "StatementOfWork" Then
            tdUploadTitle.InnerText = OrderWorkLibrary.ResourceMessageText.GetString("StatementOfWorkUploadTitle")
        Else
            tdUploadTitle.InnerText = OrderWorkLibrary.ResourceMessageText.GetString("UploadTitle")
            tdUploadTitle.VAlign = ""
        End If
    End Sub

    ''' <summary>
    ''' Function to populate attached files
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateAttachedFiles()
        If _showNewAttachment Then
            Dim dsView As DataView
            dsView = GetAttachedFilesDV()
            If _attachmentForSource = _existAttachmentSource Then
                dsView.RowFilter = "LinkSource = '" & _attachmentForSource & "'"
            End If
            If Not (dsView Is Nothing) Then
                If dsView.Count > 0 Then

                    If dsView.Count >= _maxFile Then
                        tdUpload.Visible = False
                        tdUploadTitle.Visible = False
                    Else
                        tdUpload.Visible = True
                        tdUploadTitle.Visible = True
                    End If
                    If Not divAttachTitle Is Nothing Then
                        divAttachTitle.Visible = True
                    End If

                    If Not tdAttachTitle Is Nothing Then
                        tdAttachTitle.Visible = True
                        If Not tdAttach Is Nothing Then
                            tdAttach.Visible = True
                        End If
                        'tdAttach.Visible = True
                    End If

                    dlAttList.Visible = True
                    dlAttList.DataSource = dsView
                    dlAttList.DataBind()
                    If Type = "Insurance" Or Type = "CV" Or Type = "ProofOfMeeting" Or Type = "PassportVisa" Or Type = "UKSecurityClearance" Or Type = "CSCS" Then
                        If _type = "Logo" Then
                            For Each item As DataListItem In dlAttList.Items
                                Dim pnlImage As Panel = CType(item.FindControl("pnlImage"), Panel)

                                Dim imgLogo As Image = CType(item.FindControl("imgLogo"), Image)
                                pnlImage.Visible = True
                                pnlImage.Style.Add("dispaly", "inline")
                                imgLogo.Style.Add("dispaly", "inline")
                                imgLogo.ImageUrl = getlink(dsView.Item(0).Item("FilePath")) '"D:\Projects\OrderWorkMain\Admin\Attachments\Logo_Approvedstamp.gif"
                            Next
                        Else
                            For Each item As DataListItem In dlAttList.Items
                                Dim pnlImage As Panel = CType(item.FindControl("pnlImage"), Panel)

                                Dim imgLogo As Image = CType(item.FindControl("imgLogo"), Image)
                                pnlImage.Visible = False
                                pnlImage.Style.Add("dispaly", "none")
                                imgLogo.Style.Add("dispaly", "none")
                                imgLogo.Visible = False
                            Next
                        End If
                    End If

                Else
                    tdUpload.Visible = True
                    tdUploadTitle.Visible = True
                    If Not divAttachTitle Is Nothing Then
                        divAttachTitle.Visible = False
                        If Not tdAttach Is Nothing Then
                            tdAttach.Visible = False
                        End If
                    End If

                    If Not tdAttachTitle Is Nothing Then
                        tdAttachTitle.Visible = False
                        If Not tdAttach Is Nothing Then
                            tdAttach.Visible = False
                        End If
                    End If
                    dlAttList.Visible = False
                    dlAttList.DataSource = dsView
                    dlAttList.DataBind()
                End If
            End If
        Else
            If Not divAttachTitle Is Nothing Then
                divAttachTitle.Visible = False
                If Not tdAttach Is Nothing Then
                    tdAttach.Visible = False
                End If
                'tdAttach.Visible = False
            End If

            If Not tdAttachTitle Is Nothing Then
                tdAttachTitle.Visible = False
                If Not tdAttach Is Nothing Then
                    tdAttach.Visible = False
                End If
                'tdAttach.Visible = False
            End If


            dlAttList.Visible = False
        End If
        'dlAttList.Visible = True
    End Sub

    ''' <summary>
    ''' Function to populate the existing file
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateExistingFiles()
        If _showExistAttachment Then
            Dim dsView As DataView
            'dsView = CType(Cache("ExistingFiles-" & Session.SessionID), DataView)
            'dsView.RowFilter = ""
            dsView = GetExistingFilesDV()
            dsView.RowFilter = "LinkSource = '" & _existAttachmentSource & "'"
            If Not (dsView Is Nothing) Then
                Dim strId As String = GetAttachFileIDs()
                If strId <> "" Then
                    dsView.RowFilter = "AttachmentID Not IN (" + strId + ")"
                End If
                If dsView.Count > 0 Then
                    tdExistTitle.Visible = True
                    tdExist.Visible = True
                    rptExistingAttList.DataSource = dsView
                    rptExistingAttList.DataBind()
                Else
                    tdExistTitle.Visible = False
                    tdExist.Visible = False
                    rptExistingAttList.DataSource = dsView
                    rptExistingAttList.DataBind()
                End If
            End If
        Else
            tdExistTitle.Visible = False
            tdExist.Visible = False
        End If
    End Sub


    ''' <summary>
    ''' Get all the IDs of the file in the attached file listing
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAttachFileIDs() As String
        Dim dtlist As DataList = CType(FindControl("dlAttList"), DataList)
        Dim dtItem As DataListItem
        Dim attachIDs As String = ""
        For Each dtItem In dtlist.Items
            If attachIDs = "" Then
                attachIDs = CType(dtItem.FindControl("attachID"), HtmlInputHidden).Value
            Else
                attachIDs += "," & CType(dtItem.FindControl("attachID"), HtmlInputHidden).Value
            End If
        Next
        Return attachIDs
    End Function

    ''' <summary>
    ''' To return dispplay path information of the attached file
    ''' </summary>
    ''' <param name="filePath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getlink(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachment")
                Dim str As String
                If (index > 0) Then
                    str = filePath.Substring(index)
                Else
                    str = filePath
                End If
                'str = ApplicationSettings.AttachmentDisplayPath(_bizdivid) + str
                str = ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/" + str + CommonFunctions.AzureStorageAccessKey().ToString
                Return str.Replace("\", "/")
            End If
        End If
        Return ""
    End Function

    ''' <summary>
    ''' To return physical path of the attached file
    ''' </summary>
    ''' <param name="filePath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPhysicalPath(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachment")
                Dim str As String
                If (index > 0) Then
                    str = filePath.Substring(index)
                Else
                    str = filePath
                End If
                str = ApplicationSettings.AttachmentUploadPath(_bizdivid) + str
                Return str
            End If
        End If
        Return ""
    End Function

    ''' <summary>
    ''' Function to return XSD passed to it with attachment information; the XSD passed is of type Contacts
    ''' </summary>
    ''' <param name="dsContacts"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ReturnFilledAttachments(ByVal dsContacts As OWContacts) As OWContacts
        'Return with attached file information to the passed XSD
        Dim dsView As DataView
        dsView = CType(Cache("AttachedFiles-" & _attachmentForID & "-" & _attachmentForSource & "-" & Session.SessionID), DataView)
        If IsNothing(dsView) Then Return dsContacts

        '  dsView.RowFilter = ""
        Dim dvRow As DataRowView
        For Each dvRow In dsView
            If (dvRow("Type").ToString.Contains("application/vnd.openxmlformats-officedocument")) Then
                dsContacts.tblAttachmentInfo.Rows.Add(New String() {dvRow("AttachmentID"), IIf(IsDBNull(dvRow("Type")), "", "application/vnd"), IIf(IsDBNull(dvRow("Name")), "", dvRow("Name")), IIf(IsDBNull(dvRow("FilePath")), "", dvRow("FilePath")), IIf(IsDBNull(dvRow("FileSize")), "", dvRow("FileSize")), IIf(IsDBNull(dvRow("DateCreated")), Date.Today, dvRow("DateCreated")), IIf(IsDBNull(dvRow("LinkID")), "", dvRow("LinkID")), IIf(IsDBNull(dvRow("LinkSource")), "", dvRow("LinkSource"))})
            Else
                dsContacts.tblAttachmentInfo.Rows.Add(New String() {dvRow("AttachmentID"), IIf(IsDBNull(dvRow("Type")), "", dvRow("Type")), IIf(IsDBNull(dvRow("Name")), "", dvRow("Name")), IIf(IsDBNull(dvRow("FilePath")), "", dvRow("FilePath")), IIf(IsDBNull(dvRow("FileSize")), "", dvRow("FileSize")), IIf(IsDBNull(dvRow("DateCreated")), Date.Today, dvRow("DateCreated")), IIf(IsDBNull(dvRow("LinkID")), "", dvRow("LinkID")), IIf(IsDBNull(dvRow("LinkSource")), "", dvRow("LinkSource"))})
            End If
        Next

        Return dsContacts
    End Function
    Public Function ReturnFilledAttachments(ByVal dsContacts As XSDContacts) As XSDContacts
        'Return with attached file information to the passed XSD
        Dim dsView As DataView
        dsView = CType(Cache("AttachedFiles-" & _attachmentForID & "-" & _attachmentForSource & "-" & Session.SessionID), DataView)
        If IsNothing(dsView) Then Return dsContacts

        '  dsView.RowFilter = ""
        Dim dvRow As DataRowView
        For Each dvRow In dsView
            If (dvRow("Type").ToString.Contains("application/vnd.openxmlformats-officedocument")) Then
                dsContacts.tblAttachmentInfo.Rows.Add(New String() {dvRow("AttachmentID"), IIf(IsDBNull(dvRow("Type")), "", "application/vnd"), IIf(IsDBNull(dvRow("Name")), "", dvRow("Name")), IIf(IsDBNull(dvRow("FilePath")), "", dvRow("FilePath")), IIf(IsDBNull(dvRow("FileSize")), "", dvRow("FileSize")), IIf(IsDBNull(dvRow("DateCreated")), Date.Today, dvRow("DateCreated")), IIf(IsDBNull(dvRow("LinkID")), "", dvRow("LinkID")), IIf(IsDBNull(dvRow("LinkSource")), "", dvRow("LinkSource"))})
            Else
                dsContacts.tblAttachmentInfo.Rows.Add(New String() {dvRow("AttachmentID"), IIf(IsDBNull(dvRow("Type")), "", dvRow("Type")), IIf(IsDBNull(dvRow("Name")), "", dvRow("Name")), IIf(IsDBNull(dvRow("FilePath")), "", dvRow("FilePath")), IIf(IsDBNull(dvRow("FileSize")), "", dvRow("FileSize")), IIf(IsDBNull(dvRow("DateCreated")), Date.Today, dvRow("DateCreated")), IIf(IsDBNull(dvRow("LinkID")), "", dvRow("LinkID")), IIf(IsDBNull(dvRow("LinkSource")), "", dvRow("LinkSource"))})
            End If
        Next

        Return dsContacts
    End Function

    ''' <summary>
    ''' Overloaded function to return XSD passed to it with attachment information; the XSD passed is of type WorkOrder
    ''' </summary>
    ''' <param name="dsWorkOrder"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ReturnFilledAttachments(ByVal dsWorkOrder As WorkOrderNew) As WorkOrderNew
        'Return with attached file information to the passed XSD
        Dim dsView As DataView
        dsView = CType(Cache("AttachedFiles-" & _attachmentForID & "-" & _attachmentForSource & "-" & Session.SessionID), DataView)
        'Below line is added to check of there is cache for attached file
        'If there isnt any cache then the dataset is fetched from database and cache created with data as that was originally
        If dsView Is Nothing Then
            dsView = GetAttachedFilesDV()
        End If
        'dsView.RowFilter = ""
        Dim dvRow As DataRowView
        For Each dvRow In dsView
            If (dvRow("Type").ToString.Contains("application/vnd.openxmlformats-officedocument")) Then
                dsWorkOrder.tblAttachmentInfo.Rows.Add(New String() {dvRow("AttachmentID"), "application/vnd", dvRow("Name"), dvRow("FilePath"), IIf(IsDBNull(dvRow("FileSize")), "", dvRow("FileSize")), dvRow("DateCreated"), dvRow("LinkID"), dvRow("LinkSource")})
            Else
                dsWorkOrder.tblAttachmentInfo.Rows.Add(New String() {dvRow("AttachmentID"), dvRow("Type"), dvRow("Name"), dvRow("FilePath"), IIf(IsDBNull(dvRow("FileSize")), "", dvRow("FileSize")), dvRow("DateCreated"), dvRow("LinkID"), dvRow("LinkSource")})
            End If
        Next

        Return dsWorkOrder
    End Function

    ''' <summary>
    ''' Overloaded function to return XSD passed to it with attachment information; the XSD passed is of type WorkOrder
    ''' </summary>
    ''' <param name="dsWorkOrder"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ReturnFilledAttachments(ByVal dsWorkOrder As WorkOrder) As WorkOrder
        'Return with attached file information to the passed XSD
        Dim dsView As DataView
        dsView = CType(Cache("AttachedFiles-" & _attachmentForID & "-" & _attachmentForSource & "-" & Session.SessionID), DataView)
        'Below line is added to check of there is cache for attached file
        'If there isnt any cache then the dataset is fetched from database and cache created with data as that was originally
        If dsView Is Nothing Then
            dsView = GetAttachedFilesDV()
        End If
        'dsView.RowFilter = ""
        Dim dvRow As DataRowView
        For Each dvRow In dsView
            If (dvRow("Type").ToString.Contains("application/vnd.openxmlformats-officedocument")) Then
                dsWorkOrder.tblAttachmentInfo.Rows.Add(New String() {dvRow("AttachmentID"), "application/vnd", dvRow("Name"), dvRow("FilePath"), IIf(IsDBNull(dvRow("FileSize")), "", dvRow("FileSize")), dvRow("DateCreated"), dvRow("LinkID"), dvRow("LinkSource")})
            Else
                dsWorkOrder.tblAttachmentInfo.Rows.Add(New String() {dvRow("AttachmentID"), dvRow("Type"), dvRow("Name"), dvRow("FilePath"), IIf(IsDBNull(dvRow("FileSize")), "", dvRow("FileSize")), dvRow("DateCreated"), dvRow("LinkID"), dvRow("LinkSource")})
            End If
        Next

        Return dsWorkOrder
    End Function

    ''' <summary>
    ''' Function to get attachment from the database and store in cache
    ''' </summary>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetAttachedFilesDV(Optional ByVal killcache As Boolean = False) As DataView
        'Fetch data for attached cache list
        Dim Cachekey As String = ""
        If _attachmentForSource = "WOProduct" Then
            Cachekey = "AttachedFiles-" & "0-WorkOrder-" & Session.SessionID
        Else
            Cachekey = "AttachedFiles-" & _attachmentForID & "-" & _attachmentForSource & "-" & Session.SessionID
        End If


        If killcache = True Then
            Page.Cache.Remove(Cachekey)
        End If
        Dim dsView As DataView
        If Cache(Cachekey) Is Nothing Then
            dsView = OrderWorkLibrary.DBContacts.GetAttachmentsDetails(_attachmentForID, _attachmentForSource).Copy.Tables(0).DefaultView
            Cache.Remove(Cachekey)
            Cache.Add(Cachekey, dsView, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        Else
            dsView = CType(Cache(Cachekey), DataView)
        End If
        'dsView.RowFilter = ""
        Return dsView
    End Function

    ''' <summary>
    ''' Function to get exisitng attachment from the database and store in cache
    ''' </summary>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetExistingFilesDV(Optional ByVal killcache As Boolean = False) As DataView
        'Fetch data for existing attachment
        Dim Cachekey As String = ""
        Cachekey = "ExistingFiles-" & _companyID & "-" & _existAttachmentSource & "-" & Session.SessionID

        If killcache = True Then
            Cache.Remove(Cachekey)
        End If
        Dim dsView As DataView
        If Cache(Cachekey) Is Nothing Then
            ' ws.WSContact.Timeout = 300000
            dsView = OrderWorkLibrary.DBContacts.GetAttachmentsDetails(_existAttachmentSourceID, _existAttachmentSource).Copy.Tables(0).DefaultView
            Cache.Add(Cachekey, dsView, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        Else
            dsView = CType(Cache(Cachekey), DataView)
        End If
        ' dsView.RowFilter = ""
        Return dsView
    End Function

    ''' <summary>
    ''' procedure to clear the cache for the attachment
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearUCFileUploadCache()
        'clear the cache
        Cache.Remove("ExistingFiles-" & _companyID & "-" & _existAttachmentSource & "-" & Session.SessionID)
        Cache.Remove("AttachedFiles-" & _attachmentForID & "-" & _attachmentForSource & "-" & Session.SessionID)
    End Sub

    Public Sub ShowFileUpload()
        tdUpload.Visible = True
        tdUploadTitle.Visible = True
        tdAttachTitle.Visible = False
        tdAttach.Visible = False
    End Sub

#Region "Events"


    Public Sub btnJavaClickToPopulateAttachFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnJavaClickToPopulateAttachFile.Click
        PopulateAttachedFiles()
    End Sub



    ''' <summary>
    ''' Handles the Attach functionality; to attach Existing file to the Attached List( e.g Company Profile attachement can be used to attach to "Atatched File List"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Attach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        Dim rptItem As RepeaterItem
        Dim attachIDs As String = ""
        Dim fileArray As New ArrayList

        'Get Attachment Ids of files to be added to the attached files
        For Each rptItem In rptExistingAttList.Items
            If CType(rptItem.FindControl("chkExistAtt"), HtmlInputCheckBox).Checked = True Then
                fileArray.Add(CType(rptItem.FindControl("hdnExistAttachID"), HtmlInputHidden).Value)
            End If
        Next

        'Iterate to the attachment Ids and add the file to attachedfiles cache
        If fileArray.Count > 0 Then
            Dim i As Integer

            Dim dsView As DataView
            dsView = GetExistingFilesDV()
            Dim dsRow As DataRowView

            Dim dvAttach As DataView
            dvAttach = GetAttachedFilesDV()

            'Adding data in the attached cache
            Dim drow As DataRow
            For i = 0 To fileArray.Count - 1
                For Each dsRow In dsView
                    If dsRow.Item("AttachmentID") = fileArray(i) Then
                        drow = dvAttach.Table.NewRow
                        drow("AttachmentID") = dsRow("AttachmentID")
                        drow("Type") = dsRow("Type")
                        drow("Name") = dsRow("Name")
                        drow("FilePath") = dsRow("FilePath")
                        drow("FileSize") = dsRow("FileSize")
                        drow("DateCreated") = dsRow("DateCreated")
                        drow("LinkID") = dsRow("LinkID")
                        drow("LinkSource") = dsRow("LinkSource")
                        dvAttach.Table.Rows.Add(drow)
                    End If
                Next
            Next


            PopulateAttachedFiles()
            PopulateExistingFiles()
        End If
    End Sub


    ''' <summary>
    ''' Remove event handler
    ''' </summary>
    ''' <param name="Sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub Button_ItemCommand(ByVal Sender As Object, ByVal e As DataListCommandEventArgs)
        If e.CommandName = "Remove" Then
            Dim rptItemAttach As DataList = CType(FindControl("dlAttList"), DataList)
            Dim intID As Integer = CType(rptItemAttach.Items(e.Item.ItemIndex).FindControl("attachID"), HtmlInputHidden).Value
            Dim filePath As String = CType(rptItemAttach.Items(e.Item.ItemIndex).FindControl("filePath"), HtmlInputHidden).Value

            ''Commented by Sumit to keep file on physical location  
            'Try
            '    If _showExistAttachment Then
            '        Dim dsViewExist As DataView = GetExistingFilesDV()
            '        dsViewExist.RowFilter = "AttachmentID = '" & intID & "'"
            '        If dsViewExist.Count > 0 Then
            '            'File exist in existing list; do not delete them physically
            '            'Exit try
            '            Exit Try
            '        End If
            '    End If

            '    'Delete the file from the server
            '    Dim theFile As FileInfo = New FileInfo(filePath)
            '    If theFile.Exists Then
            '        File.Delete(filePath)
            '    Else
            '        Throw New FileNotFoundException()
            '    End If
            'Catch ex As FileNotFoundException
            'Catch ex As Exception
            'End Try

            'Make suitable changes in the cache for remove operation
            Dim dsView As DataView
            Dim dvRow As DataRowView
            dsView = CType(Cache("AttachedFiles-" & _attachmentForID & "-" & _attachmentForSource & "-" & Session.SessionID), DataView)
            If intID = 0 Then
                ' dsView(e.Item.ItemIndex).Row.Delete()
                dsView.Table.Rows.Remove(dsView(e.Item.ItemIndex).Row)
                Page.Cache.Add("AttachedFiles-" & _attachmentForID & "-" & _attachmentForSource & "-" & Session.SessionID, dsView, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
            Else
                For Each dvRow In dsView
                    If CInt(dvRow("AttachmentID")) = intID Then
                        'dvRow.Row.Delete()
                        dsView.Table.Rows.Remove(dvRow.Row)
                        Page.Cache.Add("AttachedFiles-" & _attachmentForID & "-" & _attachmentForSource & "-" & Session.SessionID, dsView, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
                        Exit For
                    End If
                Next
            End If
            PopulateAttachedFiles()
            PopulateExistingFiles()


        End If
    End Sub


#End Region
    Public Sub RemoveCallback(ByVal tag As String, ByVal obj As Object, ByVal reason As CacheItemRemovedReason)
        If (CacheItemRemovedReason.Underused = reason) Then
            Cache.Remove(tag)
            If Not IsNothing(obj) Then
                Cache.Insert(tag, obj, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            End If
        Else
            Cache.Remove(tag)
        End If
    End Sub
    ''' <summary>
    ''' This function is being called to minimize the string if in case the path name is too long
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Function GetInsuranceFileName(ByVal filename As String) As String
        If filename.Length > 15 Then
            filename = filename.Substring(0, 15)
        End If
        Return filename
    End Function
End Class