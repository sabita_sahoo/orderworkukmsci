Imports System.Data.SqlClient

''' <summary>
''' Class to access the database for contacts
''' </summary>
''' <remarks></remarks>
Public Class DBContacts

    ''' <summary>
    ''' This method will return the account Migration  status  Details
    ''' </summary>
    ''' <param name="eMail"></param>
    ''' <param name="isRegisterPage"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAccountMigrationDetails(ByVal eMail As String, ByVal isRegisterPage As Boolean) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_CheckMigrationStatus"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Email", eMail)
            If isRegisterPage Then
                Select Case ApplicationSettings.Country
                    Case ApplicationSettings.CountryDE

                        Select Case ApplicationSettings.BizDivId
                            Case ApplicationSettings.OWDEBizDivId
                                cmd.Parameters.AddWithValue("@BizDivId", OrderWorkLibrary.ApplicationSettings.OWDEPeer1BizDivId)
                            Case ApplicationSettings.SFDEBizDivId
                                cmd.Parameters.AddWithValue("@BizDivId", OrderWorkLibrary.ApplicationSettings.SFDEPeer1BizDivId)
                        End Select
                    Case ApplicationSettings.CountryUK
                        Select Case ApplicationSettings.BizDivId
                            Case ApplicationSettings.OWUKBizDivId
                                cmd.Parameters.AddWithValue("@BizDivId", OrderWorkLibrary.ApplicationSettings.OWUKPeer1BizDivId)
                            Case ApplicationSettings.SFUKBizDivId
                                cmd.Parameters.AddWithValue("@BizDivId", OrderWorkLibrary.ApplicationSettings.SFUKPeer1BizDivId)
                        End Select
                End Select
            Else
                cmd.Parameters.AddWithValue("@BizDivId", ApplicationSettings.BizDivId)
            End If



            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("MigrationStatus")
            da.Fill(ds)
            ds.Tables(0).TableName = "user"
            ds.Tables(1).TableName = "spcialistCount"
            ds.Tables(2).TableName = "contactAttributes"
            ds.Tables(3).TableName = "companyAttributes"
            ds.Tables(4).TableName = "contactRoleLinkage"
            ds.Tables(5).TableName = "contactBizDivLinkage"
            ds.Tables(6).TableName = "companyBizDivLinkage"
            ds.Tables(7).TableName = "contactClasses"
            ds.Tables(8).TableName = "ContactAccreditations"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

  
    ''' <summary>
    ''' Get combinationids for AOE for a contact in a bizdiv
    ''' </summary>
    ''' <param name="combIds"></param>
    ''' <param name="paramBizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetContactsCategories(ByVal combIds As String, ByVal paramBizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetCategories"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@combIds", combIds)
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AOE")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCombIds"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    ''' <summary>
    ''' Get combinationids for AOE for a contact in a bizdiv
    ''' </summary>
    ''' <param name="combIds"></param>
    ''' <param name="paramBizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetContactsAOE(ByVal combIds As String, ByVal paramBizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetAOE"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@combIds", combIds)
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AOE")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    ''' <summary>
    ''' Get combinationids for AOE for a contact in a bizdiv
    ''' </summary>
    ''' <param name="paramBizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCategoriesForBizDivId(ByVal paramBizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetCategoriesForBizDiv"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AOE")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCombIds"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    ''' <summary>
    ''' To fetch the only Location name of the contact
    ''' </summary>
    ''' <param name="ContactID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetLocationName(ByVal ContactID) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetLocationName"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Locations")
            da.Fill(ds)
            ds.Tables(0).TableName = "LocationName"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function



    ''' <summary>
    ''' to return the details of the contact
    ''' </summary>
    ''' <param name="contactID"></param>
    ''' <param name="IsMainContact"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetContact(ByVal contactID As Integer, ByVal IsMainContact As Integer, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetContactDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@contactID", contactID)
            cmd.Parameters.AddWithValue("@IsMainContact", IsMainContact)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", HttpContext.Current.Session("PortalUserId"))
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ContactInfo")
            da.Fill(ds)

            ds.Tables(0).TableName = "Contacts"
            ds.Tables(1).TableName = "ContactSettings"
            ds.Tables(2).TableName = "ContactReferences"
            'ds.Tables(3).TableName = "ContactMethods"
            'ds.Tables(4).TableName = "ContactAttributes"
            ds.Tables(4).TableName = "ContactAttachments"
            'ds.Tables(5).TableName = "ContactAttributesMemo"
            'ds.Tables(6).TableName = "ContactLinkage"
            'ds.Tables(7).TableName = "ClassesLinkage"
            'ds.Tables(8).TableName = "ContactSettings"
            'ds.Tables(9).TableName = "ContactReferences"
            'ds.Tables(10).TableName = "ContactRolesLinkage"
            'ds.Tables(11).TableName = "ContactAttachments"
            'ds.Tables(12).TableName = "ContactBizDivLinkage"
            'ds.Tables(13).TableName = "ContactSkillsExams"
            'ds.Tables(14).TableName = "ContactInsurAttachment"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

 
    ''' <summary>
    ''' To return All Attributes for a contact
    ''' </summary>
    ''' <param name="ContactId"></param>
    ''' <param name="AttributeLabel"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAttributesForContact(ByVal ContactId As Integer, ByVal AttributeLabel As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spContacts_GetAttributeForContact"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@AttributeLabel", AttributeLabel)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Attributes")
            da.Fill(ds)
            ds.Tables(0).TableName = "Attributes"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

  

    ''' <summary>
    ''' method used by admin to fetch contacts details as well its controls privleges
    ''' </summary>
    ''' <param name="contactID"></param>
    ''' <param name="IsMainContact"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AdminGetContactDetails(ByVal contactID As Integer, ByVal IsMainContact As Integer, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spAdmin_GetContactDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@contactID", contactID)
            cmd.Parameters.AddWithValue("@IsMainContact", IsMainContact)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ContactInfo")
            da.Fill(ds)

            ds.Tables(0).TableName = "Contacts"
            ds.Tables(1).TableName = "Login"
            ds.Tables(2).TableName = "ContactAddress"
            ds.Tables(3).TableName = "ContactMethods"
            ds.Tables(4).TableName = "ContactAttributes"
            ds.Tables(5).TableName = "ContactAttributesMemo"
            ds.Tables(6).TableName = "ContactLinkage"
            ds.Tables(7).TableName = "ClassesLinkage"
            ds.Tables(8).TableName = "ContactSettings"
            ds.Tables(9).TableName = "ContactReferences"
            ds.Tables(10).TableName = "ContactRolesLinkage"
            ds.Tables(11).TableName = "ContactAttachments"
            ds.Tables(12).TableName = "ContactBizDivLinkage"
            ds.Tables(13).TableName = "ContactSkillsExams"
            ds.Tables(14).TableName = "ContactInsurAttachment"
            ds.Tables(15).TableName = "ControlsPrivileges"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    

    ''' <summary>
    ''' To fetch limited list of the web content dataset with column as type and contentid
    ''' </summary>
    ''' <param name="type"></param>
    ''' <param name="bizDivID"></param>
    ''' <param name="showonSite"></param>
    ''' <param name="NoOfRecs"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWebContentLimitedList(ByVal type As String, ByVal bizDivID As Integer, ByVal showonSite As Boolean, ByVal NoOfRecs As Integer, ByVal OnStage As Boolean, ByVal OnLive As Boolean, Optional ByVal PageName As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSContacts_GetWebContentLimitedList"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Type", type)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            cmd.Parameters.AddWithValue("@ShowOnSite", showonSite)
            cmd.Parameters.AddWithValue("@NoofRec", NoOfRecs)
            cmd.Parameters.AddWithValue("@PageName", PageName)
            cmd.Parameters.AddWithValue("@OnStage", OnStage)
            cmd.Parameters.AddWithValue("@OnLive", OnLive)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WebContents")
            da.Fill(ds)
            If (ds.Tables.Count > 0) Then
                ds.Tables(0).TableName = type
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function



    ''' <summary>
    ''' to fetch location listing for a contact
    ''' </summary>
    ''' <param name="ContactID"></param>
    ''' <param name="UserSiteType"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="recordCount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetLocationListing(ByVal ContactID As Integer, ByVal UserSiteType As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                                ByVal maximumRows As Integer, ByRef recordCount As System.Nullable(Of Integer)) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetLocations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@siteType", UserSiteType)
            cmd.Parameters.AddWithValue("@FilterString", "")
            cmd.Parameters.Add(New SqlParameter("@numresults", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, Nothing, DataRowVersion.Default, 0))
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("LocationListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "Locations"
            recordCount = ds.Tables(1).Rows(0).Item("TotalRecs")
            HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function
    ''' <summary>
    ''' to fetch location listing count for a contact
    ''' </summary>
    ''' <param name="ContactID"></param>
    ''' <param name="UserSiteType"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="recordCount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Public Shared Function SelectListingCount(ByVal ContactID As Integer, ByVal UserSiteType As String, ByRef recordCount As System.Nullable(Of Integer)) As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function
    ''' <summary>
    ''' to add the location or edit the existing location
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="addressId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AddEditLocation(ByVal xmlStr As String, ByVal addressId As Integer, ByVal VersionNo As Byte(), ByVal LoggedInUserID As Integer, Optional ByVal DoNotIncludeInOrdermatch As Boolean = False) As Integer

        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim Success As Integer
            Dim ProcedureName As String = "spContacts_SaveLocations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@AddressID", addressId)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@DoNotIncludeInOrdermatch", DoNotIncludeInOrdermatch)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Success = cmd.Parameters("@Success").Value
            Return Success

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function




    ''' <summary>
    ''' This method will migrate contrct from source to dest bizdiv
    ''' </summary>
    ''' <param name="CompanyId"></param>
    ''' <param name="sourceBizDiv"></param>
    ''' <param name="destBizDiv"></param>
    ''' <param name="xmlContent"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Migrate(ByVal CompanyId As Double, ByVal sourceBizDiv As Integer, ByVal destBizDiv As Integer, ByVal xmlContent As String, ByVal LoggedInUserID As Integer) As Boolean
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim ProcedureName As String = "spContacts_Migrate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@contactId", CompanyId)
            cmd.Parameters.AddWithValue("@sourceBizDiv", sourceBizDiv)
            cmd.Parameters.AddWithValue("@destBizDiv", destBizDiv)
            cmd.Parameters.AddWithValue("@xmlDoc", xmlContent)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value

            ' cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
        Return Nothing
    End Function


    ''' <summary>
    ''' To return web contents to be shown on site, such as press releases, articles, careers and quotation
    ''' </summary>
    ''' <param name="type"></param>
    ''' <param name="showOnSite"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWebContentForSite(ByVal type As String, ByVal showOnSite As Boolean, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_Contacts_GetWebContentForSite"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Type", type)
            cmd.Parameters.AddWithValue("@ShowOnSite", showOnSite)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@ShowOnStage", OrderWorkLibrary.ApplicationSettings.OnStage)
            cmd.Parameters.AddWithValue("@ShowOnLive", OrderWorkLibrary.ApplicationSettings.OnLive)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contents")
            da.Fill(ds)
            ds.Tables(0).TableName = "Content"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' To return record for a particular contentID
    ''' </summary>
    ''' <param name="contentID"></param>
    ''' <param name="contentType"></param>
    ''' <param name="sitefor"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWebContent(ByVal contentID As Integer, ByVal contentType As String, ByVal sitefor As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetWebContentForContentID"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContentID", contentID)
            cmd.Parameters.AddWithValue("@SiteType", sitefor)
            cmd.Parameters.AddWithValue("@ContentType", contentType)



            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contents")
            da.Fill(ds)
            ds.Tables(0).TableName = "Content"


            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetWebContent. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will create new contact
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CreateContact(ByVal xmlStr As String, ByVal LoggedInUserID As Integer) As Boolean
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_Create_Contact"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value

            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This method will create new contractor
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CreateContactor(ByVal Fname As String, ByVal Lname As String, ByVal Email As String, ByVal Phone As String, ByVal Mobile As String, ByVal PostCode As String, ByVal AOE As String, ByVal xmAttachment As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_AddContractor"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Fname", Fname)
            cmd.Parameters.AddWithValue("@Lname", Lname)
            cmd.Parameters.AddWithValue("@Email", Email)
            cmd.Parameters.AddWithValue("@Phone", Phone)
            cmd.Parameters.AddWithValue("@Mobile", Mobile)
            cmd.Parameters.AddWithValue("@PostCode", PostCode)
            cmd.Parameters.AddWithValue("@AOE", AOE)
            cmd.Parameters.AddWithValue("@xmAttachment", xmAttachment)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contents")
            da.Fill(ds)
            ds.Tables(0).TableName = "Content"
            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This method will get count of Suppliers and specialists
    ''' </summary>
    ''' <param name="AOE"></param>
    ''' <param name="Region"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSupplierCount(ByVal AOE As String, ByVal Region As String, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim ProcedureName As String = "spContacts_GetSuppliersCount"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AOE", AOE)
            cmd.Parameters.AddWithValue("@Region", Region)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Suppliers")
            da.Fill(ds)
            ds.Tables(0).TableName = "SupplierCount"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_GetSuppliersCount. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Web Method to return account summary of a contact
    ''' </summary>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetAccountSummaryForCompany(ByVal CompanyID As Integer, ByVal BizDivId As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSContacts_GetAccountSummaryForCompany"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            'Dim BizDivId As Integer = WSContacts.bizDivId
            'If BizDivId <> 0 Then
            '    cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            'End If
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AccountSummary")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact' account  information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This method returns dataset for contacts comments
    ''' </summary>
    ''' <param name="contactID"></param>
    ''' <param name="bizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCommentsForContact(ByVal contactID As Integer, ByVal bizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetComments"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", contactID)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Comments")
            da.Fill(ds)
            ds.Tables(0).TableName = "AccountInfo"
            ds.Tables(1).TableName = "Comment"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetAccountActivationStatus. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will add comment for a contact
    ''' </summary>
    ''' <param name="contactId"></param>
    ''' <param name="commentBy"></param>
    ''' <param name="comment"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AddComments(ByVal contactId As Integer, ByVal commentBy As Integer, ByVal comment As String) As Boolean
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_AddComments"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", contactId)
            cmd.Parameters.AddWithValue("@CommentBy", commentBy)
            cmd.Parameters.AddWithValue("@Comments", comment)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will create new contact
    ''' </summary>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaxRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRegistrantListing(ByVal SortExpression As String, ByVal StartRowIndex As String, ByVal MaxRows As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spAdmin_Contacts_GetRegistrantListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", MaxRows)
            cmd.ExecuteNonQuery()
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("RegistrantListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblRegistrantDetails"
            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will get Contact in the form Company Name - Contact Name. this is used to populate the dropdown.
    ''' </summary>
    ''' <param name="bizDivId"></param>
    ''' <param name="ClassID"></param>
    ''' <param name="Filter"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetContactsByName(ByVal bizDivId As String, ByVal ClassID As Integer, ByVal Filter As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim ProcedureName As String = "spMSAdmin_GetContactsByName"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ClassID", ClassID)
            cmd.Parameters.AddWithValue("@BizDivId", CInt(bizDivId))
            cmd.Parameters.AddWithValue("@Filter", Filter)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ContactsNames")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContactsNames"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="contentID"></param>
    ''' <param name="doMode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AddEditWebContent(ByVal xmlStr As String, ByVal contentID As Integer, ByVal doMode As String) As Boolean

        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContent_SaveWebContent"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@contentID", contentID)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value


        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="contactID"></param>
    ''' <param name="linkSource"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAttachmentsDetails(ByVal contactID As Integer, ByVal linkSource As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetAttachmentsDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", contactID)
            cmd.Parameters.AddWithValue("@LinkSource", linkSource)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Attachments")
            da.Fill(ds)
            ds.Tables(0).TableName = "Attachment"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetAttachmentsDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="ContactID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCompanyAccreditations(ByVal BizDivID As Integer, ByVal ContactID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "[spContacts_GetCompanyAccreditations]"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Vendors")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAllVendors"
            ds.Tables(1).TableName = "tblAllPartners"
            ds.Tables(2).TableName = "tblSelectedVendors"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="ContactID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAccreditations(ByVal CommonID As Integer, ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetAccreditations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CommonID", CommonID)
            cmd.Parameters.AddWithValue("@Mode", Mode)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Accreditations")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function GetSkills(ByVal ContactID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetSkills"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Skills")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function AddRemoveAccreditations(ByVal ContactID As Integer, ByVal TagId As Integer, ByVal TagType As String, ByVal TagName As String, ByVal TagInfo As String, ByVal OtherInfo As String, ByVal TagExpiry As String, ByVal Mode As String, ByVal TagIdToRemove As String, ByVal LoggedInUserID As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_AddRemoveAccreditations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@TagId", TagId)
            cmd.Parameters.AddWithValue("@TagType", TagType)
            cmd.Parameters.AddWithValue("@TagName", TagName)
            cmd.Parameters.AddWithValue("@TagInfo", TagInfo)
            cmd.Parameters.AddWithValue("@OtherInfo", OtherInfo)
            cmd.Parameters.AddWithValue("@TagExpiry", TagExpiry)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@TagIdToRemove", TagIdToRemove)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function AddRemoveSkills(ByVal ContactID As Integer, ByVal CombID As Integer, ByVal Mode As String, ByVal LoggedInUserID As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_AddRemoveSkills"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@CombID", CombID)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="contactId"></param>
    ''' <param name="bizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function UpdateCompanyProfile(ByVal xmlStr As String, ByVal contactId As Integer, ByVal bizDivID As Integer, ByVal VersionNo As Byte(), ByVal LoggedInUserID As Integer, Optional ByVal EmpPaymentTermDiscount As Decimal = 0.0, Optional ByVal EmpPaymentTermId As Integer = 4, Optional ByVal OwPaymentTermDiscount As Decimal = 0.0, Optional ByVal OwPaymentTermId As Integer = 1) As Integer
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim success As Integer
            Dim ProcedureName As String = "spContacts_SaveCompanyProfile"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            cmd.Parameters.AddWithValue("@ContactID", contactId)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)
            cmd.Parameters.AddWithValue("@Mode", "Portal")
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            'OA-622 discount changes
            cmd.Parameters.AddWithValue("@EmpPaymentTermId", EmpPaymentTermId)
            cmd.Parameters.AddWithValue("@EmpPaymentTermDiscount", EmpPaymentTermDiscount)
            cmd.Parameters.AddWithValue("@OwPaymentTermId", OwPaymentTermId)
            cmd.Parameters.AddWithValue("@OwPaymentTermDiscount", OwPaymentTermDiscount)
            cmd.ExecuteNonQuery()

            success = cmd.Parameters.Item("@Success").Value

            Return success
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="ContactID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetContactAccreditations(ByVal BizDivID As Integer, ByVal ContactID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "[spContacts_GetContactAccreditations]"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Accreds")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAllVendors"
            ds.Tables(1).TableName = "tblAllCerts"
            ds.Tables(2).TableName = "tblSelectedAccreds"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function DoesLoginExist(ByVal Email As String) As Boolean
        Dim mailStatus As Boolean = False
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_DoesEmailExist"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Email", Email)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("UserData")
            da.Fill(ds)
            ds.Tables(0).TableName = "UserData"
            If ds.Tables("UserData").Rows.Count > 0 Then
                If Not IsDBNull(ds.Tables("UserData").Rows(0).Item("ContactID")) Then
                    'User exists
                    mailStatus = True
                Else
                    'User not registered error.
                    mailStatus = False
                End If
            Else
                mailStatus = False
            End If
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

        DoesLoginExist = mailStatus
    End Function

    Public Shared Function AddEditSpecialistAndChangeLoginInfo(ByVal xmlStr As String, ByVal contactID As Integer, ByVal doMode As String, ByVal bizDivID As Integer, ByVal UserName As String, ByVal OldPassword As String, ByVal NewPassword As String, ByVal SecurityQuest As Integer, ByVal SecurityAns As String, ByVal VersionNo As Byte(), ByVal LoggedInUserID As Integer, ByVal IPhonePin As Integer, ByVal OtherLocations As String, ByVal xmlAccreds As String, ByVal xmlEmergencyContact As String, Optional ByVal VehicleRegNo As String = "", Optional ByVal CVRequired As Boolean = True, Optional ByVal DBSExpiryDate As String = "") As Integer
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim success As Integer
            Dim ProcedureName As String = "spMS_Contacts_SaveSpecialistAndChangeLoginInfo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@contactID", contactID)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)


            cmd.Parameters.AddWithValue("@UserName", UserName)
            cmd.Parameters.AddWithValue("@OldPassword", OldPassword)
            cmd.Parameters.AddWithValue("@NewPassword", NewPassword)
            cmd.Parameters.AddWithValue("@SecurityQues", SecurityQuest)
            cmd.Parameters.AddWithValue("@SecurityAns", SecurityAns)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@IPhonePin", IPhonePin)
            cmd.Parameters.AddWithValue("@OtherLocations", OtherLocations)
            cmd.Parameters.AddWithValue("@xmlAccreds", xmlAccreds)
            'Poonam modified on 31/12/2015 - Task - OM-36 : OM - Add Emergency Contact fields in User detail page My Portal
            cmd.Parameters.AddWithValue("@xmlEmergencyContact", xmlEmergencyContact)
            cmd.Parameters.AddWithValue("@VehicleRegNo", VehicleRegNo)
            cmd.Parameters.AddWithValue("@CVRequired", CVRequired)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            If DBSExpiryDate <> "" And DBSExpiryDate <> "Enter expiry date" Then
                cmd.Parameters.AddWithValue("@DBSExpiryDate", Date.Parse(DBSExpiryDate))
            Else
                cmd.Parameters.AddWithValue("@DBSExpiryDate", DBSExpiryDate)
            End If

            cmd.ExecuteNonQuery()

            '  Return CInt(cmd.Parameters.Item("@Success").Value)
            success = cmd.Parameters.Item("@Success").Value
            'Dim da As New SqlDataAdapter(cmd)
            'Dim ds As New DataSet("tblStatus")
            'da.Fill(ds)
            'ds.Tables(0).TableName = "tblStatus"

            Return success
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveSpecialist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function UpdateSecurityQuesAns(ByVal UserName As String, ByVal SecurityQuest As Integer, ByVal SecurityAns As String) As Integer
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim ProcedureName As String = "spSecurity_SecQuesAns"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure


            cmd.Parameters.AddWithValue("@UserName", UserName)
            cmd.Parameters.AddWithValue("@SecurityQues", SecurityQuest)
            cmd.Parameters.AddWithValue("@SecurityAns", SecurityAns)
            cmd.ExecuteNonQuery()
            '  Return CInt(cmd.Parameters.Item("@Success").Value)
            'Dim da As New SqlDataAdapter(cmd)
            'Dim ds As New DataSet("tblStatus")
            'da.Fill(ds)
            'ds.Tables(0).TableName = "tblStatus"

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveSpecialist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetContactSettings(ByVal companyID As Integer, ByVal contactId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetSettings"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", companyID)
            cmd.Parameters.AddWithValue("@ContactID", contactId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Settings")
            da.Fill(ds)
            ds.Tables(0).TableName = "Setting"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="contactId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function UpdateReferences(ByVal xmlStr As String, ByVal contactId As Integer) As Integer
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_SaveReferences"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@ContactID", contactId)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Decimal).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetSpecialistsListing(ByVal ContactID As Integer, ByVal BizDivID As Integer, ByVal UserSiteType As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                                ByVal maximumRows As Integer, ByRef recordCount As System.Nullable(Of Integer)) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetSpecialists"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)

            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@siteType", UserSiteType)
            cmd.Parameters.AddWithValue("@FilterString", "")
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.Add(New SqlParameter("@numresults", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, Nothing, DataRowVersion.Default, 0))
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SpecialistsListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "Specialists"
            recordCount = ds.Tables(1).Rows(0).Item("TotalRecs")
            Dim dvContact As DataView
            dvContact = ds.Tables(2).Copy.DefaultView
            HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
            HttpContext.Current.Items("AttributeValue") = ds.Tables(2).Rows(0).Item("AttributeValue")
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function
    Public Shared Function SelectSpecialistsListingCount(ByVal ContactID As Integer, ByVal BizDivID As Integer, ByVal UserSiteType As String, ByRef recordCount As System.Nullable(Of Integer)) As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    Public Shared Function GetContactListing(ByVal bizDivId As String, ByVal MainClassID As String, ByVal FromDate As String, ByVal ToDate As String, ByVal statusId As String, ByVal DateCriteriaField As String, ByVal Status As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try

            Dim ProcedureName As String = "spAdmin_GetContactsListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@MainClassID", MainClassID)
            cmd.Parameters.AddWithValue("@statusId", statusId)
            cmd.Parameters.AddWithValue("@Status", Status)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            End If
            cmd.Parameters.AddWithValue("@DateCriteriaField", DateCriteriaField)

            cmd.Parameters.AddWithValue("@BizDivId", CInt(BizDivID))

            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contacts")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContacts"
            ds.Tables(1).TableName = "tblCount"
            If Not IsDBNull(ds.Tables("tblCount").Rows(0)(0)) Then
                rowCount = ds.Tables("tblCount").Rows(0)(0)
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetAdminSiteUsers(ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spSecurity_GetAdminSiteUsers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AdminSiteUsers")
            da.Fill(ds)
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function UpdateCompanyType(ByVal ClassID As Integer, ByVal Status As Integer, ByVal ContactIDs As String, ByVal BizDivID As Integer, ByVal AdminComp As Integer, ByVal AdminCont As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            'Dim WOTrackingID As Integer
            Dim ProcedureName As String = "spContacts_Update_CompanyType"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@CompanyID", ContactIDs)
                .Parameters.AddWithValue("@Status", Status)
                .Parameters.AddWithValue("@ActionByComp", AdminComp)
                .Parameters.AddWithValue("@ActionByCont", AdminCont)
                .Parameters.AddWithValue("@BizDivId", BizDivID)
                .Parameters.AddWithValue("@ClassID", ClassID)
            End With

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contacts")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContacts"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function AddAdminUser(ByVal UserName As String, ByVal Password As String, ByVal SecurityQues As Integer, ByVal SecurityAns As String, ByVal FName As String, ByVal LName As String, ByVal JobTitle As String, ByVal RoleGroupID As Integer, ByVal enableLogin As Boolean) As Boolean
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            'Dim WOTrackingID As Integer
            Dim ProcedureName As String = "spAdmin_CreateNewAdminUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@UserName", UserName)
                .Parameters.AddWithValue("@Password", Password)
                .Parameters.AddWithValue("@SecurityQues", SecurityQues)
                .Parameters.AddWithValue("@SecurityAns", SecurityAns)
                .Parameters.AddWithValue("@FName", FName)
                .Parameters.AddWithValue("@LName", LName)
                .Parameters.AddWithValue("@JobTitle", JobTitle)
                .Parameters.AddWithValue("@RoleGroupID", RoleGroupID)
                .Parameters.AddWithValue("@enableLogin", enableLogin)
            End With

            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    Public Shared Function EditAdminUser(ByVal UserName As String, ByVal FName As String, ByVal LName As String, ByVal RoleGroupID As Integer, ByVal ContactID As Integer, ByVal enableLogin As Boolean) As Boolean
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            'Dim WOTrackingID As Integer
            Dim ProcedureName As String = "spAdmin_EditAdminUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@UserName", UserName)
                .Parameters.AddWithValue("@FName", FName)
                .Parameters.AddWithValue("@LName", LName)
                .Parameters.AddWithValue("@RoleGroupID", RoleGroupID)
                .Parameters.AddWithValue("@ContactID", ContactID)
                .Parameters.AddWithValue("@enableLogin", enableLogin)
            End With
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetContactExporttoExcel(ByVal bizDivId As String, ByVal MainClassID As String, ByVal FromDate As String, ByVal ToDate As String, ByVal statusId As String, ByVal DateCriteriaField As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim ProcedureName As String = "spAdmin_ContactsExportToExcel"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@MainClassID", MainClassID)
            cmd.Parameters.AddWithValue("@statusId", statusId)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            End If
            If DateCriteriaField <> "" Then
                cmd.Parameters.AddWithValue("@DateCriteriaField", DateCriteriaField)
            End If

            cmd.Parameters.AddWithValue("@BizDivId", CInt(bizDivId))

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ExportToExcel")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContacts"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    Public Shared Function GetSearchAccounts(ByVal paramBizDivId As Integer, ByVal SkillSet As String, ByVal Region As String, ByVal PostCode As String, ByVal Keyword As String, ByVal StatusId As String, ByVal fName As String, ByVal lName As String, ByVal companyName As String, ByVal email As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim i As Integer
            If SkillSet <> "" Then
                'Remove Product - Other option

                SkillSet = SkillSet.Replace("782", "")
                SkillSet = SkillSet.Replace(",,", ",")
                If SkillSet.StartsWith(",") = True Then
                    SkillSet = SkillSet.Substring(1, SkillSet.Length - 1)
                End If
                If SkillSet.EndsWith(",") = True Then
                    SkillSet = SkillSet.Substring(0, SkillSet.Length - 1)
                End If

                If SkillSet <> "" Then
                    Dim strSkills() As String = Split(SkillSet, ",")
                    SkillSet = ""
                    For i = 0 To strSkills.GetLength(0) - 1
                        If SkillSet <> "" Then
                            SkillSet &= ","
                        End If
                        SkillSet &= "'" & CStr(strSkills.GetValue(i)) & "'"
                    Next
                End If
            End If

            If Region <> "" Then
                Dim strRegionCode() As String = Split(Region, ",")
                Region = ""
                For i = 0 To strRegionCode.GetLength(0) - 1
                    If Region <> "" Then
                        Region &= ","
                    End If
                    Region &= "'" & CStr(strRegionCode.GetValue(i)) & "'"
                Next
            End If
            If Keyword.IndexOf("'") <> -1 Then
                Keyword = Keyword.Replace("'", "")
            End If
            Dim ProcedureName As String = "spContacts_GetSearchAccounts"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            cmd.Parameters.AddWithValue("@keyword", Keyword)
            cmd.Parameters.AddWithValue("@PostCode", PostCode)
            cmd.Parameters.AddWithValue("@SkillSet", SkillSet)
            cmd.Parameters.AddWithValue("@RegionCode", Region)
            cmd.Parameters.AddWithValue("@StatusId", StatusId)
            cmd.Parameters.AddWithValue("@firstName", fName)
            cmd.Parameters.AddWithValue("@lastName", lName)
            cmd.Parameters.AddWithValue("@companyName", companyName)
            cmd.Parameters.AddWithValue("@Email", email)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)

            cmd.CommandTimeout = 1800
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchAccounts"
            ds.Tables(1).TableName = "tblCount"
            rowCount = ds.Tables("tblCount").Rows(0)(0)
            Return ds
        Catch ex As Exception
            Throw ex 'ceptionUtil.CreateSoapException("Exception in spContacts_GetSearchAccounts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    Public Shared Function MS_GetFavouriteSuppliers(ByVal bizDivId As String, ByVal BuyerCompId As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim ProcedureName As String = "spMSContacts_GetFavouriteSuppliers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", bizDivId)
            cmd.Parameters.AddWithValue("@BuyerCompId", BuyerCompId)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contacts")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContacts"
            ds.Tables(1).TableName = "tblCount"
            If Not IsDBNull(ds.Tables("tblCount").Rows(0)(0)) Then
                rowCount = ds.Tables("tblCount").Rows(0)(0)
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMSContacts_GetFavouriteSuppliers. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    Public Shared Function MS_GetSuppliers(ByVal BuyerCompanyId As Integer, ByVal paramBizDivId As Integer, ByVal SkillSet As String, ByVal Region As String, ByVal PostCode As String, ByVal Keyword As String, ByVal StatusId As String, ByVal fName As String, ByVal lName As String, ByVal companyName As String, ByVal email As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim i As Integer
            If SkillSet <> "" Then
                'Remove Product - Other option

                SkillSet = SkillSet.Replace("782", "")
                SkillSet = SkillSet.Replace(",,", ",")
                If SkillSet.StartsWith(",") = True Then
                    SkillSet = SkillSet.Substring(1, SkillSet.Length - 1)
                End If
                If SkillSet.EndsWith(",") = True Then
                    SkillSet = SkillSet.Substring(0, SkillSet.Length - 1)
                End If

                If SkillSet <> "" Then
                    Dim strSkills() As String = Split(SkillSet, ",")
                    SkillSet = ""
                    For i = 0 To strSkills.GetLength(0) - 1
                        If SkillSet <> "" Then
                            SkillSet &= ","
                        End If
                        SkillSet &= "'" & CStr(strSkills.GetValue(i)) & "'"
                    Next
                End If
            End If

            If Region <> "" Then
                Dim strRegionCode() As String = Split(Region, ",")
                Region = ""
                For i = 0 To strRegionCode.GetLength(0) - 1
                    If Region <> "" Then
                        Region &= ","
                    End If
                    Region &= "'" & CStr(strRegionCode.GetValue(i)) & "'"
                Next
            End If
            If Keyword.IndexOf("'") <> -1 Then
                Keyword = Keyword.Replace("'", "")
            End If
            Dim ProcedureName As String = "spMS_Contacts_GetSearchSupplier"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            cmd.Parameters.AddWithValue("@keyword", Keyword)
            cmd.Parameters.AddWithValue("@PostCode", PostCode)
            cmd.Parameters.AddWithValue("@SkillSet", SkillSet)
            cmd.Parameters.AddWithValue("@RegionCode", Region)
            cmd.Parameters.AddWithValue("@StatusId", StatusId)
            cmd.Parameters.AddWithValue("@firstName", fName)
            cmd.Parameters.AddWithValue("@lastName", lName)
            cmd.Parameters.AddWithValue("@companyName", companyName)
            cmd.Parameters.AddWithValue("@Email", email)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@BuyerCompId", BuyerCompanyId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchAccounts"
            ds.Tables(1).TableName = "tblCount"
            rowCount = ds.Tables("tblCount").Rows(0)(0)
            Return ds
        Catch ex As Exception
            Throw ex 'ceptionUtil.CreateSoapException("Exception in spContacts_GetSearchAccounts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function MS_RemoveSupplierFromFavList(ByVal bizDivId As String, ByVal BuyerCompId As String, ByVal supplierCompanyids As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim ProcedureName As String = "spMS_Contacts_RemoveSupplierFromFavList"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", bizDivId)
            cmd.Parameters.AddWithValue("@BuyerCompId", BuyerCompId)
            cmd.Parameters.AddWithValue("@SupplierCompIds", supplierCompanyids)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Status")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMS_Contacts_RemoveSupplierFormFavList. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetInsuranceDetails(ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivId As Integer, ByVal SortExpression As String, ByVal StartRowIndex As String, ByVal MaxRows As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetInsuranceDetailsForSuppliers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", MaxRows)
            cmd.ExecuteNonQuery()
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("InsuranceDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblInsuranceDetails"
            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function AddUpdateReferenceCheck(ByVal xmlStr As String, ByVal ContactID As Integer, ByVal RatedCompany As Integer, ByVal RatedContact As Integer, ByVal RatingCompany As Integer, ByVal RatingContact As Integer, ByVal ClassID As Integer, ByVal BizDivID As Integer, ByVal IsApprove As Boolean) As Boolean
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_Update_ContactRefCheck"
            Dim cmd As New SqlCommand
            cmd = New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@RatedCompany", RatedCompany)
            cmd.Parameters.AddWithValue("@RatedContact", RatedContact)
            cmd.Parameters.AddWithValue("@RatingCompany", RatingCompany)
            cmd.Parameters.AddWithValue("@RatingContact", RatingContact)

            cmd.Parameters.AddWithValue("@Status", ClassID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@IsApprove", IsApprove)

            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value
            ' theTrans.Commit()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Update_ContactRefCheck. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will return the reference check form fields for a particular contact.
    ''' </summary>
    ''' <param name="CompanyID"></param>
    ''' <param name="ContactId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>    
    Public Shared Function GetRefCheckData(ByVal CompanyID As Integer, ByVal ContactId As Integer, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_ReferenceCheck"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            'Dim RegionId As String
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)


            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "ContactAttributes"
            ds.Tables(1).TableName = "ContactAttachInsur"
            ds.Tables(2).TableName = "ContactReferences"
            ds.Tables(3).TableName = "ContactRatingsRC"
            ds.Tables(4).TableName = "ContactApproval"
            ds.Tables(5).TableName = "ContactEmail"
            ds.Tables(6).TableName = "EmployeeLiabiliyAttachment"
            ds.Tables(7).TableName = "PublicLiabilityAttachment"
            ds.Tables(8).TableName = "ProfIndemnityAttachment"

            Return ds
        Catch ex As Exception
            'Throw ExceptionUtil.CreateSoapException("Exception in spContacts_ReferenceCheck. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function MS_AddSupplierToFavList(ByVal bizDivId As String, ByVal BuyerCompId As String, ByVal supplierCompanyids As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim ProcedureName As String = "spMS_Contacts_AddSupplierToFavList"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", bizDivId)
            cmd.Parameters.AddWithValue("@BuyerCompId", BuyerCompId)
            cmd.Parameters.AddWithValue("@SupplierCompIds", supplierCompanyids)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Status")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMS_Contacts_RemoveSupplierFormFavList. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method for fetching web content of all type
    ''' </summary>
    ''' <param name="showOnSite"></param>
    ''' <param name="type"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="recordCount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAllWebContent(ByVal showOnSite As Boolean, ByVal type As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                               ByVal maximumRows As Integer, ByRef recordCount As System.Nullable(Of Integer)) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetAllWebContent"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Type", type)
            cmd.Parameters.AddWithValue("@ShowOnSite", showOnSite)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WebContents")
            da.Fill(ds)
            ds.Tables(0).TableName = "WebContent"
            recordCount = ds.Tables(1).Rows(0).Item("TotalRecs")
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function


    Public Shared Function ConvertBuyerToSupplier(ByVal xmlStr As String, ByVal comapnyID As Integer, ByVal contactId As Integer, ByVal BizDivID As Integer) As Boolean
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_BuyerToSupplier"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@CompanyID", comapnyID)
            cmd.Parameters.AddWithValue("@ContactID", contactId)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            ' Return cmd.ExecuteNonQuery()

            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function



    Public Shared Function IsAddedUser(ByVal Email As String) As Boolean
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_IsAddedUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Email", Email)
            cmd.Parameters.AddWithValue("@IsAddedUser", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@IsAddedUser").Value
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function



    ''' <summary>
    ''' This webmethod returns accounts activation status with contactid and maincontactid
    ''' </summary>
    ''' <param name="userName"></param>
    ''' <param name="password"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns>Dataset with table value as ContactID, MainContactId, bizDivId</returns>
    ''' <remarks></remarks>
    Public Shared Function GetAccountActivationStatus(ByVal userName As String, ByVal password As String, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetActivationInfo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", userName)
            cmd.Parameters.AddWithValue("@password", password)
            cmd.Parameters.AddWithValue("@bizDivId", BizDivId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AccountStatus")
            da.Fill(ds)
            ds.Tables(0).TableName = "Account"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetAccountActivationStatus. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns accounts activation status with contactid and maincontactid
    ''' </summary>
    ''' <param name="userName"></param>
    ''' <param name="password"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns>Dataset with table value as ContactID, MainContactId, bizDivId</returns>
    ''' <remarks></remarks>
    Public Shared Function UpdateRegistrantDetails(ByVal RegID As Integer) As Integer
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_UpdateRegistrantEmailStatus"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RegID", RegID)

            cmd.Parameters.AddWithValue("@Status", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Status").Value

            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to Add Registrant Details to the DB called from UCWhitePaperDownloads
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SaveRegistrantDetails(ByVal FName As String, ByVal LName As String, ByVal Email As String, ByVal CName As String, ByVal Position As String, ByVal Phone As String, ByVal MarketingActivity As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_SaveRegistrantDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@FName", FName)
            cmd.Parameters.AddWithValue("@LName", LName)
            cmd.Parameters.AddWithValue("@Email", Email)
            cmd.Parameters.AddWithValue("@CName", CName)
            cmd.Parameters.AddWithValue("@Position", Position)
            cmd.Parameters.AddWithValue("@Phone", Phone)
            cmd.Parameters.AddWithValue("@MarketingActivity", MarketingActivity)
            'cmd.Parameters.AddWithValue("@RegID", SqlDbType.Int).Direction = ParameterDirection.Output
            'cmd.ExecuteNonQuery()
            'Return cmd.Parameters("@RegID").Value
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("RegistrantDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblRegistrantDetails"
            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Check depot location
    ''' </summary>
    ''' <param name="LocationId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CheckDepotLocation(ByVal LocationId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_Contacts_CheckDepotLocation"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@LocationId", LocationId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "UserData"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetMesssagesListing(ByVal ForContactClassID As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByVal Type As Integer, ByVal Year As Integer, ByVal Month As Integer, ByVal CompanyId As Integer)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetGeneralMessagesListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ForContactClassID", ForContactClassID)
            cmd.Parameters.AddWithValue("@Status", ApplicationSettings.WOStatusID.Accepted)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@Type", Type)
            cmd.Parameters.AddWithValue("@Month", Month)
            cmd.Parameters.AddWithValue("@Year", Year)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "MsgsDetails"
            ds.Tables(1).TableName = "tblRecords"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function SaveFeedback(ByVal Feedback As String, ByVal CompanyID As Integer, ByVal ContactID As Integer, ByVal ContactClassID As Integer, ByVal WOID As Integer, ByVal TrackingID As Integer, ByVal Type As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_PostMesagesOrFeedback"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@MsgID", 0)
            cmd.Parameters.AddWithValue("@Subject", "")
            cmd.Parameters.AddWithValue("@Message", Feedback)
            cmd.Parameters.AddWithValue("@ContactClassID", ContactClassID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@ForContactClassID", 0)
            cmd.Parameters.AddWithValue("@ForCompanyID", 0) 'Passing 0 for now
            cmd.Parameters.AddWithValue("@Status", 0)
            cmd.Parameters.AddWithValue("@WOID", 0)
            cmd.Parameters.AddWithValue("@WOTrackingID", 0)
            cmd.Parameters.AddWithValue("@Type", Type)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSuccess"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Function to SP Holiday Info
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSPHoliday(ByVal CompanyId As Integer, ByVal WorkingDays As String, ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetSPWorkingDays"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyId)
            cmd.Parameters.AddWithValue("@WorkingDays", WorkingDays)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SPHolidays")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSPHolidaySummary"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetStandards(ByVal Label As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spLookUp_GetStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Label", Label)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "StandardsGeneral"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function GetHolidayListing(ByVal SortExpression As String, ByVal StartRowIndex As String, ByVal MaxRows As String, ByVal showAll As Boolean, ByVal src As String, ByVal ContactId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetHolidayListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@showAll", showAll)
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", MaxRows)
            cmd.Parameters.AddWithValue("@src", src)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "HolidaysListing"
            ds.Tables(1).TableName = "tblRecords"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function AddUpdateHolidays(ByVal DateID As Integer, ByVal HolidayDateFrom As String, ByVal Holidaytxt As String, ByVal IsDeleted As Boolean, ByVal ContactId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_AddUpdateHolidays"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@DateID", DateID)
            cmd.Parameters.AddWithValue("@HolidayTxt", Holidaytxt)
            cmd.Parameters.AddWithValue("@HolidayDateString", HolidayDateFrom)
            cmd.Parameters.AddWithValue("@IsDeleted", IsDeleted)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function UpdateQuickCompanyAdminDetails(ByVal FName As String, ByVal LName As String, ByVal LocationName As String, ByVal Address As String, ByVal City As String, ByVal State As String, ByVal CountryId As Integer, ByVal Email As String, ByVal AltEmail As String, ByVal Phone As String, ByVal Mobile As String, ByVal Fax As String, ByVal ContactId As Integer, ByVal AddressID As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spContacts_UpdateQuickCompanyAdminDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@FName", FName)
            cmd.Parameters.AddWithValue("@LName", LName)
            cmd.Parameters.AddWithValue("@LocationName", LocationName)
            cmd.Parameters.AddWithValue("@Address", Address)
            cmd.Parameters.AddWithValue("@City", City)
            cmd.Parameters.AddWithValue("@State", State)
            cmd.Parameters.AddWithValue("@CountryId", CountryId)
            cmd.Parameters.AddWithValue("@Email", Email)
            cmd.Parameters.AddWithValue("@AltEmail", AltEmail)
            cmd.Parameters.AddWithValue("@Phone", Phone)
            cmd.Parameters.AddWithValue("@Mobile", Mobile)
            cmd.Parameters.AddWithValue("@Fax", Fax)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@AddressID", AddressID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function UpdateIPAddress(ByVal IpAddress As String, ByVal HostName As String, ByVal URL As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "SPWebSiteIPTracker"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IPAddress", IpAddress)
            cmd.Parameters.AddWithValue("@Hostname", HostName)
            cmd.Parameters.AddWithValue("@URL", URL)
            

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function UpdatePasswordEncryption(ByVal xmlPasswordEncryption As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spAdmin_UpdatePasswordEncryption"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlPasswordEncryption", xmlPasswordEncryption)
            cmd.CommandTimeout = 0
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetBookingForm(ByVal CompanyId As Integer, ByVal RoleGroupId As Integer, Optional ByVal ContactId As Integer = 0) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spAdmin_GetBookingFormCompanyAndUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@RoleGroupId", RoleGroupId)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            'cmd.Parameters.AddWithValue("@BusinessId", OrderWorkLibrary.ApplicationSettings.BusinessId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblBookingForm"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function UpdateContactImage(ByVal ContactId As Integer, ByVal ImageUrl As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spUpdateContactImage"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@imageUrl", ImageUrl)
            cmd.CommandTimeout = 0
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function UpdateUserProfileCompletionMailInfo(ByVal ContactId As Integer, ByVal IsProfileCompleted As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spContacts_UpdateIsProfileCompleted"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@IsProfileCompleted", IsProfileCompleted)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.CommandTimeout = 0
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetProfileCompleteData(ByVal ContactID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spContacts_GetProfileCompleteData"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblData"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

End Class


