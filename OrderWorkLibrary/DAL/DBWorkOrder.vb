Imports System.Data.SqlClient

''' <summary>
''' 
''' </summary>
''' <remarks></remarks>
Public Class DBWorkOrder

    ''' <summary>
    ''' This method will return the Work Orders Listing for Admin.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="Group"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAdminWOListing(ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                               ByVal maximumRows As Integer, ByVal Group As String, ByVal CompanyID As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetAdminWOListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Group", Group)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsLisitng")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrdersListing"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This method will return the Work Orders Listing for Buyer.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="Group"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetBuyerWOListing(ByVal Group As String, ByVal CompanyID As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer, _
                    ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                    ByVal maximumRows As Integer, ByVal BillLoc As Integer, ByVal WorkOrderID As String, ByVal BusinessArea As Integer, ByVal MyWorkorders As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_GetBuyerWOListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Group", Group)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            Else
                cmd.Parameters.AddWithValue("@FromDate", "")
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            Else
                cmd.Parameters.AddWithValue("@ToDate", "")
            End If

            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@WorkOrderID", WorkOrderID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@BillLoc", BillLoc)
            cmd.Parameters.AddWithValue("@BusinessArea", BusinessArea)
            cmd.Parameters.AddWithValue("@MyWorkorders", MyWorkorders)
            cmd.Parameters.AddWithValue("@LoggedInUserID", HttpContext.Current.Session("PortalUserId"))

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsLisitng")
            da.Fill(ds)
            'HttpContext.Current.Session("dsBuyerWOListing") = ds
            ds.Tables(0).TableName = "tblWorkOrdersListing"
            ds.Tables(1).TableName = "tblCount"
            ds.Tables(2).TableName = "tblExportToExcel"

            If ds.Tables.Count > 3 Then
                ds.Tables(3).TableName = "tblWOUserNotes"
                HttpContext.Current.Items("tblWOUserNotes") = ds.Tables("tblWOUserNotes")

            End If
            HttpContext.Current.Items("TotalCount") = ds.Tables("tblCount").Rows(0).Item(0)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function SelectBuyerWOListingCount(ByVal Group As String, ByVal CompanyID As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer, ByVal BillLoc As Integer, ByVal WorkOrderID As String, ByVal BusinessArea As Integer, ByVal MyWorkorders As String) As Integer
        Return HttpContext.Current.Items("TotalCount")
    End Function

    ''' <summary>
    ''' This method will return the Work Orders Listing for Buyer.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="Group"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSupplierWOListing(ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                               ByVal maximumRows As Integer, ByVal Group As String, ByVal CompanyID As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer, ByVal Rating As Integer, ByVal WorkOrderID As String, ByVal BusinessArea As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_GetSupplierWOListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Group", Group)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            'If FromDate <> "" Then
            '    cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            'Else
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            'End If
            'If ToDate <> "" Then
            '    cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            'Else
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            'End If

            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@Rating", Rating)
            cmd.Parameters.AddWithValue("@WorkOrderID", WorkOrderID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@BusinessArea", BusinessArea)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsLisitng")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrdersListing"
            ds.Tables(1).TableName = "tblCount"
            ds.Tables(2).TableName = "tblExportToExcel"

            If ds.Tables.Count > 3 Then
                ds.Tables(3).TableName = "tblWOUserNotes"
                HttpContext.Current.Items("tblWOUserNotes") = ds.Tables("tblWOUserNotes")
            End If
            If ds.Tables.Count > 4 Then
                ds.Tables(4).TableName = "tblWONotes"
                HttpContext.Current.Items("tblWONotes") = ds.Tables("tblWONotes")
            End If

            HttpContext.Current.Items("TotalCount") = ds.Tables("tblCount").Rows(0).Item(0)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function SelectSupplierWOListingCount(ByVal Group As String, ByVal CompanyID As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer, ByVal Rating As Integer, ByVal WorkOrderID As String, ByVal BusinessArea As Integer) As Integer
        Return HttpContext.Current.Items("TotalCount")
    End Function
    ''' <summary>
    ''' Webmethod to return the Work Orders Details in the admin site.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAdminWODetails(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetAdminWODetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", HttpContext.Current.Session("PortalUserId"))

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblLastAction"
            ds.Tables(4).TableName = "tblCADetails"
            ds.Tables(5).TableName = "tblComments"
            ds.Tables(6).TableName = "tblAttachments"
            ds.Tables(7).TableName = "tblWOLoc"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Webmethod to return the Work Orders Details in the site.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetBuyerWODetails(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetBuyerWODetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            'ds.Tables(3).TableName = "tblLastAction"
            'ds.Tables(4).TableName = "tblCADetails"
            ds.Tables(3).TableName = "tblComments"
            ds.Tables(4).TableName = "tblAttachments"
            ds.Tables(5).TableName = "tblWOLoc"
            ds.Tables(6).TableName = "tblActionHistory"
            ds.Tables(7).TableName = "tblWorkOrderNotes"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Webmethod to return the Work Orders Details in the site.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSupplierWODetails(ByVal WOID As Integer, ByVal CompanyID As Integer, Optional ByVal prodID As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetSupplierWODetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            If prodID = "" Then
                prodID = 0
            End If
            If prodID = "Buyer" Or prodID = "Supplier" Then
                cmd.Parameters.AddWithValue("@Viewer", prodID)
            Else
                cmd.Parameters.AddWithValue("@prodID", prodID)
            End If
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblLastAction"
            ds.Tables(4).TableName = "tblCADetails"
            ds.Tables(5).TableName = "tblComments"
            ds.Tables(6).TableName = "tblAttachments"
            ds.Tables(7).TableName = "tblWOLoc"
            ds.Tables(8).TableName = "tblActionHistory"
            ds.Tables(9).TableName = "IsAuthorised"
            ds.Tables(10).TableName = "tblClientLogo"
            ds.Tables(11).TableName = "tblWorkOrderNotes"
            ds.Tables(12).TableName = "tblQA"

            'ds.Tables(13).TableName = "tblWORating"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function GetDixonsEscalationDetails(ByVal ComplaintId As Integer, ByVal CompanyId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetDixonsEscalationDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ComplaintId", ComplaintId)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("EscalationDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblClientLogo"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Webmethod to return the Work Orders Details in the site.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSupplierWODetailsNew(ByVal WOID As Integer, ByVal CompanyID As Integer, Optional ByVal prodID As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetSupplierWODetails1"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            If prodID = "" Then
                prodID = 0
            End If
            If prodID = "Buyer" Or prodID = "Supplier" Then
                cmd.Parameters.AddWithValue("@Viewer", prodID)
            Else
                cmd.Parameters.AddWithValue("@prodID", prodID)
            End If
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblLastAction"
            ds.Tables(3).TableName = "tblCADetails"
            ds.Tables(4).TableName = "tblComments"
            ds.Tables(5).TableName = "tblAttachments"
            ds.Tables(6).TableName = "tblActionHistory"
            ds.Tables(7).TableName = "tblClientLogo"
            ds.Tables(8).TableName = "tblWorkOrderNotes"
            ds.Tables(9).TableName = "tblQA"
            If ds.Tables.Count > 10 Then
                ds.Tables(10).TableName = "tblAudit"
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method is used to delete/discard/copy wo as draft
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="ContactID"></param>
    ''' <param name="Action"></param>
    ''' <param name="TrackCompanyID"></param>
    ''' <param name="TrackContactID"></param>
    ''' <param name="TrackContactClassID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function WO_DiscardDelCopy(ByVal WOID As Integer, ByVal BizDivId As Integer, ByVal CompanyID As Integer, ByVal ContactID As Integer, ByVal Action As String, ByVal TrackCompanyID As Integer, ByVal TrackContactID As Integer, ByVal TrackContactClassID As Integer, ByVal LoggedInUserID As Integer, Optional ByVal WOVersionNo As Byte() = Nothing, Optional ByVal WOTrackingVNo As Byte() = Nothing) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_DiscardDelCopy"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@TrackCompanyID", TrackCompanyID)
            cmd.Parameters.AddWithValue("@TrackContactID", TrackContactID)
            cmd.Parameters.AddWithValue("@TrackContactClassID", TrackContactClassID)
            cmd.Parameters.AddWithValue("@Action", Action)
            cmd.Parameters.AddWithValue("@WOVersionNo", WOVersionNo)
            cmd.Parameters.AddWithValue("@WOTrackingVersionNo", WOTrackingVNo)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            Dim ds As New DataSet("WODiscardDelCopy")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            If ds.Tables.Count <> 0 Then
                ds.Tables(0).TableName = "tblWOID"
                ds.Tables(1).TableName = "tblSendMail"
                ds.Tables(2).TableName = "tblDiscardMail"
            End If

            Return ds

            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Function to get the standard values to populate the RAQ form
    ''' 1. Security Q/A
    ''' 2. Workorder types
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function woRAQGetStandards(ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spWO_RAQWorkorder_GetStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("RAQ")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrderTypes"
            ds.Tables(1).TableName = "tblWorkOrderSubTypes"
            ds.Tables(2).TableName = "tblSecurityQuestion"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woRAQGetStandards. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will return the Work order details for Create Similar WO.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function woGetDetails_SampleWO(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spWO_SampleWO_Populate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SampleWO")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrder"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woGetDetails_SampleWO. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO details for Sample WO. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Save the Contact info and workorder details created using the RAQ form
    ''' </summary>
    ''' <param name="xmlContact"></param>
    ''' <param name="xmlWorkOrder"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOAddRAQDetails(ByVal xmlContact As String, ByVal xmlWorkOrder As String, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_RAQSave"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlContact", xmlContact)
            cmd.Parameters.AddWithValue("@xmlWorkOrder", xmlWorkOrder)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim ds As New DataSet("WONotification")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSendMail"
            ds.Tables(1).TableName = "tblWorkOrder"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woAddRAQDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the RAQ details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    Public Shared Function GetSearchWorkOrders(ByVal BizDivId As Integer, ByVal WOID As String, ByVal SubmittedDate As String, ByVal PONumber As String, ByVal JRSNumber As String, ByVal CustomerNumber As String, ByVal CompanyName As String, ByVal Keyword As String, ByVal WorkOrderId As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            If Keyword.IndexOf("'") <> -1 Then
                Keyword = Keyword.Replace("'", "")
            End If
            Dim ProcedureName As String = "spMS_WO_SearchWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            ' OWDEV-787 -Performance Issue 1 - Search WO, add the followign line as per sabita
            cmd.CommandTimeout = 1800

            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            If SubmittedDate <> "" Then
                cmd.Parameters.AddWithValue("@SubmittedDate", CommonFunctions.convertDate(SubmittedDate))
            Else
                cmd.Parameters.AddWithValue("@SubmittedDate", SubmittedDate)
            End If
            cmd.Parameters.AddWithValue("@Keyword", Keyword)
            cmd.Parameters.AddWithValue("@PONumber", PONumber)
            cmd.Parameters.AddWithValue("@JRSNumber", JRSNumber)
            cmd.Parameters.AddWithValue("@CustomerNumber", CustomerNumber)
            cmd.Parameters.AddWithValue("@WorkOrderId", WorkOrderId)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyName", CompanyName)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@LoggedInUserID", HttpContext.Current.Session("PortalUserId"))

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchWorkOrders"
            ds.Tables(1).TableName = "tblCount"
            rowCount = ds.Tables("tblCount").Rows(0)(0)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_GetSearchAccounts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Web method to return the locations dataset associated for the contact
    ''' </summary>
    ''' <param name="companyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function woGetContactsLocations(ByVal companyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spWO_GetContactsLocations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", companyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrderLoc")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblLocations"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Web method to return the standard data need for create workorder form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOCreateWOGetStandards(ByVal BizDivID As Integer, ByVal ContactID As Integer, ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WO_CreateWorkOrder_GetStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("CreateWorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCountries"
            ds.Tables(1).TableName = "tblWorkOrderTypes"
            ds.Tables(2).TableName = "tblWorkOrderSubTypes"
            ds.Tables(3).TableName = "tblDressCode"
            ds.Tables(4).TableName = "tblAttachmentsCompany"
            ds.Tables(5).TableName = "tblAttachmentsWO"
            ds.Tables(6).TableName = "tblAttachmentsWOStatement"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Web method to return the standard data need for create workorder form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOCreateWOGetDetails(ByVal ContactID As Integer, ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WO_CreateWOFormDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("CreateWorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAttachmentsCompany"
            ds.Tables(1).TableName = "tblAttachmentsWO"
            ds.Tables(2).TableName = "tblLocDetails"
            ds.Tables(3).TableName = "tblProductsList"
            ds.Tables(4).TableName = "tblContactsSettings"
            If ds.Tables.Count > 6 Then
                ds.Tables(6).TableName = "tblHolidays"
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will return the Work order details for Edit WO.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOEditWOGetDetails(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WO_EditWO_Populate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("EditWO")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblWODetails"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woEditWOGetDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO details for Edit workorderform. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function MS_WOEditWOGetDetailsBTBD(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WO_EditWO_PopulateBTBD"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("EditWO")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            'ds.Tables(1).TableName = "tblWODetails"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woEditWOGetDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO details for Edit workorderform. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will return the Work order details for Edit WO.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOFormEditWOGetDetails(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WOFORM_EditWOPopulate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("EditWO")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWODetails"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woEditWOGetDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO details for Edit workorderform. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Function to add / update the workorder details from the Create workorder from
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="doMode"></param>
    ''' <param name="EditedBy"></param>
    ''' <param name="AdminCompID"></param>
    ''' <param name="AdminConID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOAddUpdateWorkOrderDetails(ByVal xmlStr As String, ByVal doMode As String, ByVal EditedBy As String, ByVal AdminCompID As Integer, ByVal AdminConID As Integer, ByVal ActionBy As String, ByVal ProductID As String, ByVal VersionNo As Byte(), Optional ByVal ClientQAnsId As Integer = 0, Optional ByVal ReceiptNumber As String = "", Optional ByVal ClientScope As String = "", Optional ByVal LoggedInUserID As Integer = 0, Optional ByVal ResourceCoveredBy As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_CreateWorkOrder_Save"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@EditedBy", EditedBy)
            cmd.Parameters.AddWithValue("@AdminCompID", AdminCompID)
            cmd.Parameters.AddWithValue("@AdminConID", AdminConID)
            cmd.Parameters.AddWithValue("@ActionBy", ActionBy)
            cmd.Parameters.AddWithValue("@ProductID", ProductID)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@ReceiptNumber", ReceiptNumber)
            cmd.Parameters.AddWithValue("@ClientQAnsId", ClientQAnsId)
            cmd.Parameters.AddWithValue("@ClientScope", ClientScope)
            cmd.Parameters.AddWithValue("@ResourceCoveredBy", ResourceCoveredBy)

            Dim ds As New DataSet("WONotification")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSendMail"
            If ds.Tables.Count > 1 Then
                ds.Tables(1).TableName = "tblWorkOrder"
                If ds.Tables.Count > 2 Then
                    ds.Tables(2).TableName = "tblSuppliers"
                End If
            End If
            ds.Tables(ds.Tables.Count - 1).TableName = "EmailStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_CreateWorkOrder_Save. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the workorder data. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will return the balance and WO Listing Details for particular company and contact of type buyer
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function woBuyWorkSummary(ByVal BizDivID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spWO_BuyWorkSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_SupplyWorkSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the dataset of WO summary with amount balance for Buy work order summary
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns>returns dataset with 3 tables with third table as account balance</returns>
    ''' <remarks></remarks>
    Public Shared Function MS_woBuyWorkSummary(ByVal BizDivID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_BuyWorkSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_SupplyWorkSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the Workorder summary for ordermatch
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOOrderMatchGetSummary(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WO_OrderMatch_GetWOSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWOSummary"
            If ds.Tables.Count > 2 Then
                ds.Tables(2).TableName = "tblGeoCode"
            Else
                ds.Tables(1).TableName = "tblGeoCode"
            End If

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woOrderMatchGetSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO Summary for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Web method to return the standard data need for Ordematch form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function woOrderMatchGetStandards(ByVal CountryID As Integer, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spWO_Ordermatch_GetStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CountryID", CountryID)
            'cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Ordermatch")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblRegionCodes"
            'ds.Tables(1).TableName = "tblWorkOrderTypes"

            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woOrderMatchGetStandards. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the supplier list for ordermatch
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function woOrderMatchGetSuppliers(ByVal CompanyName As String, ByVal Keyword As String, ByVal PostCode As String, ByVal NoOfEmployees As String, ByVal ShowSupplierWithCW As Boolean, ByVal Skills As String, ByVal WOID As Integer, ByVal StatusID As String, ByVal RegionCode As String, ByVal vendorIds As String, ByVal MinWOValue As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer, ByRef checked As Object, ByVal CRBCheck As String, ByVal UKSecurity As String, ByVal FavSupplier As Boolean) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim i As Integer
            If Skills <> "" Then
                Skills = Skills.Replace("782", "")
                Skills = Skills.Replace(",,", ",")
                If Skills.StartsWith(",") = True Then
                    Skills = Skills.Substring(1, Skills.Length - 1)
                End If
                If Skills.EndsWith(",") = True Then
                    Skills = Skills.Substring(0, Skills.Length - 1)
                End If
            End If

            If Skills <> "" Then
                Dim strSkills() As String = Split(Skills, ",")
                Skills = ""
                For i = 0 To strSkills.GetLength(0) - 1
                    If Skills <> "" Then
                        Skills &= ","
                    End If
                    Skills &= "'" & CStr(strSkills.GetValue(i)) & "'"
                Next
            End If

            If RegionCode <> "" Then
                Dim strRegionCodeCode() As String = Split(RegionCode, ",")
                RegionCode = ""
                For i = 0 To strRegionCodeCode.GetLength(0) - 1
                    If RegionCode <> "" Then
                        RegionCode &= ","
                    End If
                    RegionCode &= "'" & CStr(strRegionCodeCode.GetValue(i)) & "'"
                Next
            End If

            If vendorIds <> "" Then
                Dim strVendorIds() As String = Split(vendorIds, ",")
                vendorIds = ""
                For i = 0 To strVendorIds.GetLength(0) - 1
                    If vendorIds <> "" Then
                        vendorIds &= ","
                    End If
                    vendorIds &= "'" & CStr(strVendorIds.GetValue(i)) & "'"
                Next
            End If
            If Keyword <> "" Then
                If Keyword.IndexOf("'") <> -1 Then
                    Keyword = Keyword.Replace("'", "")
                End If
            End If
            If CompanyName <> "" Then
                If CompanyName.IndexOf("'") <> -1 Then
                    CompanyName = CompanyName.Replace("'", "")
                End If
            End If
            Dim ProcedureName As String
            ProcedureName = "spWO_Ordermatch_GetSuppliers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@SkillSet", Skills)

            cmd.Parameters.AddWithValue("@CompanyName", CompanyName)
            cmd.Parameters.AddWithValue("@NoOfEmployees", NoOfEmployees)
            cmd.Parameters.AddWithValue("@ShowSupplierWithCW", ShowSupplierWithCW)
            cmd.Parameters.AddWithValue("@vendorIds", vendorIds)


            cmd.Parameters.AddWithValue("@keyword", Keyword)
            cmd.Parameters.AddWithValue("@PostCode", PostCode)
            cmd.Parameters.AddWithValue("@RegionCode", RegionCode)
            cmd.Parameters.AddWithValue("@MinWOValue", MinWOValue)
            cmd.Parameters.AddWithValue("@StatusId", StatusID)
            cmd.Parameters.AddWithValue("@CRBCheck", CRBCheck)
            cmd.Parameters.AddWithValue("@UKSecurity", UKSecurity)
            cmd.Parameters.AddWithValue("@FavSupp", FavSupplier)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchSuppliers"
            ds.Tables(1).TableName = "tblCount"
            ds.Tables(2).TableName = "tblPostCode"
            rowCount = ds.Tables("tblCount").Rows(0)(0)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woOrderMatchGetSuppliers. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning list of suppliers for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method will insert the coordinates of the post code in the tblGeoCodes table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DoesPostCodeExists(ByVal Postcode As String)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim rowCount As Boolean
            Dim ProcedureName As String
            ProcedureName = "sp_InsertPostCodeCheck"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Postcode", Postcode)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCount"
            rowCount = CBool(ds.Tables("tblCount").Rows(0)(0))
            Return rowCount
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in InsertPostCodeCoordinates. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while inserting the values. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method will insert the coordinates of the post code in the tblGeoCodes table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertPostCodeCoordinates(ByVal xmlContent As String)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String
            ProcedureName = "sp_InsertPostCodeCoordinates"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlContent", xmlContent)
            Dim da As New SqlDataAdapter(cmd)
            cmd.ExecuteNonQuery()
            Return ""
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in InsertPostCodeCoordinates. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while inserting the values. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This method will make entries for OW Rep and each supplier to whom the WO is sent and return all those Supplier contacts to whom the notification email of WO needs to be sent.
    ''' </summary>
    ''' <param name="SupContactIDs"></param>
    ''' <param name="WOID"></param>
    ''' <param name="DateStart"></param>
    ''' <param name="DateEnd"></param>
    ''' <param name="EstimatedTime"></param>
    ''' <param name="Value"></param>
    ''' <param name="SpecialistSuppliesParts"></param>
    ''' <param name="ActualTime"></param>
    ''' <param name="OWCompanyID"></param>
    ''' <param name="OWRepID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SendWO(ByVal SupContactIDs As String, ByVal WOID As Integer, ByVal DateStart As String, ByVal DateEnd As String, ByVal EstimatedTime As String, ByVal Value As Decimal, ByVal SpecialistSuppliesParts As Boolean, ByVal ActualTime As String, ByVal OWCompanyID As Integer, ByVal OWRepID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try

            Dim ProcedureName As String = "spWO_OrderMatch_SendWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@SupContactIDs", SupContactIDs)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            If DateStart <> "" Then
                cmd.Parameters.AddWithValue("@DateStart", CommonFunctions.convertDate(DateStart))
            Else
                cmd.Parameters.AddWithValue("@DateStart", DateStart)
            End If
            If DateEnd <> "" Then
                cmd.Parameters.AddWithValue("@DateEnd", CommonFunctions.convertDate(DateEnd))
            Else
                cmd.Parameters.AddWithValue("@DateEnd", DateEnd)
            End If

            cmd.Parameters.AddWithValue("@EstimatedTime", EstimatedTime)
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@SpecialistSuppliesParts", SpecialistSuppliesParts)
            cmd.Parameters.AddWithValue("@ActualTime", ActualTime)
            cmd.Parameters.AddWithValue("@OWCompanyID", OWCompanyID)
            cmd.Parameters.AddWithValue("@OWRepID", OWRepID)
            cmd.Parameters.AddWithValue("@NearestTo", "No")
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contacts")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSuppliers"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_OrderMatch_SendWO. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Work Orders Histroy.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetBuyerWOHistory(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetBuyerWOHistory"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsHistory")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblActionHistory"
            ds.Tables(4).TableName = "tblComments"
            ds.Tables(5).TableName = "tblAttachments"
            ds.Tables(6).TableName = "tblWOLoc"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Webmethod to return the Work Orders Histroy.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSupplierWOHistory(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetSupplierWOHistory"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsHistory")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblActionHistory"
            ds.Tables(4).TableName = "tblComments"
            ds.Tables(5).TableName = "tblAttachments"
            ds.Tables(6).TableName = "tblWOLoc"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Webmethod to return the Work Orders Histroy.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAdminWOHistory(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetAdminWOHistory"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsHistory")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblActionHistory"
            ds.Tables(4).TableName = "tblComments"
            ds.Tables(5).TableName = "tblAttachments"
            ds.Tables(6).TableName = "tblWOLoc"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the dataset of WO summary with amount balance for supply workorder summary
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns>returns dataset with 3 tables with third table as account balance</returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOSupplyWorkSummary(ByVal BizDivID As Integer, ByVal CompanyID As Integer, ByVal Mode As String, Optional ByVal FromTab As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_SupplyWorkSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@FromTab", FromTab)
            cmd.Parameters.AddWithValue("@BusinessArea", 101)
            cmd.Parameters.AddWithValue("@LoggedInUserID", HttpContext.Current.Session("PortalUserId"))
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_SupplyWorkSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the complete Work Order information.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Action"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="bizDivID"></param>
    ''' <param name="chkValid"></param>
    ''' <param name="Viewer"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetWO(ByVal WOID As Integer, ByVal Action As String, ByVal CompanyID As Integer, ByVal bizDivID As Integer, ByVal chkValid As Boolean, ByVal Viewer As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WO_GetWOProcessDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@SupCompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@Action", Action)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            cmd.Parameters.AddWithValue("@ChkValid", chkValid)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOInfo")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrderSummary"
            ''specialist list
            If Action = "wospcaccept" Or Action = "wospaccept" Or Action = "wospchangeca" Or Action = "woraiseissue" _
                Or Action = "wochangeissue" Then
                If ds.Tables.Count > 1 Then
                    ds.Tables(1).TableName = "tblSpecialists"
                End If
            End If

            'last action 
            If Action = "wospchangeca" Or Action = "wochangeissue" Or Action = "woacceptissue" Then
                ds.Tables(2).TableName = "tblLastAction"
                ds.Tables(3).TableName = "tblComments"
            ElseIf Action = "woacceptca" Or Action = "woacceptissue" Or Action = "wodiscussca" Or Action = "wodiscuss" Then
                ds.Tables(1).TableName = "tblLastAction"
                ds.Tables(2).TableName = "tblComments"
            ElseIf Action = "wocomplete" Then
                If ds.Tables.Count > 1 Then
                    ds.Tables(1).TableName = "tblclientQA"
                    ds.Tables(2).TableName = "tblSpecialists"
                End If
            End If

            If Action = "woraiseissue" Then
                ds.Tables(4).TableName = "tblCommentsRaiseIssue"
            End If

            If Action = "woacceptissue" Then
                ds.Tables(6).TableName = "tblCommentsRaiseIssue"
            End If

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' WebMethod to perform the wo process
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="SupCompanyID"></param>
    ''' <param name="SupContactID"></param>
    ''' <param name="ContactId"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="Action"></param>
    ''' <param name="bizDivID"></param>
    ''' <param name="DateStart"></param>
    ''' <param name="DateEnd"></param>
    ''' <param name="Value"></param>
    ''' <param name="SpecialistSuppliesParts"></param>
    ''' <param name="Comments"></param>
    ''' <param name="SpecialistID"></param>
    ''' <param name="ContactClassId"></param>
    ''' <param name="WOTrackID"></param>
    ''' <param name="DataRef"></param>
    ''' <param name="LastActionRef"></param>
    ''' <param name="Rating"></param>
    ''' <param name="RatingComments"></param>
    ''' <param name="AttachmentsComplete"></param>
    ''' <param name="RatedContactClassId"></param>
    ''' <param name="NewProposedDayRate"></param>
    ''' <param name="NewEstimatedTimeInDays"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_PerformWOProcess(ByVal WOID As Integer, ByVal SupCompanyID As Integer, ByVal SupContactID As Integer, ByVal ContactId As Integer, ByVal CompanyId As Integer, ByVal Action As String, ByVal bizDivID As Integer, ByVal DateStart As String, ByVal DateEnd As String, ByVal Value As Decimal, ByVal Comments As String, ByVal SpecialistID As Integer, ByVal ContactClassId As Integer, ByVal WOTrackID As Integer, ByVal DataRef As Integer, ByVal LastActionRef As Integer, ByVal Rating As Integer, ByVal RatingComments As String, ByVal AttachmentsComplete As String, ByVal RatedContactClassId As Integer, ByVal NewProposedDayRate As Decimal, ByVal NewEstimatedTimeInDays As Integer, ByVal SLA As Integer, Optional ByVal AptTime As String = "", Optional ByVal WOVersionNo As Byte() = Nothing, Optional ByVal WOTrackingVNo As Byte() = Nothing, Optional ByVal xmlQA As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_SaveWOActions"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@WOTrackID", WOTrackID)
            cmd.Parameters.AddWithValue("@DataRef", DataRef)
            cmd.Parameters.AddWithValue("@LastActionRef", LastActionRef)
            cmd.Parameters.AddWithValue("@SupCompanyID", SupCompanyID)
            cmd.Parameters.AddWithValue("@SupContactID", SupContactID)
            cmd.Parameters.AddWithValue("@Action", Action)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            cmd.Parameters.AddWithValue("@xmlQA", xmlQA)
            cmd.Parameters.AddWithValue("@SLA", SLA)
            cmd.Parameters.AddWithValue("@LoggedInUserID", HttpContext.Current.Session("PortalUserId"))
            If DateStart <> "" Then
                cmd.Parameters.AddWithValue("@DateStart", CommonFunctions.convertDate(DateStart))
            End If
            If DateEnd <> "" Then
                cmd.Parameters.AddWithValue("@DateEnd", CommonFunctions.convertDate(DateEnd))
            End If
            If AptTime <> "" Then
                cmd.Parameters.AddWithValue("@AptTime", AptTime)
            End If
            cmd.Parameters.AddWithValue("@Value", Value)
            'cmd.Parameters.AddWithValue("@SpecialistSuppliesParts", SpecialistSuppliesParts)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@SpecialistID", SpecialistID)
            cmd.Parameters.AddWithValue("@ContactClassId", ContactClassId)
            cmd.Parameters.AddWithValue("@Rating", Rating)
            cmd.Parameters.AddWithValue("@RatingComments", RatingComments)
            If AttachmentsComplete <> "" Then
                cmd.Parameters.AddWithValue("@AttachmentXSD", AttachmentsComplete)
            End If
            cmd.Parameters.AddWithValue("@RatedContactClassId", RatedContactClassId)
            If CStr(NewProposedDayRate) = "" Then
                NewProposedDayRate = 0.0
            End If
            cmd.Parameters.AddWithValue("@NewProposedDayRate", NewProposedDayRate)
            If CStr(NewEstimatedTimeInDays) = "" Then
                NewEstimatedTimeInDays = 1
            End If
            cmd.Parameters.AddWithValue("@NewEstimatedTimeInDays", NewEstimatedTimeInDays)
            cmd.Parameters.AddWithValue("@WOVersionNo", WOVersionNo)
            cmd.Parameters.AddWithValue("@WOTrackingVersionNo", WOTrackingVNo)
            Dim ds As New DataSet("WOProcess")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            If ds.Tables.Count <> 0 Then
                If ds.Tables(0).Rows(0).Item("ContactID") <> -1 Or ds.Tables(0).Rows(0).Item("ContactID") <> -10 Or ds.Tables(0).Rows(0).Item("ContactID") <> -11 Then
                    Select Case Action
                        Case "wospaccept", "woacceptca"
                            If ds.Tables.Count = 4 Then
                                ds.Tables(1).TableName = "Mailers"
                                ds.Tables(2).TableName = "LostDiscardSupp"
                                If ds.Tables.Count > 3 Then
                                    ds.Tables(3).TableName = "WOStatus"
                                End If
                            ElseIf ds.Tables.Count = 3 Then
                                ds.Tables(0).TableName = "Mailers"
                                ds.Tables(1).TableName = "LostDiscardSupp"
                                If ds.Tables.Count > 2 Then
                                    ds.Tables(2).TableName = "WOStatus"
                                End If
                            End If
                        Case "woacceptissue"
                            If ds.Tables.Count = 3 Then
                                ds.Tables(1).TableName = "Mailers"
                                ds.Tables(2).TableName = "tblWOStatus"
                            ElseIf ds.Tables.Count = 2 Then
                                ds.Tables(0).TableName = "Mailers"
                                ds.Tables(1).TableName = "tblWOStatus"
                            End If
                        Case "woclose"
                            If ds.Tables.Count = 3 Then
                                ds.Tables(1).TableName = "Mailers"
                                ds.Tables(2).TableName = "tblPayments"
                            ElseIf ds.Tables.Count = 2 Then
                                ds.Tables(0).TableName = "Mailers"
                                ds.Tables(1).TableName = "tblPayments"
                            End If
                        Case Else
                            If ds.Tables.Count = 2 Then
                                ds.Tables(1).TableName = "Mailers"
                            ElseIf ds.Tables.Count = 1 Then
                                ds.Tables(0).TableName = "Mailers"
                            End If
                    End Select
                End If
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the Workorder summary 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function woGetSummary(ByVal WOID As Integer, ByVal Viewer As String, ByVal SupCompId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_GetWOSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            cmd.Parameters.AddWithValue("@SupCompanyID", SupCompId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWOSummary"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woOrderMatchGetSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO Summary for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This web method will update the Platform Price and make a new tracking entry for the same
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Value"></param>
    ''' <param name="AdminCompId"></param>
    ''' <param name="AdminContId"></param>
    ''' <param name="ClassId"></param>
    ''' <param name="lastActionRef"></param>
    ''' <param name="Comments"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOSetPlatformPrice(ByVal WOID As Integer, ByVal Value As Decimal, ByVal AdminCompId As Integer, ByVal AdminContId As Integer, ByVal ClassId As Integer, ByVal lastActionRef As Integer, ByVal Comments As String, ByVal BizDivId As Integer, ByVal ReviewBids As Boolean) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try

            Dim ProcedureName As String = "spMS_WO_SetPlatformPrice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@AdminCompId", AdminCompId)
            cmd.Parameters.AddWithValue("@AdminContId", AdminContId)
            cmd.Parameters.AddWithValue("@ContactClassID", ClassId)
            cmd.Parameters.AddWithValue("@LastActionRef", lastActionRef)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@ReviewBids", ReviewBids)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMSWO_SetPlatformPrice. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This web method will update the Wholesale Price and make a new tracking entry for the same
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Value"></param>
    ''' <param name="AdminCompId"></param>
    ''' <param name="AdminContId"></param>
    ''' <param name="ClassId"></param>
    ''' <param name="lastActionRef"></param>
    ''' <param name="Comments"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOSetWholesalePrice(ByVal WOID As Integer, ByVal Value As Decimal, ByVal AdminCompId As Integer, ByVal AdminContId As Integer, ByVal ClassId As Integer, ByVal lastActionRef As Integer, ByVal Comments As String, ByVal BizDivId As Integer, ByVal specialInst As String, ByVal PONumber As String, ByVal JobNumber As String, ByVal CustNumber As String, ByVal BusinessDivision As String, ByVal InvoiceTitle As Object, ByVal WOTitle As String, ByVal WOStartDate As Object, ByVal WOEndDate As Object, ByVal WOUpSellPrice As Decimal, ByVal WOUpSellInvoiceTitle As String, ByVal EstimatedTimeInDays As String, ByVal WholesaleDayRate As Decimal) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try

            Dim ProcedureName As String = "spMS_WO_SetWholesalePrice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@AdminCompId", AdminCompId)
            cmd.Parameters.AddWithValue("@AdminContId", AdminContId)
            cmd.Parameters.AddWithValue("@ContactClassID", ClassId)
            cmd.Parameters.AddWithValue("@LastActionRef", lastActionRef)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@specialInst", specialInst)
            cmd.Parameters.AddWithValue("@PONumber", PONumber)
            cmd.Parameters.AddWithValue("@JobNumber", JobNumber)
            cmd.Parameters.AddWithValue("@CustNumber", CustNumber)
            cmd.Parameters.AddWithValue("@BusinessDivision", BusinessDivision)
            cmd.Parameters.AddWithValue("@InvoiceTitle", InvoiceTitle)
            '*********************Staged WO***************************
            cmd.Parameters.AddWithValue("@WOTitle", WOTitle)
            cmd.Parameters.AddWithValue("@WOStartDate", WOStartDate)
            cmd.Parameters.AddWithValue("@WOEndDate", WOEndDate)
            cmd.Parameters.AddWithValue("@EstimatedTimeInDays", EstimatedTimeInDays)
            cmd.Parameters.AddWithValue("@WholesaleDayRate", WholesaleDayRate)
            '*********************Staged WO***************************

            'UpSell Price and Invoice Title
            cmd.Parameters.AddWithValue("@WOUpSellPrice", WOUpSellPrice)
            cmd.Parameters.AddWithValue("@WOUpSellInvoiceTitle", WOUpSellInvoiceTitle)


            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"

            'If the version no is not same the show message
            If ds.Tables(0).Rows(0).Item(0) = -10 Then

            End If

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMSWO_SetPlatformPrice. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Work Orders Details in the site.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Viewer"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetWODetails(ByVal WOID As Integer, ByVal Viewer As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetWODetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContacts"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblAttachments"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will update the Buyer Accepted status in DB
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Value"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="ContactId"></param>
    ''' <param name="ClassId"></param>
    ''' <param name="lastActionRef"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOSetBuyerAccepted(ByVal WOID As Integer, ByVal Value As Decimal, ByVal CompanyId As Integer, ByVal ContactId As Integer, ByVal ClassId As Integer, ByVal lastActionRef As Integer, ByVal PONumber As String, ByVal JRSNumber As String, ByVal CustNumber As String, ByVal BusinessDivision As String, ByVal ProductName As String, ByVal AddnProductName As String, ByVal xmlAttachments As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try

            Dim ProcedureName As String = "spMS_WO_SetBuyerAccepted"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@PONumber", PONumber)
            cmd.Parameters.AddWithValue("@JRSNumber", JRSNumber)
            cmd.Parameters.AddWithValue("@CustNumber", CustNumber)
            cmd.Parameters.AddWithValue("@BusinessDivision", BusinessDivision)
            cmd.Parameters.AddWithValue("@ProductName", ProductName)
            cmd.Parameters.AddWithValue("@AddnProductName", AddnProductName)
            cmd.Parameters.AddWithValue("@xmlAttachments", xmlAttachments)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@ContactClassID", ClassId)
            cmd.Parameters.AddWithValue("@LastActionRef", lastActionRef)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            ds.Tables(1).TableName = "tblWOStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMSWO_SetPlatformPrice. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Function to get the Workorder detals related to the Cancellation form
    ''' </summary>
    ''' <param name="WorkOrderID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOCancellationGetDetails(ByVal WorkOrderID As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_Cancellation_GetDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WorkOrderID", WorkOrderID)
            Dim ds As New DataSet("WOCancellation")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWODetails"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woCancellationGetDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the RAQ details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Function to save account settings and Workorder details after WO cancellation
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Action"></param>
    ''' <param name="AdminContactID"></param>
    ''' <param name="AdminCompID"></param>
    ''' <param name="ContactClassId"></param>
    ''' <param name="CancelForSupp"></param>
    ''' <param name="BuyerCancel"></param>
    ''' <param name="SupplierCancel"></param>
    ''' <param name="Comments"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOCancellationSave(ByVal WOID As String, ByVal Action As String, ByVal AdminContactID As Integer, ByVal AdminCompID As Integer, ByVal ContactClassId As Integer, ByVal CancelForSupp As Boolean, ByVal BuyerCancel As Decimal, ByVal SupplierCancel As Decimal, ByVal Comments As String, ByVal BizDivID As Object, ByVal WOStatus As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_CancelWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BuyerCanChr", BuyerCancel)
            cmd.Parameters.AddWithValue("@SuppCanChr", SupplierCancel)
            cmd.Parameters.AddWithValue("@CancelForSupp", CancelForSupp)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Action", Action)
            cmd.Parameters.AddWithValue("@AdminCompId ", AdminCompID)
            cmd.Parameters.AddWithValue("@AdminContId", AdminContactID)
            cmd.Parameters.AddWithValue("@ContactClassID", ContactClassId)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@WOStatus", WOStatus)
            Dim ds As New DataSet("WOSendMail")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblEmailRecipients"
            If (ds.Tables.Count > 1) Then
                ds.Tables(1).TableName = "tblEmailRecipientsSupp"
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woCancellationGetDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the RAQ details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    '''  Webmethod to return the Work Orders Listing.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="Viewer"></param>
    ''' <param name="Group"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWOListing(ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                               ByVal maximumRows As Integer, ByVal Viewer As String, ByVal Group As String, ByVal CompanyID As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spWO_GetWOListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            cmd.Parameters.AddWithValue("@Group", Group)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsLisitng")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrdersListing"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Work Orders Details.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Viewer"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWODetails(ByVal WOID As Integer, ByVal Viewer As String, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spWO_GetWODetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblLastAction"
            ds.Tables(4).TableName = "tblCADetails"
            ds.Tables(5).TableName = "tblComments"
            ds.Tables(6).TableName = "tblAttachments"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Work Orders Histroy.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Viewer"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWOHistory(ByVal WOID As Integer, ByVal Viewer As String, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spWO_GetWOHistory"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsHistory")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblActionHistory"
            ds.Tables(4).TableName = "tblComments"
            ds.Tables(5).TableName = "tblAttachments"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Webmethod to return the Balance for the Account
    ''' </summary>
    ''' <param name="Account"></param>
    ''' <param name="SubAccount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CheckBalance(ByVal Account As String, ByVal SubAccount As Integer, ByVal BizDivId As Integer) As Decimal
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spAccounts_CheckBalance"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            Dim balance As Decimal

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Account", Account)
            cmd.Parameters.AddWithValue("@SubAccount", SubAccount)
            cmd.Parameters.Add("@Balance", SqlDbType.Float).Direction = ParameterDirection.Output
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.ExecuteNonQuery()
            balance = CDec(cmd.Parameters.Item("@balance").Value)
            Return balance
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetClosedWOUnInvoiced(ByVal BizDivId As Integer, ByVal CompanyId As String, ByVal FromDate As String, ByVal ToDate As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMSAccounts_GetClosedWOUnInvoced"
            Dim cmd As New SqlCommand(ProcedureName, conn)


            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ClosedWos")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblClosedWOs"
            ds.Tables(1).TableName = "tblCount"
            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Sample Work Order's Details
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="bizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSampleWOsDetails(ByVal WOID As Integer, ByVal bizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spWO_GetSampleWorkOrdersDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@bizDivID", bizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SampleWOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrdersDetails"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Sample Work Orders
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="recordCount"></param>
    ''' <param name="showOnSite"></param>
    ''' <param name="bizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSampleWOs(ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                               ByVal maximumRows As Integer, ByRef recordCount As System.Nullable(Of Integer), ByVal showOnSite As String, ByVal bizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spWO_GetSampleWorkOrders"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@showOnSite", showOnSite)
            cmd.Parameters.AddWithValue("@bizDivID", bizDivID)
            cmd.Parameters.Add(New SqlParameter("@numresults", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, Nothing, DataRowVersion.Default, 0))
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SampleWOsListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrders"
            'ds.Tables(1).TableName = "tblCount"
            recordCount = cmd.Parameters("@numresults").Value
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method returns the WO Ratings.
    ''' </summary>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivID"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWORatings(ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spWO_AdminGetRatings"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)


            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchWorkOrders"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_GetSearchAccounts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method returns the details of the sample Work Order.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSampleWOsDetailsAdmin(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spWO_GetSampleWorkOrdersDetailsAdmin"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SampleWOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrdersDetails"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning the details of the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to add / update the sample WO
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="WOCategoryID"></param>
    ''' <param name="WOTitle"></param>
    ''' <param name="WOLongDesc"></param>
    ''' <param name="PONumber"></param>
    ''' <param name="JRSNumber"></param>
    ''' <param name="CustomerNumber"></param>
    ''' <param name="SpecialInstructions"></param>
    ''' <param name="DateStart"></param>
    ''' <param name="DateEnd"></param>
    ''' <param name="EstimatedTime"></param>
    ''' <param name="Value"></param>
    ''' <param name="SpecialistSuppliesParts"></param>
    ''' <param name="LocationID"></param>
    ''' <param name="SuppDet"></param>
    ''' <param name="ClientDet"></param>
    ''' <param name="ShowOnSite"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AddUpdateSampleWO(ByVal WOID As Integer, ByVal WOCategoryID As Integer, ByVal WOTitle As String, ByVal WOLongDesc As String, ByVal PONumber As String, ByVal JRSNumber As String, ByVal CustomerNumber As String, ByVal SpecialInstructions As String, ByVal DateStart As String, ByVal DateEnd As String, ByVal EstimatedTime As String, ByVal Value As String, ByVal SpecialistSuppliesParts As Boolean, ByVal LocationID As Integer, ByVal SuppDet As String, ByVal ClientDet As String, ByVal ShowOnSite As Boolean, ByVal BizDivId As Integer) As Integer
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spWO_UpdateSampleWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOCategoryID ", WOCategoryID)
            cmd.Parameters.AddWithValue("@WOTitle", WOTitle)
            cmd.Parameters.AddWithValue("@WOLongDesc", WOLongDesc)
            cmd.Parameters.AddWithValue("@PONumber", PONumber)
            cmd.Parameters.AddWithValue("@JRSNumber", JRSNumber)
            cmd.Parameters.AddWithValue("@CustomerNumber", CustomerNumber)
            cmd.Parameters.AddWithValue("@SpecialInstructions", SpecialInstructions)
            cmd.Parameters.AddWithValue("@LocationId", LocationID)
            cmd.Parameters.AddWithValue("@SupplierDetails", SuppDet)
            cmd.Parameters.AddWithValue("@ClientDetails", ClientDet)
            cmd.Parameters.AddWithValue("@ShowOnSite", ShowOnSite)
            'cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            'Dim BizDivId As Integer = getBizDivId()
            If BizDivId <> 0 Then
                cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            End If
            Try
                If DateStart <> "" Then
                    cmd.Parameters.AddWithValue("@DateStart", CommonFunctions.convertDate(DateStart))
                End If
            Catch ex As Exception
            End Try
            Try
                If DateEnd <> "" Then
                    cmd.Parameters.AddWithValue("@DateEnd", CommonFunctions.convertDate(DateEnd))
                End If
            Catch ex As Exception

            End Try
            Try
                cmd.Parameters.AddWithValue("@Value", CDec(Value))
            Catch ex As Exception

            End Try
            cmd.Parameters.AddWithValue("@EstimatedTime", EstimatedTime)

            cmd.Parameters.AddWithValue("@SpecialistSuppliesParts", SpecialistSuppliesParts)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters("@WOID").Direction = ParameterDirection.InputOutput
            cmd.ExecuteNonQuery()
            WOID = CInt(cmd.Parameters.Item("@WOID").Value)
            Return WOID
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This method will delete the Sample Work Order
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DeleteSampleWorkOrder(ByVal WOID As Integer)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spWO_DeleteSampleWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("DeleteSampleWorkOrder")
            cmd.ExecuteNonQuery()
            Return Nothing
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Save the Contact info and workorder details created using the RAQ form
    ''' </summary>
    ''' <param name="xmlContact"></param>
    ''' <param name="xmlWorkOrder"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function woAddRAQDetails(ByVal xmlContact As String, ByVal xmlWorkOrder As String, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spWO_AddRAQ_Save"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlContact", xmlContact)
            cmd.Parameters.AddWithValue("@xmlWorkOrder", xmlWorkOrder)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim ds As New DataSet("WONotification")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSendMail"
            ds.Tables(1).TableName = "tblWorkOrder"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woAddRAQDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the RAQ details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the standard data need for create workorder form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function woCreateWOGetStandards(ByVal BizDivID As Integer, ByVal ContactID As Integer, ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spWO_CreateWorkOrder_GetStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("CreateWorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCountries"
            ds.Tables(1).TableName = "tblWorkOrderTypes"
            ds.Tables(2).TableName = "tblWorkOrderSubTypes"
            ds.Tables(3).TableName = "tblDressCode"
            ds.Tables(4).TableName = "tblAttachmentsCompany"
            ds.Tables(5).TableName = "tblAttachmentsWO"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function woMSGenerateReportCompanyWO(ByVal BizDivID As Integer, ByVal companyId As Integer, ByVal fromDate As String, ByVal toDate As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "sp_MSGenerateReportCompanyWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@CompanyId", companyId)
            cmd.Parameters.AddWithValue("@FromDate", fromDate)
            cmd.Parameters.AddWithValue("@ToDate", toDate)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrderDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrderDetails"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function AddUpdateTemplateWO(ByVal xmlStr As String, ByVal doMode As String, ByVal SpecialistSuppliesParts As String, ByVal EstimatedTime As String) As Integer
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Dim WOID As Integer

        Try
            Dim ProcedureName As String = "spMS_WO_AddUpdateTemplate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@SpecialistSuppliesParts", SpecialistSuppliesParts)
            cmd.Parameters.AddWithValue("@EstimatedTime", EstimatedTime)
            cmd.Parameters.AddWithValue("@WOID", 0)
            cmd.Parameters("@WOID").Direction = ParameterDirection.InputOutput
            cmd.ExecuteNonQuery()
            WOID = CInt(cmd.Parameters.Item("@WOID").Value)
            Return WOID
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetTemplateForTemplateID(ByVal TemplateID As Integer, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WO_GetTemplateForTemplateID"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", TemplateID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrderDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWODetails"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetTemplateListing(ByVal BizDivID As Integer, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WO_GetTemplateList"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("TemplateListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrderTemplate"
            ds.Tables(1).TableName = "tblNoOfRows"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Web method to return the data to show on upload parameters page
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetWOUploadParametersDetails(ByVal BizDivId As Integer, ByVal CompanyID As Integer, Optional ByVal WorkOrderId As String = "", Optional ByVal AccountId As String = "", Optional ByVal IPAddress As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_GetWOUploadParametersDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@WorkOrderId", WorkOrderId)
            cmd.Parameters.AddWithValue("@AccountId", AccountId)
            cmd.Parameters.AddWithValue("@IPAddress", IPAddress)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("UploadParams")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWOCombID"
            ds.Tables(1).TableName = "tblDressCode"
            ds.Tables(2).TableName = "tblAttachments"
            ds.Tables(3).TableName = "tblWOUploadAccounts"
            ds.Tables(4).TableName = "tblWOProducts"
            ds.Tables(5).TableName = "tblBilling"
            ds.Tables(6).TableName = "tblAMStatus"
            ds.Tables(7).TableName = "tblFreesatMake"
            ds.Tables(8).TableName = "tblGoodsLocation"
            If ds.Tables.Count > 9 Then
                ds.Tables(9).TableName = "tblWOStatus"
            End If

            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Web method to save the data from upload parameters page to db
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_SaveWOUploadParametersDetails(ByVal BizDivId As Integer, ByVal CompanyID As Integer, ByVal DressCodeId As String, ByVal IsEnable As Boolean, ByVal WOCombId As String, ByVal XMLDoc As String, ByVal LoggedInUserID As Integer)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_SaveWOUploadParametersDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@DressCodeId", DressCodeId)
            cmd.Parameters.AddWithValue("@WOCombId", WOCombId)
            cmd.Parameters.AddWithValue("@IsEnable", IsEnable)
            cmd.Parameters.AddWithValue("@xmlDoc", XMLDoc)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.ExecuteNonQuery()
            Return Nothing
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the authenticated conatctid and maincontactid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_AuthenticateWOUploadAccount(ByVal BizDivId As Integer, ByVal AccountId As String, ByVal Password As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_AuthenticateWOUploadAccount"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@AccountId", AccountId)
            cmd.Parameters.AddWithValue("@Password", Password)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ContactDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContactDetails"
            ds.Tables(1).TableName = "tblContactAddress"
            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''
    ''' <summary>
    ''' Web method to make an entry in [tblWSWOUploadLog] for transaction made by buyer application with wxposed web service
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_InsertWOUploadLogEntry(ByVal AccountId As String, ByVal BizDivId As Integer, ByVal IPAddress As String, ByVal Method As String, ByVal Status As String, ByVal ResponseMessage As String)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_InsertWOUploadLogEntry"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AccountId", AccountId)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@IPAddress", IPAddress)
            cmd.Parameters.AddWithValue("@Method", Method)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@ResponseMessage", ResponseMessage)
            cmd.ExecuteNonQuery()
            Return Nothing
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''
    ''' <summary>
    ''' Web method to make an entry in [tblWSWOUploadLog] for transaction made by buyer application with wxposed web service
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_getWebServiceLogDetails(ByVal companyId As Integer, ByVal BizDivId As Integer, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_getWebServiceLogDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@companyId", companyId)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("LogDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblLogDetails"
            ds.Tables(1).TableName = "tblCount"
            HttpContext.Current.Items("TotalCount") = ds.Tables("tblCount").Rows(0).Item(0)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    Public Shared Function MS_getWebServiceLogDetails_Count(ByVal CompanyId As Integer, ByVal BizDivID As Integer) As Integer
        Return HttpContext.Current.Items("TotalCount")
    End Function

    ''
    ''' <summary>
    ''' Web Method to get data to Split the work order
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function woGetDataToSplitWO(ByVal WOID As Object, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetDataToSplitWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("PricingDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "PricingDetails"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''
    ''' <summary>
    ''' Web Method to create Staged Work orders
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub CreatStagedWorkOrders(ByVal ContactID As Integer, ByVal CompanyID As Integer, ByVal BizDivID As Integer, ByVal NoOfStages As Integer, ByVal EstimatedTimeInDays As Integer, ByVal WholesalePrice As Object, ByVal WholesaleJobDayRate As Object, ByVal PlatformPrice As Object, ByVal PlatformJobDayRate As Object, ByVal WorkOrderID As Object, ByVal BuyerCompanyId As Object, ByVal BuyerContactId As Object)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_SplitWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@NoOfStages", NoOfStages)
            cmd.Parameters.AddWithValue("@EstimatedTimeInDays", EstimatedTimeInDays)
            cmd.Parameters.AddWithValue("@WholesalePrice", WholesalePrice)
            cmd.Parameters.AddWithValue("@WholesaleJobDayRate", WholesaleJobDayRate)
            cmd.Parameters.AddWithValue("@PlatformPrice", PlatformPrice)
            cmd.Parameters.AddWithValue("@PlatformJobDayRate", PlatformJobDayRate)
            cmd.Parameters.AddWithValue("@WorkOrderID", WorkOrderID)
            cmd.Parameters.AddWithValue("@BuyerCompanyId", BuyerCompanyId)
            cmd.Parameters.AddWithValue("@BuyerContactId", BuyerContactId)
            Dim da As New SqlDataAdapter(cmd)
            'Dim ds As New DataSet("Success")
            cmd.ExecuteNonQuery()
            'da.Fill(ds)
            'ds.Tables(0).TableName = "Success"
            'Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Sub
    ''

    ''' <summary>
    ''' this webmethod  returns the data for web services access for the given bizdivid and companyid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetWebServiceAccessKey(ByVal companyId As Integer, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetWebServiceAccessKey"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@companyId", companyId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WebServiceAccessKey")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWebServiceAccessKey"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' this webmethod  creates the entry for credentials to access OW  web services 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_CreateWebSrviceAccessKeyEntry(ByVal companyId As Integer, ByVal BizDivId As Integer, ByVal password As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_CreateWebSrviceAccessKeyEntry"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyId", companyId)
            cmd.Parameters.AddWithValue("@Password", password)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WebServiceAccessKey")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWebServiceAccessKey"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' this webmethod  updates the IsEnable status for the web service acconut
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>    
    Public Shared Function MS_UpdateWebServiceAccessKeyDetails(ByVal AccountId As String, ByVal isEnable As Boolean)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_UpdateWebServiceAccessKeyDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AccountId", AccountId)
            cmd.Parameters.AddWithValue("@IsEnable", isEnable)
            cmd.ExecuteNonQuery()
            Return Nothing
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' this webmethod  returns the status of the wo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>   
    Public Shared Function woGetWOStatus(ByVal WOID As String, ByVal WOType As String, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetWOStatus"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@WOTYPE", WOType)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOStaus")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWOStaus"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' this webmethod  returns the status of the wo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>   
    Public Shared Function PerformMailerActions(ByVal Mode As String, ByVal Type As String, ByVal ContactId As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_PerformMailerActions"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@Type", Type)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Staus")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' this webmethod  returns the List of products for a given company id
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>   
    Public Shared Function GetProductListing(ByVal BizDivID As Integer, ByVal CompanyID As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                                ByVal maximumRows As Integer, ByRef recordCount As System.Nullable(Of Integer), ByVal ShowHidden As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetProductListing "
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID.ToString)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID.ToString)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@ShowDeleted", 0)
            cmd.Parameters.AddWithValue("@ShowHidden", ShowHidden)
            cmd.Parameters.Add(New SqlParameter("@numresults", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, Nothing, DataRowVersion.Default, 0))
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ProductListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "Product"
            ds.Tables(1).TableName = "tblCount"
            HttpContext.Current.Items("recordCount") = ds.Tables("tblCount").Rows(0).Item(0)
            HttpContext.Current.Items("TotalCustPrice") = ds.Tables("tblCount").Rows(0).Item("TotalCustPrice")
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Returns count of number of records in Product listing
    ''' </summary>
    ''' <param name="CompanyID"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SelectProductListingCount(ByVal CompanyID As Integer, ByVal BizDivID As Integer, ByRef recordCount As System.Nullable(Of Integer), ByVal ShowHidden As Integer) As Integer
        Return HttpContext.Current.Items("recordCount")
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="ProductID"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function populateProductDetails(ByVal ProductID As Integer, Optional ByVal CompanyID As Integer = 0, Optional ByVal BizDivId As Integer = 1) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            If CompanyID = 0 Then
                CompanyID = HttpContext.Current.Session("PortalCompanyId")
            End If
            If BizDivId = 1 Then
                BizDivId = ApplicationSettings.BizDivId
            End If
            Dim ProcedureName As String = "spMS_WO_GetProductDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ProductID", ProductID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This is created specifically for New WO form created on 12 May 2005
    ''' </summary>
    ''' <param name="ProductID"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function populateWOFormProductDetails(ByVal ProductID As Integer, ByVal LoggedInUserID As Integer, ByVal Mode As String, Optional ByVal CompanyID As Integer = 0, Optional ByVal BizDivId As Integer = 1, Optional ByVal ServiceID As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WOFORM_GetProductDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ProductID", ProductID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivId)
            cmd.Parameters.AddWithValue("@LoggedInUserId", LoggedInUserID)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@ServiceID", ServiceID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the Billing locations dataset associated for the contact
    ''' </summary>
    ''' <param name="companyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function woGetContactsBillingLocations(ByVal companyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spWO_GetContactsBillingLocations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", companyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrderLoc")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblLocations"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This method will retrive the AutoMatch Settings for a user
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAutoMatchDetails(ByVal BizDivId As Integer, ByVal CompanyID As Integer, ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetAutoMatchDetailsAndFavSuppliers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@BuyerCompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@NewWOId", WOID)
            cmd.ExecuteNonQuery()
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AutoMatchSettings")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAutomatch"
            ds.Tables(1).TableName = "tblSearchSuppliers"
            ds.Tables(2).TableName = "tblCount"
            If ds.Tables.Count > 3 Then
                ds.Tables(3).TableName = "tblPostCode"
            End If
            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMS_GetAutoMatchDetailsAndFavSuppliers. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="SupContactIDs"></param>
    ''' <param name="WOID"></param>
    ''' <param name="DateStart"></param>
    ''' <param name="DateEnd"></param>
    ''' <param name="EstimatedTime"></param>
    ''' <param name="Value"></param>
    ''' <param name="SpecialistSuppliesParts"></param>
    ''' <param name="ActualTime"></param>
    ''' <param name="OWCompanyID"></param>
    ''' <param name="OWRepID"></param>
    ''' <param name="IsAutoMatch"></param>
    ''' <param name="BuyerCompID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SendWO(ByVal SupContactIDs As String, ByVal WOID As Integer, ByVal DateStart As String, ByVal DateEnd As String, ByVal EstimatedTime As String, ByVal Value As Decimal, ByVal SpecialistSuppliesParts As Boolean, ByVal ActualTime As String, ByVal OWCompanyID As Integer, ByVal OWRepID As Integer, ByVal IsAutoMatch As Boolean, ByVal BuyerCompID As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try

            Dim ProcedureName As String = "spWO_OrderMatch_SendWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@SupContactIDs", SupContactIDs)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            If DateStart <> "" Then
                cmd.Parameters.AddWithValue("@DateStart", CommonFunctions.convertDate(DateStart))
            Else
                cmd.Parameters.AddWithValue("@DateStart", DateStart)
            End If
            If DateEnd <> "" Then
                cmd.Parameters.AddWithValue("@DateEnd", CommonFunctions.convertDate(DateEnd))
            Else
                cmd.Parameters.AddWithValue("@DateEnd", DateEnd)
            End If

            cmd.Parameters.AddWithValue("@EstimatedTime", EstimatedTime)
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@SpecialistSuppliesParts", SpecialistSuppliesParts)
            cmd.Parameters.AddWithValue("@ActualTime", ActualTime)
            cmd.Parameters.AddWithValue("@OWCompanyID", OWCompanyID)
            cmd.Parameters.AddWithValue("@OWRepID", OWRepID)
            cmd.Parameters.AddWithValue("@IsAutoMatched", IsAutoMatch)
            cmd.Parameters.AddWithValue("@BuyerCompID", BuyerCompID)
            cmd.Parameters.AddWithValue("@NearestTo", "No")
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contacts")
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "tblSuppliers"
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_OrderMatch_SendWO. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Function to add / update the workorder details from the Retail from
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="doMode"></param>
    ''' <param name="ProductId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOAddUpdateRetailWO(ByVal xmlStr As String, ByVal doMode As String, ByVal ProductId As Integer, Optional ByVal source As String = "", Optional ByVal ClientQAnsId As Integer = 0, Optional ByVal ClientTBQAns As String = "", Optional ByVal LoggedInUserID As Integer = 0, Optional ByVal SessionIdString As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_RetailWOSave"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@ProductId", ProductId)
            cmd.Parameters.AddWithValue("@Source", source)
            cmd.Parameters.AddWithValue("@ClientQAnsId", ClientQAnsId)
            cmd.Parameters.AddWithValue("@ClientTBQAns", ClientTBQAns)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@SessionIdString", SessionIdString)
            Dim ds As New DataSet("WONotification")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSendMail"
            ds.Tables(1).TableName = "tblWorkOrder"
            If ds.Tables.Count > 2 Then
                ds.Tables(2).TableName = "tblSuppliers"
            End If
            ds.Tables(ds.Tables.Count - 1).TableName = "EmailStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_CreateWorkOrder_Save. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the workorder data. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    Public Shared Function GetHolidays(ByVal SortExpression As String, ByVal StartRowIndex As String, ByVal MaxRows As String, ByVal showAll As Boolean, ByVal src As String, ByVal ContactId As Integer) As DataSet
        Dim IsDepot As Boolean = False
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetHolidayListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@showAll", showAll)
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", MaxRows)
            cmd.Parameters.AddWithValue("@src", src)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "HolidaysListing"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    Public Shared Function CheckVersionNo(ByVal WOID As String, Optional ByVal WOVersionNo As Byte() = Nothing, Optional ByVal WOTrackingVNo As Byte() = Nothing) As DataSet
        Dim IsDepot As Boolean = False
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_CheckVersionNo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", CInt(WOID))
            cmd.Parameters.AddWithValue("@WorkOrderVersionNo", WOVersionNo)
            cmd.Parameters.AddWithValue("@WOTrackingVersionNo", WOTrackingVNo)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function UpdateWONotes(ByVal WOID As String, ByVal ContactID As Integer, ByVal Note As String, ByVal ReturnData As Boolean, ByVal CompanyID As Integer, ByVal ContactClassID As Integer, Optional ByVal ShowOrderWork As Boolean = False, Optional ByVal ShowSupplier As Boolean = False, Optional ByVal ShowClient As Boolean = False) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WO_SaveWONotes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@Note", Note)
            cmd.Parameters.AddWithValue("@ReturnData", ReturnData)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ContactClassID", ContactClassID)
            cmd.Parameters.AddWithValue("@ShowOrderWork", ShowOrderWork)
            cmd.Parameters.AddWithValue("@ShowSupplier", ShowSupplier)
            cmd.Parameters.AddWithValue("@ShowClient", ShowClient)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            If ReturnData = True Then
                ds.Tables(1).TableName = "Notes"
            End If
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetWONotes(ByVal WOID As Integer, ByVal ContactClassid As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WO_GetWONotes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@ContactClassID", ContactClassid)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Notes"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <param name="Val"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetGoodsLocation(ByVal mode As String, ByVal Val As String, ByVal CompanyID As Integer) As DataTable
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_AutoSuggestGoodsLocation"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Mode", mode)
            cmd.Parameters.AddWithValue("@Val", Val)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsSuggest")
            da.Fill(ds)
            Return ds.Tables(0)
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetContactsTimeSlots(ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetContactsTimeSlots"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("TimeSlots")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetDepotLocDetails(ByVal AddressID As Integer) As DataTable
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_GetDepotLocDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AddressID", AddressID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDepotLocDetails")
            da.Fill(ds)
            Return ds.Tables(0)
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Function to get Buyer Todaye cancelled or active WorkOrder Summary welcome panel
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns>returns dataset with 1 table</returns>
    ''' <remarks></remarks>
    Public Shared Function MS_WOBuyerWorkSummary(ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_BuyerWorkSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_SupplyWorkSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Function to return Client SLA Report
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetClientSLAReport(ByVal CompanyId As Integer, ByVal FromDate As String, ByVal ToDate As String, Optional ByVal Mode As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_ReportsBestBuy"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyId)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@Mode", mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ClientSLAReport")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblClientSLAReport"
            If (Mode <> "Excel") Then
                ds.Tables(1).TableName = "tblProductSLAReport"
                ds.Tables(2).TableName = "tblBillingLocSLAReport"
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Function to return Client Submitted WO Report
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetClientSubmittedReport(ByVal CompanyId As Integer, ByVal FromDate As String, ByVal ToDate As String, Optional ByVal Mode As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_ReportsClientSubJobs"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyId)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ClientSubJobs")
            da.Fill(ds)
            If (Mode = "") Then
                ds.Tables(0).TableName = "tblClientTotalSubJobs"
                ds.Tables(1).TableName = "tblClientServices"
            ElseIf (Mode = "Excel") Then
                ds.Tables(0).TableName = "tblClientSubJobs"
            End If

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Method to return WorkOrder List for IPhone native app
    ''' </summary>
    ''' <param name="StatusID"></param>
    ''' <param name="AccountID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetIPhoneWorkOrderListing(ByVal StatusID As Integer, ByVal AccountID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spIPhoneApp_GetWOListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@StatusID", StatusID)
            cmd.Parameters.AddWithValue("@AccountID", AccountID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrderListing")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Method to return WorkOrder List
    ''' </summary>
    ''' <param name="StatusID"></param>
    ''' <param name="AccountID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetIPhoneWorkOrderDetails(ByVal StatusID As Integer, ByVal AccountID As Integer, ByVal WOID As Integer, ByVal contactid As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spIPhoneApp_GetWODetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@StatusID", StatusID)
            cmd.Parameters.AddWithValue("@AccountID", AccountID)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@ContactID", contactid)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrderDetails")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Method to return WorkOrder List
    ''' </summary>
    ''' <param name="StatusID"></param>
    ''' <param name="AccountID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function OWIPhoneWorkOrderActions(ByVal ContactID As Integer, ByVal AccountID As Integer, ByVal WOID As Integer, ByVal Action As String, ByVal Value As Decimal, ByVal deviceID As String, ByVal pin As Integer, Optional ByVal WOVersionNo As Byte() = Nothing, Optional ByVal WOTrackingVersionNo As Byte() = Nothing) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spIPhoneApp_WODiscardAccept"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", AccountID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@WOVersionNo", WOVersionNo)
            cmd.Parameters.AddWithValue("@WOTrackingVersionNo", WOTrackingVersionNo)
            cmd.Parameters.AddWithValue("@WOAction", Action)
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@Pin", pin)
            cmd.Parameters.AddWithValue("@DeviceID", deviceID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SendEmail")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function IPhoneNotification(ByVal compIds As String, ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spIPhoneApp_GetTokenForNotification"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@SupplierCompanyID", compIds)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "TokenDetails"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function GetBillingLocationTab(ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spGetBillingLocationTab"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", HttpContext.Current.Session("PortalUserId"))
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblBillingLocation"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetNotificationDetails(ByVal CompanyID As Integer, ByVal ContactID As Integer, ByVal AlwaysDisplayLogin As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spAdmin_GetSPNotifications"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@AlwaysDisplayLogin", AlwaysDisplayLogin)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("NotificationDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblNotificationDetails"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function Getworkorders(ByVal CompanyId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_Getworkorders"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblworkorder"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function GetAutoSuggest(ByVal mode As String, ByVal Val As String, ByVal CompanyID As Integer) As DataTable
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_AutoSuggest"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Mode", mode)
            cmd.Parameters.AddWithValue("@Val", Val)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsSuggest")
            da.Fill(ds)
            
            Return ds.Tables(0)

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function AddDixonsComplaints(ByVal ComplaintId As String, ByVal SalesAgentName As String, ByVal SalesAgentMainPhone As String, ByVal BranchID As String, ByVal CustomerName As String, ByVal CustomerPostCode As String, ByVal Complaint As String, ByVal IsProcessed As Integer, ByVal ContactID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spAdmin_AddUpdateDixonsComplaints"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ComplaintId", ComplaintId)
            cmd.Parameters.AddWithValue("@SalesAgentName", SalesAgentName)
            cmd.Parameters.AddWithValue("@SalesAgentMainPhone", SalesAgentMainPhone)
            cmd.Parameters.AddWithValue("@BranchID", BranchID)
            cmd.Parameters.AddWithValue("@CustomerName", CustomerName)
            cmd.Parameters.AddWithValue("@CustomerPostCode", CustomerPostCode)
            cmd.Parameters.AddWithValue("@Complaint", Complaint)
            cmd.Parameters.AddWithValue("@IsProcessed", IsProcessed)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function WOSubmitQuestionAnswers(ByVal xmlQA As String, ByVal LoggedInUserID As Integer, ByVal ProductId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_WOSubmitQuestionAnswers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlQA", xmlQA)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@ProductId", ProductId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' To fetch the only PONumber of the workorder
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CheckUniqueness(ByVal Value As String, ByVal Type As String, Optional ByVal TagId As Integer = 0) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_CheckUniqueness"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@Type", Type)
            cmd.Parameters.AddWithValue("@Id", TagId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Value")
            da.Fill(ds)
            ds.Tables(0).TableName = "Value"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

	<System.Web.Services.WebMethod()>
	Public Shared Function SearchWorkOrder(WorkOrderID As String, Postcode As String) As String
		'Public Shared Function SearchWorkOrder(WorkOrderID As String, Postcode As String, CompanyID As Integer) As String
		CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
		Dim conn As SqlConnection = DBUtil.getDBConn
		Try
			Dim ProcedureName As String = "AT800FindWOFromPostcodeAndRefWOID"
			Dim cmd As New SqlCommand(ProcedureName, conn)
			cmd.CommandType = CommandType.StoredProcedure
			cmd.Parameters.AddWithValue("@RefWOID", WorkOrderID)
			cmd.Parameters.AddWithValue("@PostCode", Postcode)
			cmd.ExecuteNonQuery()
			Dim da As New SqlDataAdapter(cmd)
			Dim ds As New DataSet("WO")
			da.Fill(ds)

			ds.Tables(0).TableName = "WO"

			Try
				Dim returnString As String
				If ds.Tables("WO").Rows.Count > 0 Then
					returnString = ds.Tables("WO").Rows(0)("JobTitle").ToString + vbNewLine _
					 + ds.Tables("WO").Rows(0)("ContactFullName").ToString + vbNewLine _
					 + ds.Tables("WO").Rows(0)("ContactAddress").ToString
				Else
					returnString = ""
				End If

				Return (returnString)
			Catch ex As Exception
				Return "NOT FOUND"
			End Try

		Catch ex As Exception
			Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
		Finally
			Try : conn.Close() : Catch : End Try
		End Try
	End Function

	<System.Web.Services.WebMethod()>
	Public Shared Function AT800InsertSignOffSheet(RefWOID As String, Installer_Return_Code As Integer, Filt_Type As Integer, Filt_Manuf As Integer, ProblemNot4G As Boolean, Installer_Notes As String)

		CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
		Dim conn As SqlConnection = DBUtil.getDBConn
		Try
			Dim ProcedureName As String
			ProcedureName = "AT800InsertSignOffSheet"
			Dim cmd As New SqlCommand(ProcedureName, conn)
			cmd.CommandType = CommandType.StoredProcedure
			cmd.Parameters.AddWithValue("@RefWOID", RefWOID)
			cmd.Parameters.AddWithValue("@Installer_Return_Code", Installer_Return_Code)
			cmd.Parameters.AddWithValue("@Filt_Type", Filt_Type)
			cmd.Parameters.AddWithValue("@Filt_Manuf", Filt_Manuf)
			cmd.Parameters.AddWithValue("@ProblemNot4G", ProblemNot4G)
			cmd.Parameters.AddWithValue("@Installer_Notes", Installer_Notes)

			Dim da As New SqlDataAdapter(cmd)
			cmd.ExecuteNonQuery()
			Return ""
		Catch ex As Exception
			Throw ex 'ExceptionUtil.CreateSoapException("Exception in InsertPostCodeCoordinates. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while inserting the values. Please try again.", ex)
		Finally
			Try : conn.Close() : Catch : End Try
		End Try

	End Function

	Public Shared Function GetValidDates(ByVal CompanyId As Integer, ByVal BillingId As Integer, ByVal ProductId As Integer, ByVal TimeSlot As String, ByVal Mode As String, ByVal DateSelected As String) As DataSet
		CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
		Dim conn As SqlConnection = DBUtil.getDBConn
		Try
			Dim ProcedureName As String = "spMS_WO_GetValidDates"
			Dim cmd As New SqlCommand(ProcedureName, conn)
			cmd.CommandType = CommandType.StoredProcedure
			cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
			cmd.Parameters.AddWithValue("@BillingId", BillingId)
			cmd.Parameters.AddWithValue("@ProductId", ProductId)
			cmd.Parameters.AddWithValue("@TimeSlot", TimeSlot)
			cmd.Parameters.AddWithValue("@DateSelected", DateSelected)
			cmd.Parameters.AddWithValue("@Mode", Mode)
			Dim da As New SqlDataAdapter(cmd)
			Dim ds As New DataSet("ValidDates")
			da.Fill(ds)
			ds.Tables(0).TableName = "ValidDates"
			Return ds
		Catch ex As Exception
			Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
		Finally
			Try : conn.Close() : Catch : End Try
		End Try

    End Function

    Public Shared Function InsertURLforSONY(ByVal RefWOID As String, ByVal URL As String, ByVal Status As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim ProcedureName As String = "spMS_InsertSONYURL"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("@RefWOID", RefWOID)
            cmd.Parameters.AddWithValue("@URL", URL)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetBasicScopesServices(ByVal CompanyId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMs_WO_GetBasicServices"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("BasicServices")
            da.Fill(ds)
            ds.Tables(0).TableName = "BasicServices"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    'Sabita added another parameter LoginiD for task #OA 575
    Public Shared Function GetBasicScopesServices(ByVal CompanyId As Integer, ByVal ScopeServiceId As Integer, Optional ByVal BrandId As Integer = 0, Optional ByVal JobPostCode As String = "", Optional ByVal DBS As Integer = 0, Optional ByVal ServiceId As String = "", Optional ByVal Quantity As Integer = 0, Optional ByVal IsBTBDTypeClient As Integer = 0, Optional ByVal loginId As Integer = 0) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMs_WO_GetBasicRelatedServices"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@ScopeServiceId", ScopeServiceId)
            cmd.Parameters.AddWithValue("@BrandId", BrandId)
            cmd.Parameters.AddWithValue("@JobPostCode", JobPostCode)
            cmd.Parameters.AddWithValue("@DBS", DBS)
            cmd.Parameters.AddWithValue("@Quantity", Quantity)
            cmd.Parameters.AddWithValue("@ServiceId", ServiceId)
            cmd.Parameters.AddWithValue("@IsBTBDTypeClient", IsBTBDTypeClient)
            cmd.Parameters.AddWithValue("@LoginId", loginId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("BasicRelatedServices")
            da.Fill(ds)
            ds.Tables(0).TableName = "BasicRelatedServices"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    Public Shared Function MS_WOAddUpdateRetailWO_NewDixonsLayout(ByVal xmlStr As String, ByVal doMode As String, ByVal ServiceId As String, ByVal LoggedInUserID As Integer, Optional ByVal ThermostatsQuestions As String = "", Optional ByVal TVSize As String = "", Optional ByVal xmlContentShopCart As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_RetailWOSave_NewDixonsLayout"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@ServiceId", ServiceId)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@ThermostatsQuestions", ThermostatsQuestions)
            cmd.Parameters.AddWithValue("@TVSize", TVSize)
            cmd.Parameters.AddWithValue("@xmlContentShopCart", xmlContentShopCart)
            Dim ds As New DataSet("WONotification")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSendMail"
            ds.Tables(1).TableName = "tblWorkOrder"
            If ds.Tables.Count > 2 Then
                ds.Tables(2).TableName = "tblSuppliers"
            End If
            ds.Tables(ds.Tables.Count - 1).TableName = "EmailStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_CreateWorkOrder_Save. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the workorder data. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function MS_WOAddUpdateRetailWO_BTBD(ByVal xmlStr As String, ByVal doMode As String, ByVal ServiceId As String, ByVal LoggedInUserID As Integer, Optional ByVal ThermostatsQuestions As String = "", Optional ByVal TVSize As String = "", Optional ByVal xmlContentShopCart As String = "") As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_RetailWOSave_BTBD"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@ServiceId", ServiceId)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@ThermostatsQuestions", ThermostatsQuestions)
            cmd.Parameters.AddWithValue("@TVSize", TVSize)
            cmd.Parameters.AddWithValue("@xmlContentShopCart", xmlContentShopCart)
            Dim ds As New DataSet("WONotification")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSendMail"
            ds.Tables(1).TableName = "tblWorkOrder"
            If ds.Tables.Count > 2 Then
                ds.Tables(2).TableName = "tblSuppliers"
            End If
            ds.Tables(ds.Tables.Count - 1).TableName = "EmailStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_CreateWorkOrder_Save. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the workorder data. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetTheCombinedScopeofWork(ByVal CompanyId As Integer, ByVal ServiceId As String, ByVal xmlContentShopCart As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMs_WO_GetCombinedScopeofWorkServices"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@ServiceId", ServiceId)
            cmd.Parameters.AddWithValue("@xmlContentShopCart", xmlContentShopCart)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("CombinedScopeofWork")
            da.Fill(ds)
            ds.Tables(0).TableName = "CombinedScopeofWork"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function
    Public Shared Function GetBTBDMultiServiceInfo(ByVal CompanyId As Integer, ByVal ServiceId As Integer, ByVal IsMultiServices As Integer, ByVal FirstService As String, ByVal Quantity As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMs_WO_GetBTBDMultiServiceInfo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@ServiceId", ServiceId)
            cmd.Parameters.AddWithValue("@Quantity", Quantity)
            cmd.Parameters.AddWithValue("@IsMultiServices", IsMultiServices)
            cmd.Parameters.AddWithValue("@FirstService", FirstService)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("MultiServiceInfo")
            da.Fill(ds)
            ds.Tables(0).TableName = "MultiServiceInfo"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    Public Shared Function InsertDixonsServicesSelection(ByVal CompanyId As Integer, ByVal ServiceId As String)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String
            ProcedureName = "spMs_WO_InsertDixonsServicesSelection"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@ServiceId", ServiceId)

            Dim da As New SqlDataAdapter(cmd)
            cmd.ExecuteNonQuery()
            Return ""
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in InsertPostCodeCoordinates. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while inserting the values. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetDixonsServicesSelection(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMs_WO_GetDixonsServicesSelection"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ServicesSelection")
            da.Fill(ds)
            ds.Tables(0).TableName = "ServicesSelection"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    Public Shared Function populateProductDetails_ForNewDixonsLayout(ByVal ServiceId As Integer, Optional ByVal CompanyID As Integer = 0) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            If CompanyID = 0 Then
                CompanyID = HttpContext.Current.Session("PortalCompanyId")
            End If

            Dim ProcedureName As String = "spMS_WO_GetProductDetails_NewDixonsLayout"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ServiceId", ServiceId)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function GetWorkorderId(ByVal CustomerMobile As String) As ArrayList
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetworkorderID"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CustomerMobile", CustomerMobile)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WO")
            da.Fill(ds)
            ds.Tables(0).TableName = "WorkOrderID"

            Try
                Dim wodetails As ArrayList = New ArrayList
                If ds.Tables("WorkOrderID").Rows.Count > 0 Then
                    If ds.Tables("WorkOrderID").Rows.Count > 0 Then
                        wodetails.Add(ds.Tables("WorkOrderID").Rows(0)("WorkOrderID").ToString())
                        wodetails.Add(ds.Tables("WorkOrderID").Rows(0)("CompanyName").ToString())
                        wodetails.Add(ds.Tables("WorkOrderID").Rows(0)("CustomerEmail").ToString())
                        wodetails.Add(ds.Tables("WorkOrderID").Rows(0)("CustomerName").ToString())
                    End If
                End If

                Return wodetails
            Catch ex As Exception
                'Return "NOT FOUND"
            End Try
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function SendAndAutoaccept(ByVal WOID As Integer, ByVal SupplierCompID As Integer, ByVal LoggedInUserID As Integer, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String
            ProcedureName = "spAdmin_AutoAcceptWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@SupplierCompID", SupplierCompID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivId)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in InsertPostCodeCoordinates. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while inserting the values. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function BTBDServiceQuery(ByVal xmlServiceQueryInfo As String, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_BTBDServiceQuery"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlServiceQueryInfo", xmlServiceQueryInfo)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetSmartTechBrandsSelection(ByVal CompanyId As Integer, ByVal BrandId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "sp_SmartTechBrandsSelection"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@BrandId", BrandId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SmartTechBrandsInfo")
            da.Fill(ds)
            ds.Tables(0).TableName = "SmartTechBrandsInfo"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    Public Shared Function GetEngineerRateInfo(ByVal ContactId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "sp_GetEngineerRateInfo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactId", ContactId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("EngineerRateInfo")
            da.Fill(ds)
            ds.Tables(0).TableName = "EngineerRateInfo"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    Public Shared Function SaveEngineerRateInfo(ByVal xmlEngineerRateInfo As String, ByVal ContactId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "sp_UpdateEngineerRateInfo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlEngineerRateInfo", xmlEngineerRateInfo)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function UpdatePostCodeLatLong(ByVal Postcode As String, ByVal latitude As String, ByVal longitude As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "SP_CheckPostcodeExist"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            Postcode = OrderWorkLibrary.CommonFunctions.FormatPostcode(Postcode)
            cmd.Parameters.AddWithValue("@postCode", Postcode)
            cmd.Parameters.AddWithValue("@latitude", latitude)
            cmd.Parameters.AddWithValue("@longitude", longitude)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetAddress(ByVal RecordType As String, ByVal RecordID As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetAddress"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RecordType", RecordType)
            cmd.Parameters.AddWithValue("@RecordID", RecordID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetWOInfoAfterCreation(ByVal refWOID As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spGetWOInfoAfterCreation"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@refWOID", refWOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOInfoAfterCreation")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    Public Shared Function GetPostCodeCoordinates(ByVal postCode As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_WO_GetPostCodeCo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@postCode", postCode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("postCode")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblpostCode"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function GetAccountSourceAndPaymentTerms(ByVal WOID As String, ByVal CompanyID As String) As DataSet
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spGetAccountSourceAndPaymentTerms"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Source"
            ds.Tables(1).TableName = "PaymentTerm"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function MS_GetContactsDefaultAddress(ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spMS_GetContactsDefaultAddress"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


End Class
