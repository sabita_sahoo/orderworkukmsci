Imports System.Data.SqlClient

''' <summary>
''' Class to access the database for finance
''' </summary>
''' <remarks></remarks>
Public Class DBFinance

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="SelMonth"></param>
    ''' <param name="SelYear"></param>
    ''' <param name="EndDate"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CompletedSalesReport(ByVal SelMonth As String, ByVal SelYear As String, ByVal EndDate As String, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spAdmin_CompletedSalesRpt"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@SelMonth", SelMonth)
            cmd.Parameters.AddWithValue("@SelYear", SelYear)

            cmd.Parameters.AddWithValue("@EndDate", EndDate)

            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            ds.Tables(0).TableName = "Closed"
            ds.Tables(1).TableName = "ClosedVal"
            ds.Tables(2).TableName = "Listing"
            ds.Tables(3).TableName = "VATonListing"
            ds.Tables(4).TableName = "Comm"
            ds.Tables(5).TableName = "VATonComm"
            ds.Tables(6).TableName = "Cancelled"
            ds.Tables(7).TableName = "CancelledVal"

            ''funds
            ds.Tables(8).TableName = "FundsToRecvd"
            ds.Tables(9).TableName = "FundsRecvd"
            ds.Tables(10).TableName = "OutFundsToRecv"
            ds.Tables(11).TableName = "SuppPaym"
            ds.Tables(12).TableName = "ClientBal"



            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="ContactID"></param>
    ''' <param name="Particulars"></param>
    ''' <param name="Amount"></param>
    ''' <param name="Comments"></param>
    ''' <param name="AdminCompanyID"></param>
    ''' <param name="AdminContactID"></param>
    ''' <param name="DateCreated"></param>
    ''' <remarks></remarks>
    Public Shared Sub MS_CreatePartPayment(ByVal BizDivId As Integer, ByVal CompanyID As Integer, ByVal ContactID As Integer, ByVal Particulars As String, ByVal Amount As Decimal, ByVal Comments As String, ByVal AdminCompanyID As Integer, ByVal AdminContactID As Integer, ByVal DateCreated As String)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_CreatePartPayment"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@Particulars", Particulars)
            cmd.Parameters.AddWithValue("@Amount", Amount)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@AdminCompanyID", AdminCompanyID)
            cmd.Parameters.AddWithValue("@AdminContactID", AdminContactID)
            cmd.Parameters.AddWithValue("@DateCreated", DateCreated)

            cmd.ExecuteNonQuery()
            'Dim ds As New DataSet("UpSellSalesInvoices")
            'Dim da As New SqlDataAdapter(cmd)
            'da.Fill(ds)
            'ds.Tables(0).TableName = "tblUpSellSalesInvoices"
            'ds.Tables(1).TableName = "tblCount"
            'Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CurrentSalesReport(ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim ProcedureName As String = "spAdmin_CurrentSalesRpt"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            ds.Tables(0).TableName = "Submitted"
            ds.Tables(1).TableName = "SubmittedVal"
            ds.Tables(2).TableName = "CA"
            ds.Tables(3).TableName = "CAVal"
            ds.Tables(4).TableName = "Active"
            ds.Tables(5).TableName = "ActiveVal"
            ' ds.Tables(4).TableName = "MinusCancelledVal"
            ds.Tables(6).TableName = "Closed"
            ds.Tables(7).TableName = "ClosedVal"
            ds.Tables(8).TableName = "Cancelled"
            ds.Tables(9).TableName = "CancelledVal"
            ds.Tables(10).TableName = "Listing"
            ds.Tables(11).TableName = "VATonListingFee"
            ds.Tables(12).TableName = "Comm"
            ds.Tables(13).TableName = "VATonComm"

            ''outstanding wos
            ds.Tables(14).TableName = "OSubmit"
            'ds.Tables(15).TableName = "OSent"
            ds.Tables(15).TableName = "OCA"
            ds.Tables(16).TableName = "OActive"
            ds.Tables(17).TableName = "OIssue"
            ds.Tables(18).TableName = "OCompleted"

            ''funds
            ds.Tables(19).TableName = "FundsToRecvd"
            ds.Tables(20).TableName = "FundsRecvd"
            ds.Tables(21).TableName = "OutFundsToRecv"
            ds.Tables(22).TableName = "SuppPaym"
            ds.Tables(23).TableName = "ClientBal"

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="AdviceType"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="Status"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="RequestFrom"></param>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetSalesReceiptAdviceListing(ByVal BizDivId As Integer, ByVal AdviceType As String, ByVal CompanyId As String, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal RequestFrom As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            If FromDate Is Nothing Then
                FromDate = ""
            End If

            If ToDate Is Nothing Then
                ToDate = ""
            End If
            Dim ProcedureName As String = "spMSAccounts_GetOrderWorkInvoicesForBuyer"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@InvoiceType", AdviceType)
            cmd.Parameters.AddWithValue("@BuyerId", CompanyId)
            cmd.Parameters.AddWithValue("@InvoiceStatus", Status)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", MaximumRows)
            Dim ds As New DataSet("SalesInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSalesInvoices"
            ds.Tables(1).TableName = "tblCount"
            HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="AdviceType"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="Status"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="RequestFrom"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetSalesReceiptAdviceListingCount(ByVal BizDivId As Integer, ByVal AdviceType As String, ByVal CompanyId As String, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal RequestFrom As String) As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function




    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="Status"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="RequestFrom"></param>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetOrderWorkInvoicesForBuyer(ByVal BizDivId As Integer, ByVal CompanyId As String, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal RequestFrom As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer, ByVal BillAddress As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            If FromDate Is Nothing Then
                FromDate = ""
            End If

            If ToDate Is Nothing Then
                ToDate = ""
            End If
            Dim ProcedureName As String = "spMSAccounts_GetOrderWorkInvoicesForBuyer"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@BuyerId", CompanyId)
            cmd.Parameters.AddWithValue("@InvoiceStatus", Status)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", MaximumRows)
            cmd.Parameters.AddWithValue("@BillAddress", BillAddress)
            cmd.Parameters.AddWithValue("@LoggedInUserID", HttpContext.Current.Session("PortalUserId"))
            Dim ds As New DataSet("SalesInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            HttpContext.Current.Session("dsOWFinance") = ds
            ds.Tables(0).TableName = "tblSalesInvoices"
            ds.Tables(1).TableName = "tblCount"
            HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="Status"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="RequestFrom"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetOrderWorkInvoicesForBuyerCount(ByVal BizDivId As Integer, ByVal CompanyId As String, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal RequestFrom As String, ByVal BillAddress As Integer) As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="CompanyID"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetBankDetails(ByVal CompanyID As Integer, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetBankDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblBankDetails"
            Return ds
        Catch ex As Exception
            Throw ex    'ExceptionUtil.CreateSoapException("Exception in spAccounts_GetBankDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving bank details and the supplier balance for the given company id. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="bizDivId"></param>
    ''' <param name="InvoiceNos"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MSAccounts_ChangePIStatusToRequested(ByVal bizDivId As Integer, ByVal InvoiceNos As String, ByVal CompanyID As Integer, ByVal UserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_ChangePIStatusToRequested"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", bizDivId)
            cmd.Parameters.AddWithValue("@InvoiceNos", InvoiceNos)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserID)
            Dim ds As New DataSet("InvoiceRequested")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="Account"></param>
    ''' <param name="SubAccount"></param>
    ''' <param name="StartDate"></param>
    ''' <param name="EndDate"></param>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetAccountVoucherDetails(ByVal BizDivId As Integer, ByVal Account As String, ByVal SubAccount As Integer, ByVal StartDate As String, ByVal EndDate As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetAccountVoucherDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@Account", Account)
            cmd.Parameters.AddWithValue("@SubAccount", SubAccount)
            cmd.Parameters.AddWithValue("@FromDate", StartDate)
            cmd.Parameters.AddWithValue("@ToDate", EndDate)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim ds As New DataSet("VoucherDetails")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAccountVoucherDetails"
            ds.Tables(1).TableName = "tblCount"
            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetPurchaseAdviceDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetPurchaseAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblBillingAddress"
            ds.Tables(2).TableName = "tblCustomerDetails"
            ds.Tables(3).TableName = "tblOWAddress"
            Return ds
        Catch ex As Exception
            Throw ex        'ExceptionUtil.CreateSoapException("Exception in spAccounts_GetAdviceListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Listing information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This function will get the list of invoices for the print form
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetPurchaseAdviceDetails_PrintList(ByVal BizDivId As Integer, ByVal InvoiceNumber As String, ByVal UserId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "[spMSAccounts_GetPurchaseAdviceDetails_PrintList]"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@CustomerId", UserId)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblOWAddress"
            Return ds
        Catch ex As Exception
            Throw ex        'ExceptionUtil.CreateSoapException("Exception in spAccounts_GetAdviceListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Listing information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="bizDivId"></param>
    ''' <param name="InvoiceNos"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_ChangePIStatusToAvailable(ByVal bizDivId As Integer, ByVal InvoiceNos As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_ChangePIStatusToAvailable"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", bizDivId)
            cmd.Parameters.AddWithValue("@InvoiceNos", InvoiceNos)
            Dim ds As New DataSet("InvoiceAvailable")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="SupplierAccount"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="Over30Day"></param>
    ''' <param name="HideAvailable"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetPurchaseInvoicesForStatusUA(ByVal SupplierAccount As String, ByVal BizDivId As Integer, ByVal Over30Day As Boolean, ByVal HideAvailable As Boolean, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetPurchaseInvoicesForStatusUA"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@SupplierAccount", SupplierAccount)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@Over30Day", Over30Day.ToString)
            cmd.Parameters.AddWithValue("@HideAvailable", HideAvailable)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblPurchInvListing"
            ds.Tables(1).TableName = "tblCount"
            ds.Tables(2).TableName = "tblUnPaidCount"
            Return ds
        Catch ex As Exception
            'Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetAdviceListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Listing information for the search criterion. Please try again.", ex)
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="SIs"></param>
    ''' <param name="BuyerId"></param>
    ''' <param name="ReceiptDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MSAccounts_GenerateCashReceipt(ByVal BizDivId As Integer, ByVal SIs As String, ByVal BuyerId As Integer, ByVal ReceiptDate As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GenCashReceipt"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@SIs", SIs)
            cmd.Parameters.AddWithValue("@BuyerId", BuyerId)
            cmd.Parameters.AddWithValue("@ReceiptDate", ReceiptDate)
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            'Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetSupplierPaymentDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="XMLContent"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="SIs"></param>
    ''' <param name="BuyerId"></param>
    ''' <param name="ReceiptDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MSAccounts_AllocateFunds(ByVal XMLContent As String, ByVal BizDivId As Integer, ByVal SIs As String, ByVal BuyerId As Integer, ByVal ReceiptDate As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_AssignUnAllocatedFunds"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@XMLContent", XMLContent)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@SIs", SIs)
            cmd.Parameters.AddWithValue("@BuyerId", BuyerId)
            cmd.Parameters.AddWithValue("@ReceiptDate", ReceiptDate)
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            'Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GenCashReceipt. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="AdviceNumber"></param>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetWOsForSalesInvoice(ByVal BizDivId As Integer, ByVal AdviceNumber As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetWOsForSalesInvoice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@AdviceNumber", AdviceNumber)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrders")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrders"
            ds.Tables(1).TableName = "tblCount"
            HttpContext.Current.Items("rowCountWOSalesinvoice") = ds.Tables(1).Rows(0).Item("TotalRecs")
            Return ds
        Catch ex As Exception
            'Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetSupplierPaymentDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="AdviceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetWOsForSalesInvoiceCount(ByVal BizDivId As Integer, ByVal AdviceNumber As String) As Integer
        Return HttpContext.Current.Items("rowCountWOSalesinvoice")
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="xmlContent"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_RefundBuyer(ByVal xmlContent As String)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String
            ProcedureName = "spMSAccounts_RefundBuyer"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlContent", xmlContent)
            Dim da As New SqlDataAdapter(cmd)
            cmd.ExecuteNonQuery()
            Return ""
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in InsertPostCodeCoordinates. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while inserting the values. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="WOIDs"></param>
    ''' <param name="BuyerId"></param>
    ''' <param name="InvoiceDate"></param>
    ''' <param name="ListingFee"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MSAccounts_GenerateSalesInvoice(ByVal BizDivId As Integer, ByVal WOIDs As String, ByVal BuyerId As Integer, ByVal InvoiceDate As String, ByVal ListingFee As Decimal) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GenSalesInvoice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@WOIDs", WOIDs)
            cmd.Parameters.AddWithValue("@BuyerId", BuyerId)
            cmd.Parameters.AddWithValue("@InvoiceDate", InvoiceDate)
            cmd.Parameters.AddWithValue("@ListingFee", ListingFee)
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            'Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetSupplierPaymentDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetSalesAdviceDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetSalesAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblWODetails"
            ds.Tables(2).TableName = "tblBillingAddress"
            ds.Tables(3).TableName = "tblOrderWorkDetails"
            ds.Tables(4).TableName = "tblOWAddress"
            ds.Tables(5).TableName = "tblBankDetails"
            ds.Tables(6).TableName = "tblListingFee"
            ds.Tables(7).TableName = "tblWorkOrderList"
            ds.Tables(8).TableName = "tblFinanceFields"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <param name="UserId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetSalesAdviceDetails_PrintList(ByVal BizDivId As Integer, ByVal InvoiceNumber As String, ByVal UserId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetSalesAdviceDetails_PrintList"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@CustomerId", UserId)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblWODetails"
            ds.Tables(2).TableName = "tblOrderWorkDetails"
            ds.Tables(3).TableName = "tblOWAddress"
            ds.Tables(4).TableName = "tblBankDetails"
            Return ds
        Catch ex As Exception
            Throw ex    'ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GetSalesAdviceDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Listing information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="AdviceType"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="Status"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="RequestFrom"></param>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetPaidSalesListing(ByVal BizDivId As Integer, ByVal AdviceType As String, ByVal CompanyId As String, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal RequestFrom As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetPaidSalesListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@AdviceType", AdviceType)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@RequestFrom", RequestFrom)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim ds As New DataSet("SalesInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSalesInvoices"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="AdviceType"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="Status"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="RequestFrom"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetPurchasePaymentAdviceListing(ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                               ByVal maximumRows As Integer, ByVal BizDivId As Integer, ByVal AdviceType As String, ByVal CompanyId As String, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal RequestFrom As String) As DataSet
        If sortExpression = "" Then
            sortExpression = "InvoiceDate"
        End If
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            If FromDate Is Nothing Then
                FromDate = ""
            End If

            If ToDate Is Nothing Then
                ToDate = ""
            End If

            Dim ProcedureName As String = "spMSAccounts_GetPurchaseInvoicesForSupplier"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@AdviceType", AdviceType)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@RequestFrom", RequestFrom)
            cmd.Parameters.AddWithValue("@SortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", maximumRows)
            Dim ds As New DataSet("PurchaseInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblPurchaseInvoices"
            ds.Tables(1).TableName = "tblCount"
            HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
            If Status = "UnpaidAvailable" Then
                ds.Tables(2).TableName = "tblAvailableCount"
            End If
            If Status = "Available" Then
                HttpContext.Current.Items("ShowWithDraw") = "No"
                If Not ds.Tables(2) Is Nothing Then
                    If ds.Tables(2).Rows.Count > 0 Then
                        HttpContext.Current.Items("ShowWithDraw") = ds.Tables(2).Rows(0).Item(0)
                    End If
                End If
            End If
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="AdviceType"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="Status"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="RequestFrom"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SelectMyInvoiceListingCount(ByVal BizDivId As Integer, ByVal AdviceType As String, ByVal CompanyId As String, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal RequestFrom As String) As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="SubAccount"></param>
    ''' <param name="Status"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="UpdateVoucherStatus"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MSAccounts_GetSupplierPaymentDetails(ByVal SubAccount As Integer, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivId As Integer, ByVal UpdateVoucherStatus As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetSupplierPaymentDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@SubAccount", SubAccount)
            cmd.Parameters.AddWithValue("@Status", Status)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
                'cmd.Parameters.AddWithValue("@FromDate", FromDate)
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
                'cmd.Parameters.AddWithValue("@ToDate", ToDate)
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@UpdateVoucherStatus", UpdateVoucherStatus)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblPaymentDetails"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            'Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetSupplierPaymentDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving records for the search criterion. Please try again.", ex)
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="voucherDetails"></param>
    ''' <param name="dateCompleted"></param>
    ''' <param name="bizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MSAccounts_UpdateVoucherStatusToComplete(ByVal voucherDetails As String, ByVal dateCompleted As String, ByVal bizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_UpdateVoucherStatusToComplete"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@VoucherDetails", voucherDetails)
            If dateCompleted <> "" Then
                cmd.Parameters.AddWithValue("@DateCompleted", CommonFunctions.convertDate(dateCompleted))
            Else
                cmd.Parameters.AddWithValue("@DateCompleted", dateCompleted)
            End If

            cmd.Parameters.AddWithValue("@BizDivId", bizDivID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSuppliers"
            ds.Tables(1).TableName = "tblPaymentDetails"
            Return ds
        Catch ex As Exception
            'Throw ExceptionUtil.CreateSoapException("Exception in UpdateVoucherStatusToComplete. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while upda. Please try again.", ex)
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="Todate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MSAccounts_UpdateStatusToInProcess(ByVal BizDivId As Integer, ByVal FromDate As String, ByVal Todate As String)
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_UpdateVoucherStatusToInprocess"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", Todate)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            cmd.ExecuteNonQuery()
            Return Nothing
        Catch ex As Exception
            'Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetSupplierPaymentDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="Status"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetUpSellSalesReceiptAdviceListing(ByVal BizDivId As Integer, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetUpSellSalesReceiptAdviceListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)

            Dim ds As New DataSet("UpSellSalesInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblUpSellSalesInvoices"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="SINumber"></param>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetCRCNListing(ByVal BizDivId As Integer, ByVal SINumber As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetCRCNListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@SINumber", SINumber)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim ds As New DataSet("SalesInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCRCNs"
            ds.Tables(1).TableName = "tblCount"
            HttpContext.Current.Items("rowCountCRCN") = ds.Tables(1).Rows(0).Item("TotalRecs")
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="SINumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetCRCNListingCount(ByVal BizDivId As Integer, ByVal SINumber As String) As Integer
        Return HttpContext.Current.Items("rowCountCRCN")
    End Function


    ''' <summary>
    ''' This method returns the details about the cash payment receipt
    ''' </summary>
    ''' <param name="bizDivId"></param>
    ''' <param name="AdviceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetPaymentAdviceDetails(ByVal bizDivId As Integer, ByVal AdviceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetPaymentAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", bizDivId)
            cmd.Parameters.AddWithValue("@AdviceNumber", AdviceNumber)
            Dim ds As New DataSet("PaymentDetails")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblPaymentDetails"
            ds.Tables(2).TableName = "tblBillingAddress"
            ds.Tables(3).TableName = "tblCustomerDetails"
            ds.Tables(4).TableName = "tblOWAddress"

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function



    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetUnAllocatedReceiptAdviceDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetUnAllocatedReceiptAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblInvoices"
            ds.Tables(2).TableName = "tblBillingAddress"
            ds.Tables(3).TableName = "tblOrderWorkDetails"
            ds.Tables(4).TableName = "tblOWAddress"
            ds.Tables(5).TableName = "tblUnAllocatedReceipt"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetReceiptDetails(ByVal BizDivId As Integer, ByVal ReceiptNo As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetReceiptDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ReceiptNo", ReceiptNo)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblInvoices"
            ds.Tables(2).TableName = "tblBillingAddress"
            ds.Tables(3).TableName = "tblOrderWorkDetails"
            ds.Tables(4).TableName = "tblOWAddress"
            ds.Tables(5).TableName = "tblUnAllocatedReceipt"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetReceiptAdviceDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetReceiptAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblInvoices"
            ds.Tables(2).TableName = "tblBillingAddress"
            ds.Tables(3).TableName = "tblOrderWorkDetails"
            ds.Tables(4).TableName = "tblOWAddress"
            ds.Tables(5).TableName = "tblUnAllocatedReceipt"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Returns a dataset with credit note details
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns>Returns a dataset with credit note details </returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetCreditNoteDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetCreditNoteDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            ds.Tables(0).TableName = "tblBillingAddress"
            ds.Tables(1).TableName = "tblCompRegNo"
            ds.Tables(2).TableName = "tblOWAddress"
            ds.Tables(3).TableName = "tblCreditNoteDetails"
            ds.Tables(4).TableName = "tblTotalAmount"
            ds.Tables(5).TableName = "tblCreditTerms"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Returns a dataset with Debit note details
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns>Returns a dataset with credit note details </returns>
    ''' <remarks></remarks>
    Public Shared Function MS_GetDebitNoteDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMSAccounts_GetDebitNoteDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            ds.Tables(0).TableName = "tblBillingAddress"
            ds.Tables(1).TableName = "tblCompRegNo"
            ds.Tables(2).TableName = "tblOWAddress"
            ds.Tables(3).TableName = "tblDebitNoteDetails"
            ds.Tables(4).TableName = "tblTotalAmount"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

End Class
