Option Strict On

Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Xml
Imports System.Web
Imports System.IO
Imports System.Net
Imports System.Diagnostics


Public Class DBUtil

    Private Shared _ConnectionString As String = System.Configuration.ConfigurationManager.AppSettings("connection.webdb")
    Public Shared Property ConnectionString() As String
        Get
            Select Case OrderWorkLibrary.ApplicationSettings.Country.ToString
                Case OrderWorkLibrary.ApplicationSettings.CountryUK.ToString
                    Return System.Configuration.ConfigurationManager.AppSettings("connection.webdbUK")
                Case OrderWorkLibrary.ApplicationSettings.CountryDE.ToString
                    Return System.Configuration.ConfigurationManager.AppSettings("connection.webdbDE")
                Case Else
                    Return System.Configuration.ConfigurationManager.AppSettings("connection.webdb")
            End Select
        End Get
        Set(ByVal value As String)
            _ConnectionString = value
        End Set
    End Property

    Shared Function GetDBTime() As DateTime
        Return GetDBTime()
    End Function

    Shared Function getDBConn() As SqlClient.SqlConnection
        Dim Con As New SqlConnection
        Con = DBUtil.Connect(ConnectionString)
        Return Con
    End Function

    Shared Function GetDBTime(ByVal ConnectionName As String) As DateTime
        Dim sqlConn As SqlConnection = Connect(ConnectionName)
        Dim cmd As New SqlCommand("select getdate()", sqlConn)
        Dim CurrentTime As DateTime = CType(cmd.ExecuteScalar, DateTime)
        Return CurrentTime
    End Function

    Shared Function Connect(ByVal ConnectionName As String) As SqlConnection
        Dim connectionObject As New SqlConnection
        Try
            connectionObject = New SqlConnection(ConnectionString)
            connectionObject.Open()
            Return connectionObject
        Catch Ex As Exception
            connectionObject.Close()
            Throw Ex
        Finally
            Trace.Unindent() : Trace.WriteLine("Exiting...")
        End Try
    End Function


End Class

