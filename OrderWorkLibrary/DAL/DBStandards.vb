Imports System.Data.SqlClient

''' <summary>
''' Class to access the database for standard data
''' </summary>
''' <remarks></remarks>
Public Class DBStandards

    ''' <summary>
    ''' to return all main category types defined in table "tblStandardsWOTypesLinkage"
    ''' </summary>
    ''' <param name="paramBizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetMainCategoryTypes(ByVal paramBizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spStandards_GetMainCatTypes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "MainCategoryTypes"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This method will return all sub  category types for maincatid defined in tblStandardsWOTypesLinkage.
    ''' </summary>
    ''' <param name="mainCatId"></param>
    ''' <param name="paramBizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSubCategoryTypes(ByVal mainCatId As Integer, ByVal paramBizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spStandards_GetSubCatTypes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@MainCatId", mainCatId)
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "SubCategoryTypes"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' to return general standards required
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetGeneralStandards() As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spLookUp_GetCompanyStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "General"
            ds.Tables(1).TableName = "CertQual"
            ds.Tables(2).TableName = "Regions"
            ds.Tables(3).TableName = "Skills"
            ds.Tables(4).TableName = "Country"
            ds.Tables(5).TableName = "Industries"
            ds.Tables(6).TableName = "RolesGroups"
            ds.Tables(7).TableName = "AdminUsers"
            'OA-622
            ds.Tables(8).TableName = "DiscountPaymentTerm"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Method to return all standards defined in tblStandardsGeneral for the passed label and business of orderwork.
    ''' </summary>
    ''' <param name="Label"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStandardsForWebContent(ByVal Label As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn


        Try
            Dim ProcedureName As String = "spLookUp_GetStandardsForWebContent"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Label", Label)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "StandardsGeneral"
            ds.Tables(1).TableName = "Business"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function GetServices(ByVal companyID As Integer) As DataSet
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spLookUp_GetServices"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", companyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Services"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function InsertServiceVouchers(ByVal xmlContent As String, ByVal companyID As Integer) As DataSet
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "sp_InsertServiceVoucher"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", companyID)
            cmd.Parameters.AddWithValue("@xmlDoc", xmlContent)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function IsDuplicateVoucher(ByVal CompanyID As Integer, ByVal VoucherCode As String) As DataSet
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "sp_IsDuplicateVoucherCode"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@VoucherCode", VoucherCode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    Public Shared Function DoesServiceExist(ByVal CompanyID As Integer, ByVal ServiceID As Integer) As DataSet
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "sp_IsServiceExist"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ServiceID", ServiceID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


End Class
