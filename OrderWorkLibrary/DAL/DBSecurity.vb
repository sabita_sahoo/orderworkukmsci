Imports System.Data.SqlClient

Public Class DBSecurity


    Public Shared Function AuthenticateUser(ByVal UserName As String, ByVal Password As String, ByVal paramBizDivId As Integer, ByVal BusinessID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.CountryUK, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            'Defining the stored procedure to use "spSecurity_AuthenticateUser"
            Dim ProcedureName As String = "spSecurity_AuthenticateUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            'setting the command type and also parameter of the Stored procedure
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", UserName)
            cmd.Parameters.AddWithValue("@Password", Password)
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            cmd.Parameters.AddWithValue("@BusinessID", BusinessID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            'Fill dataset as return by the stored procedure "spSecurity_AuthenticateUser" 
            da.Fill(ds)
            ' Name the tables of the dataset
            If ds.Tables(0).Rows.Count <> 0 Then
                ds.Tables(0).TableName = "LoginInfo"
                ds.Tables(1).TableName = "ParentMenu"
                ds.Tables(2).TableName = "ChildMenu"
            End If
            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try

        End Try
    End Function



    Public Shared Function AuthenticateAdminUser(ByVal UserName As String, ByVal Password As String, ByVal BusinessID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.CountryUK, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            'Defining the stored procedure to use "spSecurity_AuthenticateUser"
            Dim ProcedureName As String = "spSecurity_AuthenticateUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            'setting the command type and also parameter of the Stored procedure
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", UserName)
            cmd.Parameters.AddWithValue("@Password", Password)
            cmd.Parameters.AddWithValue("@BizDivId", 0)
            cmd.Parameters.AddWithValue("@BusinessID", BusinessID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            'Fill dataset as return by the stored procedure "spSecurity_AuthenticateUser" 
            da.Fill(ds)
            ' Name the tables of the dataset
            If ds.Tables(0).Rows.Count <> 0 Then
                ds.Tables(0).TableName = "LoginInfo"
            End If
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function



    Public Shared Function UpdateLoginDate(ByVal ContactID As Integer) As Boolean
        CommonFunctions.setCultureSettings(ApplicationSettings.CountryUK, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spSecurity_UpdateLoginDate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will return the different levels of menu that user has access to.
    ''' </summary>
    ''' <param name="RoleCategory"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRolesGroups(ByVal RoleCategory As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.CountryUK, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spSecurity_GetRolesGroups"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RoleCategory", RoleCategory)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    



  

    ''' <summary>
    ''' To return status that is accessible to the Role Group that Contact belongs to
    ''' </summary>
    ''' <param name="RoleGroupID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStatuses(ByVal RoleGroupID As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.CountryUK, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn

        Try
            Dim ProcedureName As String = "spSecurity_GetStatuses"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RoleGroupID", RoleGroupID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    Public Shared Function GetSecurityQuesAns(ByVal Email As String) As DataSet
        'Opening a connection string to connect to the DB and dispose as not required.
        CommonFunctions.setCultureSettings(ApplicationSettings.CountryUK, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spContacts_GetSecurityQuesAns"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", Email)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try


    End Function

    Public Shared Function ChangeLoginInfo(ByVal ContactID As Integer, ByVal UserName As String, ByVal OldPassword As String, ByVal NewPassword As String, ByVal SecurityQuest As Integer, ByVal SecurityAns As String, ByVal LoggedInUserID As Integer) As Integer
        CommonFunctions.setCultureSettings(ApplicationSettings.CountryUK, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try

            Dim success As Integer
            Dim ProcedureName As String

            ProcedureName = "spSecurity_ChangeLoginInfo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@contactID", ContactID)
            cmd.Parameters.AddWithValue("@UserName", UserName)
            cmd.Parameters.AddWithValue("@OldPassword", OldPassword)
            cmd.Parameters.AddWithValue("@NewPassword", NewPassword)
            cmd.Parameters.AddWithValue("@SecurityQues", SecurityQuest)
            cmd.Parameters.AddWithValue("@SecurityAns", SecurityAns)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Decimal).Direction = ParameterDirection.Output
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.ExecuteNonQuery()

            'Change made by PB: to get bizdivid as the return parameter. For mailers.
            'If CInt(cmd.Parameters.Item("@Success").Value) = 1 Then
            '    success = True
            'Else
            '    success = False
            'End If
            success = cmd.Parameters.Item("@Success").Value
            Return success
        Catch TransEx As Exception
            Throw TransEx 'ExceptionUtil.CreateSoapException("Error in Update for tables: " & tablesForUpdate & ControlChars.CrLf & TransEx.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Error updating contacts information.")
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    Public Shared Function ForgotPassword(ByVal UserName As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.CountryUK, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Dim tablesForUpdate As String = ""
        Try

            Dim ProcedureName As String
            Dim cmd As SqlCommand

            'Login Table
            ProcedureName = "spSecurity_ForgotPassword"
            cmd = New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", UserName)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            cmd.ExecuteNonQuery()

            '  theTrans.Commit()
            Return ds 'password
        Catch TransEx As Exception
            Trace.WriteLine("Exception Updating " & tablesForUpdate & " Tables. Trying Rollback.")
            Trace.WriteLine(TransEx.Message)
            Trace.WriteLine(TransEx.StackTrace)
            Try
                '  theTrans.Rollback()
                '  Trace.WriteLine("Rollback Successfull")
            Catch Rex As Exception
                Trace.WriteLine("RollBack Failed...")
                Trace.WriteLine(TransEx.Message)
                Trace.WriteLine(TransEx.StackTrace)
                Throw Rex 'ExceptionUtil.CreateSoapException("Error occurred during Rollback " & TransEx.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Error while updating Branch information")
            End Try
            Throw TransEx 'ExceptionUtil.CreateSoapException("Error in Update for tables: " & tablesForUpdate & ControlChars.CrLf & TransEx.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Error updating contacts information.")
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This webmethod returns accounts activation status with contactid and maincontactid
    ''' </summary>
    ''' <param name="ContactId"></param>
    ''' <param name="MainContactId"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="oldStatus"></param>
    ''' <param name="newStatus"></param>
    ''' <returns>Dataset with table value as ContactID, MainContactId, bizDivId</returns>
    ''' <remarks></remarks>
    Public Shared Function UpdateStatus(ByVal ContactId As Integer, ByVal MainContactId As Integer, ByVal BizDivId As Integer, ByVal oldStatus As Integer, ByVal newStatus As Integer) As Boolean
        CommonFunctions.setCultureSettings(ApplicationSettings.CountryUK, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spSecurity_UpdateUserStatus"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@MainContactId", MainContactId)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@oldStatus", oldStatus)
            cmd.Parameters.AddWithValue("@newStatus", newStatus)

            'Return 1 if command executed sucessfully
            cmd.Parameters.Add("@Success", SqlDbType.Bit).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value


        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    Public Shared Function UpdateAddedUserStatus(ByVal Email As String, ByVal question As Integer, ByVal answer As String, ByVal password As String, ByVal BizDivId As Integer)
        CommonFunctions.setCultureSettings(ApplicationSettings.CountryUK, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spSecurity_UpdateAddedUserStatus"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Email", Email)
            cmd.Parameters.AddWithValue("@Question", question)
            cmd.Parameters.AddWithValue("@Answer", answer)
            cmd.Parameters.AddWithValue("@Password", password)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value


        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This function is intended to Authenticate user using IPhone Application
    ''' </summary>
    ''' <param name="UserName"></param>
    ''' <param name="Password"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AuthenticateIPhoneUser(ByVal UserName As String, ByVal Password As String, ByVal DeviceID As String, ByVal token As String) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.CountryUK, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            'Defining the stored procedure to use "spSecurity_AuthenticateUser"
            Dim ProcedureName As String = "spSecurity_AuthenticateIPhoneUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            'setting the command type and also parameter of the Stored procedure
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", UserName)
            cmd.Parameters.AddWithValue("@Password", Password)
            cmd.Parameters.AddWithValue("@BizDivId", 1)
            cmd.Parameters.AddWithValue("@DeviceID", DeviceID)
            cmd.Parameters.AddWithValue("@Token", token)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            'Fill dataset as return by the stored procedure "spSecurity_AuthenticateUser" 
            da.Fill(ds)
            ' Name the tables of the dataset
            If ds.Tables(0).Rows.Count <> 0 Then
                ds.Tables(0).TableName = "LoginInfo"
            End If
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

End Class
