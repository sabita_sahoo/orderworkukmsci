﻿var keycode;
var idArr = new Array();
function getGoodsLocation(myval)
{if (/^13$|^9$|^27$|^38$|^40$/.test(keycode))
        return false;
    else
        $.ajax({
            type: "POST",
            url: "WOForm.aspx/GetGoodsLocation",
            data: "{'prefixText':'" + myval + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var json = JSON.stringify(data);
                //alert(json);              
                $('#searchResult').html("");
                var sresult = "<table id='tbl'>";
                var loop = data.d;
                $.each(loop.rows, function (i, rows) {
                    sresult += "<tr id=" + rows.AddressID + " onclick='javascript:callback(this.id)' onmouseover='javascript:highlight(this.id)'  ><td>"
                        + rows.Name + "</td></tr>";
                    idArr[i] = rows.AddressID;
                });
                sresult += "</table>";
                results = sresult;
                $('#searchResult').append(sresult);
                $('#searchResult').slideDown("slow");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var jsonError = JSON.parse(XMLHttpRequest.responseText);
                alert(JSON.stringify(jsonError));
            } 
        });
    }

    function getWorkOrderID(myval) 
    {
        if (/^13$|^9$|^27$|^38$|^40$/.test(keycode))
            return false;
        else
            $.ajax({
                type: "POST",
                url: "SupplierWOListing.aspx/GetWorkOrderID",
                data: "{'prefixText':'" + myval + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.stringify(data);
                    //alert(json);              
                    $('#searchResult').html("");
                    var sresult = "<table id='tbl'>";
                    var loop = data.d;
                    $.each(loop.rows, function (i, rows) {
                        sresult += "<tr id=" + rows.RefWOID + " onclick='javascript:callback(this.id)'  onmouseover='javascript:highlight(this.id)'  ><td>"
                        + rows.RefWOID + "</td></tr>";
                        idArr[i] = rows.RefWOID;
                    });
                    sresult += "</table>";
                    results = sresult;
                    $('#searchResult').append(sresult);
                    $('#searchResult').slideDown("slow");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var jsonError = JSON.parse(XMLHttpRequest.responseText);
                    alert(JSON.stringify(jsonError));
                }
            });
        }

function highlight(id)
{normalize();document.getElementById(id).className = "highlight";}
function normalize()
{var i = 0;
for(i=0; i<idArr.length; i++)
document.getElementById(idArr[i]).className = "normalizeKeyword";
}
function processKey(e) {
    // handling up/down/escape requires results to be visible
    // handling enter/tab requires that AND a result to be selected    
keycode = e.keyCode;  
if (/^13$|^9$|^27$|^38$|^40$/.test(keycode)) {
    keycode = e.keyCode;	    
    switch(keycode) {
    case 38:prevRes();	break;	// up
    case 40:nextRes(); break;	// down
    case 27:Hide();break;	//	escape
    case 9:	case 13:selCurRes();break;	// tab/enter
}}}
function getCurSel() {  
    var selected = 0;
    if (document.getElementById(idArr[0]) != null)
	{for(i=0; i<idArr.length; i++)
	    if (document.getElementById(idArr[i]).className == "highlight")
            return idArr[i];            
    return 0;}
    else
        return null;}

function selCurRes() {
	var curRes = getCurSel();
	callback(curRes);}

function nextRes() {
var curRes = getCurSel();	
var flag = false;
normalize();
for(i=0; i<idArr.length - 1; i++)
    if (idArr[i] == curRes)
    {   flag = true;
        document.getElementById(idArr[i+1]).className = "highlight";}
if (flag == false)
    document.getElementById(idArr[0]).className = "highlight";}
function prevRes() {
var curRes = getCurSel();
var flag = false;
normalize();
for(i=1; i<idArr.length; i++)
    if (idArr[i] == curRes)
    {   flag = true;
        document.getElementById(idArr[i-1]).className = "highlight";}
if (flag == false)
document.getElementById(idArr[idArr.length-1]).className = "highlight";}


//****************Accreditations starts here******************//
function getResultsAccred(id) {
    var txtExpertise = GetClientId("txtExpertise");
    var divMoreInfoAutoSuggest = txtExpertise.replace("txtExpertise", "divMoreInfoAutoSuggest");
    var hdnType = divMoreInfoAutoSuggest.replace("divMoreInfoAutoSuggest", "hdnType");
    var mode = document.getElementById(hdnType).value;
    var hdnCommonId = divMoreInfoAutoSuggest.replace("divMoreInfoAutoSuggest", "hdnCommonId");
    var mode = document.getElementById(hdnType).value;
    var CommonId = document.getElementById(hdnCommonId).value;

    document.getElementById(divMoreInfoAutoSuggest).style.display = "none";
    var value = document.getElementById(id).value;
    value = replaceAll(value, "<", "");
    value = replaceAll(value, ">", "");
    //value = replaceAll(value, ".", "");
    GetAccred(value, mode, CommonId);
}

function GetAccred(myval, mode, CommonId) {
    //alert(mode);
    if (/^13$|^9$|^27$|^38$|^40$/.test(keycode))
        return false;
    else
        $.ajax({
            type: "POST",
            url: "CompanyProfile.aspx/GetAccreditationsAutoSuggest",
            data: "{'prefixText':'" + myval + "', 'mode':'" + mode + "', 'CommonID':'" + CommonId + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                idArr = new Array();
                var json = JSON.stringify(data);
                $('#searchResult').html("");
                var sresult = "<table id='tbl'>";
                var loop = data.d;
                if (loop.rows != null) {

                    $.each(loop.rows, function (i, rows) {
                        if (myval.length != 0) {
                            var startIndex = parseInt(rows.TagName.search(new RegExp(myval, "i")));
                            var endIndex = parseInt(rows.TagName.search(new RegExp(myval, "i"))) + parseInt(myval.length);
                            var existingWord = rows.TagName.substring(startIndex, endIndex);
                            sresult += "<tr id=Accred" + rows.TagId + " title = " + rows.Type + " onclick='javascript:callbackForAccred(this.id,this.title)' onmouseover='javascript:highlightKeyword(this.id)'  ><td>"
                             + (myval === "." ? rows.TagName.replace(myval, '<span class="txthighlight">' + myval + '</span>') + "</td></tr>" : rows.TagName.replace(new RegExp(myval, "gi"), '<span class="txthighlight">' + existingWord + '</span>') + "</td></tr>");
                            //+rows.TagName.replace(new RegExp(myval, "gi"), '<span class="txthighlight">' + existingWord + '</span>') + "</td></tr>";
                        }
                        else
                            sresult += "<tr id=Accred" + rows.TagId + " title = " + rows.Type + " onclick='javascript:callbackForAccred(this.id,this.title)' onmouseover='javascript:highlightKeyword(this.id)'  ><td>"
                        + rows.TagName + "</td></tr>";
                        idArr[i] = "Accred" + rows.TagId;
                    });
                }
                else
                //sresult += "<tr><td></td></tr>";
                    sresult += "<tr title = '' onclick='javascript:AddAccr(this.title)'  style='background-color:#F3F3F3;font-weight:bold;cursor:pointer;'><td style='padding:5px;width:'>Add   " + myval + " as new?</td></tr>";
                sresult += "</table>";
                results = sresult;
                $('#searchResult').append(sresult);
                $('#searchResult').slideDown("slow");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var jsonError = JSON.parse(XMLHttpRequest.responseText);
                alert(JSON.stringify(jsonError));
            }
        });
}


function callbackForAccred(item, SelectedType) {
    if (item != null) {
        var TagId = GetClientId("hdnTagID");
        var hdnSelectedType = TagId.replace("hdnTagID", "hdnSelectedType");
        var txtExpertise = TagId.replace("hdnTagID", "txtExpertise");
        var hdnType = txtExpertise.replace("txtExpertise", "hdnType");
        var mode = document.getElementById(hdnType).value;
        var AccrTagId = item.replace("Accred", "");
        document.getElementById(TagId).value = AccrTagId;
        document.getElementById(hdnSelectedType).value = SelectedType;
        document.getElementById(txtExpertise).value = replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(document.getElementById(item).innerHTML, "<TD>", ""), "</TD>", ""), '<SPAN class=txthighlight>', ''), '</SPAN>', ""), '<SPAN class="txthighlight">', ""), "&nbsp;", " ")
        var str = document.getElementById(txtExpertise).value;
        document.getElementById(txtExpertise).value = str.replace("&amp;", "&");
        $('#searchResult').html("");
        //document.getElementById("ctl00_ContentPlaceHolder1_hdnCompName").value = document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value;
        document.getElementById("searchResult").style.display = "none";
        //alert(mode);
        if (mode != "AllAccreditations")
            AddAccr('FromAutoSuggest');
        else
            AddAccr('');
    }
}
function hideSearchResultAccred()
{ setTimeout('hideSearchAccred()', 350); }

function hideSearchAccred() {
    $('#searchResult').slideUp("slow");
}

function AddAccr(type) {
    var TagId = GetClientId("hdnTagID");
    var hdnType = TagId.replace("hdnTagID", "hdnType");
    var mode = document.getElementById(hdnType).value;
    if (mode != "AllAccreditations") {
        callAddAccrButton(type);
    }
    else {
        if (document.getElementById(TagId).value != "") {
            callAddAccrButton(type);
        }
        else {
            alert("Please select from autosuggest")
        }
    }
}
function callAddAccrButton(type) {
    var txtExpertise = GetClientId("txtExpertise");
    var hdnAccrSubmit = txtExpertise.replace("txtExpertise", "hdnAccrSubmit");
    var divMoreInfoAutoSuggest = txtExpertise.replace("txtExpertise", "divMoreInfoAutoSuggest");
    if (type == 'FromAutoSuggest') {
        txtExpertise = document.getElementById(txtExpertise).value;
        var hdnSelectedType = divMoreInfoAutoSuggest.replace("divMoreInfoAutoSuggest", "hdnSelectedType");
        if (document.getElementById(hdnSelectedType).value != "Others") {
            if (txtExpertise != "") {
                document.getElementById(divMoreInfoAutoSuggest).style.display = "block";
            }
            else {
                document.getElementById(divMoreInfoAutoSuggest).style.display = "none";
            }
        }
        else {
            if (txtExpertise != "") {
                if (document.getElementById(divMoreInfoAutoSuggest).style.display == "none")
                    AddAccrMoreInfo();
            }

        }
    }
    else {
        txtExpertise = document.getElementById(txtExpertise).value;
        if (txtExpertise != "") {
            if (document.getElementById(divMoreInfoAutoSuggest).style.display == "none")
                AddAccrMoreInfo();
        }
    }
}

function AddAccrMoreInfo() {
    var hdnAccrSubmit = GetClientId("hdnAccrSubmit");
    var divMoreInfoAutoSuggest = hdnAccrSubmit.replace("hdnAccrSubmit", "divMoreInfoAutoSuggest");
    document.getElementById(divMoreInfoAutoSuggest).style.display = "none";
    document.getElementById(hdnAccrSubmit).click();
}
function highlightKeyword(id)
{ normalizeKeyword(); document.getElementById(id).className = "highlightKeyword"; }
function normalizeKeyword() {
    var i = 0;
    if (idArr.length != null)
        for (i = 0; i < idArr.length; i++) {
            if (document.getElementById(idArr[i]) != null)
                document.getElementById(idArr[i]).className = "normalizeKeyword";
        }
}

function AddAccrMoreInfo() {
    var hdnAccrSubmit = GetClientId("hdnAccrSubmit");
    var divMoreInfoAutoSuggest = hdnAccrSubmit.replace("hdnAccrSubmit", "divMoreInfoAutoSuggest");
    document.getElementById(divMoreInfoAutoSuggest).style.display = "none";
    document.getElementById(hdnAccrSubmit).click();
}

function GetClientId(strid) {
    var count = document.forms[0].length;
    var i = 0;
    var eleName;
    for (i = 0; i < count; i++) {
        eleName = document.forms[0].elements[i].id;
        pos = eleName.indexOf(strid);
        if (pos >= 0) break;
    }
    return eleName;
}
function replaceAll(txt, replace, with_this)
{ return txt.replace(new RegExp(replace, 'gi'), with_this); }


function ShowTagInfo(tagId, type) {
    // Hide all opened details    
    elements = $('div.divMoreInfoAccr');
    elements.each(function () { $(this).css("display", "none"); });
    var divMoreInfo = tagId.replace("Spaninfo" + type, "divMoreInfo" + type);
    document.getElementById(divMoreInfo).style.display = "block";
}
function HideTagInfo(tagId, type) {
    var divMoreInfo = tagId.replace("Spaninfo" + type, "divMoreInfo" + type);
    document.getElementById(divMoreInfo).style.display = "none";
}
function HideDivMoreInfo() {
    var txtExpertise = GetClientId("txtExpertise");
    var divMoreInfoAutoSuggest = txtExpertise.replace("txtExpertise", "divMoreInfoAutoSuggest");
    document.getElementById(divMoreInfoAutoSuggest).style.display = "none";
}
//****************Accreditations ends here******************//


//****************Skills starts here******************//
function getResultsSkills(id) {
    var txtSkills = GetClientId("txtSkills");
    var value = document.getElementById(id).value;
    var hdnType = txtSkills.replace("txtSkills", "hdnType");
    var hdnCommonId = txtSkills.replace("txtSkills", "hdnCommonId");
    var mode = document.getElementById(hdnType).value;
    var CommonId = document.getElementById(hdnCommonId).value;

    GetSkills(value, mode, CommonId);
}

function GetSkills(myval, mode, CommonId) {
    //alert(CommonId);
    if (/^13$|^9$|^27$|^38$|^40$/.test(keycode))
        return false;
    else
        $.ajax({
            type: "POST",
            url: "CompanyProfile.aspx/GetSkillsAutoSuggest",
            data: "{'prefixText':'" + myval + "', 'mode':'" + mode + "', 'CommonID':'" + CommonId + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                idArr = new Array();
                var json = JSON.stringify(data);
                $('#SkillSearchResult').html("");
                var sresult = "<table id='tbl'>";
                var loop = data.d;
                if (loop.rows != null) {

                    $.each(loop.rows, function (i, rows) {
                        if (myval.length != 0) {
                            var startIndex = parseInt(rows.SkillName.search(new RegExp(myval, "i")));
                            var endIndex = parseInt(rows.SkillName.search(new RegExp(myval, "i"))) + parseInt(myval.length);
                            var existingWord = rows.SkillName.substring(startIndex, endIndex);
                            sresult += "<tr id=Skill" + rows.CombID + " title=" + rows.CategoryType + " onclick='javascript:callbackForSkill(this.id,this.title)' onmouseover='javascript:highlightKeyword(this.id)'  ><td>"
                        + rows.SkillName.replace(new RegExp(myval, "gi"), '<span class="txthighlight">' + existingWord + '</span>') + "</td></tr>";
                        }
                        else
                            sresult += "<tr id=Skill" + rows.CombID + " title=" + rows.CategoryType + " onclick='javascript:callbackForSkill(this.id,this.title)' onmouseover='javascript:highlightKeyword(this.id)'  ><td>"
                        + rows.SkillName + "</td></tr>";
                        idArr[i] = "Skill" + rows.CombID;
                    });
                }
                else
                sresult += "<tr><td></td></tr>";                    
                sresult += "</table>";
                results = sresult;
                $('#SkillSearchResult').append(sresult);
                $('#SkillSearchResult').slideDown("slow");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var jsonError = JSON.parse(XMLHttpRequest.responseText);
                alert(JSON.stringify(jsonError));
            }
        });
}


function callbackForSkill(item, CategoryType) {
    if (item != null) {
        var CombID = GetClientId("hdnCombID");
        var txtSkills = CombID.replace("hdnCombID", "txtSkills");
        var hdnType = txtSkills.replace("txtSkills", "hdnType");
        var mode = document.getElementById(hdnType).value;
        var SkillCombId = item.replace("Skill", "");
        document.getElementById(CombID).value = SkillCombId;
        document.getElementById(txtSkills).value = replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(document.getElementById(item).innerHTML, "<TD>", ""), "</TD>", ""), '<SPAN class=txthighlight>', ''), '</SPAN>', ""), '<SPAN class="txthighlight">', ""), "&nbsp;", " ")
        var str = document.getElementById(txtSkills).value;
        document.getElementById(txtSkills).value = str.replace("&amp;", "&");
        $('#SkillSearchResult').html("");
        document.getElementById("SkillSearchResult").style.display = "none";
        if (document.getElementById(CombID).value != "") {

            if (CategoryType == "SubCategory") {
                var hdnSkillSubmit = CombID.replace("hdnCombID", "hdnSkillSubmit");
                document.getElementById(hdnSkillSubmit).click();
            }
            else {
                var txtSkillsId = CombID.replace("hdnCombID", "txtSkills");
                document.getElementById(txtSkillsId).focus();
                document.getElementById(txtSkillsId).click();               
            }  
        }
        else {
            alert("Please select from autosuggest")
        }
    }
}
function hideSearchResultSkills() {
    //setTimeout('hideSearchSkills()', 350);
    hideSearchSkills();
}

function hideSearchSkills() {
    $('#SkillSearchResult').slideUp("slow");
}
//****************Skills ends here******************//

//****************CommonSkills starts here******************//
function getResultsCommonSkills(id) {
    var txtCommonSkills = GetClientId("txtCommonSkills");
    var value = document.getElementById(id).value;
    var hdnType = txtCommonSkills.replace("txtCommonSkills", "hdnCommonSkillType");
    var hdnCommonIDForCommon = txtCommonSkills.replace("txtCommonSkills", "hdnCommonIDForCommon");
    var mode = document.getElementById(hdnType).value;
    var CommonId = document.getElementById(hdnCommonIDForCommon).value;
    GetCommonSkills(value, mode, CommonId);
}

function GetCommonSkills(myval, mode, CommonId) {
    //alert(mode);
    if (/^13$|^9$|^27$|^38$|^40$/.test(keycode))
        return false;
    else
        $.ajax({
            type: "POST",
            url: "CompanyProfile.aspx/GetSkillsAutoSuggest",
            data: "{'prefixText':'" + myval + "', 'mode':'" + mode + "', 'CommonID':'" + CommonId + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                idArr = new Array();
                var json = JSON.stringify(data);
                $('#CommonSkillSearchResult').html("");
                var sresult = "<table id='tbl'>";
                var loop = data.d;
                if (loop.rows != null) {

                    $.each(loop.rows, function (i, rows) {
                        if (myval.length != 0) {
                            var startIndex = parseInt(rows.SkillName.search(new RegExp(myval, "i")));
                            var endIndex = parseInt(rows.SkillName.search(new RegExp(myval, "i"))) + parseInt(myval.length);
                            var existingWord = rows.SkillName.substring(startIndex, endIndex);
                            sresult += "<tr id=CommonSkill" + rows.CombID + " title=" + rows.CategoryType + " onclick='javascript:callbackForCommonSkill(this.id,this.title)' onmouseover='javascript:highlightKeyword(this.id)'  ><td>"
                        + rows.SkillName.replace(new RegExp(myval, "gi"), '<span class="txthighlight">' + existingWord + '</span>') + "</td></tr>";
                        }
                        else
                            sresult += "<tr id=CommonSkill" + rows.CombID + " title=" + rows.CategoryType + " onclick='javascript:callbackForCommonSkill(this.id,this.title)' onmouseover='javascript:highlightKeyword(this.id)'  ><td>"
                        + rows.SkillName + "</td></tr>";
                        idArr[i] = "CommonSkill" + rows.CombID;
                    });
                }
                else
                    sresult += "<tr><td></td></tr>";
                sresult += "</table>";
                results = sresult;
                $('#CommonSkillSearchResult').append(sresult);
                $('#CommonSkillSearchResult').slideDown("slow");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var jsonError = JSON.parse(XMLHttpRequest.responseText);
                alert(JSON.stringify(jsonError));
            }
        });
}


function callbackForCommonSkill(item, CategoryType) {
    if (item != null) {
        var CombID = GetClientId("hdnCommonSkillCombID");
        var txtSkills = CombID.replace("hdnCommonSkillCombID", "txtCommonSkills");
        var hdnType = txtSkills.replace("txtCommonSkills", "hdnCommonSkillType");
        var mode = document.getElementById(hdnType).value;
        var SkillCombId = item.replace("CommonSkill", "");
        document.getElementById(CombID).value = SkillCombId;
        document.getElementById(txtSkills).value = replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(document.getElementById(item).innerHTML, "<TD>", ""), "</TD>", ""), '<SPAN class=txthighlight>', ''), '</SPAN>', ""), '<SPAN class="txthighlight">', ""), "&nbsp;", " ")
        var str = document.getElementById(txtSkills).value;
        document.getElementById(txtSkills).value = str.replace("&amp;", "&");
        $('#CommonSkillSearchResult').html("");
        document.getElementById("CommonSkillSearchResult").style.display = "none";
        if (document.getElementById(CombID).value != "") {
            if (CategoryType == "SubCategory") {
                var hdnSkillSubmit = CombID.replace("hdnCommonSkillCombID", "hdnCommonSkillSubmit");
                document.getElementById(hdnSkillSubmit).click();
            }
            else {
                var txtSkillsId = CombID.replace("hdnCommonSkillCombID", "txtCommonSkills");
                document.getElementById(txtSkillsId).focus();
                document.getElementById(txtSkillsId).click();
            }             
        }
        else {
            alert("Please select from autosuggest")
        }
    }
}
function hideSearchResultCommonSkills() {
    //setTimeout('hideSearchCommonSkills()', 350);
    hideSearchCommonSkills();
 }

function hideSearchCommonSkills() {
    $('#CommonSkillSearchResult').slideUp("slow");
}
//****************CommonSkills ends here******************//