﻿// JScript File


function show(id)
 {
     var a=document.getElementById("ctl00_linkForCSS");
     var bit =a.disabled;
     
    if (bit==false)
   
    {
        var d = document.getElementById(id);
	    for (var i = 1; i<=10; i++)
	     {
		    if (document.getElementById('smenu'+i))
		    {
		      document.getElementById('smenu'+i).style.display='none';
		    }
	    }
       if (d) 
        {
         d.style.display='block';
         //This style will overlap the DD
         d.style.position='absolute';
         //d.style.z-index=10000;
        }
     }   
}

function Hide(id)
{
    var a=document.getElementById("ctl00_linkForCSS");
     var bit =a.disabled;
     
    if (bit==false)
   
    {
   var d = document.getElementById(id);
   d.style.display='none';
   }
 }
 

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

 

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

 

function MM_findObj(n, d) { //v4.01

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && d.getElementById) x=d.getElementById(n); return x;

}

 

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

  function ShowTab(id,Count)
  {
    //this function will hide all tabs default
    var a=document.getElementById("ctl00_linkForCSS");
     var bit =a.disabled;
     
    if (bit==false)
   
    {
        HideTab(Count);

        document.getElementById(id).style.color="#5C5C5C";
        document.getElementById("div"+id).style.display = "inline";
        document.getElementById("Bullet"+id).style.display = "inline";
        document.getElementById(id).style.backgroundImage = "url(Images/Grey_MdlCurve_Btn.jpg)";
        document.getElementById("Left"+id).src  = "Images/Grey_LeftCurve_Btn.jpg";
        document.getElementById("Right"+id).src  = "Images/Grey_RightCurve_Btn.jpg";
     }
    } 
function HideTab(Count)
{
        //This function will hide all the divs
             var a=document.getElementById("ctl00_linkForCSS");
         var bit =a.disabled;
         
        if (bit==false)
       
        {
            if(Count > 2)
            {
            document.getElementById("divWhatWeDO").style.display = "none";
            document.getElementById("WhatWeDO").style.color="#C3C3C3";
            document.getElementById("BulletWhatWeDO").style.display = "none";
            document.getElementById("WhatWeDO").style.backgroundImage = "url(Images/Grey_MdlCurve_Rlover.jpg)";
            document.getElementById("LeftWhatWeDO").src  = "Images/Grey_LeftCurve_Rlover.jpg";
            document.getElementById("RightWhatWeDO").src  = "Images/Grey_RightCurve_Rlover.jpg"; 
            
             
            document.getElementById("divBenefits").style.display = "none";
            document.getElementById("Benefits").style.color="#C3C3C3";
            document.getElementById("BulletBenefits").style.display = "none";
            document.getElementById("Benefits").style.backgroundImage = "url(Images/Grey_MdlCurve_Rlover.jpg)";
            document.getElementById("LeftBenefits").src  = "Images/Grey_LeftCurve_Rlover.jpg";
            document.getElementById("RightBenefits").src  = "Images/Grey_RightCurve_Rlover.jpg";
            
            document.getElementById("divCaseStudy").style.display = "none";
            document.getElementById("CaseStudy").style.color="#C3C3C3";
            document.getElementById("BulletCaseStudy").style.display = "none";
            document.getElementById("CaseStudy").style.backgroundImage = "url(Images/Grey_MdlCurve_Rlover.jpg)";
            document.getElementById("LeftCaseStudy").src  = "Images/Grey_LeftCurve_Rlover.jpg";
            document.getElementById("RightCaseStudy").src  = "Images/Grey_RightCurve_Rlover.jpg";
            }
            
            if (Count == 4)
            {
            document.getElementById("divSecurityVetting").style.display = "none";      
            document.getElementById("SecurityVetting").style.color="#C3C3C3";
            document.getElementById("BulletSecurityVetting").style.display = "none";
            document.getElementById("SecurityVetting").style.backgroundImage = "url(Images/Grey_MdlCurve_Rlover.jpg)";
            document.getElementById("LeftSecurityVetting").src  = "Images/Grey_LeftCurve_Rlover.jpg";
            document.getElementById("RightSecurityVetting").src  = "Images/Grey_RightCurve_Rlover.jpg";
            }
            
            if (Count == 2)
            {    
            document.getElementById("divPrivacyPolicy").style.display = "none";
            document.getElementById("divQProcedure").style.display = "none";
            
            document.getElementById("PrivacyPolicy").style.color="#C3C3C3";
            document.getElementById("QProcedure").style.color="#C3C3C3";
            
            document.getElementById("PrivacyPolicy").style.backgroundImage = "url(Images/Grey_MdlCurve_Rlover.jpg)";
            document.getElementById("QProcedure").style.backgroundImage = "url(Images/Grey_MdlCurve_Rlover.jpg)";
            document.getElementById("LeftPrivacyPolicy").src  = "Images/Grey_LeftCurve_Rlover.jpg";
            document.getElementById("RightPrivacyPolicy").src  = "Images/Grey_RightCurve_Rlover.jpg";
            document.getElementById("LeftQProcedure").src  = "Images/Grey_LeftCurve_Rlover.jpg";
            document.getElementById("RightQProcedure").src  = "Images/Grey_RightCurve_Rlover.jpg";  
            
            document.getElementById("BulletPrivacyPolicy").style.display = "none";
            document.getElementById("BulletQProcedure").style.display = "none";
           
            }
       }
}
  
  
function ClickEffectRetailer(id)
{
     document.getElementById(id).style.backgroundImage = "url(Images/Main-Image-Retailers_Rlover.jpg)" ;
     //document.getElementById(id).style.backgroundColor = "#CCC" 
}
function FedOutEffectRetailer(id)
{
     document.getElementById(id).style.backgroundImage = "url(Images/MainImgRet.jpg)" ;
} 

function ClickEffectIT(id)
{
     document.getElementById(id).style.backgroundImage = "url(Images/Main-Image-IT-Firms_Rlover.jpg)" ;
     //document.getElementById(id).style.backgroundColor = "#CCC" 
}
function FedOutEffectIT(id)
{
     document.getElementById(id).style.backgroundImage = "url(Images/MainImgITFirms.jpg)" ;
} 

function ClickEffectAppscon(id)
{
     document.getElementById(id).style.backgroundImage = "url(Images/Main-Image-AppCons_Rlover.jpg)" ;
     //document.getElementById(id).style.backgroundColor = "#CCC" 
}
function FedOutEffectAppscon(id)
{
     document.getElementById(id).style.backgroundImage = "url(Images/MainImgConsulting.jpg)" ;
} 


