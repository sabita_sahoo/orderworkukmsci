function HighLightMainMenu(menu) {

    var submenuid;
    menuid = menu.replace('linkh', '');

    var MainMenuId = menuArr[menuid][0][1];
    var MainMenuClass = document.getElementById(MainMenuId).className
//    alert(MainMenuClass);
    if (MainMenuClass.indexOf('Dashboard') != -1) {

        document.getElementById(MainMenuId).className = MainMenuClass.replace('Dashboard', 'HighlightMainMenu');
        document.getElementById(MainMenuId).style.backgroundColor = "#f8f8f8";
    }
    else {

        document.getElementById(MainMenuId).className = MainMenuClass.replace('HighlightMainMenu', 'Dashboard');
        document.getElementById(MainMenuId).style.backgroundColor = "";
    }
}

function HideAllSubMenu() {

    try {
        for (k = 1; k < menuArr.length; k++) {
            if (menuArr[k].length != 0) {
                for (l = 1; l < menuArr[k].length; l++) {
                    try {
                        document.getElementById("submenu" + k + l).style.visibility = "hidden";
                        document.getElementById("imgbullet" + k + l).style.visibility = "hidden";
                        //Now reset the flash sequence at home page also.
                        resetFlash();
                    }
                    catch (error) { }
                }
            }
        }
    }
    catch (error) { }
}

try {
    var divwidth = 1050;
    var divleft = 0;
    for (i = 1; i < menuArr.length; i++) {
        str = '';
        if (menuArr[i].length != 0) {
            divleft = document.getElementById(menuArr[i][0][1]).offsetLeft;

            str = str + '<div  id="div' + i + '"   style="width:400px;position:absolute;z-index:99990; top: 116px;visibility:hidden;" onmouseover="dontHideSlowly();" onmouseout="HideAllSubMenu();">';
//            if (document.getElementById("ctl00_hdnClientMenu").value == 1) {
//                str = str + '<div style="width:200px;" class="divDDMenuInnerOrange">'
//            }
//            else {
                str = str + '<div style="width:200px; " class="divDDMenuInner">'
//            }

            for (j = 1; j < menuArr[i].length; j++) {
                if (j == 1) {
                    
//                    if (document.getElementById("ctl00_hdnClientMenu").value == 1) {
//                        str = str + '<div style="height:4px;background-color:#f8f8f8; overflow:visible;" class="divDDMenuInnerOrangeDummy"></div>'
//                    }
//                    else {
                        str = str + '<div style="height:4px;background-color:#f8f8f8; overflow:visible;" class="divDDMenuInnerDummy"></div>'
//                    }

                }

                var MasterPageLink;
                MasterPageLink = document.getElementById("ctl00_hdnSubMenuLink");

                if (menuArr[i][j][1] == "Book Work Order" || menuArr[i][j][1] == "Create Work Request") {
                    var strURLBookingForm;
                    strURLBookingForm = MasterPageLink.href + '/BookingForms/' + menuArr[i][j][2];
                    str = str + '<a onMouseOver="javascript:HighLightMainMenu(this.id);" onMouseOut="javascript:HighLightMainMenu(this.id);" href="' + strURLBookingForm + '"  class="DDMenuLink" id="linkh' + i + '">' + menuArr[i][j][1] + '</a>';
                }
                else if (menuArr[i][j][1] == "Book Workorder Beta") {
                    var strURLBookingForm;
                    strURLBookingForm = MasterPageLink.href + '/BookingForms/BookWorkorder.aspx'
                    str = str + '<a onMouseOver="javascript:HighLightMainMenu(this.id);" onMouseOut="javascript:HighLightMainMenu(this.id);" href="' + strURLBookingForm + '" target="_blank" class="DDMenuLink" id="linkh' + i + '">' + menuArr[i][j][1] + '</a>';
                }
                else if (menuArr[i][j][1] == "DashBoard") {
                    var strURLDashBoard;
                    strURLDashBoard = MasterPageLink.href + '/' + menuArr[i][j][2];
                    str = str + '<a onMouseOver="javascript:HighLightMainMenu(this.id);" onMouseOut="javascript:HighLightMainMenu(this.id);" href="' + strURLDashBoard + '"  class="DDMenuLink" id="linkh' + i + '">' + menuArr[i][j][1] + '</a>';
                }
                else {
                    var strURL;
                    strURL = MasterPageLink.href + '/' + menuArr[i][j][2];
                    str = str + '<a onMouseOver="javascript:HighLightMainMenu(this.id);" onMouseOut="javascript:HighLightMainMenu(this.id);" href="' + strURL + '"  class="DDMenuLink" id="linkh' + i + '">' + menuArr[i][j][1] + '</a>';
               }
            }

            str = str + '</div>';
            str = str + '</div>';

            document.write(str);

            var el = document.getElementById('div' + i);
            if (typeof el.contains == 'undefined') {
                el.contains = function (node) {
                    if (null == node)
                        return false;
                    if (this == node)
                        return true;
                    else return this.contains(node.parentNode);
                }
            }
            el.onmouseout = handle_mouseout;
        }
    }
    document.write('<iframe id="posFrame" allowtransparency="true"  src="blank.htm" onmouseout="hide(current)" onmouseover="javascript:false;" scrolling="no" frameborder="0" style="height:175px;position:absolute;z-index:0; top:0px; left:0px;visibility:hidden;"></iframe>');

}
catch (error) { alert(error); }

function handle_mouseout(e) {
    try {
        e = (e) ? e : window.event;
        if (!this.contains(e.relatedTarget || e.toElement))
            hideSlowly();
    }
    catch (error) { }
}

function show(subMenu, mainMenu, height) {

    try {
        clearTimeout(scrollTim);
    }
    catch (error) { }

    try {

        current = subMenu

        var p = fGetXY(mainMenu);

        var posFr = document.getElementById("posFrame");


        posFr.style.width = document.getElementById(subMenu).style.width;

        document.getElementById(subMenu).style.left = findPosX(mainMenu) + 'px';

        document.getElementById(subMenu).style.visibility = "visible";
        posFr.style.zIndex = document.getElementById(subMenu).style.zIndex - 1;

        posFr.style.left = document.getElementById(subMenu).style.left;
        posFr.style.top = document.getElementById(subMenu).style.top;

        posFr.style.visibility = "visible";
        posFr.style.height = height;
    }
    catch (error) { }
}
function hide(subMenu) {
    try {
        document.getElementById("posFrame").style.visibility = "hidden";
        document.getElementById(subMenu).style.visibility = "hidden";
    }
    catch (error) { }
}
function hideSlowly() {
    try {
        scrollTim = setTimeout("hideall(); HideAllSubMenu();", 1000);
    }
    catch (error) { }
}
function dontHideSlowly() {
    try {
        clearTimeout(scrollTim);
    }
    catch (error) { }
}

function hideall() {
    try {
        document.getElementById("posFrame").style.visibility = "hidden";
        for (i = 1; i <= (6 + 1); i++) {
            try {
                document.getElementById("div" + i).style.visibility = "hidden";
                //Now reset the flash sequence at home page also.
                resetFlash();
            }
            catch (error) { }
        }
    }
    catch (error) { }
}
function fGetXY(aTag) {
    try {
        var p = [0, 0];
        while (aTag != null) {
            p[0] += aTag.offsetLeft;

            p[1] += aTag.offsetTop;
            aTag = aTag.offsetParent;
        }
        return p;
    }
    catch (error) { }
}

function findPosX(obj) {
    var curleft = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft;
            obj = obj.offsetParent;
        }
    }
    else if (obj.x)
        curleft += obj.x;
    return curleft;
}

