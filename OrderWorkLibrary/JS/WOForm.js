﻿var xmlDoc;
function loadXMLDoc(dname) 
{
if (window.XMLHttpRequest)
  {
  xmlDoc=new window.XMLHttpRequest();
  xmlDoc.open("GET",dname,false);
  xmlDoc.send("");
  return xmlDoc.responseXML;
  }

else if (ActiveXObject("Microsoft.XMLDOM"))
  {
  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
  xmlDoc.async=false;
  xmlDoc.load(dname);
  return xmlDoc;
  }
alert("Error loading document");
return null;
}

function onLocationChange(ddlLocName)
{  
 
    if (document.getElementById(ddlLocName).value != "")
    {
        populateLocDetails(document.getElementById(ddlLocName).value, ddlLocName, locArr);
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnLocationId").value = document.getElementById(ddlLocName).value;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_btnFind").disabled = true; 
     
    }
    else
    {
        clearForTempLoc();
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_btnFind").disabled = false;
    }
}

function populateLocDetails(curr, ddlLocName, arrayStore)
{  
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tblContactInfo").style.display = "none";  
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lbltxtCompLocName").innerHTML = "Location Name";    
    
        document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdFName').enabled = false;
        document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdLName').enabled = false;  
        
        
    for(var i= 1 ; i<=arrayStore.length- 1 ; i++)
    {
        if(arrayStore[i][0]== curr)
        {
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tdCompName").style.display = "block";
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tdtxtCompName").style.display = "block";
            
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtName").value = arrayStore[i][1];                 
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyAddress").value = arrayStore[i][2];
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCity").value = arrayStore[i][3];
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCounty").value = arrayStore[i][4];
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPostalCode").value = arrayStore[i][5];
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPhone").value = arrayStore[i][6];
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyFax").value = arrayStore[i][7];
            
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtName").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyAddress").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCity").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCounty").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPostalCode").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPhone").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyFax").disabled = true;
        }
    }
}

function clearForTempLoc() 
{               
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
        {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tdCompName").style.display = "none";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tdtxtCompName").style.display = "none";}
            
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lbltxtCompLocName").innerHTML = "Company Name";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tblContactInfo").style.display = "block";
        
        document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdFName').enabled = true;
        document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdLName').enabled = true;  
        
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtName").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyAddress").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCity").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCounty").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPostalCode").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPhone").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyFax").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtFName").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtLName").value = "";        
        
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtName").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyAddress").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCity").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCounty").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPostalCode").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPhone").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyFax").disabled = false;
}

function onMainCatChange(ddlMainCat)
{  
    var hdnsubcatval = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategoryVal");
    var hdnsubcattxt = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategorytxt");
    var subcat = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType");
    hdnsubcatval.value = "";
    hdnsubcattxt.value = ""; 
    if (document.getElementById(ddlMainCat).value != "")
    {
        subcat.disabled = false;
        clearOptions(subcat);

        populateSubCat(document.getElementById(ddlMainCat).value);
        hdnsubcattxt.value = subcat.options[subcat.selectedIndex].text;        
        hdnsubcatval.value = subcat.value;  
    }
    else
    {
        clearOptions(subcat);
        subcat.disabled = true;
    }
}


function populateSubCat(ddlMainCatValue)
{
    var browserName=navigator.appName;
    xmlDoc=loadXMLDoc("OWLibrary/XML/WOFormStandards.xml");  
    if (browserName=="Netscape")
    {
        for (i = 1; i < xmlDoc.childNodes[0].childNodes.length; i++)    
        { 
            
            if (xmlDoc.childNodes[0].childNodes[i].tagName == 'MainCat')            
            {
               
                 var selectedCombID=xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue;
                 
                 if(xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue==ddlMainCatValue)
                 { 
                     for (j = 1; i < xmlDoc.childNodes[0].childNodes.length; j++)    
                     { 
                        if (xmlDoc.childNodes[0].childNodes[j].tagName == 'SubCat')            
                        {
                            
                          if(xmlDoc.childNodes[0].childNodes[j].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue==selectedCombID)
                            {
                                addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType"),xmlDoc.childNodes[0].childNodes[j].getElementsByTagName("Name")[0].childNodes[0].nodeValue,xmlDoc.childNodes[0].childNodes[j].getElementsByTagName("CombID")[0].childNodes[0].nodeValue);                
                            }
                        }
                      }
                  }
            }
        }
    }
    else if (browserName=="Microsoft Internet Explorer")
    {
        for (i = 1; i < xmlDoc.childNodes[1].childNodes.length; i++)    
        { 
            
            if (xmlDoc.childNodes[1].childNodes[i].tagName == 'MainCat')            
            {
                var selectedCombID=xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue;
                 if(xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue==ddlMainCatValue)
                 { 
                     for (j = 1; i < xmlDoc.childNodes[1].childNodes.length; j++)    
                     { 
                        if (xmlDoc.childNodes[1].childNodes[j].tagName == 'SubCat')            
                        {
                            
                          if(xmlDoc.childNodes[1].childNodes[j].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue==selectedCombID)
                            {
                                addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType"),xmlDoc.childNodes[1].childNodes[j].getElementsByTagName("Name")[0].childNodes[0].nodeValue,xmlDoc.childNodes[1].childNodes[j].getElementsByTagName("CombID")[0].childNodes[0].nodeValue);                
                            }
                        }
                      }
                  }
             }
        }
    }   
}




function onSubCatChange(ddlSubCat)
{      
    var subcat = document.getElementById(ddlSubCat).value;
    var subcattxt = document.getElementById(ddlSubCat).options[document.getElementById(ddlSubCat).selectedIndex].text;
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = subcat;
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = subcattxt;
}

function addOption(selectbox,text,value )
{ 
    var optn = document.createElement("OPTION");
    optn.text = text;
    optn.value = value;
    selectbox.options.add(optn); 
}
function removeOption(selectbox,text)
{
   
  var i;
  for (i = selectbox.length - 1; i>=0; i--) 
  {
    if (selectbox.options[i].value==text) 
    {
      selectbox.remove(i);
    }
  }
}
function clearOptions(selectbox)
{ 
    while(selectbox.options.length>0)
        selectbox.remove(0)
}
function ZeroValueWO(status)
{   
    if(status == "true") {    
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value = "0";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").disabled = true;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_CustProposedPriceValidator").enabled = false;        
    }
    if(status == "false") {    
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_CustProposedPriceValidator").enabled = true;
    }
}
function getdate()
{   
    var startdate = new Date(getDateObject(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value,"/"));
    return startdate;
}
function getbegindate()
{   
    var startdate = new Date(getDateObject(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value,"/"));
    return startdate;
}
function getWOProcessDate()
{
    var startdate = new Date(getDateObject(document.getElementById("ctl00_ContentHolder_UCWOProcess1_txtDateStart").value,"/"));
    return startdate;
}
function getDateObject(dateString,dateSeperator)
{

var curValue=dateString;
var sepChar=dateSeperator;
var curPos=0;
var cDate,cMonth,cYear;

curPos=dateString.indexOf(sepChar);
cDate=dateString.substring(0,curPos);

endPos=dateString.indexOf(sepChar,curPos+1); 
cMonth=dateString.substring(curPos+1,endPos);

curPos=endPos;
endPos=curPos+5;
cYear=dateString.substring(curPos+1,endPos);

var changeddate = cMonth+"/"+cDate+"/"+cYear;
return changeddate;
}

function getProductDescription(myval)
{       
   
    if (myval == "0")
    {
  
      removeProductInfo("Service"); 	       
	}
	else if(myval == "1")
    {
   
        removeProductInfo("Quote");
    }
	else
	{
	    
	    $.ajax({
        type:"POST",
        url:"WOForm.aspx/GetProductDetails",
        data: "{'productid':'"+myval+"'}",
        contentType: "application/json; charset=utf-8",    
        dataType:"json",        
        success:function(data)    
        {               
            
            var json = JSON.stringify(data);             
            $.each(data.rows, function(i,rows){    
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnProductId").value = parseInt(rows.WOID);                     
            
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnbtnTrigger").click(); 
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").value = rows.WOTitle;
            
           document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlScopeWork").style.display = "block";
           document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlClientScope").style.display = "none";
           document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = rows.WOLongDesc.replace(/<BR>/g,"");
           ValidatorEnable(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rqdScopeOfWork"),false);
            if (rows.ClientScope != "")
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlClientScope").style.display = "block";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlScopeWork").style.display = "none";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value = rows.ClientScope.replace(/<BR>/g,"");
                ValidatorEnable(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rqdScopeOfWork"),true);}
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value = rows.PONumber;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value = rows.JRSNumber;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpecialInstructions").value = rows.SpecialInstructions.replace(/<BR>/g,"");                    
            if (parseInt(rows.LocationID) > 0)
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnBillingLocation").value = parseInt(rows.LocationID);
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnBillingLocation").disabled = true;}
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value = rows.WholesalePrice;                     
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").value = parseInt(rows.CombId); 
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").disabled = false;
            clearOptions(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType"));
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = "";
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = ""; 
            
            populateSubCat(parseInt(rows.CombId));          
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").value = parseInt(rows.WOCategoryID);       
            onSubCatChange("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType") ;                                  
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value = rows.DeferredDate;  
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = rows.DeferredDate;                                              
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnPlatformPrice").value = rows.PlatformPrice;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlProposedPrice").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdoNon0Value").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdoNon0Value").checked = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").checked = false;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").disabled = true;
            document.getElementById("tdRadioButtonPrice").style.display="block"
            document.getElementById("tdTextBoxPrice").style.display="block"
            if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object]") 
             {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = true;}
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").disabled = true;
            if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object]") 
             {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").disabled = true;}
            if (rows.FreesatService == true && document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
            {
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display = "block";          
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "block";
                populateFreesatMake();
            }
            else
            {
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display = "none";          
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "none";
            }
            
            if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
                {   if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnEvengInst").value == "True" && document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").options.length == 4)
                    removeOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime"),"All Day");
                    addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime"),"6pm - 8pm","6pm - 8pm");
                    addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime"),"All Day","All Day");
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value ='';
                }
           });                 
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) 
	    {            
		    var jsonError = JSON.parse(XMLHttpRequest.responseText);            
		    alert(JSON.stringify(jsonError));
	    }});
	}
}
function removeProductInfo(id)
{
    if (id=="Service")
    {
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlProposedPrice").disabled = false;
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnProductId").value = "";
	    
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").value = "";
	    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object]") 
             {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = "";
             document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = false;}            
       
         {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value = "";
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").disabled = false;}
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlClientScope").style.display = "block";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlScopeWork").style.display = "none";
        ValidatorEnable(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rqdScopeOfWork"),true);
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpecialInstructions").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientInstru").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value = 0;
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = ""; 
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = getTodayDate();
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnBillingLocation").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display = "none";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "none";        
        var StartDate = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value;
        var SecStartDate = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value;
        
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
        {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value = ""
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value == "6pm - 8pm" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value == "1pm - 6pm")
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value = "8am - 1pm";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").disabled = false;    
                
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = StartDate;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value = SecStartDate;                            
                }
        else
            {
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = SecStartDate;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value = StartDate;}
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnEvengInst").value == "True")
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").remove(3);}
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlProposedPrice").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdoNon0Value").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").disabled = false;
         document.getElementById("tdRadioButtonPrice").style.display="table-cell"
         document.getElementById("tdTextBoxPrice").style.display="table-cell"
     }
     else if(id=="Quote")
     {
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlProposedPrice").disabled = false;
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnProductId").value = "";
	    
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").value = "";
	    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object]") 
             {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = "";
             document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = false;}            
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object]") 
         {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value = "";
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").disabled = false;}
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlClientScope").style.display = "block";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlScopeWork").style.display = "none";
        ValidatorEnable(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rqdScopeOfWork"),true);
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpecialInstructions").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value = 0;
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = ""; 
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = getTodayDate();
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnBillingLocation").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display = "none";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "none";        
        var StartDate = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value;
        var SecStartDate = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value;
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value == "6pm - 8pm" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value == "1pm - 6pm")
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value = "8am - 1pm";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").disabled = false;    
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = StartDate;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value = SecStartDate;                            
                }
        else
            {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value = SecStartDate;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = SecStartDate;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value = StartDate;}
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnEvengInst").value == "True")
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").remove(3);}
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlProposedPrice").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").disabled = true;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").checked = true;
        document.getElementById("tdRadioButtonPrice").style.display="none"
        document.getElementById("tdTextBoxPrice").style.display="none"
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdoNon0Value").disabled = false;
        
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").disabled = false;
     }
         
}
function populateWOFOrm()
{
   
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlWoForm").style.display = "block";
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlWoSummary").style.display = "none";
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divValidationMain").style.display = "none";  
}

function populateConfirmationSummary()
 {       
        var flag;
        var errorstring;
        flag = true;
        errorstring = "";
        if (Page_ClientValidate() == false)
        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlLocation").value != "")
                {document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdFName').enabled = false;
                 document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdLName').enabled = false;}
            else
                {document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdFName').enabled = true;
                 document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdLName').enabled = true;}   
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divValidationMain").style.display = "block";}
        else
        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object]") 
            {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection").selectedIndex].text == "None Specified")
                {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                    {flag = false;
                     errorstring = "Please enter the location where the specified goods will be located";}}}
            if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur") == "[object]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur") == "[object HTMLInputElement]") 
            {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur").value.length > 2000 )
                {flag = false;
                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                    {errorstring = errorstring + "<br>Maximum 2000 characters are allowed in Product Purchased";}
                    else
                    {errorstring = errorstring + "<br>Maximum 2000 characters are allowed in Equipment Details";}}}
            
                
           
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object]")                    
                {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display == "block" )
                    {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").selectedIndex].text == "None Specified")
                        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                            {flag = false;
                             errorstring = "Please select the Freesat Make/Model";}}}}
                
            if (flag == false)
            {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divValidationMain").style.display = "block";
             document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblError").innerHTML = errorstring;}
            else
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divValidationMain").style.display = "none"; 
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlWoForm").style.display = "none";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlWoSummary").style.display = "block";
                var category = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").selectedIndex].text;
                category = category + " - " + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").selectedIndex].text;            
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblCategory").innerHTML = category;  
            
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value != "") 
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value)//.replace(/\\n/g,"<br>");
                else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value)//.replace(/\\n/g,"<br>");
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblSpecialInstructions").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientInstru").value);//.replace(/\\n/g,"<br>");}
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblDateCreated").innerHTML = getTodayDate();
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLocName").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCity").value + ", " + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPostalCode").value;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblContact").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnContactName").value;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblWOTitle").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").value;               
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPrice").innerHTML = "&" + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnCurrency").value + " " + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value;                       
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblStartDate").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnClientStrtDate").value;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblDateStart").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value;
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value != "Retail")
                {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInEnd").value == "")
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblEndDate").innerHTML = "None Specified";            
                    else
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblEndDate").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInEnd").value;
                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "IT")
                        {document.getElementById("divConfProductsPur").style.display = "block";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblProdPur").innerHTML = "Equipment Details: ";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfProdPur").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur").value);
                        document.getElementById("divConfRetail").style.display = "none";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfGoods").innerHTML = "Location of Equipment";}
                    else
                        {document.getElementById("divConfProductsPur").style.display = "none";
                        document.getElementById("divConfRetail").style.display = "none";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divSummGoodsLocation").style.display = "none";}}
                else
                    {
                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
                    {
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblStartDateLabel").innerHTML = "Appointment";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtlblEndDate").innerHTML = "Installation Time";                    
                    }                    
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblEndDate").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").selectedIndex].text;
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnInstTime").value = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").selectedIndex].text;
                    document.getElementById("divConfProductsPur").style.display = "block";
                    document.getElementById("divConfRetail").style.display = "block";
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfProdPur").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur").value);
                    
                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSalesAgent").value == "")
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfSalesAgent").innerHTML = "None Specified";
                    else
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfSalesAgent").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSalesAgent").value);                    
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnFreesat").value = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").value;
                        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object]")
                        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display == "block" )
                            {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfFreesatMake").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").selectedIndex].text;}
                         else document.getElementById("divConfSFreesat").style.display = "none";
                        }}
                
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblSupplyParts").innerHTML = "No";
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value == "")
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPONumber").innerHTML = "None Specified";
                else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPONumber").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value;
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value == "")
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblJRSNumber").innerHTML = "None Specified";
                else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblJRSNumber").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value;          
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblDressCode").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlDressCode").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlDressCode").selectedIndex].text;            
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object]") 
                    {var GoodsLocation = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection").selectedIndex].text;
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblBusinessDivision").innerHTML = GoodsLocation;}
                else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblBusinessDivision").innerHTML = "None Specified";                
            }
        }
}
function convertTextToHTML(strMultiLineText)
{
var strSingleLineText = strMultiLineText.replace(
new RegExp( "\\n", "g" ), 
" <br> " 
);
return strSingleLineText;
}
function getTodayDate()
{
    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    return (day + "/" + month + "/" + year);
}
function populateFreesatMake()
{       
    var browserName=navigator.appName;
    xmlDoc=loadXMLDoc("OWLibrary/XML/WOFormStandards.xml");  
    if (browserName=="Netscape")
    {
        for (i = 1; i < xmlDoc.childNodes[0].childNodes.length; i++)    
        { 
            if (xmlDoc.childNodes[0].childNodes[i].tagName == 'FreesatMake')            
            {
                 addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake"),xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("StandardValue")[0].childNodes[0].nodeValue,xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("StandardID")[0].childNodes[0].nodeValue);
            }
        }
    }
    else if (browserName=="Microsoft Internet Explorer")
    {
        for (i = 1; i < xmlDoc.childNodes[1].childNodes.length; i++)    
        { 
            if (xmlDoc.childNodes[1].childNodes[i].tagName == 'FreesatMake')            
            {
                 addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake"),xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("StandardValue")[0].childNodes[0].nodeValue,xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("StandardID")[0].childNodes[0].nodeValue);
            }
        }
    }   
}

function ondrpTimeChange(objID)
{
    var WP = objID.replace("drpTime","txtSpendLimit");
    var WPUplift = objID.replace("drpTime","hdnWPUplift");
    var InstTime = objID.replace("drpTime","hdnInstTime");
    var startdateID = objID.replace("drpTime","hdnStartDate");
    var secondstartdateID = objID.replace("drpTime","hdnSecStartDate");
    var calendarID = objID.replace("drpTime","calBeginDate");
    var ApptDate = objID.replace("drpTime","txtScheduleWInBegin");
    if (document.getElementById(objID).value == "6pm - 8pm" && document.getElementById(InstTime) != "6pm - 8pm")
       {document.getElementById(WP).value = parseFloat(document.getElementById(WP).value) + parseFloat(document.getElementById(WPUplift).value);}
    if (document.getElementById(objID).value != "6pm - 8pm" && document.getElementById(InstTime).value == "6pm - 8pm")
       {document.getElementById(WP).value = parseFloat(document.getElementById(WP).value) - parseFloat(document.getElementById(WPUplift).value);}
    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnProductId").value == "" && document.getElementById(objID).value != "8am - 1pm")
        {var startDate = document.getElementById(startdateID).value;
        var secondDate = document.getElementById(secondstartdateID).value;
        document.getElementById(startdateID).value = secondDate;
        document.getElementById(ApptDate).disabled = false;
        document.getElementById(ApptDate).value = secondDate;        
        document.getElementById(secondstartdateID).value = startDate;
        //alert(document.getElementById(calendarID).value);
        document.getElementById(ApptDate).disabled = true;}
    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnProductId").value == "" && document.getElementById(objID).value == "8am - 1pm")
        {var startDate = document.getElementById(startdateID).value;
        var secondDate = document.getElementById(secondstartdateID).value;
        document.getElementById(startdateID).value = secondDate;
        document.getElementById(ApptDate).value = secondDate;
        document.getElementById(secondstartdateID).value = startDate;}    
    document.getElementById(InstTime).value = document.getElementById(objID).value;    
}
function holidaydates()
{return HolidayArr;
}
function holidaydatesretail()
{return HolidayArr;
}
function GetStartDate(sender,args)
{
var stDate=new Date();
stDate=sender._selectedDate;
var cMonth=stDate.getMonth()+1;
if (cMonth<10)
cMonth='0'+cMonth;
var clienSelectedDate=stDate.getDate() + '/'+ cMonth +'/' + stDate.getFullYear();
document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnClientStrtDate").value=clienSelectedDate;
}