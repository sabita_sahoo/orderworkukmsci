var xmlDoc;
var OldWP = 0.0;
var OldPP = 0.0;

function loadXMLDoc(dname) 
{
if (window.XMLHttpRequest)
  {
  xmlDoc=new window.XMLHttpRequest();
  xmlDoc.open("GET",dname,false);
  xmlDoc.send("");
  return xmlDoc.responseXML;
  }
// IE 5 and IE 6
else if (ActiveXObject("Microsoft.XMLDOM"))
  {
  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
  xmlDoc.async=false;
  xmlDoc.load(dname);
  return xmlDoc;
  }
alert("Error loading document");
return null;
}
// REquired for image roll over on pageload
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

//Script required for the welcome page
function showPasswordFld(defaultText)
{	
    if(document.getElementById("UCLogin1_txtLoginPassword"))
    if(document.getElementById("UCLogin1_txtLoginPassword").value == "")
	{
		document.getElementById("txtLoginPasswordText").className = "passwordFieldHidden";
		document.getElementById("txtLoginPasswordText").value = defaultText; 
		document.getElementById("UCLogin1_txtLoginPassword").className = "passwordField";		
		document.getElementById("UCLogin1_txtLoginPassword").focus();
	}
}
function hidePasswordFld(defaultText)
{ 
	if(document.getElementById("UCLogin1_txtLoginPassword"))
	if(document.getElementById("UCLogin1_txtLoginPassword").value == "")
	{
		document.getElementById("txtLoginPasswordText").className = "passwordField";
		document.getElementById("txtLoginPasswordText").value = defaultText;
		document.getElementById("UCLogin1_txtLoginPassword").className = "passwordFieldHidden";		
	}
}
function showHideAnswerHIW(objID)
{
	if(document.getElementById("A"+objID).className=="faqAnsHIW")
	{		
		document.getElementById("A"+objID).className="faqAnsShowHIW";
		document.getElementById("Q"+objID).className="QTabSelected";
	}
	else
	{
		document.getElementById("A"+objID).className="faqAnsHIW";
		document.getElementById("Q"+objID).className="QTab";
	}
	for(i=1; i<=4; i++)
	{
	    if(i != objID)
	    {
	        document.getElementById("A" + i).className="faqAnsHIW";
	        document.getElementById("Q"+i).className="QTab";
	    }
	}
}
function showDiv(iframe)
{   
	 document.getElementById(iframe).style.visibility = "visible";   
     document.getElementById(iframe).style.position = "absolute";
	 document.getElementById(iframe).style.height = "225px";
	 document.getElementById(iframe).style.width = "400px";
	 document.getElementById(iframe).style.zIndex = "9999"; 
}
	
function hideDiv(iframe)
{		
	document.getElementById(iframe).style.visibility = "hidden";
	document.getElementById(iframe).style.position = "absolute";
	document.getElementById(iframe).style.overflow = "hidden";
	document.getElementById(iframe).style.width = "0px";				
	document.getElementById(iframe).style.height = "0px";
}

//Script for ratings
function showHideRatings(idDiv)
{ 
 if(document.getElementById(idDiv))
	 { 
	        //hide Div
			if(document.getElementById(idDiv).style.visibility == "visible")
			 { 
				document.getElementById('divMainRating').className = 'backGroundRating formTxtRedBold2'
			 
				document.getElementById(idDiv).style.visibility = "hidden";
				document.getElementById(idDiv).style.position = "absolute";
				document.getElementById(idDiv).style.overflow = "hidden";
				document.getElementById(idDiv).style.width = "0px";
				document.getElementById(idDiv).style.height = "0px";
			 }
			 else
			{  
			//show Div
				document.getElementById('divMainRating').className = 'backGroundRatingOnclick formTxtRedBold2'
				
				document.getElementById(idDiv).style.visibility = "visible";
				document.getElementById(idDiv).style.position = "relative";
				document.getElementById(idDiv).style.height = "100%";
				document.getElementById(idDiv).style.width = "100%";
				document.getElementById(idDiv).style.overflow = "visible"; 
			}
	}
}

function getTrimmedLabelID(objID)
{
	var strID = new String(objID);
	var strLabelId = new String(strID.substr(0,strID.lastIndexOf("_") + 1));
	return strLabelId;
}

function proposedPrice(obj)
{ 
    var ucName = getTrimmedLabelID(obj);
	if(document.getElementById(ucName + "rdoNon0Value").checked == true)
	{
		document.getElementById(ucName + "txtSpendLimit").disabled = false
		if (document.getElementById(ucName + "tdReview") != null)
    	{
			document.getElementById(ucName + "tdReview").style.visibility = 'visible'
			document.getElementById(ucName + "tdChkReview").style.visibility = 'visible'							
		}
	}
	else
	{
		document.getElementById(ucName + "txtSpendLimit").value=0
		document.getElementById(ucName + "txtSpendLimit").disabled = true
		if (document.getElementById(ucName + "tdReview") != null)
		{
			document.getElementById(ucName + "tdReview").style.visibility = 'hidden'
			document.getElementById(ucName + "tdChkReview").style.visibility = 'hidden'		
			document.getElementById(ucName + "chkReviewBid").checked = false			
		}
	}
}

function clearExpiryDate(obj)
{
		if(obj.value == "Enter expiry date")	
			obj.value=""
}
function fillExpiryDate(obj)
{ 
		if(obj.value=="")		
		obj.value="Enter expiry date"			
}
function clearDateOfIssue(obj)
{
		if(obj.value == "Date of issue")	
			obj.value=""
	
}
function clearDateOfIssue1(obj)
{
    if(obj.value == "Enter expiry date")	
        obj.value=""
	
}
function fillDateOfIssue1(obj)
{ 
    if(obj.value=="")		
        obj.value="Enter expiry date"	
			
}
function fillDateOfIssue(obj)
{ 
		if(obj.value=="")		
		obj.value="Date of issue"	
			
}
function populateAttachedFiles(objId)
{ 
    document.getElementById(objId + "_btnJavaClickToPopulateAttachFile").click();
}

function CollapseComments(WOTrackingID, objID)
{ 
	var strTrimValue = getTrimmedLabelID(objID);
	var strModifiedValue = strTrimValue + WOTrackingID;
	if (document.getElementById(strModifiedValue ).style.visibility == "hidden")
	{ 	document.getElementById(strModifiedValue ).style.visibility = "visible";
		
	}	
	else if(document.getElementById(strModifiedValue).style.visibility == "visible")
	{ document.getElementById(strModifiedValue).style.visibility = "hidden";
	} 		
}

function getTrimmedLabelID(objID)
{
	var strID = new String(objID);
	var strLabelId = new String(strID.substr(0,strID.lastIndexOf("_")));
	strLabelId  = strLabelId.concat("_divCommentsDetail"); 
	return strLabelId;
}

//Function used in all the listing pages
// checkBoxId: check box id used in grid view for check all
// selectId: id of hidden variabl
// struc - is the gird view is inside a UC
//page - pagename for the print link
function CheckAllPrint(me,checkBoxId,selectId,strUC,page)
{ 
    var index = me.name.indexOf('_');  
    var prefix = me.name.substr(0,index); 
    // Looks for the right checkbox
    for(i=0; i<document.forms[0].length; i++) 
    { 
        var o = document.forms[0][i];   
        var str =new String();
        str=o.id;       
        if (o.type == 'checkbox' && (me.name != o.name) && (o.name.substring(0, prefix.length) == prefix) ) 
        {       
            if (str.indexOf(checkBoxId)!=-1) 
            {    
                o.checked = me.checked;  
                GetSelectedIds(o,selectId,strUC,page);                                        
            }
        } 
    }   
}
//Function to get the selected ids in the grid view
// checkBoxId: check box id used in grid view for check all
// selectId: id os hidden variable
// struc - is the gird view is inside a UC
//page - pagename for the print link
function GetSelectedIds(checkBoxId,selectId,strUC,page){   
    
    if (!strUC)
        strUC = '';  
        
    var ancHr = 'btnPrintInvoice';
    try {
        var ancExp = 'btnExport';
    }
    catch(err) {    
    }     
    var checkId = checkBoxId.id;              
    var index = checkId.lastIndexOf('_');  
    var prefix = checkId.substring(0,index); 
    var objId = prefix + '_' + selectId;


     
    var UCindex;
    var UCprefix = '';
     
    if (strUC != '')
    {
      UCindex = prefix.lastIndexOf(strUC);
      UCprefix = prefix.substring(0,UCindex) +  strUC + '_';
      ancHr = UCprefix + ancHr;
      //For Export To Excel Link
    try {
        ancExp = UCprefix + ancExp;
    }
    catch(err) {}
    
    }
    
        
    var strIDs = new String();
    if (checkBoxId.checked)
    {
    
        if (document.getElementById(UCprefix + 'hdnIds').value == '')      
            {  
            document.getElementById(UCprefix + 'hdnIds').value += document.getElementById(objId).value + ',';          
            
            }
        else 
            { 
                document.getElementById(UCprefix  + 'hdnIds').value += document.getElementById(objId).value + ',';          
                
            }
    }        
    else if (!checkBoxId.checked)      
    {
        var currentIds = document.getElementById(UCprefix  + 'hdnIds').value;        
        var removeId = document.getElementById(objId).value;
        if (currentIds.indexOf(removeId) != -1)
        {  
            var finalIds;            
            finalIds = currentIds.substring(0, currentIds.indexOf(removeId)-1)                 
            if(currentIds.indexOf(removeId) + removeId.length == currentIds.length)            
                {
                finalIds +="," + currentIds.substring(currentIds.indexOf(removeId) + removeId.length + 1, currentIds.length);
                }
            else
                {
                finalIds +="," + currentIds.substring(currentIds.indexOf(removeId) + removeId.length + 1, currentIds.length);
                }      
            document.getElementById(UCprefix + 'hdnIds').value = finalIds;                      
        }                           
    } 
   // var linkExcel = document.getElementById('lnkExport').id
    
    document.getElementById(ancHr).href = "../SecurePages/" + page + "?InvoiceNo=" + document.getElementById(UCprefix  + 'hdnIds').value;
    try {
        document.getElementById(ancExp).href = "../SecurePages/ExportToExcel.aspx?page=MyInvoices&status=paid&InvoiceNo=" + document.getElementById(UCprefix  + 'hdnIds').value + "&FDate=" + document.getElementById(UCprefix  + 'hdnFromDate').value + "&TDate=" + document.getElementById(UCprefix  + 'hdnToDate').value;                                               
    }
    catch(err) {    
    }
    
}

function CheckAll(me,checkBoxId)
{

    var index = me.name.indexOf('_');  
    var prefix = me.name.substr(0,index); 

    // Looks for the right checkbox
    for(i=0; i<document.forms[0].length; i++) 
    { 
        var o = document.forms[0][i]; 
        var str =new String();
        str=o.id;       
        if (o.type == 'checkbox' && (me.name != o.name) && (o.name.substring(0, prefix.length) == prefix) ) 
        {       
            if (str.indexOf(checkBoxId)!=-1) 
            {    
                o.checked = me.checked;                 
                //GetSelectedIds(o.id, selectId,strUC);                                        
            }
        } 
    }   
}


  function showHideRatingDetails(objId, visible)
{
	document.getElementById(objId).style.visibility = visible;
}


// code to set focus on validation
function callSetFocusValidation(parentObjID)
{   
   if (parentObjID != "")
   {
     parentObjID = parentObjID + "_"
     if (parentObjID == "UCSpecialistsForm1_")
        {           
            if (document.getElementById("ctl00_ContentHolder_" +parentObjID+"txtVisaExpDate").value == "Enter expiry date")
              {document.getElementById("ctl00_ContentHolder_"+parentObjID+"txtVisaExpDate").value = "";}
            if (document.getElementById("ctl00_ContentHolder_"+parentObjID+"txtDBSExpiryDate").value == "Enter expiry date")
                {document.getElementById("ctl00_ContentHolder_"+parentObjID+"txtDBSExpiryDate").value = "";}            
            if (document.getElementById("ctl00_ContentHolder_"+parentObjID+"txtCSCSDate").value == "Enter expiry date")
                {document.getElementById("ctl00_ContentHolder_"+parentObjID+"txtCSCSDate").value = "";}            
        }
      }        
   	focusTime = setTimeout("setFocusValidation('" + parentObjID + "')", 5000);
   	//populateConfirmationSummary();
}
function setFocusValidation(objID)
{  
	try
	{  
		if(document.getElementById(objID  + "validationSummarySubmit").innerHTML != "" || document.getElementById(objID  + "lblError").innerText != "" || document.getElementById(objID  + "divValidationMsg").innerHTML != "")
		{
			setFocus(objID);
			 if (parentObjID == "UCSpecialistsForm1_")
            {       
                if (document.getElementById("ctl00_ContentHolder_"+parentObjID+"txtVisaExpDate").value == "")
                  {document.getElementById("ctl00_ContentHolder_"+parentObjID+"txtVisaExpDate").value = "Enter expiry date";}
                if (document.getElementById("ctl00_ContentHolder_"+parentObjID+"txtDBSExpiryDate").value == "")
                    {document.getElementById("ctl00_ContentHolder_"+parentObjID+"txtDBSExpiryDate").value = "Enter expiry date";}                
                if (document.getElementById("ctl00_ContentHolder_"+parentObjID+"txtCSCSDate").value == "")
                    {document.getElementById("ctl00_ContentHolder_"+parentObjID+"txtCSCSDate").value = "Enter expiry date";}            
            }
		}
						
	}
	catch(error){}
	try
	{
	if(document.getElementById("CustomValidatorCtrl"))
		{
		if(document.getElementById("CustomValidatorCtrl").innerHTML != "")
		{
			setFocus(objID);
		}
		}
	}
	catch(error){}
	try
	{
		if(document.getElementById(objID  + "divValidationMsg").innerHTML != "")
			setFocus(objID);
	}
	catch(error){}
}
function setFocus(objId)
{
	try
	{
		if(document.getElementById(objId  + "validationSummarySubmit").innerHTML != "" || document.getElementById(objId  + "lblError").innerText != "" || document.getElementById(objId  + "divValidationMsg").innerHTML != "")
		{		
		      document.getElementById(objId  + "btnFocus").focus()				
		}
		
	}
	catch(error)
	{
		try
		{
			document.getElementById(objId + "btnFocus").focus()
		}
		catch(error){}
	}
	
}


  function isValidDate(srcValue)
 {
	return /^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$/.test(srcValue);
 }
 
function setValFocus()
  {
  	setTimeout("document.getElementById('divPage').focus()",200);
  }
  

 /*-----------------Enter Kry ----------------------------------------------------------- */
 
document.onkeydown =
	function (evt)
	{
	   
		var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
		var eventTarget = evt ? evt.target : event.srcElement;
		if (keyCode == 13 && ((eventTarget.id == "UCLogin1_txtLoginUserName")|| (eventTarget.id == "UCLogin1_txtLoginPassword") || (eventTarget.id == "UCActivateAccount1_txtPassword") ) )
		{
		if (document.getElementById("UCLogin1_lnkBtnLogin")=="javascript:__doPostBack('UCLogin1$lnkBtnLogin','')")
		{
		    __doPostBack('UCLogin1$lnkBtnLogin','');					
				return false;
		}
		if (document.getElementById("UCActivateAccount1_lnkbtnSubmit")=="javascript:__doPostBack('UCActivateAccount1$lnkbtnSubmit','')")
		{
		    __doPostBack('UCActivateAccount1$lnkbtnSubmit','');					
				return false;
		}			
		}
					
	}
 /*---------------------------------------------------------------------------------------*/
 /*---------------------------------AJAX Error Handling Code------------------------------------START-------*/
 
 
 setTimeout("InitializeSysObject()",1000);
 function InitializeSysObject()
 {
    if (typeof Sys != 'undefined')
   {
    // alert(Sys);    
    Sys.Application.add_load(AppLoad);
   }
 }  
function AppLoad()
{
  Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
  Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequest);
}

function BeginRequest(sender, args) {
  // Hide AJAX error Message
 //TODO
} 
function EndRequest(sender, args) {
  // Check to see if there's an error on this request.
  if (args.get_error != undefined && args.get_error()!=null)
  {
     var URL = unescape(location.href)          
     
    // Show Ajax Error Message
    //TODO
    
   //Uncomment line below  this is cxommented for testing purpose only
  // alert("We're sorry, an error has occurred. We Request you to kindly try again. If the error re-occurs please try again the next day as our technical team would have fixed the error by then.");
  
    // Let the framework know that the error is handled, 
    //  so it doesn't throw the JavaScript alert.
    
    //Uncomment line below  this is cxommented for testing purpose only
    // args.set_errorHandled(true);       
  sendAJAXErrorDetails(URL,args.get_error().message)
  }
}
function createRequestObject() {
    var ro;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}
var http = createRequestObject();
function sendAJAXErrorDetails(url,message) {
    var errorPageSite="/SecurePages/errorpage.aspx?url="       
    http.open("get", errorPageSite + url +"&message="+message);
    http.onreadystatechange = handleResponse;
    http.send(null);
}
function handleResponse() {
    if(http.readyState == 4){
        var response = http.responseText;                
        //TODO  
       // alert(response) 
    }
}

/*---------------------------------AJAX Error Handling Code--------------------------------------END-----*/

/*------------------------------------------------------------------------------------------------------*/
function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}

function CalculatePrice(ID)
{  

    if(ID == "UCCreateWorkOrderUK1_txtWholesaleDayJobRate" || ID == "UCCreateWorkOrderUK1_txtEstTimeRqrd")
        {  
            if(document.getElementById("UCCreateWorkOrderUK1_txtWholesaleDayJobRate") != null)
                {
                    document.getElementById("UCCreateWorkOrderUK1_txtSpendLimitWP").value =roundNumber( document.getElementById("UCCreateWorkOrderUK1_txtWholesaleDayJobRate").value * document.getElementById("UCCreateWorkOrderUK1_txtEstTimeRqrd").value,2)
                }
            
        }    
     if(ID == "UCCreateWorkOrderUK1_txtPlatformDayJobRate" || ID == "UCCreateWorkOrderUK1_txtEstTimeRqrd")
        {
            if(document.getElementById("UCCreateWorkOrderUK1_txtPlatformDayJobRate") != null)
                {
                    document.getElementById("UCCreateWorkOrderUK1_txtSpendLimitPP").value = roundNumber(document.getElementById("UCCreateWorkOrderUK1_txtPlatformDayJobRate").value * document.getElementById("UCCreateWorkOrderUK1_txtEstTimeRqrd").value,2)
                }
        }
     if(ID == "ctl00_ContentHolder_UCWOProcess1_txtProposedRate" || ID == "ctl00_ContentHolder_UCWOProcess1_txtEstimatedTimeInDays")
        {
            document.getElementById("ctl00_ContentHolder_UCWOProcess1_txtValue").value =roundNumber(document.getElementById("ctl00_ContentHolder_UCWOProcess1_txtProposedRate").value * document.getElementById("ctl00_ContentHolder_UCWOProcess1_txtEstimatedTimeInDays").value,2)
        }
        
      if(ID == "txtNoOfStages" && document.getElementById("hdnPricingMethod").value == "Fixed")
        { 
            document.getElementById("txtWholesaleStagePrice").value = roundNumber(document.getElementById("txtWholesalePrice").value / document.getElementById("txtNoOfStages").value,2)
            document.getElementById("txtPlatformStagePrice").value =roundNumber(document.getElementById("txtPlatformPrice").value / document.getElementById("txtNoOfStages").value ,2)
        }  
      if(ID == "txtEstimatedTimeInDays" && document.getElementById("hdnPricingMethod").value == "DailyRate")
        { 
            document.getElementById("txtWholesaleStagePrice").value = roundNumber(document.getElementById("txtEstimatedTimeInDays").value * document.getElementById("txtWholesaleDayRatePrice").value,2)
            document.getElementById("txtPlatformStagePrice").value = roundNumber(document.getElementById("txtEstimatedTimeInDays").value * document.getElementById("txtPlatformDayRatePrice").value ,2)
            document.getElementById("txtWholesaleStagePrice").disabled = true
            document.getElementById("txtPlatformStagePrice").disabled = true
        }  
      if(ID == "txtEstimatedTimeInDaysSETWP" || ID == "txtWholesaleDayRateSETWP")
        { 
            document.getElementById("txtWholesalePrice").value = roundNumber(document.getElementById("txtEstimatedTimeInDaysSETWP").value * document.getElementById("txtWholesaleDayRateSETWP").value,2)            
        }          
}
/*---------------------------------------------------------------------------------------------*/
/*  ===================================================================================================================  */

function showSiteSubMenu(subMenu)
{
  //  alert("in show")
    document.getElementById(subMenu).style.visibility="visible";   

}
function hideSiteSubMenu(subMenu)
{ 
 //   alert("in hide" + subMenu) 
  
 setTimeout("document.getElementById('"+subMenu+"').style.visibility='hidden'",2000);
  
 // document.getElementById(subMenu).style.visibility="hidden";  
  
}

//Check if OW Admin has provided the PONUmber or Statement of Works

//--------------------------------------------------------------------------------------------------------
//Function to call the function to populate the Location details on Selection of a Location from ddlLocation
//This function calls the PopulateLocDetails which further populates the location details.
//Added by Pratik Trivedi on 30th Dec, 2008
//--------------------------------------------------------------------------------------------------------
function onLocationChange(ddlLocName)
{  
    //alert(ddlLocName);
    if (document.getElementById(ddlLocName).value != "")
    {
        populateLocDetails(document.getElementById(ddlLocName).value, ddlLocName, locArr);
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnLocationId").value = document.getElementById(ddlLocName).value;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_btnFind").disabled = true;        
    }
    else
    {
        clearForTempLoc();
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_btnFind").disabled = false;
    }
}

//--------------------------------------------------------------------------------------------------------
//Function to populate the Location details on Selection of a Location from ddlLocation
//This function hides the FirstName and LastName fields, populates location details from array and then disables the textboxes.
//Added by Pratik Trivedi on 30th Dec, 2008
//--------------------------------------------------------------------------------------------------------
function populateLocDetails(curr, ddlLocName, arrayStore)
{  
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tblContactInfo").style.display = "none";  
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lbltxtCompLocName").innerHTML = "Location Name";    
    
    document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdFName').enabled = false;
    document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdLName').enabled = false;      
        
    for(var i= 1 ; i<=arrayStore.length- 1 ; i++)
    {
        if(arrayStore[i][0]== curr)
        {
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tdCompName").style.display = "block";
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tdtxtCompName").style.display = "block";
            
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtName").value = arrayStore[i][1];                 
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyAddress").value = arrayStore[i][2];
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCity").value = arrayStore[i][3];
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCounty").value = arrayStore[i][4];
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPostalCode").value = arrayStore[i][5];
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPhone").value = arrayStore[i][6];
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyFax").value = arrayStore[i][7];
            
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtName").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyAddress").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCity").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCounty").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPostalCode").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPhone").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyFax").disabled = true;
        }
    }
}

//--------------------------------------------------------------------------------------------------------
//Function to reset the form for Creating a temporary location
//This function shows the FirstName and LastName fields and sets the value of all the textboxes to blank.
//Added by Pratik Trivedi on 30th Dec, 2008
//--------------------------------------------------------------------------------------------------------
function clearForTempLoc() 
{               
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
        {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tdCompName").style.display = "none";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tdtxtCompName").style.display = "none";}
            
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lbltxtCompLocName").innerHTML = "Company Name";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_tblContactInfo").style.display = "block";
        
        document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdFName').enabled = true;
        document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdLName').enabled = true;  
        
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtName").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyAddress").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCity").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCounty").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPostalCode").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPhone").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyFax").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtFName").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtLName").value = "";        
        
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtName").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyAddress").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCity").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCounty").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPostalCode").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPhone").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyFax").disabled = false;
}

function onMainCatChange(ddlMainCat)
{  
    var hdnsubcatval = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategoryVal");
    var hdnsubcattxt = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategorytxt");
    var subcat = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType");
    hdnsubcatval.value = "";
    hdnsubcattxt.value = ""; 
    if (document.getElementById(ddlMainCat).value != "")
    {
        subcat.disabled = false;
        clearOptions(subcat);
        //populateSubCat(document.getElementById(ddlMainCat).value, MainCatArr, SubCatArr);
        populateSubCat(document.getElementById(ddlMainCat).value);
        hdnsubcattxt.value = subcat.options[subcat.selectedIndex].text;        
        hdnsubcatval.value = subcat.value;  
    }
    else
    {
        clearOptions(subcat);
        subcat.disabled = true;
    }
}


function populateSubCat(ddlMainCatValue)
{
    var browserName=navigator.appName;
    var selectedCombID=0;    
    xmlDoc=loadXMLDoc("../../OWLibrary/XML/WOFormStandards.xml");    
    if (browserName=="Netscape")
    {        
        for (i = 1; i <= xmlDoc.childNodes[0].childNodes.length; i++)
        { if (xmlDoc.childNodes[0].childNodes[i] == "[object Element]")
           {
            if (xmlDoc.childNodes[0].childNodes[i].tagName == 'MainCat')            
            {if(xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue==ddlMainCatValue)
                 {selectedCombID=xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue;}
            }
            if (selectedCombID != 0)
            {
                if (xmlDoc.childNodes[0].childNodes[i].tagName == 'SubCat')                
                {    try
                    {if (xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue==selectedCombID)
                     {
                        addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType"),xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("Name")[0].childNodes[0].nodeValue,xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue);
                     }}
                     catch(err)
                     {
                    }
                }
            }
           }
        }        
    }
    else if(browserName=="Microsoft Internet Explorer")
    {
        for (i = 1; i <= xmlDoc.childNodes[1].childNodes.length; i++)
        { if (xmlDoc.childNodes[1].childNodes[i])
           {
           
           if (xmlDoc.childNodes[1].childNodes[i].tagName == 'MainCat')            
            {if(xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue==ddlMainCatValue)
                 {selectedCombID=xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue;}
            }
            if (selectedCombID != 0)
            {
                if (xmlDoc.childNodes[1].childNodes[i].tagName == 'SubCat')
                {
                    try
                    {
                      
                        if (xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue==selectedCombID)
                         {
                            addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType"),xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("Name")[0].childNodes[0].nodeValue,xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue);
                         }
                     }
                     catch(err)
                     {
               
                    }
                }
            }
           }
        }
    }
       
}

function onSubCatChange(ddlSubCat)
{      
    var subcat = document.getElementById(ddlSubCat).value;    
    var subcattxt = ""
    if(subcat != "")
    {
        subcattxt = document.getElementById(ddlSubCat).options[document.getElementById(ddlSubCat).selectedIndex].text;        
    }    
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = subcat;
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = subcattxt;
}

function addOption(selectbox,text,value )
{ 
    var optn = document.createElement("OPTION");
    optn.text = text;
    optn.value = value;
    selectbox.options.add(optn); 
}
function removeOption(selectbox,text)
{
   
  var i;
  for (i = selectbox.length - 1; i>=0; i--) 
  {
    if (selectbox.options[i].value==text) 
    {
      selectbox.remove(i);
    }
  }
}
function clearOptions(selectbox)
{ //Function for removing all options of a Select Box
    while(selectbox.options.length>0)
        selectbox.remove(0)
}
function ZeroValueWO(status)
{   
    if(status == "true") {    
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value = "0";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").disabled = true;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_CustProposedPriceValidator").enabled = false;        
    }
    if(status == "false") {    
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_CustProposedPriceValidator").enabled = true;
    }
}
function getdate()
{   
    var startdate = new Date(getDateObject(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value,"/"));
    return startdate;
}
function getbegindate()
{   
    var startdate = new Date(getDateObject(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value,"/"));
    return startdate;
}
function getWOProcessDate()
{
    var startdate = new Date(getDateObject(document.getElementById("ctl00_ContentHolder_UCWOProcess1_txtDateStart").value,"/"));
    return startdate;
}
function getDateObject(dateString,dateSeperator)
{
//This function return a date object after accepting 
//a date string ans dateseparator as arguments
var curValue=dateString;
var sepChar=dateSeperator;
var curPos=0;
var cDate,cMonth,cYear;
//extract day portion
curPos=dateString.indexOf(sepChar);
cDate=dateString.substring(0,curPos);
//extract month portion 
endPos=dateString.indexOf(sepChar,curPos+1); 
cMonth=dateString.substring(curPos+1,endPos);
//extract year portion 
curPos=endPos;
endPos=curPos+5;
cYear=dateString.substring(curPos+1,endPos);
//Create Date Object
var changeddate = cMonth+"/"+cDate+"/"+cYear;
return changeddate;
}

function getProductDescription(myval,HiddenWoTitle,HiddenScopeOfWork)
{      
    
    
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSelectedService").value = parseInt(myval);
    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divService").className == "displayBlock" && myval != "0") // && myval != "1" )
    {
      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divService").className = "displayNone";
      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divCompForm").className = "displayBlock";
      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct").value = myval;      
      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtbxDupPONumber").value;      
    }
    
    if (myval == "0")
    {removeProductInfo("Service");
      document.getElementById("divProfile").className = "displayNone";
    
//     if (document.getElementById("divchkForceSignOff")) {
     document.getElementById("divchkForceSignOff").className = "displayNone";
//       }
    }
	else if(myval == "1")
    {removeProductInfo("Quote");
    document.getElementById("divProfile").className = "displayBlock";
//    if (document.getElementById("divchkForceSignOff")) {
     document.getElementById("divchkForceSignOff").className = "displayBlock";
//       }
    }
	else
	{
    document.getElementById("divProfile").className = "displayBlock";
//    if (document.getElementById("divchkForceSignOff")) {
     document.getElementById("divchkForceSignOff").className = "displayBlock";
//       }
         if(HiddenWoTitle != "")
          {
              var i;
              var ISItemExists;
              ISItemExists = 0
              for (i = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct").length - 1; i>=0; i--) 
              {             
              if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct").options[i].value == myval) 
                {ISItemExists = 1}
              }          
              if(ISItemExists == 0)
              { 
               
                if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnPrevHiddenProductId").value != "")
                removeOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct"),document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnPrevHiddenProductId").value);
               
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnPrevHiddenProductId").value = myval;      
                addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct"),HiddenWoTitle,myval);
              }            
          }
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct").value = parseInt(myval);
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSelectedService").value = parseInt(myval);
	    $.ajax({
        type:"POST",
        url:"WOForm.aspx/GetProductDetails",
        data: "{'productid':'"+myval+"'}",
        contentType: "application/json; charset=utf-8",    
        dataType:"json",        
        success:function(data)    
        {var json = JSON.stringify(data);
            var loop = data.d;
           $.each(loop.rows, function (i, rows) {
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnProductId").value = parseInt(rows.WOID);            
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnbtnTrigger").click(); 
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").value = rows.WOTitle;
              if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "IT")
        {
            if (rows.IsSignOffSheetReqd == true )
            { 
              document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_chkForceSignOff").checked = true;
                }
            else
            {
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_chkForceSignOff").checked = false;
            }
         }
            if (rows.ReviewBids == true )
            { 
              document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnReviewBids").value = true;
            }
            else
            {
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnReviewBids").value = false;
            }    
            //alert(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnReviewBids").value);
            //alert(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle"));
           document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlScopeWork").style.display = "block";
           document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlClientScope").style.display = "none";
           //alert(rows.WOLongDesc);
           document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = rows.WOLongDesc.replace(/<BR>/g,"");
           ValidatorEnable(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rqdScopeOfWork"),false);
           //alert(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnShowPnlScopeWork").value)
          
            if (rows.ClientScope != "")
                {
                 if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnShowPnlScopeWork").value == "")
                {
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlClientScope").style.display = "block";
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlScopeWork").style.display = "none";
                }
                else
                {
                    document.getElementById("divchkForceSignOff").className = "displayNone";
                }
                if(HiddenScopeOfWork != "")
                {
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value = HiddenScopeOfWork.replace(/<BR>/g,"") + rows.ClientScope.replace(/<BR>/g,"");
                }
                else
                {
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value = rows.ClientScope.replace(/<BR>/g,"");
                }
                ValidatorEnable(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rqdScopeOfWork"),true);}            
            
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value = rows.JRSNumber;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpecialInstructions").value = rows.SpecialInstructions.replace(/<BR>/g,"");                    
            if (parseInt(rows.LocationID) > 0)
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnBillingLocation").value = parseInt(rows.LocationID);}
                //document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnBillingLocation").disabled = true;}
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value = rows.WholesalePrice;            
            if (rows.CombId != 0)
            {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").value = parseInt(rows.CombId);
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").disabled = false;
            clearOptions(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType"));
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = "";
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = "";
            populateSubCat(parseInt(rows.CombId));
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").value = parseInt(rows.WOCategoryID);
            
            if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType") != null)
                { 
                onSubCatChange("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType");
            }
            }
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value = rows.DeferredDate;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = rows.DeferredDate;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnPlatformPrice").value = rows.PlatformPrice;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlProposedPrice").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdoNon0Value").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdoNon0Value").checked = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").checked = false;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnClientStrtDate").value="";
            document.getElementById("tdRadioButtonPrice").style.display="block";
            document.getElementById("tdTextBoxPrice").style.display="block";            
            if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object]") 
             {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = true;}
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").disabled = true;
            if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object]") 
             {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").disabled = true;}
            if (rows.FreesatService == true && document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
            {               

                if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1") != null)
                { 
                    if(rows.DefaultGoodsLocation != 1)
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").value = rows.DefaultGoodsLocation
                    else
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").selectedIndex = 1
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").disabled = true;
                }                
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display = "block";          
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "block";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_chkForceSignOff").style.display = "none";
                 document.getElementById("divchkForceSignOff").style.display = "none";
                populateFreesatMake();
                  
            }
            else
            {        
               if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1") != null)
               { 
                 if(rows.DefaultGoodsLocation != 1)
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").value = rows.DefaultGoodsLocation
                 else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").selectedIndex = 0
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").disabled = false;        
               }
                
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display = "none";          
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "none";
           
            }

                 if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "IT")
                 {
                      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").disabled = true;
                      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = true;
                       if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnShowPnlScopeWork").value == "1" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnShowPnlScopeWork").value == "2")
                       {
                       document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = false;
                       }
                 }    
                 else
                 {
                     //Sumit
                    if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnCompanyId").value == 14897)
                    {
                        var IsMorningSlot 
                        IsMorningSlot = 0
                        var i
                        for (i = 0; i < document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").options.length; i++) 
                        {
                        //alert(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").options[i].value);
                            if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").options[i].value == "8am - 1pm")
                            {
                                IsMorningSlot = 1
                            }                            
                        }
                       
                        //alert(rows.RemoveMorningSlot);
                        if(rows.RemoveMorningSlot == 1)
                        {
                            if(IsMorningSlot == 1)
                            removeOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime"),"8am - 1pm");
                        }
                        else
                        {
                        // alert(IsMorningSlot);
                            if(IsMorningSlot == 0)
                            addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime"),"8am - 1pm","8am - 1pm");
                        }
                    }                    
                 }

                  if (rows.TimeBuilderQuestions > 0)             
                  {
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBtnShowHideMdlTBQuestions").click();
                  }
                  else
                  {
                    if (rows.ServiceQuestion == 1)             
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBtnShowHideMdlQuestions").click();
                  }                
                 
           });                 
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) 
	    {            
		    var jsonError = JSON.parse(XMLHttpRequest.responseText);            
		    alert(JSON.stringify(jsonError));
	    }});
	}
}
function removeProductInfo(id)
{
    if (id=="Service")
    {
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct").selectedIndex = 0
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlProposedPrice").disabled = false;
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnProductId").value = "";
	    
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").value = "";
	    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object]") 
             {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = "";
             document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = false;}            
        //if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object]") 
         {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value = "";
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").disabled = false;}
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlClientScope").style.display = "block";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlScopeWork").style.display = "none";
        ValidatorEnable(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rqdScopeOfWork"),true);
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpecialInstructions").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientInstru").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value = 0;
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = ""; 
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = getTodayDate();
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnBillingLocation").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display = "none";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "none";        
        var StartDate = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value;
        var SecStartDate = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value;
        
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
        {
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value == "6pm - 8pm" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value == "1pm - 6pm")
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value = "8am - 1pm";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").disabled = false;    
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = StartDate;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value = SecStartDate;                            
                }
        else
            {//document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value = SecStartDate;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = SecStartDate;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value = StartDate;}
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnEvengInst").value == "True")
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").remove(2);}
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlProposedPrice").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdoNon0Value").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").disabled = false;
         document.getElementById("tdRadioButtonPrice").style.display="block"
         document.getElementById("tdTextBoxPrice").style.display="block"
     }
     else if(id=="Quote")
     {
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlProposedPrice").disabled = false;
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnProductId").value = "";
	    
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").value = "";
	    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object]") 
             {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = "";
             document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = false;}            
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object]") 
         {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value = "";
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").disabled = false;}
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlClientScope").style.display = "block";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlScopeWork").style.display = "none";
        ValidatorEnable(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rqdScopeOfWork"),true);
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpecialInstructions").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value = 0;
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").value = "";
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = "";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = ""; 
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = getTodayDate();
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnBillingLocation").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display = "none";
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "none";        
        var StartDate = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value;
        var SecStartDate = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value;
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value == "6pm - 8pm" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value == "1pm - 6pm")
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").value = "8am - 1pm";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").disabled = false;    
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = StartDate;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value = SecStartDate;                            
                }
        else
            {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value = SecStartDate;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = SecStartDate;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSecStartDate").value = StartDate;}
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnEvengInst").value == "True")
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").remove(3);}
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlProposedPrice").disabled = false;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").disabled = true;
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").checked = true;
        document.getElementById("tdRadioButtonPrice").style.display="none"
        document.getElementById("tdTextBoxPrice").style.display="none"
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdoNon0Value").disabled = false;
        
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").disabled = false;
         document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").disabled = false;
     }
         
}
function populateWOFOrm()
{
    //alert("Hi I've reached");
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlWoForm").style.display = "block";
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlWoSummary").style.display = "none";
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divValidationMain").style.display = "none";  
}

function populateConfirmationSummary()
 {      
        var flag;
        var errorstring;
        flag = true;
        errorstring = "";        
        if (Page_ClientValidate() == false)
        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlLocation").value != "")
                {document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdFName').enabled = false;
                 document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdLName').enabled = false;}
            else
                {document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdFName').enabled = true;
                 document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdLName').enabled = true;}   
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divValidationMain").style.display = "block";}
        else
        {
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object HTMLInputElement]") 
            {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection").value == "None Specified")
                {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                    {flag = false;
                     errorstring = "Please enter the location where the specified goods will be located";}}}
        else
            {if ((document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1")=="[object HTMLSelectElement]") || (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1") == "[object]")){ if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").value == "None Specified" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").selectedIndex].text == "None Specified")
                {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                    {flag = false;
                     errorstring = "Please enter the location where the specified goods will be located";}}}}
            
            if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur") == "[object]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur") == "[object HTMLInputElement]") 
            {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur").value.length > 2000 )
                {flag = false;
                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                    {errorstring = errorstring + "<br>Maximum 2000 characters are allowed in Product Purchased";}
                    else
                    {errorstring = errorstring + "<br>Maximum 2000 characters are allowed in Equipment Details";}}}
            
                
            //added by PratikT on 1st July, 2009 for validating Freesat Make/Model
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object]")                    
                {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display == "block" )
                    {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").selectedIndex].text == "None Specified")
                        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                            {flag = false;
                             errorstring = "Please select the Freesat Make/Model";}}}}
                
            if (flag == false)
            {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divValidationMain").style.display = "block";
             document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblError").innerHTML = errorstring;}
            else
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divValidationMain").style.display = "none"; 
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlWoForm").style.display = "none";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlWoSummary").style.display = "block";                
                var category = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").selectedIndex].text;
                category = category + " - " + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").selectedIndex].text;            
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblCategory").innerHTML = category;  
                //alert(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpecialInstructions").value);                       
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value != "") 
                    {
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value)//.replace("(bold)","<b>").replace("(/bold)","</b>");
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML = ReplaceAll(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML,"(bold)","<b>"); 
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML = ReplaceAll(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML,"(/bold)","</b>"); 
                    }
                else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value)//.replace(/\\n/g,"<br>");
            
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblSpecialInstructions").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpecialInstructions").value);//.replace(/\\n/g,"<br>");}
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblDateCreated").innerHTML = getTodayDate();
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLocName").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCity").value + ", " + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPostalCode").value;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblContact").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnContactName").value;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblWOTitle").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").value;               
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPrice").innerHTML = "&" + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnCurrency").value + " " + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value;                       
                if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnClientStrtDate").value !="")
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblStartDate").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnClientStrtDate").value;}
                else{document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblStartDate").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value;}
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblDateStart").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value;
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value != "Retail")
                {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInEnd").value == "")
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblEndDate").innerHTML = "None Specified";            
                    else
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblEndDate").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInEnd").value;
                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "IT")
                        {document.getElementById("divConfProductsPur").style.display = "block";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblProdPur").innerHTML = "Equipment Details: ";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfProdPur").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur").value);
                        document.getElementById("divConfRetail").style.display = "none";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfGoods").innerHTML = "Location of Equipment";}
                    else
                        {document.getElementById("divConfProductsPur").style.display = "none";
                        document.getElementById("divConfRetail").style.display = "none";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divSummGoodsLocation").style.display = "none";}}
                else
                    {
                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
                    {
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblStartDateLabel").innerHTML = "Appointment";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtlblEndDate").innerHTML = "Installation Time";  
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPONumberSummary").innerHTML="Customer Ref. No."
                    }                    
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblEndDate").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").selectedIndex].text;
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnInstTime").value = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").selectedIndex].text;
                    document.getElementById("divConfProductsPur").style.display = "block";
                    document.getElementById("divConfRetail").style.display = "block";
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfProdPur").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur").value);
                    
                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSalesAgent").value == "")
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfSalesAgent").innerHTML = "None Specified";
                    else
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfSalesAgent").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSalesAgent").value);                    
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnFreesat").value = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").value;
                        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object]")
                        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display == "block" )
                            {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfFreesatMake").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").selectedIndex].text;}
                         else document.getElementById("divConfSFreesat").style.display = "none";
                        }}
               
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblSupplyParts").innerHTML = "No";
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value == "")
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPONumber").innerHTML = "None Specified";
                else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPONumber").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value;
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientRefNumber").value == "")
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblClientRefNumberSum").innerHTML = "None Specified";
                else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblClientRefNumberSum").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientRefNumber").value;
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value == "")
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblJRSNumber").innerHTML = "None Specified";
                else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblJRSNumber").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value;          
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblDressCode").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlDressCode").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlDressCode").selectedIndex].text;                            
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object HTMLInputElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object]")
                {
                    var GoodsLocation = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection").value;                    
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblBusinessDivision").innerHTML = GoodsLocation;
                }
                else if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1") == "[object HTMLSelectElement]")
                {
                    var GoodsLocation = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").selectedIndex].text;                    
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblBusinessDivision").innerHTML = GoodsLocation;
                }
                else
                {
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblBusinessDivision").innerHTML = "None Specified";
                }
            }

             
        }
            if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnShowPnlScopeWork").value == "1" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnShowPnlScopeWork").value == "2")
            {
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divLongDesc").style.display = "none";
                document.getElementById("divConfProductsPur").style.display = "none";
                 document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPONumberSummary").innerHTML="PO Number"
              
               document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblClientScope").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value)//.replace(/\\n/g,"<br>");
               document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblClientScope").innerHTML = ReplaceAll(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblClientScope").innerHTML,"(bold)","<b>"); 
               document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblClientScope").innerHTML = ReplaceAll(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblClientScope").innerHTML,"(/bold)","</b>"); 
               if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnShowPnlScopeWork").value == "2" )
               {
                   document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblStartDateLabel").innerHTML="Appointment Date"
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtlblEndDate").innerHTML = "Start Time";  
               }
               else
                {
                   document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblStartDateLabel").innerHTML="Start Date"
                }
            }
}

function populateConfirmationSummaryAT800()
 {      
        var flag;
        var errorstring;
        flag = true;
        errorstring = "";        
        if (Page_ClientValidate() == false)
        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlLocation").value != "")
                {document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdFName').enabled = false;
                 document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdLName').enabled = false;}
            else
                {document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdFName').enabled = true;
                 document.getElementById('ctl00_ContentHolder_UCCreateWorkOrderUK1_RqdLName').enabled = true;}   
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divValidationMain").style.display = "block";}
        else
        {
        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object HTMLInputElement]") 
            {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection").value == "None Specified")
                {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                    {flag = false;
                     errorstring = "Please enter the location where the specified goods will be located";}}}
        else
            {if ((document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1")=="[object HTMLSelectElement]") || (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1") == "[object]")){ if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").value == "None Specified" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").selectedIndex].text == "None Specified")
                {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                    {flag = false;
                     errorstring = "Please enter the location where the specified goods will be located";}}}}
            
            if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur") == "[object]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur") == "[object HTMLInputElement]") 
            {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur").value.length > 2000 )
                {flag = false;
                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                    {errorstring = errorstring + "<br>Maximum 2000 characters are allowed in Product Purchased";}
                    else
                    {errorstring = errorstring + "<br>Maximum 2000 characters are allowed in Equipment Details";}}}
            
                
            //added by PratikT on 1st July, 2009 for validating Freesat Make/Model
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object]")                    
                {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display == "block" )
                    {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").selectedIndex].text == "None Specified")
                        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                            {flag = false;
                             errorstring = "Please select the Freesat Make/Model";}}}}
                
            if (flag == false)
            {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divValidationMain").style.display = "block";
             document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblError").innerHTML = errorstring;}
            else
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divValidationMain").style.display = "none"; 
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlWoForm").style.display = "none";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlWoSummary").style.display = "block";                
                var category = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").selectedIndex].text;
                category = category + " - " + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").selectedIndex].text;            
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblCategory").innerHTML = category;  
                //alert(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpecialInstructions").value);                       
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value != "") 
                    {
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value)//.replace("(bold)","<b>").replace("(/bold)","</b>");
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML = ReplaceAll(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML,"(bold)","<b>"); 
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML = ReplaceAll(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML,"(/bold)","</b>"); 
                    }
                else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLongDesc").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value)//.replace(/\\n/g,"<br>");
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblSpecialInstructions").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpecialInstructions").value);//.replace(/\\n/g,"<br>");}
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblDateCreated").innerHTML = getTodayDate();
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblLocName").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCity").value + ", " + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtCompanyPostalCode").value;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblContact").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnContactName").value;
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblWOTitle").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").value;               
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPrice").innerHTML = "&" + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnCurrency").value + " 0";// + document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value;                       
                if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnClientStrtDate").value !="")
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblStartDate").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnClientStrtDate").value;}
                else{document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblStartDate").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value;}
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblDateStart").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value;
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value != "Retail")
                {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInEnd").value == "")
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblEndDate").innerHTML = "None Specified";            
                    else
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblEndDate").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInEnd").value;
                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "IT")
                        {document.getElementById("divConfProductsPur").style.display = "block";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblProdPur").innerHTML = "Equipment Details: ";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfProdPur").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur").value);
                        document.getElementById("divConfRetail").style.display = "none";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfGoods").innerHTML = "Location of Equipment";}
                    else
                        {document.getElementById("divConfProductsPur").style.display = "none";
                        document.getElementById("divConfRetail").style.display = "none";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divSummGoodsLocation").style.display = "none";}}
                else
                    {
                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
                    {
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblStartDateLabel").innerHTML = "Appointment";
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtlblEndDate").innerHTML = "Installation Time";  
                       // document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPONumberSummary").innerHTML="Customer Ref. No."
                    }                    
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblEndDate").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").selectedIndex].text;
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnInstTime").value = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpTime").selectedIndex].text;
                    document.getElementById("divConfProductsPur").style.display = "block";
                    document.getElementById("divConfRetail").style.display = "block";
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfProdPur").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtProductsPur").value);
                    
//                    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSalesAgent").value == "")
//                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfSalesAgent").innerHTML = "None Specified";
//                    else
//                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfSalesAgent").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSalesAgent").value);                    
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnFreesat").value = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").value;
                        if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake") == "[object]")
                        {if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display == "block" )
                            {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblConfFreesatMake").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake").selectedIndex].text;}
                         else document.getElementById("divConfSFreesat").style.display = "none";
                        }}
               
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblSupplyParts").innerHTML = "No";
//                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value == "")
//                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPONumber").innerHTML = "None Specified";
//                else
//                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblPONumber").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtPONumber").value;
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientRefNumber").value == "")
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblClientRefNumberSum").innerHTML = "None Specified";
                else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblClientRefNumberSum").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientRefNumber").value;
//                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value == "")
//                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblJRSNumber").innerHTML = "None Specified";
//                else
//                    //document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblJRSNumber").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value;          
               // document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblDressCode").innerHTML = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlDressCode").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlDressCode").selectedIndex].text;                            
                if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object HTMLInputElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object]")
                {
                    var GoodsLocation = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection").value;                    
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblBusinessDivision").innerHTML = GoodsLocation;
                }
                else if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1") == "[object HTMLSelectElement]")
                {
                    var GoodsLocation = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").options[document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").selectedIndex].text;                    
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblBusinessDivision").innerHTML = GoodsLocation;
                }
                else
                {
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_lblBusinessDivision").innerHTML = "None Specified";
                }
            }
        }
}

function convertTextToHTML(strMultiLineText)
{
var strSingleLineText = strMultiLineText.replace(
// Replace out the new line character.
new RegExp( "\\n", "g" ), 
 

// Put in ... so we can see a visual representation of where
// the new line characters were replaced out.
" <br> " 
);
return strSingleLineText;
}
function getTodayDate()
{
    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    return (day + "/" + month + "/" + year);
}
function populateFreesatMake()
{       
    var browserName=navigator.appName;
    xmlDoc=loadXMLDoc("../../OWLibrary/XML/WOFormStandards.xml");  
    if (browserName=="Netscape")
    {
        for (i = 1; i < xmlDoc.childNodes[0].childNodes.length; i++)    
        { 
            if (xmlDoc.childNodes[0].childNodes[i].tagName == 'FreesatMake')            
            {
                 addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake"),xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("StandardValue")[0].childNodes[0].nodeValue,xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("StandardID")[0].childNodes[0].nodeValue);
            }
        }
    }
    else if (browserName=="Microsoft Internet Explorer")
    {
        for (i = 1; i < xmlDoc.childNodes[1].childNodes.length; i++)    
        { 
            if (xmlDoc.childNodes[1].childNodes[i].tagName == 'FreesatMake')            
            {
                 addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlFreesatMake"),xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("StandardValue")[0].childNodes[0].nodeValue,xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("StandardID")[0].childNodes[0].nodeValue);
            }
        }
    }   
}

function ondrpTimeChange(objID)
{
    var WP = objID.replace("drpTime","txtSpendLimit");
    var WPUplift = objID.replace("drpTime","hdnWPUplift");
    var InstTime = objID.replace("drpTime","hdnInstTime");
    var startdateID = objID.replace("drpTime","hdnStartDate");
    var secondstartdateID = objID.replace("drpTime","hdnSecStartDate");
    var calendarID = objID.replace("drpTime","calBeginDate");
    var ApptDate = objID.replace("drpTime","txtScheduleWInBegin");
    var EvngTimeID = objID.replace("drpTime","hdnEvngInstTime");
    var EvngInstTime = document.getElementById(EvngTimeID).value;
    
    var WPUpliftPercID = objID.replace("drpTime","hdnWPUPliftPercent");
    var WPUpliftPerc = "False";
    WPUpliftPerc = document.getElementById(WPUpliftPercID).value;
    
    var PPUpliftPerc = "False";
    var PPUpliftPercID = objID.replace("drpTime","hdnPPUpliftPercent");
    PPUpliftPerc = document.getElementById(PPUpliftPercID).value;
    
    if ((document.getElementById(objID).value == "6pm - 8pm" || document.getElementById(objID).value == EvngInstTime) && (document.getElementById(InstTime) != "6pm - 8pm" || document.getElementById(InstTime) != EvngInstTime))
       {
            OldWP = document.getElementById(WP).value;
            if (WPUpliftPerc == "True")
                document.getElementById(WP).value = (parseFloat(document.getElementById(WP).value) + parseFloat((document.getElementById(WPUplift).value / 100) * parseFloat(document.getElementById(WP).value))).toFixed(2);
            else
                document.getElementById(WP).value = parseFloat(document.getElementById(WP).value) + parseFloat(document.getElementById(WPUplift).value);
       }
    if ((document.getElementById(objID).value != "6pm - 8pm" || document.getElementById(objID).value != EvngInstTime) && (document.getElementById(InstTime).value == "6pm - 8pm" || document.getElementById(InstTime).value == EvngInstTime))
       {
            if (OldWP == 0.00)
             OldWP = document.getElementById(WP).value;
             
            if (WPUpliftPerc == "True")
                document.getElementById(WP).value = (parseFloat(document.getElementById(WP).value) - parseFloat((document.getElementById(WPUplift).value / 100) * OldWP)).toFixed(2);
            else
                document.getElementById(WP).value = parseFloat(document.getElementById(WP).value) - parseFloat(document.getElementById(WPUplift).value);
       }
    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnProductId").value == "" && document.getElementById(objID).value != "8am - 1pm")
        {var startDate = document.getElementById(startdateID).value;
        var secondDate = document.getElementById(secondstartdateID).value;
        document.getElementById(startdateID).value = ReplaceAll(secondDate,"&#47;","/");         
       // document.getElementById(ApptDate).disabled = false;
        document.getElementById(ApptDate).value = ReplaceAll(secondDate,"&#47;","/"); 
        document.getElementById(secondstartdateID).value = startDate;
     
        //document.getElementById(ApptDate).disabled = true;
        }
    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnProductId").value == "" && document.getElementById(objID).value == "8am - 1pm")
        {var startDate = document.getElementById(startdateID).value;
        var secondDate = document.getElementById(secondstartdateID).value;
        //alert(secondDate);
        document.getElementById(startdateID).value = ReplaceAll(secondDate,"&#47;","/"); 
        document.getElementById(ApptDate).value = ReplaceAll(secondDate,"&#47;","/"); 
        document.getElementById(secondstartdateID).value = startDate;}    
    document.getElementById(InstTime).value = document.getElementById(objID).value;    
    SetDates('TimeSlot');
}
function holidaydates()
{return HolidayArr;
}
function holidaydatesretail()
{return HolidayArr;
}
function GetStartDate(sender,args)
{
var stDate=new Date();
stDate=sender._selectedDate;
var cMonth=stDate.getMonth()+1;
if (cMonth<10)
cMonth='0'+cMonth;
var clienSelectedDate=stDate.getDate() + '/'+ cMonth +'/' + stDate.getFullYear();
document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnClientStrtDate").value=clienSelectedDate;
}
function ChangeAptTimeNPrice(objID)
{
     EvengInstTime = document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnEvngInstTime").value
     if(document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnAptTM").value!="6pm - 8pm" || document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnAptTM").value!= EvengInstTime)
     {
         if (document.getElementById(objID).value == "6pm - 8pm" || document.getElementById(objID).value == EvengInstTime)
         {
             var originalPrice=0.0;
             originalPrice=parseFloat(document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnPrice").value);     
             var uplift=0.0;
             uplift=parseFloat(document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnPPUp").value);
             var upliftPerc = "False";
             upliftPerc = document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnPPUpliftPercent").value;
             var newprice = 0.0;
             if (upliftPerc == "True")
                newprice = (originalPrice + ((uplift / 100) * originalPrice)).toFixed(2);
             else
                newprice = (originalPrice + uplift).toFixed(2);
             document.getElementById("ctl00_ContentHolder_UCWOProcess1_txtValue").value=newprice;
         }
         else
         {
            //document.getElementById("ctl00_ContentHolder_UCWOProcess1_txtValue").value=parseFloat(document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnPrice").value).toFixed(2);  
         }
     }
     else if(document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnAptTM").value=="6pm - 8pm" || document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnAptTM").value == EvengInstTime) 
     {
        
        if (document.getElementById(objID).value != "6pm - 8pm" || document.getElementById(objID).value != EvengInstTime)
         {
             var originalPrice=0.0;
             originalPrice=parseFloat(document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnPrice").value);     
             var uplift=0.0;
             uplift=parseFloat(document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnPPUp").value);
             var newprice = 0.0;
             newprice = (originalPrice - uplift).toFixed(2);
             document.getElementById("ctl00_ContentHolder_UCWOProcess1_txtValue").value=newprice;
         }
         else
         {
            //document.getElementById("ctl00_ContentHolder_UCWOProcess1_txtValue").value=parseFloat(document.getElementById("ctl00_ContentHolder_UCWOProcess1_hdnPrice").value).toFixed(2);  
         }
     
     }
}


function Expand(objid,calledFrom){

if (calledFrom == "PageLoad")
   objid = objid.id
 else
  objid = objid 

CollapseAll()

    var index = objid.lastIndexOf('_');  
    
    var identify = objid.substr(0,index); 
    
    var prefix = identify+'_divMessageDetails';
        
    if (document.getElementById(prefix).style.display="none")
    {
            document.getElementById(prefix).style.display="block";
            document.getElementById(identify+'_trdlRow').style.backgroundColor="#FFFFBC";
    }
}

function CollapseAll()
{   
    var objs=document.getElementsByTagName("div");
      for (i = 0; i < objs.length; i++)
      {
                  {
                        if(objs[i].id.indexOf("divMessageDetails") != -1)
                             document.getElementById(objs[i].id).style.display="none";
                  }
      }
      var objs1=document.getElementsByTagName("tr");
      for (i = 0; i < objs1.length; i++)
      {
                  {
                        if(objs1[i].id.indexOf("trdlRow") != -1)
                             document.getElementById(objs1[i].id).style.backgroundColor="#F7F3F4";
                  }
      }
      
       
}
function MarkMsgSelected(MsgID)
{
    
    var objs=document.getElementsByTagName("input");
    for (i = 0; i < objs.length; i++)
      {
                  {
                        if(objs[i].id.indexOf("hdnMsgID") != -1)
                        {
                            
                            if (document.getElementById(objs[i].id).value == MsgID)
                            {
                                Expand(document.getElementById(objs[i].id),"PageLoad");
                                break;
                            }
                        }     
                  }
      }
}
function validate_required(field,alerttxt,btnID)
{
    var text = document.getElementById(field).value;
    
    if (text != "Enter a new note here")
    {
         while(text.indexOf(" ") != -1 )
        {
            text = text.replace(" ","");
        }
    }
      
      if (text==null||text==""||text=="Enter a new note here")
            {alert(alerttxt);
           }
        else {                      
            document.getElementById(btnID).click();
        }
}
function clearText(field)
{
    document.getElementById(field).value = "";
}

function ShowDiv(id)
{
   document.getElementById(id).style.display = "inline"; 
}
function HideDiv(id)
{
   document.getElementById(id).style.display = "none"; 
}
function SelectGoods(inp, data){
      inp.value = data[0];
      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnGoodsLocation").value = data[1];
}

/*Accordian Panel on welcome page */
function ChangeOnHover(id)
{
 
   if (id == "NeutralRow") 
   {
   document.getElementById(id).style.backgroundColor = "#999999";
   }
   
   if(id == "NegativeRow")
   {
   document.getElementById(id).style.backgroundColor = "#FF9999";  
   }
}
function BackToPreviousColor(id)
{

   if (id == "NeutralRow") 
   {
   document.getElementById(id).style.backgroundColor = "#E5E5E5";
   }
   
   if(id == "NegativeRow")
   {
   document.getElementById(id).style.backgroundColor = "#F0DDD6";  
   }
}

var cntr = 0
function ShowHide(id)
{
    if(cntr == 0)
    {
      document.getElementById(id).style.display = "none";
      cntr = cntr + 1;
    }
    else
    {
      $('#divRatingScore').slideDown("fast");  
      cntr = 0;
    }   
}

function PopulateDepotDetails(myval)
{
    myval = myval.replace("'", "");
   try
   {
        var ScopeOfWork = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = false;
        
        ScopeOfWork = ScopeOfWork.replace("Collection Point Details:" ,"")
             
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = ScopeOfWork;
        
        if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnDepotDetails").value != "" && document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnDepotDetails").value != "0")
        {
            ScopeOfWork = ScopeOfWork.replace(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnDepotDetails").value,"");
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = ScopeOfWork;
            ScopeOfWork = ScopeOfWork.replace(/[\r\n]+/, "");
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = ScopeOfWork;
        }
     
        if (myval != "0" && myval != "1")
        {
            $.ajax({
            type:"POST",
            url:"WOForm.aspx/DepotLocDetails",
            data: "{'AddressID':'"+myval+"'}",
            contentType: "application/json; charset=utf-8",    
            dataType:"json",        
            success:function(data)    
            {   
                var json = JSON.stringify(data);
                 var loop = data.d;
           $.each(loop.rows, function (i, rows) {   
                var ScopeOfWork = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value
                
                if (rows.ServiceLocDetails != '')
                {
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = "Collection Point Details:" + "\n" +rows.ServiceLocDetails.replace(/<BR>/g,"") + "\n" + "\n" + ScopeOfWork;
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnDepotDetails").value = rows.ServiceLocDetails.replace(/<BR>/g,"");
                }                    
               });
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) 
	        {            
		        var jsonError = JSON.parse(XMLHttpRequest.responseText);            
		        alert(JSON.stringify(jsonError));
	        }});	       
	    }
	    else
	    {
	       document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnDepotDetails").value = "";
	    }
	    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = true;
	}
	catch(err) {    
    } 
}
function validateMaxLen(textID,maxlen,alertMsg)
{
    var textToVal = document.getElementById(textID).value;
    if (textToVal.length > maxlen)
    {
       alert(alertMsg);
       textToVal = textToVal.substring(0,maxlen - 1);
       document.getElementById(textID).value = textToVal;
    }
}

function ShowHideRating(objID)
{
if (document.getElementById(objID).style.display == "none")
	{ 	document.getElementById(objID).style.display = "block";
		
	}	
	else if(document.getElementById(objID).style.display == "block")
	{ document.getElementById(objID).style.display = "none";
	} 

}
function getdateBestBuy()
{var today = new Date();
    var thisYear = today.getFullYear();
    var minstartDate = "01/01/" + thisYear;
    var startdate = new Date(getDateObject(minstartDate,"/"));
    return startdate;
}

function getbegindateBestBuy()
{
var startdate = new Date(getDateObject(document.getElementById("ctl00_ContentHolder_txtScheduleWInBegin").value,"/"));
return startdate;
}
function GetFromDate(sender,args)
{

var stDate=new Date();
stDate=sender._selectedDate;
var cMonth=stDate.getMonth()+1;
if (cMonth<10)
cMonth='0'+cMonth;
var clienSelectedDate=stDate.getDate() + '/'+ cMonth +'/' + stDate.getFullYear();
document.getElementById("ctl00_ContentHolder_hdnStartDate").value=clienSelectedDate;
}
function GetToDate(sender,args)
{
var stDate=new Date();
stDate=sender._selectedDate;
var cMonth=stDate.getMonth()+1;
if (cMonth<10)
cMonth='0'+cMonth;
var clienSelectedDate=stDate.getDate() + '/'+ cMonth +'/' + stDate.getFullYear();
document.getElementById("ctl00_ContentHolder_hdnToDate").value=clienSelectedDate;
}

///*****************************************************Function For List Box Selection Starts Here *******************************************

function addToList(SourceListID, TargetListID, HiddenFieldID,Type)
{
    arr=new Array();	
    var opts = document.getElementById(SourceListID).options
    found = true
    while (found)
	{
	    found=false
	    for (j=0;j<opts.length;j++)
	    {
	        if (opts[j].selected==true)
        	{
	            moveListItem(j,opts[j],SourceListID, TargetListID)
	            found=true;
	        }
	    }
	}
    if(Type == 'Add')
    {    
      addToHiddenFromList(HiddenFieldID,TargetListID)		
    }
    else
    {    
       addToHiddenFromList(HiddenFieldID,SourceListID)		
    }        
}
	
function moveListItem(I,optsI,SourceListID, TargetListID)
{
    SourceList=document.getElementById(SourceListID);
    TargetList=document.getElementById(TargetListID);
    opt = new Option(optsI.text,optsI.value);
    TargetList.options[TargetList.options.length]=opt;
    SourceList.options[I]=null;
}
function addToHiddenFromList(HiddenFieldID, SourceListID)			
{
	SourceList=document.getElementById(SourceListID);
	HiddenField=document.getElementById(HiddenFieldID);
	HiddenField.value=""
	var opts = SourceList.options
	for (j=0;j<opts.length;j++)
	{
	    HiddenField.value = HiddenField.value + opts[j].value + ","		
	}
	if (opts.length!=0)
	{
	    HiddenField.value=HiddenField.value.substring(0,HiddenField.value.length-1)
	}
}
function ShowVettingListBox(TableID,Action)
{    

    if(Action == "Yes")
    {
     if(TableID == "CRB")
     {
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCRBListBox").style.display = "inline";
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCRBCheckCalender").style.display = "inline";
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCRBCheckFileUpload").style.display = "inline";
     }      
     else if(TableID == "CSCS")
     {      
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCSCSCalender").style.display = "inline";
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCSCSFileUpload").style.display = "inline";
     }  
     else if(TableID == "EmpLiability")
     {      
      document.getElementById("ctl00_ContentHolder_UCCompanyProfile1_divEmpLiability").style.display = "inline";      
     }
     else if(TableID == "PublicLiability")
     {      
      document.getElementById("ctl00_ContentHolder_UCCompanyProfile1_divPublicLiability").style.display = "inline";      
     }
     else if(TableID == "ProfIndemnity")
     {      
      document.getElementById("ctl00_ContentHolder_UCCompanyProfile1_divProfIndemnity").style.display = "inline";      
     }
      else if(TableID == "tblSecuCheckListbox")
     {      
      document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_tblSecuCheckListbox").style.display = "inline";      
      document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivUKSecuCalender").style.display = "inline";      
     }
       else if(TableID == "VAT")
        {      
            document.getElementById("ctl00_ContentHolder_UCCompanyProfile1_DivVat").style.display = "";      
        }
      else if(TableID == "DBS")
     {      
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCRBCertificate").style.display = "";  
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCRBCheckCalender").style.display = ""; 
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_tblCRBCheckFileUpload").style.display = "";  
      }
       else if(TableID == "UKSecurity")
        {      
            document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trUKSecurityList").style.display = "";      
            document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trUKSecurityFileUpload").style.display = "";      
        }
    }
    if(Action == "No")
    {
      
          if(TableID == "CRB")
         {
          document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCRBListBox").style.display = "none";
          document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCRBCheckCalender").style.display = "none";
          document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCRBCheckFileUpload").style.display = "none";
         }      
        else if(TableID == "CSCS")
         {      
          document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCSCSCalender").style.display = "none";
          document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCSCSFileUpload").style.display = "none";
         }  
        else if(TableID == "EmpLiability")
        {      
            document.getElementById("ctl00_ContentHolder_UCCompanyProfile1_divEmpLiability").style.display = "none";      
        }
        else if(TableID == "PublicLiability")
        {      
            document.getElementById("ctl00_ContentHolder_UCCompanyProfile1_divPublicLiability").style.display = "none";      
        }
        else if(TableID == "ProfIndemnity")
        {      
            document.getElementById("ctl00_ContentHolder_UCCompanyProfile1_divProfIndemnity").style.display = "none";      
        }
        else if(TableID == "tblSecuCheckListbox")
        {      
            document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_tblSecuCheckListbox").style.display = "none";      
            document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivUKSecuCalender").style.display = "none";      
        }
         else if(TableID == "VAT")
        {      
            document.getElementById("ctl00_ContentHolder_UCCompanyProfile1_DivVat").style.display = "none"; 
            document.getElementById("ctl00_ContentHolder_UCCompanyProfile1_divValidationComplainces").style.display = "none"; 
                                       
        }
         else if(TableID == "UKSecurity")
        {      
            document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trUKSecurityList").style.display = "none";      
            document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trUKSecurityFileUpload").style.display = "none";      
        }
        else if(TableID == "DBS")
     {      
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCRBCertificate").style.display = "none";  
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_trCRBCheckCalender").style.display = "none"; 
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_tblCRBCheckFileUpload").style.display = "none";  
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_divValidationVetting").style.display = "none";     
                     }
    }   
}
function ShowVettingListBoxForAdd(TableID,Action)
{    
    if(Action == "Yes")
    {
     if(TableID == "CRB")
     {
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm2_tabContainer_tabProfile_trCRBListBox").style.display = "inline";
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm2_tabContainer_tabProfile_trCRBCheckCalender").style.display = "inline";
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm2_tabContainer_tabProfile_trCRBCheckFileUpload").style.display = "inline";
     }      
     else if(TableID == "CSCS")
     {      
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm2_tabContainer_tabProfile_trCSCSCalender").style.display = "inline";
      document.getElementById("ctl00_ContentHolder_UCSpecialistsForm2_tabContainer_tabProfile_trCSCSFileUpload").style.display = "inline";
     }       
    }
    if(Action == "No")
    {
      
          if(TableID == "CRB")
         {
          document.getElementById("ctl00_ContentHolder_UCSpecialistsForm2_tabContainer_tabProfile_trCRBListBox").style.display = "none";
          document.getElementById("ctl00_ContentHolder_UCSpecialistsForm2_tabContainer_tabProfile_trCRBCheckCalender").style.display = "none";
          document.getElementById("ctl00_ContentHolder_UCSpecialistsForm2_tabContainer_tabProfile_trCRBCheckFileUpload").style.display = "none";
         }      
        else if(TableID == "CSCS")
         {      
          document.getElementById("ctl00_ContentHolder_UCSpecialistsForm2_tabContainer_tabProfile_trCSCSCalender").style.display = "none";
          document.getElementById("ctl00_ContentHolder_UCSpecialistsForm2_tabContainer_tabProfile_trCSCSFileUpload").style.display = "none";
         }         
    }

}

///*****************************************************Function For List Box Selection Ends Here *******************************************
var c=0;
var t;
var timer_is_on=0;
var tempvar1
var tempvar2 	
function ShowNotification(TimeOutTime,ShowNotification)
{
    if(ShowNotification == "1" && document.getElementById("ctl00_hdnRoleGroupID").value != "19")
    {    
        tempvar1 = TimeOutTime
        tempvar2 = ShowNotification        
        FetchNotification();
        c=c+1;
        t=setTimeout("ShowNotification(tempvar1,tempvar2)",tempvar1);
    }
}
function doTimer()
{
if (!timer_is_on)
  {
  timer_is_on=1;
  ShowNotification(tempvar1,tempvar2);
  }
}

function FetchNotification()
{
     $.ajax({
            type:"POST",
            //url:"Welcome.aspx/GetNotificationDetails",
            url:document.getElementById("hdnWebPath").value + "Welcome.aspx/GetNotificationDetails",
            data: "{'AlwaysDisplayLogin':'"+ document.getElementById("ctl00_hdnAlwaysDisplayLogin").value +"'}",
            //data: "{}",
            contentType: "application/json; charset=utf-8",    
            dataType:"json",        
            success:function(data)    
            {   
                var json = JSON.stringify(data);
                 var loop = data.d;
           $.each(loop.rows, function (i, rows) {                
                
                if (rows.cntMessage != '0')
                {                                                                              
                    if(rows.Type == 'WorkOrder')        
                    {     
                          if (rows.cntMessage == '1')
                          {
                            $('#DivWONotification').slideDown("slow");      
                            document.getElementById("lblWONotificationTitle").innerHTML = rows.Type;   
                            document.getElementById("lblWONotification").innerHTML = "New Workorder No. " + rows.WorkOrderID;      
                            document.getElementById("ancWONotification").href = document.getElementById("hdnWebPath").value + "SupplierWODetails.aspx?Viewer=Supplier&Group=InTray&sender=UCWOsListing&PS=10&PN=0&SC=DateStart&SO=1&WOID=" +  rows.encWOID + "&WorkOrderID=" +  rows.encWorkOrderID +  "&SupContactId=" +  rows.encSupContactId + "&SupCompId=" +  rows.encSupCompId                                    
                          }
                          else
                          {
                           $('#DivWONotification').slideDown("slow");      
                           document.getElementById("lblWONotificationTitle").innerHTML = rows.Type;   
                           document.getElementById("lblWONotification").innerHTML = rows.cntMessage + " " +rows.Message;
                           document.getElementById("ancWONotification").href =  document.getElementById("hdnWebPath").value + "SupplierWOListing.aspx?Group=InTray"                    
                          }
                     }  
                    else if(rows.Type == 'Message')        
                    {     
                          if (rows.cntMessage == '1')
                          {  
                            $('#DivMsgNotification').slideDown("slow");      
                            document.getElementById("lblMsgNotificationTitle").innerHTML = rows.Type;   
                            document.getElementById("lblMsgNotification").innerHTML =  rows.Message; 
                            document.getElementById("ancMsgNotification").href =  document.getElementById("hdnWebPath").value +  "ListMessages.aspx?MsgID=" + rows.encMsgID                         
                          }
                          else
                          {
                            $('#DivMsgNotification').slideDown("slow");      
                            document.getElementById("lblMsgNotificationTitle").innerHTML = rows.Type;   
                            document.getElementById("lblMsgNotification").innerHTML = rows.cntMessage + " " + rows.Message; 
                            document.getElementById("ancMsgNotification").href =  document.getElementById("hdnWebPath").value +  "ListMessages.aspx"   
                          }
                    }      
                    else if(rows.Type == 'Imp Message')        
                    {   
                          document.getElementById("DivLayer").style.width = "100%";                                           
                          if (rows.cntMessage == '1')
                          {
                            $('#DivImpMsgNotification').slideDown("slow");      
                            document.getElementById("lblImpMsgNotificationTitle").innerHTML = rows.Type;   
                            document.getElementById("lblImpMsgNotification").innerHTML = "<img src='../OWLibrary/Images/Icons/Validation-Alert.gif' alt='Imp Message' title='Imp Message' border='0' style='float:left;'/> <div style='float:left;padding-top:10px;padding-left:10px;'>" + rows.Message + "</div>"; 
                            document.getElementById("ancImpMsgNotification").href =  document.getElementById("hdnWebPath").value +  "ListMessages.aspx?MsgID=" + rows.encImpMsgID  
                          }
                          else
                          {
                            $('#DivImpMsgNotification').slideDown("slow");      
                            document.getElementById("lblImpMsgNotificationTitle").innerHTML = rows.Type;   
                            document.getElementById("lblImpMsgNotification").innerHTML = "<img src='../OWLibrary/Images/Icons/Validation-Alert.gif' alt='Imp Message' title='Imp Message' border='0' style='float:left;'/> <div style='float:left;padding-top:10px;padding-left:10px;'>" + rows.cntMessage + " " + rows.Message + "</div>"; 
                            document.getElementById("ancImpMsgNotification").href =  document.getElementById("hdnWebPath").value +  "ListMessages.aspx"   
                          }
                    }   
                }
          
               });
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) 
	        {            
		        //var jsonError = JSON.parse(XMLHttpRequest.responseText);            
		       // alert(XMLHttpRequest.responseText);
	        }});	        

}
function ReplaceAll(Source,stringToFind,stringToReplace){

  var temp = Source;

    var index = temp.indexOf(stringToFind);

        while(index != -1){

            temp = temp.replace(stringToFind,stringToReplace);

            index = temp.indexOf(stringToFind);

        }

        return temp;

}
 function CheckOWSpecialInstruction(Id)
 {
     if(document.getElementById(Id).value != "")
    {
        createCustomAlert("Are you sure you want to notify Orderwork with these instructions?",Id)	    
        removeHTML(Id); 
        validateMaxLen(Id, '700', 'Special instructions to Orderwork can have max 700 characters.');
    }
 } 

// constants to define the title of the alert and button text.
var ALERT_TITLE = "OrderWork Confirmation";
var ALERT_BUTTON_YES_TEXT = "Yes";
var ALERT_BUTTON_NO_TEXT = "NO";

// over-ride the alert method only if this a newer browser.
function createCustomAlert(txt,Id) {
	// shortcut reference to the document object
	d = document;

	// if the modalContainer object already exists in the DOM, bail out.
	if(d.getElementById("modalContainer")) return;

	// create the modalContainer div as a child of the BODY element
	mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
	mObj.id = "modalContainer";
	 // make sure its as tall as it needs to be to overlay all the content on the page
	mObj.style.height = document.documentElement.scrollHeight + "px";

	// create the DIV that will be the alert 
	alertObj = mObj.appendChild(d.createElement("div"));
	alertObj.id = "alertBox";
	// MSIE doesnt treat position:fixed correctly, so this compensates for positioning the alert
	if(d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
	// center the alert box
	alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth)/2 + "px";
    alertObj.style.top = "550px";

	// create an H1 element as the title bar
	h1 = alertObj.appendChild(d.createElement("h1"));
	h1.appendChild(d.createTextNode(ALERT_TITLE));

	// create a paragraph element to contain the txt argument
	msg = alertObj.appendChild(d.createElement("p"));
	if (msg.innerHTML == "")
	msg.innerHTML = txt;
	else
	msg.innerHTML = "<br>" + txt;
	
    // create an anchor element to use as the confirmation button.
	btn = alertObj.appendChild(d.createElement("a"));
	btn.id = "yesBtn";
	btn.appendChild(d.createTextNode(ALERT_BUTTON_YES_TEXT));
	btn.href = "#";
	// set up the onclick event to remove the alert when the anchor is clicked
	btn.onclick = function() { YesAlert();return false; }	

	// create an anchor element to use as the confirmation button.
	btn = alertObj.appendChild(d.createElement("a"));
	btn.id = "noBtn";
	btn.appendChild(d.createTextNode(ALERT_BUTTON_NO_TEXT));
	btn.href = "#";
	// set up the onclick event to remove the alert when the anchor is clicked
	btn.onclick = function() { NoAlert(Id);return false; }	
}
// Yes action and then removes the custom alert from the DOM
function YesAlert() {
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
}

// removes the custom alert from the DOM
function NoAlert(Id) {
     document.getElementById(Id).value = "";
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
}

function CloseNotification(Type)
{

document.getElementById("ctl00_hdnAlwaysDisplayLogin").value = "0"

 if(Type == 'WorkOrder')
 {
   $('#DivWONotification').slideUp('slow');
 }
 else if(Type == 'Message')        
 {
  $('#DivMsgNotification').slideUp('slow');  
 }       
 else if(Type == 'ImpMessage')        
 {
  $('#DivImpMsgNotification').slideUp('slow'); 
  document.getElementById("DivLayer").style.width = "0%";
 }
}

///*****************************************************Function For Tabular AOE selection Starts Here *******************************************

function CheckAOEMainCat(AOEDivId, hdnCombIDs) {
    var objId = AOEDivId.replace("AOE", "");
    var strIDs = document.getElementById(hdnCombIDs).value;
    var SelSubCatCombId = document.getElementById("hdnSelSubCatCombId" + objId).value;
    var SubCatOfMainCat = document.getElementById("hdnSubCatOfMainCat" + objId).value;
    SelSubCatCombId = SelSubCatCombId.replace("," + objId, "");
    if (document.getElementById(AOEDivId).className == "clsNotSelectedSkillLeftDiv") {
        if (strIDs != "")
        { strIDs = strIDs + "," }
        strIDs = strIDs + objId

        var items=SubCatOfMainCat.split(","); 
        for (var i = 0; i < items.length; i++) 
        {
          var item = items[i];          
          if (strIDs != "")
          {
            if (strIDs.indexOf("," + item)  == -1)
            {
               strIDs = strIDs + "," + item
               if (SelSubCatCombId != "")
              { SelSubCatCombId = SelSubCatCombId + "," }
                SelSubCatCombId = SelSubCatCombId + item
            }
          }
          else
          {
            strIDs = strIDs + item
          }
          document.getElementById("AOE" + item).className = "clsSelectedSkillDiv";
        }

        document.getElementById(AOEDivId).className = "clsSelectedSkillLeftDiv";
    }
    else {
      //  if(SelSubCatCombId == "0")
       // {
           var str = "," + objId;
           strIDs = strIDs.replace(str, "")
           
            var items=SubCatOfMainCat.split(","); 
            for (var i = 0; i < items.length; i++) 
            {
              var item = items[i];
              
                str = "," + item;
                strIDs = strIDs.replace(str, "")   
                
                  var strSelSubCatCombId = "," + item;
                 SelSubCatCombId = SelSubCatCombId.replace(strSelSubCatCombId, "");      

              document.getElementById("AOE" + item).className = "clsNotSelectedSkillDiv";
            }

           document.getElementById(AOEDivId).className = "clsNotSelectedSkillLeftDiv";
        //}
       
    }
    document.getElementById(hdnCombIDs).value = strIDs;       
    document.getElementById("hdnSelSubCatCombId" + objId).value = SelSubCatCombId; 
}

function CheckAOESubCat(AOEDivId, hdnCombIDs) {
    var objId = AOEDivId.replace("AOE", "");
    var MainCatCombId = document.getElementById("hdnMainCatCombId" + objId).value;
    var SelSubCatCombId = document.getElementById("hdnSelSubCatCombId" + MainCatCombId).value;
    SelSubCatCombId = SelSubCatCombId.replace("," + MainCatCombId, "")
    var strIDs = document.getElementById(hdnCombIDs).value;
   if (document.getElementById(AOEDivId).className == "clsNotSelectedSkillDiv") {
        if (strIDs != "")
        { 
         strIDs = strIDs.replace("," + MainCatCombId, "")
         strIDs = strIDs + "," 
        }
        strIDs = strIDs + objId
        strIDs = strIDs + "," + MainCatCombId

        if (SelSubCatCombId != "")
        { SelSubCatCombId = SelSubCatCombId + "," }
        SelSubCatCombId = SelSubCatCombId + objId

        document.getElementById(AOEDivId).className = "clsSelectedSkillDiv";
        document.getElementById("AOE" + MainCatCombId).className = "clsSelectedSkillLeftDiv";
        
    }
    else {
        var str = "," + objId;
        strIDs = strIDs.replace(str, "")
        
        var strSelSubCatCombId = "," + objId;
        SelSubCatCombId = SelSubCatCombId.replace(strSelSubCatCombId, "");
        //alert(SelSubCatCombId);
        if(SelSubCatCombId == "0")
        {
            document.getElementById("AOE" + MainCatCombId).className = "clsNotSelectedSkillLeftDiv";
            var str = "," + MainCatCombId;
            strIDs = strIDs.replace(str, "")
        }

       

        document.getElementById(AOEDivId).className = "clsNotSelectedSkillDiv";
        
    }
    document.getElementById(hdnCombIDs).value = strIDs;  
    document.getElementById("hdnSelSubCatCombId" + MainCatCombId).value = SelSubCatCombId; 
   // alert(document.getElementById(hdnCombIDs).value);
}



///*****************************************************Function For Tabular AOE selection Ends Here *******************************************

///*****************************************************Function For Company Profile Redesign starts Here *******************************************

function ShowHideTabs(TabName,PageName) {
        HideAllTabs(PageName);
        if (TabName == 'Profile') {
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelProfile').style.display = 'block';
            $("#divTabProfile").addClass('clsInnerTabStyleSecurePageSelectedTab');
        }
        else if (TabName == 'Finance') {
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_txtSortCode').value = "";
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_txtAccountNo').value = "";
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_txtAccountName').value = "";

            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelFinance').style.display = 'block';
            $("#divTabFinance").addClass('clsInnerTabStyleSecurePageSelectedTab');

            
        }
        else if (TabName == 'References') {
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelReferences').style.display = 'block';
            $("#divTabReferences").addClass('clsInnerTabStyleSecurePageSelectedTab');
        }
        else if (TabName == 'Complainces') {
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelComplainces').style.display = 'block';
            $("#divTabComplainces").addClass('clsInnerTabStyleSecurePageSelectedTab');
        }
        else if (TabName == 'Insurance') {
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelInsurance').style.display = 'block';
            $("#divTabInsurance").addClass('clsInnerTabStyleSecurePageSelectedTab');
        }
         //OA-622
        else if (TabName == 'Payment') {
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelPayment').style.display = 'block';
            $("#divTabPayment").addClass('clsInnerTabStyleSecurePageSelectedTab');
        }
         else if (TabName == 'PersonalInfo') {
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelPersonalInfo').style.display = 'block';
            $("#divTabPersonalInfo").addClass('clsInnerTabStyleSecurePageSelectedTab');
//            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_lblEmail').innerHTML = document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_lblNewEmail').innerHTML;       
//            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_txtUserEmail').value = document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_lblNewEmail').innerHTML;       
            
        }
        else if (TabName == 'UserSetting') {
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelUserSetting').style.display = 'block';
            $("#divTabUserSetting").addClass('clsInnerTabStyleSecurePageSelectedTab');
        }
        else if (TabName == 'LoginInfo') {
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelLoginInfo').style.display = 'block';
            $("#divTabLoginInfo").addClass('clsInnerTabStyleSecurePageSelectedTab');
        }
        else if (TabName == 'Locations') {
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelLocations').style.display = 'block';
            $("#divTabLocations").addClass('clsInnerTabStyleSecurePageSelectedTab');
        }
        else if (TabName == 'Vetting') {
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelVetting').style.display = 'block';
            $("#divTabVetting").addClass('clsInnerTabStyleSecurePageSelectedTab');
         }
        else if (TabName == 'Rates') {
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelRates').style.display = 'block';
            $("#divTabRates").addClass('clsInnerTabStyleSecurePageSelectedTab');
        }
    }
    function HideAllTabs(PageName) {
        if(PageName == 'Company')
        {
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelProfile').style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelFinance').style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelReferences').style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelComplainces').style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelInsurance').style.display = 'none';
           //OA-622
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_PanelPayment').style.display = 'none';

            $("#divTabProfile").addClass('clsInnerTabSecurePageStyleTab');
            $("#divTabFinance").addClass('clsInnerTabSecurePageStyleTab');
            $("#divTabReferences").addClass('clsInnerTabSecurePageStyleTab');
            $("#divTabComplainces").addClass('clsInnerTabSecurePageStyleTab');
            $("#divTabInsurance").addClass('clsInnerTabSecurePageStyleTab');
           //OA-622
            $("#divTabPayment").addClass('clsInnerTabSecurePageStyleTab');

            $("#divTabProfile").removeClass('clsInnerTabStyleSecurePageSelectedTab');
            $("#divTabFinance").removeClass('clsInnerTabStyleSecurePageSelectedTab');
            $("#divTabReferences").removeClass('clsInnerTabStyleSecurePageSelectedTab');
            $("#divTabComplainces").removeClass('clsInnerTabStyleSecurePageSelectedTab');
            $("#divTabInsurance").removeClass('clsInnerTabStyleSecurePageSelectedTab');
            //OA-622
             $("#divTabPayment").removeClass('clsInnerTabStyleSecurePageSelectedTab');
         }
         else if(PageName == 'User')
        {
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelPersonalInfo').style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelUserSetting').style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelLoginInfo').style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelLocations').style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelVetting').style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_PanelRates').style.display = 'none';

            $("#divTabPersonalInfo").addClass('clsInnerTabSecurePageStyleTab');
            $("#divTabUserSetting").addClass('clsInnerTabSecurePageStyleTab');
            $("#divTabLoginInfo").addClass('clsInnerTabSecurePageStyleTab');
            $("#divTabLocations").addClass('clsInnerTabSecurePageStyleTab');
            $("#divTabVetting").addClass('clsInnerTabSecurePageStyleTab');
            $("#divTabRates").addClass('clsInnerTabSecurePageStyleTab');

            $("#divTabPersonalInfo").removeClass('clsInnerTabStyleSecurePageSelectedTab');
            $("#divTabUserSetting").removeClass('clsInnerTabStyleSecurePageSelectedTab');
            $("#divTabLoginInfo").removeClass('clsInnerTabStyleSecurePageSelectedTab');
            $("#divTabLocations").removeClass('clsInnerTabStyleSecurePageSelectedTab');
            $("#divTabVetting").removeClass('clsInnerTabStyleSecurePageSelectedTab');
            $("#divTabRates").removeClass('clsInnerTabStyleSecurePageSelectedTab');
        }
    }

    function ShowHideTabAction(TabName, Action,PageName) {
        if(PageName == 'Company')
            {
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_divEdit' + TabName).style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_divView' + TabName).style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_ancEdit' + TabName).style.display = 'none';

            if(TabName=='Finance')
            {
              document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_txtSortCode').value = "";
              document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_txtAccountNo').value = "";
              document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_txtAccountName').value = "";
              }

            if (Action == 'Edit') {
                document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_divEdit' + TabName).style.display = 'block';
            }
            else {
                document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_divView' + TabName).style.display = 'block';
                document.getElementById('ctl00_ContentHolder_UCCompanyProfile1_ancEdit' + TabName).style.display = 'inline';

              
            }
        }
        else if(PageName == 'User')
        {
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_divEdit' + TabName).style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_divView' + TabName).style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_ancEdit' + TabName).style.display = 'none';
            if (Action == 'Edit') {
                document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_divEdit' + TabName).style.display = 'block';
            }
            else {
                document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_divView' + TabName).style.display = 'block';
                document.getElementById('ctl00_ContentHolder_UCSpecialistsForm1_ancEdit' + TabName).style.display = 'inline';
            }
        }
         else if(PageName == 'Location')
        {          
            document.getElementById('ctl00_ContentHolder_UCLocationForm1_divEdit' + TabName).style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCLocationForm1_divView' + TabName).style.display = 'none';
            document.getElementById('ctl00_ContentHolder_UCLocationForm1_ancEdit' + TabName).style.display = 'none';
            if (Action == 'Edit') {
                document.getElementById('ctl00_ContentHolder_UCLocationForm1_divEdit' + TabName).style.display = 'block';
            }
            else {
                document.getElementById('ctl00_ContentHolder_UCLocationForm1_divView' + TabName).style.display = 'block';
                document.getElementById('ctl00_ContentHolder_UCLocationForm1_ancEdit' + TabName).style.display = 'inline';
            }
        }
    }   

///*****************************************************Function For Company Profile Redesign  Ends Here *******************************************

///*****************************************************Function For Set Dates Starts Here *******************************************

function SetDates(Mode) 
{

    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSetDatesMode").value = Mode;
    //alert(Mode);
    //alert(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSetDatesMode").value);
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_btnSetDates").click();
    
}

///*****************************************************Function For Set Dates Ends Here *******************************************

//*******************************************************AT800BookingForm***********************************************************

function getProductDescriptionAT800(myval,HiddenWoTitle,HiddenScopeOfWork)
{      
    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSelectedService").value = parseInt(myval);
    if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divService").className == "displayBlock" && myval != "0") // && myval != "1" )
    {
      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divService").className = "displayNone";
      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divCompForm").className = "displayBlock";
      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct").value = myval;      
    }
    
    if (myval == "0")
    {removeProductInfo("Service");}
	else if(myval == "1")
    {removeProductInfo("Quote");}
	else
	{
         if(HiddenWoTitle != "")
          {
              var i;
              var ISItemExists;
              ISItemExists = 0
              for (i = document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct").length - 1; i>=0; i--) 
              {             
              if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct").options[i].value == myval) 
                {ISItemExists = 1}
              }          
              if(ISItemExists == 0)
              { 
               
                if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnPrevHiddenProductId").value != "")
                removeOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct"),document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnPrevHiddenProductId").value);
               
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnPrevHiddenProductId").value = myval;      
                addOption(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct"),HiddenWoTitle,myval);
              }            
          }
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnProduct").value = parseInt(myval);
        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSelectedService").value = parseInt(myval);
	    $.ajax({
        type:"POST",
        url:"AT800BookingForm.aspx/GetProductDetails",
        data: "{'productid':'"+myval+"'}",
        contentType: "application/json; charset=utf-8",    
        dataType:"json",        
        success:function(data)    
        {var json = JSON.stringify(data);
            var loop = data.d;
           $.each(loop.rows, function (i, rows) {
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnProductId").value = parseInt(rows.WOID);            
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnbtnTrigger").click(); 
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").value = rows.WOTitle;
              if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "IT")
        {
            if (rows.IsSignOffSheetReqd == true )
            { 
              document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_chkForceSignOff").checked = true;
                }
            else
            {
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_chkForceSignOff").checked = false;
            }
         }
            if (rows.ReviewBids == true )
            { 
              document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnReviewBids").value = true;
            }
            else
            {
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnReviewBids").value = false;
            }    
            //alert(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnReviewBids").value);
            //alert(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle"));
           document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlScopeWork").style.display = "block";
           document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlClientScope").style.display = "none";
           //alert(rows.WOLongDesc);
           document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").value = rows.WOLongDesc.replace(/<BR>/g,"");
           ValidatorEnable(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rqdScopeOfWork"),false);
            if (rows.ClientScope != "")
                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlClientScope").style.display = "block";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlScopeWork").style.display = "none";
                if(HiddenScopeOfWork != "")
                {
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value = HiddenScopeOfWork.replace(/<BR>/g,"") + rows.ClientScope.replace(/<BR>/g,"");
                }
                else
                {
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").value = rows.ClientScope.replace(/<BR>/g,"");
                }
                ValidatorEnable(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rqdScopeOfWork"),true);}            
            //document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtJRSNumber").value = rows.JRSNumber;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpecialInstructions").value = rows.SpecialInstructions.replace(/<BR>/g,"");                    
//            if (parseInt(rows.LocationID) > 0)
//                {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnBillingLocation").value = parseInt(rows.LocationID);}
                //document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnBillingLocation").disabled = true;}
           // document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").value = rows.WholesalePrice;            
            if (rows.CombId != 0)
            {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").value = parseInt(rows.CombId);
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").disabled = false;
            clearOptions(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType"));
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = "";
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = "";
            populateSubCat(parseInt(rows.CombId));
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").value = parseInt(rows.WOCategoryID);
            
            if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType") != null)
                { 
                onSubCatChange("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType");
            }
            }
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtScheduleWInBegin").value = rows.DeferredDate;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnStartDate").value = rows.DeferredDate;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnPlatformPrice").value = rows.PlatformPrice;
            //document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_pnlProposedPrice").disabled = true;
            //document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtSpendLimit").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOType").disabled = true;
//            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").disabled = true;
//            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdoNon0Value").disabled = true;
//            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdoNon0Value").checked = true;
//            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_rdo0Value").checked = false;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_ddlWOSubType").disabled = true;
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnClientStrtDate").value="";
            //document.getElementById("tdRadioButtonPrice").style.display="block";
            //document.getElementById("tdTextBoxPrice").style.display="block";            
            if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc") == "[object]") 
             {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = true;}
            document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtTitle").disabled = true;
            if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope") == "[object]") 
             {document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").disabled = true;}
            if (rows.FreesatService == true && document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
            {
                if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1") != null)
                { 
                    if(rows.DefaultGoodsLocation != 1)
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").value = rows.DefaultGoodsLocation
                    else
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").selectedIndex = 1
                        document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").disabled = true;
                }                
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display = "block";          
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "block";
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_chkForceSignOff").style.display = "none";
                 document.getElementById("divchkForceSignOff").style.display = "none";
                populateFreesatMake();
                  
            }
            else
            {        
               if(document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1") != null)
               { 
                 if(rows.DefaultGoodsLocation != 1)
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").value = rows.DefaultGoodsLocation
                 else
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").selectedIndex = 0
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_drpdwnGoodsCollection1").disabled = false;        
               }
                
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMake").style.display = "none";          
                document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "none";
           
            }

                 if (document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBusinessArea").value == "IT")
                 {
                      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtClientScope").disabled = true;
                      document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_txtWOLongDesc").disabled = true;
                 }    
                  
                  if (rows.TimeBuilderQuestions > 0)             
                  {
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBtnShowHideMdlTBQuestions").click();
                  }
                  else
                  {
                    if (rows.ServiceQuestion == 1)             
                    document.getElementById("ctl00_ContentHolder_UCCreateWorkOrderUK1_hdnBtnShowHideMdlQuestions").click();
                  }                
                 
           });                 
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) 
	    {            
		    var jsonError = JSON.parse(XMLHttpRequest.responseText);            
		    alert(JSON.stringify(jsonError));
	    }});
	}
}

//**********************************************************************************************************************************
function makeUppercase(ID) {
    var x = ID;
    var text = document.getElementById(ID).value.toUpperCase(); 
   document.getElementById(ID).value = text;
   }