﻿// JScript File


var selectedTab = "";

function LinkActivate() 
{
	var pagename = document.URL;
	pagename = pagename.toLowerCase();
	
	if(pagename.indexOf("privacypolicy") != -1)	
		selectedTab = "Privacy"
	
}


function showHide(newTab, height)
{   //alert(" newTab =  " + newTab)
    
    
	var pagename = document.URL;
	pagename = pagename.toLowerCase();
		
	
	if(document.getElementById("div"+newTab)) 
	{   
		if(selectedTab != newTab) 
		{   //alert("selectedTab1 = " + selectedTab)
			resetTabs();	
			//hide previously selected tab
			if(document.getElementById("div"+selectedTab)) 
			{
				document.getElementById("div"+selectedTab).style.visibility = "hidden";
				document.getElementById("div"+selectedTab).style.position = "absolute";
				document.getElementById("div"+selectedTab).style.overflow = "hidden";
				document.getElementById("div"+selectedTab).style.width = "0px";
				document.getElementById("div"+selectedTab).style.height = "0px";				
				document.getElementById("tdtab"+selectedTab).style.cursor = "hand";
				document.getElementById("tdtab"+selectedTab).style.cursor = "pointer";				
			}
			//show selected tab
			document.getElementById("div"+newTab).style.visibility = "visible";
			document.getElementById("div"+newTab).style.position = "relative";
			document.getElementById("div"+newTab).style.height = height;
			//document.getElementById("div"+newTab).style.width = "100%";
			if(!document.all)//for Firefox 
			{ //alert("For FireFox")
				if (pagename.indexOf("spprivacypolicy") != -1)
				    //The specific width is assigened to the page coz it dont have QL region.
					document.getElementById("div"+newTab).style.width = "905px";
				else if (pagename.indexOf("forgotpassword") != -1)
					document.getElementById("div"+newTab).style.width = "318px";
				else
					document.getElementById("div"+newTab).style.width = "680px";
			}
            else
               document.getElementById("div"+newTab).style.width = "100%";
            
            document.getElementById("div"+newTab).style.overflow = "visible";
			document.getElementById("tdtab"+newTab).onmouseout = dummy;
			document.getElementById("tdtab"+newTab).onmouseover =  dummy;
			selectTab(newTab);
			document.getElementById("tdtab"+newTab).style.cursor = "default";
			selectedTab = newTab;
		}		
		//To set the initial position of cursor on first boxes
		 
	
	}
}

function resetTabs() 
{
	var objs = document.getElementsByTagName("td");
	var ObjId;
	var lnkid;
	for (i = 0; i < objs.length; i++) 
	{
		if(objs[i].id.indexOf("tab") != -1)	
		{
			lnkid = objs[i].id;
			lnkid = lnkid.replace("tdtab","");			
			deselectTab(lnkid);
			
		}
	}
}


function selectTab(obj)
{	
	var imgsrc = document.getElementById("imgtab"+obj+"Left").src
	var imgPath = imgsrc.substring(0,imgsrc.indexOf("Images/") + 7)
		
	if (document.getElementById("tdtab"+obj).className.indexOf("Red") != -1)
		{colorName = "Red"}
	
		
	if(obj != null) 
	{
		if(selectedTab != obj)
		{ 
			document.getElementById("tdtab"+obj).className="ActiveLink" + colorName;
			document.getElementById("imgtab"+obj+"Left").src= imgPath + "Curves/" + colorName + "Tab-Left.gif";
			document.getElementById("imgtab"+obj+"Right").src= imgPath + "Curves/" + colorName + "Tab-Right.gif";
		}			
	}			
}


function deselectTab(obj)
{   	
	var imgsrc = document.getElementById("imgtab"+obj+"Left").src
	var imgPath = imgsrc.substring(0,imgsrc.indexOf("Images/") + 7)
	
	
	if (document.getElementById("tdtab"+obj).className.indexOf("Red") != -1)
	{
		colorName = "Red"
		curve="S"
	}
	else if (document.getElementById("tdtab"+obj).className.indexOf("Orange") != -1)
	{
		colorName = "Orange"
		curve = "C"
	}
	if(obj != null) 
	{
		document.getElementById("tdtab"+obj).className="NonActiveLink" + colorName;
		document.getElementById("imgtab"+obj+"Left").src= imgPath + "Curves/GreyTab-Left-" + curve + ".gif";
		document.getElementById("imgtab"+obj+"Right").src=imgPath + "Curves/GreyTab-Right-" + curve + ".gif";
	}
}

function dummy(){}
//Function for showing and hiding of Tabs in Buyer WOdetails and Supplier WODetails

function showhidetabWOdetails(objid,calledby,divName)
{
      var newdetails = objid.replace(calledby,divName) ;
	  //if visibility of div is hidden then show the div			  			  
	  hideAllTabWODetails();
	  document.getElementById(newdetails).className = "displayBlock";
	  //Highlighting header 
	  document.getElementById(objid).className = "headerTabHighLighted";
}
//Function for rollover images
function rollovertabWOdetails(objid)
{
      //Normalise all the tabs      
	  normalTabWODetails();
	  //Highlight the tab on whom the mouse over is done
	  document.getElementById(objid).className = "headerTabHighLighted";	    
}
function hideAllTabWODetails()
{
      document.getElementById("ctl00_ContentHolder_divOverview").className = "displayNone";
      document.getElementById("ctl00_ContentHolder_divSupplierInfo").className = "displayNone";
      if (document.getElementById("ctl00_ContentHolder_divHistoryHeader") != null)
      {
         document.getElementById("ctl00_ContentHolder_divHistory").className = "displayNone";
         document.getElementById("ctl00_ContentHolder_divHistoryHeader").className = "headerTabNormal";
      }
      document.getElementById("ctl00_ContentHolder_divAddnlInfo").className = "displayNone";      
      document.getElementById("ctl00_ContentHolder_divOverviewHeader").className = "headerTabNormal";
      document.getElementById("ctl00_ContentHolder_divSupplierInfoHeader").className = "headerTabNormal";      
      document.getElementById("ctl00_ContentHolder_divAddnlInfoHeader").className = "headerTabNormal";
}
function normalTabWODetails()
{           
      document.getElementById("ctl00_ContentHolder_divOverviewHeader").className = "headerTabNormal";
      document.getElementById("ctl00_ContentHolder_divSupplierInfoHeader").className = "headerTabNormal";
      if (document.getElementById("ctl00_ContentHolder_divHistoryHeader") != null)
      {
      document.getElementById("ctl00_ContentHolder_divHistoryHeader").className = "headerTabNormal";
      if (document.getElementById("ctl00_ContentHolder_divHistory").className == "displayBlock")
	        document.getElementById("ctl00_ContentHolder_divHistoryHeader").className = "headerTabHighLighted";
      }
      document.getElementById("ctl00_ContentHolder_divAddnlInfoHeader").className = "headerTabNormal";
      if (document.getElementById("ctl00_ContentHolder_divOverview").className == "displayBlock")
	    document.getElementById("ctl00_ContentHolder_divOverviewHeader").className = "headerTabHighLighted";
	  if (document.getElementById("ctl00_ContentHolder_divSupplierInfo").className == "displayBlock")
	    document.getElementById("ctl00_ContentHolder_divSupplierInfoHeader").className = "headerTabHighLighted";	  
	  if (document.getElementById("ctl00_ContentHolder_divAddnlInfo").className == "displayBlock")
	    document.getElementById("ctl00_ContentHolder_divAddnlInfoHeader").className = "headerTabHighLighted";
}

function showhidetabCommonWOdetails(objid,calledby,divName)
{
      var newdetails = objid.replace(calledby,divName) ;
	  //if visibility of div is hidden then show the div			  			  
	  hideAllTabCommonWODetails();
	  document.getElementById(newdetails).className = "displayBlock";
	  //Highlighting header 
	  document.getElementById(objid).className = "headerTabCommonWODetHighLighted";
}
//Function for rollover images
function rollovertabCommonWOdetails(objid)
{
      //Normalise all the tabs      
	  normalTabCommonWODetails();
	  //Highlight the tab on whom the mouse over is done
	  document.getElementById(objid).className = "headerTabCommonWODetHighLighted";	    
}
function hideAllTabCommonWODetails()
{      
      if (document.getElementById("ctl00_ContentHolder_divSupplierInfoHeader") != null)
      {
        document.getElementById("ctl00_ContentHolder_divSupplierInfo").className = "displayNone";
        document.getElementById("ctl00_ContentHolder_divSupplierInfoHeader").className = "headerTabCommonWODet";    
      }
      if (document.getElementById("ctl00_ContentHolder_divCAHeader") != null)
      {
        document.getElementById("ctl00_ContentHolder_divCA").className = "displayNone";
        document.getElementById("ctl00_ContentHolder_divCAHeader").className = "headerTabCommonWODet";    
      }
      if (document.getElementById("ctl00_ContentHolder_divHistoryHeader") != null)
      {
         document.getElementById("ctl00_ContentHolder_divHistory").className = "displayNone";
         document.getElementById("ctl00_ContentHolder_divHistoryHeader").className = "headerTabCommonWODet";
      }
      if (document.getElementById("ctl00_ContentHolder_divResponseHeader") != null)
      {
         document.getElementById("ctl00_ContentHolder_divResponse").className = "displayNone";
         document.getElementById("ctl00_ContentHolder_divResponseHeader").className = "headerTabCommonWODet";
      }
      if (document.getElementById("ctl00_ContentHolder_divProductInfoHeader") != null)
      {
         document.getElementById("ctl00_ContentHolder_divProductInfo").className = "displayNone";
         document.getElementById("ctl00_ContentHolder_divProductInfoHeader").className = "headerTabCommonWODet";
      }
      if (document.getElementById("ctl00_ContentHolder_divNotesHeader") != null)
      {
         document.getElementById("ctl00_ContentHolder_divNotes").className = "displayNone";
         document.getElementById("ctl00_ContentHolder_divNotesHeader").className = "headerTabCommonWODet";
      }      
      document.getElementById("ctl00_ContentHolder_divOverview").className = "displayNone";
      document.getElementById("ctl00_ContentHolder_divAddnlInfo").className = "displayNone";      
      document.getElementById("ctl00_ContentHolder_divOverviewHeader").className = "headerTabCommonWODet";       
      document.getElementById("ctl00_ContentHolder_divAddnlInfoHeader").className = "headerTabCommonWODet";

}
function normalTabCommonWODetails()
{           
      document.getElementById("ctl00_ContentHolder_divOverviewHeader").className = "headerTabCommonWODet";
      document.getElementById("ctl00_ContentHolder_divAddnlInfoHeader").className = "headerTabCommonWODet";
      
      if (document.getElementById("ctl00_ContentHolder_divSupplierInfoHeader") != null)
      {
        document.getElementById("ctl00_ContentHolder_divSupplierInfoHeader").className = "headerTabCommonWODet";
        if (document.getElementById("ctl00_ContentHolder_divSupplierInfo").className == "displayBlock")
	        document.getElementById("ctl00_ContentHolder_divSupplierInfoHeader").className = "headerTabCommonWODetHighLighted";
      }
      if (document.getElementById("ctl00_ContentHolder_divHistoryHeader") != null)
      {
        document.getElementById("ctl00_ContentHolder_divHistoryHeader").className = "headerTabCommonWODet";
        if (document.getElementById("ctl00_ContentHolder_divHistory").className == "displayBlock")
	        document.getElementById("ctl00_ContentHolder_divHistoryHeader").className = "headerTabCommonWODetHighLighted";
      }
      if (document.getElementById("ctl00_ContentHolder_divProductInfoHeader") != null)
      {
        document.getElementById("ctl00_ContentHolder_divProductInfoHeader").className = "headerTabCommonWODet";
	      if (document.getElementById("ctl00_ContentHolder_divProductInfo").className == "displayBlock")
	        document.getElementById("ctl00_ContentHolder_divProductInfoHeader").className = "headerTabCommonWODetHighLighted";
      }
       if (document.getElementById("ctl00_ContentHolder_divCAHeader") != null)
      {
        document.getElementById("ctl00_ContentHolder_divCAHeader").className = "headerTabCommonWODet";
        if (document.getElementById("ctl00_ContentHolder_divCA").className == "displayBlock")
	        document.getElementById("ctl00_ContentHolder_divCAHeader").className = "headerTabCommonWODetHighLighted";
      }
       if (document.getElementById("ctl00_ContentHolder_divNotesHeader") != null)
      {
        document.getElementById("ctl00_ContentHolder_divNotesHeader").className = "headerTabCommonWODet";
        if (document.getElementById("ctl00_ContentHolder_divNotes").className == "displayBlock")
	        document.getElementById("ctl00_ContentHolder_divNotesHeader").className = "headerTabCommonWODetHighLighted";
      }      
      if (document.getElementById("ctl00_ContentHolder_divOverview").className == "displayBlock")
	    document.getElementById("ctl00_ContentHolder_divOverviewHeader").className = "headerTabCommonWODetHighLighted";	 	  
	  if (document.getElementById("ctl00_ContentHolder_divAddnlInfo").className == "displayBlock")
	    document.getElementById("ctl00_ContentHolder_divAddnlInfoHeader").className = "headerTabCommonWODetHighLighted";
	    
	    if (document.getElementById("ctl00_ContentHolder_divResponseHeader") != null)
      {
        document.getElementById("ctl00_ContentHolder_divResponseHeader").className = "headerTabCommonWODet";
        if (document.getElementById("ctl00_ContentHolder_divResponse").className == "displayBlock")
	        document.getElementById("ctl00_ContentHolder_divResponseHeader").className = "headerTabCommonWODetHighLighted";
      }
}