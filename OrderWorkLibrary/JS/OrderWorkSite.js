﻿//function to change rollover image
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


//functions for menu dropdowns
function show(id)
 {
 var a=document.getElementById("linkCSS");
     var bit =a.disabled;
     
    if (bit==false)
   
    {
        var d = document.getElementById(id);
	    for (var i = 1; i<=10; i++)
	     {
		    if (document.getElementById('smenu'+i))
		    {
		      document.getElementById('smenu'+i).style.display='none';
		    }
	    }
       if (d) 
        {
         d.style.display='block';
         //This style will overlap the DD
         d.style.position='absolute';
         //d.style.z-index=10000;
        }
    }
}

//Functions for menus on Client Master
//functions for menu dropdowns
function showClient(id)
 {
 var a=document.getElementById("linkCSS");
     var bit =a.disabled;
     
    if (bit==false)
   
    {
        var d = document.getElementById(id);
	    for (var i = 1; i<=10; i++)
	     {
		    if (document.getElementById('smenuClients'+i))
		    {
		      document.getElementById('smenuClients'+i).style.display='none';
		    }
	    }
       if (d) 
        {
         d.style.display='block';
         //This style will overlap the DD
         d.style.position='absolute';
         //d.style.z-index=10000;
        }
    }
}

function Hide(id)
{
    var a=document.getElementById("linkCSS");
     var bit =a.disabled;
     
    if (bit==false)
   
    {
       var d = document.getElementById(id);
       d.style.display='none';

   }
 }

function rounded()
{
  DD_roundies.addRule('.roundifyRefine20', '20px');
  DD_roundies.addRule('.roundifyRefine10', '10px');
}
function roundedaboutus()
{
  DD_roundies.addRule('.roundifyRefineAbout20', '18px 18px 0px 0px');
  DD_roundies.addRule('.roundifyRefineAboutbottom20', '0px 0px 18px 18px');
  DD_roundies.addRule('.roundifyRefineAbout10', '10px');
  DD_roundies.addRule('.roundifyRefine4', '4px');
}
function roundedSP()
{
  DD_roundies.addRule('.roundifyRefineAbout20', '20px 0px 0px 0px');
  DD_roundies.addRule('.roundifyRefinesp', '0px 0px 0px 20px');
  DD_roundies.addRule('.roundifyRefineAbout10', '10px');
    DD_roundies.addRule('.roundifyRefinesp1', '0px 20px 0px 0px');
        DD_roundies.addRule('.roundifyRefinesp2', '0px 0px 20px 0px');
  
}

function CheckAll(me,checkBoxId)
{

    var index = me.name.indexOf('_');  
    var prefix = me.name.substr(0,index); 

    // Looks for the right checkbox
    for(i=0; i<document.forms[0].length; i++) 
    { 
        var o = document.forms[0][i]; 
        var str =new String();
        str=o.id;       
        if (o.type == 'checkbox' && (me.name != o.name) && (o.name.substring(0, prefix.length) == prefix) ) 
        {       
            if (str.indexOf(checkBoxId)!=-1) 
            {    
                o.checked = me.checked;                 
                //GetSelectedIds(o.id, selectId,strUC);                                        
            }
        } 
    }   
}

function SelectCompanyOrContractor() {
    if (document.getElementById("rbCompany").checked) {
        document.getElementById("divRegSP").style.display = "inline";
        document.getElementById("divRegContractor").style.display = "none";
    }
    if (document.getElementById("rbContractor").checked) {
        document.getElementById("divRegSP").style.display = "none";
        document.getElementById("divRegContractor").style.display = "inline";
    }


}

///*****************************************************Function For Tabular AOE selection Starts Here *******************************************

function CheckAOEMainCat(AOEDivId, hdnCombIDs) {
    var objId = AOEDivId.replace("AOE", "");
    var strIDs = document.getElementById(hdnCombIDs).value;
    var SelSubCatCombId = document.getElementById("hdnSelSubCatCombId" + objId).value;
    var SubCatOfMainCat = document.getElementById("hdnSubCatOfMainCat" + objId).value;
    SelSubCatCombId = SelSubCatCombId.replace("," + objId, "");
    if (document.getElementById(AOEDivId).className == "clsNotSelectedSkillLeftDiv") {
        if (strIDs != "")
        { strIDs = strIDs + "," }
        strIDs = strIDs + objId

        var items = SubCatOfMainCat.split(",");
        for (var i = 0; i < items.length; i++)
        {
            var item = items[i];
            if (strIDs != "") {
                if (strIDs.indexOf("," + item) == -1) {
                    strIDs = strIDs + "," + item
                    if (SelSubCatCombId != "")
                    { SelSubCatCombId = SelSubCatCombId + "," }
                    SelSubCatCombId = SelSubCatCombId + item
                }
            }
            else {
                strIDs = strIDs + item
            }
            document.getElementById("AOE" + item).className = "clsSelectedSkillDiv";
        }

        document.getElementById(AOEDivId).className = "clsSelectedSkillLeftDiv";
    }
    else {
        //  if(SelSubCatCombId == "0")
        // {
        var str = "," + objId;
        strIDs = strIDs.replace(str, "")

        var items = SubCatOfMainCat.split(",");
        for (var i = 0; i < items.length; i++) 
        {
            var item = items[i];

            str = "," + item;
            strIDs = strIDs.replace(str, "")

            var strSelSubCatCombId = "," + item;
            SelSubCatCombId = SelSubCatCombId.replace(strSelSubCatCombId, "");

            document.getElementById("AOE" + item).className = "clsNotSelectedSkillDiv";
        }

        document.getElementById(AOEDivId).className = "clsNotSelectedSkillLeftDiv";
        //}

    }
    document.getElementById(hdnCombIDs).value = strIDs;
    document.getElementById("hdnSelSubCatCombId" + objId).value = SelSubCatCombId;
}

function CheckAOESubCat(AOEDivId, hdnCombIDs) {
    var objId = AOEDivId.replace("AOE", "");
    var MainCatCombId = document.getElementById("hdnMainCatCombId" + objId).value;
    var SelSubCatCombId = document.getElementById("hdnSelSubCatCombId" + MainCatCombId).value;
    SelSubCatCombId = SelSubCatCombId.replace("," + MainCatCombId, "")
    var strIDs = document.getElementById(hdnCombIDs).value;
    if (document.getElementById(AOEDivId).className == "clsNotSelectedSkillDiv") {
        if (strIDs != "") {
            strIDs = strIDs.replace("," + MainCatCombId, "")
            strIDs = strIDs + ","
        }
        strIDs = strIDs + objId
        strIDs = strIDs + "," + MainCatCombId

        if (SelSubCatCombId != "")
        { SelSubCatCombId = SelSubCatCombId + "," }
        SelSubCatCombId = SelSubCatCombId + objId

        document.getElementById(AOEDivId).className = "clsSelectedSkillDiv";
        document.getElementById("AOE" + MainCatCombId).className = "clsSelectedSkillLeftDiv";

    }
    else {
        var str = "," + objId;
        strIDs = strIDs.replace(str, "")

        var strSelSubCatCombId = "," + objId;
        SelSubCatCombId = SelSubCatCombId.replace(strSelSubCatCombId, "");
        //alert(SelSubCatCombId);
        if (SelSubCatCombId == "0") {
            document.getElementById("AOE" + MainCatCombId).className = "clsNotSelectedSkillLeftDiv";
            var str = "," + MainCatCombId;
            strIDs = strIDs.replace(str, "")
        }



        document.getElementById(AOEDivId).className = "clsNotSelectedSkillDiv";

    }
    document.getElementById(hdnCombIDs).value = strIDs;
    document.getElementById("hdnSelSubCatCombId" + MainCatCombId).value = SelSubCatCombId;
    // alert(document.getElementById(hdnCombIDs).value);
}



///*****************************************************Function For Tabular AOE selection Ends Here *******************************************