<p>Dear <Name>,<BR><BR></p>

<p>An engineer has now been assigned to fulfil your work request <WorkOrderID>, a summary of which is listed below.<BR><BR></p>

<p>WO No: <WorkOrderID><BR><br />
WO Title: <WOTitle><BR><br />
WO Category: <WOCategory> <BR><br />
Date Accepted: <Date><BR><br />
Location: <Location><BR><br />
Price: <Price><BR><br />
Start Date: <StartDate>End Date: <EndDate><BR><br />
Questionnaire:<BR><br />
<ThermostatsQuestions><BR> </p>

<p>If you have any questions, please feel free to contact us.<BR><BR> </p>

<p>Kind regards,<BR> <br />
The OrderWork Team<BR><BR> </p>

<p>Telephone: 0203 053 0343<BR> <br />
E-mail: info@orderwork.co.uk <BR><br />
My OrderWork: http://orderwork.co.uk/login.php<BR><br />
Website: www.orderwork.co.uk<BR><BR></p>

<p>Note: This email was automatically generated following a Work Order action performed on the OrderWork site.</p>