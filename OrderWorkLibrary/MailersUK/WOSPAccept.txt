<p>Dear <Name>,<BR><BR></p>

<p>Thank you for accepting Work Order <WorkOrderID>, a short summary of which is shown below.<BR><BR></p>

<p>WO No: <WorkOrderID><BR><br />
WO Title: <WOTitle><BR><br />
WO Category: <WOCategory><BR><br />
Date Accepted: <Date><BR><br />
Location: <Location><BR><br />
Price: <Price><BR><br />
Start Date: <StartDate>End Date: <EndDate><BR><br />
Questionnaire:<BR><br />
<ThermostatsQuestions><BR><BR> </p>

<p>Comments: <Comment><BR><BR></p>

<p>In accordance with OrderWork's terms and conditions you are now obliged to undertake the work as outlined in the work order within the stated timeframe.<BR><BR> </p>

<p>If you have any questions, please feel free to contact us. <BR><BR></p>

<p>Kind regards, <BR><br />
The OrderWork Team <BR><BR></p>

<p>Telephone: 0203 053 0343 <BR><br />
E-mail: info@orderwork.co.uk <BR><br />
My OrderWork: http://orderwork.co.uk/login.php<BR><br />
Website: www.orderwork.co.uk<BR><BR></p>

<p>Note: This email was automatically generated following a Work Order action performed on the OrderWork site.</p>