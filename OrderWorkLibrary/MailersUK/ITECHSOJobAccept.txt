<p>Dear User,<BR><BR></p>

<p>The following workorder is now Accepted and Active but does not have and associated Purchase Order.<BR><BR></p>

<p>
Workorder: <b><WorkordeId></b><BR><BR></p>

<p>Appointment Date: <b><StartDate></b><BR><BR></p>

<p>Start Time: <b><AptTime></b><BR></p>

<p>Please enter a valid PO number for this workorder.<BR><BR></p>

<p>
Kind regards<BR><BR></p>

<p>OrderWork Team<BR><br />
0203 053 0343<BR><br />
info@orderwork.co.uk<BR><BR></p>

<p>Note: This email was automatically generated following a Workorder action performed on the Orderwork website.<BR><br />
</p>