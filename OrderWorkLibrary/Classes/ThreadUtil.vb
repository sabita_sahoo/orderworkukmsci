Imports System
Imports System.Threading

Public Class ThreadUtil
    ''' 
    ''' Delegate to wrap another delegate and its arguments 
    ''' 
    Private Delegate Sub DelegateWrapper(ByVal d As [Delegate], ByVal args As Object())

    ''' 
    ''' An instance of DelegateWrapper which calls InvokeWrappedDelegate, 
    ''' which in turn calls the DynamicInvoke method of the wrapped 
    ''' delegate. 
    ''' 
    Shared wrapperInstance As New DelegateWrapper(AddressOf InvokeWrappedDelegate)

    ''' 
    ''' Callback used to call EndInvoke on the asynchronously 
    ''' invoked DelegateWrapper. 
    ''' 
    Shared callback As New AsyncCallback(AddressOf EndWrapperInvoke)

    ''' 
    ''' Executes the specified delegate with the specified arguments 
    ''' asynchronously on a thread pool thread. 
    ''' 
    Public Shared Sub FireAndForget(ByVal d As [Delegate], ByVal ParamArray args As Object())
        ' Invoke the wrapper asynchronously, which will then 
        ' execute the wrapped delegate synchronously (in the 
        ' thread pool thread) 
        wrapperInstance.BeginInvoke(d, args, callback, Nothing)
    End Sub

    ''' 
    ''' Invokes the wrapped delegate synchronously 
    ''' 
    Private Shared Sub InvokeWrappedDelegate(ByVal d As [Delegate], ByVal args As Object())
        d.DynamicInvoke(args)
    End Sub

    ''' 
    ''' Calls EndInvoke on the wrapper and Close on the resulting WaitHandle 
    ''' to prevent resource leaks. 
    ''' 
    Private Shared Sub EndWrapperInvoke(ByVal ar As IAsyncResult)
        wrapperInstance.EndInvoke(ar)
        ar.AsyncWaitHandle.Close()
    End Sub
End Class
