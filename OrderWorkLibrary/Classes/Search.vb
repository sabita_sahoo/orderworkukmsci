Imports System.Data.SqlClient
Public Class Search

    Public Shared Function SearchData(ByVal CompanyID As String, ByVal UserRoleID As String, ByVal SearchString As String, ByVal SearchArea As String, ByVal SortExpression As String, ByVal maximumRows As Integer, ByVal startRowIndex As Integer) As DataSet
        CommonFunctions.setCultureSettings(ApplicationSettings.Country, ApplicationSettings.SiteType)
        Dim conn As SqlConnection = DBUtil.getDBConn
        Try
            Dim ProcedureName As String = "spMS_GetSearch"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 1000

            cmd.Parameters.AddWithValue("@CompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@keyword", SearchString)
            cmd.Parameters.AddWithValue("@UserRoleID", UserRoleID)
            cmd.Parameters.AddWithValue("@SearchArea", SearchArea)
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("DSSreach")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchResult"
            ds.Tables(1).TableName = "tblRowCount"
            If ds.Tables(1).Rows.Count > 0 Then
                HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalCount")
            Else
                HttpContext.Current.Items("rowCount") = 0
            End If
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function


    Public Shared Function SelectSearchCount(ByVal CompanyID As String, ByVal UserRoleID As String, ByVal SearchString As String, ByVal SearchArea As String) As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

End Class
