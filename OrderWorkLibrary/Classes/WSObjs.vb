Imports System.IO

Public Class WSObjs
    Inherits System.Web.UI.Page
    Private _siteCountry As String
    Private _bizDivId As Integer
    Private _siteType As String
  
#Region "WS Calls"

    Public Sub New()
        MyBase.New()
        country = ApplicationSettings.Country
        bizDivId = ApplicationSettings.BizDivId
        siteType = ApplicationSettings.SiteType
    End Sub
    Public Property country() As String
        Get
            Return _siteCountry
        End Get
        Set(ByVal value As String)
            _siteCountry = value
        End Set
    End Property
    Public Property bizDivId() As Integer
        Get
            Return _bizDivId
        End Get
        Set(ByVal value As Integer)
            _bizDivId = value
        End Set
    End Property

    Public Property siteType() As String
        Get
            Return _siteType
        End Get
        Set(ByVal value As String)
            _siteType = value
        End Set
    End Property

    Public ReadOnly Property WSiPhoneNotification() As WSiPhoneNotification.iPhoneNotificationService
        Get
            Try
                Static ws As WSiPhoneNotification.iPhoneNotificationService

                If ws Is Nothing Then
                    Try
                        ws = New WSiPhoneNotification.iPhoneNotificationService
                        ws.Url = Path.Combine(ApplicationSettings.WSiPhoneRoot, "iPhoneNotificationService.asmx")
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If
                Return ws
            Catch ex As Exception
                Throw ex
            Finally : End Try
        End Get
    End Property

    'Public ReadOnly Property WSPostCode() As WSPostCode.LookupUK
    '    Get
    '        Try
    '            Static ws As WSPostCode.LookupUK

    '            If ws Is Nothing Then
    '                Try
    '                    ws = New WSPostCode.LookupUK
    '                Catch ex As Exception
    '                    Throw ex
    '                End Try
    '            End If

    '            Dim cookieContainer As System.Net.CookieContainer = CType(Session.Item("SIOCookieContainer"), System.Net.CookieContainer)
    '            If cookieContainer Is Nothing Then
    '                cookieContainer = New System.Net.CookieContainer
    '            End If
    '            ws.CookieContainer = cookieContainer
    '            Return ws
    '        Catch ex As Exception
    '            Throw ex
    '        Finally
    '            ' do something smart here todo
    '        End Try
    '    End Get
    'End Property
#End Region

#Region "Generic Function"
    Public Function getUserName() As String
        
        Return ApplicationSettings.GuestLogin

    End Function
    Public Function getPassword() As String
       
        Return Encryption.Encrypt(ApplicationSettings.GuestPassword)

    End Function

#End Region
End Class
