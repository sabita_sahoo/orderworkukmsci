Public Class ControlMail

    ''' <summary>
    ''' Send mail to Admin on submit of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnBuyerAcceptToBuyer As Boolean = False
    Public Shared ReadOnly Property IsMailOnBuyerAcceptToBuyer() As Boolean
        Get
            Return _IsMailOnBuyerAcceptToBuyer And OrderWorkLibrary.ApplicationSettings.SendMailBuyer
        End Get
    End Property
    Private Shared _IsMailOnBuyerAcceptToAdmin As Boolean = False
    Public Shared ReadOnly Property IsMailOnBuyerAcceptToAdmin() As Boolean
        Get
            Return _IsMailOnBuyerAcceptToAdmin And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Admin on submit of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSubmitWO As Boolean = True
    Public Shared ReadOnly Property IsMailOnSubmitWO() As Boolean
        Get
            Return _IsMailOnSubmitWO And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Buyer on submit of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSubmitWOToBuyer As Boolean = True
    Public Shared ReadOnly Property IsMailOnSubmitWOToBuyer() As Boolean
        Get
            Return _IsMailOnSubmitWOToBuyer And OrderWorkLibrary.ApplicationSettings.SendMailBuyer
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on Discard of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnDiscard As Boolean = False
    Public Shared ReadOnly Property IsMailOnDiscard() As Boolean
        Get
            Return _IsMailOnDiscard And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on SPAccept of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPAccept As Boolean = False
    Public Shared ReadOnly Property IsMailOnSPAccept() As Boolean
        Get
            Return _IsMailOnSPAccept And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on SPAccept of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPAcceptToSP As Boolean = False
    Public Shared ReadOnly Property IsMailOnSPAcceptToSP() As Boolean
        Get
            Return _IsMailOnSPAcceptToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on SPAccept of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPAcceptToBuyer As Boolean = True
    Public Shared ReadOnly Property IsMailOnSPAcceptToBuyer() As Boolean
        Get
            Return _IsMailOnSPAcceptToBuyer And OrderWorkLibrary.ApplicationSettings.SendMailBuyer
        End Get
    End Property


    ''' <summary>
    ''' Send mail to Admin on SPCAccept of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPCAccept As Boolean = False
    Public Shared ReadOnly Property IsMailOnSPCAccept() As Boolean
        Get
            Return _IsMailOnSPCAccept And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property

    ''' <summary>
    ''' IsMailOnSPCAcceptToSP
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPCAcceptToSP As Boolean = False
    Public Shared ReadOnly Property IsMailOnSPCAcceptToSP() As Boolean
        Get
            Return _IsMailOnSPCAcceptToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property

    ''' <summary>
    ''' IsMailOnSPCAcceptToBuyer
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPCAcceptToBuyer As Boolean = False
    Public Shared ReadOnly Property IsMailOnSPCAcceptToBuyer() As Boolean
        Get
            Return _IsMailOnSPCAcceptToBuyer And OrderWorkLibrary.ApplicationSettings.SendMailBuyer
        End Get
    End Property
    
    ''' <summary>
    ''' Send mail to Admin on SPChangeCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPChangeCA As Boolean = False
    Public Shared ReadOnly Property IsMailOnSPChangeCA() As Boolean
        Get
            Return _IsMailOnSPChangeCA And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Admin on SPChangeCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPChangeCAToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnSPChangeCAToSP() As Boolean
        Get
            Return _IsMailOnSPChangeCAToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on DiscussCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnDiscussCA As Boolean = False
    Public Shared ReadOnly Property IsMailOnDiscussCA() As Boolean
        Get
            Return _IsMailOnDiscussCA And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on DiscussCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnDiscussCAToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnDiscussCAToSP() As Boolean
        Get
            Return _IsMailOnDiscussCAToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property


    ''' <summary>
    ''' Send mail to Admin on AcceptCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnAcceptCA As Boolean = False
    Public Shared ReadOnly Property IsMailOnAcceptCA() As Boolean
        Get
            Return _IsMailOnAcceptCA And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on AcceptCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnAcceptCAToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnAcceptCAToSP() As Boolean
        Get
            Return _IsMailOnAcceptCAToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Buyer on AcceptCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnAcceptCAToBuyer As Boolean = True
    Public Shared ReadOnly Property IsMailOnAcceptCAToBuyer() As Boolean
        Get
            Return _IsMailOnAcceptCAToBuyer And OrderWorkLibrary.ApplicationSettings.SendMailBuyer
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on RaiseIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailRaiseIssue As Boolean = True
    Public Shared ReadOnly Property IsMailRaiseIssue() As Boolean
        Get
            Return _IsMailRaiseIssue And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on RaiseIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailRaiseIssueToSP As Boolean = True
    Public Shared ReadOnly Property IsMailRaiseIssueToSP() As Boolean
        Get
            Return _IsMailRaiseIssueToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on ChangeIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnChangeIssue As Boolean = True
    Public Shared ReadOnly Property IsMailOnChangeIssue() As Boolean
        Get
            Return _IsMailOnChangeIssue And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on ChangeIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnChangeIssueToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnChangeIssueToSP() As Boolean
        Get
            Return _IsMailOnChangeIssueToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Admin on ChangeIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnDiscuss As Boolean = True
    Public Shared ReadOnly Property IsMailOnDiscuss() As Boolean
        Get
            Return _IsMailOnDiscuss And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on ChangeIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnDiscussToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnDiscussToSP() As Boolean
        Get
            Return _IsMailOnDiscussToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property


    ''' <summary>
    ''' Send mail to Admin on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnAcceptIssue As Boolean = True
    Public Shared ReadOnly Property IsMailOnAcceptIssue() As Boolean
        Get
            Return _IsMailOnAcceptIssue And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnAcceptIssueToSP As Boolean = False
    Public Shared ReadOnly Property IsMailOnAcceptIssueToSP() As Boolean
        Get
            Return _IsMailOnAcceptIssueToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property


    ''' <summary>
    ''' Send mail to Admin on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnComplete As Boolean = False
    Public Shared ReadOnly Property IsMailOnComplete() As Boolean
        Get
            Return _IsMailOnComplete And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnCompleteToSP As Boolean = False
    Public Shared ReadOnly Property IsMailOnCompleteToSP() As Boolean
        Get
            Return _IsMailOnCompleteToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Buyer on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnCompleteToBuyer As Boolean = False
    Public Shared ReadOnly Property IsMailOnCompleteToBuyer() As Boolean
        Get
            Return _IsMailOnCompleteToBuyer And OrderWorkLibrary.ApplicationSettings.SendMailBuyer
        End Get
    End Property


    ''' <summary>
    ''' Send mail to Admin on Close of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnClose As Boolean = False
    Public Shared ReadOnly Property IsMailOnClose() As Boolean
        Get
            Return _IsMailOnClose And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Admin on Close of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnCloseToSP As Boolean = False
    Public Shared ReadOnly Property IsMailOnCloseToSP() As Boolean
        Get
            Return _IsMailOnCloseToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property ''' <summary>
    ''' Send mail to Admin on Close of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnCloseToBuyer As Boolean = False
    Public Shared ReadOnly Property IsMailOnCloseToBuyer() As Boolean
        Get
            Return _IsMailOnCloseToBuyer And OrderWorkLibrary.ApplicationSettings.SendMailBuyer
        End Get
    End Property



    ''' <summary>
    ''' Send mail to Admin on Close of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailWithdrawFunds As Boolean = False
    Public Shared ReadOnly Property IsMailWithdrawFunds() As Boolean
        Get
            Return _IsMailWithdrawFunds And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property

    ''' <summary>
    ''' _IsMailOnLostWOToAdmin
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnLostWOToAdmin As Boolean = False
    Public Shared ReadOnly Property IsMailOnLostWOToAdmin() As Boolean
        Get
            Return _IsMailOnLostWOToAdmin And OrderWorkLibrary.ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' _IsMailOnLostWOToSP
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnLostWOToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnLostWOToSP() As Boolean
        Get
            Return _IsMailOnLostWOToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property
    ''' <summary>
    ''' _IsMailOnDiscardToSP
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnDiscardToSP As Boolean = False
    Public Shared ReadOnly Property IsMailOnDiscardToSP() As Boolean
        Get
            Return _IsMailOnDiscardToSP And OrderWorkLibrary.ApplicationSettings.SendMailSupplier
        End Get
    End Property
End Class
