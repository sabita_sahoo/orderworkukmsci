﻿Imports System.IO
Imports System.Drawing
Imports System.Drawing.Imaging

Public Class PhotoUpload
    ''' <summary>
    ''' Function to get the Image format 
    ''' </summary>
    ''' <param name="format"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getGDIFormat(ByVal format As String) As ImageFormat
        Dim fmt As ImageFormat = Nothing
        Select Case format
            Case "gif"
                fmt = ImageFormat.Gif
            Case "jpg"
                fmt = ImageFormat.Jpeg
            Case "png"
                fmt = ImageFormat.Png
        End Select
        Return fmt
    End Function

    Public Shared Function UploadImage(ByVal bmpW As Integer, ByVal bmpH As Integer, ByRef imgName As String, ByVal imgPath As String, ByVal ImageFormat As String, ByRef FileUpload1 As FileUpload) As Boolean
        Dim newWidth As Integer = bmpW
        Dim newHeight As Integer = bmpH

        Dim filePath As String = imgPath & "\" & imgName
        'Create a new Bitmap using the uploaded picture as a Stream 
        'Set the new bitmap resolution to 72 pixels per inch 
        Dim upBmp As Bitmap = Bitmap.FromStream(FileUpload1.PostedFile.InputStream)
        Dim newBmp As Bitmap = New Bitmap(newWidth, newHeight, Imaging.PixelFormat.Format24bppRgb)
        newBmp.SetResolution(72, 72)
        'Get the uploaded image width and height 
        Dim upWidth As Integer = upBmp.Width
        Dim upHeight As Integer = upBmp.Height
        Dim newX As Integer = 0 'Set the new top left drawing position on the image canvas
        Dim newY As Integer = 0
        Dim reDuce As Decimal
        ' Aspect Ratio
        'Keep the aspect ratio of image the same if not 4:3 and work out the newX and newY positions 
        'to ensure the image is always in the centre of the canvas vertically and horizontally 
        If upWidth > upHeight Then 'Landscape picture 
            reDuce = newWidth / upWidth
            'calculate the width percentage reduction as decimal 
            newHeight = Int(upHeight * reDuce)
            newWidth = Int(upWidth * reDuce)
            'reduce the uploaded image height by the reduce amount 
            newY = Int((bmpH - newHeight) / 2)
            'Position the image centrally down the canvas 
            newX = 0 'Picture will be full width 
        ElseIf upWidth < upHeight Then 'Portrait picture 
            reDuce = newHeight / upHeight
            'calculate the height percentage reduction as decimal 
            newWidth = Int(upWidth * reDuce)
            newHeight = Int(upHeight * reDuce)
            'reduce the uploaded image height by the reduce amount 
            newX = Int((bmpW - newWidth) / 2)
            'Position the image centrally across the canvas 
            newY = 0 'Picture will be full hieght 
        ElseIf upWidth = upHeight Then 'square picture 
            reDuce = newHeight / upHeight
            'calculate the height percentage reduction as decimal 
            newWidth = Int(upWidth * reDuce)
            newHeight = Int(upHeight * reDuce)
            'reduce the uploaded image height by the reduce amount 
            newX = Int((bmpW - newWidth) / 2) 'Position the image centrally across the canvas
            newY = Int((bmpH - newHeight) / 2) 'Position the image centrally down the canvas 
        End If

        'Create a new image from the uploaded picture using the Graphics class 
        'Clear the graphic and set the background colour to white 
        'Use Antialias and High Quality Bicubic to maintain a good quality picture 
        'Save the new bitmap image using '' picture format provided and the calculated canvas positioning 
        Dim newGraphic As Graphics = Graphics.FromImage(newBmp)
        Try
            newGraphic.Clear(Color.White)
            newGraphic.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
            newGraphic.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
            newGraphic.DrawImage(upBmp, newX, newY, newWidth, newHeight)

            If (File.Exists(filePath)) Then
                File.Delete(filePath)
            End If
            newBmp.Save((filePath), getGDIFormat(ImageFormat))

            'frmConfirmation.Visible = True
            AzureFileUpload.UploadFileThroughFileDir(filePath, imgName, ApplicationSettings.AttachmentUploadPath(1) + "ContactPhoto")

        Catch ex As Exception
            Return False
        Finally
            upBmp.Dispose()
            newBmp.Dispose()
            newGraphic.Dispose()
        End Try
        Return True
    End Function

    Public Shared Function CheckFileType(ByVal fileName As String, ByVal MimeType As String) As Boolean
        Dim ext As String = Path.GetExtension(fileName)
        Select Case ext.ToLower()
            Case ".gif"
                If MimeType.ToLower = "image/gif" Then
                    Return True
                Else
                    Return False
                End If
            Case ".png"
                If MimeType.ToLower = "image/png" Then
                    Return True
                ElseIf MimeType.ToLower = "image/x-png" Then
                    Return True
                Else
                    Return False
                End If
            Case ".jpg"
                If MimeType.ToLower = "image/jpeg" Then
                    Return True
                ElseIf MimeType.ToLower = "image/pjpeg" Then
                    Return True
                Else
                    Return False
                End If
            Case ".jpeg"
                If MimeType.ToLower = "image/jpeg" Then
                    Return True
                ElseIf MimeType.ToLower = "image/pjpeg" Then
                    Return True
                Else
                    Return False
                End If
            Case ".bmp"
                If MimeType.ToLower = "image/x-bmp" Or MimeType.ToLower = "image/bmp" Then
                    Return True
                Else
                    Return False
                End If
            Case Else
                Return False
        End Select
    End Function

End Class
