Imports System.Net
Imports System.Web.Mail
Imports System.Xml
Imports System.IO
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
'Imports System.Linq
Imports System.Linq

''' <summary>
''' Common utility functions class shared by all projects
''' </summary>
''' <remarks></remarks>
Public Class CommonFunctions



    ''' <summary>
    ''' Validate mail exist and migration
    ''' </summary>
    ''' <param name="mailID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ValidateMailAndMigration(ByVal mailID As String) As String
        Dim str As String = ""
        Dim Msg1 As String = ""
        Dim migrationStatusDisabled As Boolean = False
        Dim responseText = CommonFunctions.CheckAccountExistAndMigration(mailID, True)
        If responseText = "" Then
            Return ""
        End If
        Dim action As String = CommonFunctions.getXMLElementValue(responseText, "action")
        Select Case action
            Case "Exists"
                str = OrderWorkLibrary.ResourceMessageText.GetString("EmailAlreadyExist")
            Case "nonMsSupplier"
                Select Case ApplicationSettings.BizDivId
                    Case ApplicationSettings.SFUKBizDivId
                        str = OrderWorkLibrary.ResourceMessageText.GetString("SupplierAccExist")
                        str += OrderWorkLibrary.ResourceMessageText.GetString("DontHaveMSCertForComp")
                        str += OrderWorkLibrary.ResourceMessageText.GetString("ContactOWRep")
                End Select
            Case "typeBuyer"
                Select Case ApplicationSettings.BizDivId
                    Case ApplicationSettings.SFUKBizDivId
                        str = OrderWorkLibrary.ResourceMessageText.GetString("BuyerAccAlreadyExist")
                        str += OrderWorkLibrary.ResourceMessageText.GetString("ContactOW")
                        str = str.Replace("<MigrateSite>", ApplicationSettings.OtherSiteName)
                End Select
            Case "userNotActive", "companySuspended"
                str = OrderWorkLibrary.ResourceMessageText.GetString("AccAlreadyExistOnSite")
                str = str.Replace("<siteName>", ApplicationSettings.OtherSiteName)
                str = str.Replace("<siteURL>", ApplicationSettings.OtherWebPath)
                str += OrderWorkLibrary.ResourceMessageText.GetString("AccInactive")
            Case "nonAdmin"
                str = OrderWorkLibrary.ResourceMessageText.GetString("AccAlreadyExistOnSite")
                str = str.Replace("<siteURL>", ApplicationSettings.OtherWebPath)
                str += OrderWorkLibrary.ResourceMessageText.GetString("NoAdminPrivilegeToMigrate")
                str = str.Replace("<siteName>", ApplicationSettings.OtherSiteName)
                str += OrderWorkLibrary.ResourceMessageText.GetString("ContactOWRep")
            Case "Migrate"
                If CommonFunctions.getXMLElementValue(responseText, "disabled") = "true" Then
                    migrationStatusDisabled = True
                End If
                str = OrderWorkLibrary.ResourceMessageText.GetString("AccAlreadyExistOnSite")
                str = str.Replace("<siteName>", ApplicationSettings.OtherSiteName)
                str = str.Replace("<siteURL>", ApplicationSettings.OtherWebPath)

                If migrationStatusDisabled = True Then
                    str += OrderWorkLibrary.ResourceMessageText.GetString("ContactOW")
                    str = str.Replace("<MigrateSite>", ApplicationSettings.OtherSiteName)
                Else
                    str += "Account Details:</b>"
                    str += "Company Name:</b>&nbsp;&nbsp;" + CommonFunctions.getXMLElementValue(responseText, "companyName") + "</br>"
                    str += "Admin:</b>&nbsp;&nbsp;" + CommonFunctions.getXMLElementValue(responseText, "admin") + "</br>"
                    str += "Admin Email:</b>&nbsp;&nbsp;" + CommonFunctions.getXMLElementValue(responseText, "adminMail") + "</br>"
                    If CommonFunctions.getXMLElementValue(responseText, "noRegLoc") <> "" Then
                        str += "No of registered locations:</b>&nbsp;&nbsp;" + CommonFunctions.getXMLElementValue(responseText, "noRegLoc") + "</br>"
                    End If
                    str += "No of registered users:</b>&nbsp;&nbsp;" + CommonFunctions.getXMLElementValue(responseText, "noRegUser") + "</br><br/>"
                    str += OrderWorkLibrary.ResourceMessageText.GetString("ClickToConfirmA") + "' href='" + CommonFunctions.getXMLElementValue(responseText, "migrateLink") + "'>confirm</a> " + OrderWorkLibrary.ResourceMessageText.GetString("ClickToConfirmB")  '"'>confirm</a> your identity to migrate your Skills Finder account to Order Work.<br/>"
                    str += OrderWorkLibrary.ResourceMessageText.GetString("ContinueWithDiffEmail")
                    str = str.Replace("<site>", ApplicationSettings.SiteName)
                    str = str.Replace("<fromSite>", ApplicationSettings.SiteName)
                    str = str.Replace("<toSite>", ApplicationSettings.OtherSiteName)
                End If

                Return str
            Case Else
                Return ""
        End Select
        Return str
    End Function
    ''' <summary>
    ''' Function to check for Account Exist before migration
    ''' </summary>
    ''' <param name="Email"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CheckAccountExistAndMigration(ByVal Email As String, ByVal isRegisterPage As Boolean) As String
        Dim str As String = ""
        Dim adminName As String = ""
        Dim ds As DataSet
        Dim dv As DataView
        Dim statusId As String

        If Trim(Email) = "" Then
            Return ""
        End If
        ds = DBContacts.GetAccountMigrationDetails(Email, isRegisterPage)

        Dim BizExists As Boolean = False

        ' If email already exist
        If ds.Tables("contactBizDivLinkage").DefaultView.Count > 0 Then

            If isRegisterPage Then
                If ds.Tables("contactBizDivLinkage").Rows(0).Item("BizDivID") = ApplicationSettings.BizDivId Then
                    BizExists = True
                End If

            Else

                Select Case ApplicationSettings.Country
                    Case ApplicationSettings.CountryDE
                        Select Case ApplicationSettings.BizDivId
                            Case ApplicationSettings.OWDEBizDivId
                                If ds.Tables("contactBizDivLinkage").Rows(0).Item("BizDivID") = ApplicationSettings.OWDEPeer1BizDivId Then
                                    BizExists = True
                                End If
                            Case ApplicationSettings.SFDEBizDivId
                                If ds.Tables("contactBizDivLinkage").Rows(0).Item("BizDivID") = ApplicationSettings.SFDEPeer1BizDivId Then
                                    BizExists = True
                                End If
                        End Select
                    Case ApplicationSettings.CountryUK
                        Select Case ApplicationSettings.BizDivId
                            Case ApplicationSettings.OWUKBizDivId
                                If ds.Tables("contactBizDivLinkage").Rows(0).Item("BizDivID") = ApplicationSettings.OWUKPeer1BizDivId Then
                                    BizExists = True
                                End If
                            Case ApplicationSettings.SFUKBizDivId
                                If ds.Tables("contactBizDivLinkage").Rows(0).Item("BizDivID") = ApplicationSettings.SFUKPeer1BizDivId Then '
                                    BizExists = True
                                End If
                        End Select
                End Select

            End If



            If BizExists = True Then
                str = "<action>Exists</action>"
                Return str
            End If
        End If
        If ApplicationSettings.MigrationSwitchStatus = "1" Then
            str += "<disabled>false</disabled>"
        Else ' Migration is disabled
            str += "<disabled>true</disabled>"
        End If
        If ds.Tables("user").DefaultView.Count > 0 Then


            ' Check for User type and certifications this check is implemented for Skills Finder Sites only
            ' Check for buyer if user is buyer then dnt allow to register
            If isRegisterPage Then
                If ApplicationSettings.BizDivId = ApplicationSettings.SFUKBizDivId Or ApplicationSettings.BizDivId = ApplicationSettings.SFDEBizDivId Then
                    'ds.Tables("contactClasses").DefaultView.RowFilter = "ClassID=" & ApplicationSettings.RoleSupplierID
                    If ds.Tables("contactBizDivLinkage").Rows(0).Item("ClassID") = ApplicationSettings.RoleSupplierID Then
                        ' check if user has MS certification                 
                        dv = ds.Tables("ContactAccreditations").Copy.DefaultView
                        If dv.Count = 0 Then
                            str = "<action>nonMsSupplier</action>"
                            Return str
                        End If
                    Else
                        str = "<action>typeBuyer</action>"
                        Return str
                    End If
                End If
            Else ' for home page Migration UC Check
                If ApplicationSettings.BizDivId = ApplicationSettings.OWUKBizDivId Or ApplicationSettings.BizDivId = ApplicationSettings.OWDEBizDivId Then
                    'ds.Tables("contactClasses").DefaultView.RowFilter = "ClassID=" & ApplicationSettings.RoleSupplierID
                    If ds.Tables("contactBizDivLinkage").Rows(0).Item("ClassID") = ApplicationSettings.RoleSupplierID Then
                        ' check if user has MS certification                 
                        dv = ds.Tables("ContactAccreditations").Copy.DefaultView
                        If dv.Count = 0 Then
                            str = "<action>nonMsSupplier</action>"
                            Return str
                        End If
                    Else
                        str = "<action>typeBuyer</action>"
                        Return str
                    End If
                End If
            End If

            statusId = ApplicationSettings.StatusActive

            'Check for account Status- contact
            If Not ds.Tables("contactBizDivLinkage") Is Nothing Then
                If ds.Tables("contactBizDivLinkage").Rows(0)("Status") <> statusId Then
                    'User account is not active
                    str = "<action>userNotActive</action>"
                    Return str
                End If
            End If
            statusId = ApplicationSettings.StatusSuspendedCompany
            'Check for account Status- company
            If Not ds.Tables("companyBizDivLinkage") Is Nothing Then
                If ds.Tables("companyBizDivLinkage").Rows.Count > 0 Then
                    If ds.Tables("companyBizDivLinkage").Rows(0)("Status") = statusId Then
                        ' Company account is suspended
                        str = "<action>companySuspended</action>"
                        Return str
                    End If
                End If
            End If

            Dim dsRollgrp As DataSet = DBSecurity.GetRolesGroups("")
            dsRollgrp.Tables(0).DefaultView.RowFilter = "RoleGroupName = 'Administrator'"
            dv = dsRollgrp.Tables(0).DefaultView
            Dim adminFlag As Boolean = False
            Dim i As Integer = 0
            'Check for administrative privileges
            For i = 0 To dv.Count - 1
                If dv.Item(i)("RoleGroupID") = ds.Tables("contactRoleLinkage").Rows(0)("RoleGroupID") Then
                    adminFlag = True
                    Exit For
                End If
            Next
            ' User account dont have administrative privileges
            If adminFlag = False Then
                str = "<action>nonAdmin</action>"
                Return str
            End If

            'Action
            str &= "<action>Migrate</action>"
            'Company Name
            'ds.Tables("companyAttributes").DefaultView.RowFilter = "AttributeLabel = 'CompanyName'"
            If Not IsDBNull(ds.Tables("companyAttributes").Rows(0).Item("CompanyName")) Then
                str &= "<companyName>" & ds.Tables("companyAttributes").Rows(0).Item("CompanyName") & "</companyName>"
            Else
                str &= "<companyName>" & "" & "</companyName>"
            End If

            'Admin Name
            'ds.Tables("contactAttributes").DefaultView.RowFilter = "AttributeLabel = 'FName'"
            If Not IsDBNull(ds.Tables("contactAttributes").Rows(0).Item("FName")) Then
                adminName = ds.Tables("contactAttributes").Rows(0).Item("FName")
            End If
            'ds.Tables("contactAttributes").DefaultView.RowFilter = "AttributeLabel = 'LName'"
            If Not IsDBNull(ds.Tables("contactAttributes").Rows(0).Item("LName")) Then
                adminName &= "&nbsp;&nbsp;" & ds.Tables("contactAttributes").Rows(0).Item("LName")
            End If
            str &= "<admin>" & adminName & "</admin>"
            'Admin Email            
            str &= "<adminMail>" & Email & "</adminMail>"
            ' No of registered location
            'ds.Tables("companyAttributes").DefaultView.RowFilter = "AttributeLabel = 'NoOfLocations'"
            ' If ds.Tables("companyAttributes").DefaultView.Count > 0 Then
            If Not IsDBNull(ds.Tables("companyAttributes").Rows(0).Item("NoOfLocations")) Then
                str &= "<noRegLoc>" & ds.Tables("companyAttributes").Rows(0).Item("NoOfLocations") & "</noRegLoc>"
            Else
                str &= "<noRegLoc>" & "" & "</noRegLoc>"
            End If
            'Else
            '    str &= "<noRegLoc>" & "" & "</noRegLoc>"
            'End If

            'No of registered users    
            If Not IsDBNull(ds.Tables("spcialistCount").Rows(0)("TotalSpecialists")) Then
                str &= "<noRegUser>" & ds.Tables("spcialistCount").Rows(0)("TotalSpecialists") & "</noRegUser>"
            Else
                str &= "<noRegUser>" & "" & "</noRegUser>"
            End If
            'migrateLink
            str &= "<migrateLink>" & ApplicationSettings.WebPath & ApplicationSettings.OuterMigratePage & "?accountId=" & Email & "</migrateLink>"

            Return str
        Else
        End If
        Return str
    End Function

    ''' <summary>
    ''' Extracts the values from trhe XML tags
    ''' </summary>
    ''' <param name="XMLStr"></param>
    ''' <param name="atrName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getXMLElementValue(ByVal XMLStr As String, ByVal atrName As String)
        If XMLStr.IndexOf("<" + atrName + ">") > -1 Then
            Return XMLStr.Substring(XMLStr.IndexOf("<" + atrName + ">") + atrName.Length + 2, XMLStr.IndexOf("</" + atrName + ">") - XMLStr.IndexOf("<" + atrName + ">") - atrName.Length - 2)
        Else
            Return ""
        End If
    End Function

    ''' <summary>
    ''' Function to get the standards for the RAQ form and store them in cache
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRAQWorkRequestStandards(ByRef page As Page, ByVal BizDivId As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        Dim cacheKey As String = ""
        cacheKey = "Standard-RAQ-Session" & "-" & page.Session.SessionID
        If killcache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            ds = DBWorkOrder.woRAQGetStandards(BizDivId)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Common function to get the standards for the create workorder form
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCreateWorkRequestStandards(ByRef page As Page, ByVal BizDivId As Integer, ByVal ContactID As Integer, ByVal WOID As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        Dim cacheKey As String = ""
        cacheKey = "Standard-WOID" & WOID & "-Session" & "-" & page.Session.SessionID

        If killcache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            ds = DBWorkOrder.MS_WOCreateWOGetStandards(BizDivId, ContactID, WOID)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Common function to get the standards for the create workorder form
    ''' </summary>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCreateWorkRequestDetails(ByVal ContactID As Integer, ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        Dim ds As DataSet
        ds = DBWorkOrder.MS_WOCreateWOGetDetails(ContactID, WOID, CompanyID)
        Return ds
    End Function

    ''' <summary>
    ''' Common function to get locations for the workorder form
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="companyId"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetLocations(ByRef page As Page, ByVal companyId As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        Dim cacheKey As String = ""
        cacheKey = "Location-Company" & "-" & "-" & companyId & "-SessionID-" & page.Session.SessionID

        If killcache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            'ws.WSContact.Timeout = 300000
            ds = DBWorkOrder.woGetContactsLocations(companyId)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

#Region "AOE"
    ''' <summary>
    ''' To fetch  all category types defined in table "tblStandardsWOTypesLinkage"
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="BizDivID"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetMainCategoriesTypes(ByRef page As Page, ByVal BizDivID As Integer, Optional ByVal KillCache As Boolean = False) As DataSet
        Dim CacheKey As String = "MainCategoriesTypes" + CStr(BizDivID)
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = DBStandards.GetMainCategoryTypes(BizDivID)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function
    ''' <summary>
    ''' To fetch  all category types defined in table "tblStandardsWOTypesLinkage"
    ''' </summary> 
    ''' <param name="page"></param>
    ''' <param name="BizDivID"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSubCategoriesTypes(ByRef page As Page, ByVal mainCatId As Integer, ByVal BizDivId As Integer, Optional ByVal KillCache As Boolean = False) As DataSet
        Dim CacheKey As String = "SubCategoriesTypes" + CStr(mainCatId) + CStr(BizDivId)
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = DBStandards.GetSubCategoryTypes(mainCatId, BizDivId)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function
    ''' <summary>
    ''' To fetch  all category for a contact in bizdiv from tblcontacts attributes as combid
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="BizDivID"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetContactsCategories(ByRef page As Page, ByVal combIds As String, ByVal paramBizDivId As Integer, Optional ByVal KillCache As Boolean = False) As DataSet
        Dim CacheKey As String = "ContactsCategories" + page.Session.SessionID + CStr(paramBizDivId)
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = DBContacts.GetContactsCategories(combIds, paramBizDivId)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function
    ''' <summary>
    ''' To fetch   maincat ,subcat ,maincatname,subcatname for a combid in a bizdiv
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCategoryCombDetails(ByRef page As Page, ByVal combId As Integer, ByVal paramBizDivId As Integer, Optional ByVal KillCache As Boolean = False) As DataView
        Dim CacheKey As String = "BizDivCategories" + CStr(paramBizDivId)
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = DBContacts.GetCategoriesForBizDivId(paramBizDivId)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Dim dv As DataView = ds.Tables("tblCombIds").DefaultView
        dv.RowFilter = "CombID=" & combId
        Return dv
    End Function
#End Region

    ''' <summary>
    ''' Get the standard data for Ordermatch form: Regions and AOE
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="cacheKey"></param>
    ''' <param name="bizDivId"></param>
    ''' <param name="countryID"></param>
    ''' <param name="killCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function OrderMatch_GetStandards(ByRef page As Page, ByVal cacheKey As String, ByVal bizDivId As Integer, Optional ByVal countryID As Integer = 0, Optional ByVal killCache As Boolean = False) As DataSet
        If killCache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            ds = DBWorkOrder.woOrderMatchGetStandards(countryID, bizDivId)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function


    ''' <summary>
    ''' Common function to iterate through the grid and get the list of the selected ids...
    ''' </summary>
    ''' <param name="gv"></param>
    ''' <param name="checkboxId"></param>
    ''' <param name="hdnId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getSelectedIdsOfGrid(ByVal gv As GridView, ByVal checkboxId As String, ByVal hdnId As String) As String
        Dim selectedIds As String = ""
        For Each row As GridViewRow In gv.Rows
            Dim chkBox As CheckBox = CType(row.FindControl(checkboxId), CheckBox)
            If Not (chkBox Is Nothing) Then
                If chkBox.Checked Then
                    If selectedIds.Contains(CStr(CType(row.FindControl(hdnId), HiddenField).Value)) = False Then
                        If selectedIds = "" Then
                            selectedIds += "" & CStr(CType(row.FindControl(hdnId), HiddenField).Value)
                        Else
                            selectedIds += " ," & CStr(CType(row.FindControl(hdnId), HiddenField).Value)
                        End If
                    End If
                End If
            End If
        Next
        Return selectedIds
    End Function


    ''' <summary>
    ''' Function GetCommonStandards to fetch standards for no of employees ,Distance,Response time,partner level,Skills And certificate,Regions,Security Question,Country ,skills
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetGeneralStandards(ByRef page As Page, Optional ByVal killcache As Boolean = False) As DataSet
        Dim CacheKey As String
        CacheKey = "Lookup_GeneralStandards"
        If killcache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As New DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = DBStandards.GetGeneralStandards()
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Function to populate security question
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlQuestion"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateSecurityQues(ByVal page As Page, ByRef ddlQuestion As DropDownList, Optional ByRef ddlHrdAbt As DropDownList = Nothing)
        Dim dsStandards As New DataSet
        dsStandards = CommonFunctions.GetGeneralStandards(page, False)
        Dim dsView As DataView = dsStandards.Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel = 'SecurityQuest'"
        ddlQuestion.DataSource = dsView
        ddlQuestion.DataTextField = "StandardValue"
        ddlQuestion.DataValueField = "StandardID"
        ddlQuestion.DataBind()
        ddlQuestion.Items.Insert(0, New ListItem("Select Question", ""))

        If Not IsNothing(ddlHrdAbt) Then
            dsView = dsStandards.Tables(0).Copy.DefaultView
            dsView.RowFilter = "StandardLabel = 'HeardAboutUs'"
            ddlHrdAbt.DataSource = dsView
            ddlHrdAbt.DataTextField = "StandardValue"
            ddlHrdAbt.DataValueField = "StandardValue"
            ddlHrdAbt.DataBind()
            ddlHrdAbt.Items.Insert(0, New ListItem("Select", ""))
        End If
    End Sub

    ''' <summary>
    ''' Procedure to populate AOE Main Cat drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlAOE"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateAOEMainCat(ByVal page As Page, ByRef ddlAOE As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(3).Copy.DefaultView
        dsView.RowFilter = "BizDivId=" & ApplicationSettings.BizDivId
        ddlAOE.DataSource = dsView
        ddlAOE.DataTextField = "Name"
        ddlAOE.DataValueField = "MainCatID"
        ddlAOE.DataBind()
        ddlAOE.Items.Insert(0, New ListItem("Skills Category", ""))
    End Sub

    ''' <summary>
    ''' Procedure to populate AOE Main Cat drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlSkills"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateRegions(ByVal page As Page, ByRef ddlSkills As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(2).Copy.DefaultView
        ddlSkills.DataSource = dsView
        ddlSkills.DataTextField = "RegionName"
        ddlSkills.DataValueField = "RegionID"
        ddlSkills.DataBind()
        ddlSkills.Items.Insert(0, New ListItem("Region", ""))
    End Sub

    ''' <summary>
    ''' Class having methods to add and post the parameters to destination page
    ''' </summary>
    ''' <remarks></remarks>
    Public Class RemotePost
        Private Inputs As System.Collections.Specialized.NameValueCollection = New System.Collections.Specialized.NameValueCollection

        Public Url As String = ""
        Public Method As String = "post"
        Public FormName As String = "form1"
        ''' <summary>
        ''' Add parameters to post
        ''' </summary>
        ''' <param name="name"></param>
        ''' <param name="value"></param>
        ''' <remarks></remarks>
        Public Sub Add(ByVal name As String, ByVal value As String)
            Inputs.Add(name, value)
        End Sub
        ''' <summary>
        ''' senfds a post request to specified page
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Post()
            System.Web.HttpContext.Current.Response.Clear()
            System.Web.HttpContext.Current.Response.Write("<html><head>")
            System.Web.HttpContext.Current.Response.Write(String.Format("</head><body onload=""setTimeout('document.{0}.submit()', 1000)"">", FormName))
            System.Web.HttpContext.Current.Response.Write(String.Format("<form name=""{0}"" method=""{1}"" action=""{2}"" >", FormName, Method, Url))
            Dim i As Integer = 0
            Do While i < Inputs.Keys.Count
                System.Web.HttpContext.Current.Response.Write(String.Format("<input name=""{0}"" type=""hidden"" value=""{1}"">", Inputs.Keys(i), Inputs(Inputs.Keys(i))))
                i += 1
            Loop
            System.Web.HttpContext.Current.Response.Write(" Loading...<img height='16' width='16' src='" & ApplicationSettings.OrderWorkMyUKURL & "/Images/indicator.gif'/>")
            System.Web.HttpContext.Current.Response.Write("</form>")
            System.Web.HttpContext.Current.Response.Write("</body></html>")
            System.Web.HttpContext.Current.Response.End()
        End Sub
    End Class


    ''' <summary>
    ''' This functions accepts the destination site and page to login and redirects user
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="loginTo"></param>
    ''' <param name="redirectURL"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AutoLogin(ByVal page As Page, ByVal loginTo As Integer, ByVal redirectURL As String) As Boolean
        Dim myremotepost As RemotePost = New RemotePost
        Select Case ApplicationSettings.SiteType

            Case ApplicationSettings.siteTypes.admin
                Select Case ApplicationSettings.Country
                    Case ApplicationSettings.CountryUK
                        Select Case loginTo
                            Case ApplicationSettings.OWUKBizDivId
                                myremotepost.Url = ApplicationSettings.OrderWorkUKAdminURL & redirectURL
                            Case ApplicationSettings.SFUKBizDivId
                                myremotepost.Url = ApplicationSettings.SkillsFinderUKAdminURL & redirectURL
                        End Select
                    Case ApplicationSettings.CountryDE
                End Select

            Case ApplicationSettings.siteTypes.site
                Select Case ApplicationSettings.Country
                    Case ApplicationSettings.CountryUK
                        Select Case loginTo
                            Case ApplicationSettings.OWUKBizDivId
                                myremotepost.Url = ApplicationSettings.OrderWorkMyUKURL & "SecurePages/" + redirectURL
                            Case ApplicationSettings.SFUKBizDivId
                                myremotepost.Url = ApplicationSettings.SkillsFinderUKURL & "SecurePages/" + redirectURL
                        End Select
                    Case ApplicationSettings.CountryDE
                        Select Case loginTo
                            Case ApplicationSettings.OWDEBizDivId
                                myremotepost.Url = ApplicationSettings.OrderWorkDEURL & "SecurePages/" + redirectURL
                            Case ApplicationSettings.SFDEBizDivId
                                myremotepost.Url = ApplicationSettings.SkillsFinderDEURL & "SecurePages/" + redirectURL
                        End Select
                End Select
        End Select


        myremotepost.Add("userName", page.Session("UserName"))
        myremotepost.Add("password", page.Session("Password"))

        'Dim ds As DataSet
        'ds = DBSecurity.AuthenticateUser(page.Session("UserName"), page.Session("Password"), OrderWorkLibrary.ApplicationSettings.BizDivId, OrderWorkLibrary.ApplicationSettings.BusinessId)

        'If ds.Tables(0).Rows.Count > 0 Then
        '    'Maintain session for the following with the value as in dataset
        '    myremotepost.Add("CompanyId", ds.Tables(0).Rows(0).Item("CompanyId"))
        '    myremotepost.Add("CompanyName", ds.Tables(0).Rows(0).Item("CompanyName"))
        '    If Not IsDBNull(ds.Tables(0).Rows(0).Item("Logo")) Then
        '        myremotepost.Add("CompanyLogo", ds.Tables(0).Rows(0).Item("Logo"))
        '    Else
        '        myremotepost.Add("CompanyLogo", "")
        '    End If

        '    myremotepost.Add("UserId", ds.Tables(0).Rows(0).Item("UserID"))
        '    myremotepost.Add("UserName", page.Session("UserName"))
        '    myremotepost.Add("FirstName", ds.Tables(0).Rows(0).Item("FName"))
        '    myremotepost.Add("LastName", ds.Tables(0).Rows(0).Item("LName"))
        '    myremotepost.Add("SecQues", ds.Tables(0).Rows(0).Item("SecurityQues"))
        '    myremotepost.Add("SecAns", ds.Tables(0).Rows(0).Item("SecurityAns"))
        '    myremotepost.Add("Password", ds.Tables(0).Rows(0).Item("Password"))
        '    myremotepost.Add("ProductCount", ds.Tables(0).Rows(0).Item("ProductCount"))
        '    myremotepost.Add("BusinessArea", ds.Tables(0).Rows(0).Item("BusinessArea"))
        '    If Not IsDBNull(ds.Tables(0).Rows(0).Item("AcceptWOValueLimit")) Then
        '        myremotepost.Add("AcceptWOValueLimit", ds.Tables(0).Rows(0).Item("AcceptWOValueLimit"))
        '    End If

        '    If Not IsDBNull(ds.Tables(0).Rows(0).Item("SpendLimit")) Then
        '        myremotepost.Add("SpendLimit", ds.Tables(0).Rows(0).Item("SpendLimit"))
        '    End If

        '    myremotepost.Add("RoleGroupID", ds.Tables(0).Rows(0).Item("RoleGroupID"))
        '    myremotepost.Add("ContactClassID", ds.Tables(0).Rows(0).Item("ClassID"))
        '    If ds.Tables(0).Rows.Count > 1 Then
        '        myremotepost.Add("SecondaryClassID", ds.Tables(0).Rows(1).Item("ClassID"))
        '    End If
        '    myremotepost.Add("SubClassID", ds.Tables(0).Rows(0).Item("Status"))
        'End If
        page.Session.Abandon()
        page.Session.Clear()
        myremotepost.Post()

    End Function


    ''' <summary>
    ''' Procedure to populate communication preferences
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlCommunication"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateCommunicationPref(ByVal page As Page, ByRef ddlCommunication As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel = 'CommunicationPref'"
        ddlCommunication.DataSource = dsView
        ddlCommunication.DataTextField = "StandardValue"
        ddlCommunication.DataValueField = "StandardID"
        ddlCommunication.DataBind()
        ddlCommunication.Items.Insert(0, New ListItem(ResourceMessageText.GetString("SelectPreference"), ""))
    End Sub

    ''' <summary>
    ''' Procedure to populate Distance drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlTravelDistance"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateDistance(ByVal page As Page, ByRef ddlTravelDistance As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel = 'TravelDistance'"
        ddlTravelDistance.DataSource = dsView
        ddlTravelDistance.DataTextField = "StandardValue"
        ddlTravelDistance.DataValueField = "StandardID"
        ddlTravelDistance.DataBind()
        ddlTravelDistance.Items.Insert(0, New ListItem(ResourceMessageText.GetString("SelectTravelDistance"), ""))
    End Sub



    ''' <summary>
    ''' Procedure to populate role type i.e Administrator, Manager , Specialist , User.
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="dropDownListID"></param>
    ''' <param name="classID"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateUserType(ByVal page As Page, ByRef dropDownListID As DropDownList, ByVal classID As Integer)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(6).Copy.DefaultView
        dsView.RowFilter = "RoleCategory='" & IIf(classID = ApplicationSettings.RoleSupplierID, "Supplier", "Client") & "'"
        If Not IsNothing(HttpContext.Current.Session("BusinessArea")) Then
            If HttpContext.Current.Session("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                dropDownListID.DataSource = dsView
                dropDownListID.DataTextField = "RoleGroupName"
                dropDownListID.DataValueField = "RoleGroupID"
                dropDownListID.DataBind()
                dropDownListID.Items.Insert(0, New ListItem(OrderWorkLibrary.ResourceMessageText.GetString("SelectUserType"), ""))
            Else
                Dim dsUserType As DataView
                dsUserType = dsView.ToTable.DefaultView
                dsUserType.RowFilter = "RoleGroupID not in (" & ApplicationSettings.RoleClientDepotID & "," & ApplicationSettings.RoleSupplierDepotID & ")"
                dropDownListID.DataSource = dsUserType
                dropDownListID.DataTextField = "RoleGroupName"
                dropDownListID.DataValueField = "RoleGroupID"
                dropDownListID.DataBind()
                dropDownListID.Items.Insert(0, New ListItem(OrderWorkLibrary.ResourceMessageText.GetString("SelectUserType"), ""))
                For Each Item As ListItem In dropDownListID.Items
                    If Item.Text.ToString.ToLower = "retail" Then
                        Item.Text = "Support staff"
                    End If
                Next
            End If
        Else
            Dim dsUserType As DataView
            dsUserType = dsView.ToTable.DefaultView
            dsUserType.RowFilter = "RoleGroupID not in (" & ApplicationSettings.RoleClientDepotID & "," & ApplicationSettings.RoleSupplierDepotID & ")"
            dropDownListID.DataSource = dsUserType
            dropDownListID.DataTextField = "RoleGroupName"
            dropDownListID.DataValueField = "RoleGroupID"
            dropDownListID.DataBind()
            dropDownListID.Items.Insert(0, New ListItem(OrderWorkLibrary.ResourceMessageText.GetString("SelectUserType"), ""))
            For Each Item As ListItem In dropDownListID.Items
                If Item.Text.ToString.ToLower = "retail" Then
                    Item.Text = "Support staff"
                End If
            Next
        End If
    End Sub

    ''' <summary>
    ''' Procedure to populate locations of a particular company.
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="dropDownListID"></param>
    ''' <param name="companyID"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateLocationsofCompany(ByVal page As Page, ByRef dropDownListID As DropDownList, ByVal companyID As Integer, Optional ByVal killcache As Boolean = False)
        'Cache is deleted from location form
        Dim cachekey As String = "LocationNamesList-" & companyID
        If killcache = True Then
            page.Cache.Remove(cachekey)
        End If
        Dim ds As DataSet
        If page.Cache(cachekey) Is Nothing Then
            ds = DBContacts.GetLocationName(companyID)
            page.Cache.Add(cachekey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cachekey), DataSet)
        End If

        ' Filter deleted location 
        Dim dvLoc As DataView
        dvLoc = ds.Tables(0).DefaultView
        dvLoc.RowFilter = "IsDeleted <> 1"
        dropDownListID.DataSource = dvLoc
        dropDownListID.DataTextField = "Name"
        dropDownListID.DataValueField = "AddressID"
        dropDownListID.DataBind()
        dropDownListID.Items.Insert(0, New ListItem(OrderWorkLibrary.ResourceMessageText.GetString("SelectLocation"), ""))
    End Sub

    ''' <summary>
    ''' Procedure to populate other locations of a particular company.
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="dropDownListID"></param>
    ''' <param name="companyID"></param>
    ''' <remarks></remarks>
    Public Shared Function GetOtherLocationsofCompany(ByVal page As Page, ByVal companyID As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        'Cache is deleted from location form
        Dim cachekey As String = "LocationNamesList-" & companyID
        If killcache = True Then
            page.Cache.Remove(cachekey)
        End If
        Dim ds As DataSet
        If page.Cache(cachekey) Is Nothing Then
            ds = DBContacts.GetLocationName(companyID)
            page.Cache.Add(cachekey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cachekey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Procedure to populate user status drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="dropDownListID"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateUserStatus(ByVal page As Page, ByRef dropDownListID As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='Status'"
        dropDownListID.DataSource = dsView
        dropDownListID.DataTextField = "StandardValue"
        dropDownListID.DataValueField = "StandardID"
        dropDownListID.DataBind()
    End Sub

    ''' <summary>
    ''' Get contact ds - for storing in cache (for a single contact - returns all tables)
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="sessionid"></param>
    ''' <param name="ContactID"></param>
    ''' <param name="IsMainContact"></param>
    ''' <param name="CacheKey"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetContactsDS(ByRef page As Page, ByVal sessionid As String, Optional ByVal ContactID As Integer = 0, Optional ByVal IsMainContact As Boolean = False, Optional ByVal CacheKey As String = "", Optional ByVal KillCache As Boolean = False) As DataSet
        If CacheKey = "" Then
            CacheKey = "Contact" & "-" & CStr(ContactID) & "-" & sessionid
        End If
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            'ws.WSContact.Timeout = 300000
            ds = DBContacts.GetContact(ContactID, IsMainContact, ApplicationSettings.BizDivId)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Function to return Certification and qualification dataview
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="bizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCertQual(ByVal page As Page, ByVal bizDivID As Integer) As DataView
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(1).Copy.DefaultView
        dsView.RowFilter = "BizDivID = " & bizDivID
        Return dsView
    End Function

    ''' <summary>
    ''' Procedure to populate No Of Employees drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlNoOfEmployees"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateNoOfEmployees(ByVal page As Page, ByRef ddlNoOfEmployees As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='CompanyStrength'"
        ddlNoOfEmployees.DataSource = dsView
        ddlNoOfEmployees.DataTextField = "StandardValue"
        ddlNoOfEmployees.DataValueField = "StandardID"
        ddlNoOfEmployees.DataBind()
        ddlNoOfEmployees.Items.Insert(0, New ListItem(ResourceMessageText.GetString("SelectNoOfEmployees"), ""))
    End Sub




    ''' <summary>
    ''' Procedure to populate vehicle type
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="cblVehicleType"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateVehicleType(ByVal page As Page, ByRef cblVehicleTypeEdit As CheckBoxList, ByRef cblVehicleTypeView As CheckBoxList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel = 'Vehicle Type'"
        cblVehicleTypeEdit.DataSource = dsView
        cblVehicleTypeEdit.DataTextField = "StandardValue"
        cblVehicleTypeEdit.DataValueField = "StandardID"
        cblVehicleTypeEdit.DataBind()

        cblVehicleTypeView.DataSource = dsView
        cblVehicleTypeView.DataTextField = "StandardValue"
        cblVehicleTypeView.DataValueField = "StandardID"
        cblVehicleTypeView.DataBind()

        'cblVehicleType.DataSource = dsView
        'cblVehicleType.DataTextField = "StandardValue"
        'cblVehicleType.DataValueField = "StandardID"
        'cblVehicleType.DataBind()
    End Sub

    ''' <summary>
    ''' Procedure to populate Dress code in the drop down list
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlDressCode"></param>
    ''' <param name="dvView"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateDressCode(ByVal page As Page, ByRef ddlDressCode As DropDownList, ByVal dvView As DataView)
        dvView.RowFilter = "StandardLabel='DressCode'"
        ddlDressCode.DataSource = dvView
        ddlDressCode.DataTextField = "StandardValue"
        ddlDressCode.DataValueField = "StandardID"
        ddlDressCode.DataBind()
    End Sub


    ''' <summary>
    ''' Procedure to populate Service Response time  drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlResponseTime"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateServiceResponseTime(ByVal page As Page, ByRef ddlResponseTime As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='ServiceResponseTime'"
        ddlResponseTime.DataSource = dsView
        ddlResponseTime.DataTextField = "StandardValue"
        ddlResponseTime.DataValueField = "StandardID"
        ddlResponseTime.DataBind()
        ddlResponseTime.Items.Insert(0, New ListItem(ResourceMessageText.GetString("ServiceResponseTime"), ""))

    End Sub

    ''' <summary>
    ''' Procedure to populate No Of Employees drop down control 
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlpartner"></param>
    ''' <param name="ofPartner"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulatePartnerLevel(ByVal page As Page, ByRef ddlpartner As DropDownList, ByVal ofPartner As String)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='" & ofPartner & "PartnerLevel'"
        ddlpartner.DataSource = dsView
        ddlpartner.DataTextField = "StandardValue"
        ddlpartner.DataValueField = "StandardID"
        ddlpartner.DataBind()
        ddlpartner.Items.Insert(0, New ListItem(ResourceMessageText.GetString("SelectLevel"), ""))
    End Sub
    Public Shared Sub PopulateAdminUsers(ByVal page As Page, ByRef ddlOWAccountManager As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(7).Copy.DefaultView
        ddlOWAccountManager.DataSource = dsView
        ddlOWAccountManager.DataTextField = "Name"
        ddlOWAccountManager.DataValueField = "ContactID"
        ddlOWAccountManager.DataBind()
    End Sub

    ''' <summary>
    ''' Procedure to populate credit terms
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub PopulateCreditTerms(ByVal page As Page, ByRef ddlCreditTerms As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='CreditTerms'"
        ddlCreditTerms.DataSource = dsView
        ddlCreditTerms.DataTextField = "StandardValue"
        ddlCreditTerms.DataValueField = "StandardID"
        ddlCreditTerms.DataBind()
        ddlCreditTerms.Items.Insert(0, New ListItem("Select Payment Term", ""))
    End Sub

    ''' <summary>
    ''' Function to return regions dataview from standards
    ''' </summary>
    ''' <param name="page"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRegions(ByVal page As Page) As DataView
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(2).Copy.DefaultView
        Return dsView
    End Function

    Public Shared Function GetContactsDSAdmin(ByRef page As Page, ByVal ContactID As Integer, ByVal IsMainContact As Boolean, ByVal CacheKey As String, Optional ByVal KillCache As Boolean = False) As DataSet
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            'ws.WSContact.Timeout = 300000

            ds = DBContacts.AdminGetContactDetails(ContactID, IIf(IsMainContact, 1, 0), page.Session("BizDivId"))
            'ds = ws.WSAdmin.AdminGetContactDetails(ContactID, 1, BizDivID)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Populate industries in drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlIndustry"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateIndustries(ByVal page As Page, ByRef ddlIndustry As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(5).Copy.DefaultView
        ddlIndustry.DataSource = dsView
        ddlIndustry.DataTextField = "Name"
        ddlIndustry.DataValueField = "IndustryID"
        ddlIndustry.DataBind()
        ddlIndustry.Items.Insert(0, New ListItem(ResourceMessageText.GetString("SelectIndustry"), ""))
    End Sub

    Public Shared Function GetStandardsForLabel(ByVal page As Page, ByVal label As String) As DataView
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='" & label & "'"
        Return dsView
    End Function

    ''' <summary>
    ''' Procedure to populate Countries drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlCompanyCountry"></param>
    ''' <param name="defaultCountry"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateCountry(ByVal page As Page, ByRef ddlCompanyCountry As DropDownList, ByVal defaultCountry As String)

        Dim dv_country As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(4).Copy.DefaultView
        dv_country.Sort = "CountryName"
        ddlCompanyCountry.DataSource = dv_country
        ddlCompanyCountry.DataTextField = "CountryName"
        ddlCompanyCountry.DataValueField = "CountryID"
        ddlCompanyCountry.DataBind()

        'To show the default selected country
        dv_country.RowFilter = "CountryName = '" & defaultCountry & "'"
        Dim UKcountryId As String
        UKcountryId = dv_country.Item(0).Item("CountryID")
        ddlCompanyCountry.SelectedValue = CInt(UKcountryId)

    End Sub

#Region "WebContent"


    ''' <summary>
    ''' To fetch web content standards from database , it also fetch division
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="StandardLabel"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStandardsForWebContent(ByRef page As Page, Optional ByVal StandardLabel As String = "", Optional ByVal killcache As Boolean = False) As DataSet
        Dim CacheKey As String
        CacheKey = "GetStandardsAndBusiness"
        If killcache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = DBStandards.GetStandardsForWebContent(StandardLabel)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function
    Public Shared Function GetStandards(ByRef page As Page, Optional ByVal StandardLabel As String = "") As DataSet
        Dim ds As DataSet
        ds = DBContacts.GetStandards(StandardLabel)
        Return ds
    End Function

    ''' <summary>
    ''' Populate webcontent type in drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlWebContent"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateWebContentType(ByVal page As Page, ByRef ddlWebContent As DropDownList)
        Dim dvStandard As DataView = GetStandardsForWebContent(page).Tables(0).Copy.DefaultView
        dvStandard.RowFilter = "StandardLabel='WebContent'"
        ddlWebContent.DataSource = dvStandard
        ddlWebContent.DataTextField = "StandardValue"
        ddlWebContent.DataValueField = "StandardID"
        ddlWebContent.DataBind()
        ddlWebContent.Items.Insert(0, New ListItem(ResourceMessageText.GetString("SelectAllWebContent"), ""))
    End Sub

    ''' <summary>
    ''' Procedure to populate business per country
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlBusiness"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateBusiness(ByVal page As Page, ByRef ddlBusiness As DropDownList)
        Dim ds As DataSet
        ds = GetStandardsForWebContent(page)
        Dim dvBusiness As DataView = ds.Tables(1).Copy.DefaultView
        ddlBusiness.DataSource = dvBusiness
        ddlBusiness.DataTextField = "BusinessName"
        ddlBusiness.DataValueField = "BusinessID"
        ddlBusiness.DataBind()
        ddlBusiness.Items.Insert(0, New ListItem(ResourceMessageText.GetString("SelectSite"), ""))
        ddlBusiness.Items.Insert(1, New ListItem("All Site", "All"))
    End Sub



#End Region

#Region "PostcodeLookup"

    ''' <summary>
    ''' Get the list of addresses for the Given Postcode/Keyword
    ''' </summary>
    ''' <param name="keyword"></param>
    ''' <param name="lblErr"></param>
    ''' <param name="lstProperties"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Shared Function GetAddressList(ByVal keyword As String, ByRef lblErr As Label, ByRef lstProperties As ListBox)
    '    Dim IsValid As Boolean
    '    IsValid = False
    '    If ApplicationSettings.ValidatePostcodeUK = "True" Then
    '        If Regex.IsMatch(keyword.ToUpper, ApplicationSettings.ValidateExpressionPC) Then
    '            IsValid = True
    '        End If
    '    Else
    '        IsValid = True
    '    End If

    '    If IsValid Then
    '        Dim objLookup As New WSAddressLookup.LookupUK
    '        Dim objInterimResults 'As WSAddressLookup.InterimResults
    '        Dim objInterimResult 'As WSAddressLookup.InterimResult

    '        'Make the request
    '        Dim AccountKey As String = OrderWorkLibrary.ApplicationSettings.AccountKey
    '        Dim LicenceKey As String = OrderWorkLibrary.ApplicationSettings.LicenceKey
    '        objInterimResults = objLookup.ByPostcode(keyword, AccountKey, LicenceKey, "")

    '        'Tidy up
    '        objLookup.Dispose()


    '        'Copy the results into the list box
    '        If objInterimResults.IsError Then
    '            'Disable the List box
    '            lstProperties.Visible = False

    '            'Write the error message to the address label
    '            lblErr.Text = OrderWorkLibrary.ResourceMessageText.GetString("PostcodeLookupError")

    '        Else
    '            'Pass blank error string
    '            lblErr.Text = ""

    '            'Enable the List box
    '            lstProperties.Visible = True

    '            'Clear the items in the list
    '            lstProperties.Items.Clear()

    '            'Add the new items to the list
    '            If Not objInterimResults.Results Is Nothing Then
    '                For Each objInterimResult In objInterimResults.Results
    '                    lstProperties.Items.Add(New  _
    '                         ListItem(objInterimResult.Description, objInterimResult.Id))
    '                Next
    '            End If

    '        End If
    '    Else
    '        lstProperties.Visible = False
    '        lblErr.Text = OrderWorkLibrary.ResourceMessageText.GetString("PostcodeLookupError")
    '    End If
    '    Return Nothing
    'End Function

    ''' <summary>
    ''' Fetch the details of selected and populate the addresss in the form.
    ''' </summary>
    ''' <param name="lblErr"></param>
    ''' <param name="lstProperties"></param>
    ''' <param name="txtCompanyName"></param>
    ''' <param name="txtCompanyAddress"></param>
    ''' <param name="txtCity"></param>
    ''' <param name="txtCounty"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Shared Function PopulateAddress(ByRef lblErr As Label, ByVal PostCodeToCheck As String, ByRef lstProperties As ListBox, ByRef txtPostcode As TextBox, ByRef txtCompanyName As TextBox, ByRef txtCompanyAddress As TextBox, ByRef txtCity As TextBox, ByRef txtCounty As TextBox)
    '    Dim objLookup As New WSAddressLookup.LookupUK
    '    Dim objAddressResults As WSAddressLookup.AddressResults
    '    Dim objAddress As WSAddressLookup.Address

    '    'Make the request
    '    Dim AccountKey As String = OrderWorkLibrary.ApplicationSettings.AccountKey
    '    Dim LicenceKey As String = OrderWorkLibrary.ApplicationSettings.LicenceKey

    '    Dim PostCodeExists As Boolean = OrderWorkLibrary.DBWorkOrder.DoesPostCodeExists(PostCodeToCheck)

    '    If PostCodeExists Then
    '        objAddressResults = objLookup.FetchAddress(lstProperties.SelectedValue, _
    '           WSAddressLookup.enLanguage.enLanguageEnglish, _
    '           WSAddressLookup.enContentType.enContentStandardAddress, AccountKey, LicenceKey, "")
    '    Else
    '        objAddressResults = objLookup.FetchAddress(lstProperties.SelectedValue, _
    '           WSAddressLookup.enLanguage.enLanguageEnglish, _
    '           WSAddressLookup.enContentType.enContentGeographicAddress, AccountKey, LicenceKey, "")
    '    End If


    '    'Tidy up
    '    objLookup.Dispose()


    '    'Get the address
    '    If objAddressResults.IsError Then

    '        'Write the error message to the address label
    '        lblErr.Text = OrderWorkLibrary.ResourceMessageText.GetString("PostcodeLookupError")

    '        'Clear the labels
    '        txtCompanyAddress.Text = String.Empty
    '        txtCity.Text = String.Empty
    '        txtCounty.Text = String.Empty

    '    Else
    '        'Pass blank error string
    '        lblErr.Text = ""

    '        'Get the address
    '        objAddress = objAddressResults.Results(0)

    '        'Write the labels
    '        If objAddress.OrganisationName <> String.Empty Then
    '            txtCompanyName.Text = String.Empty
    '            txtCompanyName.Text = objAddress.OrganisationName
    '        End If
    '        txtCompanyAddress.Text = objAddress.Line1

    '        'Append Line 2 with a comma only if it exists
    '        If objAddress.Line2.ToString <> "" Then
    '            txtCompanyAddress.Text = txtCompanyAddress.Text & " ," & Chr(13) & objAddress.Line2
    '        End If

    '        'Append Line 3 with a comma only if it exists
    '        If objAddress.Line3.ToString <> "" Then
    '            txtCompanyAddress.Text = txtCompanyAddress.Text & " ," & Chr(13) & objAddress.Line3
    '        End If

    '        txtCity.Text = objAddress.PostTown
    '        txtCounty.Text = objAddress.County
    '        txtPostcode.Text = objAddress.Postcode

    '        'Clear any previous error messages
    '        lblErr.Text = String.Empty

    '        'Get PostCode GeoCode of the location
    '        Dim PostCode As String = objAddress.Postcode
    '        Dim PostCodeGeoCode As OrderWorkLibrary.WSAddressLookup.AddressGeographicData = objAddress.GeographicData
    '        If Not PostCodeExists Then
    '            getWOPostCodeGeoCode(PostCode, PostCodeGeoCode, objAddress)
    '        End If

    '    End If
    '    Return Nothing
    'End Function

    ''' <summary>
    ''' Extract the GeoCode info from the lookup done.
    ''' </summary>
    ''' <param name="PostCodeToCheck"></param>
    ''' <param name="PostCodeGeoCode"></param>
    ''' <remarks></remarks>
    'Public Shared Sub getWOPostCodeGeoCode(ByVal PostCodeToCheck As String, ByVal PostCodeGeoCode As OrderWorkLibrary.WSAddressLookup.AddressGeographicData, ByVal objAddress As OrderWorkLibrary.WSAddressLookup.Address)

    '    'DO NOT DELETE - FOR FUTURE USE - pratik

    '    Dim PostCodeExists As Boolean = False 'CURRENTLY, WE CHECK IN SP WHICH SAVES GEOCODES - pratik

    '    '*****************************************************************************************************************************
    '    'The following code checks the postcode associated with the address. If the geocode info of the postcode in present in the DB
    '    'then it is collected in the viewstate variables and can be used later. If the geocode info is not there with us, the code 
    '    'uses the saved coordinates which were fetched from the web service to get the addresses lookup.
    '    '*****************************************************************************************************************************

    '    If PostCodeExists = False Then
    '        Dim results 'As OrderWorkLibrary.WSAddressLookup.AddressGeographicData = PostCodeGeoCode
    '        Dim GridEastM As String = String.Empty
    '        Dim GridNorthM As String = String.Empty

    '        If Not IsNothing(results.GridEastM) Then
    '            If results.GridEastM <> 0 Then
    '                GridEastM = results.GridEastM
    '            Else
    '                GridEastM = "Invalid"
    '            End If
    '        Else
    '            GridEastM = "Invalid"
    '        End If

    '        If Not IsNothing(results.GridNorthM) Then
    '            If results.GridNorthM <> 0 Then
    '                GridNorthM = results.GridNorthM
    '            Else
    '                GridNorthM = "Invalid"
    '            End If
    '        Else
    '            GridNorthM = "Invalid"
    '        End If

    '        Dim dt As New DataTable("tblGeoCode")
    '        Dim drow As DataRow
    '        Dim col1 As New DataColumn("PostCode")
    '        Dim col2 As New DataColumn("GridEast")
    '        Dim col3 As New DataColumn("GridNorth")

    '        Dim col4 As New DataColumn("CompanyName")
    '        Dim col5 As New DataColumn("Address")
    '        Dim col6 As New DataColumn("City")
    '        Dim col7 As New DataColumn("County")

    '        dt.Columns.Add(col1)
    '        dt.Columns.Add(col2)
    '        dt.Columns.Add(col3)
    '        If objAddress.Line1 <> String.Empty Then
    '            dt.Columns.Add(col4)
    '            dt.Columns.Add(col5)
    '            dt.Columns.Add(col6)
    '            dt.Columns.Add(col7)
    '        End If
    '        drow = dt.NewRow
    '        drow("PostCode") = PostCodeToCheck
    '        drow("GridEast") = GridEastM
    '        drow("GridNorth") = GridNorthM

    '        'Insert Address details in the XML only if available
    '        If objAddress.Line1 <> String.Empty Then
    '            drow("CompanyName") = objAddress.OrganisationName
    '            drow("Address") = objAddress.Line1 & " " & objAddress.Line2 & " " & objAddress.Line3
    '            drow("City") = objAddress.PostTown
    '            drow("County") = objAddress.County
    '        End If

    '        dt.Rows.Add(drow)
    '        Dim dsWOGeoCode As New DataSet
    '        dt.TableName = "tblGeoCodes"
    '        dsWOGeoCode.Tables.Add(dt)
    '        InsertPostCodeCoordinates(dsWOGeoCode)
    '    End If

    'End Sub

    ''' <summary>
    ''' insert the coordinates of the postcode in the table tblGeoCodes
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <remarks></remarks>
    Public Shared Sub InsertPostCodeCoordinates(ByVal ds As DataSet)
        ds.DataSetName = "GeoCodes"
        Dim xmlContent As String = ds.GetXml
        OrderWorkLibrary.DBWorkOrder.InsertPostCodeCoordinates(xmlContent)
    End Sub

#End Region

#Region "DECommonFunctions"
    ''' <summary>
    ''' Common function to populate the workorder types
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlWOType"></param>
    ''' <param name="cacheKey"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function PopulateRAQWorkOrderType(ByVal page As Page, ByRef ddlWOType As DropDownList, ByVal cacheKey As String, ByVal BizDivId As Integer)

        Dim ds As DataSet
        ds = GetRAQStandards(page, cacheKey, BizDivId)
        Dim dv_WOType As DataView = ds.Tables("tblWorkOrderTypes").Copy.DefaultView
        dv_WOType.Sort = "Name"

        ddlWOType.DataTextField = "Name"
        ddlWOType.DataValueField = "CombId"
        ddlWOType.DataSource = dv_WOType
        ddlWOType.DataBind()

        Dim li As New ListItem
        li = New ListItem
        li.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOCategoryList")
        li.Value = ""
        ddlWOType.Items.Insert(0, li)
    End Function

    ''' <summary>
    ''' common function to get the security question and answers.
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlQuestion"></param>
    ''' <param name="cacheKey"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function PopulateRAQSecurityQuestions(ByVal page As Page, ByRef ddlQuestion As DropDownList, ByVal cacheKey As String, ByVal BizDivId As Integer)

        Dim ds As DataSet
        ds = GetRAQStandards(page, cacheKey, BizDivId)
        Dim dv_QA As DataView = ds.Tables("tblSecurityQuestion").Copy.DefaultView

        ddlQuestion.DataTextField = "StandardValue"
        ddlQuestion.DataValueField = "StandardID"
        ddlQuestion.DataSource = dv_QA
        ddlQuestion.DataBind()

        Dim li As New ListItem
        li = New ListItem
        li.Text = ResourceMessageText.GetString("SelectQuestion")
        li.Value = ""
        ddlQuestion.Items.Insert(0, li)
    End Function

    ''' <summary>
    ''' Common function to get the standards for the RAQ form
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="cacheKey"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRAQStandards(ByRef page As Page, ByVal cacheKey As String, ByVal BizDivId As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        If killcache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            ds = DBWorkOrder.woRAQGetStandards(BizDivId)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Common function to get the standards for the create workorder form
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="cacheKey"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWorkOrderStandards(ByRef page As Page, ByVal cacheKey As String, ByVal BizDivId As Integer, ByVal ContactID As Integer, ByVal WOID As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        If killcache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            ds = DBWorkOrder.woCreateWOGetStandards(BizDivId, ContactID, WOID)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

#End Region


    Public Shared Sub setCultureSettings(ByVal country As String, ByVal siteType As String)
        If siteType = OrderWorkLibrary.ApplicationSettings.siteTypes.admin Or country = OrderWorkLibrary.ApplicationSettings.CountryUK Then
            Dim cl As New System.Globalization.CultureInfo("en-GB")
            System.Threading.Thread.CurrentThread.CurrentCulture = cl
            System.Threading.Thread.CurrentThread.CurrentUICulture = cl
        ElseIf siteType <> OrderWorkLibrary.ApplicationSettings.siteTypes.admin And country = OrderWorkLibrary.ApplicationSettings.CountryDE Then
            Dim cl As New System.Globalization.CultureInfo("de-DE")
            System.Threading.Thread.CurrentThread.CurrentCulture = cl
            System.Threading.Thread.CurrentThread.CurrentUICulture = cl
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = "."
        End If
    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlAccountType"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateAccount(ByVal page As Page, ByRef ddlAccountType As DropDownList)
        Dim ds As DataSet
        ds = GetStandardsForWebContent(page)
        Dim dvAccount As DataView = ds.Tables(2).Copy.DefaultView
        ddlAccountType.DataSource = dvAccount
        ddlAccountType.DataTextField = "Name"
        ddlAccountType.DataValueField = "ClassID"
        ddlAccountType.DataBind()
        ddlAccountType.Items.Insert(0, New ListItem("None", ""))
    End Sub

    Public Shared Function convertDate(ByVal dateToConvert As Date) As String
        Dim converteddate As String
        converteddate = CType(dateToConvert.Year, String) & "/" & CType(dateToConvert.Day, String) & "/" & CType(dateToConvert.Month, String)
        Return converteddate
    End Function

    Public Shared Function GetFileIcon(ByVal fileName As String) As String

        Dim extn = fileName.Substring(fileName.LastIndexOf(".") + 1)
        Dim imgsrc As String
        Select Case extn.ToString.ToLower
            Case "doc"
                imgsrc = "../OWLibrary/images/icons/icon-word.jpg"
            Case "jpg", "jpeg", "gif", "png"
                imgsrc = "../OWLibrary/images/icons/icon-image.jpg"
            Case "pdf"
                imgsrc = "../OWLibrary/images/icons/icon-pdf.jpg"
            Case "xls"
                imgsrc = "../OWLibrary/images/icons/icon-excel.jpg"
            Case Else
                imgsrc = "../OWLibrary/images/icons/icon-attachement.jpg"
        End Select
        Return imgsrc

    End Function

    Public Shared Function WithdrawFunds(ByVal companyID As Integer, ByVal bizDivID As Integer, ByVal invoiceNo As String, ByVal username As String, ByVal name As String, ByVal UserId As Integer, Optional ByVal adviceno As String = "") As String
        Dim ds As New DataSet
        ds = DBFinance.MS_GetBankDetails(companyID, bizDivID)
        If ds.Tables("tblBankDetails").Rows.Count <> 0 Then
            Dim drow As DataRow = ds.Tables("tblBankDetails").Rows(0)
            If IsDBNull(drow("SortCode")) And Not IsDBNull(drow("AccountNo")) And Not IsDBNull(drow("AccountName")) Then
                Return ResourceMessageText.GetString("WithdrawFundsBankDetails")
            End If
        Else
            Return ResourceMessageText.GetString("WithdrawFundsBankDetails")
        End If


        Dim invoiceNumbers As String = invoiceNo.ToString

        ds = DBFinance.MSAccounts_ChangePIStatusToRequested(bizDivID, invoiceNumbers, companyID, UserId)
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                If .Item("Success") = 0 Then
                    Return OrderWorkLibrary.ResourceMessageText.GetString("DBUpdateFail")
                Else
                    Emails.SendWithdrawFundsMail(Strings.FormatDateTime(Date.Today, DateFormat.ShortDate), "", .Item("ValueWithdrawn"), .Item("WithdrawBalance"), username, name, .Item("CompanyName"), adviceno)
                    Return "Success"
                End If
            End With
        Else
            Return OrderWorkLibrary.ResourceMessageText.GetString("DBUpdateFail")
        End If
    End Function


    Public Shared Function AddSpecialistUser(ByVal bizDivID As Integer, ByVal AddressID As Integer, ByVal classID As Integer, _
                                                ByVal companyID As Integer, ByVal userID As Integer, ByVal fName As String, ByVal lName As String)

        Dim strMode As String
        strMode = "add"

        Dim contactid = 0

        Dim dsContacts As New XSDContacts

        Dim strAppend As String
        Select Case bizDivID
            Case ApplicationSettings.OWUKBizDivId
                strAppend = "OW"
            Case ApplicationSettings.SFUKBizDivId
                strAppend = "MS"
            Case ApplicationSettings.OWDEBizDivId
                strAppend = "DE"
        End Select

        Dim nrow As XSDContacts.ContactsRow = dsContacts.Contacts.NewRow
        With nrow
            .ContactID = contactid
            '.ContactType = "Person"
            .MainContactID = companyID
            .Src = "OrderWork"
            .IsDeleted = False
            .IsCompany = False
            .Fname = fName
            .Lname = lName
            .FullName = fName + " " + lName
            .FulFilWOs = 1
        End With
        dsContacts.Contacts.Rows.Add(nrow)

        'For updating tblClassesLinkage
        Dim nrowClass As XSDContacts.ContactsSetupRow = dsContacts.ContactsSetup.NewRow
        With nrowClass
            .BizDivID = bizDivID
            .ClassID = classID
            .ContactID = contactid
            .Status = 1
        End With
        dsContacts.ContactsSetup.Rows.Add(nrowClass)

        'For updating tblRolesContactLinkage
        Dim nrowRClink As XSDContacts.tblRolesContactLinkageRow = dsContacts.tblRolesContactLinkage.NewRow
        With nrowRClink
            .ContactID = contactid
            .BizDivID = bizDivID
            .RoleGroupID = ApplicationSettings.RoleSupplierSpecialistID
            .IsDefault = 0
            .IsActive = 0
            .AddressID = AddressID
        End With
        dsContacts.tblRolesContactLinkage.Rows.Add(nrowRClink)

        ''For updating
        'Dim nrowCBizDivL As XSDContacts.tblContactsBizDivLinkageRow = dsContacts.tblContactsBizDivLinkage.NewRow
        'With nrowCBizDivL
        '    .BizDivID = bizDivID
        '    .ContactID = contactid
        '    .IsDefault = 1
        '    .Status = ApplicationSettings.StatusActive
        'End With
        'dsContacts.tblContactsBizDivLinkage.Rows.Add(nrowCBizDivL)


        Dim nrowCL As XSDContacts.tblContactsLoginRow = dsContacts.tblContactsLogin.NewRow
        With nrowCL
            .ContactID = contactid
            .UserName = ""
            .SecurityQues = 0
            .SecurityAns = ""
            .EmailVerified = False
            .NewsletterStatus = 0
            .EnableLogin = False
        End With
        dsContacts.tblContactsLogin.Rows.Add(nrowCL)

        'For updating tblContactsAttributes
        'Save First Name
        Dim nrowCA As XSDContacts.ContactsAttributesRow = dsContacts.ContactsAttributes.NewRow
        With nrowCA
            .ContactID = contactid

            '.AttributeLabel = "FName"
            '.AttributeValue = fName
            '.Sequence = 0
        End With
        dsContacts.ContactsAttributes.Rows.Add(nrowCA)

        ''Save Last Name
        'Dim nrowCLN As XSDContacts.ContactsAttributesRow = dsContacts.ContactsAttributes.NewRow
        'With nrowCLN
        '    .ContactID = 1111111111
        '    '.AttributeLabel = "LName"
        '    '.AttributeValue = lName
        '    '.Sequence = 0
        'End With
        'dsContacts.ContactsAttributes.Rows.Add(nrowCLN)

        ''Save "Job Title"
        'Dim nrowCJT As XSDContacts.ContactsAttributesRow = dsContacts.ContactsAttributes.NewRow
        'With nrowCJT
        '    .ContactID = contactid
        '    '.AttributeLabel = "JobTitle"
        '    '.AttributeValue = ""
        '    '.Sequence = 0
        'End With
        'dsContacts.ContactsAttributes.Rows.Add(nrowCJT)

        ''Save Other Email Address
        'Dim nrowOEAdd As OWContacts.tblContactsMethodsRow = dsContacts.tblContactsMethods.NewRow
        'With nrowOEAdd
        '    .ContactID = contactid
        '    .AddressID = AddressID
        '    .Type = "OtherEmail"
        '    .Info = ""
        '    .Sequence = 1
        '    .IsDefault = 0
        'End With
        'dsContacts.tblContactsMethods.Rows.Add(nrowOEAdd)


        ''Save Phone Number
        'Dim nrowPhone As OWContacts.tblContactsMethodsRow = dsContacts.tblContactsMethods.NewRow
        'With nrowPhone
        '    .ContactID = contactid
        '    .AddressID = AddressID
        '    .Type = "Phone"
        '    .Info = ""
        '    .Sequence = 1
        '    .IsDefault = 0
        'End With
        'dsContacts.tblContactsMethods.Rows.Add(nrowPhone)

        ''Save Mobile Number
        'Dim nrowMobile As OWContacts.tblContactsMethodsRow = dsContacts.tblContactsMethods.NewRow
        'With nrowMobile
        '    .ContactID = contactid
        '    .AddressID = AddressID
        '    .Type = "Mobile"
        '    .Info = ""
        '    .Sequence = 1
        '    .IsDefault = 0
        'End With
        'dsContacts.tblContactsMethods.Rows.Add(nrowMobile)

        ''Save Fax Number
        'Dim nrowFax As OWContacts.tblContactsMethodsRow = dsContacts.tblContactsMethods.NewRow
        'With nrowFax
        '    .ContactID = contactid
        '    .AddressID = AddressID
        '    .Type = "Fax"
        '    .Info = ""
        '    .Sequence = 1
        '    .IsDefault = 0
        'End With
        'dsContacts.tblContactsMethods.Rows.Add(nrowFax)

        'Saving "Communication Preference"
        Dim nrowCP As XSDContacts.ContactsSettingsRow = dsContacts.ContactsSettings.NewRow
        With nrowCP
            .ContactID = contactid
            '.SettingLabel = "CommunicationPref"
            '.SettingValue = ""
            '.Sequence = 0
        End With
        dsContacts.ContactsSettings.Rows.Add(nrowCP)


        ''Saving("Location" Or "Contact for Location")
        'dsContacts.ContactsSettings.Rows.Add(New String() {companyID, 0, contactid, False})

        ''Saving "Receive WO Notification"
        'Dim nrowRNot As XSDContacts.ContactsSettingsRow = dsContacts.ContactsSettings.NewRow
        'With nrowRNot
        '    .ContactID = contactid
        '    '.SettingLabel = "ReceiveWONotif"
        '    '.SettingValue = "No"
        '    '.Sequence = 0
        'End With
        'dsContacts.ContactsSettings.Rows.Add(nrowRNot)

        ''Saving "Can fulfil Work Orders?"
        'Dim nrowFFWO As XSDContacts.ContactsSettingsRow = dsContacts.ContactsSettings.NewRow
        'With nrowFFWO
        '    .ContactID = contactid
        '    '.SettingLabel = "FulFilWOs"
        '    '.SettingValue = "Yes"
        '    '.Sequence = 0
        'End With
        'dsContacts.ContactsSettings.Rows.Add(nrowFFWO)

        Dim nrowALog As XSDContacts.tblActionLogRow = dsContacts.tblActionLog.NewRow
        With nrowALog
            .LogID = 1
            .BizDivID = bizDivID
            .ActionID = ApplicationSettings.ActionLogID.Activate
            .ActionByCompany = companyID
            .ActionByContact = userID
            .ActionForCompany = companyID
            .ActionForContact = 0
            .ActionRef = 0
            .ActionSrc = ""
            ._Date = System.DateTime.Now()

        End With
        dsContacts.tblActionLog.Rows.Add(nrowALog)

        Return dsContacts
    End Function

    ''' <summary>
    ''' Used to Redirect to the Corresponding Listing page if the user is not authorised to see the data.
    ''' </summary>
    ''' <param name="Page"></param>
    ''' <remarks>Pratik Trivedi - 5 Aug, 2008</remarks>
    Public Shared Sub ErrorAccessDenied(ByVal Page As Page, ByVal ajaxErrorMessage As String, Optional ByVal URL As String = "", Optional ByVal Target As String = "")
        OrderWorkLibrary.Emails.SendMailURLTamperError(Page.Session("PortalUserId"), Page.Session("PortalCompanyId"), ajaxErrorMessage, URL)
        Page.Response.Redirect(Target & "&err=1")
    End Sub
    ''' <summary>
    ''' Common function to get Billing locations for the workorder form
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="companyId"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetBillingLocations(ByRef page As Page, ByVal companyId As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        Dim cacheKey As String = ""
        cacheKey = "BillingLocation-Company" & "-" & "-" & companyId & "-SessionID-" & page.Session.SessionID

        If killcache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            'DBContacts.timeout = 300000
            ds = DBWorkOrder.woGetContactsBillingLocations(companyId)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

    Public Shared Function createLog(ByVal message As String)
        Dim logger As log4net.ILog
        Dim str As String
        str = "" & Chr(13) & "" & Chr(10) & "Log Entry : "
        str = str & ApplicationSettings.WebPath & "" & Chr(13)
        str = str & DateTime.Now.ToLongTimeString() & DateTime.Now.ToLongDateString() & "" & Chr(13)
        str = str & "Log Message " & message & Chr(9) & "" & Chr(13)
        str = str & "--------------------------------------------------------------------------"
        logger = log4net.LogManager.GetLogger("File")
        logger.Info(str)
        Return Nothing
    End Function

    ''' <summary>
    ''' Leena T. 12-Nov-2009
    ''' This function will save the version no in the session for that page
    ''' </summary>
    ''' <param name="dtVer"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Shared Sub StoreVerNum(ByVal dtVer As DataTable)
        Dim VerdvGet As DataView
        If Not IsNothing(dtVer) Then
            VerdvGet = dtVer.DefaultView
            HttpContext.Current.Session("VerNumArray") = VerdvGet.Item(0)("VersionNo")
        End If
    End Sub

    ''' <summary>
    ''' Leena T. 12-Nov-2009
    ''' This fucntion will retrieve the version no from the session for that page 
    ''' </summary>
    ''' <param name="page"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FetchVerNum() As Byte()
        Dim VerNum As Byte()
        If Not IsNothing(HttpContext.Current.Session("VerNumArray")) Then
            VerNum = CType(HttpContext.Current.Session("VerNumArray"), Byte())
        End If
        Return VerNum
    End Function
    ''' <summary>
    ''' Leena T. 12-Nov-2009
    ''' This function will save the version no for the WO Tracking in the session for that page
    ''' </summary>
    ''' <param name="dtVer"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Shared Sub StoreWOTrackingVerNum(ByVal dvVer As DataView)
        ' Dim VerdvGet As DataView
        If Not IsNothing(dvVer) Then
            'VerdvGet = dvVer.DefaultView
            HttpContext.Current.Session("WOTrackingVerNumArray") = dvVer.Item(0)("WOTrackingVersionNo")
        End If
    End Sub
    ''' <summary>
    ''' Leena T. 12-Nov-2009
    ''' This fucntion will retrieve the version no from the session for that page 
    ''' </summary>
    ''' <param name="page"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FetchWOTrackingVerNum() As Byte()
        Dim VerNum As Byte()
        If Not IsNothing(HttpContext.Current.Session("WOTrackingVerNumArray")) Then
            VerNum = CType(HttpContext.Current.Session("WOTrackingVerNumArray"), Byte())
        End If
        Return VerNum
    End Function
    ''' <summary> 
    ''' method to ensure the provided string contains only valid 
    ''' characters 
    ''' <param name="inputStr">string we want to check</param> 
    ''' </summary> 
    Public Shared Function IsValidAlphaNumeric(ByVal inputStr As String) As Boolean
        'make sure the user provided us with data to check 
        'if not then return false 
        If String.IsNullOrEmpty(inputStr) Then
            Return False
        End If

        'now we need to loop through the string, examining each character 
        For i As Integer = 0 To inputStr.Length - 1
            'if this character isn't a number then return false 
            If Not (Char.IsLetter(inputStr(i))) Then
                Return False
            End If
        Next
        'we made it this far so return true 
        Return True
    End Function
    Public Shared Function GetDSFromXML(ByVal strXML As String) As DataSet
        Dim ds As New DataSet
        Dim xmlread1 As New System.Xml.XmlTextReader(strXML, XmlNodeType.Document, Nothing)
        ds.ReadXml(xmlread1)
        Return ds
    End Function

    Public Shared Function ReplaceCSScriptingData(ByVal inputStr As String) As String
        Dim returnStr As String
        returnStr = inputStr.Replace("<", "").Replace(">", "")
        Return returnStr
    End Function


    Public Shared Function RemoveDuplicates(ByVal items As String()) As String()
        Dim noDupsArrList As New ArrayList()
        For i As Integer = 0 To items.Length - 1
            If Not noDupsArrList.Contains(items(i).Trim()) Then
                noDupsArrList.Add(items(i).Trim())
            End If
        Next

        Dim uniqueItems As String() = New String(noDupsArrList.Count - 1) {}
        noDupsArrList.CopyTo(uniqueItems)
        Return uniqueItems
    End Function

    Public Shared Function SendStatusUpdateToSONY(ByVal RefWOID As String, ByVal PONumber As String, ByVal Status As String, ByVal StartDate As String)

        Dim ds_WOSuccessForSONY As DataSet
        Dim strStatusUpdateURL As String = ""

        Dim dt As DateTime = StartDate
        Dim strDate As String
        strDate = dt.ToString("yyy-MM-dd")

        If Status.ToLower = "appmd" Then
            strStatusUpdateURL = ApplicationSettings.SONYStatusUpdateURL & PONumber & "/" & Status & "?remark=" & strDate

        ElseIf Status.ToLower = "execu" Then
            strStatusUpdateURL = ApplicationSettings.SONYStatusUpdateURL & PONumber & "/" & Status & "?remark=" & strDate

        ElseIf Status.ToLower = "claim" Then
            strStatusUpdateURL = ApplicationSettings.SONYStatusUpdateURL & PONumber & "/CLAIM"
        End If

        Try
            Dim myHttpWebRequest As HttpWebRequest
            Dim myHttpWebResponse As HttpWebResponse
            myHttpWebRequest = HttpWebRequest.Create(strStatusUpdateURL)
            myHttpWebRequest.Credentials = New System.Net.NetworkCredential(ApplicationSettings.SONYUserName, ApplicationSettings.SONYPassword)
            myHttpWebRequest.Method = "GET"

            myHttpWebResponse = myHttpWebRequest.GetResponse()

            If myHttpWebResponse.StatusCode = HttpStatusCode.OK Then
                ds_WOSuccessForSONY = DBWorkOrder.InsertURLforSONY(RefWOID, strStatusUpdateURL, "Success")
            Else
                ds_WOSuccessForSONY = DBWorkOrder.InsertURLforSONY(RefWOID, strStatusUpdateURL, "Failed")
            End If

            myHttpWebResponse.Close()
        Catch ex As Exception
            ds_WOSuccessForSONY = DBWorkOrder.InsertURLforSONY(RefWOID, strStatusUpdateURL, "Failed")
        End Try
    End Function

    ' Poonam modified on 6/1/2016 - Task - OM-36 : OM - Add Emergency Contact fields in User detail page My Portal
    Public Shared Sub PopulateRelationType(ByVal page As Page, ByRef dropDownListID As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='Relationship Type'"
        dropDownListID.DataSource = dsView
        dropDownListID.DataTextField = "StandardValue"
        dropDownListID.DataValueField = "StandardID"
        dropDownListID.DataBind()
        dropDownListID.Items.Insert(0, New ListItem("Select Relation type", ""))
    End Sub


    Public Shared Function AutoLoginUser(ByVal page As Page, ByVal redirectURL As String) As Boolean
        Dim myremotepost As RemotePost = New RemotePost
        myremotepost.Url = ApplicationSettings.OrderWorkMyUKURL & "SecurePages/" + redirectURL

        myremotepost.Add("userName", page.Session("UserName"))
        myremotepost.Add("password", page.Session("Password"))

        page.Session.Abandon()
        page.Session.Clear()
        myremotepost.Post()

    End Function


#Region "GetAddressAPI"

    Public Shared Function callGetAddressList(ByVal PostCode As String, ByRef lblErr As Label, ByRef lstProperties As ListBox, ByRef txtPostCode As TextBox)
        Dim IsValid As Boolean
        IsValid = False

        If ApplicationSettings.ValidatePostcodeUK = "True" Then
            If Regex.IsMatch(PostCode.ToUpper, ApplicationSettings.ValidateExpressionPC) Then
                IsValid = True
            End If
        Else
            IsValid = False
        End If

        If IsValid Then
            Dim result As String = ""
            Dim URL = System.Configuration.ConfigurationManager.AppSettings("GetAddressAPI").ToString + PostCode + "?api-key=" + System.Configuration.ConfigurationManager.AppSettings("GetAddressAPIAccessKey").ToString().Trim()

            'Create the web request  
            Dim request As HttpWebRequest = TryCast(WebRequest.Create(URL), HttpWebRequest)
            request.Method = "GET"
            request.ContentLength = 0



            Try
                'Get response  
                ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
                Using response As HttpWebResponse = TryCast(request.GetResponse(), HttpWebResponse)
                    ' Get the response stream  
                    Dim reader As New StreamReader(response.GetResponseStream())

                    ' Read the whole contents and return as a string  
                    result = reader.ReadToEnd()
                    'result = result.Substring(1, result.Length - 2)

                End Using

            Catch ex As Exception
                lblErr.Text = OrderWorkLibrary.ResourceMessageText.GetString("PostcodeLookupError")
                lstProperties.Visible = False
                result = ""
                Return 0
            End Try

            'Copy the results into the list box
            If result = "" Then
                'Disable the List box
                lstProperties.Visible = False

                'Write the error message to the address label
                lblErr.Text = OrderWorkLibrary.ResourceMessageText.GetString("PostcodeLookupError")

            Else
                'Pass blank error string
                lblErr.Text = ""

                'Enable the List box
                lstProperties.Visible = True

                'Clear the items in the list
                lstProperties.Items.Clear()

                ' Deserialize the Json data into an object
                Dim details As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.JsonConvert.DeserializeObject(result)

                Dim latitude As String = Convert.ToString(details.Property("latitude").Value)
                Dim longitude As String = Convert.ToString(details.Property("longitude").Value)

                'HttpContext.Current.Session("latitude") = latitude
                'HttpContext.Current.Session("longitude") = longitude

                'If PostCode <> "" Then
                '    If Not PostCode.Contains(" ") Then
                '        '        PostCode = PostCode.Substring(0, 2) + " " + PostCode.Substring(2)
                '        lstProperties.Visible = False
                '        lblErr.Text = "Please ensure that you have entered your postcode in the correct format (with a space in the middle)"
                '        Return Nothing
                '    End If
                '    PostCode = PostCode.ToUpper()
                'End If

                If PostCode <> "" Then
                    If Not PostCode.Contains(" ") Then
                        '        PostCode = PostCode.Substring(0, 2) + " " + PostCode.Substring(2)
                        'PostCode = PostCode.Insert(PostCode.Length - 3, " ")
                        txtPostCode.Text = PostCode
                        'lblErr.Text = "Please ensure that you have entered your postcode in the correct format (with a space in the middle)"
                        'Return Nothing
                    End If
                    PostCode = PostCode.ToUpper()
                End If
                HttpContext.Current.Session("PostCode") = PostCode

                Dim ds As DataSet = DBWorkOrder.UpdatePostCodeLatLong(PostCode, latitude, longitude)


                Dim addresses = details.Value(Of JArray)("addresses").Values(Of String)()

                'Add the new items to the list
                For Each obj As String In addresses
                    Dim location() As String = obj.ToString().Split(",")
                    Dim text As String = ""
                    For Each value As String In location
                        If value.Trim() <> "" Then
                            If text.Trim() = "" Then
                                text = value
                            Else
                                text = text + "," + value
                            End If

                        End If
                    Next
                    lstProperties.Items.Add(New  _
                            System.Web.UI.WebControls.ListItem(text, obj))
                Next
            End If
        Else
            lstProperties.Visible = False
            lblErr.Text = OrderWorkLibrary.ResourceMessageText.GetString("PostcodeLookupError")
        End If
        Return Nothing
    End Function

    Public Shared Function callPopulateAddress(ByRef lblErr As Label, ByVal strSelectedItem As String, ByRef lstProperties As ListBox, ByRef txtPostcode As TextBox, ByRef txtCompanyName As TextBox, ByRef txtCompanyAddress As TextBox, ByRef txtCity As TextBox, ByRef txtCounty As TextBox)
        Dim SelectedAddress() As String = strSelectedItem.Trim().Split(",")
        txtCompanyAddress.Text = String.Empty
        txtCity.Text = String.Empty
        txtCounty.Text = String.Empty
        lblErr.Text = ""

        'If SelectedAddress(0).Trim() <> "" Then
        '    txtCompanyName.Text = SelectedAddress(0)
        'End If

        'If SelectedAddress(1).Trim() <> "" And SelectedAddress(2).Trim() <> "" Then
        '    txtCompanyAddress.Text = SelectedAddress(1).Trim() & "," & Chr(13) & SelectedAddress(2).Trim()
        'ElseIf SelectedAddress(1).Trim() <> "" Then
        '    txtCompanyAddress.Text = SelectedAddress(1).Trim()
        'Else
        '    txtCompanyAddress.Text = SelectedAddress(0).Trim()
        'End If

        txtCompanyAddress.Text = ""
        For index As Integer = 0 To 4
            If SelectedAddress(index).Trim() <> "" Then
                If txtCompanyAddress.Text = "" Then
                    txtCompanyAddress.Text = SelectedAddress(index).Trim()
                Else
                    txtCompanyAddress.Text = txtCompanyAddress.Text + "," + SelectedAddress(index)
                End If
            End If
        Next

        txtPostcode.Text = HttpContext.Current.Session("PostCode")
        txtCity.Text = SelectedAddress(5)
        txtCounty.Text = SelectedAddress(6)

        Return Nothing
    End Function

    Public Shared Function GetPostcodeLatLong(ByVal PostCode As String, Optional ByVal mode As String = "")
        Dim IsValid As Boolean
        IsValid = False

        If ApplicationSettings.ValidatePostcodeUK = "True" Then
            If Regex.IsMatch(PostCode.ToUpper, ApplicationSettings.ValidateExpressionPC) Then
                IsValid = True
            End If
        Else
            IsValid = True
        End If

        If IsValid Then
            Dim result As String = ""
            Dim URL = System.Configuration.ConfigurationManager.AppSettings("GetAddressAPI").ToString + PostCode + "?api-key=" + System.Configuration.ConfigurationManager.AppSettings("GetAddressAPIAccessKey").ToString().Trim()

            'Create the web request  
            Dim request As HttpWebRequest = TryCast(WebRequest.Create(URL), HttpWebRequest)
            request.Method = "GET"
            request.ContentLength = 0


            'Get response  
            ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
            Using response As HttpWebResponse = TryCast(request.GetResponse(), HttpWebResponse)
                ' Get the response stream  
                Dim reader As New StreamReader(response.GetResponseStream())

                ' Read the whole contents and return as a string  
                result = reader.ReadToEnd()
                'result = result.Substring(1, result.Length - 2)

            End Using

            ' Deserialize the Json data into an object
            Dim details As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.JsonConvert.DeserializeObject(result)

            Dim latitude As String = Convert.ToString(details.Property("latitude").Value)
            Dim longitude As String = Convert.ToString(details.Property("longitude").Value)

            'HttpContext.Current.Session("latitude") = latitude
            'HttpContext.Current.Session("longitude") = longitude

            'If PostCode <> "" Then
            '    If PostCode.Trim().IndexOf(" ") <> 2 Then
            '        PostCode = PostCode.Substring(0, 2) + " " + PostCode.Substring(2)
            '    End If
            PostCode = PostCode.ToUpper()
            'End If

            If (mode <> "XLS") Then
                HttpContext.Current.Session("PostCode") = PostCode
            End If

            Dim ds As DataSet = DBWorkOrder.UpdatePostCodeLatLong(PostCode, latitude, longitude)

            Return details

        Else
            Return Nothing
        End If
    End Function

    Public Shared Function FormatPostcode(ByVal fullString As String) As String
        Dim postcode As String
        postcode = fullString
        'Remove whitespaces
        If postcode <> "" Then
            postcode = Convert.ToString(fullString.Where(Function(x) Not Char.IsWhiteSpace(x)).ToArray())
            'Insert space before 3rd last char
            postcode = postcode.Insert(postcode.Length - 3, " ")
        End If
        Return postcode
    End Function

    Public Shared Function IsValidLatLong(ByVal Postcode As String, ByVal RecordType As String, ByVal RecordID As String) As String
        Dim str As String = RecordID
        'Remove whitespaces
        If RecordID <> "" Then
            Dim latitude As String = ""
            Dim longitude As String = ""
            Dim dt As DataTable

            dt = OrderWorkLibrary.DBWorkOrder.GetPostCodeCoordinates(Postcode).Tables(0)

            If Not IsNothing(dt) Then
                If (dt.Rows.Count > 0) Then
                    latitude = dt.Rows(0).Item("Latitude")
                    longitude = dt.Rows(0).Item("Longitude")
                End If
            End If

            If latitude = "" Or longitude = "" Or latitude = "0.0" Or longitude = "0.0" Then
                Emails.SendInvalidLatLongEmail(Postcode, RecordType, RecordID)
                CallLatLongAPI(Postcode, RecordType, RecordID)
            End If

        End If
        Return vbNullString
    End Function

    Public Shared Function CallLatLongAPI(ByVal Postcode As String, ByVal RecordType As String, ByVal RecordID As String) As Integer
        Try
            Dim result As String = ""
            Postcode = FormatPostcode(Postcode)
            Dim URL = "https://api.postcodes.io/postcodes/" + Postcode


            Dim request As HttpWebRequest = TryCast(WebRequest.Create(URL), HttpWebRequest)
            request.Method = "GET"
            request.ContentLength = 0

            Try

                ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
                CommonFunctions.createLog("Orderwork � Calling postcodes.io API for Incomplete location coordinates for :" + Postcode + "-" + RecordID)
                Using response As HttpWebResponse = TryCast(request.GetResponse(), HttpWebResponse)

                    Dim reader As New StreamReader(response.GetResponseStream())

                    result = reader.ReadToEnd()

                    If result <> "" Then
                        Dim LatLongData As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.JsonConvert.DeserializeObject(result)
                        Dim status As String = Convert.ToString(LatLongData.Property("status").Value)

                        If status = 200 Then
                            Dim objDetails As Newtonsoft.Json.Linq.JObject = JObject.Parse(Convert.ToString(LatLongData.Property("result").Value))
                            Dim latitude As String = Convert.ToString(objDetails.Property("latitude").Value)
                            Dim longitude As String = Convert.ToString(objDetails.Property("longitude").Value)

                            'Insert/update geocodes in our table
                            CommonFunctions.createLog("Orderwork � UpdatePostCodeLatLong for :" + Postcode + "-" + RecordID)
                            Dim ds As DataSet = DBWorkOrder.UpdatePostCodeLatLong(Postcode, latitude, longitude)
                            CommonFunctions.createLog("Orderwork � Success UpdatePostCodeLatLong for :" + Postcode + "-" + RecordID)

                            'add postcode to getaddress
                            If latitude <> "" And longitude <> "" Then
                                Postcode = Postcode.Replace(" ", "")
                                URL = "https://api.getAddress.io/private-address/" + Postcode.Trim() + "?api-key=" + Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("GetAddressAPIAdminKey").Trim)

                                Dim objGetaddressAPI As New GetaddressAPI
                                Dim dsAddress As DataSet = DBWorkOrder.GetAddress(RecordType, RecordID)
                                If dsAddress.Tables.Count <> 0 Then
                                    objGetaddressAPI.line1 = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("Address"))
                                    objGetaddressAPI.line2 = ""
                                    objGetaddressAPI.line3 = ""
                                    objGetaddressAPI.line4 = ""
                                    objGetaddressAPI.locality = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("City"))
                                    objGetaddressAPI.townOrCity = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("City"))
                                    objGetaddressAPI.county = ""

                                    Dim objResult As New APIResult
                                    CommonFunctions.createLog("Orderwork � Calling GetaddressAPI for :" + Postcode + "-" + RecordID)
                                    objResult = GenerateJSONandCallAPI(objGetaddressAPI, URL)

                                    If objResult.id = 1 Then
                                        CommonFunctions.createLog("Empowered-Potal- completed location coordinates for :" + Postcode + "-" + RecordID)
                                        ' successfully added to getaddress list, now update excel accordingly
                                    End If
                                End If
                            End If

                        End If
                    Else
                        CommonFunctions.createLog("Orderwork � Calling doogle API email for Incomplete location coordinates for :" + Postcode + "-" + RecordID)
                        CalldoogalLatLongAPI(Postcode, RecordType, RecordID)
                    End If
                    Return 1
                End Using

            Catch ex As Exception
                CommonFunctions.createLog("Orderwork � Calling doogle API email for Incomplete location coordinates for :" + Postcode + "-" + RecordID)
                CalldoogalLatLongAPI(Postcode, RecordType, RecordID)
                'result = ""
                'Return 0
            End Try
        Catch ex As Exception
            CommonFunctions.createLog("Empowered-Potal- Error occured while completing location coordinates for :" + Postcode + "-" + RecordID)
        End Try
    End Function

    Public Shared Function CalldoogalLatLongAPI(ByVal Postcode As String, ByVal RecordType As String, ByVal RecordID As String) As Integer
        Try
            Dim result As String = ""
            Dim URL = "https://www.doogal.co.uk/GetPostcode.ashx?postcode=" + Postcode

            Dim request As HttpWebRequest = TryCast(WebRequest.Create(URL), HttpWebRequest)
            request.Method = "GET"
            request.ContentLength = 0

            Try

                ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
                Using response As HttpWebResponse = TryCast(request.GetResponse(), HttpWebResponse)

                    Dim reader As New StreamReader(response.GetResponseStream())

                    result = reader.ReadToEnd()

                    If result <> "" Then
                        result = result.Replace(Postcode, "")
                        result = result.Substring(0, 21).Trim()

                        Dim latitude As String = result.Substring(0, 9)
                        result = result.Replace(latitude, "")
                        Dim longitude As String = result.Trim()

                        'Insert/update geocodes in our table
                        CommonFunctions.createLog("Orderwork � upadating incomplete location coordinates from doogal for :" + Postcode + "-" + RecordID)
                        Dim ds As DataSet = DBWorkOrder.UpdatePostCodeLatLong(Postcode, latitude, longitude)
                        CommonFunctions.createLog("Orderwork � successfully updated coordinates from doogal for :" + Postcode + "-" + RecordID)

                        'add postcode to getaddress
                        If latitude <> "" And longitude <> "" Then
                            Postcode = Postcode.Replace(" ", "")
                            URL = "https://api.getAddress.io/private-address/" + Postcode.Trim() + "?api-key=" + Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("GetAddressAPIAdminKey").Trim)

                            Dim objGetaddressAPI As New GetaddressAPI
                            Dim dsAddress As DataSet = DBWorkOrder.GetAddress(RecordType, RecordID)
                            If dsAddress.Tables.Count <> 0 Then
                                objGetaddressAPI.line1 = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("Address"))
                                objGetaddressAPI.line2 = ""
                                objGetaddressAPI.line3 = ""
                                objGetaddressAPI.line4 = ""
                                objGetaddressAPI.locality = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("City"))
                                objGetaddressAPI.townOrCity = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("City"))
                                objGetaddressAPI.county = ""

                                Dim objResult As New APIResult
                                objResult = GenerateJSONandCallAPI(objGetaddressAPI, URL)

                                If objResult.id = 1 Then
                                    ' successfully added to getaddress list, now update excel accordingly
                                    CommonFunctions.createLog("Orderwork � successfully added coordinates to getaddress list :" + Postcode + "-" + objGetaddressAPI.line1 + ", " + objGetaddressAPI.locality + ", " + objGetaddressAPI.townOrCity)
                                End If
                            End If
                        End If

                    Else
                        CommonFunctions.createLog("Empowered � Lat long update failed from both API(co-ordinates not found), details :" + Postcode + "-" + RecordID)
                    End If

                    Return 1
                End Using

            Catch ex As Exception
                CommonFunctions.createLog("Orderwork � Lat long update failed from both API(co-ordinates not found), details :" + Postcode + "-" + RecordID)
                result = ""
                Return 0
            End Try
        Catch ex As Exception

        End Try
    End Function

    'Generate the JSON Object and Call the respective API
    Public Shared Function GenerateJSONandCallAPI(ByVal objAPI As Object, ByVal Uri As String) As APIResult
        Dim objResult As New APIResult
        Dim JsonObject As String = Newtonsoft.Json.JsonConvert.SerializeObject(objAPI)

        ' Request
        Dim request As System.Net.HttpWebRequest = TryCast(System.Net.HttpWebRequest.Create(Uri), System.Net.HttpWebRequest)
        request.Method = "POST"
        request.ContentType = "application/json"

        Using writer As New System.IO.StreamWriter(request.GetRequestStream())
            writer.Write(JsonObject)
            writer.Flush()
            writer.Close()
        End Using

        ''Get response  
        Using response As System.Net.HttpWebResponse = TryCast(request.GetResponse(), System.Net.HttpWebResponse)

            Dim reader As New System.IO.StreamReader(response.GetResponseStream())
            Dim result As String = ""
            result = reader.ReadToEnd()
            ' Deserialize the Json data into an object
            objResult = Newtonsoft.Json.JsonConvert.DeserializeObject(Of APIResult)(Convert.ToString(result))
        End Using
        Return objResult
    End Function

#End Region

    'OA-622
#Region "Payment"
    Public Shared Sub PopulateDiscountPaymentTerms(ByVal page As Page, ByRef ddlSuppDiscountPaymentsTerms As DropDownList, ByVal source As String)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(8).Copy.DefaultView
        dsView.RowFilter = "source='" + source + " '"
        ddlSuppDiscountPaymentsTerms.DataSource = dsView
        ddlSuppDiscountPaymentsTerms.DataTextField = "paymentTerm"
        ddlSuppDiscountPaymentsTerms.DataValueField = "discountPaymentTermId"
        ddlSuppDiscountPaymentsTerms.DataBind()
        ' ddlSuppDiscountPaymentsTerms.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Select Payment Term", ""))
    End Sub

    Public Shared Sub GetStandardDiscount(ByVal page As Page, ByRef lblDiscountEdit As Label, ByVal paymentTermId As Integer, ByVal source As String)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(8).Copy.DefaultView
        dsView.RowFilter = "source='" + source + " '"
        dsView.RowFilter = "discountPaymentTermId=" & paymentTermId
        Dim discount As Decimal
        discount = CDec(dsView(0)("discount").ToString())
        lblDiscountEdit.Text = discount
    End Sub
#End Region


    Public Shared Sub GetServices(ByVal page As Page, ByRef ddlService As DropDownList, ByVal companyID As Integer)
        Dim dsView As DataView = DBStandards.GetServices(companyID).Tables(0).Copy.DefaultView
        ddlService.DataSource = dsView
        ddlService.DataTextField = "ClientServiceTitle"
        ddlService.DataValueField = "ServiceId"
        ddlService.DataBind()
    End Sub

    Public Shared Function InsertServiceVouchers(ByVal xmlContent As String, ByVal companyID As Integer) As DataSet
        Dim ds As DataSet = DBStandards.InsertServiceVouchers(Convert.ToString(xmlContent), companyID)
        Return ds
    End Function
    Public Shared Function IsDuplicateVoucher(ByVal companyID As Integer, ByVal VoucherCode As String) As DataSet
        Dim ds As DataSet = DBStandards.IsDuplicateVoucher(companyID, VoucherCode)
        Return ds
    End Function
    Public Shared Function DoesServiceExist(ByVal companyID As Integer, ByVal ServiceID As Integer) As DataSet
        Dim ds As DataSet = DBStandards.DoesServiceExist(companyID, ServiceID)
        Return ds
    End Function

    Public Shared Function AzureStorageAccessKey(Optional ByVal isAPI As Boolean = False) As String
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim AccessKey As String
        If (isAPI = True) Then
            WebRequest = HttpWebRequest.Create(("D:\home\site\wwwroot\AccessData\AzureStorageAccessKey.txt"))
        Else
            WebRequest = HttpWebRequest.Create((HttpContext.Current.Server.MapPath("~/AccessData/") & "AzureStorageAccessKey.txt"))
        End If
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        AccessKey = sr.ReadToEnd
        Return AccessKey
    End Function

    Public Shared Sub AfterWOCreation(ByVal RefWOID As String, ByVal IsSendCustBookingEmail As Integer)
        Try
            Dim ds As DataSet = DBWorkOrder.GetWOInfoAfterCreation(RefWOID)
            If (ds.Tables(0).Rows.Count > 0) Then

                If ds.Tables(0).Rows(0).Item("woStatus") = OrderWorkLibrary.ApplicationSettings.WOStatusID.Submitted Then
                    Dim QuestionnaireInfo As String = ds.Tables(0).Rows(0).Item("QuestionnaireInfo").ToString.Trim
                    Dim StartDate As String = ds.Tables(0).Rows(0).Item("StartDate").ToString.Trim
                    Dim EndDate As String = ds.Tables(0).Rows(0).Item("EndDate").ToString.Trim
                    Dim AptTime As String = ds.Tables(0).Rows(0).Item("AptTime").ToString.Trim
                    Dim ScopeOfWork As String = ds.Tables(0).Rows(0).Item("ScopeOfWork").ToString.Trim
                    Dim ClientScope As String = ds.Tables(0).Rows(0).Item("ClientScope").ToString.Trim
                    Dim PONumber As String = ds.Tables(0).Rows(0).Item("PONumber").ToString.Trim
                    Dim customerName As String = ds.Tables(0).Rows(0).Item("customerName").ToString.Trim
                    Dim customerFName As String = ds.Tables(0).Rows(0).Item("customerFName").ToString.Trim
                    Dim customerLName As String = ds.Tables(0).Rows(0).Item("customerLName").ToString.Trim
                    Dim CustomerEmail As String = ds.Tables(0).Rows(0).Item("customerEmail").ToString.Trim
                    Dim BusinessArea As Integer = ds.Tables(0).Rows(0).Item("BusinessArea")

                    Dim LoggedInCompanyId As String = ds.Tables(0).Rows(0).Item("CompanyId").ToString
                    Dim LoggedInUserId As String = ds.Tables(0).Rows(0).Item("ContactId").ToString


                  'Send zero billing location email
                    If ds.Tables(0).Rows(0).Item("BillingLocID") = 0 Then
                        OrderWorkLibrary.Emails.SendWOSubmittedWithoutBillingLocation(RefWOID)
                    End If
                  
                    If (IsSendCustBookingEmail = 1) Then
                        'Send email to customer about booking
                        If CustomerEmail.Length > 0 Then
                            OrderWorkLibrary.Emails.SendCustomerWOBookConfirmationEmail(ds)
                        End If
                        'If (LoggedInCompanyId = OrderWorkLibrary.ApplicationSettings.ITECCompany Or LoggedInCompanyId = OrderWorkLibrary.ApplicationSettings.ITECHSOCompany Or LoggedInCompanyId = OrderWorkLibrary.ApplicationSettings.WigglyAmpsCompany) Then
                        '    If CustomerEmail.Length > 0 Then
                        '        Dim VerbalSurvey As String
                        '        VerbalSurvey = QuestionnaireInfo
                        '        If (LoggedInCompanyId = OrderWorkLibrary.ApplicationSettings.ITECCompany) Then
                        '            OrderWorkLibrary.Emails.SendCustomerJobBookEmail("Booking confirmation", CustomerEmail, RefWOID, StartDate, AptTime, ScopeOfWork.Replace(Chr(13), "<BR>").Replace(Chr(10), "<BR>").Replace("<pre>", "").Replace("</pre>", ""), customerFName & " " & customerLName, CInt(LoggedInCompanyId), "", VerbalSurvey, "", customerName, 1)
                        '        Else
                        '            OrderWorkLibrary.Emails.SendCustomerJobBookEmail("Booking confirmation", CustomerEmail, RefWOID, StartDate, AptTime, ScopeOfWork.Replace(Chr(13), "<BR>").Replace(Chr(10), "<BR>").Replace("<pre>", "").Replace("</pre>", ""), customerFName & " " & customerLName, CInt(LoggedInCompanyId), "", VerbalSurvey, "", "", 1)
                        '        End If

                        '    Else
                        '        If (LoggedInCompanyId = OrderWorkLibrary.ApplicationSettings.WigglyAmpsCompany Or LoggedInCompanyId = OrderWorkLibrary.ApplicationSettings.ITECCompany Or LoggedInCompanyId = OrderWorkLibrary.ApplicationSettings.ITECHSOCompany) Then
                        '            Dim VerbalSurvey As String
                        '            VerbalSurvey = QuestionnaireInfo
                        '            If (LoggedInCompanyId = OrderWorkLibrary.ApplicationSettings.ITECCompany) Then
                        '                OrderWorkLibrary.Emails.SendCustomerJobBookEmail("Booking confirmation", CustomerEmail, RefWOID, StartDate, AptTime, ScopeOfWork.Replace(Chr(13), "<BR>").Replace(Chr(10), "<BR>").Replace("<pre>", "").Replace("</pre>", ""), customerFName & " " & customerLName, CInt(LoggedInCompanyId), "", VerbalSurvey, "", customerLName, 1)
                        '            Else
                        '                OrderWorkLibrary.Emails.SendCustomerJobBookEmail("Booking confirmation", CustomerEmail, RefWOID, StartDate, AptTime, ScopeOfWork.Replace(Chr(13), "<BR>").Replace(Chr(10), "<BR>").Replace("<pre>", "").Replace("</pre>", ""), customerFName & " " & customerLName, CInt(LoggedInCompanyId), "", VerbalSurvey, "", "", 1)
                        '            End If
                        '        End If
                        '    End If
                        'Else
                        '    If CustomerEmail.Length > 0 Then
                        '        If Not IsNothing(BusinessArea) Then
                        '            If BusinessArea = OrderWorkLibrary.ApplicationSettings.BusinessAreaRetail Then
                        '                'Send the Retail template 
                        '                OrderWorkLibrary.Emails.SendCustomerJobBookEmail("Booking confirmation", CustomerEmail, RefWOID, StartDate, AptTime, ScopeOfWork.Replace(Chr(13), "<BR>").Replace(Chr(10), "<BR>").Replace("<pre>", "").Replace("</pre>", ""), customerFName & " " & customerLName, CInt(LoggedInCompanyId), PONumber, "", "", "", 1)
                        '            ElseIf BusinessArea = OrderWorkLibrary.ApplicationSettings.BusinessAreaIT Then
                        '                'Mail with IT Template
                        '                If EndDate = "" Then
                        '                    EndDate = CType(CDate(StartDate).AddDays(1), Date).ToString("dd/MM/yyyy")
                        '                End If
                        '                OrderWorkLibrary.Emails.SendITCustomerJobBookEmail("Booking confirmation", CustomerEmail, RefWOID, StartDate, EndDate, ClientScope.Replace(Chr(13), "<BR>").Replace(Chr(10), "<BR>").Replace("<pre>", "").Replace("</pre>", ""), customerFName & " " & customerLName, PONumber)
                        '            End If
                        '        End If
                        '    End If
                        'End If
                    End If

                    'Send Email to client
                    If (ds.Tables(0).Rows(0).Item("WONotification") = True) Then
                        If (ds.Tables(1).Rows.Count > 0) Then
                            Dim dvEmail As New DataView
                            Dim objEmail As New OrderWorkLibrary.Emails
                            dvEmail = ds.Tables(1).Copy.DefaultView
                            objEmail.WorkOrderID = RefWOID
                            If ds.Tables(0).Rows(0).Item("postCode").Length > 5 Then
                                objEmail.WOLoc = ds.Tables(0).Rows(0).Item("City") & "," & ds.Tables(0).Rows(0).Item("postCode").Substring(0, 5)
                            Else
                                objEmail.WOLoc = ds.Tables(0).Rows(0).Item("City") & "," & ds.Tables(0).Rows(0).Item("postCode")
                            End If
                            objEmail.WOTitle = ds.Tables(0).Rows(0).Item("WOTitle")
                            objEmail.WOCategory = ds.Tables(0).Rows(0).Item("WOCategory")
                            objEmail.WOPrice = ds.Tables(0).Rows(0).Item("WholesalePrice")
                            objEmail.WOStartDate = ds.Tables(0).Rows(0).Item("WholesalePrice")
                            If EndDate = "" Then
                                objEmail.WOEndDate = CType(CDate(StartDate).AddDays(1), Date).ToString("dd/MM/yyyy")
                            Else
                                objEmail.WOEndDate = EndDate
                            End If

                            objEmail.WPPrice = ds.Tables(0).Rows(0).Item("WholesalePrice")

                            If ds.Tables(0).Rows(0).Item("ReviewBids") = True Then
                                objEmail.QuoteRequired = True
                            End If
                            'Mail to the client & admin

                            OrderWorkLibrary.Emails.SendWorkOrderMail(objEmail, OrderWorkLibrary.ApplicationSettings.UserType.buyer, OrderWorkLibrary.ApplicationSettings.WOAction.SubmitWO, "", dvEmail)
                        End If
                    End If

                    Dim Automatch As Boolean
                    Automatch = True
                    If (ds.Tables(2).Rows.Count > 0) Then
                        If (ds.Tables(2).Rows(0).Item("autoAcceptSupplier") <> 0) Then
                            Automatch = False
                            Dim dsAutoAccept As DataSet
                            dsAutoAccept = OrderWorkLibrary.DBWorkOrder.SendAndAutoaccept(CInt(ds.Tables(0).Rows(0).Item("WOID")), CInt(ds.Tables(2).Rows(0).Item("autoAcceptSupplier")), CInt(LoggedInUserId), 1)
                            Try
                                If dsAutoAccept.Tables(0).Rows.Count > 0 Then
                                    If dsAutoAccept.Tables(0).Rows(0)("Success") = 1 Then

                                        'To Client
                                        If (dsAutoAccept.Tables(0).Rows(0)("clientMailbody") <> "" And dsAutoAccept.Tables(0).Rows(0)("clientRecipients") <> "") Then
                                            Emails.SendAutoAcceptWOEmailToClient(dsAutoAccept.Tables(0).Rows(0)("clientSubject"), dsAutoAccept.Tables(0).Rows(0)("clientMailbody"), dsAutoAccept.Tables(0).Rows(0)("clientRecipients"), dsAutoAccept.Tables(0).Rows(0)("ClientCCList"))
                                        End If

                                        'To SP
                                        If (dsAutoAccept.Tables(0).Rows(0)("SPMailbody") <> "" And dsAutoAccept.Tables(0).Rows(0)("SPRecipients") <> "") Then
                                            Emails.SendAutoAcceptWOEmailToSP(dsAutoAccept.Tables(0).Rows(0)("SPSubject"), dsAutoAccept.Tables(0).Rows(0)("SPMailbody"), dsAutoAccept.Tables(0).Rows(0)("SPRecipients"), dsAutoAccept.Tables(0).Rows(0)("SPCCList"))
                                        End If
                                    End If
                                End If
                            Catch ex As Exception
                                OrderWorkLibrary.CommonFunctions.createLog(ex.ToString)
                            End Try
                        End If
                    End If
                    'Automatch 
                    If (Automatch = True) Then
                        If (ds.Tables(0).Rows(0).Item("PlatformPrice") <> 0 And ds.Tables(0).Rows(0).Item("woStatus") <> OrderWorkLibrary.ApplicationSettings.WOStatusID.Draft And ds.Tables(0).Rows(0).Item("BillingLocID") <> 0) Then

                            'Use of Success Bit can be done in future
                            Dim Success As Boolean = OrderWorkLibrary.OrderWork.Process.OrderMatch(True, ds.Tables(0).Rows(0).Item("CompanyId"), ds.Tables(0).Rows(0).Item("ContactId"), ds.Tables(0).Rows(0).Item("WOID"), StartDate, EndDate, ds.Tables(0).Rows(0).Item("EstimatedTime").ToString, ds.Tables(0).Rows(0).Item("Location").ToString, CDec(ds.Tables(0).Rows(0).Item("PlatformPrice")), RefWOID, ds.Tables(0).Rows(0).Item("WOTitle").ToString, False, ds.Tables(0).Rows(0).Item("ScopeOfWork").ToString, "", "1", ds.Tables(0).Rows(0).Item("WOCategory").ToString, False, "", "", CType(Date.Now, Date).ToString("dd/MM/yyyy"), LoggedInCompanyId)

                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            OrderWorkLibrary.CommonFunctions.createLog("OWPORTAL - Error in AfterWOCreation function called from API  - " & ex.ToString)
        End Try
    End Sub

    Public Shared Function GetAccountSourceAndPaymentTerms(ByVal WOID As String, ByVal CompanyID As String) As DataSet
        Dim ds As DataSet = OrderWorkLibrary.DBWorkOrder.GetAccountSourceAndPaymentTerms(WOID, CompanyID)
        Return ds
    End Function

    Public Shared Sub AfterWOCreationSendCustomerEmail(ByVal RefWOID As String)
        Try
            Dim ds As DataSet = DBWorkOrder.GetWOInfoAfterCreation(RefWOID)
            Dim CustomerEmail As String = ds.Tables(0).Rows(0).Item("customerEmail").ToString.Trim
            'Send email to customer about booking
            If CustomerEmail.Length > 0 Then
                OrderWorkLibrary.Emails.SendCustomerWOBookConfirmationEmail(ds)
            End If
        Catch ex As Exception
            OrderWorkLibrary.CommonFunctions.createLog("OWPORTAL - Error in AfterWOCreation function called from API  - " & ex.ToString)
        End Try
    End Sub

End Class

Public Class GetaddressAPI
    Private _line1 As String
    Private _line2 As String
    Private _line3 As String
    Private _line4 As String
    Private _locality As String
    Private _townOrCity As String
    Private _county As String


    Public Property line1() As String
        Get
            Return _line1
        End Get
        Set(ByVal value As String)
            _line1 = value
        End Set
    End Property
    Public Property line2() As String
        Get
            Return _line2
        End Get
        Set(ByVal value As String)
            _line2 = value
        End Set
    End Property
    Public Property line3() As String
        Get
            Return _line3
        End Get
        Set(ByVal value As String)
            _line3 = value
        End Set
    End Property
    Public Property line4() As String
        Get
            Return _line4
        End Get
        Set(ByVal value As String)
            _line4 = value
        End Set
    End Property
    Public Property locality() As String
        Get
            Return _locality
        End Get
        Set(ByVal value As String)
            _locality = value
        End Set
    End Property
    Public Property townOrCity() As String
        Get
            Return _townOrCity
        End Get
        Set(ByVal value As String)
            _townOrCity = value
        End Set
    End Property
    Public Property county() As String
        Get
            Return _county
        End Get
        Set(ByVal value As String)
            _county = value
        End Set
    End Property
End Class

Public Class APIResult
    Private _id As Integer
    Private _message As String
    Public Property message() As String
        Get
            Return _message
        End Get
        Set(ByVal value As String)
            _message = value
        End Set
    End Property

    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
End Class



