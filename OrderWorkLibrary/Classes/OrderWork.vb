Imports System.Net
Imports System.Web.Mail

''' <summary>
''' Class contains Function which are related to OrderMatch
''' </summary>
''' <author>Pratik</author>
''' <remarks></remarks>
Public Class OrderWork

    ''' <summary>
    ''' Shared wobject to access web service webmethods
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs
    Private Delegate Sub AfterOrderMatch(ByVal SelectedIDs As String, ByVal WOID As Integer)



    Public Class Process

        Private Shared _WOLatitude As String = "Invalid"
        Public Shared Property WOLatitude() As String
            Get
                Return _WOLatitude
            End Get
            Set(ByVal value As String)
                _WOLatitude = value
            End Set
        End Property


        Private Shared _WOLongitude As String = "Invalid"
        Public Shared Property WOLongitude() As String
            Get
                Return _WOLongitude
            End Get
            Set(ByVal value As String)
                _WOLongitude = value
            End Set
        End Property

        Public Shared Function OrderMatch(ByVal autoMatch As Boolean, ByVal CompanyID As Integer, ByVal ContactID As Integer, _
                ByVal WOID As Integer, ByVal dateStart As String, ByVal dateEnd As String, ByVal estimatedTimeInDays As String, _
                ByVal Location As String, ByVal PlatformPrice As Integer, ByVal RefWOID As String, ByVal WOTitle As String, _
                ByVal StagedWO As Boolean, ByVal WOLongDesc As String, ByVal PricingMethod As String, ByVal bizDivId As String, _
                ByVal WOCategory As String, ByVal SpecialistSuppliesParts As Boolean, ByVal PlatformDayJobRate As String, _
                ByVal ActualTime As String, ByVal DateCreated As String, ByVal BuyerCompID As String, Optional ByVal ISDixonBookingForm As Integer = 0) As Boolean
            'CommonFunctions.createLog("OWPORTAL - In OrderMatch Funtion for AutoMatch of WorkOrderID - " & RefWOID & " Getting AutoMatch Details from DB.")
            Dim dsAutoMatchDetails As DataSet
            Dim dsSupplierDetails As DataSet = New DataSet
            ' Fetch the details of the WOID - 
            ' Table 1 - Buyer company automatch settings
            ' Table 2 - Favorite supplier - Depending on automatch settings like CRB check, Reference check, Security check and Nearesto
            dsAutoMatchDetails = DBWorkOrder.GetAutoMatchDetails(bizDivId, BuyerCompID, WOID)

            'Make Entries in DB for each Supplier to whom WO is sent
            Dim SupplierIds As String = ""

            ' Check if nearesto is set in the automatch setting of the buyer -
            ' IF yes - process nearest to functionality 
            ' IF No - ignore nearesto functionality
            ' In both case fetch CSV of the supplierid and sent to DB
            If dsAutoMatchDetails.Tables("tblAutomatch").Rows.Count <> 0 Then
                If dsAutoMatchDetails.Tables("tblAutomatch").Rows(0).Item("AutoMatchStatus") = True Then
                    ' Can proceed to process for AutoMatch
                    If dsAutoMatchDetails.Tables("tblAutomatch").Rows(0).Item("NearestDistance") <> "0" Then
                        If (ActualTime = "XLS") Then
                            getWOPostCodeGeoCode(WOID, "XLS")
                            ActualTime = ""
                        Else
                            getWOPostCodeGeoCode(WOID)
                        End If
                        If WOLatitude <> "Invalid" And WOLatitude <> "" Then
                            Dim dv As DataView = dsAutoMatchDetails.Tables(1).Copy.DefaultView
                            SupplierIds = GetCsvIDsFromData(dv)
                            If (SupplierIds = "") Then
                                dsAutoMatchDetails = DBWorkOrder.GetAutoMatchDetails(bizDivId, BuyerCompID, WOID)
                                SupplierIds = GetCsvIDsFromData(dsAutoMatchDetails.Tables(1).Copy.DefaultView)
                            End If
                        Else
                            SupplierIds = ""
                        End If
                    Else
                        SupplierIds = GetCsvIDsFromData(dsAutoMatchDetails.Tables(1).Copy.DefaultView)
                    End If
                Else
                    ' Will not process as per Automatch
                    SupplierIds = ""
                End If
            Else
                ' Will not process as per Automatch
                SupplierIds = ""
            End If
            'CommonFunctions.createLog("OWPORTAL - In OrderMatch Funtion for AutoMatch of WorkOrderID - " & RefWOID & " Suppliers Returned from DB are " & SupplierIds)
            If SupplierIds <> "" Then
                SendWOToSupplier(SupplierIds, True, CompanyID, ContactID, WOID, dateStart, dateEnd, estimatedTimeInDays, _
                                                        Location, PlatformPrice, RefWOID, WOTitle, StagedWO, WOLongDesc, PricingMethod, bizDivId, _
                                                        WOCategory, SpecialistSuppliesParts, PlatformDayJobRate, ActualTime, DateCreated, BuyerCompID, ISDixonBookingForm)
            End If



        End Function

        Public Shared Sub getWOPostCodeGeoCode(ByVal WOID As Integer, Optional ByVal mode As String = "")
            Dim ds As DataSet = DBWorkOrder.MS_WOOrderMatchGetSummary(WOID)

            '*****************************************************************************************************************************
            'NearestTo Functionality Starts
            'The following code checks the postcode associated with the workorder. If the geocode info of the postcode in present in the DB
            'then it is collected in the viewstate variables and later used to calculate the distance between the WO location and the supplier
            'location. If the geocode info is not there with us, the code calls the web service to get the coordinates of the postcode, collects
            'them in the viewstate variables and also stores the geocode info in the DB against the WO Postcode, so that if the same WO is ordermatched
            'again, we need not use the web service.
            '*****************************************************************************************************************************
            If Not IsNothing(ds.Tables("tblGeoCode")) Then
                If (ds.Tables("tblGeoCode").Rows.Count > 0) Then
                    WOLatitude = ds.Tables("tblGeoCode").Rows(0).Item("Latitude")
                    WOLongitude = ds.Tables("tblGeoCode").Rows(0).Item("Longitude")
                End If
            End If

            If WOLatitude = "Invalid" Or WOLongitude = "Invalid" Or WOLatitude = "" Or WOLongitude = "" Then
                Dim details As Newtonsoft.Json.Linq.JObject = CommonFunctions.GetPostcodeLatLong(ds.Tables("tblGeoCode").Rows(0).Item("PostCode"), mode)

                If Not IsNothing(details) Then
                    WOLatitude = Convert.ToString(details.Property("latitude").Value)
                    WOLongitude = Convert.ToString(details.Property("longitude").Value)
                Else
                    WOLatitude = "Invalid"
                    WOLongitude = "Invalid"
                End If
            End If
        End Sub

        Private Shared Function GetCsvIDsFromData(ByVal dvSupp As DataView) As String
            Dim SupplierIds As String = ""
            Dim dvr As Integer = 0
            ' Converting all supplierID in csv format
            For dvr = 0 To dvSupp.Count - 1
                SupplierIds &= dvSupp.Item(dvr)("ContactId") & ","
            Next
            ' Remove last comma
            If SupplierIds.EndsWith(",") Then
                SupplierIds = SupplierIds.Substring(0, SupplierIds.Length - 1)
            End If

            Return SupplierIds
        End Function

        Private Shared Function SendWOToSupplier(ByVal SupplierIds As String, ByVal autoMatch As Boolean, ByVal CompanyID As Integer, ByVal ContactID As Integer, _
                ByVal WOID As Integer, ByVal dateStart As String, ByVal dateEnd As String, ByVal estimatedTimeInDays As String, _
                ByVal Location As String, ByVal PlatformPrice As Integer, ByVal RefWOID As String, ByVal WOTitle As String, _
                ByVal StagedWO As Boolean, ByVal WOLongDesc As String, ByVal PricingMethod As String, ByVal bizDivId As String, _
                ByVal WOCategory As String, ByVal SpecialistSuppliesParts As Boolean, ByVal PlatformDayJobRate As String, _
                ByVal ActualTime As String, ByVal DateCreated As String, ByVal BuyerCompID As String , Optional ByVal ISDixonBookingForm As Integer = 0) As Boolean

            CommonFunctions.createLog("OWPORTAL - In OrderMatch Mailer Funtion for AutoMatch Mail of WorkOrderID - " & RefWOID & " Sending WorkOrder to Suppliers.")

            Dim executionTimeWatchAMMail As New Stopwatch
            executionTimeWatchAMMail.Start()

            'Send WO Notification Emails to Supplier Accounts (Administrator + All those that have Receive WO Notification = True)
            If OrderWorkLibrary.ApplicationSettings.SendIphoneNotification Then
                Dim dSendNotification As [Delegate] = New AfterOrderMatch(AddressOf IPhoneNotification)
                ThreadUtil.FireAndForget(dSendNotification, New Object() {SupplierIds, WOID})
            End If


            Dim dsEmailContacts As DataSet
            dsEmailContacts = DBWorkOrder.SendWO(SupplierIds, WOID, dateStart, dateEnd, estimatedTimeInDays, PlatformPrice, SpecialistSuppliesParts, _
                                                   ActualTime, CompanyID, ContactID, autoMatch, BuyerCompID)



            If dsEmailContacts.Tables.Count > 0 Then
                If dsEmailContacts.Tables("tblSuppliers").Rows.Count <> 0 Then
                    Dim objEmail As New OrderWorkLibrary.Emails

                    'Add data to the Email object
                    objEmail.WOCategory = WOCategory
                    objEmail.WOEndDate = dateEnd
                    objEmail.WOLoc = Location
                    objEmail.PPPrice = PlatformPrice
                    objEmail.WorkOrderID = RefWOID
                    objEmail.WOStartDate = dateStart
                    objEmail.WOTitle = WOTitle
                    objEmail.EstimatedTimeInDays = estimatedTimeInDays
                    objEmail.WorkOrderDayRate = PlatformDayJobRate
                    objEmail.StagedWO = StagedWO
                    objEmail.WODate = DateCreated
                    objEmail.WODesc = WOLongDesc
                    'poonam modified on 18/2/2016 - Task - OA-187 : OA - Auto-match notification emails do not always give full info
                    objEmail.IsNearestToUsed = True

                    OrderWorkLibrary.Emails.SendOrderMatchMail(objEmail, dsEmailContacts, bizDivId, PricingMethod, ISDixonBookingForm)
                End If
            End If
            executionTimeWatchAMMail.Stop()
            Dim WOAMMail As String
            WOAMMail = executionTimeWatchAMMail.ElapsedMilliseconds.ToString
            CommonFunctions.createLog("OWPORTAL - AutoMatch Mail for WorkOrderId : " & RefWOID & " is sent, Execution Time: " & WOAMMail & "ms")
        End Function

        Public Shared Sub IPhoneNotification(ByVal SelectedIDs As String, ByVal WOID As Integer)
            Try
                Dim dsNotification As DataSet = DBWorkOrder.IPhoneNotification(SelectedIDs, WOID)
                If dsNotification.Tables.Count > 0 Then

                    If dsNotification.Tables("TokenDetails").Rows.Count > 0 Then
                        For i As Integer = 0 To dsNotification.Tables("TokenDetails").Rows.Count - 1
                            ws.WSiPhoneNotification.SendAlert(dsNotification.Tables("TokenDetails").Rows(i)("Token"), "New JOB -" & dsNotification.Tables("TokenDetails").Rows(i)("RefWOID") & " arrived", "default", 1)
                        Next
                    End If
                End If
            Catch ex As Exception

            End Try
        End Sub


    End Class
End Class

