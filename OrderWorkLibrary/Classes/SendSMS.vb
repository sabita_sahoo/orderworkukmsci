﻿Imports System.Net
Imports System.IO
Public Class SendSMS

    Public Shared Function PostSMS(ByVal MobileTo As String, ByVal MessageBody As String, ByVal SenderName As String)
        Dim apikey As String = ConfigurationManager.AppSettings("GetSMSKey").ToString().Trim()
        Dim Number As String = MobileTo
        If (Number.Length = 11 And Number.StartsWith(0)) Then
            Number = Number.Remove(0, 1).Insert(0, 44)
        ElseIf (Number.Length = 10 And Number.StartsWith(7)) Then
            Number = Number.Insert(0, 44)
        Else
            Number = Number
        End If
        'Dim Message As String = "Thank you for choosing to use our installation services, we would really appreciate your feedback! Just click on the link " + MessageBody
        Dim URL As String = "https://api.txtlocal.com/send/?"
        Dim PostData As String = "apikey=" & apikey & "&sender=" & SenderName & "&numbers=" & Number & "&message=" & MessageBody
        Dim req As HttpWebRequest = WebRequest.Create(URL)
        req.Method = "POST"
        Dim encoding As New ASCIIEncoding()
        Dim byte1 As Byte() = encoding.GetBytes(PostData)
        req.ContentType = "application/x-www-form-urlencoded"
        req.ContentLength = byte1.Length
        Dim newStream As Stream = req.GetRequestStream()
        newStream.Write(byte1, 0, byte1.Length)

        Try
            Dim resp As HttpWebResponse = req.GetResponse()
            Dim sr As New StreamReader(resp.GetResponseStream())
            Dim results As String = sr.ReadToEnd()
            sr.Close()
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Sending SMS Survey Successful to Mobile Number : " & MobileTo, "Sending SMS Survey Successful")
            'Dim html As String = results
        Catch wex As WebException
            Dim ex1 As New Exception("SOMETHING WENT AWRY!Status: " & wex.Status & "Message: " & wex.Message & "")
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error occurred while sending SMS Survey (" & "Exception details" & ex1.ToString, "Error in UCCloseComplete")
            Throw (ex1)
        End Try
        Return True
    End Function

End Class
