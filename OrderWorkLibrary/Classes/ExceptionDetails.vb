Imports System.Net
Imports System.Xml
Imports System.Text
Public Enum ExceptionTypes
    ServerFault
    ClientInducedServerFault
    ClientFault
    UnknownFault
End Enum
Public Interface IExceptionDetails
    ReadOnly Property Exception() As Exception
    ReadOnly Property ExceptionType() As ExceptionTypes
    ReadOnly Property DisplayMessage() As String
    ReadOnly Property Actor() As String
    ReadOnly Property CLRVersion() As String
    ReadOnly Property MachineName() As String
    ReadOnly Property NetworkUser() As String
    ReadOnly Property NetworkUserDomain() As String
    ReadOnly Property OSVersion() As String
    ReadOnly Property TimeOccurredUTC() As DateTime
    ReadOnly Property Login() As String
End Interface


Public Class ExceptionDetails
    Implements IExceptionDetails

    Private mException As Exception
    Private msDefaultDisplayMessage As String
    Private mbParsed As Boolean = False ' only parse if necessary

    ' Post parse details
    Private msDisplayMessage As String
    Private LoanRepricingMessage As String
    Private msActor As String
    Private mExceptionType As ExceptionTypes

    Private msCLRVersion As String
    Private msLogin As String
    Private msMachineName As String
    Private msNetworkUser As String
    Private msNetworkUserDomain As String
    Private msOSVersion As String
    Private mdtTimeOccurredUTC As DateTime

    Public Sub New(ByVal ex As Exception, ByVal Login As String, ByVal DefaultDisplayMessage As String)
        mException = ex
        msDefaultDisplayMessage = DefaultDisplayMessage
        msCLRVersion = System.Environment.Version.ToString
        msLogin = Login
        msMachineName = System.Environment.MachineName
        msNetworkUser = System.Environment.UserName
        msNetworkUserDomain = System.Environment.UserDomainName
        msOSVersion = System.Environment.OSVersion.ToString
        mdtTimeOccurredUTC = DateTime.UtcNow
    End Sub

    Public ReadOnly Property Actor() As String Implements IExceptionDetails.Actor
        Get
            If Not mbParsed Then ParseException()
            Return msActor
        End Get
    End Property

    Public ReadOnly Property DisplayMessage() As String Implements IExceptionDetails.DisplayMessage
        Get
            If Not mbParsed Then ParseException()
            Return msDisplayMessage
        End Get
    End Property

    Public ReadOnly Property Exception() As System.Exception Implements IExceptionDetails.Exception
        Get
            Return mException
        End Get
    End Property

    Public ReadOnly Property ExceptionType() As ExceptionTypes Implements IExceptionDetails.ExceptionType
        Get
            If Not mbParsed Then ParseException()
            Return mExceptionType
        End Get
    End Property

    Private Sub ParseException()
        Try
            If TypeOf mException Is WebException Then
                Dim wex As WebException = CType(mException, WebException)
                Dim sClientMessage As String = wex.Response.Headers.Get("ClientDisplayMessage")
                If sClientMessage Is Nothing Then
                    msDisplayMessage = msDefaultDisplayMessage
                Else
                    msDisplayMessage = sClientMessage
                End If

                msActor = wex.Response.ResponseUri.ToString
                mExceptionType = ExceptionTypes.ServerFault
            Else
                If mException.InnerException.Message <> "" Then
                    msDisplayMessage = mException.InnerException.Message
                Else
                    msDisplayMessage = msDefaultDisplayMessage
                End If
                msActor = "Unknown"
                mExceptionType = ExceptionTypes.ClientFault ' means exception was thrown locally (assumed since not a soap exception)
            End If

            mbParsed = True
        Catch ex As Exception
            'WriteLine("Error occurred while processing exception.")
        End Try

    End Sub

    Public ReadOnly Property CLRVersion() As String Implements IExceptionDetails.CLRVersion
        Get
            Return msCLRVersion
        End Get
    End Property

    Public ReadOnly Property Login() As String Implements IExceptionDetails.Login
        Get
            Return msLogin
        End Get
    End Property

    Public ReadOnly Property MachineName() As String Implements IExceptionDetails.MachineName
        Get
            Return msMachineName
        End Get
    End Property

    Public ReadOnly Property NetworkUser() As String Implements IExceptionDetails.NetworkUser
        Get
            Return msNetworkUser
        End Get
    End Property

    Public ReadOnly Property NetworkUserDomain() As String Implements IExceptionDetails.NetworkUserDomain
        Get
            Return msNetworkUserDomain
        End Get
    End Property

    Public ReadOnly Property OSVersion() As String Implements IExceptionDetails.OSVersion
        Get
            Return msOSVersion
        End Get
    End Property

    Public ReadOnly Property TimeOccurredUTC() As Date Implements IExceptionDetails.TimeOccurredUTC
        Get
            Return mdtTimeOccurredUTC
        End Get
    End Property

    Public Overrides Function ToString() As String

        Dim sb As New StringBuilder
        With sb
            .Append("Error Details: " & "<br>")
            .Append(ControlChars.Tab & "Display Message:<br>" & DisplayMessage & "<br>")
            .Append(ControlChars.Tab & "Developer Message:<br>" & mException.Message & "<br>")
            .Append(ControlChars.Tab & "Actor:<br>" & DisplayMessage & "<br>")
            .Append(ControlChars.Tab & "Machine Name:<br>" & MachineName & "<br>")
            .Append(ControlChars.Tab & "OS Version:<br>" & OSVersion & "<br>")
            .Append(ControlChars.Tab & "Time Occurred (UTC):<br>" & TimeOccurredUTC & "<br>")
            .Append(ControlChars.Tab & "CLR Version:<br>" & CLRVersion & "<br>")
            .Append(ControlChars.Tab & "Exception Type:<br>" & ExceptionType.ToString & "<br>")
            '.Append("Inner Exception:<br>" & mException.InnerException.ToString & "<br>")
            '.Append("Exception StackTrace:<br>" & mException.StackTrace & "<br>")
        End With

        Return sb.ToString

    End Function

End Class