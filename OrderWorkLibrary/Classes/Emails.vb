'------------------------------------------------------------------------------
' <Summary>
'   <ProjectName>Orderwork</ProjectName>
'   <FileName>EMails.vb</FileName>
'   <Module>Compose Mail</Module>
'   <Description> Contains Function to compose mails as per request</Description>
'   <CreatedDate>1/03/2007</CreatedDate>
'   <References>
'   </References>
' </Summary>
'------------------------------------------------------------------------------

'import the namespace
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports System.IO
Imports System.Collections
Imports System.Globalization

Public Class Emails



    ''' <summary>
    ''' Fields to populate the Workorder mail. Worko order ID
    ''' </summary>
    ''' <remarks></remarks>
    Private _WorkOrderID As String
    Public Property WorkOrderID() As String
        Get
            Return _WorkOrderID
        End Get
        Set(ByVal value As String)
            _WorkOrderID = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder title
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOTitle As String
    Public Property WOTitle() As String
        Get
            Return _WOTitle
        End Get
        Set(ByVal value As String)
            _WOTitle = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder location
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOLoc As String
    Public Property WOLoc() As String
        Get
            Return _WOLoc
        End Get
        Set(ByVal value As String)
            _WOLoc = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder category
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOCategory As String
    Public Property WOCategory() As String
        Get
            Return _WOCategory
        End Get
        Set(ByVal value As String)
            _WOCategory = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder Price
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOPrice As Decimal
    Public Property WOPrice() As Decimal
        Get
            Return _WOPrice
        End Get
        Set(ByVal value As Decimal)
            _WOPrice = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder Wholesale Price
    ''' </summary>
    ''' <remarks></remarks>
    Private _WPPrice As Decimal
    Public Property WPPrice() As Decimal
        Get
            Return _WPPrice
        End Get
        Set(ByVal value As Decimal)
            _WPPrice = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder Platform Price
    ''' </summary>
    ''' <remarks></remarks>
    Private _PPPrice As Decimal
    Public Property PPPrice() As Decimal
        Get
            Return _PPPrice
        End Get
        Set(ByVal value As Decimal)
            _PPPrice = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder Start Date
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOStartDate As String
    Public Property WOStartDate() As String
        Get
            Return _WOStartDate
        End Get
        Set(ByVal value As String)
            _WOStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder End Date
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOEndDate As String
    Public Property WOEndDate() As String
        Get
            Return _WOEndDate
        End Get
        Set(ByVal value As String)
            _WOEndDate = value
        End Set
    End Property

    ''' <summary>
    ''' New Workorder Price
    ''' </summary>
    ''' <remarks></remarks>
    Private _NewWOPrice As Decimal
    Public Property NewWOPrice() As Decimal
        Get
            Return _NewWOPrice
        End Get
        Set(ByVal value As Decimal)
            _NewWOPrice = value
        End Set
    End Property

    ''' <summary>
    ''' New Workorder Start Date
    ''' </summary>
    ''' <remarks></remarks>
    Private _NewWOStartDate As String
    Public Property NewWOStartDate() As String
        Get
            Return _NewWOStartDate
        End Get
        Set(ByVal value As String)
            _NewWOStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' New Workorder End Date
    ''' </summary>
    ''' <remarks></remarks>
    Private _NewWOEndDate As String
    Public Property NewWOEndDate() As String
        Get
            Return _NewWOEndDate
        End Get
        Set(ByVal value As String)
            _NewWOEndDate = value
        End Set
    End Property

    ''' <summary>
    ''' Estimated Time in days
    ''' </summary>
    ''' <remarks></remarks>
    Private _EstimatedTimeInDays As String
    Public Property EstimatedTimeInDays() As String
        Get
            Return _EstimatedTimeInDays
        End Get
        Set(ByVal value As String)
            _EstimatedTimeInDays = value
        End Set
    End Property
    ''' <summary>
    ''' Estimated Time in days
    ''' </summary>
    ''' <remarks></remarks>
    Private _EstimatedTimeInDaysNew As String
    Public Property EstimatedTimeInDaysNew() As String
        Get
            Return _EstimatedTimeInDaysNew
        End Get
        Set(ByVal value As String)
            _EstimatedTimeInDaysNew = value
        End Set
    End Property
    ''' <summary>
    ''' Work Order Platform Day Rate
    ''' </summary>
    ''' <remarks></remarks>
    Private _WorkOrderDayRate As String
    Public Property WorkOrderDayRate() As String
        Get
            Return _WorkOrderDayRate
        End Get
        Set(ByVal value As String)
            _WorkOrderDayRate = value
        End Set
    End Property
    ''' <summary>
    ''' Work Order Wholesale Day Rate
    ''' </summary>
    ''' <remarks></remarks>
    Private _WorkOrderWholesaleDayRate As String
    Public Property WorkOrderWholesaleDayRate() As String
        Get
            Return _WorkOrderWholesaleDayRate
        End Get
        Set(ByVal value As String)
            _WorkOrderWholesaleDayRate = value
        End Set
    End Property
    ''' <summary>
    ''' Work Order Day Rate
    ''' </summary>
    ''' <remarks></remarks>
    Private _WorkOrderDayRateNew As String
    Public Property WorkOrderDayRateNew() As String
        Get
            Return _WorkOrderDayRateNew
        End Get
        Set(ByVal value As String)
            _WorkOrderDayRateNew = value
        End Set
    End Property
    ''' <summary>
    ''' Staged WO or not
    ''' </summary>
    ''' <remarks></remarks>
    Private _StagedWO As String
    Public Property StagedWO() As String
        Get
            Return _StagedWO
        End Get
        Set(ByVal value As String)
            _StagedWO = value
        End Set
    End Property
    ''' <summary>
    ''' QuoteRequired
    ''' </summary>
    ''' <remarks></remarks>
    Private _QuoteRequired As Boolean = False
    Public Property QuoteRequired() As Boolean
        Get
            Return _QuoteRequired
        End Get
        Set(ByVal value As Boolean)
            _QuoteRequired = value
        End Set
    End Property

    ''' <summary>
    ''' Pass WorkOrder related date depending on WO Status e.g. Submitted Date, Accepted Date etc.
    ''' </summary>
    ''' <remarks></remarks>
    Private _WODate As String = ""
    Public Property WODate() As String
        Get
            Return _WODate
        End Get
        Set(ByVal value As String)
            _WODate = value
        End Set
    End Property

    ''' <summary>
    ''' Pass WorkOrder description
    ''' </summary>
    ''' <remarks></remarks>
    Private _WODesc As String = ""
    Public Property WODesc() As String
        Get
            Return _WODesc
        End Get
        Set(ByVal value As String)
            _WODesc = value
        End Set
    End Property

    Private _ThermostatsQuestions As String = ""


    'poonam modified on 18/2/2016 - Task - OA-187 : OA - Auto-match notification emails do not always give full info
    Private _Accreditation As String = ""
    Public Property Accreditation() As String
        Get
            Return _Accreditation
        End Get
        Set(ByVal value As String)
            _Accreditation = value
        End Set
    End Property

    Private _IsNearestToUsed As Boolean = False
    Public Property IsNearestToUsed() As Boolean
        Get
            Return _IsNearestToUsed
        End Get
        Set(ByVal value As Boolean)
            _IsNearestToUsed = value
        End Set
    End Property

    Private _EngCRBChecked As Boolean = False
    Public Property EngCRBChecked() As Boolean
        Get
            Return _EngCRBChecked
        End Get
        Set(ByVal value As Boolean)
            _EngCRBChecked = value
        End Set
    End Property
    Private _EngUKSecurity As Boolean = False
    Public Property EngUKSecurity() As Boolean
        Get
            Return _EngUKSecurity
        End Get
        Set(ByVal value As Boolean)
            _EngUKSecurity = value
        End Set
    End Property
    Private _EngCSCSChecked As Boolean = False
    Public Property EngCSCSChecked() As Boolean
        Get
            Return _EngCSCSChecked
        End Get
        Set(ByVal value As Boolean)
            _EngCSCSChecked = value
        End Set
    End Property
    Private _EngRightToWorkChecked As Boolean = False
    Public Property EngRightToWorkChecked() As Boolean
        Get
            Return _EngRightToWorkChecked
        End Get
        Set(ByVal value As Boolean)
            _EngRightToWorkChecked = value
        End Set
    End Property
    Private _CRBChecked As Boolean = False
    Public Property CRBChecked() As Boolean
        Get
            Return _CRBChecked
        End Get
        Set(ByVal value As Boolean)
            _CRBChecked = value
        End Set
    End Property
    Private _UKSecurityChecked As Boolean = False
    Public Property UKSecurityChecked() As Boolean
        Get
            Return _UKSecurityChecked
        End Get
        Set(ByVal value As Boolean)
            _UKSecurityChecked = value
        End Set
    End Property
    Private _CSCSChecked As Boolean = False
    Public Property CSCSChecked() As Boolean
        Get
            Return _CSCSChecked
        End Get
        Set(ByVal value As Boolean)
            _CSCSChecked = value
        End Set
    End Property
    Public Property ThermostatsQuestions() As String
        Get
            Return _ThermostatsQuestions
        End Get
        Set(ByVal value As String)
            _ThermostatsQuestions = value
        End Set
    End Property

    ''' <summary>
    ''' Send mail to the Client/Supplier for new workorder submitted and new registration
    ''' Send mail to admin for new registration and workorder submitted
    ''' </summary>
    ''' <param name="objEmail"></param>
    ''' <param name="contacts"></param>
    ''' <param name="mailToAdmin"></param>
    ''' <param name="Country"></param>
    ''' <param name="intuserType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SendRAQMail(ByVal objEmail As Emails, ByVal contacts As OWContacts, Optional ByVal mailToAdmin As Boolean = True, Optional ByVal Country As String = "UK", Optional ByVal intuserType As ApplicationSettings.UserType = ApplicationSettings.UserType.buyer) As Boolean
        CommonFunctions.createLog("OrderworkLibrary - SendRAQMail")
        Dim Success As Boolean = False
        ' Fetching value from typed dataset - Contacts
        Dim dv As DataView
        dv = contacts.tblContactsAttributes.Copy.DefaultView
        Dim strName As String
        dv.RowFilter = "AttributeLabel = 'FName'"
        strName = Trim(dv.Item(0).Item("AttributeValue"))
        dv.RowFilter = "AttributeLabel = 'LName'"
        strName &= " " & Trim(dv.Item(0).Item("AttributeValue")).Replace("""", "")
        Dim Email As String = contacts.tblContactsLogin(0).UserName

        'Subject of Email
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Confirmation of your OrderWork " & ApplicationSettings.ContactTypeBuyerName & " account registration"
        Else
            'Put German text here
            strsubject = "Confirmation of your OrderWork " & ApplicationSettings.ContactTypeBuyerName & " account registration"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "RAQ.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<Name>", strName)
        MsgBody = MsgBody.Replace("<WorkOrderID>", objEmail.WorkOrderID)
        MsgBody = MsgBody.Replace("<WOTitle>", objEmail.WOTitle)
        MsgBody = MsgBody.Replace("<Location>", objEmail.WOLoc)
        MsgBody = MsgBody.Replace("<StartDate>", objEmail.WOStartDate)
        MsgBody = MsgBody.Replace("<EndDate>", objEmail.WOEndDate)
        MsgBody = MsgBody.Replace("<Price>", FormatCurrency(objEmail.WOPrice, 2, TriState.True, TriState.True, TriState.False))
        MsgBody = MsgBody.Replace("<ActivationLink>", ApplicationSettings.WebPath & "ActivateAccount.aspx?Email=" & Email & "&User=" & ApplicationSettings.ViewerBuyer)

        'Success = SendMail.SendMail( ApplicationSettings.EmailInfo() , strName + " <" + Email + ">", MsgBody, strsubject)
        'Check whether mail to be send to admin
        Return Success
    End Function

    Public Shared Sub SendWorkOrderMail(ByVal objEmail As Emails, ByVal ActionBy As String, ByVal WOAction As String, Optional ByVal Comments As String = "", Optional ByVal dvEmail As DataView = Nothing, Optional ByVal WorkOrderStatus As Integer = 0, Optional ByVal ISDixonBookingForm As Integer = 0, Optional ByVal SpecialistName As String = "")
        CommonFunctions.createLog("OrderworkLibrary - SendWorkOrderMail")

        Dim strSubjectB, strSubjectS, strSubjectA As String
        Dim strContentB, strContentS, strContentA As String
        Dim SendMailToA As Boolean = False
        Dim SendMailToB As Boolean = False
        Dim SendMailToS As Boolean = False
        Dim BusinessArea As Integer
        'Supplier & Buyer Information
        Dim BName, BEmail As String
        Dim SName, SEmail As String
        Dim CompanyId As Integer
        If Not IsNothing(dvEmail) Then
            dvEmail.RowFilter = "ContactType = '" & OrderWorkLibrary.ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'To'"
            BusinessArea = dvEmail.Item(0).Item("BusinessArea")
            If dvEmail.Count > 0 Then
                If Not IsDBNull(dvEmail.Item(0).Item("Name")) Then
                    BName = Trim(dvEmail.Item(0).Item("Name")).Replace("""", "")
                    BEmail = Trim(dvEmail.Item(0).Item("Email"))
                    CompanyId = dvEmail.Item(0).Item("CompanyId")
                End If
            End If
            dvEmail.RowFilter = "ContactType = '" & OrderWorkLibrary.ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'To'"
            If dvEmail.Count > 0 Then
                If Not IsDBNull(dvEmail.Item(0).Item("Name")) Then
                    'SName = Trim(dvEmail.Item(0).Item("Name")).Replace("""", "")
                    SName = SpecialistName 'Trim(dvEmail.Item(0).Item("Name")).Replace("""", "")
                    SEmail = Trim(dvEmail.Item(0).Item("Email"))
                End If
            End If
        End If


        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String

        'Submit WO
        If WOAction = ApplicationSettings.WOAction.SubmitWO Then
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectB = "Submission of work request"
            Else
                'Put German text here
                strSubjectB = "Submission of work request"
            End If
            strSubjectA = strSubjectB
            'Email Content
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOSubmission.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd

            strContentB = Buffer
            sr.Close()
            strContentA = strContentB

            If (ISDixonBookingForm = 1) Then
                strContentB = strContentB.Replace("Start Date:", "Preferred Date:")
                strContentA = strContentA.Replace("Start Date:", "Preferred Date:")
            End If

            'Send Mail to Buyer & Admin
            SendMailToA = ControlMail.IsMailOnSubmitWO
            SendMailToB = ControlMail.IsMailOnSubmitWOToBuyer
        End If
        If WOAction = ApplicationSettings.WOAction.WOSPAccept Then
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "Acceptance of Work Order " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "Acceptance of Work Order " & objEmail.WorkOrderID
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOSPAccept.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            If (objEmail.ThermostatsQuestions <> "") Then
                strContentS = strContentS.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
                strContentA = strContentA.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
            Else
                strContentS = strContentS.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
                strContentA = strContentA.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
            End If
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnSPAccept
            SendMailToS = ControlMail.IsMailOnSPAcceptToSP
            'Check if WO is Active after Supplier Accept or Accept of CA and send Buyer Active Email
            ' If WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectB = "Work Request " & objEmail.WorkOrderID & " is now Active"
            Else
                'Put German text here
                strSubjectB = "Work Request " & objEmail.WorkOrderID & " is now Active"
            End If
            'Email Content
            If CompanyId = ApplicationSettings.ExponentialECompany Then
                WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOActiveBuyerForExponentialE.htm")
                CommonFunctions.createLog("OrderworkLibrary - SendWorkOrderMail - WOActiveBuyerForExponentialE")
            Else
                WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOActiveBuyer.htm")
                CommonFunctions.createLog("OrderworkLibrary - SendWorkOrderMail - WOActiveBuyer")
            End If

            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentB = Buffer
            sr.Close()
            If (objEmail.ThermostatsQuestions <> "") Then
                strContentB = strContentB.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
                If CompanyId = ApplicationSettings.ExponentialECompany Then
                    strContentB = strContentB.Replace("<Specialist>", SName)
                End If
            Else
                strContentB = strContentB.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
                If CompanyId = ApplicationSettings.ExponentialECompany Then
                    strContentB = strContentB.Replace("<Specialist>", SName)
                End If
            End If
            'Send Mail to Buyer
            SendMailToB = ControlMail.IsMailOnSPAcceptToBuyer
            ' End If
        End If
        If WOAction = ApplicationSettings.WOAction.WOSPCAccept Then
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "Conditional Acceptance of Work Order " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "Conditional Acceptance of Work Order " & objEmail.WorkOrderID
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOCA.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnSPCAccept
            SendMailToS = ControlMail.IsMailOnSPCAcceptToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WOSPChangeCA Then
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "JOB TERMS CHANGED - " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "You have revised the terms of your conditional acceptance relating to work order ID " & objEmail.WorkOrderID
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOChangeCATerms.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnSPChangeCA
            SendMailToS = ControlMail.IsMailOnSPChangeCAToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WODiscussCA Then
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "DISCUSS JOB TERMS - " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "Conditional Acceptance discussion details relating to Work Order " & objEmail.WorkOrderID
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WODiscussCA.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnDiscussCA
            SendMailToS = ControlMail.IsMailOnDiscussCAToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WOAcceptCA Then
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "ACCEPT JOB TERMS - " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "The terms of your Conditional Acceptance for work order " & objEmail.WorkOrderID & " have been accepted."
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOAcceptCA.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            If (objEmail.ThermostatsQuestions <> "") Then
                strContentS = strContentS.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
                strContentA = strContentA.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
            Else
                strContentS = strContentS.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
                strContentA = strContentA.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
            End If
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnAcceptCA
            SendMailToS = ControlMail.IsMailOnAcceptCAToSP
            'Check if WO is Active after Supplier Accept or Accept of CA and send Buyer Active Email
            If WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                'Email Subject
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectB = "Work Request " & objEmail.WorkOrderID & " is now Active."
                Else
                    'Put German text here
                    strSubjectB = "Work Request " & objEmail.WorkOrderID & " is now Active."
                End If
                'Email Content
                'WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOActiveBuyer.txt")
                If CompanyId = ApplicationSettings.ExponentialECompany Then
                    WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOActiveBuyerForExponentialE.htm")
                Else
                    WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOActiveBuyer.htm")
                End If
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentB = Buffer
                sr.Close()
                If (objEmail.ThermostatsQuestions <> "") Then
                    strContentB = strContentB.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
                    If CompanyId = ApplicationSettings.ExponentialECompany Then
                        strContentB = strContentB.Replace("<Specialist>", SName)
                    End If
                Else
                    strContentB = strContentB.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
                    If CompanyId = ApplicationSettings.ExponentialECompany Then
                        strContentB = strContentB.Replace("<Specialist>", SName)
                    End If
                End If
                'Send Mail to Buyer
                SendMailToB = ControlMail.IsMailOnAcceptCAToBuyer
                CommonFunctions.createLog("OrderworkLibrary - SendWorkOrderMail - SendMailToB")
            End If
        End If
        If WOAction = ApplicationSettings.WOAction.WORaiseIssue Then
            If ActionBy = ApplicationSettings.ViewerSupplier Then
                'Email Subject
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectS = "You have raised an Issue relating to Work Order " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectS = "You have raised an Issue relating to Work Order " & objEmail.WorkOrderID
                End If
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectA = "An Issue has been raised relating to Work Order " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectA = "An Issue has been raised relating to Work Order " & objEmail.WorkOrderID
                End If
                'Email Content
                WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOSPRaiseIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentS = Buffer
                sr.Close()

                WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOOWRaiseIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentA = Buffer
                sr.Close()
            Else
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectS = "An Issue has been raised relating to Work Order " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectS = "An Issue has been raised relating to Work Order " & objEmail.WorkOrderID
                End If
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectA = "You have raised an Issue relating to Work Order " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectA = "You have raised an Issue relating to Work Order " & objEmail.WorkOrderID
                End If
                'Email Content
                WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOOWRaiseIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentS = Buffer
                sr.Close()

                WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOSPRaiseIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentA = Buffer
                sr.Close()
            End If
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailRaiseIssue
            SendMailToS = ControlMail.IsMailRaiseIssueToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WOChangeIssue Then
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "Issue terms relating to Work Order " & objEmail.WorkOrderID & " have been revised"
            Else
                'Put German text here
                strSubjectS = "Issue terms relating to Work Order " & objEmail.WorkOrderID & " have been revised"
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOChangeIssueTerms.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnChangeIssue
            SendMailToS = ControlMail.IsMailOnChangeIssueToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WODiscuss Then
            If ActionBy = ApplicationSettings.ViewerSupplier Then
                'Email Subject
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectS = "Discussion details of Issue relating to Work Order " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectS = "Discussion details of Issue relating to Work Order " & objEmail.WorkOrderID
                End If
                strSubjectA = strSubjectS

                'Email Content
                WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOSPDiscussIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentS = Buffer
                sr.Close()
                WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOOWDiscussIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentA = Buffer
                sr.Close()
            Else
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectS = "Discussion details of Issue relating to Work Order " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectS = "Discussion details of Issue relating to Work Order " & objEmail.WorkOrderID
                End If
                strSubjectA = strSubjectS

                'Email Content
                WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOOWDiscussIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentS = Buffer
                sr.Close()
                WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOSPDiscussIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentA = Buffer
                sr.Close()
            End If            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnDiscuss
            SendMailToS = ControlMail.IsMailOnDiscussToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WOAcceptIssue Then
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "Issue terms relating to Work Order " & objEmail.WorkOrderID & " have been accepted"
            Else
                'Put German text here
                strSubjectS = "Issue terms relating to Work Order " & objEmail.WorkOrderID & " have been accepted"
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOAcceptIssueTerms.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnAcceptIssue
            SendMailToS = ControlMail.IsMailOnAcceptIssueToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WOComplete Then
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "Work Order " & objEmail.WorkOrderID & " has been marked as Complete"
            Else
                'Put German text here
                strSubjectS = "Work Order " & objEmail.WorkOrderID & " has been marked as Complete"
            End If
            strSubjectA = strSubjectS
            strSubjectB = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOComplete.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            strContentB = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnComplete
            SendMailToS = ControlMail.IsMailOnCompleteToSP
            SendMailToB = ControlMail.IsMailOnCompleteToBuyer
        End If
        If WOAction = ApplicationSettings.WOAction.WOClose Then
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "Work Order " & objEmail.WorkOrderID & " has been marked as Closed"
            Else
                'Put German text here
                strSubjectS = "Work Order " & objEmail.WorkOrderID & " has been marked as Closed"
            End If
            strSubjectA = strSubjectS
            strSubjectB = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOClose.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            strContentB = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnClose
            SendMailToS = ControlMail.IsMailOnCloseToSP
            SendMailToB = ControlMail.IsMailOnCloseToBuyer
        End If

        Dim strPrice As String = ""
        'Populate WO Details
        If Not IsNothing(strContentS) Then
            strContentS = strContentS.Replace("<Name>", SName)
            strContentS = strContentS.Replace("<WorkOrderID>", objEmail.WorkOrderID)
            strContentS = strContentS.Replace("<WOTitle>", objEmail.WOTitle)
            strContentS = strContentS.Replace("<WOCategory>", objEmail.WOCategory)
            strContentS = strContentS.Replace("<Location>", objEmail.WOLoc)

            If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
                strContentS = strContentS.Replace("<StartDate>", objEmail.WOStartDate)
                strContentS = strContentS.Replace("Start Date:", "Appointment Date:")
                strContentS = strContentS.Replace("Proposed End Date:", "").Replace("End Date:", "")
                strContentS = strContentS.Replace("<EndDate>", "")
            Else
                strContentS = strContentS.Replace("<StartDate>", objEmail.WOStartDate & Chr(10))
                strContentS = strContentS.Replace("<EndDate>", objEmail.WOEndDate)
            End If
            strContentS = strContentS.Replace("<Price>", FormatCurrency(objEmail.PPPrice, 2, TriState.True, TriState.True, TriState.False))
            strContentS = strContentS.Replace("<Comment>", Trim(Comments))
            strContentS = strContentS.Replace("<Date>", objEmail.WODate)
        End If
        If Not IsNothing(strContentB) Then
            strContentB = strContentB.Replace("<Name>", BName)
            strContentB = strContentB.Replace("<WorkOrderID>", objEmail.WorkOrderID)
            strContentB = strContentB.Replace("<WOTitle>", objEmail.WOTitle)
            strContentB = strContentB.Replace("<WOCategory>", objEmail.WOCategory)
            strContentB = strContentB.Replace("<Location>", objEmail.WOLoc)

            If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
                strContentB = strContentB.Replace("<StartDate>", objEmail.WOStartDate)
                strContentB = strContentB.Replace("Start Date:", "Appointment Date:")
                strContentB = strContentB.Replace("Proposed End Date:", "").Replace("End Date:", "")
                strContentB = strContentB.Replace("<EndDate>", "")
            Else
                strContentB = strContentB.Replace("<StartDate>", objEmail.WOStartDate & Chr(10))
                strContentB = strContentB.Replace("<EndDate>", objEmail.WOEndDate)
            End If
            If objEmail.QuoteRequired = True Then
                strPrice = "Quotation Requested"
            Else
                strPrice = FormatCurrency(objEmail.WPPrice, 2, TriState.True, TriState.True, TriState.False)
            End If
            strContentB = strContentB.Replace("<Price>", strPrice)
            strContentB = strContentB.Replace("<Comment>", Trim(Comments))
            strContentB = strContentB.Replace("<Date>", objEmail.WODate)
        End If
        If Not IsNothing(strContentA) Then
            strContentA = strContentA.Replace("<Name>", "OrderWork Rep")
            strContentA = strContentA.Replace("<WorkOrderID>", objEmail.WorkOrderID)
            strContentA = strContentA.Replace("<WOTitle>", objEmail.WOTitle)
            strContentA = strContentA.Replace("<WOCategory>", objEmail.WOCategory)
            strContentA = strContentA.Replace("<Location>", objEmail.WOLoc)
            If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
                strContentA = strContentA.Replace("<StartDate>", objEmail.WOStartDate)
                strContentA = strContentA.Replace("Start Date:", "Appointment Date:")
                strContentA = strContentA.Replace("Proposed End Date:", "").Replace("End Date:", "")
                strContentA = strContentA.Replace("<EndDate>", "")
            Else
                strContentA = strContentA.Replace("<StartDate>", objEmail.WOStartDate & Chr(10))
                strContentA = strContentA.Replace("<EndDate>", objEmail.WOEndDate)
            End If
            strPrice = "WholeSale Price - " & FormatCurrency(objEmail.WPPrice, 2, TriState.True, TriState.True, TriState.False) & ", Platform Price - " & FormatCurrency(objEmail.PPPrice, 2, TriState.True, TriState.True, TriState.False)
            strContentA = strContentA.Replace("<Price>", strPrice)
            strContentA = strContentA.Replace("<Comment>", Trim(Comments))
            strContentA = strContentA.Replace("<Date>", objEmail.WODate)
        End If

        'Populate Proposed WO Terms
        If WOAction = ApplicationSettings.WOAction.WOSPCAccept Or WOAction = ApplicationSettings.WOAction.WODiscussCA _
            Or WOAction = ApplicationSettings.WOAction.WOSPChangeCA Or WOAction = ApplicationSettings.WOAction.WORaiseIssue _
            Or WOAction = ApplicationSettings.WOAction.WODiscuss Or WOAction = ApplicationSettings.WOAction.WOChangeIssue Then
            'Proposed Start Date

            If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
                strContentS = strContentS.Replace("Start Date:", "Appointment Date:")
                strContentA = strContentA.Replace("Start Date:", "Appointment Date:")
                strContentS = strContentS.Replace("<ProposedStartDate>", objEmail.NewWOStartDate)
                strContentA = strContentA.Replace("<ProposedStartDate>", objEmail.NewWOStartDate)
                strContentS = strContentS.Replace("Proposed End Date:", "").Replace("End Date:", "")
                strContentA = strContentA.Replace("Proposed End Date:", "").Replace("End Date:", "")
                strContentS = strContentS.Replace("<ProposedEndDate>", "")
                strContentA = strContentA.Replace("<ProposedEndDate>", "")
            Else
                strContentS = strContentS.Replace("<ProposedStartDate>", objEmail.NewWOStartDate & Chr(10))
                strContentA = strContentA.Replace("<ProposedStartDate>", objEmail.NewWOStartDate & Chr(10))
                strContentS = strContentS.Replace("<ProposedEndDate>", objEmail.NewWOEndDate)
                strContentA = strContentA.Replace("<ProposedEndDate>", objEmail.NewWOEndDate)
            End If

            'Proposed Price
            strContentS = strContentS.Replace("<ProposedPrice>", FormatCurrency(objEmail.NewWOPrice, 2, TriState.True, TriState.True, TriState.False))
            strContentA = strContentA.Replace("<ProposedPrice>", "Platform Price - " & FormatCurrency(objEmail.NewWOPrice, 2, TriState.True, TriState.True, TriState.False))
        End If

        If SendMailToA = True Then
            If (WOAction = ApplicationSettings.WOAction.WOSPAccept Or WOAction = ApplicationSettings.WOAction.WOAcceptCA) And WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), strContentA, strSubjectA, False, "", True)
            Else
                SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), strContentA, strSubjectA)
            End If
        End If
        Try


            Dim strCC As String = ""
            If SendMailToB = True Then
                dvEmail.RowFilter = "ContactType = '" & OrderWorkLibrary.ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'CC'"
                'Get the Email id list for the CC field
                strCC = GetCCRecipients(dvEmail)
                dvEmail.RowFilter = ""
                CommonFunctions.createLog("OrderworkLibrary - SendWorkOrderMail - inside sendmailtoB")
                If strCC <> "" Then
                    If (WOAction = ApplicationSettings.WOAction.WOSPAccept Or WOAction = ApplicationSettings.WOAction.WOAcceptCA) And WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                        CommonFunctions.createLog("OrderworkLibrary - SendWorkOrderMail - inside sendmailtoB1")
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), BEmail, strContentB, strSubjectB, True, strCC, True)
                        CommonFunctions.createLog("OrderworkLibrary - SendWorkOrderMail - Success sendmailtoB1")
                    Else
                        CommonFunctions.createLog("OrderworkLibrary - SendWorkOrderMail - inside sendmailtoB2")
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), BEmail, strContentB, strSubjectB, True, strCC)
                        CommonFunctions.createLog("OrderworkLibrary - SendWorkOrderMail - Success sendmailtoB2")

                    End If
                Else
                    If (WOAction = ApplicationSettings.WOAction.WOSPAccept Or WOAction = ApplicationSettings.WOAction.WOAcceptCA) And WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), BEmail, strContentB, strSubjectB, False, "", True)
                    Else
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), BEmail, strContentB, strSubjectB)
                    End If
                End If
            End If
            If SendMailToS = True Then
                strCC = ""
                dvEmail.RowFilter = "ContactType = '" & OrderWorkLibrary.ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'CC'"
                'Get the Email id list for the CC field
                strCC = GetCCRecipients(dvEmail)
                dvEmail.RowFilter = ""
                If strCC <> "" Then
                    If (WOAction = ApplicationSettings.WOAction.WOSPAccept Or WOAction = ApplicationSettings.WOAction.WOAcceptCA) And WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), SEmail, strContentS, strSubjectS, True, strCC, True)
                    Else
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), SEmail, strContentS, strSubjectS, True, strCC)
                    End If
                Else
                    If (WOAction = ApplicationSettings.WOAction.WOSPAccept Or WOAction = ApplicationSettings.WOAction.WOAcceptCA) And WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), SEmail, strContentS, strSubjectS, False, "", True)
                    Else
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), SEmail, strContentS, strSubjectS)
                    End If
                End If
            End If
            CommonFunctions.createLog("OrderworkLibrary - SendWorkOrderMail - Success")
        Catch ex As Exception
            OrderWorkLibrary.CommonFunctions.createLog("OrderworkLibrary - SendWorkOrderMail Exception - " & ex.InnerException().ToString())
        End Try

    End Sub

    ''' <summary>
    ''' Send mail to the suppliers to whom the workorder is ordermatched
    ''' </summary>
    ''' <param name="objEmail"></param>
    ''' <param name="dsEmail"></param>
    ''' <remarks></remarks>
    'Public Shared Sub SendOrderMatchMail(ByVal objEmail As Emails, ByVal dsEmail As DataSet, ByVal BizDivId As Integer, ByVal PricingMethod As String)
    '    Dim strsubject As String
    '    If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
    '        If Not IsNothing(objEmail.WorkOrderID) Then
    '            strsubject = "NEW JOB - " & objEmail.WorkOrderID.ToString
    '        Else
    '            strsubject = "NEW JOB - " & dsEmail.Tables("tblSuppliers").Rows(0)("RefWOID").ToString
    '        End If
    '    Else
    '        'Put German text here
    '        strsubject = "A new Work Order has been routed to you"
    '    End If

    '    Dim MsgBody As String
    '    Dim dvAdminContacts, dvEmailCopied As DataView
    '    dvAdminContacts = dsEmail.Tables("tblSuppliers").DefaultView
    '    dvEmailCopied = dsEmail.Tables("tblSuppliers").Copy.DefaultView
    '    dvAdminContacts.RowFilter = "Email <> '' AND RoleGroupID = '" & ApplicationSettings.RoleSupplierAdminID & "'"
    '    Dim dvACrow, dvCCrow As DataRowView
    '    Dim ccEmails As String = ""
    '    Dim finalMailContent As String
    '    Dim BusinessArea As Integer
    '    BusinessArea = dsEmail.Tables("tblSuppliers").Rows(0)("BusinessArea")

    '    'Get content from txt file to get email content.
    '    Dim WebRequest As WebRequest
    '    Dim WebResponse As WebResponse
    '    Dim sr As StreamReader
    '    Dim Buffer As String
    '    WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "OrderMatch.txt")
    '    WebResponse = WebRequest.GetResponse()
    '    sr = New StreamReader(WebResponse.GetResponseStream())
    '    Buffer = sr.ReadToEnd
    '    MsgBody = Buffer
    '    sr.Close()
    '    If Not IsNothing(objEmail.WorkOrderID) Then
    '        MsgBody = MsgBody.Replace("<WorkOrderID>", objEmail.WorkOrderID)
    '    Else
    '        MsgBody = MsgBody.Replace("<WorkOrderID>", dsEmail.Tables("tblSuppliers").Rows(0)("RefWOID").ToString)
    '    End If

    '    MsgBody = MsgBody.Replace("<WOTitle>", objEmail.WOTitle)
    '    MsgBody = MsgBody.Replace("<WOCategory>", objEmail.WOCategory)
    '    MsgBody = MsgBody.Replace("<Location>", objEmail.WOLoc)

    '    If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
    '        MsgBody = MsgBody.Replace("Start Date:", "Appointment Date:")
    '        MsgBody = MsgBody.Replace("<StartDate>", IIf(IsDBNull(objEmail.WOStartDate), "", objEmail.WOStartDate))
    '        MsgBody = MsgBody.Replace("Proposed End Date:", "").Replace("End Date:", "")
    '        MsgBody = MsgBody.Replace("<EndDate>", "")
    '    Else
    '        MsgBody = MsgBody.Replace("<StartDate>", IIf(IsDBNull(objEmail.WOStartDate), "", objEmail.WOStartDate) & Chr(10))
    '        MsgBody = MsgBody.Replace("<EndDate>", IIf(IsDBNull(objEmail.WOEndDate), "", objEmail.WOEndDate))
    '    End If
    '    MsgBody = MsgBody.Replace("<Date>", FormatDateTime(objEmail.WODate, DateFormat.ShortDate))
    '    MsgBody = MsgBody.Replace("<Description>", objEmail.WODesc)
    '    If CDec(objEmail.PPPrice) > 0 Then
    '        MsgBody = MsgBody.Replace("<Price>", FormatCurrency(objEmail.PPPrice, 2, TriState.True, TriState.True, TriState.False))
    '    Else
    '        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
    '            MsgBody = MsgBody.Replace("<Price>", "Quotation Requested")
    '        Else
    '            MsgBody = MsgBody.Replace("<Price>", "Quotation Requested")
    '        End If

    '    End If

    '    For Each dvACrow In dvAdminContacts
    '        finalMailContent = MsgBody
    '        dvEmailCopied.RowFilter = "MainContactID = " & dvACrow("MainContactID") & " AND RoleGroupID <> '" & ApplicationSettings.RoleSupplierAdminID & "'"
    '        For Each dvCCrow In dvEmailCopied
    '            'ccEmails
    '            If ccEmails <> "" Then
    '                ccEmails &= ";"
    '            End If
    '            ccEmails &= dvCCrow("Name") & " <" & dvCCrow("Email") & ">"
    '        Next
    '        finalMailContent = finalMailContent.Replace("<Name>", dvACrow("Name"))
    '        SendMail.SendMail( ApplicationSettings.EmailInfo() , dvACrow("Name") + " <" + dvACrow("Email") + ">", finalMailContent, strsubject, True, ccEmails)
    '        If ccEmails <> "" Then
    '            ccEmails = ""
    '        End If
    '    Next
    'End Sub

    'poonam modified on 18/2/2016 - Task - OA-187 : OA - Auto-match notification emails do not always give full info
    Public Shared Sub SendOrderMatchMail(ByVal objEmail As Emails, ByVal dsEmail As DataSet, ByVal BizDivId As Integer, ByVal PricingMethod As String, Optional ByVal ISDixonBookingForm As Integer = 0)
        CommonFunctions.createLog("OrderworkLibrary - SendOrderMatchMail")

        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            If Not IsNothing(objEmail.WorkOrderID) Then
                strsubject = "NEW JOB - " & objEmail.WorkOrderID.ToString
            Else
                strsubject = "NEW JOB - " & dsEmail.Tables("tblSuppliers").Rows(0)("RefWOID").ToString
            End If
            If CDec(objEmail.PPPrice) > 0 Then
                strsubject = strsubject & " (Postcode: " & dsEmail.Tables(1).Rows(0)("Location").ToString & ") Proposed Price is �" & objEmail.PPPrice
                'If (dsEmail.Tables(1).Rows(0)("PPPerRate").ToString <> objEmail.PPPrice) Then
                If (CInt(dsEmail.Tables(1).Rows(0)("PPPerRate")) <> 0) Then
                    strsubject = strsubject & " (�" & dsEmail.Tables(1).Rows(0)("PPPerRate").ToString & " " & dsEmail.Tables(1).Rows(0)("PerRate").ToString & ")"
                Else
                    strsubject = strsubject & " " & dsEmail.Tables(1).Rows(0)("PerRate").ToString
                End If
                'Else
                '    strsubject = strsubject & " Per Job"
                'End If
            End If
        Else
            'Put German text here
            strsubject = "A new Work Order has been routed to you"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        Dim BusinessArea As Integer
        Dim AptTime As String

        BusinessArea = dsEmail.Tables("tblSuppliers").Rows(0)("BusinessArea")
        AptTime = dsEmail.Tables("tblSuppliers").Rows(0)("AptTime")
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "OrderMatch.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()


        Dim MsgBodyUnApproved As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "OrderMatchUnApproved.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBodyUnApproved = Buffer
        sr.Close()

        If Not IsNothing(objEmail.WorkOrderID) Then
            MsgBody = MsgBody.Replace("<WorkOrderID>", objEmail.WorkOrderID)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<WorkOrderID>", objEmail.WorkOrderID)
        Else
            MsgBody = MsgBody.Replace("<WorkOrderID>", dsEmail.Tables("tblSuppliers").Rows(0)("RefWOID").ToString)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<WorkOrderID>", dsEmail.Tables("tblSuppliers").Rows(0)("RefWOID").ToString)
        End If

        Dim WOCAWOlink As String = ""
        Dim WODiscardWOLink As String = ""

        If (dsEmail.Tables(1).Rows.Count > 0) Then

            Dim WODetailslink As String = ""

            Dim appendLink As New StringBuilder
            Dim encWOID As String = Encryption.EncryptURL_DES(dsEmail.Tables(1).Rows(0)("WOID").ToString)
            Dim encWorkOrderID As String = Encryption.EncryptURL_DES(dsEmail.Tables(1).Rows(0)("WorkOrderID").ToString)
            Dim encSupContactId As String = Encryption.EncryptURL_DES(dsEmail.Tables(1).Rows(0)("SupplierContactID").ToString)
            'Dim encSupCompId As String = Encryption.EncryptURL_DES(dsEmail.Tables(0).Rows(0)("MainContactId").ToString)
            appendLink.Append("WOID=" & encWOID)
            appendLink.Append("&WorkOrderID=" & encWorkOrderID)
            appendLink.Append("&SupContactId=" & encSupContactId)
            appendLink.Append("&SupCompId=")
            appendLink.Append("&FromDate=&ToDate=&SearchWorkOrderID=&PS=10&PN=0&SC=DateStart&SO=1&Viewer=Supplier&BizDivID=1&Rating=")
            appendLink.Append("&FromMailer=1")

            WODetailslink &= OrderWorkLibrary.ApplicationSettings.OrderWorkMyUKURL & "SecurePages/SupplierWODetails.aspx?" & appendLink.ToString & "&sender=UCWOsListing"
            MsgBody = MsgBody.Replace("<ViewDetailsLink>", WODetailslink)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<ViewDetailsLink>", WODetailslink)

            WODiscardWOLink = WODetailslink.Replace("&sender=UCWOsListing", "&sender=UCWODetails") & "&MailerDiscard=1"

            MsgBody = MsgBody.Replace("<DiscardWO>", WODiscardWOLink)


            appendLink.Append("&ContactID=" & Encryption.EncryptURL_DES(0))
            appendLink.Append("&CompanyID=" & Encryption.EncryptURL_DES(0))

            WOCAWOlink &= OrderWorkLibrary.ApplicationSettings.OrderWorkMyUKURL & "SecurePages/SupplierWODetails.aspx?" & appendLink.ToString & "&sender=UCWODetails" & "&MailerCAccept=1"
            MsgBody = MsgBody.Replace("<CAWO>", WOCAWOlink)

        End If


        If (objEmail.Accreditation <> "") Then
            Dim Accreditation As String = ""

            Dim strAccreditation() As String = Split(objEmail.Accreditation, ",")

            Dim i As Integer

            Dim AccreditationId As String
            Dim AccreditationName As String
            For i = 0 To strAccreditation.GetLength(0) - 1
                Dim strCombineAccreditation() As String = Split(CStr(strAccreditation.GetValue(i)), "#")

                Accreditation &= "<tr><td style='width:1%;border-bottom:1px solid #f4f5f0;padding-top:3px;padding-bottom:3px;'>&nbsp;</td><td style='width:16.8%;border-bottom:1px solid #f4f5f0;padding-top:3px;padding-bottom:3px;font-family: verdana;font-size: 12px;'>Accreditation</td><td style='width:46.84%;border-bottom:1px solid #f4f5f0;padding-top:3px;padding-bottom:3px;font-family: verdana;font-size: 12px;'>" & CStr(strCombineAccreditation.GetValue(1)) & "</td><td style='border-bottom:1px solid #f4f5f0;padding-top:3px;padding-bottom:3px;'><a class='ancStyle' style='color:#086faa;text-decoration:none;cursor:pointer;font-family: verdana;font-size: 12px;' href='<AccreditationLink>AccreditationId=" & Encryption.EncryptURL_DES(CStr(strCombineAccreditation.GetValue(0))).ToString & "'  target='_blank'>- Delete/Disable this Accreditation</a></td></tr>"
            Next
            MsgBody = MsgBody.Replace("<Accreditation>", Accreditation)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Accreditation>", Accreditation)
        Else
            MsgBody = MsgBody.Replace("<Accreditation>", "")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Accreditation>", "")
        End If




        MsgBody = MsgBody.Replace("<WOTitle>", objEmail.WOTitle)
        MsgBody = MsgBody.Replace("<WOCategory>", objEmail.WOCategory)
        MsgBody = MsgBody.Replace("<Location>", objEmail.WOLoc)

        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<WOTitle>", objEmail.WOTitle)
        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<WOCategory>", objEmail.WOCategory)
        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Location>", objEmail.WOLoc)

        If (ISDixonBookingForm = 1) Then
            MsgBody = MsgBody.Replace("Start Date:", "Preferred Date:")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("Start Date:", "Preferred Date:")
        End If

        If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
            MsgBody = MsgBody.Replace("Start Date:", "Appointment Date:")
            MsgBody = MsgBody.Replace("<StartDate>", IIf(IsDBNull(objEmail.WOStartDate), "", objEmail.WOStartDate))
            MsgBody = MsgBody.Replace("Proposed End Date:", "Appointment Time:").Replace("End Date:", "Appointment Time:")
            MsgBody = MsgBody.Replace("<EndDate>", IIf(AptTime = "All Day", "", AptTime))

            MsgBodyUnApproved = MsgBodyUnApproved.Replace("Start Date:", "Appointment Date:")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<StartDate>", IIf(IsDBNull(objEmail.WOStartDate), "", objEmail.WOStartDate))
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("Proposed End Date:", "Appointment Time:").Replace("End Date:", "Appointment Time:")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<EndDate>", IIf(AptTime = "All Day", "", AptTime))
        Else
            MsgBody = MsgBody.Replace("<StartDate>", IIf(IsDBNull(objEmail.WOStartDate), "", objEmail.WOStartDate) & Chr(10))
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<EndDate>", IIf(IsDBNull(objEmail.WOEndDate), "", objEmail.WOEndDate))

            MsgBody = MsgBody.Replace("<StartDate>", IIf(IsDBNull(objEmail.WOStartDate), "", objEmail.WOStartDate) & Chr(10))
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<EndDate>", IIf(IsDBNull(objEmail.WOEndDate), "", objEmail.WOEndDate))
        End If

        MsgBody = MsgBody.Replace("<Date>", objEmail.WODate)
        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Date>", objEmail.WODate)
        'MsgBody = MsgBody.Replace("<Date>", FormatDateTime(objEmail.WODate, DateFormat.ShortDate))
        MsgBody = MsgBody.Replace("<Description>", objEmail.WODesc)
        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Description>", objEmail.WODesc)
        If CDec(objEmail.PPPrice) > 0 Then
            Dim ProposedPrice As String = ""
            ProposedPrice = FormatCurrency(objEmail.PPPrice, 2, TriState.True, TriState.True, TriState.False)

            'If (dsEmail.Tables(1).Rows(0)("PPPerRate").ToString <> objEmail.PPPrice) Then
            If (CInt(dsEmail.Tables(1).Rows(0)("PPPerRate")) <> 0) Then
                ProposedPrice = ProposedPrice & " (Rate of �" & dsEmail.Tables(1).Rows(0)("PPPerRate").ToString & " " & dsEmail.Tables(1).Rows(0)("PerRate").ToString & ")"
            Else
                ProposedPrice = ProposedPrice & " " & dsEmail.Tables(1).Rows(0)("PerRate").ToString
            End If
            'Else
            '    ProposedPrice = ProposedPrice & " Per Job"
            'End If

            MsgBody = MsgBody.Replace("<ProposedPrice>", ProposedPrice)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<ProposedPrice>", ProposedPrice)

        Else
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                MsgBody = MsgBody.Replace("<Price>", "Quotation Requested")
                MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Price>", "Quotation Requested")
            Else
                MsgBody = MsgBody.Replace("<Price>", "Quotation Requested")
                MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Price>", "Quotation Requested")
            End If

        End If
        Dim dvAdminContacts, dvEmailCopied As DataView
        dvAdminContacts = dsEmail.Tables("tblSuppliers").DefaultView
        dvEmailCopied = dsEmail.Tables("tblSuppliers").Copy.DefaultView
        dvAdminContacts.RowFilter = "Email <> '' AND RoleGroupID = '" & ApplicationSettings.RoleSupplierAdminID & "'"
        Dim dvACrow, dvCCrow As DataRowView
        Dim ccEmails As String = ""
        Dim finalMailContent As String

        Dim SpecialistDetails As String = ""
        Dim LocationDetails As String = ""
        Dim CompanyProfileDetails As String = ""
        Dim EncContactID As String = ""

        CompanyProfileDetails &= OrderWorkLibrary.ApplicationSettings.OrderWorkMyUKURL & "SecurePages/CompanyProfile.aspx?"

        If (objEmail.Accreditation <> "") Then
            MsgBody = MsgBody.Replace("<AccreditationLink>", CompanyProfileDetails)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<AccreditationLink>", CompanyProfileDetails)
        End If

        If (objEmail.CRBChecked = True) Then
            MsgBody = MsgBody.Replace("id='CRBChecked' style='display:none;'", "")
            MsgBody = MsgBody.Replace("<CRBChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("CRBChecked").ToString)

            MsgBodyUnApproved = MsgBodyUnApproved.Replace("id='CRBChecked' style='display:none;'", "")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<CRBChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("CRBChecked").ToString)
        End If

        If (objEmail.UKSecurityChecked = True) Then
            MsgBody = MsgBody.Replace("id='UKSecurityChecked' style='display:none;'", "")
            MsgBody = MsgBody.Replace("<UKSecurityChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("UKSecurityChecked").ToString)

            MsgBodyUnApproved = MsgBodyUnApproved.Replace("id='UKSecurityChecked' style='display:none;'", "")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<UKSecurityChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("UKSecurityChecked").ToString)
        End If

        If (objEmail.CSCSChecked = True) Then
            MsgBody = MsgBody.Replace("id='CSCSChecked' style='display:none;'", "")
            MsgBody = MsgBody.Replace("<CSCSChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("CSCSChecked").ToString)

            MsgBodyUnApproved = MsgBodyUnApproved.Replace("id='CSCSChecked' style='display:none;'", "")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<CSCSChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("CSCSChecked").ToString)
        End If

        MsgBody = MsgBody.Replace("<WOCategoryLink>", CompanyProfileDetails & "CategoryId=" & Encryption.EncryptURL_DES(dsEmail.Tables(1).Rows(0)("WOCategoryID").ToString))
        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<WOCategoryLink>", CompanyProfileDetails & "CategoryId=" & Encryption.EncryptURL_DES(dsEmail.Tables(1).Rows(0)("WOCategoryID").ToString))


        For Each dvACrow In dvAdminContacts

            If (dvACrow("CompanyStatus") <> 51) Then
                finalMailContent = MsgBodyUnApproved
            Else
                finalMailContent = MsgBody
            End If

            dvEmailCopied.RowFilter = "MainContactID = " & dvACrow("MainContactID") & " AND RoleGroupID <> '" & ApplicationSettings.RoleSupplierAdminID & "'"
            For Each dvCCrow In dvEmailCopied
                'ccEmails
                If ccEmails <> "" Then
                    '-- space is needed for proper split in the sendemail class
                    ccEmails &= "; "
                End If
                ccEmails &= dvCCrow("Email")
            Next

            SpecialistDetails = ""
            LocationDetails = ""

            'If (dvACrow("CompanyStatus") <> 51) Then
            '    finalMailContent = finalMailContent.Replace(WOCAWOlink, "")
            '    finalMailContent = finalMailContent.Replace(WODiscardWOLink, "")
            '    finalMailContent = finalMailContent.Replace("id='tblInnerBottom'", "id='tblInnerBottom' style='display:none;'")
            '    finalMailContent = finalMailContent.Replace("id='tblInnerBottomNonApproveMsg' style='display:none;'", "id='tblInnerBottomNonApproveMsg'")
            'End If

            finalMailContent = finalMailContent.Replace("<Name>", dvACrow("Name"))
            finalMailContent = finalMailContent.Replace("&SupCompId=", "&SupCompId=" & Encryption.EncryptURL_DES(dvACrow("MainContactId")).ToString)

            SpecialistDetails &= OrderWorkLibrary.ApplicationSettings.OrderWorkMyUKURL & "SecurePages/SpecialistsForm.aspx?ContactID=" & Encryption.EncryptURL_DES(dvACrow("ContactID")).ToString


            If (objEmail.IsNearestToUsed = True And dvACrow("AddressID") <> 0) Then
                LocationDetails &= OrderWorkLibrary.ApplicationSettings.OrderWorkMyUKURL & "SecurePages/LocationForm.aspx?AddressID=" & Encryption.EncryptURL_DES(dvACrow("AddressID")).ToString
                finalMailContent = finalMailContent.Replace("<Distance>", dvACrow("Distance"))
                finalMailContent = finalMailContent.Replace("id='Location' style='display:none;'", "")
                finalMailContent = finalMailContent.Replace("<LocationLink>", LocationDetails & "&FromMailer=" & Encryption.EncryptURL_DES("1").ToString)
            End If

            If (objEmail.EngCRBChecked = True) Then
                finalMailContent = finalMailContent.Replace("id='EngCRBChecked' style='display:none;'", "")
                'finalMailContent = finalMailContent.Replace("<EngCRBChecked>", SpecialistDetails & "&Vetting=" & Encryption.EncryptURL_DES("EngCRBChecked").ToString)
            End If

            If (objEmail.EngUKSecurity = True) Then
                finalMailContent = finalMailContent.Replace("id='EngUKSecurity' style='display:none;'", "")
                'finalMailContent = finalMailContent.Replace("<EngUKSecurity>", SpecialistDetails & "&Vetting=" & Encryption.EncryptURL_DES("EngUKSecurity").ToString)
            End If

            If (objEmail.EngCSCSChecked = True) Then
                finalMailContent = finalMailContent.Replace("id='EngCSCSChecked' style='display:none;'", "")
                finalMailContent = finalMailContent.Replace("<EngCSCSChecked>", SpecialistDetails & "&Vetting=" & Encryption.EncryptURL_DES("EngCSCSChecked").ToString)
            End If

            If (objEmail.EngRightToWorkChecked = True) Then
                finalMailContent = finalMailContent.Replace("id='EngRightToWorkChecked' style='display:none;'", "")
                'finalMailContent = finalMailContent.Replace("<EngRightToWorkChecked>", SpecialistDetails & "&Vetting=" & Encryption.EncryptURL_DES("EngRightToWorkChecked").ToString)
            End If
            OrderWorkLibrary.CommonFunctions.createLog("OWPORTAL - Ordermatch Email Sent to  - " & dvACrow("Email") & "For - " & objEmail.WorkOrderID.ToString)
            SendMail.SendMail(ApplicationSettings.EmailInfo(), dvACrow("Email"), finalMailContent, strsubject, True, ccEmails, True)
            If ccEmails <> "" Then
                ccEmails = ""
            End If
        Next
    End Sub




    ''' <summary>
    ''' get the cc recipients list
    ''' </summary>
    ''' <param name="dvnotif"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCCRecipients(ByVal dvnotif As DataView) As String
        Dim drowNotif As DataRowView
        'this string has all the cc of the email to be sent
        Dim recipients As String = ""
        For Each drowNotif In dvnotif
            recipients &= drowNotif.Item("Email")
            If recipients <> "" Then
                recipients &= "; "
            End If
        Next

        Return recipients
    End Function

    ''' <summary>
    ''' Function to send registeration mail to user as well as admin
    ''' </summary>
    ''' <param name="contacts">XSD for registering contact</param>
    ''' <param name="mailToAdmin">Boolean value to send mail to admin</param>
    ''' <param name="Country">Specify Country</param>
    ''' <param name="intuserType">Specify Type of user</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SendMailRegister(ByVal contacts As XSDContacts, Optional ByVal mailToAdmin As Boolean = True, Optional ByVal Country As String = "UK", Optional ByVal intuserType As ApplicationSettings.UserType = ApplicationSettings.UserType.buyer) As Boolean
        CommonFunctions.createLog("OrderworkLibrary - SendMailRegister")
        Dim Success As Boolean = False
        ' Fetching value from typed dataset - Contacts
        Dim dv As DataView
        dv = contacts.Contacts.Copy.DefaultView
        dv.RowFilter = "ContactID = '1'"
        Dim strName As String
        strName = Trim(dv.Item(0).Item("FullName")).Replace("""", "")
        Dim Email As String = contacts.tblContactsLogin(0).UserName
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "OrderWork - Account Activation"
        Else
            'Put German text here
            strsubject = "Confirmation of your OrderWork <ContactType> account registration"
            If intuserType = ApplicationSettings.UserType.buyer Then
                strsubject = strsubject.Replace("<ContactType>", ApplicationSettings.ContactTypeBuyerName)
            ElseIf intuserType = ApplicationSettings.UserType.supplier Then
                strsubject = strsubject.Replace("<ContactType>", ApplicationSettings.ContactTypeSupplierName)
            End If
        End If

        Dim strActivateLink As String = ""
        If intuserType = ApplicationSettings.UserType.buyer Then
            strActivateLink = ApplicationSettings.ViewerBuyer
        ElseIf intuserType = ApplicationSettings.UserType.supplier Then
            strActivateLink = ApplicationSettings.ViewerSupplier
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "Registration.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Name>", strName)
        MsgBody = MsgBody.Replace("<ActivationLink>", ApplicationSettings.OrderWorkMyUKURL & "Supplier/ActivateAccount.php?Email=" & System.Web.HttpContext.Current.Server.UrlEncode(Email) & "&User=" & strActivateLink)

        SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject)
        'Check whether mail to be send to admin
        Return Success
    End Function

    Public Shared Function SendMailRegisterContractor(ByVal FullName As String, ByVal Email As String, Optional ByVal mailToAdmin As Boolean = True, Optional ByVal Country As String = "UK") As Boolean
        CommonFunctions.createLog("OrderworkLibrary - SendMailRegisterContractor")
        Dim Success As Boolean = False

        Dim strsubject As String
        strsubject = "Confirmation of your OrderWork Contractor account registration"

        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "RegistrationContractor.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Name>", FullName)

        SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject)
        'Check whether mail to be send to admin
        Return Success
    End Function

    ''' <summary>
    ''' Fucntion to send mail to the Supplier who have lost the workorder
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="drEmail"></param>
    ''' <param name="dvCC"></param>
    ''' <param name="ActionBy"></param>
    ''' <param name="BizDivId"></param>
    ''' <remarks></remarks>
    Public Shared Sub SendLostWOMail(ByVal WOID As String, ByVal drEmail As DataRowView, ByVal dvCC As DataView)
        Dim strSubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strSubject = "Work Order " & WOID & " has been accepted by another Supplier"
        Else
            'Put German text here
            strSubject = "Work Order " & WOID & " has been accepted by another Supplier"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOLost.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<WorkOrderID>", WOID)
        MsgBody = MsgBody.Replace("<Name>", drEmail.Item("Name").Replace("""", ""))
        Dim strCC As String
        strCC = GetCCRecipients(dvCC)
        Dim IsMailOnLostWOToSP As Boolean = ControlMail.IsMailOnLostWOToSP
        If IsMailOnLostWOToSP Then
            CommonFunctions.createLog("OrderworkLibrary - SendLostWOMail : " + WOID + " " + drEmail.Item("Email"))
            If strCC <> "" Then
                SendMail.SendMail(ApplicationSettings.EmailInfo(), drEmail.Item("Email"), MsgBody, strSubject, True, strCC)
            Else
                SendMail.SendMail(ApplicationSettings.EmailInfo(), drEmail.Item("Email"), MsgBody, strSubject)
            End If
        End If
    End Sub
    ''' <summary>
    ''' For new user approval email by resourcing team.
    ''' </summary>
    ''' <param name="CompanyName"></param>
    ''' <param name="fName"></param>
    ''' <param name="lName"></param>
    ''' <param name="CompanyURL"></param>
    ''' <remarks></remarks>
    Public Shared Sub SendNewuserApprovalMail(ByVal user As Hashtable)
        CommonFunctions.createLog("OrderworkLibrary - SendNewuserApprovalMail")
        Dim strSubject As String = ""
        Dim msgBody As String = ""
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        Dim userEmail As String = Trim(user.Item("UserEmail"))
        Dim userName As String = Trim(user.Item("UserFName").Replace("""", "")) & " " & Trim(user.Item("UserLName").Replace("""", ""))
        Dim CompanyName As String = Trim(user.Item("CompanyName"))
        Dim userPhone As String = Trim(user.Item("Phone"))
        Dim userMobile As String = Trim(user.Item("Mobile"))
        Dim userRole As String = Trim(user.Item("UserRoleGroup"))
        'OA-548
        Dim userType As String = Trim(user.Item("userType"))
        Dim AccountManagerEmail As String = Trim(user.Item("AccountManagerEmail"))

        strSubject = CompanyName + " - New user to be Approved"

        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "NewUserApproval.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        msgBody = Buffer
        sr.Close()

        msgBody = msgBody.Replace("<userName>", userName)
        msgBody = msgBody.Replace("<CompanyName>", CompanyName)
        msgBody = msgBody.Replace("<userEmail>", userEmail)
        msgBody = msgBody.Replace("<userPhone>", userPhone)
        msgBody = msgBody.Replace("<userMobile>", userMobile)
        msgBody = msgBody.Replace("<userRole>", userRole)

        'OA-548
        If userType = OrderWorkLibrary.ResourceMessageText.GetString("Supplier") Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.AddEmployeeSupplierToEmail, msgBody, strSubject)
        ElseIf userType = OrderWorkLibrary.ResourceMessageText.GetString("Buyer") Then
            If ApplicationSettings.IsLive = "False" Then
                SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.AddEmployeeClientToEmail, msgBody, strSubject)
            ElseIf ApplicationSettings.IsLive = "True" Then
                If AccountManagerEmail <> "" Then
                    SendMail.SendMail(ApplicationSettings.EmailInfo(), AccountManagerEmail, msgBody, strSubject, True, ApplicationSettings.AddEmployeeClientCCEmail)
                Else 'To account manager is null
                    SendMail.SendMail(ApplicationSettings.EmailInfo(), "", msgBody, strSubject, True, ApplicationSettings.AddEmployeeClientCCEmail)
                End If

            End If
        End If

        'SendMail.SendMail( ApplicationSettings.EmailInfo() , ApplicationSettings.ResourcingTeamEmail, msgBody, strSubject)
    End Sub

    Public Shared Sub SendUserStatusMail(ByVal AccountStatus As String, ByVal user As Hashtable, ByVal subject As String, ByVal topmessage As String, ByVal BizDivId As Integer)
        CommonFunctions.createLog("OrderworkLibrary - SendUserStatusMail")
        Dim strSubject As String = ""
        Dim msgBody As String = ""
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        Dim strEmail As String = Trim(user.Item("UserEmail"))
        Dim strName As String = Trim(user.Item("UserFName").Replace("""", "")) & " " & Trim(user.Item("UserLName").Replace("""", ""))
        If AccountStatus = "Activate" Or AccountStatus = "EnableLogin" Then
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubject = "OrderWork user account registration details"
            Else
                'Put German text here
                strSubject = "OrderWork user account registration details"
            End If
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "UserActivation.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            msgBody = Buffer
            sr.Close()
            msgBody = msgBody.Replace("<Email>", strEmail)
            msgBody = msgBody.Replace("<Password>", user.Item("UserPassword"))
            msgBody = msgBody.Replace("<AdminName>", user.Item("AdminFName") & " " & user.Item("AdminLName"))
            msgBody = msgBody.Replace("<ActivationLink>", ApplicationSettings.WebPath & "ActivateAccount.aspx?Email=" & System.Web.HttpContext.Current.Server.UrlEncode(strEmail) & "&FName=" & user.Item("UserFName") & "&LName=" & user.Item("UserLName") & "&CreatorFName=" &
                                   user.Item("AdminFName") & "&CreatorLName=" & user.Item("AdminLName") & "&User=AddedUser")
        End If
        If AccountStatus = "ReActivate" Then
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubject = "Welcome to OrderWork - User Account Re-activation"
            Else
                'Put German text here
                strSubject = "Welcome to OrderWork - User Account Re-activation"
            End If
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "UserReActivation.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            msgBody = Buffer
            sr.Close()
            msgBody = msgBody.Replace("<Email>", strEmail)
            msgBody = msgBody.Replace("<Password>", user.Item("UserPassword"))
            msgBody = msgBody.Replace("<AdminName>", user.Item("AdminFName") & " " & user.Item("AdminLName"))
        End If
        If AccountStatus = "InActivate" Then
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubject = "Your OrderWork user account has been deactivated"
            Else
                'Put German text here
                strSubject = "Your OrderWork user account has been deactivated"
            End If
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "UserDeActivation.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            msgBody = Buffer
            sr.Close()

            msgBody = msgBody.Replace("<CompanyName>", user.Item("CompanyName"))
            msgBody = msgBody.Replace("<AdminName>", user.Item("AdminFName") & " " & user.Item("AdminLName"))
        End If

        If AccountStatus = "ReActivateWOLogin" Then
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubject = "Welcome to OrderWork - User Account Re-activation"
            Else
                'Put German text here
                strSubject = "Welcome to OrderWork - User Account Re-activation"
            End If
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "UserReActivateWOLogin.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            msgBody = Buffer
            sr.Close()

            msgBody = msgBody.Replace("<AdminName>", user.Item("AdminFName") & " " & user.Item("AdminLName"))
        End If

        msgBody = msgBody.Replace("<Name>", strName)

        SendMail.SendMail(ApplicationSettings.EmailInfo(), strEmail, msgBody, strSubject)
    End Sub

    ''' <summary>
    ''' Function to send mail on Buyer Accepted
    ''' </summary>
    ''' <param name="objEmail"></param>
    ''' <param name="dvEmail"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Sub BuyerAccepted(ByVal objEmail As Emails, ByVal dvEmail As DataView, ByVal BizDivId As Integer, ByVal WorkOrderStatus As Integer)
        CommonFunctions.createLog("OrderworkLibrary - BuyerAccepted")
        Dim strName As String = ""
        Dim Email As String = ""
        Dim strCC As String = ""
        Dim BusinessArea As Integer
        BusinessArea = dvEmail.Item(0).Item("BusinessArea")

        dvEmail.RowFilter = "ContactType = '" & OrderWorkLibrary.ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'To'"
        'Contact ID is the id of the contact who either created the workorder or performed the action.
        strName = dvEmail.Item(0).Item("Name").Replace("""", "")
        Email = dvEmail.Item(0).Item("Email")
        'Else Send mail to Admin
        dvEmail.RowFilter = "ContactType = '" & OrderWorkLibrary.ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'CC'"
        'Get the Email id list for the CC field
        strCC = GetCCRecipients(dvEmail)
        dvEmail.RowFilter = ""
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Acceptance of Work Order " & objEmail.WorkOrderID
        Else
            'Put German text here
            strsubject = "Acceptance of Work Order " & objEmail.WorkOrderID
        End If
        Dim MsgBody As String = ""
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOBuyerAccept.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<Name>", strName)
        MsgBody = MsgBody.Replace("<WorkOrderID>", objEmail.WorkOrderID)
        MsgBody = MsgBody.Replace("<WOTitle>", objEmail.WOTitle)
        MsgBody = MsgBody.Replace("<Location>", objEmail.WOLoc)
        If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
            MsgBody = MsgBody.Replace("Start Date:", "Appointment Date:")
            MsgBody = MsgBody.Replace("<StartDate>", objEmail.WOStartDate)
            MsgBody = MsgBody.Replace("Proposed End Date:", "").Replace("End Date:", "")
            MsgBody = MsgBody.Replace("<EndDate>", "")
        Else
            MsgBody = MsgBody.Replace("<StartDate>", objEmail.WOStartDate & Chr(10))
            MsgBody = MsgBody.Replace("<EndDate>", objEmail.WOEndDate)
        End If
        MsgBody = MsgBody.Replace("<Price>", FormatCurrency(objEmail.WOPrice, 2, TriState.True, TriState.True, TriState.False))
        Dim IsMailOnBuyerAcceptToBuyer As Boolean = ControlMail.IsMailOnBuyerAcceptToBuyer
        Dim IsMailOnBuyerAcceptToAdmin As Boolean = ControlMail.IsMailOnBuyerAcceptToAdmin
        'Admin Email
        If (IsMailOnBuyerAcceptToAdmin) Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), MsgBody, strsubject)
        End If
        'Buyer Email
        If (IsMailOnBuyerAcceptToBuyer) Then
            If strCC <> "" Then
                SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject, True, strCC)
            Else
                SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject)
            End If
        End If


    End Sub

    Public Shared Sub SendDiscardWOMail(ByVal WorkOrderID As String, ByVal dvEmail As DataView)
        CommonFunctions.createLog("OrderworkLibrary - SendDiscardWOMail")
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Confirmation of Discarded Work Order " & WorkOrderID
        Else
            'Put German text here
            strsubject = "Confirmation of Discarded Work Order " & WorkOrderID
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WODiscard.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<WorkOrderID>", WorkOrderID)
        Dim strName As String = ""
        Dim strEmail As String = ""
        Dim strCC As String = ""
        'To Supplier
        'Dim dvEmail As New DataView(dsEmail.Tables(0))
        dvEmail.RowFilter = ""
        dvEmail.RowFilter = "ContactType = '" & OrderWorkLibrary.ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'To'"
        'WO Process:
        'Client: To � person who created wo, performed action (if both are different), CC � all those that have wo notif checked.
        'Supplier: To � person who accepted wo, performed action (if both are different), CC � all those that have wo notif checked.
        'Contact ID is the id of the contact who either created the workorder or performed the action.
        strName = dvEmail.Item(0).Item("Name").Replace("""", "")
        strEmail = dvEmail.Item(0).Item("Email")
        MsgBody = MsgBody.Replace("<Name>", strName)
        'Else Send mail to Admin
        dvEmail.RowFilter = ""
        dvEmail.RowFilter = "ContactType = '" & OrderWorkLibrary.ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'CC'"
        'Get the Email id list for the CC field
        strCC = GetCCRecipients(dvEmail)
        Dim IsMailOnDiscardToSP As Boolean = ControlMail.IsMailOnDiscardToSP
        If (IsMailOnDiscardToSP) Then
            If strCC <> "" Then
                SendMail.SendMail(ApplicationSettings.EmailInfo(), strEmail, MsgBody, strsubject, True, strCC)
            Else
                SendMail.SendMail(ApplicationSettings.EmailInfo(), strEmail, MsgBody, strsubject)
            End If
        End If
        'TO Admin
        'Contact ID is the id of the contact who either created the workorder or performed the action.
        Dim SendMailToA As Boolean = False
        SendMailToA = ControlMail.IsMailOnDiscard
        If SendMailToA = True Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), MsgBody, strsubject)
        End If

    End Sub

    ''' <summary>
    ''' Function to send forgot mail
    ''' </summary>
    ''' <param name="email">Email of the contact</param>
    ''' <param name="firstName">First Name of the Contact</param>
    ''' <param name="password">New password</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SendMailForgotPassword(ByVal email As String, ByVal Name As String, ByVal password As String, ByVal BizDivId As Integer) As Boolean
        CommonFunctions.createLog("OrderworkLibrary - SendDiscardWOMail")
        Dim Success As Boolean = False
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "OrderWork - Password Assistance"
        Else
            'Put German text here
            strsubject = "OrderWork - Password Assistance"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "ForgotPassword.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Name>", Name.Replace("""", ""))
        MsgBody = MsgBody.Replace("<Email>", email)
        MsgBody = MsgBody.Replace("<Password>", password)
        SendMail.SendMail(ApplicationSettings.EmailInfo(), email, MsgBody, strsubject)
        'Check whether mail to be send to admin
        Return Success
    End Function

    Public Shared Sub SendWithdrawFundsMail(ByVal dateCreated As String, ByVal refId As String, ByVal value As String, ByVal balance As String, ByVal email As String, ByVal name As String, ByVal company As String, Optional ByVal AdviceNumbers As String = "")
        CommonFunctions.createLog("OrderworkLibrary - SendWithdrawFundsMail")
        Dim strsubject As String
        Dim amount As String
        Dim SendMailToA As Boolean = False

        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Payment Requested - " & "�" & value
        Else
            'Put German text here
            strsubject = "Funds withdrawal request received"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WithdrawalRequest.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        amount = "�" & value
        MsgBody = MsgBody.Replace("<Name>", name.Replace("""", ""))
        MsgBody = MsgBody.Replace("<Amount>", amount)
        MsgBody = MsgBody.Replace("<advicenumbers>", AdviceNumbers)
        'Send Mail to Supplier
        SendMail.SendMail(ApplicationSettings.EmailInfo(), email, MsgBody, strsubject)
        'Send Mail to Admin

        SendMailToA = ControlMail.IsMailWithdrawFunds

        If SendMailToA = True Then
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WithdrawalRequestAdmin.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            MsgBody = Buffer
            sr.Close()
            MsgBody = MsgBody.Replace("<Name>", "OrderWork Rep")
            MsgBody = MsgBody.Replace("<Amount>", value)
            MsgBody = MsgBody.Replace("<Balance>", balance)
            MsgBody = MsgBody.Replace("<Company>", company)
            SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), MsgBody, strsubject)
        End If

    End Sub

    Public Shared Sub SendCompanyStatusMail(ByVal eMail As String, ByVal name As String, ByVal message As String, ByVal status As String)
        CommonFunctions.createLog("OrderworkLibrary - SendCompanyStatusMail")
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Your OrderWork account status has changed"
        Else
            'Put German text here
            strsubject = "Your OrderWork account status has changed"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "AccountStatus.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Name>", name.Replace("""", ""))
        MsgBody = MsgBody.Replace("<AccountStatus>", status)
        SendMail.SendMail(ApplicationSettings.EmailInfo(), eMail, MsgBody, strsubject)
    End Sub
    ''' <summary>
    ''' this function is used to send mail for confirmation of Email Address in White Paper downloads
    ''' </summary>
    ''' <param name="AccountStatus"></param>
    ''' <param name="user"></param>
    ''' <param name="subject"></param>
    ''' <param name="topmessage"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Public Shared Sub SendMailRegistrant(ByVal Name As String, ByVal UserEmail As String, ByVal RegID As Integer, ByVal MarketingActivity As String)
        CommonFunctions.createLog("OrderworkLibrary - SendMailRegistrant")
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "An OrderWork White Paper download request was made"
        Else
            'Put German text here
            strsubject = "An OrderWork White Paper download request was made"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WhitePaper.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        Dim strLink As String

        If MarketingActivity = "yousellit" Then
            strLink = OrderWorkLibrary.ApplicationSettings.SiteURL & "Documents/OW_TechServices_CaseStudies_Public.pdf"
        Else
            strLink = OrderWorkLibrary.ApplicationSettings.SiteURL & "Documents/OW_TechServices_CaseStudies_Industry.pdf"
        End If

        MsgBody = MsgBody.Replace("<Link>", strLink)
        MsgBody = MsgBody.Replace("<Name>", Name.Replace("""", ""))
        If MarketingActivity = "yousellit" Then
            MsgBody = MsgBody.Replace("<WPTypeMsg>", "importance of services to your business and how to make these work")
        Else
            MsgBody = MsgBody.Replace("<WPTypeMsg>", "benefits of flexible resourcing")
        End If
        SendMail.SendMail(ApplicationSettings.EmailInfo(), UserEmail, MsgBody, strsubject)
    End Sub

    Public Shared Sub SendMailURLTamperError(ByVal ContactID As String, ByVal CompanyID As String, ByVal ajaxErrorMessage As String, ByVal URL As String)

        CommonFunctions.createLog("OrderworkLibrary - SendMailRegistrant")
        Dim msgSubject As String
        Dim msgBody As String
        msgSubject = "Attempt to manipulate the URL"
        msgBody = "An attempt has been made to manipulate the URL where the encrypted querystring is tampered.<br>"

        If Not CompanyID Is Nothing Then
            SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, msgBody & "<p> Company Id = " & CompanyID & " ContactId = " & ContactID & "</p>" + ajaxErrorMessage, msgSubject & " Error in Orderwork: " & URL & " at " & Now())
            SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, msgBody & "<p> Company Id = " & CompanyID & " ContactId = " & ContactID & "</p>" + ajaxErrorMessage, msgSubject & " Error in Orderwork: " & URL & " at " & Now())
        Else
            SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, msgBody & ajaxErrorMessage, msgSubject & "Error in Orderwork: " & URL & " at " & Now())
            SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, msgBody & ajaxErrorMessage, msgSubject & "Error in Orderwork: " & URL & " at " & Now())
        End If
    End Sub
    ''' <summary>
    ''' This function will send the mail to OrderWork from User  
    ''' </summary>
    ''' <param name="Name"></param>
    ''' <param name="Company"></param>
    ''' <param name="UserEmail"></param>
    ''' <param name="ContactNo"></param>
    ''' <param name="Enquiry"></param>
    ''' <remarks></remarks>
    Public Shared Sub SendMailForInquiry(ByVal Name As String, ByVal Company As String, ByVal UserEmail As String, ByVal ContactNo As String, ByVal Enquiry As String)
        CommonFunctions.createLog("OrderworkLibrary - SendMailForInquiry")

        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "An Enquiry made on OrderWork"
        Else
            'Put German text here
            strsubject = "An Enquiry made on OrderWork"
        End If

        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "UserEnquiry.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Name>", Name)
        MsgBody = MsgBody.Replace("<CompanyName>", Company)
        MsgBody = MsgBody.Replace("<UserEmail>", UserEmail)
        MsgBody = MsgBody.Replace("<ContactNo>", ContactNo)
        MsgBody = MsgBody.Replace("<Enquiry>", Enquiry)

        Try
            SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EnquiryToEmail(), MsgBody, strsubject)
        Catch ex As Exception
            ' Possible error - send mail to dev team - not using error mail approach as we may lose content of the inquirer
            SendMail.SendMail(UserEmail, ApplicationSettings.Error_Email_Contact1, MsgBody, strsubject, True, ApplicationSettings.Error_Email_Contact2)
        End Try

    End Sub

    Public Shared Sub SendMailForDixonsComplaint(ByVal ComplaintRefNo As String, ByVal SalesAgentName As String, ByVal SalesAgentMainPhone As String, ByVal BranchID As String, ByVal CustomerName As String, ByVal CustomerPostCode As String, ByVal Complaint As String)
        CommonFunctions.createLog("OrderworkLibrary - SendMailForDixonsComplaint")
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Complaint with Ref.No. - " & ComplaintRefNo & " is registered from Dixons."
        Else
            'Put German text here
            strsubject = "Complaint with Ref.No. - " & ComplaintRefNo & " is registered from Dixons."
        End If

        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "DixonsComplaint.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<SalesAgentName>", SalesAgentName)
        MsgBody = MsgBody.Replace("<SalesAgentMainPhone>", SalesAgentMainPhone)
        MsgBody = MsgBody.Replace("<BranchID>", BranchID)
        MsgBody = MsgBody.Replace("<CustomerName>", CustomerName)
        MsgBody = MsgBody.Replace("<CustomerPostCode>", CustomerPostCode)
        MsgBody = MsgBody.Replace("<Complaint>", Complaint)

        Dim strArr() As String
        strArr = ApplicationSettings.DixonsComplaintEmail().Split("; ")
        Dim MailTo As String = strArr(0).ToString.Trim

        Dim ccEmail As String = ""
        Dim count As Integer
        For count = 1 To strArr.Length - 1
            If (ccEmail = "") Then
                ccEmail = strArr(count)
            Else
                ccEmail = ccEmail & "; " + strArr(count)
            End If

        Next

        Try
            SendMail.SendMail(ApplicationSettings.EmailInfo(), MailTo, MsgBody, strsubject, True, ccEmail)
        Catch ex As Exception
            ' Possible error - send mail to dev team - not using error mail approach as we may lose content of the inquirer
            'SendMail.SendMail( UserEmail , ApplicationSettings.Error_Email_Contact1, MsgBody, strsubject, True, ApplicationSettings.Error_Email_Contact2)
        End Try

    End Sub

    Public Shared Function SendMailKnowhowBookingConfirmation(ByVal email As String, ByVal RefWOID As String, ByVal strScope As String, ByVal Name As String, ByVal DateStart As String, ByVal Time As String, Optional ByVal VerbalSurvey As String = "", Optional ByVal ServicesSelection As String = "", Optional ByVal IsSmartTech As Boolean = False) As Boolean
        CommonFunctions.createLog("OrderworkLibrary - SendMailKnowhowBookingConfirmation")
        Dim Success As Boolean = False
        Dim strsubject As String

        'OM-91 : OA - Rebranding - Update KH booking confirmation templates with new Knowhow Team terminology
        'strsubject = "Knowhow Installation - Booking confirmation"
        strsubject = "Team Knowhow - Booking confirmation"

        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        If (IsSmartTech = True) Then
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "DixonsBookingConfirmationSmarttech.htm")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            MsgBody = Buffer
            sr.Close()

            MsgBody = MsgBody.Replace("<customername>", Name.Trim())
            MsgBody = MsgBody.Replace("<refwoid>", RefWOID)
            'MsgBody = MsgBody.Replace("<datestart>", DateStart)
            'MsgBody = MsgBody.Replace("<time>", Time)
            MsgBody = MsgBody.Replace("<scopeofwork>", strScope)
        Else
            WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "DixonsBookingConfirmation.htm")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            MsgBody = Buffer
            sr.Close()

            MsgBody = MsgBody.Replace("<customername>", Name.Trim())
            MsgBody = MsgBody.Replace("<refwoid>", RefWOID)
            MsgBody = MsgBody.Replace("<datestart>", DateStart)
            MsgBody = MsgBody.Replace("<time>", Time)
            MsgBody = MsgBody.Replace("<scopeofwork>", strScope)
        End If

        If (VerbalSurvey <> "") Then
            MsgBody = MsgBody.Replace("<verbalsurveyheader>", "Survey Confirmation")
            MsgBody = MsgBody.Replace("<verbalsurvey>", VerbalSurvey)
        End If
        If (ServicesSelection <> "") Then
            MsgBody = MsgBody.Replace("<servicesselectionheader>", "Services Selection")
            MsgBody = MsgBody.Replace("<servicesselection>", ServicesSelection)
        End If
        If (IsSmartTech = False) Then
            SendMail.SendMailKnowhowDixons(ApplicationSettings.EmailInfo(), email, MsgBody, strsubject, False, "", True)
        Else
            Dim strBCC As String = ""
            strBCC = ApplicationSettings.SmartTechBccEmail
            SendMail.SendMailKnowhowDixons(ApplicationSettings.EmailInfo(), email, MsgBody, strsubject, False, "", True, True, strBCC)
        End If

        'Check whether mail to be send to admin
        Return Success
    End Function

    Public Shared Function SendMailHarrodsBookingConfirmation(ByVal email As String, ByVal RefWOID As String, ByVal strScope As String, ByVal Name As String, ByVal DateStart As String, ByVal Time As String, Optional ByVal VerbalSurvey As String = "") As Boolean
        CommonFunctions.createLog("OrderworkLibrary - SendMailHarrodsBookingConfirmation")
        Dim Success As Boolean = False
        Dim strsubject As String
        'OM-91 : OA - Rebranding - Update KH booking confirmation templates with new Knowhow Team terminology
        'If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
        '    strsubject = "Knowhow Harrods Installation - Booking confirmation"
        'Else
        '    'Put German text here
        '    strsubject = "Knowhow Harrods Installation - Booking confirmation"
        'End If
        strsubject = "Team Knowhow - Booking confirmation"
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "HarrodsBookingConfirmation.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<customername>", Name.Trim())
        MsgBody = MsgBody.Replace("<refwoid>", RefWOID)
        MsgBody = MsgBody.Replace("<datestart>", DateStart)
        MsgBody = MsgBody.Replace("<time>", Time)
        MsgBody = MsgBody.Replace("<scopeofwork>", strScope)

        If (VerbalSurvey <> "") Then
            MsgBody = MsgBody.Replace("<VerbalSurveyHeader>", "Survey Confirmation")
            MsgBody = MsgBody.Replace("<VerbalSurvey>", VerbalSurvey)
        End If

        SendMail.SendMailKnowhowDixons(ApplicationSettings.EmailInfo(), email, MsgBody, strsubject, False, "", True)
        'Check whether mail to be send to admin
        Return Success

    End Function
    Public Shared Function SendWOSubmittedWithoutBillingLocation(ByVal RefWOID As String) As Boolean
        CommonFunctions.createLog("OrderworkLibrary - SendWOSubmittedWithoutBillingLocation")
        Dim Success As Boolean = False
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "WO " & RefWOID & " has been book with no known Billing location"
        Else
            'Put German text here
            strsubject = "WO " & RefWOID & " has been book with no known Billing location"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(OrderWorkLibrary.ApplicationSettings.MailerFolder & "/" & "WOWithoutBillingLocation.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<WorkOrderID>", RefWOID)

        SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailPortalAdmin, MsgBody, strsubject)
        'Check whether mail to be send to admin
        Return Success
    End Function

    Public Shared Sub SendCustomerJobBookEmail(ByVal strsubject As String, ByVal CustomerEmail As String, ByVal WorkordeId As String, ByVal AppointmentDate As String, ByVal AppointmentTimeSlot As String, ByVal ScopeOfWork As String, ByVal CustomerName As String, ByVal ClientCompany As Integer, Optional ByVal CustomerRefNo As String = "", Optional ByVal VerbalSurvey As String = "", Optional ByVal ClientRefNo As String = "", Optional ByVal Location As String = "", Optional ByVal MailSendUsingAPI As Integer = 0)
        CommonFunctions.createLog("OrderworkLibrary - SendCustomerJobBookEmail")
        Dim MsgBody As String

        'Get content from txt file to get email content.

        Dim WebRequest As WebRequest

        Dim WebResponse As WebResponse

        Dim sr As StreamReader

        Dim Buffer As String

        Dim strBCC As String = ""

        Dim strCC As String = ""

        If (ClientCompany = CInt(ApplicationSettings.Telecare24Company)) Then

            strBCC = ApplicationSettings.BookingConfirmationTelecare24Bcc

            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_Telecare24.htm")

        ElseIf (ClientCompany = CInt(ApplicationSettings.Lifeline24Company)) Then

            strBCC = ApplicationSettings.BookingConfirmationLifeline24Bcc

            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_Lifeline24.htm")


        ElseIf (ClientCompany = CInt(ApplicationSettings.VirginCompany)) Then

            strBCC = ApplicationSettings.BookingConfirmationBcc

            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_generic.htm")

        ElseIf (ClientCompany = CInt(ApplicationSettings.SamsungElectronicsCompany)) Then

            strBCC = ApplicationSettings.SamsungElectronicsBcc

            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_generic.htm")

        ElseIf (ClientCompany = CInt(ApplicationSettings.SamsungPRSCompany)) Then

            'strBCC = ApplicationSettings.SamsungPRSBcc
            If (MailSendUsingAPI = 0) Then
                strBCC = HttpContext.Current.Session("UserName")
            Else
                strBCC = ApplicationSettings.SamsungPRSBcc
            End If

            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_generic.htm")

        ElseIf (ClientCompany = CInt(ApplicationSettings.OutsourceCompany)) Then

            strBCC = ApplicationSettings.OutsourceBccEmail

            strCC = ApplicationSettings.OutsourceCCEmail

            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_generic.htm")

        ElseIf (ClientCompany = CInt(ApplicationSettings.ITECCompany)) Then

            strBCC = ApplicationSettings.ITECBccEmail

            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_generic.htm")

        ElseIf (ClientCompany = CInt(ApplicationSettings.ITECHSOCompany)) Then

            strBCC = ApplicationSettings.ITECHSOBccEmail

            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_generic.htm")
            ' OA 154 - Sabita added 
        ElseIf (ClientCompany = CInt(ApplicationSettings.Hemmersbach)) Then

            strBCC = ApplicationSettings.HBBccEmail

            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_generic.htm")

        ElseIf (ClientCompany = CInt(ApplicationSettings.WigglyAmpsCompany)) Then

            strBCC = ApplicationSettings.WigglyAmpsBccEmail

            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_WigglyAmps.htm")

        Else

            strBCC = ApplicationSettings.BookingConfirmationGenericBcc

            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_generic.htm")

        End If

        WebResponse = WebRequest.GetResponse()

        sr = New StreamReader(WebResponse.GetResponseStream())

        Buffer = sr.ReadToEnd

        MsgBody = Buffer

        sr.Close()

        MsgBody = MsgBody.Replace("<WorkordeId>", WorkordeId)

        MsgBody = MsgBody.Replace("<AppointmentDate>", AppointmentDate)

        MsgBody = MsgBody.Replace("<ScopeOfWork>", ScopeOfWork)

        If (VerbalSurvey <> "") Then

            MsgBody = MsgBody.Replace("<VerbalSurveyHeader>", "Additional Information")

            MsgBody = MsgBody.Replace("<VerbalSurvey>", VerbalSurvey)

        End If


        If (ClientCompany = CInt(ApplicationSettings.SamsungElectronicsCompany) Or ClientCompany = CInt(ApplicationSettings.SamsungPRSCompany)) Then

            MsgBody = MsgBody.Replace("<PhoneNumber>", "0203 249 7604")

            MsgBody = MsgBody.Replace("<PhoneNumberwithSpace>", "0203&nbsp;249&nbsp;7604")

        ElseIf (ClientCompany = CInt(ApplicationSettings.Lifeline24Company)) Then

            MsgBody = MsgBody.Replace("<PhoneNumber>", "0800 999 0400")

            MsgBody = MsgBody.Replace("<PhoneNumberwithSpace>", "0800&nbsp;999&nbsp;0400")

        Else

            MsgBody = MsgBody.Replace("<PhoneNumber>", "0203 053 0343")

            MsgBody = MsgBody.Replace("<PhoneNumberwithSpace>", "0203&nbsp;053&nbsp;0343")

        End If

        If (ClientCompany = CInt(ApplicationSettings.Telecare24Company)) Then

            MsgBody = MsgBody.Replace("<CustomerReferenceNo>", CustomerRefNo)

        End If

        'Poonam : OM-116 - OM - Replace Fax with Email for booking form with no forms

        If (CustomerName.Trim() = "") Then

            CustomerName = "Customer"

        End If

        If (ClientCompany = CInt(ApplicationSettings.ITECCompany) Or ClientCompany = CInt(ApplicationSettings.ITECHSOCompany)) Then

            MsgBody = MsgBody.Replace("<ScopeOfWorkHeader>", "Engineer Work Details:")

            If (ClientCompany = CInt(ApplicationSettings.ITECHSOCompany)) Then

                MsgBody = MsgBody.Replace("<CustomerName>", CustomerName)

                MsgBody = MsgBody.Replace("<AppointmentTimeSlotHeader>", "Start Time:")

                MsgBody = MsgBody.Replace("<AppointmentDateHeader>", "Appointment Date:")


            Else

                MsgBody = MsgBody.Replace("<CustomerName>", "ITEC")

                MsgBody = MsgBody.Replace("<AppointmentTimeSlotHeader>", "")

                MsgBody = MsgBody.Replace("<AppointmentDateHeader>", "Start Date:")

                MsgBody = MsgBody.Replace("<locationheader>", "Location:")

                MsgBody = MsgBody.Replace("<location>", Location)

                AppointmentTimeSlot = ""

            End If

        Else

            MsgBody = MsgBody.Replace("<AppointmentDateHeader>", "Preferred Date:")

            MsgBody = MsgBody.Replace("<AppointmentTimeSlotHeader>", "Preferred Time:")

            MsgBody = MsgBody.Replace("<ScopeOfWorkHeader>", "Scope of Work:")

            MsgBody = MsgBody.Replace("<CustomerName>", CustomerName)

        End If

        'If (ClientCompany = CInt(ApplicationSettings.ExponentialECompany) Or ClientCompany = CInt(ApplicationSettings.OutsourceCompany)) Then

        '    If (VerbalSurvey <> "") Then

        '        MsgBody = MsgBody.Replace("<VerbalSurveyHeader>", "Questionnaire")

        '        MsgBody = MsgBody.Replace("<VerbalSurvey>", VerbalSurvey)

        '    End If

        'End If

        If ClientCompany = CInt(ApplicationSettings.OutsourceCompany) Then

            MsgBody = MsgBody.Replace("<ClientRefNoHeader>", "Client Ref No: ")

            MsgBody = MsgBody.Replace("<ClientRefNo>", ClientRefNo)

            MsgBody = MsgBody.Replace("<LocationHeader>", "Location: ")

            MsgBody = MsgBody.Replace("<Location>", Location)

        End If

        If (AppointmentTimeSlot <> "") Then

            MsgBody = MsgBody.Replace("<AppointmentTimeSlot>", AppointmentTimeSlot)

        End If

        Dim finalMailContent As String

        finalMailContent = MsgBody

        'Send mail to CustomerEmail

        If (ClientCompany = CInt(ApplicationSettings.OutsourceCompany)) Then

            SendMail.SendMail(ApplicationSettings.EmailInfo(), CustomerEmail, finalMailContent, strsubject, True, strCC, True, True, strBCC)
            'OM -154 sabita added 
        ElseIf (ClientCompany = CInt(ApplicationSettings.WigglyAmpsCompany) Or ClientCompany = CInt(ApplicationSettings.ITECCompany) Or ClientCompany = CInt(ApplicationSettings.ITECHSOCompany) Or ClientCompany = CInt(ApplicationSettings.Hemmersbach)) Then
            If (CustomerEmail <> "") Then
                SendMail.SendMail(ApplicationSettings.EmailInfo(), CustomerEmail, finalMailContent, strsubject, False, "", True, True, strBCC)
            Else
                Dim BCCs() As String = Split(strBCC, "; ")
                If ClientCompany = CInt(ApplicationSettings.ITECCompany) Then
                    For Each item As String In BCCs
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), item, finalMailContent, strsubject, False, "", True, True, item)
                    Next
                Else
                    SendMail.SendMail(ApplicationSettings.EmailInfo(), BCCs.GetValue(0), finalMailContent, strsubject, False, "", True, True, BCCs.GetValue(1))
                End If

            End If
        Else

            SendMail.SendMail(ApplicationSettings.EmailInfo(), CustomerEmail, finalMailContent, strsubject, False, "", True, True, strBCC)

        End If

    End Sub

    Public Shared Sub SendBTBDCustomerJobBookEmail(ByVal strsubject As String, ByVal CustomerEmail As String, ByVal WorkordeId As String, ByVal AppointmentDate As String, ByVal ScopeOfWork As String, ByVal CustomerName As String, ByVal AppointmentTimeSlot As String, ByVal CompanyId As Integer, Optional ByVal VerbalSurvey As String = "")
        CommonFunctions.createLog("OrderworkLibrary - SendBTBDCustomerJobBookEmail")
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String

        Dim strBCC As String = ""
        If (CompanyId = OrderWorkLibrary.ApplicationSettings.BTBDCompany) Then
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_BTBD.htm")
            strBCC = ApplicationSettings.BTBDBccEmail
        ElseIf (CompanyId = OrderWorkLibrary.ApplicationSettings.DemoBTBDCompany) Then
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_DemoBTBD.htm")
            strBCC = ApplicationSettings.DemoBTBDBccEmail
        ElseIf (CompanyId = OrderWorkLibrary.ApplicationSettings.MiscoCompany) Then
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_MiscoBTBD.htm")
            strBCC = ApplicationSettings.MiscoBccEmail
        ElseIf (CompanyId = OrderWorkLibrary.ApplicationSettings.TotalComputerNetworksCompany) Then
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_TotalComputerNetworksBTBD.htm")
            strBCC = ApplicationSettings.TotalComputerNetworksBccEmail
        ElseIf (CompanyId = OrderWorkLibrary.ApplicationSettings.ebuyerCompany) Then
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_ebuyerBTBD.htm")
            strBCC = ApplicationSettings.ebuyerBccEmail
        End If

        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()



        MsgBody = MsgBody.Replace("<WorkordeId>", WorkordeId)
        MsgBody = MsgBody.Replace("<AppointmentDate>", AppointmentDate)
        MsgBody = MsgBody.Replace("<AppointmentTime>", AppointmentTimeSlot)

        MsgBody = MsgBody.Replace("<ScopeOfWork>", ScopeOfWork)

        MsgBody = MsgBody.Replace("<PhoneNumber>", "0203 053 0343")
        MsgBody = MsgBody.Replace("<PhoneNumberwithSpace>", "0203&nbsp;053&nbsp;0343")

        MsgBody = MsgBody.Replace("<CustomerName>", CustomerName)

        If (VerbalSurvey <> "") Then
            If (CompanyId = OrderWorkLibrary.ApplicationSettings.ebuyerCompany) Then
                MsgBody = MsgBody.Replace("<VerbalSurveyHeader>", "Questionnaire")
            Else
                MsgBody = MsgBody.Replace("<VerbalSurveyHeader>", "Verbal Survey Confirmation")
            End If
            MsgBody = MsgBody.Replace("<VerbalSurvey>", VerbalSurvey)
        End If

        Dim finalMailContent As String
        finalMailContent = MsgBody



        'Send mail to CustomerEmail
        SendMail.SendMail(ApplicationSettings.EmailInfo(), CustomerEmail, finalMailContent, strsubject, False, "", True, True, strBCC)

    End Sub

    Public Shared Sub SendBTBDServiceQuery(ByVal dsEmailInfo As DataSet, ByVal AttachmentInfo As DataTable)
        CommonFunctions.createLog("OrderworkLibrary - SendBTBDServiceQuery")
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String

        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "BTBDServiceQuery.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        Dim strsubject As String = "Orderwork - BT BD Service Request [" + dsEmailInfo.Tables(0).Rows(0)("SRNumber").ToString + "]"
        Dim strBCC As String = ApplicationSettings.BTBDBccEmail
        Dim strCC As String = dsEmailInfo.Tables(0).Rows(0)("BTBDAdminEmail").ToString


        MsgBody = MsgBody.Replace("<customername>", dsEmailInfo.Tables(0).Rows(0)("FName").ToString + " " + dsEmailInfo.Tables(0).Rows(0)("LName").ToString)
        MsgBody = MsgBody.Replace("<srnumber>", dsEmailInfo.Tables(0).Rows(0)("SRNumber").ToString)
        MsgBody = MsgBody.Replace("<typeofrequest>", dsEmailInfo.Tables(0).Rows(0)("TypeOfRequest").ToString)
        MsgBody = MsgBody.Replace("<firstname>", dsEmailInfo.Tables(0).Rows(0)("FName").ToString)
        MsgBody = MsgBody.Replace("<lastname>", dsEmailInfo.Tables(0).Rows(0)("LName").ToString)
        MsgBody = MsgBody.Replace("<position>", dsEmailInfo.Tables(0).Rows(0)("Position").ToString)
        MsgBody = MsgBody.Replace("<contactnumber>", dsEmailInfo.Tables(0).Rows(0)("ConNumber").ToString)
        MsgBody = MsgBody.Replace("<email>", dsEmailInfo.Tables(0).Rows(0)("Email").ToString)
        MsgBody = MsgBody.Replace("<endusername>", dsEmailInfo.Tables(0).Rows(0)("EndUserName").ToString)
        MsgBody = MsgBody.Replace("<enduserconnumber>", dsEmailInfo.Tables(0).Rows(0)("EndUserPhone").ToString)
        MsgBody = MsgBody.Replace("<enduseremail>", dsEmailInfo.Tables(0).Rows(0)("EndUserEmail").ToString)
        MsgBody = MsgBody.Replace("<enduserlocation>", dsEmailInfo.Tables(0).Rows(0)("EndUserLocation").ToString)
        MsgBody = MsgBody.Replace("<projectoverview>", dsEmailInfo.Tables(0).Rows(0)("ProjOverview").ToString)
        MsgBody = MsgBody.Replace("<longinfo>", dsEmailInfo.Tables(0).Rows(0)("LongInfo").ToString)
        MsgBody = MsgBody.Replace("<dbsrequired>", dsEmailInfo.Tables(0).Rows(0)("DBSReq").ToString)
        MsgBody = MsgBody.Replace("<singlemultioffice>", dsEmailInfo.Tables(0).Rows(0)("SingleOrMultiOffice").ToString)
        MsgBody = MsgBody.Replace("<distfromprod>", dsEmailInfo.Tables(0).Rows(0)("DistFromProd").ToString)
        MsgBody = MsgBody.Replace("<typeofcable>", dsEmailInfo.Tables(0).Rows(0)("TypeOfCable").ToString)
        MsgBody = MsgBody.Replace("<datatransferneeded>", dsEmailInfo.Tables(0).Rows(0)("DataTransferNeeded").ToString)
        MsgBody = MsgBody.Replace("<nameofsoftware>", dsEmailInfo.Tables(0).Rows(0)("NameOfSoftware").ToString)
        MsgBody = MsgBody.Replace("<netspeed>", dsEmailInfo.Tables(0).Rows(0)("NetSpeed").ToString)

        Dim finalMailContent As String
        finalMailContent = MsgBody

        If (AttachmentInfo.Rows.Count > 0) Then
            'Send mail to CustomerEmail with multiple attachment
            SendMail.SendMailWithMultipleAttachment(AttachmentInfo, ApplicationSettings.EmailInfo(), dsEmailInfo.Tables(0).Rows(0)("Email").ToString, finalMailContent, strsubject, True, strCC, True, True, strBCC)
        Else
            'Send mail to CustomerEmail
            SendMail.SendMail(ApplicationSettings.EmailInfo(), dsEmailInfo.Tables(0).Rows(0)("Email").ToString, finalMailContent, strsubject, True, strCC, True, True, strBCC)
        End If

    End Sub

    Public Shared Sub SendITCustomerJobBookEmail(ByVal strsubject As String, ByVal CustomerEmail As String, ByVal WorkordeId As String, ByVal StartDate As String, ByVal EndDate As String, ByVal ScopeOfWork As String, ByVal CustomerName As String, Optional ByVal CustomerRefNo As String = "")
        CommonFunctions.createLog("OrderworkLibrary - SendITCustomerJobBookEmail")
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String

        Dim strBCC As String = ""

        strBCC = ApplicationSettings.BookingConfirmationGenericBcc
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_generic.htm")

        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        'Poonam : OM-116 - OM - Replace Fax with Email for booking form with no forms,added Else part below
        If (CustomerName.Trim() = "") Then
            CustomerName = "Customer"
        End If

        MsgBody = MsgBody.Replace("<WorkordeId>", WorkordeId)
        MsgBody = MsgBody.Replace("<StartDate>", StartDate)
        MsgBody = MsgBody.Replace("<EndDate>", EndDate)
        MsgBody = MsgBody.Replace("<ScopeOfWork>", ScopeOfWork)
        MsgBody = MsgBody.Replace("<PhoneNumber>", "0203 053 0343")
        MsgBody = MsgBody.Replace("<PhoneNumberwithSpace>", "0203&nbsp;053&nbsp;0343")
        MsgBody = MsgBody.Replace("<StartDateHeader>", "Start Date:")
        MsgBody = MsgBody.Replace("<EndDateHeader>", "End Date:")
        MsgBody = MsgBody.Replace("<ScopeOfWorkHeader>", "Scope of Work:")
        MsgBody = MsgBody.Replace("<CustomerName>", CustomerName)

        Dim finalMailContent As String
        finalMailContent = MsgBody


        'Send mail to CustomerEmail
        SendMail.SendMail(ApplicationSettings.EmailInfo(), CustomerEmail, finalMailContent, strsubject, False, "", True, True, strBCC)

    End Sub
    Public Shared Sub SendTrustPilotEmail(ByVal fromEmail As String, ByVal strsubject As String, ByVal CustomerEmail As String, ByVal CustomerName As String)
        CommonFunctions.createLog("OrderworkLibrary - SendTrustPilotEmail")
        Dim MsgBody As String
        'Get content from htm file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String

        Dim strBCC As String = ""

        strBCC = ApplicationSettings.BookingConfirmationGenericBcc
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "ReviewEmailTrustpilot.htm")

        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()


        If (CustomerName.Trim() = "") Then
            CustomerName = "Customer"
        Else
            CustomerName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(CustomerName.Trim())
        End If

        MsgBody = MsgBody.Replace("<username>", CustomerName.Trim())

        Dim finalMailContent As String
        finalMailContent = MsgBody


        'Send mail to CustomerEmail
        SendMail.SendMail(fromEmail, CustomerEmail, finalMailContent, strsubject, False, "", True, True, strBCC)

    End Sub
    Public Shared Sub SendITECHSOJobAcceptEmail(ByVal WorkOrderID As String, ByVal WOStartDate As String, ByVal AptTime As String)
        CommonFunctions.createLog("OrderworkLibrary - SendITECHSOJobAcceptEmail")
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String

        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "ITECHSOJobAccept.txt")

        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()


        MsgBody = MsgBody.Replace("<WorkordeId>", WorkOrderID)
        MsgBody = MsgBody.Replace("<StartDate>", WOStartDate)
        MsgBody = MsgBody.Replace("<AptTime>", AptTime)


        Dim finalMailContent As String
        finalMailContent = MsgBody


        'Send mail to SA
        SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailSoftwareAdmin, finalMailContent, "Purchase Order Confirmation", False, "", True, True, "")

    End Sub
    Public Shared Sub SendUserProfileCompletionMail(ByVal UserName As String, ByVal Name As String)
        CommonFunctions.createLog("OrderworkLibrary - SendUserProfileCompletionMail")
        Dim strsubject As String
        strsubject = "OrderWork - Profile Completion (Minimum Requirements Met)"
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "UserProfileCompletion.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<name>", Name)
        SendMail.SendMail(ApplicationSettings.EmailInfo(), UserName, MsgBody, strsubject, False, "", True, False, "")
    End Sub

    Public Shared Sub SendRecruitmentProfileCompletionMail(ByVal dsEmailInfo As DataSet)
        CommonFunctions.createLog("OrderworkLibrary - SendRecruitmentProfileCompletionMail")
        Dim strsubject As String
        strsubject = "OrderWork - Profile Completion (Minimum Requirements Met) - " + dsEmailInfo.Tables(0).Rows(0)("Email").ToString
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "RecruitmentProfileCompletion.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        Dim CompanyProfileLink As String = ""
        If (dsEmailInfo.Tables(0).Rows.Count > 0) Then
            MsgBody = MsgBody.Replace("<companyname>", dsEmailInfo.Tables(0).Rows(0)("CompanyName").ToString)
            MsgBody = MsgBody.Replace("<companystatus>", dsEmailInfo.Tables(0).Rows(0)("CompanyStatus").ToString)
            MsgBody = MsgBody.Replace("<emailid>", dsEmailInfo.Tables(0).Rows(0)("Email").ToString)
            MsgBody = MsgBody.Replace("<fullname>", dsEmailInfo.Tables(0).Rows(0)("Fullname").ToString)
            MsgBody = MsgBody.Replace("<landline>", dsEmailInfo.Tables(0).Rows(0)("Phone").ToString)
            MsgBody = MsgBody.Replace("<mobile>", dsEmailInfo.Tables(0).Rows(0)("Mobile").ToString)
            CompanyProfileLink &= ApplicationSettings.AdminAttachmentPath & "CompanyProfile.aspx?bizDivId=1&classId=2&companyId=" & dsEmailInfo.Tables(0).Rows(0)("MainContactID").ToString

            MsgBody = MsgBody.Replace("<companyprofilelink>", CompanyProfileLink)
        End If

        SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.recruitmentEmail(), MsgBody, strsubject, False, "", True, False, "")
    End Sub

    'OM-125 : SendMail
    Public Shared Function SendOutsourceWOCompleteEmail(ByVal WorkOrderID As String, ByVal WOTitle As String, ByVal clientrefno As String, ByVal aptdate As String, ByVal ponumber As String, ByVal apttime As String, ByVal location As String) As Boolean
        CommonFunctions.createLog("OrderworkLibrary - SendOutsourceWOCompleteEmail")
        Dim Success As Boolean = False
        Dim strsubject As String
        strsubject = "Orderwork Workorder - " & WorkOrderID & " has now been marked as completed"
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        Dim aptdatetime As String = ""
        aptdatetime = aptdate + " - " + apttime
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "OutsourceWOCompleteNotification.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<WorkordeId>", WorkOrderID)
        MsgBody = MsgBody.Replace("<wotitle>", WOTitle)
        MsgBody = MsgBody.Replace("<clientrefno>", clientrefno)
        MsgBody = MsgBody.Replace("<ponumber>", ponumber)
        MsgBody = MsgBody.Replace("<aptdate>", aptdatetime)
        MsgBody = MsgBody.Replace("<location>", location)

        'Success = SendMail.SendMail( ApplicationSettings.EmailInfo() ,  ApplicationSettings.EmailSoftwareAdmin, MsgBody, strsubject)
        SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.OutsourceWOCompleteNotificationTO, MsgBody, strsubject, False, "", True, True, ApplicationSettings.OutsourceWOCompleteNotificationBCC)
        'Check whether mail to be send to admin
        Return Success
    End Function

    'NPOM-16
    Public Shared Sub SendRecruitmentNotificationEMail(ByVal dsEmailInfo As DataSet, ByVal mode As String)
        CommonFunctions.createLog("OrderworkLibrary - SendRecruitmentNotificationEMail")
        Dim strsubject As String
        strsubject = "OrderWork - Minimum requirements are updated - " + dsEmailInfo.Tables(0).Rows(0)("CompanyName").ToString
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        'NPOM-16 : New template added
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "SendRecruitmentNotificationEmail.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        Dim CompanyProfileLink As String = ""
        If (dsEmailInfo.Tables(0).Rows.Count > 0) Then
            MsgBody = MsgBody.Replace("<emailid>", dsEmailInfo.Tables(0).Rows(0)("AdminEmail").ToString)
            MsgBody = MsgBody.Replace("<fullname>", dsEmailInfo.Tables(0).Rows(0)("AdminName").ToString)
            MsgBody = MsgBody.Replace("<data>", mode)
            MsgBody = MsgBody.Replace("<updatedate>", DateTime.Now.ToString("dd MMMM yyyy : hh:mm"))

            CompanyProfileLink &= ApplicationSettings.AdminAttachmentPath & "CompanyProfile.aspx?bizDivId=1&classId=2&companyId=" & dsEmailInfo.Tables(0).Rows(0)("MainContactID").ToString

            MsgBody = MsgBody.Replace("<companyprofilelink>", CompanyProfileLink)
        End If
        SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.recruitmentEmail(), MsgBody, strsubject, False, "", True, False, "")
    End Sub

    Public Shared Function SendAccountInfoUpdate(ByVal UserName As String, ByVal EmailIdToSend As String, ByVal ccEmailIdToSend As String, ByVal SortCode As String, ByVal AccountNumber As String, ByVal AccountName As String, ByVal Name As String, ByVal PostalCode As String, ByVal Address As String, ByVal City As String, ByVal State As String, ByVal Phone As String, ByVal FirstName As String, ByVal LastName As String, ByVal Mobile As String)
        CommonFunctions.createLog("OrderworkLibrary - SendAccountInfoUpdate")
        Dim Success As Boolean = False
        ' Fetching value from typed dataset - Contacts
        Dim strsubject As String
        strsubject = "Account Information Change"
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "AccountInfoUpdate.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        Dim Mailcontent As String = ""

        If (Name <> "") Then
            Mailcontent = Mailcontent & "<br/> * Name - " & Name
        End If
        If (PostalCode <> "") Then
            Mailcontent = Mailcontent & "<br/> * PostalCode - " & PostalCode
        End If
        If (Address <> "") Then
            Mailcontent = Mailcontent & "<br/> * Address - " & Address
        End If
        If (City <> "") Then
            Mailcontent = Mailcontent & "<br/> * City - " & City
        End If
        If (State <> "") Then
            Mailcontent = Mailcontent & "<br/> * State - " & State
        End If
        If (Phone <> "") Then
            Mailcontent = Mailcontent & "<br/> * Phone - " & Phone
        End If
        If (FirstName <> "") Then
            Mailcontent = Mailcontent & "<br/> * First Name - " & FirstName
        End If
        If (LastName <> "") Then
            Mailcontent = Mailcontent & "<br/> * Last Name - " & LastName
        End If
        If (Mobile <> "") Then
            Mailcontent = Mailcontent & "<br/> * Mobile Number - " & Mobile
        End If

        MsgBody = MsgBody.Replace("<UserName>", UserName)
        MsgBody = MsgBody.Replace("<content>", Mailcontent)

        Dim strBCC As String = ""
        strBCC = ApplicationSettings.BccEmail

        Success = SendMail.SendMail(ApplicationSettings.EmailInfo(), EmailIdToSend, MsgBody, strsubject, True, ccEmailIdToSend, True, True, strBCC)

        Return Nothing
    End Function
    Public Shared Sub SendCustomerWOBookConfirmationEmail(ByVal ds As DataSet)
        Try
            CommonFunctions.createLog("OrderworkLibrary - SendCustomerWOBookConfirmationEmail")
            Dim MsgBody As String
            'Get content from txt file to get email content.
            Dim WebRequest As WebRequest
            Dim WebResponse As WebResponse
            Dim sr As StreamReader
            Dim Buffer As String

            Dim strBCC As String = ""
            If (ds.Tables(0).Rows.Count > 0) Then
                Dim customerName As String
                Dim totalTime As Integer = 0
                Dim RefWOID As String = ds.Tables(0).Rows(0).Item("RefWOID").ToString.Trim
                Dim WOTitle As String = ds.Tables(0).Rows(0).Item("WOTitle").ToString.Trim
                Dim QuestionnaireInfo As String = ds.Tables(0).Rows(0).Item("QuestionnaireInfo").ToString.Trim
                Dim StartDate As String = ds.Tables(0).Rows(0).Item("StartDate").ToString.Trim
                Dim EndDate As String = ds.Tables(0).Rows(0).Item("EndDate").ToString.Trim
                Dim AptTime As String = ds.Tables(0).Rows(0).Item("AptTime").ToString.Trim
                Dim ScopeOfWork As String = ds.Tables(0).Rows(0).Item("ScopeOfWork").ToString.Trim.Replace("&amp;", "&")
                Dim ClientScope As String = ds.Tables(0).Rows(0).Item("ClientScope").ToString.Trim.Replace("&amp;", "&")
                Dim SpecialInstructions As String = ds.Tables(0).Rows(0).Item("SpecialInstructions").ToString.Trim

                If (ds.Tables(0).Rows(0).Item("customerName").ToString.Trim = "") Then
                    customerName = ds.Tables(0).Rows(0).Item("customerFName").ToString.Trim
                Else
                    customerName = ds.Tables(0).Rows(0).Item("customerName").ToString.Trim
                End If

                Dim customerFName As String = ds.Tables(0).Rows(0).Item("customerFName").ToString.Trim
                Dim customerLName As String = ds.Tables(0).Rows(0).Item("customerLName").ToString.Trim
                Dim CustomerEmail As String = ds.Tables(0).Rows(0).Item("customerEmail").ToString.Trim
                Dim startDateDay As String = ds.Tables(0).Rows(0).Item("startDateDay").ToString.Trim
                Dim startDateMonth As String = ds.Tables(0).Rows(0).Item("startDateMonth").ToString.Trim
                Dim startDateYear As String = ds.Tables(0).Rows(0).Item("startDateYear").ToString.Trim
                Dim startDateDayName As String = ds.Tables(0).Rows(0).Item("startDateDayName").ToString.Trim
                Dim adddress As String = ds.Tables(0).Rows(0).Item("Location").ToString.Trim
                Dim accountManagerName As String = ds.Tables(0).Rows(0).Item("accountManagerName").ToString.Trim
                Dim companyLogo As String = ds.Tables(0).Rows(0).Item("companyLogo").ToString.Trim
                Dim companyName As String = ds.Tables(0).Rows(0).Item("companyName").ToString.Trim

                Dim ServicesSelectionString As String = ""
                Dim dvServicesSelectionItem As DataView
                dvServicesSelectionItem = ds.Tables(2).DefaultView
                If (dvServicesSelectionItem.Count > 0) Then
                    ServicesSelectionString = ServicesSelectionString + "<TABLE cellSpacing=0 cellPadding=8 width=80% border=0>"
                    ServicesSelectionString = ServicesSelectionString + " <TR style='white-space: nowrap;font-size: 14px;color: #fff;text-decoration: none;background-color: #B22A6A;font-weight: bold;text-align: center;border-top: 1px solid #E3E3E3;border-right: 0px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;'align = left > "
                    ServicesSelectionString = ServicesSelectionString + "      <TD style='white-space: nowrap;font-size: 14px;color: #fff;text-decoration: none;background-color: #B22A6A;font-weight: bold;text-align: center;border-top: 1px solid #E3E3E3;border-right: 1px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;' vAlign=center  height=20>Service</TD>"
                    ServicesSelectionString = ServicesSelectionString + "      <TD style='white-space: nowrap;font-size: 14px;color: #fff;text-decoration: none;background-color: #B22A6A;font-weight: bold;text-align: center;border-top: 1px solid #E3E3E3;border-right: 0px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;' vAlign=centeralign=center width=200 height=20>SKU</TD>"
                    ServicesSelectionString = ServicesSelectionString + "      <TD vAlign=center style='white-space: nowrap;font-size: 14px;color: #fff;text-decoration: none;background-color: #B22A6A;font-weight: bold;text-align: center;border-top: 1px solid #E3E3E3;border-right: 0px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;' width='75' height=20 >Quantity</TD>"
                    ServicesSelectionString = ServicesSelectionString + " </TR>"

                    For i As Integer = 0 To dvServicesSelectionItem.Count - 1
                        totalTime = totalTime + CInt(ds.Tables(2).Rows(i)("serviceTimeRequired"))
                        ServicesSelectionString = ServicesSelectionString + " <TR align=middle>"
                        ServicesSelectionString = ServicesSelectionString + "     <TD height=25 align='left' style='white-space: nowrap;text-decoration: none;background-color: #F5F4F2;border-top: 0px solid #E3E3E3;border-right: 1px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;padding-left: 20px;' >" + ds.Tables(2).Rows(i)("serviceName") + "</TD>"
                        ServicesSelectionString = ServicesSelectionString + "     <TD height=25 style='white-space: nowrap;text-decoration: none;background-color: #F5F4F2;border-top: 0px solid #E3E3E3;border-right: 0px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;'>" + ds.Tables(2).Rows(i)("serviceSKU") + "</TD>"
                        ServicesSelectionString = ServicesSelectionString + "     <TD height=25 style='white-space: nowrap;text-decoration: none;background-color: #F5F4F2;border-top: 0px solid #E3E3E3;border-right: 0px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;' >" + ds.Tables(2).Rows(i)("serviceQuantity").ToString + "</TD>"
                        ServicesSelectionString = ServicesSelectionString + " </TR>"
                    Next
                    ServicesSelectionString = ServicesSelectionString + " </TABLE>"
                End If

                Dim ServicesQAnsString As String = ""
                Dim dvServicesQAnsSelectionItem As DataView
                dvServicesQAnsSelectionItem = ds.Tables(3).DefaultView
                If (dvServicesQAnsSelectionItem.Count > 0) Then
                    ServicesQAnsString = ServicesQAnsString + "<TABLE cellSpacing=0 cellPadding=8 width=80% border=0>"
                    For i As Integer = 0 To dvServicesSelectionItem.Count - 1
                        dvServicesQAnsSelectionItem.RowFilter = "serviceId = " & dvServicesSelectionItem.Item(i)("serviceId")
                        If dvServicesQAnsSelectionItem.Count > 0 Then
                            ServicesQAnsString = ServicesQAnsString + " <TR style='white-space: nowrap;font-size: 14px;color: #fff;text-decoration: none;background-color: #B22A6A;font-weight: bold;text-align: center;border-top: 1px solid #E3E3E3;border-right: 0px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;'align = left > "
                            ServicesQAnsString = ServicesQAnsString + "      <TD style='white-space: nowrap;font-size: 14px;color: #fff;text-decoration: none;background-color: #B22A6A;font-weight: bold;text-align: center;border-top: 1px solid #E3E3E3;border-right: 1px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;' vAlign=center  height=20>" + dvServicesSelectionItem.Item(i)("serviceName").ToString + "</TD>"
                            ServicesQAnsString = ServicesQAnsString + "      <TD style='white-space: nowrap;font-size: 14px;color: #fff;text-decoration: none;background-color: #B22A6A;font-weight: bold;text-align: center;border-top: 1px solid #E3E3E3;border-right: 0px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;' vAlign=centeralign=center width=500 height=20></TD>"
                            ServicesQAnsString = ServicesQAnsString + " </TR>"

                            For j As Integer = 0 To dvServicesQAnsSelectionItem.Count - 1
                                ServicesQAnsString = ServicesQAnsString + " <TR align=middle>"
                                ServicesQAnsString = ServicesQAnsString + "     <TD height=25 align='left' style='white-space: nowrap;text-decoration: none;background-color: #F5F4F2;border-top: 0px solid #E3E3E3;border-right: 1px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;padding-left: 20px;' >" + dvServicesQAnsSelectionItem.Item(j)("questionText") + "</TD>"
                                ServicesQAnsString = ServicesQAnsString + "     <TD height=25 style='white-space: nowrap;text-decoration: none;background-color: #F5F4F2;border-top: 0px solid #E3E3E3;border-right: 0px solid #E3E3E3;border-bottom: 1px solid #E3E3E3;border-left: 1px solid #E3E3E3;'>&nbsp;" + dvServicesQAnsSelectionItem.Item(j)("answerText") + "</TD>"
                                ServicesQAnsString = ServicesQAnsString + " </TR>"
                            Next
                        End If
                    Next
                    ServicesQAnsString = ServicesQAnsString + " </TABLE>"
                End If

                Dim strsubject As String = "Orderwork - Booking reference " & RefWOID

                If (QuestionnaireInfo <> "" And QuestionnaireInfo <> "''") Then
                    WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "CustomerWOBookConfirmationWithQuestion.html")
                Else
                    WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "CustomerWOBookConfirmationWithoutQuestion.html")
                End If

                strBCC = ApplicationSettings.BookingConfirmationGenericBcc

                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                MsgBody = Buffer
                sr.Close()

                MsgBody = MsgBody.Replace("<servicesselection>", ServicesSelectionString)
                MsgBody = MsgBody.Replace("<username>", customerName)
                'MsgBody = MsgBody.Replace("<servicename>", WOTitle)
                'MsgBody = MsgBody.Replace("<questionAnswers>", QuestionnaireInfo)
                MsgBody = MsgBody.Replace("<questionAnswers>", ServicesQAnsString)
                MsgBody = MsgBody.Replace("<startDateDay>", startDateDay)
                MsgBody = MsgBody.Replace("<startDateMonth>", startDateMonth)
                MsgBody = MsgBody.Replace("<startDateYear>", startDateYear)
                MsgBody = MsgBody.Replace("<startDateDayName>", startDateDayName)
                MsgBody = MsgBody.Replace("<preferredTime>", AptTime)
                MsgBody = MsgBody.Replace("<adddress>", adddress)

                Dim hours As Integer = totalTime \ 60
                Dim minutes As Integer = totalTime - (hours * 60)

                If (minutes = 0) Then
                    MsgBody = MsgBody.Replace("<totalTime>", hours.ToString & " Hour(s)")
                Else
                    MsgBody = MsgBody.Replace("<totalTime>", hours.ToString & " Hour(s) " & minutes.ToString & " Minute(s)")
                End If

                MsgBody = MsgBody.Replace("<scopeOfWork>", ScopeOfWork)

                If (SpecialInstructions <> "") Then
                    MsgBody = MsgBody.Replace("<specialInstructions>", "Please ensure that you have &#058;<pre style='font-size:15.5px;line-height:25px;font-family:sans-serif;color:red;'>" & SpecialInstructions & "</pre>")
                End If
                If (accountManagerName <> "") Then
                    MsgBody = MsgBody.Replace("<accountManagerName>", "<p>Your service desk representative for this job is " & accountManagerName & " however any member of our team will be able to assist you on the day&#46;</p>")
                End If

                If (companyLogo <> "") Then
                    MsgBody = MsgBody.Replace("<companyLogo>", OrderWorkLibrary.ApplicationSettings.AzureStoragePath.ToString + "OWMyUK/AttachmentsUK/ContactPhoto/" + companyLogo.Substring(companyLogo.LastIndexOf("\") + 1).Replace("E:/", "") + "?sv=2017-11-09&ss=bfqt&srt=sco&sp=rwdlacup&se=2019-12-31T00:41:35Z&st=2018-11-19T16:41:35Z&spr=https,http&sig=F2ZFOB2PrFBCTl0VuBgwJcLxIxjB9rvDMskqkGoOIC8%3D")
                    MsgBody = MsgBody.Replace("<companyName>", companyName)
                Else
                    MsgBody = MsgBody.Replace("<companyLogo>", "https://my.orderwork.co.uk/register/assets/ordersupportlink.png")
                    MsgBody = MsgBody.Replace("<companyName>", "OrderWork")
                End If

                MsgBody = MsgBody.Replace("<postConfirmationURL>", "<a class='ancStyle' style='color:#086faa;text-decoration:none;cursor:pointer;font-family: verdana;font-size: 12px;' href='" + ApplicationSettings.NewPortalURL + "postconfirmation/" & ds.Tables(0).Rows(0).Item("WOID").ToString.Trim & "/" & ds.Tables(0).Rows(0).Item("CompanyId").ToString.Trim & "'  target='_blank'>click here</a>")

                Dim finalMailContent As String
                finalMailContent = MsgBody

                OrderWorkLibrary.CommonFunctions.createLog("OWPORTAL - Customer Email Sent to  - " & CustomerEmail & "For - " & RefWOID)

                'Send mail to CustomerEmail
                SendMail.SendMail(ApplicationSettings.EmailInfo(), CustomerEmail, finalMailContent, strsubject, False, "", True, True, strBCC)

            End If
        Catch ex As Exception
            OrderWorkLibrary.CommonFunctions.createLog("OWPORTAL - Issue in Customer email sending  - " & ex.ToString)
        End Try
    End Sub
    Public Shared Function SendInvalidLatLongEmail(ByVal Postcode As String, ByVal RecordType As String, ByVal ID As String) As Boolean
        CommonFunctions.createLog("OrderworkLibrary - SendInvalidLatLongEmail")
        Dim Success As Boolean = False
        Dim strsubject As String = "Orderwork � Incomplete location coordinates"
        Dim WebRequest As WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "InvalidLatLongEmail.htm")
        Dim WebResponse As WebResponse = WebRequest.GetResponse()
        Dim sr As StreamReader = New StreamReader(WebResponse.GetResponseStream())
        Dim Buffer As String = sr.ReadToEnd
        Dim MsgBody As String = Buffer

        sr.Close()

        If RecordType = "workorder" Then
            MsgBody = MsgBody.Replace("<iswoorlocation>", "workorder")
        Else
            MsgBody = MsgBody.Replace("<iswoorlocation>", "location")
        End If
        MsgBody = MsgBody.Replace("<postcode>", Postcode)
        MsgBody = MsgBody.Replace("<recordtype>", RecordType)
        MsgBody = MsgBody.Replace("<id>", ID)
        CommonFunctions.createLog("Orderwork � Sending email for Incomplete location coordinates for :" + Postcode + "-" + ID)
        SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailPortalAdmin, MsgBody, strsubject, False, "", True, False, "")
        CommonFunctions.createLog("Orderwork � Sent email for Incomplete location coordinates for :" + Postcode)
        'Check whether mail to be send to admin
        Return Success
    End Function
    Public Shared Function Send7dayPortalpaymentalert(ByVal WOID As String, ByVal ClientCompanyName As String, ByVal SupplierCompanyName As String, ByVal CompletedDate As String) As Boolean
        Try
            CommonFunctions.createLog("OrderworkLibrary - Send7dayPortalpaymentalert")
            Dim Success As Boolean = False
            Dim strsubject As String = "Orderwork - Empowered Workorder with 7-day Payment Terms"
            Dim WebRequest As WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "EmpoweredWorkorderwithPaymentAlert.htm")
            Dim WebResponse As WebResponse = WebRequest.GetResponse()
            Dim sr As StreamReader = New StreamReader(WebResponse.GetResponseStream())
            Dim Buffer As String = sr.ReadToEnd
            Dim MsgBody As String = Buffer

            sr.Close()

            MsgBody = MsgBody.Replace("<woid>", WOID)
            MsgBody = MsgBody.Replace("<client>", ClientCompanyName)
            MsgBody = MsgBody.Replace("<supplier>", SupplierCompanyName)
            MsgBody = MsgBody.Replace("<completeddate>", CompletedDate)
            CommonFunctions.createLog("Orderwork - Empowered Workorder with 7-day Payment Terms for :" + WOID)
            SendMail.SendMail(ApplicationSettings.EmailPortalAdmin, ApplicationSettings.FinanceEmail, MsgBody, strsubject, False, "", True, False, "")
            CommonFunctions.createLog("Orderwork - Empowered Workorder with 7-day Payment Terms email sent successfully for :" + WOID)
            Return Success
        Catch ex As Exception
            CommonFunctions.createLog(ex.ToString())
        End Try
    End Function
    Public Shared Sub SendAutoAcceptWOEmailToClient(ByVal clientSubject As String, ByVal clientMailbody As String, ByVal clientRecipients As String, ByVal clientCCList As String)
        Try
            CommonFunctions.createLog("OrderworkLibrary - SendAutoAcceptWOEmailToClient")
            SendMail.SendMail(ApplicationSettings.EmailInfo(), clientRecipients, clientMailbody, clientSubject, True, clientCCList, True, False, "")
        Catch ex As Exception
            CommonFunctions.createLog(ex.ToString())
        End Try
    End Sub
    Public Shared Sub SendAutoAcceptWOEmailToSP(ByVal SPSubject As String, ByVal SPMailbody As String, ByVal SPRecipients As String, ByVal SPCCList As String)
        Try
            CommonFunctions.createLog("OrderworkLibrary - SendAutoAcceptWOEmailToSP")
            SendMail.SendMail(ApplicationSettings.EmailInfo(), SPRecipients, SPMailbody, SPSubject, True, SPCCList, True, False, "")
        Catch ex As Exception
            CommonFunctions.createLog(ex.ToString())
        End Try
    End Sub
End Class
