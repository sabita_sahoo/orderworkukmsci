'------------------------------------------------------------------------------
' <Summary>
'   <ProjectName>Orderwork</ProjectName>
'   <FileName>SendMail.vb</FileName>
'   <Module>Send Mail</Module>
'   <Description> Contains Function to send mails </Description>
'   <CreatedDate>1/03/2007</CreatedDate>
'   <References>
'        <Reference>http://msdn2.microsoft.com/en-us/library/system.net.mail.aspx</Reference>
'   </References>
' </Summary>
'------------------------------------------------------------------------------



'import the namespace
Imports System.Net.Mail
Imports System.Text
Imports System.Net
Imports System.Collections
Imports SendGrid
Imports SendGrid.Helpers.Mail
Imports Attachment = System.Net.Mail.Attachment

''' <summary>
'''  Sendmail class to handle send mail functionality
''' </summary>
''' <remarks></remarks>
Public Class SendMail

    ''' <summary>
    ''' Function to send mail with the paramter as From, To, Subject and body message
    ''' </summary>
    ''' <param name="mailFrom">Specify Mail from id</param>
    ''' <param name="mailTo">Specify Mail to id</param>
    ''' <param name="mailBody">Specify Mail Body</param>
    ''' <param name="mailSubject">Specify Mail subject</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SendMail(ByVal mailFrom As String, ByVal mailTo As String, ByVal mailBody As String, ByVal mailSubject As String, Optional ByVal isCCSend As Boolean = False, Optional ByVal ccEmail As String = "", Optional ByVal IsHtml As Boolean = False, Optional ByVal isBCCSend As Boolean = False, Optional ByVal bccEmail As String = "") As Boolean
        'If the SMTP MAIL SERVER is not provided in config file, throw a Server Fault Code Soap Exception
        Dim SMTPMAILSERVER As String = ""
        Try
            SMTPMAILSERVER = ApplicationSettings.SMTPServer
        Catch Ex As Exception
            Dim ex1 As New Exception("Unable to find value for key - webSite_Smtp_Mail_Server - . Unexpected error occured in server while sending email.")
            Throw (ex1)
        End Try
        If IsNothing(mailTo) Then
            Return False
        ElseIf mailTo = "" Then
            Return False
        Else
        End If
        ' setting the "from" and "to" field using MailAddress property value
        'Dim from As New MailAddress(mailFrom)
        Dim from As New MailAddress(mailFrom)
        Dim sendTo As New MailAddress(mailTo)
        Dim message As New MailMessage(from, sendTo)
        Dim personalization As New Personalization()

        Dim i As Integer
        If isCCSend = True And ccEmail <> "" Then
            Dim CCs() As String = Split(ccEmail, "; ")
            Dim ccList As New List(Of EmailAddress)
            For i = 0 To CCs.GetLength(0) - 1
                If CCs.GetValue(i) <> "" And CCs.GetValue(i).ToString() <> mailTo Then
                    Dim sendCC As New MailAddress(CCs.GetValue(i).ToString())
                    Dim sendGCC As New EmailAddress(CCs.GetValue(i).ToString())
                    ccList.Add(sendGCC)
                    message.CC.Add(sendCC)
                End If
            Next
            personalization.Ccs = ccList
        End If
        'BCC  
        If isBCCSend = True And bccEmail <> "" Then
            Dim BCCs() As String = Split(bccEmail, "; ")
            Dim bccList As New List(Of EmailAddress)
            For i = 0 To BCCs.GetLength(0) - 1
                If BCCs.GetValue(i) <> "" And BCCs.GetValue(i).ToString() <> mailTo Then
                    Dim sendBCC As New MailAddress(BCCs.GetValue(i).ToString())
                    Dim sendGBCC As New EmailAddress(BCCs.GetValue(i).ToString())
                    message.Bcc.Add(sendBCC)
                    bccList.Add(sendGBCC)
                End If
            Next
            personalization.Bccs = bccList
        End If
        'set the message subject
        message.Subject = mailSubject
        'set the message body    
        message.Body = mailBody
        'set message priority 
        message.Priority = MailPriority.Normal
        message.IsBodyHtml = IsHtml

        'creating a new smtpclient object to send the mail
        If (ApplicationSettings.SMTPServer.ToString.ToLower = "localhost") Then
            Dim client As New SmtpClient(SMTPMAILSERVER)
            client.UseDefaultCredentials = True
            ' Sabita added Try catch on 17/02/15
            Try
                client.Send(message)
            Catch ex As Exception
                'Do Nothing - In dev email doesnt work. Live it will be good.
            End Try
        ElseIf (ApplicationSettings.SMTPServer.ToString.ToLower = "sendgrid") Then
            'Dim SendGridApiKey As String = "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"
            Try
                Dim SendGridApiKey As String = ApplicationSettings.SendGridApiKey.ToString()
                SendGridMail.SGMail.Execute(mailFrom, mailTo, mailBody, mailSubject, personalization, SendGridApiKey).Wait()
            Catch ex As Exception
                OrderWorkLibrary.CommonFunctions.createLog("Send Emails using Sengdrid : " & ex.ToString())
            End Try
        Else
            Dim client As New SmtpClient("smtp.office365.com", 587)
            client.DeliveryMethod = SmtpDeliveryMethod.Network
            client.UseDefaultCredentials = False
            client.Credentials = New NetworkCredential(ApplicationSettings.SMTPServerUserId.ToString, ApplicationSettings.SMTPServerPassword.ToString)
            client.TargetName = "STARTTLS/smtp.office365.com"
            client.EnableSsl = True
            ' Sabita added Try catch on 17/02/15
            Try
                client.Send(message)
            Catch ex As Exception
                OrderWorkLibrary.CommonFunctions.createLog("Send Emails using O365 : " & ex.ToString())
            End Try
        End If
        Return True
    End Function

    Public Shared Function SendMailKnowhowDixons(ByVal mailFrom As String, ByVal mailTo As String, ByVal mailBody As String, ByVal mailSubject As String, Optional ByVal isCCSend As Boolean = False, Optional ByVal ccEmail As String = "", Optional ByVal IsHtml As Boolean = False, Optional ByVal isBCCSend As Boolean = False, Optional ByVal bccEmail As String = "") As Boolean
        'If the SMTP MAIL SERVER is not provided in config file, throw a Server Fault Code Soap Exception
        Dim SMTPMAILSERVER As String = ""
        Try
            SMTPMAILSERVER = ApplicationSettings.SMTPServer
        Catch Ex As Exception
            Dim ex1 As New Exception("Unable to find value for key - webSite_Smtp_Mail_Server - . Unexpected error occured in server while sending email.")
            Throw (ex1)
        End Try
        If IsNothing(mailTo) Then
            Return False
        ElseIf mailTo = "" Then
            Return False
        Else
        End If
        ' setting the "from" and "to" field using MailAddress property value
        'Dim from As New MailAddress(mailFrom)
        Dim from As New MailAddress(mailFrom)
        Dim sendTo As New MailAddress(mailTo)
        Dim message As New MailMessage(from, sendTo)
        Dim i As Integer
        Dim personalization As New Personalization()
        If isCCSend = True And ccEmail <> "" Then
            Dim CCs() As String = Split(ccEmail, "; ")
            Dim ccList As New List(Of EmailAddress)
            For i = 0 To CCs.GetLength(0) - 1
                If CCs.GetValue(i) <> "" And CCs.GetValue(i).ToString() <> mailTo Then
                    Dim sendCC As New MailAddress(CCs.GetValue(i).ToString())
                    Dim sendGCC As New EmailAddress(CCs.GetValue(i).ToString())
                    ccList.Add(sendGCC)
                    message.CC.Add(sendCC)
                End If
            Next
            Personalization.Ccs = ccList
        End If
        'BCC  
        If isBCCSend = True And bccEmail <> "" Then
            Dim BCCs() As String = Split(bccEmail, "; ")
            Dim bccList As New List(Of EmailAddress)
            For i = 0 To BCCs.GetLength(0) - 1
                If BCCs.GetValue(i) <> "" And BCCs.GetValue(i).ToString() <> mailTo Then
                    Dim sendBCC As New MailAddress(BCCs.GetValue(i).ToString())
                    Dim sendGBCC As New EmailAddress(BCCs.GetValue(i).ToString())
                    message.Bcc.Add(sendBCC)
                    bccList.Add(sendGBCC)
                End If
            Next
            Personalization.Bccs = bccList
        End If
        'set the message subject
        message.Subject = mailSubject
        'set the message body    
        message.Body = mailBody
        'set message priority 
        message.Priority = MailPriority.Normal
        message.IsBodyHtml = IsHtml

        'creating a new smtpclient object to send the mail
        If (ApplicationSettings.SMTPServer.ToString.ToLower = "localhost") Then
            Dim client As New SmtpClient(SMTPMAILSERVER)
            client.UseDefaultCredentials = True
            ' Sabita added Try catch on 17/02/15
            Try
                client.Send(message)
            Catch ex As Exception
                'Do Nothing - In dev email doesnt work. Live it will be good.
            End Try
        ElseIf (ApplicationSettings.SMTPServer.ToString.ToLower = "sendgrid") Then
            'Dim SendGridApiKey As String = "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"
            Try
                Dim SendGridApiKey As String = ApplicationSettings.SendGridApiKey.ToString()
                SendGridMail.SGMail.Execute(mailFrom, mailTo, mailBody, mailSubject, Personalization, SendGridApiKey).Wait()
            Catch ex As Exception
                OrderWorkLibrary.CommonFunctions.createLog("Send Emails using Sendgrid : " & ex.ToString())
            End Try
        Else
            Dim client As New SmtpClient("smtp.office365.com", 587)
            client.DeliveryMethod = SmtpDeliveryMethod.Network
            client.UseDefaultCredentials = False
            client.Credentials = New NetworkCredential(ApplicationSettings.SMTPServerUserId.ToString, ApplicationSettings.SMTPServerPassword.ToString)
            client.TargetName = "STARTTLS/smtp.office365.com"
            client.EnableSsl = True
            ' Sabita added Try catch on 17/02/15
            Try
                client.Send(message)
            Catch ex As Exception
                OrderWorkLibrary.CommonFunctions.createLog("Send Emails using O365 : " & ex.ToString())
            End Try


        End If
        Return True
    End Function

    Public Shared Function SendMailWithAttachment(ByVal pAttachment As Attachment, ByVal mailFrom As String, ByVal mailTo As String, ByVal mailBody As String, ByVal mailSubject As String, Optional ByVal isCCSend As Boolean = False, Optional ByVal ccEmail As String = "", Optional ByVal IsHtml As Boolean = False, Optional ByVal isBCCSend As Boolean = False, Optional ByVal bccEmail As String = "") As Boolean
        'If the SMTP MAIL SERVER is not provided in config file, throw a Server Fault Code Soap Exception
        Dim SMTPMAILSERVER As String = ""

        Try
            SMTPMAILSERVER = ApplicationSettings.SMTPServer
        Catch Ex As Exception
            Dim ex1 As New Exception("Unable to find value for key - webSite_Smtp_Mail_Server - . Unexpected error occured in server while sending email.")
            Throw (ex1)
        End Try
        If IsNothing(mailTo) Then
            Return False
        ElseIf mailTo = "" Then
            Return False
        Else
        End If
        ' setting the "from" and "to" field using MailAddress property value
        'Dim from As New MailAddress(mailFrom)
        Dim from As New MailAddress(mailFrom)
        Dim sendTo As New MailAddress(mailTo)
        Dim message As New MailMessage(from, sendTo)
        Dim i As Integer
        If isCCSend = True And ccEmail <> "" Then
            Dim CCs() As String = Split(ccEmail, "; ")
            For i = 0 To CCs.GetLength(0) - 1
                If CCs.GetValue(i) <> "" Then
                    Dim sendCC As New MailAddress(CCs.GetValue(i))
                    message.CC.Add(sendCC)
                End If
            Next
        End If

        'BCC  
        If isBCCSend = True And bccEmail <> "" Then
            Dim BCCs() As String = Split(bccEmail, "; ")
            For i = 0 To BCCs.GetLength(0) - 1
                If BCCs.GetValue(i) <> "" Then
                    Dim sendBCC As New MailAddress(BCCs.GetValue(i))
                    message.Bcc.Add(sendBCC)
                End If
            Next
        End If

        'set the message subject
        message.Subject = mailSubject
        'set the message body    
        message.Body = mailBody
        'set message priority 
        message.Priority = MailPriority.Normal
        message.IsBodyHtml = IsHtml

        'send the attachment with email, if any
        If Not IsNothing(pAttachment) Then
            message.Attachments.Add(pAttachment)
        End If

        'creating a new smtpclient object to send the mail
        If (ApplicationSettings.SMTPServer.ToString.ToLower = "localhost") Then
            Dim client As New SmtpClient(SMTPMAILSERVER)
            client.UseDefaultCredentials = True
            ' Sabita added Try catch on 17/02/15
            Try
                client.Send(message)
            Catch ex As Exception
                'Do Nothing - In dev email doesnt work. Live it will be good.
            End Try
        Else
            Dim client As New SmtpClient("smtp.office365.com", 587)
            client.DeliveryMethod = SmtpDeliveryMethod.Network
            client.UseDefaultCredentials = False
            client.Credentials = New NetworkCredential(ApplicationSettings.SMTPServerUserId.ToString, ApplicationSettings.SMTPServerPassword.ToString)
            client.TargetName = "STARTTLS/smtp.office365.com"
            client.EnableSsl = True
            ' Sabita added Try catch on 17/02/15
            Try
                client.Send(message)
            Catch ex As Exception
                'Do Nothing - In dev email doesnt work. Live it will be good.
            End Try
        End If
        Return True
    End Function


    Public Shared Function SendMailWithMultipleAttachment(ByVal AttachmentInfo As DataTable, ByVal mailFrom As String, ByVal mailTo As String, ByVal mailBody As String, ByVal mailSubject As String, Optional ByVal isCCSend As Boolean = False, Optional ByVal ccEmail As String = "", Optional ByVal IsHtml As Boolean = False, Optional ByVal isBCCSend As Boolean = False, Optional ByVal bccEmail As String = "") As Boolean
        'If the SMTP MAIL SERVER is not provided in config file, throw a Server Fault Code Soap Exception
        Dim SMTPMAILSERVER As String = ""

        Try
            SMTPMAILSERVER = ApplicationSettings.SMTPServer
        Catch Ex As Exception
            Dim ex1 As New Exception("Unable to find value for key - webSite_Smtp_Mail_Server - . Unexpected error occured in server while sending email.")
            Throw (ex1)
        End Try
        If IsNothing(mailTo) Then
            Return False
        ElseIf mailTo = "" Then
            Return False
        Else
        End If
        ' setting the "from" and "to" field using MailAddress property value
        'Dim from As New MailAddress(mailFrom)
        Dim from As New MailAddress(mailFrom)
        Dim sendTo As New MailAddress(mailTo)
        Dim message As New MailMessage(from, sendTo)
        Dim i As Integer
        If isCCSend = True And ccEmail <> "" Then
            Dim CCs() As String = Split(ccEmail, "; ")
            For i = 0 To CCs.GetLength(0) - 1
                If CCs.GetValue(i) <> "" Then
                    Dim sendCC As New MailAddress(CCs.GetValue(i))
                    message.CC.Add(sendCC)
                End If
            Next
        End If

        'BCC  
        If isBCCSend = True And bccEmail <> "" Then
            Dim BCCs() As String = Split(bccEmail, "; ")
            For i = 0 To BCCs.GetLength(0) - 1
                If BCCs.GetValue(i) <> "" Then
                    Dim sendBCC As New MailAddress(BCCs.GetValue(i))
                    message.Bcc.Add(sendBCC)
                End If
            Next
        End If

        'set the message subject
        message.Subject = mailSubject
        'set the message body    
        message.Body = mailBody
        'set message priority 
        message.Priority = MailPriority.Normal
        message.IsBodyHtml = IsHtml

        'send the multiple attachment with email, if any
        Dim strAttachmenURL As System.Net.Mail.Attachment
        For Each objDataRow As DataRow In AttachmentInfo.Rows
            strAttachmenURL = New System.Net.Mail.Attachment(objDataRow("FilePath").ToString)
            strAttachmenURL.ContentDisposition.FileName = objDataRow("Name").ToString
            message.Attachments.Add(strAttachmenURL)
        Next
        'If Not IsNothing(pAttachment) Then
        '    message.Attachments.Add(pAttachment)
        'End If

        'creating a new smtpclient object to send the mail
        If (ApplicationSettings.SMTPServer.ToString.ToLower = "localhost") Then
            Dim client As New SmtpClient(SMTPMAILSERVER)
            client.UseDefaultCredentials = True
            ' Sabita added Try catch on 17/02/15
            Try
                client.Send(message)
            Catch ex As Exception
                'Do Nothing - In dev email doesnt work. Live it will be good.
            End Try
        Else
            Dim client As New SmtpClient("smtp.office365.com", 587)
            client.DeliveryMethod = SmtpDeliveryMethod.Network
            client.UseDefaultCredentials = False
            client.Credentials = New NetworkCredential(ApplicationSettings.SMTPServerUserId.ToString, ApplicationSettings.SMTPServerPassword.ToString)
            client.TargetName = "STARTTLS/smtp.office365.com"
            client.EnableSsl = True
            ' Sabita added Try catch on 17/02/15
            Try
                client.Send(message)
            Catch ex As Exception
                'Do Nothing - In dev email doesnt work. Live it will be good.
            End Try
        End If
        Return True
    End Function



End Class
