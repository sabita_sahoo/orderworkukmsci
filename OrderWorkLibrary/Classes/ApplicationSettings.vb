''' <summary>
''' Class contains shared properties which returns application settings key values.
''' Calling project must contain the  required key in web.config
''' </summary>
''' <author>Chandrashekhar Muradnar</author>
''' <remarks></remarks>
Public Class ApplicationSettings
    ''' <summary>
    ''' Enumerated datatype for user type
    ''' </summary>
    ''' <remarks></remarks>

    Public Enum UserType
        buyer
        supplier
    End Enum
    ''' <summary>
    ''' Enumerated datatype for site types (site/admin)
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum siteTypes
        admin
        site
    End Enum

    ''' <summary>
    ''' Enumerated contact type used in AMContacts listing
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum ContactType
        approvedBuyer
        suspendedBuyer
        newSupplier
        approvedSupplier
        suspendedSupplier
        validatedSpecialists
        nonValidatedSpecialists
        deletedAccount
    End Enum
    Public Enum WOStatusID
        Draft = 1
        Submitted = 2
        Accepted = 6
        Issue = 7
        Completed = 9
        Closed = 10
        Cancelled = 11
        InTray = 28
        Lost = 34
        EnquiryDraft = 41
        EnquirySubmitted = 42
        CA = 5
        CAMailSent = 63
        Sent = 3
        BuyerAccepted = 50
        SupplierAccepted = 51
        Discarded = 4
    End Enum
    ''' <summary>
    ''' WOActions enum
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum WOAction
        SubmitWO
        WOSPAccept
        WOSPCAccept
        WOSPChangeCA
        WORaiseIssue
        WOChangeIssue
        WOAcceptIssue
        WOAcceptCA
        WODiscuss
        WODiscussCA
        WOComplete
        WOClose
        CancelWO
    End Enum

    Public Enum Menus
        Home = 219
        WorkOrders = 232
        WorkOrdersBuyer = 231
        AccountSettings = 220
        Billing = 235
        Bookings = 392
        Messages = 414
        LeaveFeedback = 416
        BestBuySLA = 427
    End Enum

    Public Enum WebContents
        PressReleases = 67
        Articles = 68
        Careers = 69
        Quotation = 70
    End Enum

    ''' <summary>
    ''' defines the source for message either from site resource file or library resource file
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum MessageSource
        site
        library
        libraryde
    End Enum

    ''' <summary>
    ''' Enumerated datatype for sane mail to - Client / Supplier / Admin
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum SendMailTo
        Buyer
        Supplier
        OWAdmin
        BuyerNOWAdmin
        SupplierNBuyer
        SupplierNOWAdmin
        All
    End Enum

    ''' <summary>
    ''' property returns the value, if rating should be shown or not
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ShowRating()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ShowRating")
        End Get
    End Property
    ''' <summary>
    ''' this gets the migrate page name
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OuterMigratePage()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OuterMigratePage").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Site name
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SiteName()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SiteName").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property OnStage()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OnStage").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property OnLive()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OnLive").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Other Site Name
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OtherSiteName()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OtherSiteName").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Returns ID for Goods location
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property GetLibInput()
        Get
            Return "drpdwnGoodsCollection".ToString
        End Get
    End Property


    ''' <summary>
    ''' otehr site url
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OtherWebPath()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OtherWebPath").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns the country for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Country()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("Country").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns the UK string  for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CountryUK()
        Get
            Return "UK"
        End Get
    End Property

    ''' <summary>
    ''' property returns the German string  for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CountryDE()
        Get
            Return "DE"
        End Get
    End Property


    ''' <summary>
    ''' property returns the country for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property DefaultCountry()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("DefaultCountry").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns BusinessId for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property BusinessId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BusinessId").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns bizdivid for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property BizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BizDivId").ToString()
        End Get
    End Property
    ''' <summary>
    ''' This property returns the page size of the gridview
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property GridViewPageSize()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("GridViewPageSize").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property BusinessAreaUndefined()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BusinessAreaUndefined").ToString()
        End Get
    End Property

    ''' <summary>
    ''' propery returns type of website, wether its admin website or client site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SiteType()
        Get
            Select Case System.Configuration.ConfigurationManager.AppSettings("SiteType").ToString()
                Case "site"
                    Return siteTypes.site
                Case "admin"
                    Return siteTypes.admin
                Case Else
                    Return siteTypes.site
            End Select

        End Get
    End Property

    ''' <summary>
    ''' propery returns type of website, wether its admin website or client site in String format
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SiteTypeString()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SiteType").ToString()

        End Get
    End Property

    ''' <summary>
    ''' propery returns OWID for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleOWID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleOWID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns guest login
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property GuestLogin()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("GuestLogin").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns guest password
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property GuestPassword()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("GuestPassword").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns "SendGridApiKey" name
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SendGridApiKey()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SendGridApiKey").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns "SMTP server" name
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SMTPServer()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SMTPServer").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property SMTPServerUserId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SMTPServerUserId").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property SMTPServerPassword()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SMTPServerPassword").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns "MailFrom" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MailFrom()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MailFrom").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns "EmailInfo" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property EmailInfo()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("EmailInfo").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property EnquiryToEmail()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("EnquiryToEmail").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property DixonsComplaintEmail()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("DixonsComplaintEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property TimeBuilderQLimit()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("TimeBuilderQLimit").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns "Role_SupplierID" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleSupplierID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleSupplierID")
        End Get
    End Property


    ''' <summary>
    ''' property returns "Role_ClientID" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleClientID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleClientID").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns "Role_ClientID" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Shared ReadOnly Property ClassApprovedSupplier()
    '    Get
    '        Return System.Configuration.ConfigurationManager.AppSettings("ClassApprovedSupplier").ToString()
    '    End Get
    'End Property

    ''' <summary>
    ''' property returns "Class_NewSupplier" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Shared ReadOnly Property ClassNewSupplier()
    '    Get
    '        Return System.Configuration.ConfigurationManager.AppSettings("ClassNewSupplier").ToString()
    '    End Get
    'End Property

    ''' <summary>
    ''' property returns "Status_New" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Shared ReadOnly Property StatusNew()
    '    Get
    '        Return System.Configuration.ConfigurationManager.AppSettings("StatusNew").ToString()
    '    End Get
    'End Property

    ''' <summary>
    ''' property returns MigrationSwitchStatus value if its 1 then migration checks will be done
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MigrationSwitchStatus()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MigrationSwitchStatus").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns "CountryID_UK" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CountryID_UK()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CountryID_UK").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns "CountryID_DE" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CountryID_DE()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CountryID_DE").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns Micrsoft Exam Id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MSExamID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MSExamID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns ClassSuspendedSupplier
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Shared ReadOnly Property ClassSuspendedSupplier()
    '    Get
    '        Return System.Configuration.ConfigurationManager.AppSettings("ClassSuspendedSupplier").ToString()
    '    End Get
    'End Property



    ''' <summary>
    ''' Returns Value of RoleClientAdminID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleOWAdminID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleOWAdminID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Value of Roleowrep id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Public Shared ReadOnly Property RoleOWRepID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleOWRepID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Value of RoleClientAdminID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleClientAdminID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleClientAdminID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Value of RoleSupplierAdminID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleSupplierAdminID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleSupplierAdminID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Value of RoleClientAdminID
    ''' </summary>
    ''' <value>2</value>
    ''' <returns>2</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleSupplierManagerID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleSupplierManagerID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Value of RoleClientAdminID
    ''' </summary>
    ''' <value>5</value>
    ''' <returns>5</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleClientManagerID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleClientManagerID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Value of RoleClientAdminID
    ''' </summary>
    ''' <value>8</value>
    ''' <returns>8</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleSupplierUserID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleSupplierUserID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Value of RoleClientAdminID
    ''' </summary>
    ''' <value>6</value>
    ''' <returns>6</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleClientUserID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleClientUserID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Value of RoleSupplierAccountsID
    ''' </summary>
    ''' <value>6</value>
    ''' <returns>6</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleSupplierAccountsID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleSupplierAccountsID").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Returns Value of RoleClientAccountsID
    ''' </summary>
    ''' <value>6</value>
    ''' <returns>6</returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleClientAccountsID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleClientAccountsID").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property RoleClientRetailID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleClientRetailID").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property RoleSupplierReadOnly()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleSupplierReadOnly").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property RoleSupplierRetailID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleSupplierRetailID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns BizDivId for Order Work UK site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OWUKBizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OWUKBizDivId").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns BizDivId for Skills Finder UK    
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SFUKBizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SFUKBizDivId").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns BizDivId for Order Work DE
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OWDEBizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OWDEBizDivId").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns BizDivId for Skills Finder DE    
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SFDEBizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SFDEBizDivId").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns BizDivId of peer application for SF UK
    ''' for example for OW UK peer1 application is SF UK 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SFUKPeer1BizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SFUKPeer1BizDivId").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns BizDivId of peer application for OW
    ''' for example for OW UK peer1 application is SF UK 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OWUKPeer1BizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OWUKPeer1BizDivId").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns BizDivId of peer application for SF DE
    ''' for example for OW UK peer1 application is SF UK 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SFDEPeer1BizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SFDEPeer1BizDivId").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns BizDivId of peer application for OW DE
    ''' for example for OW UK peer1 application is SF UK 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OWDEPeer1BizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OWDEPeer1BizDivId").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Porperty returns application root path
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WSRoot()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WSRoot").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returnsweb application path
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WebPath()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WebPath").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns  enableSSL value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property enableSSL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("enableSSL").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns  SecureSandbox value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SecureSandbox()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SecureSandbox").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns  WOGroupDraft value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOGroupDraft()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOGroupDraft").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns  WOGroupSubmitted value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOGroupSubmitted()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOGroupSubmitted").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns  WOGroupCA value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOGroupCA()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOGroupCA").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns  WOGroupAccepted value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOGroupAccepted()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOGroupAccepted").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns  WOGroupCompleted value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOGroupCompleted()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOGroupCompleted").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns  WOGroupIssue value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOGroupIssue()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOGroupIssue").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns  WOGroupSent value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOGroupSent()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOGroupSent").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns  WOGroupClosed value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOGroupClosed()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOGroupClosed").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns  WOGroupInTray value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOGroupInTray()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOGroupInTray").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns  WOGroupLost value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOGroupLost()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOGroupLost").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns  WOGroupCancelled value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOGroupCancelled()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOGroupCancelled").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns  ContractorSelfEmployed value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ContractorSelfEmployed()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ContractorSelfEmployed").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ViewerAdmin()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ViewerAdmin").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ViewerBuyer()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ViewerBuyer").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ViewerSupplier()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ViewerSupplier").ToString()
        End Get
    End Property




    ''' <summary>
    ''' property returns  RoleSupplierSpecialistID value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleSupplierSpecialistID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleSupplierSpecialistID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns  AttachmentUploadPath value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property AttachmentUploadPath(ByVal bizDivID As Integer)
        Get
            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.site Then
                Return System.Configuration.ConfigurationManager.AppSettings("AttachmentUploadPath").ToString()
            Else
                Return System.Configuration.ConfigurationManager.AppSettings("AttachmentUploadPath" & bizDivID).ToString()
            End If
        End Get
    End Property


    ''' <summary>
    ''' property returns  AttachmentDisplayPath value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property AttachmentDisplayPath(ByVal bizDivID As Integer)
        Get

            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.site Then
                Return System.Configuration.ConfigurationManager.AppSettings("AttachmentDisplayPath").ToString()
            Else
                Return System.Configuration.ConfigurationManager.AppSettings("AttachmentDisplayPath" & bizDivID).ToString()
            End If
        End Get
    End Property

    ''' <summary>
    ''' Property to return attachment path for admin
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property AdminAttachmentPath()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("AdminAttachmentPath").ToString()
        End Get
    End Property


    ''' <summary>
    ''' Property to return attachment path for OrderWork Site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SiteAttachmentPath()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SiteAttachmentPath").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Property to return attachment path for OrderWork Site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WebContentPath()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WebContentPath").ToString()
        End Get
    End Property







    ''' <summary>
    ''' To show/hide Partner control on company profile page
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ControlComProfilePartnerShow()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CntComProfilePartner").ToString()
        End Get
    End Property


    ''' <summary>
    ''' To show/hide Certification/Qualificaytion control on company profile page
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ControlComProfileCertiQual()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CntComProfileCertQual").ToString()
        End Get
    End Property


    ''' <summary>
    ''' To show/hide Category control on company profile page
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ControlComProfileCategory()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CntComProfileCategory").ToString()
        End Get
    End Property


    ''' <summary>
    ''' To show/hide Bank Details control on company profile page
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ControlComProfileBankDetails()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CntComProfileBankDetails").ToString()
        End Get
    End Property




    ''' <summary>
    ''' To show/hide "Attacments" control on company profile page
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ControlComProfileAttachments()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CntComProfileAttachments").ToString()
        End Get
    End Property



    ''' <summary>
    '''Status value of approved Company
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property StatusApprovedCompany()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("StatusApprovedCompany").ToString()
        End Get
    End Property
    ''' <summary>
    '''Status value new Company
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property StatusNewCompany()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("StatusNewCompany").ToString()
        End Get
    End Property
    ''' <summary>
    '''Status value of suspended Company
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property StatusSuspendedCompany()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("StatusSuspendedCompany").ToString()
        End Get
    End Property
    ''' <summary>
    '''Status value of Deleted Company
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property StatusDeletedCompany()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("StatusDeletedCompany").ToString()
        End Get
    End Property
    ''' <summary>
    '''Status value of Active account
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property StatusActive()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("StatusActive").ToString()
        End Get
    End Property

    ''' <summary>
    '''Status value of InActive account
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property StatusInActive()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("StatusInActive").ToString()
        End Get
    End Property

    ''' <summary>
    ''' this Property give URL for OrderWork
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OrderWorkURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OrderWorkURL").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Mailers image path
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MailerImageURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MailerImageURL").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Mailers image path
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MailerImageURLSF()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MailerImageURLSF").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Mailers image path
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MailerImageURLUK()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MailerImageURLUK").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Property to return URL for order work uk site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OrderWorkUKURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OrderWorkUKURL").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Property to return URL for order work login site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OrderWorkMyUKURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OrderWorkMyUKURL").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property OrderWorkMyURLWithOutSSL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OrderWorkMyURLWithOutSSL").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Property to return URL for skills finder uk site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SkillsFinderUKURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SkillsFinderUKURL").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Property to return URL for order work de site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OrderWorkDEURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OrderWorkDEURL").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Property to return URL for skills finder de site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SkillsFinderDEURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SkillsFinderDEURL").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Property to return BizDivId of peer site {for OW it will return bizdiv of SF and vice versa}
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property PeerBizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("PeerBizDivId").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Property to return site abbrevation for current site "OW" for OrderWrok "MS" for skills finder
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SiteAbbrv()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SiteAbbrv").ToString()
        End Get
    End Property
    ''' <summary>
    ''' '' Property to return site abbrevation for peer site "OW" for OrderWrok "MS" for skills finder
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property PeerSiteAbbrv()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("PeerSiteAbbrv").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns string array of valid extension file allowed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ValidFilesExtensions() As String()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("NewValidFileType").ToString().Split(",")
        End Get
    End Property

    ''' <summary>
    ''' Returns string array of valid extension file allowed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ValidFilesExtensionsForComplete()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ValidFileTypeWOComplete").ToString().Split(",")
        End Get
    End Property

    ''' <summary>
    ''' Returns currency value e.g euro;  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Currency() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("Currency").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns the WO fees
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WOFees() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOFees").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns the VAT Percentage
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property VATPercentage() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATPercentage").ToString()
        End Get
    End Property


    Public Shared ReadOnly Property VATPercentageOld() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATPercentageOld").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property VATChangeDate() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATChangeDate").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property LastVATChangeDate() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("LastVATChangeDate").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property VATTolerance() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATTolerance").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property VATPercentageOlder() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATPercentageOlder").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property VATPercentageOldest() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATPercentageOldest").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property SecondLastVATChangeDate() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SecondLastVATChangeDate").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Returns VAT Rate depending on the requested date
    ''' </summary>
    ''' <param name="InvoiceDate"></param>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property VATPercentage(ByVal InvoiceDate As Date) As String
        Get
            Dim vatChangeDate As Date
            Dim oldvatChangeDate As Date
            Dim oldervatChangeDate As Date
            Try
                vatChangeDate = Strings.FormatDateTime(ApplicationSettings.VATChangeDate, DateFormat.ShortDate)
                oldvatChangeDate = Strings.FormatDateTime(ApplicationSettings.LastVATChangeDate, DateFormat.ShortDate)
                oldervatChangeDate = Strings.FormatDateTime(ApplicationSettings.SecondLastVATChangeDate, DateFormat.ShortDate)
                If (InvoiceDate < vatChangeDate And InvoiceDate >= oldvatChangeDate) Then
                    Return ApplicationSettings.VATPercentageOld
                ElseIf (InvoiceDate < oldvatChangeDate And InvoiceDate >= oldervatChangeDate) Then
                    Return ApplicationSettings.VATPercentageOlder
                ElseIf (InvoiceDate >= vatChangeDate) Then
                    Return ApplicationSettings.VATPercentage
                Else
                    Return ApplicationSettings.VATPercentageOldest
                End If
            Catch ex As Exception
                Return ApplicationSettings.VATPercentage
            End Try
        End Get
    End Property


    ''' <summary>
    ''' Returns the Calculated VAT Percentage For Sales Invoice
    ''' This returns the Calculated VAT Percentage which is being used to calculate the total amount
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property VATPercentage(ByVal VATAmnt As Decimal, ByVal WOAmnt As Decimal) As String
        Get
            Dim VATPercent As Decimal
            ' Fixed by Hemisha to avoid divid by zero error in case of AT800
            VATPercent = IIf(WOAmnt = 0, (VATAmnt * 100) / WOAmnt, (VATAmnt * 100))

            Dim tolerance As Decimal = ApplicationSettings.VATTolerance
            If (VATPercent > (ApplicationSettings.VATPercentageOld - tolerance) And VATPercent < (ApplicationSettings.VATPercentageOld + tolerance)) Then
                Return ApplicationSettings.VATPercentageOld
            ElseIf (VATPercent > (ApplicationSettings.VATPercentage - tolerance) And VATPercent < (ApplicationSettings.VATPercentage + tolerance)) Then
                Return ApplicationSettings.VATPercentage
            ElseIf (VATPercent > (ApplicationSettings.VATPercentageOlder - tolerance) And VATPercent < (ApplicationSettings.VATPercentageOlder + tolerance)) Then
                Return ApplicationSettings.VATPercentageOlder
            Else
                Return ApplicationSettings.VATPercentage
            End If
        End Get
    End Property
    ''' <summary>
    ''' Returns the OW Commission Percentage
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OWCommissionPerc() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OWCommissionPerc").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns the Wo Placement Fee
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property VoucherType_WOPlacementFee() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VoucherType_WOPlacementFee").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns the Wo Placement Value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property VoucherType_WOPlacementValue() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VoucherType_WOPlacementValue").ToString()
        End Get
    End Property

    ''' <summary>
    ''' credit card handling percentage used for protx
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CreditCardHandlingPerc() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CreditCardHandlingPerc").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns the OW Commission Percentage
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property LoginLink() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("LoginLink").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns the OW Commission Percentage
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MailerLogoURL() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MailerLogoURL").ToString()
        End Get
    End Property


    Public Shared ReadOnly Property MailerLogoURLUK() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MailerLogoURLUK").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property MailerLogoURLSF() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MailerLogoURLSF").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property MailerLogoURLDE() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MailerLogoURLDE").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns the OW Commission Percentage
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MailerLogoImageURL() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MailerLogoImageURL").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns the OW Commission Percentage
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MailerLogoImageALT() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MailerLogoImageALT").ToString()
        End Get
    End Property


    ''' <summary>
    ''' Mailer folder name
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MailerFolder() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MailerFolder").ToString()
        End Get
    End Property


    ''' <summary>
    ''' CompuTop Password
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CompuTopPassword() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CompuTopPassword").ToString()
        End Get
    End Property

    ''' <summary>
    ''' CompuTop MerchantID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CompuTopMerchantID() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CompuTopMerchantID").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Returns the 1st mail id to which error mail should get sent
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Error_Email_Contact1() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("Error_Email_Contact1").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Returns the 2nd mail id to which error mail should get sent
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Error_Email_Contact2() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("Error_Email_Contact2").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Returns the 2nd mail id to which error mail should get sent
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ShowDetailedError() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ShowDetailedError").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns the URL for OWAdmin UK
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OrderWorkUKAdminURL() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OrderWorkUKAdminURL").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Returns the URL for SFAdmin UK
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SkillsFinderUKAdminURL() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SkillsFinderUKAdminURL").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns "EmailInfo" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property EmailMarketing()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("EmailMarketing").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns "CreditTerms" default value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CreditTerms()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CreditTerms").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns "PaymentTerms" default value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property PaymentTerms()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("PaymentTerms").ToString()
        End Get
    End Property

    ''' <summary>
    ''' This property returns the mail id for the Medimax functionlity
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MedimaxMailID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MedimaxMailID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' This property returns the ReportServerPath
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ReportServerPath()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ReportServerPath").ToString()
        End Get
    End Property

    ''' <summary>
    ''' This property returns the ReportServerUserName
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ReportServerUserName()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ReportServerUserName").ToString()
        End Get
    End Property

    ''' <summary>
    ''' This property returns the ReportServerPassword
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ReportServerPassword()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ReportServerPassword").ToString()
        End Get
    End Property

    ''' <summary>
    ''' This property returns the mail id for the Medimax functionlity
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ReportServerDomain()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ReportServerDomain").ToString()
        End Get
    End Property

    ''' <summary>
    ''' This property returns yes or no depending on the site (local or live)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property EnablePostCodeService()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("EnablePostCodeService").ToString()
        End Get
    End Property


    ''' <summary>
    ''' This property returns yes or no depending on the site (local or live)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ReportFolder()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ReportFolder").ToString()
        End Get
    End Property

    ''' <summary>
    ''' This property returns 1 /0 for web services functionality either they are enabled or disabled
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property EnableWebServices()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("EnableWebServices").ToString()
        End Get

    End Property


    ''' <summary>
    ''' property returns  InvoiceStatus value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property InvoiceStatusAvailable()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WOInvoiceAvailable").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property ContactTypeBuyerName()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ContactTypeBuyer_Name").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property ContactTypeSupplierName()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ContactTypeSupplier_Name").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property GetChildMenuIDWOUploadParams()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ChildMenuID_WOUploadParams").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property GetChildMenuIDWebServices()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ChildMenuID_WebServices").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property GetChildMenuClientSubJobs()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ChildMenuID_ClientSubJobs").ToString()
        End Get
    End Property


    Public Enum ActionLogID
        Register = 54
        Approve = 55
        Unapprove = 56
        Suspend = 57
        Activate = 58
        Inactivate = 59
        Migrate = 60
        Upgrade = 61
        Verify = 62
        FundsAdded = 63
        FundsTransferred = 64
        WorkOrderEdited = 65
        FundsWithdrawn = 71
        FundsRefunded = 72
        SubmittedByAdmin = 73
        WorkOrderRefunded = 74
        ValidateSpecialist = 75
        WorkOrderSubmitted = 76
        Delete = 83
        VATYes = 90
        VATNo = 91
    End Enum

    Public Shared ReadOnly Property GetRefreshInterval()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RefreshInterval").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Public Enumerator for all error types
    ''' </summary>
    ''' <remarks>PratikT - 5 Aug, 2008</remarks>
    Public Enum ErrorCode
        None
        AccessDenied
    End Enum

    ''' <summary>
    ''' Property to send or block the mails to admin
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SendMailAdmin()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SendMailAdmin").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Property to send or block the mails to Supplier
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SendMailSupplier()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SendMailSupplier").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Property to send or block the mails to Buyer
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SendMailBuyer()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SendMailBuyer").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Property to return Log Out URL for order work Client Portal
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property PortalLogOutPath()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("PortalLogOutPath").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Property to get the path of OWSite from web config
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property BlogsPath()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BlogsPath").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Supplier Depot Id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleSupplierDepotID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleSupplierDepotID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Client Depot Id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property RoleClientDepotID()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RoleClientDepotID").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Business Area Retail
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property BusinessAreaRetail()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BusinessAreaRetail").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns Business Area IT
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property BusinessAreaIT()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BusinessAreaIT").ToString()
        End Get
    End Property
    ''' <summary>
    ''' To specify the maximum file upload size
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MaxFileUploadSize()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MaxFileUploadSize").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns the value, if rating should be shown or not
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property AccountKey()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("AccountKey")
        End Get
    End Property

    ''' <summary>
    ''' property returns the value, if rating should be shown or not
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property LicenceKey()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("LicenceKey")
        End Get
    End Property

    ''' <summary>
    ''' property returns the value, of evening installation
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property EveningInstallation()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("EveningInstallation")
        End Get
    End Property


    ''' <summary>
    ''' property returns the value, of evening installation slot timings
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property EveningInstallationSlot()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("EveningInstallationSlot")
        End Get
    End Property


    ''' <summary>
    ''' property returns the value, of morning installation slot timings
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MorningInstallationSlot()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MorningInstallationSlot")
        End Get
    End Property
    Public Shared ReadOnly Property AllDaySlot()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("AllDaySlot")
        End Get
    End Property
    ''' <summary>
    ''' property returns the value, of ValidatePostcodeUK
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ValidatePostcodeUK()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ValidatePostcodeUK")
        End Get
    End Property

    ''' <summary>
    ''' property returns the expression for validating UKPost Code
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ValidateExpressionPC()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ValidateExpressionPC")
        End Get
    End Property

    ''' <summary>
    ''' property returns the URL For the OW Blogs site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OrderWorkBlogURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OrderWorkBlogURL")
        End Get
    End Property
    ''' <summary>
    ''' property returns the URL For the OW Blogs site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SendMailToAll()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SendMailToAll")
        End Get
    End Property


    ''' <summary>
    ''' property returns the path for Company Logo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property AttachmentDisplayPathLogo()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("AttachmentDisplayPathLogo")
        End Get
    End Property
    Public Enum ContactClassID
        Client = 1
        Supplier = 2
        OW = 3
        Bank = 132
        WSUser = 450
        UpSell = 451
    End Enum
    ''' <summary>
    ''' To specify the maximum file upload size for signoff sheets
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property MaxFileUploadSizeSignOffSheet()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MaxFileUploadSizeSignOffSheet").ToString()
        End Get
    End Property
    Public Enum CommentType

        Comment = 92
        Note = 93
        Messages = 115
        Feedback = 116

    End Enum
    ''' <summary>
    ''' Returns the email id as resourcing@empowereduk.com
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ResourcingTeamEmail()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ResourcingTeamEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ServicePartnerBlogURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ServicePartnerBlogURL").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns the value, if SendIphoneNotification should be shown or not
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SendIphoneNotification()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SendIphoneNotification")
        End Get
    End Property
    ''' <summary>
    ''' WebConfig for IPhone WS root.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WSiPhoneRoot() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WSiPhoneRoot").ToString
        End Get
    End Property
    Public Shared ReadOnly Property SiteURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SiteURL").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property SelectBlog()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SelectBlog").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ITSiteURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ITSiteURL").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property RetailSiteURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RetailSiteURL").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property WebSiteURL()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WebSiteURL").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property MDBlogs()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MDBlogs").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property SPBlogs()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SPBlogs").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property ShowSPNotification()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ShowSPNotification").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property NotificationTime()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("NotificationTime").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property NewSpecificRetailForm() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("NewSpecificRetailForm").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property NewSpecificRetailForm2() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("NewSpecificRetailForm2").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property NewSpecificRetailForm3() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("NewSpecificRetailForm3").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property DixonRetailForm() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("DixonRetailForm").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property Lightwave() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("Lightwave").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property SmartTech() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SmartTech").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property RetailTestAccountsForm() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("RetailTestAccountsForm").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property BaseSiteURL() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BaseSiteURL").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property SONYWebServiceURL() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SonyWebService").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property SONYStatusUpdateURL() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("StatusUpdateURL").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property SONYUserName() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SonyUsername").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property SONYPassword() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SonyPassword").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property lblIOTIssueMsg() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("lblIOTIssueMsg").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property JayexTestAccount() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("JayexTestAccount").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property SamsungElectronicsCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SamsungElectronicsCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property SamsungPRSCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SamsungPRSCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property SamsungElectronicsBcc() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SamsungElectronicsBcc").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property SamsungPRSBcc() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SamsungPRSBcc").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property Telecare24Company() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("Telecare24Company").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property StoneGroupCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("StoneGroupCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property VirginCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VirginCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property EmailSoftwareAdmin() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("EmailSoftwareAdmin").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property BookingConfirmationBcc() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BookingConfirmationBcc").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property BookingConfirmationTelecare24Bcc() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BookingConfirmationTelecare24Bcc").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property DTPGroupCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("DTPGroupCompany").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property Lifeline24Company() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("Lifeline24Company").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property PCSpecialist() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("PCSpecialist").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property BookingConfirmationLifeline24Bcc() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BookingConfirmationLifeline24Bcc").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property BTBDCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BTBDCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ITECCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ITECCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ITECHSOCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ITECHSOCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property BTBDBccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BTBDBccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property DemoBTBDCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("DemoBTBDCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property DemoBTBDBccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("DemoBTBDBccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property MiscoCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MiscoCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property MiscoBccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("MiscoBccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property TotalComputerNetworksCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("TotalComputerNetworksCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property TotalComputerNetworksBccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("TotalComputerNetworksBccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property BookingConfirmationGenericBcc() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BookingConfirmationGenericBcc").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property OutsourceCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OutsourceCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property AmazonCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("AmazonCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property OutsourceBccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OutsourceBccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ITECBccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ITECBccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ITECHSOBccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ITECHSOBccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property Hemmersbach() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("Hemmersbach").ToString()
        End Get
    End Property
    ' OA 154 - Sabita added 
    Public Shared ReadOnly Property HBBccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("HBBccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ExponentialECompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ExponentialECompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property SmartTechBccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SmartTechBccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ebuyerCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ebuyerCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property ebuyerBccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ebuyerBccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property DropDownListDemoCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("DropDownListDemoCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property OWCommonUserPassword() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OWCommonUserPassword").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property OutsourceCCEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OutsourceCCEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property recruitmentEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("recruitmentEmail").ToString()
        End Get
    End Property
#Region "OA-548"
    'OA-548 : OA - Documentation - Account and User lifecycle notifications
    Public Shared ReadOnly Property AddEmployeeSupplierToEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("AddEmployeeSupplierToEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property AddEmployeeClientToEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("AddEmployeeClientToEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property AddEmployeeClientCCEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("AddEmployeeClientCCEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property IsLive() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("IsLive").ToString()
        End Get
    End Property
#End Region
    Public Shared ReadOnly Property OutsourceWOCompleteNotificationTO() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OutsourceWOCompleteNotificationTO").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property OutsourceWOCompleteNotificationBCC() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OutsourceWOCompleteNotificationBCC").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property WigglyAmpsCompany() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WigglyAmpsCompany").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property WigglyAmpsBccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WigglyAmpsBccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property TwoFaApi() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("TwoFaApi").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property GetAddressAPI() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("GetAddressAPI ").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property GetAddressAPIAccessKey() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("GetAddressAPIAccessKey ").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property GetAddressAPIAdminKey() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("GetAddressAPIAdminKey ").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property GetAddressUsageCount() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("GetAddressUsageCount ").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property BccEmail() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BccEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property GetSMSKey() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("GetSMSKey").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property SupplierOnBoardingURL() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SupplierOnBoardingURL").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property EmailPortalAdmin() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("EmailPortalAdmin").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property FinanceEmail()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("FinanceEmail").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property NewPortalURL() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("NewPortalURL").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property AzureStorageConnectionString() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("AzureStorageConnectionString").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property AzureStoragePath() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("AzureStoragePath").ToString()
        End Get
    End Property
End Class
