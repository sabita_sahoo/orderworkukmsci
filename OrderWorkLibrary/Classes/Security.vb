'------------------------------------------------------------------------------
''' <Summary>
'''   <ProjectName>Orderwork</ProjectName>
'''   <FileName>Security.vb</FileName>
'''   <Module>Security Mail</Module>
'''   <Description> Contains Function related to security as get security question of user ..etc </Description>
'''   <CreatedDate>1/03/2007</CreatedDate>
'''  <References>
'''        
'''   </References>
''' </Summary>
'------------------------------------------------------------------------------

Imports System.Web

'Used for Encryption and Decryption algorithms used for URL Encryption
'Added by Pratik Trivedi - 23 July, 2008
Imports System.Collections.Generic
Imports System.Text
Imports System.Security.Cryptography
Imports System.IO


''' <summary>
''' Security Class implements the security for the project. This is page level, control priveliges security.
''' </summary>
''' <remarks></remarks>
Public Class Security

    ''' <summary>
    ''' Function to process Use of SSL
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="enableSSL"></param>
    ''' <param name="SSLSecure"></param>
    ''' <param name="RedirectLink"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SecurePageSSL(ByVal page As Page, ByVal enableSSL As Integer, Optional ByVal SSLSecure As Boolean = True, Optional ByVal RedirectLink As String = "")
        'Check if enableSSL is enables via web.config file, if not then skip the process
        If enableSSL = 1 Then
            Dim redirectSSLURL As String
            Dim temp As String
            If RedirectLink = "" Then
                temp = LCase(CType(page.Request.Url, System.Uri).AbsoluteUri)
            Else
                temp = RedirectLink
            End If
            'Replace the url with secure HTTPS
            If SSLSecure = True Then
                If temp.IndexOf("https://") < 0 Then
                    redirectSSLURL = temp.Replace("http://", "https://")
                    page.Response.Redirect(redirectSSLURL)
                End If
            Else
                If temp.IndexOf("http://") < 0 Then
                    redirectSSLURL = temp.Replace("https://", "http://")
                    page.Response.Redirect(redirectSSLURL)
                End If
            End If

        End If
    End Function

    ''' <summary>
    ''' Function to secure access to each pages of the system
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="SSLSecure"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Sub SecurePage(ByVal page As Page, Optional ByVal SSLSecure As Boolean = True)
        Dim RedirectLink As String = ""

        If IsNothing(CType(page.Cache("LoggedInfo-" & page.Session("UserName") & page.Session.SessionID), DataSet)) Then
            'If Cache has expired then throw user out of the system
            page.Session("SessionExpired") = True
            Dim Url As String = page.Request.RawUrl
            Dim query As String = Url.Substring(Url.LastIndexOf("/") + 1)
            Select Case ApplicationSettings.SiteType
                
                Case ApplicationSettings.siteTypes.admin

                    RedirectLink = ApplicationSettings.WebPath & "Login.aspx?preURL=" & query
                Case ApplicationSettings.siteTypes.site
                    If Url.ToString.Contains("/BookingForms/") Then
                        RedirectLink = OrderWorkLibrary.ApplicationSettings.WebPath & "securelogin.aspx?preURL=BookingForms" & query 
                    Else
                        RedirectLink = ApplicationSettings.WebPath & "securelogin.aspx?preURL=" & query
                    End If
            End Select
            page.Response.Redirect(RedirectLink)
        End If

        Dim NoAccess As Boolean = True

        'Get Pagename from requesting url
        Dim filepath As String = page.Request.Url.PathAndQuery.ToLower
        Dim filepathNoQry As String = page.Request.FilePath.ToLower
        Dim pagewithqry, pagenoqry As String
        pagewithqry = filepath.Substring(filepathNoQry.LastIndexOf("/") + 1)
        pagenoqry = filepathNoQry.Substring(filepathNoQry.LastIndexOf("/") + 1)

        Dim ds As DataSet = CType(page.Cache("LoggedInfo-" & page.Session("UserName") & page.Session.SessionID), DataSet)

        'Creating a copy of table "Menu"
        Dim dv As DataView = ds.Tables(1).Copy.DefaultView
        dv.RowFilter = "MenuLink = '" & pagewithqry & "' or MenuLink = '" & pagenoqry & "'"
        'Creating a copy of table "ChildMenu"
        Dim dvSub As DataView = ds.Tables(2).Copy.DefaultView
        dvSub.RowFilter = "MenuLink = '" & pagewithqry & "' or MenuLink = '" & pagenoqry & "'"

        If dv.Count <> 0 Or dvSub.Count <> 0 Or pagewithqry = "errorpage.aspx" Or pagewithqry = "dashboard.aspx" Or pagewithqry = "profileredesign.aspx" Then
            NoAccess = False
        End If

        'OA-473 : OA - Deactivate the At800 table fetching and Update Code and DB. 
        If pagenoqry = "newservicequery.aspx" Then
            NoAccess = False
        End If

        If NoAccess = True Then
            page.Session("NoAccess") = True
            If Not IsDBNull(ds.Tables(0).Rows(0).Item("ClassID")) Then
                If ds.Tables(0).Rows(0).Item("ClassID") = ApplicationSettings.RoleSupplierID Then
                    If page.Session("PortalCompanyId") = OrderWorkLibrary.ApplicationSettings.NewSpecificRetailForm Or page.Session("PortalCompanyId") = OrderWorkLibrary.ApplicationSettings.NewSpecificRetailForm2 Then
                        If page.Session("RoleGroupID") = OrderWorkLibrary.ApplicationSettings.RoleClientRetailID Or page.Session("RoleGroupID") = OrderWorkLibrary.ApplicationSettings.RoleSupplierRetailID Then
                            RedirectLink = ApplicationSettings.OrderWorkMyUKURL & "SecurePages/BookingForms/RetailDixonWOForm.aspx"
                        Else
                            RedirectLink = ApplicationSettings.WebPath & "SecurePages/Welcome.aspx"
                        End If
                    Else
                        RedirectLink = ApplicationSettings.WebPath & "SecurePages/Welcome.aspx"
                    End If
                End If
                If ds.Tables(0).Rows(0).Item("ClassID") = ApplicationSettings.RoleClientID Then
                    If page.Session("PortalCompanyId") = OrderWorkLibrary.ApplicationSettings.NewSpecificRetailForm Or page.Session("PortalCompanyId") = OrderWorkLibrary.ApplicationSettings.NewSpecificRetailForm2 Then
                        If page.Session("RoleGroupID") = OrderWorkLibrary.ApplicationSettings.RoleClientRetailID Or page.Session("RoleGroupID") = OrderWorkLibrary.ApplicationSettings.RoleSupplierRetailID Then
                            RedirectLink = ApplicationSettings.OrderWorkMyUKURL & "SecurePages/BookingForms/RetailDixonWOForm.aspx"
                        Else
                            RedirectLink = ApplicationSettings.WebPath & "SecurePages/Welcome.aspx"
                        End If
                    Else
                        RedirectLink = ApplicationSettings.WebPath & "SecurePages/Welcome.aspx"
                    End If
                End If
            Else
                page.Response.Redirect(ApplicationSettings.WebPath & "securelogin.aspx")
            End If

        End If

        If ApplicationSettings.enableSSL = 1 Then
            If RedirectLink = "" Then
                RedirectLink = page.Request.Url.AbsoluteUri.ToLower
            End If
            SecurePageSSL(page, ApplicationSettings.enableSSL, SSLSecure, RedirectLink)
        Else
            If RedirectLink <> "" Then
                page.Response.Redirect(RedirectLink)
            End If
        End If

        Dim dvpriv As DataView = ds.Tables(3).DefaultView
        dvpriv.RowFilter = "PageName Like('" & pagenoqry & "') "
        If dvpriv.Count <> 0 Then
            GetPageControls(page, dvpriv)
        End If
    End Sub

    ''' <summary>
    ''' To provide security to the page controls
    ''' </summary>
    ''' <param name="page"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Sub GetPageControls(ByVal page As Page, ByVal dvpriv As DataView)
        'Changed to cater for master page
        Dim content As ContentPlaceHolder
        content = CType(page.Master.FindControl("ContentHolder"), ContentPlaceHolder)
        Dim drv As DataRowView
        For Each drv In dvpriv
            Try
                If drv.Item("ControlName").IndexOf(".") = -1 Then
                    If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                        content.FindControl(drv.Item("ControlName")).Visible = CType(drv.Item("ControlPropertyValue"), Boolean)
                    End If
                    If CStr(drv.Item("ControlPropertyLabel")).ToLower = "enabled" Then
                        CType(content.FindControl(drv.Item("ControlName")), WebControl).Enabled = CType(drv.Item("ControlPropertyValue"), String)
                    End If
                Else
                    Dim obj() As String = Split(drv.Item("ControlName"), ".")

                    If content.FindControl((obj(0).ToString)).GetType.ToString.ToLower = "system.web.ui.webcontrols.gridview" Then
                        If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                            CType(content.FindControl(obj(0).ToString), GridView).Columns(CInt(obj(1))).Visible = CType(drv.Item("ControlPropertyValue"), String)
                        End If
                    ElseIf content.FindControl((obj(0).ToString)).TemplateSourceDirectory.ToLower.IndexOf("usercontrols") <> -1 Then
                        If obj.Length = 2 Then
                            If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                                CType(content.FindControl(obj(0)), UserControl).FindControl(obj(1)).Visible = CType(drv.Item("ControlPropertyValue"), String)
                            End If
                            If CStr(drv.Item("ControlPropertyLabel")).ToLower = "enabled" Then
                                CType(CType(content.FindControl(obj(0)), UserControl).FindControl(obj(1)), WebControl).Enabled = CType(drv.Item("ControlPropertyValue"), String)
                            End If
                        Else
                            If CType(content.FindControl(obj(0)), UserControl).FindControl((obj(1).ToString)).GetType.ToString.ToLower = "system.web.ui.webcontrols.gridview" Then
                                If IsNumeric(obj(2)) Then
                                    If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                                        CType(CType(content.FindControl(obj(0)), UserControl).FindControl(obj(1)), GridView).Columns(obj(2)).Visible = CType(drv.Item("ControlPropertyValue"), String)
                                    End If
                                Else
                                    If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                                        CType(CType(content.FindControl(obj(0)), UserControl).FindControl(obj(1)), GridView).FindControl(obj(2)).Visible = CType(drv.Item("ControlPropertyValue"), String)
                                    End If
                                End If
                            ElseIf CType(content.FindControl(obj(0)), UserControl).FindControl((obj(1).ToString)).TemplateSourceDirectory.ToLower.IndexOf("usercontrols") <> -1 Then
                                If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                                    CType(CType(content.FindControl(obj(0)), UserControl).FindControl(obj(1)), UserControl).FindControl(obj(2)).Visible = CType(drv.Item("ControlPropertyValue"), String)
                                End If
                                If CStr(drv.Item("ControlPropertyLabel")).ToLower = "enabled" Then
                                    CType(CType(CType(content.FindControl(obj(0)), UserControl).FindControl(obj(1)), UserControl).FindControl(obj(2)), WebControl).Enabled = CType(drv.Item("ControlPropertyValue"), String)
                                End If
                            End If
                        End If
                    ElseIf content.FindControl((obj(0).ToString)).GetType.ToString.ToLower = "ajaxcontroltoolkit.tabcontainer" Then
                        If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                            CType(CType(content.FindControl(obj(0)), AjaxControlToolkit.TabContainer).FindControl(obj(1)), AjaxControlToolkit.TabPanel).Visible = CType(drv.Item("ControlPropertyValue"), String)
                            CType(CType(content.FindControl(obj(0)), AjaxControlToolkit.TabContainer).FindControl(obj(1)), AjaxControlToolkit.TabPanel).HeaderText = ""
                        End If
                        If CStr(drv.Item("ControlPropertyLabel")).ToLower = "enabled" Then
                            CType(CType(content.FindControl(obj(0)), AjaxControlToolkit.TabContainer).FindControl(obj(1)), AjaxControlToolkit.TabPanel).Enabled = CType(drv.Item("ControlPropertyValue"), String)
                        End If
                    End If
                End If
            Catch ex As Exception
            End Try
        Next
    End Sub

    ''' <summary>
    ''' Function to get security question for the user to retrieve forgot password
    ''' </summary>
    ''' <param name="Email"></param>
    ''' <param name="Country"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getSecurityQuestion(ByVal Email As String, ByVal Country As String) As String
        Dim str, question, answer As String

        If Trim(Email) = "" Then
            Select Case Country
                Case "UK"
                    str = "Please enter an email address."
                Case "DE"
                    str = "Tragen Sie bitte ein email address ein."
            End Select
        Else
            Dim ds As DataSet
            'Get dataset of the string
            ds = DBSecurity.GetSecurityQuesAns(Trim(Email))

            If ds.Tables(0).Rows.Count = 0 Then
                'If there is no record return then show message that user does not exist
                Select Case Country
                    Case "UK"
                        str = "We're sorry, this email address does not exist in our records. Please try again"
                    Case "DE"
                        str = "Es tut uns leid, aber dieses email address besteht nicht in unseren Aufzeichnungen. Bitte Versuch wieder"
                End Select
            Else
                'If record exist then return its value as string to the caller
                question = ds.Tables(0).Rows(0).Item("standardvalue")
                answer = ds.Tables(0).Rows(0).Item("SecurityAns")
                str = "success" + "*^*" + question + "*^*" + answer
            End If
        End If
        'Return security question
        Return str
    End Function

    ''' <summary>
    ''' Function to get Roles Groups Name as per Role Category
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="RoleCategory"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRolesGroups(ByVal page As Page, Optional ByVal RoleCategory As String = "") As DataSet
        Dim ds As DataSet

        If page.Cache("RolesGroups" & page.Session("UserName") & page.Session.SessionID) Is Nothing Then
            ds = DBSecurity.GetRolesGroups(RoleCategory)
            page.Cache.Add("RolesGroups" & page.Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache("RolesGroups" & page.Session.SessionID), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Function to get status of the contact
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="RoleGroupID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStatuses(ByVal page As Page, Optional ByVal RoleGroupID As Integer = 0) As DataSet
        Dim ds As DataSet
        If page.Cache("Statuses for : " & RoleGroupID & page.Session("UserName") & page.Session.SessionID) Is Nothing Then
            ds = DBSecurity.GetStatuses(RoleGroupID)
            page.Cache.Add("Statuses for : " & RoleGroupID & page.Session("UserName") & page.Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache("Statuses for : " & RoleGroupID & page.Session("UserName") & page.Session.SessionID), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Function to encrypt URL using DES
    ''' </summary>
    ''' <param name="InputString"></param>
    ''' <returns>Encrypted String</returns>
    ''' <remarks>Uses declarations for Security Namespace - Pratik Trivedi - 23 July, 2008</remarks>
    Public Shared Function EncryptURL_DES(ByVal InputString As String) As String
        Try
            'Dim mCryptProv As SymmetricAlgorithm
            Dim mMemStr As MemoryStream

            'Encrypt the Data in  the textbox txtData, show that in MessageBox and write back to textbox   
            'Here You can provide the name of any class which supportsSymmetricAlgorithm Like DES(mCryptProv = SymmetricAlgorithm.Create("Rijndael"))
            Dim mCryptProv As New TripleDESCryptoServiceProvider

            'The encrypted data will be stored in the memory, so we need a memory stream object
            mMemStr = New MemoryStream
            Dim iv() As Byte
            Dim key() As Byte
            key = System.Text.Encoding.ASCII.GetBytes("E$@Efd45$#Wddsf#0WSdPSD4")
            iv = System.Text.Encoding.ASCII.GetBytes("5%&ERfd4")
            'Create the ICryptTransform Object. 
            Dim mTransform As ICryptoTransform = mCryptProv.CreateEncryptor(key, iv)

            'Create the Crypto Stream for writing, and pass that MemoryStream (where to write after encryption), and ICryptoTransform Object.
            Dim mCSWriter As New CryptoStream(mMemStr, mTransform, CryptoStreamMode.Write)
            'This StreamWriter object will be used to write 
            'Encrypted data to the Memory Stream 

            'Pass it the object of CryptoStream 
            Dim mSWriter As New StreamWriter(mCSWriter)
            mSWriter.Write(InputString)

            'Write data after encryption 
            mSWriter.Flush() 'Make sure to write everything from Stream 
            mCSWriter.FlushFinalBlock() 'Flush the CryptoStream as well 

            'The Data has been written in the Memory, But we need to display that back to text box and want to show in Messagebox. So do the following

            'Create byte array to receive data
            Dim mBytes(mMemStr.Length - 1) As Byte
            mMemStr.Position = 0        'Move to beginning of data
            'Read All data from the memory
            mMemStr.Read(mBytes, 0, mMemStr.Length)

            'But this data is in Bytes, and we need to convert it to string.
            'String conversion address to the problem of Encoding format. We are using UTF8 Encoding to encode data from byte to string

            Dim mEncData As String = ConvertByteArrayToString(mBytes)
            Dim inputbytes As Byte() = ConvertStringToByteArray(mEncData)

            mMemStr.Position = 0
            mTransform = mCryptProv.CreateDecryptor(key, iv)
            Dim mCSReader As New CryptoStream(mMemStr, mTransform, CryptoStreamMode.Read)
            Dim mStrREader As New StreamReader(mCSReader)
            Dim mDecData As String = mStrREader.ReadToEnd()

            Return (HttpUtility.UrlEncode(mEncData).ToString)

        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    ''' <summary>
    ''' Function to Decrypt URL using DES
    ''' </summary>
    ''' <param name="InputString"></param>
    ''' <returns>Decrypted String</returns>
    ''' <remarks>Uses declarations for Security Namespace - Pratik Trivedi - 5th Aug, 2008</remarks>
    Public Shared Function DecryptURL_DESv2(ByVal InputString As String, ByVal Page As Page, ByVal URL As String, Optional ByVal URLRedirect As String = "") As String
        Try
            Dim OutputString As String
            Dim iv() As Byte
            Dim key() As Byte
            InputString = HttpUtility.HtmlDecode(InputString)
            key = System.Text.Encoding.ASCII.GetBytes("E$@Efd45$#Wddsf#0WSdPSD4")
            iv = System.Text.Encoding.ASCII.GetBytes("5%&ERfd4")
            Dim mCryptProv As New TripleDESCryptoServiceProvider

            Dim inputbytes As Byte() = ConvertStringToByteArray(InputString.Replace(Chr(32), Chr(43))) 'Replace Space with + sign 
            Dim mMemStr As MemoryStream = New MemoryStream(inputbytes)

            mMemStr.Position = 0

            Dim mTransform As ICryptoTransform = mCryptProv.CreateDecryptor(key, iv)
            Dim mCSReader As New CryptoStream(mMemStr, mTransform, CryptoStreamMode.Read)
            Dim mStrREader As New StreamReader(mCSReader)
            Dim mDecData As String = mStrREader.ReadToEnd()

            OutputString = mDecData
            Return OutputString
        Catch ex As Exception
            OrderWorkLibrary.CommonFunctions.ErrorAccessDenied(Page, ex.ToString, URL, URLRedirect)
            'Return ex.ToString
        End Try
    End Function

    ''' <summary>
    ''' Function to Decrypt URL using DES
    ''' </summary>
    ''' <param name="InputString"></param>
    ''' <returns>Decrypted String</returns>
    ''' <remarks>Uses declarations for Security Namespace - Pratik Trivedi - 23 July, 2008</remarks>
    Public Shared Function DecryptURL_DES(ByVal InputString As String, ByVal Page As Page) As String
        Try
            Dim OutputString As String
            Dim iv() As Byte
            Dim key() As Byte
            InputString = HttpUtility.HtmlDecode(InputString)
            key = System.Text.Encoding.ASCII.GetBytes("E$@Efd45$#Wddsf#0WSdPSD4")
            iv = System.Text.Encoding.ASCII.GetBytes("5%&ERfd4")
            Dim mCryptProv As New TripleDESCryptoServiceProvider

            Dim inputbytes As Byte() = ConvertStringToByteArray(InputString.Replace(Chr(32), Chr(43))) 'Replace Space with + sign 
            Dim mMemStr As MemoryStream = New MemoryStream(inputbytes)

            mMemStr.Position = 0

            Dim mTransform As ICryptoTransform = mCryptProv.CreateDecryptor(key, iv)
            Dim mCSReader As New CryptoStream(mMemStr, mTransform, CryptoStreamMode.Read)
            Dim mStrREader As New StreamReader(mCSReader)
            Dim mDecData As String = mStrREader.ReadToEnd()

            OutputString = mDecData
            Return OutputString
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
    ''' <summary>
    ''' Function to convert string to byte array
    ''' </summary>
    ''' <param name="stringToConvert"></param>
    ''' <returns></returns>
    ''' <remarks>Pratik Trivedi - 23 July, 2008</remarks>
    Public Shared Function ConvertStringToByteArray(ByVal stringToConvert As String) As Byte()
        Return Convert.FromBase64String(stringToConvert)
    End Function

    ''' <summary>
    ''' Function to convert byter array to string
    ''' </summary>
    ''' <param name="bt"></param>
    ''' <returns></returns>
    ''' <remarks>Pratik Trivedi - 23 July, 2008</remarks>
    Public Shared Function ConvertByteArrayToString(ByVal bt As Byte()) As String
        Return (Convert.ToBase64String(bt))
    End Function


    ''' <summary>
    ''' Function which accepts encrypted string in csv format and returns decrypted csv format
    ''' </summary>
    ''' <param name="encrytedCSV"></param>
    ''' <param name="Page"></param>
    ''' <param name="URL"></param>
    ''' <param name="URLRedirect"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DecryptCSVdata(ByVal encrytedCSV As String, ByVal Page As Page, ByVal URL As String, Optional ByVal URLRedirect As String = "") As String
        Dim tempArray As Array
        Dim arrindx As Int32 = 0
        Dim decryptedCSV As String = ""

        tempArray = encrytedCSV.Split(",")
        For arrindx = 0 To tempArray.Length - 1
            If tempArray(arrindx) <> "" Then
                tempArray(arrindx) = OrderWorkLibrary.Security.DecryptURL_DESv2(HttpUtility.UrlDecode(tempArray(arrindx)).ToString.Trim, Page, URLRedirect, URLRedirect)
            End If
        Next

        For arrindx = 0 To tempArray.Length - 1
            If decryptedCSV = "" Then
                decryptedCSV = tempArray(arrindx).ToString
            Else
                decryptedCSV = decryptedCSV & "," & tempArray(arrindx).ToString
            End If
        Next
        decryptedCSV.Replace(",,", ",")
        Return decryptedCSV
    End Function



End Class
