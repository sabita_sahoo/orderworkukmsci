Imports System.Security.Cryptography
Imports System.IO

''' <summary>
''' Encryption Class
''' </summary>
''' <remarks></remarks>
Public Class Encryption

    ''' <summary>
    ''' Function to encrypt the string send to it
    ''' </summary>
    ''' <param name="InputString"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Encrypt(ByVal InputString As String) As String
        If IsDBNull(InputString) Then
            Return ""
        End If
        If InputString = String.Empty Then
            Return ""
        End If
        Try
            'Dim mCryptProv As SymmetricAlgorithm
            Dim mMemStr As MemoryStream

            'Encrypt the Data in  the textbox txtData, show that in MessageBox 
            'and write back to textbox   
            'Here You can provide the name of any class which supportsSymmetricAlgorithm Like DES(mCryptProv = SymmetricAlgorithm.Create("Rijndael"))
            Dim mCryptProv As New TripleDESCryptoServiceProvider

            'The encrypted data will be stored in the memory, so we need a memory stream 
            'object
            mMemStr = New MemoryStream
            Dim iv() As Byte
            Dim key() As Byte
            key = System.Text.Encoding.ASCII.GetBytes("E$@Efd45$#Wddsf#0WSdPSD4")
            iv = System.Text.Encoding.ASCII.GetBytes("5%&ERfd4")
            'Create the ICryptTransform Object. 
            Dim mTransform As ICryptoTransform = mCryptProv.CreateEncryptor(key, iv)

            'Create the Crypto Stream for writing, and pass that MemoryStream 
            '(where to write after encryption), and ICryptoTransform Object.
            Dim mCSWriter As New CryptoStream(mMemStr, mTransform, CryptoStreamMode.Write)
            'This StreamWriter object will be used to write 
            'Encrypted data to the Memory Stream 

            'Pass it the object of CryptoStream 
            Dim mSWriter As New StreamWriter(mCSWriter)
            mSWriter.Write(InputString)

            'Write data after encryption 
            mSWriter.Flush() 'Make sure to write everything from Stream 
            mCSWriter.FlushFinalBlock() 'Flush the CryptoStream as well 

            'The Data has been written in the Memory, But we need to display that 
            'back to text box and want to show in Messagebox. So do the following

            'Create byte array to receive data
            Dim mBytes(mMemStr.Length - 1) As Byte
            mMemStr.Position = 0        'Move to beginning of data
            'Read All data from the memory
            mMemStr.Read(mBytes, 0, mMemStr.Length)

            'But this data is in Bytes, and we need to convert it to string.
            'String conversion address to the problem of Encoding format. We are
            'using UTF8 Encoding to encode data from byte to string

            Dim mEncData As String = ConvertByteArrayToString(mBytes)
            Dim inputbytes As Byte() = ConvertStringToByteArray(mEncData)

            mMemStr.Position = 0
            mTransform = mCryptProv.CreateDecryptor(key, iv)
            Dim mCSReader As New CryptoStream(mMemStr, mTransform, CryptoStreamMode.Read)
            Dim mStrREader As New StreamReader(mCSReader)
            Dim mDecData As String = mStrREader.ReadToEnd()

            Return (mEncData)

        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    ''' <summary>
    ''' Function to decrypt the input string
    ''' </summary>
    ''' <param name="InputString"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Decrypt(ByVal InputString As Object) As String
        If IsDBNull(InputString) Then
            Return ""
        End If
        If InputString = String.Empty Then
            Return ""
        End If
        Try
            Dim OutputString As String
            Dim iv() As Byte
            Dim key() As Byte
            key = System.Text.Encoding.ASCII.GetBytes("E$@Efd45$#Wddsf#0WSdPSD4")
            iv = System.Text.Encoding.ASCII.GetBytes("5%&ERfd4")
            Dim mCryptProv As New TripleDESCryptoServiceProvider

            Dim inputbytes As Byte() = ConvertStringToByteArray(InputString)
            Dim mMemStr As MemoryStream = New MemoryStream(inputbytes)

            mMemStr.Position = 0

            Dim mTransform As ICryptoTransform = mCryptProv.CreateDecryptor(key, iv)
            Dim mCSReader As New CryptoStream(mMemStr, mTransform, CryptoStreamMode.Read)
            Dim mStrREader As New StreamReader(mCSReader)
            Dim mDecData As String = mStrREader.ReadToEnd()

            OutputString = mDecData
            Return OutputString
        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function

    ''' <summary>
    ''' Function to convert string to byte array
    ''' </summary>
    ''' <param name="stringToConvert"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ConvertStringToByteArray(ByVal stringToConvert As String) As Byte()
        'Return Convert.FromBase64String(stringToConvert.Replace("-", "+").Replace("_", "/"))
        Return Convert.FromBase64String(stringToConvert)

    End Function

    ''' <summary>
    ''' Function to convert byter array to string
    ''' </summary>
    ''' <param name="bt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ConvertByteArrayToString(ByVal bt As Byte()) As String
        'Return (Convert.ToBase64String(bt).Replace("+", "-").Replace("/", "_"))
        Return (Convert.ToBase64String(bt))
    End Function

    Public Shared Function EncryptToMD5Hash(ByVal text As String) As String
        Dim md5 As MD5 = New MD5CryptoServiceProvider()

        'compute hash from the bytes of text
        md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text))

        'get hash result after compute it
        Dim result As Byte() = md5.Hash

        Dim strBuilder As New StringBuilder()
        For i As Integer = 0 To result.Length - 1
            'change it into 2 hexadecimal digits
            'for each byte
            strBuilder.Append(result(i).ToString("x2"))
        Next

        Return strBuilder.ToString.ToUpper
    End Function

    'poonam modified on 18/2/2016 - Task - OA-187 : OA - Auto-match notification emails do not always give full info
    Public Shared Function EncryptURL_DES(ByVal InputString As String) As String
        Try
            'Dim mCryptProv As SymmetricAlgorithm
            Dim mMemStr As MemoryStream

            'Encrypt the Data in  the textbox txtData, show that in MessageBox and write back to textbox   
            'Here You can provide the name of any class which supportsSymmetricAlgorithm Like DES(mCryptProv = SymmetricAlgorithm.Create("Rijndael"))
            Dim mCryptProv As New TripleDESCryptoServiceProvider

            'The encrypted data will be stored in the memory, so we need a memory stream object
            mMemStr = New MemoryStream
            Dim iv() As Byte
            Dim key() As Byte
            key = System.Text.Encoding.ASCII.GetBytes("E$@Efd45$#Wddsf#0WSdPSD4")
            iv = System.Text.Encoding.ASCII.GetBytes("5%&ERfd4")
            'Create the ICryptTransform Object. 
            Dim mTransform As ICryptoTransform = mCryptProv.CreateEncryptor(key, iv)

            'Create the Crypto Stream for writing, and pass that MemoryStream (where to write after encryption), and ICryptoTransform Object.
            Dim mCSWriter As New CryptoStream(mMemStr, mTransform, CryptoStreamMode.Write)
            'This StreamWriter object will be used to write 
            'Encrypted data to the Memory Stream 

            'Pass it the object of CryptoStream 
            Dim mSWriter As New StreamWriter(mCSWriter)
            mSWriter.Write(InputString)

            'Write data after encryption 
            mSWriter.Flush() 'Make sure to write everything from Stream 
            mCSWriter.FlushFinalBlock() 'Flush the CryptoStream as well 

            'The Data has been written in the Memory, But we need to display that back to text box and want to show in Messagebox. So do the following

            'Create byte array to receive data
            Dim mBytes(mMemStr.Length - 1) As Byte
            mMemStr.Position = 0        'Move to beginning of data
            'Read All data from the memory
            mMemStr.Read(mBytes, 0, mMemStr.Length)

            'But this data is in Bytes, and we need to convert it to string.
            'String conversion address to the problem of Encoding format. We are using UTF8 Encoding to encode data from byte to string

            Dim mEncData As String = ConvertByteArrayToString(mBytes)
            Dim inputbytes As Byte() = ConvertStringToByteArray(mEncData)

            mMemStr.Position = 0
            mTransform = mCryptProv.CreateDecryptor(key, iv)
            Dim mCSReader As New CryptoStream(mMemStr, mTransform, CryptoStreamMode.Read)
            Dim mStrREader As New StreamReader(mCSReader)
            Dim mDecData As String = mStrREader.ReadToEnd()

            Return (HttpUtility.UrlEncode(mEncData).ToString)

        Catch ex As Exception
            Return ex.ToString
        End Try
    End Function
End Class
