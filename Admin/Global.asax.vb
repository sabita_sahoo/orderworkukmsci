Imports System.Web.SessionState

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ''''' just to create a culture

        System.Globalization.CultureInfo.DefaultThreadCurrentCulture = New System.Globalization.CultureInfo("en-GB")

        ' Fires when the application is started
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                ResourceMessageText.InitializeResources("MessageTextDE")
                EmailContents.InitializeResources("EmailContentsDE")
            Case ApplicationSettings.CountryUK
                ResourceMessageText.InitializeResources("MessageTextUK")
                EmailContents.InitializeResources("EmailContentsUK")
        End Select
        log4net.Config.XmlConfigurator.Configure()
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                ResourceMessageText.InitializeResources("MessageTextDE")
            Case ApplicationSettings.CountryUK
                ResourceMessageText.InitializeResources("MessageTextUK")
                EmailContents.InitializeResources()
        End Select
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
        Dim err As Exception = Server.GetLastError()
        'Context.Session.Add("err", err)
        Session.Add("err", err)
        Dim ErrorP As String
        'ErrorP = Request.ApplicationPath & "/errorpage.aspx"
        ErrorP = ApplicationSettings.WebPath & "errorpage.aspx"
        Response.Redirect(ErrorP)
        'Server.Transfer(ErrorP, False)

    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class