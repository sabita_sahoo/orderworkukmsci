

Partial Public Class PIUnpaidAvailable
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents ObjectDataSource1 As ObjectDataSource
    Protected WithEvents chkSelectAll, chkOver30Days As System.Web.UI.WebControls.CheckBox
    Protected WithEvents tdMakeAvailable As System.Web.UI.HtmlControls.HtmlTableCell

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            ViewState!SortExpression = "DateCreated"
            gvPurchaseInv.PageSize = ApplicationSettings.GridViewPageSize
            gvPurchaseInv.Sort(ViewState!SortExpression, SortDirection.Ascending)
            gvPurchaseInv.PageIndex = 0

            UCSearchContact1.BizDivID = Session("BizDivId")
            UCSearchContact1.ClassId = 0
            UCSearchContact1.Filter = "MakePIAvailable"
            UCSearchContact1.populateContact()
            If Not Request("sender") Is Nothing Then

                'If Not Request("CompanyId") Is Nothing Then
                '    If (Request("CompanyId") <> "") Then
                '        Dim li As ListItem = UCSearchContact1.ddlContact.Items.FindByValue(Request("CompanyId"))
                '        If Not IsNothing(li) Then
                '            UCSearchContact1.ddlContact.SelectedValue = Request("CompanyId")
                '            gvPurchaseInv.Visible = True
                '        End If
                '    End If
                'End If
                'If Not Request("Over30Days") Is Nothing Then
                '    If (Request("Over30Days") <> "") Then
                '        chkOver30Days.Checked = CBool(Request("Over30Days"))
                '    End If
                'End If
                Dim li As ListItem = UCSearchContact1.ddlContact.Items.FindByValue(Request("CompanyId"))
                If Not IsNothing(li) Then
                    UCSearchContact1.ddlContact.SelectedValue = Request("CompanyId")
                    gvPurchaseInv.Visible = True
                End If
                chkOver30Days.Checked = CBool(Request("Over30Days"))
            End If

            PopulateGrid()
            'tdMakeAvailable.Visible = True


        End If
        lblError.Text = ""
        divValidationMain.Visible = False
    End Sub

    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            divValidationMain.Visible = False
            If gvPurchaseInv.Visible = False Then
                gvPurchaseInv.Visible = True
            End If
            gvPurchaseInv.DataBind()
            ShowHideBtnAvailable()
        Else
            divValidationMain.Visible = True
        End If
    End Sub

    Public Sub CustomCompanyValidator_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomCompanyValidator.ServerValidate
        'Check if user has selected a contact from the dropdown 
        If UCSearchContact1.ddlContact.SelectedValue <> "" Then
            args.IsValid = True
        ElseIf chkShowAll.Checked = True Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

    Public Sub btnGetPurchaseInvoices_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetPurchaseInvoices.Click
        Page.Validate()
        If Page.IsValid Then
            divValidationMain.Visible = False
            lblError.Text = ""
            If Not IsNothing(ViewState("Supplier")) Then


                If ViewState("Supplier") <> UCSearchContact1.ddlContact.SelectedValue Then
                    ViewState("Supplier") = UCSearchContact1.ddlContact.SelectedValue
                    ViewState("NewSP") = 1
                Else
                    ViewState("Supplier") = UCSearchContact1.ddlContact.SelectedValue
                    ViewState("NewSP") = 0
                End If
            Else
                ViewState("Supplier") = UCSearchContact1.ddlContact.SelectedValue
            End If

            PopulateGrid()
            gvPurchaseInv.PageIndex = 0
            ShowHideBtnAvailable()
        Else
            divValidationMain.Visible = True
        End If
    End Sub

    Private Sub ShowHideBtnAvailable()
        If ViewState("UnPaid") <> 0 Then
            'If Session("RoleGroupID") <> ApplicationSettings.RoleOWRepID Then
            If ViewState("mode") = "withhold" Then

                tdMakeAvailable.Visible = True
            Else
                tdMakeAvailable.Visible = True
            End If


            'Else
            '    tdMakeAvailable.Visible = False
            'End If
        Else

        End If
    End Sub

    Public Function GetLinks(ByVal invoiceNo As String, ByVal WOID As String, ByVal CompanyId As String) As String
        Dim link As String = ""
        Dim linkParams As String = ""

        linkParams &= "invoiceNo=" & invoiceNo
        linkParams &= "&WOID=" & WOID
        linkParams &= "&CompanyId=" & CompanyId
        linkParams &= "&bizDivId=" & Session("BizDivId")
        linkParams &= "&sender=PIUnpaidAvailable"
        linkParams &= "&Group=Invoiced"
        If chkOver30Days.Checked = True Then
            linkParams &= "&Over30Days=True"
        Else
            linkParams &= "&Over30Days=False"
        End If


        link &= "<a target='_blank' href='ViewPurchaseInvoice.aspx?" & linkParams & "'><img src='Images/Icons/View-Purchase-Invoice.gif' alt='View Purchase Invoice' title='View Purchase Invoice' width='18' height='14' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        link &= "<a target='_parent' href='AdminWODetails.aspx?" & linkParams & "'><img src='Images/Icons/ViewDetails.gif' alt='view Work Order details' title='view Work Order details' width='16' height='16' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        If ViewState("mode") <> "withhold" Then
            link &= "<a target='_parent' href='InvoiceApproval.aspx?" & linkParams & "'><img src='Images/Icons/Edit.gif' alt='Edit Invoice' title='Edit Invoice' width='12' height='11' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        End If

        Return link
    End Function

    Public Sub MakeAvailable(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ISFinanceUser As Boolean = False
        If Not IsNothing(Session("RoleGroupID")) Then
            If (Session("RoleGroupID") = ApplicationSettings.RoleOWFinanceID Or Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID) Then
                ISFinanceUser = True
            End If
        End If

        If ISFinanceUser = True Then

            Page.Validate()
            If Page.IsValid Then
                divValidationMain.Visible = False
                lblError.Text = ""

                'this is to check tht something is there in the viewstate i.e. the view button is clicked or not
                If ViewState("Supplier") <> UCSearchContact1.ddlContact.SelectedValue Then
                    divValidationMain.Visible = True
                    lblError.Text = "Please select a Company - Contact and click the View button"
                    Return
                End If

                'this is to check that at least one invoice is selected
                Dim invoiceNos As String = CommonFunctions.getSelectedIdsOfGridForPIUnPaid(gvPurchaseInv, "Check", "hdnID", "hdnStatus", "available") 'getSelectedInvoices
                If invoiceNos = "" Then
                    divValidationMain.Visible = True
                    lblError.Text = "Please select at least one invoice to mark as Available"
                    Return
                ElseIf (invoiceNos = "error") Then
                    divValidationMain.Visible = True
                    lblError.Text = "The selected purchase invoice(s) cannot be made available for withdrawal. Please speak to your administrator"
                    Return
                End If

                Dim ds As New DataSet
                ds = ws.WSFinance.MS_ChangePIStatusToAvailable(Session("BizDivId"), invoiceNos, "")
                If ds.Tables.Count > 0 Then
                    If ds.Tables("Success").Rows(0).Item("Success") = 0 Then
                        divValidationMain.Visible = True
                        lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
                    Else
                        If ds.Tables("SupEmailData").Rows.Count > 0 Then
                            Emails.SendMailPaymentAvailableInInvoiceSet(ds.Tables("SupEmailData").DefaultView)
                        End If
                    End If
                End If
                gvPurchaseInv.Visible = True
                gvPurchaseInv.DataBind()
            Else
                divValidationMain.Visible = True
            End If
        Else
            lblPermissionError.Visible = True
            lblPermissionError.Text = "Permission Denied"
            Exit Sub
        End If
    End Sub
    Public Sub WithHoldPayment(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then
            divValidationMain.Visible = False
            lblError.Text = ""

            'this is to check tht something is there in the viewstate i.e. the view button is clicked or not
            If ViewState("Supplier") <> UCSearchContact1.ddlContact.SelectedValue Then
                divValidationMain.Visible = True
                lblError.Text = "Please select a Company - Contact and click the View button"
                Return
            End If

            'this is to check that at least one invoice is selected
            Dim invoiceNos As String = CommonFunctions.getSelectedIdsOfGridForPIUnPaid(gvPurchaseInv, "Check", "hdnID", "hdnStatus", "hold") 'getSelectedInvoices
            If invoiceNos = "" Then
                divValidationMain.Visible = True
                lblError.Text = "Please select at least one invoice to mark payment on hold."
                Return
            ElseIf (invoiceNos = "error") Then
                divValidationMain.Visible = True
                lblError.Text = "The selected purchase invoice(s) is not able to be put on hold. Please speak to your administrator"
                Return
            End If

            Dim ds As New DataSet
            ds = ws.WSFinance.MS_ChangePIStatusToAvailable(Session("BizDivId"), invoiceNos, "hold")
            If ds.Tables.Count > 0 Then
                If ds.Tables("Success").Rows(0).Item("Success") = 0 Then
                    divValidationMain.Visible = True
                    lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
                    'Else
                    '    If ds.Tables("SupEmailData").Rows.Count > 0 Then
                    '        Emails.SendMailPaymentAvailableInInvoiceSet(ds.Tables("SupEmailData").DefaultView)
                    '    End If
                End If
            End If
            gvPurchaseInv.Visible = True
            gvPurchaseInv.DataBind()
        Else
            divValidationMain.Visible = True
        End If
    End Sub


    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                      ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        Dim maxRows As Integer = 0
        Dim Over30Day As Boolean = False
        If chkShowAll.Checked Then
            maxRows = 0
        Else
            maxRows = maximumRows
        End If
        If chkOver30Days.Checked Then
            Over30Day = True
        End If
        If Not IsNothing(ViewState("NewSP")) Then
            If ViewState("NewSP") = 1 Then
                startRowIndex = 0
            End If
        End If
        Dim mode As String
        'If ViewState("mode") = "Available" Then
        '    mode = "Available"
        'ElseIf ViewState("mode") = "withhold" Then
        '    mode = "withhold"
        'Else
        '    mode = "All"
        'End If
        If ViewState("mode") = "withhold" Then
            mode = "withhold"
            tdMakeAvailable.Visible = True
        Else
            mode = "Available"
        End If





        ds = ws.WSFinance.MS_GetPurchaseInvoicesForStatusUA(UCSearchContact1.ddlContact.SelectedValue, Session("BizDivId"), Over30Day, chkHideAvailable.Checked, sortExpression, startRowIndex, maxRows, CheckHideDemo.Checked, mode)
        ViewState("rowCount") = ds.Tables("tblCount").Rows(0)(0)
        ViewState("UnPaid") = ds.Tables("tblUnPaidCount").Rows(0)(0)

        If Not ViewState("rowCount") Is Nothing Then
            If chkShowAll.Checked Then
                gvPurchaseInv.AllowPaging = False
            Else
                gvPurchaseInv.AllowPaging = True
            End If
        End If

        Return ds
    End Function

    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function


    Private Sub gvPurchaseInv_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPurchaseInv.PreRender
        Dim grid As GridView = CType(sender, GridView)
        Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
        Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

        If Not (grid Is Nothing) Then
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
        End If
            If Not (pagerRowBottom Is Nothing) Then
            pagerRowBottom.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvPurchaseInv.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvPurchaseInv, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex

    End Sub

    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvPurchaseInv.Visible = True
        gvPurchaseInv.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvPurchaseInv.DataBind()
    End Sub



    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvPurchaseInv.RowDataBound
        MakeGridViewHeaderClickable(gvPurchaseInv, e.Row)
    End Sub

    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        Dim grid As GridView = gvPurchaseInv
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
                If ViewState("UnPaid") <> 0 Then
                    grid.Columns(0).Visible = True
                End If
            End If

        End If
    End Sub

    Private Sub chkOver30Days_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOver30Days.CheckedChanged
        Dim str As String = ""
        str = UCSearchContact1.ddlContact.SelectedItem.Value

        UCSearchContact1.BizDivID = Session("BizDivId")
        UCSearchContact1.ClassId = 0
        If chkOver30Days.Checked = True Then
            UCSearchContact1.Filter = "MakePIAvailableOver30Days"
        Else
            UCSearchContact1.Filter = "MakePIAvailable"
        End If

        UCSearchContact1.populateContact()

        Dim li As ListItem
        li = UCSearchContact1.ddlContact.Items.FindByValue(str)
        If Not IsNothing(li) Then
            UCSearchContact1.ddlContact.SelectedValue = str
            PopulateGrid()
        Else
           
        End If


    End Sub
    Public Sub Tab1Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtn1.Click
        lnkbtn1.CssClass = "HighlightTab"
        lnkbtn2.CssClass = "NormalTab"
        tdMakeAvailable.Visible = True
        tdHold.Visible = True
        ViewState.Add("mode", "Available")

        PopulateGrid()

    End Sub
    Public Sub Tab2Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtn2.Click
        lnkbtn2.CssClass = "HighlightTab"
        lnkbtn1.CssClass = "NormalTab"
        tdMakeAvailable.Visible = True
        tdHold.Visible = False
        ViewState.Add("mode", "withhold")

        PopulateGrid()

    End Sub

    Private Sub lnkExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExport.Click
        Dim Over30Day As Boolean = False
        If chkOver30Days.Checked Then
            Over30Day = True
        End If
        Dim Mode As String
        If ViewState("mode") = "withhold" Then
            Mode = "withhold"
        Else
            Mode = "Available"
        End If

        'Dim strurl As String
        hdnEtoE.Value = ApplicationSettings.WebPath & "ExportToExcel.aspx?page=PIUnpaidAvailable&BizDivId=" & Session("BizDivId") & "&ContactId=" & UCSearchContact1.ddlContact.SelectedValue & "&Over30Day=" & Over30Day & "&HideAvailable=" & chkHideAvailable.Checked & "&sortExpression=Total&startRowIndex=0&HideDemo=" & CheckHideDemo.Checked & "&mode=" & Mode
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "js", "handleReceivablesEtoE();", True)
        PopulateGrid()

    End Sub
End Class