﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Resend Email" CodeBehind="ResendEmail.aspx.vb" Inherits="Admin.ResendEmail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


	        
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" --> 
              <table width="100%"  border="0" cellspacing="0" cellpadding="15">
                <tr>
                  <td><strong>Resend Email </strong></td>
                </tr>
                <tr>
                  <td><P>Enter Email :<BR>
                        <asp:TextBox id="txtEmailtoSend" runat="server"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rgEmail" runat="server" ControlToValidate="txtEmailtoSend" ErrorMessage="Please enter valid Email Address" ValidationExpression="^.+@[^\.].*\.[a-z ]{2,}$"></asp:RegularExpressionValidator>
                  </P>
                    <P>
                      <asp:Button id="btnResendEmail" runat="server" Text="Resend Email" onclick=btnResendEmail_Click></asp:Button>
                      <BR>
                    </P>
                    <P>
                      <asp:Label id="lblMsg"  runat="server"></asp:Label>
                    </P></td>
                </tr>
              </table>
              <P>&nbsp;</P>
			<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
         
      </div>
      
      
      </asp:Content>