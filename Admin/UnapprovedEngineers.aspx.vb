﻿
Public Class UnapprovedEngineers
    Inherits System.Web.UI.Page
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents ObjectDataSource1 As ObjectDataSource

    ''' <summary>
    ''' Shared wsobject to access web service webmethods
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)

            UCSearchContact1.BizDivID = Session("BizDivId")
            UCSearchContact1.Filter = "UnApprovedEngineers"
            UCSearchContact1.populateContact()

            Dim bizDivId As Integer

            If Not IsNothing(Request("bizDivId")) Then
                If Request("bizDivId") <> "" Then
                    bizDivId = Request("bizDivId")
                Else

                    bizDivId = getDefaultBizDiv()
                End If
            Else
                bizDivId = getDefaultBizDiv()
            End If
            ViewState("bizDivId") = bizDivId

            If Trim(Request("PS")) <> "" Then
                If IsNumeric(Trim(Request("PS"))) = True Then
                    If CType(Trim(Request("PS")), Integer) <> 0 Then
                        hdnPageSize.Value = CType(Trim(Request("PS")), Integer)
                    End If
                End If
            End If
            hdnPageNo.Value = 0
            If Trim(Request("PN")) <> "" Then
                If IsNumeric(Trim(Request("PN"))) = True Then
                    If CType(Trim(Request("PN")), Integer) <> 0 Then
                        hdnPageNo.Value = CType(Trim(Request("PN")), Integer)
                    End If
                End If
            End If

            ViewState!SortExpression = ""
            If Trim(Request("SC")) <> "" Then
                ViewState!SortExpression = Trim(Request("SC"))
            Else
                ViewState!SortExpression = "Name"
            End If
            Dim sd As SortDirection
            sd = SortDirection.Ascending
            If Trim(Request("SO")) <> "" Then
                If Trim(Request("SO")).ToLower = "true" Then
                    sd = SortDirection.Ascending
                End If
            End If

            gvContacts.PageSize = hdnPageSize.Value
            gvContacts.Sort(ViewState!SortExpression, sd)
            PopulateGrid()
            gvContacts.PageIndex = hdnPageNo.Value
        End If

        lblMsg.Text = ""
    End Sub

    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvContacts.DataBind()
        End If

    End Sub

    ''' <summary>
    ''' sets the text for the confirm button. 
    ''' </summary>
    ''' <remarks></remarks>
    'Private Sub setConfirmButtonText(ByVal gvPagerRow As GridViewRow)
    '    CType(gvPagerRow.FindControl("ConfirmBtnApprove"), AjaxControlToolkit.ConfirmButtonExtender).ConfirmText = "Do you want to approve engineer?"
    'End Sub

#Region "Grid Functions"

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        Dim rowCount As Integer
        Dim bizDivId As Integer = ViewState("bizDivId")

        If Not IsNothing(ViewState("CompanyID")) Then
            If ViewState("CompanyID") <> -1 Then
                ds = ws.WSContact.GetUnapprovedEngineersListing(bizDivId, ViewState("CompanyID"), sortExpression, startRowIndex, maximumRows, rowCount)
                ViewState("rowCount") = rowCount
            Else
                ViewState("rowCount") = 0
            End If
        Else
            ViewState("rowCount") = 0
        End If
        Return ds
    End Function

    ''' <summary>
    ''' ObjectDataSource Controls select Count Method which returns the row count for the listing 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowDataBound
        MakeGridViewHeaderClickable(gvContacts, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvContacts, e.Row, Me)
        End If
    End Sub

    Private Sub gvContacts_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvContacts.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
                ''top pager buttons
                'Dim tdApproveTop As HtmlTableCell = CType(pagerRowTop.FindControl("tdApprove"), HtmlTableCell)
                'tdApproveTop.Visible = True
                'Dim chkSelectAll As CheckBox = CType(pagerRowTop.FindControl("chkSelectAll"), CheckBox)
                'chkSelectAll.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

                ''bottom pager buttons
                'Dim tdApproveBottom As HtmlTableCell = CType(pagerRowBottom.FindControl("tdApprove"), HtmlTableCell)
                'Dim chkSelectAll As CheckBox = CType(pagerRowBottom.FindControl("chkSelectAll"), CheckBox)

                'tdApproveBottom.Visible = False
                'chkSelectAll.Visible = False
            End If
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)



        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
        'setConfirmButtonText(gvPagerRow)
    End Sub

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        gvContacts.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

#End Region

#Region "Get Functions"



    Private Function getDefaultBizDiv() As Integer
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryUK
                Return ApplicationSettings.OWUKBizDivId
            Case ApplicationSettings.CountryDE
                Return ApplicationSettings.OWDEBizDivId
        End Select
    End Function

#End Region

#Region "Actions"

    ''' <summary>
    ''' populates the list of contacts
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnView.Click
        Page.Validate()
        If Page.IsValid Then

            If UCSearchContact1.ddlContact.SelectedValue = "" Then
                ViewState("CompanyID") = -1
            ElseIf UCSearchContact1.ddlContact.SelectedValue = "ALL" Then
                ViewState("CompanyID") = 0
            Else
                ViewState("CompanyID") = UCSearchContact1.ddlContact.SelectedValue
            End If
            If ViewState("CompanyID") <> -1 Then
                lblMsg.Text = ""
                PopulateGrid()
                gvContacts.PageIndex = 0
            Else
                lblMsg.Text = "* Please select a contact to continue"
            End If
        End If
    End Sub

#End Region

#Region "Status Update"

    ''' <summary>
    ''' Function to set the company status to approve - applicable only to buyers
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ApproveContact(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ds As DataSet
        lblMsg.Text = ""
        Dim ContactIds As String = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnContactId") 'getSelectedContactIds()
        If ContactIds = "" Then
            lblMsg.Text = ResourceMessageText.GetString("SelectContact") '"Please select  contact to change the status"
            Return
        End If
        ds = ws.WSContact.UpdateContactsOWApproveStatus(ContactIds, CInt(Session("UserId")))
        If ds.Tables.Count <> 0 Then
            If ds.Tables("tblStatus").Rows(0).Item("Status") <> 0 Then
                ' For populating the Textbox with the original contents of the mail
                lblMsg.Text = "Approved Successfully."
                PopulateGrid()
            End If
        End If
    End Sub



    Public Function getSelectedContactIds() As String
        Dim ContactIds As String = ""
        For Each row As GridViewRow In gvContacts.Rows
            Dim chkBox As HtmlInputCheckBox = CType(row.FindControl("Check"), HtmlInputCheckBox)
            If chkBox.Checked Then
                If ContactIds = "" Then
                    ContactIds = CType(row.FindControl("hdnContactId"), HtmlInputHidden).Value
                Else
                    ContactIds += "," & CType(row.FindControl("hdnContactId"), HtmlInputHidden).Value
                End If
            End If
        Next
        Return ContactIds
    End Function

#End Region

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub


    ''' <summary>
    ''' To return dispplay path information of the attached file
    ''' </summary>
    ''' <param name="filePath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getlink(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachment")

                Dim str As String = filePath.Substring(index)
                'str = ApplicationSettings.AttachmentDisplayPath(ApplicationSettings.OWUKBizDivId) + str
                str = ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/" + str + CommonFunctions.AzureStorageAccessKey().ToString
                Return str.Replace("\", "/")
            End If
        End If
        Return ""
    End Function

End Class