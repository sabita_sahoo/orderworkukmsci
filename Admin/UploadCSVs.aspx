<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="UploadCSVs.aspx.vb" Inherits="Admin.UploadCSVs" 
    Title="OrderWork : Upload CSV" %>
    <%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript">
function ExecuteSSIS()
{
 document.getElementById("ctl00_ContentPlaceHolder1_divLoading").style.display = "inline";
 document.getElementById("ctl00_ContentPlaceHolder1_tblExecuteBtn").style.display = "none";
 document.getElementById("ctl00_ContentPlaceHolder1_btnExecute").click();   
}
</script>
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
 
<div style="margin-left:15px; margin-top:5px; font-size:12px;min-height:550px;">
<p class="paddingB4 HeadingRed"><strong>Upload CSV for John Lewis</strong> </p>
            
            <div class="gridText" id="divLoading" style="display:none;"  runat="server">
                <img  align=middle src="Images/indicator.gif" />
                <b>Uploading Data... Please Wait</b>
            </div>  
            
            <table id="tblUpload" runat="server" visible="true">                         
                <tr><td><asp:Label ID="lblMsg" runat="server" class="HeadingRed" ></asp:Label></td></tr>
                <tr style="display:none;">                   
                    <td>
                     <b>Company Name</b><br />
                       <uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact>
                    </td>
                </tr>  
                 <tr id="trType" runat="server" >                  
                    <td valign="top" style="padding-bottom:20px;">
                    <b>Type</b><br />
                       <asp:DropDownList ID="ddType" runat="server" AutoPostBack="true">
                            <asp:ListItem Text="Select" Value= "Select"></asp:ListItem>
                            <asp:ListItem Text="CSV" Value= "CSV"></asp:ListItem>
                            <asp:ListItem Text="Excel" Value= "Excel"></asp:ListItem>
                       </asp:DropDownList>
                       
                        <a id="ancCSVSample"  runat="server" visible="false" target="_blank" class="HeadingRed"><strong>CSV Sample</strong></a>
                        
                        <a id="ancExcelSample"  runat="server" visible="false"  target="_blank" class="HeadingRed"><strong>Excel Sample</strong></a>
                         
                    </td>
                </tr>               
                   <tr id="trUpload" runat="server" visible="false">                   
                    <td>
                    <b>Upload <asp:Label ID="lblType" runat="server"></asp:Label></b><br />
                    <asp:FileUpload ID="FileUpload1" runat="server"  /><br style="clear:both;"/><br />
                     <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                        <asp:LinkButton id="lnkbtnUpload" runat="server" CssClass="txtButtonRed" CausesValidation="false">&nbsp;Upload&nbsp;</asp:LinkButton>
                     </div>
                    </td>
                </tr>                 
                 <tr>                   
                    <td>
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:120px;" id="tblExecuteBtn" runat="server">
                        <a Class="txtButtonRed" style="cursor:pointer;" onclick="Javascript:ExecuteSSIS();"> &nbsp;Execute SSIS&nbsp;</a>
                        <asp:Button runat="server" ID="btnExecute" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
                     </div>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
            </table>
                  
            <table id="tblPostUpload" runat="server" >
                 
                
                <tr>
                <td style="Height: 26px">
                <a id="CsvValidRecords" runat="server" target="_blank" class="HeadingRed"> <strong>Valid Records</strong></a></td>
                </tr>                
                <tr style="margin-top:5px;">
                    <td style="Height: 26px">
                        <a id="CsvInValidRecords"  runat="server"  target="_blank" class="HeadingRed"><strong>InValid Records</strong></a>
                    </td>                    
                </tr>
                <tr>
                 <td>
                  <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                    <asp:LinkButton id="lnkbtnBack" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Back&nbsp;</asp:LinkButton>
                  </div>
                 </td>
                </tr>
            </table>            
        </div>
</asp:Content>
