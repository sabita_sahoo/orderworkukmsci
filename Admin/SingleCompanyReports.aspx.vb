Imports System.IO

Partial Public Class SingleCompanyReports
    Inherits System.Web.UI.Page
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents lnkBackToListing As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents tdBackToListing As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lnkExportReportExcel As Global.System.Web.UI.WebControls.LinkButton
    Protected WithEvents UCDateRange1 As UCDateRange
    Public Shared ws As New WSObjs



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            'Set property of UC to select only users with business area defined
            UCSearchContact1.ContactType = "BusinessAreaDefined"

            lnkBackToListing.HRef = getBackToListingLink()

            UCDateRange1.txtToDate.Text = Date.Today.Date
            UCDateRange1.txtFromDate.Text = CType((DateTime.Now.Year & "/" & DateTime.Now.Month & "/" & 1), DateTime)
            lnkBackToListing.HRef = getBackToListingLink()
        End If
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                UCSearchContact1.BizDivID = ApplicationSettings.OWDEBizDivId
                ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
            Case ApplicationSettings.CountryUK
                UCSearchContact1.BizDivID = ApplicationSettings.OWUKBizDivId
                ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
        End Select


        If Not IsNothing(Request("companyId")) Then
            If Request("companyId") <> "" Then
                If UCDateRange1.DateValidate() Then
                    generateReport()
                End If
            End If
        End If

    End Sub

    Private Sub lnkBtnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnGenerateReport.Click
        If UCDateRange1.DateValidate() Then
            generateReport()
        End If
    End Sub

    Private Sub generateReport()
        Dim languageCode As String = ""
        ErrLabel.Visible = False

        If (UCSearchContact1.ddlContact.SelectedIndex > 0) Or Not IsNothing(Request("companyId")) Then
            Dim ds As New DataSet
            If Request("companyId") <> "" Then
                ds = ws.WSStandards.GetSingleCompanyReport(Convert.ToInt32(ViewState("BizDivId")), Convert.ToInt32(Request("companyId")), UCDateRange1.txtToDate.Text.ToString.Trim, UCDateRange1.txtFromDate.Text.ToString.Trim)
                btnPrint.HRef = "~/SingleCompanyReportsPrint.aspx?BizDivID=" & ViewState("BizDivId") & "&CompanyId=" & Request("companyId") & "&StartDate=" & UCDateRange1.txtFromDate.Text.ToString.Trim & "&ToDate=" & UCDateRange1.txtToDate.Text.ToString.Trim
            Else
                ds = ws.WSStandards.GetSingleCompanyReport(Convert.ToInt32(ViewState("BizDivId")), Convert.ToInt32(UCSearchContact1.ddlContact.SelectedValue), UCDateRange1.txtToDate.Text.ToString.Trim, UCDateRange1.txtFromDate.Text.ToString.Trim)
                btnPrint.HRef = "~/SingleCompanyReportsPrint.aspx?BizDivID=" & ViewState("BizDivId") & "&CompanyId=" & UCSearchContact1.ddlContact.SelectedValue & "&StartDate=" & UCDateRange1.txtFromDate.Text.ToString.Trim & "&ToDate=" & UCDateRange1.txtToDate.Text.ToString.Trim
            End If
            Session("SingleCompanyReport") = ds

            If ds.Tables.Count > 0 Then
                'Poonam added on 3/6/2015 : Task - 4471404 : OWEM-ENH-ADMIN - Add export to excel function to /SingleCompanyReports.aspx
                lnkBtnExportToExcel.Visible = True
                TdPrint.Visible = True
                pnlShowRecords.Visible = True
                pnlNoRecords.Visible = False
                pnlInitialize.Visible = False
                If ds.Tables(0).Rows.Count > 0 Then
                    Repeater1.DataSource = ds.Tables(0)
                    Repeater1.DataBind()
                    lblCompanyName.Text = ds.Tables(0).Rows(0)("CompanyName").ToString
                    lblFromDate.Text = UCDateRange1.txtFromDate.Text.ToString.Trim
                    lblToDate.Text = UCDateRange1.txtToDate.Text.ToString.Trim
                Else
                    pnlNoRecords.Visible = True
                    pnlShowRecords.Visible = False
                    'Poonam added on 3/6/2015 : Task - 4471404 : OWEM-ENH-ADMIN - Add export to excel function to /SingleCompanyReports.aspx
                    lnkBtnExportToExcel.Visible = False
                    TdPrint.Visible = False
                    pnlInitialize.Visible = False
                End If
                If ds.Tables.Count > 1 Then
                    If ds.Tables(1).Rows.Count > 0 Then
                        Repeater2.DataSource = ds.Tables(1)
                        Repeater2.DataBind()
                    End If
                    If ds.Tables.Count > 2 Then
                        If ds.Tables(2).Rows.Count > 0 Then
                            divServices.Visible = True
                            Repeater3.DataSource = ds.Tables(2)
                            Repeater3.DataBind()
                        Else
                            divServices.Visible = False
                        End If
                        If ds.Tables.Count > 3 Then
                            If ds.Tables(3).Rows.Count > 0 Then
                                Repeater7.DataSource = ds.Tables(3)
                                Repeater7.DataBind()
                            End If
                        End If
                    End If
                End If
            Else
                pnlNoRecords.Visible = True
                pnlShowRecords.Visible = False
                'Poonam added on 3/6/2015 : Task - 4471404 : OWEM-ENH-ADMIN - Add export to excel function to /SingleCompanyReports.aspx
                lnkBtnExportToExcel.Visible = False
                TdPrint.Visible = False
                pnlInitialize.Visible = False
            End If

            ''ErrLabel.Text = ""
            ''pnlExportReport.Visible = True
            'ReportViewerOW.ShowParameterPrompts = False
            'ReportViewerOW.ShowToolBar = True
            'Dim reportSErverPath As Uri = New Uri(ApplicationSettings.ReportServerPath)
            'ReportViewerOW.ServerReport.ReportServerUrl = reportSErverPath
            'ReportViewerOW.ServerReport.ReportServerCredentials = New CustomReportCredentials.CustomReportCredentials(ApplicationSettings.ReportServerUserName, ApplicationSettings.ReportServerPassword, ApplicationSettings.ReportServerDomain)
            'Dim objParameter(5) As Microsoft.Reporting.WebForms.ReportParameter
            'Dim StrCompanyID As String = ""

            ''Give path to report            
            'ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/SingleCompanyReport"

            'objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)
            'If Request("companyId") <> "" Then
            '    objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyId", Request("companyId"))
            '    StrCompanyID = Request("companyId")
            'Else
            '    objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyId", UCSearchContact1.ddlContact.SelectedValue.ToString.Trim)
            '    StrCompanyID = UCSearchContact1.ddlContact.SelectedValue.ToString.Trim
            'End If

            'objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "True")

            ''Language parameters
            'Select Case ApplicationSettings.Country
            '    Case ApplicationSettings.CountryDE
            '        languageCode = "de-DE"
            '    Case ApplicationSettings.CountryUK
            '        languageCode = "en-GB"
            'End Select
            'objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)
            'If chkToDate.Checked = False Then
            '    ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/SingleCompanyReport"
            '    objParameter(4) = New Microsoft.Reporting.WebForms.ReportParameter("FromDate", UCDateRange1.txtFromDate.Text.ToString.Trim)
            '    objParameter(5) = New Microsoft.Reporting.WebForms.ReportParameter("ToDate", UCDateRange1.txtToDate.Text.ToString.Trim)
            '    ViewState("fromDate") = UCDateRange1.txtFromDate.Text.ToString.Trim
            '    ViewState("toDate") = UCDateRange1.txtToDate.Text.ToString.Trim

            'Else
            '    ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/SingleCompanyReportToDate"
            '    objParameter(4) = New Microsoft.Reporting.WebForms.ReportParameter("FromDate", "")
            '    objParameter(5) = New Microsoft.Reporting.WebForms.ReportParameter("ToDate", "")
            '    ViewState("fromDate") = ""
            '    ViewState("toDate") = ""

            'End If

            'ReportViewerOW.ServerReport.SetParameters(objParameter)
            'ReportViewerOW.ServerReport.Refresh()
        Else
            ErrLabel.Visible = True
            ErrLabel.Text = "* Please select a Company."
        End If
    End Sub

    Public Function getBackToListingLink() As String

        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender")
                Case "AdminSearchAccounts"
                    link = "~\SearchAccounts.aspx?"
                    link &= "bizDivId=" & Request("bizDivId")
                    If Not IsNothing(Request("vendorIDs")) Then
                        link &= "&vendorIDs=" & Request("vendorIDs")
                    End If

                    link &= "&companyId=" & Request("companyId")
                    link &= "&contactId=" & Request("contactId")
                    link &= "&classId=" & Request("classId")
                    link &= "&statusId=" & Request("statusId")
                    link &= "&roleGroupId=" & Request("roleGroupId")
                    link &= "&product=" & Request("product")
                    link &= "&region=" & Request("region")
                    link &= "&txtPostCode=" & Request("txtPostCode")
                    link &= "&txtKeyword=" & Request("txtKeyword")
                    link &= "&refcheck=" & Request("refcheck")
                    link &= "&txtFName=" & Request("txtFName")
                    link &= "&txtLName=" & Request("txtLName")
                    link &= "&txtCompanyName=" & Request("txtCompanyName")
                    'Poonam modified on 31-03-2017 - Task - OA-427 - OA - Unsupported spaces in Admin users
                    link &= "&txtEmail=" & (Request("txtEmail"))

                    link &= "&Email=" & (Request("Email"))
                    link &= "&Name=" & Request("Name")

                    link &= "&sender=" & "AdminSearchAccounts"
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    'Keep contact name at last
                    link &= "&companyname=" & Request("companyname")
                    link &= "&contactname=" & Request("contactname")
                Case Else
                    Return ""
            End Select
        End If
        Return link
    End Function

    Private Sub SingleCompanyReports_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsNothing(Request("companyId")) Then
            If Request("companyId") <> "" Then
                UCSearchContact1.ddlContact.SelectedIndex = UCSearchContact1.ddlContact.Items.IndexOf(UCSearchContact1.ddlContact.Items.FindByValue(Trim(Request("companyId"))))
                UCSearchContact1.ddlContact.Enabled = False
                tdBackToListing.Visible = True
            Else
                tdBackToListing.Visible = False
            End If
        Else
            tdBackToListing.Visible = False
        End If
    End Sub

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub

    Private Sub Repeater7_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles Repeater7.ItemDataBound
        Dim billingLocID As Integer
        billingLocID = CType(e.Item.FindControl("hdnBillingLocID"), HtmlInputHidden).Value
        Dim ds As New DataSet
        ds = CType(Session("SingleCompanyReport"), DataSet)
        If ds.Tables.Count > 4 Then
            If ds.Tables(4).Rows.Count > 0 Then
                Dim dv As New DataView
                dv = ds.Tables(4).DefaultView
                dv.RowFilter = "BillingLocID = " & billingLocID
                CType(e.Item.FindControl("Repeater4"), Repeater).DataSource = dv.ToTable
                CType(e.Item.FindControl("Repeater4"), Repeater).DataBind()
            End If
            If ds.Tables.Count > 5 Then
                If ds.Tables(5).Rows.Count > 0 Then
                    Dim dv As New DataView
                    dv = ds.Tables(5).DefaultView
                    dv.RowFilter = "BillingLocID = " & billingLocID
                    CType(e.Item.FindControl("Repeater5"), Repeater).DataSource = dv.ToTable
                    CType(e.Item.FindControl("Repeater5"), Repeater).DataBind()
                End If
                If ds.Tables.Count > 6 Then
                    If ds.Tables(6).Rows.Count > 0 Then
                        CType(e.Item.FindControl("divServices2"), HtmlGenericControl).Visible = True
                        Dim dv As New DataView
                        dv = ds.Tables(6).DefaultView
                        dv.RowFilter = "BillingLocID = " & billingLocID
                        If dv.ToTable.Rows.Count > 0 Then
                            CType(e.Item.FindControl("Repeater6"), Repeater).DataSource = dv.ToTable
                            CType(e.Item.FindControl("Repeater6"), Repeater).DataBind()
                        Else
                            CType(e.Item.FindControl("divServices2"), HtmlGenericControl).Visible = False
                        End If
                    Else
                        CType(e.Item.FindControl("divServices2"), HtmlGenericControl).Visible = False
                    End If
                End If
            End If

        End If
    End Sub
    'Poonam added on 3/6/2015 : Task - 4471404 : OWEM-ENH-ADMIN - Add export to excel function to /SingleCompanyReports.aspx
    Private Sub lnkBtnExportToExcel_Click(sender As Object, e As EventArgs) Handles lnkBtnExportToExcel.Click
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=RepeaterExport.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        Dim tb As New Table()
        Dim tr1 As New TableRow()
        Dim cell1 As New TableCell()
        cell1.Controls.Add(pnlShowRecords)
        tr1.Cells.Add(cell1)
        Dim cell2 As New TableCell()
        cell2.Text = "&nbsp;"
        Dim tr2 As New TableRow()
        tr2.Cells.Add(cell2)
        tb.Rows.Add(tr1)
        tb.Rows.Add(tr2)
        tb.RenderControl(hw)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub
End Class