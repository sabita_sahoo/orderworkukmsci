
Partial Public Class ChangeAdminAccount
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected WithEvents drpdwnUsers As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblCurrAdmin As System.Web.UI.WebControls.Label


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            populatedata()
        End If

    End Sub

    Protected Sub populatedata()
        Dim ds As New DataSet
        Dim contactID As Integer = Request.QueryString("ContactID")
        ViewState.Add("CompanyID", Request.QueryString("ContactID"))
        Dim BizDivID As Integer = Request.QueryString("BizDivID")
        Dim siteType As String = ApplicationSettings.siteTypes.admin
        Dim recordCount As Integer
        Dim startRowIndex As Integer = 0
        Dim maximumRows As Integer = 200
        Dim sortExpression As String = "Name"
        ds = ws.WSContact.GetSpecialistsListing(contactID, BizDivID, siteType, sortExpression, startRowIndex, maximumRows, recordCount, "")
        Session.Add("dsUserInfo", ds)
        Dim dsView As DataView
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            If ds.Tables(0).Rows(i)("Type") = "Administrator" Then
                adminCID.Value = ds.Tables(0).Rows(i)("ContactID")
                hdnAdminRoleID.Value = ds.Tables(0).Rows(i)("RoleGroupID")
            End If
        Next i
        dsView = ds.Tables(0).DefaultView
        dsView.RowFilter = "Type = 'Administrator'"
        If dsView.Count > 0 Then
            lblCurrAdmin.Text = dsView.Item(0)("Name")
        End If
        dsView.RowFilter = ""
        dsView.RowFilter = "Type <> 'Administrator'"
        drpdwnUsers.DataSource = dsView
        drpdwnUsers.DataBind()
        drpdwnUsers.Items.Insert(0, "Select User")

    End Sub

    Private Sub lnkbtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnSubmit.Click
        If drpdwnUsers.SelectedIndex <> 0 Then
            Dim CurrAdminContactID As Integer = adminCID.Value
            Dim NewAdminID As Integer = drpdwnUsers.SelectedValue
            Dim NewAdminStatus As Integer
            Dim OldAdminNewStatus As Integer
            Dim i As Integer
            Dim ds As New DataSet
            ds = Session("dsUserInfo")
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If drpdwnUsers.SelectedValue = ds.Tables(0).Rows(i)("ContactID") Then
                    OldAdminNewStatus = ds.Tables(0).Rows(i)("RoleGroupID")
                End If
            Next i
            Dim CompanyID As Integer
            CompanyID = ViewState("CompanyID")

            NewAdminStatus = hdnAdminRoleID.Value
            Dim dsUpdate As DataSet = ws.WSContact.UpdateAdminStatus(CompanyID, CurrAdminContactID, NewAdminID, NewAdminStatus, OldAdminNewStatus, Session("UserID"))
            Session.Remove("dsUserInfo")
            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            Response.Redirect("Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
        Else
            divvalidationSummary.Visible = True
            lblValidationMsg.Text = "Please Select User to Change"
        End If
    End Sub

    Private Sub lnkbtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnCancel.Click
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        Response.Redirect("Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
    End Sub

End Class