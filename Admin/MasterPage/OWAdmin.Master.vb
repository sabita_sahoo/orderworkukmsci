Public Partial Class OWAdmin
    Inherits System.Web.UI.MasterPage
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        UserAction.userAutoLoginAccept(Page)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hdnNewPortalURL.Value = OrderWorkLibrary.ApplicationSettings.NewPortalURL.ToString
            If Not IsNothing(Session("UserID")) Then
                hdnLoggedInUser.Value = Session("UserID").ToString
            Else
                hdnLoggedInUser.Value = 0
            End If
        End If
    End Sub
    'Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    '    Using htmlwriter As New HtmlTextWriter(New System.IO.StringWriter())
    '        MyBase.Render(htmlwriter)
    '        Dim html As String = htmlwriter.InnerWriter.ToString()

    '        If (ConfigurationManager.AppSettings.[Get]("RemoveWhitespace") & String.Empty).Equals("true", StringComparison.OrdinalIgnoreCase) Then
    '            html = Regex.Replace(html, "(?<=[^])\t{2,}|(?<=[>])\s{2,}(?=[<])|(?<=[>])\s{2,11}(?=[<])|(?=[\n])\s{2,}", String.Empty)
    '            html = Regex.Replace(html, "[ \f\r\t\v]?([\n\xFE\xFF/{}[\];,<>*%&|^!~?:=])[\f\r\t\v]?", "$1")
    '            html = html.Replace(";" & vbLf, ";")
    '        End If

    '        writer.Write(html.Trim())
    '    End Using
    'End Sub
End Class