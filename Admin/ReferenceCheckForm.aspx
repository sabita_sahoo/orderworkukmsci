<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master"   title="OrderWork : Reference Check Form" Codebehind="ReferenceCheckForm.aspx.vb" Inherits="Admin.ReferenceCheckForm" validateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			
			
			
			
		
		
			<asp:Panel ID="pnlSubmitForm" runat="server" visible="true">

<p class="paddingL20"><strong>Supplier Approval Form</strong></p>
<div class="divGrayBackGround paddingT10LR20B35L">

				<table width="900" border="0" cellPadding="0" cellSpacing="0">
				  <tr>
					<td width="680"></td>
					<td width="220" align="right" class="formTxtRed"><asp:Label ID="lblTimeStamp" runat="server" Text=""></asp:Label></td>
				  </tr>
				   <tr height="5">
					<td width="680"></td>
					<td width="220" align="right" class="formTxtRed"></td>
				  </tr>
				</table>
				
				
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				   <tr><td width="10"></td>
				    <td>&nbsp;</td>
					<td align="right">
					    <table width="390"  border="0" align="right" cellPadding="0"  cellSpacing="0" >
							<tr>
								<td width="100" >
                                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                        <a runat="server" id="lnkBackToListingTop"  href="#" class="txtButtonRedBack">&nbsp;Back to Listing&nbsp;</a>
                                    </div>
                                </td>
								<td width="100">
                                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                        <asp:LinkButton runat="server" ID="btnSaveTopNew" CssClass="txtButtonRed" Text=" Save Changes " CausesValidation="false"></asp:LinkButton>
                                    </div>
                                </td>
								  
								  <td width="150" align="right" id="tblApprovedSuppTop" runat="server">
                                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:150px;">
                                        <asp:LinkButton runat="server" ID="btnApprovedSuppTop" CssClass="txtButtonRed" Text="Approve Supplier" CausesValidation="false"></asp:LinkButton>
                                    </div>
                                  </td>
								  
								 
							</tr>
						</table></td>
					
				</tr>
				<tr height="15"><td>&nbsp;</td></tr>
				
						 <tr>
						 	  <td colspan="3">
								 <div id="divValidationMain" class="divValidation" runat=server >
									<div class="roundtopVal "><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
										<table width="100%"  border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td height="15" align="center">&nbsp;</td>
												<td height="15">&nbsp;</td>
												<td height="15">&nbsp;</td>
											</tr>
											<tr valign="middle">
												<td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
												<td class="validationText"><div  id="divValidationMsg"></div>
												<asp:Label ID="lblErrorMsg"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
												<asp:ValidationSummary id="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
												</td>
												<td width="20">&nbsp;</td>
											</tr>
											<tr>
												<td height="15" align="center">&nbsp;</td>
												<td height="15">&nbsp;</td>
												<td height="15">&nbsp;</td>
											</tr>
										</table>
									<div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
								</div>	
            				</td>
						 </tr>
				</table>			
				

				<table width="100%" border="0" cellPadding="0" cellSpacing="0" bgcolor="#DFDFDF" >
				<tr>
				  <td align="left" valign="top"><img src="Images/Curves/Grey-TLC.gif" width="5" height="5"></td>
				  <td valign="top">&nbsp;</td>
				  <td>&nbsp;</td>
				  <td align="right" valign="top"><img src="Images/Curves/Grey-TRC.gif" width="5" height="5"></td>
			    </tr>
										
					<tr>
					<td width="10"></td>
					
					<td width="400" valign="top">				
						<table class="paddingL10" width="100%" border="0" align="center" cellPadding="0" cellSpacing="0">
							<tr >
								<td width="100" class="formTxt">Company Name</td>
								<td class="formTxt"><asp:Label ID="lblCompanyName" runat="server" Text=" " Font-Bold="true"></asp:Label></td>								
							</tr>
							<tr>
							  <td class="formTxt">&nbsp;</td>
							  <td class="formTxt">&nbsp;</td>
						  </tr>
							<tr>
								<td class="formTxt">VAT Reg. No.<asp:RegularExpressionValidator ControlToValidate="txtVATRegNo" ErrorMessage="Please enter correct VAT Reg. Number" ForeColor="#EDEDEB" ID="rgVatNo" runat="server" ValidationExpression="(^(([GB]|[gb])\s?)*(([1-9]\d{8})|([1-9]\d{11}))$)" >*</asp:RegularExpressionValidator></td>
								<td class="formTxt"><asp:TextBox ID="txtVATRegNo" CssClass="formField150" runat="server"></asp:TextBox></td>								
							</tr>
							<tr>
							  <td class="formTxt">&nbsp;</td>
							  <td class="formTxt">&nbsp;</td>
						  </tr>
							<tr>
								<td class="formTxt">Co. Reg. No.</td>
								<td class="formTxt"><asp:Textbox ID="txtCompRegNo" runat="server" CssClass="formField150"></asp:TextBox></td>								
							</tr>
							<tr>
							  <td class="formTxt">&nbsp;</td>
							  <td class="formTxt">&nbsp;</td>
						  </tr>
							<%--<tr>
								<td class="formTxt">No. of Employees<asp:RequiredFieldValidator id="rqCompanyStrength" runat="server" ErrorMessage="Please select No. Of Employees"
																	ForeColor="#EDEDEB" ControlToValidate="ddlNoOfEmployees">*</asp:RequiredFieldValidator></td>
								<td class="formTxt"><asp:DropDownList id="ddlNoOfEmployees" runat="server" CssClass="formField150"></asp:DropDownList></td>								
							</tr>--%>
                            <tr>
								<td class="formTxt">No Of Engineers:<SPAN class="bodytxtValidationMsg">*</SPAN>
                                    <asp:RequiredFieldValidator id="rqNoOfEngg" runat="server" ErrorMessage="Please enter No. Of Engineers"	ForeColor="#EDEDEB" ControlToValidate="txtNoOfEngineers">*</asp:RequiredFieldValidator> 
                                    <asp:RangeValidator ID="RangeNoOfEngg" ControlToValidate="txtNoOfEngineers" MinimumValue="1" MaximumValue="1000" Type="Integer" ErrorMessage="No. of Engineers must be from 1 to 1000" runat="server" ForeColor="#EDEDEB" >*</asp:RangeValidator>
                                    <asp:RegularExpressionValidator ID="RegExNoOfEngineers" runat="server" ControlToValidate="txtNoOfEngineers" ErrorMessage="Error with the No Of Engineers field HTML tags not allows (remove any < and > characters from the field)" 
                                  ValidationExpression="^[^<>]+$" ForeColor="#EDEDEB">*</asp:RegularExpressionValidator></td>
								<td class="formTxt"> <asp:TextBox ID="txtNoOfEngineers" runat="server" CssClass="clsRightViewForFeilds clsFormFeildStyle width205"></asp:TextBox>  </td>								
							</tr>-
							<tr>
							  <td class="formTxt">&nbsp;</td>
							  <td class="formTxt">&nbsp;</td>
						  </tr>
							<tr>
								<td class="formTxt">&nbsp;</td>
								<td class="formTxt">
								<%-- <input runat="server" type="checkbox" id="chkTrackSP" />&nbsp;<span class="txtNavigation">Track Supplier</span>								--%>
								</td>								
							</tr>
					  </table>
					</td>
					
					<td>
						<table width="100%" border="0" cellPadding="0" cellSpacing="0">
							<tr class="paddingB3">	
								<td class="formTxtRedBold">Reference Check Details 
							  <p></p></td>
							</tr>
							<tr>	
								<td class="formTxt">
									<table>
										<tr>
											<td width="110" class="formTxt">Employee Liability</td>
											<td width="210" class="formTxt"><asp:Label ID="lblEmpLiability" runat="server" Text=" " Font-Bold="true" ></asp:Label></td>	
											<td class="formTxt">Cover Amount</td>
										</tr>
								  </table>								
							 </td>
							</tr>
							<tr>	
								<td class="formTxt">
									<table>
										<tr>
											<td width="95" align="left"><a class="formTxtRed" runat="server" id="anchorEmployee" target="_blank"  href="#"></a>  </td>
											<td width="200">
															
															<asp:CustomValidator id="CustValWOBeginDate1" runat="server" ErrorMessage="Employers Liability Date should be in DD/MM/YYYY Format "
															ForeColor="#EDEDEB" ControlToValidate="txtEmployeeLiabilityDate">*</asp:CustomValidator>
															<asp:CustomValidator id="checkFutureDate1" runat="server" ErrorMessage="Expiry date for employers liability cannot be in the past. "
															ForeColor="#EDEDEB" OnServerValidate="checkFutureDate1_ServerValidate" ControlToValidate="txtEmployeeLiabilityDate">*</asp:CustomValidator>
															
										
															<asp:TextBox id="txtEmployeeLiabilityDate" runat="server" CssClass="formFieldGrey width120" onfocus="clearExpiryDate(this)" onblur="fillExpiryDate(this)" ></asp:TextBox>		
															
															<img alt="Click to Select" src="Images/calendar.gif" id="btnEmpLDate" style="cursor:pointer; vertical-align:middle;" />
															
                                                            <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnEmpLDate TargetControlID="txtEmployeeLiabilityDate" ID="calEmpLDate" runat="server">
                                                          </cc1:CalendarExtender>
											</td>
										  <td width="150"><asp:CustomValidator id="rqCoverAmount1" runat="server" ErrorMessage="Please enter a positive number for Employee liability cover Amount . "
														   ForeColor="#EDEDEB" OnServerValidate="rqCoverAmount1_ServerValidate"   ControlToValidate="txtCoverAmountEmpLiability" >*</asp:CustomValidator>
														   <asp:TextBox id="txtCoverAmountEmpLiability" runat="server" CssClass="formFieldGrey width120" onfocus="clearCoverAmount(this)" onblur="fillCoverAmount(this)"></asp:TextBox>
														   
									      </td>
											
										</tr>
								  </table>
																
										
									 <p></p>
							  </td>
							</tr>
							
							<tr>	
								<td class="formTxt">
									<table>
										<tr>
											<td width="110" class="formTxt">Public Liability</td>
											<td width="210" class="formTxt"><asp:Label ID="lblPublicLiability" runat="server" Text=" " Font-Bold="true" ></asp:Label></td>	
											<td class="formTxt">Cover Amount</td>
										</tr>
								  </table>								
								</td>
							</tr>
							<tr>	
								<td class="formTxt">
									<table>
										<tr>
											<td width="95"><a class="formTxtRed" runat="server" id="anchorPublic" target="_blank"    href="#"></a> </td>
											<td width="200">
											
															<asp:CustomValidator id="CustValWOBeginDate2" runat="server" ErrorMessage="Public Liability Date should be in DD/MM/YYYY Format"
															ForeColor="#EDEDEB" ControlToValidate="txtPublicLiabilityDate">*</asp:CustomValidator>
															<asp:CustomValidator id="checkFutureDate2" runat="server" ErrorMessage="Expiry date for public liability cannot be in the past. "
															ForeColor="#EDEDEB" OnServerValidate="checkFutureDate2_ServerValidate"  ControlToValidate="txtPublicLiabilityDate">*</asp:CustomValidator>
															<asp:TextBox id="txtPublicLiabilityDate" runat="server" CssClass="formFieldGrey width120" onfocus="clearExpiryDate(this)" onblur="fillExpiryDate(this)"></asp:TextBox>
															<img alt="Click to Select" src="Images/calendar.gif" id="btnPubLDate" style="cursor:pointer; vertical-align:middle;" />																
                                                                  <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnPubLDate TargetControlID="txtPublicLiabilityDate" ID="calPubLDate" runat="server">
                                                                  </cc1:CalendarExtender>
											</td>
											  <td width="150"><asp:CustomValidator id="rqCoverAmount2" runat="server" ErrorMessage="Please enter a positive number for public liability cover Amount . "
																ForeColor="#EDEDEB" OnServerValidate="rqCoverAmount2_ServerValidate"  ControlToValidate="txtCoverAmountPubLiability">*</asp:CustomValidator>
																<asp:TextBox id="txtCoverAmountPubLiability" runat="server" CssClass="formFieldGrey width120" onfocus="clearCoverAmount(this)" onblur="fillCoverAmount(this)" ></asp:TextBox>
																
											 </td>
										</tr>
								  </table>
								
									
									<p></p>
								</td>
							</tr>
							<tr>	
								<td class="formTxt">
									<table>
										<tr>
											<td width="110" class="formTxt">Professional Indemnity</td>
											<td width="210" class="formTxt"><asp:Label ID="lblProfIndemnity" runat="server" Text=" " Font-Bold="true" ></asp:Label></td>	
											<td class="formTxt">Cover Amount</td>
										</tr>
								  </table>								
								</td>
							</tr>
							<tr>	
							
								
								<td class="formTxt">
									<table>
									<tr>
										<td width="95"><a class="formTxtRed" runat="server" id="anchorProfIndemnity" target="_blank"   href="#"></a> </td>
										<td width="200">	
										
															<asp:CustomValidator id="CustValWOBeginDate3" runat="server" ErrorMessage="Professional Indemnity Date should be in DD/MM/YYYY Format"
															ForeColor="#EDEDEB" ControlToValidate="txtProfessionalIndemnityDate">*</asp:CustomValidator>
															<asp:CustomValidator id="checkFutureDate3" runat="server" ErrorMessage="Expiry date for Professional Indemnity cannot be in the past. "
															ForeColor="#EDEDEB" OnServerValidate="checkFutureDate3_ServerValidate"  ControlToValidate="txtProfessionalIndemnityDate">*</asp:CustomValidator>	
															<asp:TextBox id="txtProfessionalIndemnityDate" runat="server" CssClass="formFieldGrey width120" onfocus="clearExpiryDate(this)" onblur="fillExpiryDate(this)"></asp:TextBox>
															<img alt="Click to Select" src="Images/calendar.gif" id="btnIndemDate" style="cursor:pointer; vertical-align:middle;" />															
                                                                  <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnIndemDate TargetControlID="txtProfessionalIndemnityDate" ID="calIndemDate" runat="server">
                                                                  </cc1:CalendarExtender>
															
															
										</td>
									  <td width="150"><asp:CustomValidator id="rqCoverAmount3" runat="server" ErrorMessage="Please enter a positive number  for professional indemnity cover Amount. "
															ForeColor="#EDEDEB" OnServerValidate="rqCoverAmount3_ServerValidate"  ControlToValidate="txtCoverAmountProfIndemnity">*</asp:CustomValidator>
                                        <asp:TextBox ID="txtCoverAmountProfIndemnity" runat="server" CssClass="formFieldGrey width120" onfocus="clearCoverAmount(this)" onblur="fillCoverAmount(this)"></asp:TextBox>
										
</td>															
									</tr>
								</table>
										
										
								</td>
							</tr>
							
						</table>
					</td>
					<td width="10">&nbsp;</td>
				</tr>
				<tr>
				  <td align="left" valign="bottom"><img src="Images/Curves/Grey-BLC.gif" width="5" height="5"></td>
				  <td valign="top">&nbsp;</td>
				  <td>&nbsp;</td>
				  <td align="right" valign="bottom"><img src="Images/Curves/Grey-BRC.gif" width="5" height="5"></td>
			    </tr>
				
				</table>
				
				
				<p>&nbsp;</p>
			<table bgcolor="#F7E4D3" width="900"  border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td width="20" align="left" valign="top"><img src="Images/Curves/SupApp-TLC.gif" width="6" height="6"></td>
				<td colspan="8">&nbsp;</td>
				<td width="10" align="right" valign="top"><img src="Images/Curves/SupApp-TRC.gif" width="6" height="6" align="right"></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td colspan="8"><span class="formTxtRedBold">Reference 1</span></td>
				<td>&nbsp;</td>
			  </tr>
			  
			  <tr class="paddingB7">
				<td>&nbsp;</td>
				<td width="170"  class="formTxt">Company Name :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblCompName1"  runat="server" Text=""></asp:Label>
				</span></td>
				<td width="65" align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td width="170" class="formTxt">Contact :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblContact1" runat="server" Text=""></asp:Label>
				</span></td>
				<td width="65" align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td width="170"  class="formTxt">Email :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblEmail1" runat="server" Text=""></asp:Label>
				</span></td>
				<td width="65" align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td width="120" class="formTxt">Phone :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblPhone1" runat="server" Text=""></asp:Label>
				</span></td>
				<td width="10" align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr class="paddingB7">
				<td >&nbsp;</td>
				<td class="formTxt" >Work done :<span class="paddingB7 formTxt">
				  <asp:Label ID="lblWorkDate1" runat="server" Text=""></asp:Label>
				</span></td>
				<td colspan="7" bgcolor="#F7E4D3" class="paddingB7 formTxt" >&nbsp;</td>
				<td >&nbsp;</td>
			  </tr>
			  <tr>
				<td class="paddingB7">&nbsp;</td>
				<td colspan="8" class="paddingB7 formTxt"><asp:Label ID="lblDesc1" runat="server" Text=""></asp:Label></td>
				<td class="paddingB7">&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td colspan="8"><span class="formTxtRedBold">Reference 2 </span></td>
				<td>&nbsp;</td>
				
			  </tr>
			  <tr class="paddingB7">
				<td>&nbsp;</td>
				<td  class="formTxt">Company Name :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblCompName2" runat="server" Text=""> </asp:Label>
				</span></td>
				<td align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td  class="formTxt">Contact :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblContact2" runat="server" Text=""></asp:Label>
				</span></td>
				<td align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td  class="formTxt">Email :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblEmail2" runat="server" Text=""> </asp:Label>
				</span></td>
				<td align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td class="formTxt">Phone :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblPhone2" runat="server" Text=""></asp:Label>
				</span></td>
				<td align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr class="paddingB7">
				<td class="formTxt">&nbsp;</td>
				<td class="formTxt"  >Work done :<span class="paddingB7 formTxt">
				  <asp:Label ID="lblWorkDate2" runat="server" Text=""></asp:Label>
				</span></td>
				<td colspan="7" bgcolor="#F7E4D3" class="paddingB7 formTxt" >&nbsp;</td>
				<td >&nbsp;</td>
			  </tr>
			  <tr>
				<td class="paddingB7">&nbsp;</td>
				<td colspan="8" class="paddingB7 formTxt"><asp:Label ID="lblDesc2" runat="server" Text=""></asp:Label></td>
				<td class="paddingB7">&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td colspan="8"><span class="formTxtRedBold">Reference 3 </span></td>
				<td>&nbsp;</td>
				
			  </tr>
			  <tr class="paddingB7">
				<td>&nbsp;</td>
				<td  class="formTxt">Company Name :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblCompName3" runat="server" Text=""> </asp:Label>
				</span></td>
				<td align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td  class="formTxt">Contact :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblContact3" runat="server" Text=""></asp:Label>
				</span></td>
				<td align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td  class="formTxt">Email :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblEmail3" runat="server" Text=""> </asp:Label>
				</span></td>
				<td align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td class="formTxt">Phone :<span class="paddingB3 formTxt">
				  <asp:Label ID="lblPhone3" runat="server" Text=""></asp:Label>
				</span></td>
				<td align="left" bgcolor="#F7E4D3" class="paddingB3 formTxt">&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr class="paddingB7">
				<td class="formTxt">&nbsp;</td>
				<td class="formTxt"  >Work done :<span class="paddingB7 formTxt">
				  <asp:Label ID="lblWorkDate3" runat="server" Text=""></asp:Label>
				</span></td>
				<td colspan="7" bgcolor="#F7E4D3" class="paddingB7 formTxt" >&nbsp;</td>
				<td >&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td colspan="8" class="paddingB7 formTxt"><asp:Label ID="lblDesc3" runat="server" Text=""></asp:Label></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="left" valign="bottom"><img src="Images/Curves/SupApp-BLC.gif" width="6" height="6"></td>
				<td colspan="8">&nbsp;</td>
				<td align="right" valign="bottom"><img src="Images/Curves/SupApp-BRC.gif" width="6" height="6"></td>
			  </tr>
			</table>
		    <p>&nbsp;</p>
			
			<p class="formTxtRedBold paddingT10">Choose your Rating here: </p>
			<table class="paddingALL10L20" cellSpacing="0" cellPadding="0" border="0" width="100%" >
				<tr class="paddingB3">
					<td  width="500" ><strong>1.<span class="formTxt"> Rating Score</span></strong>
					 <asp:RadioButton id="rdoRatingPositive1" style="vertical-align:baseline; " runat="server" CssClass="formTxt" Text="Positive" TextAlign="right" GroupName="rdoRating1"></asp:RadioButton>
						&nbsp;&nbsp;
   				       <asp:RadioButton id="rdoRatingNeutral1" Checked="true" runat="server" CssClass="formTxt" Text="Neutral" TextAlign="right" GroupName="rdoRating1"></asp:RadioButton>
						&nbsp;&nbsp;
        				 <asp:RadioButton id="rdoRatingNegative1" runat="server" CssClass="formTxt" Text="Negative"	TextAlign="right" GroupName="rdoRating1"></asp:RadioButton>
                   </td> 
				</tr>				
				<tr class="paddingB3">					
					<td width="500"  class="formTxt">Comments <br>
					  <asp:TextBox TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" ID="txtMessage1" runat="server" CssClass="formFieldGrey900"  Height="50" ></asp:TextBox></td>
					
				</tr>
			</table>
			<p>	</p>
			
			<table class="paddingALL10L20" cellSpacing="0" cellPadding="0" border="0" width="100%" >
				<tr class="paddingB3">
					<td width="500" ><strong>2.<span class="formTxt"> Rating Score</span></strong>
					 <asp:RadioButton id="rdoRatingPositive2" style="vertical-align:baseline; " runat="server" CssClass="formTxt" Text="Positive" TextAlign="right" GroupName="rdoRating2"></asp:RadioButton>
						&nbsp;&nbsp;
   				       <asp:RadioButton id="rdoRatingNeutral2" Checked="true" runat="server" CssClass="formTxt" Text="Neutral" TextAlign="right" GroupName="rdoRating2"></asp:RadioButton>
						&nbsp;&nbsp;
        				 <asp:RadioButton id="rdoRatingNegative2" runat="server" CssClass="formTxt" Text="Negative"	TextAlign="right" GroupName="rdoRating2"></asp:RadioButton>
                    </td>
				</tr>				
				<tr class="paddingB3">					
					<td class="formTxt" colspan="2">Comments <br><asp:TextBox TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" ID="txtMessage2" runat="server" CssClass="formFieldGrey900"  Height="50" ></asp:TextBox></td>
					
				</tr>
			</table>
			<p></p>
			
			<table class="paddingALL10L20" cellSpacing="0" cellPadding="0"  border="0" width="100%" >
				<tr class="paddingB3">
					<td  width="500" ><strong>3. <span class="formTxt">Rating Score</span></strong>
					 <asp:RadioButton id="rdoRatingPositive3" style="vertical-align:baseline; " runat="server" CssClass="formTxt" Text="Positive" TextAlign="right" GroupName="rdoRating3"></asp:RadioButton>
						&nbsp;&nbsp;
   				       <asp:RadioButton id="rdoRatingNeutral3" Checked="true" runat="server" CssClass="formTxt" Text="Neutral" TextAlign="right" GroupName="rdoRating3"></asp:RadioButton>
						&nbsp;&nbsp;
        				 <asp:RadioButton id="rdoRatingNegative3" runat="server" CssClass="formTxt" Text="Negative"	TextAlign="right" GroupName="rdoRating3"></asp:RadioButton>
                    </td>
					
				</tr>				
				<tr class="paddingB3">					
					<td class="formTxt" colspan="2">Comments <br>
					  <asp:TextBox TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" ID="txtMessage3" runat="server" CssClass="formFieldGrey900" Height="50" ></asp:TextBox>
					<p></p></td>
					
				</tr>
				<tr class="paddingB3">					
					<td class="formTxt"><strong>General Comments</strong> 
					  </td>
					
				</tr>
				<tr class="paddingB3">
				  <td ><asp:TextBox TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" ID="txtGeneralComments" runat="server" CssClass="formFieldGrey900"  Height="50" ></asp:TextBox></td>
				  </tr>
				<tr class="paddingB3">					
					<td class="formTxt" id="tblSendMail" runat="server"><input type="checkbox" checked runat="server"  name="chkSendMail" value="checkbox" id="chkSendMail" onClick="javascript: showHideElement('chkSendMail','divMailMessage')" >
					Notify Supplier Approval on email </td>
					
				</tr>
				
			</table>
			
			<div  id="divMailMessage" runat="server" >
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
				  		<td align="left"> <span class="txtNavigation">Can edit the text that is sent to the Supplier via email.<b> Please do not remove text "<AccountStatus>" - this will be replaced with the status to which users account has been changed.</b> </span>	<asp:TextBox TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" ID="txtMailMessage" runat="server" CssClass="formFieldGrey150"  style="width:900px; height:50px;" ></asp:TextBox>
						<p></p>
						</td>
				  </tr>
                </table>                
               
                
              </div>
			  <br>
			
			<table width="390"  border="0" align="right" cellPadding="0"  cellSpacing="0" >
				<tr>
					<td width="100" >
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                            <a runat="server" id="lnkBackToListing"  href="#" class="txtButtonRedBack">&nbsp;Back to Listing&nbsp;</a>
                        </div>
                    </td>
					<td width="100">
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                            <asp:LinkButton runat="server" ID="btnSaveTop" CssClass="txtButtonRed" Text=" Save Changes " CausesValidation="false"></asp:LinkButton>
                        </div>
                    </td>
					  
					  <td width="150" id="tblApprovedSupp" runat="server">
                          <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:150px;">
                            <asp:LinkButton runat="server" ID="btnApprovedSupp" CssClass="txtButtonRed" Text="Approve Supplier" CausesValidation="false"></asp:LinkButton>
                          </div>
                      </td>
					  
					 
				</tr>
			</table>
			
			
</div>			

</asp:Panel>

 <!-- Confirm Action panel --> 
			<asp:panel ID="pnlConfirm" runat="server" visible="false">
			
			  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="padTop50">
                <tr>
                  <td width="350" align="right"><table cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td class="txtBtnImage"><asp:LinkButton id="btnConfirm" runat="server" CausesValidation="false" class="txtListing" TabIndex="53" > <img src="Images/Icons/Icon-Confirm.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Confirm </asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                  <td width="10">&nbsp;</td>
                  <td align="left" ><table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0" >
                      <tr>
                 <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td align="center" class="txtBtnImage">
                            <asp:LinkButton id="btnCancel" runat="server" CausesValidation="false" class="txtListing" TabIndex="54" > <img src="Images/Icons/Icon-Cancel.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
                          </td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
            </asp:panel>
			
			
			
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      
      </asp:Content>