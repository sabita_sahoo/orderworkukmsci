<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DebitNotes.aspx.vb" Inherits="Admin.DebitNotes" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="Debit Notes" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        

             
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
     <ContentTemplate>
			<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10px"></td>
			<td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Debit Notes" runat="server"></asp:Label></td>
			</tr>
			</table>		
	<asp:Panel runat="server" ID="PnlListing">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
		
		<td width="10px"></td>
		<td width="250">
		<%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
						<uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact>
		</td>
		<td width="300"><table width="100%" cellpadding="0" cellspacing="0" border="0">
			 <tr>
			   <td width="300">
			     <%@ Register TagPrefix="uc1" TagName="UCDateRange" Src="~/UserControls/UK/UCDateRangeUK.ascx" %>
                 <uc1:UCDateRange id="UCDateRange1" runat="server"></uc1:UCDateRange> 
			   </td>
			  </tr>
			</table>
		</td>
		<td align="left">
        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
            <asp:LinkButton ID="lnkView" causesValidation=False OnClick="lnkView_Click" runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
        </div>
		</td>			 		
		<td align="left">&nbsp;</td>
		</tr>
		</table>	
		
	
           <table><tr>
             <td class="paddingL10 padB21" valign="top" padB21 paddingTop1>
		<asp:Label ID="lblMsg" runat="server"  CssClass="bodytxtValidationMsg" Text=""></asp:Label>		  	
		</td></tr>
             </table>
	   <hr style="background-color:#993366;height:5px;margin:0px;" />
           	<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td>
              <asp:GridView ID="gvInvoices"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
               PagerSettings-Mode=NextPreviousFirstLast  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               
               <PagerTemplate>
                   
            <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>																				
							<td width="10">&nbsp;</td>												  
						  					
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right" runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
        </PagerTemplate> 
               <Columns>  
                           
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="InvoiceNo" SortExpression="InvoiceNo" HeaderText="Debit Note Number" />                
                <asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="180px" HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="Invoice Date" />                    
                <asp:TemplateField SortExpression="Total" HeaderText="Debit Note Total Amount" >               
               <HeaderStyle CssClass="gridHdr" Width="200px"  Wrap=true />
               <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
               <ItemTemplate>   
                <%#FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%>                       
               </ItemTemplate>
               </asp:TemplateField>
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="BuyerName" SortExpression="BuyerName" HeaderText="Supplier" />                
                
               <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="SalesInvoiceNo" SortExpression="SalesInvoiceNo" HeaderText="Purchase Invoice No." />                
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="Status" SortExpression="Status" HeaderText="Status" />                
               
               <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">                
                <ItemStyle CssClass="gridRowIcons" Wrap=true  Width="20px"/>
                <ItemTemplate> 
				<a runat=server target="_blank" id="lnkViewCreditNote" href=<%#"~/ViewDebitNote.aspx?invoiceNo=" & Container.dataitem("SalesInvoiceNo") & "&companyId=" & Container.DataItem("CompanyId") & "&PaymentNo=" & Container.dataitem("InvoiceNo") & "&bizDivId=" & Session("BizDivId") %>><img src='Images/Icons/ViewDebitNote.gif' alt='View Debit Note' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>
				</ItemTemplate>  
               </asp:TemplateField>

               <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">                
                <ItemStyle CssClass="gridRowIcons" Wrap=true  Width="20px"/>
                <ItemTemplate> 
				<a runat=server target="_blank" id="lnkViewSalesInvoice" href=<%#"~/ViewPurchaseInvoice.aspx?invoiceNo=" & Container.DataItem("SalesInvoiceNo") & "&bizDivId=" & Session("BizDivId") %>><img src='Images/Icons/View-Purchase-Invoice.gif' alt='View Purchase Invoice' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>
				</ItemTemplate>  
               </asp:TemplateField>                              

                                         

				</Columns>
               
                 <AlternatingRowStyle    CssClass=gridRow />
                 <RowStyle CssClass=gridRow />            
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
        
              </asp:GridView>
				 </td>
            </tr>
            </table>
				
				 <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.DebitNotes" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>

        
          </asp:Panel>		
		  
		  <asp:Panel runat="server" ID="PnlConfirm" visible="false">
			        <table width="100%" cellpadding="0" cellspacing="0" style="margin-top:100px; margin-left:100px; ">
			            <tr>
			                <td width="10px"></td>
			                <td><asp:Label ID="lblConfirm" Text="" runat="server"></asp:Label></td>
			            </tr>
			            <tr>
			                <td width="10px"></td>
			                <td><asp:Label ID="lblConfirm2" Text="" runat="server"></asp:Label></td>
			            </tr>

			        </table>		
		  </asp:Panel>


		  <asp:Panel runat="server" ID="PnlSuccess" visible="false">
			        <table width="100%" cellpadding="0" cellspacing="0" style="margin-top:100px; margin-left:100px; ">
			            <tr>
			                <td width="10px"></td>
			                <td><asp:Label ID="lblSuccess" Text=""  runat="server" style="overflow:hidden;" ></asp:Label></td>
			            </tr>
			        </table>		
		  </asp:Panel>
		  
		  </ContentTemplate>
      
             </asp:UpdatePanel>

<asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
            <!-- InstanceEndEditable --></td>
         </tr>
         </table>
      </div>
      </asp:Content>
