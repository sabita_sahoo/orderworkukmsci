﻿
<%@ Page Title="OrderWork : Dixons Complaints" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="DixonsComplaints.aspx.vb" Inherits="Admin.DixonsComplaints" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


              <div id="divContent">

         

            

        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>

        

             <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">

         

         <tr>

            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->

            <input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />                          

            <input id="hdnPageNo"  value="1" type="hidden" name="hdnPageNo" runat="server">

                  <input id="hdnPageSize" value="25" type="hidden" name="hdnPageSize" runat="server">

                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate> 

                                <table width="100%" cellpadding="0" cellspacing="0">

                                    <tr>

                                    <td width="10px"></td>

                                    <td><asp:Label ID="lblMessage" CssClass="HeadingRed"  runat="server" Text="Dixons Complaints"></asp:Label></td>

                                    </tr>

                                    </table>

              <table style="margin-left:20px;">
        <tr>
            <td colspan="3">
                <div id="div2" style="width:100%;margin-top:20px;">
			            <asp:ValidationSummary id="validationSummarySearch" runat="server" DisplayMode="List" ValidationGroup="ValidationTop" CssClass="bodytxtValidationMsg"></asp:ValidationSummary>
			    </div>
           </td>
        </tr>
        <tr>
            <td>
                      <div id="divForAdvanceSearch" runat="server" visible="true">
			            <div id="divrow2" style="width:100%;margin-top:20px;">
			                <div class="formTxt" style="width:120px; float:left; margin-right:10px;">
			                    Complaint Status
                               <asp:DropDownList id="ddlStatus" tabIndex="1" runat="server" CssClass="formField105">
                                          <asp:ListItem Text="Unprocessed" Value="0"></asp:ListItem>
                                          <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                          <asp:ListItem Text="Processed " Value="1"></asp:ListItem>                                          
                               </asp:DropDownList>
			                </div>    
			                <div class="formTxt" style="width:120px; float:left; text-align:left; margin-right:10px;">
			                    Complaint Type
			                   <asp:DropDownList id="ddlType" tabIndex="2" runat="server" CssClass="formField105"></asp:DropDownList>
			                </div>
                             <div class="formTxt" style="width:120px; float:left; text-align:left; margin-right:10px;">
			                    Complaint Ref.No.
			                  <asp:TextBox ID="txtComplaintRefNo" TabIndex="3" runat="server" CssClass="formField105"></asp:TextBox>
			                </div>   	   		               
			                <div class="formTxt" style="width:150px; float:left; ">
			                     Created From Date
			                <asp:TextBox ID="txtDateCreatedFrom" TabIndex="4" runat="server" CssClass="formField105"></asp:TextBox>
			                   <img alt="Click to Select" src="Images/calendar.gif" id="btnDateCreatedFrom" style="cursor:pointer; vertical-align:top;" />		    	
                               <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnDateCreatedFrom TargetControlID="txtDateCreatedFrom" ID="calDateCreatedFrom" runat="server"></cc1:CalendarExtender>
			                   <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regDateCreatedFrom" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateCreatedFrom" ErrorMessage="Created From Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
                               <asp:CustomValidator ID="rngDateCreated" CssClass="txtOrange" runat="server" ErrorMessage="Created To Date should be greater than Created From Date"  ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateCreatedTo">*</asp:CustomValidator>
			                </div>
                            <div class="formTxt" style="width:150px; float:left; ">
                                Created To Date
                             <asp:TextBox ID="txtDateCreatedTo" TabIndex="4" runat="server" CssClass="formField105"></asp:TextBox>
                                 <img alt="Click to Select" src="Images/calendar.gif" id="btnDateCreatedTo" style="cursor:pointer; vertical-align:top;" />		    	
                                 <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnDateCreatedTo TargetControlID="txtDateCreatedTo" ID="calDateCreatedTo" runat="server"></cc1:CalendarExtender>
                                 <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regDateCreatedTo" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateCreatedTo" ErrorMessage="Created To Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
                            </div>
                             <div class="formTxt" style="width:150px; float:left; ">
			                     Processed From Date
			                <asp:TextBox ID="txtDateProcessedFrom" TabIndex="5" runat="server" CssClass="formField105"></asp:TextBox>
			                   <img alt="Click to Select" src="Images/calendar.gif" id="btnDateProcessedFrom" style="cursor:pointer; vertical-align:top;" />		    	
                               <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnDateProcessedFrom TargetControlID="txtDateProcessedFrom" ID="calDateProcessedFrom" runat="server"></cc1:CalendarExtender>
			                   <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regDateProcessedFrom" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateProcessedFrom" ErrorMessage="Processed From Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
                               <asp:CustomValidator ID="rngDateProcessed" runat="server" CssClass="txtOrange" ErrorMessage="Processed To Date should be greater than Processed From Date" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateProcessedTo">*</asp:CustomValidator>
			                </div>
                             <div class="formTxt" style="width:150px; float:left; ">
			                     Processed To Date
			                <asp:TextBox ID="txtDateProcessedTo" TabIndex="5" runat="server" CssClass="formField105"></asp:TextBox>
			                   <img alt="Click to Select" src="Images/calendar.gif" id="btnDateProcessedTo" style="cursor:pointer; vertical-align:top;" />		    	
                               <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnDateProcessedTo TargetControlID="txtDateProcessedTo" ID="calDateProcessedTo" runat="server"></cc1:CalendarExtender>
			                   <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regDateProcessedTo" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateProcessedTo" ErrorMessage="Processed To Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
			                </div>
			            </div>
			        </div>
            
            </td>
                <td align="left" width="60px" valign="bottom">
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;">
                        <asp:LinkButton ID="btnView" causesValidation=False runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton></td>
                    </div>
                </td>
                 <td runat="server" id="tdExport" valign="bottom" visible="false">
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;width:100px;">
                        <a runat="server"  target="_blank" id="btnExport" class="txtButtonRed">Export to Excel</a>&nbsp;
                    </div>
                 </td>
            </tr>
      
      </table>

            <table >

            <tr>

            <td class="paddingL30 padB21" valign="top" padB21 paddingTop1>

            <asp:Label ID="lblMsg" runat="server" CssClass="bodytxtValidationMsg"></asp:Label>                

            </td>

            </tr>

            </table>                      

                        

                 

                                          

                  

                        

                               

                               

                  <hr style="background-color:#993366;height:5px;margin:0px;" />

                  

                    <asp:Panel ID="pnlListing" runat="server">    

                                <asp:GridView ID="gvComplaints"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'

                         BorderColor="White"  dataKeyNames="ComplaintId" PagerSettings-Mode=NextPreviousFirstLast   PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          

               <EmptyDataTemplate>

               <table  width="100%" border="0" cellpadding="10" cellspacing="0">                            

                                    <tr>

                                          <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.

                                          </td>

                                    </tr>                   

                              </table>

                  

               </EmptyDataTemplate>

               <PagerTemplate>

                   

                  

                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">

                                    <tr>

                                          <td runat="server" id="tdsetType1" width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">

                                            <TR>

                                                <TD class="txtListing" align="left"><strong>

                                                  <asp:CheckBox id="chkSelectAll" runat="server" onclick="CheckAll(this,'Check')" value="checkbox" Text="Select all" valign="middle"></asp:CheckBox>

                                                </strong> </TD>

                                                <TD width="5"></TD>

                                            </TR>

                                          </TABLE>                                  

                                          </td>

                                          

                                          <td runat="server"  id="tdProcessed" width="70" align="left" class="txtWelcome paddingL10"><div id="divButton" style="width: 90px;  ">

                                                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                                           

                                                <asp:LinkButton OnClick="ProcessedComplaint" class="buttonText" style="cursor:hand;" id="btnProcessed"  runat=server><strong>Processed</strong></asp:LinkButton>

                               <cc1:ConfirmButtonExtender ID="ConfirmBtnProcessed" TargetControlID="btnProcessed" ConfirmText="Do you want to mark these complaints as processed?" runat="server">

                                </cc1:ConfirmButtonExtender> 

                                

                                                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>

                                          </div></td>                                      

                                

                                          <td>

                                                <table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">

                                                  <tr>

                                                      <td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >

                                                            <TR>

                                                                  <td align="right" valign="middle">

                                                                    <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>

                                                                  </td>

                                                                  <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">

                                                                  <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                                                        <tr>

                                                                        <td align="right" width="36">

                                                                        <div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />

                                                                              </div>      

                                                                              <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />

                                                                              </div>                                                                                                                                             

                                                                        </td>

                                                                        <td width="50" align="center" style="margin-right:3px; ">                                                                              

                                                                              <asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">

                                                    </asp:DropDownList>

                                                                        </td>

                                                                        <td width="30" valign="bottom">

                                                                              <div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />

                                                                              </div>

                                                                              <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />

                                                                              </div>

                                                                        </td>

                                                                        </tr>

                                                                  </table>

                                                                  </td>

                                                            </TR>

                                                      </TABLE></td>

                                                  </tr>

                                                </table>

                                      </td>

                                    </tr>

                              </table> 

                   

        </PagerTemplate> 

               <Columns>  

               
               <asp:TemplateField>               

               <HeaderStyle CssClass="gridHdr" />

               <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="gridRow"/>

               <ItemTemplate>             

               <asp:CheckBox ID="Check" runat="server" ></asp:CheckBox>

                <input type=hidden runat=server id="hdnComplaintId"  value='<%#Container.DataItem("ComplaintId")%>'/>

               </ItemTemplate>

               </asp:TemplateField>                

                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="5%" HeaderStyle-CssClass="gridHdr gridText"  DataField="ComplaintRefNo" SortExpression="ComplaintRefNo" HeaderText="Complaint Ref.No." />            
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="5%" HeaderStyle-CssClass="gridHdr gridText"  DataField="SalesAgentName" SortExpression="SalesAgentName" HeaderText="Sales Agent" />            
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="SalesAgentMainPhone" SortExpression="SalesAgentMainPhone" HeaderText="Sales Agent Main Phone" />            
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="5%" HeaderStyle-CssClass="gridHdr gridText"  DataField="BranchID" SortExpression="BranchID" HeaderText="Branch ID" />            
				<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="CustomerName" SortExpression="CustomerName" HeaderText="Customer Name"/> 
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="CustomerPostCode" SortExpression="CustomerPostCode" HeaderText="Customer Post Code"/> 
                 <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="15%" HeaderStyle-CssClass="gridHdr gridText"  DataField="Complaint" SortExpression="Complaint" HeaderText="Complaint"/> 
                <asp:TemplateField HeaderText="IsProcessed" HeaderStyle-CssClass= "gridHdr gridText" SortExpression="IsProcessed"  >         
                  <HeaderStyle  CssClass="gridHdr gridText"  />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" Width="5%"/>
                    <ItemTemplate>             
                   <asp:Label ID="lblIsProcessed" runat="server" Text='<%# IIF(Container.DataItem("IsProcessed"), True,False)%>'>></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>                 
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="ComplaintType" HeaderText="Complaint Type" SortExpression="ComplaintType"/> 
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="DateCreated" HeaderText="Date Created" SortExpression="DateCreated"/> 
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="DateProcessed" HeaderText="Date Processed" SortExpression="DateProcessed"/> 
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="5%" HeaderStyle-CssClass="gridHdr gridText"  DataField="HoursDifference" HeaderText="Hours Difference" SortExpression="HoursDifference"/> 
                        

                </Columns>

                 <AlternatingRowStyle CssClass=gridRow />

                 <RowStyle CssClass=gridRow />            

                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />

                <HeaderStyle  CssClass="gridHdr" HorizontalAlign=Left VerticalAlign=Top  Height="40px" />

                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />

        

              </asp:GridView>

              </asp:Panel>

      

              

                         </ContentTemplate>

                         <Triggers>

                            <asp:PostBackTrigger   ControlID="btnView"   />                       

                         </Triggers>

            

            </asp:UpdatePanel>

            

                <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">

                <ProgressTemplate >

                        <div class="gridText">

                               <img  align=middle src="Images/indicator.gif" />

                               <b>Fetching Data... Please Wait</b>

                        </div>   

                        </ProgressTemplate>

                </asp:UpdateProgress>

                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />

             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"

                SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.DixonsComplaints" SortParameterName="sortExpression">                

            </asp:ObjectDataSource>

                                            

                              

            <!-- InstanceEndEditable --></td>

                     

             

         </tr>

         </table>

      </div>
<cc1:ModalPopupExtender ID="mdlComplaintType" runat="server" TargetControlID="btnTemp" PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
<asp:Panel runat="server" ID="pnlConfirm" Width="300px" CssClass="pnlConfirm" style="display:none;">
    <div id="divModalParent">
        <span style="font-family:Tahoma; font-size:14px;"><b>Complaint Type</b></span><br /><br />
        <asp:DropDownList id="ddComplaintType" tabIndex="1" runat="server" CssClass="bodyTextGreyLeftAligned" Width="280"></asp:DropDownList>      
        <br /><br />
            <div id="divButton" class="divButton" style="width: 85px; float:left;cursor:pointer;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                    <a class="buttonText cursorHand" href='javascript:validate_required("ctl00_ContentPlaceHolder1_ddComplaintType")' id="ancSubmit"><strong>Submit</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div>
            <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;cursor:pointer;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>								
</asp:Panel>
<asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
<asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

<script type="text/javascript">
    function validate_required(field) {
       // alert(document.getElementById(field).options[document.getElementById(field).selectedIndex].value);
        if (document.getElementById(field).options[document.getElementById(field).selectedIndex].value == 0) {
            
            alert("Please select Complaint Type")
        }
        else {
            document.getElementById("ctl00_ContentPlaceHolder1_hdnButton").click();
        }      
    }
    
</script>


</asp:Content>
