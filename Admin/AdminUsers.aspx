<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AdminUsers.aspx.vb" Inherits="Admin.AdminUsers" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :"%>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
    
    
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>






	        
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			 <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
             <ContentTemplate>
			 
			 <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10px" height="30px" valign="bottom"></td>
			<td width="150px"><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Admin Site Users" runat="server"></asp:Label></td>
			<td allign="left" width="150">
                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:120px;">
                    <asp:LinkButton ID="lnkCreateAdminUser" causesValidation=False OnClick="lnkCreateAdminUser_Click" runat="server" CssClass="txtButtonRed" Text="Create Admin User"></asp:LinkButton>
                </div>
             </td>
             <td allign="left"><asp:CheckBox ID="chkHideDeleted" Checked="true" runat="server" Text='Hide deleted users' class="formTxt"  AutoPostBack="true" /> </td>
			</tr>
			</table>
			              
                               
			<hr style="background-color:#993366;height:5px;margin:0px;" />
			
			
           	<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td><asp:GridView ID="gvSiteUsers"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>' BorderColor="White"  
               PagerSettings-Mode=NextPreviousFirstLast  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               
               <PagerTemplate>
                   
                  
                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>
							<td runat="server" id="tdsetType1" width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">
							  <TR>
								<TD class="txtListing" align="left"> 
							
							
								</TD>
								<TD width="5"></TD>
							  </TR>
							</TABLE>						
							</td>
							
							
							<td width="10">&nbsp;</td>
						
						  
						  <td id="tdsetType2" align="left" width="121">
								
							</td>
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
        </PagerTemplate>
               <Columns>  
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="8%" HeaderStyle-CssClass="gridHdr gridText"  DataField="FirstName" SortExpression="FirstName" HeaderText="First Name" />            
				<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="8%" HeaderStyle-CssClass="gridHdr gridText"  DataField="LastName" SortExpression="LastName" HeaderText="Last Name"/> 
				<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="14%" HeaderStyle-CssClass="gridHdr gridText"  DataField="UserName" SortExpression="UserName" HeaderText="User Name" />            
				<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="RoleGroupName" SortExpression="RoleGroupName" HeaderText="Role Group"/> 
				<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="EnableLogin" SortExpression="EnableLogin" HeaderText="Enable Login"/> 
				<asp:TemplateField SortExpression="LastLogin" HeaderText="Last Login" HeaderStyle-CssClass="gridHdr gridText" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="12%"/>
                <ItemTemplate>             
                    <%#  chkForNull(Container.DataItem("LastLogin"))%>
				</ItemTemplate>
                </asp:TemplateField> 	    
                <asp:TemplateField ItemStyle-Width="6%">               
                <HeaderStyle  CssClass="gridHdr" />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow"/>
                    <ItemTemplate>             
                   <%#GetLinks(Container.DataItem("UserName"), Container.DataItem("FirstName"), Container.DataItem("LastName"), Container.DataItem("RoleGroupName"), Container.DataItem("ContactID"), Container.DataItem("EnableLogin"), Container.DataItem("MainCat"), Container.DataItem("RoleGroupID"), Container.DataItem("MenuID"))%>
                    </ItemTemplate>
                </asp:TemplateField>  
				</Columns>
               
                 <AlternatingRowStyle    CssClass=gridRow />
                 <RowStyle CssClass=gridRow />            
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
        
              </asp:GridView>
				 </td>
            </tr>
            </table>
				
				 <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.AdminUsers" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
  
          </ContentTemplate>
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>

</asp:Content>
