 <%@ Page Language="vb" AutoEventWireup="false" Codebehind="FreesatReport.aspx.vb" Inherits="Admin.FreesatReport" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Freesat Report"%>
    
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>   

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
 

			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
						<td>&nbsp;</td>
							<td class="HeadingRed" ><strong>Freesat Report</strong></td>
						</tr>						
					  </table>

					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					  	<tr >
							<td width="10"></td>							
							<td width="5"></td>
							 <td align="left" width="370" valign="top">
								<TABLE cellSpacing="0" cellPadding="0" width="350" border="0" >
									<TR>
										<td width="350" style="padding-top:5px; ">
											<%@ register src="~/UserControls/UK/UCDateRangeUK.ascx" tagname="UCDateRange" tagprefix="uc1" %>
											<uc1:UCDateRange id="UCDateRange1" runat="server"></uc1:UCDateRange> 
									 </td>
									</TR>
								</TABLE>
							 </td>							 
							 <td><asp:CheckBox ID="chkToDate" runat="server" CausesValidation="false" text="To Date" TextAlign="Right" class="formTxt" valign="top" height="24"/></td>
							<td width="20"></td>							
							<td align="left">
								<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                    <asp:LinkButton ID="lnkBtnGenerateReport" causesValidation=True runat="server" CssClass="txtButtonRed" Text="Generate Report"></asp:LinkButton>
                                </div>
							 </td>
							 <td width="20" align="left" ></td>							 

						</tr>
					</table>
					<table>
					  <tr>
						<td align=center >
							 <rsweb:ReportViewer ID="ReportViewerOW" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="600px" ProcessingMode="Remote" Width="100%" bgColor="#FFFFFF">
									<ServerReport ReportServerUrl="" />
							 </rsweb:ReportViewer>
						</td>
					  </tr>						  
					  </table>
			<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>

 </asp:Content>
