<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminClientSLAReport.aspx.vb" Inherits="Admin.AdminClientSLAReport" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : BestBuy SLA Report"%>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>





			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			    <div style="margin-left:20px;" class="formTxt">
              <p class="paddingB4 HeadingRed"><strong>Best Buy SLA Report</strong> </p>
              <input id="hdnCompID" name="hdnCompID" type="hidden" runat="server">
<input type="hidden" id="hdnStartDate" runat="server" value="" />
<input id="hdnToDate" name="hdnToDate" type="hidden" runat="server"> 

	<div id="divDateRange" style="padding-top:10px;" runat="server" >
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
          <%@ Register TagPrefix="uc1" TagName="UCDateRange" Src="~/UserControls/UK/UCDateRangeUK.ascx" %>
          <uc1:UCDateRange id="UCDateRange1" runat="server"></uc1:UCDateRange> 	
	    </td>
	    <td width="10px">&nbsp;</td>
	     <td>
	         <table border="0"  cellpadding="0" cellspacing="0">
                  <tr>
	    <td align="left" width="60px">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                <asp:LinkButton ID="lnkView" causesValidation="true"  runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
            </div>
		     </td>
	    <td width="10px">&nbsp;</td>
	        <td align="left" id="TdPrint" runat="server">
                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                    <a runat="server" target="_blank" id="btnPrint" class="txtButtonRed" style="cursor:pointer;" >&nbsp;Print&nbsp;</a>
                </div>
		    </td>
		    <td width="10px">&nbsp;</td>
		     <td align="left" id="TdExpToExc" runat="server">
                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:120px;">
                    <a runat="server" target="_blank" id="btnExport" class="txtButtonRed" style="cursor:pointer;" >&nbsp;Export to Excel&nbsp;</a>
                </div>
		    </td>
	    </tr>
             </table>  
	     </td>
	</tr>
    </table>  
	<div id="divClearBoth">    </div>
      <div id="divMainReport" runat="server" style="padding-left:10px;">
         <p class="paddingB4 HeadingRed"><strong>Best Buy SLA Report</strong> </p>
             <table cellpadding="0" cellspacing="0" >
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td width="200">Total Submitted Work Orders</td>
                   <td width="20">:</td>
                   <td width="50"><asp:Label ID="lblSubmittedWO" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total Closed Work Orders</td>
                   <td>:</td>
                   <td><asp:Label ID="lblClosedWO" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total Work Orders that met SLA</td>
                   <td>:</td>
                   <td><asp:Label ID="lblOnTime" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total SLA Met (%)</td>
                   <td>:</td>
                   <td><asp:Label ID="lblSLAPercentageMet" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>1st Time Success (%)</td>
                   <td>:</td>
                   <td><asp:Label ID="lblFirstTimeSuccess" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>No. of Cancellations</td>
                   <td>:</td>
                   <td><asp:Label ID="lblCancellationNo" runat="server"></asp:Label></td>
                  </tr>                  
              </table>                              
         <div runat="server" id="divProduct" style="padding-top:20px;">
          <p class="paddingB4 HeadingRed"><strong>By SKU</strong> </p>
            <div >
             <asp:datalist id="DLProducts" RepeatColumns="2" runat="server">           
              <ItemTemplate>
                <table cellpadding="0" cellspacing="0" >
                <tr>
                 <td colspan="3"><b><%#Container.DataItem("ProductName")%></b></td>
                </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td width="200">Total jobs submitted</td>
                   <td width="20">:</td>
                   <td width="50"><%#Container.DataItem("SubmittedWO")%></td>
                   <td width="150">&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs closed</td>
                   <td>:</td>
                   <td><%#Container.DataItem("ClosedWO")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs within SLA (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("SLAPercentageMet")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total 1st time success (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("FirstTimeSuccess")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Cancelled jobs</td>
                   <td>:</td>
                   <td><%#Container.DataItem("CancellationNumber")%></td>
                   <td>&nbsp;</td>
                  </tr>
                    <tr>
                      <td colspan="4">&nbsp;</td>
                    </tr>
                </table>                  
            </ItemTemplate>
          </asp:datalist>  
          </div>        
              <div class="divClearBoth"></div>   
            </div>           
            
           <div runat="server" id="divBillingLoc">
           <p class="paddingB4 HeadingRed"><strong>By Store</strong> </p>            
              <div>
                 <asp:datalist id="DLBillingLoc" RepeatColumns="3" runat="server">           
              <ItemTemplate>
                <table cellpadding="0" cellspacing="0">
                  <tr>
                     <td colspan="3"><b><%#Container.DataItem("BillingLocation")%></b></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td width="200">Total jobs submitted</td>
                   <td width="20">:</td>
                   <td width="50"><%#Container.DataItem("SubmittedWO")%></td>
                   <td width="150">&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs closed</td>
                   <td>:</td>
                   <td><%#Container.DataItem("ClosedWO")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs within SLA (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("SLAPercentageMet")%></td>
                   <td>&nbsp;</td>                   
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total 1st time success (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("FirstTimeSuccess")%></td>
                   <td>&nbsp;</td>                 
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Cancelled jobs</td>
                   <td>:</td>
                   <td><%#Container.DataItem("CancellationNumber")%></td>
                   <td>&nbsp;</td>                  
                  </tr>
                   <tr>
                      <td colspan="4">&nbsp;</td>
                    </tr>
                </table>                  
             </ItemTemplate>
            </asp:datalist>  
              </div>        
              <div class="divClearBoth"></div>   
          </div>  
          
            
        </div>
        
     
         <table id="tblNoRecords" runat="server" width="100%" border="0" cellpadding="10" cellspacing="0">					
	    <tr>
		    <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.</td>
		</tr>				
		</table>
	</div>
	<div id="divBtmBand"></div>
              </div>
			   
			    
			    
              <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>

</asp:Content>
