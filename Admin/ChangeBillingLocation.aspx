<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ChangeBillingLocation.aspx.vb" Inherits="Admin.ChangeBillingLocation" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Change Billing Location"%>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<!-- InstanceBeginEditable name="head" -->
<style>
#divMenu {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	line-height: 12px;
	font-weight: normal;
	text-decoration: none;
	width: 100%;
	background-position: right top;
	background-repeat: no-repeat;
	border-top-width: 1px;
	border-top-style: solid; 
	border-top-color: #CECECE;
	background-color: #993366;
}

.divContentTopRed{
	width: 100%;
	background-color: #993366;
	background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
	height: 5px;
	
}
</style>
<!-- InstanceEndEditable --><!-- InstanceParam name="opAccountSumm" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddUser" type="boolean" value="false" --><!-- InstanceParam name="OpRgCLAddLOcation" type="boolean" value="false" --><!-- InstanceParam name="OPWOSummary" type="boolean" value="false" --><!-- InstanceParam name="RgOpBtnNewWO" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddFunds" type="boolean" value="false" --><!-- InstanceParam name="opMenuRegion" type="boolean" value="true" --><!-- InstanceParam name="opContentTopRedCurve" type="boolean" value="false" --><!-- InstanceParam name="opUsersProfile" type="boolean" value="false" --><!-- InstanceParam name="opCompanyProfile" type="boolean" value="true" --><!-- InstanceParam name="opUsersListing" type="boolean" value="false" --><!-- InstanceParam name="opLocations" type="boolean" value="false" --><!-- InstanceParam name="opReferences" type="boolean" value="false" --><!-- InstanceParam name="opPreferences" type="boolean" value="false" --><!-- InstanceParam name="opUsers" type="boolean" value="false" --><!-- InstanceParam name="width" type="text" value="985" --><!-- InstanceParam name="opComments" type="boolean" value="false" --><!-- InstanceParam name="OpRgScriptManager" type="boolean" value="true" --><!-- InstanceParam name="OpFavSuppliers" type="boolean" value="false" --><!-- InstanceParam name="OpFavSuppliersLink" type="boolean" value="false" -->
<div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            <div style="margin-left:20px;" class="formTxt">
              <p class="paddingB4 HeadingRed"><strong>Change Billing Location</strong> </p>
               <div style="margin-bottom:20px;">
                   <div>
                        <asp:Label ID="lblBillingLocation" runat="server" Text="Billing Location "></asp:Label>
                   </div>
                   <div>
                        <asp:DropDownList ID="drpdwnBillingLocation" runat="server" Visible="False" CssClass="formFieldGrey" Width="220" DataTextField="Name" DataValueField="AddressID">
                        </asp:DropDownList>  
                   </div>
               </div>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
           </div>          
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      </asp:Content>