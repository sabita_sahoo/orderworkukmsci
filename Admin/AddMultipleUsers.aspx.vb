
Partial Public Class AddMultipleUsers
    Inherits System.Web.UI.Page
    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs
    Private _cntUsers As Integer
    Public Property cntUsers() As Integer
        Get
            Return _cntUsers
        End Get
        Set(ByVal value As Integer)
            _cntUsers = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not IsNothing(Request.QueryString("users")) Then
                cntUsers = CInt(Request.QueryString("users"))
                PopulateRepeater(cntUsers)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Populating Locations and Usertype in dropdown
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rptUsers_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptUsers.ItemDataBound
        CommonFunctions.PopulateUserType(Page, DirectCast(e.Item.FindControl("drpdwnUserType"), DropDownList), Session(Request("companyid") & "_ContactClassID"), "mode")
        CommonFunctions.PopulateLocationsofCompany(Page, DirectCast(e.Item.FindControl("drpdwnLocation"), DropDownList), Request("companyID"))
        'security questions
        CommonFunctions.PopulateSecurityQues(Page, DirectCast(e.Item.FindControl("drpdwnSecQue"), DropDownList))
    End Sub

#Region "Action Buttons"
    ''' <summary>
    ''' Handles button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub PopulateRepeater(ByVal cntUsers As Integer)
        If CInt(cntUsers) > 1 Then
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dCol As New DataColumn
            dCol.DataType = Type.[GetType]("System.String")
            dCol.ColumnName = "ContactId"
            dt.Columns.Add(dCol)
            Dim totaluser, i As Integer
            totaluser = CInt(cntUsers)
            For i = 0 To totaluser - 1
                Dim drow As DataRow
                drow = dt.NewRow()
                drow("ContactId") = (i + 1).ToString
                dt.Rows.Add(drow)
                dt.AcceptChanges()
            Next i
            ds.Tables.Add(dt)
            ds.AcceptChanges()
            rptUsers.DataSource = ds
            rptUsers.DataBind()
        Else
            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            Response.Redirect("Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
        End If

    End Sub

    ''' <summary>
    ''' Cancel button click on the second screen i.e. where admin can enter users information
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnUersCancel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUersCancel.ServerClick
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        Response.Redirect("Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
    End Sub

    ''' <summary>
    ''' Cancel button click on the third screen i.e. confirmation screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnConfCancel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfCancel.ServerClick
        tblconfConfirm.Visible = False
        tblconfSubmit.Visible = True
        tblConfCancel.Visible = False
        tblUserCancel.Visible = True
        lblTopHeading.Text = "Insert user details to be added"
        Dim i As Integer = 0
        For Each row As RepeaterItem In rptUsers.Items
            CType(row.FindControl("txtbxFName"), TextBox).Enabled = True
            CType(row.FindControl("txtbxLName"), TextBox).Enabled = True
            CType(row.FindControl("txtbxEmailAddr"), TextBox).Enabled = True
            CType(row.FindControl("txtPhone"), TextBox).Enabled = True
            CType(row.FindControl("drpdwnLocation"), DropDownList).Enabled = True
            CType(row.FindControl("drpdwnUserType"), DropDownList).Enabled = True
            CType(row.FindControl("drpdwnSecQue"), DropDownList).Enabled = True
            CType(row.FindControl("txtbxSecAns"), TextBox).Enabled = True
            CType(row.FindControl("chkbxEnableLogin"), CheckBox).Enabled = True
            If CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue = ApplicationSettings.RoleClientDepotID Or CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue = ApplicationSettings.RoleSupplierDepotID Then
                Dim id As String
                id = CType(row.FindControl("drpdwnUserType"), DropDownList).ClientID
                ScriptManager.RegisterStartupScript(Me, Me.GetType, id.ToString(), "javascript:disableLogin('" & id & "');", True)

            End If


        Next
    End Sub

    ''' <summary>
    ''' Handles submit click on second screen i.e. the screen where admin can enter user information to be saved
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSubmit_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick
        If ValidatingUsers() Then
            lblTopHeading.Text = "Please confirm user details to be submitted"
            tblconfConfirm.Visible = True
            tblconfSubmit.Visible = False
            tblConfCancel.Visible = True
            tblUserCancel.Visible = False
            Dim i As Integer = 0
            For Each row As RepeaterItem In rptUsers.Items
                CType(row.FindControl("txtbxFName"), TextBox).Enabled = False
                CType(row.FindControl("txtbxLName"), TextBox).Enabled = False
                CType(row.FindControl("txtbxEmailAddr"), TextBox).Enabled = False
                CType(row.FindControl("txtPhone"), TextBox).Enabled = False
                CType(row.FindControl("drpdwnLocation"), DropDownList).Enabled = False
                CType(row.FindControl("drpdwnUserType"), DropDownList).Enabled = False
                CType(row.FindControl("drpdwnSecQue"), DropDownList).Enabled = False
                CType(row.FindControl("txtbxSecAns"), TextBox).Enabled = False
                CType(row.FindControl("chkbxEnableLogin"), CheckBox).Enabled = False
            Next
        End If
    End Sub

    ''' <summary>
    ''' Handles click on Confirm button on third screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Dim xmlContent As String = SaveProfile().GetXml
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)
        Dim ds As New DataSet
        ds = ws.WSContact.AddMultipleSpecialist(xmlContent, 0, Session("UserID"))
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If CStr(ds.Tables(0).Rows(0)(0)) = "1" Then
                    sendMailToUser()
                    'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                    Response.Redirect("Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Handles submit button click on first screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub lnkbtnEnter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnEnter.Click
        Page.Validate()
        If IsValid Then
            divValidationMain.Style.Add("display", "none")
            pnlMultipleUser.Visible = False
            pnlInpUsers.Visible = True
            PopulateRepeater(CInt(txtbxNoUsers.Text))
        Else
            divValidationMain.Style.Add("display", "block")
            For Each row As RepeaterItem In rptUsers.Items
                If CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue = ApplicationSettings.RoleClientDepotID Or CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue = ApplicationSettings.RoleSupplierDepotID Then
                    Dim id As String
                    id = CType(row.FindControl("drpdwnUserType"), DropDownList).ClientID
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "js", "javascript:disableLogin('" & id & "');", True)
                End If
            Next
        End If
    End Sub

#End Region

#Region "Validation Functions"
    ''' <summary>
    ''' Check if email already exist in system; if yes then show appropriate message
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckNewEmailExist(ByVal emailid As String) As Boolean
        Dim loginexists As Boolean = False
        If emailid <> "" Then
            If ws.WSContact.DoesLoginExist(emailid) Then
                If emailid <> Session("EmailAddr") Then
                    loginexists = True
                End If
            End If
        End If
        Return loginexists
    End Function

    ''' <summary>
    ''' Function to validate entered users information
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidatingUsers() As Boolean
        Dim flag As Boolean = True
        Dim errmsg As String = ""
        Dim ArrEmail(10) As String
        Dim i As Integer = 0
        Dim j As Integer = 0
        If Not IsNothing(rptUsers) Then

            For Each row As RepeaterItem In rptUsers.Items
                i = i + 1
                'Validating First name is not blank
                If CType(row.FindControl("txtbxFName"), TextBox).Text.Trim.Length = 0 Then
                    If flag = True Then
                        errmsg = "- Please Enter First Name for User " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Enter First Name for User " & i
                    End If
                End If
                'Validating Last name is not blank
                If CType(row.FindControl("txtbxLName"), TextBox).Text.Trim.Length = 0 Then
                    If flag = True Then
                        errmsg = "- Please Enter Last Name for User " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Enter Last Name for User " & i
                    End If
                End If
                'Validating email address is not blank
                If CType(row.FindControl("txtbxEmailAddr"), TextBox).Text.Trim.Length = 0 Then
                    If (CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue = ApplicationSettings.SupplierRoleGroups.Specialist) Then
                        If CType(row.FindControl("chkbxEnableLogin"), CheckBox).Checked = True Then
                            If flag = True Then
                                errmsg = "- Please Enter Email Address for User " & i
                                flag = False
                            Else
                                errmsg = errmsg & "<br>- Please Enter Email Address for User " & i
                            End If
                        End If
                    Else
                        If flag = True Then
                            errmsg = "- Please Enter Email Address for User " & i
                            flag = False
                        Else
                            errmsg = errmsg & "<br>- Please Enter Email Address for User " & i
                        End If
                    End If


                    'Validating email address in proper format
                ElseIf Not (Regex.IsMatch(CType(row.FindControl("txtbxEmailAddr"), TextBox).Text.Trim, "^.+@[^\.].*\.[a-z]{2,}$")) Then
                    If flag = True Then
                        errmsg = "- Please Enter Email Address for User " & i & " in proper format"
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Enter Email Address for User " & i & " in proper format"
                    End If
                    'Validating email address is not duplicate
                ElseIf (CheckNewEmailExist(CType(row.FindControl("txtbxEmailAddr"), TextBox).Text.Trim)) Then
                    If flag = True Then
                        errmsg = "- Sorry, the Email address entered for User " & i & "  already exists in our records.  Please provide another email address. If you have any queries contact us at <a class='footerTxtSelected' href='mailto:info@orderwork.co.uk'>info@orderwork.co.uk</a>"
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Sorry, the Email address entered for User " & i & "  already exists in our records.  Please provide another email address. If you have any queries contact us at <a class='footerTxtSelected' href='mailto:info@orderwork.co.uk'>info@orderwork.co.uk</a>"
                    End If
                End If
                'Validating selection og Location
                If CType(row.FindControl("drpdwnLocation"), DropDownList).SelectedValue = 0 Then
                    If flag = True Then
                        errmsg = "- Please select a Location for User " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please select a Location for User " & i
                    End If
                End If
                'Validating selection of Security Question
                If CType(row.FindControl("drpdwnSecQue"), DropDownList).SelectedValue = "" Then
                    If flag = True Then
                        errmsg = "- Please select a Security Question for User " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please select a Security Question for User " & i
                    End If
                End If
                'Validating Security Answer is not blank
                If CType(row.FindControl("txtbxSecAns"), TextBox).Text.Trim.Length = 0 Then
                    If flag = True Then
                        errmsg = "- Please Enter Security Answer for User " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Enter Security Answer for User " & i
                    End If
                End If
                'Validating User role
                If CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue = 0 Then
                    If flag = True Then
                        errmsg = "- Please select a User type for User " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please select a User type for User " & i
                    End If
                    'Validating User role should not be an Admin
                ElseIf CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue.ToString = "1" Or CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue.ToString = "4" Then
                    If flag = True Then
                        errmsg = "- Please change User type for User " & i & " as this feature is not supported"
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please change User type for User " & i & " as this feature is not supported"
                    End If
                End If
                If CheckDepotLocation(CInt(CType(row.FindControl("drpdwnLocation"), DropDownList).SelectedValue), CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue.ToString, i) Then
                    If flag = True Then
                        errmsg = ViewState("errmsg")
                        flag = False
                    Else
                        errmsg = errmsg & "<br>" & ViewState("errmsg")
                    End If

                End If

                ArrEmail(j) = CType(row.FindControl("txtbxEmailAddr"), TextBox).Text.Trim
                j = j + 1
            Next
        End If
        Dim EmailFlag As Boolean = False
        Dim l As Integer
        Dim k As Integer
        For k = 0 To i - 2
            For l = 1 To i - 1
                If (l <> k) Then
                    If ((ArrEmail(k) <> "") And (ArrEmail(l) <> "")) Then
                        If (ArrEmail(k) = ArrEmail(l)) Then
                            EmailFlag = True
                            Exit For
                        End If
                    End If
                End If
            Next
            l = l + 1
        Next
        If EmailFlag Then
            If flag = True Then
                errmsg = "- Email addresses should not be duplicated."
                flag = False
            Else
                errmsg = errmsg & "<br>- Email addresses should not be duplicated."
            End If
        End If


        If Not flag Then
            divValidationMain.Style("display") = "block"
            lblError.Text = errmsg.ToString
            For Each row As RepeaterItem In rptUsers.Items
                If CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue = ApplicationSettings.RoleClientDepotID Or CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue = ApplicationSettings.RoleSupplierDepotID Then
                    Dim id As String
                    id = CType(row.FindControl("drpdwnUserType"), DropDownList).ClientID
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, id.ToString(), "javascript:disableLogin('" & id & "');", True)
                End If
            Next
        Else
            divValidationMain.Style("display") = "none"
            lblError.Text = ""
        End If
        ViewState.Remove("cntDepotLocation")
        ViewState.Remove("DepotLocation")
        Return flag
    End Function

    ''' <summary>
    ''' Checking Depot Location
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckDepotLocation(ByVal addressid As Integer, ByVal userType As String, ByVal i As Integer) As Boolean
        Dim IsDepot As Boolean
        Dim errmsg As String = ""
        IsDepot = False

        Dim ds As New DataSet
        ds = ws.WSContact.CheckDepotLocation(CType(addressid, Integer))
        If ds.Tables(0).Rows.Count > 0 Then
            'If selected location is a depot location
            If ds.Tables(0).Rows(0)("IsDepot") = True Then
                'If it has user assigned
                If ds.Tables(1).Rows.Count > 0 Then
                    'If the user is not the same who is viewing
                    If ds.Tables(1).Rows(0)("ContactLinkId").ToString <> "" And userType <> ApplicationSettings.RoleClientDepotID And userType <> ApplicationSettings.RoleSupplierDepotID Then
                        IsDepot = True
                        errmsg = "- A user with the role 'Depot' can only be assigned to the selected location for User " & i & ". Please re-assign to another location."
                    End If
                Else
                    'If not then check if the user is not depot
                    If userType <> ApplicationSettings.RoleClientDepotID And userType <> ApplicationSettings.RoleSupplierDepotID Then
                        IsDepot = True
                        errmsg = "- A user with the role 'Depot' can only be assigned to the selected location for User " & i & ". Please re-assign to another location."
                    End If
                End If
                If IsNothing(ViewState("DepotLocation")) Then
                    ViewState("DepotLocation") = addressid
                    ViewState("cntDepotLocation") = 1
                Else
                    If ViewState("DepotLocation").ToString.Contains(addressid) Then
                        If Not IsNothing(ViewState("cntDepotLocation")) Then
                            ViewState("cntDepotLocation") = CInt(ViewState("cntDepotLocation")) + 1
                        Else
                            ViewState("cntDepotLocation") = 1
                        End If
                    Else
                        ViewState("DepotLocation") = ViewState("DepotLocation") & ", " & addressid
                    End If
                End If
            Else
                If userType = ApplicationSettings.RoleClientDepotID Or userType = ApplicationSettings.RoleSupplierDepotID Then
                    IsDepot = True
                    'errmsg = "it should have a location tagged as a Depot."
                    errmsg = "- Only a depot location can be assigned for User " & i & ". Please re-assign to a Depot location."
                End If
            End If
        End If
        'If CInt(ViewState("cntDepotLocation")) > 1 Then
        '    IsDepot = True
        '    errmsg = "- Depot Location are used multiple times. Please make sure that a Depot location can be associated with a Single Depot User only. Check user " & i & "."
        'End If
        ViewState("errmsg") = errmsg.ToString
        Return IsDepot
    End Function

#End Region

#Region "Saving of User information and send mail function"
    ''' <summary>
    ''' Creating xsd for saving user information
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function SaveProfile() As XSDContacts
        Dim dsContacts As New XSDContacts
        If Not IsNothing(rptUsers) Then
            For Each row As RepeaterItem In rptUsers.Items
                Dim contactid = CType(row.FindControl("hdnContactID"), HtmlInputHidden).Value
                Dim AddressID As Integer = CType(row.FindControl("drpdwnLocation"), DropDownList).SelectedValue
                Dim strAppend As String
                Select Case Request("bizDivId")
                    Case ApplicationSettings.OWUKBizDivId
                        strAppend = "OW"
                    Case ApplicationSettings.SFUKBizDivId
                        strAppend = "MS"
                    Case ApplicationSettings.OWDEBizDivId
                        strAppend = "DE"
                End Select

                Dim nrow As XSDContacts.ContactsRow = dsContacts.Contacts.NewRow
                With nrow
                    .ContactID = contactid
                    .IsCompany = False
                    .Src = "OrderWork"
                    .IsDeleted = False
                    .Fname = CType(row.FindControl("txtbxFName"), TextBox).Text.Trim
                    .Lname = CType(row.FindControl("txtbxLName"), TextBox).Text.Trim
                    .Email = CType(row.FindControl("txtbxEmailAddr"), TextBox).Text.Trim
                    .Phone = CType(row.FindControl("txtPhone"), TextBox).Text.Trim
                    .MainContactID = Request("companyID")
                End With
                dsContacts.Contacts.Rows.Add(nrow)

                'For updating tblClassesLinkage
                Dim nrowClass As XSDContacts.ContactsSetupRow = dsContacts.ContactsSetup.NewRow
                With nrowClass
                    .BizDivID = Request("BizDivId")
                    .ClassID = Session(Request("companyid") & "_ContactClassID")
                    .ContactID = contactid
                    .Status = 1
                End With
                dsContacts.ContactsSetup.Rows.Add(nrowClass)

                'For updating tblRolesContactLinkage
                Dim nrowRClink As XSDContacts.tblRolesContactLinkageRow = dsContacts.tblRolesContactLinkage.NewRow
                With nrowRClink
                    .ContactID = contactid
                    .BizDivID = Request("BizDivId")
                    .RoleGroupID = CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedValue
                    .IsDefault = 0
                    .IsActive = 0
                    .AddressID = AddressID
                End With
                dsContacts.tblRolesContactLinkage.Rows.Add(nrowRClink)

                Dim nrowCL As XSDContacts.tblContactsLoginRow = dsContacts.tblContactsLogin.NewRow
                With nrowCL
                    .ContactID = contactid
                    .UserName = CType(row.FindControl("txtbxEmailAddr"), TextBox).Text.Trim
                    Dim strpass As String = System.Guid.NewGuid().ToString().Substring(0, 8)

                    If (CType(row.FindControl("chkbxEnableLogin"), CheckBox).Checked) = True Then
                        .Password = Encryption.EncryptToMD5Hash(strpass)
                        ViewState("ContactPassword") = strpass
                    End If
                    .EmailVerified = False
                    .NewsletterStatus = False
                    .SecurityQues = CInt(CType(row.FindControl("drpdwnSecQue"), DropDownList).SelectedValue)
                    .SecurityAns = CType(row.FindControl("txtbxSecAns"), TextBox).Text.Trim
                    .EnableLogin = CType(row.FindControl("chkbxEnableLogin"), CheckBox).Checked
                End With
                dsContacts.tblContactsLogin.Rows.Add(nrowCL)

                'Saving Action log entry

                Dim nrowALog As XSDContacts.tblActionLogRow = dsContacts.tblActionLog.NewRow
                With nrowALog
                    .LogID = contactid
                    .BizDivID = Request("BizDivId")
                    .ActionID = ApplicationSettings.ActionLogID.Activate
                    .ActionByCompany = Request("companyID")
                    .ActionByContact = Session(Request("companyid") & "_ContactUserID")
                    .ActionForCompany = Request("companyID")
                    .ActionForContact = ViewState("ContactID")
                    .ActionRef = 0
                    .ActionSrc = ""
                    ._Date = System.DateTime.Now()

                End With
                dsContacts.tblActionLog.Rows.Add(nrowALog)
            Next
        End If
        Return dsContacts
    End Function

    ''' <summary>
    ''' Sending mail to all users of which enable login is checked
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function sendMailToUser()
        If Not IsNothing(rptUsers) Then
            For Each row As RepeaterItem In rptUsers.Items
                Dim hashDetails As New Hashtable
                hashDetails.Add("AdminFName", Session("FirstName"))
                hashDetails.Add("AdminLName", Session("LastName"))
                hashDetails.Add("CompanyName", Session("CompanyName"))
                hashDetails.Add("UserFName", CType(row.FindControl("txtbxFName"), TextBox).Text.Trim.Replace("""", ""))
                hashDetails.Add("UserLName", CType(row.FindControl("txtbxLName"), TextBox).Text.Trim.Replace("""", ""))
                hashDetails.Add("UserEmail", CType(row.FindControl("txtbxEmailAddr"), TextBox).Text.Trim)
                hashDetails.Add("UserRoleGroup", CType(row.FindControl("drpdwnUserType"), DropDownList).SelectedItem.Text)
                hashDetails.Add("UserPassword", ViewState("ContactPassword"))

                If Not IsNothing(Session(Request("companyid") & "_ContactClassID")) Then
                    If Session(Request("companyid") & "_ContactClassID") = ApplicationSettings.RoleClientID Then
                        hashDetails.Add("userType", ResourceMessageText.GetString("Buyer"))
                    ElseIf Session(Request("companyid") & "_ContactClassID") = ApplicationSettings.RoleSupplierID Then
                        hashDetails.Add("userType", ResourceMessageText.GetString("Supplier"))
                    End If
                End If
                Dim topmessage As String
                If (CType(row.FindControl("chkbxEnableLogin"), CheckBox).Checked) Then
                    topmessage = ResourceMessageText.GetString("MailCongratulateString") & " " & _
                                           hashDetails.Item("AdminFName") & " " & hashDetails.Item("AdminLName") & " " & _
                                                ResourceMessageText.GetString("MailBodyActivate")

                    Emails.SendUserStatusMail("Activate", hashDetails, _
                                            ResourceMessageText.GetString("MailSubjectActivate"), topmessage, Request("bizDivId"))
                End If
            Next
        End If
        Return Nothing
    End Function
#End Region

End Class