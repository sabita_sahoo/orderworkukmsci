<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreateDebitNotes.aspx.vb" Inherits="Admin.CreateDebitNotes" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Create Debit Note"%>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<script language="javascript" type="text/javascript">
function OpenNewWindowInvoice(InvoiceNo,DebitNo,BizDivId)
{
var w = 800;
var h = 800;
var left = (screen.width/2)-(w/2);
var top = (screen.height/2)-(h/2);
 window.open ('ViewDebitNote.aspx?invoiceNo='+ InvoiceNo + '&bizDivId='+ BizDivId + '&PaymentNo='+ DebitNo +'', 'DebitNote', 'toolbar=0,location=0, directories=0, status=0, menubar=0, scrollbars=0, resizable=1, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
function CheckDebitAmount(id) {
    var PIAmt = parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblPIAmt").innerHTML).toFixed(2);
    var DebitAmt = parseFloat(document.getElementById(id).value).toFixed(2);
    var NewAmt = parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_txtbxAmt").value).toFixed(2);
    var AdminFee = parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_hdnAdminFee").value).toFixed(2);
    var PaymentDiscount = parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_hdnPaymentDiscount").value).toFixed(2);
    var NewNet = parseFloat(PIAmt - DebitAmt).toFixed(2);
    var IsNew = document.getElementById("ctl00_ContentPlaceHolder1_hdnIsNew").value;
    if (IsFloat(document.getElementById(id).value) == false) {
        alert("Please enter positive integer value.");
        document.getElementById(id).value = parseFloat(PIAmt - NewAmt).toFixed(2);        
    }
    else {
        //if (DebitAmt > PIAmt) {
        if (Math.round(DebitAmt * 100) > Math.round(PIAmt * 100)) {
            alert("Please enter valid amount for Debit Note, It cannot be greater than the Purchase Invoice Amount.");
            document.getElementById(id).value = parseFloat(PIAmt - NewAmt).toFixed(2);
        } else {
            if (IsNew == "False") {
                //old format
                document.getElementById("ctl00_ContentPlaceHolder1_txtbxAmt").value = parseFloat(PIAmt - DebitAmt).toFixed(2);
                document.getElementById("ctl00_ContentPlaceHolder1_txtFinalAmt").value = parseFloat((document.getElementById("ctl00_ContentPlaceHolder1_txtbxAmt").value) * 0.9).toFixed(2);
            } else {


                //new format
                document.getElementById("ctl00_ContentPlaceHolder1_txtbxAmt").value = parseFloat(NewNet - (NewNet * AdminFee / 100)).toFixed(2);
                document.getElementById("ctl00_ContentPlaceHolder1_txtFinalAmt").value = parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_txtbxAmt").value - ((document.getElementById("ctl00_ContentPlaceHolder1_txtbxAmt").value) * PaymentDiscount / 100)).toFixed(2);
               
            }
        }
    }

}
function IsFloat(sText) {
    return parseFloat(sText.match(/^-?\d*(\.\d+)?$/)) >= 0;
}
</script>	
<div id="divContent">
<div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
         <ContentTemplate>
         <input type="hidden" id="hdnAdminFee" runat="Server" value="0" />
         <input type="hidden" id="hdnPaymentDiscount" runat="Server" value="0" />
         <input type="hidden" id="hdnIsNew" runat="Server" value="0" />
            <table width="100%" cellpadding="0" cellspacing="0">
			    <tr>
			    <td width="10px"></td>
			    <td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Create Debit Note"  runat="server"></asp:Label></td>
			    </tr>
			    </table>
		   <div id="divValidationMain" class="divValidation" runat="server" visible="false" style="margin:15px">
				<div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  <table width="96%"  border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					</tr>
					<tr valign="middle">
					  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
					  <td width="565" class="validationText"><div  id="divValidationMsg"></div>
					   <span class="validationText">
					  	<asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
					  </span>
					   
					  </td>
					  <td width="20">&nbsp;</td>
					</tr>
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15"><input type="button" id="btnFocus" name="btnFocus" class="hdnClass"  /></td>
					  <td height="15">&nbsp;</td>
					</tr>

					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15"><asp:Label ID="lblErrMsg" CssClass="HeadingRed" Text=""  runat="server"></asp:Label></td>
					  <td height="15">&nbsp;</td>
					</tr>

				  </table>  
				  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				</div>
				<asp:RequiredFieldValidator id="rqWOValue_WP" runat="server" ErrorMessage="Please enter Amount" ForeColor="#EDEDEB"
								                                                        ControlToValidate="txtbxAmt">*</asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="regexp" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtbxAmt"
												                                                                            ErrorMessage="Please enter valid Amount" ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator>

              <p class="txtHeading">&nbsp;                </p>
              <table height="54" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server" id="tblSINumber">
                <tr valign="top">
                  <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                  <td class="formTxt" valign="top" align="left" width="166" height="14"><span class="formLabelGrey">&nbsp; Purchase Invoice Number&nbsp;</span><span class="bodytxtValidationMsg">*</span> <asp:RequiredFieldValidator id="rqSINumber" runat="server" ErrorMessage="Please enter Purchase Invoice Number" ForeColor="#EDEDEB"
									ControlToValidate="txtPINumber">*</asp:RequiredFieldValidator></td>
                  <td width="60" align="left" class="formTxt" >&nbsp;</td>				  
                </tr>
                <tr valign="top">
                  <td align="left">&nbsp;</td>
                  <td align="left" height="20"><asp:TextBox id="txtPINumber" TabIndex="1" runat="server" CssClass="formFieldGrey150"></asp:TextBox>
                  </td>                  
				  <td width="4" align="left">&nbsp;</td>
                </tr>
                <tr>
                <td height="20" colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left">&nbsp;</td>
                    <td align="left">
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                            <asp:LinkButton id="lnkbtnSubmit" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Submit&nbsp;</asp:LinkButton>
                        </div> 
                    </td>
                    <td align="left">&nbsp;</td>
                </tr>
              </table>
         

              <table height="114" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server" id="tblProceed" visible="false">
               <tr>
                <td class="formTxt" valign="top" align="left" width="28" height="40" >&nbsp;</td>
                <td align="left" class="formTxt" width="130" >
                    <b>Purchase Invoice: </b>
                </td>
                <td align="left" class="formTxt" width="200">
                    <asp:Label ID="lblPINo" runat="server" Text="Label"></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28" height="30" >&nbsp;</td>
                <td align="left" class="formTxt" valign="top" >
                    <b>Original Amount: </b>
                </td>
                <td align="left" class="formTxt" valign="top" >
                    <span class="formLabelGrey" style="vertical-align:top">&pound;</span><asp:Label ID="lblPIAmt" runat="server" Text="Label"></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>
               <tr>
                  <td class="formTxt" valign="top" align="left" width="28" height="20" >&nbsp;</td>
                  <td class="formTxt" align="left" ><b>Debit Amount</b></td>
                  <td class="formTxt" align="left"><b>Amount (exclusive of VAT)</b></td>
                  <td>&nbsp;</td>
                </tr>
                <tr valign="top">
                  <td class="formTxt" valign="top" align="left" width="28" height="30" >&nbsp;</td>
                  <td class="formTxt" align="left" >&nbsp;</td>
                  <td class="formTxt" align="left" ><span class="formLabelGrey" style="vertical-align:top">&pound;</span><asp:TextBox  CssClass="formFieldGrey150" ID="txtDebitAmt" runat="server" Text="0" MaxLength="8" onchange="Javascript:CheckDebitAmount(this.id);"></asp:TextBox></td>
                   <td>&nbsp;</td>
                </tr> 
                <tr>
                  <td class="formTxt" valign="top" align="left" width="28" height="20" >&nbsp;</td>
                  <td class="formTxt" align="left" ><b>NEW NET</b></td>
                  <td colspan="2" class="formTxt" align="left"><b><asp:Label id="lblNewNet" text="" runat="server"></asp:Label></b></td>
                  </tr>
                <tr valign="top">
                  <td class="formTxt" valign="top" align="left" width="28" height="30" >&nbsp;</td>
                  <td class="formTxt" align="left" >&nbsp;</td>
                  <td colspan="2" class="formTxt" align="left" ><span class="formLabelGrey" style="vertical-align:top">&pound;</span><asp:TextBox  CssClass="formFieldGrey150" ID="txtbxAmt" runat="server" Text="0" MaxLength="8"></asp:TextBox></td>
               </tr>  
                 <tr>
                  <td class="formTxt" valign="top" align="left" width="28" height="20" >&nbsp;</td>
                  <td class="formTxt" align="left" ><b>Final Value</b></td>
                  <td colspan="2" class="formTxt" align="left"><b><asp:Label ID="lblNewFinalAmt" runat="server" Text=""></asp:Label></b></td>
                  </tr>
                <tr valign="top">
                  <td class="formTxt" valign="top" align="left" width="28" height="30" >&nbsp;</td>
                  <td class="formTxt" align="left" >&nbsp;</td>
                  <td colspan="2" class="formTxt" align="left" ><span class="formLabelGrey" style="vertical-align:top">&pound;</span><asp:TextBox  CssClass="formFieldGrey150" ID="txtFinalAmt" runat="server" Text="0" MaxLength="8"></asp:TextBox></td>
                   </tr>  
                <tr>
                    <td colspan="4">&nbsp;
                    </td>
                </tr>        
                <tr>                   
                <td align="left" >&nbsp;</td>
                     <td align="left" width="170">
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                            <asp:LinkButton id="lnkbtnCancel" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false">&nbsp;Cancel&nbsp;</asp:LinkButton>
                        </div>
                      </td>				      
				      <td width="160" align="right">
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                            <asp:LinkButton id="lnkbtnNext" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Next&nbsp;</asp:LinkButton>
                        </div>
                      </td>
                      <td >&nbsp;</td>
                </tr>
				<tr valign="top">
				  <td align="left">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                  <td align="left" height="20">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
              </table>
              
         
         <%--table confirmation old--%>
              <table height="54" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server" id="tblConfirmation" visible="false">
              <tr>
              <td>&nbsp;</td>
                <td class="formTxt" style="font-size:13px" colspan="3" height="30" valign="top"><b>Purchase Invoice</b></td>
              </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" width="190" >
                    <b>Purchase Invoice Number: </b>
                </td>
                <td align="left" class="formTxt" width="150">
                    <asp:Label ID="lblConfPINo" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>Net Amount: </b>
                </td>
                <td align="left" class="formTxt" >
                    <span class="formLabelGrey">&pound;</span><asp:Label ID="lblConfNetAmt" runat="server" Text=""></asp:Label>
                </td>
                <td align="left" width="700">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>VAT Amount: </b>
                </td>
                <td align="left" class="formTxt" >
                    <span class="formLabelGrey">&pound;</span><asp:Label ID="lblConfVatAmt" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>Total Amount: </b>
                </td>
                <td align="left" class="formTxt" >
                   <span class="formLabelGrey">&pound;</span><asp:Label ID="lblConfTotAmt" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" colspan="4" height="45">&nbsp;</td>                
               </tr>               
               <tr>
               <td>&nbsp;</td>
                <td class="formTxt" style="font-size:13px" colspan="3" height="30" valign="top"><b>Debit Note</b></td>
              </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>Debit Note Net Amount: </b>
                </td>
                <td align="left" class="formTxt" >
                   <asp:Label ID="lblConfDNNetAmt" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>Debit Note VAT Amount: </b>
                </td>
                <td align="left" class="formTxt" >
                    <asp:Label ID="lblConfDNVat" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>Debit Note Total Amount: </b>
                </td>
                <td align="left" class="formTxt" >
                    <asp:Label ID="lblConfDNTotAmt" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>                              
                <tr>
                    <td colspan="4" height="50">&nbsp;
                    </td>
               </tr>        
              </table>
              
         

         <%--============================================================--%>
         <%--Table new confimation--%>
         <table height="54" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server" id="tblConfirmationNew" visible="false">
         <tr>
             <td>&nbsp;</td>
             <td class="formTxt" style="font-size:13px" colspan="3" height="30" valign="top"><b>Purchase Invoice</b></td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" width="190" >
                 <b>Purchase Invoice Number: </b>
             </td>
             <td width="10">&nbsp;</td>
             <td align="left" class="formTxt" width="150">
                 <asp:Label id="lblConfPINoNew" runat="server" Text=""></asp:Label>
             </td>
             <td align="left">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>Net Amount (Platform Price): </b>
             </td>
             <td>&nbsp;</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label ID="lblConfNetAmtNew" runat="server" Text=""></asp:Label>
             </td>
             <td align="left" width="700">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>Net Amount less <asp:Label ID="lblAdminFeePI" runat="server" Text=""></asp:Label>% Admin Fee: </b>
             </td>
             <td>&nbsp;</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label id="lblPIAmtAdminFee" runat="server" Text=""></asp:Label>
             </td>
             <td align="left" width="700">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>Fast Payment Fee <asp:Label ID="lblPaymentDiscount" runat="server" Text=""></asp:Label>%: </b>
             </td>
             <td>-</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label id="lblPIPaymentDisc" runat="server" Text=""></asp:Label>
             </td>
             <td align="left" width="700">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>Insurance: </b>
             </td>
             <td>-</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span>0.00
             </td>
             <td align="left" width="700">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>Invoice Amount ex. VAT: </b>
             </td>
             <td>&nbsp;</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label ID="lblPIAmtExVat" runat="server" Text=""></asp:Label>
             </td>
             <td align="left" width="700">&nbsp;</td>
         </tr>
         <tr valign="top">				 
             <td align="left" colspan="5">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>VAT Amount: </b>
             </td>
             <td>&nbsp;</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label ID="lblConfVatAmtNew" runat="server" Text=""></asp:Label>
             </td>
             <td align="left">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>Total Amount: </b>
             </td>
             <td>&nbsp;</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label ID="lblConfTotAmtNew" runat="server" Text=""></asp:Label>
             </td>
             <td align="left">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" colspan="5" height="45">&nbsp;</td>                
         </tr>               
         <tr>
             <td>&nbsp;</td>
             <td class="formTxt" style="font-size:13px" colspan="3" height="30" valign="top"><b>Debit Note</b></td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>DN Net Amount: </b>
             </td>
             <td>&nbsp;</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label ID="lblConfDNNetAmtNew" runat="server" Text=""></asp:Label>
             </td>
             <td align="left">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>DN Net Amount less <asp:Label ID="lblDNAdminFee" runat="server" Text=""></asp:Label>% Admin Fee: </b>
             </td>
             <td>&nbsp;</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label ID="lblDNAmtAdminFee" runat="server" Text=""></asp:Label>
             </td>
             <td align="left">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>DN Fast Payment Fee <asp:Label ID="lblDNPaymentdiscount" runat="server" Text=""></asp:Label>%: </b>
             </td>
             <td>-</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label ID="lblDNAmtPaymentdiscount" runat="server" Text=""></asp:Label>
             </td>
             <td align="left">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>DN Insurance: </b>
             </td>
             <td>-</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span>0.00
             </td>
             <td align="left">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>DN Invoice Amount ex. VAT: </b>
             </td>
             <td>&nbsp;</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label ID="lblDNAmtExVat" runat="server" Text=""></asp:Label>
             </td>
             <td align="left">&nbsp;</td>
         </tr>
         
         <tr>
             <td class="formTxt" valign="top" align="left" colspan="5" height="45">&nbsp;</td>                
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>DN VAT Amount: </b>
             </td>
             <td>&nbsp;</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label ID="lblDNVat" runat="server" Text=""></asp:Label>
             </td>
             <td align="left">&nbsp;</td>
         </tr>
         <tr>
             <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
             <td align="left" class="formTxt" >
                 <b>DN Total Amount: </b>
             </td>
             <td>&nbsp;</td>
             <td align="left" class="formTxt" >
                 <span class="formLabelGrey">&pound;</span><asp:Label ID="lblDNTotalAmt" runat="server" Text=""></asp:Label>
             </td>
             <td align="left">&nbsp;</td>
         </tr>
         <tr>
             <td colspan="5" height="50">&nbsp;
             </td>
         </tr>    
         </table>
         <%--===================================================--%>
         

         
         <%--Table buttons--%>
         <table height="54" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server" id="tblConfirmationButtons" visible="false">
             <tr>                   
                 <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                 <td align="left" width="205">
                     <div style="width:60px; float:left;">
                         <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                             <asp:LinkButton id="lnkbtnBack" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false">&nbsp;Back&nbsp;</asp:LinkButton>
                         </div>
                     </div>
                     <div style="width:60px; float:right; margin-left:30px;">
                     <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                     <asp:LinkButton id="lnkbtnConfCan" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Cancel&nbsp;</asp:LinkButton><div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                     </div></td>				      
                 <td align="right" width="145">
                     <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                         <asp:LinkButton id="lnkbtnConf" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Confirm&nbsp;</asp:LinkButton>
                     </div>
                 </td>
                 <td align="left">&nbsp;</td>
             </tr>
             <tr valign="top">				 
                 <td align="left" colspan="4">&nbsp;</td>
             </tr>
         </table>

              <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="3%"  align="left">&nbsp;  
                    		
		        </td>                
                <td width="97%"  align="left"><asp:Label ID="lblMsgInner" runat="server" class="bodytxtValidationMsg" ></asp:Label>	</td>
              </tr>
              </table> 

           	<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="HeadingRed" style="padding:0px 0px 0px 10px;"><asp:Label ID="lblErrorMsg" Visible="false" runat=server></asp:Label></td>
			</tr>
			</table>
			           
          </ContentTemplate>
      
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />

		
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
 </asp:Content>