
Partial Public Class InsuranceExpiry
    Inherits System.Web.UI.Page

    Protected WithEvents UCDateRange1 As UCDateRange

    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)

            If Request("sender") = "CompanyProfile" Then
                ProcessBackToListing()
            Else
                Dim currDate As DateTime = System.DateTime.Today
                'UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
                UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(DateTime.Now.AddDays(-30), DateFormat.ShortDate)
                ViewState("fromDate") = Strings.FormatDateTime(DateTime.Now.AddDays(-30), DateFormat.ShortDate) 'Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
                UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
                ViewState("toDate") = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
                ViewState!SortExpression = "PublicLiabilityExpDate"
                gvInvoices.Sort(ViewState!SortExpression, SortDirection.Ascending)
                gvInvoices.PageIndex = 0
            End If
        End If
    End Sub

    Public Sub ProcessBackToListing()
        Dim currDate As DateTime = System.DateTime.Today
        UCDateRange1.txtFromDate.Text = Request("FromDate")
        ViewState("fromDate") =  Request("FromDate")
        UCDateRange1.txtToDate.Text = Request("ToDate")
        ViewState("toDate") = Request("ToDate")
        ViewState!SortExpression = Request("SC")
        gvInvoices.Sort(ViewState!SortExpression, Request("SO"))
        PopulateGrid()
        gvInvoices.PageIndex = Request("PN")
    End Sub

    Public Sub PopulateGrid()
        gvInvoices.DataBind()
    End Sub
    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then
            ViewState("fromDate") = UCDateRange1.txtFromDate.Text
            ViewState("toDate") = UCDateRange1.txtToDate.Text
            PopulateGrid()
        End If
    End Sub
   

    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvInvoices.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvInvoices.DataBind()
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me

    End Sub
    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                       ByVal maximumRows As System.Nullable(Of Integer)) As DataSet

        If sortExpression = "" Then
            sortExpression = "PublicLiabilityExpDate"
        End If
        Dim ds As DataSet
        Dim bizDivId As Integer = Session("BizDivId")
        ds = ws.WSContact.GetInsuranceDetails(ViewState("fromDate"), ViewState("toDate"), bizDivId, sortExpression, startRowIndex, maximumRows)
        ViewState("rowCount") = ds.Tables(1).Rows(0)(0)
        btnExport.HRef = prepareExportToExcelLink()
        Return ds
    End Function
    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function
    Public Function GetLinks(ByVal companyId As String, ByVal ClassID As String) As String
        Dim link As String = ""
        Dim linkParams As String = ""

        linkParams &= "&bizDivId=" & Session("BizDivId")
        linkParams &= "&companyId=" & companyId
        linkParams &= "&FromDate=" & ViewState("fromDate")
        linkParams &= "&ToDate=" & ViewState("toDate")
        linkParams &= "&sender=" & "InsuranceExpiry"
        linkParams &= "&PS=" & gvInvoices.PageSize
        linkParams &= "&PN=" & gvInvoices.PageIndex
        linkParams &= "&SC=" & gvInvoices.SortExpression
        linkParams &= "&SO=" & gvInvoices.SortDirection
        linkParams &= "&classid=" & ClassID
        link &= "<a class=footerTxtSelected href='CompanyProfile.aspx?" & linkParams & "'><img src='Images/Icons/ViewDetails.gif' alt='View Details' width='16' height='16' hspace='2' vspace='0' border='0'></a>"
        Return link
    End Function
    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvInvoices.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvInvoices, e.Row, Me)
        End If
    End Sub
    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex



    End Sub
    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvInvoices.RowDataBound
        MakeGridViewHeaderClickable(gvInvoices, e.Row)
    End Sub
    Public Function prepareExportToExcelLink() As String

        Dim BizDivId As Integer = ViewState("BizDivId")
        Dim fromDate As String = ViewState("fromDate")
        Dim toDate As String = ViewState("toDate")
        Dim sortExpression As String = ""
        If Not IsNothing(ViewState("sortExpression")) Then
            sortExpression = ViewState("sortExpression")
        End If
        If sortExpression = "" Then
            sortExpression = "PublicLiabilityExpDate"
        End If
        Dim linkParams As String = ""
        linkParams &= "BizDivId=" & BizDivId
        linkParams &= "&fromDate=" & fromDate
        linkParams &= "&toDate=" & toDate
        linkParams &= "&sortExpression=" & sortExpression
        linkParams &= "&page=" & "IE"
        Return "ExportToExcel.aspx?" & linkParams
    End Function

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub


End Class