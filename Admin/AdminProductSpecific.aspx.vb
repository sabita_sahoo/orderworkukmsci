﻿Public Class AdminProductSpecific
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblMessage.Text = ""
            If Not IsNothing(Request.QueryString("ProductID")) Then
                ViewState.Add("ProductID", Request.QueryString("ProductID"))
                lnkAddEditServiceQA.Visible = True
                lnkAddEditServiceQA.HRef = "~\AddEditServiceQA.aspx?CompanyID=" & Request("companyid") & "&ProductID=" & Request.QueryString("ProductID")
            Else
                ViewState.Add("ProductID", 0)
                lnkAddEditServiceQA.Visible = False
            End If
        End If
    End Sub

    Private Sub btnCancelTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTop.Click, btnCancelBtm.Click
        If Not IsNothing(Request.QueryString("pagename")) Then
            If Request.QueryString("pagename") = "AccountService" Then
                Response.Redirect("AccountServiceList.aspx?sender=productform")
            Else
                'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                Response.Redirect("AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
            End If
        Else
            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            Response.Redirect("AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
        End If
    End Sub
End Class