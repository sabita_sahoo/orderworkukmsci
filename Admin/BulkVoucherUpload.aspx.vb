﻿Imports System.IO
Imports System.Linq
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet

Public Class BulkVoucherUpload
    Inherits System.Web.UI.Page

    Dim dt As New DataTable()
    Dim dv As New DataView()
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
            End If
        Catch ex As Exception
            CommonFunctions.createLog("Bulk Voucher Upload - Page_Load - " + ex.ToString())
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, "<br><br> Error in Admin - Voucher Code Upload - Page_Load")
        End Try
    End Sub

    Private Sub btn_upload_excel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_upload_excel.Click
        Try
            Dim filePath As String = ""
            If fileuploader.HasFile Then
                Try
                    'Dim connectionString As String = ""
                    'Dim fileName As String = System.IO.Path.GetFileName(fileuploader.PostedFile.FileName)
                    'Dim Extension As String = System.IO.Path.GetExtension(fileuploader.FileName).ToString

                    'fileLocation = Server.MapPath(Convert.ToString("~/Templates/") & fileName)

                    'fileuploader.SaveAs(fileLocation)

                    'Select Case Extension
                    '    Case ".xls"
                    '        'Excel 97-03
                    '        connectionString = ConfigurationManager.ConnectionStrings("Excel03ConString") _
                    '                   .ConnectionString
                    '        Exit Select
                    '    Case ".xlsx"
                    '        'Excel 07
                    '        connectionString = ConfigurationManager.ConnectionStrings("Excel07ConString") _
                    '                  .ConnectionString
                    '        Exit Select

                    'End Select

                    'connectionString = String.Format(connectionString, fileLocation, "true")

                    'Dim con As New OleDbConnection(connectionString)

                    'Dim cmd As New OleDbCommand()

                    'cmd.CommandType = System.Data.CommandType.Text

                    'cmd.Connection = con

                    'Dim dAdapter As New OleDbDataAdapter(cmd)

                    'con.Open()

                    'Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)

                    'Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()

                    ''Check if the excel contain valid values

                    'cmd.CommandText = (Convert.ToString("SELECT TOP 101 VoucherCode,SerialNumber,ServiceNumber FROM [") & getExcelSheetName) + "]"

                    'dAdapter.SelectCommand = cmd

                    'dAdapter.Fill(dt)

                    'con.Close()

                    'Save the uploaded Excel file.
                    filePath = Server.MapPath("~/Templates/") + Path.GetFileName(fileuploader.PostedFile.FileName)
                    fileuploader.SaveAs(filePath)
                    CommonFunctions.createLog("Bulk Voucher Upload - file saved")
                    'Open the Excel file in Read Mode using OpenXml.
                    Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(filePath, False)
                        'Read the first Sheet from Excel file.
                        Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild(Of Sheet)()

                        'Get the Worksheet instance.
                        Dim worksheet As Worksheet = TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart).Worksheet

                        'Fetch all the rows present in the Worksheet.
                        Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()



                        'Loop through the Worksheet rows.
                        For Each row As Row In rows
                            'Use the first row to add columns to DataTable.
                            If row.RowIndex.Value = 1 Then
                                For Each cell As Cell In row.Descendants(Of Cell)()
                                    dt.Columns.Add(GetValue(doc, cell))
                                Next
                            Else
                                'Add rows to DataTable.
                                dt.Rows.Add()
                                Dim i As Integer = 0
                                For Each cell As Cell In row.Descendants(Of Cell)()
                                    dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
                                    i = i + 1
                                Next
                            End If
                        Next
                    End Using

                    'Delete blank rows
                    For i As Integer = dt.Rows.Count - 1 To 0 Step -1
                        If IsDBNull(dt.Rows(i)(0)) Or Convert.ToString(dt.Rows(i)(0)) = "" Then
                            dt.Rows(i).Delete()
                        End If
                    Next
                    CommonFunctions.createLog("Bulk Voucher Upload - dt formed")
                    dt.AcceptChanges()



                    If dt.Columns(0).ColumnName = "VoucherCode" And dt.Columns(1).ColumnName = "SerialNumber" And dt.Columns(2).ColumnName = "ServiceNumber" Then
                        If dt.Rows.Count > 0 Then
                            CommonFunctions.createLog("Bulk Voucher Upload - valid file")
                            Dim newColumn As New DataColumn("IsValid", GetType(String))
                            newColumn.DefaultValue = "Y"
                            dt.Columns.Add(newColumn)
                            dt.Columns.Add("Reason", GetType(String))

                            'Validation to check if any value is missing then make this entry as invalid
                            For i As Integer = dt.Rows.Count - 1 To 0 Step -1
                                For j As Integer = dt.Columns.Count - 2 To 0 Step -1
                                    If IsDBNull(dt.Rows(i)(j)) Then
                                        dt.Rows(i)("IsValid") = "N"
                                        dt.Rows(i)("Reason") = "Blank value"
                                    End If
                                Next
                            Next
                            CommonFunctions.createLog("Bulk Voucher Upload - missing records deleted")
                            'Duplicate in Excel Check 
                            dt = getDuplicateInExcel(dt)
                            CommonFunctions.createLog("Bulk Voucher Upload - getDuplicateInExcel done")
                            'Duplicate in database Check 
                            dt = getDuplicateInDatabase(dt)
                            CommonFunctions.createLog("Bulk Voucher Upload - getDuplicateInDatabase done")

                            'Get the Valid Records
                            dv = dt.AsDataView()
                            dv.RowFilter = "IsValid='Y'"
                            DataList2.DataSource = dv
                            DataList2.DataBind()
                            If (dv.Count > 0) Then
                                btn_upload.Visible = True
                            Else
                                btn_upload.Visible = False
                            End If

                            CommonFunctions.createLog("Bulk Voucher Upload - DataList2 done")
                            'Get the Invalid Records
                            dv = dt.AsDataView()
                            dv.RowFilter = "IsValid='N'"
                            DataList3.DataSource = dv
                            DataList3.DataBind()
                            'If (dv.Count > 0) Then
                            '    btn_download.Visible = True
                            'Else
                            '    btn_download.Visible = False
                            'End If
                            CommonFunctions.createLog("Bulk Voucher Upload - Invalid done")
                            divUpload.Attributes.Add("class", "Hide")
                            divConfirm.Attributes.Add("class", "Show")
                            divConfirmSuccessfullRecordsForRetailClient.Attributes.Add("class", "Show")
                            divConfirmInvalidRecordsForRetailClient.Attributes.Add("class", "Show")

                            Session("dt") = dt
                            CommonFunctions.createLog("Bulk Voucher Upload - session set done")
                        Else
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "Alert", "<script>alert('Cannot Read From Empty File');</script>", False)
                        End If
                    Else
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "Alert", "<script>alert('Please Upload a Valid File');</script>", False)

                    End If

                Catch ex As Exception
                Finally
                    If File.Exists(filePath) Then
                        CommonFunctions.createLog("Bulk Voucher Upload - File Delete done")
                        File.Delete(filePath)
                    End If
                End Try
            End If
        Catch ex As Exception
            CommonFunctions.createLog("Bulk Voucher Upload - btn_upload_excel_Click - " + ex.ToString())
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, "<br><br> Error in Admin - Voucher Code Upload - btn_upload_excel_Click")
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "Error - " + ex.ToString, "<br><br> Error in Admin - Voucher Code Upload - btn_upload_excel_Click")
        End Try
    End Sub

    Private Sub btn_upload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_upload.Click
        Try
            Dim count As Integer = 0
            Dim dt_success As New DataTable()
            Dim dt_invalid As New DataTable()
            Dim ds As New DataSet()

            dt = TryCast(Session("dt"), DataTable)
            dv = dt.AsDataView()
            dv.RowFilter = "IsValid='Y'"
            dt_success = dv.ToTable()
            dt_success.Columns.Remove("IsValid")

            ds.Tables.Add(dt_success)
            Dim xmlContent As String = ds.GetXml

            ds = OrderWorkLibrary.CommonFunctions.InsertServiceVouchers(xmlContent, Convert.ToInt32(ddlService.SelectedValue))
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "Alert", "<script>alert('Record inserted successfully');</script>", False)

            divUpload.Attributes.Add("class", "Show")
            divConfirm.Attributes.Add("class", "Hide")
            divConfirmSuccessfullRecordsForRetailClient.Attributes.Add("class", "Hide")
            divConfirmInvalidRecordsForRetailClient.Attributes.Add("class", "Hide")
        Catch ex As Exception
            CommonFunctions.createLog("Bulk Voucher Upload - btn_upload_Click - " + ex.ToString())
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, "<br><br> Error in Portal - Voucher Code Upload - btn_upload_Click")
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "Error - " + ex.ToString, "<br><br> Error in Portal - Voucher Code Upload - btn_upload_Click")
        End Try
    End Sub



    Public Function getDuplicateInExcel(ByVal dt As DataTable) As DataTable

        Dim duplicates = dt.AsEnumerable().GroupBy(Function(i) i.Field(Of String)("VoucherCode")).Where(Function(g) g.Count() > 1).Select(Function(g) g.Key)
        For Each dup As String In duplicates
            dv = dt.AsDataView()
            dv.RowFilter = "VoucherCode='" & dup & "'"

            'Counter starts from 1st element not 0 becauase we have to take 1 record from duplicates
            For n As Integer = 1 To dv.Count - 1
                dv(n)("IsValid") = "N"
                If Convert.ToString(dv(n)("Reason")) = "" Then
                    dv(n)("Reason") = "Duplicate in excel"
                Else
                    dv(n)("Reason") = dv(n)("Reason") + ", " + "Duplicate in excel"
                End If

            Next
            dt.AcceptChanges()
        Next
        Return dt

    End Function

    Public Function getDuplicateInDatabase(ByVal dt As DataTable) As DataTable
        Try
            Dim ds As DataSet
            dv = dt.AsDataView()
            For n As Integer = 0 To dv.Count - 1
                '1. Fist check if the voucher code is in the proper format
                Dim regex As Regex = New Regex("^[a-zA-Z]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}[a-zA-Z]{1}$")

                Dim vouchercode As String = Convert.ToString(dv(n)("VoucherCode"))
                Dim match As Match = regex.Match(vouchercode)
                If match.Success Then
                    ds = OrderWorkLibrary.CommonFunctions.IsDuplicateVoucher(Convert.ToInt32(ddlService.SelectedValue), vouchercode)

                    If ds.Tables(0).Rows.Count > 0 Then
                        dv(n)("IsValid") = "N"
                        If Convert.ToString(dv(n)("Reason")) = "" Then
                            dv(n)("Reason") = "Duplicate in database"
                        Else
                            dv(n)("Reason") = Convert.ToString(dv(n)("Reason")) + ", " + "Duplicate in database"
                        End If

                    End If

                    ' Check if such service is present in database, else make record invalid
                    Dim ServiceID As Integer = dv(n)("ServiceNumber")
                    Dim CompanyID As Integer = Convert.ToInt32(ddlService.SelectedValue)

                    ds = OrderWorkLibrary.CommonFunctions.DoesServiceExist(CompanyID, ServiceID)

                    If ds.Tables(0).Rows.Count = 0 Then
                        dv(n)("IsValid") = "N"
                        If Convert.ToString(dv(n)("Reason")) = "" Then
                            dv(n)("Reason") = "Invalid Service"
                        Else
                            dv(n)("Reason") = Convert.ToString(dv(n)("Reason")) + ", " + "Invalid Service"
                        End If

                    End If

                Else
                    dv(n)("IsValid") = "N"
                    If Convert.ToString(dv(n)("Reason")) = "" Then
                        dv(n)("Reason") = "Invalid voucher code"
                    Else
                        dv(n)("Reason") = Convert.ToString(dv(n)("Reason")) + ", " + "Invalid voucher code"
                    End If
                End If
            Next
            Return dt
        Catch ex As Exception
            CommonFunctions.createLog("Bulk Voucher Upload - getDuplicateInDatabase - " + ex.ToString())
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, "<br><br> Error in Portal - Voucher Code Upload - btn_upload_Click")
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "Error - " + ex.ToString, "<br><br> Error in Portal - Voucher Code Upload - btn_upload_Click")
        End Try
    End Function

    Private Sub btn_cancel_upload_Click(sender As Object, e As System.EventArgs) Handles btn_cancel_upload.Click
        Try
            divUpload.Attributes.Add("class", "Show")
            divConfirm.Attributes.Add("class", "Hide")
            divConfirmSuccessfullRecordsForRetailClient.Attributes.Add("class", "Hide")
            divConfirmInvalidRecordsForRetailClient.Attributes.Add("class", "Hide")
        Catch ex As Exception
            CommonFunctions.createLog("Bulk Voucher Upload - btn_cancel_upload_Click - " + ex.ToString())
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, "<br><br> Error in Portal - Voucher Code Upload - btn_cancel_upload_Click")
        End Try
    End Sub

    'Private Sub btn_download_Click(sender As Object, e As System.EventArgs) Handles btn_download.Click
    '    dt = TryCast(Session("dt"), DataTable)
    '    dv = dt.AsDataView()
    '    dv.RowFilter = "IsValid='N'"

    '    Dim dt_invalid As New DataTable
    '    Dim ds As New DataSet

    '    dt_invalid = dv.ToTable()
    '    ds.Tables.Add(dt_invalid)
    '    ds.AcceptChanges()

    '    toExcel()

    'End Sub

    Private Sub toExcel()
        Response.Clear()
        Response.Buffer = True
        Response.AppendHeader("Content-Disposition", "attachment;filename=schoolsinfo.xlsx")
        Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8")
        Response.ContentType = "application/ms-excel"
        Me.EnableViewState = False
        Dim myCItrad As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("EN-US", True)
        Dim oStringWriter As System.IO.StringWriter = New System.IO.StringWriter(myCItrad)
        Dim oHtmlTextWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(oStringWriter)
        DataList3.RenderControl(oHtmlTextWriter)
        HttpContext.Current.Response.Write(oStringWriter.ToString())
    End Sub
    'Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
    '    Try
    '        Return
    '    Catch ex As Exception
    '    End Try
    'End Sub
    Private Function GetValue(doc As SpreadsheetDocument, cell As Cell) As String
        Try
            Dim value As String = ""
            value = cell.CellValue.Text
            If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = CellValues.SharedString Then
                Return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(Integer.Parse(value)).InnerText
            End If
            Return value
        Catch ex As Exception
            Return ""
        End Try
    End Function
End Class