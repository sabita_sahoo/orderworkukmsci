﻿<%@ Page Title="OrderWork : Special Login Access" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="SpecialLoginAccess.aspx.vb" Inherits="Admin.SpecialLoginAccess" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


              <div id="divContent">

         

            

        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>

        

             <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">

         

         <tr>

            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->

            <input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />                          

            <input id="hdnPageNo"  value="1" type="hidden" name="hdnPageNo" runat="server">

                  <input id="hdnPageSize" value="25" type="hidden" name="hdnPageSize" runat="server">

                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate> 

                                <table width="100%" cellpadding="0" cellspacing="0">

                                    <tr>

                                    <td width="10px"></td>

                                    <td><asp:Label ID="lblMessage" CssClass="HeadingRed"  runat="server" Text="Special Login Access"></asp:Label></td>

                                    </tr>

                                    </table>

              <table style="margin-left:20px;">
      
      <tr>
        <td colspan="2">
             <div id="divValidationSpecialLoginAccess" class="divValidation floatLeft" style="margin-top:10px;" runat=server visible=false>
	            <div class="roundtopVal"><img src="~/Images/Curves/Validation-TLC.gif" runat="server" id="img7" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
			            <tr>
				            <td height="15" align="center">&nbsp;</td>
				            <td height="15">&nbsp;</td>
				            <td height="15">&nbsp;</td>
			            </tr>
			            <tr valign="middle">
				            <td width="63" align="center" valign="top"><img src="~/Images/Icons/Validation-Alert.gif" runat="server" id="img8"></td>
				            <td class="validationText"><div  id="div4"></div>
				                <asp:Label ID="lblErrorSpecialLoginAccess"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
				                <asp:ValidationSummary id="ValSummSpecialLoginAccess"  runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" ValidationGroup="SpecialLoginAccess" EnableClientScript="false"></asp:ValidationSummary>
				            </td>
				            <td width="20">&nbsp;</td>
			            </tr>
			            <tr>
				            <td height="15" align="center">&nbsp;</td>
				            <td height="15">&nbsp;</td>
				            <td height="15">&nbsp;</td>
			            </tr>
		            </table>
	            <div class="roundbottomVal"><img src="~/Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        </div> 
        </td>
      </tr>
        <tr>
            <td>
                      <div id="divForAdvanceSearch" runat="server" visible="true">
			            <div id="divrow2" style="width:100%;margin-top:20px;">
			                <div class="formTxt" style="width:150px; float:left; margin-right:10px;">
			                    New Password
                                <asp:CustomValidator ID="custPassword"  runat="server" ValidationGroup="SpecialLoginAccess"  ErrorMessage="Please enter your Password"  ForeColor="#EDEDEB" ControlToValidate="txtPassword">*</asp:CustomValidator>
                                <asp:RegularExpressionValidator ControlToValidate="txtPassword" ValidationGroup="SpecialLoginAccess"  ErrorMessage="Password must contain 8 or more alphanumeric characters." ForeColor="#EDEDEB" ID="rgPassword" runat="server" ValidationExpression="[0-9a-zA-Z]{8,}" >*</asp:RegularExpressionValidator>
                                <asp:TextBox id="txtPassword" runat="server" Width="140"  CssClass="formField105" TextMode="Password"></asp:TextBox>                               
			                </div>  
                              <div class="formTxt" style="width:150px; float:left; margin-right:10px;">
			                    Confirm Password
                                 <asp:CompareValidator id="CompareValidator1" runat="server" ErrorMessage="Password does not match, please reconfirm" ValidationGroup="SpecialLoginAccess" 
			                        ControlToValidate="txtPassword" ControlToCompare="txtConfirmPassword">*</asp:CompareValidator>
                                <asp:TextBox id="txtConfirmPassword" runat="server" Width="140"  CssClass="formField105" TextMode="Password"></asp:TextBox>                               
			                </div>  
                            <div class="formTxt" style="width:70px; float:left;margin-top:15px; ">
                                <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" Checked="true" AutoPostBack="true" />
			                 </div>  			              
			            </div>
			        </div>
            
            </td>
                <td align="left" width="60px" valign="bottom">
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                        <asp:LinkButton ID="btnSend" causesValidation=False runat="server" CssClass="txtButtonRed" Text="Send"></asp:LinkButton>
                    </div>
                </td>                
            </tr>
      
      </table>

            <table >

            <tr>

            <td class="paddingL30 padB21" valign="top" padB21 paddingTop1>

            <asp:Label ID="lblMsg" runat="server" CssClass="bodytxtValidationMsg"></asp:Label>                

            </td>

            </tr>

            </table>                      

                        

                 

                                          

                  

                        

                               

                               

                  <hr style="background-color:#993366;height:5px;margin:0px;" />

                  

                    <asp:Panel ID="pnlListing" runat="server">    

                                <asp:GridView ID="gvSpecialLoginAccess"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'

                         BorderColor="White"  dataKeyNames="StandardID" PagerSettings-Mode=NextPreviousFirstLast   PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          

               <EmptyDataTemplate>

               <table  width="100%" border="0" cellpadding="10" cellspacing="0">                            

                                    <tr>

                                          <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.

                                          </td>

                                    </tr>                   

                              </table>

                  

               </EmptyDataTemplate>

               <PagerTemplate>

                   

                  

                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">

                                    <tr>                                                                                   

                                          <td runat="server"  id="tdAddNewStandard" align="left" class="txtWelcome paddingL10">
                                               <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;float:left;">
                                                <asp:LinkButton OnClick="AddNewStandard" class="buttonText" style="cursor:hand;line-height:20px;vertical-align:middle;" id="btnProcessed"  runat=server><strong>Add New</strong></asp:LinkButton>                           
                                          </div></td>                                      

                                

                                          <td>

                                                <table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">

                                                  <tr>

                                                      <td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >

                                                            <TR>

                                                                  <td align="right" valign="middle">

                                                                    <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>

                                                                  </td>

                                                                  <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">

                                                                  <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                                                        <tr>

                                                                        <td align="right" width="36">

                                                                        <div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />

                                                                              </div>      

                                                                              <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />

                                                                              </div>                                                                                                                                             

                                                                        </td>

                                                                        <td width="50" align="center" style="margin-right:3px; ">                                                                              

                                                                              <asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">

                                                    </asp:DropDownList>

                                                                        </td>

                                                                        <td width="30" valign="bottom">

                                                                              <div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />

                                                                              </div>

                                                                              <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />

                                                                              </div>

                                                                        </td>

                                                                        </tr>

                                                                  </table>

                                                                  </td>

                                                            </TR>

                                                      </TABLE></td>

                                                  </tr>

                                                </table>

                                      </td>

                                    </tr>

                              </table> 

                   

        </PagerTemplate> 

               <Columns>                 
                            
               <asp:TemplateField ItemStyle-Width="60%" HeaderText="Email" HeaderStyle-CssClass="gridHdr" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="20%"/>
                <ItemTemplate>             
                    <%# (Container.DataItem("StandardValue"))%>               
                    <input type=hidden runat=server id="hdnStandardID"  value='<%#Container.DataItem("StandardID")%>'/>    
                     <input type=hidden runat=server id="hdnStandardValue"  value='<%#Container.DataItem("StandardValue")%>'/>
				</ItemTemplate>
                </asp:TemplateField>            
                <asp:TemplateField HeaderText="IsActive" HeaderStyle-CssClass= "gridHdr gridText"  >         
                  <HeaderStyle  CssClass="gridHdr gridText"  />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" Width="5%"/>
                    <ItemTemplate>             
                   <asp:Label ID="lblIsActive" runat="server" Text='<%# IIF(Container.DataItem("IsActive"), True,False)%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>   
                   <asp:TemplateField ItemStyle-Width="6%">               
                <HeaderStyle  CssClass="gridHdr" />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow"/>
                    <ItemTemplate>    
                    <asp:LinkButton ID="lnkBtnEdit" CausesValidation="false"   CommandName="EditStd" CommandArgument='<%#Container.DataItem("StandardID") & "#" & Container.DataItem("StandardValue") & "#" & Container.DataItem("IsActive") %>' runat="server">
                           <img src="Images/Icons/Edit.gif" title='Edit' width="12" height="11" hspace="2" vspace="0" border="0">
                    </asp:LinkButton>                           
                    </ItemTemplate>
                </asp:TemplateField>  
                </Columns>

                 <AlternatingRowStyle CssClass=gridRow />

                 <RowStyle CssClass=gridRow />            

                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />

                <HeaderStyle  CssClass="gridHdr" HorizontalAlign=Left VerticalAlign=Top  Height="40px" />

                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />

        

              </asp:GridView>

              </asp:Panel>

      <cc1:ModalPopupExtender ID="mdlStandard" runat="server" TargetControlID="btnTemp" PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
<asp:Panel runat="server" ID="pnlConfirm" Width="300px" CssClass="pnlConfirm" style="display:none;">
    <div id="divModalParent">
        <span style="font-family:Tahoma; font-size:14px;"><b>Email</b></span><br /><br />        
            <asp:TextBox runat="server" ID="txtStandardValue" Width="280" CssClass="bodyTextGreyLeftAligned" MaxLength="50"></asp:TextBox>   
             <br /><br />
          <asp:CheckBox ID="chkIsActivePopup" runat="server" Text="IsActive" />                
        <br /><br />
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;float:left;">             
                    <a class="buttonText cursorHand" href='javascript:validate_required("ctl00_ContentPlaceHolder1_txtStandardValue")' id="ancSubmit" style="vertical-align:middle;line-height:20px;"><strong>Submit</strong></a>
            </div>
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;margin-left:20px;float:left;">
                    <a class="buttonText cursorHand" runat="server" id="ancCancel" style="vertical-align:middle;line-height:20px;"><strong>Cancel</strong></a>
            </div> 
    </div>								
</asp:Panel>
<asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
<asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

              

                         </ContentTemplate>

                         <Triggers>

                            <asp:PostBackTrigger   ControlID="btnSend"   />                       

                         </Triggers>

            

            </asp:UpdatePanel>

            

                <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">

                <ProgressTemplate >

                        <div class="gridText">

                               <img  align=middle src="Images/indicator.gif" />

                               <b>Fetching Data... Please Wait</b>

                        </div>   

                        </ProgressTemplate>

                </asp:UpdateProgress>

                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />

             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"

                SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.SpecialLoginAccess" SortParameterName="sortExpression">                

            </asp:ObjectDataSource>

                                            

                              

            <!-- InstanceEndEditable --></td>

                     

             

         </tr>

         </table>

      </div>


<script type="text/javascript">
    function validate_required(field) {        
        if (document.getElementById(field).value == "") {

            alert("Please enter email")
        }
        else {
            if (ValidateEmail(document.getElementById(field).value))
            {
                document.getElementById("ctl00_ContentPlaceHolder1_hdnButton").click();
            }
            else
            {
                alert("You have entered an invalid email address!");  
            }
        }
    }
    function ValidateEmail(uemail) {        
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/;
        if (uemail.match(mailformat)) {
            return true;
        }
        else {           
            return false;
        }
    }  
    
</script>


</asp:Content>
