<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Work Order Notes" Codebehind="WONotes.aspx.vb" Inherits="Admin.WONotes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
.summaryCSS
{
min-height:51px; 
height:100%;
width:542px; 
padding-top:15px; 
padding-left:15px; 
padding-right:15px;
padding-bottom:15px; 
font-size:11px;
float:left; 
margin-top:10px; 
background-color:#FEFDE1; 
border:solid 1px #EAE8C1;
}
</style>


<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
 



 
			
	        <div id="divContent">
       
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
		<table class="gridBorderB" width="100%"  border="0" cellpadding="10" cellspacing="0" bgcolor="#DAD8D9">
                      <tr>
					     <td>
							<div id="divButton" class="divButton" style="width: 115px;">
								<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
									<a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" alt="Back to Listing" width="4" height="8" hspace="0" vspace="0" border="0" class="marginR8"><strong>Back to Listing</strong></a>
								<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
							</div>
						</td>
                          <td width="176px" >							
						</td>
					 </tr>
         </table>
         
      <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline" UpdateMode="conditional">
         <ContentTemplate>
         <%--poonam added on 15/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2 --%>
            <div id="divValidationMain" class="summaryCSS" runat="server" visible="false"  style="width:830px;">			                
  <img src="Images/Note_Image.jpg" align="left">
  <div style="margin-left:80px;">
    <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
    <asp:ValidationSummary ValidationGroup="VG"  id="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false" ></asp:ValidationSummary>									
  
    </div>
</div>
	        
         <asp:Panel runat="server" ID="pnlNoNotesMsg" Visible="False">
            <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
				<tr>
					<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">
					    No comments have yet been submitted for this work order.
					</td>
				</tr>				
			</table>
         </asp:Panel>

        <asp:Panel runat="server" ID="pnlNotesList" Visible="False" >
                <asp:Repeater ID="rptNotes" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#993366">
                            <tr>                            
                                <td width="100" style="color:#FFFFFF; font-weight:bold; font-size:11px; border-right:solid 1px #EDEEE9;">Date Created</td>                            
                                <td width="100" style="color:#FFFFFF; font-weight:bold; font-size:11px; border-right:solid 1px #EDEEE9;">Written By</td>
                                <%--poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2 --%>
                                <td width="250" style="color:#FFFFFF; font-weight:bold; font-size:11px;">Note Type</td>
                                <td style="color:#FFFFFF; font-weight:bold; font-size:11px;">Note</td>
                                
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#EDEDED">
                            <tr>                            
                                <td width="100" class="bodyTextGreyLeftAligned" style="padding-left:5px; border-right:solid 1px #FFFFFF; border-bottom:solid 1px #FFFFFF;">
                                    <%#Strings.Format(Container.DataItem("DateCreated"), "dd/MM/yyyy <br /> hh:mm tt")%>                                
                                </td>                            
                                <td width="100" class="bodyTextGreyLeftAligned" style="padding-left:5px; border-bottom:solid 1px #FFFFFF; border-right:solid 1px #FFFFFF;">
                                    <%#Container.DataItem("OWRepFname")%> <%#Container.DataItem("OWRepLname")%>
                                </td>
                                <%--poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2 --%>
                                  <td width="250" class="bodyTextGreyLeftAligned" style="padding-left:5px; border-bottom:solid 1px #FFFFFF;">
                                    <%#Container.DataItem("NoteCategory")%>
                                </td>
                                <td  class="bodyTextGreyLeftAligned" style="padding-left:5px; border-bottom:solid 1px #FFFFFF;">
                                    <%#Container.DataItem("Comments")%>
                                </td>
                              
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
                
                       </asp:Panel>
                <br />
                <div style="margin-left:20px;">
                    <asp:TextBox runat="server" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" ID="txtNote" Height="50" Width="700" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtNote" WatermarkText="Enter a new note here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>
                   
                   
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNote" ValidationGroup="VG" EnableClientScript="true"  runat="server" ErrorMessage="Please Enter a Note" ControlToValidate="txtNote" ForeColor="#EDEDEB" Display="None" ></asp:RequiredFieldValidator>
               
                    <br />
                     <%--poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2 --%>
                     <asp:label text="Note type" runat="server" ID="lblNoteType" class="formLabelGrey"></asp:label>
                     <span class="bodytxtValidationMsg">*</span>
					                <br />
                         <asp:DropDownList  Width="280px" Height="25px" CssClass="bodyTextGreyLeftAligned" ID="ddlNoteType" runat="server" AutoPostBack="false"></asp:DropDownList> 


                    <asp:RequiredFieldValidator id="reqNoteType" ValidationGroup="VG" EnableClientScript="true" runat="server" ErrorMessage="Please Select Note Type." ControlToValidate="ddlNoteType" ForeColor="#EDEDEB" Display="None"></asp:RequiredFieldValidator>
				
                            <br /><br />
                        <div id="divButton" class="divButton" style="width: 115px;">
							<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
								<a class="buttonText" runat="server" id="ancSubmit" ><strong>Submit</strong></a>
                          
							<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
						</div>                    
                </div>
                
         
            </ContentTemplate>
           
         </asp:UpdatePanel>
         <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Saving ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>                
         <cc2:UpdateProgressOverlayExtender  ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      
      
  </asp:Content>