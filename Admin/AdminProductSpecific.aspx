﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminProductSpecific.aspx.vb" Inherits="Admin.AdminProductSpecific" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Admin Product"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>

        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
              <table width="725"  border="0" cellspacing="0" cellpadding="0" >    			
                <tr>
                  <td> 
                  <asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server">
                    <ContentTemplate>
                            <asp:Panel ID="pnlWoForm" runat="server" Visible="true">
                            <div id="divValidationMain" class="divValidation" runat=server visible="false" >
	                                <div class="roundtopVal">
	                                  <blockquote>
	                                    <p><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	                                    </blockquote>
	                                </div>
		                                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
			                                <tr valign="middle">
				                                <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				                                <td class="validationText"><div  id="divValidationMsg"></div>
				                                <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
				                                <asp:ValidationSummary id="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList"></asp:ValidationSummary>
				                                </td>
				                                <td width="20">&nbsp;</td>
			                                </tr>
		                                </table>
	                                <div class="roundbottomVal">
	                                  <blockquote>
	                                    <p><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	                                    </blockquote>
	                                </div>
                            </div>     
                            <TABLE width="100%" border="0" align="center" cellPadding="0" cellSpacing="0" style="PADDING-RIGHT:0px; PADDING-LEFT:0px; PADDING-BOTTOM:16px; MARGIN:0px; PADDING-TOP:0px">
	                            <TR>
	                            <TD width="300" align="right" vAlign="top"><asp:Label ID="lblMessage" runat="server" CssClass="bodytxtRedSmall " style="font-weight:bold;"></asp:Label></TD>
	                            <TD width="100" align="right" vAlign="top">&nbsp;</TD>
	                            <TD width="130" align="right" vAlign="top">
	                            <TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0" width="130">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD width="120" class="txtBtnImage">
			                              <asp:LinkButton ID="btnConfirmTop" runat="server" class="txtListing" CausesValidation="false" OnClientClick="callSetFocusValidation('UCTemplateWO1')" > <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:LinkButton>
		                              </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
	                            </TD>
	                            <TD width="10"></TD>
	                            <TD vAlign="top" align="right" width="80">
		                            <TABLE cellSpacing="0" cellPadding="0"  bgColor="#f0f0f0" border="0">
			                            <TR>
				                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
				                            <TD class="txtBtnImage" width="120">
					                            <asp:Linkbutton id="btnResetTop"  runat="server" CausesValidation="false" CssClass="txtListing"><img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton>
				                            </TD>
				                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
			                            </TR>
		                            </TABLE>
	                            </td>
	                            <td width="10">&nbsp;</td>
	                            <td vAlign="top" align="right" width="60">
	                                <TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD width="80" class="txtBtnImage">
			                              <asp:LinkButton ID="btnCancelTop" runat="server" class="txtListing" CausesValidation="false"> <img src="Images/Icons/Icon-Cancel.gif" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
		                              </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
	                            </td>
	                            </TR>
                              </TABLE> 

                              <div class="tabDivOuter" style="width:1200px;" id="divMainTab">
                                <div class="roundtopLightGreyCurve"  ><img src="Images/Curves/LightGrey-LTC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
                                <DIV id="divProfile" class="paddedBox" style="height:100%" >
                                <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td height="35" class="HeadingRed">
	                                   <strong><asp:Label ID="lblHeading" runat="server" Text="Add / Edit Service" ></asp:Label></strong>
                                       <div style="float:right;text-align:right;">
                                       <div> <a id="lnkAddEditServiceQA" runat="server" class="footerTxtSelected">Service Questions</a></div>                                       
                                       </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                   <tr>
                                    <td height="45">&nbsp;</td>
                                  </tr>
                                </table>
                                 </DIV>
                                </div>

                                <TABLE width="100%" border="0" align="center" cellPadding="0" cellSpacing="0" style="PADDING-RIGHT:0px; PADDING-LEFT:0px; PADDING-BOTTOM:0px; MARGIN:0px; PADDING-TOP:16px">
                                <TR>
                                <TD width="400" align="right" vAlign="top">&nbsp;</TD>
                                <TD width="130" align="right" vAlign="top">
                                <TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0">
		                                <TR>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                                <TD width="120" class="txtBtnImage">
			                                  <asp:LinkButton ID="btnConfirmBtm" runat="server" class="txtListing" CausesValidation="false" OnClientClick="callSetFocusValidation('UCTemplateWO1')"> <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:LinkButton>
			                                </TD>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                                </TR>
	                                </TABLE>
                                </TD>
                                <TD width="10">&nbsp;</TD>
                                <TD vAlign="top" align="right" width="80">
	                                <TABLE cellSpacing="0" cellPadding="0"  bgColor="#f0f0f0" border="0">
		                                <TR>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                                <TD class="txtBtnImage" width="120">
				                                <asp:Linkbutton id="btnResetBtm"  runat="server" CausesValidation="false" CssClass="txtListing"><img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton>
			                                </TD>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                                </TR>
	                                </TABLE>
                                </td>
                                <TD width="10">&nbsp;</TD>
                                <td vAlign="top" align="right" width="60">
	                                    <TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0">
		                                <TR>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                                <TD width="80" class="txtBtnImage">
			                                  <asp:LinkButton ID="btnCancelBtm" runat="server" class="txtListing" CausesValidation="false"> <img src="Images/Icons/Icon-Cancel.gif" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
		                                  </TD>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                                </TR>
	                                </TABLE>
	                                </td>
                                </TR>
                                </TABLE>
                            </asp:Panel>
                    </ContentTemplate>
                   </asp:UpdatePanel>
				  </td>
                </tr>
              </table>
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>

 </asp:Content>

