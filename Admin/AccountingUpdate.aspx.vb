

Partial Public Class AccountingUpdate
    Inherits System.Web.UI.Page

    ''' <summary>
    ''' Shared wsobject to access web service webmethods
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID Then
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Include VAT Amount"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Update Wholesale Price"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Update Portal Price"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Create Upsell Invoice"))
        ElseIf Session("RoleGroupID") = ApplicationSettings.RoleOWRepID Or Session("RoleGroupID") = ApplicationSettings.RoleOWManagerID Then
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Update Invoice date"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Update Invoice PO"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Include VAT Amount"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Update Wholesale Price"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Update Portal Price"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Update Rating"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Change Cash Receipt value"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Change Upsell value"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Change Invoice Title"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Create Upsell Invoice"))
            drpdwnAccountingUpdate.Items.Remove(drpdwnAccountingUpdate.Items.FindByText("Change Quantity"))
        End If
    End Sub

    Protected Sub drpdwnAccountingUpdate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpdwnAccountingUpdate.SelectedIndexChanged
        pnlUpdateInvoiceDate.Visible = False
        pnlUpdateVATDetails.Visible = False
        pnlUpdatePrice.Visible = False
        pnlUpdateUpsellInvoice.Visible = False
        pnlMsg.Visible = False
        pnlUpdatePONum.Visible = False
        pnlUpdateRatings.Visible = False
        pnlChangeCashReceipt.Visible = False
        pnlUpdateInvoiceTitle.Visible = False
        pnlUpdateQuantity.Visible = False
        Select Case drpdwnAccountingUpdate.SelectedItem.Value
            Case "UpdateDate"
                pnlUpdateInvoiceDate.Visible = True
            Case "UpdateVAT"
                pnlUpdateVATDetails.Visible = True
            Case "UpdateWPPrice"
                pnlUpdatePrice.Visible = True
                lblPriceText.Text = "Wholesale Price"
                txtWOID.Text = ""
                txtOrigPrice.Text = ""
                txtPrice.Text = ""
                divOriginalPrice.Visible = False

            Case "UpdatePFPrice"
                pnlUpdatePrice.Visible = True
                lblPriceText.Text = "Portal Price"
                txtWOID.Text = ""
                txtOrigPrice.Text = ""
                txtPrice.Text = ""
                divOriginalPrice.Visible = False
            Case "UpdateUpsellInvoice"
                pnlUpdateUpsellInvoice.Visible = True
                txtUpSellPrice.Text = ""
                txtUpdateUpsellInvoiceWOID.Text = ""
            Case "UpdatePONum"
                pnlUpdatePONum.Visible = True
            Case "UpdateInvoicePO"
                PnlUpdateInvoicePO.Visible = True
            Case "UpdateQuantity"
                pnlUpdateQuantity.Visible = True
            Case "UpdateRatings"
                pnlUpdateRatings.Visible = True
                divRatingBox.Visible = True
                divRating.Visible = False
                divSelectRatings.Visible = False
                lblRatingText.Visible = False
                txtWOIDRatings.Text = ""
                rdBtnNegative.Checked = False
                rdBtnNeutral.Checked = False
                rdBtnPositive.Checked = False
                rdoNegative.Checked = False
                rdoNeutral.Checked = False
                rdoPositive.Checked = False
                rdoNegative.Enabled = True
                rdoNeutral.Enabled = True
                rdoPositive.Enabled = True
            Case "UpdateReceipt"
                pnlChangeCashReceipt.Visible = True
                txtbxReceiptNo.Text = ""
                txtbxReceiptVal1.Text = ""
                txtbxReceiptVal2.Text = ""
                lbl.Text = "Cash Receipt Number"
                RequiredFieldValidator6.ErrorMessage = "Please enter a sales cash receipt"
            Case "UpdateUpsell"
                pnlChangeCashReceipt.Visible = True
                txtbxReceiptNo.Text = ""
                txtbxReceiptVal1.Text = ""
                txtbxReceiptVal2.Text = ""
                lbl.Text = "Upsell Invoice Number"
                RequiredFieldValidator6.ErrorMessage = "Please enter a Upsell sales Invoice Number"
            Case "UpdateInvoiceTitle"
                pnlUpdateInvoiceTitle.Visible = True
            Case Else
                pnlUpdateInvoiceDate.Visible = False
        End Select
    End Sub
    Private Sub btnUpdateUpsellInvoice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateUpsellInvoice.Click
        Dim Result As Integer
        Result = ws.WSFinance.UpdateUpsellInvoice(txtUpdateUpsellInvoiceWOID.Text.Trim, CDec(txtUpSellPrice.Text.Trim), Session("UserId"))
        pnlUpdateUpsellInvoice.Visible = False
        txtUpdateUpsellInvoiceWOID.Text = ""
        txtUpSellPrice.Text = ""
        pnlMsg.Visible = True
        Select Case Result
            Case 0
                lblMsg.Text = "You cannot create a new upsell invoice to this workorder, it already has one. use 'Change upsell value' under the 'Accounting Update' to change the value of the upsell."
            Case 1
                lblMsg.Text = "Upsell Invoice updated successfully."
        End Select
        drpdwnAccountingUpdate.SelectedValue = "0"
    End Sub
    Private Sub btnUpdatePrice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePrice.Click
        Dim type As String
        Dim Success As DataSet
        Dim UpdatedField As String
        If drpdwnAccountingUpdate.SelectedItem.Value = "UpdateWPPrice" Then
            type = "WPChange"
            UpdatedField = "Wholesale Price"
        ElseIf drpdwnAccountingUpdate.SelectedItem.Value = "UpdatePFPrice" Then
            type = "PPChange"
            UpdatedField = "Portal Price"
        Else
            type = "PFChange"
            UpdatedField = "Portal Price"
        End If
        Success = ws.WSFinance.UpdatePrice(Session("BizDivId"), txtWOID.Text.Trim, CDec(txtPrice.Text.Trim), type, Session("UserId"))
        pnlUpdateInvoiceDate.Visible = False
        pnlUpdateVATDetails.Visible = False
        'pnlUpdatePrice.Visible = False
        pnlMsg.Visible = True
        Dim Result As Integer
        Dim oldPrice As Double
        If (Success.Tables(0).Rows.Count > 0) Then
            Result = Convert.ToInt16(Success.Tables(0).Rows(0)("Success"))
            oldPrice = Convert.ToDouble(Success.Tables(0).Rows(0)("OldPrice"))
        End If
        Select Case Result
            Case 1
                lblMsg.Text = ResourceMessageText.GetString("SuccessMsgAccoutingUpdate")
                pnlUpdatePrice.Visible = False
                'Send email to finance team
                Emails.SendEmailAccountingUpdate("Workorder", txtWOID.Text.Trim, UpdatedField, Success.Tables(0).Rows(0)("OldPrice").ToString, Success.Tables(0).Rows(0)("NewPrice").ToString, Success.Tables(0).Rows(0)("LoggedInUserName").ToString)
            Case 2
                lblMsg.Text = ResourceMessageText.GetString("InvalidWOIDMsg")
                txtWOID.Text = ""
            Case 3
                lblMsg.Text = ResourceMessageText.GetString("FailMsgAccountingUpdate")
            Case 4
                lblMsg.Text = ResourceMessageText.GetString("SalesInvMsg")
                txtWOID.Text = ""
            Case 5
                lblMsg.Text = ResourceMessageText.GetString("CompNotVATRegistered")
                txtWOID.Text = ""
            Case 7
                If oldPrice > 0 Then
                    lblMsg.Text = "This Workorder's PP cannot be updated because it has already been closed."
                Else
                    lblMsg.Text = "This Workorder's PP cannot be updated from 0 to a positive value because it has already been closed."
                End If


        End Select
        drpdwnAccountingUpdate.SelectedValue = "0"
    End Sub

    Private Sub btnUpdateVAT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateVAT.Click
        Dim Result As Integer
        Result = ws.WSFinance.UpdateVATDetails(Session("BizDivId"), txtPurchaseInvoiceNumber.Text.Trim, Session("UserId"))
        pnlUpdateInvoiceDate.Visible = False
        'pnlUpdateVATDetails.Visible = False
        pnlUpdatePrice.Visible = False
        pnlMsg.Visible = True
        Select Case Result
            Case 1
                lblMsg.Text = ResourceMessageText.GetString("SuccessMsgAccoutingUpdate")
                pnlUpdateVATDetails.Visible = False
            Case 7
                lblMsg.Text = ResourceMessageText.GetString("InvalidInvoiceMsg")
                txtPurchaseInvoiceNumber.Text = ""
            Case 3
                lblMsg.Text = ResourceMessageText.GetString("AlreadyVATMsg")
                txtPurchaseInvoiceNumber.Text = ""
            Case 4
                lblMsg.Text = ResourceMessageText.GetString("FailMsgAccountingUpdate")
            Case 5
                lblMsg.Text = ResourceMessageText.GetString("CompNotVATRegistered")
                txtPurchaseInvoiceNumber.Text = ""
            Case 8
                lblMsg.Text = ResourceMessageText.GetString("VatOnlyUpdatedForPurchase")
                txtPurchaseInvoiceNumber.Text = ""
        End Select
        drpdwnAccountingUpdate.SelectedValue = "0"
    End Sub

    Private Sub btnUpdateInvoiceDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateInvoiceDate.Click
        Dim Success As Integer
        Dim result As DataSet
        result = ws.WSFinance.UpdateAccountsInvoiceDate(Session("BizDivId"), txtInvoiceNumber.Text.Trim, txtNewInvoiceDate.Text.Trim, Session("UserId"))
        'pnlUpdateInvoiceDate.Visible = False
        pnlUpdateVATDetails.Visible = False
        pnlUpdatePrice.Visible = False
        pnlMsg.Visible = True
        If (result.Tables(0).Rows.Count > 0) Then
            Success = Convert.ToInt16(result.Tables(0).Rows(0)("Success"))
        End If
        Select Case Success
            Case 1
                lblMsg.Text = ResourceMessageText.GetString("SuccessMsgAccoutingUpdate")
                pnlUpdateInvoiceDate.Visible = False
                'Send email to finance team
                Emails.SendEmailAccountingUpdate("Invoice", txtInvoiceNumber.Text.Trim, "Date", result.Tables(0).Rows(0)("OldDate").ToString, result.Tables(0).Rows(0)("NewDate").ToString, result.Tables(0).Rows(0)("LoggedInUserName").ToString)
            Case 2
                lblMsg.Text = ResourceMessageText.GetString("InvalidInvoiceMsg")
                txtInvoiceNumber.Text = ""
            Case 3
                lblMsg.Text = ResourceMessageText.GetString("FailMsgAccountingUpdate")
        End Select
        drpdwnAccountingUpdate.SelectedValue = "0"
    End Sub

    ''' <summary>
    ''' This function is used to call the original price of the WO whose id has been entered in the textbox.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Pratik Trivedi: 25 Aug, 2008</remarks>
    Private Sub txtWOID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWOID.TextChanged
        Dim WOID As String
        Dim Type As Int32
        If txtWOID.Text <> "" Then
            WOID = txtWOID.Text
            If drpdwnAccountingUpdate.SelectedValue = "UpdateWPPrice" Then
                Type = 1 'Wholesale Price
            ElseIf drpdwnAccountingUpdate.SelectedValue = "UpdatePFPrice" Then
                Type = 2 'Platform Price
            End If
            getOriginalWOPrice(WOID, Type)
        Else
            divOriginalPrice.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' This function gets the Original price of the WO and displays on the Accounting Update page.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Mode"></param>
    ''' <remarks></remarks>
    Private Sub getOriginalWOPrice(ByVal WOID As String, ByVal Mode As Int32)
        Dim ds As New DataSet
        ds = ws.WSWorkOrder.GetWOPrice(WOID, Mode)
        If ds.Tables(0).Rows.Count <> 0 Then
            If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
                txtOrigPrice.Text = ds.Tables(0).Rows(0)(0)
                divOriginalPrice.Visible = True
            End If
        End If

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If txtWOIDPONum.Text <> "" Then
            Dim ds As New DataSet
            ds = ws.WSWorkOrder.GetPONumber(txtWOIDPONum.Text)
            If ds.Tables(0).Rows.Count <> 0 Then
                txtCurrPONum.Text = ds.Tables(0).Rows(0)(0)
                divUpdatePONum.Visible = True
                lblPOError.Visible = False
            Else
                lblPOError.Visible = True
            End If
        End If

    End Sub
    Private Sub btnSubmitQuantity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitQuantity.Click
        If txtWOIDQuantity.Text <> "" Then
            Dim ds As New DataSet
            ds = ws.WSWorkOrder.GetPONumber(txtWOIDQuantity.Text)
            If ds.Tables(0).Rows.Count <> 0 Then
                txtCurrQuantity.Text = ds.Tables(0).Rows(0)("Quantity")
                divUpdateQuantity.Visible = True
                lblQuantityError.Visible = False
            Else
                lblQuantityError.Visible = True
            End If
        End If

    End Sub

    Private Sub txtWOIDInvTitle_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWOIDInvTitle.TextChanged
        If txtWOIDInvTitle.Text <> "" Then
            Dim ds As New DataSet
            ds = ws.WSWorkOrder.GetPONumber(txtWOIDInvTitle.Text)
            If ds.Tables(0).Rows.Count <> 0 Then
                txtCurTitle.Text = ds.Tables(0).Rows(0)("InvoiceTitle")
                divTitle.Visible = True
                lblInvoiceTitle.Visible = False
            Else
                lblInvoiceTitle.Visible = True
            End If
        Else
            lblInvoiceTitle.Visible = True
        End If
    End Sub

    Private Sub btnUpdatePONum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePONum.Click
        Dim ds As New DataSet
        ds = ws.WSWorkOrder.UpdatePONumber(txtWOIDPONum.Text, txtNewPONum.Text, Session("UserId"), "", "")
        drpdwnAccountingUpdate.SelectedValue = "0"
        Response.Redirect("AccountingUpdate.aspx")
    End Sub
    Private Sub btnUpdateQuantity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateQuantity.Click
        Dim ds As New DataSet
        ds = ws.WSWorkOrder.UpdatePONumber(txtWOIDQuantity.Text, txtNewQuantity.Text, Session("UserId"), "UpdateQuantity", "")
        drpdwnAccountingUpdate.SelectedValue = "0"
        Response.Redirect("AccountingUpdate.aspx")
    End Sub
    Private Sub btnCancelPONum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelPONum.Click
        drpdwnAccountingUpdate.SelectedValue = "0"
        Response.Redirect("AccountingUpdate.aspx")
    End Sub
    Private Sub btnCancelQuantity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelQuantity.Click
        drpdwnAccountingUpdate.SelectedValue = "0"
        Response.Redirect("AccountingUpdate.aspx")
    End Sub
    Private Sub btnUpdateRatings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateRatings.Click
        If rdoPositive.Checked = False And rdoNeutral.Checked = False And rdoNegative.Checked = False Then
            pnlMsg.Visible = True
            lblMsg.Text = ResourceMessageText.GetString("RatingsValidation")
        Else
            Dim Rating As Integer
            If rdoNegative.Checked = True Then
                Rating = -1
            ElseIf rdoPositive.Checked = True Then
                Rating = 1
            Else
                Rating = 0
            End If



            Dim WOID As String = txtWOIDRatings.Text
            Dim Mode As String = "Update"
            Dim LoggedInUserID As Integer = Session("UserId")
            Dim ds As New DataSet
            ds = ws.WSWorkOrder.UpdateRating(WOID, Mode, Rating, LoggedInUserID)

            divRatingBox.Visible = False
            'lblRatingText.Visible = True
            pnlMsg.Visible = True

            If ds.Tables(0).Rows.Count <> 0 Then


                If (ds.Tables(0).Rows(0)("Success") = 1) Then
                    lblMsg.Text = ResourceMessageText.GetString("UpdateRatings")
                ElseIf (ds.Tables(0).Rows(0)("Success") = -1) Then
                    lblMsg.Text = "No Supplier available for this Workorder to rate."
                Else
                    lblMsg.Text = ResourceMessageText.GetString("UpdateRatingsFail")
                End If
            End If
        End If
        drpdwnAccountingUpdate.SelectedValue = "0"

    End Sub
    ''' <summary>
    ''' This function is used to call the original price of the WO whose id has been entered in the textbox.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Snehal Pasarkar:  6 Nov 2009</remarks>
    Private Sub txtWOIDRatings_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWOIDRatings.TextChanged
        Dim WOID As String
        Dim Mode As String
        If txtWOIDRatings.Text <> "" Then
            WOID = txtWOIDRatings.Text
            If drpdwnAccountingUpdate.SelectedValue = "UpdateRatings" Then
                Mode = "Get"
            End If
            getRatingsWO(WOID, Mode)
        Else
            'Enter WOID
        End If
    End Sub
    ''' <summary>
    ''' This function gets the  Ratings of the WO.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Mode"></param>
    ''' <remarks></remarks>
    Private Sub getRatingsWO(ByVal WOID As String, ByVal Mode As String)

        rdBtnPositive.Checked = False
        rdBtnNeutral.Checked = False
        rdBtnNegative.Checked = False

        rdoPositive.Enabled = True
        rdoNegative.Enabled = True
        rdoNeutral.Enabled = True

        Dim ds As New DataSet
        Dim Rating As Integer = 0
        ds = ws.WSWorkOrder.UpdateRating(WOID, Mode, Rating, 0)
        If Not IsDBNull(ds) Then
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
                        Dim Result As Integer = ds.Tables(0).Rows(0)(0)
                        Select Case Result
                            Case -1
                                rdBtnNegative.Checked = True
                                rdoNegative.Enabled = False
                                divRating.Visible = True
                            Case 0
                                rdBtnNeutral.Checked = True
                                rdoNeutral.Enabled = False
                                divRating.Visible = True
                            Case 1
                                rdBtnPositive.Checked = True
                                rdoPositive.Enabled = False
                                divRating.Visible = True
                            Case -2
                                rdBtnPositive.Checked = False
                                rdBtnNegative.Checked = False
                                rdBtnNeutral.Checked = False
                                rdoPositive.Enabled = True
                        End Select

                        divSelectRatings.Visible = True
                        pnlMsg.Visible = False
                    End If
                Else
                    pnlMsg.Visible = True
                    lblMsg.Text = ResourceMessageText.GetString("UpdateRatingsFail")
                    divRating.Visible = False
                    divSelectRatings.Visible = False
                End If
            Else
                pnlMsg.Visible = True
                lblMsg.Text = ResourceMessageText.GetString("UpdateRatingsFail")
                divRating.Visible = False
                divSelectRatings.Visible = False
            End If
        End If
    End Sub

    ''' <summary>
    ''' Function to validate and populate Unallocated Cash Receipt
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtbxReceiptNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbxReceiptNo.TextChanged
        Dim InvoiceNO As String
        InvoiceNO = txtbxReceiptNo.Text.Trim
        Dim ds As New DataSet
        If drpdwnAccountingUpdate.SelectedItem.Value = "UpdateUpsell" Then
            ds = ws.WSFinance.MS_GetInvoiceValue(InvoiceNO, "UpSellSales")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Not IsDBNull(ds.Tables(0).Rows(0)("WOVal")) Then
                    pnlMsg.Visible = False
                    txtbxReceiptVal1.Text = ds.Tables(0).Rows(0)("WOVal")
                    divCashReceiptVal1.Visible = True
                    divCashReceiptVal2.Visible = True
                    btnChangeCashReceipt.Visible = True
                Else
                    pnlMsg.Visible = True
                    divCashReceiptVal2.Visible = False
                    btnChangeCashReceipt.Visible = False
                    lblMsg.Text = "Please enter a valid UpSell Sales Invoice"
                End If
            Else
                pnlMsg.Visible = True
                divCashReceiptVal2.Visible = False
                btnChangeCashReceipt.Visible = False
                lblMsg.Text = "Please enter a valid UpSell Sales Invoice"
            End If
        Else
            ds = ws.WSFinance.MS_GetInvoiceValue(InvoiceNO, "UA Receipt")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Not IsDBNull(ds.Tables(0).Rows(0)("Total")) Then
                    If ds.Tables(0).Rows(0)("Status") = "UnAllocated" Then
                        pnlMsg.Visible = False
                        txtbxReceiptVal1.Text = ds.Tables(0).Rows(0)("Total")
                        divCashReceiptVal1.Visible = True
                        divCashReceiptVal2.Visible = True
                        btnChangeCashReceipt.Visible = True
                    Else
                        pnlMsg.Visible = True
                        divCashReceiptVal2.Visible = False
                        btnChangeCashReceipt.Visible = False
                        lblMsg.Text = "This cash receipt is marked as Allocated. Only Unallocated Cash Receipts can be updated"
                    End If
                Else
                    pnlMsg.Visible = True
                    divCashReceiptVal2.Visible = False
                    btnChangeCashReceipt.Visible = False
                    lblMsg.Text = "Please enter a valid sales cash receipt"
                End If
            Else
                pnlMsg.Visible = True
                divCashReceiptVal2.Visible = False
                btnChangeCashReceipt.Visible = False
                lblMsg.Text = "Please enter a valid sales cash receipt"
            End If

        End If

    End Sub

    ''' <summary>
    ''' Onclick of change price for Invoices
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnChangeCashReceipt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangeCashReceipt.Click
        Dim type As String
        Dim Result As Integer
        If drpdwnAccountingUpdate.SelectedItem.Value = "UpdateUpsell" Then
            type = "UpSellSales"
        Else
            type = "UA Receipt"
        End If


        Result = ws.WSFinance.UpdateInvoicePrice(Session("BizDivId"), txtbxReceiptNo.Text.Trim, CDec(txtbxReceiptVal2.Text.Trim), type, Session("UserId"))
        pnlUpdateInvoiceDate.Visible = False
        pnlUpdateVATDetails.Visible = False
        pnlChangeCashReceipt.Visible = False
        pnlMsg.Visible = True
        Select Case Result
            Case 1
                lblMsg.Text = ResourceMessageText.GetString("SuccessMsgAccoutingUpdate")
                pnlChangeCashReceipt.Visible = False
            Case Else
                lblMsg.Text = "Something is not right. Please try later"
        End Select
        drpdwnAccountingUpdate.SelectedValue = "0"
    End Sub

    Private Sub btnUpdateInvoiceTitle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateInvoiceTitle.Click
        Dim ds As DataSet
        ds = ws.WSWorkOrder.UpdatePONumber(txtWOIDInvTitle.Text, txtInvoiceTitleNew.Text, Session("UserId"), "UpdateInvTitle", "")
        pnlUpdateInvoiceTitle.Visible = False
        pnlMsg.Visible = True
        If ds.Tables(0).Rows(0)("Success") = 1 Then
            lblMsg.Text = ResourceMessageText.GetString("SuccessMsgAccoutingUpdate")
        Else
            lblMsg.Text = "Something is not right. Please try later"
        End If
        drpdwnAccountingUpdate.SelectedValue = "0"
    End Sub

    Protected Sub btnCancelInvoicePO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelInvoicePO.Click
        drpdwnAccountingUpdate.SelectedValue = "0"
        Response.Redirect("AccountingUpdate.aspx")
    End Sub

    Protected Sub btnUpdateInvoicePO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateInvoicePO.Click
        Dim Success As Integer
        Success = ws.WSFinance.UpdateAccountsInvoicePO(Session("BizDivId"), txtInvoiceNumber1.Text, txtNewInvoicePO.Text, Session("UserId"))
        PnlUpdateInvoicePO.Visible = False
        pnlMsg.Visible = True
        If Success = 1 Then
            lblMsg.Text = ResourceMessageText.GetString("SuccessMsgAccoutingUpdate")
        Else
            lblMsg.Text = "Something is not right. Please try later"
        End If
        txtInvoiceNumber1.Text = ""
        txtInvoiceNumber1.Visible = False
        txtNewInvoicePO.Text = ""
        drpdwnAccountingUpdate.SelectedValue = "0"
    End Sub

    'Protected Sub btnGetInvoicePO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetInvoicePO.Click

    'End Sub
End Class