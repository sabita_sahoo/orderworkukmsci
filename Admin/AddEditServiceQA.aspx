﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddEditServiceQA.aspx.vb" Inherits="Admin.AddEditServiceQA" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Add/Edit Service Question/Answer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanelServiceQ" runat="server" >
<ContentTemplate>
    <div id="divContent">
         <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
         
         <tr>
            <td valign="top">
             <table cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
                <tr>
                  <td width="165" class="txtWelcome paddingL10"><div id="divButton" style="width: 120px;  ">
                      <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                      <a class="buttonText" runat="server" id="lnkBackToService"><img src="Images/Arrows/Arrow-Back.gif" title="Back to Company Profile" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8" /><strong>Back to Service</strong></a>
                      <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                  </div></td>                  
                </tr>
             </table>
            </td>
         </tr>
         </table>
         <div style="margin:0px 5px 0px 5px;" id="divMainTab">
            <div class="paddedBox"  id="divProfile" >
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="left" class="HeadingRed"><strong>Service Questions</strong></td>
                    <td align="left" class="padTop17">
                     <asp:Panel ID="pnlRemoveallServiceQ" runat="server" Visible="false">                            
                                    <TABLE cellSpacing="0" cellPadding="0" width="140" bgColor="#f0f0f0" border="0" style="float:left;">
		                                <TR>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                                <TD class="txtBtnImage" width="120">
				                                <asp:Linkbutton id="lnkBtnRemoveallServiceQ"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Remove All Questions&nbsp;</asp:LinkButton>
			                                </TD>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                                </TR>
	                                 </TABLE>
                    </asp:Panel>       
                    <table id="tblSaveTop" runat="server" cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                <tr>
                  <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveTop" ValidationGroup="VGAcc" CssClass="txtListing" runat="server" TabIndex="67"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                  <td width="10"></td>
                  <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetTop" CausesValidation="false" runat="server" TabIndex="68"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                </tr>
            </table>            
                    </td>
                  </tr>
             </table>

              <div class="divWorkOrder" style="margin-top:17px;float:left;">	      
                    <div id="divValidationServiceQuestions" class="divValidation" runat=server visible="false" >
	                    <div class="roundtopVal">
	                      <blockquote>
	                        <p><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	                        </blockquote>
	                    </div>
		                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
			                    <tr valign="middle">
				                    <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				                    <td class="validationText"><div  id="divValidationMsg"></div>
				                        <asp:Label ID="lblValidationServiceQuestions"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>				                    
				                    </td>
				                    <td width="20">&nbsp;</td>
			                    </tr>
		                    </table>
	                    <div class="roundbottomVal">
	                      <blockquote>
	                        <p><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	                        </blockquote>
	                    </div>
                </div>                             
                  <table width="100%" cellspacing="0" cellpadding="0" border="0">	
                     <tr>
				        <td>				       				       
				          <div id = "divOuterpnlServiceQuestions" class="divOuterpnlServiceQuestions">
                           <div id="divOuterTab1" runat="server">
                                <table width="100%">
                           <tr>
                            <td>                           
                             <TABLE cellSpacing="0" cellPadding="0" width="92" bgColor="#f0f0f0" border="0" style="float:right;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkAddServiceTab1Q"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Add Question&nbsp;</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
                            </td>
                           </tr>
                           </table>
                                <asp:Repeater ID="rptServiceTab1Q" runat="server" >
                                <ItemTemplate>
                                     <table style="padding:5px;margin-top:10px;border:solid 1px black;">
                                     <tr>
                                      <td>Q<%# Container.DataItem("RowNum")%>                               
                                       <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />            
                                       <input type="hidden" id="hdnSequence" runat="server" value=<%#Container.dataitem("Sequence")%> />            
                                                                            
                                       &nbsp;<asp:TextBox ID="txtServiceQ" runat="server" CssClass="formField" Width="750" Text=<%#Container.dataitem("Question")%>></asp:TextBox>
                                      <TABLE cellSpacing="0" cellPadding="0" width="60" bgColor="#f0f0f0" border="0" style="float:right;margin-left:20px;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkBtnRemoveServiceQ"  runat="server" CausesValidation="false" CssClass="txtListing" CommandName="RemoveQuestion" CommandArgument='<%#Container.DataItem("QuestionId") %>'>&nbsp;Remove&nbsp;</asp:LinkButton>
                                            
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                        <TD>
                                            <div style="width:50px; height:26px;float:left;margin-right:10px;margin-left:-20px;">
                                                <div style="float:left; width:24px; height:26px; margin-right:7px;"><asp:LinkButton ID="lnkBtnMoveUp" Visible='<%#IIF(Container.DataItem("Sequence") = 1 , False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>' CommandName="MoveUp" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:35px;"><img id="imgMoveUp" runat="server" src="~/Images/Arrows/arrow-up.jpg" title="Move Up" border="0" class="cursorHand" /></asp:LinkButton></div>
                                                <div style="float:left; width:10px; height:26px;"><asp:LinkButton ID="lnkBtnMoveDown" Visible='<%#IIF(Container.DataItem("Sequence") = Container.DataItem("MaxSequence"), False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>'  CommandName="MoveDown" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:20px;"><img id="imgMoveDown" runat="server" src="~/Images/Arrows/arrow-down.jpg" title="Move Down" border="0" class="cursorHand" /></asp:LinkButton></div>
                                            </div>                                        
                                        </TD>
		                            </TR>
	                            </TABLE>
                                      </td>
					              </tr>
                                  <tr>
                                    <td>                                       
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="95">Is Mandatory</td>                                              
                                                <td>Qty Ans</td>
                                            </tr>
                                            
                                        </table>      
                                        <table>
                                            <tr style="vertical-align:top;">                                               
                                                <td width="100"><asp:CheckBox ID="chkIsMandatory" valign="top" runat="server" Text="Is Mandatory" Checked=<%#Container.dataitem("IsMandatory")%> /> </td>
                                                <td>
                                                    <asp:DropDownList id="ddQAnsCount" runat="server" CssClass="formFieldGrey" Width="35" AutoPostBack="true" OnSelectedIndexChanged="ddQAnsCount_SelectedIndexChanged" TabIndex=<%#Container.dataitem("QuestionId")%>>               
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>                    
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>                         
                                    </td>
                                  </tr>
                                  <tr>
                                        <td>
                                             <table style="padding:5px;margin-top:10px;border:dashed 1px black;">
                                               <tr>
                                                <td>
                                                 <asp:Repeater ID="rptServiceQuestionAnswers" runat="server" >                                        
                                                      <ItemTemplate>
                                                        Ans<%# Container.DataItem("RowNum")%>                                            
                                                        <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />              
                                                        <input type="hidden" id="hdnAnswerId" runat="server" value=<%#Container.dataitem("AnswerId")%> />                                                                                              
                                                        &nbsp;<asp:TextBox ID="txtAnswer" runat="server" CssClass="formField width150" Text=<%#Container.dataitem("Answer")%>></asp:TextBox>
                                                        &nbsp;&nbsp;Message<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerMessage" runat="server" CssClass="formField width150" TextMode="MultiLine" Rows="4"  Width="400" Text=<%#Container.dataitem("AnswerMessage").Replace("<BR>",chr(10))%>></asp:TextBox>                                                                                                                                                     
                                                        &nbsp;&nbsp;<asp:CheckBox ID="chkStopBooking" valign="top" runat="server" Text="Stops the booking" Checked=<%#Container.dataitem("StopBooking")%> />
                                                        <br />
                                                     </ItemTemplate>
                                                </asp:Repeater>
                                              </td>
                                            </tr>
                                            </table>
                                        </td>
                                     </tr>					     
				                    </table>
                             </ItemTemplate>
                            </asp:Repeater>
                            </div>                         
                          </div>
				        </td>
				      </tr>				      

                 </table>		
		     </div>       
             
             <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
              <td align="left" class="HeadingRed">&nbsp;</td>
              <td align="left" class="padTop17"><table id="tblSaveBottom" runat="server" cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                  <tr>
                    <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveBottom" ValidationGroup="VGAcc"  CssClass="txtListing" runat="server" TabIndex="63"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                        </tr>
                    </table></td>
                    <td width="10"></td>
                    <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetBottom"  CausesValidation="false" runat="server" TabIndex="64"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                        </tr>
                    </table></td>
                  </tr>
              </table></td>
            </tr>
          </table>       
            </div>          
               
         </div>
      </div>
</ContentTemplate>								
</asp:UpdatePanel>
</asp:Content>