Partial Public Class RetrievePassword
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
    End Sub



    Private Sub btnDecrypt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecrypt.Click
        Dim str As String = Trim(TextBox1.Text)
        str = HttpUtility.UrlDecode(str)
        Label1.Text = Encryption.Decrypt(HttpUtility.HtmlEncode(str).Replace(" ", "+"))
    End Sub
    Private Sub btnEncrypt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEncrypt.Click
        Dim str As String = Trim(TextBox1.Text)
        Label1.Text = Encryption.Encrypt(str)
    End Sub
    Private Sub btnDecrypt_DES_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecrypt_DES.Click
        Dim str As String = Trim(TextBox1.Text)
        str = HttpUtility.UrlDecode(str)
        Label1.Text = OrderWorkLibrary.Security.DecryptURL_DES(HttpUtility.HtmlEncode(str).Replace(" ", "+"), Page)
    End Sub
    Private Sub btnEncrypt_DES_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEncrypt_DES.Click
        Dim str As String = Trim(TextBox1.Text)
        Label1.Text = OrderWorkLibrary.Security.EncryptURL_DES(str)
    End Sub
    Private Sub btnEncryptMD5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEncryptMD5.Click
        Dim str As String = Trim(TextBox1.Text)
        Label1.Text = Encryption.EncryptToMD5Hash(str)
    End Sub

    Private Sub btnGetPwd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetPwd.Click
        Dim ds As DataSet = ws.WSSecurity.ForgotPassword(Trim(txtEmail.Text))
        If ds.Tables(0).Rows.Count <> 0 Then
            Label1.Text = Encryption.Decrypt(HttpUtility.HtmlEncode(ds.Tables(0).Rows(0).Item("Password")).Replace(" ", "+"))
        Else
            Label1.Text = "Email doesnt exist"
        End If
    End Sub
End Class