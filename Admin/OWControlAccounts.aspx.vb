
Partial Public Class OWControlAccounts
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected WithEvents UCTopMSgLabel1 As UCTopMSgLabel
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents UCDateRange1 As UCDateRange
    Protected WithEvents ObjectDataSource1 As ObjectDataSource
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            Dim currDate As DateTime = System.DateTime.Today
            UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
            UCSearchContact1.BizDivID = Session("BizDivId")
            ViewState("fromDate") = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            ViewState("toDate") = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
            ViewState("ContactId") = UCSearchContact1.ddlContact.SelectedValue
            ViewState!SortExpression = "Date"
            gvAccountStatements.Sort(ViewState!SortExpression, SortDirection.Descending)
            btnExport.HRef = "ExportToExcel.aspx?page=OWControlAccounts&bizDivId=" & Session("BizDivId") & "&AccountType=" & ddlAcountType.SelectedValue & "&ContactId=" & ViewState("ContactId") & "&fromDate=" & ViewState("fromDate") & "&toDate=" & ViewState("toDate") & "&sortExpression=" & gvAccountStatements.SortExpression
            lblmsg.Visible = False

        End If
    End Sub
    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If UCDateRange1.DateValidate() Then
            lblmsg.Visible = False
            ViewState("fromDate") = UCDateRange1.txtFromDate.Text
            ViewState("toDate") = UCDateRange1.txtToDate.Text
            ViewState("ContactId") = UCSearchContact1.ddlContact.SelectedValue
            PopulateGrid()
        Else
            lblmsg.Visible = True
            lblmsg.Text = "* " & ResourceMessageText.GetString("errInvalidDate")
        End If
    End Sub
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                        ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        If ViewState("ContactId") = "" Then
            ViewState("ContactId") = "0"
        End If
        ds = ws.WSFinance.MS_GetAccountVoucherDetails(Session("BizDivId"), "'" & ddlAcountType.SelectedValue & "'", ViewState("ContactId"), ViewState("fromDate"), ViewState("toDate"), sortExpression, startRowIndex, maximumRows)
        ViewState("rowCount") = ds.Tables("tblCount").DefaultView(0)(0)
        Return ds
    End Function

    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub


    Private Sub gvAccountStatements_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAccountStatements.PreRender        
        If ddlAcountType.SelectedValue = "VATCollected" Or ddlAcountType.SelectedValue = "VATPaid" Then
            gvAccountStatements.Columns(5).Visible = False
            gvAccountStatements.Columns(0).Visible = False
        ElseIf ddlAcountType.SelectedValue = "CostOfSales" Or ddlAcountType.SelectedValue = "Sales" Then
            gvAccountStatements.Columns(0).Visible = True
            gvAccountStatements.Columns(5).Visible = True
        ElseIf gvAccountStatements.Columns(5).Visible = False Then
            gvAccountStatements.Columns(5).Visible = True
            gvAccountStatements.Columns(0).Visible = False
        Else
            gvAccountStatements.Columns(0).Visible = False
        End If
    End Sub





    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvAccountStatements.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvAccountStatements, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex


     
    End Sub

    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvAccountStatements.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvAccountStatements.DataBind()
    End Sub



    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Public Sub PopulateGrid()
        gvAccountStatements.DataBind()        
        btnExport.HRef = "ExportToExcel.aspx?page=OWControlAccounts&bizDivId=" & Session("BizDivId") & "&AccountType=" & ddlAcountType.SelectedValue & "&ContactId=" & ViewState("ContactId") & "&fromDate=" & ViewState("fromDate") & "&toDate=" & ViewState("toDate") & "&sortExpression=" & gvAccountStatements.SortExpression
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvAccountStatements.RowDataBound
        MakeGridViewHeaderClickable(gvAccountStatements, e.Row)
        
    End Sub
    Public Function FormatBalance(ByVal balance As Object) As String
        If Not IsDBNull(balance) Then
            If balance < 0 Then
                Return "bodytxtValidationMsg"
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function
    Public Function getVAT(ByVal VAT As Object, ByVal InvoiceNumber As Object) As String        
        If CStr(InvoiceNumber).StartsWith("R-") Or CStr(InvoiceNumber).StartsWith("P-") Then
            Return FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
        Else
            Return FormatCurrency(IIf(Not IsDBNull(VAT), VAT, "0"), 2, TriState.True, TriState.True, TriState.False)
        End If
    End Function

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
End Class
