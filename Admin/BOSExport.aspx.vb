Public Partial Class BOSExport
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BizDivId As Integer = ApplicationSettings.BizDivId
        Dim ds As New DataSet
        ds = ws.WSFinance.GetBOSExports(0, "In Process", "", "", BizDivId, 0, "SupplierName", 0, 0, Request("Invoices").ToString)

        'Update the function below by Hemisha Desai on 19/08/2014 in order to export file to HSBC according to HSBC's format
        'Also changed the bank details in web.config file
        GetDataForBOSExport(ds)

        'If ds.Tables(0).Rows.Count <> 0 Then
        '    Dim BOSExport As New StringBuilder
        '    'Header
        '    BOSExport.Append("UHL2B")
        '    'Filler as space
        '    'BOSExport.Append(" ")
        '    'Append Julian date
        '    BOSExport.AppendLine(CommonFunctions.GetJulianDate(Convert.ToDateTime(ds.Tables(0).Rows(0)("JulianDate"))))

        '    'Payement for each record
        '    'Amounts are multiplied by 100 to show the final amount in pence
        '    For Each dvrow As DataRowView In ds.Tables("tblBOSPayments").DefaultView
        '        If dvrow("SortCode") = "" Then '14 Blank spaces for Sort Code if it is not present
        '            BOSExport.Append("      ")
        '        Else
        '            BOSExport.Append(CStr(Encryption.Decrypt(dvrow("SortCode"))).PadLeft(6, "0 ").Replace("-", ""))
        '        End If
        '        If dvrow("AccountNo") = "" Then '16 blank spaces for Account no. if it is not present.
        '            BOSExport.Append("        ")
        '        Else
        '            BOSExport.Append(CStr(Encryption.Decrypt(dvrow("AccountNo"))).PadLeft(8, "0"))
        '        End If
        '        BOSExport.Append(" ")
        '        BOSExport.Append(ApplicationSettings.BOSPaymentTransCode)
        '        BOSExport.Append(ApplicationSettings.OWSortCode)
        '        BOSExport.Append(ApplicationSettings.OWAccNo)
        '        BOSExport.Append("    ")
        '        BOSExport.Append(CStr(CInt(dvrow("Total") * 100)).PadLeft(11, "0"))
        '        BOSExport.Append(CStr(ApplicationSettings.OWAccName).PadLeft(18, " "))
        '        'Added By pankaj Malav on 18 Jan 2009 as remitters reference is changed from supplier name to OrderWork Ltd.
        '        BOSExport.Append(CStr(ApplicationSettings.OWAccName).PadLeft(18, " "))
        '        'Dim OWReference As String = dvrow("SupplierName") + " - " + CStr(dvrow("SubAccount"))
        '        'If OWReference.Length > 18 Then
        '        '    BOSExport.Append(OWReference.Substring(0, 18))
        '        'Else
        '        '    BOSExport.Append(OWReference.PadLeft(18, " "))
        '        'End If

        '        If dvrow("AccountName") <> "" Then
        '            If Encryption.Decrypt(dvrow("AccountName")).Length > 18 Then
        '                BOSExport.AppendLine(CStr(Encryption.Decrypt(dvrow("AccountName"))).Substring(0, 18))
        '            Else
        '                BOSExport.AppendLine(CStr(Encryption.Decrypt(dvrow("AccountName"))).PadLeft(18, " "))
        '            End If
        '        Else
        '            If dvrow("SupplierName").Length > 18 Then
        '                BOSExport.AppendLine(CStr(dvrow("SupplierName")).Substring(0, 18))
        '            Else
        '                BOSExport.AppendLine(CStr(dvrow("SupplierName")).PadLeft(18, " "))
        '            End If
        '        End If


        '    Next

        '    'Contra Record
        '    BOSExport.Append(ApplicationSettings.OWSortCode)
        '    BOSExport.Append(ApplicationSettings.OWAccNo)
        '    BOSExport.Append(" ")
        '    BOSExport.Append(ApplicationSettings.BOSContraTranCode)
        '    BOSExport.Append(ApplicationSettings.OWSortCode)
        '    BOSExport.Append(ApplicationSettings.OWAccNo)
        '    BOSExport.Append("    ")
        '    BOSExport.Append(CStr(CInt(ds.Tables("tblTrailer").Rows(0)("TotalFunds") * 100)).PadLeft(11, "0"))
        '    BOSExport.Append(ApplicationSettings.BOSContraNarrative)
        '    BOSExport.AppendLine("CONTRA")

        '    'Trailer
        '    BOSExport.Append("UTL2B")
        '    BOSExport.Append(CStr(CInt(ds.Tables("tblTrailer").Rows(0)("TotalFunds") * 100)).PadLeft(13, "0"))
        '    BOSExport.Append(CStr(CInt(ds.Tables("tblTrailer").Rows(0)("TotalFunds") * 100)).PadLeft(13, "0"))
        '    BOSExport.Append(CStr(ds.Tables("tblTrailer").Rows(0)("NoOfDebits")).PadLeft(7, "0"))
        '    BOSExport.AppendLine(CStr(ds.Tables("tblTrailer").Rows(0)("TotalRecs")).PadLeft(7, "0"))

        '    'Export the Prepared File
        '    Response.Clear()
        '    Response.AddHeader("content-disposition", "attachment; filename=BOSExport.txt")
        '    Response.ContentType = "text/plain"
        '    Response.Write(BOSExport)
        '    Response.End()
        'Else
        '    Response.Clear()
        '    Response.Write(ResourceMessageText.GetString("DBUpdateFail"))
        'End If

    End Sub

    Public Sub GetDataForBOSExport(ByVal ds As DataSet)

        Dim RandomNumber As String
        RandomNumber = Date.Now.ToString("ddMMyy")

        If ds.Tables(0).Rows.Count <> 0 Then
            Dim BOSExport As New StringBuilder
            'Header
            BOSExport.AppendLine("VOL1" & RandomNumber & "                     HSBC                                            1")
            BOSExport.AppendLine("HDR1A      S  1      " & RandomNumber & "00010001       " & CommonFunctions.GetJulianDate(Convert.ToDateTime(ds.Tables(0).Rows(0)("JulianDate"))) & " " & CommonFunctions.GetJulianDate(Convert.ToDateTime(ds.Tables(0).Rows(0)("JulianDate")).AddDays(3)) & " 000000                    ")
            BOSExport.AppendLine("HDR2F0200000100                                   00                            ")
            BOSExport.AppendLine("UHL1 " & CommonFunctions.GetJulianDate(Convert.ToDateTime(ds.Tables(0).Rows(0)("JulianDate")).AddDays(1)) & "999999    000000001 DAILY  001                                        ")

            'Filler as space
            'BOSExport.Append(" ")
            'Append Julian date
            'BOSExport.AppendLine(CommonFunctions.GetJulianDate(Convert.ToDateTime(ds.Tables(0).Rows(0)("JulianDate"))))

            'Payement for each record
            'Amounts are multiplied by 100 to show the final amount in pence
            For Each dvrow As DataRowView In ds.Tables("tblBOSPayments").DefaultView
                If dvrow("SortCode") = "" Then '14 Blank spaces for Sort Code if it is not present
                    BOSExport.Append("      ")
                Else
                    BOSExport.Append(CStr(Encryption.Decrypt(dvrow("SortCode"))).PadLeft(6, "0 ").Replace("-", ""))
                End If
                If dvrow("AccountNo") = "" Then '16 blank spaces for Account no. if it is not present.
                    BOSExport.Append("        ")
                Else
                    BOSExport.Append(CStr(Encryption.Decrypt(dvrow("AccountNo"))).PadLeft(8, "0"))
                End If
                BOSExport.Append("099")
                BOSExport.Append(ApplicationSettings.OWSortCode)
                BOSExport.Append(ApplicationSettings.OWAccNo)
                BOSExport.Append("    ") ' 4 spaces
                BOSExport.Append(CStr(CInt(dvrow("Total") * 100)).PadLeft(11, "0"))
                BOSExport.Append(CStr(ApplicationSettings.OWAccName).PadLeft(18, " "))
                ' BOSExport.Append(CStr(ApplicationSettings.OWAccName).PadLeft(18, " "))
                If dvrow("SupplierAdminname") <> "" Then
                    If dvrow("SupplierAdminname").Length > 18 Then
                        BOSExport.Append(CStr(dvrow("SupplierAdminname").ToString.Replace(")", "").Replace("(", "")).Substring(0, 18))
                    Else
                        BOSExport.Append(CStr(dvrow("SupplierAdminname").ToString.Replace(")", "").Replace("(", "")).PadLeft(18, " "))
                    End If
                End If

                If dvrow("AccountName") <> "" Then
                    If Encryption.Decrypt(dvrow("AccountName")).Length > 18 Then
                        BOSExport.AppendLine(CStr(Encryption.Decrypt(dvrow("AccountName"))).Substring(0, 18))
                    Else
                        BOSExport.AppendLine(CStr(Encryption.Decrypt(dvrow("AccountName"))).PadLeft(18, " "))
                    End If
                Else
                    If dvrow("SupplierName").Length > 18 Then
                        BOSExport.AppendLine(CStr(dvrow("SupplierName").ToString.Replace(")", "").Replace("(", "")).Substring(0, 18))
                    Else
                        BOSExport.AppendLine(CStr(dvrow("SupplierName").ToString.Replace(")", "").Replace("(", "")).PadLeft(18, " "))
                    End If
                End If

            Next

            'Contra Record
            BOSExport.AppendLine(ApplicationSettings.OWSortCode & ApplicationSettings.OWAccNo & "017" & ApplicationSettings.OWSortCode & ApplicationSettings.OWAccNo & "    " & CStr(CInt(ds.Tables("tblTrailer").Rows(0)("TotalFunds") * 100)).PadLeft(11, "0") & ApplicationSettings.BOSContraNarrative & "              CONTRA                " & ApplicationSettings.OWAccName)
            'EOF1 record
            BOSExport.AppendLine("EOF1A      S  1      " & RandomNumber & "00010001       " & CommonFunctions.GetJulianDate(Convert.ToDateTime(ds.Tables(0).Rows(0)("JulianDate"))) & " " & CommonFunctions.GetJulianDate(Convert.ToDateTime(ds.Tables(0).Rows(0)("JulianDate")).AddDays(3)) & " 000000                    ")
            'EOF2 record
            BOSExport.AppendLine("EOF2F0200000100                                   00                            ")
            'UTL1 record
            BOSExport.AppendLine("UTL1" & CStr(CInt(ds.Tables("tblTrailer").Rows(0)("TotalFunds") * 100)).PadLeft(13, "0") & CStr(CInt(ds.Tables("tblTrailer").Rows(0)("TotalFunds") * 100)).PadLeft(13, "0") & "0000001" & CStr(ds.Tables("tblTrailer").Rows(0)("TotalRecs")).PadLeft(7, "0") & "                                    ")

            'Export the Prepared File
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment; filename=HSBCExport.txt")
            Response.ContentType = "text/plain"
            Response.Write(BOSExport)
            Response.End()
        Else
            Response.Clear()
            Response.Write(ResourceMessageText.GetString("DBUpdateFail"))
        End If

    End Sub


End Class
