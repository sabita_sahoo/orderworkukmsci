<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreateAdminUser.aspx.vb" Inherits="Admin.CreateAdminUser" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<style>
.clshideundo
{
display:none; !important
}
.clsalignleft
{
text-align:left; !important

}
.clshieghtwidth
{
width:599px; 
height:201px; 
}


#divMenu {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	line-height: 12px;
	font-weight: normal;
	text-decoration: none;
	border-top-width: 1px;
	border-top-style: solid; 
	border-top-color: #CECECE;
}

.divContentTopRed{
	width: 100%;
	background-color: #993366;
	background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
	height: 5px;
	
}
.DialogueBackground
{
background-color: Gray;
filter: alpha(opacity=50);
opacity: 0.50;
position: fixed;
left: 0;
top: 0;
width: 0%;
height: 100%;
}

.Dialogue
{
background-color: #ededed;
display: none;
z-index: 2;
border: 1px solid Black;
position: absolute;
text-align: center;
margin-left:120px;
}
.DialogueImg
{
height: 30px;
position: absolute;
width: 27px;
float: right;
background-image: url(../images/closebox.png);
cursor: pointer;
right: -6px;
top: -15px;
background-repeat: no-repeat;
border: 0;
}
.DialogueTitle
{
font-size: small;
font-weight: bolder;
padding: auto;
height: 25px;
color: #ffffff;
position: relative;
background: url(../images/app_tab2.jpg) repeat-x;
top: 0px;
}
.tabDivOuter
{
width:400px;
}
</style>
<div id="divlayer"></div>
	        <div id="divContent" style="position:relative; min-height:700px">
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;margin:10px;" id="tblCancelBtn" runat="server">
            <asp:LinkButton runat="server" CausesValidation="false" ID="lnkCancel" CssClass="txtButtonRed" Text="Back to Listing"></asp:LinkButton>
        </div>


       <asp:Panel runat="server" ID="pnlCreateUsr" CssClass="paddingLeft8">
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
              <p class="formTxt" style="padding:0px 0px 5px 10px; margin:0px; "><b><asp:Label ID="lblMsg" runat="server"></asp:Label></b></p>
			 		  
			   <div id="divValidationMain" class="divValidation" runat=server visible=false>
				<div class="roundtopVal"><img src="../Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
					  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
						<tr>
						  <td height="15" align="center">&nbsp;</td>
						  <td height="15">&nbsp;</td>
						  <td height="15">&nbsp;</td>
						</tr>
						<tr valign="middle">
						  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
						  <td class="validationText"><div  id="divValidationMsg"></div>
						  <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
							<asp:ValidationSummary id="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
						  </td>
						  <td width="20">&nbsp;</td>
						</tr>
						<tr>
						  <td height="15" align="center">&nbsp;</td>
						  <td height="15"><input type="button" id="btnFocus" name="btnFocus" class="hdnClass"  /></td>
						  <td height="15">&nbsp;</td>
						</tr>
					  </table>
				   <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				</div>																		
			  <table width="500" style="padding-bottom:10px;" class="tblBdrBlack" cellspacing="0" cellpadding="0">
			  <tr>
				<td>
					<table width="500"  border="0" cellspacing="0" cellpadding="5">
  					  <tr>
						<td width="240" class="gridHdrNotBold">Email - Address<SPAN class="bodytxtValidationMsg">*</SPAN><asp:RequiredFieldValidator id="rqEmail" runat="server" ErrorMessage="Please enter Email Address" ForeColor="#EDEDEB" ControlToValidate="txtUserName">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="rgEmail" runat="server" ControlToValidate="txtUserName" ErrorMessage="Please enter Email Address in correct format" ValidationExpression="^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,}$">*</asp:RegularExpressionValidator></td>
						<td width="450" class="gridHdrNotBold">:&nbsp;<asp:TextBox runat="server" ID="txtUserName" CssClass="formFieldGrey" Width="300"></asp:TextBox></td>
						<td class="gridHdrNotBold"></td>
					  </tr>
					  <tr id="trPassword" runat="server">
						<td width="240" class="gridRow">Password<SPAN class="bodytxtValidationMsg">*</SPAN><asp:RequiredFieldValidator id="rqPassword" runat="server" ErrorMessage="Please enter Password" ForeColor="#EDEDEB" ControlToValidate="txtPassword">*</asp:RequiredFieldValidator></td>
						<td width="150" class="gridRow">:&nbsp;<asp:TextBox runat="server" TextMode="Password" ID="txtPassword" CssClass="formFieldGrey"></asp:TextBox>
                        <%-- 'poonam added on 11/1/2016 - Task - OA-156 : OA - Implement Password complexity policy for Admin Portal users--%>
                                                            <asp:CustomValidator ID="cvPassword" runat="server" ErrorMessage="Password must have at least 8 characters, at least one upper case letter, at least one lower case letter, at least one digit and cannot refer to the name of the company."
                                                                ForeColor="#EDEDEB" ControlToValidate="txtPassword" OnServerValidate="cvPassword_ServerValidate">*</asp:CustomValidator>

                       
                        </td>
						<td class="gridRow"></td>
					  </tr>
					  <tr>
						<td width="240" class="gridHdrNotBold">First Name<SPAN class="bodytxtValidationMsg">*</SPAN><asp:RequiredFieldValidator id="rqFName" runat="server" ErrorMessage="Please enter First Name" ForeColor="#EDEDEB" ControlToValidate="txtFName">*</asp:RequiredFieldValidator></td>
						<td width="150" class="gridHdrNotBold">:&nbsp;<asp:TextBox runat="server" ID="txtFName" CssClass="formFieldGrey"></asp:TextBox></td>
						<td class="gridHdrNotBold"></td>
						</tr>
					  <tr>
						<td width="240" class="gridRow">Last Name<SPAN class="bodytxtValidationMsg">*</SPAN><asp:RequiredFieldValidator id="rqLName" runat="server" ErrorMessage="Please enter Last Name" ForeColor="#EDEDEB" ControlToValidate="txtLName">*</asp:RequiredFieldValidator></td>
						<td width="150" class="gridRow">:&nbsp;<asp:TextBox runat="server" ID="txtLName" CssClass="formFieldGrey"></asp:TextBox></td>
						<td class="gridRow"></td>
						</tr>

						
						<tr>
					        <td colspan="3">
                            <TABLE id="tblchangepass" runat="server" visible="false" width="500" cellSpacing="0" cellPadding="0" border="0">
								  <TR>
									<td width="118" align="left" class="gridRow" style="margin-left:0px;padding-left:0px;">New Password
                                    <asp:CustomValidator ID="custPassword"  runat="server" ErrorMessage="Please enter your Password"  ForeColor="#EDEDEB" ControlToValidate="txtPassword">*</asp:CustomValidator>
									 <%-- 'poonam added on 11/1/2016 - Task - OA-156 : OA - Implement Password complexity policy for Admin Portal users--%>
                                                            <asp:CustomValidator ID="custPasswordValidate" runat="server" ErrorMessage="Password must have at least 8 characters, at least one upper case letter, at least one lower case letter, at least one digit and cannot refer to the name of the company."
                                                                ForeColor="#EDEDEB" ControlToValidate="txtNewPassword" OnServerValidate="custPasswordValidate_ServerValidate">*</asp:CustomValidator>

                                    </td>
						            <td width="150" align="left" class="gridRow">:&nbsp;<asp:TextBox runat="server" ID="txtNewPassword" CssClass="formFieldGrey" TextMode="Password"></asp:TextBox></td>
						            <td class="gridRow"></td>
								  </TR>
                                  <TR>
									<td class="gridRow" style="margin-left:0px;padding-left:0px;">Confirm Password
                                    <asp:CompareValidator id="CompareValidator1" runat="server" ErrorMessage="Password does not match, please reconfirm"
                                        ControlToValidate="txtNewPassword" ControlToCompare="txtConfirmPassword">*</asp:CompareValidator>
                                    </td>
						            <td class="gridRow">:&nbsp;<asp:TextBox runat="server" ID="txtConfirmPassword" CssClass="formFieldGrey" TextMode="Password"></asp:TextBox></td>
						            <td class="gridRow"></td>
								  </TR>
							  </TABLE>
							  </td>							
                         </tr>
                        <tr id="trrolegp" runat="server" visible="false">
						<td width="240" class="gridHdrNotBold">Role Group</td>
						<td width="150" class="gridHdrNotBold">:&nbsp;<asp:DropDownList runat="server" ID="ddlRoleGroup" CssClass="formField105"></asp:DropDownList></td>
						<td class="gridHdrNotBold"></td>
						</tr>
						<tr>
						<td width="240" class="gridHdrNotBold">Home Page</td>
						<td width="150" class="gridHdrNotBold">:&nbsp;<asp:DropDownList runat="server" ID="ddlhomepage" CssClass="formField105"></asp:DropDownList></td>
						<td class="gridHdrNotBold"></td>
						</tr>		
						<tr id="trJobTitle" runat="server">
						<td width="240" class="gridHdrNotBold">Job Title</td>
						<td width="20" class="gridHdrNotBold">:</td>
						<td class="gridHdrNotBold"><asp:TextBox runat="server" ID="txtJobTitle" CssClass="formFieldGrey"></asp:TextBox></td>
						</tr>
						<tr >
						<td colspan="3" class="gridHdrNotBold">
						<table cellpadding="0" cellspacing="0" >
                                          <tr>
                    
                                            <td>
                                            <input type="hidden" runat="server" id="hdnMainCatSelected" name="hdnMainCatSelected" >
                                                <span class="gridHdrNotBold">Add Main Category</span><br />
                                                <asp:ListBox ID="listMainCat"  title="Main Category" 
                                                    SelectionMode="Multiple" runat="server" Width="200" Height="80" 
                                                    ToolTip="Main Category" DataValueField="MainCatID" DataTextField="Name" DataMember="GroupLevel"
                                                    CssClass="formField"></asp:ListBox>
                                             </td>
                                            <td >
                                                <input type="button" title="Add" width="20" height="20" onclick="addToList('ctl00_ContentPlaceHolder1_listMainCat','ctl00_ContentPlaceHolder1_listMainCatSel','ctl00_ContentPlaceHolder1_hdnMainCatSelected','Add')"
                                                    value='>>' style="width: 35px; height: 20px; padding-bottom: 0px; margin-bottom: 0px;
                                                    vertical-align: middle; text-align: center;" title="Add an item from Left"><br />
                                             
                                                <input type="button" title="Remove" width="20" height="20" onclick="addToList('ctl00_ContentPlaceHolder1_listMainCatSel','ctl00_ContentPlaceHolder1_listMainCat','ctl00_ContentPlaceHolder1_hdnMainCatSelected','Remove')"
                                                    value='<<' style="width: 35px; height: 20px; padding-bottom: 0px; margin-bottom: 0px"
                                                    title="Remove an item from Right">
                                            </td>
                                            <td>
                                                <span class="gridHdrNotBold">Selected Main Category</span>
                                                <br />
                                                <asp:ListBox ID="listMainCatSel" title="Selected Main Category" ToolTip="Main Category" 
                                                    runat="server" Width="200" Height="80" DataTextField="Name" 
                                                    DataValueField="MainCatID" SelectionMode="Multiple" CssClass="formField"></asp:ListBox>
                                                
                                            </td>                    
                                        </tr>
                                        <tr>  
                                        <td colspan="3">
                                        <span style="color: #999999; font-size: 11px; line-height: 11px;">Note: Hold down CTRL
                                        to select more than one.Use Command key for
                                        Mac </span>
                                        </td>                                      
                                        </tr>
            
                                        </table> 
						</td>						  
						  </tr>	 
						<tr >
						  <td class="gridHdrNotBold" colspan="3">Enable Login : <asp:CheckBox ID="chkEnableLogin" runat="server" /></td>
						  </tr>	 
					  </table>
					   <table cellspacing="0" cellpadding="0" width="500" border="0">
						  <tr>
							<td align="center">
								<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;margin-top:15px;" id="tblCreateUserBtn" runat="server">
                                    <asp:LinkButton runat="server" CausesValidation="false" ID="lnkCreateUser" CssClass="txtButtonRed" Text="Create User"></asp:LinkButton></TD>
								</div>
							</td>
							<td align="center">
							    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;margin-top:15px;" id="tblEditUserBtn" runat="server">
                                    <asp:LinkButton runat="server" CausesValidation="false" ID="lnkEditUser" CssClass="txtButtonRed" Text="Edit User"></asp:LinkButton>
                                </div>
							  </td>                            
						  </tr>
					    </table>
				</td>
			  </tr>
			 
			</table>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlSuccess" Visible="false">
				<p class="bodytxtValidationMsg"><asp:Label ID="lblMsgCreatedEdited" runat="server" CssClass="paddingL10"></asp:Label></p>
			</asp:Panel>
            <!-- InstanceEndEditable --></div>

 </asp:Content>

