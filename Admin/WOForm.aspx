<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="WOForm.aspx.vb" Inherits="Admin.WOForm"
title="WOForm"
 %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<script language="javascript" src="JS/json2.js"  type="text/javascript"></script>
<script language="javascript" src="JS/Standards.js"  type="text/javascript"></script>
<script language="javascript" src="JS/jquery.js"  type="text/javascript"></script>
<script language="javascript" src="JS/JQuery.jsonSuggestCustom.js"  type="text/javascript"></script>

<script><asp:Literal id="litData" runat="server"></asp:Literal></script>
<style type="text/css">
#divMenu {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	line-height: 12px;
	font-weight: normal;
	text-decoration: none;
	border-top-width: 1px;
	border-top-style: solid; 
	border-top-color: #CECECE;
}

.divContentTopRed{
	width: 100%;
	background-color: #993366;
	background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
	height: 5px;	
}
</style>
<script language="javascript" type="text/javascript">
var input = $('input#txtContact').attr("autocomplete", "off");
if ($.browser.mozilla)
    input.keypress(processKey);	// onkeypress repeats arrow keys in Mozilla/Opera
else
    input.keydown(processKey);
    </script>








			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			
			<table class="gridBorderB" width="100%"  border="0" cellpadding="0" cellspacing="0">
                      <tr>
					     <td id="tdBackToListing" runat=server>
							<div id="divButton" class="divButton" style="width: 115px;">
								<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
									<a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" alt="Back to Listing" width="4" height="8" hspace="0" vspace="0" border="0" class="marginR8"><strong>Back to Listing</strong></a>
								<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
							</div>
						</td>
					 </tr>
             </table>

			 <table width="100%"  border="0" cellspacing="0" cellpadding="0" >    			
                <tr>
                  <td> <%@ Register TagPrefix="uc1" TagName="UCMSCreateWorkRequestUK" Src="~/UserControls/UK/UCMSWOForm.ascx" %>
                       <uc1:UCMSCreateWorkRequestUK id="UCCreateWorkOrderUK1" runat="server"></uc1:UCMSCreateWorkRequestUK>
				  </td>
                </tr>
              </table>
			 
            
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      
</asp:Content>
      
      
