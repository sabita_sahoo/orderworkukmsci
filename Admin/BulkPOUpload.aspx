﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="BulkPOUpload.aspx.vb" Inherits="Admin.BulkPOUpload" title="OrderWork : Bulk PO Upload"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript">
    function ProcessFile() {
        document.getElementById("ctl00_ContentPlaceHolder1_divLoading").style.display = "inline";
        document.getElementById("ctl00_ContentPlaceHolder1_tblProcess").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_btnProcess").click();
    }
</script>
	<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<div style="margin-left:15px; margin-top:5px; font-size:12px;min-height:550px;">
<p class="paddingB4 HeadingRed"><strong>Bulk PO Upload</strong> </p>
            
            <div class="gridText" id="divLoading" style="display:none;"  runat="server">
                <img  align=middle src="Images/indicator.gif" />
                <b>Uploading Data... Please Wait</b>
            </div>  
            
            <table id="tblUpload" runat="server" visible="true">                         
                <tr><td><asp:Label ID="lblMsg" runat="server" class="HeadingRed" ></asp:Label></td></tr>                 
                   <tr id="trUpload" runat="server">                   
                    <td>
                    <b>Upload</b><br />
                    <asp:FileUpload ID="FileUpload1" runat="server"  /><br style="clear:both;"/><br />
                     <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                        <asp:LinkButton id="lnkbtnUpload" runat="server" CssClass="txtButtonRed" CausesValidation="false">&nbsp;Upload&nbsp;</asp:LinkButton>
                     </div>
                    </td>
                </tr>                 
                 <tr>                   
                    <td>
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:120px;" id="tblProcess" runat="server">
                        <a Class="txtButtonRed" style="cursor:pointer;" onclick="Javascript:ProcessFile();"> &nbsp;Process File&nbsp;</a>
                        <asp:Button runat="server" ID="btnProcess" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
                     </div>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
            </table>
                  
            <table id="tblPostUpload" runat="server" >
                    
                <tr style="margin-top:5px;">
                    <td style="Height: 26px">
                       <asp:Label ID="lblSuccess" runat="server" class="HeadingRed" ></asp:Label>
                    </td>                    
                </tr>
                <tr>
                 <td>
                  <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                    <asp:LinkButton id="lnkbtnBack" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Back&nbsp;</asp:LinkButton>
                  </div>
                 </td>
                </tr>
            </table>            
        </div>

</asp:Content>