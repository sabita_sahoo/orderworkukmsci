﻿<%@ Page Title="Bulk voucher upload" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master"
    CodeBehind="BulkVoucherUpload.aspx.vb" Inherits="Admin.BulkVoucherUpload" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="JS/json2.js" type="text/javascript"></script>
    <script language="javascript" src="JS/Standards.js" type="text/javascript"></script>
    <script language="javascript" src="JS/jquery.js" type="text/javascript"></script>
    <script language="javascript" src="JS/JQuery.jsonSuggestCustom.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var input = $('input#txtContact').attr("autocomplete", "off");
        if ($.browser.mozilla)
            input.keypress(processKey); // onkeypress repeats arrow keys in Mozilla/Opera
        else
            input.keydown(processKey);
    </script>
    <script type="text/javascript">
        function validate() {

            var array = ['xls', 'xlsx'];

            var xyz = document.getElementById('<%= fileuploader.ClientID %>');

            var Extension = xyz.value.substring(xyz.value.lastIndexOf('.') + 1).toLowerCase();

            if (array.indexOf(Extension) <= -1) {

                alert("Please Upload a File with Valid Format");

                return false;
            }
        }      
    </script>
    <style>
        .BookHead {
            background-color: #EFEBEF;
            border-radius: 8px 8px 0px 0px;
            behavior: url(../Styles/PIE.htc);
            position: relative;
            width: 668px;
            width: 467px\9;
            height: 16px;
            *margin-top: -5px;
            padding: 8px 0px 15px 10px;
            *margin-left: 0px;
            font-family: Tahoma;
            font-size: 14px;
            font-weight: bolder;
        }

        .textField {
            width: 280px;
        }

        .BookTable {
            margin-top: 10px;
            background-color: white;
            border-radius: 8px;
            behavior: url(../Styles/PIE.htc);
            position: relative;
            width: 668px;
            height: 400px;
            padding-bottom: 10px;
            border: solid 1px #c0c0c0;
            text-align: left;
            overflow: hidden;
        }

         .OTP {
            margin-top: 10px;
            background-color: white;
            border-radius: 8px;
            behavior: url(../Styles/PIE.htc);
            position: relative;
            width: 668px;
            height: 400px;
            padding-bottom: 10px;
            border: solid 1px #c0c0c0;
            text-align: left;
            overflow: hidden;
        }

        .BookTableContent {
            font-family: tahoma;
            font-size: 12px;
            padding: 0 15px;
        }

        .Authentication {
            float: left;
            margin-bottom: 10px;
            width: 100%;
        }

        .AuthenticationLeft {
            float: left;
            width: 150px;
        }

        .AuthenticationRight {
            float: left;
        }

        .DateOfBirthFormat {
            color: #7f7f7f;
            float: left;
            font-family: tahoma;
            font-size: 10px;
            margin-left: 150px;
        }

        body {
        }

        .mGrid {
            border-style: solid;
            border-color: inherit;
            border-width: 0;
            border-top: 0px;
            border-bottom: 0px;
        }

            .mGrid td {
                border-color: -moz-use-text-color #FFFFFF #F7EBF7 -moz-use-text-color;
                border-style: none solid solid none;
                border-width: 0 1px 1px 0;
                color: #717171;
                border-right: 1px solid #FFFFFF\9;
                border-bottom: 1px solid #F7EBF7\9;
                border-left: 0px\9;
                border-top: 0px\9;
            }

        @media screen and (-webkit-min-device-pixel-ratio:0) {
            .mGrid td {
                border-bottom: 1px solid #F7EBF7;
                border-top: 0px;
                border-right: 1px solid #FFFFFF;
                border-left: 0px;
            }
        }

        * + html .mGrid td {
            border-right: 1px solid #FFFFFF;
            border-bottom: 1px solid #F7EBF7;
            border-left: 0px;
            border-top: 0px;
        }

        .mGrid th {
            background-color: #DEDFDE;
            border-color: -moz-use-text-color #FFFFFF -moz-use-text-color -moz-use-text-color;
            border-right-color: #FFFFFF\9;
            border-style: none solid none none;
            border-width: 0 1px 0 0;
            color: #636163;
            font-family: Tahoma;
            font-size: 12px;
            padding: 6px 2px;
        }

        @media screen and (-webkit-min-device-pixel-ratio:0) {
            .mGrid th {
                border-bottom: Opx;
                border-top: 0px;
                border-right: 1px solid;
                border-right-color: #FFFFFF;
                border-left: 0px;
            }
        }

        * + html .mGrid th {
            border-bottom: 0px;
            border-top: 0px;
            border-right: 1px solid #FFFFFF;
            border-left: 0px;
        }

        .one {
            font-family: tahoma;
            font-size: 12px;
            padding: 8px 2px 8px 10px;
            width: 159px;
        }

        .two {
            font-family: tahoma;
            font-size: 12px;
            padding: 8px 2px 8px 10px;
            width: 77px;
            text-align: center;
        }

        .three {
            font-family: tahoma;
            font-size: 12px;
            padding: 8px 2px 8px 10px;
            width: 71px;
            text-align: center;
        }

        .four {
            font-family: tahoma;
            font-size: 12px;
            padding: 8px 2px 8px 10px;
            width: 55px;
            text-align: center;
        }

        .five {
            font-family: tahoma;
            font-size: 12px;
            padding: 8px 2px 8px 10px;
            width: 20px;
            text-align: center;
        }

        .mGrid .alt {
            background: #F7F7F7;
        }

        .mGrid .pgr td {
            padding: 0px;
        }


        .gridSubContainer {
            width: 480px;
                      border: 1px solid #BDBEBD;
            border-top: 0px;
            border-bottom: 0px;
        }

        .beneficiaryTopGrid {
            width: 480px;
            background-color: #EFEBEF;
            float: left;
           
            border: 1px solid #BDBEBD;
            border-bottom: 0px;
            behavior: url(../Styles/PIE.htc);
        }

        .beneTopGridLeft {
            float: left;
            font-family: Tahoma;
            font-size: 14px;
            padding: 16px 0 18px 10px;
            border-radius: 8px 0 0 0;
            behavior: url(../Styles/PIE.htc);
        }

        .beneTopGridRight {
            float: right;
            font-family: Tahoma;
            font-size: 12px;
            margin-right: 10px;
            padding: 16px 0 16px 0;
            border-radius: 0 8px 0 0;
            behavior: url(../Styles/PIE.htc);
        }

        .beneficiaryBottomGrid {
            width: 480px;
            float: left;
            border-radius: 0 0 8px 8px;
            behavior: url(../Styles/PIE.htc);
            border: 1px solid #BDBEBD;
            border-top: 0px;
        }

        .beneBottomGridLeft {
            float: left;
            font-family: Tahoma;
            font-size: 14px;
            padding: 16px 0 18px 10px;
            border-radius: 0 0 0 8px;
            behavior: url(../Styles/PIE.htc);
        }

        .beneBottomGridRight {
            float: right;
            font-family: Tahoma;
            font-size: 12px;
            margin-right: 10px;
            padding: 16px 0;
            border-radius: 0 0 8px 0;
            behavior: url(../Styles/PIE.htc);
        }

        .Top {
            background-color: #EFEBEF;
            border-radius: 8px 8px 0 0;
            behavior: url(../Styles/PIE.htc);
        }

        .Bottom {
            background-color: #FFFFFF;
            border-radius: 0 0 8px 8px;
            behavior: url(../Styles/PIE.htc);
        }

        .paging {
            width: 480px;
            color: Black;
            float: left;
        }

        .lblPagingTop {
            float: left;
            font-family: Tahoma;
            font-size: 14px;
            padding: 16px 0 18px 10px;
            border-radius: 8px 0 0 0;
            behavior: url(../Styles/PIE.htc);
        }

        .pagingRightTop {
            float: right;
            font-family: Tahoma;
            font-size: 12px;
            margin-right: 10px;
            padding: 18px 0 16px 0;
            border-radius: 0 8px 0 0;
            behavior: url(../Styles/PIE.htc);
        }

        .lblShowPaging {
            float: left;
            margin-right: 5px;
        }

        #prevPagingImageTop {
            float: left;
            margin-right: 5px;
            margin-top: -2px;
        }

        .firstPaging {
            float: left;
            border: 1px solid #DEDBDE;
            background-color: #FFFFFF;
            margin-right: 5px;
            margin-top: -2px;
            padding: 2px 5px;
        }

        .secondPaging {
            float: left;
            border: 1px solid #DEDBDE;
            background-color: #FFFFFF;
            margin-right: 5px;
            margin-top: -2px;
            padding: 2px 5px;
        }

        .thirdPaging {
            float: left;
            border: 1px solid #DEDBDE;
            background-color: #FFFFFF;
            margin-right: 5px;
            margin-top: -2px;
            padding: 2px 5px;
        }

        #nextPagingImageTop {
            float: left;
            margin-top: -2px;
        }

        .pagingRightBtn {
            float: right;
            font-family: Tahoma;
            font-size: 12px;
            margin-right: 10px;
            padding: 18px 0 16px 0;
            border-radius: 0 0 8px 0;
            behavior: url(../Styles/PIE.htc);
        }

        #prevPagingImageBtn {
            float: left;
            margin-right: 5px;
            margin-top: -2px;
        }

        #nextPagingImageBtn {
            float: left;
            margin-top: -2px;
        }



        #divProgress {
            background-color: #eeeeeb;
            position: absolute;
            font-family: Tahoma;
            font-size: 13px;
            font-weight: bold;
            filter: alpha(opacity=70);
            MozOpacity: 0.7;
            opacity: 0.7;
            padding: 15% 0 0;
            margin: 0px auto;
            height: 1920px;
            width: 1020px;
            text-align: center;
        }

        .benestyl {
            margin-left: 16px;
        }
    </style>
    <style>
        table.bgWhite tr:nth-child(even) table
        {
            background: #f7f7f7;
        }
        
        body
        {
            font-family: tahoma;
            font-size: 12px;
            overflow-y: scroll;
        }
    </style>
    <style type="text/css">
        #HideDetails1, #HideDetails2, #HideDetails3
        {
            width: 98%;
            float: left;
            margin-bottom: 25px;
        }
        
        .Hide
        {
            display: none;
        }
        
        .Show
        {
            display: block;
        }
    </style>
    <input type="hidden" id="hdnContactID" runat="Server" value="" />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div style="margin-left: 15px; margin-top: 5px; font-size: 12px; min-height: 550px;">
        <p class="paddingB4 HeadingRed">
            <strong>Bulk Voucher Upload</strong>
        </p>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divUpload" runat="server">
                    <div class="container containerLeft">
                        <div>
                            <div class="beneficiaryTopGrid">
                                <div class="paddingB4 HeadingRed">
                                    &nbsp;&nbsp;<b>Upload File</b></div>
                            </div>
                            <div class="gridSubContainer">
                                <div style="padding: 12px 0; margin-left: 15px;">
                                </div>
                                <div style="padding: 12px 0;">
                                    <br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select client
                                    <asp:DropDownList Width="250px" ID="ddlService" runat="server" Style="margin-left: 10px;">
                                        <%--<asp:ListItem Value="20201" Selected="True">Exertis</asp:ListItem>--%>
                                        <asp:ListItem Value="19596">Shop Direct Group</asp:ListItem>
                                        <asp:ListItem Value="19825">Exertis</asp:ListItem>
                                        <%--<asp:ListItem Value="20225">Westcoast Ltd</asp:ListItem>--%>

                                    </asp:DropDownList>
                                </div>
                                <div style="padding: 12px 0; margin-left: 15px;">
                                </div>
                                <div style="border-bottom: 1px solid #c6c4c5; padding: 12px 0;">
                                    <asp:FileUpload ID="fileuploader" runat="server" Style="margin-left: 15px;" size="50"
                                        class="formText" />&nbsp;&nbsp;
                                    <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                        color: #FFFFFF; text-align: center; width: 60px; margin: 17px 370px;">
                                        <asp:LinkButton ID="btn_upload_excel" runat="server" CssClass="txtButtonRed" OnClientClick="return validate();">&nbsp;Upload&nbsp;</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="divConfirm" runat="server" class="Hide">
                    <div id="Div2" class=" containerLeft">
                        <div id="divConfirmSuccessfullRecordsForRetailClient" runat="server" class="Hide">
                            <b>Valid Records</b>
                            <asp:GridView ID="DataList2" runat="server" ForeColor="#333333" BorderWidth="1px"
                                PageSize="25" EmptyDataText="No valid voucher codes " BorderColor="White" CssClass="gridrow"
                                Width="50%" AutoGenerateColumns="false">
                                <Columns>
                                    <%-- <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sr No" SortExpression="Sr No" HeaderText="Sr No" HeaderStyle-Width="10px" />--%>
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-CssClass="gridHdr"
                                        DataField="VoucherCode" SortExpression="VoucherCode" HeaderText="Voucher Code"
                                        HeaderStyle-Width="300px" />
                                    <asp:TemplateField ItemStyle-CssClass="gridRow" HeaderText="Status" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <img src="BulkWOFiles/ActiveIcon.PNG" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="gridRow" />
                                <PagerStyle BorderWidth="0px" BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top" Height="40px" />
                                <FooterStyle BackColor="#D7D7D7" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </div>
                        <br />
                        <div style="float: right; margin: 15px 25px; width: 200px;">
                            <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                color: #FFFFFF; text-align: center; width: 60px; margin: 0px 82px;">
                                <asp:LinkButton ID="btn_upload" runat="server" CssClass="txtButtonRed" Visible="false">&nbsp;Upload&nbsp;</asp:LinkButton>
                            </div>
                            <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                color: #FFFFFF; text-align: center; width: 60px; float: left; margin: -18px 150px;">
                                <asp:LinkButton ID="btn_cancel_upload" runat="server" CssClass="txtButtonRed">&nbsp;Cancel&nbsp;</asp:LinkButton>
                            </div>
                        </div>
                        <br />
                        <div id="divConfirmInvalidRecordsForRetailClient" runat="server" class="Hide">
                            <b>Invalid Records</b>
                            <asp:GridView ID="DataList3" runat="server" ForeColor="#333333" BorderWidth="1px"
                                PageSize="25" BorderColor="White" CssClass="gridrow" Width="100%" AutoGenerateColumns="false"
                                EmptyDataText="No invalid voucher codes " EmptyDataRowStyle-Font-Bold="true"
                                EmptyDataRowStyle-HorizontalAlign="Center">
                                <Columns>
                                    <%--  <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sr No" SortExpression="Sr No" HeaderText="Sr No" HeaderStyle-Width="10px" />--%>
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-CssClass="gridHdr"
                                        DataField="VoucherCode" SortExpression="VoucherCode" HeaderText="Voucher Code"
                                        HeaderStyle-Width="300px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Reason" SortExpression="Reason" HeaderText="Reason" HeaderStyle-Width="300px" />
                                    <asp:TemplateField ItemStyle-CssClass="gridRow" HeaderText="Status" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <img src="BulkWOFiles/in-activeIcon.png" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="gridRow" />
                                <PagerStyle BorderWidth="0px" BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top" Height="40px" />
                                <FooterStyle BackColor="#D7D7D7" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </div>
                        <br />
                      <%--  <div style="float: right; margin: 15px 25px; width: 200px;">
                            <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                color: #FFFFFF; text-align: center; width: 60px; margin: 0px 82px;">
                                <asp:LinkButton ID="btn_download" runat="server" CssClass="txtButtonRed" Visible="false">&nbsp;Download&nbsp;</asp:LinkButton>
                              
                            </div>
                        </div>--%>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btn_upload_excel" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdateProgress AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                <div>
                    <img align="middle" src="Images/indicator.gif" />
                    Loading ...
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1"
            CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
</asp:Content>
