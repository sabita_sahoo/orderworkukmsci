<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master"
    Title="OrderWork : Wholesale Price" CodeBehind="WOSetWholesalePrice.aspx.vb"
    EnableEventValidation="false" EnableViewStateMac="false" Inherits="Admin.WOSetWholesalePrice" %>

<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"> 
    </asp:ScriptManager>
    <script type="text/javascript">
        function ChkCCPayeeDetailsUpdateWP() {
            if (document.getElementById("ctl00_ContentPlaceHolder1_ChkCCPayeeDetails").checked == true) {
                document.getElementById("ctl00_ContentPlaceHolder1_tblContactInfoUpSell").style.display = "inline";
            }
            else {
                document.getElementById("ctl00_ContentPlaceHolder1_tblContactInfoUpSell").style.display = "none";
            }
        }
        function CalculateQuantity() {
            // alert(myDatediff(document.getElementById("ctl00_ContentPlaceHolder1_txtScheduleWInBegin").value, document.getElementById("ctl00_ContentPlaceHolder1_txtScheduleWInEnd").value, "days"));
            if (document.getElementById("ctl00_ContentPlaceHolder1_txtScheduleWInEnd") != null) {
                if (document.getElementById("ctl00_ContentPlaceHolder1_tdTxtScheduleWInEnd").style.display != "none") {
                    //            document.getElementById("ctl00_ContentPlaceHolder1_txtQuantity").value = myDatediff(document.getElementById("ctl00_ContentPlaceHolder1_txtScheduleWInBegin").value, document.getElementById("ctl00_ContentPlaceHolder1_txtScheduleWInEnd").value, "days") + 1;
                    document.getElementById("ctl00_ContentPlaceHolder1_txtQuantity").value = GetDaysInfo(document.getElementById("ctl00_ContentPlaceHolder1_txtScheduleWInBegin").value, document.getElementById("ctl00_ContentPlaceHolder1_txtScheduleWInEnd").value);
                }
                else {
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtQuantity").value = 1
                }
            }
            else {
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtQuantity").value = 1
            }
        }
        //date1 & date2in dd/mm/yyyy format
        function myDatediff(date1, date2, interval) {
            var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
            var date1Parts = date1.split("/");
            var date2Parts = date2.split("/");
            date1 = new Date(date1Parts[2], date1Parts[1] - 1, date1Parts[0]);
            date2 = new Date(date2Parts[2], date2Parts[1] - 1, date2Parts[0]);
            var timediff = date2 - date1;
            if (isNaN(timediff)) return 0;
            switch (interval) {
                case "years": return date2.getFullYear() - date1.getFullYear();
                case "months": return (
                (date2.getFullYear() * 12 + date2.getMonth())
                -
                (date1.getFullYear() * 12 + date1.getMonth())
            );
                case "weeks": return Math.floor(timediff / week);
                case "days": return Math.floor(timediff / day);
                case "hours": return Math.floor(timediff / hour);
                case "minutes": return Math.floor(timediff / minute);
                case "seconds": return Math.floor(timediff / second);
                default: return undefined;
            }
        }
        function GetDaysInfo(d0, d1) {
            //alert(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnIsSaturdayDisabled").value);
            //alert(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnIsSundayDisabled").value);
            var date1Parts = d0.split("/");
            var date2Parts = d1.split("/");
            d0 = new Date(date1Parts[2], date1Parts[1] - 1, date1Parts[0]);
            d1 = new Date(date2Parts[2], date2Parts[1] - 1, date2Parts[0]);
            var ndays = 1 + Math.round((d1.getTime() - d0.getTime()) / (24 * 3600 * 1000));

            if (isNaN(ndays)) return 1;

            var nsaturdays = Math.floor((d0.getDay() + ndays) / 7);
            var snsundays = (2 * nsaturdays + (d0.getDay() == 0) - (d1.getDay() == 6)) - nsaturdays;

            if (document.getElementById("ctl00_ContentPlaceHolder1_hdnIsSaturdayDisabled").value == "True") {
                ndays = ndays - nsaturdays;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_hdnIsSundayDisabled").value == "True") {
                ndays = ndays - snsundays;
            }

            return ndays;
            //        return 2 * nsaturdays + (d0.getDay() == 0) - (d1.getDay() == 6);
        }
    </script>
    <div id="divContent">
        <input type="hidden" id="hdnIsSundayDisabled" runat="Server" value="True" />
        <input type="hidden" id="hdnIsSaturdayDisabled" runat="Server" value="True" />
        <div class="roundtopWhite">
            <img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner"
                style="display: none" /></div>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
            <tr>
                <td valign="top">
                    <!-- InstanceBeginEditable name="EdtContent" -->
                    <asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server">
                        <ContentTemplate>
                            <div id="divValidationMain" class="divValidation" runat="server" visible="false">
                                <div class="roundtopVal">
                                    <img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner"
                                        style="display: none" /></div>
                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td height="15" align="center">
                                            &nbsp;
                                        </td>
                                        <td width="565" height="15">
                                            &nbsp;
                                        </td>
                                        <td height="15">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="middle">
                                        <td width="63" align="center" valign="top">
                                            <img src="Images/Icons/Validation-Alert.gif" width="31" height="31">
                                        </td>
                                        <td width="565" class="validationText">
                                            <div id="divValidationMsg">
                                            </div>
                                            <asp:Label ID="lblError" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                                            <span class="validationText">
                                                <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg"
                                                    DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
                                            </span>
                                        </td>
                                        <td width="20">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="15" align="center">
                                            &nbsp;
                                        </td>
                                        <td width="565" height="15">
                                            &nbsp;
                                        </td>
                                        <td height="15">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <div class="roundbottomVal">
                                    <img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner"
                                        style="display: none" /></div>
                            </div>
                            <table width="940" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td height="35" class="HeadingRed">
                                        <strong>&nbsp;Wholesale Price</strong>
                                    </td>
                                </tr>
                            </table>
                            <asp:Panel ID="pnlSetPrice" runat="server">
                                <table width="940" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <table id="DGWorkOrders" width="100%" border="0" cellpadding="5" cellspacing="0">
                                                <tr valign="top">
                                                    <td class="gridHdr gridBorderRB">
                                                        WO No
                                                    </td>
                                                    <td class="gridHdr gridBorderRB">
                                                        Submitted
                                                    </td>
                                                    <td class="gridHdr gridBorderRB">
                                                        Location
                                                    </td>
                                                    <td class="gridHdr gridBorderRB">
                                                        Client Company
                                                    </td>
                                                    <td class="gridHdr gridBorderRB">
                                                        Client Contact
                                                    </td>
                                                    <td class="gridHdr gridBorderRB">
                                                        WO Title
                                                    </td>
                                                    <td class="gridHdr gridBorderRB">
                                                        WO Start
                                                    </td>
                                                    <td class="gridHdr gridBorderRB">
                                                        WO End
                                                    </td>
                                                    <td class="gridHdrHighlight gridBorderRB">
                                                        Status
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="65px" class="gridRow gridBorderRB">
                                                        <asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label>
                                                    </td>
                                                    <td width="70px" class="gridRow gridBorderRB">
                                                        <asp:Label runat="server" CssClass="formTxt" ID="lblSubmitted"></asp:Label>
                                                    </td>
                                                    <td width="65px" class="gridRow gridBorderRB">
                                                        <asp:Label runat="server" CssClass="formTxt" ID="lblLocation"></asp:Label>
                                                    </td>
                                                    <td width="90px" class="gridRow gridBorderRB">
                                                        <asp:Label runat="server" CssClass="formTxt" ID="lblBuyerCompany"></asp:Label>
                                                    </td>
                                                    <td width="90px" class="gridRow gridBorderRB">
                                                        <asp:Label runat="server" CssClass="formTxt" ID="lblBuyerContact"></asp:Label>
                                                    </td>
                                                    <td class="gridRow gridBorderRB">
                                                        <asp:Label runat="server" CssClass="formTxt" ID="lblTitle"></asp:Label>
                                                    </td>
                                                    <td width="60px" class="gridRow gridBorderRB">
                                                        <asp:Label runat="server" CssClass="formTxt" ID="lblWOStart"></asp:Label>
                                                    </td>
                                                    <td width="90px" class="gridRow gridBorderRB">
                                                        <asp:Label runat="server" CssClass="formTxt" ID="lblWOEnd"></asp:Label>
                                                    </td>
                                                    <td width="57px" class="gridRowHighlight gridBorderRB">
                                                        <asp:Label runat="server" CssClass="gridRowHighlight" ID="lblStatus"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formLabelGrey" height="24" valign="bottom">
                                            The present Wholesale Price for the Work Request is: &pound;
                                            <asp:Label ID="lblWholesalePrice" runat="server" CssClass="formLabelGrey"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formLabelGrey" height="24" valign="bottom">
                                            The present Portal Price for the Work Request is: &pound;
                                            <asp:Label ID="lblPlatformPrice" runat="server" CssClass="formLabelGrey"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr valign="top">
                                                    <td width="165" class="formLabelGrey" valign="bottom">
                                                        &nbsp;WO Title
                                                    </td>
                                                    <td width="148" class="formLabelGrey" valign="bottom">
                                                        &nbsp;Invoice Title
                                                    </td>
                                                    <td id="tdScheduleWindowBegin" runat="server" width="168" height="15" class="formLabelGrey"
                                                        valign="top">
                                                        Schedule Window Begin<span class="bodytxtValidationMsg">*</span>
                                                        <asp:RequiredFieldValidator ID="rqWOBegin" runat="server" ErrorMessage="Please enter Work Request Schedule Window Begin."
                                                            ForeColor="#EDEDEB" ControlToValidate="txtScheduleWInBegin">*</asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="rqWOBeginDate" ControlToValidate="txtScheduleWInBegin"
                                                            ErrorMessage="Work Request Schedule Window Begin Date should be in DD/MM/YYYY Format"
                                                            ForeColor="#EDEDEB" ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                            runat="server">*</asp:RegularExpressionValidator>
                                                        <asp:RangeValidator ID="rngWOBeginDate" ControlToValidate="txtScheduleWInBegin" MinimumValue="31/12/2005"
                                                            MaximumValue="31/12/2206" Type="Date" ForeColor="#EDEDEB" ErrorMessage="Work Request Schedule Window Begin Date should not be earlier than the current date."
                                                            runat="server">*</asp:RangeValidator>
                                                    </td>
                                                    <td id="tdScheduleEndDate" runat="server" width="160" height="15" class="formLabelGrey"
                                                        valign="top">
                                                        Schedule Window End<span class="bodytxtValidationMsg">*</span>
                                                        <asp:RegularExpressionValidator ID="rqWOEnd" ControlToValidate="txtScheduleWInEnd"
                                                            ErrorMessage="Work Request Schedule Window End Date should be in DD/MM/YYYY Format"
                                                            ForeColor="#EDEDEB" ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                            runat="server">*</asp:RegularExpressionValidator>
                                                        <asp:CustomValidator ID="rqWODateRange" runat="server" ErrorMessage="Work Request Schedule Window Begin Date should be earlier than Work Request Schedule Window End Date"
                                                            OnServerValidate="custValiDateRange_ServerValidate" ForeColor="#EDEDEB" ControlToValidate="txtScheduleWInEnd">*</asp:CustomValidator>
                                                    </td>
                                                    <td width="165" class="formLabelGrey" valign="top" runat="server" id="tdAppointmentTime"
                                                        visible="false">
                                                        Appointment Time
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td class="formLabelGrey" valign="top">
                                                        Quantity<span class="bodytxtValidationMsg">*</span>
                                                        <asp:RequiredFieldValidator ID="ReqValQuantity" runat="server" ErrorMessage="Please enter Quantity"
                                                            ForeColor="#EDEDEB" ControlToValidate="txtQuantity">*</asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegExpQuantity" ControlToValidate="txtQuantity"
                                                            ErrorMessage="Quantity should be in decimal Format" ForeColor="#EDEDEB" ValidationExpression="^[1-9]\d*(\.\d+)?$"
                                                            runat="server">*</asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td width="165" class="formLabelGrey" valign="top">
                                                        <asp:TextBox ID="txtWOTitle" runat="server" CssClass="formFieldGrey width150" TabIndex="19"></asp:TextBox>
                                                    </td>
                                                    <td width="148" class="formLabelGrey" valign="top">
                                                        <asp:TextBox ID="txtInvoiceTitle" MaxLength="600" runat="server" CssClass="formFieldGrey width150"
                                                            TabIndex="23"></asp:TextBox>
                                                    </td>
                                                    <td width="168" height="24" valign="top" id="tdTxtScheduleWInBegin" runat="server">
                                                        <asp:TextBox ID="txtScheduleWInBegin" runat="server" CssClass="formFieldGrey width120"
                                                            TabIndex="2" onChange="javascript:CalculateQuantity()"></asp:TextBox>
                                                        <img alt="Click to Select" src="Images/calendar.gif" id="btnBeginDate" style="cursor: pointer;
                                                            vertical-align: middle;" />
                                                        <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnBeginDate"
                                                            TargetControlID="txtScheduleWInBegin" ID="calBeginDate" runat="server">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                    <td width="160" height="24" valign="top" id="tdTxtScheduleWInEnd" runat="server">
                                                        <asp:TextBox ID="txtScheduleWInEnd" runat="server" CssClass="formFieldGrey width120"
                                                            TabIndex="3" onChange="javascript:CalculateQuantity()"></asp:TextBox>
                                                        <img alt="Click to Select" src="Images/calendar.gif" id="btnEndDate" style="cursor: pointer;
                                                            vertical-align: middle;" />
                                                        <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnEndDate"
                                                            TargetControlID="txtScheduleWInEnd" ID="calEndDate" runat="server">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                    <td width="165" class="formLabelGrey" valign="top" runat="server" id="tdDdbxAppointmentTime"
                                                        visible="false">
                                                        <asp:DropDownList Visible="true" ID="drpTime" runat="server" CssClass="formFieldGrey"
                                                            Width="80" TabIndex="3" onkeydown='javascript:ondrpTimeChange(this.id);' onkeyup='javascript:ondrpTimeChange(this.id);'
                                                            onchange='javascript:ondrpTimeChange(this.id);'>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RqFldAptTime" Enabled="false" runat="server" ErrorMessage="Please select Appointment time"
                                                            ForeColor="#EDEDEB" ControlToValidate="drpTime">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td valign="top">
                                                        <asp:TextBox ID="txtQuantity" runat="server" CssClass="formFieldGrey" TabIndex="8"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr valign="top" height="10">
                                                    <td width="165" class="formLabelGrey" valign="bottom">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td width="165" class="formLabelGrey" valign="bottom">
                                                        &nbsp;PO Number
                                                    </td>
                                                    <td width="163" class="formLabelGrey" valign="bottom">
                                                        Job Number
                                                    </td>
                                                    <td width="163" class="formLabelGrey" valign="bottom">
                                                        <asp:Label ID="lblLocationOfGoods" runat="server" Text="Location of Goods"></asp:Label>
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td width="165" class="formLabelGrey" valign="top">
                                                        <asp:TextBox ID="txtPONumber" runat="server" CssClass="formFieldGrey width150" TabIndex="19"></asp:TextBox>
                                                    </td>
                                                    <td width="163" class="formLabelGrey" valign="top">
                                                        <asp:TextBox ID="txtJRSNumber" runat="server" CssClass="formFieldGrey width150" TabIndex="20"></asp:TextBox>
                                                    </td>
                                                    <td width="163" class="formLabelGrey" valign="top">
                                                        <asp:DropDownList ID="drpdwnGoodsCollection" runat="server" DataTextField="Name"
                                                            DataValueField="AddressId" CssClass="formFieldGrey width150" AutoPostBack="False">
                                                        </asp:DropDownList>
                                                        <asp:CustomValidator ID="CustomValidatorLocationOfGoods" runat="server" ErrorMessage="Please enter goods location"
                                                            OnServerValidate="CustomValidatorLocationOfGoods_ServerValidate" ForeColor="#EDEDEB"
                                                            ControlToValidate="drpdwnGoodsCollection">*</asp:CustomValidator>
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                    </tr>
                                    <tr valign="top" height="10">
                                        <td width="165" class="formLabelGrey" valign="bottom">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td width="165" class="formLabelGrey" valign="bottom">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Wholesale Price
                                            <asp:RegularExpressionValidator ID="RgValPositiveNumber1" runat="server" ForeColor="#EDEDEB"
                                                ControlToValidate="txtWholesalePrice" ErrorMessage="Please enter a positive whole number for Wholesale Price."
                                                ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator>
                                        </td>
                                        <td width="165" class="formLabelGrey" valign="bottom">
                                            Related Workorder
                                        </td>
                                        <td width="165" class="formLabelGrey" valign="bottom">
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtRelatedWorkorder" Display="None"
                                                ErrorMessage="Please Enter Valid Related Work Order Number" ForeColor="#EDEDEB" 
                                                ValidationExpression="^\d{11,11}$" runat="server"></asp:RegularExpressionValidator>
                                        </td>
                                        <td id="tdHdrEstimatedTimeInDays" runat="server" visible="false" width="163" class="formLabelGrey"
                                            valign="bottom">
                                            Estimated Time (Days)
                                        </td>
                                        <td id="tdHdrWholesaleDayRate" runat="server" visible="false" width="163" class="formLabelGrey"
                                            valign="bottom">
                                            Wholesale Day Rate
                                        </td>
                                        <td width="160" class="formLabelGrey" valign="bottom">
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td width="165" class="formLabelGrey" valign="top">
                                            &pound;
                                            <asp:TextBox ID="txtWholesalePrice" runat="server" CssClass="formFieldGrey width120"
                                                Style="text-align: right" Text="0" Width="140"></asp:TextBox>
                                        </td>
                                        <td width="165" class="formLabelGrey" valign="top">
                                            <asp:TextBox ID="txtRelatedWorkorder" runat="server" CssClass="formFieldGrey width120"
                                                Width="140"></asp:TextBox>
                                        </td>
                                        <td id="tdEstimatedTimeInDays" runat="server" visible="false" width="163" class="formLabelGrey"
                                            valign="top">
                                            <asp:TextBox ID="txtEstimatedTimeInDaysSETWP" onChange="javascript:CalculatePrice(this.id)"
                                                runat="server" CssClass="formFieldGrey width150" TabIndex="20"></asp:TextBox>
                                        </td>
                                        <td id="tdWholesaleDayRate" runat="server" visible="false" width="163" class="formLabelGrey"
                                            valign="top">
                                            &pound;
                                            <asp:TextBox ID="txtWholesaleDayRateSETWP" onChange="javascript:CalculatePrice(this.id)"
                                                runat="server" CssClass="formFieldGrey width150" TabIndex="21"></asp:TextBox>
                                        </td>
                                        <td width="160" class="formLabelGrey" valign="top">
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr runat="server" id="trSetPPtxt" visible="false">
                                        <td class="formLabelGrey" height="35" valign="bottom">
                                            <asp:CustomValidator ID="CustPlatformPriceValidator" runat="server" ControlToValidate="txtSpendLimitPP"
                                                OnServerValidate="ServerValidatePP" ErrorMessage="Please enter a positive number for Portal Price">*</asp:CustomValidator>
                                            &nbsp;&nbsp;&nbsp;&nbsp;Portal Price:
                                            <asp:RegularExpressionValidator ID="RgValPPPositiveNumber" runat="server" ForeColor="#EDEDEB"
                                                ControlToValidate="txtSpendLimitPP" ErrorMessage="Please enter a positive whole number for Portal Price."
                                                ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr id="trSetPPtxtbox" visible="false" runat="server">
                                        <td width="155">
                                            <span class="txtWelcome">&pound;</span>
                                            <asp:TextBox ID="txtSpendLimitPP" Text="0" runat="server" CssClass="formFieldGrey width120"
                                                Style="text-align: right" TabIndex="17" Width="140"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                </td> </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr id="trupsell" runat="server" visible="false">
                                    <td>
                                        <table id="tblUpSellFields" width="100%" align="left" border="0" cellspacing="0"
                                            cellpadding="0" style="display: inline;" runat="server">
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" id="tblUpSell" style="display: inline;" runat="Server" border="0"
                                                                    cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td id="td1" width="165" height="15" class="formLabelGrey">
                                                                            &nbsp;&nbsp;&nbsp;UpSell Price(excl.VAT)
                                                                        </td>
                                                                        <td id="td2" height="15" width="175" class="formLabelGrey">
                                                                            UpSell Invoice Title
                                                                        </td>
                                                                        <td id="td5" height="15" class="formLabelGrey">
                                                                            Customer Email
                                                                            <asp:RegularExpressionValidator ID="regularEmail" runat="server" ErrorMessage="Please enter Customer Email Address in correct format"
                                                                                ForeColor="#EDEDEB" ControlToValidate="txtCustEmail" ValidationExpression="^.+@[^\.].*\.[a-z]{2,}$">*</asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="top">
                                                                        <td height="24" id="td3" width="165" class="formLabelGrey">
                                                                            &pound;
                                                                            <asp:TextBox ID="txtUpSellPrice" TabIndex="547" runat="server" Style="text-align: right"
                                                                                CssClass="formFieldGrey width120" Text="0" Width="140"></asp:TextBox>
                                                                        </td>
                                                                        <td height="24" id="td4" width="175" class="formLabelGrey">
                                                                            <asp:TextBox ID="txtUpSellInvoiceTitle" TabIndex="548" runat="server" CssClass="formFieldGrey width150"
                                                                                Text="OrderWork Services"></asp:TextBox>
                                                                        </td>
                                                                        <td height="24" id="td6" class="formLabelGrey">
                                                                            <asp:TextBox ID="txtCustEmail" TabIndex="548" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                            <asp:CustomValidator ID="CustomValCustomerEmail" runat="server" ErrorMessage="Please add a customer email for this workorder"
                                                                                ForeColor="#EDEDEB" ControlToValidate="txtUpSellPrice">*</asp:CustomValidator>
                                                                            <asp:CustomValidator ID="CustomValInvoiceTitle" runat="server" ErrorMessage="Please specify an Upsell invoice Title"
                                                                                ForeColor="#EDEDEB" ControlToValidate="txtUpSellPrice">*</asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="24" colspan="3" class="formLabelGrey">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td id="tdChkCCPayeeDetails" height="15" width="171" class="formLabelGrey">
                                                                            <asp:CheckBox ID="ChkCCPayeeDetails" TabIndex="549" runat="server" Text='CC Payee Details'
                                                                                CssClass="formLabelGrey" AutoPostBack="false" onclick="javascript:ChkCCPayeeDetailsUpdateWP();">
                                                                            </asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <asp:RegularExpressionValidator ID="reUpSellPrice" runat="server" ForeColor="#EDEDEB"
                                                                                ControlToValidate="txtUpSellPrice" ErrorMessage="Please enter a positive number for UpSell Price"
                                                                                ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblContactInfoUpSell"
                                                                    runat="server" style="display: none;">
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <table id="Table1" border="0" cellspacing="0" cellpadding="0" runat="server">
                                                                                <tr>
                                                                                    <td width="171" class="formLabelGrey">
                                                                                        CC First Name
                                                                                    </td>
                                                                                    <td width="160" id="tdCCLastName" class="formLabelGrey" runat="server">
                                                                                        CC Last Name
                                                                                    </td>
                                                                                    <td class="formLabelGrey">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr valign="top">
                                                                                    <td width="171">
                                                                                        <asp:TextBox ID="txtBCFName" TabIndex="550" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                                    </td>
                                                                                    <td width="160">
                                                                                        <asp:TextBox ID="txtBCLName" TabIndex="551" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="formLabelGrey">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15" colspan="2" class="formLabelGrey" width="320">
                                                                            CC Address
                                                                        </td>
                                                                        <td height="15" class="formLabelGrey">
                                                                            CC City
                                                                        </td>
                                                                        <td height="15" class="formLabelGrey">
                                                                            CC County/State
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="top">
                                                                        <td height="24" colspan="2" width="320">
                                                                            <asp:TextBox ID="txtBCAddress" TabIndex="552" runat="server" CssClass="formFieldGrey width330"
                                                                                Width="322"></asp:TextBox>
                                                                        </td>
                                                                        <td height="24">
                                                                            <asp:TextBox ID="txtBCCity" TabIndex="553" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                        </td>
                                                                        <td height="24">
                                                                            <asp:TextBox ID="txtBCCounty" TabIndex="554" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15" class="formLabelGrey" width="170">
                                                                            CC Postal Code
                                                                        </td>
                                                                        <td height="15" class="formLabelGrey" width="170">
                                                                            CC Phone
                                                                        </td>
                                                                        <td height="15" class="formLabelGrey" width="170">
                                                                            CC Fax
                                                                        </td>
                                                                        <td height="15" class="formLabelGrey">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="top">
                                                                        <td height="24">
                                                                            <asp:TextBox ID="txtBCPostalCode" TabIndex="555" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                        </td>
                                                                        <td height="24">
                                                                            <asp:TextBox ID="txtBCPhone" TabIndex="556" runat="server" CssClass="formFieldGrey width150"
                                                                                MaxLength="49"></asp:TextBox>
                                                                        </td>
                                                                        <td height="24" colspan="2">
                                                                            <asp:TextBox ID="txtBCFax" TabIndex="557" runat="server" CssClass="formFieldGrey width150"
                                                                                MaxLength="49"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="22" valign="bottom" id="tdSpecialInstText" runat="server" class="formLabelGrey">
                                        Special Instructions (These instructions are only shared with the specialist who
                                        will do the work)
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtSpecialInstructions" runat="server" onblur="javascript:removeHTML(this.id);"
                                            CssClass="formFieldGrey width934" TextMode="MultiLine" Rows="4" TabIndex="8"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formLabelGrey" height="24" valign="bottom">
                                        <asp:Label ID="lblProductsPurchased" runat="server" Text="Products Pruchased"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtProductsPurchased" runat="server" CssClass="formFieldGrey width934"
                                            TextMode="MultiLine" onblur="javascript:removeHTML(this.id);" Rows="4"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formLabelGrey" height="24" valign="bottom">
                                        Please add any comments you have below. Thank You.
                                        <asp:RequiredFieldValidator ID="rqWOComments" runat="server" ErrorMessage="Please enter Comment."
                                            ForeColor="#EDEDEB" ControlToValidate="txtWOComment">*</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="rqWOupsale" runat="server" ErrorMessage="UpSell Price(excl.VAT) Cannot be Blank"
                                            ForeColor="#EDEDEB" ControlToValidate="txtUpSellPrice">*</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="rqWOwholesale" runat="server" ErrorMessage="Wholesale Price Cannot be Blank"
                                            ForeColor="#EDEDEB" ControlToValidate="txtWholesalePrice">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtWOComment" onblur="javascript:removeHTML(this.id);" runat="server"
                                            CssClass="formFieldGrey width934" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                    </td>
                                </tr>
                                </table>
                                <table border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td width="20">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            <table class="marginL20" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0"
                                                runat="server" id="tblConfirm">
                                                <tr>
                                                    <td width="5">
                                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                    </td>
                                                    <td class="txtBtnImage">
                                                        <asp:LinkButton ID="lnkConfirm" CausesValidation="false" runat="server" CssClass="txtListing"
                                                            Text="<img src='Images/Icons/Icon-Confirm.gif' title='Cancel' width='12' height='12' align='absmiddle' hspace='1' vspace='2' border='0'> Confirm"></asp:LinkButton>
                                                    </td>
                                                    <td width="5">
                                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right">
                                            <table class="marginL20" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                                <tr>
                                                    <td width="5">
                                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                    </td>
                                                    <td class="txtBtnImage">
                                                        <asp:LinkButton ID="lnkCancel" CausesValidation="false" runat="server" CssClass="txtListing"
                                                            Text="<img src='Images/Icons/Cancel.gif' title='Cancel' width='11' height='11' align='absmiddle' hspace='1' vspace='2' border='0'> Cancel"></asp:LinkButton>
                                                    </td>
                                                    <td width="5">
                                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="20" align="right">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlConfirmation" runat="server" Visible="false">
                                <table border="0" cellspacing="0" cellpadding="10">
                                    <tr>
                                        <td colspan="3">
                                            <asp:Label ID="Label1" runat="server" CssClass="formLabelGrey" Text="Send WorkOrder Update to all suppliers"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <table class="marginL20" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                                <tr>
                                                    <td width="5">
                                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                    </td>
                                                    <td width="150" class="txtBtnImage">
                                                        <asp:LinkButton ID="lnkbtnConfirm" CausesValidation="false" runat="server" CssClass="txtListing"
                                                            Text="Send Mail"></asp:LinkButton>
                                                    </td>
                                                    <td width="5">
                                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="30">
                                        </td>
                                        <td>
                                            <table class="marginL20" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                                <tr>
                                                    <td width="5">
                                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                    </td>
                                                    <td width="200" class="txtBtnImage">
                                                        <asp:LinkButton ID="lnkbtnCancel" CausesValidation="false" runat="server" CssClass="txtListing"
                                                            Text="Continue without sending email"></asp:LinkButton>
                                                    </td>
                                                    <td width="5">
                                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlSuccess" runat="server" Visible="false">
                                <table border="0" cellspacing="0" cellpadding="10">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblSuccess" runat="server" CssClass="formLabelGrey"></asp:Label>
                                        </td>
                                        <td align="right">
                                            <table class="marginL20" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                                <tr>
                                                    <td width="5">
                                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                    </td>
                                                    <td width="30" class="txtBtnImage">
                                                        <asp:LinkButton ID="lnkBack" CausesValidation="false" runat="server" CssClass="txtListing"
                                                            Text="Back"></asp:LinkButton>
                                                    </td>
                                                    <td width="5">
                                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger EventName="Click" ControlID="lnkConfirm" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelCreateWorkOrder"
                        runat="server">
                        <ProgressTemplate>
                            <div>
                                <img align="middle" src="Images/indicator.gif" />
                                Please Wait...
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePanelCreateWorkOrder"
                        CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
                    <!-- InstanceEndEditable -->
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
