<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :References" CodeBehind="References.aspx.vb" Inherits="Admin.References" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
			
	        <div id="divContent">
         
		
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr height="9">
           <td></td>
           <td></td>
         </tr>
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<asp:Label ID="lblError" runat="server" CssClass="bodytxtValidationMsg"></asp:Label>
			<asp:Panel id="pnlReferenceForm" runat=server>
			 <%@ Register TagPrefix="ucS1" TagName="UCReferenceUK" Src="~/UserControls/UK/UCReferenceUK.ascx" %>
    				<ucS1:UCReferenceUK id="UCReferenceUK1" runat="server"></ucS1:UCReferenceUK>
			</asp:Panel>			               
            <!-- InstanceEndEditable --></td>
			   
		  
            <td width="220" align="center" valign="top">
			 
				 <%@ Register TagPrefix="uc1" TagName="UCAccountSumm" Src="~/UserControls/UK/UCAccountSummaryUK.ascx" %>
             <uc1:UCAccountSumm id="UCAccountSumm1" runat="server"></uc1:UCAccountSumm> 
			 
              
			
			<a  id="lnkAddUsers"  tabindex="120" runat="server" onMouseOver="MM_swapImage('AddUsers','','Images/buttons/Btn-AddUser-Roll.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="Images/buttons/Btn-AddUser.gif" title="Add Users" name="AddUsers" width="197" height="43" border="0" class="marginB20"  ></a><br><br>
				
			<a  tabindex="119" id="lnkAddLocationsSupp" runat="server" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Locations1','','Images/buttons/AddLocation-Rolover.gif',1)"><img src="Images/buttons/AddLocation.gif" title="Add Locations" name="Locations1" width="197" height="43" border="0" class="marginB20" id="Locations1" ></a>   <br>     
             <a id="lnkCompProfile" runat="server" class="footerTxtSelected ">Company Profile</a><br>
             <a id="lnkloc"  runat="server" class="footerTxtSelected ">Locations Listing</a><br>
             <a id="lnkuser"  runat="server"  class="footerTxtSelected ">Users Listing</a> <br> 
			<span runat="server" id="rgnReferences"></span>
			 
			 <%--<a href="Comments.aspx" class="footerTxtSelected ">Comments</a><br>  --%>
		<a class="footerTxtSelected " id="lnkViewFavSuppliers" runat="server">View All Favourite Suppliers</a><br>
			    <a  class="footerTxtSelected " id="lnkAddProduct" runat="server">Service Listing</a><br>
                <a  class="footerTxtSelected " id="lnkAddProductNew" runat="server" target="_blank" >Service Listing New</a><br>
			 <a class="footerTxtSelected " id="lnkAutoMatch" runat="server" title="AutoMatch Settings">AutoMatch Settings</a><br>
			 <a class="footerTxtSelected " id="lnkAccSettings" runat="server" title="Account Settings">Account Settings</a><br>
             <%--Poonam - OA-494 - OA - Links missing in Specialist form - Just changed the visibility to True from False--%>     
                 <a  class="footerTxtSelected " id="lnkViewBlackListClient" runat="server" visible="True">View Client Blacklist</a><br>
                <a  class="footerTxtSelected " id="lnkViewWorkingDays" runat="server" visible="True" >View Working Days</a><br>
                <a  class="footerTxtSelected " id="lnkViewAllInvoices" runat="server">View All Invoices</a><br>
                <a  class="footerTxtSelected " id="lnkViewAccountHistory" runat="server">Account History</a><br>
             </td>
            
	   
         </tr>
         </table>
      </div>
         </asp:Content>