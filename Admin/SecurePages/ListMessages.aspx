<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile ="~/MasterPage/MyOrderWork.Master" CodeBehind="ListMessages.aspx.vb" Inherits="OrderWorkUK.ListMessages" 
    title="List Messages" validateRequest="true"  %>
    <%@ Register TagPrefix="uc1" TagName="GridPager" Src="~/UserControls/UK/GridPager.ascx"%>
    
<asp:Content ID="Content2" ContentPlaceHolderID="ContentHolder" runat="server">
<style type="text/css">
.gridHdr
{
	background-color:#993366;
	font-family:Verdana;
	font-size: 11px;
	font-weight: bold;
	color: #555354;
	text-decoration: none;
	height:28px;
	vertical-align:middle;
	text-align:left;
}
.gridRowText {

/*border-left:none 0px;
border-right:none 0px;*/
/*font-size:12px;*/

}
.gridRow {
border-left:none 0px;
border-right:none 0px;

}
#divPagerBtm{
    float:left; 
    clear:right; 
    width:300px; 
    margin-left:140px;
}
*html #divPagerBtm{
    float:left; 
    clear:right; 
    width:300px; 
    margin-left:50px;
}

</style>
<div id="divTopBand" class="divContainerStyle floatLeft paddingBottom10" >
    <div style="width:992px;padding-left:30px;"  class="clsGreyBandStyleFund marginBottom10">
                             <strong >My Messages </strong>
                             </div>
	
	 <asp:UpdatePanel ID="updatePnlListMessages" runat="server"  RenderMode="Inline">
  <ContentTemplate>	
	<div id="divMiddleBand" style="padding-left:0px; clear:none;">
	<div style="width:1015px;margin-top:2px;margin-bottom:2px; float:left; clear:right;">
	<div id="tdTotalRecs" style="width:100px; padding-top:10px; padding-left:5px; text-align:left; float:left; clear:right; " runat="server" ></div>
<div id="divPagerTop" style="float:left; clear:right; width:300px;margin-left:30px;"><uc1:GridPager ID="GridPagerTop" runat="server"></uc1:GridPager></div>
<div id="Div1" style="width:200px; padding-top:10px; text-align:right; font-size:11px;float:right;clear:right; " runat="server" >View Records													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" CssClass="formField marginLR5" ID="ddlPageSize" runat="server" AutoPostBack="true">
													<asp:ListItem>10</asp:ListItem>
													<asp:ListItem>15</asp:ListItem>
													<asp:ListItem>20</asp:ListItem>
													<asp:ListItem>25</asp:ListItem>
													</asp:DropDownList> per page
											  </div>
</div>
<div style="float:left;">
	    <asp:DataList runat="server" ID="dlMessagesList" Width="1022px">
	    <HeaderTemplate>
	    <table width="1022px" cellspacing="0" border="0">
	        <tr class="gridHdr" style="height:15px;" >
	        <td class="txtGrid" width="108px" style="padding-left:0px;"><asp:LinkButton id="lnkDate" runat="server"   CommandName="DateModified" style=" txtGrid;" >Date</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/OWLibrary/Images/WhiteArrow-Up.gif" id="imgDateModified" runat="server" Visible="false"/></td>
	        <td class="txtGrid" width="400px" style="padding-left:0px;"><asp:LinkButton id="lnkSubject" runat="server"  CommandName="Subject" style=" txtGrid;" >Subject</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/OWLibrary/Images/WhiteArrow-Up.gif" id="imgSubject" runat="server" Visible="false"  /></td>
	        </tr>
	    </table>
	    </HeaderTemplate>
	    <ItemTemplate>
	        <table width="1022px" cellspacing="0" border="0" id="tblSubject" runat="server" onclick='javascript:Expand(this.id)' style="margin-top:-2px;" >
	        <tr class="gridRow" id="trdlRow">
	        <td class="gridRowText colorBlueListing" width="103px" style="height:15px;" ><%#Container.DataItem("DateModified")%></td>
	        <td class="gridRowText" width="400px" style="height:15px;"><%#Container.DataItem("Subject")%>
	        <asp:Image ID="IsImp" runat="server" ImageUrl="~/OWLibrary/Images/Icons/alertsmall.gif" ImageAlign="absMiddle" AlternateText="Important Message" ToolTip="Important Message"  Visible='<%#IIf(Container.DataItem("IsImp") = 0 ,False,True)%>' />
	        </td>
	        </tr>
	    </table>
	    	    <asp:HiddenField runat="server" id="hdnMsgID" value='<%#Container.DataItem("MsgID") %>' />

	    <div class="txtGrid" style="display:none; background:#FFFFDE; font-size:11px;" id='divMessageDetails' runat="server" >
	       <div style="width:635px;">
           <pre>
	        <%#Container.DataItem("Comments")%></pre>
	        </div>
	    </div>
	    </ItemTemplate>
	    </asp:DataList>
</div>	    
<div style="width:1015px;margin-top:2px;margin-bottom:2px;" >
<div id="divPagerBtm" ><uc1:GridPager id="GridPagerBtm" runat="server"></uc1:GridPager></div>
<div id="Div2" style="width:200px; padding-right:0px; padding-top:5px; text-align:right; font-size:11px;float:right; " runat="server" >View Records													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSizeBtm_SelectedIndexChanged" CssClass="formField marginLR5" ID="ddlPageSizeBtm" runat="server" AutoPostBack="true">
													<asp:ListItem>10</asp:ListItem>
													<asp:ListItem>15</asp:ListItem>
													<asp:ListItem>20</asp:ListItem>
													<asp:ListItem>25</asp:ListItem>
													</asp:DropDownList> per page
											  </div>
</div>
	</div>
	
	<div id="divClearBoth"></div>
	</ContentTemplate>
</asp:UpdatePanel> 
	</div>
     <div class="clearBoth"></div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
