<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SalesInvDetails.aspx.vb" Inherits="OrderWorkUK.SalesInvDetails" Title="OrderWork: Sales Invoice Details" MasterPageFile ="~/MasterPage/MyOrderWork.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">

	<div id="divTopBand" ></div>
	<div id="divMiddleBand">
	<%@ Register TagPrefix="uc1" TagName="UCOWInvDetails" Src="~/UserControls/UK/UCOWInvDetails.ascx" %>
	<uc1:UCOWInvDetails id="UCOWInvDetails1" runat="server"></uc1:UCOWInvDetails></div>
	<div id="divBtmBand"></div>
</asp:content>
