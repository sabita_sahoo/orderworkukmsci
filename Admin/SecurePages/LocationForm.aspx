<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LocationForm.aspx.vb" Title="OrderWork: Location" Inherits="OrderWorkUK.LocationForm" MasterPageFile ="~/MasterPage/MyOrderWork.Master" ValidateRequest="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">

<div id="divLocation" class="divContainerStyle floatLeft" style="border:0px;">
    
	<%@ Register TagPrefix="uc1" TagName="UCLocationForm" Src="~/UserControls/UK/UCLocation.ascx" %>
	<uc1:UCLocationForm id="UCLocationForm1" runat="server"></uc1:UCLocationForm>

    	
	<%@ Register TagPrefix="uc2" TagName="UCAddLocationForm" Src="~/UserControls/UK/UCLocationForm.ascx" %>
	<uc2:UCAddLocationForm id="UCLocationForm2" runat="server"></uc2:UCAddLocationForm></div>
	

<div style="clear:both;"></div>
</asp:Content>