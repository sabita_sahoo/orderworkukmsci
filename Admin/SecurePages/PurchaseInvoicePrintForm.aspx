<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PurchaseInvoicePrintForm.aspx.vb" Inherits="OrderWorkUK.PurchaseInvoicePrintForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>OrderWork: Self Bill Invoice</title>
<style type="text/css">
<!--
.invoiceLabel {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 32px;
	color: #B7B7B7;
	font-weight: bold;
	line-height:38px;

}
.InvoiceText {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.bdr1 {
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 4px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.bdr2 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #C6C6C6;
	border-right-color: #C6C6C6;
	border-bottom-color: #C6C6C6;
	border-left-color: #C6C6C6;
}
.bdr3 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.style3 {font-size: 11px}
.style5 {font-weight: bold}
.style6 {font-weight: bold}
td
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 16px;
	color: #000000;
}
body {
	background-color: #CECECE;
	height:100%;
}.VatMessage {
	text-align: center;
}
.pb
 { page-break-after : always } ;
  }


-->
</style>
</head>
<body style="height:100%;" onload="javascript:this.print();">
		<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
			<table height="500" width="700" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FEFFD3">
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" align="center">
						  <tr>
							<td>&nbsp;</td>
							<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#CC3300; font-weight:bold;"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
						  </tr>
						</table>
					</td>
				</tr>
			</table>	
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlPrintContent" Visible="true">
			<form id="form1" runat="server">
			  
			<asp:Repeater ID="rptPrint" runat="server">
			<ItemTemplate> 
					<table border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
				  <tr>
					<td width="45" height="140">&nbsp;</td>
					<td width="650" height="140"><table width="100%" cellpadding="0" cellspacing="0" style=" border-width:medium; border-bottom-width:5px;border-bottom-color:#000000;">
						<tr>
						  <td width="311">&nbsp;</td>
						  <td width="131" align="left" valign="bottom" >&nbsp;</td>
						</tr>
						<tr>
						  <td width="311" valign="middle"><img src="OWLibrary/Images/Invoice_Logo_New.gif" runat="server" id="imgLogo" alt="OrderWork - Services Reinvented" border="0" class="LogoImg" /></td>
						  <td align="right" valign="bottom" ><table width="99%"  border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="right" valign="bottom"  style="height: 22px;"><span class="style1 style5">
								<span class="AddressTxt">
								<%# Container.DataItem("CompanyName") %>
								</span> </span></td>
							  </tr>
							  <tr>
								<td align="right" valign="bottom"  ><span class="AddressTxt">
								  <%#Container.DataItem("Address")%>
								</span></td>
							  </tr>
							  <tr>
								<td align="right" valign="bottom"><span class="AddressTxt">
									<%# Container.DataItem("City") %>
								<span class="AddressTxt">
								</span></td>
							  </tr>
							  <tr>
								<td align="right" valign="bottom"  > 
								   <%# Container.DataItem("PostCode") %>
							  </tr>
							  <tr>
								<td align="right" valign="bottom" > &nbsp;
									<span class="AddressTxt">
									<%#GetCompanyRegNo(Container.DataItem("CompanyRegNo"))%>
								</span></td>
							  </tr>
						  </table></td>
						</tr>
						<tr>
						  <td width="311" class="bdr1" >&nbsp;</td>
						  <td align="left" valign="bottom" class="bdr1" >&nbsp;</td>
						</tr>
					</table></td>
					<td width="45" height="140">&nbsp;</td>
				  </tr>
				  <tr>
					<td width="45" height="180" align="center">&nbsp;</td>
					<td width="650" height="180" align="center" valign="top"><p class="invoiceLabel">SELF BILL INVOICE</p>
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
						  <tr>
							<td align="left" valign="top"  style="border-bottom: 0px; border-right-width: 0px"><strong>Invoice To:<br>
							  </strong>
								<asp:Label ID="lblCompNameInvoiceTo" runat="server" Text='<%# GetCompanyName() %>' ></asp:Label>
								<br />
								<asp:Label ID="lblInvoiceTo" runat="server" Text='<%# getOWAddress() %>'></asp:Label>
							&nbsp;</td>
							<td align="right" valign="top"><table border="0" cellspacing="0" cellpadding="0">
								<tr>
								  <td width="95" align="left"><strong>Invoice No.&nbsp;</strong></td>
								  <td width="8" align="left">:</td>
								  <td align="left"><%# Container.DataItem("InvoiceNo") %></td>
								</tr>
								<tr>
								  <td width="95" align="left"><strong><asp:Label ID="lblCreditTermLabel" runat="server" Text='<%#IIF(Container.DataItem("IsNew")="0", "Credit Terms", "Payment Terms")%>'></asp:Label>&nbsp;</strong></td>
								  <td align="left">:</td>
								  <td align="left"><%#Container.DataItem("DynamicPaymentTerm") %></td>
								</tr>
								<tr>
								  <td width="95" align="left"><strong>Invoice Date&nbsp;</strong></td>
								  <td align="left">:</td>
								  <td align="left"><%#Strings.FormatDateTime(Container.DataItem("InvoiceDate").ToString(), DateFormat.ShortDate)%></td>
								</tr>
								<tr>
								  <td width="95" align="left"><strong>VAT No.</strong></td>
								  <td align="left">:</td>
								  <td align="left"><strong>
									<%# Container.DataItem("VATRegNo") %>
								  </strong></td>
								</tr>
							</table></td>
						  </tr>
						</table>
					<p>&nbsp;</p></td>
					<td width="45" height="180" align="center">&nbsp;</td>
				  </tr>
				  <tr>
					<td width="45" align="right" valign="top">&nbsp;</td>
                    
				      <%--Old format--%>
				      <td width="650" align="right" valign="top" id="tdOldFormat" runat="server" Visible='<%#IIF(Container.DataItem("IsNew")="0", True, False)%>'>
                      <table height="60" width="650" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td width="80" align="left" height="20"><strong>Date</strong></td>
						<td width="120" align="left" height="20"><strong>Work Order ID</strong></td>
						<td width="206" align="left" height="20"><strong>Description</strong></td>
						<td width="82" align="right" height="20"><strong>Net Price</strong></td>
					    <td width="90" align="right" height="20"><strong>VAT( <%#OrderWorkLibrary.ApplicationSettings.VATPercentage(Container.DataItem("InvoiceDate").ToString()) %> %)</strong></td>
						<td width="72" align="right" style=" border-bottom:0px; height: 33px;"><strong>Amount</strong></td>
					  </tr>
					  <tr>        
					  
						<td rowspan="2" align="left" valign="top"  style=" border-right:0px; ;">
							<%#Strings.FormatDateTime(Container.DataItem("CloseDate").ToString(), DateFormat.ShortDate)%>
						</td>
						<td rowspan="2" align="left" valign="top"  style=" border-right:0px; width: 120px;">
							 <%#Container.DataItem("WorkOrderId")%>
						</td>
						<td height="20" align="left"  style=" border-right:0px; ">
						<%# Container.DataItem("Description") %>
					</td>
						<td width="82" height="20" align="right"  style=" border-right:0px; ">
						<%# FormatCurrency(Container.DataItem("WOVal"), 2, TriState.True, TriState.True, TriState.False) %>
				   </td>
						<td width="90" height="20" align="right"  style=" border-right:0px;">
						<%# FormatCurrency(Container.DataItem("VATOnWP"), 2, TriState.True, TriState.True, TriState.False) %>
					</td>
						<td width="72" height="20"  align="right">
						<%#FormatCurrency(Container.DataItem("WOVal") + Container.DataItem("VATOnWP"), 2, TriState.True, TriState.True, TriState.False)%>
						
					</td>
					  </tr>
					  <tr>
						<td align="left"  style="border-right: 0px;border-top:0px; "> Less <asp:label ID="lblSPFees" runat="server" Text='<%#Container.DataItem("SPFees")%>'></asp:label>% discount</td>
						<td width="82" height="20" align="right"  style="border-right: 0px;border-top:0px; ">
							<%#FormatCurrency(Container.DataItem("Discount"), 2, TriState.True, TriState.True, TriState.False)%>
						</td>
						<td width="90" height="20" align="right"  style="border-right: 0px;border-top:0px; ">
							<%# FormatCurrency(Container.DataItem("VATOnDisc"), 2, TriState.True, TriState.True, TriState.False) %>
						</td>
						<td width="72"  align="right"  style="border-top:0px;">
							<%# FormatCurrency(Container.DataItem("Discount") + Container.DataItem("VATOnDisc"), 2, TriState.True, TriState.True, TriState.False) %>
						</td>
					  </tr>
					  </table>
					  <br>
					  <table border="0" cellspacing="0" >
						  <tr>
							<td width="190" align="right"  style=" border-bottom:0px; border-right-width:0px; "><strong>Sub-Total</strong></td>
							<td width="70" align="right"  style=" border-bottom:0px; ">
								<%# FormatCurrency(Container.DataItem("NetPrice"), 2, TriState.True, TriState.True, TriState.False) %>
							</td>
						  </tr>
						  <tr>
						    <td width="190" height="20" align="right"   style=" border-bottom:0px; border-right-width:0px; "><strong>Total VAT (<%# OrderWorkLibrary.ApplicationSettings.VATPercentage(Container.DataItem("InvoiceDate").ToString()) %>%) </strong></td>
							<td width="70" height="20" align="right"  style=" border-bottom:0px; "><%# FormatCurrency(Container.DataItem("VAT"), 2, TriState.True, TriState.True, TriState.False) %>
							</td>
						  </tr>
					      <tr>
					          <td width="190" height="20" align="right"   style=" border-bottom:0px; border-right-width:0px;">&nbsp;</td>
					          <td width="70" height="20" align="right"  style=" border-bottom:0px;">&nbsp;</td>
					      </tr>
						  <tr>
							<td width="190" height="20" align="right"   style="border-right-width:0px; "><strong>Total Amount </strong></td>
							<td width="70" height="20"  align="right" ><span class="style6">
							  <%# FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False) %>
							</span> </td>
						  </tr>
					  </table>
						<p class="VatMessage"><strong>The VAT shown is your output tax due to Customs & Excise</strong></p>
					  <table width="100%" border="0" cellpadding="0" cellspacing="0">
						  <tr>
							<td height="40" colspan="3"  align="left" valign="middle" class="bdr2" ><strong >Supplier Bank Details</strong></td>
						  </tr>
						  <tr>
							<td width="110" align="left"><strong>Account Name </strong></td>
							<td width="8" align="left" ><strong>:</strong></td>
							<td align="left"  ><%#OrderWorkLibrary.Encryption.Decrypt(Container.DataItem("BankAccountName"))%></td>
						  </tr>
						  <tr>
							<td align="left"><strong>Sort-Code &nbsp;</strong></td>
							<td align="left" ><strong>:</strong></td>
							<td align="left"  ><%#OrderWorkLibrary.Encryption.Decrypt(Container.DataItem("BankSortCode"))%></td>
						  </tr>
						  <tr>
							<td align="left"><strong>Account Number</strong></td>
							<td align="left" ><strong>:</strong></td>
							<td align="left"  ><%#OrderWorkLibrary.Encryption.Decrypt(Container.DataItem("BankAccountNo"))%></td>
						  </tr>
						  <tr>
							<td align="left" colspan="3">&nbsp;</td>
						  </tr>
				    	</table>
				      </td>
			
                   
				      <%--New format--%>
				      <td width="650" align="right" valign="top" id="tdNewFormat" runat="server"  Visible='<%#IIF(Container.DataItem("IsNew")="1", True, False)%>'>
					 <table height="60" width="650" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td width="80" align="left" height="20"><strong>Date</strong></td>
						<td width="120" align="left" height="20"><strong>Work Order ID</strong></td>
						<td width="350" align="left" height="20"><strong>Description</strong></td>
						<td width="72" align="right" style=" border-bottom:0px; height: 33px;"><strong>Amount</strong></td>
					  </tr>
					  <tr>     
					  <td rowspan="8" align="left" valign="top"  style=" border-right:0px; ;">
							<%#Strings.FormatDateTime(Container.DataItem("CloseDate").ToString(), DateFormat.ShortDate)%>
						</td>
						<td rowspan="8" align="left" valign="top"  style=" border-right:0px; width: 120px;">
							 <%#Container.DataItem("WorkOrderId")%>
						</td>
						<td height="20" align="left"  style=" border-right:0px; ">
						<strong><%# Container.DataItem("Description") %></strong>
					    </td>
						<td width="72" height="20"  align="right">
						    <strong><%#FormatCurrency(Container.DataItem("WoValWithAdminFee"), 2, TriState.True, TriState.True, TriState.False)%></strong>
						</td>
					  </tr>
                         <tr><td>&nbsp;</td></tr>
				      <tr>   
					            <td  align="left" width="350" style=" border-right:0px; ">
					                Option Fast Payment Discount (<%# Container.DataItem("PaymentTerms")%> days selected - <%# Container.DataItem("PaymentDiscount")%>%)
					            </td>
					            <td width="72"  align="right">-&nbsp;<%#FormatCurrency(Container.DataItem("PaymentTermDiscountApplied"), 2, TriState.True, TriState.True, TriState.False)%>
					          	</td>
					    </tr>

					        <tr>   
					            <td  align="left" width="350" style=" border-right:0px; ">
					                Insurance Option (not selected)
					            </td>
					            <td width="72"   align="right">-&nbsp;<%# FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)%>
					            </td>
					      </tr>
					        <tr><td>&nbsp;</td></tr>
					        <tr>   
					            <td  align="left" width="350" style=" border-right:0px; ">
					                Total Value exc. VAT
					            </td>
					            <td width="72"   align="right">
					                <%#FormatCurrency(Container.DataItem("NetPrice"), 2, TriState.True, TriState.True, TriState.False)%>
					            </td>
					        </tr>
					        <tr>   
					            <td  align="left" width="350" style=" border-right:0px; ">
					                VAT( <%#OrderWorkLibrary.ApplicationSettings.VATPercentage(Container.DataItem("InvoiceDate").ToString()) %>%)
					            </td>
					            <td width="72"   align="right">
					                <%#FormatCurrency(Container.DataItem("VAT"), 2, TriState.True, TriState.True, TriState.False)%>
					            </td>
					        </tr>
					        <tr>   
					            <td  align="left" width="350" style=" border-right:0px; ">
					                <strong>Total Value inc.VAT</strong>
					            </td>
					            <td width="72"   align="right">
					                <strong>  <%#FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%></strong>
					            </td>
					        </tr>
					                    </table>
					 <br>
					    <p class="VatMessage"><strong>The VAT shown is your output tax due to Customs & Excise</strong></p>
				     <table width="100%" border="0" cellpadding="0" cellspacing="0">
					                    <tr>
					                    <td height="40" colspan="3"  align="left" valign="middle" class="bdr2" ><strong >Supplier Bank Details</strong></td>
					                    </tr>
					                    <tr>
					                    <td width="110" align="left"><strong>Account Name </strong></td>
					                    <td width="8" align="left" ><strong>:</strong></td>
					                    <td align="left"  ><%#OrderWorkLibrary.Encryption.Decrypt(Container.DataItem("BankAccountName"))%></td>
					                	  </tr>
						                     <tr>
						                   	<td align="left"><strong>Sort-Code &nbsp;</strong></td>
						                	<td align="left" ><strong>:</strong></td>
						                	<td align="left"  ><%#OrderWorkLibrary.Encryption.Decrypt(Container.DataItem("BankSortCode"))%></td>
					                	  </tr>
						                     <tr>
					                		<td align="left"><strong>Account Number</strong></td>
					                		<td align="left" ><strong>:</strong></td>
					                		<td align="left"  ><%#OrderWorkLibrary.Encryption.Decrypt(Container.DataItem("BankAccountNo"))%></td>
				                    		  </tr>
				            		  <tr>
					            		<td align="left" colspan="3">&nbsp;</td>
						                </tr>
					                </table>
					</td>
                      
					<td width="45" align="right" valign="top">&nbsp;</td>
				  </tr>
				</table>
				<div id="divPageBreak" runat="server" class="pb">&nbsp;</div>
				</ItemTemplate>
            </asp:Repeater>
			</form>
		</asp:Panel>
</body>
</html>
