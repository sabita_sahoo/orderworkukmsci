<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClientSLAReportPrintForm.aspx.vb" Inherits="OrderWorkUK.ClientSLAReportPrintForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>OrderWork: Best Buy SLA Report</title>
<style type="text/css">
<!--
.invoiceLabel {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 32px;
	color: #B7B7B7;
	font-weight: bold;
	line-height:38px;

}
.InvoiceText {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.bdr1 {
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 4px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.bdr2 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #C6C6C6;
	border-right-color: #C6C6C6;
	border-bottom-color: #C6C6C6;
	border-left-color: #C6C6C6;
}
.bdr3 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.style3 {font-size: 11px}
.style5 {font-weight: bold}
.style6 {font-weight: bold}
td
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 16px;
	color: #000000;
}
body {
	background-color: #CECECE;
	height:100%;
}.VatMessage {
	text-align: center;
}
-->
</style>
</head>
<body style="height:100%;" onload="javascript:this.print()">
		<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
			<table height="500" width="700" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FEFFD3">
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" align="center">
						  <tr>
							<td>&nbsp;</td>
							<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#CC3300; font-weight:bold;"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
						  </tr>
						</table>
					</td>
				</tr>
			</table>	
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlPrintContent" Visible="true">
			 <table height="95%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
            <tr>
              <td width="45" height="140">&nbsp;</td>
              <td width="650" height="140"><table width="100%" cellpadding="0" cellspacing="0" style=" border-width:medium; border-bottom-width:5px;border-bottom-color:#000000;">
                  <tr>
                    <td width="311">&nbsp;</td>
                    <td width="131" align="left" valign="bottom" >&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="311" valign="middle"><img src="OWLibrary/Images/Invoice_Logo_New.gif" runat="server" id="imgLogo" alt="OrderWork - Services Reinvented" border="0" class="LogoImg" /></td>
                    <td align="right" valign="bottom" >&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="311" class="bdr1" >&nbsp;</td>
                    <td align="left" valign="bottom" class="bdr1" >&nbsp;</td>
                  </tr>
              </table></td>
              <td width="45" height="140">&nbsp;</td>
            </tr>
            <tr>
            
            </tr>
             <tr>
                <td width="45">&nbsp;</td>
                <td width="650"><p class="invoiceLabel" style="padding-left:10px;">Best Buy SLA Report</p>
                    <div id="divMainReport" runat="server" style="padding-left:10px;">
         
             <table cellpadding="0" cellspacing="0" >
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td width="200">Total Submitted Work Orders</td>
                   <td width="20">:</td>
                   <td width="50"><asp:Label ID="lblSubmittedWO" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total Closed Work Orders</td>
                   <td>:</td>
                   <td><asp:Label ID="lblClosedWO" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total Work Orders that met SLA</td>
                   <td>:</td>
                   <td><asp:Label ID="lblOnTime" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total SLA Met (%)</td>
                   <td>:</td>
                   <td><asp:Label ID="lblSLAPercentageMet" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>1st Time Success (%)</td>
                   <td>:</td>
                   <td><asp:Label ID="lblFirstTimeSuccess" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>No. of Cancellations</td>
                   <td>:</td>
                   <td><asp:Label ID="lblCancellationNo" runat="server"></asp:Label></td>
                  </tr>                  
              </table>                              
         <div runat="server" id="divProduct" style="padding-top:20px;">
          <p class="paddingB4 HeadingRed"><strong>By SKU</strong> </p>
            <div >
             <asp:datalist id="DLProducts" RepeatColumns="2" runat="server">           
              <ItemTemplate>
                <table cellpadding="0" cellspacing="0" >
                <tr>
                 <td colspan="3"><b><%#Container.DataItem("ProductName")%></b></td>
                </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td width="200">Total jobs submitted</td>
                   <td width="20">:</td>
                   <td width="50"><%#Container.DataItem("SubmittedWO")%></td>
                   <td width="150">&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs closed</td>
                   <td>:</td>
                   <td><%#Container.DataItem("ClosedWO")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs within SLA (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("SLAPercentageMet")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total 1st time success (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("FirstTimeSuccess")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Cancelled jobs</td>
                   <td>:</td>
                   <td><%#Container.DataItem("CancellationNumber")%></td>
                   <td>&nbsp;</td>
                  </tr>
                    <tr>
                      <td colspan="4">&nbsp;</td>
                    </tr>
                </table>                  
            </ItemTemplate>
          </asp:datalist>  
          </div>        
              <div class="divClearBoth"></div>   
            </div>           
            
           <div runat="server" id="divBillingLoc">
           <p class="paddingB4 HeadingRed"><strong>By Store</strong> </p>            
              <div>
                 <asp:datalist id="DLBillingLoc" RepeatColumns="3" runat="server">           
              <ItemTemplate>
                <table cellpadding="0" cellspacing="0">
                  <tr>
                     <td colspan="3"><b><%#Container.DataItem("BillingLocation")%></b></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td width="200">Total jobs submitted</td>
                   <td width="20">:</td>
                   <td width="50"><%#Container.DataItem("SubmittedWO")%></td>
                   <td width="150">&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs closed</td>
                   <td>:</td>
                   <td><%#Container.DataItem("ClosedWO")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs within SLA (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("SLAPercentageMet")%></td>
                   <td>&nbsp;</td>                   
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total 1st time success (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("FirstTimeSuccess")%></td>
                   <td>&nbsp;</td>                 
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Cancelled jobs</td>
                   <td>:</td>
                   <td><%#Container.DataItem("CancellationNumber")%></td>
                   <td>&nbsp;</td>                  
                  </tr>
                   <tr>
                      <td colspan="4">&nbsp;</td>
                    </tr>
                </table>                  
             </ItemTemplate>
            </asp:datalist>  
              </div>        
              <div class="divClearBoth"></div>   
          </div>  
          
            
        </div>
                <p>&nbsp;</p></td>
                <td width="45">&nbsp;</td>
              </tr>
			
               <tr>
              <td width="45" height="60" align="center">&nbsp;</td>
              <td width="650" height="60" align="center" class="bdr3 style3"><strong>Registered Office: </strong>Montpelier Chambers, 61-63 High Street South, Dunstable, Bedfordshire LU6 3SF<br /></td>
              <td width="45" height="60" align="center">&nbsp;</td>
            </tr>
          </table>
			</form>
		</asp:Panel>
</body>
</html>
