﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/MyOrderWork.Master" CodeBehind="Alerts.aspx.vb" Inherits="OrderWorkUK.Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">

<script type="text/javascript">
    $(document).ready(function () {
        var lblBillingLocationContent = $('#ctl00_ContentHolder_lblBillingLocation').html()
        var lblCompanyDetContent = $('#ctl00_ContentHolder_lblCompanyDet').html()
        var lblAccntDetailContent = $('#ctl00_ContentHolder_lblAccountDetails').html()


        if (lblAccntDetailContent == "")
           {
           document.getElementById("divAccountDetailsAlert").style.display = "none";
           }
      else { 
           document.getElementById("divAccountDetailsAlert").style.display = "block";

           }
       if (lblCompanyDetContent == "") {
           document.getElementById("divComapnyDetAlert").style.display = "none";
       }
       else {
           document.getElementById("divComapnyDetAlert").style.display = "block";

       }

       if (lblBillingLocationContent == "") {
           document.getElementById("divBillingLocationAlert").style.display = "none";
       }
       else {
           document.getElementById("divBillingLocationAlert").style.display = "block";

       }


  });



   
   </script>
<div class="floatLeft divContainerStyle"> 
<div  style="width:992px;" class="clsGreyBandStyleFund"><strong>Account Alerts</strong></div>
<div class="floatLeft paddingBottom10">
                            
                            <div id="divAccountDetailsAlert" class="divAddMoreSkills roundifyIETop floatleft marginLeft10 marginTop10 paddingBottom10" style="background-color:#fefaf9;border:1px solid #f6cccd;line-height:18px;width:972px;">
                             <asp:label id="lblAccountDetails" runat="server" CssClass="floatLeft"></asp:label>
                             <div class="clsButtonGreyDetailsAlert roundifyAddressForIE floatRight marginRight10 marginTop10" id="divBtm" style="width:210px;"><a class="txtListing"> Click here to assign</a></div>
                             </div>
                             <div id="divComapnyDetAlert" class="divAddMoreSkills roundifyIETop floatleft marginLeft10 marginTop10 paddingBottom10" style="background-color:#fefaf9;border:1px solid #f6cccd;line-height:18px;width:972px;">
                                <asp:label id="lblCompanyDet" runat="server" CssClass="floatLeft"></asp:label>
                             <div class="clsButtonGreyDetailsAlert roundifyAddressForIE floatRight marginRight10" style="width:210px;" id="div1"><a href="CompanyProfile.aspx" class="txtListing"> Click here to go to Company Profile</a></div>
                            </div>

                            <div id="divBillingLocationAlert" class="divAddMoreSkills roundifyIETop floatleft marginLeft10 marginTop10 paddingBottom10" style="background-color:#fefaf9;border:1px solid #f6cccd;line-height:18px;width:972px;">
                                <asp:label id="lblBillingLocation" runat="server" CssClass="floatLeft"></asp:label>  
                                <div class="clsButtonGreyDetailsAccept roundifyAddressForIE floatRight marginRight10 marginTop10" id="div2" style="width:160px;margin-left:0px;"><a onclick="populateConfirmationSummary();" class="txtListing" id="A2"> Click here to assign</a></div>
                            </div>
                            
                  </div> 

                
  </div>
                 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
