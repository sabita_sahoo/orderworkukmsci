﻿<%@ Page Title="OrderWork: Welcome"  Language="vb" AutoEventWireup="false" MasterPageFile ="~/MasterPage/OrderWork.Master" CodeBehind="Dashboard.aspx.vb" Inherits="OrderWorkUK.Dashboard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">

<link href="../Styles/Welcome.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="/OWLibrary/JS/Scripts.js"  type="text/javascript"></script>

<script language="javascript" src="/OWLibrary/JS/jquery.js"  type="text/javascript"></script>

<script language="javascript" src="/OWLibrary/JS/DD_roundies.js"  type="text/javascript"></script>

 
<script type="text/javascript">

    function getTodayDate() {

        var currentTime = new Date();

        var month = currentTime.getMonth() + 1;

        var day = currentTime.getDate();

        var year = currentTime.getFullYear();

        alert(day + "/" + month + "/" + year);

        return (day + "/" + month + "/" + year);

    }

 

</script>

<script type="text/javascript">

    DD_roundies.addRule('.roundifyRefine', '8px 0px 0px 8px');

    DD_roundies.addRule('.roundifyRefine8', '8px 8px 8px 8px');

</script>

 

    

<asp:Literal runat="server" ID="hdnDate1"></asp:Literal>

 

<asp:Panel ID="pnlRetail" runat="server" Visible="false">

<%--
<div style="float:LEFT; margin:10px;"><asp:Image ID="imgCompLogoRetail" runat="server"  /></div>
<div style="font-family:Verdana;  font-size:22px; color:#c70000; font-weight:bold; float:left; margin-bottom:10px"><asp:Label runat="server" ID="lblcompanyretail" ></asp:Label></div>
--%>

<div id="divInnerLeftRetail">

      <div id="divInnerLeftTopBandRetail"></div>

      <div id="divInnerLeftMiddleBandRetail" >

        <div id="divSummaryContentRetail" class="clsSummaryContentRetail">

                  Book your new appointment by clicking the button below.<br /><br />

                  You can view your appointment by using the booking list link found on the left navigation panel.

                  <div id="divSupplyWorkTotal"  style="margin-left:190px;margin-top:20px;">

                      <div id="divBookInstallation" runat="server">

                          <div style="width:100%;"><a href="~/SecurePages/BookingForms/RetailWOForm.aspx" runat="server" id="lnkBookInst" class="textDecorationNone"><div class="txtFundsWithdrawl"><asp:Label ID="lblRetailRoleBookNewJob" runat="server" Text="Book Work Order" CssClass="fontSize16"></asp:Label></div></a><br /><br /><br /></div>                     

                        </div>

               </div>     

        </div>

        <div class="clsNoLabelTxt" style=""><asp:Label ID="lblNoProduct" runat="server" Text="No Product descriptions have been defined for your account. Please contact your account administrator" ForeColor="red" Visible="false"></asp:Label></div>

      </div>

      <div id="divInnerLeftBottomBandRetail"></div>

      <div id="divClearBoth"></div>

</div>

</asp:Panel>

 

 

 

<asp:Panel ID="pnlBuyer" runat="server" CssClass="floatLeft" Width="100%">

<div>

 


<%--
<div style="float:LEFT; margin-right:10px;"><asp:Image ID="imgCompLogo" runat="server"  /></div>
<div style="font-family:Verdana;  font-size:22px; color:#c70000; font-weight:bold; float:left; margin-top:20px;"><asp:Label runat="server" ID="lblcompname" ></asp:Label></div>
--%>
</div>

 

 

 

<div style="clear:both"></div>

 

 

 

 

 

<div id="divInnerLeftIT" class="clsRounded8 roundifyRefine8">

      

 

      <div style="font-family:Verdana; font-size:16px; font-weight:bold; color:#ee9e3f; margin-left:18px; margin-top:10px; height:30px;">Current Service Activities</div>

 

        

        <div id="divSummaryContent" class="floatLeft">

          <div id="divSummaryHeaderRow1" style="float:right;padding-right:10px;" >

              

              <div class="CenterPlain1">Total Work Orders</div>

           

          </div>

          <%@ Register TagPrefix="uc1" TagName="UCWOPlacedSummary" Src="~/UserControls/UK/UCWOPlacedSummaryITUK.ascx" %>

             <uc1:UCWOPlacedSummary id="UCWOPlacedSummary1" runat="server"></uc1:UCWOPlacedSummary>    

        </div>

        <div id="divBookNewJobBuyer" class="clsSummaryContentIT floatLeft clsBookNewJobIT" runat="server"><a href="~/SecurePages/BookingForms/WOForm.aspx" runat="server" id="lnkBookNewJob" class="textDecorationNone"><div class="txtFundsWithdrawl" style="cursor:pointer; margin-left:0px; padding-left:0px;width:244px"><asp:Label ID="lblBookNewJob" runat="server" Text="Book Work Order" CssClass="fontSize16"></asp:Label> </div></a><br /><br /><br /></div>                        

        <div style="float:right; margin-right:10px; ">

 <a runat="server" target="_blank" id="btnExport" href="exportToExcel.aspx?page=WelcomeHome">

        <img src="../OWLibrary/Images/Export_btn.gif" title="Export to Excel" alt="Export to Excel" border="0" />

        </a>

        

        </div>

        

        <div class="clearBoth"></div>

      

 

      

      <div class="clearBoth"></div>

</div>

 

 

 

 

</asp:Panel>

<asp:Panel ID="pnlSupplier" runat="server">
<%--
<div style=" height:70px; width:475px;">
<div style="float:LEFT; margin:10px;"><asp:Image ID="imgSPLogo" runat="server"  /></div>
<div style="font-family:Verdana;  font-size:22px; color:#c70000; font-weight:bold; float:left; margin-top:20px;"><asp:Label runat="server" ID="lblSPCompany" ></asp:Label></div>

</div>
--%>
<div style="clear:both"></div>
<div id="divInnerLeft">

        

          <cc1:TabContainer id="tabContainer"  runat="server"   cssclass="CustomTabStyle">

        <cc1:TabPanel ID="tabSupplyWork" runat="server" HeaderText="Supply Work">

        <ContentTemplate>

            <div id="divSupply">

                  <div class="Left">&nbsp;</div>

                  <div class="CenterPlain">Total Work Orders</div>

                  <div Class="Right">Total Value</div>

              </div>

            <div ID="divSupplyWork" >

                   <%@ Register TagPrefix="uc1" TagName="UCWOAcceptedSummary" Src="~/UserControls/UK/UCWOAcceptedSummaryUK.ascx"%>

                 <uc1:UCWOAcceptedSummary id="UCWOAcceptedSummary1" runat="server"></uc1:UCWOAcceptedSummary>

              </div>

              <div id="divimgSupplier" class="ApprovedImgOuter" style="float:right; width:98px; height:87px;" >

            <asp:Image ID="imgSide" runat="server" ImageUrl="~/OWLibrary/Images/ImgSecurePages/SideImage.jpg" CssClass="logoUnApproved floatRight" AlternateText="My Account"/>

            <asp:HyperLink ID="hlinkImg" visible="false" runat="server" AlternateText="Click to know 'How to get approved?'" CssClass="floatRight logoUnApproved"/>

            <div class="clearBoth"></div>

        </div>

         </ContentTemplate>

        </cc1:TabPanel>

        <cc1:TabPanel ID="tabBuyWork" runat="server" HeaderText="Buy Work">

        <ContentTemplate>

            <div class="clearBoth">

                  <div class="Left">&nbsp;</div>

                  <div class="CenterPlain">Total Work Orders</div>

                  <div Class="Right">Total Value</div>

              </div>       

                 <div ID="divBuyWork">

                   <%@ Register TagPrefix="uc1" TagName="UCWOPlacedSummarySupplier" Src="~/UserControls/UK/UCWOPlacedSummaryUK.ascx" %>

                 <uc1:UCWOPlacedSummarySupplier id="UCWOPlacedSummarySupplier1" runat="server"></uc1:UCWOPlacedSummarySupplier>  

             </div>

              <div id="divBookNewJob" class="clsSummaryContentRetail floatLeft clsBookNewJob" runat="server"><a id="A1" href="~/SecurePages/BookingForms/WOForm.aspx?sender=Welcome" runat="server" class="textDecorationNone"><div class="txtFundsWithdrawl" style="cursor:pointer;margin-left:0px; padding-left:0px; width:244px"><asp:Label ID="lblSuppAccBookNewJob" runat="server" Text="Book Work Order" CssClass="fontSize16"></asp:Label> </div></a><br /><br /><br /></div>                     

             <div class="clearBoth"></div> 

        </ContentTemplate>

        </cc1:TabPanel>

         </cc1:TabContainer>

         

         <div id="divSPMessagesOuter" runat="server" class="MessagesImgOuter" style="float:left;" >

         <div id="divSPMessages" runat="server" style="float:left;clear:right; width:343px;" class="marginMessagesNdApprvdLogo">

             <div id="divMsgsDashboardHeader" runat="server">

             <div id="divMsgsHeaderLeft" runat="server">&nbsp;</div>

             <div id="divMsgsHeaderMiddle" runat="server" class="fontArial14Bold">Recent Messages <img src="../OWLibrary/Images/Mail_SmallIcon.gif" class="imgMailIcon" /></div>

             <div id="divMsgsHeaderRight" runat="server">&nbsp;</div>

             </div>

  <div id="divMsgsDL" runat="server">   

  <asp:datalist id="dlSPMessages" runat="server" Width="435px">

  <HeaderTemplate>

  <div style="background-color:#ffffff; height:20px; margin-top:5px;" >

    <div class="clsListingColumn fontArial14Bold" style="padding-left:15px; width:75px;">Date</div>

     <div class="clsListingColumn Width90 fontArial14Bold" >Subject</div>

     </div>

  </HeaderTemplate>

    <ItemTemplate>

    <a class="clsRow" href="<%# "ListMessages.aspx?MsgID=" & OrderWorkLibrary.Encryption.Encrypt(Container.DataItem("MsgID"))%>" >

     

     <div class="clsListingColumn RowMsgsDashboardBlue" style="float:left; clear:right; padding-left:15px; width:75px;"><%#Container.DataItem("DateModified")%></div>

     <div class="clsListingColumn RowMsgsDashboard" style="width:343px;float:left;"><%#Container.DataItem("Subject")%></div>

     

    </a>

   </ItemTemplate>

 </asp:datalist>

 <a href='ListMessages.aspx' id="lnkViewMoreMessages" runat="server" class="colorBlueListing" style="font-weight:normal; float:right; margin-right:10px; margin-top:10px;">View more messages</a>

 <div class="clearBoth"></div>

 

 </div>

 <div class="clearBoth"></div>

 <div id="divMsgsDashboardBtm">&nbsp;</div>

</div>

         

         

         </div>

         

        <div class="clearBoth"></div>

         <div id="divSupplierBottomBand"></div>

</div>

</asp:Panel>

<div id="divSummaryRight"  class="divSummaryRight" runat="server" >

<div id="divInnerRight">

      <div id="divInnerRightTopBand"></div>

      <div id="divInnerRightMiddleBand" >

          <%@ Register TagPrefix="uc1" TagName="UCAccountSumm" Src="~/UserControls/UK/UCAccountSummaryUK.ascx" %>

             <uc1:UCAccountSumm id="UCAccountSumm1" runat="server"></uc1:UCAccountSumm>

      </div>

      <div class="clearBoth"></div>

      <div id="divInnerRightBottomBand"></div>  

</div>

 

</div>

<div class="clearBoth"></div>

<asp:Panel ID="pnlSupplierWorkListing" runat="server" CssClass="floatLeft">

 <div  class="JobsListingOuter">

<div id="divAccountAlertTopBand"></div>

<div  class="colorBlack clsMiddleBandBox WOListingMdl">

 <div runat="server" id="divTodayWOListing" class="paddingBottom20">

    <b class="marginLeft20"><i>Today's Job:&nbsp;&nbsp;<asp:Label runat="server" ID="lbTodayDate"></asp:Label></i></b>

    <div class="marginTop10">

     <div class="clsListingColumn Width90 marginLeft20"><b>WO No</b></div>

     <div class="clsListingColumn Width90"><b>PostCode</b></div>

     <div class="clsListingColumn Width90"><b>Time</b></div>

     <div class="clsListingColumn Width90"><b>Status</b></div>

     <div class="clsListingColumn Width180"><b>Client</b></div>

     <div class="clsListingColumn Width130"><b>Customer Name</b></div>

     </div>

     <div class="clearBoth"></div>

<asp:datalist id="rpTodayWOListing1" runat="server"  AlternatingItemStyle-CssClass="SummaryAlternateRowLI">

    <ItemTemplate>

                 <a class="clsRow" href="<%# "SupplierWODetails.aspx?WOID=" & OrderWorkLibrary.Encryption.Encrypt(Container.DataItem("WOID")) & "&WorkOrderID=" & OrderWorkLibrary.Encryption.Encrypt(Container.DataItem("WorkOrderId")) & "&CompID=" & OrderWorkLibrary.Encryption.Encrypt(Container.DataItem("CompanyID")) & "&FromDate=&ToDate=&PS=10&PN=0&SC=DateCreated&SO=1&sender=UCWOsListing&Group=" & IIf(Container.DataItem("Status") = "Active", "Accepted", Container.DataItem("Status"))%>" >

                 <div class="clsListingColumn clsRowBlue Width90 marginLeft20"><%#Container.DataItem("WorkOrderId")%></div>

                 <div class="clsListingColumn Width90"><%#Container.DataItem("PostCode")%></div>

                 <div class="clsListingColumn Width90"><%#IIf(Container.DataItem("AptTime") = "", "NA", Container.DataItem("AptTime"))%></div>

                 <div class="clsListingColumn Width90"><%#Container.DataItem("Status")%></div>

                  <div class="clsListingColumn Width180"><b><%#Container.DataItem("ClientName")%></b></div>

                 <div class="clsListingColumn Width130"><%#Container.DataItem("CustomerName")%></div>

             </a>

    </ItemTemplate>

 </asp:datalist>

 </div>

 <div class="clearBoth"></div>

 <div runat="server" id="divTomorrowWOListing" style="margin:0px 0px 0px 0px;">

    <b class="marginLeft20"><i>Tomorrow's Job:&nbsp;&nbsp;<asp:Label runat="server" ID="lbTommorowDate"></asp:Label></i></b> 

    <div class="marginTop10">

     <div class="clsListingColumn Width90 marginLeft20"><b>WO No</b></div>

     <div class="clsListingColumn Width90"><b>PostCode</b></div>

     <div class="clsListingColumn Width90"><b>Time</b></div>

     <div class="clsListingColumn Width90"><b>Status</b></div>

     <div class="clsListingColumn Width180"><b>Client</b></div>

     <div class="clsListingColumn Width130"><b>Customer Name</b></div>

     </div>

     <div class="clearBoth"></div>

  <asp:datalist id="rpTomorowWOListing1" runat="server"  AlternatingItemStyle-CssClass="SummaryAlternateRowLI">

    <ItemTemplate>

    <a class="clsRow" href="<%# "SupplierWODetails.aspx?WOID=" & OrderWorkLibrary.Encryption.Encrypt(Container.DataItem("WOID")) & "&WorkOrderID=" & OrderWorkLibrary.Encryption.Encrypt(Container.DataItem("WorkOrderId")) & "&CompID=" & OrderWorkLibrary.Encryption.Encrypt(Container.DataItem("CompanyID")) & "&FromDate=&ToDate=&PS=10&PN=0&SC=DateCreated&SO=1&sender=UCWOsListing&Group=" & IIf(Container.DataItem("Status") = "Active", "Accepted", Container.DataItem("Status"))%>" >

     <div class="clsListingColumn clsRowBlue Width90 marginLeft20"><%#Container.DataItem("WorkOrderId")%></div>

     <div class="clsListingColumn Width90"><%#Container.DataItem("PostCode")%></div>

     <div class="clsListingColumn Width90"><%#IIf(Container.DataItem("AptTime") = "", "NA", Container.DataItem("AptTime"))%></div>

     <div class="clsListingColumn Width90"><%#Container.DataItem("Status")%></div>

     <div class="clsListingColumn Width180"><b><%#Container.DataItem("ClientName")%></b></div>

     <div class="clsListingColumn Width130"><%#Container.DataItem("CustomerName")%></div>

    </a>

     <div class="clearBoth"></div>

   </ItemTemplate>

 </asp:datalist>

 </div>

</div>

<div id="divAccountAlertBottomBand"></div> 

 

  </div>

</asp:Panel>

<asp:Panel ID="pnlBuyerWorkListing" runat="server" Visible="false" CssClass="floatLeft">

<div  class="JobsListingOuter marginBottom20" id="DivBuyerWorkSumOuter" runat="server">

<div  class="colorBlack clsBuyerMiddleBandBox BuyerSumMdl clsRound roundifyRefine">

<div style="overflow-x:hidden;overflow-y:scroll;height:120px;">

 <div runat="server" id="divBuyerTodayWOListing" class="paddingBottom20">

    <div  class="marginTop10" style="padding:0px 0px 0px 20px;"><b><i>Today's Jobs:&nbsp;&nbsp;<asp:Label runat="server" ID="lblBuyerTodayDate"></asp:Label></i></b></div>    

     <asp:datalist id="rpBuyerTodayWOListing" runat="server" CssClass="marginTop10"  AlternatingItemStyle-CssClass="SummaryAlternateRowLI">

     <HeaderTemplate>    

       <table cellpadding="0" cellspacing="0">

        <tr class="clsListingColumn">

         <td width="20">&nbsp;</td>

        <td width="130"><b>Customer Name</b></td>

        <td width="10">&nbsp;</td>

        <td width="90"><b>Postcode</b></td>

        <td width="10">&nbsp;</td>

        <td width="90"><b>Appt. Time</b></td>      

        <td width="10">&nbsp;</td>

        <td width="90"><b>WO No.</b></td>      

        <td width="10">&nbsp;</td>

        <td width="130"><b>Your Reference</b> </td>        

        <td width="10">&nbsp;</td>

        </tr>

    </table>    

     </HeaderTemplate>

    <ItemTemplate>

     <table cellpadding="0" cellspacing="0">

        <tr>

         <a class="clsRow" href="<%# "BuyerWODetails.aspx?WOID=" & OrderWorkLibrary.Encryption.Encrypt(Container.DataItem("WOID")) & "&WorkOrderID=" & OrderWorkLibrary.Encryption.Encrypt(Container.DataItem("WorkOrderId")) & "&CompID=" & OrderWorkLibrary.Encryption.Encrypt(Container.DataItem("CompanyID")) & "&FromDate=&ToDate=&PS=10&PN=0&SC=DateCreated&SO=1&sender=UCWOsListing&Group=" & IIf(Container.DataItem("Status") = "Active", "Accepted", Container.DataItem("Status"))%>" >

         <td width="20">&nbsp;</td>

        <td width="130" class="clsRowBlue"><%#Container.DataItem("CustomerName")%></td>

        <td width="10">&nbsp;</td>

        <td width="90" class="clsListingColumn"><%#Container.DataItem("PostCode")%></td>

        <td width="10">&nbsp;</td>

        <td width="90" class="clsListingColumn"><%#Container.DataItem("AptTime")%></td>      

        <td width="10">&nbsp;</td>

        <td width="90" class="clsListingColumn"><%#Container.DataItem("WorkOrderId")%></td>      

        <td width="10">&nbsp;</td>

        <td width="130" class="clsListingColumn"><%#Container.DataItem("PONumber")%></td>       

        <td width="10">&nbsp;</td>

        </a> 

        </tr>

     </table>

      

    </ItemTemplate>

 </asp:datalist>

 </div>

 </div>

 </div>

<div class="clearBoth"></div>

</div>

</asp:Panel>

</asp:Content>

 <asp:Content  runat="server" ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2">

 

</asp:Content>

 <asp:Content  runat="server" ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1">

 <asp:Panel ID="pnlAccAlerts" runat="server" CssClass="clsAccountAlertBox">

            <div id="divAccountAlert" runat="server">

                <div id="divSideLinkMenuTopBand"></div>

                <div id="220Content" class="AlertMdlContent">

                      <img src="../OWLibrary/Images/Icons/Alert_Icon1_small.gif" alt="Account Alert" align="middle"  />  <asp:Label ID="lblHeading" runat="server" CssClass="txtFont15"  Text="Account Alerts"></asp:Label>

                        <div style="clear:both;"></div>

                         <br />

                                <asp:label id="lblAccountDetails" runat="server"></asp:label>

                                <asp:label id="lblCompanyDet" runat="server"></asp:label>

                                <asp:label id="lblBillingLocation" runat="server"></asp:label>  

                 </div> 

                <div id="divSideLinkMenuBottom"></div>             

       </div>

  </asp:Panel>

  </asp:Content>

 




