﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewServiceQuery.aspx.vb"
    Title="OrderWork: New Service Query" Inherits="OrderWorkUK.NewServiceQuery" MasterPageFile="~/MasterPage/MyOrderWork.Master"
    ValidateRequest="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">
    <style type="text/css">
        .DialogueBackground
        {
            background-color: Gray;
            filter: alpha(opacity=50);
            opacity: 0.50;
            position: fixed;
            left: 0;
            top: 0;
            width: 0%;
            height: 100%;
        }
    </style>
    <div id="divTopBand" class="divContainerStyle floatLeft">
        <div class="clsGreyBandStyleFund" style="width: 992px;" id="divPaymentMethod">
            <strong>New Service Query</strong></div>
        <div id="divBGLayer" class="DialogueBackground" style="display: none; width: 100%">
            <div style="vertical-align: middle; left: 42%; position: absolute; top: 50%;" align="center">
                <img src="../../OWLibrary/Images/indicator.gif" />
                <asp:Label ForeColor="floralwhite" Font-Size="Medium" Font-Bold="true" ID="lblMessage"
                    runat="server" Text="Please Wait, Processing..."></asp:Label>
            </div>
        </div>
        <div id="divMiddleBand" style="width: 972px; padding: 5px;">
            <div id="divValidationMain" class="divValidation floatLeft" runat="server" visible="false">
                <div class="roundtopVal">
                    <img src="~/OWLibrary/Images/Curves/Validation-TLC.gif" runat="server" id="imgValidation"
                        alt="" width="5" height="5" class="corner" style="display: none" /></div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="15" align="center">
                            &nbsp;
                        </td>
                        <td height="15">
                            &nbsp;
                        </td>
                        <td height="15">
                            &nbsp;
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td width="63" align="center" valign="top">
                            <img src="~/Images/Icons/Validation-Alert.gif" runat="server" id="imgAlert">
                        </td>
                        <td class="validationText">
                            <div id="divValidationMsg">
                            </div>
                            <asp:Label ID="lblError" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                            <a runat="server" id="lnkError" class="footerTxtSelected" visible="false" href="mailto:info@orderwork.co.uk">
                                info@orderwork.co.uk</a>
                            <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg"
                                DisplayMode="BulletList" ValidationGroup="PersonalInfo" EnableClientScript="false">
                            </asp:ValidationSummary>
                        </td>
                        <td width="20">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td height="15" align="center">
                            &nbsp;
                        </td>
                        <td height="15">
                            &nbsp;
                        </td>
                        <td height="15">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div class="roundbottomVal">
                    <img src="../../OrderWorkLibrary/Images/Curves/Validation-BLC.gif" alt="" width="5"
                        height="5" class="corner" style="display: none" /></div>
            </div>
            <div class="divWorkOrder" style="margin-left: 10px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="215" valign="middle" class="formTxt">
                            Type of Request
                        </td>
                        <td class="formTxt" valign="bottom" align="left" width="215" height="24">
                            First Name<span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="rqFName" runat="server" ErrorMessage="Please enter First Name"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtFName">*</asp:RequiredFieldValidator>
                        </td>
                        <td width="215" height="24" align="left" valign="bottom" class="formTxt">
                            Last Name<span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="rqLName" runat="server" ErrorMessage="Please enter Last Name"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtLName">*</asp:RequiredFieldValidator>
                        </td>
                        <td width="215" height="24" align="left" valign="bottom" class="formTxt">
                            Position<span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValPosition" runat="server" ErrorMessage="Please enter Position"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtPosition">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" class="formTxt">
                            <asp:DropDownList ID="ddlReqType" runat="server" CssClass="clsFormFeildStyle width190">
                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                <asp:ListItem Text="Order" Value="Order"></asp:ListItem>
                                <asp:ListItem Text="Enquiry" Value="Enquiry"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFName" runat="server" CssClass="clsFormFeildStyle width190"></asp:TextBox>
                        </td>
                        <td width="168">
                            <asp:TextBox ID="txtLName" runat="server" CssClass="clsFormFeildStyle width190"></asp:TextBox>
                        </td>
                        <td width="160">
                            <asp:TextBox ID="txtPosition" runat="server" CssClass="clsFormFeildStyle width190"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" class="formTxt">
                            Contact Number<span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValContactNumber" runat="server" ErrorMessage="Please enter Contact Number"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtConNumber">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegExpConNumber" runat="server" ErrorMessage="Contact Number Only Numbers allowed"
                                ValidationGroup="PersonalInfo" ForeColor="#EDEDEB" ControlToValidate="txtConNumber"
                                ValidationExpression="\d+">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left" valign="bottom" class="formTxt">
                            Email<span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValEmail" runat="server" ErrorMessage="Please enter Email"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtEmail">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regularEmail" runat="server" ErrorMessage="Please enter Email Address in correct format"
                                ValidationGroup="PersonalInfo" ForeColor="#EDEDEB" ControlToValidate="txtEmail"
                                ValidationExpression="^.+@[^\.].*\.[a-z]{2,}$">*</asp:RegularExpressionValidator>
                            <asp:CompareValidator ID="CompareValEmail" runat="server" ErrorMessage="Email and Confirm Email are not same"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtEmail"
                                ControlToCompare="txtConfirmEmail">*</asp:CompareValidator>
                        </td>
                        <td align="left" valign="bottom" class="formTxt">
                            Confirm Email<span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValConfEmail" runat="server" ErrorMessage="Please enter Confirm Email"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtConfirmEmail">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regularOtherEmail" runat="server" ErrorMessage="Please enter Confirm Email Address in correct format"
                                ValidationGroup="PersonalInfo" ForeColor="#EDEDEB" ControlToValidate="txtConfirmEmail"
                                ValidationExpression="^.+@[^\.].*\.[a-z]{2,}$">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left" valign="bottom" class="formTxt">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtConNumber" runat="server" CssClass="clsFormFeildStyle width190"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="clsFormFeildStyle width190"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtConfirmEmail" runat="server" CssClass="clsFormFeildStyle width190"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div style="height: 0px; font-size: 0px; line-height: 0px; border-bottom: solid 2px #E1E2DD;
                    margin-top: 20px; margin-bottom: 20px;">
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="215" valign="middle" class="formTxt">
                            End User Name<span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValEndUserName" runat="server" ErrorMessage="Please enter End User Name"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtEndUserName">*</asp:RequiredFieldValidator>
                        </td>
                        <td class="formTxt" valign="bottom" align="left" width="215" height="24">
                            End User Contact Number<span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValEndUserPhone" runat="server" ErrorMessage="Please enter End User Contact Number"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtEndUserPhone">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegExpEndUserPhone" runat="server" ErrorMessage="End User Contact Number Only Numbers allowed"
                                ValidationGroup="PersonalInfo" ForeColor="#EDEDEB" ControlToValidate="txtEndUserPhone"
                                ValidationExpression="\d+">*</asp:RegularExpressionValidator>
                        </td>
                        <td width="215" height="24" align="left" valign="bottom" class="formTxt">
                            End User Email<span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValEndUserEmail" runat="server" ErrorMessage="Please enter End User Email"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtEndUserEmail">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegExpEndUserEmail" runat="server" ErrorMessage="Please enter End User Email Address in correct format"
                                ValidationGroup="PersonalInfo" ForeColor="#EDEDEB" ControlToValidate="txtEndUserEmail"
                                ValidationExpression="^.+@[^\.].*\.[a-z]{2,}$">*</asp:RegularExpressionValidator>
                        </td>
                        <td width="215" height="24" align="left" valign="bottom" class="formTxt">
                            DBS Required <span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValDBSRequired" runat="server" ErrorMessage="Please select DBS Required"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="ddDBSReq">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" class="formTxt">
                            <asp:TextBox ID="txtEndUserName" runat="server" CssClass="clsFormFeildStyle width190"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEndUserPhone" runat="server" CssClass="clsFormFeildStyle width190"></asp:TextBox>
                        </td>
                        <td width="168">
                            <asp:TextBox ID="txtEndUserEmail" runat="server" CssClass="clsFormFeildStyle width190"></asp:TextBox>
                        </td>
                        <td width="160">
                            <asp:DropDownList ID="ddDBSReq" runat="server" CssClass="clsFormFeildStyle width190">
                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                <asp:ListItem Text="Not Sure" Value="Not Sure"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" class="formTxt" colspan="2">
                            End User Location(s)<span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValEndUserLocation" runat="server" ErrorMessage="Please enter End User Location"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtEndUserLocation">*</asp:RequiredFieldValidator>
                        </td>
                        <td valign="middle" class="formTxt" colspan="2">
                            Project Overview<span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValProjOverview" runat="server" ErrorMessage="Please enter Project Overview"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtProjOverview">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtEndUserLocation" runat="server" Height="100" Width="405" TextMode="MultiLine"
                                CssClass="clsFormFeildStyle"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtProjOverview" runat="server" Height="100" Width="405" TextMode="MultiLine"
                                CssClass="clsFormFeildStyle"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" class="formTxt" colspan="5">
                            Please enter as much information as you can regarding what the job involves, the
                            equipment we are installing, location, and restrictions <span class="bodytxtValidationMsg">
                                *</span>
                            <asp:RequiredFieldValidator ID="ReqValLongInfo" runat="server" ErrorMessage="Please enter information regarding what the job involves"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtLongInfo">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <asp:TextBox ID="txtLongInfo" runat="server" Height="100" Width="835" TextMode="MultiLine"
                                CssClass="clsFormFeildStyle"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <div style="height: 0px; font-size: 0px; line-height: 0px; border-bottom: solid 2px #E1E2DD;
                    margin-top: 20px; margin-bottom: 20px;">
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" class="formTxt" colspan="2">
                            Is the location of installation in a single office/multi room same location or multiple
                            locations? <span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValSingleOrMultiOffice" runat="server" ErrorMessage="Please enter location of installation"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtSingleOrMultiOffice">*</asp:RequiredFieldValidator>
                        </td>
                        <td valign="middle" class="formTxt" colspan="2">
                            Distance from product(s) location to Power/Mains and Ethernet Sockets (meters) <span
                                class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValDistFromProd" runat="server" ErrorMessage="Please enter Distance From Product"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtDistFromProd">*</asp:RequiredFieldValidator>
                        </td>
                        <td width="105">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtSingleOrMultiOffice" runat="server" Height="100" Width="405"
                                TextMode="MultiLine" CssClass="clsFormFeildStyle"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtDistFromProd" runat="server" Height="100" Width="405" TextMode="MultiLine"
                                CssClass="clsFormFeildStyle"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" class="formTxt" colspan="2">
                            Type/Length of cables being supplied and Quantity <span class="bodytxtValidationMsg">
                                *</span>
                            <asp:RequiredFieldValidator ID="ReqValTypeOfCable" runat="server" ErrorMessage="Please enter Type Of Cable Info"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtTypeOfCable">*</asp:RequiredFieldValidator>
                        </td>
                        <td valign="middle" class="formTxt" colspan="2">
                            Is there Data Transfer needed and if so how much and from where <span class="bodytxtValidationMsg">
                                *</span>
                            <asp:RequiredFieldValidator ID="ReqValIsDataTransferNeeded" runat="server" ErrorMessage="Please enter Is Data Transfer Needed"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtDataTransferNeeded">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtTypeOfCable" runat="server" Height="100" Width="405" TextMode="MultiLine"
                                CssClass="clsFormFeildStyle"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtDataTransferNeeded" runat="server" Height="100" Width="405" TextMode="MultiLine"
                                CssClass="clsFormFeildStyle"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" class="formTxt" colspan="2">
                            Name of all software that needs installing <span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValNameOfSoftware" runat="server" ErrorMessage="Please enter Name Of Softwares needs"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtNameOfSoftware">*</asp:RequiredFieldValidator>
                        </td>
                        <td valign="middle" class="formTxt" colspan="2">
                            Approximate Internet speed on site <span class="bodytxtValidationMsg">*</span>
                            <asp:RequiredFieldValidator ID="ReqValNetSpeed" runat="server" ErrorMessage="Please enter Internet speed"
                                ForeColor="#EDEDEB" ValidationGroup="PersonalInfo" ControlToValidate="txtNetSpeed">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtNameOfSoftware" runat="server" Height="100" Width="405" TextMode="MultiLine"
                                CssClass="clsFormFeildStyle"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtNetSpeed" runat="server" Height="100" Width="405" TextMode="MultiLine"
                                CssClass="clsFormFeildStyle"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="clearBoth">
                            </div>
                            <div class="divRightTop marginBottom10 floatleft" style="width: 840px;">
                                <div class="clsGreyBandStyle floatleft" style="width: 830px;">
                                    <b>Documents</b>
                                </div>
                                <%@ register tagprefix="uc2" tagname="UCFileUpload" src="~/OWLibrary/UserControls/UK/UCFileUpload_Outer_Btm.ascx" %>
                                <uc2:UCFileUpload id="UCFileUpload6" runat="server" Control="ctl00_ContentHolder_UCFileUpload6"
                                    ExistAttachmentSource="CompanyProfile" Type="Company" AttachmentForSource="CompanyProfile"
                                    UploadCount="1" ShowExistAttach="False" ShowNewAttach="True" ShowUpload="True"
                                    MaxFiles="10">
                                </uc2:UCFileUpload>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:LinkButton ID="btnSend" runat="server" CssClass="clsButtonGreyViewAll roundifyAddressForIE marginRight10 marginTop10 floatRight marginBottom10 textDecorationNone roundifyAddressForIE"
                                TabIndex="38" CausesValidation="false" ValidationGroup="PersonalInfo" OnClientClick="foo()"> Send &nbsp;</asp:LinkButton>
                        </td>
                        <td colspan="2" style="float: left">
                            <a class="clsButtonGreyAddSpec marginTop10 floatRight marginBottom10 textDecorationNone roundifyAddressForIE"
                                id="ancCancelVetting" runat="server">Cancel</a>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function foo() {
            document.getElementById('divBGLayer').style.display = "block";
        }
    </script>
</asp:Content>
