<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierWOListing.aspx.vb" Title="OrderWork: Supplier Work Order Listing" Inherits="OrderWorkUK.SupplierWOListing" MasterPageFile="~/MasterPage/MyOrderWork.Master"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">
<script language="javascript" type="text/javascript" src="OWLibrary/JS/json2.js"></script>
<script language="javascript" type="text/javascript" src="OWLibrary/JS/jquery.js"></script>
<script language="JavaScript" src="OWLibrary/JS/JQuery.jsonSuggestCustom.js" type="text/javascript"></script>
<style type="text/css">
.gridHdr
{
	background-color:#993366;
	font-family:Verdana;
	font-size: 11px;
	font-weight: bold;
	color: #555354;
	text-decoration: none;
	height:28px;
	vertical-align:middle;
	text-align:left;
}
.gridRow
{
	background-color:#F7F3F4;
	font-family:Verdana;
	font-size: 11px;	
	color: #555354;
	text-decoration: none;
	padding-left:6px;
	padding-right:6px;
	padding-top: 8px;
	margin-left:6px;
	margin-top:8px;	
	border:solid 1px #DFDFDF;
}
#AlertDiv{
left: 40%; top: 40%;
position: absolute; width: 200px;
padding: 12px; 
border: #000000 1px solid;
background-color: white; 
text-align: left;
visibility: hidden;
z-index: 99;
}
#AlertButtons{
position: absolute; right: 5%; bottom: 5%;
}
/*Styles for Modal PopUp = START*/
#divModalParent {
    margin: auto;
    text-align: left;
}
.modalBackground 
 {
    background-color:Silver;
    filter:alpha(opacity=30) !important;
    opacity:0.3 !important;
    position:absolute !important;
    z-index:1 !important;
 }
.pnlConfirm {	
	background-color:#FFEBDE;
	font-family:Verdana;
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	line-height: 18px;
	border: 1px solid #CECBCE;
	padding: 10px;
	position:absolute !important;
	margin-left:650;
	margin-top:350;
}
.cursorHand
{
	cursor: hand;
}
.bottonTopGrey {
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-TR.gif) no-repeat right top;
	height:4px;
}
.bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
}
*+html .bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
	margin-top:-3px;
}

#divButton {
background-color:#999;
width:56px;
height:19px;
margin:10px 0 0;
padding:0;
}
img.btnCorner {
   width: 4px;
   height: 4px;
   border: none;
   display: block !important;
}
.buttonText {
font-family:Verdana;
font-size:11px;
color:#FFF;
text-decoration:none;
padding-left:11px;
padding-right:8px;
font-style:normal;
font-weight:400;
font-variant:normal;
line-height:11px;
}

.buttonText:hover {
color:#3F3F3F;
text-decoration:none;
}
</Style>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div id="divComplete" runat="server">
    <div id="AlertDiv">
        <div id="AlertMessage">
        </div>
        <br />
        <div id="AlertButtons">
            <input id="OKButton" type="button" value="OK" runat="server" onclick="ClearErrorState()" />
        </div>
    </div>

	<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
		<div class="errContainer">
			<div><img src="../OWLibrary/Images/Error_TopBand.gif" width="480" height="10"></div>
			<div class="errMdlBox">
				<table border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
					<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
				  </tr>
				</table>			
			</div>
			<div><img src="../OWLibrary/Images/Error_BtmBand.gif" width="480" height="10"></div>				
		</div>
	</asp:Panel>
	<div id="divClearBoth"></div>
	<div id="divTopBand1" class="divContainerStyle floatLeft" >
	
	
	<%@ Register TagPrefix="uc2" TagName="UCSupplierTabs" Src="~/UserControls/UK/UCSupplierTabs.ascx" %>
                 <uc2:UCSupplierTabs id="UCSupplierTabs1" Location="List" runat="server"></uc2:UCSupplierTabs> 
                 
                 
           <%@ Register TagPrefix="uc1" TagName="UCSupplierWOsListing" Src="~/UserControls/UK/UCSupplierWOsListingUK.ascx" %>
                 <uc1:UCSupplierWOsListing id="UCSupplierWOsListing1" runat="server"></uc1:UCSupplierWOsListing>       
                 
	</div>
	<%--<div id="divBtmBand"  style="margin-top:-15px;"></div>--%>
	</div>
    <div id="div1" class="clearBoth"></div>
	</ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="updatePanel1" runat="server">
        <ProgressTemplate>
            <div class="gridText">
                <img  align=middle src="../OWLibrary/Images/indicator.gif" />
                <b>Fetching Data... Please Wait</b>
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="divComplete" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
           
	
	
	
</asp:Content>

