<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewCreditNote.aspx.vb" Inherits="OrderWorkUK.ViewCreditNote" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>OrderWork: Credit Note</title>
</head>
<body onload="javascript:this.print()">
    <form id="form1" runat="server">
   				 <%@ Register TagPrefix="uc1" TagName="UCViewCreditNote" Src="~/UserControls/UK/UCMSViewCreditNoteUK.ascx" %>
                 <uc1:UCViewCreditNote id="UCMSViewCreditNoteUK1" runat="server"></uc1:UCViewCreditNote> 
    </form>
</body>
</html>
