<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewCashReceiptsCreditNotes.aspx.vb" Title="OrderWork: View Cash Receipts Credit Notes" Inherits="OrderWorkUK.ViewCashReceiptsCreditNotes" MasterPageFile ="~/MasterPage/MyOrderWork.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
	<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
		<div class="errContainer">
			<div><img src="../OWLibrary/Images/Error_TopBand.gif" width="480" height="10"></div>
			<div class="errMdlBox">
				<table border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
					<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
				  </tr>
				</table>			
			</div>
			<div><img src="../OWLibrary/Images/Error_BtmBand.gif" width="480" height="10"></div>				
		</div>
	</asp:Panel>
	<div id="divClearBoth"></div>
	<div id="divTopBand" ></div>
	<div id="divMiddleBand">
	<%@ Register TagPrefix="uc1" TagName="UCMSPR_SalesCashPayment" Src="~/UserControls/UK/UCMSPR_SalesCashPayment.ascx" %>
	<uc1:UCMSPR_SalesCashPayment id="UCMSPR_SalesCashPayment1" runat="server"></uc1:UCMSPR_SalesCashPayment></div>
	<div id="divBtmBand"></div>
</asp:content>
