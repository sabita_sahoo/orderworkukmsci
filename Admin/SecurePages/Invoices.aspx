<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Invoices.aspx.vb" Title="OrderWork: Sales Invoices" Inherits="OrderWorkUK.Invoices" MasterPageFile ="~/MasterPage/MyOrderWork.Master" ValidateRequest="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
<asp:UpdatePanel ID="UpdatePnlMyInvoices" runat="server"  RenderMode=Inline>
  <ContentTemplate>	
	<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg floatLeft" Visible="false" runat="server">
		<div class="errContainer">
			<div><img src="../OWLibrary/Images/Error_TopBand.gif" width="480" height="10"></div>
			<div class="errMdlBox">
				<table border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
					<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
				  </tr>
				</table>			
			</div>
			<div><img src="../OWLibrary/Images/Error_BtmBand.gif" width="480" height="10"></div>				
		</div><br>
	</asp:Panel>
	<div id="divClearBoth"></div>
	</ContentTemplate>
</asp:UpdatePanel>
<div id="divClearBoth"></div>
<div class="floatLeft">
		<div><%@ Register TagPrefix="uc2" TagName="UCOWInvoices" Src="~/UserControls/UK/UCOWInvoices.ascx" %>
	<uc2:UCOWInvoices id="UCOWInvoices1" runat="server" AdviceType="Sales"></uc2:UCOWInvoices></div>
    </div>
    <div id="divClearBoth"></div>
</asp:content>
