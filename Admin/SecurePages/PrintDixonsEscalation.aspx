﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintDixonsEscalation.aspx.vb" Inherits="OrderWorkUK.PrintDixonsEscalation" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Order Work: Print Escalation</title>
    <link href="../Styles/OWPortal.css" rel="stylesheet" type="text/css" />
    <style>
    #divPrintWOLeft {
    float: left;
    width: 180px;
    }
    #divPrintWOContent {
    height: 100%;
    margin-bottom: 1px;
    margin-top: 1px;
    min-height: 250px;
    width: 738px;
}
#divPrintWOContentMiddleBand {
    background-color: #FFFFFF;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 12px;
    min-height: 250px;
    padding-top: 15px;
    width: 738px;
}
body {
    background-color: #CECECE;
}
    
    </style>
</head>
<body bgcolor="#F4F5F0" onload="javascript:this.print()">
    <form id="form1" runat="server">
    <div id="divPrintWOBody">
    	<div id="divPrintWOHeader">
			<div id="divPrintWOHeaderTopBand"></div>
			<div id="divPrintWOHeaderMiddleBand">
			<asp:Panel ID="pnlOWLogo" runat="server">
			<a href="Welcome.aspx"><img id="Img1" src="~/OWLibrary/Images/OrderWorks-Logo-Grey.gif" runat="server" align="left" class="imgLogo" alt="OrderWork - Service Reinvented" border="0"/></a>
			</asp:Panel>
			<asp:Panel ID="pnlClientLogo" runat="server">
			<asp:Image CssClass="clsClientLogo" runat="server" id="clientLogo" visible="false" ImageUrl="../../Images/ImgSecurePages/Logo.gif" />
			</asp:Panel>
			<div style="clear:both;"></div>
			</div>
			<div id="divPrintWOHeaderBtmBand"></div>
		</div>
		<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
		<div id="divPrintWOContent">
				<div id="divPrintWOContentTopBand"></div>
				<div id="divPrintWOContentMiddleBand">
					<table border="0" cellpadding="0" cellspacing="0" align="center">
					  <tr>
						<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
						<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
					  </tr>
					</table>
				</div>
				<div id="divPrintWOContentBtmBand"></div>
			</div>			
		</asp:Panel>		
		<asp:Panel runat="server" ID="pnlPrintContent" Visible="true">
			<div id="divPrintWOContent">
				<div id="divPrintWOContentTopBand"></div>
				<div id="divPrintWOContentMiddleBand">
					<div id="divPrintWOTopInfo">
						<div id="divPrintWORow"><div id="divPrintWOLeft"><b>Sales Agent Name</b></div><div id="divPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblSalesAgentName"></asp:Label></div></div>
						<div id="divPrintWORow"><div id="divPrintWOLeft"><b>Sales Agent Main Phone</b></div><div id="divPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblSalesAgentMainPhone"></asp:Label></div></div>
						<div id="divPrintWORow"><div id="divPrintWOLeft"><b>Branch ID</b></div><div id="divPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblBranchID"></asp:Label></div></div>												
						<div id="divPrintWORow"><div id="divPrintWOLeft"><b>Customer Name</b></div><div id="divPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblCustomerName"></asp:Label></div></div>						
						<div id="divPrintWORow"><div id="divPrintWOLeft"><b>Customer Post Code</b></div><div id="divPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblCustomerPostCode"></asp:Label></div></div>
						
						<div id="divClearBoth"></div>
					</div>					
					<div id="divClearBoth"></div>
					<div id="divPrintWOSpecialInstruction">												
						    <b>Details</b><br />
							<asp:Label ID="lblComplaint" runat="server"></asp:Label>						
						<div id="divClearBoth"></div>
					</div>								
				</div>
				<div id="divPrintWOContentBtmBand"></div>
			</div>
		</asp:Panel>
		<div id="divPrintWOFooter">
			<div id="divPrintWOFooterTopBand"></div>
			<div id="divPrintWOFooterMiddleBand">
				<table width="915"  height="36"  border="0" align="center" cellpadding="0" cellspacing="0" class="BdrTop"> 
				  <tr valign="top">
					<td width="145" align="left" class="footerTxt" style="text-align:left;float:left;" ><img src="../OWLibrary/Images/Phone-Icon.gif" title="Phone" alt="Phone" width="15" height="12" align="absmiddle"/> <asp:Label ID="lblPhoneNumber" runat="server" Text="0203 053 0343"></asp:Label> </td>
					<td width="606" align="left" class="footerTxt" runat="server" id="tdEmail" ><a href="mailto:info@orderwork.co.uk" class="footerTxtSelected"><img src="../OWLibrary/Images/Email-Icon.gif" title="Contact Us" width="16" height="15" border="0" align="absmiddle" class="marginR5" />info@orderwork.co.uk</a>&nbsp;</td>
					<td width="214" align="left" class="footerTxt">
						
					</td>
				  </tr>
				</table>
			</div>
			<div id="divPrintWOFooterBtmBand"></div>
		</div>
    </div>
    </form>
</body>
</html>
