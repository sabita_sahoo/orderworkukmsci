<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintRetailForm.aspx.vb" Inherits="OrderWorkUK.PrintRetailForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Order Work: Print Work Order</title>
    <link href="../Styles/OWPortal.css" rel="stylesheet" type="text/css" />
    <style>
    body {
    background-color: #CECECE;
     }
     
        
    </style>
</head>
<body bgcolor="#F4F5F0" onload="javascript:this.print()">
    <form id="form1" runat="server">
    <div id="divPrintWOBody">
    	<div id="divPrintWOHeader">
			<div id="divPrintWOHeaderTopBand"></div>
			<div id="divPrintWOHeaderMiddleBand">
			<asp:Panel ID="pnlOWLogo" runat="server">
			<a href="Welcome.aspx"><img id="Img1" src="~/OWLibrary/Images/OrderWorks-Logo-Grey.gif" runat="server" align="left" class="imgLogo" alt="OrderWork - Service Reinvented" border="0"/></a>
			</asp:Panel>
			<asp:Panel ID="pnlClientLogo" runat="server">
			<asp:Image CssClass="clsClientLogo" runat="server" id="clientLogo" visible="false" ImageUrl="../../Images/ImgSecurePages/Logo.gif" />
			</asp:Panel>
			<div style="clear:both;"></div>
			</div>
			<div id="divPrintWOHeaderBtmBand"></div>
		</div>
		<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
		<div id="divPrintWOContent">
				<div id="divPrintWOContentTopBand"></div>
				<div id="divPrintWOContentMiddleBand">
					<table border="0" cellpadding="0" cellspacing="0" align="center">
					  <tr>
						<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
						<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
					  </tr>
					</table>
				</div>
				<div id="divPrintWOContentBtmBand"></div>
			</div>			
		</asp:Panel>		
		<asp:Panel runat="server" ID="pnlPrintContent" Visible="true">
			<div id="divPrintWOContent">
				<div id="divPrintWOContentTopBand"></div>
				<div id="divPrintWOContentMiddleBand">
					<div id="divPrintWOTopInfo">
						<div id="divPrintWORow"><div id="divPrintWOLeft"><b>Work Order No.</b></div><div id="divPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label></div></div>
						<div id="divPrintWORow"><div id="divPrintWOLeft"><b>Service</b></div><div id="divPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblTitle"></asp:Label></div></div>
						<div id="divPrintWORow" class="divPrintWORow" runat="server"><div id="divPrintWOLeft"><b><asp:Label runat="server" CssClass="formTxt" ID="lblProposedPriceHeader"></asp:Label></b></div><div id="divPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblProposedPrice"></asp:Label></div></div>						
						<asp:panel id="pnlPONumber" runat="Server" ><div id="divPrintWORow"><div id="divPrintWOLeft"><b><asp:Label ID="lblPONumberSummary" runat="server" Text="PO Number"></asp:Label></b></div><div id="divPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblPONumber"></asp:Label></div></div></asp:panel>
						<div id="divPrintWORow" class="divPrintWORow"><div id="divPrintWOLeft"><b>Appointment</b></div><div id="divPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblInstallationDate"></asp:Label></div></div>						
						<asp:panel id="pnlSalesAgent" runat="Server"><div id="divPrintWORow" class="divPrintWORow"><div id="divPrintWOLeft"><b>Sales Agent</b></div><div id="divPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblSalesAgent"></asp:Label></div></div></asp:panel>
						
						<div id="divClearBoth"></div>
					</div>
					<div id="divPrintWorkLocation" style="border-bottom:solid 1px #E1E1DF; padding-top:15px; padding-bottom:15px; margin-left:20px; margin-right:20px;" runat="server">
						<asp:Panel ID="pnlWorkLocation" runat="server">							
							<div id="PrintWOLocation">
							    <b>Customer Location:</b><br />
							    <asp:Label ID="lblWorkLocAddrDetails" runat="server"></asp:Label>
							</div>
							<div id="PrintWOLocationOfGoodsContainer">
							    <div id="PrintWOLocationOfGoods" runat="server">
							        <b><asp:Label runat="server" ID="lblGoodsLocationLabel"></asp:Label>:</b><br />
							        <span id="spnLocationName" runat="Server">
							            <div id="divPWORow"><div id="divPWOLeft">Name</div><div style="float:left">:&nbsp;</div><div id="divPWORight"><asp:Label ID="lblDepotName" runat="server"></asp:Label></div></div>
							        </span>
							        <div id="divPWORow"><div id="divPWOLeft">Address</div><div style="float:left">:&nbsp;</div><div id="divPWORight"><asp:Label ID="lblBusinessDivision" runat="server"></asp:Label></div></div>
							        
							    </div>							    
						        <div id="divPWORow"><div id="divPWOLeft"><asp:Label runat="Server" ID="lblProductsLabel"></asp:Label></div><div style="float:left">:&nbsp;</div><div id="divPWORight"><asp:Label ID="lblProducts" runat="server"></asp:Label></div></div>
						        
						     </div>
						</asp:Panel>
						<div id="divClearBoth"></div>
					</div>
					<div id="divClearBoth"></div>
					<div id="divPrintWOSpecialInstruction"  style="border-bottom:solid 1px #E1E1DF;">						
						<asp:Panel ID="pnlSpecialInst" runat="server">
						    <b>Scope of Work:</b><br />
							<asp:Label ID="lblScopeOfWork"  SafeEncode="false" runat="server"></asp:Label>
                            <asp:Panel ID="pnlInstruction" runat="server">
                                <br /><br />
							    <b>Special Instructions:</b><br />
							    <asp:Label ID="lblSpecialInstructions" runat="server"></asp:Label>
                            </asp:Panel>
						</asp:Panel>
						<div id="divClearBoth"></div>
					</div>		
                    <div id="divClearBoth"></div>
					<asp:Panel ID="pnlQuestionnaireInfo" runat="server" Visible="false">
					<div id="divPrintWODesc">
						<b>Questionnaire:</b><br />
						<asp:Label ID="lblQuestionnaireInfo" SafeEncode="false" runat="server"></asp:Label>
						<div id="divClearBoth"></div>
					</div>
					</asp:Panel>						
				</div>
				<div id="divPrintWOContentBtmBand"></div>
			</div>
		</asp:Panel>
		<div id="divPrintWOFooter">
			<div id="divPrintWOFooterTopBand"></div>
			<div id="divPrintWOFooterMiddleBand">
				<table width="915"  height="36"  border="0" align="center" cellpadding="0" cellspacing="0" class="BdrTop"> 
				  <tr valign="top">
					<td width="145" align="left" class="footerTxt" style="text-align:left;float:left;" ><img src="../OWLibrary/Images/Phone-Icon.gif" title="Phone" alt="Phone" width="15" height="12" align="absmiddle"/> <asp:Label ID="lblPhoneNumber" runat="server" Text="0203 053 0343"></asp:Label> </td>
					<td width="606" align="left" class="footerTxt" runat="server" id="tdEmail" ><a href="mailto:info@orderwork.co.uk" class="footerTxtSelected"><img src="../OWLibrary/Images/Email-Icon.gif" title="Contact Us" width="16" height="15" border="0" align="absmiddle" class="marginR5" />info@orderwork.co.uk</a>&nbsp;</td>
					<td width="214" align="left" class="footerTxt">
						
					</td>
				  </tr>
				</table>
			</div>
			<div id="divPrintWOFooterBtmBand"></div>
		</div>
    </div>
    </form>
</body>
</html>
