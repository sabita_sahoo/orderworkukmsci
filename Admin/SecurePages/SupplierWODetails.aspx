<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierWODetails.aspx.vb"
    Title="OrderWork: Supplier Work Order Detail" Inherits="OrderWorkUK.SupplierWODetails"
    MasterPageFile="~/MasterPage/MyOrderWork.Master" ValidateRequest="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">
    <style type="text/css">
<!--
.bottonTopGrey {
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-TR.gif) no-repeat right top;
	height:4px;
}
.bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
}
*+html .bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
	margin-top:-3px;
}
#divButton {
background-color:#999;
width:56px;
height:19px;
margin:10px 0 0;
padding:0;
}
img.btnCorner {
   width: 4px;
   height: 4px;
   border: none;
   display: block !important;
}
.buttonText {
font-family:Verdana;
font-size:11px;
color:#FFF;
text-decoration:none;
padding-left:11px;
padding-right:8px;
font-style:normal;
font-weight:400;
font-variant:normal;
line-height:11px;
}

.buttonText:hover {
color:#3F3F3F;
text-decoration:none;
}
#divModalParent {
    margin: auto;
    text-align: left;
}
.modalBackground 
 {
    background-color:Silver;
    filter:alpha(opacity=30) !important;
    opacity:0.3 !important;
    position:absolute !important;
    z-index:1 !important;
 }
.pnlConfirm {	
	background-color:#FFEBDE;
	font-family:Verdana;
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	line-height: 18px;
	border: 1px solid #CECBCE;
	padding: 10px;
	position:absolute !important;
	margin-left:650;
	margin-top:350;
}
.cursorHand
{
	cursor: hand;
}
.Hidden
{
visibility:hidden;
}
-->
</style>
    <script language="javascript" type="text/javascript" src="../OWLibrary/JS/Scripts.js"></script>
    <script language="javascript" type="text/javascript" src="../OWLibrary/JS/TabScripts.js"></script>
    <div id="divTopBand1" class="floatleft" style="border: 1px solid #E1E2DD; border-radius: 5px 5px 5px 5px;
        padding-bottom: 10px;">
        <asp:UpdatePanel ID="updatePnlSuppWOdetails" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <%@ Register TagPrefix="uc2" TagName="UCSupplierTabs" Src="~/UserControls/UK/UCSupplierTabs.ascx" %>
                <uc2:UCSupplierTabs ID="UCSupplierTabs1" runat="server"></uc2:UCSupplierTabs>
                <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
                    <div id="divconfirmPanel" class="mainLocationForm">
                        <div id="divConfirm">
                            <div id="divConfirmTopBand">
                            </div>
                            <div id="divConfirmMiddleBand">
                                <table width="350" border="0" cellpadding="0" cellspacing="0" height="100px">
                                    <tr>
                                        <td colspan="3">
                                            <strong>
                                                <asp:Label ID="lblConfirmMsgTxt" runat="server" CssClass="confirmmessageWODetail"></asp:Label></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <div id="divbtn">
                                                <asp:LinkButton ID="btnConfirm" runat="server" CausesValidation="false" class="txtListing"
                                                    TabIndex="29">
                                                    <img src="~/OWLibrary/Images/Icons/Icon-Confirm.gif" runat="server" id="imgbtnConfirm"
                                                        width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Confirm
                                                </asp:LinkButton></div>
                                        </td>
                                        <td width="10">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            <div id="divbtn">
                                                <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" class="txtListing"
                                                    TabIndex="30">
                                                    <img src="~/OWLibrary/Images/Icons/Icon-Cancel.gif" runat="server" id="imgbtnReset"
                                                        width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Back&nbsp;</asp:LinkButton></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divConfirmBtmBand">
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <div id="divValidationMain" class="divValidation" runat="server" visible="false">
                    <div class="roundtopVal">
                        <img src="OWLibrary/Images/Curves/Validation-TLC.gif" alt="" width="5" height="5"
                            class="corner" style="display: none" /></div>
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td height="15" align="center">
                                &nbsp;
                            </td>
                            <td width="565" height="15">
                                &nbsp;
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                        </tr>
                        <tr valign="middle">
                            <td width="63" align="center" valign="top">
                                <img src="OWLibrary/Images/Icons/Validation-Alert.gif" width="31" height="31">
                            </td>
                            <td width="565" class="validationText">
                                <div id="divValidationMsg">
                                </div>
                                <asp:Label ID="lblError" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                            </td>
                            <td width="20">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="15" align="center">
                                &nbsp;
                            </td>
                            <td width="565" height="15">
                                &nbsp;
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <div class="roundbottomVal">
                        <img src="OWLibrary/Images/Curves/Validation-BLC.gif" alt="" width="5" height="5"
                            class="corner" style="display: none" /></div>
                </div>
                <table cellpadding="0" cellspacing="0" bgcolor="#FBF9FA" width="1022" class="mainTableBorderr marginTop6">
                    <tr>
                        <td>
                            <%@ register tagprefix="uc1" tagname="UCSupplierWOSummary" src="~/UserControls/UK/UCSupplierWOSummary.ascx" %>
                            <uc1:UCSupplierWOSummary ID="UCSupplierWOSummary1" runat="server"></uc1:UCSupplierWOSummary>
                            <div id="divSupplierWODetailsHeader" runat="server" class="headerBuyerWODetails">
                                <div id="divCAHeader" runat="server" class="headerTabCommonWODet" onmouseover='javascript:rollovertabCommonWOdetails(this.id)'
                                    onclick='javascript:showhidetabCommonWOdetails(this.id,"divCAHeader","divCA")'
                                    onmouseout='javascript:normalTabCommonWODetails()'>
                                    <asp:Label ID="lblCAHeader" runat="server" Text="CA Header"></asp:Label></div>
                                <div id="divOverviewHeader" runat="server" class="headerTabCommonWODetHighLighted"
                                    onmouseover='javascript:rollovertabCommonWOdetails(this.id)' onclick='javascript:showhidetabCommonWOdetails(this.id,"divOverviewHeader","divOverview")'
                                    onmouseout='javascript:normalTabCommonWODetails()'>
                                    <asp:Label ID="lblOverViewHeader" runat="server" Text="Overview"></asp:Label></div>
                                <div id="divProductInfoHeader" runat="server" class="headerTabCommonWODet" onmouseover='javascript:rollovertabCommonWOdetails(this.id)'
                                    onclick='javascript:showhidetabCommonWOdetails(this.id,"divProductInfoHeader","divProductInfo")'
                                    onmouseout='javascript:normalTabCommonWODetails()'>
                                    Product Details</div>
                                <div id="divSupplierInfoHeader" runat="server" class="headerTabCommonWODet" onmouseover='javascript:rollovertabCommonWOdetails(this.id)'
                                    onclick='javascript:showhidetabCommonWOdetails(this.id,"divSupplierInfoHeader","divSupplierInfo")'
                                    onmouseout='javascript:normalTabCommonWODetails()'>
                                    Customer Details</div>
                                <div id="divAddnlInfoHeader" runat="server" class="headerTabCommonWODet" onmouseover='javascript:rollovertabCommonWOdetails(this.id)'
                                    onclick='javascript:showhidetabCommonWOdetails(this.id,"divAddnlInfoHeader","divAddnlInfo")'
                                    onmouseout='javascript:normalTabCommonWODetails()'>
                                    Additional Info</div>
                                <div id="divHistoryHeader" runat="server" class="headerTabCommonWODet" onmouseover='javascript:rollovertabCommonWOdetails(this.id)'
                                    onclick='javascript:showhidetabCommonWOdetails(this.id,"divHistoryHeader","divHistory")'
                                    onmouseout='javascript:normalTabCommonWODetails()'>
                                    History</div>
                                <div id="divResponseHeader" runat="server" class="headerTabCommonWODet" onmouseover='javascript:rollovertabCommonWOdetails(this.id)'
                                    onclick='javascript:showhidetabCommonWOdetails(this.id,"divResponseHeader","divResponse")'
                                    onmouseout='javascript:normalTabCommonWODetails()'>
                                    Responses</div>
                                <div class="divClearBoth">
                                </div>
                            </div>
                            <div id="divBuyerWODetailsContainer" class="detailsContainer" runat="server">
                                <div id="divCA" runat="server" class="displayNone" style="padding-top: 15px;">
                                    <div style="padding-left: 5px;">
                                        <table class="woSummaryText" style="margin-left: 15px; margin-right: 15px; margin-bottom: 0px;">
                                            <tr>
                                                <td valign="top" width="120">
                                                    <strong>Submitted</strong>
                                                </td>
                                                <td width="30" valign="top">
                                                    <strong>:</strong>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" CssClass="formTxt" ID="lblSubmittedDate"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" width="120">
                                                    <strong>
                                                        <asp:Label Text="Start Date" ID="lblStartD" runat="server"></asp:Label>
                                                    </strong>
                                                </td>
                                                <td valign="top" width="30">
                                                    <strong>:</strong>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" CssClass="formTxt" ID="lblStDateDtl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trProposedPrice" runat="server">
                                                <td width="120">
                                                    <strong>Proposed Price</strong>
                                                </td>
                                                <td width="30">
                                                    <strong>:</strong>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" CssClass="formTxt" ID="lblPrice"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120">
                                                    <strong>Specialist</strong>
                                                </td>
                                                <td width="30">
                                                    <strong>:</strong>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" CssClass="formTxt" ID="lblSpecialist"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- remarks area-->
                                        <asp:Panel runat="server" ID="pnlRemarks" Visible="false" Style="margin-left: 18px;
                                            margin-right: 18px; background-color: #ffffff;">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td height="10" style="padding: 0px; margin: 0px">
                                                    </td>
                                                    <td width="10" height="10" style="padding: 0px; margin: 0px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table id="tblDiscussion" border="0" cellspacing="0" cellpadding="0" class="formLabelGrey">
                                                            <tr>
                                                                <td height="32px" align="left" class="labelText" style="padding: 0px 10px 0px 0px">
                                                                    <strong>Remarks :</strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLDiscussion"
                                                                        runat="server">
                                                                        <ItemTemplate>
                                                                            <table width="640px" border="0" cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td width="220" valign="top" style="padding: 0px 0px 0px 0px; color: #993366;">
                                                                                        <%#IIf(DataBinder.Eval(Container.DataItem, "Status") <> OrderWorkLibrary.ApplicationSettings.WOStatusID.CAMailSent, (IIf(DataBinder.Eval(Container.DataItem, "ContactClassID") <> OrderWorkLibrary.ApplicationSettings.RoleOWID, (IIf(DataBinder.Eval(Container.DataItem, "ContactClassID") = OrderWorkLibrary.ApplicationSettings.RoleClientID, "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - <b>Client's Remarks </b></span>", "<span class='discussionTextSupplier'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - <b>Knowhow User Remarks </b></span>")), "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - <b>Admin's Remarks</b> </span>")), "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - " & ("Admin's CA Mail") & "  </span>")%>
                                                                                    </td>
                                                                                    <td width="10" align="center" valign="top">
                                                                                        :
                                                                                    </td>
                                                                                    <td valign="top" style="padding: 1px 10px 0px 0px">
                                                                                        <span class="labelText">
                                                                                            <%#Container.DataItem("Comments").Replace(Chr(13), "<BR>")%>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:DataList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="10">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="10" style="padding: 0px; margin: 0px">
                                                    </td>
                                                    <td height="10" style="padding: 0px; margin: 0px">
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="divClearBoth">
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="divClearBoth">
                                    </div>
                                </div>
                                <div id="divOverview" runat="server" class="displayBlock" style="padding-top: 0px;
                                    width: 1010px;">
                                    <asp:Panel ID="pnlCommInfo" runat="server" Visible="false" Style="margin-left: 20px;
                                        margin-bottom: 20px; margin-top: 5px; color: #FB0000; text-align: justify;">
                                        <b>Note:</b> By Accepting or Conditionally Accepting this workorder, you confirm
                                        that you agree with the T&C.<br />
                                        <asp:Label ID="lblSPFees" runat="server"></asp:Label><br />
                                        All prices submitted are inclusive of expenses, unless otherwise agreed and authorised.
                                        <br />
                                    </asp:Panel>
                                    <div id="divAttachments" runat="server" style="width: 1010px; margin-bottom: 20px;">
                                        <div class="divClearBoth">
                                        </div>
                                        <div style="margin-left: 20px; margin-top: 15px; margin-bottom: 10px;">
                                            <strong>Attachments:</strong></div>
                                        <div class="divClearBoth">
                                        </div>
                                        <asp:Panel ID="pnlAttachments" runat="server" Width="687">
                                            <asp:DataList runat="server" ID="dlAttList" Visible="true" AlternatingItemStyle-BackColor="#ffffff"
                                                ItemStyle-BackColor="#FAF8F9" ItemStyle-Width="687">
                                                <ItemTemplate>
                                                    <div style="width: 670px; height: 25px; padding-left: 17px;">
                                                        <asp:HiddenField runat="server" SafeEnCode="false" ID="filePath" Value='<%#Container.DataItem("FilePath")%>' />
                                                        <div style="margin-right: 15px; float: left; width: 15px; padding-top: 5px;">
                                                            <img id="imgFileIcon" src='<%# OrderWorkLibrary.CommonFunctions.GetFileIcon(Container.DataItem("Name"))  %>' /></div>
                                                        <div style="width: 500px; float: left; padding-top: 5px;">
                                                            <a class='footerTxtSelected' href='<%# getlink(Container.DataItem("FilePath")) %>'
                                                                target='_blank'>
                                                                <%# Container.DataItem("Name")%>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </asp:Panel>
                                    </div>
                                    <div class="divClearBoth">
                                    </div>
                                    <div class="bottomBorderHeightnone" id="divClientSpecialInsTopBorder" runat="server">
                                    </div>
                                    <div style="padding-left: 20px; margin-bottom: 15px; margin-top: 15px; width: 1010px;"
                                        id="divSpecialIns" runat="server">
                                        <div runat="server" id="divCustSpecialIns" style="padding-right: 15px;">
                                            <asp:Label ID="lblCustSpecIns" CssClass="redTextv" runat="server" Text="Customer Special Instructions:<br /><br />"></asp:Label>
                                            <asp:Label ID="lblCustSpecialInstructions" runat="server"></asp:Label>
                                            <div class="divClearBoth">
                                            </div>
                                        </div>
                                        <div runat="server" id="divOWSpecialIns" style="padding-right: 15px;">
                                            <asp:Label ID="lblSpecIns" CssClass="redTextv" Text="OrderWork Special Instructions:<br /><br />"
                                                runat="server"></asp:Label>
                                            <div runat="server" id="divSpecialInstructions"> 
                                            <%--<asp:Label ID="lblSpecialInstructions" runat="server"></asp:Label>--%>
                                            </div>
                                            <div class="divClearBoth">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divClearBoth">
                                    </div>
                                    <div class="bottomBorderHeightnone" id="divSpecialInsTopBorder" runat="server">
                                    </div>
                                    <div style="padding-left: 20px; margin-bottom: 15px; padding-right: 15px; width: 650px;
                                        padding-top: 10px;">
                                        <strong>Scope of Work:<br />
                                            <br />
                                        </strong>
                                        <div runat="server" id="divScopeOfWork"> 
                                        <%--<asp:Label ID="lblLongDesc" runat="server"></asp:Label>--%>
                                        </div>
                                        <div class="divClearBoth">
                                        </div>
                                    </div>
                                    <div class="divClearBoth">
                                    </div>
                                </div>
                                <div runat="server" id="divSupplierInfo" class="displayNone" style="padding-top: 15px;">
                                    <div style="width: 100%;">
                                        <table cellpadding="0" cellspacing="0" width="100%" style="line-height: 18px;">
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <table cellpadding="0" cellspacing="0" style="color: Gray;">
                                                        <tr>
                                                            <td width="20">
                                                                &nbsp;
                                                            </td>
                                                            <td valign="top" width="125">
                                                                <asp:Label ID="lblWorkLocAddrDetailsText" Style="line-height: 18px; color: Gray;"
                                                                    runat="server" CssClass="formLabelGreyBold" Text="Customer"></asp:Label>
                                                            </td>
                                                            <td width="30" valign="top">
                                                                :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblWorkLocAddrDetails" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td id="Td1" valign="top" align="right" runat="server" visible="false">
                                                    <div id="divRatingOuter" style="visibility: hidden;">
                                                        <div id="divRatingOuterTopBand">
                                                        </div>
                                                        <div id="divRatingOuterMiddleBand">
                                                            <div id="divRatingRow" style="background-color: #F6F3DO">
                                                                <strong>Rating Feedback:
                                                                    <asp:Label ID="lblRatingSupplier" runat="server"></asp:Label></strong></div>
                                                            <div id="divRatingRow" style="background-color: #F9F8E4">
                                                                <img src="~/OWLibrary/Images/Icons/Icon-Positive.gif" runat="server" id="imgPositive"
                                                                    align="absmiddle" class="imgRating" />Positive:
                                                                <asp:Label ID="lblPosRatingSupplier" runat="server"></asp:Label></div>
                                                            <div id="divRatingRow" style="background-color: #F6F3DO">
                                                                <img src="~/OWLibrary/Images/Icons/Icon-Neutral.gif" runat="server" id="imgNeutral"
                                                                    align="absmiddle" class="imgRating" />Neutral:
                                                                <asp:Label ID="lblNeuRatingSupplier" runat="server"></asp:Label></div>
                                                            <div id="divRatingRow" style="background-color: #F9F8E4">
                                                                <img src="~/OWLibrary/Images/Icons/Icon-Negative.gif" runat="server" id="imgNegative"
                                                                    align="absmiddle" class="imgRating" />Negative:
                                                                <asp:Label ID="lblNegRatingSupplier" runat="server"></asp:Label></div>
                                                        </div>
                                                        <div id="divRatingOuterBtmBand">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="divClearBoth">
                                        </div>
                                    </div>
                                    <div class="divClearBoth">
                                    </div>
                                </div>
                                <div runat="server" id="divHistory" class="displayNone" style="padding-top: 15px;">
                                    <div>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="pnlSupplierResponse" runat="server">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="left" valign="top">
                                                                    <asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLSupplierResponse"
                                                                        runat="server" Width="100%">
                                                                        <HeaderTemplate>
                                                                            <table width="1000" cellspacing="0" cellpadding="0" style="background-color: #993366;
                                                                                margin-left: 15px; margin-right: 15px; color: #ffffff; font-weight: bold; border-top: solid 1px #E3E2DD">
                                                                                <tr id="tblDGYourResponseHdr" align="left">
                                                                                    <td style="width: 155px" height="40" class="rightWhiteBorderHeader paddingLeft10">
                                                                                        Action Date
                                                                                    </td>
                                                                                    <td runat="server" style="width: 165px" id="tdDLCompany" class="rightWhiteBorderHeader paddingLeft10">
                                                                                        User
                                                                                    </td>
                                                                                    <td class="paddingLeft10">
                                                                                        <span class="formTxtOrange">Action Performed</span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <table width="1000px" cellspacing="0" cellpadding="0" style="background-color: #F7F3F4;
                                                                                margin-left: 15px; margin-right: 15px; color: #8A8889; border-bottom: solid 1px #E3E2DD">
                                                                                <tr id="tblDLRow" align="left">
                                                                                    <td style="width: 155px" height="40" class="brdrRightGrey paddingLeft10">
                                                                                        <%#Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateModified"), DateFormat.GeneralDate)%>
                                                                                    </td>
                                                                                    <td id="tdDLCompany1" style="width: 165px" runat="server" class="brdrRightGrey paddingLeft10"
                                                                                        valign="middle">
                                                                                        <%# iif(DataBinder.Eval(Container.DataItem, "ActionBy") = "","null",DataBinder.Eval(Container.DataItem, "ActionBy"))%>
                                                                                    </td>
                                                                                    <td class="brdrRightGrey paddingLeft10" style="padding-right: 10px; padding-top: 10px;">
                                                                                        <div class="bodytxtValidationMsg">
                                                                                            <span style="float: left;">
                                                                                                <%# DataBinder.Eval(Container.DataItem, "StandardValue")%>
                                                                                            </span>
                                                                                            <br />
                                                                                            <asp:Panel runat="server" ID="pnlChangedData" Visible='<%#IIf((Eval("StandardValue") <> "New" AND Eval("StandardValue") <> "UnDiscarded" AND Eval("StandardValue") <> "Submitted" AND Eval("StandardValue") <> "Sent" AND Eval("StandardValue") <> "Lost" AND Eval("StandardValue") <> "Rejected" AND Eval("StandardValue") <> "Accepted" AND Eval("StandardValue") <> "Closed" AND Eval("StandardValue") <> "Completed" AND Eval("StandardValue") <> "Supplier Accepted" AND Eval("StandardValue") <> "Issue Rejected"),True, False)%>'>
                                                                                                <asp:Repeater ID="Repeater1" runat="server" DataSource='<%#getChangedValue(Container.DataItem("WOTrackingId"))%>'>
                                                                                                    <ItemTemplate>
                                                                                                        <span style="line-height: 16px;" runat="server" id="span5" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "IsDataChanged") <> 0 ,True, False)%>'>
                                                                                                            <b>
                                                                                                                <%# Eval("FullName")%>
                                                                                                                -
                                                                                                                <%#Eval("DateModified")%></b><br />
                                                                                                        </span><span id="Span1" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldValue") <> DataBinder.Eval(Container.DataItem, "NewValue"), True, False)%>'>
                                                                                                            Price Changed:
                                                                                                            <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br />
                                                                                                        </span><span id="Span2" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldAptTime") & "" <> DataBinder.Eval(Container.DataItem, "NewAptTime"), True, False)%>'>
                                                                                                            Appointment Time Changed:
                                                                                                            <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br />
                                                                                                        </span><span id="Span3" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldDateStart") <> DataBinder.Eval(Container.DataItem, "NewDateStart"), True, False)%>'>
                                                                                                            <asp:Label ID="lblStartdate" runat="server" Text='<%#IIf(DataBinder.Eval(Container.DataItem, "BusinessArea") <> 101,"Start Date Changed","Appointment Date Changed")%>'></asp:Label>
                                                                                                            <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br />
                                                                                                        </span><span id="Span4" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldDateEnd") <> DataBinder.Eval(Container.DataItem, "NewDateEnd"), True, False)%>'>
                                                                                                            End Date Changed:
                                                                                                            <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br />
                                                                                                        </span>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemTemplate>
                                                                                                        <p>
                                                                                                            <div id="divUpdate" runat="server" visible='<%#IIf(Eval("DMLType")="U", True, False) %>'>
                                                                                                                <%--<span style="line-height:16px;" runat="server" id="span5"  visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "IsDataChanged") <> 0 ,True, False), True)%>' ><b><%# Eval("FullName")%> - <%#Eval("DateModified")%></b><br /></span>--%>
                                                                                                                <span id="Span1" style="line-height: 14px;" runat="server" visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldValue") <> DataBinder.Eval(Container.DataItem, "NewValue"),True, False), IIF(Eval("OldValue")="" AND Eval("NewValue")="",False, True))%>'>
                                                                                                                    <asp:Label runat="server" ID="lblprice" Text=''></asp:Label>
                                                                                                                    Portal Price Changed from
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "OldValue")%>
                                                                                                                    to
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br />
                                                                                                                </span><span id="Span2" style="line-height: 14px;" runat="server" visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldAptTime") & "" <> DataBinder.Eval(Container.DataItem, "NewAptTime"), True, False), IIF(Eval("OldAptTime")="" AND Eval("NewAptTime")="",False, True))%>'>
                                                                                                                    Appointment Time Changed from
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "OldAptTime")%>
                                                                                                                    to
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br />
                                                                                                                </span><span id="Span3" style="line-height: 14px;" runat="server" visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldDateStart") <> DataBinder.Eval(Container.DataItem, "NewDateStart"), True, False), IIF(Eval("OldDateStart")="" AND Eval("NewDateStart")="",False, True))%>'>
                                                                                                                    Start Date Changed from
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "OldDateStart")%>
                                                                                                                    to
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br />
                                                                                                                </span><span id="Span4" style="line-height: 14px;" runat="server" visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldDateEnd") <> DataBinder.Eval(Container.DataItem, "NewDateEnd"), True, False), IIF(Eval("OldDateEnd")="" AND Eval("NewDateEnd")="",False, True))%>'>
                                                                                                                    End Date Changed from
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "OldDateEnd")%>
                                                                                                                    to
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br />
                                                                                                                </span>
                                                                                                            </div>
                                                                                                            <div id="divInsert" runat="server" visible='<%#IIf(Eval("DMLType")="I" AND (Eval("Status")=7), True, False) %>'>
                                                                                                                <span style="line-height: 16px;" runat="server" id="span6"><b>
                                                                                                                    <%#Eval("FullName")%>
                                                                                                                    -
                                                                                                                    <%#Eval("DateModified")%></b></span><br />
                                                                                                                <span id="Span7" style="line-height: 14px;" runat="server">
                                                                                                                    <asp:Label runat="server" ID="Label1" Text=''></asp:Label>
                                                                                                                    Portal Price -
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br />
                                                                                                                </span><span id="Span8" style="line-height: 14px;" runat="server" visible='<%#IIf(Eval("BusinessArea")=101 ,True,False) %>'>
                                                                                                                    Appointment Time -
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br />
                                                                                                                </span><span id="Span9" style="line-height: 14px;" runat="server">
                                                                                                                    <%#IIf(Eval("NewAptTime") = "", "Start Date - ", "Appointment Date - ")%>
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br />
                                                                                                                </span><span id="Span10" style="line-height: 14px;" runat="server" visible='<%#IIf(Eval("NewAptTime")="",True,False)%>'>
                                                                                                                    End Date -
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br />
                                                                                                                </span>
                                                                                                            </div>
                                                                                                            <div id="divInsertWP" runat="server" visible='<%#IIf(Eval("DMLType")="I" AND Eval("Status")=53, True, False) %>'>
                                                                                                                <span style="line-height: 16px;" runat="server" id="span11"><b>
                                                                                                                    <%#Eval("FullName")%>
                                                                                                                    -
                                                                                                                    <%#Eval("DateModified")%></b></span><br />
                                                                                                                <span id="Span12" style="line-height: 14px;" runat="server">
                                                                                                                    <asp:Label runat="server" ID="Label2" Text=''></asp:Label>
                                                                                                                    Wholesale Price is changed to
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br />
                                                                                                                </span><span id="Span13" style="line-height: 14px;" runat="server">Appointment Time
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br />
                                                                                                                </span><span id="Span14" style="line-height: 14px;" runat="server">
                                                                                                                    <%#IIf(Eval("NewAptTime") = "", "Start Date", "Appointment Date")%>
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br />
                                                                                                                </span><span id="Span15" style="line-height: 14px;" runat="server" visible='<%#IIf(Eval("NewAptTime")="",True,False)%>'>
                                                                                                                    End Date
                                                                                                                    <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br />
                                                                                                                </span>
                                                                                                            </div>
                                                                                                        </p>
                                                                                                    </ItemTemplate>
                                                                                                </asp:Repeater>
                                                                                            </asp:Panel>
                                                                                            <img src="../OWLibrary/Images/Icon.gif" style='visibility: <%#ShowHideComments(Container.DataItem("WOTrackingID"))%>;
                                                                                                float: right; margin-top: 0px;' class="imgIconModal" id="imgshow" onmouseover='<%# "javascript:CollapseComments(" & Container.DataItem("WOTrackingID") & ", this.id)" %>'
                                                                                                onmouseout='<%# "javascript:CollapseComments(" & Container.DataItem("WOTrackingID") & ", this.id)" %>' /></div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <div id='_divCommentsDetail<%# DataBinder.Eval(Container.DataItem,"WOTrackingID") %>'
                                                                                style='visibility: hidden; position: absolute; margin-left: 740px; margin-top: -10px;
                                                                                background-color: #FFFFE1; border: solid 1px #000000; width: 200px; padding: 10px;
                                                                                font-size: 11px;'>
                                                                                <asp:Repeater ID="DLDiscussion" runat="server" DataSource='<%#GetDiscussions(Container.DataItem("WOTrackingId"))%>'>
                                                                                    <ItemTemplate>
                                                                                        <div style='visible: <%# iif( Trim(Container.DataItem("Comments")) = "" , "hidden", "visible") %>'>
                                                                                            <div>
                                                                                                <%#IIf(DataBinder.Eval(Container.DataItem, "Status") <> OrderWorkLibrary.ApplicationSettings.WOStatusID.CAMailSent, (IIf(DataBinder.Eval(Container.DataItem, "ContactClassID") <> OrderWorkLibrary.ApplicationSettings.RoleOWID, (IIf(DataBinder.Eval(Container.DataItem, "ContactClassID") = OrderWorkLibrary.ApplicationSettings.RoleClientID, "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - <b> Orderwork Remarks </b></span>", "<span class='discussionTextSupplier'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - <b>Knowhow User Remarks </b></span>")), "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - <b>Admin's Remarks</b> </span>")), "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - " & ("Admin's CA Mail") & "  </span>")%>
                                                                                            </div>
                                                                                            <div style="margin-top: 0px; margin-bottom: 10px;">
                                                                                                <%# Container.DataItem("Comments") %>
                                                                                            </div>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:Repeater>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:DataList>
                                                                    <div style="height: 10px; background-color: #ffffff; margin: 0px;">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="divClearBoth">
                                    </div>
                                </div>
                                <div runat="server" id="divResponse" class="displayNone">
                                    <div class="divRatings" style="padding: 10px; padding-left: 20px;">
                                        <asp:Repeater ID="rpQA" runat="server">
                                            <HeaderTemplate>
                                                <table>
                                                    <tr class="formTxt">
                                                        <td style="width: 350px;" class="formLabelGreyBold">
                                                            <b>Question</b>
                                                        </td>
                                                        <td style="width: 20px;">
                                                            &nbsp;
                                                        </td>
                                                        <td class="formLabelGreyBold">
                                                            <b>Answer</b>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table class="formTxt">
                                                    <tr>
                                                        <td style="width: 350px;" class="formTxt">
                                                            <%#Eval("Question")%>
                                                        </td>
                                                        <td style="width: 20px;" class="formTxt">
                                                            :
                                                        </td>
                                                        <td class="formTxt">
                                                            <%#Eval("Answer")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div runat="server" id="divAddnlInfo" class="displayNone" style="padding-top: 15px;
                                    width: 100%;">
                                    <div>
                                    </div>
                                    <table cellpadding="0" cellspacing="0" style="color: Gray;">
                                        <tr height="20">
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="125">
                                                <b>WO Type</b>
                                            </td>
                                            <td width="30">
                                                :&nbsp;
                                            </td>
                                            <td>
                                                <asp:Label runat="server" CssClass="formTxt" ID="lblCategory"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr height="20" runat="server" id="trJobNumber">
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="125">
                                                <b>Job Number</b>
                                            </td>
                                            <td width="30">
                                                :&nbsp;
                                            </td>
                                            <td>
                                                <asp:Label runat="server" CssClass="formTxt" ID="lblJRSNumber"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr height="20">
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="125">
                                                <b>Dress Code</b>
                                            </td>
                                            <td width="30">
                                                :&nbsp;
                                            </td>
                                            <td>
                                                <asp:Label runat="server" CssClass="formTxt" ID="lblDressCode"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr height="20">
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="125">
                                                <b>Supply Parts</b>
                                            </td>
                                            <td width="30">
                                                :&nbsp;
                                            </td>
                                            <td>
                                                <asp:Label runat="server" CssClass="formTxt" ID="lblSupplyParts"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr height="20" runat="server" id="trSalesAgent">
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="125">
                                                <b>Sales Agent</b>
                                            </td>
                                            <td width="30">
                                                :&nbsp;
                                            </td>
                                            <td>
                                                <asp:Label runat="server" CssClass="formTxt" ID="lblSalesAgent"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr height="20" runat="server" id="trFreesatMake">
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="125">
                                                <b>Freesat Make/Model</b>
                                            </td>
                                            <td width="30">
                                                :&nbsp;
                                            </td>
                                            <td>
                                                <asp:Label runat="server" CssClass="formTxt" ID="lblFreesatMake"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <div runat="server" id="divThermostatsQuestionsInfo" style="padding-top: 15px; color: Gray;
                                        padding-left: 20px;">
                                        <div>
                                            <b>Questionnaire</b></div>
                                        <div>
                                            <asp:Label runat="server" CssClass="formTxt" ID="lblThermostatsQuestionsInfo"></asp:Label></div>
                                    </div>
                                    <div id="divClearBoth">
                                    </div>
                                </div>
                                <div runat="server" id="divProductInfo" class="displayNone" style="padding-top: 15px;">
                                    <div>
                                    </div>
                                    <table cellpadding="0" cellspacing="0" id="tblGoodsLocation" style="display: block;
                                        color: Gray;" runat="Server">
                                        <tr>
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="125" valign="top">
                                                <b>Location of Goods</b>
                                            </td>
                                            <td width="30" valign="top">
                                                :&nbsp;
                                            </td>
                                            <td>
                                                <asp:Label runat="server" CssClass="formTxt" ID="lblDepotName"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr runat="Server" id="trGoodsAddrMargin">
                                            <td colspan="4" height="10">
                                            </td>
                                        </tr>
                                        <tr runat="server" id="trGoodsAddr">
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="125" valign="top">
                                                <b>Address</b>
                                            </td>
                                            <td width="30" valign="top">
                                                :&nbsp;
                                            </td>
                                            <td>
                                                <asp:Label runat="server" CssClass="formTxt" ID="lblLocationGoods"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" id="tblTVSize" style="display: block; color: Gray;
                                        padding-top: 10px;" runat="Server">
                                        <tr>
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="125" valign="top">
                                                <b>TV Size</b>
                                            </td>
                                            <td width="30" valign="top">
                                                :&nbsp;
                                            </td>
                                            <td>
                                                <asp:Label runat="server" CssClass="formTxt" ID="lblTVSize"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellpadding="0" cellspacing="0" style="color: Gray;">
                                        <tr runat="Server" id="trProductsMargin">
                                            <td colspan="4" height="10">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="125" valign="top">
                                                <b>
                                                    <asp:Label runat="Server" ID="lblProductsLabel"></asp:Label></b>
                                            </td>
                                            <td width="30" valign="top">
                                                :&nbsp;
                                            </td>
                                            <td valign="top">
                                                <asp:Label runat="server" CssClass="formTxt" ID="lblProducts"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="divClearBoth">
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="divClearBoth">
                </div>
                <%--<table cellpadding="0" cellspacing="0" class="supplierBtmSummary">
                <tr>
                    <td>
                        
                        <%@ register tagprefix="uc1" tagname="UCSupplierWOSummaryRepeat" Src="~/UserControls/UK/UCSupplierWOSummary.ascx" %>
                        <uc1:UCSupplierWOSummary id="UCSupplierWOSummaryRepeat" runat="server">
                        </uc1:UCSupplierWOSummary>
                    </td>
                </tr>
            </table>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="clearBoth">
    </div>
</asp:Content>
