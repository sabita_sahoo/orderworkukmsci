﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Cust0m3rD0cum3nt00018059.aspx.vb"
    Inherits="OrderWorkUK.Cust0m3rD0cum3nt00018059" Title="OrderWork: Customer Documents" %>

<style>
    .bodytxtRed
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13px;
        font-weight: bold;
        color: #C54043;
        text-decoration: none;
    }
    .blueBold
    {
        font-size: 12px;
        font-weight: bold;
        color: #1882B5;
    }
    .blueBold:hover
    {
        font-size: 12px;
        font-weight: bold;
        color: #C54043;
    }
    
    .blueText
    {
        font-size: 12px;
        font-weight: bold;
        color: #1882B5;
        text-decoration: none;
    }
    .blueText:hover
    {
        font-size: 12px;
        font-weight: bold;
        color: #C54043;
        text-decoration: none;
    }
    .divBody
    {
        min-height: 450px;
        width: 1025px;
        margin: 10px auto 20px auto;
        font-family: Verdana;
        font-size: 12px;
        margin-bottom: 10px;
    }
    .divContainerStyle
    {
        height: 540px;
        width: 940px;
        border: 1px solid #e2e2e2;
    }
    #divPageMain
    {
        font-family: Verdana;
        font-size: 12px;
        margin-bottom: 10px;
    }
    .clearBoth
    {
        clear: both;
    }
</style>
<header>
<title>OrderWork: Customer Documents
</title>
</header>
<body>
    <div class="divBody">
        <div>
            <img id="ctl00_imgOWLogo" title="myOrderwork" alt="myOrderwork" src="../OWLibrary/Images/OrderWorkUkRedesign/Logo-MyOW-supplier.gif"
                style="height: 54px; width: 265px; border-width: 0px; margin-top: 2px; align: left"
                align="middle" border="0">
        </div>
        <br />
        <div class="clearBoth">
        </div>
        <div class="divContainerStyle">
            <div style="width: 930px; padding-left: 10px; padding-top: 6px; height: 27px; color: Black;
                margin-left: 0px; margin-top: 0px; background-image: url(../OWLibrary/Images/OrderWorkUkRedesign/Title_bg.gif);">
                <strong>Customer Documents </strong>
                <div id="divMiddleBand">
                    <div id="divMainTab">
                        <div class="paddedBox" id="divProfile">
                            <div id="divMain" style="padding-left: 15px; padding-right: 20px">
                                <p align="justify" class="bodyTxt">
                                    <p class="marginT24B10">
                                        <br />
                                        <p class="bodytxtRed">
                                            Download our Terms and Conditions here: <span class="paddingL483"></span><a href="~/Downloads/Customer Terms and Conditions.pdf"
                                                runat="server" id="lnlDownLoad" target="_blank" class="blueText"><strong>
                                                    <img src="../OWLibrary/Images/Icons/pdf.gif" width="16" height="20" hspace="5" border="0"
                                                        align="absmiddle">Download PDF</strong></a></p>
                                        <br />
                                        <p class="bodytxtRed">
                                            Download our Customer Complaint Procedure here: <span class="paddingL483"></span><a href="~/Downloads/Orderwork Complaint Procedure.pdf"
                                                runat="server" id="A2" target="_blank" class="blueText"><strong>
                                                    <img src="../OWLibrary/Images/Icons/pdf.gif" width="16" height="20" hspace="5" border="0"
                                                        align="absmiddle">Download PDF</strong></a></p>
                                    </p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
