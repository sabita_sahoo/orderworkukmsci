<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Specialists.aspx.vb" Title="OrderWork: Users Listing" Inherits="OrderWorkUK.Specialists" MasterPageFile ="~/MasterPage/MyOrderWork.Master" ValidateRequest="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
	<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
		<div class="errContainer">
			<div><img src="../OWLibrary/Images/Error_TopBand.gif" width="480" height="10"></div>
			<div class="errMdlBox">
				<table border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
					<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
				  </tr>
				</table>			
			</div>
			<div><img src="../OWLibrary/Images/Error_BtmBand.gif" width="480" height="10"></div>				
		</div>
	</asp:Panel>
	<div id="divClearBoth"></div>
	<div id="divTopBand" class="divContainerStyle floatLeft" >
	
	<%@ Register TagPrefix="uc1" TagName="UCSpecialistsListingUK" Src="~/UserControls/UK/UCSpecialistsListingUK.ascx" %>
	<uc1:UCSpecialistsListingUK id="UCSpecialistsListing1" runat="server"></uc1:UCSpecialistsListingUK>
    </div>
	<div id="div1" class="clearBoth"></div>
</asp:content>
