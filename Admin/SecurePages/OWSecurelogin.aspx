<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWContentOnly.Master" CodeBehind="OWSecurelogin.aspx.vb" enableViewState="false" enableViewStateMac="false"  Inherits="OrderWorkUK.OWsecureLogin" 
    title="OrderWork - Services Reinvented - Secure Login" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
<script language="javascript" type="text/javascript">
function CheckKeyPress(evt) 
    {
        try 
        {
            if (window.event && window.event.keyCode == 13) 
            {
                //alert('enter key pressed, evt:' + evt);
                document.getElementById("ctl00_ContentPlaceHolder1_hdnbtnLogin").click();
                event.returnValue = false;
                event.cancelBubble = true;
            }
            else if (evt == 13) 
            {
                //Non IE browsers
                document.getElementById("ctl00_ContentPlaceHolder1_hdnbtnLogin").click();
                evt.cancelBubble = true;
                evt.stopPropagation();
                return false;
            }
        }
        catch (ex) 
        {
            //alert(ex.message);
        }
    }
    //Script required for the welcome page
function showPasswordFld(defaultText)
{	
    if(document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword"))
    if(document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword").value == "")
	{
		document.getElementById("txtLoginPasswordText").className = "passwordFieldHidden";
		document.getElementById("txtLoginPasswordText").value = defaultText; 
		document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword").className = "passwordField";		
		document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword").focus();
	}
}
function hidePasswordFld(defaultText)
{ 
	if(document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword"))
	if(document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword").value == "")
	{
		document.getElementById("txtLoginPasswordText").className = "passwordField";
		document.getElementById("txtLoginPasswordText").value = defaultText;
		document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword").className = "passwordFieldHidden";		
	}
}
</script>
 <div id="divLoginBodyInner" style="padding-bottom:3px;">

            <asp:UpdatePanel ID="UpdatePnlLogin" runat="server"  UpdateMode="Conditional" RenderMode=Inline>
              <ContentTemplate>
            <div class="displaynone">&nbsp;&nbsp;</div>
             <div id="HmMain" style="float:left; height:350px; padding-top:15px;margin-left:20px; background-image:url(OWLibrary/Images/Bg_GreyBg.jpg); background-position:bottom right; background-color:#FCFCFC; background-repeat:no-repeat;  ">
              <div id="divLoginMain">
                <div id="divLoginLogo"><img  src="OWLibrary/Images/myorderwork.jpg" style=" margin-top:0px; margin-left:0px;" alt="myOrderWork Client Portal" title="myOrderWork Client Portal" /> </div>
                 <div class="displaynone">&nbsp;&nbsp;</div>
                 <div id="divLogin">
                  <asp:Panel ID="pnlLogin" runat="server" defaultbutton="hdnbtnLogin">
                   <div id="divLoginRight">
                   <img  src="OWLibrary/Images/LoginUpperband.jpg" />
                   
                   <div id="divRightImage">
	               <div id="divLoginInner">
	                 <div><b>Log into your account</b></div> 
	                 <div class="displaynone">&nbsp;&nbsp;</div>
		              <div style="width:250px;">
		             <asp:TextBox id="txtLoginUserName" TabIndex="1" ValidationGroup="FP"  runat="server" CssClass="TxtBx"  Width="240px"></asp:TextBox>
		             <cc1:TextBoxWatermarkExtender TargetControlID="txtLoginUserName" ID="TextBoxWatermarkExtender1" WatermarkText="Enter email address"  runat="server"/>		 		 
		             <div class="displaynone">&nbsp;&nbsp;</div>
		             <div style="float:left; width:250px;">
		             <div style="float:left; width:175px; clear:right;">
		                 <input TabIndex="2" type="text" id="txtLoginPasswordText" class="passwordField" value="Password" onfocus="showPasswordFld('Password')" >
		                <asp:TextBox TabIndex="2" onblur="hidePasswordFld('Password')" id="txtLoginPassword" Width="160px" cssclass="passwordFieldHidden" runat="server" TextMode="Password" ></asp:TextBox>
		            </div>
		            <div class="displaynone">&nbsp;&nbsp;</div>
		            <div style="float:left; width:62px; padding-top:3px;">
		                <%--<asp:LinkButton runat="server" ID="lnkBtnLogin" TabIndex="4" Text="Sign In" OnClick="LoginMe" CausesValidation="True" CssClass="FPasswordYellowBtn SignInTextBx" ToolTip="Sign In" />--%>
						<asp:Button runat="server" ID="hdnbtnLogin" TabIndex="4" BorderWidth="0" BorderColor="#F8F8F8" Text="Sign In" OnClick="LoginMe" CausesValidation="True" CssClass="FPasswordYellowBtn SignInTextBx" />
		            </div>
		            <div class="displaynone">&nbsp;&nbsp;</div>
		            </div>
	            </div>
            		  
		               <div id="divValidationMain" class="PopUpTailDiv" runat="server" visible="false">
		                        <img src="OWLibrary/Images/PopupTail.gif" class="PopTail">
		                        <img src="OWLibrary/Images/Note_Image.jpg" align="left">
		                        <div style="float:right; width:150px; color:#FF0000; font-size:11px;  ">
		                            <asp:Label ID="lblMsg"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                                    <asp:ValidationSummary id="validationSummarySubmit" ShowSummary="true" ValidationGroup="FP" EnableClientScript="true" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="List"  ></asp:ValidationSummary>		            
                                  
		                        </div>
		                    </div> 
            		 
		             <a  href="OWForgotPassword.aspx" class="ForgotLink" TabIndex="5" target="_self">Forgot&nbsp; password?</a>
		             <asp:CheckBox ID="chkRemember" Text="Remember me" CssClass="ChechBox" TabIndex="3" runat="server" />
		             <div class="displaynone">&nbsp;&nbsp;</div>
		             <div> </div>
	               </div>
	               </div>
	               </div>
	                 </asp:Panel>
                 </div>
                </div>
              </div>
            <div style="clear:both;"></div>
              </ContentTemplate>
             
            </asp:UpdatePanel>

            <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePnlLogin" runat="server">
                    <ProgressTemplate>
                        <div>
                            <img  align=middle src="OWLibrary/Images/indicator.gif" />
                            Verifying Credentials ...
                        </div>      
                    </ProgressTemplate>
            </asp:UpdateProgress>
            <cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="PnlLogin" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />

            </div>
            
</asp:Content>

