<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/MyOrderWork.Master" Title="OrderWork: Work Order Upload Parameters" CodeBehind="WOUploadParams.aspx.vb" Inherits="OrderWorkUK.WOUploadParams" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">
<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
	<div class="errContainer">
		<div><img src="../OWLibrary/Images/Error_TopBand.gif" width="480" height="10"></div>
		<div class="errMdlBox">
			<table border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
				<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
			  </tr>
			</table>			
		</div>
		<div><img src="../OWLibrary/Images/Error_BtmBand.gif" width="480" height="10"></div>				
	</div>
</asp:Panel>
<div id="divClearBoth"></div>
<div id="divTopBand" class="floatLeft" >
	<div class="divContainerStyle clsInfoHeaderStyle  paymentHeading" id="div1" style="width:992px;padding-left:30px;height:25px;color:White;border-bottom-width:0px;"><strong>WO Upload Paramters</strong></div>
	     <div class="floatleft divContainerStyle" style="border-bottom-width:0px;border-top-width:0px;height:10px;"></div>
	      <%@ Register TagPrefix="uc1" TagName="UCMSWOUploadParamsUK" Src="~/UserControls/UK/UCMSWOUploadParamsUK.ascx" %>
          <uc1:UCMSWOUploadParamsUK id="UCMSWOUploadParamsUK1" runat="server"></uc1:UCMSWOUploadParamsUK>
         
          </div>
    <div class="clearBoth"></div>
 

</asp:Content>
