<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuyerWODetails.aspx.vb" Title="OrderWork: Client Work Order Details" Inherits="OrderWorkUK.BuyerWODetails" MasterPageFile="~/MasterPage/MyOrderWork.Master" ValidateRequest="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">
<style type="text/css">
<!--
.bottonTopGrey {
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-TR.gif) no-repeat right top;
	height:4px;
}
.bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
}
*+html .bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
	margin-top:-3px;
}
#divButton {
background-color:#999;
width:56px;
height:19px;
margin:10px 0 0;
padding:0;
}
img.btnCorner {
   width: 4px;
   height: 4px;
   border: none;
   display: block !important;
}
.buttonText {
font-family:Verdana;
font-size:11px;
color:#FFF;
text-decoration:none;
padding-left:11px;
padding-right:8px;
font-style:normal;
font-weight:400;
font-variant:normal;
line-height:11px;
}

.buttonText:hover {
color:#3F3F3F;
text-decoration:none;
}
#divModalParent {
    margin: auto;
    text-align: left;
}
.modalBackground 
 {
    background-color:Silver;
    filter:alpha(opacity=30) !important;
    opacity:0.3 !important;
    position:absolute !important;
    z-index:1 !important;
 }
.pnlConfirm {	
	background-color:#FFEBDE;
	font-family:Verdana;
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	line-height: 18px;
	border: 1px solid #CECBCE;
	padding: 10px;
	position:absolute !important;
	margin-left:650;
	margin-top:350;
}
.cursorHand
{
	cursor: hand;
}
.Hidden
{
visibility:hidden;
}
-->
</style>
<script language="javascript" type="text/javascript" src="../OWLibrary/JS/Scripts.js"></script>	
<script language="javascript" type="text/javascript" src="../OWLibrary/JS/TabScripts.js"></script>	
	<div id="divTopBand1" class="floatleft" style="border:1px solid #E1E2DD;border-radius:5px 5px 5px 5px;padding-bottom:10px;" >
	<asp:UpdatePanel ID="updatePnlBuyerWOdetails" runat="server"  RenderMode=Inline>
  <ContentTemplate>	
	<%@ Register TagPrefix="uc2" TagName="UCBuyerTabs" Src="~/UserControls/UK/UCBuyerTabs.ascx" %>
                 <uc2:UCBuyerTabs id="UCBuyerTabs1" runat="server"></uc2:UCBuyerTabs>
     
	  <asp:Panel ID="pnlConfirm" runat="server" Visible="false" >
 <div id="divconfirmPanel" class="mainLocationForm"  >
 
 <div id="divConfirm">
  <div id="divConfirmTopBand"></div>
    <div id="divConfirmMiddleBand" class="paddingLeft29" >
    <table width="350" border="0" cellpadding="0" cellspacing="0" Height="100px" >
        <tr>
            <td colspan="3"><strong><asp:Label ID="lblConfirmMsgTxt" runat="server" CssClass="confirmmessageWODetail"></asp:Label></strong></td>
        </tr>
      <tr>
        <td align="left" style="width:160px;">
                <div><asp:LinkButton ID="btnConfirm" runat="server" CausesValidation="false" class="clsButtonGreyDetails"  style="border:0px;mar" width="100" TabIndex="29" > <img src="~/OWLibrary/Images/Icons/Icon-Confirm.gif" runat="server" id="imgbtnConfirm" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Confirm </asp:LinkButton></div></td>
        <td width="10">&nbsp;</td>
        <td align="left" ><div><asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" class="clsButtonGreyDetails" style="border:0px;margin-left:10px;" width="100" TabIndex="30" > <img src="~/OWLibrary/Images/Icons/Icon-Cancel.gif" runat="server" id="imgbtnReset" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Back&nbsp;</asp:LinkButton></div></td>
      </tr>
    </table>
   </div>
   <div id="divConfirmBtmBand"></div>
   </div>
 </div> 
  </asp:Panel>
<div id="divValidationMain" class="divValidation" runat="server" visible="false" >
            <div class="roundtopVal"><img src="OWLibrary/Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
              <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
	            <tr>
	              <td height="15" align="center">&nbsp;</td>
	              <td width="565" height="15">&nbsp;</td>
	              <td height="15">&nbsp;</td>
	            </tr>
	            <tr valign="middle">
	              <td width="63" align="center" valign="top"><img src="OWLibrary/Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
	              <td width="565" class="validationText"><div  id="divValidationMsg"></div>
	              <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
	              
	              </td>
	              <td width="20">&nbsp;</td>
	            </tr>
	            <tr>
	              <td height="15" align="center">&nbsp;</td>
	              <td width="565" height="15">&nbsp;</td>
	              <td height="15">&nbsp;</td>
	            </tr>
              </table>
              
              <div class="roundbottomVal"><img src="OWLibrary/Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
            </div>
    <table cellpadding="0" cellspacing="0" bgcolor="#FBF9FA" class="mainTableBorderr marginTop6" width="1022">
	<tr>
	<td>
	<%@ Register TagPrefix="uc1" TagName="UCBuyerWOSummary" Src="~/UserControls/UK/UCBuyerWOSummary.ascx" %>
         <uc1:UCBuyerWOSummary id="UCBuyerWOSummary1" runat="server"></uc1:UCBuyerWOSummary> 
    <div id="divBuyerWODetailsHeader" runat="server" class="headerBuyerWODetails">
        <div id="divOverviewHeader" runat="server" class="headerTabCommonWODetHighLighted" onmouseover='javascript:rollovertabCommonWOdetails(this.id)' onclick='javascript:showhidetabCommonWOdetails(this.id,"divOverviewHeader","divOverview")' onmouseout='javascript:normalTabCommonWODetails()' >Overview</div>
        <div id="divSupplierInfoHeader" runat="server" class="headerTabCommonWODet" onmouseover='javascript:rollovertabCommonWOdetails(this.id)' onclick='javascript:showhidetabCommonWOdetails(this.id,"divSupplierInfoHeader","divSupplierInfo")' onmouseout='javascript:normalTabCommonWODetails()'>Customer Details</div>
        <div id="divAddnlInfoHeader" runat="server" class="headerTabCommonWODet" onmouseover='javascript:rollovertabCommonWOdetails(this.id)' onclick='javascript:showhidetabCommonWOdetails(this.id,"divAddnlInfoHeader","divAddnlInfo")' onmouseout='javascript:normalTabCommonWODetails()'>Additional Info</div>
        <div id="divHistoryHeader" runat="server" class="headerTabCommonWODet" onmouseover='javascript:rollovertabCommonWOdetails(this.id)' onclick='javascript:showhidetabCommonWOdetails(this.id,"divHistoryHeader","divHistory")' onmouseout='javascript:normalTabCommonWODetails()'>History</div>
        
        <div class="divClearBoth"></div>
    </div>
	<div id="divBuyerWODetailsContainer" class="detailsContainer" runat="server"  >
        <div id="divOverview" runat="server" class="displayBlock"> 			
				<div id="divAttachments" runat="server" style="width:694px;margin-bottom:20px; text-align:left;">			
			               <div class="divClearBoth"></div>
			               <div style="float:left; margin-left:20px; margin-top:15px; margin-bottom:10px; "><strong>Attachments:</strong></div>
			               <div class="divClearBoth"></div>
                         			<asp:Panel ID="pnlAttachments" runat="server" Width="687">
                                    	  <asp:datalist runat="server" ID="dlAttList" Visible="true" AlternatingItemStyle-BackColor="#ffffff" ItemStyle-BackColor="#FAF8F9" ItemStyle-Width="687">
                                              <itemtemplate>
                                                <div style="width:674px; height:20px;padding-left:17px; padding-top:5px;">
                                                    <asp:HiddenField runat="server" id="filePath" SafeEncode="false"  value='<%#Container.DataItem("FilePath")%>'/>
                                                    <div style="margin-right:15px; float:left; width:15px;"><img id="imgFileIcon" src='<%# OrderWorkLibrary.CommonFunctions.GetFileIcon(Container.DataItem("Name"))  %>' /></div>
                                                    <div class='txtGreySmallSelected' style="width:500px; float:left; vertical-align:middle;">
                                                          <a class='footerTxtSelected' href='<%# getlink(Container.DataItem("FilePath")) %>' target='_blank'><%# Container.DataItem("Name")%></a>
                                                    </div>
                                                </div>
                                                
                                              </itemtemplate>
                                            </asp:datalist>
            						</asp:Panel>
                 </div>
								
					<asp:panel ID="pnlScopeOfWork" runat="server">			
	 			<div style="padding-left:20px; margin-bottom:15px; padding-right:15px; padding-top:15px;  width:650px;">	 			             
	 			        <strong>Scope of Work:<br /><br /></strong>
				     <div runat="server" id="divScopeOfWork">
				         <%-- <asp:Label ID="lblLongDesc" runat="server"></asp:Label></strong>--%>
				     </div>

				        <div class="divClearBoth"></div>
		        </div>
		        </asp:Panel>
		        <asp:panel ID="pnlClientScope" runat="server">
		        <div style="padding-left:20px; margin-bottom:15px; padding-right:15px; width:650px; padding-top:15px;">
	 			             
	 			        <strong>Scope of Work:<br /><br /></strong>
		            <div runat="server" id="divClientScope"> 
		                <%-- <asp:Label ID="lblClientScope" runat="server"></asp:Label></strong>--%>
		            </div>

				        <div class="divClearBoth"></div>
		        </div>
		        </asp:panel>
		        <div class="divClearBoth"></div>
		        <div class="bottomBorderHeightnone" id="divSpecialInsTopBorder" runat="server"></div>
				<div style="padding-left:20px;" id="divSpecialIns" runat="server">				
				    <asp:Label ID="lblSpecIns" CssClass="redTextv" Text="Special Instructions:" runat="server"></asp:Label><br /><br />
				    <div runat="server" id="divSpecialInstructions"> 
				        <%-- <asp:Label ID="lblSpecialInstructions" runat="server"></asp:Label>--%>
				    </div>

				    <div class="divClearBoth"></div>
				</div>
				<div class="divClearBoth"></div>
	 		</div>	  
		<div runat="server" id="divSupplierInfo" class="displayNone">
                <div style="float: left;width:100%;padding-top:20px; ">
				<table cellpadding="0" cellspacing="0" width="100%" style="float: left;">
					<tr>
						<td style="vertical-align:top;">
							<table cellpadding="0" cellspacing="0">
						        <tr>
							        <td width="20">&nbsp;</td>
							        <td valign="top" width="125"><asp:Label ID="lblWorkLocAddrDetailsText" runat="server" CssClass="formLabelGreyBold" Text="Customer" ></asp:Label></td>

							        <td width="30" valign="top">:</td>
							        <td><asp:Label ID="lblWorkLocAddrDetails" runat="server"></asp:Label></td>
						        </tr>
        						
					        </table>
					<br>
					<table cellpadding="0" cellspacing="0" id="tblGoodsLocation" style="display:block;" runat="Server">
			            <tr >
			                <td width="20">&nbsp;</td>
			                <td width="125" valign="top"><b><asp:Label runat="Server" ID="lblDepotNameLabel"  CssClass="formLabelGreyBold" ></asp:Label></b></td>
			                <td width="30" valign="top">:&nbsp;</td>
			                <td valign="top"><asp:Label runat="server" CssClass="formTxt" ID="lblDepotName"></asp:Label></td>
			            </tr>
			            <tr runat="Server" id="trGoodsAddrMargin"><td colspan="4" height="10"></td></tr>
			            <tr style="margin-top:20px;" runat="server" id="trGoodsAddr">
			                <td width="20">&nbsp;</td>
			                <td width="125" valign="top" class="formLabelGreyBold" ><b>Address</b></td>
			                <td width="30" valign="top">:&nbsp;</td>
			                <td><asp:Label runat="server" CssClass="formTxt" ID="lblLocationGoods"></asp:Label></td>
			            </tr>
					</table>
					<table cellpadding="0" cellspacing="0" >
					    <tr runat="Server" id="trProductsMargin"><td colspan="4" height="10"></td></tr>
					    <tr >
			                <td width="20">&nbsp;</td>
			                <td width="125" valign="top"><b><asp:Label runat="server" ID="lblProductsLabel" CssClass="formLabelGreyBold" ></asp:Label></b></td>
			                <td width="30" valign="top">:&nbsp;</td>
			                <td><asp:Label runat="server" CssClass="formTxt" ID="lblProducts"></asp:Label></td>
			            </tr>
			            
					</table>
					
					<br><br>
						</td>
						<td valign="top" align="right">
						    <div id="divRatingOuter" class="divRatingOuter" runat="Server">
						        <div id="divRatingOuterTopBandBuyerWODet"></div>
						        <div id="divRatingOuterMiddleBandBuyerWODet">
						         
						            <div id="divRatingRowBuyerWODet" style="background-color:#F6F3DO"><strong>Supplier Details</strong></div>
						            <div id="divRatingMessage" style="display:none;text-align:left;margin-top:10px;" class="RatingMsg">
The "Performance rating" provides a weighted average score for the work that Supplier have 
undertaken.  Each rating is given the following weight when calculating your overall score.
<br />
Positive � 1<br />
Neutral � 0.5<br />
Negative � 0<br />
</div> 
						            <div id="divRatingRowBuyerWODet" style="background-color:#F9F8E4; color:#993366;" onmouseover='javascript:ShowHideRating("divRatingMessage")' onmouseout='javascript:ShowHideRating("divRatingMessage")'><strong>Performance Rating: <asp:Label ID="lblRatingSupplier" runat="server"></asp:Label></strong></div>
						            <div id="divRatingRowBuyerWODet" style="background-color:#F6F3DO"><img src="~/OWLibrary/Images/Icons/Icon-Positive.gif" runat="server" id="imgPositive" align="absmiddle" class="imgRating" />Positive: <asp:Label ID="lblPosRatingSupplier" runat="server"></asp:Label></div>
						            <div id="divRatingRowBuyerWODet" style="background-color:#F9F8E4"><img src="~/OWLibrary/Images/Icons/Icon-Neutral.gif" runat="server" id="imgNeutral" align="absmiddle" class="imgRating" />Neutral: <asp:Label ID="lblNeuRatingSupplier" runat="server"></asp:Label></div>
						            <div id="divRatingRowBuyerWODet" style="background-color:#F6F3DO"><img src="~/OWLibrary/Images/Icons/Icon-Negative.gif" runat="server" id="imgNegative" align="absmiddle" class="imgRating" />Negative: <asp:Label ID="lblNegRatingSupplier" runat="server"></asp:Label></div>
						            <div id="divRatingRowBuyerWODet" style="background-color:#F9F8E4;font-weight:700;"><img src="~/OWLibrary/Images/Icons/Icon-Total.gif" runat="server" id="img1" align="absmiddle" class="imgRating" />Total: <asp:Label ID="lblTotal" runat="server"></asp:Label></div>
						        </div>
						        <div id="divRatingOuterBtmBand"></div>
						    </div>						
						</td>			
					</tr>
				</table>
				<div class="divClearBoth"></div>	
				</div>	
				<div class="divClearBoth"></div>	
	 	</div>
		<div runat="server" id="divHistory" class="displayNone">
				 	<table cellpadding="0" cellspacing="0" width="100%" style="float: left;">
			<tr>
				<td><asp:Panel ID="pnlSupplierResponse" runat="server">
			 <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="28" align="left" valign="middle" class="SubMenuTxtBoldSelected"><asp:Label ID="lblDLHeader" runat="server"></asp:Label></td>
                        </tr>
                    </table>
					 <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                      
                        <td align="left" valign="top">
				<asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLSupplierResponse" Runat="server" Width="100%" >
				<HeaderTemplate>
					<table width="900px" cellspacing="0" cellpadding="0" style="background-color:#993366; margin-left:15px; margin-right:15px; color:#ffffff; font-weight:bold;">
						 <tr id="tblDGYourResponseHdr" align="left">
                              
                              <td width="200" height="40" class="rightWhiteBorderHeader paddingLeft10">Action Date</td>
                              <td width="300" runat="server" id="tdDLCompany" class="rightWhiteBorderHeader paddingLeft10">User</td>
							  <td class="paddingLeft10"><span class="formTxtOrange">Action Performed</span></td>
                            </tr>
					</table>
				</HeaderTemplate>
				<ItemTemplate>
					<table width="900px" cellspacing="0" cellpadding="0" style="background-color:#F7F3F4; margin-left:15px; margin-right:15px; color:#8A8889;border-bottom:solid 1px #E3E2DD;">
						<tr id="tblDLRow" align="left">
                              <td width="200" height="40" class="brdrRightGrey paddingLeft10"><%#Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateModified"), DateFormat.GeneralDate)%></td> 						      														  
                              <td width="300" id="tdDLCompany1" runat="server" class="brdrRightGrey paddingLeft10"><%# iif(DataBinder.Eval(Container.DataItem, "ActionBy") = "","null",DataBinder.Eval(Container.DataItem, "ActionBy"))%></td>                             
                              <td  class="brdrRightGrey paddingLeft10">
                              <span class="bodytxtValidationMsg" ><%#IIf(DataBinder.Eval(Container.DataItem, "StandardValue") & "" = "Accepted", "Active", DataBinder.Eval(Container.DataItem, "StandardValue"))%>
                              <asp:Repeater  ID="Repeater1" Runat="server" DataSource='<%#getChangedValue(Container.DataItem("WOTrackingId"))%>'  >
                    <ItemTemplate> 
                    <span id="Span1" runat="server" visible='<%#IIf((DataBinder.Eval(Container.DataItem, "Status") = 53), True, False)%>' >- <%#DataBinder.Eval(Container.DataItem, "Value")%><br /></span>
                    </ItemTemplate>
                    </asp:Repeater>	
                        <img src="../OWLibrary/Images/Icon.gif" align="right" style ='visibility:<%# ShowHideComments(Container.DataItem("WOTrackingId"))%>' class="imgIconModal" id="imgshow" onmouseover='<%# "javascript:CollapseComments(" & Container.DataItem("WOTrackingID") & ", this.id)" %>' onmouseout='<%# "javascript:CollapseComments(" & Container.DataItem("WOTrackingID") & ", this.id)" %>'/></span>
                        
                        </td>
                        
                            </tr>
					</table>
					
					<div id='_divCommentsDetail<%# DataBinder.Eval(Container.DataItem,"WOTrackingID") %>' style='visibility:hidden;position:absolute;margin-left:400px; background-color:#FFFFE1; border:solid 1px #000000; width:200px; padding:10px; font-size:11px;'>
					 
											    <asp:Repeater  ID="DLDiscussion" Runat="server" DataSource='<%#GetDiscussions(Container.DataItem("WOTrackingId"))%>'  >
						                          <ItemTemplate> 
							                          <div style='visible:<%# iif( Trim(Container.DataItem("Comments")) = "" , "hidden", "visible") %>'>
						                             	   <div><%#IIf(DataBinder.Eval(Container.DataItem, "ContactClassID") <> OrderWorkLibrary.ApplicationSettings.RoleOWID, (IIf(DataBinder.Eval(Container.DataItem, "ContactClassID") = OrderWorkLibrary.ApplicationSettings.RoleClientID, "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - " & (IIf(Request("Viewer") = OrderWorkLibrary.ApplicationSettings.ViewerBuyer, "Your Remarks", "Buyer's Remarks")) & "  </span>", "<span class='discussionTextSupplier'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - " & (IIf(Request("Viewer") = OrderWorkLibrary.ApplicationSettings.ViewerSupplier, "Your Remarks", "Supplier Remarks")) & "  </span>")), "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - Admin's Remarks  </span>")%></div>
												           <div style="margin-top:0px; margin-bottom:10px;"><%# Container.DataItem("Comments") %></div>
												           
									                </div>
						                          </ItemTemplate>
					                            </asp:Repeater>									
					</div>
					 
				</ItemTemplate>
			</asp:DataList>
				<div style="height:10px; background-color:#ffffff; margin:0px;" ></div>	
					
			</td>
			
			 </tr>
			</table>
			  </asp:Panel>
			  
			  </td>
			</tr>
			
		</table>
		<div class="divClearBoth"></div>		
	    </div>
	    <div runat="server" id="divAddnlInfo" class="displayNone">
                <div style="float:left; margin-top:15px; margin-bottom:15px;width:100%;">
                     <table cellpadding="0" cellspacing="0" style="float: left;">						    					    
						    <tr height="20">
						        <td width="20">&nbsp;</td>
						        <td width="125" class="formLabelGreyBold" ><b>WO Type</b></td>
						        <td width="30">:&nbsp;</td>
						        <td><asp:Label runat="server" CssClass="formTxt" ID="lblCategory"></asp:Label></td>
						    </tr>						    
						    <tr height="20" runat="Server" id="trJobNumber">
						        <td width="20">&nbsp;</td>
						        <td width="125" class="formLabelGreyBold"><b>Job Number</b></td>
						        <td width="30">:&nbsp;</td>
						        <td><asp:Label runat="server" CssClass="formTxt" ID="lblJRSNumber"></asp:Label></td>
						    </tr>
						    <tr height="20">
						        <td width="20">&nbsp;</td>
						        <td width="125" class="formLabelGreyBold"><b>Dress Code</b></td>
						        <td width="30">:&nbsp;</td>
						        <td><asp:Label runat="server" CssClass="formTxt" ID="lblDressCode"></asp:Label></td>
						    </tr>
						    <tr height="20">
						        <td width="20">&nbsp;</td>
						        <td width="125" class="formLabelGreyBold"><b>Supply Parts</b></td>
						        <td width="30">:&nbsp;</td>
						        <td><asp:Label runat="server" CssClass="formTxt" ID="lblSupplyParts"></asp:Label></td>
						    </tr>					    
						    <tr height="20" runat="server" id="trSalesAgent">
						        <td width="20">&nbsp;</td>
						        <td width="125" class="formLabelGreyBold"><b>Sales Agent</b></td>
						        <td width="30">:&nbsp;</td>
						        <td><asp:Label runat="server" CssClass="formTxt" ID="lblSalesAgent"></asp:Label></td>
						    </tr>
					        <tr height="20" runat="server" id="trFreesatMake">
					            <td width="20">&nbsp;</td>
					            <td width="125" class="formLabelGreyBold"><b>Freesat Make/Model</b></td>
					            <td width="30">:&nbsp;</td>
					            <td><asp:Label runat="server" CssClass="formTxt" ID="lblFreesatMake"></asp:Label></td>
					        </tr>						    
					    </table>
				     </div>
                        <div  runat="server" id="divThermostatsQuestionsInfo" style="padding-top: 15px;color:Gray;padding-left:20px;">
                                    <div><b>Questionnaire</b></div>
                                    <div><asp:Label runat="server" CssClass="formTxt" ID="lblThermostatsQuestionsInfo"></asp:Label></div>
                                </div>
				     <div class="divClearBoth"></div>
				     </div>
				     <div class="divClearBoth"></div>
    		</div>

	 
	 </td>
	</tr>
	</table>  
	 <%--<table cellpadding="0" cellspacing="0" class="supplierBtmSummary" >
		 <tr>
			 <td>
			
				 <%@ Register TagPrefix="uc2" TagName="UCBuyerWOSummaryRepeat" Src="~/UserControls/UK/UCBuyerWOSummary.ascx" %>
					 <uc2:UCBuyerWOSummaryRepeat id="UCBuyerWOSummaryRepeat1" runat="server"></uc2:UCBuyerWOSummaryRepeat> 
			 </td>
		 </tr>
	 </table>  --%>             
  </ContentTemplate>
</asp:UpdatePanel>     
    </div>          
	<%--<div id="divBtmBandColored"></div>--%>
    <div class="clearBoth">
    </div>
</asp:Content>