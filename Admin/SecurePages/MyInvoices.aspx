<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MyInvoices.aspx.vb" Inherits="OrderWorkUK.MyInvoices" Title="OrderWork: Purchase Invoice" MasterPageFile ="~/MasterPage/MyOrderWork.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
<asp:UpdatePanel ID="UpdatePnlMyInvoices" runat="server"  RenderMode=Inline>
  <ContentTemplate>

  	<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="false" runat="server">
		<div class="errContainer">
			<div><img src="../OWLibrary/Images/Error_TopBand.gif" width="480" height="10"></div>
			<div class="errMdlBox">
				<table border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
					<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
				  </tr>
				</table>			
			</div>
			<div><img src="../OWLibrary/Images/Error_BtmBand.gif" width="480" height="10"></div>				
		</div>
	</asp:Panel>
	<div id="divClearBoth"></div>
</ContentTemplate>
</asp:UpdatePanel>


	<div id="divMiddleBand" class="divContainerStyle">
		<div><%@ Register TagPrefix="uc2" TagName="UCMyInvoices" Src="~/UserControls/UK/UCMyInvoices.ascx" %>
	<uc2:UCMyInvoices id="UCMyInvoices1" runat="server" AdviceType="Purchase"></uc2:UCMyInvoices></div>
	</div>
	<div id="div1" class="clearBoth"></div>
</asp:content>
