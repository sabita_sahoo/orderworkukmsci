<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWContentOnly.Master" CodeBehind="ActivateAccount.aspx.vb" Inherits="OrderWorkUK.ActivateAccount" 
    title="OrderWork - Services Reinvented - Account Activation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
<div id="divBodyInner" style="padding-bottom:3px; padding-top:0px; ">
 <div id="HmMain" style="float:left; height:540px; padding-top:23px; background-image:url(OWLibrary/Images/Bg_GreyBg.jpg); background-position:bottom right; background-color:#FCFCFC; background-repeat:no-repeat;  ">
  <div id="divLoginMain">
    <div id="divLoginLogo"><img  src="OWLibrary/Images/myorderwork.jpg" style=" margin-top:40px; margin-left:28px;" alt="myOrderWork Client Portal" title="myOrderWork Client Portal" /> </div>
     <div id="divLogin">
       <div id="divLoginRight">
       <img  src="OWLibrary/Images/LoginUpperband.jpg" alt="" />
       <div id="divRightImage" style="line-height:15px;">
           <div class="ActAccountOuter" >
   		       <%@ Register TagPrefix="uc1" TagName="UCActivateAccount" Src="~/UserControls/UK/UCMSActivateAccountUK.ascx" %>
    	       <uc1:UCActivateAccount id="UCActivateAccount1" runat="server"> </uc1:UCActivateAccount>	   
    	   </div>
	   </div>
	   </div>
     </div>
    </div>
 </div>
</div>
</asp:Content>
