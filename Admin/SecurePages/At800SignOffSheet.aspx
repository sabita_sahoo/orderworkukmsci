﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWIT.Master" CodeBehind="At800SignOffSheet.aspx.vb" Inherits="OrderWorkUK.At800SignOffSheet"  Title="OrderWork: AT800 Sign-Off Sheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
<style>
      td {
    border: medium none;
    margin: 0;
    padding: 0;
}
table{border: medium none;
    margin: 0;
    padding: 0;}
    .clsButtonGreyDetailsAccept {
    background-image: url("./OWLibrary/Images/OrderWorkUkRedesign/Accept_Btn.png");
    color: Black;
    cursor: pointer;
    font-size: 11px;
    height: 15px;
    margin-left: 45px;
    padding-bottom: 5px;
    padding-left: 10px;
    padding-top: 5px;
    text-decoration: none;
    width: 160px;
}
.divContainerStyle {
    border: 1px solid #E2E2E2;
    width: 910px;
    margin-left:10px;
    margin-bottom:10px;
}

.paddingBottom10 {
    padding-bottom: 10px;
}
.clsGreyBandStyleFund {
    background-image: url("./OWLibrary/Images/OrderWorkUkRedesign/Title_bg.gif");
    color: Black;
    height: 24px;
    padding-left: 30px;
    padding-top: 8px;
    width: 880px;
}

    </style>
<script type="text/javascript">
    function SearchJob() {
        var txtWorkOrder = document.getElementById("ctl00_ContentHolder_SignOffSheetAT800_wzd_txtWorkOrder").value;
        var txtPostcode = document.getElementById("ctl00_ContentHolder_SignOffSheetAT800_wzd_txtPostCode").value;
        //OrderWorkLibrary.DBWorkOrder.SearchWorkOrder(txtWorkOrder.Text, txtPostCode.Text, Session("PortalCompanyId"));
        PageMethods.SearchWorkOrder(txtWorkOrder, txtPostcode, SuccessFunction, FailedFunction);

    }
</script>
<%@ Register TagPrefix="uc1" TagName="UCSignOffSheetAT800" Src="~/UserControls/UK/UCSignOffSheetAT800.ascx" %>
<uc1:UCSignOffSheetAT800 id="SignOffSheetAT800" runat="server"></uc1:UCSignOffSheetAT800>
</asp:Content>