<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWContentOnly.Master" CodeBehind="OWForgotPassword.aspx.vb" Inherits="OrderWorkUK.OWForgotPassword" 
    title="OrderWork - Services Reinvented - Forgot Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
<div id="divLoginBodyInner">
<div class="displaynone">&nbsp;&nbsp;</div>
 <div id="HmMain" style="float:left; height:350px; padding-top:15px;margin-left:20px; background-image:url(OWLibrary/Images/Bg_GreyBg.jpg); background-position:bottom right; background-color:#FCFCFC; background-repeat:no-repeat;  ">
  <div id="divForgotPassMain">
  <div id="divLoginLogo"><img  src="OWLibrary/Images/myorderwork.jpg" style=" margin-top:0px; margin-left:0px;" alt="myOrderWork Client Portal" title="myOrderWork Client Portal" /> </div>
     <div class="displaynone">&nbsp;&nbsp;</div>
     <div id="divForgotPassword">   
     <div id="divForgotRight" >
       <img  src="OWLibrary/Images/LoginTopband.jpg" valign="top" />
       <div id="divRightImageFP">
	   <div id="divLoginInner">
	   <div class="displaynone">&nbsp;&nbsp;</div> 
	     <%@ Register TagPrefix="uc1" TagName="ForgotPassword"  Src="~/UserControls/UK/UCOWForgotPasswordUK.ascx" %>
          <uc1:ForgotPassword id="UCForgotPassword" runat="server"></uc1:ForgotPassword>
	   </div>
	   </div>
	   </div>
     </div>
    </div>
  </div>
</div>
</asp:Content>
