<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SpecialistsForm.aspx.vb" Title="OrderWork: Users" Inherits="OrderWorkUK.SpecialistsForm" MasterPageFile="~/MasterPage/MyOrderWork.Master" ValidateRequest="true" %>
<%@ MasterType virtualpath="~/MasterPage/OrderWork.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
   <script language="javascript" type="text/javascript" src="OWLibrary/JS/json2.js"></script>
<script language="javascript" type="text/javascript" src="OWLibrary/JS/jquery.js"></script>
<script language="javascript" type="text/javascript" src="OWLibrary/JS/JQuery.jsonSuggestCustom.js"></script>
<script language="javascript" src="OWLibrary/JS/Scripts.js"  type="text/javascript"></script>
 	<div>
	<%@ Register TagPrefix="uc1" TagName="UCSpecialistsForm" Src="~/UserControls/UK/UCSpecialistsForm.ascx" %>
	<uc1:UCSpecialistsForm id="UCSpecialistsForm1" runat="server"></uc1:UCSpecialistsForm>
    <%@ Register TagPrefix="uc2" TagName="UCAddSpecialistsForm" Src="~/UserControls/UK/UCMSSpecialistsFormUK.ascx" %>
	<uc2:UCAddSpecialistsForm id="UCSpecialistsForm2" runat="server"></uc2:UCAddSpecialistsForm>
    </div>
	<div class="clearBoth"></div>
</asp:content>
