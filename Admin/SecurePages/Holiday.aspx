
<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OrderWork.Master" CodeBehind="Holiday.aspx.vb" Inherits="OrderWorkUK.Holiday" 
    title="OrderWork : Holiday" %>
    <%@ Register TagPrefix="uc1" TagName="GridPager" Src="~/UserControls/UK/GridPager.ascx"%>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
    
<asp:Content ID="Content2" ContentPlaceHolderID="ContentHolder" runat="server">
<style type="text/css">
.gridHdr
{
	background-color:#993366;
	font-family:Verdana;
	font-size: 11px;
	font-weight: bold;
	color: #555354;
	text-decoration: none;
	height:28px;
	vertical-align:middle;
	text-align:left;
}
.gridRowText {

/*border-left:none 0px;
border-right:none 0px;*/
/*font-size:12px;*/

}
.gridRow {
border-left:none 0px;
border-right:none 0px;

}
#divPagerBtm{
    float:left; 
    clear:right; 
    width:300px; 
    margin-left:105px;
}
*html #divPagerBtm{
    float:left; 
    clear:right; 
    width:300px; 
    margin-left:50px;
}
#AlertDiv{
left: 40%; top: 40%;
position: absolute; width: 200px;
padding: 12px; 
border: #000000 1px solid;
background-color: white; 
text-align: left;
visibility: hidden;
z-index: 99;
}
#AlertButtons{
position: absolute; right: 5%; bottom: 5%;
}
/*Styles for Modal PopUp = START*/
#divModalParent {
    margin: auto;
    text-align: left;
}
.modalBackground 
 {
    background-color:Silver;
    filter:alpha(opacity=30) !important;
    opacity:0.3 !important;
    position:absolute !important;
    z-index:1 !important;
 }
.pnlConfirm {	
	background-color:#FFEBDE;
	font-family:Verdana;
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	line-height: 18px;
	border: 1px solid #CECBCE;
	padding: 10px;
	position:absolute !important;
	margin-left:650;
	margin-top:350;
}
.cursorPointer
{
	cursor:pointer;
}
.bottonTopGrey {
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-TR.gif) no-repeat right top;
	height:4px;
}
.bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
}
*+html .bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
	margin-top:-3px;
}

#divButton {
background-color:#999;
width:56px;
height:19px;
margin:10px 0 0;
padding:0;
}
img.btnCorner {
   width: 4px;
   height: 4px;
   border: none;
   display: block !important;
}
.buttonText {
font-family:Verdana;
font-size:11px;
color:#FFF;
text-decoration:none;
padding-left:11px;
padding-right:8px;
font-style:normal;
font-weight:400;
font-variant:normal;
line-height:11px;
}

.buttonText:hover {
color:#3F3F3F;
text-decoration:none;
}
</style>

	<div class="mainTableBorderr paymentHeading" id="divPaymentMethod"><strong>Holiday</strong></div>	
	 <asp:UpdatePanel ID="updatePnlListMessages" runat="server"  RenderMode="Inline">
  <ContentTemplate>	
	<div id="divMiddleBand" style="padding-left:0px; clear:none;">
	<div id="divMediumBtnImage" style="margin-left:10px;"><a id="btnAddHoliday" onclick="btnAddHoliday_Click" runat=server class="txtListing" tabindex="11" ><strong><img src="~/OWLibrary/Images/Icons/Icon-SpecialistSmall.gif" runat="server" id="imgbtnSpecialist" width="12" height="16" hspace="5" vspace="0" align="absMiddle"  border="0">&nbsp;Add Holiday</strong></a></div>
<asp:Panel ID="pnlHolidayListing" runat="server">
	<div style="width:650px;margin-top:2px;margin-bottom:2px; float:left; clear:right;">
	<div id="tdTotalRecs" style="width:100px; padding-top:10px; padding-left:5px; text-align:left; float:left; clear:right; " runat="server" ></div>
<div id="divPagerTop" style="float:left; clear:right; width:300px;"><uc1:GridPager ID="GridPagerTop" runat="server"></uc1:GridPager></div>
<div id="Div1" style="width:200px; padding-top:10px; text-align:right; font-size:11px;float:right;clear:right; " runat="server" >View Records													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" CssClass="formField marginLR5" ID="ddlPageSize" runat="server" AutoPostBack="true">
													<asp:ListItem>10</asp:ListItem>
													<asp:ListItem>15</asp:ListItem>
													<asp:ListItem>20</asp:ListItem>
													<asp:ListItem>25</asp:ListItem>
													</asp:DropDownList> per page
											  </div>
</div>
<div style="float:left;">
	    <asp:DataList runat="server" ID="dlHolidayList" Width="690px">
	    <HeaderTemplate>
	    <table width="695px" cellspacing="0" border="0">
	        <tr class="gridHdr" style="height:15px;" >
	        <td class="txtGrid" width="105px" style="padding-left:0px;"><asp:LinkButton id="lnkDate" runat="server"   CommandName="HolidayDate" style=" txtGrid;" >Holiday Date</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/WhiteArrow-Up.gif" id="imgHolidayDate" runat="server" Visible="false"/></td>
	        <td class="txtGrid" width="400px" style="padding-left:0px;"><asp:LinkButton id="lnkSubject" runat="server"  CommandName="HolidayTxt" style=" txtGrid;" >Holiday Caption</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/WhiteArrow-Up.gif" id="imgHolidayCaption" runat="server" Visible="false"  /></td>
	        
	        </tr>
	    </table>
	    </HeaderTemplate>
	    <ItemTemplate>
	        <table width="695px" cellspacing="0" border="0" id="tblHoliday" runat="server" style="margin-top:-2px;" >
	        <tr class="gridRow" id="trdlRow">
	        <td class="gridRowText colorBlueListing" width="103px" style="height:15px;" ><%#Container.DataItem("HolidayDate")%></td>
	        <td class="gridRowText" width="350px" style="height:15px;"><%#Container.DataItem("HolidayTxt")%></td>
	         <td class="gridRowText" width="50px" style="height:15px;">
	         <asp:LinkButton ID="lnkbtnEditHoliday" CommandName="EditHoliday" CommandArgument='<%#Container.DataItem("DateID")& "," & Container.DataItem("HolidayDate") & "," & Container.DataItem("HolidayTxt") & "," & Container.DataItem("IsDeleted") %>' runat="server">
              <img id="imgEditHoliday" runat="server" src="~/OWLibrary/Images/Icons/Icon-Pencil.gif"
               title="Edit Holiday" border="0" class="cursorPointer" />
             </asp:LinkButton>
	         
	         </td>
	        </tr>
	    </table>	    
	    </ItemTemplate>
	    </asp:DataList>
	    

	    
	    
	    
	    
	    
	    
</div>	    
<div style="width:650px;margin-top:2px;margin-bottom:2px;" >
<div id="divPagerBtm" ><uc1:GridPager id="GridPagerBtm" runat="server"></uc1:GridPager></div>
<div id="Div2" style="width:200px; padding-right:0px; padding-top:5px; text-align:right; font-size:11px;float:right; " runat="server" >View Records													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSizeBtm_SelectedIndexChanged" CssClass="formField marginLR5" ID="ddlPageSizeBtm" runat="server" AutoPostBack="true">
													<asp:ListItem>10</asp:ListItem>
													<asp:ListItem>15</asp:ListItem>
													<asp:ListItem>20</asp:ListItem>
													<asp:ListItem>25</asp:ListItem>
													</asp:DropDownList> per page
											  </div>
</div>
</asp:Panel>
<asp:Panel ID="pnlnoRecords" runat="server">
    <div style="text-align:center; margin-top:20px;">No Records Available</div>
</asp:Panel>
	</div>
	
	<div id="divClearBoth"></div>
<cc1:ModalPopupExtender ID="mdlHoliday" runat="server" TargetControlID="btnTemp"
    PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground"
    Drag="False" DropShadow="False">
</cc1:ModalPopupExtender>
<asp:Panel runat="server" ID="pnlConfirm" Width="200px" CssClass="pnlConfirm" Style="display: none;">
    <asp:Label ID="lblError" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
    <div id="divModalParent">
        <span style="font-family: Tahoma; font-size: 14px;"><b>Holiday</b></span> 
        <br />
        <br />           
        <asp:TextBox runat="server" ID="txtHolidayDate" Height="15" Width="120" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
          <img alt="Click to Select" src="../OWLibrary/Images/calendar.gif" id="btnHolidayDate" style="cursor:pointer; vertical-align:middle;" />
		 <asp:RegularExpressionValidator ID="regHolidayDate"  ControlToValidate="txtHolidayDate" ErrorMessage="Holiday Date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB" ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"  runat="server">*</asp:RegularExpressionValidator>									
		 <cc1:CalendarExtender CssClass="dddddd" Format="dd/MM/yyyy" PopupButtonID="btnHolidayDate" TargetControlID="txtHolidayDate" ID="calHolidayDate" runat="server" Enabled="true">
		 </cc1:CalendarExtender>
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtHolidayDate"
            WatermarkText="Select Holiday Date" WatermarkCssClass="bodyTextGreyLeftAligned">
        </cc1:TextBoxWatermarkExtender>
        <br />
        <br />
        <asp:TextBox runat="server" ID="txtHolidayCaption" Height="15" Width="120" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtHolidayCaption"
            WatermarkText="Enter Holiday Caption" WatermarkCssClass="bodyTextGreyLeftAligned">
        </cc1:TextBoxWatermarkExtender>
        <br />
        <br />
        <asp:CheckBox ID="chkbxIsActive" runat="server" Text="Is Active" CssClass="bodyTextGreyLeftAligned" />
        <br />
        <br />        
        <div id="divButton" class="divButton" style="width: 85px; float: left;">
            <div class="bottonTopGrey">
                <img src="../OWLibrary/Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0"
                    width="4" height="4" class="btnCorner" style="display: none" /></div>
            <a class="buttonText cursorPointer" onclick='javascript:validate_required()'
                id="ancSubmit">Submit</a>
            <div class="bottonBottomGrey">
                <img src="../OWLibrary/Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0"
                    width="4" height="4" class="btnCorner" style="display: none" /></div>
        </div>
        <div id="divButton" class="divButton" style="width: 85px; float: left; margin-left: 20px;">
            <div class="bottonTopGrey">
                <img src="../OWLibrary/Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0"
                    width="4" height="4" class="btnCorner" style="display: none" /></div>
            <a class="buttonText cursorPointer" onclick='javascript:clearText("ctl00_ContentHolder_UCSupplierWOsListing1_txtNote")'
                runat="server" id="ancCancel">Cancel</a>
            <div class="bottonBottomGrey">
                <img src="../OWLibrary/Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0"
                    width="4" height="4" class="btnCorner" style="display: none" /></div>
        </div>
    </div>
</asp:Panel>
<script>
function validate_required(field,alerttxt,btnID)
{
    var text = document.getElementById("ctl00_ContentHolder_txtHolidayDate").value;
    
    if (text != "Select Holiday Date")
    {
         while(text.indexOf(" ") != -1 )
        {
            text = text.replace(" ","");
        }
    }
      
      if (text==null||text==""||text=="Select Holiday Date")
      {
        alert("Select Holiday Date");
      }
      else
      {   
        text = document.getElementById("ctl00_ContentHolder_txtHolidayCaption").value;    
         if (text != "Enter Holiday Caption")
         {
             while(text.indexOf(" ") != -1 )
            {
                text = text.replace(" ","");
            }
         }  
        if (text==null||text==""||text=="Enter Holiday Caption")
        {
            alert("Enter Holiday Caption");
        }
        else
        {   
          document.getElementById("ctl00_ContentHolder_hdnButton").click();
        }                         
        
      }
}
function clearText()
{
    document.getElementById("ctl00_ContentHolder_txtHolidayDate").value = "";
    document.getElementById("ctl00_ContentHolder_txtHolidayCaption").value = "";
}
</script>
<asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click" Height="0" Width="0"
    BorderWidth="0" Style="visibility: hidden;" />
<asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" Style="visibility: hidden;" />
	
	
	</ContentTemplate>
</asp:UpdatePanel> 
	<div class="clearBoth"></div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
