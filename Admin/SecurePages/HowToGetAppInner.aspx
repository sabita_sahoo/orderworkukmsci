<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HowToGetAppInner.aspx.vb" Title="OrderWork: How To Get Approved" Inherits="OrderWorkUK.HowToGetAppInner" MasterPageFile ="~/MasterPage/OrderWork.Master" %>

<asp:Content ContentPlaceHolderID="ContentHolder" runat="Server">
<script language="javascript" src="OWLibrary/JS/Scripts.js"  type="text/javascript"></script>

	<div id="divTopBand" ></div>
	<div class="mainTableBorderr paymentHeading" id="divPaymentMethod"><strong>How To Get Approved</strong></div>
	<div id="divMiddleBand">
	<div id="divMainTab">  
	<DIV class="paddedBox" id="divProfile">
	<div id="divMain" style="padding-left:15px;padding-right:20px ">
	
	<p align="justify" class="bodyTxt">
		  
		  <strong>We can only send work orders to approved Suppliers</strong>. Our registration team can help you complete your second stage of registration so that you're ready to receive work via our portal.<br>
		 <br> <div id="divGettingStarted">
		         <div id="divDownload"><a href="../Downloads/GuideToGettingStartedWithOrderWork.pdf" target="_blank" class="DownloadRedColor">Click Here To Download</a></div>
		           <div id="divServicePartners">&nbsp;Suppliers</div>
						 <div id="divGettingStartedBottomContent">
							  A Guide to <br>Getting Started with OrderWork
						</div>
		      </div> 
		<strong><span style="font-size:14px ">There are 2 stages of registration:</span></strong><br>
		  <br>
		
		  <strong><span style="font-size:14px ">1.	Initial registration on website & link verification</span><br>
		  <span style="font-size:14px ">2.	Supplying references & insurance</span></strong><br><br><br>
		  
			
		  <p class="boldUnderline12">Stage 1: Initial registration on website & link verification</p>
		  IT firms or self-employed IT engineers who wish to use us as an extra revenue channel can sign up on our website. You will then be sent an email with a <br>verification link. You must ensure you click on this link so that we know your email address is valid.<br>
		  <br>
		 <br> <p class="boldUnderline12">Stage 2: Supplying references & insurance </p>
		  Once we have verified your email address, you should proceed to the second stage of registration.<br>
		  <br>
		  (1)	Log into your account<br>
		  <br>
		  (2)	Click on account profile<br>
		  <br>
		  (3)	Insert <strong>two reference contacts</strong> - this is simply the name, contact number and company name of two previous or current clients. We will give these contacts a call, just to verify the quality of your work.<br>
		  <br>
		  (4)	Upload your <span style='cursor:hand;' class="blueBold" onMouseOut=javascript:showHideRatingDetails('divPublicLiability','hidden') onMouseOver=javascript:showHideRatingDetails('divPublicLiability','visible')><strong>Public Liability Insurance</strong></span>
		  <div style='visibility:hidden;position:absolute;margin-Top:20px; margin-left:-230px; ' id=divPublicLiability>
			<table width="550" height="100" bgcolor="#D6D7D6" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;" >
			
			<tr>
						<td  align="left" class="innerTables"> All Suppliers, even self-employed engineers must provide this.</td>
			</tr>
			
						<tr>
						 <td  align="left" class="innerTables">If you do not have Public Liability Insurance, it is relatively inexpensive and can be obtained by contacting any insurance broker. This expense can be set against your tax return at the end of the financial year.</td>
						</tr>
						
			<tr>
						 <td  align="left" class="innerTables">We advise self-employed engineers to get a quote online at <span class="blue">https://business.hiscox.co.uk/</span> whilst for larger companies requiring more complex insurance premiums we recommend <span class="blue">http://www.noyceinsurance.co.uk</span> </td>
					</tr>
			
			
			</table>
												
			</div>
		  
		   <strong>or/and</strong> <span style='cursor:hand;' class="blueBold" onMouseOut=javascript:showHideRatingDetails('divEmpLiability','hidden') onMouseOver=javascript:showHideRatingDetails('divEmpLiability','visible')><strong>Employer's Liability Insurance</strong></span>
           <div style='visibility:hidden;position:absolute;margin-Top:20px; margin-left:-230px; ' id=divEmpLiability>
			<table width="450" height="100" bgcolor="#D6D7D6" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;" >
			
			<tr>
						<td  align='left' class="innerTables" >If you employ a team of engineers, you must also provide us with a copy of your Employer's Liability Insurance.</td>
			</tr>
			<tr>
						 <td  align="left" class="innerTables">Self-employed engineers do not need Employer's Liability Insurance</td>
			</tr>
			</table>
											
			</div>
		  <br>
          <br><br>
          <p align="center">If you are having difficulty uploading your insurance, you can either fax it to us on 
              <span class="blueNo">0845 900 1302</span></span> or scan and email it to <a href="mailto:info@orderwork.co.uk" class="blueBold " >info@orderwork.co.uk</a></p>
          <div align="left"><br>
            Once we have your references and insurance details, our registration team is able to make the necessary checks and hopefully approve you a Supplier with OrderWork.<br>
            <br>
            Once you become approved you will be able to accept any work orders in your local area that we send out to you.<br>
            <br>
            Once you start completing work with OrderWork you will be rated one ach job. It is these ratings that ensure that you will continue to receive work orders from us, so maintaining your ratings is crucial.<br>
            <br>
            </p>
            <br>
            If you have any further questions on how to register, or how our portal works, please do not hesitate to call our team on <span class="blueNo">0203 053 0343</span><br><br><br>		
		      </div>				
	
	 </div> 
   
  </div>
  
</div>
  
</div>
  
	<div id="divBtmBand" ></div>
	
</asp:content>

