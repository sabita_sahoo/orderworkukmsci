﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SPTermsConditions.aspx.vb" Title="OrderWork: Privacy Policy" Inherits="OrderWorkUK.SPTermsConditions" MasterPageFile ="~/MasterPage/MyOrderWork.Master" %>


<asp:Content ContentPlaceHolderID="ContentHolder" runat="Server">
<script language="javascript" src="OWLibrary/JS/Scripts.js"  type="text/javascript"></script>
<style>
<!--
.bodytxtRed {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 15px;
	font-weight: bold;
	color: #C54043;
	text-decoration: none;
}
.blueBold{font-size: 12px; font-weight: bold;color:#1882B5;}
.blueBold:hover {font-size: 12px; font-weight: bold;color:#C54043;}
.blueText{font-size: 12px; font-weight: bold;color:#1882B5; text-decoration:none;}
.blueText:hover {font-size: 12px; font-weight: bold;color:#C54043; text-decoration:none;}

-->
</style>
	<div id="divTopBand" class="divContainerStyle floatLeft" >
    <div style="width:1012px;padding-left:10px;"  class="clsGreyBandStyleFund marginBottom10">
                             <strong >OrderWork Terms And Conditions </strong>
                             </div>
	
	<div id="divMiddleBand">
	<div id="divMainTab">  
	<DIV class="paddedBox" id="divProfile">
	<div id="divMain" style="padding-left:15px;padding-right:20px ">
	
	<p align="justify" class="bodyTxt">
		  
		<p class="marginT24B10"><strong>AGREEMENT FOR THE USE OF ORDERWORK SITE</strong></p>  
		        
              <p><strong>Welcome to the User Agreement for OrderWork.co.uk and My.Orderwork.co.uk </strong></p>
              <a href="~/Downloads/UKTermsConditions.pdf" runat="server" id="lnlDownLoad" target="_blank" class="blueText"><strong><img src="../OWLibrary/Images/Icons/pdf.gif" width="16" height="20" hspace="5" border="0" align="absmiddle">Download PDF</strong></a></p>
              
              <p class="bodytxtRed">l. The User Agreement (&quot;User Agreement&quot;)</p>
              <p align="justify"> The services available at <a href="http://www.orderwork.co.uk" target="_blank" class="blueBold">http://www.orderwork.co.uk</a> and <a href="https://my.orderwork.co.uk" target="_blank" class="blueBold">https://my.orderwork.co.uk</a> are provided by OrderWork Ltd (Registered No: 05662167), of Montpelier Chambers, 61-63 High Street South, Dunstable, Bedfordshire LU6 3SF. (&quot;OrderWork&quot;, &quot;we&quot;, &quot;us&quot; or &quot;our&quot;). </p>
              <p align="justify"><strong>You are a user (&quot;User&quot;) of our services. This User Agreement describes the terms and conditions applicable to your use of our services (the &ldquo;Services&rdquo;) available under the domain and sub-domains at </strong><a href="http://www.orderwork.co.uk" target="_blank" class="blueBold">http://www.orderwork.co.uk</a> and <a href="https://my.orderwork.co.uk" target="_blank" class="blueBold">https://my.orderwork.co.uk</a> <strong>(the &quot;Site&quot;). If you do not agree to be bound by this User Agreement, you may not use or access our services. We provide services to people who need Services work carried out (&ldquo;Buyers&rdquo;) and those who have resources available to carry out work (&ldquo;Suppliers&rdquo;). A Buyer may enable access to these Services for its “End Users” where the End User can enter single order detail. The acceptance of this Agreement by ticking the box means that you agree to be legally bound by all of the terms and conditions in this Agreement, as related to Buyers, End Users and Suppliers, as the case may be.  </strong></p>
              <p align="justify"> You must read, agree with and accept all of the terms and conditions contained in this User Agreement, which includes those terms and conditions expressly set out below and those incorporated by reference, before you may become a user of the Site.</p>
              <p>We may amend this User Agreement at any time by giving you notice ("Notice") by either e-mail or by posting the amended User Agreement on the Site. Any amended User Agreement will govern new user registrations from the date that it is posted on the Site. Existing users will be bound by the amended User Agreement after the expiry of 14 days following the date of a Notice. No other amendment to this User Agreement will be effective. Please note that different notice provisions apply to our Fees and Credit policy as set out below.  </p>
              
              <p align="justify"><strong> Information on the Site is restricted for the sole purpose of obtaining the Services and as described in our Privacy Policy. Any other use, commercial or personal, is prohibited.  </strong></p>
              
              <p class="bodytxtRed"> II. General Terms and Conditions (applies to all Users) </p>
              <p><strong> A. Eligibility to use our services</strong></p>
              <p align="justify"> Our Services are available only to corporate or other entities, or individuals who can form legally binding contracts under applicable law. Without limiting the foregoing, our Services are not available to minors, to anyone under 18 years of age, or to temporarily or indefinitely suspended OrderWork members. If you do not qualify, please do not use our Services. Further, your OrderWork account (including feedback) and User ID may not be transferred or sold to another party without our prior written consent. If you are registering as a business entity, you represent that you have the authority to bind the entity to this User Agreement.   If you are entering our Services as an End User, you are entering information to request a service as provided by a Buyer – your personal information is used only for the service requested as detailed in our Privacy Policy.   </p>
              <p><strong> B. Services and Fees</strong></p>
              <p align="justify">We provide a range of Services that include the ability for Buyers to list work on our website for Suppliers to bid on or accept, and for OrderWork to carry out work on behalf of Buyers using the OrderWork network of Suppliers. Each of these Services has applicable fees (the “Fees”) and we may change our Fees and credit policy from time to time. Our changes to the policy are effective immediately upon posting on the Site. We may in our sole discretion change some or all of our Services at any time and the Fees associated with them. In the event that we introduce a new service, the Fees for that service will be effective at the launch of the service.   <br>
                  <br>
  As an End User, fees for the Service are paid to the Buyer.  We may take additional fee for additional services via our call centre, we do not take payments at the location where the service is delivered.   </p>
                   All Fees are quoted in GB Pounds Sterling. You are responsible for paying all Fees associated with using our Services and Site and all applicable taxes, such as VAT, if any. </p>

              <p align="justify"><strong>C. Information control</strong></p>
              <p align="justify"> We do not control the information provided by other Users that is made available through our Site. You may find their information to be harmful, inaccurate or deceptive. Please use caution, common sense, and safe business practices when using our Site. <br>
                  <br>
                  <strong>D. Third-party links and content </strong></p>
              <p align="justify"> Links contained on the Site may cause a User to leave the Site. Any linked websites are not under the control of OrderWork and OrderWork is not responsible for the contents of any linked website or information contained in a linked website, or any changes or updates to such website. OrderWork provides these links from the Site only as a convenience, and the inclusion of any link does not imply any endorsement of the website or any of its contents. All Users' use of materials and information in third-party websites is at their own risk. </p>
              <p><strong> E. Your information</strong></p>
              <ol>
                <li>
                  <div align="justify"><strong><i>Definition</i></strong><br>
&quot;Your Content&quot; means any information you provide to us or make available to other Users which is designated as being publicly available content for use on the Site. You are solely responsible for Your Content, and we act as a passive conduit for your online distribution and publication of Your Content.</div>
                </li><br />
                <li>
                  <div align="justify"><strong><i> Restricted activities</i></strong><br>
      Your Content, any information you provide to other Users and your activities on the Site must not: 
        
         <p> a) be false, inaccurate or misleading <br />
         b) be fraudulent or relate to the provision of services for which you are not qualified  <br />
         c) infringe any third party's copyright, patent, trademark, trade secret or other proprietary rights or rights of publicity or privacy  <br />
         d) be in breach of English law or any other applicable law or regulation (including, but not limited to, those governing consumer protection, unfair competition, antidiscrimination or false advertising)  <br />
         e) be defamatory, libellous, unlawfully threatening or unlawfully harassing; contain any viruses, trojan horses, worms, time bombs, cancelbots, Easter eggs or other computer programming routines that may damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or personal information  <br />
         f) link directly or indirectly to or include descriptions of goods or services that are prohibited under this User Agreement  <br />
         g) or consummate any transaction that was initiated using our Site that, by paying to us any fees could cause us to break any applicable law or regulation.  <br />
             
                  </div>
                </li>
                <li>
                  <div align="justify"><strong><i> Licence </i></strong><br>
      To enable OrderWork to use Your Content, you grant us a non-exclusive, world-wide, perpetual, irrevocable, fully paid-up, sub-licensable (through multiple tiers) licence to exercise the copyright, trade mark, publicity, and database rights you have in Your Content, in any media now known or not currently known. You also waive all moral rights you have in Your Content to the fullest extent permitted by law.  </div> <br />
                </li>
                <li>
                  <div align="justify"><strong><i>Data</i></strong><br>
      All data derived or collected by OrderWork from our Site, including but not limited to statistical information, IP addresses, or other relevant data shall belong to OrderWork and you agree that OrderWork shall have the right to use such data as it determines in its sole discretion. This includes the right to display industry affiliations, certifications and / or preferred provider network logos, where appropriate on the Site or in User profiles.  Our use of personal data is restricted to the use described in our Privacy Policy.  </div>
                </li>
              </ol>
              <p align="justify"><strong> F. Access and interference</strong></p>
              <div align="justify">
                <ol type="a">
                  <li> Our Site contains robot exclusion headers and you agree that you will not use any robot, spider, other automatic device, or manual process to monitor or copy our Web pages or the content contained herein without our prior written consent.  </li>
                  <li> You agree that you will not use any device, software or routine to bypass our robot exclusion headers, or to interfere or attempt to interfere with the proper working of the Site or any activities conducted on our Site.  </li>
                  <li> You acknowledge that our Site and our Services are protected by our intellectual property rights. You will not take any action which might infringe those intellectual property rights.  </li>
                  <li> You agree to receive all email/electronic transmissions sent by OrderWork in connection with or relating to the Site or our Services. You also agree that OrderWork may notify you of available Work Order(s), or Work Order updates by telephone, including automated message(s), using the telephone number(s) provided by you in your registration details.  </li>
                  <li> Much of the information on our site is updated on a real-time basis and is proprietary or is licensed to OrderWork by our Users or third parties. You agree that you will not copy, reproduce, alter, modify, create derivative works, or publicly display any content (except for Your Information) from our Site without the prior written consent of OrderWork or the appropriate third party.</li>
                </ol>
              </div>
              <ol>
              </ol>
              <p><strong>G. Breach</strong></p>
              <p align="justify">Without limiting other remedies, we may limit your activity on the Site, immediately remove you from the Site, warn our community of your actions, issue a warning, temporarily suspend, indefinitely suspend or terminate your membership and refuse to provide our Services to you if: </p>
              <div align="justify">
                <ol type="a">
                  <li> you breach this Agreement or the documents it incorporates by reference </li>
                  <li> we are unable to verify or authenticate any information you provide to us </li>
                  <li> or we believe that your actions may cause any loss or liability for you, our Users or us.  </li>
                </ol>
              </div>
            
              <p align="justify"><strong> H. No Warranty</strong></p>
              <p align="justify">We (and our parent, subsidiaries, affiliates, officers, directors, agents and employees) do not guarantee continuous, uninterrupted or secure access to our services, and operation of the Site may be interfered with by numerous factors outside of our control. Our Site and our Services are provided "as is" and as and when available, and to the extent permissible by law we exclude all implied warranties, conditions or other terms, whether implied by statute or otherwise, including without limitation any terms as to skill and care or timeliness of performance. </p>
              <p align="justify"> <strong>I. Force Majeure</strong></p>
              <p align="justify"> If the delivery of our Services is delayed, prevented or otherwise made impracticable by reason of any acts of God, floods, earthquakes, or other natural catastrophes; national emergencies, strikes, lockouts or other labour difficulties; computer "hacking" attack or computer virus; any law, order, regulation or other action of any governing authority; or any other cause beyond OrderWork's reasonable control, OrderWork shall be excused from such delivery to the extent that its delay is prevented by such cause. </p>
              <p><strong> J. OrderWork limit of Liability</strong></p>
              <p align="justify">Nothing in this User Agreement shall limit or exclude our liability for fraudulent misrepresentation, or for death or personal injury resulting from our negligence or the negligence of our, agents or employees. Subject to the foregoing, we (including our parent, subsidiaries, affiliates, officers, directors, agents and employees) will not be liable for any economic losses (including, without limitation, loss of revenues, profits, contracts, business or anticipated savings), any loss of goodwill or reputation, or any special, indirect or consequential damages arising out of or in connection with this User Agreement.</p>
              <p>We (including our parent, subsidiaries, affiliates, officers, directors, agents and employees) also have no liability of any sort (including liability for negligence) for the acts or omissions of other providers of telecommunications services or for faults in or failures of their networks and equipment.  </p>
              <p>Our liability to you or any third party, and the liability of our parent, subsidiaries, affiliates, officers, directors, agents and employees, in any circumstance is limited to the fees, net of any irrecoverable payments we have made to our Suppliers, that you have paid to us in the one month period prior to the action giving rise to the liability.  </p>
              <p align="justify"><strong> K. Supplier Limit of Liability </strong></p>
              <p align="justify">The Supplier's liability to a Buyer or OrderWork for death or injury resulting from his own negligence or that of his employees, agents or sub-contractors shall not be limited.  </p>
              <p>The Supplier's entire liability to a Buyer or OrderWork in respect of any breach of his contractual obligations, any breach of warranty, any representation, statement or tortuous act or omission including negligence arising under or in connection with this agreement shall be limited to the limit of their insurance or the value of the Work Order whichever is the greater, unless otherwise specified in the particular Work Order that has been accepted by the Supplier. </p>
              <p>The Supplier shall not be liable to a Buyer or OrderWork for any indirect or consequential loss a Buyer or OrderWork may suffer, even if the loss is reasonably foreseeable or the Supplier has been advised of the possibility of a Buyer or OrderWork incurring it.  </p>
              <p><strong>L. Indemnity</strong></p>
              <p align="justify">You agree to indemnify and hold us and (as applicable) our parent, subsidiaries, affiliates, officers, directors, agents, and employees, harmless from any claim, demand or loss, including reasonable legal fees, made by any third party arising out of your breach of this User Agreement or use of the Site or Services. This indemnity shall not apply where such claim, demand or loss arises solely due to (a) our negligence or (b) our breach of this User Agreement. </p>
              <p><strong> M. No agency</strong></p>
              <p align="justify">You and OrderWork are independent contractors, and no agency, partnership, joint venture, employee-employer or franchiser-franchisee relationship is intended or created by this User Agreement. </p>
              <p align="justify"><strong> N. Notices</strong></p>
              <p align="justify">Unless otherwise explicitly stated, notices to OrderWork must be sent by registered mail to OrderWork Ltd, Montpelier Chambers, 61-63 High Street South, Dunstable, LU6 3SF, and notices to you will be sent to the email address that you provide to OrderWork during the registration process (receipt is deemed 24 hours after an email is sent, unless we receive notice that the email address is invalid), or by registered mail.  </p>
              <p align="justify"><strong> O. Assignments</strong></p>
              <p align="justify">You agree that this User Agreement and all incorporated agreements may be automatically assigned by OrderWork, in our sole discretion, to a third party in the event of a merger or acquisition or similar corporate transaction. You may not assign, license or sub-contract any of your rights or obligations under this User Agreement. </p>
              <p align="justify"><strong> P</strong><strong>. Third Party Rights</strong></p>
              <p align="justify">A person who is not a party to this User Agreement has no right under the Contracts (Rights of Third Parties) Act 1999 to enforce any term of this User Agreement but this does not affect any right or remedy of a third party specified in this User Agreement or which exists or is available apart from that Act </p>
              <p><strong> Q. Intellectual Property Rights </strong></p>
              <ol type="a">
                <li>
                  <div align="justify"> The Buyer grants to the Supplier and OrderWork a royalty-free, world-wide, nonexclusive licence to use their content and other intellectual property solely for the purposes of completing the Work Order. </div>
                </li>
                <li>
                  <div align="justify"> Except as expressly set out in clause 1 above, this agreement does not transfer or grant to the Supplier any right, title or interest in any intellectual property rights in or to the Buyer or OrderWork’s Content. </div>
                </li>               
              </ol>
              <p align="justify"><strong>R. Non-solicitation of Staff</strong></p>
              <p align="justify">A User, whether acting as a Buyer or a Supplier, shall not directly or indirectly canvas with a view to offering or providing employment to, offer to contract with or entice to leave any employee of, or contractor to, another User who has been involved in the provision of services to that Buyer or Supplier as the case may be through OrderWork within the last six months, without the prior written consent of OrderWork and that Buyer or Supplier.  </p>
              <p align="justify"> <strong>S. General</strong></p>
              <p align="justify">This Agreement shall be governed in all respects by the English law. If any provision of this Agreement is held to be invalid or unenforceable, such provision shall be deemed deleted and the remaining provisions shall be enforced.  </p>
              <p>Headings are for reference purposes only and in no way define, limit, construe or describe the scope or extent of such section. Our failure to act with respect to a breach by you or others does not waive our right to act with respect to subsequent or similar breaches. Except as otherwise indicated, all provisions of the User Agreement shall survive any termination or expiration of this Agreement. </p>
              
              <p align="justify" class="bodytxtRed"> III. Additional Terms and Conditions for Suppliers </p>
              <p align="justify"><strong> A. Quality of Service</strong></p>
              <p align="justify">You agree to provide courteous and professional service to OrderWork and the Buyers and their end users at all times. The Buyer and OrderWork must approve your work prior to release of payment to you through our system. Failure to provide quality work to the satisfaction of the Buyer may result in non-payment for the services provided and a negative rating on our Site. Any disputes should between the Supplier and Buyer should be notified to OrderWork for resolution. If necessary, a dispute will be resolved by referral to an Arbitration Service.  <strong></strong></p>
              <p align="justify"><strong> B. Description of your Skills and Services</strong></p>
              <p align="justify">Your Content must describe your services and all terms associated with the delivery of such service. Your description may only include text descriptions, a logo or picture and other content relevant to your services.  </p>
              <p><strong> C. Accepting Work Orders</strong></p>
              <p align="justify">If you receive a Work Order and accept, you are obliged to use your best endeavours to complete the Work Order within the timescales provided.  </p>
              <p align="justify"><strong> D. Restriction on Automated Use of Site </strong></p>
              <p align="justify">The use of any automated means, including, without limitation, agents, robots, scripts, or spiders, to access, monitor, manipulate or copy any parts of the Site is STRICTLY PROHIBITED. This includes any automated refreshing of pages of our Site and any scripts or automated procedure used to accept work orders. </p>
              <p align="justify"><strong> E. Travel Time</strong></p>
              <p align="justify">The Site is designed to provide OrderWork and our Buyers with fast response times to their IT needs. As a Supplier, you have the ability to establish your range of coverage and response times at the time you set up Your Content on our Site. The range and response times that you select are considered local coverage and OrderWork and our Buyers assume that no travel time will be charged. </p>
              <p align="justify"><strong>F. Work Order Price</strong></p>
              <p align="justify">The Price is defined as the maximum total in GB pounds sterling that can be charged for the work specified in the Work Order inclusive of all charges for labour, parts (if applicable) and tax. This applies to both flat fee and hourly rate Work Orders. On hourly rate Work Orders, the labour rate you charge or agree to with OrderWork or the buyer is inclusive of all applicable taxes.  </p>
              <p>As a Supplier, you agree not to exceed the Price on any Work Order without OrderWork authorisation for an increase in the Work Order Price. In the event that you do work that exceeds the Work Order Price without OrderWork or the Buyer’s authorisation, you assume the risk of not being paid for this additional work. A Work Order cannot be closed out for sums in excess of the Work Order Price.  </p>
              <p><strong>G. Confidentiality </strong></p>
              <p align="justify">As a Supplier, you agree that you will not disclose to any third party any information whatsoever concerning OrderWork, any Buyer or the Buyer’s end customer unless you can prove that such information is already in the public domain at the time of disclosure. </p>
              <p>To the fullest extent permissible by law (and save always in relation to any existing business with any Buyer separate from that obtained through OrderWork), you shall not, without OrderWork’s prior written consent solicit or approach OrderWork’s Buyer or any other contact or referral that you may receive as a result of work that you have carried out for that Buyer: (i) in the context of offering or winning new business with the Buyer or any referral or contact from that Buyer in any way similar to that undertaken via OrderWork; or (ii) act in any manner prejudicial to OrderWork’s relationship with the Buyer or any contact or referral from that Buyer (or any future relationship and/or contract(s)) with the Buyer or any contact or referral; provided always that this shall exclude any bona fide bid for provision of (and thereafter performance of any awarded) IT services, in response to the Buyer’s (or a contact or referral of the Buyer) invitation to tender (“ITT”) for same (and provided always that such ITT has not been induced or suggested by you).  </p>
              <p>In the context of any actual or threaten breach by you of all or any part of the provision(s) of this clause, it is expressly acknowledged by the parties that damages would not be an adequate remedy, and accordingly OrderWork shall be entitled to seek an injunction, interim order or similar equitable relief, to restrain, prevent or avoid same.</p>
              <p>Furthermore, when you accept a Work Order from OrderWork or a Buyer which involves or relates to work on behalf of the Buyer’s customer (End Customer), you agree not to solicit further work from that End Customer without the prior written approval of OrderWork and the Buyer. This clause will only be waived if you can prove that you have carried out the same type of work for that End Customer (or division of the End Customer in the case of an End Customer with several divisions), in the 6 months prior to the acceptance of the Work Order.</p>
              <p><strong> H. Suspension or Removal from the Site </strong></p>
              <p align="justify">You agree and acknowledge that OrderWork has, in its sole discretion, the right, in addition to all other legal rights, to suspend or remove you from being listed as a Supplier on our Site for the following reasons:  </p>
              <div align="justify" type="a">
                <ol>
                  <li> Violations of the policies contained herein or as amended from time to time;  </li>
                  <li> If you accept a Work Order and fail to complete such Work Order in a timely manner and/or fail to contact the Buyer or OrderWork if there is a problem;  </li>
                  <li> If you receive negative feedback from our Buyers or their end customers;  </li>
                  <li> If the information you have provided to us or entered into the Site is false or misleading;  </li>
                  <li> If you circumvent or attempt to circumvent the OrderWork system to work directly with our Buyers; or  </li>
                  <li> If your actions, in any way, are deemed to be detrimental by OrderWork to the goodwill and reputation of OrderWork.  </li>
                </ol>
              </div>
              <p align="justify">Removal from our Site for violation of our policies may result in your forfeiting any monies due you from the Buyer. </p>
              <p align="justify"><strong> I. Feedback </strong></p>
              <div align="justify">
                <ol>
                  <li><i> Integrity</i><br>
      You may not take any actions that may undermine the integrity of the Feedback System. Using the Feedback System in a dishonest manner, including entering a rating for yourself, is a violation of this User Agreement. If you earn a negative feedback your membership may be suspended, and you may be unable to use our Site.  </li><br />
                  <li><i> Export </i><br>
      You acknowledge that your feedback consists of comments left by other Users and a rating compiled by OrderWork, and that the rating without the comments does not completely reflect your reputation among Users. Because feedback ratings are designed for facilitating service request decisions between Users and your eligibility to remain on the Site, you agree that you shall not market or export your OrderWork feedback rating to any venue or web-site other than an OrderWork-owned website. You agree that OrderWork shall have no liability to you relating to the feedback system.  </li><br />
                  <li><i> Import</i><br>
      We do not provide you with the technical ability to import feedback from other (non-OrderWork operated) web sites to OrderWork because a rating, without the corresponding comments does not completely reflect your reputation among OrderWork users. </li>
                </ol>
              </div>
              <p align="justify"><strong>J. Representations and Warranties</strong></p>
              <p align="justify"> You represent and warrant that:</p>
              <div align="justify">
                <ol>
                  <li> You are a completely independent contractor/contracting firm with no ties to or relationship with OrderWork and that at no time, unless instructed to do so by the Buyer through the Work Order request, shall in no manner indicate to a third party that you are anything other than a completely independent contractor. In addition, you agree that as an independent contractor you will not under any circumstances file employment or unemployment related claims against OrderWork or Buyers. </li><br />
                  <li> You are in good standing, and are not in violation of any applicable laws, rules or regulations, and that you have made and will make all required legal and tax filings, including VAT, PAYE and any other relevant requirements. If relevant, you shall file all necessary legal documentation relating to your self-employment required by any governmental body, and pay, in a timely manner, all pertinent self-employment taxes. </li><br />
                  <li> Unless agreed in writing by OrderWork, you warrant that you have professional indemnity, public liability and employer’s liability insurance and all other required insurances to the full extent required for the services you provide. </li><br />
                  <li> You acknowledge that the Site is based on the integrity and reliability of our Suppliers and, therefore, you warrant and represent that all information that you give to OrderWork or place on the Site is true and correct. OrderWork reserves the right to request a background check and/or drug test on you or your employees if required or requested by Buyers. You further warrant and represent that your listed employees have never been accused of harassment in any employment situation or sexual harassment in any situation. </li><br />
                  <li> You are responsible for complying with all Buyer instructions and to complete the work using reasonable skill and care in a timely and professional fashion in accordance with best industry practice. In addition, for all Work Orders that you accept, you must (a) register as completed the Work Order within three (3) business days of completion, unless otherwise specifically instructed by Buyer or risk not getting paid for the Work Order; (b) include if appropriate a valid identification number for parts when closing the Work Order, if applicable. – if you don’t you can be held financially responsible to OrderWork or the Buyer for the cost of the parts; (c) not remove equipment from the end user location; (d) be on-time for service call - if you are going to be later than fifteen (15) minutes for an appointment you must notify OrderWork and the Buyer. Failure to do so may cause the Work Order to be reassigned. </li><br />
                  <li> You acknowledge that OrderWork is not responsible for the action or inaction of any Buyer using the Site. </li><br />
                  <li> You acknowledge that OrderWork has expended significant sums to create its Site, which is proprietary. You will not attempt to circumvent any aspect of the Site in any way, including dealing outside of the Site with any party introduced to you through the Site. You will indemnify and hold harmless OrderWork against any claims, liabilities or losses arising directly or indirectly from any action or inaction as a Supplier. </li>
                </ol>
                <strong>K. Fees, Invoicing and Payment</strong></div>
              <p align="justify">OrderWork operates a self-billing system whereby it generates invoices on behalf of its Suppliers. By agreeing to these Terms and Conditions you agree to enter into a Self-Billing relationship with OrderWork and not to issue sales or VAT invoices to OrderWork or OrderWork’s Buyers. The self-billing agreement will last for 12 months from the date of your registration as a Supplier and will be subject to automatic renewal for further 12 month periods provided that your VAT status as a Supplier has not changed.  </p>
              <p>You agree that you are totally responsible for taxes such as VAT and PAYE and that you will remit all relevant and applicable taxes to the appropriate tax authorities when due. Furthermore, you agree to notify OrderWork immediately if you cease to be VAT registered, transfer your business as a going concern or become registered under another VAT number.  </p>
              <p>Unless otherwise stated in a Work Order, OrderWork matches Supplier payment terms to its Buyer payment terms. Therefore, when the Work Order is completed and has been marked as ‘Closed’ by you and OrderWork, or the Buyer, you may request payment, which will be actioned once the Buyer pays OrderWork (usually 7-30 days).  </p>
               
                  <strong>L. Protection of End User Information <br>
                  <br>
                  </strong> You agree that you will comply with our Privacy Policy.  <br>
              <p  align="justify" class="bodytxtRed">IV. Additional Terms and Conditions for Buyers </p>
              <strong> </strong>
              <p></p>
              <p align="justify"><strong> A. Work Order Creation </strong></p>
              <p align="justify">As a Buyer, you are obliged to complete the transaction with OrderWork if you: </p>
               <p align="justify">Confirm in writing, by email or via the OrderWork website, that you wish to proceed with a Work Order, and a Supplier in turn accepts and incurs costs associated with it.  </p>
                <p align="justify">In the event that a Work Order is cancelled after acceptance, the Work Order may be subject to a cancellation fee if the Supplier or OrderWork has already incurred costs associated with the Work Order at the time of cancellation.  </p>
                 <p align="justify">If the Services that you buy are set to allow it, then the data entry on our portal may be performed by your End Users.  This may be via a voucher service or other agreed method of approval for us to supply service to you.   </p>
              <p><strong>B. Warranties:<br>
                    <br>
&nbsp;&nbsp; &nbsp;</strong> You warrant that:  <strong>&nbsp;&nbsp;<br>
</strong></p>
              <div align="justify">
                <ol>
                  <li> Data Loss: you acknowledge that OrderWork is not responsible for any data loss by any User or any end user/customer of any User as a result of the fulfilment of a Work Order. You are solely responsible for any data or information stored in or on any equipment or for having the end user responsible for making "back up" or security copies of such data or information.  Personal data processing within OrderWork systems is covered within our Privacy Policy.  </li><br />
                  <li> You acknowledge that the Suppliers are "independent contractors" and will comply with the terms of engagement for Suppliers contained in this User Agreement. </li><br />
                  <li> You acknowledge that Suppliers who are asked as part of a Work Order to supply parts may use new OEM parts, refurbished parts or after-market parts at the Suppliers discretion, unless you specifically request otherwise in the Work Order. </li><br />
                  <li> You acknowledge that Work Orders completed by the Supplier are subject to auto-closure within 5 business days, if no further action is taken by you to approve and close the Work Order, or to raise an issue with respect to it. </li><br />
                  <li> If you are a business you hereby authorise OrderWork to use your name, and relationship with OrderWork in any reports and publications, including websites. OrderWork may also use your logos and trade marks in connection therewith. This includes the right to display industry affiliations, certifications and / or preferred provider network logos, where appropriate on the Site or in User profiles. </li><br />
                  <li> You agree that if any instructions contained in the Work Order should cause a claim (e.g., to ask a Supplier to represent himself to your end user in a manner which gives rise to a claim from the end user) that you shall indemnify OrderWork and the Supplier from any loss. </li>
                </ol>
              </div>
              <p align="justify"><strong>C. Non-Solicitation of Suppliers or Circumvention of OrderWork  </strong></p>
              <p align="justify">You acknowledge that OrderWork has expended significant sums to create its Site, which is proprietary. You will not attempt to circumvent any aspect of the Site in any way, including dealing outside of the Site with any Supplier introduced to you through the Site. In addition, you may not employ directly any such Supplier.  </p>
              <p>Therefore, to the fullest extent permissible by law (and save always in relation to any existing business with any Supplier separate from that obtained through OrderWork), you shall not, without OrderWork’s prior written consent solicit or approach OrderWork’s Suppliers:  </p>
              <p>(i) in the context of working with, or offering any business to, that Supplier in any way similar to that undertaken or possible via OrderWork  </p>
              <p>(ii) or act in any manner prejudicial to OrderWork’s relationship with the Supplier (or any future relationship and/or contract(s)) with the Supplier   </p>
              <p>provided always that this shall exclude any bona fide bid for provision of (and thereafter performance of any awarded) IT services, in response to your invitation to tender (“ITT”) for same (and provided always that such ITT has not been produced to circumvent this Agreement). </p>
              <p>In the context of any actual or threaten breach by you of all or any part of the provision(s) of this clause, it is expressly acknowledged by the parties that damages would not be an adequate remedy, and accordingly OrderWork shall be entitled to seek an injunction, interim order or similar equitable relief, to restrain, prevent or avoid same.  </p>
              <p align="justify"><strong> D. Refund Policy</strong></p>
              <p align="justify">If relevant, OrderWork will refund any unused portion of your funds on deposit with us in the event that you are unsatisfied with the use of our Site. All refunds will be made within 30 days after your account is closed on the Site. An account cannot be closed if there are any open, pending or disputed Work Orders.  <br>
                  <br>
                  <strong>E. Expiration Of Deposits <br>
                  <br>
                </strong> Any Buyer deposits with OrderWork do not have any expiration date. However, if your account is inactive on the Site for twelve months, the account may be subject to maintenance and administrative fees, or closure at our discretion. </p>
		  
			
	
	 </div> 
   
  </div>
  
</div>
  
</div>
  
	</div>
    <div class="clearBoth"></div>
	
</asp:content>


