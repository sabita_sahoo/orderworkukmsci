<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SPWorkingDays.aspx.vb"
    Title="OrderWork: Supplier Working Days" Inherits="OrderWorkUK.SPWorkingDays"
    MasterPageFile="~/MasterPage/MyOrderWork.Master" %>

<%@ Register TagPrefix="uc1" TagName="GridPager" Src="~/UserControls/UK/GridPager.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
    <script language="javascript" src="OWLibrary/JS/Scripts.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript" src="OWLibrary/JS/json2.js"></script>
    <script language="javascript" type="text/javascript" src="OWLibrary/JS/jquery.js"></script>
    <script language="JavaScript" src="OWLibrary/JS/JQuery.jsonSuggestCustom.js" type="text/javascript"></script>
    <script language="JavaScript" src="OWLibrary/JS/DD_roundies.js" type="text/javascript"></script>
    <script type="text/javascript">        DD_roundies.addRule('.roundifyRefineMainBody1', '5px 5px 0px 0px');
        //    DD_roundies.addRule('.roundifyCircle', '50% 50% 50% 50%');
    </script>
    <script type="text/javascript">
        DD_roundies.addRule('.roundifyRefine20', '20px');
        DD_roundies.addRule('.roundifyRefine10', '10px');
        DD_roundies.addRule('.roundifyRefine8', '8px');

    </script>

    
    <style type="text/css">
       
    
    


.gridHdr
{
	background-color:#993366;
	font-family:Verdana;
	font-size: 11px;
	font-weight: bold;
	color: #555354;
	text-decoration: none;
	height:28px;
	vertical-align:middle;
	text-align:left;
}
.gridRowText {

/*border-left:none 0px;
border-right:none 0px;*/
/*font-size:12px;*/

}
.gridRow {
border-left:none 0px;
border-right:none 0px;

}
#divPagerBtm{
    float:left; 
    clear:right; 
    width:300px; 
    margin-left:105px;
}
*html #divPagerBtm{
    float:left; 
    clear:right; 
    width:300px; 
    margin-left:50px;
}
#AlertDiv{
left: 40%; top: 40%;
position: absolute; width: 200px;
padding: 12px; 
border: #000000 1px solid;
background-color: white; 
text-align: left;
visibility: hidden;
z-index: 99;
}
#AlertButtons{
position: absolute; right: 5%; bottom: 5%;
}
/*Styles for Modal PopUp = START*/
#divModalParent {
    margin: auto;
    text-align: left;
}
.modalBackground 
 {
    background-color:Silver;
    filter:alpha(opacity=30) !important;
    opacity:0.3 !important;
    position:absolute !important;
    z-index:1 !important;
 }
.pnlConfirm {	
	background-color:#FFEBDE;
	font-family:Verdana;
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	line-height: 18px;
	border: 1px solid #CECBCE;
	padding: 10px;
	position:absolute !important;
	margin-left:650;
	margin-top:350;
}
.cursorPointer
{
	cursor:pointer;
}
.bottonTopGrey {
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-TR.gif) no-repeat right top;
	height:4px;
}
.bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
}
*+html .bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
	margin-top:-3px;
}

#divButton {
background-color:#999;
width:56px;
height:19px;
margin:10px 0 0;
padding:0;
}
img.btnCorner {
   width: 4px;
   height: 4px;
   border: none;
   display: block !important;
}
.buttonText {
font-family:Verdana;
font-size:11px;
color:#FFF;
text-decoration:none;
padding-left:11px;
padding-right:8px;
font-style:normal;
font-weight:400;
font-variant:normal;
line-height:11px;
}

.buttonText:hover {
color:#3F3F3F;
text-decoration:none;
}

 .divHolidayOuterHolidays
        {
            -moz-border-radius: 8px 8px 8px 8px;
            -webkit-border-radius: 8px 8px 8px 8px;
            border: 1px solid red;
            height: 100px;
            margin-left: 20px;
            padding-left: 20px;
            margin-right: 20px;
            margin-top: 60px;
            /*background-color: #fff3de;*/
        }
        
</style>
    <div id="div4" class="divContainerStyle floatLeft paddingBottom10">
        <div class="floatLeft">
            <asp:UpdatePanel ID="updatePnlWorkingDays" runat="server" RenderMode="Inline">
                <ContentTemplate>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="clsGreyBandStyleFund marginBottom10 floatleft" id="div5" style="width: 992px;">
                            <strong>Working Days</strong>
                        </div>
                    </div>
                    <div id="divMiddleBand" style="border-top: 1px solid #efebef;">
                       <%-- <div style="float: right; width: 1000px; display: none;">
                            <div id="div6" class="clsButtonGreyViewAll roundifyAddressForIE floatRight " style="margin-right: 20px;">
                                <asp:LinkButton ID="btnResetTop" runat="server" AccessKey="U" TabIndex="48" CausesValidation="false"
                                    class="txtListing"> Undo&nbsp;</asp:LinkButton></div>
                            <div id="div7" class="clsButtonGreyViewAll roundifyAddressForIE floatRight" style="margin-right: 10px;">
                                <asp:LinkButton ID="btnSaveTop" AccessKey="S" runat="server" CausesValidation="false"
                                    TabIndex="47" CssClass="txtListing"> Save&nbsp;</asp:LinkButton></div>
                        </div>--%>
                        <%--<div class="divHolidayOuter roundifyRefine8 floatleft marginBottom10" style="width: 960px;">--%>
                         <div class="divHolidayOuterHolidays roundifyRefine8">
                            <asp:Repeater ID="rptHolidays" runat="server">
                                <ItemTemplate>
                                    <div class='<%#IIF(Container.DataItem("ISSelected")="1", "divHolidayInnerSelected", "divHolidayInner")%>'>
                                        <div style="float: left; margin-top: 3px;">
                                            <asp:CheckBox ID="chkHoliday" runat="server" Checked='<%#IIF(Container.DataItem("ISSelected")="1", True, False)%>'
                                                Style="float: left;" />
                                            <div style="float: left; margin-top: 2px;">
                                                <strong>
                                                    <%#Container.DataItem("StandardValue")%></strong>
                                                <asp:HiddenField Value='<%#Container.DataItem("StandardID")%>' runat="server" ID="StandardID" />
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                    
                        <div style="width: 100%; height: 40px; margin-top: 20px;">
                            <div style="float: right; width: 450px;">
                                <div id="divbtn" class="clsButtonGreyViewAll roundifyAddressForIE floatRight " style="margin-right: 20px;">
                                    <asp:LinkButton ID="btnResetBottom" runat="server" AccessKey="U" TabIndex="48" CausesValidation="false"
                                        class="txtListing"> Undo&nbsp;</asp:LinkButton></div>
                                <div id="divbtn" class="clsButtonGreyViewAll roundifyAddressForIE floatRight " style="margin-right: 10px;">
                                    <asp:LinkButton ID="btnSaveBottom" AccessKey="S" runat="server" CausesValidation="false"
                                        TabIndex="47" CssClass="txtListing"> Save&nbsp;</asp:LinkButton></div>
                            </div>
                        </div>
                    </div>
                        </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="floatLeft">
            <div class="clsGreyBandStyleFund marginBottom10" id="divPaymentMethod" style="width: 992px;">
                <strong>Holiday</strong>
            </div>

            <div style="margin-left:20px;width:auto;height:80%" class="divHolidayOuterHolidays roundifyRefine8" >
            <asp:UpdatePanel ID="updatePnlListMessages" runat="server" RenderMode="Inline">
                <ContentTemplate>
                    <div id="div1" style="padding-left: 40px; clear: none;">
                        <div id="divMediumBtnImage" class="clsButtonGreyDetailsAccept " style="margin-left: 30px;
                            text-align: center;">
                            <a id="btnAddHoliday" onclick="btnAddHoliday_Click" runat="server" class="txtListing"
                                tabindex="11"><strong>
                               <%-- 'Poonam modified on 29/07/2015 - OM-1 : OW-DEV-ISSUE-MY Broken image SupplierWODetails.aspx--%>
                                    <img src="../OWLibrary/Images/Icons/Icon-SpecialistSmall.gif" runat="server" id="imgbtnSpecialist"
                                        width="12" height="16" hspace="5" vspace="0" align="absMiddle" border="0">&nbsp;Add
                                    Holiday</strong></a></div>
                             </div>
        <table  style="margin-left:2px; width:100%;">          

         <tr>
                <td style="width:20%;">
                    &nbsp;
                </td>
                <td>
                    <asp:Button ID="btnClearSelection" Visible="false" runat="server" Text="Clear Calendar Selection" class="txtListing" />
                </td>
                 <td style="width:20%;">
                    &nbsp;
                </td>
            </tr>
                        <tr>
                 <td style="width:32%; text-align:right; vertical-align:top;">
                    <asp:Label ID="lblSelectDate" Visible="false" runat="server" Text="Choose Dates: "></asp:Label>
                </td>


                <td valign="top" align="left">
                <%-- 'Poonam modified on 29/07/2015 - OM-1 : OW-DEV-ISSUE-MY Broken image SupplierWODetails.aspx--%>
                    <asp:Calendar ID="Calendar1" SelectionMode="None" Width="480px" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66"
                        BorderWidth="1px" DayNameFormat="Short" Font-Names="Verdana" Font-Size="8pt" 
                        ForeColor="#663399"  ShowGridLines="True" 
                        SelectWeekText="Week <img height='10' border='0' src='../OWLibrary/Images/Arrows/Black-Arrow.gif' width='5'>"  
                        SelectMonthText="Month <img height='10' border='0' src='../OWLibrary/Images/Icons/Icon-SpecialistSmall.gif' width='10'>" 
                        ShowNextPrevMonth="true" NextMonthText="" >
                         <%--SelectMonthText="<img height='10' border='0' src='OWLibrary/Images/Icons/Icon-SpecialistSmall.gif' width='5'>"--%>
                        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                        <SelectorStyle BackColor="#FFCC66"/>
                        <TodayDayStyle BackColor="#FFCC66" />
                        <OtherMonthDayStyle ForeColor="#CC9966" BackColor="LightGray" />
                        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />

                    </asp:Calendar>
                    <td valign="top" >
                    <%-- 'Poonam modified on 29/07/2015 - OM-1 : OW-DEV-ISSUE-MY Broken image SupplierWODetails.aspx--%>
                    <asp:Calendar Width="480px" ID="Calendar2" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66"
                        BorderWidth="1px" DayNameFormat="Short" Font-Names="Verdana" Font-Size="8pt" 
                        ForeColor="#663399" SelectionMode="None" ShowGridLines="True" 
                         SelectWeekText="Week <img height='10' border='0' src='../OWLibrary/Images/Arrows/Black-Arrow.gif' width='5'>"  
                        SelectMonthText="Month <img height='10' border='0' src='../OWLibrary/Images/Icons/Icon-SpecialistSmall.gif' width='10'>" 
                        ShowNextPrevMonth="true" PrevMonthText="" >
            
                        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                        <SelectorStyle BackColor="#FFCC66" />
                        <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                        <OtherMonthDayStyle ForeColor="#CC9966" BackColor="LightGray" />
                        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />

                    </asp:Calendar></td>
                </td>
            </tr>
             <tr>
                <td style="width:20%; text-align:right;">
                    <asp:Label ID="lblHolidatDate" Visible="false" runat="server" Text="Add Holidays Caption: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtHolidayDateTop" Visible="false" Width="60%" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="btnClick" ID="reqHolidayCaption" ControlToValidate="txtHolidayDateTop"  runat="server" ErrorMessage="Please Enter Holiday Caption" Display="Dynamic" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                </td>
                 <td style="width:20%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                  <td style="width:20%;">
                    &nbsp;
                </td>
                <td align="left">
                    <asp:Button ID="btnSelectDates" ValidationGroup="btnClick" Visible="false" runat="server" Text="Add/Remove Holidays" />
                    <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                </td>
                 <td style="width:20%;">
                    &nbsp;
                </td>
            </tr>
            
        </table>

                        <asp:Panel ID="pnlHolidayListing" Visible="false" runat="server">
                            <div style="width: 1010px; margin-top: 2px; margin-bottom: 2px; float: left; clear: right;">
                                <div id="tdTotalRecs" style="width: 100px; padding-top: 10px; padding-left: 5px;
                                    text-align: left; float: left; clear: right;" runat="server">
                                </div>
                                <div id="divPagerTop" style="float: left; clear: right; width: 300px;">
                                    <uc1:GridPager ID="GridPagerTop" runat="server"></uc1:GridPager>
                                </div>
                                <div id="Div2" style="padding-top: 10px; text-align: right; font-size: 11px; float: right;
                                    clear: right;" runat="server">
                                    View Records
                                    <asp:DropDownList Style="margin-left: 4px;" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                                        CssClass="formField marginLR5" ID="ddlPageSize" runat="server" AutoPostBack="true">
                                        <asp:ListItem>10</asp:ListItem>
                                        <asp:ListItem>15</asp:ListItem>
                                        <asp:ListItem>20</asp:ListItem>
                                        <asp:ListItem>25</asp:ListItem>
                                    </asp:DropDownList>
                                    per page
                                </div>
                            </div>
                            <div style="float: left;">
                                <asp:DataList runat="server" ID="dlHolidayList" Width="690px">
                                    <HeaderTemplate>
                                        <table width="1000px" cellspacing="0" border="0">
                                            <tr class="gridHdr" style="height: 15px;">
                                                <td class="txtGrid" width="240px" style="padding-left: 0px;">
                                                    <asp:LinkButton ID="lnkDate" runat="server" CommandName="HolidayDate" CssClass="txtgrid">Holiday Date</asp:LinkButton>&nbsp;<asp:Image
                                                        ImageUrl="~/Images/WhiteArrow-Up.gif" ID="imgHolidayDate" runat="server" Visible="false" />
                                                </td>
                                                <td class="txtGrid" width="480px" style="padding-left: 0px;">
                                                    <asp:LinkButton ID="lnkSubject" runat="server" CommandName="HolidayTxt" CssClass="txtgrid">Holiday Caption</asp:LinkButton>&nbsp;<asp:Image
                                                        ImageUrl="~/Images/WhiteArrow-Up.gif" ID="imgHolidayCaption" runat="server" Visible="false" />
                                                </td>
                                                <td class="txtGrid" width="80px" style="padding-left: 0px;">
                                                    <asp:LinkButton ID="lnkIsActive" runat="server" CommandName="IsDeleted" CssClass="txtgrid">IsActive</asp:LinkButton>&nbsp;<asp:Image
                                                        ImageUrl="~/Images/WhiteArrow-Up.gif" ID="imgIsActive" runat="server" Visible="false" />
                                                </td>
                                                <td class="txtGrid" width="100px" style="padding-left: 0px;">
                                                    &nbsp
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table width="1000px" cellspacing="0" border="0" id="tblHoliday" runat="server">
                                            <tr class="gridRow" id="trdlRow">
                                                <td class="gridRowText colorBlueListing" width="240px" style="height: 15px;">
                                                    <%--<%#Container.DataItem("HolidayDate")%>--%>
                                                    <asp:Label ID="lblHolidayDate" runat="server" Text='<%#Container.DataItem("HolidayDate") %>'></asp:Label>
                                                    <asp:TextBox runat="server" Visible="false" Text='<%#Container.DataItem("HolidayDate") %>'
                                                        ID="txtHolidayDate1" Height="15" Width="120" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
                                                    <img alt="Click to Select" runat="server" visible="false" src="../OWLibrary/Images/calendar.gif"
                                                        id="imgCalHoliday" style="cursor: pointer; vertical-align: middle;" />
                                                    <asp:RegularExpressionValidator ForeColor="Red" ID="regHolidayDate1" Display="Dynamic"
                                                        ControlToValidate="txtHolidayDate1" ErrorMessage="Holiday Date should be in DD/MM/YYYY Format"
                                                        ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                        runat="server"></asp:RegularExpressionValidator>
                                                    <cc1:CalendarExtender CssClass="dddddd" Format="dd/MM/yyyy" PopupButtonID="imgCalHoliday"
                                                        TargetControlID="txtHolidayDate1" ID="CalendarExtender1" runat="server" Enabled="true">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td class="gridRowText" width="500px" style="height: 15px;">
                                                    <%--<%#Container.DataItem("HolidayTxt")%>--%>
                                                    <asp:Label ID="lblHolidayCaption" runat="server" Text='<%#Container.DataItem("HolidayTxt") %>'></asp:Label>
                                                    <asp:TextBox ID="txtHolidayCaption" Width="500px" runat="server" Text='<%#Container.DataItem("HolidayTxt") %>'
                                                        Visible="false"></asp:TextBox>
                                                </td>
                                                <td class="gridRowText" width="80px" style="height: 15px; text-align: center;">
                                                    <asp:Label ID="lblIsActive" runat="server" Text='<%#IIF(Eval("IsDeleted")=0,"True","False") %>'></asp:Label>
                                                    <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%#IIF(Eval("IsDeleted")=0,"True","False") %>'
                                                        Visible="false"></asp:CheckBox>
                                                </td>
                                                <td class="gridRowText" width="100px" style="height: 15px;">
                                                    <%--<asp:LinkButton ID="lnkbtnEditHoliday" CommandName="EditHoliday" CommandArgument='<%#Container.DataItem("DateID")& "," & Container.DataItem("HolidayDate") & "," & Container.DataItem("HolidayTxt") & "," & Container.DataItem("IsDeleted") %>'--%>
                                                    <asp:LinkButton ID="lnkbtnEditHoliday" CommandName="EditHoliday" CommandArgument='<%#Container.DataItem("DateID") %>'
                                                        runat="server">
                                                        <img id="imgEditHoliday" runat="server" src="~/OWLibrary/Images/Icons/Icon-Pencil.gif"
                                                            title="Edit Holiday" border="0" class="cursorPointer" /></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Cancel" Visible="false" CommandName="Cancel"></asp:LinkButton>
                                                     &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkInActive" runat="server" CommandName="InActive" CommandArgument='<%#Eval("DateID")%>' >
                                                            <img id="img1" runat="server" src="~/OWLibrary/Images/Icons/Cancel.gif"
                                                            title="Delete Holiday" border="0" class="cursorPointer" />
                                                        </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                                <%--<asp:GridView ID="grdHolidayList" ForeColor="Maroon" Font-Size="11px" BorderWidth="1px"
                                    BorderColor="Black" runat="server" AutoGenerateColumns="false" Width="1000px">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Holidaye Date" ItemStyle-Width="300px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHolidayDate" runat="server" Text='<%#Eval("HolidayDate") %>'></asp:Label>
                                                <asp:TextBox runat="server" Visible="false" Text='<%#Eval("HolidayDate") %>' ID="txtHolidayDate1"
                                                    Height="15" Width="120" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
                                                <img alt="Click to Select" runat="server" visible="false" src="../OWLibrary/Images/calendar.gif"
                                                    id="imgCalHoliday" style="cursor: pointer; vertical-align: middle;" />
                                                <asp:RegularExpressionValidator ForeColor="Red" ID="regHolidayDate1" Display="Dynamic"
                                                    ControlToValidate="txtHolidayDate1" ErrorMessage="Holiday Date should be in DD/MM/YYYY Format"
                                                    ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                    runat="server"></asp:RegularExpressionValidator>
                                                <cc1:CalendarExtender CssClass="dddddd" Format="dd/MM/yyyy" PopupButtonID="imgCalHoliday"
                                                    TargetControlID="txtHolidayDate1" ID="CalendarExtender1" runat="server" Enabled="true">
                                                </cc1:CalendarExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Holiday Text" ItemStyle-Width="500px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHolidayCaption" runat="server" Text='<%#Eval("HolidayTxt") %>'></asp:Label>
                                                <asp:TextBox ID="txtHolidayCaption" Width="500px" runat="server" Text='<%#Eval("HolidayTxt") %>'
                                                    Visible="false"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="IsActive" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIsActive" runat="server" Text='<%#IIF(Eval("IsDeleted")=0,"True","False") %>'></asp:Label>
                                                <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%#IIF(Eval("IsDeleted")=0,"True","False") %>'
                                                    Visible="false"></asp:CheckBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandName="EditHoliday"
                                                    CommandArgument='<%#Eval("DateID")%>'></asp:LinkButton>
                                                <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Cancel" Visible="false" CommandName="Cancel"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>--%>
                            </div>
                            <div style="width: 1010px; margin-top: 2px; margin-bottom: 2px;">
                                <div id="divPagerBtm">
                                    <uc1:GridPager ID="GridPagerBtm" runat="server"></uc1:GridPager>
                                </div>
                                <div id="Div3" style="padding-right: 0px; padding-top: 5px; text-align: right; font-size: 11px;
                                    float: right;" runat="server">
                                    View Records
                                    <asp:DropDownList Style="margin-left: 4px;" OnSelectedIndexChanged="ddlPageSizeBtm_SelectedIndexChanged"
                                        CssClass="formField marginLR5" ID="ddlPageSizeBtm" runat="server" AutoPostBack="true">
                                        <asp:ListItem>10</asp:ListItem>
                                        <asp:ListItem>15</asp:ListItem>
                                        <asp:ListItem>20</asp:ListItem>
                                        <asp:ListItem>25</asp:ListItem>
                                    </asp:DropDownList>
                                    per page
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlnoRecords" Visible="false" runat="server">
                            <div style="text-align: center; margin-top: 20px;">
                                No Records Available</div>
                        </asp:Panel>
                    </div>
                    <div id="divClearBoth">
                    </div>
                    
                    <asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click" Height="0" Width="0"
                        BorderWidth="0" Style="visibility: hidden;" />
                    <asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" Style="visibility: hidden;" />
                </ContentTemplate>
                 <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Calendar1" EventName="SelectionChanged" />
                        <asp:AsyncPostBackTrigger ControlID="Calendar2" EventName="SelectionChanged" />
                </Triggers>
            </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div class="clearBoth">
    </div>
</asp:Content>
