<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClientSubmittedJobs.aspx.vb" Title="OrderWork: Client Submitted Jobs"  Inherits="OrderWorkUK.ClientSubmittedJobs"  MasterPageFile ="~/MasterPage/MyOrderWork.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
<script type="text/javascript" src="../OWLibrary/JS/Scripts.js"></script>  
<script language="javascript" src="OWLibrary/JS/DD_roundies.js"  type="text/javascript"></script>
<script type="text/javascript">
        DD_roundies.addRule('.roundifyRefine', '8px 8px 8px 8px');
</script>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:HiddenField id="hdnCompID" name="hdnCompID" runat="server" />
<asp:HiddenField id="hdnStartDate" SafeEncode="false" runat="server" value="" />
<asp:HiddenField id="hdnToDate" SafeEncode="false" name="hdnToDate" runat="server" /> 

	<div id="divTopBand" class="divContainerStyle floatLeft" >
    <div style="width:992px;padding-left:30px;"  class="clsGreyBandStyleFund marginBottom10">
                             <strong >Client Submitted Jobs Report </strong>
                             </div>
	
	<div id="divDateRange" runat="server" class="divDateRange" >
    <asp:ValidationSummary ID="valSumm" runat="server" CssClass="bodytxtValidationMsg"></asp:ValidationSummary>
    <table border="0" cellpadding="0" cellspacing="0">
    <tr><td width="80" valign="middle" style="padding-left:17px;font-size:12px;" class="datelbl">From Date</td>
    <td width="150" height="24" valign="top" style="padding-top:4px;">
	 &nbsp;<asp:TextBox id="txtScheduleWInBegin" runat="server"  CssClass="formFieldGrey width120" TabIndex="2" Width="66px" ></asp:TextBox>
	 <asp:RegularExpressionValidator ID="regExpFromDate" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtScheduleWInBegin" display="none"
							ErrorMessage="From Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
	<img alt="Click to Select" src="~/OWLibrary/Images/calendar.gif" runat="server" id="btnBeginDate" style="cursor:pointer; vertical-align:middle;" />		                               
	<cc1:OWCalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID="btnBeginDate" TargetControlID="txtScheduleWInBegin" ID="calBeginDate" runat="server" OnClientDateSelectionChanged="GetFromDate"></cc1:OWCalendarExtender>
	</td><td width="80" valign="middle" class="datelbl" style="font-size:12px;">To Date</td>
	<td width="450" height="24" valign="top" style="padding-top:4px;" >
	<asp:TextBox id="txtScheduleWInEnd" runat="server" CssClass="formFieldGrey width120" TabIndex="3" Width="66px"></asp:TextBox>
	<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtScheduleWInEnd" display="none"
							ErrorMessage="To Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
	<img alt="Click to Select" src="~/OWLibrary/Images/calendar.gif" id="btnEndDate" runat="server" style="cursor:pointer;vertical-align:middle;" />		                               																	
	<cc1:OWCalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID="btnEndDate" TargetControlID="txtScheduleWInEnd" ID="calEndDate" runat="server" OnClientDateSelectionChanged="GetToDate" OnClientShowing="function x(s, e) { s.set_MinimumAllowableDate(getbegindateBestBuy()); }"  ></cc1:OWCalendarExtender>
	</td>
	
	<td>   <div id="divBtn" class="BGSLA divMediumBtnImage clsButtonGreyDetails roundifyAddressForIE" style="text-align: left;">
        <asp:LinkButton ID="lnkView" CausesValidation="true" runat="server"
            Style="cursor: hand;" CssClass="textDecorationNone">
            <img src="~/OWLibrary/Images/Icons/ViewBtnIcon.gif" OnClick="lnkView_Click"  runat="server" id="imgbtnView"
                hspace="5" vspace="0" align="absMiddle" border="0" class="imgPrintSpace">
            View
        </asp:LinkButton>
    </div></td>   
    <td>
     <div id="divExpToExc" runat="server">
               <div id="divMediumBtnImage" class="clsButtonGreyDetailsAccept " style="float:right;margin-right:15px;valign:bottom;">
                    <a runat="server" target="_blank" id="btnExport">
                        <img src="~/OWLibrary/Images/Icons/Icon-Excel.gif" runat="server" id="imgbtnexceltop" width="15" height="15" hspace="5" vspace="0" align="absMiddle"  border="0">
                     &nbsp;Export to Excel&nbsp;</a>
               </div>
           </div>
    </td>
	</tr>
    </table>  
         
 
    
   
    <div id="divClearBoth">
    </div>
</div>
        <div style="padding-top:20px;padding-left:20px;padding-bottom:20px;" id="divMainReport" runat="server">
        <div style="margin-bottom:10px;"><strong>Client Submitted Jobs Report</strong></div>
        Total submitted work orders :   <asp:Label ID="lblTotalSubmittedWO" runat="server"></asp:Label> 
        <div runat="server" id="divServicesListing" style="padding-top:20px;">
            
            <div style="margin-bottom:5px;"><strong>Services Used</strong></div>
              <div  style="width:410px;">
             <asp:datalist id="rptServices" runat="server" >           
            <ItemTemplate>
          
            <table cellpadding="0" cellspacing="0">
              <tr class="clsListingColumn" style="line-height:19px;">
             
              <td width="300"><%#Container.DataItem("ServiceName")%></td>
              <td width="20">:</td>
              <td width="50"><%#Container.DataItem("ServiceCount")%></td>
             
              </tr>
            </table>    
                
            </ItemTemplate>
          </asp:datalist>  
          </div>        
           <div class="divClearBoth"></div>   
            </div>            
       
        </div>
       <table id="tblNoRecords" runat="server" width="100%" border="0" cellpadding="10" cellspacing="0">					
	    <tr>
		    <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.</td>
		</tr>				
		</table>
	</div>
	
    <div class="clearBoth"></div>
 </asp:content>
