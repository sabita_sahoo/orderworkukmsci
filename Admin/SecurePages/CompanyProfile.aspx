<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CompanyProfile.aspx.vb" Title="OrderWork: Company Profile" Inherits="OrderWorkUK.CompanyProfile" MasterPageFile ="~/MasterPage/MyOrderWork.Master"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">
<script language="javascript" src="OWLibrary/JS/Scripts.js"  type="text/javascript" ></script>
    <script language="javascript" type="text/javascript" src="OWLibrary/JS/json2.js"></script>
<script language="javascript" type="text/javascript" src="OWLibrary/JS/jquery.js"></script>
<script language="JavaScript" src="OWLibrary/JS/JQuery.jsonSuggestCustom.js" type="text/javascript"></script>
<script language="JavaScript" src="OWLibrary/JS/DD_roundies.js" type="text/javascript"></script>
<script type="text/javascript">    DD_roundies.addRule('.roundifyRefineMainBody1', '5px 5px 0px 0px');
    //    DD_roundies.addRule('.roundifyCircle', '50% 50% 50% 50%');
</script>
<div id="divLocation">
    <div>	
	<%@ Register TagPrefix="uc1" TagName="UCCompanyProfile" Src="~/UserControls/UK/UCCompanyProfile.ascx" %>
	<uc1:UCCompanyProfile id="UCCompanyProfile1" runat="server"></uc1:UCCompanyProfile></div>
	
</div>

</asp:Content>
