<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OWInvoicesPrintForm.aspx.vb" Inherits="OrderWorkUK.OWInvoicesPrintForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>OrderWork: Sales Invoice</title>
<style type="text/css">
<!--
.invoiceLabel {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 32px;
	color: #B7B7B7;
	font-weight: bold;
	line-height:38px;
}
.InvoiceText {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.bdr1 {
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 4px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.bdr2 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #C6C6C6;
	border-right-color: #C6C6C6;
	border-bottom-color: #C6C6C6;
	border-left-color: #C6C6C6;
}
.bdr3 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.style3 {font-size: 11px}
.style5 {font-weight: bold}
.style6 {font-weight: bold}
td
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 16px;
	color: #000000;
}
body {
	background-color: #CECECE;
	height:100%;
}.VatMessage {
	text-align: center;
}
.pb
 { page-break-after : always } ;
  }
-->
</style>
</head>
<body style="height:100%;" onload="javascript:this.print();">

  <form id="form1" runat="server" style="height:100%;">
    
    <asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
      <table height="500" width="700" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FEFFD3">
        <tr>
          <td>
            <table border="0" cellpadding="0" cellspacing="0" align="center">
              <tr>
                <td>&nbsp;</td>
                <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#CC3300; font-weight:bold;">
                  <asp:Label runat="server" ID="lblErrMsg"></asp:Label>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </asp:Panel>
  
    <asp:Panel runat="server" ID="pnlPrintContent" Visible="true">
      <asp:Repeater ID="dlPrint" runat="server">
        <itemtemplate>
          <table border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
            <tr>
              <td width="45" height="140">&nbsp;</td>
              <td width="650" height="140"><table width="100%" cellpadding="0" cellspacing="0" style=" border-width:medium; border-bottom-width:5px;border-bottom-color:#000000;">
                  <tr>
                    <td width="311">&nbsp;</td>
                    <td width="131" align="left" valign="bottom" >&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="311" valign="middle"><img src="OWLibrary/Images/Invoice_Logo_New.gif" runat="server" id="imgLogo" alt="OrderWork - Services Reinvented" border="0" class="LogoImg" /></td>
                    <td align="right" valign="bottom" ><table width="99%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="right" valign="bottom"  style="height: 22px;"><span class="style1 style5"> <%#ViewState("CompanyName")%> </span></td>
                        </tr>
                        <tr>
                          <td align="right" valign="bottom"  ><%#ViewState("Address")%> </td>
                        </tr>
                        <tr>
                          <td align="right" valign="bottom" style="height: 15px"  ><%#ViewState("City")%> </td>
                        </tr>
                        <tr>
                          <td align="right" style="height: 15px" valign="bottom"><%#ViewState("PostCode")%> </td>
                        </tr>
                        <tr>
                          <td align="right" valign="bottom"  > Tel:&nbsp; <%#ViewState("Phone")%></td>
                        </tr>
                        <tr>
                          <td align="right" valign="bottom" > Fax:&nbsp; <%#ViewState("Fax")%></td>
                        </tr>
                        <tr>
                          <td valign="bottom" ></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td width="311" class="bdr1" >&nbsp;</td>
                    <td align="left" valign="bottom" class="bdr1" >&nbsp;</td>
                  </tr>
              </table></td>
              <td width="45" height="140">&nbsp;</td>
            </tr>
            <asp:Panel runat="server" ID="pnlInvoice" Visible='<%#IIF(Container.DataItem("AdviceType") = "Sales",True,False)%>'>
              <tr>
                <td width="45" height="180" align="center">&nbsp;</td>
                <td width="650" height="180" align="center"><p class="invoiceLabel">Invoice</p>
                    <table border="0" cellpadding="5" cellspacing="0" width="100%">
                      <tr>
                        <td align="left" valign="top"  style="border-bottom: 0px; border-right-width: 0px"><strong>Invoice To:</strong><br />
                          <%#GetBillingAddress(Container.DataItem("InvoiceNo"))%>
                        <td align="right" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="95" align="left"><strong>Account No.&nbsp;</strong></td>
                              <td width="8" align="left">:</td>
                              <td width="85" align="left"><%# Container.DataItem("AccountNo") %></td>
                            </tr>
                            <tr>
                              <td width="95" align="left"><strong>Credit Terms&nbsp;</strong></td>
                              <td align="left">:</td>
                              <td align="left"><%# Container.DataItem("CreditTerms") %></td>
                            </tr>
                            <tr>
                              <td width="95" align="left"><strong>Invoice No.&nbsp;</strong></td>
                              <td align="left">:</td>
                              <td align="left"><strong> <%#Container.DataItem("InvoiceNo")%> </strong> </td>
                            </tr>
                            <tr>
                              <td width="95" align="left"><strong>Invoice Date&nbsp;</strong></td>
                              <td align="left">:</td>
                              <td align="left"><%# Strings.FormatDateTime(Container.DataItem("InvoiceDate").ToString(), DateFormat.ShortDate) %></td>
                            </tr>
                            <tr>
                              <td width="95" align="left"><strong>VAT No.</strong></td>
                              <td align="left">:</td>
                              <td align="left"><strong> <%# ViewState("VATRegNo") %> </strong></td>
                            </tr>
                        </table></td>
                      </tr>
                    </table>
                    <p>&nbsp;</p></td>
                <td width="45" height="180" align="center">&nbsp;</td>
              </tr>
              <tr>
                <td width="45" align="right" valign="top">&nbsp;</td>
                <td width="650" align="right" valign="top"><table width="650" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="70" height="30" align="left" ><strong>Date</strong></td>
                      <td width="194" height="30"  align="left"  style="border-right-width: 0px;"><strong>Description</strong><strong></strong></td>
                      <td width="97" height="30" align="center"  style="border-right-width: 0px"><strong>YourRef</strong></td>
                      <td width="77" height="30" align="right"  ><strong>Net Price</strong></td>
                      <td width="114" height="30" align="right" background="Drag to a file to choose it." style="width:border-right-width: 0px"><strong>VAT( <%# OrderWorkLibrary.ApplicationSettings.VATPercentage(Container.DataItem("InvoiceDate").ToString()  ) %> %)</strong></td>
                      <td width="98" height="30" align="right" ><strong>Amount</strong></td>
                    </tr>
                    <asp:Repeater ID="rptList" runat="server" Visible="true">
                      <itemtemplate>
                        <tr>
                          <td align="left" width="70" valign="top"  style="border-right-width: 0px;border-top-width: 0px;"><%#Strings.FormatDateTime(Container.DataItem("CloseDate"), DateFormat.ShortDate)%> &nbsp;</td>
                          <td align="left" width="194"  style="border-right-width: 0px;border-top-width: 0px;"><%#Container.DataItem("Description")%> </td>
                          <td align="center" width="97"  style="border-right-width: 0px;border-top-width: 0px;"><%#Container.DataItem("PONumber")%> </td>
                          <td width="77" align="right"  style="border-right-width: 0px;border-top-width: 0px;"><%#FormatCurrency(Container.DataItem("InvoiceNetAmnt"), 2, TriState.True, TriState.True, TriState.False)%></td>
                          <td align="right" width="114"  style="border-right-width: 0px;border-top-width: 0px;"><%#FormatCurrency(Container.DataItem("VAT"), 2, TriState.True, TriState.True, TriState.False)%></td>
                          <td align="right" width="98"  style="border-top-width: 0px;"><%#FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%></td>
                        </tr>
                      </itemtemplate>
                    </asp:Repeater>
                  </table>
                    <asp:Panel Visible=<%# ShowListFee(Container.DataItem("ListVAT"), Container.DataItem("ListWoVal"),Container.DataItem("ListTotal")) %> runat="server" ID="pnlListingFee">
                      <table width="650" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="70" valign="top" align="left" ><%# Strings.FormatDateTime(Container.DataItem("InvoiceDate").ToString(), DateFormat.ShortDate) %></td>
                          <td align="left" width="194"  style="border-right-width: 0px;"><asp:Label runat="server" ID="lblListingDesc" Text='<%# GetListOfWorkorder(Container.DataItem("WorkOrderId")) %>'></asp:Label></td>
                          <td width="97"  align="center"  style="border-right-width: 0px">&nbsp;</td>
                          <td width="77"  align="right"  ><%#FormatCurrency(Container.DataItem("ListWoVal"), 2, TriState.True, TriState.True, TriState.False)%> </td>
                          <td width="114" align="right" style="border-right-width: 0px"><%#FormatCurrency(Container.DataItem("ListVAT"), 2, TriState.True, TriState.True, TriState.False)%> </td>
                          <td width="98" align="right" ><%#FormatCurrency(Container.DataItem("ListTotal"), 2, TriState.True, TriState.True, TriState.False)%> </td>
                        </tr>
                      </table>
                    </asp:Panel>
                    <br />
                    <table border="0" cellspacing="0" width="400">
                      <tr>
                        <td width="330" align="right"  style=" border-bottom:0px; border-right-width:0px; "><strong>Sub-Total</strong></td>
                        <td  align="right"  style=" border-bottom:0px; "><%# Container.DataItem("NetAmount") %> </td>
                      </tr>
                      <tr runat="server" visible="false" >
                        <td height="20" align="right"   style=" border-bottom:0px; border-right-width:0px; "><strong>Total VAT (<%# OrderWorkLibrary.ApplicationSettings.VATPercentage(Container.DataItem("InvoiceDate").ToString()  ) %> %) </strong></td>
                        <td height="20" align="right"  style=" border-bottom:0px; "><%#Container.DataItem("NetVat")%> </td>
                      </tr>
                      <tr Visible='<%#IIF(Container.DataItem("Discount") > 0.00,True,False)%>' runat="server">
                         <td height="20" align="right"   style=" border-bottom:0px; border-right-width:0px; "><strong>Payment within 30 days Discount (<%#Container.DataItem("DiscountPer")%>%)</strong></td>
                        <td  height="20" align="right"  style=" border-bottom:0px; "><%#Container.DataItem("Discount")%> </td>
                      </tr>
                       <tr  runat="server">
                        <td  height="20" align="right" style="border-right-width:0px; "><strong>Total VAT (<%# OrderWorkLibrary.ApplicationSettings.VATPercentage(Container.DataItem("InvoiceDate").ToString()  ) %> %) </strong></td>
                        <td  align="right" style=" border-bottom:0px; "><%#Container.DataItem("NetVat") - Container.DataItem("VATOnDiscount")%></td>
                      </tr>
                      
                      <tr>
                        <td  height="20" align="right"   style="border-right-width:0px; padding-top:10px; "><strong><%#IIf(Container.DataItem("Discount") > 0.0, "Payment after 30 days - Total Amount", "Total Amount")%></strong></td>
                        <td   align="right" style="padding-top:10px;" ><span class="style6"> <%#Container.DataItem("Total")%> </span> </td>
                      </tr>
                      <tr Visible='<%#IIF(Container.DataItem("Discount") > 0.00,True,False)%>' runat="server">
                        <td height="20" align="right" style="border-right-width:0px; "><strong>Payment within 30 days - Total Amount</strong></td>
                        <td align="right"><span class="style6"> <%#Container.DataItem("Total") - Container.DataItem("Discount") - Container.DataItem("VATOnDiscount")%> </span> </td>
                      </tr>
                      <tr><td>&nbsp;</td></tr>
                    </table>
                    </td>
                <td width="45" align="right" valign="top">&nbsp;</td>
              </tr>
              <tr>
              <td width="45" align="right" valign="top">&nbsp;</td>
              <td>
              <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
              <td width="50%"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td height="40" colspan="3"  align="left" valign="middle" class="bdr2" ><strong >OrderWork Bank Details</strong></td>
                      </tr>
                      <tr>
                        <td width="110" align="left"><strong>Account Name </strong></td>
                        <td width="8" align="left" ><strong>:</strong></td>
                        <td align="left"  style="border-right:2px solid #c6c6c6; padding-right:20px;" ><%# OrderWorkLibrary.Encryption.Decrypt(ViewState("BankAccountName"))%></td>
                      </tr>
                      <tr>
                        <td align="left"><strong>Sort-Code &nbsp;</strong></td>
                        <td align="left" ><strong>:</strong></td>
                        <td align="left"  style="border-right:2px solid #c6c6c6; padding-right:20px;" ><%# OrderWorkLibrary.Encryption.Decrypt(ViewState("BankSortCode"))%></td>
                      </tr>
                      <tr>
                        <td align="left"><strong>Account Number</strong></td>
                        <td align="left" ><strong>:</strong></td>
                        <td align="left"  style="border-right:2px solid #c6c6c6; padding-right:20px;" ><%#OrderWorkLibrary.Encryption.Decrypt(ViewState("BankAccountNo"))%></td>
                      </tr>
                      <tr runat="server" visible="false">
                        <td align="left" ><strong>Bank</strong></td>
                        <td align="left" ><strong>:</strong></td>
                        <td align="left"  style="border-right:2px solid #c6c6c6; padding-right:20px;" ><%#GetBankDetails()%></td>
                      </tr>
                      <tr>
                        <td align="left" ><strong>Reference Number&nbsp; </strong></td>
                        <td align="left" ><strong>:</strong></td>
                        <td align="left" style="border-right:2px solid #c6c6c6; padding-right:20px;"  ><%#Container.DataItem("InvoiceNo") %></td>
                      </tr>
                      <tr>
                        <td height="30" align="left">&nbsp;</td>
                        <td height="30" align="left" >&nbsp;</td>
                        <td height="30" align="left" >&nbsp;</td>
                      </tr>
                  </table></td>
                  <td valign="top">
                  <asp:Repeater ID="rptInvoiceVar" DataSource='<%#getInvoiceVariables(Container.DataItem("InvoiceNo"))%>' runat="server">
                  <ItemTemplate>
                  <table cellspacing="0" cellpadding="0" border="0" width="100%">
	                <tr valign="top">
		              <td height="40"  width="120px"  class="gridTextNew bdr2">&nbsp;&nbsp;</td>
		              <td width="5" class="gridTextNew bdr2">&nbsp;</td>
		              <td  class="gridTextNew bdr2">&nbsp;</td>
		            </tr>
	               <tr  valign="top" runat="server"  visible='<%# IIF((Container.DataItem("Field1Enabled")), True,False)%>'>
	                <td style="padding-left:20px;"><b><%#Container.DataItem("Field1Label")%></b></td>
	                <td width="5" >:</td>
	                <td><%#Container.DataItem("Field1Value") %></td>
	               </tr>
	               <tr  valign="top" runat="server"  visible='<%# IIF(Container.DataItem("Field2Enabled"), True,False)%>'>
	                <td style="padding-left:20px;"><b><%#Container.DataItem("Field2Label")%></b></td>
	                <td width="5">:</td>
	                <td><%#Container.DataItem("Field2Value")%></td>
	               </tr>
	               <tr  valign="top" runat="server" visible='<%# IIF(Container.DataItem("Field3Enabled"), True,False)%>'>
	                <td style="padding-left:20px;"><b><%#Container.DataItem("Field3Label")%></b></td>
	                <td width="5">:</td>
	                <td><%#Container.DataItem("Field3Value")%></td>
	               </tr>
	               <tr  valign="top" runat="server"  visible='<%# IIF(Container.DataItem("Field4Enabled"), True,False)%>'>
	                <td style="padding-left:20px;"><b><%#Container.DataItem("Field4Label")%></b></td>
	                <td width="5">:</td>
	                <td><%#Container.DataItem("Field4Value")%></td>
	               </tr>
                 </table>
                 </ItemTemplate>
                  </asp:Repeater>   
                  </td>
                </tr>
              </table>
              </td>
              <td width="45" align="right" valign="top">&nbsp;</td>
              </tr>
              
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlReceipt" Visible='<%#IIF(Container.DataItem("AdviceType") = "UA Receipt",True,False)%>'>
              <tr>
                <td width="45" height="180" align="center">&nbsp;</td>
                <td width="650" height="200" align="center"><p class="invoiceLabel" >RECEIPT</p>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td align="left" valign="top"  style="border-bottom: 0px; border-right-width: 0px"><strong>Invoice To:</strong><br />
                          <%#GetBillingAddress(Container.DataItem("InvoiceNo"))%>
                        <td align="right" valign="top">&nbsp;</td>
                      </tr>
                    </table>
                    <p>&nbsp;</p>
                    <table border="0" align="right" cellpadding="0" cellspacing="0">
                      <tr>
                        <td
						      width="110" height="30" align="left"  style="border-bottom: 0px; border-right-width: 0px"><strong> Account No.</strong></td>
                        <td align="right"  style="border-bottom: 0px"><strong> Date</strong></td>
                      </tr>
                      <tr>
                        <td align="left" style="border-right: 0px"><%# Container.DataItem("AccountNo") %>&nbsp;</td>
                        <td align="right">&nbsp; <%# Strings.FormatDateTime(Container.DataItem("InvoiceDate").ToString(), DateFormat.ShortDate) %></td>
                      </tr>
                      <tr>
                        <td align="left" style="border-right: 0px">&nbsp;</td>
                        <td align="right">&nbsp;</td>
                      </tr>
                    </table>
                    <p>&nbsp;</p></td>
                <td width="45" height="180" align="center">&nbsp;</td>
              </tr>
              <tr>
                <td align="right" valign="top">&nbsp;</td>
                <td align="center" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-weight: bold">
                    <tr>
                      <td height="30" align="left"  style="width: 345px;border-right-width: 0px"><strong>Description</strong> </td>
                      <td height="30" align="right" ><strong> Amount</strong> </td>
                    </tr>
                    <asp:Repeater ID="rptListReceipt" runat="server" Visible="true">
                      <itemtemplate>
                        <tr>
                          <td align="left"  style="width: 345px; border-top: 0px;border-right-width: 0px;font-weight:normal"><%#Container.DataItem("Description")%> </td>
                          <td align="right"  style="border-top: 0px;font-weight:normal"><%#FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%></td>
                        </tr>
                      </itemtemplate>
                    </asp:Repeater>
                </table></td>
                <td align="right" valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td width="45" align="right" valign="top">&nbsp;</td>
                <td width="650" height="100" align="right" valign="top"><table width="551" border="0" cellpadding="5" cellspacing="0" style="height: 1px" >
                    <tr>
                      <td width="323" height="30" align="left"  style="border-top-width: 0px;  border-right-width: 0px"></td>
                      <td width="104" align="right"  style="border-top-width: 0px;  border-right-width: 0px"><strong>Total Amount </strong> </td>
                      <td align="right"  style="border-top-width: 0px; "><span class="style6"> <%#FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%> </span> </td>
                    </tr>
                </table></td>
                <td width="45" align="right" valign="top">&nbsp;</td>
              </tr>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlCreditNote" Visible='<%#IIF(Container.DataItem("AdviceType") = "Credit Note",True,False)%>'>
              <tr>
                <td width="45" height="180" align="center">&nbsp;</td>
                <td width="650" height="180" align="center"><p class="invoiceLabel">Credit Note</p>
                    <table border="0" cellpadding="5" cellspacing="0" align="left" >
                      <tr>
                        <td align="left" valign="top"  style="border-bottom: 0px; border-right-width: 0px"><strong>Invoice To:</strong><br />
                          <%#GetBillingAddress(Container.DataItem("InvoiceNo"))%></td>
                        <td align="right" valign="top">&nbsp;</td>
                      </tr>
                    </table>
                    <table border="0" align="right" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="left" width="100" style="border-bottom: 0px; border-right-width: 0px"><strong> Account No.</strong></td>
                        <td width="140" align="left" >:&nbsp;<%# Container.DataItem("AccountNo") %> &nbsp;</td>
                      </tr>
                      <tr>
                        <td align="left" style="border-bottom: 0px"><strong>Credit No.</strong></td>
                        <td align="left">:&nbsp;<%#Container.DataItem("InvoiceNo")%></td>
                      </tr>
                      <tr>
                        <td align="left" style="border-bottom: 0px"><strong> Date</strong></td>
                        <td align="left">:&nbsp;<%# Strings.FormatDateTime(Container.DataItem("InvoiceDate").ToString(), DateFormat.ShortDate) %></td>
                      </tr>
                      <tr>
                        <td align="left" style="border-bottom: 0px"><strong>Invoice Number</strong></td>
                        <td align="left">:&nbsp;<%#Container.DataItem("SalesInvoiceNo")%></td>
                      </tr>
                    </table>
                    <p>&nbsp;</p></td>
                <td width="45" height="180" align="center">&nbsp;</td>
              </tr>
              <tr>
                <td align="right" valign="top">&nbsp;</td>
                <td><table cellpadding="0" cellspacing="0" width="100%" style="margin-top:15px;">
                    <tr height="30" valign="top">
                      <td width="100"><strong>Date</strong></td>
                      <td><strong>Description</strong></td>
                      <td width="5">&nbsp;</td>
                      <td align="left" width="70"><strong>YourRef</strong></td>
                      <td align="right" width="80"><strong>Net Price</strong></td>
                        <td align="right" width="80"><strong>VAT (<%#OrderWorkLibrary.ApplicationSettings.VATPercentage(Container.DataItem("NetVat"), Container.DataItem("NetAmount")) %>%)</strong></td>
                      <td align="right" width="80"><strong>Amount</strong></td>
                    </tr>
                    <asp:Repeater ID="rptListCN" runat="server" Visible="true">
                      <itemtemplate>
                        <tr>
                          <td align="left"><%#Strings.FormatDateTime(Container.DataItem("CloseDate"), DateFormat.ShortDate)%></td>
                          <td width="200" align="left" ><%#Container.DataItem("Description")%></td>
                          <td width="5">&nbsp;</td>
                          <td align="left" ><%#Container.DataItem("PONumber")%></td>
                          <td align="right" ><%#FormatCurrency(Container.DataItem("InvoiceNetAmnt"), 2, TriState.True, TriState.True, TriState.False)%></td>
                          <td align="right" ><%#Container.DataItem("VAT")%></td>
                          <td align="right" ><%#Container.DataItem("Total")%></td>
                        </tr>
                      </itemtemplate>
                    </asp:Repeater>
                </table></td>
                <td align="right" valign="top">&nbsp;</td>
              </tr>
              <tr height="15">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td width="45" align="right" valign="top">&nbsp;</td>
                <td width="650" height="100" align="right" valign="top"><table border="0" cellspacing="0">
                    <tr>
                      <td width="140" align="right"  style=" border-bottom:0px; border-right-width:0px; "><strong>Sub-Total</strong></td>
                      <td width="80" align="right"  style=" border-bottom:0px; "><%# Container.DataItem("NetAmount") %> </td>
                    </tr>
                    <tr>
                       <td width="140" height="20" align="right"   style=" border-bottom:0px; border-right-width:0px; "><strong>Total VAT (<%#OrderWorkLibrary.ApplicationSettings.VATPercentage(Container.DataItem("NetVat"), Container.DataItem("NetAmount")) %>%)</strong></td>
                      <td width="80" height="20" align="right"  style=" border-bottom:0px; "><%#Container.DataItem("NetVat")%> </td>
                    </tr>
                    <tr>
                      <td width="140" height="50" align="right"   style="border-right-width:0px; "><strong>Total Amount </strong></td>
                      <td width="80" height="40"  align="right" ><span class="style6"> <%#Container.DataItem("Total")%> </span> </td>
                    </tr>
                </table></td>
                <td width="45" align="right" valign="top">&nbsp;</td>
              </tr>
            </asp:Panel>
            <tr>
              <td width="45" height="60" align="center">&nbsp;</td>
              <td width="650" height="60" align="center" class="bdr3 style3" valign="top" style="padding-top:10px;"><strong>Registered Office: </strong>Montpelier Chambers, 61-63 High Street South, Dunstable, Bedfordshire LU6 3SF<br />
                  <strong>
                  <asp:Label ID="lblRegistartionNo" runat="server" Text='<%# ViewState("RegistartionIN") %>'></asp:Label>
          :</strong>&nbsp;
          <asp:Label ID="lblRegistartionNoLine2" runat="server" Text='<%# ViewState("RegistartionNo") %>'></asp:Label>
              </td>
              <td width="45" valign="top">&nbsp;</td>
            </tr>
          </table>
          <div id="divPageBreak" runat="server" class="pb">&nbsp;</div>
        </itemtemplate>
      </asp:Repeater>    
      </asp:Panel>

  </form>
</body>
</html>
