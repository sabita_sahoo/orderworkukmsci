<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/MyOrderWork.Master" CodeBehind="ViewWONotes.aspx.vb" Inherits="OrderWorkUK.ViewWONotes" ValidateRequest="true"
    title="View WO Notes" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">
<style type="text/css">
<!--
.cursorHand
{
	cursor: hand;
}
.bottonTopGrey {
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-TR.gif) no-repeat right top;
	height:4px;
}
.bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
}
*+html .bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
	margin-top:-2px;
}
#divButton {
background-color:#999;
width:56px;
height:19px;
margin:10px 0 0;
padding:0;
}
img.btnCorner {
   width: 4px;
   height: 4px;
   border: none;
   display: block !important;
}
.buttonText {
font-family:Verdana;
font-size:11px;
color:#FFF;
text-decoration:none;
padding-left:11px;
padding-right:8px;
font-style:normal;
font-weight:400;
font-variant:normal;
line-height:11px;
}

.buttonText:hover {
color:#3F3F3F;
text-decoration:none;
}
.gridHdr
{
	background-color:#993366;
	font-family:Verdana;
	font-size: 11px;
	font-weight: bold;
	color: #ffffff;
	text-decoration: none;
	height:28px;
	vertical-align:middle;
	text-align:left;
	padding-left: 2px;
}
.gridRow
{
	background-color:#F7F3F4;
	font-family:Verdana;
	font-size: 11px;	
	color: #555354;
	text-decoration: none;
	padding-left:6px;
	padding-right:6px;
	padding-top: 8px;
	margin-left:6px;
	margin-top:8px;	
	border:solid 1px #DFDFDF;
}
-->
</style>

<div class="divContainerStyle floatLeft" id="divTopBand1" >
	<div class="WOProcessTopHeader" id="divPaymentMethod"><strong>WO Notes</strong></div>
	<div id="divMiddleBand" class="padding10">
	<asp:UpdatePanel ID="updatePnlSuppWOdetails" runat="server"  RenderMode=Inline>
  <ContentTemplate>
	<asp:GridView ID="gvWONotes" runat="server" AllowPaging="False" AutoGenerateColumns="False" BorderColor="#E2E1DD" BorderWidth="1px" 
       Width="1000" AllowSorting="true" GridLines="Both" RowStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr">
            
                               
              
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               
               
        
               <Columns>  
                   
               <asp:TemplateField HeaderText="Date Created" ItemStyle-Width="20%" HeaderStyle-CssClass="txtGrid" ItemStyle-CssClass="txtGrid" ItemStyle-Height="40px">
               <ItemTemplate>
                            <%# Strings.Format(Container.DataItem("DateCreated"), "dd/MM/yyyy <br /> hh:mm tt")%>
               </ItemTemplate>
               
               </asp:TemplateField>
               
                <asp:TemplateField HeaderText="Written By" ItemStyle-Width="30%" HeaderStyle-CssClass="txtGrid" ItemStyle-CssClass="txtGrid" ItemStyle-Height="40px">
               <ItemTemplate>
                            <%#Container.DataItem("OWRepFname")%> <%#Container.DataItem("OWRepLname")%>
               </ItemTemplate>
               
               </asp:TemplateField>
               
               
                <asp:TemplateField HeaderText="Notes" ItemStyle-Width="50%" HeaderStyle-CssClass="txtGrid" ItemStyle-CssClass="txtGrid" ItemStyle-Height="40px">
               <ItemTemplate>
                            <%# Container.DataItem("Comments")%>
               </ItemTemplate>
               
               </asp:TemplateField>
              
                  
                </Columns>
               
                           
        </asp:GridView>
        <br />            
        <div style="margin-left:10px;">
        <p class="clsColorRed"><b>
        <!--This feature is intended for your company use only and NOT as a means of communicating with OrderWork.
        <br />-->
        Please untick the checkbox to not to show the entered notes to the OrderWork Team.</b></p>
        <asp:CheckBox ID="chkShowOrderWork" runat="server" Font-Bold="true" Checked="true" text="Show Notes To OrderWOrk" />
        <br /><br />
                    <asp:TextBox runat="server" TextMode="MultiLine"    onblur="javascript:removeHTML(this.id);" ID="txtNote" Height="50" Width="500" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtNote" WatermarkText="Enter a new note here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNote" runat="server" ErrorMessage="<b>Required Field Missing</b><br>Please enter a note" ControlToValidate="txtNote" Display="None"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegExReceiptNo" runat="server" ControlToValidate="txtNote" ErrorMessage="Error with the Client Reference field: HTML tags not allows (remove any < and > characters from the field)" ValidationExpression="^[^<>]+$" Display="None"></asp:RegularExpressionValidator>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidatorNote"></cc1:ValidatorCalloutExtender>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RegExReceiptNo"></cc1:ValidatorCalloutExtender>
                    <br /><br />
                    <div>
                     
                     <div id="divbtn" style="float:Left;margin-left:0px;" class="clsButtonGreyDetails roundifyAddressForIE"><a id="ancSubmit" runat="server" class="txtListing">
                     <%-- 'Poonam modified on 29/07/2015 - OM-1 : OW-DEV-ISSUE-MY Broken image SupplierWODetails.aspx--%>
                      <img src="../OWLibrary/Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;<b>Submit</b>&nbsp;</a></div>
                     <div id="divbtnBack" style="float:Left;margin-left:20px;" class="clsButtonGreyDetails roundifyAddressForIE"><a id="ancBack" href="Welcome.aspx?User=Supplier" runat="server" class="txtListing" >
                     <%-- 'Poonam modified on 29/07/2015 - OM-1 : OW-DEV-ISSUE-MY Broken image SupplierWODetails.aspx--%>
                      <img src="../OWLibrary/Images/Icons/Icon-Back.gif" width="11" height="11" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;<b>Back</b>&nbsp;</a></div>
                   
						
						</div>
                </div>
	
		<div id="divClearBoth"></div>
		 </ContentTemplate>
</asp:UpdatePanel>  
</div>
	</div>

</asp:Content>

