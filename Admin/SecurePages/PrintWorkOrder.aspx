<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintWorkOrder.aspx.vb" Inherits="OrderWorkUK.PrintWorkOrder" ValidateRequest="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Order Work: Print Work Order</title>
    <link href="../Styles/OWPortal.css" rel="stylesheet" type="text/css" />
    <style>
    body {
    background-color: #CECECE;
     }
    
    </style>
</head>
<body bgcolor="#F4F5F0" onload="javascript:this.print()">
    <form id="form1" runat="server">
    <div id="divPrintWOBody">
    	<div id="divPrintWOHeader">
			<div id="divPrintWOHeaderTopBand"></div>
			<div id="divPrintWOHeaderMiddleBand">
			<asp:Panel ID="pnlOWLogo" runat="server">
			<a href="Welcome.aspx"><img src="~/OWLibrary/Images/OrderWorks-Logo-Grey.gif" runat="server" align="left" class="imgLogo" alt="OrderWork - Service Reinvented" border="0"/></a>
			</asp:Panel>
			<asp:Panel ID="pnlClientLogo" runat="server">
			<asp:Image CssClass="clsClientLogo" runat="server" SafeEncode="false" id="clientLogo" visible="false" ImageUrl="../../Images/ImgSecurePages/Logo.gif" style="height:37px;border-width:0px;" />
			</asp:Panel>
			    <div style="clear:both;"></div>
			</div>
			
			<div id="divPrintWOHeaderBtmBand"></div>
		</div>
		<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
		<div id="divPrintWOContent">
				<div id="divPrintWOContentTopBand"></div>
				<div id="divPrintWOContentMiddleBand">
					<table border="0" cellpadding="0" cellspacing="0" align="center">
					  <tr>
						<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
						<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
					  </tr>
					</table>
				</div>
				<div id="divPrintWOContentBtmBand"></div>
			</div>			
		</asp:Panel>		
		<asp:Panel runat="server" ID="pnlPrintContent" Visible="true">
			<div id="divPrintWOContent">
				<div id="divPrintWOContentTopBand"></div>
				<div id="divPrintWOContentMiddleBand">
					<div id="divPrintWOTopInfo">
						<div style="float:left; width:480px;">
						<div class="clsPrintWORow"><div class="clsPrintWOLeft"><b>Work Order No.</b></div><div class="clsPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label></div></div>
						<div class="clsPrintWORow"><div class="clsPrintWOLeft"><b>Work Order Title</b></div>
									<div class="clsPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" SafeEncode="false" ID="lblTitle"></asp:Label></div></div>																				
						<asp:Panel runat="server" ID="pnlPrice"><div class="clsPrintWORow" id="divPrintWORow" style="width:490px;" runat="server"><div class="clsPrintWOLeft"><b><asp:Label runat="server" CssClass="formTxt" ID="lblProposedPriceHeader"></asp:Label></b></div><div class="clsPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblProposedPrice"></asp:Label></div></div></asp:Panel>
						<div class="clsPrintWORow"><div class="clsPrintWOLeft"><b>Location</b></div><div class="clsPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblLocation"></asp:Label></div></div>
						<asp:panel id="pnlPONumber" runat="Server" >
						<div class="clsPrintWORow"><div class="clsPrintWOLeft"><b><asp:Label ID="lblPONumberSummary" runat="server" Text="PO Number"></asp:Label></b></div><div class="clsPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblPONumber"></asp:Label></div></div></asp:panel>
						<div class="clsPrintWORow"><div class="clsPrintWOLeft"><b><asp:Label runat="Server" ID="lblStartDateLabel"></asp:Label></b></div><div class="clsPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblStartDate"></asp:Label></div></div>
						<asp:panel id="pnlEndDate" runat="Server">
						<div class="clsPrintWORow"><div class="clsPrintWOLeft"><b>End Date</b></div><div class="clsPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt" ID="lblEndDate"></asp:Label> </div></div></asp:panel>
						<asp:panel id="pnlBuyerCompany" runat="Server">
						<div  class="clsPrintWORow"><div class="clsPrintWOLeft"><b class="redTextvLarge">Client Name</b></div>
													<div  class="clsPrintWORight">:&nbsp;<asp:Label runat="server" CssClass="formTxt redTextvLarge" ID="lblBuyerCompany"></asp:Label> </div></div></asp:panel>
						<div class="clsPrintWORow"><div class="clsPrintWOLeft"><b class="redTextvLarge">&nbsp;</b></div><div class="clsPrintWORight">&nbsp;&nbsp;</div></div>
						</div>
						<asp:Image SafeEncode="false" style="height:37px;border-width:0px;"  runat="server" id="ClientLogoForSP" visible="false" ImageUrl="../../Images/ImgSecurePages/Logo.gif" />
						<div id="divClearBoth"></div>
					</div>
					
					<div id="divClearBoth"></div>
					<div id="divPrintWorkLocation" style="border-bottom:solid 1px #E1E1DF; padding-top:15px; padding-bottom:15px; margin-left:20px; margin-right:20px;" runat="server">
						<asp:Panel ID="pnlWorkLocation" runat="server">
						    <div id="PrintWOLocation">
							    <b><asp:Label runat="server" ID="lblLocHeading" Text="Work Location:"></asp:Label></b><br />
							    <asp:Label ID="lblWorkLocAddrDetails" Width="250" runat="server"></asp:Label>
							</div>
							<div id="PrintWOLocationOfGoodsContainer">
							    <div id="PrintWOLocationOfGoods" runat="Server">
							        <b><asp:Label runat="server" ID="lblGoodsLocationLabel"></asp:Label>:</b><br />
							        <span id="spnLocationName" runat="Server">
							            <div id="divPWORow"><div id="divPWOLeft">Name</div><div id="divPWORight">:&nbsp;<asp:Label ID="lblDepotName" runat="server"></asp:Label></div></div>
							        </span>
							        <div id="divPWORow"><div id="divPWOLeft">Address</div><div style="float:left">:&nbsp;</div><div id="divPWORight"><asp:Label SafeEncode="false" ID="lblBusinessDivision" runat="server"></asp:Label></div></div>							    
						        </div>							
							    <div id="divPWORow"><div id="divPWOLeft"><asp:Label runat="Server" ID="lblProductsLabel"></asp:Label></div><div style="float:left">:&nbsp;</div><div id="divPWORight"><asp:Label ID="lblProducts" runat="server"></asp:Label></div></div>
							    
							    <span id="SpanJobNumber" runat="server"><div id="divPWORow"><div id="divPWOLeft">Job Number</div><div style="float:left">:&nbsp;</div><div id="divPWORight"><asp:Label ID="lblJobNumber" runat="server"></asp:Label></div></div></span>
							    <div></div>
							    	
						    </div>					    
						</asp:Panel>
						<div id="divClearBoth"></div>
					</div>
					
					<div id="divPrintWOSpecialInstruction" style="border-bottom:1px solid #E1E1DF;">
						<div id="PrintWOAdditionalInfo" style="display:none;">
							<b>Additional Info:</b><br />
							<div id="div1"><div id="div2" style="width:79px;">WO Type</div><span style="float:left;">:&nbsp;</span><div id="div3" style="width:100px;"><asp:Label ID="lblCategory" runat="server" style="line-height:14px;"></asp:Label></div></div>
							<span runat="Server" id="spnJobNumber"><div id="div4"><div id="div5" style="width:79px;">Job Number</div><div style="float:left">:&nbsp;</div><div id="div6" style="width:100px;"></div></div></span>
							<div id="div7"><div id="div8" style="width:79px;">Dress Code</div><div style="float:left">:&nbsp;</div><div id="div9" style="width:100px;"><asp:Label ID="lblDressCode" runat="server"></asp:Label></div></div>
							<div id="div10"><div id="div11" style="width:79px;">Supply Parts</div><div style="float:left">:&nbsp;</div><div id="div12" style="width:100px;"><asp:Label ID="lblSupplyParts" runat="server"></asp:Label></div></div>
							<span runat="Server" id="spnSalesAgent"><div id="div13"><div id="div14" style="width:79px;">Sales Agent</div><div style="float:left">:&nbsp;</div><div id="div15" style="width:100px;"><asp:Label ID="lblSalesAgent" runat="server"></asp:Label></div></div></span>
						</div>
						<asp:Panel ID="pnlSpecialInst" runat="server">
						    <asp:Panel ID="pnlCustSpecialInst" runat="server">
						        <b><asp:label runat="server" ID="lblCustSpecialInstrLabel"></asp:label></b><br /><br/>
						        <div runat="server" id="divCustSpecialInstructions"> 
							    <asp:Label ID="lblCustSpecialInstructions" runat="server"></asp:Label>
                                </div>
						    </asp:Panel>							
						    <asp:Panel ID="pnlOWSpecialInst" runat="server">
							    <b><asp:label ID="lblOWSpecialInst" runat="server" Text="OrderWork Special Instructions:"></asp:label></b><br /><br/>
						        <div runat="server" id="divSpecialInstructions"> 
							    <%--<asp:Label ID="lblSpecialInstructions" runat="server"></asp:Label>--%>
                                </div>
						    </asp:Panel>
						</asp:Panel>
						<div id="divClearBoth"></div>
					</div>		
					<div id="divClearBoth"></div>
					<asp:panel ID="pnlScopeOfWork" runat="server">
					<div id="divPrintWODesc" style="border-bottom:0px solid #fff">
						<b>Scope of Work:</b><br /><br/>
					<div runat="server" id="divScopeOfWork"> 
						<%--<asp:Label ID="lblLongDesc" SafeEncode="false" runat="server"></asp:Label>--%>
                        </div>
						<div id="divClearBoth"></div>
					</div>
					</asp:panel>
					<div id="divClearBoth"></div>
					<asp:Panel ID="pnlClientScope" runat="server" Visible="false">
					<div id="divPrintClientScope">
						<b>Scope of Work:</b><br /><br/>
					<div runat="server" id="divClientScope">  
						<%--<asp:Label ID="lblClientscope" SafeEncode="false" runat="server"></asp:Label>--%>
                        </div>
						<div id="divClearBoth"></div>
					</div>
					</asp:Panel>
                    <div id="divClearBoth"></div>
					<asp:Panel ID="pnlQuestionnaireInfo" runat="server" Visible="false">
					<div id="divPrintWODesc" style="border-bottom:0px solid #fff">
						<b>Questionnaire:</b><br />
						<asp:Label ID="lblQuestionnaireInfo" SafeEncode="false" runat="server"></asp:Label>
						<div id="divClearBoth"></div>
					</div>
					</asp:Panel>
					<div id="divClearBoth"></div>
											
				</div>
				<div id="divPrintWOContentBtmBand"></div>
			</div>
		</asp:Panel>
		<div id="divPrintWOFooter">
			<div id="divPrintWOFooterTopBand"></div>
			<div id="divPrintWOFooterMiddleBand">
				<table width="915"  height="36"  border="0" align="center" cellpadding="0" cellspacing="0" class="BdrTop"> 
				  <tr valign="top">
					<td width="140" align="left" class="footerTxt" style="text-align:left;" ><img src="../OWLibrary/Images/Phone-Icon.gif" title="Phone" alt="Phone" width="15" height="12" align="absmiddle"/> <asp:Label ID="lblPhoneNumber" runat="server" Text="0203 053 0343"></asp:Label> </td>
					<td width="606" align="right" class="footerTxt" runat="server" id="tdEmail" ><a href="mailto:info@orderwork.co.uk" class="footerTxtSelected"><img src="../OWLibrary/Images/Email-Icon.gif" title="Contact Us" width="16" height="15" border="0" align="absmiddle" class="marginR5" />info@orderwork.co.uk</a>&nbsp;</td>
					<td width="214" align="left" class="footerTxt">
						
					</td>
				  </tr>
				</table>
			</div>
			<div id="divPrintWOFooterBtmBand"></div>
		</div>
    </div>
    </form>
</body>
</html>
