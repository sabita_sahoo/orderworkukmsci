<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWIT.Master"
    CodeBehind="secureLogin.aspx.vb" EnableViewState="false" EnableViewStateMac="false"
    Inherits="OrderWorkUK.secureLogin" Title="OrderWork - Services Reinvented - Secure Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
    <link rel="stylesheet" href="OWLibrary/Styles/font-awesome.min.css">
    <link rel="stylesheet" href="OWLibrary/Styles/bootstrap.min.css">
    <link rel="stylesheet" href="OWLibrary/Styles/newStyle.css">
    <style type="text/css">
    .divFooter, .headerpd
    {
        display:none !important;
    }
    .divSeperator{padding:0 !important;}
</style>
 <script language="javascript" type="text/JavaScript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/getusermedia-js@1.0.0/dist/getUserMedia.min.js"></script>
    <script src="OWLibrary/JS/WaterMark.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function CheckKeyPress(evt) {
            try {
                if (window.event && window.event.keyCode == 13) {
                    //alert('enter key pressed, evt:' + evt);
                    document.getElementById("ctl00_ContentPlaceHolder1_hdnbtnLogin").click();
                    event.returnValue = false;
                    event.cancelBubble = true;
                }
                else if (evt == 13) {
                    //Non IE browsers
                    document.getElementById("ctl00_ContentPlaceHolder1_hdnbtnLogin").click();
                    evt.cancelBubble = true;
                    evt.stopPropagation();
                    return false;
                }
            }
            catch (ex) {
                //alert(ex.message);
            }
        }
        //Script required for the welcome page
        function showPasswordFld(defaultText) {
            if (document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword"))
                if (document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword").value == "") {
                    document.getElementById("txtLoginPasswordText").className = "passwordFieldHidden";
                    document.getElementById("txtLoginPasswordText").value = defaultText;
                    document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword").className = "passwordField";
                    document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword").focus();
                }
        }
        function hidePasswordFld(defaultText) {
            if (document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword")) {
                if (document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword").value == "") {
                    document.getElementById("txtLoginPasswordText").className = "passwordField";
                    document.getElementById("txtLoginPasswordText").value = defaultText;
                    document.getElementById("ctl00_ContentPlaceHolder1_txtLoginPassword").className = "passwordFieldHidden";
                }
            }
        }
        
        jQuery(function () {
            //jQuery("[id*=ctl00_ContentPlaceHolder1_txtLoginPassword]").WaterMark();

            //To change the color of Watermark
            jQuery("[id*=ctl00_ContentPlaceHolder1_txtLoginPassword]").WaterMark(
            {
                WaterMarkTextColor: '#878787'
            });
        });            
    </script>
    <!---Changed and Added below code to solve IE issue (jquery not loading) Shrutika 04 Oct 2018---->
    <script type="text/javascript">
        window.console = window.console || { log: function () { } };
        if (typeof console === "undefined") {
            console = {};
            console.log = function () { };
        }
        jQuery(document).ready(function () {
            console.log("ready!");
            if (jQuery) {
                //setTimeout(function () {
                    var windowHeight = jQuery(window).innerHeight();
                    console.log("Window Height", windowHeight);
                    var windowWidth = jQuery(window).innerWidth();

                    if (windowWidth < 500) {
                        windowHeight = windowHeight / 2;
                        jQuery('.image-content').css('top', '60%');
                        jQuery('.text-main-content').css('font-size', '1.5em');
                        jQuery('.text-sub-content').css('font-size', '1em');

                    } else {
                        windowHeight = windowHeight - 48;
                    }
                    if (windowWidth > 500) {
                        console.log(windowHeight);
                        console.log(jQuery('.image'));
                        jQuery('.image').css('min-height', windowHeight);
                        jQuery('.information').css('min-height', windowHeight);
                        jQuery('.information').css('max-height', windowHeight);
                    }

                    var images = [{ image: "45160380.jpg", mainContent: "earn in your spare time with small local jobs.", subContent: " register to become orderwork professional" },
                            { image: "painter-2247395.jpg", content: "" }, { image: "building-2748840.jpg", content: "" }, { image: "electrician-1080554.jpg", content: "" },
                            { image: "installation-872778.jpg", content: "" }, { image: "boiler-1816642.jpg", content: "" }, { image: "plumber-228010.jpg", content: "" },
                            { image: "service-428539.jpg", content: "" }, { image: "woman-1484279.jpg", content: ""}];

                    setInterval(function () {
                        var index = Math.round(Math.random() * (images.length - 1));
                        var currentImage = 'OWLibrary/Images/Photos/' + images[index].image;
                        jQuery('.image').css('background-image', "url('" + currentImage + "')");

                        //console.log(this.currentImage, index);
                    }, 8000)
                //}, 500);
            }
            else {
                location.reload();
            }
        });
    </script>
    <!---END-->
    <div id="divLoginBodyInner">
        <div class="divLoginBodyNew">
            <div class="col-md-12 col-xs-12 col-sm-12 padding0" style="background-color: #fff;
                margin: 15px 0px 15px 0px;">
                <div class="col-md-5 col-sm-12 col-xs-12 col-lg-5 padding0 images  hidden-xs hidden-sm">
                    <div class="image" style="background-image: url('OWLibrary/Images/Photos/boiler-1816642.jpg');">
                    </div>
                    <div class="image-content">
                        <span>
                            <span class="text-main-content">Welcome to <br /> MyOrderWork </span>
                            <span class="text-sub-content">Installation and repair<br />services made simple </span>
                        </span>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12 col-xs-12 col-lg-7 col rightsec padding0 information" style="min-height: 565px; max-height: 565px;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-6 col-sm-6 col-xs-6  hidden-xs hidden-sm" style="margin-top: 25px;">
                            <span class="heading common-heading"></span>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-2 padding0 rtlogo" style="margin-top: 25px;">
                            <a href="http://orderwork.co.uk/" title="MyOrderwork"><img alt="orderwork" style="height: 60px; width: auto; float:right;" src="OWLibrary/Images/Logo-MyOW-client.jpg"></a>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 error-message text-center" style="margin-bottom: 35px">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 fix-bottom">
                        <div class="col-md-6 col-sm-6 col-xs-12 fix-bottom padding0 login-section">
                            <div class="col-md-12 col-sm-12 col-xs-12 fix-bottom login-top">
                                <span class="login-heading">Sign In</span><br /> 
                                <span class="login-subheading">Sign in to your account</span>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 fix-bottom">
                                <asp:UpdatePanel ID="UpdatePnlLogin" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                    <ContentTemplate>
                                        <%--<div id="HmMain">--%>
                                            <div id="divLoginMain" class="divLoginMainNew">
                                                <div id="divLogin" class="divLoginNew">
                                                    <asp:Panel ID="pnlLogin" runat="server" DefaultButton="lnkBtnLogin">
                                                        <div id="divLoginRight">
                                                            <%--<img src="OWLibrary/Images/LoginUpperband.jpg" />--%>
                                                            <div id="divRightImage">
                                                                <div id="divLoginInner">
                                                                    <%--<div><b>Log into your account</b></div>
                                                                    <div class="displaynone">&nbsp;&nbsp;</div>--%>
                                                                    <div>
                                                                        <asp:TextBox ID="txtLoginUserName" TabIndex="1" ValidationGroup="FP" runat="server"
                                                                            CssClass="TxtBx"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender TargetControlID="txtLoginUserName" ID="TextBoxWatermarkExtender1"
                                                                            WatermarkText="Your email" runat="server" />
                                                                        <div class="displaynone">&nbsp;&nbsp;</div>
                                                                        <div class="col-md-12 col-sm-12 col-xs-12 padding0">
                                                                            <%--poonam modified on 5/5/2015 - Task - 4441558 EMOW-LIVE-ISSUE-MY - Login Password Field displays Characters--%>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12 padding0">
                                                                                <asp:TextBox  TabIndex="2" ID="txtLoginPassword" CssClass="passwordField TxtBx" runat="server" 
                                                                                    TextMode="Password"  ToolTip="Enter password" ></asp:TextBox>
                                                                                     <%--<cc1:TextBoxWatermarkExtender TargetControlID="txtLoginPassword" ID="TextBoxWatermarkExtender2"
                                                                            WatermarkText="Enter password" runat="server"></cc1:TextBoxWatermarkExtender>--%>
                                                                            </div>
                                                                            <div class="displaynone">&nbsp;&nbsp;</div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12 padding0">
                                                                                <asp:CheckBox ID="chkRemember" Text="Remember me" CssClass="ChechBox" TabIndex="3" runat="server" />
                                                                                <div class="displaynone">&nbsp;&nbsp;</div>
                                                                                <a href="ForgotPassword.aspx" class="forgotPassword" tabindex="5">Forgot password?</a>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12 padding0" style="padding-top: 25px;">
                                                                                <asp:LinkButton runat="server" ID="lnkBtnLogin" TabIndex="4" Text="Sign In" OnClick="LoginMe"
                                                                                    CausesValidation="True" CssClass="FPasswordBtn SignInTextBx" ToolTip="Sign In" />
                                                                                
                                                                                <asp:Button runat="server" ID="hdnbtnLogin" TabIndex="4" Width="0" Height="0" BorderWidth="0"
                                                                                    BorderColor="#F8F8F8" Text="" OnClick="LoginMe" CausesValidation="True" CssClass="displaynone" />
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12 padding0">
                                                                                <asp:LinkButton runat="server" ID="Register" TabIndex="4"  CssClass="SignInTextBx RegisterTextBx" Text="Register" OnClick="RegisterMe" ToolTip="Register" />
                                                                            </div>
                                                                            <div class="displaynone">&nbsp;&nbsp;</div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="divValidationMain" class="PopUpTailDiv" runat="server" visible="false">
                                                                        <img src="OWLibrary/Images/PopupTail.gif" class="PopTail">
                                                                        <img src="OWLibrary/Images/Note_Image.jpg" align="left">
                                                                        <div class="errMsg">
                                                                            <asp:Label ID="lblMsg" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                                                                            <br/>
                                                                            <a runat="server" id="aLoginFailed">myorderwork platform</a>
                                                                            <asp:ValidationSummary ID="validationSummarySubmit" ShowSummary="true" ValidationGroup="FP"
                                                                                EnableClientScript="true" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="List">
                                                                            </asp:ValidationSummary>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both;">
                                       <%-- </div>--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePnlLogin"
                                    runat="server">
                                    <ProgressTemplate>
                                        <div>
                                            <img align="middle" src="OWLibrary/Images/indicator.gif" />
                                            Verifying Credentials ...
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="PnlLogin"
                                    CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
   
</asp:Content>



    

    
