<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/MyOrderWork.Master" Title="OrderWork: Work Order Close" CodeBehind="WOClose.aspx.vb" Inherits="OrderWorkUK.WOClose" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">
<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
		<div class="errContainer">
			<div><img src="../OWLibrary/Images/Error_TopBand.gif" width="480" height="10"></div>
			<div class="errMdlBox">
				<table border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
					<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
				  </tr>
				</table>			
			</div>
			<div><img src="../OWLibrary/Images/Error_BtmBand.gif" width="480" height="10"></div>				
		</div>
	</asp:Panel>
	<div id="divClearBoth"></div>
<div id="divTopBand" ></div>
        <%@ Register TagPrefix="uc2" TagName="UCBuyerTabs" Src="~/UserControls/UK/UCBuyerTabs.ascx" %>
                 <uc2:UCBuyerTabs id="UCBuyerTabs1" runat="server"></uc2:UCBuyerTabs>
          <%@ Register TagPrefix="uc1" TagName="UCWOCloseComplete" Src="~/UserControls/UK/UCMSWOCloseComplete.ascx" %>
          <uc1:UCWOCloseComplete id="UCWOCloseComplete1" runat="server"></uc1:UCWOCloseComplete>
</asp:Content>
