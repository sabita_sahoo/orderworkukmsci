<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PurchaseInvDetails.aspx.vb" Title="OrderWork: Purchase Invoice Details" Inherits="OrderWorkUK.PurchaseInvDetails" MasterPageFile ="~/MasterPage/MyOrderWork.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">

	<div id="divTopBand" ></div>
	<div id="divMiddleBand">
	<%@ Register TagPrefix="uc1" TagName="UCMyInvoicesDetails" Src="~/UserControls/UK/UCMyInvoicesDetails.ascx" %>
	<uc1:UCMyInvoicesDetails id="UCMyInvoicesDetails1" runat="server"></uc1:UCMyInvoicesDetails></div>
	<div id="divBtmBand"></div>
</asp:content>
