<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DosDonts.aspx.vb" Title="OrderWork: Dos Donts" Inherits="OrderWorkUK.DosDonts" MasterPageFile ="~/MasterPage/MyOrderWork.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
    <h1 class="marginB20">Stand out from the Crowd &ndash; Use our Do&rsquo;s and Don&rsquo;ts to get more jobs</h1>
          <p><img src="../OWLibrary/Images/Image-Do.jpg" alt="Do" width="77" height="70"></p>
          <ul>
            <li>keep your Profile up-to-date</li>
            <li>a good job every job &ndash; this ensures you have consistently high ratings</li>
            <li>be flexible with your standard daily rate</li>
            <li> be on time for each job</li>
            <li>keep a tidy appearance</li>
            <li>communicate well with the Client and our Service Delivery Team &ndash; you can can&rsquo;t make a job &ndash; we need to know &ndash; call us</li>
            <li>get your quotes in as fast as you can<br>
                (You may have to quote generically or use caveats where needed. All the information the client has is placed on the order) </li>
            <li> send in sign off sheets promptly<br>
                (The deadline is 10:00am the following day for all clients)</li>
            </ul>
          <p><br>
                </p>
          <p><img src="../OWLibrary/Images/Image-Dont.jpg" alt="Dont" width="115" height="75"></p>
          <ul>
            <li> be late for jobs &ndash; if you are running late &ndash; let us know</li>
            <li>give out your business card to Clients &ndash; this is not allowed. Suppliers caught doing this will be immediately suspended.</li>
            <li>give out your company name &ndash; you represent our client.</li>
            <li>call our call centre unless you absolutely need to. For general enquiries check our FAQ or email.</li>
            <li>accept or conditionally accept a work order if you are unable to complete the work </li>
            </ul>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p><br>
                                  </p>


</asp:Content>