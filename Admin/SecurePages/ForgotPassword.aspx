<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWIT.Master"
    CodeBehind="ForgotPassword.aspx.vb" Inherits="OrderWorkUK.ForgotPassword" Title="OrderWork - Services Reinvented - Forgot Password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <link rel="stylesheet" href="OWLibrary/Styles/font-awesome.min.css">
    <link rel="stylesheet" href="OWLibrary/Styles/bootstrap.min.css">
    <link rel="stylesheet" href="OWLibrary/Styles/newStyle.css">
    <style type="text/css">
        .divFooter, .headerpd
        {
            display: none !important;
        }
        .divSeperator
        {
            padding: 0 !important;
        }
    </style>
    <script language="javascript" type="text/JavaScript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/getusermedia-js@1.0.0/dist/getUserMedia.min.js"></script>
    <script src="OWLibrary/JS/WaterMark.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        jQuery(function () {
            //jQuery("[id*=ctl00_ContentPlaceHolder1_txtLoginPassword]").WaterMark();

            //To change the color of Watermark
            jQuery("[id*=ctl00_ContentPlaceHolder1_txtLoginPassword]").WaterMark(
            {
                WaterMarkTextColor: '#878787'
            });
        });          
    </script>
    <!---Changed and Added below code to solve IE issue (jquery not loading) Shrutika 04 Oct 2018---->
    <script type="text/javascript">
        window.console = window.console || { log: function () { } };
        if (typeof console === "undefined") {
            console = {};
            console.log = function () { };
        }
        jQuery(document).ready(function () {
            console.log("ready!");
            if (jQuery) {
                //setTimeout(function () {
                var windowHeight = jQuery(window).innerHeight();
                console.log("Window Height", windowHeight);
                var windowWidth = jQuery(window).innerWidth();

                if (windowWidth < 500) {
                    windowHeight = windowHeight / 2;
                    jQuery('.image-content').css('top', '60%');
                    jQuery('.text-main-content').css('font-size', '1.5em');
                    jQuery('.text-sub-content').css('font-size', '1em');

                } else {
                    windowHeight = windowHeight - 48;
                }
                if (windowWidth > 500) {
                    console.log(windowHeight);
                    console.log(jQuery('.image'));
                    jQuery('.image').css('min-height', windowHeight);
                    jQuery('.information').css('min-height', windowHeight);
                    jQuery('.information').css('max-height', windowHeight);
                }

                var images = [{ image: "45160380.jpg", mainContent: "earn in your spare time with small local jobs.", subContent: " register to become orderwork professional" },
                            { image: "painter-2247395.jpg", content: "" }, { image: "building-2748840.jpg", content: "" }, { image: "electrician-1080554.jpg", content: "" },
                            { image: "installation-872778.jpg", content: "" }, { image: "boiler-1816642.jpg", content: "" }, { image: "plumber-228010.jpg", content: "" },
                            { image: "service-428539.jpg", content: "" }, { image: "woman-1484279.jpg", content: ""}];

                setInterval(function () {
                    var index = Math.round(Math.random() * (images.length - 1));
                    var currentImage = 'OWLibrary/Images/Photos/' + images[index].image;
                    jQuery('.image').css('background-image', "url('" + currentImage + "')");

                    //console.log(this.currentImage, index);
                }, 8000)
                //}, 500);
            }
            else {
                location.reload();
            }
        });
    </script>
    <!---END-->
    <%--<div id="divLoginBodyInner">
        
        <div id="HmMain" style="float: left; height: 350px; padding-top: 15px; margin-left: 20px;
            background-image: url(OWLibrary/Images/Bg_GreyBg.jpg); background-position: bottom right;
            background-color: #FCFCFC; background-repeat: no-repeat;">
            <div id="divForgotPassMain">
                <div id="divLoginLogo">
                    <img src="OWLibrary/Images/LoginLogo.jpg" style="margin-top: 40px; margin-left: 28px;"
                        alt="myOrderWork Client Portal" title="myOrderWork Client Portal" />
                </div>
                <div class="displaynone">
                    &nbsp;&nbsp;</div>
                <div id="divForgotPassword">
                    <div id="divForgotRight">
                        <img src="OWLibrary/Images/LoginTopband.jpg" valign="top" />
                        <div id="divRightImageFP">
                            <div id="divLoginInner">
                                <div class="displaynone">
                                    &nbsp;&nbsp;</div>
                                <%@ register tagprefix="uc1" tagname="ForgotPassword" src="~/UserControls/UK/UCForgotPasswordUK.ascx" %>
                                <uc1:ForgotPassword ID="UCForgotPassword" runat="server"></uc1:ForgotPassword>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
    <div id="divLoginBodyInner">
        <div class="divLoginBodyNew">
            <div class="col-md-12 col-xs-12 col-sm-12 padding0" style="background-color: #fff;
                margin: 15px 0px 15px 0px;">
                <div class="col-md-5 col-sm-12 col-xs-12 col-lg-5 padding0 images  hidden-xs hidden-sm">
                    <div class="image" style="background-image: url('OWLibrary/Images/Photos/boiler-1816642.jpg');">
                    </div>
                    <div class="image-content">
                        <span>
                            <span class="text-main-content">Welcome to <br /> MyOrderWork </span>
                            <span class="text-sub-content">Installation and repair<br />services made simple </span>
                        </span>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12 col-xs-12 col-lg-7 col rightsec padding0 information" style="min-height: 565px; max-height: 565px;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-6 col-sm-6 col-xs-6  hidden-xs hidden-sm" style="margin-top: 25px;">
                            <span class="heading common-heading"></span>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-2 padding0 rtlogo" style="margin-top: 25px;">
                            <a href="http://orderwork.co.uk/" title="MyOrderwork"><img alt="orderwork" style="height: 60px; width: auto; float:right;" src="OWLibrary/Images/Logo-MyOW-client.jpg"></a>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 error-message text-center" style="margin-bottom: 35px">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 fix-bottom">
                        <div class="col-md-6 col-sm-6 col-xs-12 fix-bottom padding0 login-section">
                            <div class="col-md-12 col-sm-12 col-xs-12 fix-bottom login-top">
                                <span class="login-heading">Forgot Password</span><br /> 
                                <%--<span class="login-subheading">Sign in to your account</span>--%>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 fix-bottom">
                                <%@ register tagprefix="uc1" tagname="ForgotPassword" src="~/UserControls/UK/UCForgotPasswordUK.ascx" %>
                                <uc1:ForgotPassword ID="UCForgotPassword" runat="server"></uc1:ForgotPassword>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
