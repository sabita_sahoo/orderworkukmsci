<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Locations.aspx.vb" Inherits="OrderWorkUK.Locations" Title="OrderWork: Locations" MasterPageFile ="~/MasterPage/MyOrderWork.Master" ValidateRequest="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
	<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
		<div class="errContainer">
			<div><img src="../OWLibrary/Images/Error_TopBand.gif" width="480" height="10"></div>
			<div class="errMdlBox">
				<table border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
					<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
				  </tr>
				</table>			
			</div>
			<div><img src="../OWLibrary/Images/Error_BtmBand.gif" width="480" height="10"></div>				
		</div>
	</asp:Panel>
	<div id="divClearBoth"></div>
<div id="divTopBand" class="floatLeft divContainerStyle" >
	<%@ Register TagPrefix="uc1" TagName="UCLocationListing" Src="~/UserControls/UK/UCLocationListing.ascx" %>
	<uc1:UCLocationListing id="UCLocationListing1" runat="server"></uc1:UCLocationListing>
    </div>
	<div id="div1" class="clearBoth"></div>
</asp:content>

