﻿<%@ Page ValidateRequest="false" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/MyOrderWork.Master" Title="OrderWork: Create Work Order" CodeBehind="Itecform2.aspx.vb" Inherits="OrderWorkUK.Itecform2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">

<script language="javascript" type="text/javascript" src="../OWLibrary/JS/json2.js"></script>
<script language="javascript" type="text/javascript" src="../OWLibrary/JS/jquery.js"></script>
<script language="JavaScript" src="../OWLibrary/JS/JQuery.jsonSuggestCustom.js" type="text/javascript"></script>
<style type="text/css">
*+html #divInfoContent {
margin-top:-2px !important;}
.searchResult {
    background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    color: #5F5F5F;
    display: none;
    float: left;
    font-family: Verdana;
    font-size: 12px;
    margin: 0;
    padding: 0;
    position: absolute;
    width: 305px;
    z-index: 9998;


}


</style>
<style type="text/css">
.gridHdr
{
	background-color:#993366;
	font-family:Verdana;
	font-size: 11px;
	font-weight: bold;
	color: #555354;
	text-decoration: none;
	height:28px;
	vertical-align:middle;
	text-align:none;
}
.gridRow
{
	background-color:#F7F3F4;
	font-family:Verdana;
	font-size: 11px;	
	color: #555354;
	text-decoration: none;
	padding-left:6px;
	padding-right:6px;
	padding-top: 8px;
	margin-left:6px;
	margin-top:8px;	
	border:solid 1px #DFDFDF;
}
#AlertDiv{
left: 40%; top: 40%;
position: absolute; width: 200px;
padding: 12px; 
border: #000000 1px solid;
background-color: white; 
text-align: left;
visibility: hidden;
z-index: 99;
}
#AlertButtons{
position: absolute; right: 5%; bottom: 5%;
}
/*Styles for Modal PopUp = START*/
#divModalParent {
    margin: auto;
    text-align: left;
}
.modalBackground 
 {
    background-color:Silver;
    filter:alpha(opacity=30) !important;
    opacity:0.3 !important;
    position:absolute !important;
    z-index:1 !important;
 }
.pnlConfirm {	
	background-color:#FFEBDE;
	font-family:Verdana;
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	line-height: 18px;
	border: 1px solid #CECBCE;
	padding: 10px;
	position:absolute !important;
	margin-left:650;
	margin-top:350;
}
.cursorHand
{
	cursor: hand;
}
.bottonTopGrey {
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-TR.gif) no-repeat right top;
	height:4px;
}
.bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
}
*+html .bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
	margin-top:-3px;
}

#divButton {
background-color:#999;
width:56px;
height:19px;
margin:10px 0 0;
padding:0;
}
img.btnCorner {
   width: 4px;
   height: 4px;
   border: none;
   display: block !important;
}
.buttonText {
font-family:Verdana;
font-size:11px;
color:#FFF;
text-decoration:none;
padding-left:11px;
padding-right:8px;
font-style:normal;
font-weight:400;
font-variant:normal;
line-height:11px;
}

.buttonText:hover {
color:#3F3F3F;
text-decoration:none;
}
</Style>
<%--<link href="../Styles/OWPortal.css" rel="stylesheet" type="text/css">--%>

         <div id="divClearBoth"></div>
	<div class="divContainerStyle floatLeft" style="border:0px;" id="divTopBand1" >
		<!-- Create Work order UC -->
		  <%@ Register TagPrefix="uc1" TagName="UCMSCreateWorkRequestUK" Src="~/UserControls/UK/UCMSWOForm.ascx" %>
          <uc1:UCMSCreateWorkRequestUK id="UCCreateWorkOrderUK1" runat="server"></uc1:UCMSCreateWorkRequestUK>

    </div>
    <div style="clear:both;"></div>
</asp:Content>