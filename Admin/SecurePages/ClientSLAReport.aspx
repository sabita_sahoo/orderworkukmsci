<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClientSLAReport.aspx.vb" Title="OrderWork: Client SLA Report" Inherits="OrderWorkUK.ClientSLAReport" MasterPageFile ="~/MasterPage/MyOrderWork.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
<script type="text/javascript" src="../OWLibrary/JS/Scripts.js"></script>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:HiddenField id="hdnCompID" name="hdnCompID" runat="server" />
<asp:HiddenField id="hdnStartDate" SafeEncode="false" runat="server" value="" />
<asp:HiddenField id="hdnToDate" SafeEncode="false" name="hdnToDate" runat="server" /> 

	<div id="divTopBand" ></div>
	<div class="mainTableBorderr paymentHeading" id="divPaymentMethod"><strong>Best Buy SLA Report</strong></div>
	<div id="divMiddleBand">
	<div id="divDateRange" runat="server" class="divDateRange" style="border-left: 1px solid #E1E2DC;
    border-right: 1px solid #E1E2DC; border-top: 1px solid #E1E2DC;">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr><td width="60" valign="top" style="padding-left:17px;font-size:12px;" class="datelbl">From Date</td>
    <td width="110" height="24" valign="top" style="padding-top:4px;">
	 &nbsp;<asp:TextBox id="txtScheduleWInBegin" runat="server"  CssClass="formFieldGrey width120" TabIndex="2" Width="66px" ></asp:TextBox>
	<img alt="Click to Select" src="~/Images/calendar.gif" runat="server" id="btnBeginDate" style="cursor:pointer; vertical-align:middle;" />		                               
	<cc1:OWCalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID="btnBeginDate" TargetControlID="txtScheduleWInBegin" ID="calBeginDate" runat="server" OnClientDateSelectionChanged="GetFromDate"></cc1:OWCalendarExtender>
	</td><td width="50" valign="top" class="datelbl" style="font-size:12px;">To Date</td>
	<td width="110" height="24" valign="top" style="padding-top:4px;" >
	<asp:TextBox id="txtScheduleWInEnd" runat="server" CssClass="formFieldGrey width120" TabIndex="3" Width="66px"></asp:TextBox>
	<img alt="Click to Select" src="~/Images/calendar.gif" id="btnEndDate" runat="server" style="cursor:pointer;vertical-align:middle;" />		                               																	
	<cc1:OWCalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID="btnEndDate" TargetControlID="txtScheduleWInEnd" ID="calEndDate" runat="server" OnClientDateSelectionChanged="GetToDate" OnClientShowing="function x(s, e) { s.set_MinimumAllowableDate(getbegindateBestBuy()); }"  ></cc1:OWCalendarExtender>
	</td>
	
	<td>   <div class="BGSLA divMediumBtnImage" style="text-align: left;width:70px;">
        <asp:LinkButton ID="lnkView" CausesValidation="true" runat="server"
            Style="cursor: hand;">
            <img src="~/OWLibrary/Images/Icons/ViewBtnIcon.gif" OnClick="lnkView_Click"  runat="server" id="imgbtnView"
                hspace="5" vspace="0" align="absMiddle" border="0" class="imgPrintSpace">
           View
        </asp:LinkButton>
    </div></td>
    <td> <div id="divPrint" class="BGSLA divMediumBtnImage" style="text-align:left;width:70px;" runat="server">
        <a id="btnPrint" runat="server" target="_blank" tabindex="10">
            <img src="~/OWLibrary/Images/Icons/Icon-Print-P.gif" runat="server" id="imgbtnPrint"
                hspace="5" vspace="0" align="absMiddle" border="0" class="imgPrintSpace" >
            <strong>Print</strong>
        </a>
    </div></td>
    <td>
     <div id="divExpToExc" runat="server">
               <div id="divMediumBtnImage" style="float:right;margin-right:15px;valign:bottom;">
                    <a runat="server" target="_blank" id="btnExport">
                        <img src="~/OWLibrary/Images/Icons/Icon-Excel.gif" runat="server" id="imgbtnexceltop" width="15" height="15" hspace="5" vspace="0" align="absMiddle"  border="0">
                     &nbsp;Export to Excel&nbsp;</a>
               </div>
           </div>
    </td>
	</tr>
    </table>  
         
 
    
   
    <div id="divClearBoth">
    </div>
</div>
        <div id="divMainReport" runat="server" style="padding-top:30px;padding-left:20px;">
        <div style="margin-bottom:20px;"><strong>Best Buy SLA Report</strong></div>
             <table cellpadding="0" cellspacing="0">
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td width="200">Total Submitted Work Orders</td>
                   <td width="20">:</td>
                   <td width="50"><asp:Label ID="lblSubmittedWO" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total Closed Work Orders</td>
                   <td>:</td>
                   <td><asp:Label ID="lblClosedWO" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total Work Orders that met SLA</td>
                   <td>:</td>
                   <td><asp:Label ID="lblOnTime" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total SLA Met (%)</td>
                   <td>:</td>
                   <td><asp:Label ID="lblSLAPercentageMet" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>1st Time Success (%)</td>
                   <td>:</td>
                   <td><asp:Label ID="lblFirstTimeSuccess" runat="server"></asp:Label></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>No. of Cancellations</td>
                   <td>:</td>
                   <td><asp:Label ID="lblCancellationNo" runat="server"></asp:Label></td>
                  </tr>                  
              </table>         
                        
            <div runat="server" id="divProduct" style="padding-top:20px;">
           <div class="paddingB4 HeadingRed"><strong>By SKU</strong> </div>
              <div>
              <asp:datalist id="DLProducts" RepeatColumns="2" runat="server"  Width="100%" >           
              <ItemTemplate>
                <table cellpadding="0" cellspacing="0">
                <tr>
                 <td colspan="3"><b><%#Container.DataItem("ProductName")%></b></td>
                </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td width="200">Total jobs submitted</td>
                   <td width="20">:</td>
                   <td width="50"><%#Container.DataItem("SubmittedWO")%></td>
                    <td width="50">&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs closed</td>
                   <td>:</td>
                   <td><%#Container.DataItem("ClosedWO")%></td>
                    <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs within SLA (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("SLAPercentageMet")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total 1st time success (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("FirstTimeSuccess")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Cancelled jobs</td>
                   <td>:</td>
                   <td><%#Container.DataItem("CancellationNumber")%></td>
                   <td>&nbsp;</td>
                  </tr>
                    <tr>
                      <td colspan="4">&nbsp;</td>
                    </tr>
                </table>                  
            </ItemTemplate>
          </asp:datalist>  
          </div>        
              <div class="divClearBoth"></div>   
            </div>           
            
           <div runat="server" id="divBillingLoc">
             <div class="paddingB4 HeadingRed"><strong>By Store</strong> </div>     
              <div >
                     <asp:datalist id="DLBillingLoc" RepeatColumns="2" runat="server"  Width="100%" >           
              <ItemTemplate>
                <table cellpadding="0" cellspacing="0">
                  <tr>
                     <td colspan="3"><b><%#Container.DataItem("BillingLocation")%></b></td>
                  </tr>
                  <tr class="clsListingColumn" style="line-height:19px;">             
                   <td width="200">Total jobs submitted</td>
                   <td width="20">:</td>
                   <td width="50"><%#Container.DataItem("SubmittedWO")%></td>
                    <td width="50">&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs closed</td>
                   <td>:</td>
                   <td><%#Container.DataItem("ClosedWO")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total jobs within SLA (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("SLAPercentageMet")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Total 1st time success (%)</td>
                   <td>:</td>
                   <td><%#Container.DataItem("FirstTimeSuccess")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr class="clsListingColumn" style="line-height:19px;">             
                   <td>Cancelled jobs</td>
                   <td>:</td>
                   <td><%#Container.DataItem("CancellationNumber")%></td>
                   <td>&nbsp;</td>
                  </tr>
                   <tr>
                      <td colspan="4">&nbsp;</td>
                    </tr>
                </table>                  
             </ItemTemplate>
            </asp:datalist>   
              </div>        
              <div class="divClearBoth"></div>   
          </div>      
            
        </div>
         <table id="tblNoRecords" runat="server" width="100%" border="0" cellpadding="10" cellspacing="0">					
	    <tr>
		    <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.</td>
		</tr>				
		</table>
	</div>
	<div id="divBtmBand"></div>
</asp:content>
