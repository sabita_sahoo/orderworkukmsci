<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/MyOrderWork.Master" Title="OrderWork: Work Order Change Issue" CodeBehind="WOChangeIssue.aspx.vb" Inherits="OrderWorkUK.WOChangeIssue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">
	<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
		<div class="errContainer">
			<div><img src="../OWLibrary/Images/Error_TopBand.gif" width="480" height="10"></div>
			<div class="errMdlBox">
				<table border="0" cellpadding="0" cellspacing="0">
				  <tr>
					<td><img src="../OWLibrary/Images/Icons/Stop_Icon.jpg" class="errImg" width="53" height="54"></td>
					<td class="errText"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
				  </tr>
				</table>			
			</div>
			<div><img src="../OWLibrary/Images/Error_BtmBand.gif" width="480" height="10"></div>				
		</div>
	</asp:Panel>
	<div id="divClearBoth"></div>
	<div class="divContainerStyle floatLeft" id="divTopBand1" >
	<%@ Register TagPrefix="uc2" TagName="UCSupplierTabs" Src="~/UserControls/UK/UCSupplierTabs.ascx" %>
                 <uc2:UCSupplierTabs id="UCSupplierTabs1" runat="server"></uc2:UCSupplierTabs>

		 <%@ Register TagPrefix="uc1" TagName="UCWOProcess" Src="~/UserControls/UK/UCMSWOProcess.ascx" %>
          <uc1:UCWOProcess id="UCWOProcess1" runat="server"></uc1:UCWOProcess>
    </div>
    <div style="clear:both;"></div>
</asp:Content>
