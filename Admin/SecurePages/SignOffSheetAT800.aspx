﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/MyOrderWork.Master" CodeBehind="SignOffSheetAT800.aspx.vb" Inherits="OrderWorkUK.SignOffSheetAT800"  Title="OrderWork: AT800 Sign-Off Sheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="Server">
<style>
.clsGreyBandStyleFund {
    background-image: url("./OWLibrary/Images/OrderWorkUkRedesign/Title_bg.gif");
    color: Black;
    height: 24px;
    padding-left: 30px;
    padding-top: 8px;
    width: 992px;
}
</style>
<script type="text/javascript">
function SearchJob() {
        var txtWorkOrder = document.getElementById("ctl00_ContentHolder_SignOffSheetAT800_wzd_txtWorkOrder").value;
        var txtPostcode = document.getElementById("ctl00_ContentHolder_SignOffSheetAT800_wzd_txtPostCode").value;
        //OrderWorkLibrary.DBWorkOrder.SearchWorkOrder(txtWorkOrder.Text, txtPostCode.Text, Session("PortalCompanyId"));
        PageMethods.SearchWorkOrder(txtWorkOrder, txtPostcode, SuccessFunction, FailedFunction);

    }
</script>
<%@ Register TagPrefix="uc1" TagName="UCSignOffSheetAT800" Src="~/UserControls/UK/UCSignOffSheetAT800.ascx" %>
<uc1:UCSignOffSheetAT800 id="SignOffSheetAT800" runat="server"></uc1:UCSignOffSheetAT800>
</asp:Content>