<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SPPrivacyPolicy.aspx.vb"
    Title="OrderWork: Privacy Policy" Inherits="OrderWorkUK.SPPrivacyPolicy" MasterPageFile="~/MasterPage/MyOrderWork.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ContentPlaceHolderID="ContentHolder" runat="Server">
    <script language="javascript" src="OWLibrary/JS/Scripts.js" type="text/javascript"></script>
    <style>
<!--
.bodytxtRed {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 15px;
	font-weight: bold;
	color: #C54043;
	text-decoration: none;
}
.blueBold{font-size: 12px; font-weight: bold;color:#1882B5;}
.blueBold:hover {font-size: 12px; font-weight: bold;color:#C54043;}
        .style1
        {
            width: 100%;
        }
       #privacy td
        { border: 1px solid #e2e2e2; padding:0 10px;}
-->
</style>
    <div id="divTopBand" class="divContainerStyle floatLeft">
        <cc1:TabContainer ID="tabContainer" runat="server" CssClass="CustomTabPrivacyPolicy">
            <cc1:TabPanel ID="tabSupplyWork" runat="server" HeaderText="Privacy Policy ">
                <ContentTemplate>
                    <div id="div1" style="padding-left: 15px; padding-right: 20px">
                        <p align="justify" class="bodyTxt">
                            <br>
                            <p class="MsoNormal" align="center" style='margin-bottom: 0cm; margin-bottom: .0001pt;
                                text-align: center'>
                                <b style='mso-bidi-font-weight: normal'>Privacy Policy<o:p></o:p></b></p>
                            <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify'>
                                <o:p>&nbsp;</o:p>
                            </p>
                            <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify'>
                                Orderwork Limited treats all information processing seriously. We have a continuous
                                awareness that some of the data that we process is personal data, we need to ensure
                                our processes treat personal information with respect and protect it from misuse.
                                We take the processing of personal data very seriously.&nbsp;</p>
                            <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify'>
                                We process data only for our core business purpose, we only process data for the
                                original purpose that it was collected for. We do not keep lists of names for mail
                                shots or cold calling, we will never pass on information to other parties for that
                                purpose.&nbsp;&nbsp;
                            </p>
                            <p class="bodytxtRed">
                                <b>Here are the principles we operate to in respect of the personal data that we process;</b></p>
                            <p class="bodytxtRed">
                                <b>Services to our end user customers </b>
                            </p>
                            <p class="MsoListParagraphCxSpFirst" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l5 level1 lfo7'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>We process personal data about the individuals
                                that we deliver services to. We only take the information that we need to be able
                                to communicate with our customers about arrangements to plan the service, to deliver
                                the service and then carry out any follow up work straight after the service which
                                may include a satisfaction survey. &nbsp;For this we hold their name, address, phone
                                number and email address. We do not request, record or process any other data, including
                                Special data, about our customers.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0cm; margin-right: 0cm;
                                margin-bottom: 0cm; margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto;
                                text-align: justify; text-indent: -18.0pt; mso-list: l5 level1 lfo7'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>If there is a credit card payment made in relation
                                to the service, we pass card details directly to our payment service and do not
                                record details of the card ourselves.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0cm; margin-right: 0cm;
                                margin-bottom: 0cm; margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto;
                                text-align: justify; text-indent: -18.0pt; mso-list: l5 level1 lfo7'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>After the service is completed and invoiced we
                                retain all the information about the service event to enable analysis of our services,
                                for historical trending and planning for the future of our business.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0cm; margin-right: 0cm;
                                margin-bottom: 0cm; margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto;
                                text-align: justify; text-indent: -18.0pt; mso-list: l5 level1 lfo7'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>If we use a supplier to deliver a service for
                                us, customer details will be passed to them to enable the service.&nbsp; After the
                                supplier completes their tasks they will not retain that information.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0cm; margin-right: 0cm;
                                margin-bottom: 0cm; margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto;
                                text-align: justify; text-indent: -18.0pt; mso-list: l5 level1 lfo7'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>We never pass our customer data on to other parties
                                after a service event is closed and we will not contact end user customers after
                                all service events are closed.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpLast" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l5 level1 lfo7'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>All our customer data is held on our own servers,
                                processed and hosted in the UK.&nbsp;
                            </p>
                            <p class="bodytxtRed">
                                <b>Services to our corporate partners and customers </b>
                            </p>
                            <p class="MsoListParagraphCxSpFirst" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l2 level1 lfo8'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>We process personal data about the individuals
                                that we deliver services to. We only take the information that we need to be able
                                to communicate with our customers about arrangements to plan the service, to deliver
                                the service and then carry out any follow up work straight after the service which
                                may include a satisfaction survey. &nbsp;For this we hold their name, address, phone
                                number and email address. We do not request, record or process any other data, including
                                Special data, about our customers.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0cm; margin-right: 0cm;
                                margin-bottom: 0cm; margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto;
                                text-align: justify; text-indent: -18.0pt; mso-list: l2 level1 lfo8'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>After the service is completed and invoiced we
                                retain all the information about the service event to enable analysis of our services,
                                for historical trending and planning for the future of our business.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0cm; margin-right: 0cm;
                                margin-bottom: 0cm; margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto;
                                text-align: justify; text-indent: -18.0pt; mso-list: l2 level1 lfo8'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>If we use a supplier to deliver a service for
                                us, customer details will be passed to them to enable the service.&nbsp; After the
                                supplier completes their tasks they will not retain that information.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0cm; margin-right: 0cm;
                                margin-bottom: 0cm; margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto;
                                text-align: justify; text-indent: -18.0pt; mso-list: l2 level1 lfo8'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>We never pass our customer data on to other parties
                                after a service event is closed and we will not contact end user customers after
                                all service events are closed.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpLast" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l2 level1 lfo8'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>All our customer data is held on our own servers,
                                processed and hosted in the UK.&nbsp;
                            </p>
                            <p class="bodytxtRed">
                                <b>Our suppliers and service partners </b>
                            </p>
                            <p>
                            </p>
                            <p class="MsoListParagraphCxSpFirst" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l9 level1 lfo9'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>We use suppliers and partners to deliver service
                                for us.&nbsp; Our relationship with them is a contractual one, where they choose
                                to work with us. We hold information about them to enable us to contact them in
                                relation to delivery of services.&nbsp; If we stop trading with a supplier we continue
                                to hold the information about them so that we can analyse our services, for historical
                                trending and planning for the future of our business.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0cm; margin-right: 0cm;
                                margin-bottom: 0cm; margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto;
                                text-align: justify; text-indent: -18.0pt; mso-list: l9 level1 lfo9'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>We pass personal information about our customers
                                to our suppliers in relation to a service event to enable our suppliers to deliver
                                service to our end users.&nbsp; In this case the supplier does not retain this information
                                after the service event is completed and invoiced with us.&nbsp;</p>
                            <p class="MsoListParagraphCxSpLast" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l9 level1 lfo9'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>Our supplier information is held our own servers,
                                processed and hosted in the UK.&nbsp; &nbsp;&nbsp;</p>
                            <p class="bodytxtRed">
                                <b>Our relationship with our corporate customers</b></p>
                            <p class="MsoListParagraphCxSpFirst" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l8 level1 lfo10'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>We provide services to many customers and have
                                many customers within our sales pipeline, where we have engaged with them and wish
                                to do business with them.&nbsp; For this purpose personal contact details are exchanged
                                between peers (like project managers, sales teams <span class="GramE">and &nbsp;supplier</span>
                                managers) within our organisation and theirs.&nbsp; The personal information is
                                names, phone numbers and email addresses; the phone and email address will normally
                                be for a business location but some individuals choose to circulate personal contact
                                details.&nbsp; The information is offered and exchanged in relation to maintaining
                                a business relationship.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0cm; margin-right: 0cm;
                                margin-bottom: 0cm; margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto;
                                text-align: justify; text-indent: -18.0pt; mso-list: l8 level1 lfo10'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>The information is never passed to other parties.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpLast" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l8 level1 lfo10'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>The information is recorded on the Microsoft
                                Office365 and Salesforce systems.&nbsp; &nbsp;&nbsp;</p>
                            <p class="bodytxtRed">
                                <b>Our recruitment and vetting of supplier�s staff</b></p>
                            <p class="MsoListParagraphCxSpFirst" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l7 level1 lfo11'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>We process CVs for individuals where they apply
                                for roles that we have advertised.&nbsp; This can be a direct applicant or, through
                                one of our suppliers.&nbsp; CVs contain personal information and we treat this information
                                with care.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0cm; margin-right: 0cm;
                                margin-bottom: 0cm; margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto;
                                text-align: justify; text-indent: -18.0pt; mso-list: l7 level1 lfo11'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>As CV�s are always offered to us, we are assuming
                                that the individuals are opting in to allowing us to process their data; they have
                                offered their CV information to us by applying for our role or by advertising their
                                CV on a job board web site.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpMiddle" style='margin-top: 0cm; margin-right: 0cm;
                                margin-bottom: 0cm; margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto;
                                text-align: justify; text-indent: -18.0pt; mso-list: l7 level1 lfo11'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>We hold CVs within our Microsoft Office365 system
                                where access is restricted to only the staff needing to access it.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpLast" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l7 level1 lfo11'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>As a vacancy is fulfilled, we ask unsuccessful
                                applicants whether we can continue to hold their details, we do this <span class="GramE">
                                    for the purpose of</span> finding them employment in the future.&nbsp;
                            </p>
                            <p class="bodytxtRed">
                                <b>Our own HR services and employee information </b>
                            </p>
                            <p class="MsoListParagraphCxSpFirst" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l6 level1 lfo12'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>We process personal information, including Special
                                information, about the people we employ and have employed. We need this information
                                to be able to gain references, check entitlement to work and pay our team, all normal
                                UK employment processes. We process information on paper and on our secure HR portal.&nbsp;
                                Our paper files are under lock and key with our HR manager.&nbsp; Our HR Portal
                                is secured for its purpose and the personal information is only available to our
                                HR manager.&nbsp; Each employee can access their own data, where they can also use
                                the Portal for booking holidays.&nbsp;
                            </p>
                            <p class="MsoListParagraphCxSpLast" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l6 level1 lfo12'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>Our HR Portal is provided by a specialist HR
                                systems provider and is hosted within the UK and Ireland.&nbsp; &nbsp;</p>
                            <b><span style='font-size: 11.0pt; line-height: 107%; font-family: "Calibri",sans-serif;
                                mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin;
                                mso-hansi-theme-font: minor-latin; mso-bidi-font-family: "Times New Roman"; mso-bidi-theme-font: minor-bidi;
                                mso-ansi-language: EN-GB; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'>
                                <br clear="all" style='mso-special-character: line-break; page-break-before: always'>
                            </span></b>
                            <p class="bodytxtRed">
                                <b>Special category data</b></p>
                            <p class="MsoListParagraph" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 18.0pt; margin-bottom: .0001pt; mso-add-space: auto; text-align: justify;
                                text-indent: -18.0pt; mso-list: l0 level1 lfo13'>
                                <![if !supportLists]><span style='font-family: Symbol; mso-fareast-font-family: Symbol;
                                    mso-bidi-font-family: Symbol'><span style='mso-list: Ignore'>�<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]>Within the GDPR regulations the term Special
                                Category Data is defined (also called sensitive data) as this data needs more protection.&nbsp;
                                The term is used in this policy and refers to more sensitive data like ethnic origin,
                                biometric information or health matters. We only process data of this type within
                                our HR system described above.&nbsp; &nbsp;</p>
                            <p class="bodytxtRed">
                                <b>Disclosing Your Information </b>
                            </p>
                            <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify'>
                                We will not disclose your personal information to any other party other than in
                                accordance with this Privacy Policy and in the circumstances detailed below:
                            </p>
                            <ol style='margin-top: 0cm' start="1" type="1">
                                <li class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify;
                                    line-height: 105%; mso-list: l10 level1 lfo5'><span class="GramE"><span style='mso-fareast-font-family: "Times New Roman"'>
                                        In the event that</span></span><span style='mso-fareast-font-family: "Times New Roman"'>
                                            we sell any or all of our business to the buyer.
                                            <o:p></o:p>
                                        </span></li>
                                <li class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify;
                                    line-height: 105%; mso-list: l10 level1 lfo5'><span style='mso-fareast-font-family: "Times New Roman"'>
                                        Where we are legally required by law to disclose your personal information.
                                        <o:p></o:p>
                                    </span></li>
                                <li class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify;
                                    line-height: 105%; mso-list: l10 level1 lfo5'><span style='mso-fareast-font-family: "Times New Roman"'>
                                        To further fraud protection and reduce the risk of fraud.
                                        <o:p></o:p>
                                    </span></li>
                            </ol>
                            <p class="bodytxtRed">
                                <b>Your rights under the GDPR; </b>
                            </p>
                            <p class="bodytxtRed">
                                <b>Access to Information</b></p>
                            <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify'>
                                In accordance with the GDPR, you have the right to access any information that we
                                hold relating to you by making a subject access request.
                            </p>
                            <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify'>
                                All individuals who are the subject of personal data held by Orderwork are entitled
                                to:
                            </p>
                            <p class="MsoNormal" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 36.0pt; margin-bottom: .0001pt; text-align: justify; text-indent: -18.0pt;
                                line-height: 105%; mso-list: l4 level1 lfo6'>
                                <![if !supportLists]><span style='mso-ascii-font-family: Calibri; mso-fareast-font-family: Calibri;
                                    mso-hansi-font-family: Calibri; mso-bidi-font-family: Calibri'><span style='mso-list: Ignore'>�<span
                                        style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]><span style='mso-fareast-font-family: "Times New Roman"'>Ask
                                        what information the company holds about them and why.
                                        <o:p></o:p>
                                    </span>
                            </p>
                            <p class="MsoNormal" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 36.0pt; margin-bottom: .0001pt; text-align: justify; text-indent: -18.0pt;
                                line-height: 105%; mso-list: l4 level1 lfo6'>
                                <![if !supportLists]><span style='mso-ascii-font-family: Calibri; mso-fareast-font-family: Calibri;
                                    mso-hansi-font-family: Calibri; mso-bidi-font-family: Calibri'><span style='mso-list: Ignore'>�<span
                                        style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]><span style='mso-fareast-font-family: "Times New Roman"'>Ask
                                        how to gain access to it.
                                        <o:p></o:p>
                                    </span>
                            </p>
                            <p class="MsoNormal" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 36.0pt; margin-bottom: .0001pt; text-align: justify; text-indent: -18.0pt;
                                line-height: 105%; mso-list: l4 level1 lfo6'>
                                <![if !supportLists]><span style='mso-ascii-font-family: Calibri; mso-fareast-font-family: Calibri;
                                    mso-hansi-font-family: Calibri; mso-bidi-font-family: Calibri'><span style='mso-list: Ignore'>�<span
                                        style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]><span style='mso-fareast-font-family: "Times New Roman"'>Be
                                        informed how to keep it up to date.
                                        <o:p></o:p>
                                    </span>
                            </p>
                            <p class="MsoNormal" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 36.0pt; margin-bottom: .0001pt; text-align: justify; text-indent: -18.0pt;
                                line-height: 105%; mso-list: l4 level1 lfo6'>
                                <![if !supportLists]><span style='mso-ascii-font-family: Calibri; mso-fareast-font-family: Calibri;
                                    mso-hansi-font-family: Calibri; mso-bidi-font-family: Calibri'><span style='mso-list: Ignore'>�<span
                                        style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]><span style='mso-fareast-font-family: "Times New Roman"'>Be
                                        informed how the company is meeting its data protection obligations.
                                        <o:p></o:p>
                                    </span>
                            </p>
                            <p class="MsoNormal" style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm;
                                margin-left: 36.0pt; margin-bottom: .0001pt; text-align: justify; text-indent: -18.0pt;
                                line-height: 105%; mso-list: l4 level1 lfo6'>
                                <![if !supportLists]><span style='mso-ascii-font-family: Calibri; mso-fareast-font-family: Calibri;
                                    mso-hansi-font-family: Calibri; mso-bidi-font-family: Calibri'><span style='mso-list: Ignore'>�<span
                                        style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span></span><![endif]><span style='mso-fareast-font-family: "Times New Roman"'>Request
                                        for the data to be destroyed.
                                        <o:p></o:p>
                                    </span>
                            </p>
                            <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify'>
                                If <span class="GramE">an individual contacts</span> the company requesting this
                                information, this is called a subject access request.&nbsp; Subject access requests
                                from individuals should be made by email, addressed to the data controller at data-controller@orderwork.co.uk
                            </p>
                            <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify'>
                                The data controller can supply a standard request form, although individuals do
                                not have to use this. Individuals may be charged �10 per subject access request.
                            </p>
                            <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify'>
                                The data controller will aim to provide the relevant data within 14 days.
                            </p>
                            <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify'>
                                The data controller will always verify the identity of anyone making a subject access
                                request before handing over any information.</p>
                            <b><span style='font-size: 11.0pt; line-height: 107%; font-family: "Calibri",sans-serif;
                                mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin;
                                mso-hansi-theme-font: minor-latin; mso-bidi-font-family: "Times New Roman"; mso-bidi-theme-font: minor-bidi;
                                mso-ansi-language: EN-GB; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'>
                                <br clear="all" style='mso-special-character: line-break; page-break-before: always'>
                            </span></b>
                            <p class="bodytxtRed">
                                Your rights under GDPR
                            </p>
                            <p>
                                In particular, but without limitation, you may have the following rights
                                under applicable European data protection law:
                            </p>
                            <p class="bodytxtRed">
                                Right of access:</p>
                            <p>
                                You have the right to obtain confirmation from us as to <span class="GramE">whether
                                    or not</span> we process personal data from you and you also have the right
                                to at any time obtain access to your personal data stored by us. To exercise this
                                right, you may at any time contact us.
                            </p>
                            <p class="bodytxtRed">
                                Right to rectification of your personal data:</p>
                            <p>
                                If we process your personal data, we shall endeavour to ensure by implementing suitable
                                measures that your personal data is accurate and up-to-date for the purposes for
                                which we collected your personal data. If your personal data is inaccurate or incomplete,
                                you have the right to obtain the rectification of such data. To exercise this right,
                                you may at any time contact us.
                            </p>
                            <p class="bodytxtRed">
                                Right to erasure of your personal data or right to restriction of processing:
                            </p>
                            <p>
                                You may have the right to obtain the erasure of your personal data or the restriction
                                of processing of your personal data. To exercise this right, you may at any time
                                contact us.
                            </p>
                            <p class="bodytxtRed">
                                Right to withdraw your consent:</p>
                            <p>
                                If you have given your consent to the processing of your personal data, you have
                                the right to withdraw your consent at any time, without affecting the lawfulness
                                of processing based on the consent before the withdrawal. To exercise this right,
                                you may at any time contact us.
                            </p>
                            <p class="bodytxtRed">
                                Right to data portability:
                            </p>
                            <p>
                                You may have the right to receive the personal data concerning you and which you
                                have provided to us, in a structured, commonly used and machine-readable format
                                or to transmit those data to another controller. To exercise this right, you may
                                at any time contact us.</p>
                            <p class="bodytxtRed">
                                Right to object:</p>
                            <p>
                                You may have the right to object to the processing of your personal data as further
                                specified in this Privacy Policy. Right to lodge a complaint with supervisory authority:
                                You have the right to lodge a complaint with a data protection supervisory authority
                                located in the European Union.
                            </p>
                            <p class="bodytxtRed">
                                Contacting Us</p>
                            <p class="MsoNormal" style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: justify'>
                                Please do not hesitate to contact us regarding any matter relating to this Privacy
                                Statement at <a href="mailto:info@orderwork.co.uk">info@orderwork.co.uk</a> or <a
                                    name="_Hlk514937749"></a><a href="mailto:data-controller@orderwork.co.uk"><span style='mso-bookmark: _Hlk514937749'>
                                        data-controller@orderwork.co.uk</span><span style='mso-bookmark: _Hlk514937749'></span></a>
                                if the nature of the enquiry is specifically in regard to data protection and security.</p>
                            <p class="MsoNormal" style='text-align: justify'>
                                <o:p>&nbsp;</o:p>
                            </p>
                    </div>
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel ID="tabBuyWork" runat="server" HeaderText="Quality Procedure">
                <ContentTemplate>
                    <div id="divMain" style="padding-left: 15px; padding-right: 15px">
                        <p  class="bodyTxt"></p>
                            <br>
                            <p class="paddingB4">
                                <strong>Associate Quality Assurance Policy</strong>
                            </p>
                            <p>
                                The Empowered/OrderWork Associate Quality Assurance Policy helps insure that our
                                client�s installations are completed to the best standards. Strong, positive outcomes
                                encourage clients to continue working with both Empowered/OrderWork and the talented
                                service associate�s available through the Empowered/OrderWork portal platform.</p>
                            <p>
                                When a client/customer reports a quality infraction, Empowered/OrderWork�s team
                                of Service Desk experts follow this Associate Quality Assurance Policy. Examples
                                of the most common Quality Events and consequences are outlined below:
                            </p>
                         <div style="width:100%">
                            <table id="privacy" width="100%" align="center" style="border: 1px solid #e2e2e2"
                                cellspacing="0" cellpadding="0">
                                <tr bgcolor="#993366" valign="middle" style="border: 1px solid #e2e2e2; font-size: 15px;
                                    color: White; font-weight: bold">
                                    <td style="width: 50%" align="center" height="50px">
                                        Infraction Examples
                                    </td>
                                    <td align="center">
                                        Quality Action
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Sexual harassment
                                        </p>
                                        <p>
                                            Physical or verbal assault
                                        </p>
                                        <p>
                                            Unethical behaviour, such as threats, solicitation, or violating Empowered/OrderWork�s
                                            terms and conditions</p>
                                        <p>
                                            Discrimination</p>
                                        <p>
                                            Being under the influence of, using, or possessing alcohol or non-prescription drugs
                                            of any kind while on site</p>
                                        <p>
                                            Committing an illegal act while on site</p>
                                        <p>
                                            Attempting to or carrying out private installations �off-portal� work from a customer
                                            while onsite or after the original installation</p>
                                    </td>
                                    <td align="left" valign="top">
                                        <p>
                                            Deactivation of the Associates Account</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Falsified deliverables, time sheet reports reporting or customer sign off</p>
                                        <p>
                                            Failure to return parts or careless packaging of returned parts</p>
                                        <p>
                                            Failure to attend an assigned and accepted job without attempting to notify the
                                            Empowered/OrderWork team within the agreed timeframes.</p>
                                        <p>
                                            Abandoning a job before completion that could have been completed to the scope of
                                            work</p>
                                        <p>
                                            Asking for or demanding additional payments after accepting a work order i.e. for
                                            fuel, distance etc.</p>
                                    </td>
                                    <td align="left" valign="top">
                                        <p>
                                            <b>Over any consecutive 12 month period </b>
                                        </p>
                                        <br />
                                        <p>
                                            1st Incident: 30 day Portal Suspension from carryout work.</p>
                                        <p>
                                            Incident: Deactivation of the Associates Account</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Failure or refusal to properly complete a work order�s scope of work</p>
                                    </td>
                                    <td align="left" valign="top">
                                        <p>
                                            <b>Over any consecutive 12 month period </b>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Double booking or overbooking causing a installation to fail or require rescheduling</p>
                                        <p>
                                            Falsifying a skill set in order to get more jobs</p>
                                        <p>
                                            Arriving to a job unprepared (without the correct tools, parts, etc.)</p>
                                        <p>
                                            Accepting a job assignment and sending someone else to complete the work order</p>
                                        <p>
                                            Use of foul/offensive language, either with a client, the client�s customers, or
                                            Empowered/OrderWork staff</p>
                                    </td>
                                    <td align="left" valign="top">
                                        <p>
                                            1st Incident: 60 day Portal Suspension from carryout work.</p>
                                        <p>
                                            2nd Incident: 90 day Portal Suspension from carryout work.</p>
                                        <p>
                                            3rd Incident: Deactivation of the Associates Account</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Late arrival or missed ETA</p>
                                        <p>
                                            Violation of the required dress code, as communicated through the work order</p>
                                        <p>
                                            Short notice of cancellations (less than 24-hours)</p>
                                        <p>
                                            Generally being unresponsive to communications attempts through the platform</p>
                                    </td>
                                    <td align="left" valign="top">
                                        <p>
                                            <b>Over any consecutive 12 month period </b>
                                        </p>
                                        <p>
                                            1st Incident: Informal Verbal Warning</p>
                                        <p>
                                            2nd Incident: Formal Verbal Warning</p>
                                        <p>
                                            3rd Incident: Written Warning</p>
                                        <p>
                                            4th Incident: 14 day Portal Suspension from carryout work.</p>
                                        <p>
                                            5th Incident: 30 day Portal Suspension from carryout work.</p>
                                        <p>
                                            6th Incident: Deactivation of the Associates Account</p>
                                    </td>
                                </tr>
                            </table>
                            </div>
                            <br />
                            <p>
                                Empowered/OrderWork reserves the right to initiate the Associate Quality Assurance
                                Policy process for other events that are not listed above, skip steps, modify, and
                                depart from the Associate Quality Assurance Policy process at its sole discretion.
                                When an associates portal access is restricted, they will be unable to request new
                                work. Once an associate�s portal has been unrestricted, they must complete a minimum
                                of 20 jobs that will be monitored and quality checked without any further infractions.</p>
                                <br />
                            <p>
                                If you have any questions regarding incidents or would like to speak with someone
                                directly about the Associate Quality Assurance Policy, please contact Empowered/OrderWork
                                at: <a href="recruitment@empowereduk.com">recruitment@empowereduk.com</a> or <a href="recruitment@orderwork.co.uk">
                                    recruitment@orderwork.co.uk</a><br />
                            </p>
                            </div>
                </ContentTemplate>
            </cc1:TabPanel>
        </cc1:TabContainer>
    </div>
    <div class="clearBoth">
    </div>
</asp:Content>
