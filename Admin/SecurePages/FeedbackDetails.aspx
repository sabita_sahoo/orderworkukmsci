<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile ="~/MasterPage/MyOrderWork.Master" CodeBehind="FeedbackDetails.aspx.vb" Inherits="OrderWorkUK.FeedbackDetails" 
    title="Feedback Details" %>
        <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentHolder" runat="server">
<style type="text/css">
<!--
.cursorHand
{
	cursor: hand;
}
.bottonTopGrey {
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-TR.gif) no-repeat right top;
	height:4px;
}
.bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
}
*+html .bottonBottomGrey
{
	background: url(../OWLibrary/Images/Curves/Bdr-Btn-BR.gif) no-repeat right bottom;
	height:4px;
	background-color:#9C9A9C;
	margin-top:-2px;
}
#divButton {
background-color:#999;
width:56px;
height:19px;
margin:10px 0 0;
padding:0;
}
img.btnCorner {
   width: 4px;
   height: 4px;
   border: none;
   display: block !important;
}
.buttonText {
font-family:Verdana;
font-size:11px;
color:#FFF;
text-decoration:none;
padding-left:11px;
padding-right:8px;
font-style:normal;
font-weight:400;
font-variant:normal;
line-height:11px;
}

.buttonText:hover {
color:#3F3F3F;
text-decoration:none;
}

/*Rounded corners styles*/
.b1, .b2, .b3, .b4{font-size:1px; overflow:hidden; display:block;}
.b1 {height:1px; background:#EFEFEF; margin:0 5px;}
.b2 {height:1px; background:#F7F7F7; border-right:2px solid #EFEFEF; border-left:2px solid #EFEFEF; margin:0 3px;}
.b3 {height:1px; background:#F7F7F7; border-right:2px solid #EFEFEF; border-left:2px solid #EFEFEF; margin:0 2px;}
.b4 {height:1px; background:#F7F7F7; border-right:2px solid #EFEFEF; border-left:2px solid #EFEFEF; margin:0 1px;}

}
-->
</style>
<div id="divTopBand" class="divContainerStyle floatLeft paddingBottom10" >
    <div style="width:992px;padding-left:30px;"  class="clsGreyBandStyleFund marginBottom10">
                             <strong >Feedback Details </strong>
                             </div>
	
	<div id="divMiddleBand">
	 <div style="margin-left:10px;" id="divFeedback" runat="server">
	               	       <div style ="width:675px; " >
             <%--<div><b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b></div>--%>
             <div style="border:1px solid #EFEFEF;padding:10px;">
             <b style="color:#F77500; font-size:13px;">We'd love to hear your views!</b><br /><br />
                    <span style="font-family:Verdana; font-size:12px; color:#8a879a;">Service excellence is what we here at OrderWork strive for every day. We are continually looking at <br />ways to ensure the service we provide to both our clients and Suppliers are second to none. <br />
                    <br />
                    If you have any comments or views about ways to improve the service we provide to you and our <br />clients please submit them using the comments box below. 
                    <br /><br />
                    Thank you</span>
	    	 </div>
			 <%--<div><b class="b4"></b><b class="b3"></b><b class="b2"></b><b class="b1"></b></div>--%>
			 
		   </div>
                <br /><br />
                    <asp:TextBox runat="server" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" ID="txtFeedback" Height="50" Width="670" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtFeedback" WatermarkText="Please enter your feedback here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNote" runat="server" ErrorMessage="<b>Required Field Missing</b><br>Please enter your feedback here" ControlToValidate="txtFeedback" Display="None"></asp:RequiredFieldValidator>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidatorNote"></cc1:ValidatorCalloutExtender>
                    <br /><br />
                    <div>
                    <div id="divbtn" style="float:left;" class="clsButtonGreyViewAll roundifyAddressForIE"><a id="ancSubmit" runat="server" class="txtListing"> Submit&nbsp;</a></div>
                    <div id="divbtn" style="float:left;margin-left:10px;" class="clsButtonGreyViewAll roundifyAddressForIE"><a id="ancBack" href="Welcome.aspx?User=Supplier" runat="server" class="txtListing" > Back&nbsp;</a></div>
                                           
						
						</div>
                </div>
                  
        <div id="divSuccess" runat="server" style="margin-left:10px;margin-top:5px;font-family:Verdana; font-size:12px; color:#8a879a;width:675px; ">
        
        <div style="padding-left:10px;background-color:#FFFFFF; padding-top:10px; padding-bottom:10px;">
        Thank you for providing us with your feedback.<br /> <br /> 
        A member of the service delivery team will be reviewing your comments and will come back to you <br />if necessary.
        </div>
        
        <br />    <br />
        <div id="divbtn" style="float:left; margin-left:250px;" class="clsButtonGreyViewAll roundifyAddressForIE"><a id="ancOK" href="Welcome.aspx?User=Supplier" runat="server" class="txtListing" > <img src="OWLibrary/Images/Icons/Icon-Confirm.gif" width="11" height="11" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;<strong>OK</strong>&nbsp;</a></div>

            
        </div>
	
		<div id="divClearBoth"></div>
	</div>
	</div>
     <div class="clearBoth"></div>
</asp:Content>

