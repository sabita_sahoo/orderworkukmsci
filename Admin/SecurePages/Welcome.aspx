<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Welcome.aspx.vb" Title="OrderWork: Welcome" Inherits="OrderWorkUK.Welcome" MasterPageFile="~/MasterPage/MyOrderWork.Master" ValidateRequest="true"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHolder" runat="server">
<%--<script language="javascript" src="/OWLibrary/JS/Scripts.js"  type="text/javascript"></script>

<script language="javascript" src="/OWLibrary/JS/jquery.js"  type="text/javascript"></script>

<script language="javascript" src="/OWLibrary/JS/DD_roundies.js"  type="text/javascript"></script>--%>
<script type="text/javascript" language="javascript">


    function ShowHideListingTabs(AncId,FromTab) {
        HideAllListingTabs();

        document.getElementById(AncId).style.fontWeight = 'bold';
        $('#' + AncId + "1").removeClass('clsInnerTabSecurePageStyleTabWO');
        $('#' + AncId + "1").addClass('clsInnerTabStyleSecurePageSelectedTabWO');

        var LinkExport = document.getElementById('ctl00_ContentHolder_lnkExportToExcel');
        if (AncId == "ctl00_ContentHolder_ancNewWO") {
            document.getElementById('ctl00_ContentHolder_divNewWorkOrders').style.display = 'block';
            LinkExport.href = 'ExportToExcel.aspx?page=WelcomeNewWorkOrders';
        }
        else if (AncId == "ctl00_ContentHolder_ancTodaysJob") {
            document.getElementById('ctl00_ContentHolder_divTodaysJobs').style.display = 'block';
            LinkExport.href = 'ExportToExcel.aspx?page=WelcomeTodayWorkOrders';
            if (document.getElementById("ctl00_hdnRoleGroupID").value == "19") {
                document.getElementById('ctl00_ContentHolder_DivSupplierListMain').style.width = '901px';
                document.getElementById('ctl00_ContentHolder_DivSupplierListChild').style.width = '901px';
            }
        }
        else if (AncId == "ctl00_ContentHolder_ancTommorowsJob") {
            document.getElementById('ctl00_ContentHolder_divTomorrowsJob').style.display = 'block';
            LinkExport.href = 'ExportToExcel.aspx?page=WelcomeTomorrowWorkOrders';
            if (document.getElementById("ctl00_hdnRoleGroupID").value == "19") {
                document.getElementById('ctl00_ContentHolder_DivSupplierListMain').style.width = '901px';
                document.getElementById('ctl00_ContentHolder_DivSupplierListChild').style.width = '901px';
            }
        }
    }
    function HideAllListingTabs() {

        document.getElementById('ctl00_ContentHolder_divNewWorkOrders').style.display = 'none';
        document.getElementById('ctl00_ContentHolder_divTodaysJobs').style.display = 'none';
        document.getElementById('ctl00_ContentHolder_divTomorrowsJob').style.display = 'none';
        $('#ctl00_ContentHolder_ancNewWO1').removeClass('clsInnerTabStyleSecurePageSelectedTabWO');
        $('#ctl00_ContentHolder_ancNewWO1').addClass('clsInnerTabSecurePageStyleTabWO');
        $('#ctl00_ContentHolder_ancTodaysJob1').removeClass('clsInnerTabStyleSecurePageSelectedTabWO');
        $('#ctl00_ContentHolder_ancTodaysJob1').addClass('clsInnerTabSecurePageStyleTabWO');
        $('#ctl00_ContentHolder_ancTommorowsJob1').removeClass('clsInnerTabStyleSecurePageSelectedTabWO');
        $('#ctl00_ContentHolder_ancTommorowsJob1').addClass('clsInnerTabSecurePageStyleTabWO');
        document.getElementById('ctl00_ContentHolder_ancNewWO1').style.fontWeight = 'normal';
        document.getElementById('ctl00_ContentHolder_ancTodaysJob1').style.fontWeight = 'normal';
        document.getElementById('ctl00_ContentHolder_ancTommorowsJob1').style.fontWeight = 'normal';
        if (document.getElementById("ctl00_hdnRoleGroupID").value == "19") {
            document.getElementById('ctl00_ContentHolder_DivSupplierListMain').style.width = '875px';
            document.getElementById('ctl00_ContentHolder_DivSupplierListChild').style.width = '875px';
        }
    }         
    </script>


<asp:Panel ID="pnlRetail" runat="server" Visible="false">

<%--
<div style="float:LEFT; margin:10px;"><asp:Image ID="imgCompLogoRetail" runat="server"  /></div>
<div style="font-family:Verdana;  font-size:22px; color:#c70000; font-weight:bold; float:left; margin-bottom:10px"><asp:Label runat="server" ID="lblcompanyretail" ></asp:Label></div>
--%>

<div id="divInnerLeftRetail">

     

      <div id="divInnerLeftMiddleBandRetail" >

        <div id="divSummaryContentRetail" class="divAddMoreSkills floatleft" style="width:1001px;">

                  Book your new appointment by clicking the button below.<br /><br />

                  You can view your appointment by using the booking list link found on the left navigation panel.

                  <div id="divSupplyWorkTotal"  style="margin-top:20px;float:right;margin-right:20px;">

                      <div id="divBookInstallation" runat="server" style="float: left;">

                          <div style="width:100%;"><a href="~/SecurePages/BookingForms/RetailWOForm.aspx?sender=Welcome" runat="server" id="lnkBookInst" class="textDecorationNone"><div class="clsButtonGreyDetailsAccept" style="cursor:pointer; margin-left:0px;text-align:center;"><asp:Label ID="lblRetailRoleBookNewJob" runat="server" Text="Book Work Order" CssClass="fontSize16"></asp:Label></div></a><br /></div>                     
                          <a href="../SecurePages/BookingForms/" + menuArr[i][j][2] + ".replace('/SecurePages','')"></a>
                        </div>
                        <a runat="server" id="lnkBookInstBeta" target="_blank" class="textDecorationNone clsButtonGreyDetailsAccept"  style="float:left; margin-left:60px;text-align:center; ">
    <asp:Label ID="lblRetailRoleBookNewJobBeta" runat="server" Text="Book Work Order Beta" CssClass="fontSize16"></asp:Label>
</a>

               </div>     

        </div>

        <div class="clsNoLabelTxt" style=""><asp:Label ID="lblNoProduct" runat="server" Text="No Product descriptions have been defined for your account. Please contact your account administrator" ForeColor="red" Visible="false"></asp:Label></div>

      </div>

      

      <div id="divClearBoth"></div>

</div>

</asp:Panel>

<asp:Panel ID="pnlBuyer" runat="server"  >

<div style="clear:both"></div>
<div class="divRightTop marginBottom10 floatleft" style="width:1022px;">
<div id="divInnerLeftIT" class="clsRounded8 roundifyRefine8">
<div class="clsGreyBandStyle" style="width:1012px;"><b>Current Service Activities</b></div>
<div id="divSummaryContent" class="floatLeft paddingleft10" style="width:900px;">

          <div id="divSummaryHeaderRow1 " class="clsGreyBandStyle" style="width:1002px;margin-bottom:10px;padding-right:10px;margin-top:10px;">

              

              <div class="CenterPlain1 floatRight" style="margin-right:570px;"><b>Total Work Orders</b></div>

           

          </div>

          <%@ Register TagPrefix="uc1" TagName="UCWOPlacedSummary" Src="~/UserControls/UK/UCWOPlacedSummaryIT.ascx" %>

             <uc1:UCWOPlacedSummary id="UCWOPlacedSummary1" runat="server"></uc1:UCWOPlacedSummary>    

        </div>

        <div id="divBookNewJobBuyer" class="clsSummaryContentIT floatLeft clsBookNewJobIT" runat="server" style="float:left;margin-left:17px;margin-top:20px;"><a runat="server" id="lnkBookNewJob" class="textDecorationNone"><div class="clsButtonGreyDetailsAccept" style="cursor:pointer; margin-left:0px; text-align:center;"><asp:Label ID="lblBookNewJob" runat="server" Text="Book Work Order" CssClass="fontSize16"></asp:Label> </div></a><br /></div>                        

        
<a runat="server" id="lnkBookNewJobBeta" class="textDecorationNone clsButtonGreyDetailsAccept" target="_blank" style="float:left; margin-left:60px;margin-top:20px;text-align:center; ">
    <asp:Label ID="lblBookNewJobBeta" runat="server" Text="Book Work Order Beta" CssClass="fontSize16"></asp:Label>
</a>
 <a runat="server" target="_blank" id="btnExport" href="exportToExcel.aspx?page=WelcomeHome" class="textDecorationNone clsButtonGreyDetailsAccept" style="float:left; margin-left:60px;margin-top:20px;text-align:center; ">

        Export

        </a>

        


        <div class="clearBoth"></div>

      <div class="clearBoth"></div>

</div>
</div>
</asp:Panel>
<asp:Panel ID="pnlBuyerWorkListing" runat="server" Visible="false" CssClass="floatLeft">
 <div class="divRightTop marginBottom10 floatleft" style="width:1022px;border-bottom:0px;">
  <div style="width:1022px;background-color:#f8f8f8;float:left;"><a class="clsGreyBandDashboard floatleft cursorPointer" style="font-weight:bold;">Today's Jobs <asp:Label runat="server" ID="lblBuyerTodayDate"></asp:Label></a></div>
  <div class="floatleft" id="divBuyerTodaysJobs" runat="server">
         <div class="floatleft" style="border-bottom:1px solid #e2e2e2;margin-left:-2px;padding-right:2px;">
        <div class="clsGreyBandSubTitle floatleft" style="width:156px;"><b>Customer Name</b></div>
        <div class="clsGreyBandSubTitle floatleft" style="width:248px;"><b>Post code</b></div>
        <div class="clsGreyBandSubTitle floatleft"><b>Appt. Time</b> </div>
        <div class="clsGreyBandSubTitle floatleft"><b>WO No.</b> </div>
        <div class="clsGreyBandSubTitle floatleft"><b>Your Reference</b> </div>
        <div class="clsGreyBandSubTitle floatleft" style="width:175px;border-right:0px;"><b>Action</b></div>
        </div>

            <asp:datalist id="DlBuyerTodayWOListing" runat="server">
                <ItemTemplate>                     
                        <div class="floatleft" style="border-bottom:1px solid #e2e2e2;margin-left:-2px;padding-right:3px;">
                        <div class="clsGreyBandSubTitle floatleft" style="width:155px;"><%# Container.DataItem("CustomerName")%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:248px;"><%# Container.DataItem("PostCode")%></div>
                        <div class="clsGreyBandSubTitle floatleft"><%# Container.DataItem("AptTime")%></div>
                        <div class="clsGreyBandSubTitle floatleft"><%# Container.DataItem("WorkOrderId")%></div>
                        <div class="clsGreyBandSubTitle floatleft"><%# Container.DataItem("PONumber")%> </div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:173px;border-right:0px;">
                        <a class="clsButtonGreyDetails roundifyAddressForIE floatleft"  href='<%#GetSupplierWODetailsLink(Container.DataItem("WOID"),Container.DataItem("WorkOrderId"),Container.DataItem("CompanyID"),Container.DataItem("SupplierContactID"),Container.DataItem("SupplierCompanyID"),IIf(Container.DataItem("Status") = "Active", "Accepted", IIf(Container.DataItem("Status") = "Submitted","InTray",Container.DataItem("Status"))),"Buyer")%>' >
                         <img style="border-width:0px;" alt="My Account" src="../OWLibrary/Images/OrderWorkUkRedesign/Details-icon.gif" class=" floatleft marginLeft10 " width="18" height="16"/> Details </a></div>
                        </div>                            
                </ItemTemplate>
            </asp:datalist>
        </div>

 </div>
</asp:Panel>

<asp:Panel ID="pnlSupplier" runat="server">
 <div class="divLeft floatleft marginBottom10">
                 
                 <asp:Panel ID="pnlAccAlerts" runat="server">
                <div class="divAddMoreSkills roundifyIETop floatleft" style="background-color:#fefaf9;border:1px solid #f6cccd;">
                            <div class="divAddMoreSkillsTop floatleft">
                            <img style="border-width:0px;float:left;margin-right:10px;" width="31" height="28" alt="alert icon" src="../OWLibrary/Images/OrderWorkUkRedesign/Alert-icon.gif"/>
                               <span class="floatleft" style="color:#d23836;">Account Alerts</span>
                               <a class="floatRight" style="margin-top:-5px;font-weight:bold;" id="ancCloseAlerts" runat="server">
                              <%-- 'Poonam modified on 29/07/2015 - OM-1 : OW-DEV-ISSUE-MY Broken image SupplierWODetails.aspx--%>
                               <img style="border-width:0px;float:left;" width="14" height="15" alt="close icon" src="../OWLibrary/Images/OrderWorkUkRedesign/Close-icon.gif"/></a>
                            </div>
                            <div class="marginBottom10" style="line-height:18px;">
                             <asp:label id="lblAccountDetails" SafeEncode="false" runat="server"></asp:label>

                                <asp:label id="lblCompanyDet" SafeEncode="false" runat="server"></asp:label>

                                <asp:label id="lblBillingLocation" SafeEncode="false" runat="server"></asp:label>  
                            </div>
                            
                  </div>          
                
                </asp:Panel>
                
                <asp:Panel ID="pnlDashboardSkills" runat="server">
                     <div class="divWorkOrderForSkillSet floatLeft">
                        <div id="divSkillSetNotificationAddSkill" class="skillSetNotificationAddSkill" runat="server">
                                 <div class="skillSetNotificationTop">Important Message</div>
                                 <div class="skillSetNotificationBottom">If you would like to change your skills, please contact Orderwork.</div>
                              </div>
                    <div class="DialogueBackgroundforMyskillset" id="DialogueBackgroundforCompanyAddSkill" runat="server"></div>
                    <asp:Panel ID="pnlMoreSkills" runat="server">    
                        <%@ Register TagPrefix="uc1" TagName="UCMoreSkills" Src="~/UserControls/UK/UCCommonSkills.ascx"%>
                        <uc1:UCMoreSkills id="UCMoreSkills" runat="server" PageName="Dashboard"></uc1:UCMoreSkills>
                    </asp:Panel>
                    </div>
                </asp:Panel>
               

                <div class="divLeftBottom marginBottom10 floatleft">
                    <%@ Register TagPrefix="uc1" TagName="UCWOAcceptedSummary" Src="~/UserControls/UK/UCWOAcceptedSummary.ascx"%>
                    <uc1:UCWOAcceptedSummary id="UCWOAcceptedSummary1" runat="server"></uc1:UCWOAcceptedSummary>
                </div>

                
</div>

<asp:Panel ID="pnlDashboardAccountSummary" runat="server">
 <div class="divRight floatRight marginBottom10">
          <%@ Register TagPrefix="uc1" TagName="UCAccountSumm" Src="~/UserControls/UK/UCAccountSummary.ascx" %>

          <uc1:UCAccountSumm id="UCAccountSumm1" runat="server"></uc1:UCAccountSumm>

        <div class="clearBoth"></div>
        <div class="divRightTop marginBottom10 floatleft paddingBottom10" style="display:none;">
        <div class="clsGreyBandStyle floatleft"><b>Profile Completeness</b> </div>
        <div class="marginTop10 clsOrangeBorderStyle marginLeft10 floatleft">
                            <div class="clsOrangeTestStyle"><b>80% Completed</b></div>
                            
                            </div>
        </div>
        <div class="clearBoth"></div>
        <div class="divRightTop marginBottom10 floatleft">
        <%@ Register TagPrefix="uc1" TagName="UCFundStatus" Src="~/UserControls/UK/UCFundStatus.ascx" %>

          <uc1:UCFundStatus id="UCFundStatus" runat="server"></uc1:UCFundStatus>
        
        <div class="clearBoth"></div>
        
        
        </div>
        <div class="clearBoth"></div>
       
        </div> 
</asp:Panel>
</asp:Panel>
 <asp:Panel ID="pnlSupplierWorkListing" runat="server" CssClass="floatLeft">
        <div class="divRightTop marginBottom10 floatleft" style="width:1022px;border-bottom:0px;" id="DivSupplierListMain" runat="server">
        <div style="width:1022px;background-color:#993366;float:left;padding-top:4px;" id="DivSupplierListChild" runat="server">
       <div id="ancNewWO" runat="server" onclick="Javascript:ShowHideListingTabs(this.id,'WelcomeNewWorkOrders');" class="floatLeft"><div class="clsLeftBandImage"></div> <a class="clsInnerTabSecurePageStyleTabWO floatleft cursorPointer" id="ancNewWO1" runat="server"  ><b>New Work Orders</b></a> <div class="clsRightBandImage"></div> </div>
       <div id="ancTodaysJob" runat="server"  onclick="Javascript:ShowHideListingTabs(this.id,'WelcomeTodayWorkOrders');" class="floatLeft"><div class="clsLeftBandImage"></div> <a class="clsInnerTabSecurePageStyleTabWO floatleft cursorPointer" id="ancTodaysJob1" runat="server"   ><b>Today's Jobs <asp:Label runat="server" ID="lbTodayDate"></asp:Label></b></a> <div class="clsRightBandImage"></div> </div>
       <div id="ancTommorowsJob" runat="server"  onclick="Javascript:ShowHideListingTabs(this.id,'WelcomeTomorrowWorkOrders');" class="floatLeft"><div class="clsLeftBandImage"></div> <a class="clsInnerTabSecurePageStyleTabWO floatleft cursorPointer" id="ancTommorowsJob1" runat="server"   ><b>Tomorrow's Jobs <asp:Label runat="server" ID="lbTommorowDate"></asp:Label></b> </a> <div class="clsRightBandImage"></div></div>
       <div id="divExportToExcel" style="display:none;" runat="server"><div class="clsRightBandImageExp"></div><a class="clsInnerTabSecurePageStyleTabWO cursorPointer" style="text-align:center; float:right;" runat="server" href="ExportToExcel.aspx?page=WelcomeNewWorkOrders" id="lnkExportToExcel"><b>Export To Excel</b></a><div class="clsLeftBandImageExp"></div></div>

        </div>

       
        <div class="floatleft" id="divNewWorkOrders" runat="server">
        <div class="floatleft" style="border-bottom:1px solid #e2e2e2;padding-right:2px;">
        <div class="clsGreyBandSubTitle floatleft" style="width:100px;"><b>WO NO.</b></div>
        <div class="clsGreyBandSubTitle floatleft" style="width:248px;"><b>Title</b></div>
        <div class="clsGreyBandSubTitle floatleft"><b>Post Code</b> </div>
        <div class="clsGreyBandSubTitle floatleft" style="width:200px;"><b>Start Date</b> </div>
        <div class="clsGreyBandSubTitle floatleft" id="DivNewWorkOrdersValueHeader" runat="server"><b>Price</b> </div>
        <div class="clsGreyBandSubTitle floatleft" style="width:173px;border-right:0px;"><b>Action</b></div>
        </div>
           <asp:datalist id="SupplierDLInTrayWOListing1" runat="server">
                <ItemTemplate>                     
                        <div class="floatleft" style="border-bottom:1px solid #e2e2e2;padding-right:2px;">
                        <div class="clsGreyBandSubTitle floatleft" style="width:99px;"><%#Container.DataItem("WorkOrderId")%></div>
                       <%-- 'Poonam modified on 9/04/2015 - 3861868: EMOW-MY-ISSUE - DASHBOARD: The Text in the New WO Title section needs more characters entered as test is overlapping the edges --%>
                        <div class="clsGreyBandSubTitle floatleft" style="width:248px; height:auto;"><%# Container.DataItem("WOTitle")%></div>
                        <div class="clsGreyBandSubTitle floatleft"><%#Container.DataItem("PostCode")%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:200px;"><%#IIf(Container.DataItem("ClientBusinessArea") = "101", Container.DataItem("DateStart") + " (" + Container.DataItem("AptTime") + ")", Container.DataItem("DateStart"))%></div>
                        <div class="clsGreyBandSubTitle floatleft" id="DivNewWorkOrdersValueItem" runat="server"><%#Container.DataItem("PlatformPrice")%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:171px;border-right:0px;">
                        <a class="clsButtonGreyDetails roundifyAddressForIE floatleft" href='<%#GetSupplierWODetailsLink(Container.DataItem("WOID"),Container.DataItem("WorkOrderId"),Container.DataItem("CompanyID"),Container.DataItem("SupplierContactID"),Container.DataItem("SupplierCompanyID"),IIf(Container.DataItem("Status") = "Conditional Accept", "InTray", IIf(Container.DataItem("Status")= "Submitted","InTray",Container.DataItem("Status"))),"Supplier")%>'  >
                        <img style="border-width:0px;" alt="My Account" src="../OWLibrary/Images/OrderWorkUkRedesign/Details-icon.gif" class=" floatleft marginLeft10 " width="18" height="16"/> Details </a></div>
                        </div>                            
                </ItemTemplate>
            </asp:datalist>            
        </div>


        <div class="floatleft" id="divTodaysJobs" runat="server" style="display:none;">
         <div class="floatleft" style="border-bottom:1px solid #e2e2e2;margin-left:-2px;padding-right:2px;">
        <div class="clsGreyBandSubTitle floatleft" style="width:156px;"><b>WO NO.</b></div>
        <div class="clsGreyBandSubTitle floatleft" style="width:248px;"><b>WO Title</b></div>
        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><b>Post Code</b> </div>
        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><b>Time</b> </div>
        <div class="clsGreyBandSubTitle floatleft" style="width:120px;" id="DivTodaysJobValueHeader" runat="server"><b>Price</b> </div>
        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><b>Status</b> </div>
        <div class="clsGreyBandSubTitle floatleft" style="width:132px;border-right:0px;"><b>Action</b></div>
        </div>

            <asp:datalist id="SupplierDLTodayWOListing1" runat="server">
                <ItemTemplate>                     
                        <div class="floatleft" style="border-bottom:1px solid #e2e2e2;margin-left:-2px;padding-right:3px;">
                        <div class="clsGreyBandSubTitle floatleft" style="width:155px;"><%#Container.DataItem("WorkOrderId")%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:248px; height:auto;"><%# Container.DataItem("WOTitle")%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><%#Container.DataItem("PostCode")%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><%#IIf(Container.DataItem("AptTime") = "", "NA", Container.DataItem("AptTime"))%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:120px;" id="DivTodaysJobValueItem" runat="server"><%#Container.DataItem("PlatformPrice")%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><%# Container.DataItem("Status")%> </div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:129px;border-right:0px;padding-top:5px;padding-bottom:5px;">
                        <a class="clsButtonGreyDetails roundifyAddressForIE floatleft" style="margin-left:20px;" href='<%#GetSupplierWODetailsLink(Container.DataItem("WOID"),Container.DataItem("WorkOrderId"),Container.DataItem("CompanyID"),Container.DataItem("SupplierContactID"),Container.DataItem("SupplierCompanyID"),IIf(Container.DataItem("Status") = "Active", "Accepted", IIf(Container.DataItem("Status")= "Submitted","InTray",Container.DataItem("Status"))),"Supplier")%>' >
                        <img style="border-width:0px;" alt="My Account" src="../OWLibrary/Images/OrderWorkUkRedesign/Details-icon.gif" class=" floatleft marginLeft10 " width="18" height="16"/> Details </a></div> 
                        </div>                            
                </ItemTemplate>
            </asp:datalist>
        </div>
        <div class="floatleft" id="divTomorrowsJob" runat="server" style="display:none;">
        <div class="floatleft" style="border-bottom:1px solid #e2e2e2;margin-left:-2px;padding-right:2px;">
        <div class="clsGreyBandSubTitle floatleft" style="width:156px;"><b>WO NO.</b></div>
        <div class="clsGreyBandSubTitle floatleft" style="width:248px;"><b>WO Title</b></div>
        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><b>Post Code</b> </div>
        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><b>Time</b> </div>
        <div class="clsGreyBandSubTitle floatleft" style="width:120px;" id="DivTomorrowsJobValueHeader" runat="server"><b>Price</b> </div>
        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><b>Status</b> </div>
        <div class="clsGreyBandSubTitle floatleft" style="width:132px;border-right:0px;"><b>Action</b></div>
        </div>
        <asp:datalist id="SupplierDLTomorrowsWOListing1" runat="server">
                <ItemTemplate>                     
                        <div class="floatleft" style="border-bottom:1px solid #e2e2e2;margin-left:-2px;padding-right:3px;">
                        <div class="clsGreyBandSubTitle floatleft" style="width:155px;"><%#Container.DataItem("WorkOrderId")%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:248px; height:auto;"><%# Container.DataItem("WOTitle")%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><%#Container.DataItem("PostCode")%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><%#IIf(Container.DataItem("AptTime") = "", "NA", Container.DataItem("AptTime"))%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:120px;" id="DivTomorrowsJobValueItem" runat="server"><%#Container.DataItem("PlatformPrice")%></div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:120px;"><%# Container.DataItem("Status")%> </div>
                        <div class="clsGreyBandSubTitle floatleft" style="width:129px;border-right:0px;padding-top:5px;padding-bottom:5px;">
                        <a class="clsButtonGreyDetails roundifyAddressForIE floatleft" style="margin-left:20px;"  href='<%#GetSupplierWODetailsLink(Container.DataItem("WOID"),Container.DataItem("WorkOrderId"),Container.DataItem("CompanyID"),Container.DataItem("SupplierContactID"),Container.DataItem("SupplierCompanyID"),IIf(Container.DataItem("Status") = "Active", "Accepted", IIf(Container.DataItem("Status")= "Submitted","InTray",Container.DataItem("Status"))),"Supplier")%>' >
                        <img style="border-width:0px;" alt="My Account" src="../OWLibrary/Images/OrderWorkUkRedesign/Details-icon.gif" class=" floatleft marginLeft10 " width="18" height="16"/> Details </a></div>
                        </div>                            
                </ItemTemplate>
            </asp:datalist>
        </div>

       
        
        </div>
         </asp:Panel>   
         <script type="text/javascript" language="javascript">

        ShowHideExportToExcel();
       function ShowHideExportToExcel() {
            var RoleGroupId = <%=Session("RoleGroupID") %>;
            if ((RoleGroupId == 1) || (RoleGroupId == 2) || (RoleGroupId == 4) || (RoleGroupId == 5) || (RoleGroupId == 7) || (RoleGroupId == 10) || (RoleGroupId == 11) || (RoleGroupId == 16) || (RoleGroupId == 17))
            {
                var ExportButton = document.getElementById('ctl00_ContentHolder_divExportToExcel');
                if (ExportButton != null) {
                  ExportButton.style.display = 'block';}
            }     
        }

        </script>
        
</asp:Content>

 
