Partial Public Class CompanyProfileBuyer
    Inherits System.Web.UI.Page

    Protected WithEvents UCTopMSgLabel1 As UCTopMSgLabel
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents UCCompanyProfile1 As UCMSCompanyProfile
    Protected WithEvents UCAccountSumm1 As UCAccountSummary
    Protected WithEvents lnkViewFavSuppliers As System.Web.UI.HtmlControls.HtmlAnchor

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        'Security.SetPageControlsByAdmin(Me, GetContact(True, True, Session("ContactCompanyID")).Tables(15).Copy.DefaultView)

        'Security.GetPageControls(Page)
        If Not IsPostBack Then


            If Not IsNothing(Request("companyid")) Then
                If Request("companyid") <> "" Then
                    Session("ContactCompanyID") = Request("companyid")
                End If
            End If
            Dim ComID As String = Request("companyid")
            If Not IsNothing(Request("bizdivid")) Then
                If Request("bizdivid") <> "" Then
                    Session(ComID & "_ContactBizDivID") = Request("bizdivid")
                End If
            End If


            If Not IsNothing(Request("rolegroupid")) Then
                If Request("rolegroupid") <> "" Then
                    Session(ComID & "_ContactRoleGroupID") = Request("rolegroupid")
                End If
            End If

            If Not IsNothing(Request("userid")) Then
                If Request("userid") <> "" Then
                    Session(ComID & "_ContactUserID") = Request("userid")
                End If
            End If

            If Not IsNothing(Request("classid")) Then
                If Request("classid") <> "" Then
                    Session(ComID & "_ContactClassID") = Request("classid")
                End If
            End If

            If Not IsNothing(Request("contacttype")) Then
                If Request("contacttype") <> "" Then
                    Session(ComID & "_ContactType") = Request("contacttype")
                End If
            End If

            If Not IsNothing(Request("statusId")) Then
                If Request("statusId") <> "" Then
                    Session(ComID & "_ContactStatusId") = Request("statusId")
                End If
            End If

            If Not IsNothing(Request("mode")) Then
                If Request("mode") <> "" Then
                    Session(ComID & "_ListingMode") = Request("mode")
                End If
            End If


            ' Code to Show/Hide control for OW Admin
            Dim pagename As String
            pagename = CType(CType(Page.Request.Url, Object), System.Uri).AbsolutePath()
            'pagename = pagename.ToLower.Substring(pagename.LastIndexOf("/") + 1, (pagename.Length - 1) - pagename.LastIndexOf("/"))
            pagename = pagename.Substring(pagename.LastIndexOf("/") + 1, (pagename.Length - 1) - pagename.LastIndexOf("/"))
            Dim dvpriv As DataView = UCMSCompanyProfile.GetContact(Me, Request("bizDivId"), True, True, Request("companyid")).Tables(6).Copy.DefaultView
            dvpriv.RowFilter = "PageName Like('" & pagename & "') "
            Security.GetPageControls(Page, dvpriv)

            'Following session variable added to fix the issue: Admin Company profile back to listing does not work
            If Not IsNothing(Request("sender")) Then
                Session("CompanyProfileQS") = "?sender=" & Request("sender") & "&bizDivId=" & Request("bizDivId") & "&contactType=" & Request("contactType") & "&companyid=" & Request("companyid") & "&userid=" & Request("userid") & "&classid=" & Request("classid") & "&statusId=" & Request("statusId") & "&rolegroupid=" & Request("rolegroupid") & "&mode=" & Request("mode") & "&ps=" & Request("ps") & "&pn=" & Request("pn") & "&sc=" & Request("sc") & "&so=" & Request("so") & "&fromDate=" & Request("fromDate") & "&toDate=" & Request("toDate")
            End If
        End If
        Dim CompID As String = Request("companyid")
        UCCompanyProfile1.CompanyID = Request("companyid")
        UCCompanyProfile1.BizDivID = Session(CompID & "_ContactBizDivID")
        UCCompanyProfile1.UserID = Session(CompID & "_ContactUserID")
        UCCompanyProfile1.ClassID = Session(CompID & "_ContactClassID")
        If Session(CompID & "_ContactClassID") = ApplicationSettings.RoleSupplierID Then
            UCAccountSumm1.ShowRating = True
        ElseIf Session(CompID & "_ContactClassID") = ApplicationSettings.RoleClientID Then
            UCAccountSumm1.ShowRating = False
        End If
        UCAccountSumm1.BizDivId = Request("bizDivId")

        'Following session variable added to fix the issue: Admin Company profile back to listing does not work
        UCCompanyProfile1.strBackToListing = Session("CompanyProfileQS")

        'Show Favourite supplier link
        lnkViewFavSuppliers.Visible = False
        'poonam modified on 13/7/2016 - Task - OA-296 : OA - Missing Favorite Suppliers link on Account profile
        If Session(CompID & "_ContactClassID") = ApplicationSettings.RoleClientID Then
            lnkViewFavSuppliers.Visible = True
        End If

        'Prepare AutoMatch Link URL
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        lnkAutoMatch.HRef = "AutoMatchSettings.aspx?CompanyID=" & Request("CompanyID") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid")
        lnkAccSettings.HRef = "AccountSettings.aspx?CompanyID=" & Request("CompanyID") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid")
        lnkViewAllInvoices.HRef = "ViewAllInvoices.aspx?CompanyID=" & Request("CompanyID") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid")
        lnkViewAccountHistory.HRef = "AccountHistory.aspx?CompanyID=" & Request("companyid") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid")
        lnkViewFavSuppliers.HRef = "FavouriteSupplierList.aspx?mode=favourite&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid")
        lnkAddProduct.HRef = "AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid")
        lnkAddProductNew.HRef = "ServiceListing.aspx?CompanyID=" & Request("CompanyID")
        lnkViewBlackListsupplier.HRef = "FavouriteSupplierList.aspx?mode=blacklist&CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid") & "&sender=buyer"
        lnkReferences.HRef = "References.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid")
        lnkuser.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid") & "&sender=buyer"
        lnkloc.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid") & "&sender=buyer"
        'lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        lnkAddUsers.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid") & "&sender=buyer"
        lnkAddLocationsSupp.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid") & "&sender=buyer"
        lnkComments.HRef = "Comments.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid")
    End Sub

End Class