Public Partial Class ViewUpSellSalesInvoice
    Inherits System.Web.UI.Page

    Protected WithEvents ltCountry As Literal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If ApplicationSettings.Country = ApplicationSettings.CountryDE Then
            ltCountry.Text = "var country = 'DE'"
        Else
            ltCountry.Text = "var country = 'UK'"
        End If
    End Sub

End Class