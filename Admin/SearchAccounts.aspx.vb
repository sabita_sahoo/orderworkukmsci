


Partial Public Class SearchAccounts
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected WithEvents UCRegions As UCDualListBox
    Protected WithEvents rdAOEList As RadioButtonList
    Protected WithEvents UCAOE As UCTableAOE
    Protected WithEvents UCAccreditations As UCAccreditationForSearch

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (CType(UCAOE.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "" Then
            If (CType(UCAOE.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "0" Then
                hdnProductIds.Value = CType(UCAOE.FindControl("hdnCombIDs"), HtmlInputHidden).Value
            Else
                hdnProductIds.Value = ""
            End If
        Else
            hdnProductIds.Value = ""
        End If
        ' Populate Selected value of Area of expertise
        UCAOE.CombIds = hdnProductIds.Value
        UCAOE.PopulateAOE()
        UCAOE.PageName = "NoMainCatSel"

        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            UCAccreditations.Type = "AllAccreditations"
            If Not IsNothing(Request("sender")) Then
                If Request("sender") = "AdminSearchAccounts" Then
                    processBackToListing()
                Else
                    InitializeForm()
                End If
            Else
                InitializeForm()
            End If
        End If

    End Sub
    ''' <summary>
    ''' Initialize all the fields on the form
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub InitializeForm(Optional ByVal resetFields As Boolean = True)

        gvContacts.PageSize = ApplicationSettings.GridViewPageSize
        'Initialize the Bizdivid with OrderWrok ID
        If IsNothing(ViewState("BizDivId")) Then
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryUK
                    ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
                    UCAOE.BizDivId = ApplicationSettings.OWUKBizDivId
                Case ApplicationSettings.CountryDE
                    ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
                    UCAOE.BizDivId = ApplicationSettings.OWDEBizDivId
            End Select
        End If
        ' Populate regions
        UCRegions.populateListBox(Nothing, CommonFunctions.GetRegions(Page), "RegionName", "SettingValue", "RegionID")

        ' Populate Selected value of Area of expertise
        UCAOE.CombIds = hdnProductIds.Value
        UCAOE.PopulateAOE()

        'UCCompAccreds1.BizDivId = ViewState("BizDivId")
        'UCCompAccreds1.CompanyID = 0
        'UCCompAccreds1.PopulateSelectedGrid()


        UCAccreditations.BizDivId = ViewState("BizDivId")
        UCAccreditations.CommonID = 0

        UCAccreditations.PopulateSelectedGrid()

        'Populate the SkillsArea
        Dim dsXML As New DataSet()
        dsXML.ReadXml(MapPath("~/XML/WOFormStandards.xml"))
        'Dim dv_skills As DataView = dsXML.Tables("skillsarea").DefaultView
        ' ddlSkillsArea.DataSource = dv_skills
        'ddlSkillsArea.DataBind()
        ' Set visibility of columns
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryUK
                Select Case ViewState("BizDivId")
                    Case ApplicationSettings.OWUKBizDivId
                        gvContacts.Columns(5).Visible = False ' Ref Checked
                        gvContacts.Columns(6).Visible = False ' Has validated Specialist                     
                    Case ApplicationSettings.SFUKBizDivId
                        gvContacts.Columns(5).Visible = True ' Ref Checked
                        gvContacts.Columns(6).Visible = True ' Has validated Specialist                        
                End Select
            Case ApplicationSettings.CountryDE
                gvContacts.Columns(5).Visible = False ' Ref Checked
                gvContacts.Columns(6).Visible = False ' Has validated Specialist                
        End Select


        If resetFields Then
            txtKeyword.Text = ""
            txtPostCode.Text = ""
            txtFName.Text = ""
            txtLName.Text = ""
            txtCompanyName.Text = ""
            txtEmail.Text = ""
            lblSearchCriteria.Text = ""
            txtContactIdAccountId.Text = ""
            pnlSearchResult.Visible = False
            ViewState!SortExpression = "CompanyName"
            gvContacts.Sort(ViewState!SortExpression, SortDirection.Ascending)
        Else
            ' Repopulate region list to show selected regions
            UCRegions.showSelected(hdnRegionValue.Value)
        End If

    End Sub
    ''' <summary>
    ''' Search Function which handles forms submission event
    ''' and preapres the search result
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub Search_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If chkNearestTo.Checked Then
                If txtPostCode.Text.Trim = "" Then
                    lblInvalidPostCode.Visible = True
                    lblInvalidPostCode.Text = ResourceMessageText.GetString("ReqpostCode")
                    Return
                Else
                    Dim IsValid As Boolean
                    If ApplicationSettings.ValidatePostcodeUK = "True" Then
                        If Regex.IsMatch(txtPostCode.Text.Trim.ToString.ToUpper, ApplicationSettings.ValidateExpressionPC) Then
                            IsValid = True
                        End If
                    Else
                        IsValid = True
                    End If
                    If Not IsValid Then
                        lblInvalidPostCode.Visible = True
                        lblInvalidPostCode.Text = "The post code is incorrect."
                        Return
                    End If
                End If
            End If
            lblInvalidPostCode.Text = ""
            lblInvalidPostCode.Visible = False
            ' display search criteria
            lblSearchCriteria.Text = PrepareSearchCriteria()

            If chkRefChecked.Checked Then
                ViewState("statusId") = ApplicationSettings.StatusApprovedCompany
            Else
                ViewState("statusId") = ""
            End If

            pnlSearchResult.Visible = True
            PopulateGrid()
            gvContacts.PageIndex = 0
            'Sumit - Populate Selected value of Area of expertise
            UCAOE.CombIds = hdnProductIds.Value
            UCAOE.PopulateAOE()
        Catch ex As Exception
            CommonFunctions.createLog("Issue in Search Account " & ex.ToString)
        End Try

    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function PrepareSearchCriteria() As String
        Dim searchCriteria As String = ""
        searchCriteria &= "<strong>Search Accounts Criteria:</strong>"
        hdnRegionName.Value = getSelectedRegions("Text")
        hdnRegionValue.Value = getSelectedRegions("Value")
        hdnVendorName.Value = GetAccredinCSV("Text")
        hdnVendorValue.Value = GetAccredinCSV("Value")

        'region name
        If hdnRegionName.Value.EndsWith(",") Then
            hdnRegionName.Value = hdnRegionName.Value.Substring(0, hdnRegionName.Value.Length - 1)
        End If
        If hdnRegionValue.Value.EndsWith(",") Then
            hdnRegionValue.Value = hdnRegionValue.Value.Substring(0, hdnRegionValue.Value.Length - 1)
        End If
        If hdnRegionName.Value <> "" Then
            searchCriteria &= " Region - <span class='txtOrange'>" & hdnRegionName.Value & "</span>"
        End If

        'vendor name
        If hdnVendorName.Value.EndsWith(",") Then
            hdnVendorName.Value = hdnVendorName.Value.Substring(0, hdnVendorName.Value.Length - 1)
        End If
        If hdnVendorValue.Value.EndsWith(",") Then
            hdnVendorValue.Value = hdnVendorValue.Value.Substring(0, hdnVendorValue.Value.Length - 1)
        End If
        If hdnVendorName.Value.Trim <> "" Then
            searchCriteria &= " Vendor - <span class='txtOrange'>" & hdnVendorName.Value.Trim & "</span>"
        End If

        'keyword
        If txtKeyword.Text.Trim <> "" Then
            searchCriteria &= ", Keyword - <span class='txtOrange'>" & txtKeyword.Text.Trim & "</span>"
        End If

        'postcode
        If txtPostCode.Text.Trim <> "" Then
            searchCriteria &= ", PostCode - <span class='txtOrange'>" & txtPostCode.Text.Trim & "</span>"
        End If

        'first name
        If txtFName.Text.Trim <> "" Then
            searchCriteria &= ", First Name - <span class='txtOrange'>" & txtFName.Text.Trim & "</span>"
        End If

        'last name
        If txtLName.Text.Trim <> "" Then
            searchCriteria &= ", Last Name - <span class='txtOrange'>" & txtLName.Text.Trim & "</span>"
        End If

        'company name
        If txtCompanyName.Text.Trim <> "" Then
            searchCriteria &= ", Company Name - <span class='txtOrange'>" & txtCompanyName.Text.Trim & "</span>"
        End If

        'email
        If txtEmail.Text.Trim <> "" Then
            searchCriteria &= ", Email - <span class='txtOrange'>" & txtEmail.Text.Trim & "</span>"
        End If

        'ContactId/AccountId
        If txtContactIdAccountId.Text.Trim <> "" Then
            searchCriteria &= ", ContactId - <span class='txtOrange'>" & txtContactIdAccountId.Text.Trim & "</span>"
        End If

        'ref checked
        If chkRefChecked.Checked = True Then
            searchCriteria &= " Reference Checked - <span class='txtOrange'>Yes</span>"
        Else
            searchCriteria &= " Reference Checked - <span class='txtOrange'>No</span>"
        End If
        'CRB Check
        'If chkCRBChecked.Checked = True Then
        '    searchCriteria &= " DBS Checked - <span class='txtOrange'>Yes</span>"
        'Else
        '    searchCriteria &= " DBS Checked - <span class='txtOrange'>No</span>"
        'End If
        'DBS(Check)
        If CRBCheckedEnhanced.Checked = True Then
            searchCriteria &= " DBS Checked - <span class='txtOrange'>Enhanced</span>"
        ElseIf CRBCheckedYes.Checked = True Then
            searchCriteria &= " DBS Checked - <span class='txtOrange'>Basic</span>"
        Else
            searchCriteria &= " DBS Checked - <span class='txtOrange'>No</span>"
        End If

        'ExcelPartner
        If chkExcelPartner.Checked = True Then
            searchCriteria &= " Excel Partner - <span class='txtOrange'>Yes</span>"
        Else
            searchCriteria &= " Excel Partner - <span class='txtOrange'>No</span>"
        End If
        'EPCP Check
        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        If chkEPCP.Checked = True Then
            searchCriteria &= " EPCP - <span class='txtOrange'>Yes</span>"
        Else
            searchCriteria &= " EPCP - <span class='txtOrange'>No</span>"
        End If

        'CRB Check
        If chkCSCS.Checked = True Then
            searchCriteria &= " CSCS checked - <span class='txtOrange'>Yes</span>"
        Else
            searchCriteria &= " CSCS checked - <span class='txtOrange'>No</span>"
        End If

        'UK Security Clearance
        If chkUKSecurity.Checked = True Then
            searchCriteria &= " UK Security Clearance - <span class='txtOrange'>Yes</span>"
        Else
            searchCriteria &= " UK Security Clearance - <span class='txtOrange'>No</span>"
        End If
        'Search Track Supplier
        If chkTrackSP.Checked = True Then
            searchCriteria &= " Track Supplier - <span class='txtOrange'>Yes</span>"
        Else
            searchCriteria &= " Track Supplier - <span    class='txtOrange'>No</span>"
        End If

        'searchCriteria &= " Skills Area - <span class='txtOrange'>" & ddlSkillsArea.SelectedItem.Text & "</span>"
        'reset search criteria
        hdnRegionName.Value = ""
        Return searchCriteria
    End Function



    ''' <summary>
    ''' Populates the list when new tab is selected for bizdiv
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ChangeSite(ByVal sender As Object, ByVal e As System.EventArgs)
        If CType(sender, LinkButton).ID = "lnkBtnOrderWork" Then
            'OrderWork Site
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryUK
                    ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
                    UCAOE.BizDivId = ApplicationSettings.OWUKBizDivId
                    UCAOE.PopulateAOE()
                Case ApplicationSettings.CountryDE
                    ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
                    UCAOE.BizDivId = ApplicationSettings.OWDEBizDivId
                    UCAOE.PopulateAOE()
            End Select

        Else
            'SkillsFinder Site
            'Currently SkillsFinder Site only exists for UK. 
            ViewState("BizDivId") = ApplicationSettings.SFUKBizDivId
            UCAOE.BizDivId = ApplicationSettings.SFUKBizDivId
            UCAOE.PopulateAOE()

        End If
        InitializeForm()

    End Sub

    Public Sub PopulateGrid(Optional ByVal PageIndex As Integer = 0, Optional ByVal SortExpression As String = "", Optional ByVal SortDirection As String = "")
        Page.Validate()
        If Page.IsValid Then
            setGridSettings()

            If SortExpression <> "" Then
                gvContacts.Sort(SortExpression, SortDirection)
            End If
            If PageIndex > 0 Then
                gvContacts.PageIndex = PageIndex
            Else
                gvContacts.PageIndex = 0
            End If
            gvContacts.DataBind()
        End If
    End Sub

    Public Sub setGridSettings()
        If chkNearestTo.Checked = True Then
            gvContacts.Columns.Item(11).Visible = True
            gvContacts.Sort("Distance", SortDirection.Ascending)
        Else
            gvContacts.Columns.Item(11).Visible = False
            gvContacts.Sort("CompanyName", SortDirection.Ascending)
        End If
    End Sub

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As Object

        If chkNearestTo.Checked = True Then
            If sortExpression = "" Then
                sortExpression = "Distance"
                gvContacts.Sort("Distance", SortDirection.Ascending)
            End If
        Else
            txtPostCode.Text = ""
            If sortExpression = "" Then
                sortExpression = "CompanyName"
                gvContacts.Sort("CompanyName", SortDirection.Ascending)
            End If
        End If
        Dim rowCount As Integer
        Dim CRBChecked As String = "No"
        Dim UKSecurity As String = "No"
        Dim vendorIDs As String = GetAccredinCSV("Value")
        Dim bizDivId As Integer = ViewState("BizDivId")
        Dim statusId As String = ViewState("statusId")
        Dim skillSet As String = ""
        If (CType(UCAOE.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "" Then
            If (CType(UCAOE.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "0" Then
                skillSet = CType(UCAOE.FindControl("hdnCombIDs"), HtmlInputHidden).Value
                If (skillSet.Substring(0, 2) = "0,") Then
                    skillSet = skillSet.Remove(0, 2)
                End If
            End If
        End If
        hdnProductIds.Value = skillSet
        Dim region As String = getSelectedRegions("Value")
        Dim postCode As String = txtPostCode.Text.Trim
        Dim keyword As String = txtKeyword.Text.Trim
        Dim fName As String = txtFName.Text.Trim
        Dim lName As String = txtLName.Text.Trim
        Dim companyName As String = txtCompanyName.Text.Trim
        Dim eMail As String = txtEmail.Text.Trim
        Dim ContactId As Integer = IIf(txtContactIdAccountId.Text.Trim.Length > 0, txtContactIdAccountId.Text.Trim, 0)
        Dim EngineerCRBCheck As Integer
        Dim EngineerCSCSCheck As Boolean
        Dim EngineerUKSecurityCheck As Boolean
        Dim EngineerRightToWorkInUK As Boolean
        Dim EngProofOfMeeting As Boolean
        Dim strCRB As String = "No"
        Dim strUKSec As String = "No"
        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        Dim strExcelPartner As String = "No"
        Dim strEPCP As String = "No"
        Dim strCSCS As String = "No"
        Dim strTrackSp As Boolean = 0
        If chkCRBChecked.Checked Then
            strCRB = "Yes"
        End If

        If chkUKSecurity.Checked Then
            strUKSec = "Yes"
        End If

        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        If chkExcelPartner.Checked Then
            strExcelPartner = "Yes"
        End If
        If chkEPCP.Checked Then
            strEPCP = "Yes"
        End If

        If chkCSCS.Checked Then
            strCSCS = "Yes"
        End If
        'Search Track Supplier
        If chkTrackSP.Checked Then
            strTrackSp = 1
        End If


        ' If Find Nearest to is checked then Selected regions shoudl not be considered in the Match Supplier fetch logic hence 
        ' sending region code as blank
        If chkNearestTo.Checked = True Then
            region = ""
            If txtPostCode.Text.Trim <> "" Then
                Dim WOLatitude As String = "Invalid"
                Dim WOLongitude As String = "Invalid"


                Dim dt As DataTable
                dt = ws.WSWorkOrder.WO_GetPostCodeCoordinates(txtPostCode.Text.Trim).Tables(0)
                If Not IsNothing(dt) Then
                    If (dt.Rows.Count > 0) Then
                        WOLatitude = dt.Rows(0).Item("Latitude")
                        WOLongitude = dt.Rows(0).Item("Longitude")
                    End If
                End If

                If WOLatitude = "Invalid" Or WOLongitude = "Invalid" Or WOLatitude = "" Or WOLongitude = "" Then
                    Dim details As Newtonsoft.Json.Linq.JObject = CommonFunctions.GetPostcodeLatLong(txtPostCode.Text.Trim)
                    If IsNothing(details) Then
                        lblInvalidPostCode.Visible = True
                        lblInvalidPostCode.Text = "There has been some issue with post code fetch"
                        Return Nothing
                    End If
                End If
            End If
        Else
            lblInvalidPostCode.Visible = False
            lblInvalidPostCode.Text = ""
        End If
        If (CRBCheckedEnhanced.Checked = True) Then
            EngineerCRBCheck = 137
        ElseIf (CRBCheckedYes.Checked = True) Then
            EngineerCRBCheck = 1
        Else
            EngineerCRBCheck = 0
        End If

        EngineerCSCSCheck = chkEngCSCS.Checked
        EngineerUKSecurityCheck = chkEngUKSecurity.Checked
        EngineerRightToWorkInUK = chkEngRightToWork.Checked
        EngProofOfMeeting = chkEngProofOfMeeting.Checked
        Dim ds As DataSet = ws.WSContact.GetMSSearchAccounts(bizDivId, skillSet, region, postCode, keyword, statusId, fName, lName, companyName, eMail, vendorIDs, strCRB, strCSCS, strUKSec, ddlNearestTo.SelectedValue, sortExpression, startRowIndex, maximumRows, rowCount, strTrackSp, EngineerCRBCheck, EngineerCSCSCheck, EngineerUKSecurityCheck, EngineerRightToWorkInUK, EngProofOfMeeting, ContactId, strExcelPartner, strEPCP, drpCategoryType.SelectedItem.Value)
        ViewState("rowCount") = rowCount
        'If chkNearestTo.Checked = True Then
        '    If Not IsNothing(ds.Tables(2)) Then
        '        If ds.Tables(2).Rows.Count > 0 Then
        '            Dim WOGridEast As String = ""
        '            Dim WOGridNorth As String = ""
        '            OrderWork.Process.getWOPostCodeGeoCode(ds.Tables(2), WOGridEast, WOGridNorth)
        '            Dim isError As Boolean = False
        '            If WOGridEast = "Invalid" Or WOGridNorth = "Invalid" Then
        '                lblInvalidPostCode.Text = ResourceMessageText.GetString("PostCodeIgnored")
        '                lblInvalidPostCode.Visible = True
        '            Else
        '                lblInvalidPostCode.Visible = False
        '            End If
        '        Else
        '            lblInvalidPostCode.Text = ResourceMessageText.GetString("PostCodeIgnored")
        '            lblInvalidPostCode.Visible = True
        '        End If
        '    Else
        '        lblInvalidPostCode.Text = ResourceMessageText.GetString("PostCodeIgnored")
        '        lblInvalidPostCode.Visible = True
        '    End If
        'Else
        '    lblInvalidPostCode.Text = ResourceMessageText.GetString("PostCodeIgnored")
        '    lblInvalidPostCode.Visible = False
        'End If


        Return ds
    End Function
    ''' <summary>
    ''' ObjectDataSource Controls select Count Method which returns the row count for the listing 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        gvContacts.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid(gvContacts.PageIndex, gvContacts.SortExpression, gvContacts.SortDirection)
    End Sub

    Private Sub gvContacts_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvContacts.PreRender
        Me.gvContacts.Controls(0).Controls(Me.gvContacts.Controls(0).Controls.Count - 1).Visible = True
        If ViewState("rowCount") < 1 Then
            Return
        End If
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

            End If
        End If
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowDataBound
        'Poonam added on 31/03/2015 -4044201 - To disable export to excel button to non admin users
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim tdsetType1 As HtmlTableCell = e.Row.FindControl("tdsetType1")
            Dim IsAdmin As Boolean
            IsAdmin = False

            If Not IsNothing(Session("RoleGroupID")) Then
                Select Case Session("RoleGroupID")
                    Case ApplicationSettings.RoleSupplierAdminID
                        IsAdmin = True
                    Case ApplicationSettings.RoleClientAdminID
                        IsAdmin = True
                    Case ApplicationSettings.RoleOWAdminID
                        IsAdmin = True
                        'Updated by Sabit for OA-627 : Add account search export button to Manager and Representative roles
                    Case ApplicationSettings.RoleOWRepID
                        IsAdmin = True
                    Case ApplicationSettings.RoleOWManagerID
                        IsAdmin = True
                    Case ApplicationSettings.RoleOWFinanceID
                        IsAdmin = True
                        'Updated by Sumit for OA-724 : OA - In finance role can we add export to excel feature in account search page
                    Case ApplicationSettings.RoleFinanceManagerID
                        IsAdmin = True
                End Select
            End If

            If IsAdmin = True Then
                tdsetType1.Visible = True
            Else
                tdsetType1.Visible = False
            End If
        End If
        MakeGridViewHeaderClickable(gvContacts, e.Row)
    End Sub
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvContacts, e.Row, Me)
        End If

    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)
        Dim btnExport As HtmlAnchor = CType(gvPagerRow.FindControl("btnExport"), HtmlAnchor)

        btnExport.HRef = prepareExportToExcelLink()

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then

            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex

    End Sub
    ''' <summary>
    ''' Returns the links column which has different images and links associated with it
    ''' </summary>
    ''' <param name="paramBizDivId"></param>
    ''' <param name="companyId"></param>
    ''' <param name="contactId"></param>
    ''' <param name="classId"></param>
    ''' <param name="paramstatusId"></param>
    ''' <param name="roleGroupId"></param>
    ''' <param name="Status"></param>
    ''' <param name="paramactionDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLinks(ByVal bizDivId As Integer, ByVal companyId As Integer, ByVal contactId As Integer, ByVal classId As Integer, ByVal statusId As Object, ByVal roleGroupId As Integer, ByVal CompanyName As String, ByVal contactname As String, ByVal refChkDate As Object, ByVal Email As String, ByVal BusinessArea As String) As String

        Dim link As String = ""
        Dim companyPageName As String = ""
        Dim invoicePageName As String = ""
        Dim linkParams As String = ""

        Dim linkStatusId As String = ""
        If chkRefChecked.Checked Then
            linkStatusId = ApplicationSettings.StatusApprovedCompany
        Else
            linkStatusId = ""
        End If

        '   this code decides which page is used to show company profile depending upon classid
        If classId = ApplicationSettings.RoleSupplierID Then
            companyPageName = "CompanyProfile.aspx"
            invoicePageName = "SupplierInvoices.aspx"
        ElseIf classId = ApplicationSettings.RoleClientID Then
            companyPageName = "CompanyProfileBuyer.aspx"
            invoicePageName = "Invoices.aspx"
        End If


        linkParams &= "bizDivId=" & bizDivId
        linkParams &= "&companyId=" & companyId
        linkParams &= "&contactId=" & contactId
        linkParams &= "&classId=" & classId
        linkParams &= "&statusId=" & linkStatusId
        linkParams &= "&roleGroupId=" & roleGroupId
        linkParams &= "&vendorIDs=" & GetAccredinCSV("Value")
        'linkParams &= "&skillsarea=" & ddlSkillsArea.SelectedItem.Value
        linkParams &= "&product=" & hdnProductIds.Value.Trim
        linkParams &= "&region=" & hdnRegionValue.Value.Trim
        linkParams &= "&txtPostCode=" & txtPostCode.Text.Trim
        'Poonam modified on 31-03-2017 - Task - OA-427 - OA - Unsupported spaces in Admin users
        linkParams &= "&txtKeyword=" & (txtKeyword.Text.Trim.Replace("'", "dquotes"))
        linkParams &= "&refcheck=" & chkRefChecked.Checked
        linkParams &= "&txtFName=" & (txtFName.Text.Trim.Replace("'", "dquotes"))
        linkParams &= "&txtLName=" & (txtLName.Text.Trim.Replace("'", "dquotes"))
        linkParams &= "&txtCompanyName=" & (txtCompanyName.Text.Trim.Replace(":", "515"))
        linkParams &= "&txtEmail=" & (txtEmail.Text.Trim)
        linkParams &= "&Email=" & (Email)
        linkParams &= "&Dts=" & ddlNearestTo.SelectedValue
        linkParams &= IIf(chkCRBChecked.Checked, "&CRB=Yes", "&CRB=No")
        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        linkParams &= IIf(chkExcelPartner.Checked, "&ExcelPartner=Yes", "&ExcelPartner=No")
        linkParams &= IIf(chkEPCP.Checked, "&EPCP=Yes", "&EPCP=No")
        linkParams &= IIf(chkUKSecurity.Checked, "&UKSec=Yes", "&UKSec=No")
        linkParams &= IIf(chkCSCS.Checked, "&CSCS=Yes", "&CSCS=No")
        linkParams &= IIf(chkNearestTo.Checked, "&Nto=Yes", "&Nto=No")
        linkParams &= IIf(chkTrackSP.Checked, "&TrackSp=1", "&TrackSp=0")
        'linkParams &= IIf(chkEngCRBChecked.Checked, "&EngineerCRBCheck=1", "&EngineerCRBCheck=0")
        linkParams &= IIf(CRBCheckedEnhanced.Checked, IIf(CRBCheckedYes.Checked, "&EngineerCRBCheck=1", "&EngineerCRBCheck=137"), "&EngineerCRBCheck=0")
        linkParams &= IIf(chkEngCSCS.Checked, "&EngineerCSCSCheck=1", "&EngineerCSCSCheck=0")
        linkParams &= IIf(chkEngUKSecurity.Checked, "&EngineerUKSecurityCheck=1", "&EngineerUKSecurityCheck=0")
        linkParams &= IIf(chkEngRightToWork.Checked, "&EngineerRightToWorkInUK=1", "&EngineerRightToWorkInUK=0")
        linkParams &= IIf(chkEngProofOfMeeting.Checked, "&EngProofOfMeeting=1", "&EngProofOfMeeting=0")
        ' TODO
        'linkParams &= "&mode=" & ""
        linkParams &= "&sender=" & "AdminSearchAccounts"
        linkParams &= "&PS=" & gvContacts.PageSize
        linkParams &= "&PN=" & gvContacts.PageIndex
        linkParams &= "&SC=" & gvContacts.SortExpression
        linkParams &= "&SO=" & gvContacts.SortDirection
        'Keep contact name at last
        linkParams &= "&companyname=" & Trim(CompanyName)
        linkParams &= "&contactname=" & (contactname.Replace("'", "&rsquo;"))
        link &= "<a class=footerTxtSelected href='" & companyPageName & "?" & linkParams & "'><img src='Images/Icons/ViewDetails.gif' alt='View Details' width='16' height='16' hspace='2' vspace='0' border='0'></a>"


        If classId = ApplicationSettings.RoleSupplierID Then
            link &= "<a href='Invoices.aspx?" & linkParams & "'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Sales Invoices' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
            link &= "<a href='SupplierInvoices.aspx?" & linkParams & "'><img src='Images/Icons/View-Purchase-Invoice.gif' alt='View Purchase Invoices' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        ElseIf classId = ApplicationSettings.RoleClientID Then
            link &= "<a href='Invoices.aspx?" & linkParams & "'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Sales Invoices' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        End If



        If statusId <> ApplicationSettings.StatusApprovedCompany And statusId <> ApplicationSettings.StatusDeletedCompany Then
            link &= "<a href='ReferenceCheckForm.aspx?" & linkParams & "'><img src='Images/Icons/Icon-RefCheck.gif' alt='Reference Check' width='14' height='13' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        End If
        'Create work request link

        link &= "<a href='woform.aspx?" & linkParams & "'><img src='images/icons/Create-New-WO.gif' alt='Create New Work Request' width='14' height='14' hspace='2' vspace='0' border='0'></a>"

        If statusId <> ApplicationSettings.StatusSuspendedCompany Then
            If BusinessArea <> 0 Then
                link &= "<a href='SingleCompanyReports.aspx?" & linkParams & "'><img src='Images/Icons/Report.gif' alt='View Company Report' width='16' height='11' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
            End If
            'link &= "<a href='Comments.aspx?sender=SearchAccounts&bizdivid=" & bizDivId & "&companyid=" & companyId & "'><img src='Images/Icons/ViewHistory.gif' alt='Add Comments' width='16' height='11' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        End If

        'This session varibale is used at comments.aspx page
        Session("ContactClassID") = classId
        Session("ContactCompanyID") = companyId

        Return link
    End Function
    Public Function getSelectedRegions(ByVal columnName As String) As String
        Dim strRegions = ""

        Dim dt As DataTable = UCRegions.saveListBox()
        For Each dr As DataRow In dt.Rows
            strRegions &= dr(columnName) & ","
        Next
        Return strRegions
    End Function


    Public Function GetAccredinCSV(ByVal columnName As String) As String
        Dim strAccred = ""

        Dim ds As DataSet = UCAccreditations.GetSelectedAccred()
        Dim dsContacts As New OWContacts()
        For Each drowAccreds As DataRow In ds.Tables(0).Rows
            If (columnName = "Text") Then
                If strAccred = "" Then
                    strAccred = drowAccreds("TagName").ToString
                Else
                    strAccred = strAccred & "," & drowAccreds("TagName").ToString
                End If
            Else
                If strAccred = "" Then
                    strAccred = drowAccreds("TagId").ToString
                Else
                    strAccred = strAccred & "," & drowAccreds("TagId").ToString
                End If
            End If
        Next
        Return strAccred
    End Function



    Public Function prepareExportToExcelLink() As String
        If Not chkNearestTo.Checked Then
            txtPostCode.Text = ""
        End If

        Dim linkParams As String = ""
        linkParams &= "bizDivId=" & ViewState("BizDivId")

        Dim statusId As String = ""
        If chkRefChecked.Checked Then
            statusId = ApplicationSettings.StatusApprovedCompany
        Else
            statusId = ""
        End If
        linkParams &= "&statusId=" & statusId
        If (CType(UCAOE.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "" Then
            If (CType(UCAOE.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "0" Then
                linkParams &= "&skillSet=" & CType(UCAOE.FindControl("hdnCombIDs"), HtmlInputHidden).Value
            End If
        End If
        linkParams &= "&region=" & getSelectedRegions("Value")
        linkParams &= "&postCode=" & txtPostCode.Text.Trim
        linkParams &= "&keyword=" & txtKeyword.Text.Trim
        linkParams &= "&fName=" & txtFName.Text.Trim
        linkParams &= "&lName=" & txtLName.Text.Trim
        linkParams &= "&companyName=" & txtCompanyName.Text.Trim
        'Poonam modified on 31-03-2017 - Task - OA-427 - OA - Unsupported spaces in Admin users
        linkParams &= "&eMail=" & (txtEmail.Text.Trim)
        linkParams &= IIf(chkCRBChecked.Checked, "&CRB=Yes", "&CRB=No")
        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        linkParams &= IIf(chkExcelPartner.Checked, "&ExcelPartner=Yes", "&ExcelPartner=No")
        linkParams &= IIf(chkEPCP.Checked, "&EPCP=Yes", "&EPCP=No")
        linkParams &= IIf(chkUKSecurity.Checked, "&UKSec=Yes", "&UKSec=No")
        linkParams &= IIf(chkCSCS.Checked, "&CSCS=Yes", "&CSCS=No")
        linkParams &= IIf(chkTrackSP.Checked, "&TrackSp=1", "&TrackSp=0")
        linkParams &= "&sortExpression=" & gvContacts.SortExpression
        linkParams &= "&page=" & "AdminSearchAccounts"
        linkParams &= "&vendorIDs=" & GetAccredinCSV("Value")
        linkParams &= "&nearestto=" & ddlNearestTo.SelectedValue
        'linkParams &= IIf(chkEngCRBChecked.Checked, "&EngineerCRBCheck=1", "&EngineerCRBCheck=0")
        linkParams &= IIf(CRBCheckedEnhanced.Checked, IIf(CRBCheckedYes.Checked, "&EngineerCRBCheck=1", "&EngineerCRBCheck=137"), "&EngineerCRBCheck=0")
        linkParams &= IIf(chkEngCSCS.Checked, "&EngineerCSCSCheck=1", "&EngineerCSCSCheck=0")
        linkParams &= IIf(chkEngUKSecurity.Checked, "&EngineerUKSecurityCheck=1", "&EngineerUKSecurityCheck=0")
        linkParams &= IIf(chkEngRightToWork.Checked, "&EngineerRightToWorkInUK=1", "&EngineerRightToWorkInUK=0")
        linkParams &= IIf(chkEngProofOfMeeting.Checked, "&EngProofOfMeeting=1", "&EngProofOfMeeting=0")
        linkParams &= "&CategoryType=" & drpCategoryType.SelectedItem.Value


        Return "ExportToExcel.aspx?" & linkParams
    End Function

    Public Sub processBackToListing()

        ViewState("BizDivId") = Request("bizDivId")
        UCAOE.BizDivId = Request("bizDivId")
        HttpContext.Current.Items("companyId") = Request("companyId")
        HttpContext.Current.Items("contactId") = Request("contactId")
        HttpContext.Current.Items("classId") = Request("classId")
        HttpContext.Current.Items("statusId") = Request("statusId")
        HttpContext.Current.Items("roleGroupId") = Request("roleGroupId")
        hdnProductIds.Value = Request("product")
        hdnRegionValue.Value = Request("region")
        txtPostCode.Text = Request("txtPostCode")
        txtKeyword.Text = Server.UrlDecode(Request("txtKeyword")).Replace("dquotes", "'")
        chkRefChecked.Checked = Request("refcheck")
        txtFName.Text = Server.UrlDecode(Request("txtFName")).Replace("dquotes", "'")
        txtLName.Text = Server.UrlDecode(Request("txtLName")).Replace("dquotes", "'")
        txtCompanyName.Text = (Request("txtCompanyName").Replace("dquotes", "'").Replace("515", ":"))
        txtEmail.Text = Server.UrlDecode(Request("txtEmail"))
        chkCRBChecked.Checked = IIf(Request("CRB") & "" = "Yes", True, False)
        chkUKSecurity.Checked = IIf(Request("UKSec") & "" = "Yes", True, False)
        chkCSCS.Checked = IIf(Request("CSCS") & "" = "Yes", True, False)
        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        chkExcelPartner.Checked = IIf(Request("ExcelPartner") & "" = "Yes", True, False)
        chkEPCP.Checked = IIf(Request("EPCP") & "" = "Yes", True, False)
        chkTrackSP.Checked = IIf(Request("TrackSp") & "" = "1", True, False)
        'chkEngCRBChecked.Checked = IIf(Request("EngineerCRBCheck") & "" = "1", True, False)
        If (Request("EngineerCRBCheck") & "" = "137") Then
            CRBCheckedEnhanced.Checked = True
        ElseIf (Request("EngineerCRBCheck") & "" = "1") Then
            CRBCheckedYes.Checked = True
        Else
            CRBCheckedNo.Checked = True
        End If
        chkEngCSCS.Checked = IIf(Request("EngineerCSCSCheck") & "" = "1", True, False)
        chkEngUKSecurity.Checked = IIf(Request("EngineerUKSecurityCheck") & "" = "1", True, False)
        chkEngRightToWork.Checked = IIf(Request("EngineerRightToWorkInUK") & "" = "1", True, False)
        chkEngProofOfMeeting.Checked = IIf(Request("EngProofOfMeeting") & "" = "1", True, False)

        ' ddlSkillsArea.SelectedValue = Request("skillsarea")
        If Request("Nto") & "" = "Yes" Then
            chkNearestTo.Checked = True
            divNearestTo.Visible = True
            ddlNearestTo.SelectedValue = Request("Dts")
        Else
            chkNearestTo.Checked = False
            divNearestTo.Visible = False
        End If

        gvContacts.PageSize = Request("PS")


        If chkRefChecked.Checked Then
            ViewState("statusId") = ApplicationSettings.StatusApprovedCompany
        Else
            ViewState("statusId") = ""
        End If

        'Sort Expe
        ViewState!SortExpression = ""
        If Trim(Request("SC")) <> "" Then
            ViewState!SortExpression = Trim(Request("SC"))
        Else
            ViewState!SortExpression = "DateCreated"
        End If

        'Sort direction
        Dim sd As SortDirection
        sd = SortDirection.Descending
        If Trim(Request("SO")) <> "" Then
            If Trim(Request("SO")).ToLower = "0" Then
                sd = SortDirection.Ascending
            End If
        End If

        InitializeForm(False)
        lblSearchCriteria.Text = PrepareSearchCriteria()
        pnlSearchResult.Visible = True
        gvContacts.Sort(ViewState!SortExpression, sd)
        PopulateGrid()
        gvContacts.PageIndex = Request("PN")
    End Sub

   
    Public Function SetRefCheckedStatus(ByVal refChkDate As Object) As String
        If Not IsDBNull(refChkDate) Then
            If CStr(refChkDate) <> "" Then
                Return CDate(refChkDate).ToShortDateString
            End If
        End If
        Return "No"
    End Function
    Public Function SetValidatedSpecialistsStatus(ByVal validatedSpecialists As Object) As String

        If Not IsDBNull(validatedSpecialists) Then
            If Convert.ToInt32(validatedSpecialists) > 0 Then
                Return "Yes (" & Convert.ToInt32(validatedSpecialists) & ")"
            End If
        End If
        Return "No"
    End Function
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub

    Public Function getAOEBizDivIdFromRadioListBtn() As Integer
        Select Case rdAOEList.SelectedValue
            Case 1
                Return ApplicationSettings.OWUKBizDivId
            Case 2
                Return ApplicationSettings.SFUKBizDivId
        End Select
    End Function
    Public Sub rdAOEList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdAOEList.SelectedIndexChanged
        UCAOE.BizDivId = getAOEBizDivIdFromRadioListBtn()
        UCAOE.PopulateAOE()
    End Sub



    Public Sub RemoveCallback(ByVal tag As String, ByVal obj As Object, ByVal reason As CacheItemRemovedReason)
        If (CacheItemRemovedReason.Underused = reason) Then
            Cache.Remove(tag)
            If Not IsNothing(obj) Then
                Cache.Insert(tag, obj, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            End If
        Else
            Cache.Remove(tag)
        End If
    End Sub

    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub chkNearestTo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNearestTo.CheckedChanged
        If chkNearestTo.Checked = True Then
            divNearestTo.Visible = True
        ElseIf chkNearestTo.Checked = False Then
            divNearestTo.Visible = False
        End If
    End Sub

End Class