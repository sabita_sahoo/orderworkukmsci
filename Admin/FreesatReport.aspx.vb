
Partial Public Class FreesatReport
    Inherits System.Web.UI.Page
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents lnkBackToListing As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents tdBackToListing As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lnkExportReportExcel As Global.System.Web.UI.WebControls.LinkButton
    Protected WithEvents UCDateRange1 As UCDateRange



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            UCDateRange1.txtToDate.Text = Date.Today.Date
            UCDateRange1.txtFromDate.Text = CType((DateTime.Now.Year & "/" & DateTime.Now.Month & "/" & 1), DateTime)
        End If
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
            Case ApplicationSettings.CountryUK
                ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
        End Select

        If Not IsNothing(Request("companyId")) Then
            If Request("companyId") <> "" Then
                If UCDateRange1.DateValidate() Then
                    generateReport()
                End If

            End If
        End If

    End Sub

    Private Sub lnkBtnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnGenerateReport.Click
        If UCDateRange1.DateValidate() Then
            generateReport()
        End If
    End Sub

    Private Sub generateReport()
        Dim languageCode As String = ""

        ReportViewerOW.ShowParameterPrompts = False
        ReportViewerOW.ShowToolBar = True
        Dim reportSErverPath As Uri = New Uri(ApplicationSettings.ReportServerPath)
        ReportViewerOW.ServerReport.ReportServerUrl = reportSErverPath
        ReportViewerOW.ServerReport.ReportServerCredentials = New CustomReportCredentials.CustomReportCredentials(ApplicationSettings.ReportServerUserName, ApplicationSettings.ReportServerPassword, ApplicationSettings.ReportServerDomain)
        Dim objParameter(4) As Microsoft.Reporting.WebForms.ReportParameter
        Dim StrCompanyID As String = ""

        'Give path to report            
        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/SingleCompanyReport"

        objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)

        objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "True")

        'Language parameters
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                languageCode = "de-DE"
            Case ApplicationSettings.CountryUK
                languageCode = "en-GB"
        End Select
        objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)
        If chkToDate.Checked = False Then
            ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/FreesatReport"
            objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("FromDate", UCDateRange1.txtFromDate.Text.ToString.Trim)
            objParameter(4) = New Microsoft.Reporting.WebForms.ReportParameter("ToDate", UCDateRange1.txtToDate.Text.ToString.Trim)
            ViewState("fromDate") = UCDateRange1.txtFromDate.Text.ToString.Trim
            ViewState("toDate") = UCDateRange1.txtToDate.Text.ToString.Trim

        Else
            ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/FreesatReportToDate"
            objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("FromDate", "")
            objParameter(4) = New Microsoft.Reporting.WebForms.ReportParameter("ToDate", "")
            ViewState("fromDate") = ""
            ViewState("toDate") = ""

        End If

        ReportViewerOW.ServerReport.SetParameters(objParameter)
        ReportViewerOW.ServerReport.Refresh()
    End Sub

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
End Class