<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ServicePartnerApprovalLeagueReport.aspx.vb" Inherits="Admin.ServicePartnerApprovalLeagueReport" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Supplier Approval Statistics"%>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>  

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
  




	        
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
						<td>&nbsp;</td>
							<td class="HeadingRed" ><strong>Supplier Report</strong></td>
						</tr>
					  </table>

					  <table width="500px" cellpadding="0" cellspacing="0" border="0">
					  	<tr >
					  	    <td width="15">&nbsp;</td>
							<td height="24" align="left">&nbsp;</td>
											  <td align="left"><asp:DropDownList CssClass="formFieldGrey121" ID="ddMonth" runat="server">
										<asp:ListItem Text="January" value="1"></asp:ListItem>
												  <asp:ListItem Text="February" value="2"></asp:ListItem>
												  <asp:ListItem Text= "March" value="3"></asp:ListItem>
												  <asp:ListItem Text= "April" value="4"></asp:ListItem>
												  <asp:ListItem Text= "May" value="5"></asp:ListItem>
												  <asp:ListItem Text= "June" value="6"></asp:ListItem>
												  <asp:ListItem Text= "July" value="7"></asp:ListItem>
												  <asp:ListItem Text= "August" value="8"></asp:ListItem>
												  <asp:ListItem Text= "September" value="9"></asp:ListItem>
												  <asp:ListItem Text= "October" value="10"></asp:ListItem>
												  <asp:ListItem Text= "November" value="11"></asp:ListItem>
												  <asp:ListItem Text= "December" value="12"></asp:ListItem>
										</asp:DropDownList>
											  </td>
											  <td height="24" align="left" width="10">&nbsp;</td>
											  <td align="left"><asp:DropDownList CssClass="formFieldGrey121" ID="ddYear" runat="server">
											  <asp:ListItem Text="2006" value="2006"></asp:ListItem>
											  <asp:ListItem Text="2007" value="2007"></asp:ListItem>
											  <asp:ListItem Text="2008" value="2008"></asp:ListItem>
											  <asp:ListItem Text="2009" value="2009"></asp:ListItem>
											  <asp:ListItem Text="2010" value="2010"></asp:ListItem>
											  <asp:ListItem Text="2011" value="2011"></asp:ListItem>
                                              <asp:ListItem Text="2012" value="2012"></asp:ListItem>
                                              <asp:ListItem Text="2013" value="2013"></asp:ListItem>
                                              <asp:ListItem Text="2014" value="2014"></asp:ListItem>
                                              <asp:ListItem Text="2015" value="2015"></asp:ListItem>
											  </asp:DropDownList>
											  </td>
							<td width="20"></td>
							<td align="left">
								<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                    <asp:LinkButton ID="lnkBtnGenerateReport" causesValidation=True runat="server" CssClass="txtButtonRed" Text="Generate Report"></asp:LinkButton>
                                </div>
							 </td>		 

						</tr>
					</table>
					<table>
					  <tr>
						<td align=center >
							 <rsweb:ReportViewer ID="ReportViewerOW" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="600px" ProcessingMode="Remote" Width="100%" bgColor="#FFFFFF">
									<ServerReport ReportServerUrl="" />
							 </rsweb:ReportViewer>
						</td>
					  </tr>						  
					  </table>
			<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>

</asp:Content>
