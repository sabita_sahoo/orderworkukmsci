Public Partial Class SpecialistsForm
    Inherits System.Web.UI.Page
    Protected WithEvents lnkViewFavSuppliers As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents lnkAccSettings As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents UCSpecialistsForm1 As UCMSSpecialistsForm
    Protected WithEvents UCAccountSumm1 As UCAccountSummary
    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsNothing(Request("sender")) Then
            If (Request("sender") = "UnapprovedEngineers") Then
                If Not IsNothing(Request("ClassId")) Then
                    Dim CompID As String = Request("companyid")
                    Session("ContactClassID") = CInt(Request("ClassId"))
                    Session(Request("companyid") & "_ContactClassID") = CInt(Request("ClassId"))
                End If
            End If
        End If
        UCSpecialistsForm1.CompanyID = Request("CompanyID")
        UCSpecialistsForm1.BizDivID = Request("bizDivId")
        UCSpecialistsForm1.ClassID = (Session(Request("companyid") & "_ContactClassID"))
        UCSpecialistsForm1.RoleGroupID = Session("ContactRoleGroupID")
        UCSpecialistsForm1.UserID = (Session(Request("companyid") & "_ContactUserID"))
        UCAccountSumm1.BizDivId = Request("bizDivId")
        
        If Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
            UCAccountSumm1.ShowRating = True
            lnkViewBlackListClient.Visible = True
            lnkViewWorkingDays.Visible = True
        ElseIf Session("ContactClassID") = ApplicationSettings.RoleClientID Then
            UCAccountSumm1.ShowRating = False
        End If

        Dim contactType As Integer
        If Not IsNothing(Session(Request("companyid") & "_ContactClassID")) Then
            contactType = Session(Request("companyid") & "_ContactClassID")
        End If

        If Not IsNothing(Session(Request("companyid") & "_ContactClassID")) Then
            If contactType = ApplicationSettings.ContactType.newSupplier Or contactType = ApplicationSettings.ContactType.approvedSupplier Or contactType = ApplicationSettings.ContactType.suspendedSupplier Then
                'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                lnkCompProfile.HRef = "CompanyProfile.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                rgnReferences.Visible = True

                'Prepare AutoMatch Link URL
                lnkAutoMatch.HRef = "AutoMatchSettings.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&sender=supplier"
                lnkAccSettings.HRef = "AccountSettings.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&sender=supplier"
                lnkViewAllInvoices.HRef = "ViewAllInvoices.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&sender=supplier"
                lnkViewAccountHistory.HRef = "AccountHistory.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&sender=supplier" & "&ContactId=" & Request("ContactId")
            ElseIf contactType = ApplicationSettings.ContactType.approvedBuyer Or contactType = ApplicationSettings.ContactType.suspendedBuyer Then
                'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                lnkCompProfile.HRef = "CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                rgnReferences.Visible = False

                'Prepare AutoMatch Link URL
                lnkAutoMatch.HRef = "AutoMatchSettings.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&sender=buyer" & "&classId=" & Request("classId")
                lnkAccSettings.HRef = "AccountSettings.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&sender=buyer" & "&classId=" & Request("classId")
                lnkViewAllInvoices.HRef = "ViewAllInvoices.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&sender=buyer" & "&classId=" & Request("classId")
                lnkViewAccountHistory.HRef = "AccountHistory.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&sender=buyer" & "&classId=" & Request("classId") & "&ContactId=" & Request("ContactId")
            End If
            lnkViewFavSuppliers.HRef = "FavouriteSupplierList.aspx?mode=favourite&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            lnkAddProduct.HRef = "AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            lnkAddProductNew.HRef = "ServiceListing.aspx?CompanyID=" & Request("CompanyID")
            'lnkViewBlackListsupplier.HRef = "FavouriteSupplierList.aspx?mode=blacklist&CompanyID=" & Session("ContactCompanyID")
            lnkReferences.HRef = "References.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            lnkuser.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            lnkloc.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            'lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            If Not IsNothing(Request("sender")) Then
                If (Request("sender") = "UnapprovedEngineers") Then
                    If Not IsNothing(Request("ClassId")) Then
                        lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&ContactId=" & Request("ContactId") & "&sender=" & Request("sender") & "&ClassId=" & Request("ClassId")
                    End If
                End If
            End If
            lnkAddLocationsSupp.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&ClassId=" & Request("ClassId")
            lnkViewBlackListClient.HRef = "FavouriteSupplierList.aspx?mode=ClientBlacklist&CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&ClassId=" & Request("ClassId")
            lnkViewWorkingDays.HRef = "WorkingDays.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&ClassId=" & Request("ClassId")

        End If
        If Not IsPostBack Then
            Dim ds As New DataSet
            ds = ws.WSContact.MS_GetContactRatings(Request("ContactId"))
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    litRating.Visible = True
                    lblPosRating.Text = Convert.ToString(ds.Tables(0).Rows(0)("PositiveRating"))
                    lblNegRating.Text = Convert.ToString(ds.Tables(0).Rows(0)("NegativeRating"))
                    lblNeuRating.Text = Convert.ToString(ds.Tables(0).Rows(0)("NeutralRating"))
                    litRating.Text = "<a href=javascript:showHideRatings('ctl00_ContentPlaceHolder1_divDispAllRatings','Contact') style='text-decoration: none;' class='TxtListing'>" + "<div id=divMainRatingContact class='backGroundRating formTxtRedBold2' >&nbsp;&nbsp;&nbsp;" & ResourceMessageText.GetString("RatingScore") + CType(ds.Tables(0).Rows(0)("PercentageRating"), String) + "</div>" + "</a>"
                Else
                    litRating.Visible = True
                    litRating.Text = "No ratings available for this contact"
                End If
            Else
                litRating.Visible = True
                litRating.Text = "No ratings available for this contact"
            End If
        End If
        'Code commeneted as Suppliers can also behave as buyers
        ''Show Favourite supplier link
        'lnkViewFavSuppliers.Visible = False
        'If Session("ContactClassID") = ApplicationSettings.RoleClientID Then
        '    lnkViewFavSuppliers.Visible = True
        'End If


    End Sub

End Class