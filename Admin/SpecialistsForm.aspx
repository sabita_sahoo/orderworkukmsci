<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Specialist Form" CodeBehind="SpecialistsForm.aspx.vb" Inherits="Admin.SpecialistsForm" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    
<script src="JS/jquery.js" language="javascript" type="text/javascript" ></script>
<script type="text/javascript" language="javascript">
    $(window).keydown(function (event) {
        if ((event.which == 13) && ($(event.target)[0] != $("textarea")[0])) {
            event.preventDefault();
            return false;
        }
    });
</script>

</asp:Content>

 <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>

   
			
	        <div id="divContent">
         
		
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr height="9">
           <td></td>
           <td></td>
         </tr>
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			
			<%@ Register TagPrefix="uc1" TagName="UCSpecialistsAdminForm" Src="~/UserControls/UK/UCMSSpecialistsFormUK.ascx" %>
         <uc1:UCSpecialistsAdminForm id="UCSpecialistsForm1" runat="server"></uc1:UCSpecialistsAdminForm> 
			
            <!-- InstanceEndEditable --></td>
			   
		  
            <td width="220" align="center" valign="top">
			 
				 <%@ Register TagPrefix="uc1" TagName="UCAccountSumm" Src="~/UserControls/UK/UCAccountSummaryUK.ascx" %>
             <uc1:UCAccountSumm id="UCAccountSumm1" runat="server"></uc1:UCAccountSumm> 
			 
              <div id="divAccountSummary" style="padding-left:15px;">
	            <div class="divAccountTopCurves"><img src="Images/Curves/MAS-Bdr-TL.gif" alt="" width="5" height="5" class="corner" style="display: none" />
	            </div>
	            <p class="TxtListing" ><strong style="padding-left:30px;">Personal Rating</strong></p>
	            <asp:Literal id="litRating" runat="server"></asp:Literal>
                    		<DIV id="divDispAllRatings" style="visibility:hidden; overflow:hidden; position:absolute; height:0px;"  runat="server">
								<table class="whiteBackground" style="margin:0px;" border="0" cellspacing="0" cellpadding="0">
									  <tr>
										<td width="20" align="center"><span class="liNavigationBlack"><img src="Images/Icons/Icon-Positive.gif" width="18" height="18"  align="absmiddle" /></span></td>
										<td align="left"><span class="liNavigationBlack">Positive
											<asp:Label ID="lblPosRating" Text="" runat="server"></asp:Label>
										</span></td>
									  </tr>
									  <tr>
										<td align="center"><span sclass="liNavigationBlack"><img src="Images/Icons/Icon-Neutral.gif" width="13" height="5" align="absmiddle" /></span></td>
										<td align="left"><span class="liNavigationBlack">Neutral   
											<asp:Label ID="lblNeuRating" Text="" runat="server"></asp:Label>
										</span></td>
									  </tr>
									  <tr>
										<td align="center"><span class="liNavigationBlack"><img src="Images/Icons/Icon-Negative.gif" width="17" height="18"  align="absmiddle" /></span></td>
										<td align="left"><span class="liNavigationBlack">Negative 
										  <asp:Label ID="lblNegRating" Text="" runat="server"></asp:Label>
										</span></td>
									  </tr>
								</table>
								<div class="bottomCurveWhiteRight width152"><img src="Images/Curves/Bdr-White-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
							</DIV>
                <div class="divAccountBotCurves"><img src="Images/Curves/MAS-Bdr-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" />
	            </div>	
            </div>
			
				
			<a href="LocationForm.aspx" tabindex="119" id="lnkAddLocationsSupp" runat="server" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Locations1','','Images/buttons/AddLocation-Rolover.gif',1)"><img src="Images/buttons/AddLocation.gif" title="Add Locations" name="Locations1" width="197" height="43" border="0" class="marginB20" id="Locations1" ></a>   <br>     
              <a id="lnkCompProfile" runat="server" class="footerTxtSelected ">Company Profile</a><br>
             <a id="lnkloc"  runat="server" class="footerTxtSelected ">Locations Listing</a><br>
             <a id="lnkuser"  runat="server"  class="footerTxtSelected ">Users Listing</a> <br> 
			<span runat="server" id="rgnReferences"><a href="References.aspx" id="lnkReferences" runat="server" class="footerTxtSelected ">References</a><br> </span>
			  
			 <%--<a href="Comments.aspx" class="footerTxtSelected ">Comments</a><br>  --%>
	<a class="footerTxtSelected " id="lnkViewFavSuppliers" runat="server">View All Favourite Suppliers</a><br>
			<a  class="footerTxtSelected " id="lnkAddProduct" runat="server">Service Listing</a><br>
            <a  class="footerTxtSelected " id="lnkAddProductNew" runat="server" target="_blank" >Service Listing New</a><br>
			 <a class="footerTxtSelected " id="lnkAutoMatch" runat="server" title="AutoMatch Settings">AutoMatch Settings</a><br>
			 <a class="footerTxtSelected " id="lnkAccSettings" runat="server" title="Account Settings">Account Settings</a><br> 
               <%--Poonam - OA-494 - OA - Links missing in Specialist form - Just changed the visibility to True from False--%>
               <a  class="footerTxtSelected " id="lnkViewBlackListClient" runat="server" visible="True">View Client Blacklist</a><br>
                <a  class="footerTxtSelected " id="lnkViewWorkingDays" runat="server" visible="True" >View Working Days</a><br>
                <a  class="footerTxtSelected " id="lnkViewAllInvoices" runat="server">View All Invoices</a><br>
                <a  class="footerTxtSelected " id="lnkViewAccountHistory" runat="server">User History</a><br>
             </td>
            
	   
         </tr>
         </table>
      </div>
      
                </asp:Content>  