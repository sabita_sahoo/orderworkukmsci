﻿<%@ Page Title="OrderWork : Invoice Approval Listing" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="InvoiceApprovalListing.aspx.vb" Inherits="Admin.InvoiceApprovalListing" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


              <div id="divContent">

         

            

        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>

        

             <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">

         

         <tr>

            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->

            <input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />                          

            <input id="hdnPageNo"  value="1" type="hidden" name="hdnPageNo" runat="server">

                  <input id="hdnPageSize" value="25" type="hidden" name="hdnPageSize" runat="server">

                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate> 

                                <table width="100%" cellpadding="0" cellspacing="0">

                                    <tr>

                                    <td width="10px"></td>

                                    <td><asp:Label ID="lblMessage" CssClass="HeadingRed"  runat="server" Text="Invoice Approval Listing"></asp:Label></td>

                                    </tr>

                                    </table>
            
            <table >

            <tr>

            <td class="paddingL30 padB21" valign="top" padB21 paddingTop1>

            <asp:Label ID="lblMsg" runat="server" CssClass="bodytxtValidationMsg"></asp:Label>                

            </td>

            </tr>

            </table>                      

                        

                 

                                          

                  

                        

                               

                               

                  <div id="divredBorder" class="divredBorder">

                        <div class="roundtabRed"><img src="Images/Curves/TabRed-LC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>

                  </div>

                  

                    <asp:Panel ID="pnlListing" runat="server">    

                                <asp:GridView ID="gvSInvoiceToEdit"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'

                         BorderColor="White"  dataKeyNames="AdviceNumber" PagerSettings-Mode=NextPreviousFirstLast   PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          

               <EmptyDataTemplate>

               <table  width="100%" border="0" cellpadding="10" cellspacing="0">                            

                                    <tr>

                                          <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.

                                          </td>

                                    </tr>                   

                              </table>

                  

               </EmptyDataTemplate>

               <PagerTemplate>

                   

                  

                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">

                                    <tr>                                                                                 
                                        <td>

                                                <table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">

                                                  <tr>

                                                      <td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >

                                                            <TR>

                                                                  <td align="right" valign="middle">

                                                                    <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>

                                                                  </td>

                                                                  <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">

                                                                  <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                                                        <tr>

                                                                        <td align="right" width="36">

                                                                        <div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />

                                                                              </div>      

                                                                              <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />

                                                                              </div>                                                                                                                                             

                                                                        </td>

                                                                        <td width="50" align="center" style="margin-right:3px; ">                                                                              

                                                                              <asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">

                                                    </asp:DropDownList>

                                                                        </td>

                                                                        <td width="30" valign="bottom">

                                                                              <div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />

                                                                              </div>

                                                                              <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />

                                                                              </div>

                                                                        </td>

                                                                        </tr>

                                                                  </table>

                                                                  </td>

                                                            </TR>

                                                      </TABLE></td>

                                                  </tr>

                                                </table>

                                      </td>
                                    </tr>

                              </table> 

                   

        </PagerTemplate> 

               <Columns>                 
                            
               <asp:TemplateField SortExpression="AdviceNumber" ItemStyle-Width="60%" HeaderText="AdviceNumber" HeaderStyle-CssClass="gridHdr" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="20%"/>
                <ItemTemplate>             
                    <%# (Container.DataItem("AdviceNumber"))%>                                  
				</ItemTemplate>
                </asp:TemplateField> 

               <asp:TemplateField SortExpression="InvoiceType" ItemStyle-Width="10%" HeaderText="InvoiceType" HeaderStyle-CssClass="gridHdr" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="10%"/>
                <ItemTemplate>             
                    <%# (Container.DataItem("InvoiceType"))%>                    
				</ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField SortExpression="Comments" HeaderText="Comments" HeaderStyle-CssClass="gridHdr" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true />
                <ItemTemplate>             
                    <%# (Container.DataItem("Comments"))%>                    
				</ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField SortExpression="BuyerName" ItemStyle-Width="20%" HeaderText="BuyerName" HeaderStyle-CssClass="gridHdr" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="20%"/>
                <ItemTemplate>             
                    <%# (Container.DataItem("BuyerName"))%>                    
				</ItemTemplate>
                </asp:TemplateField>                  
                   <asp:TemplateField ItemStyle-Width="6%">               
                <HeaderStyle  CssClass="gridHdr" />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow"/>
                    <ItemTemplate>  
                     <a href='<%#"InvoiceApproval.aspx?InvoiceNo=" & Container.DataItem("AdviceNumber") & "&Sender=InvoiceApprovalList" %>'><img src="Images/Icons/Edit.gif" title="Edit" width="12" height="11" hspace="2" vspace="0" border="0"></a>   
                    <%--<asp:LinkButton ID="lnkBtnEdit" CausesValidation="false"   CommandName="EditInvoice" CommandArgument='<%#Container.DataItem("AdviceNumber")%>' runat="server">
                           <img src="Images/Icons/Edit.gif" title='Edit' width="12" height="11" hspace="2" vspace="0" border="0">
                    </asp:LinkButton>     --%>                      
                    </ItemTemplate>
                </asp:TemplateField>  


                </Columns>

                 <AlternatingRowStyle CssClass=gridRow />

                 <RowStyle CssClass=gridRow />            

                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />

                <HeaderStyle  CssClass="gridHdr" HorizontalAlign=Left VerticalAlign=Top  Height="40px" />

                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />

        

              </asp:GridView>

              </asp:Panel>
              

                         </ContentTemplate>
            </asp:UpdatePanel>

            

                <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">

                <ProgressTemplate >

                        <div class="gridText">

                               <img  align=middle src="Images/indicator.gif" />

                               <b>Fetching Data... Please Wait</b>

                        </div>   

                        </ProgressTemplate>

                </asp:UpdateProgress>

                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />

             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"

                SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.InvoiceApprovalListing" SortParameterName="sortExpression">                

            </asp:ObjectDataSource>

                                            

                              

            <!-- InstanceEndEditable --></td>

                     

             

         </tr>

         </table>

      </div>
</asp:Content>
