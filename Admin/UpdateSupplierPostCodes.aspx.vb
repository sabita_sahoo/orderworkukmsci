

Partial Public Class UpdateSupplierPostCodes
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            SelectFromDB()
        End If
    End Sub

    Public Function SetValidateStatus(ByVal PostCode As String) As String
        If Regex.IsMatch(PostCode, "^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$") Then
            If IsNothing(ViewState("count")) Then
                ViewState("count") = 1
                ViewState("PostCode") = PostCode
            Else
                ViewState("count") = ViewState("count") + 1
                ViewState("PostCode") = ViewState("PostCode") & ", " & PostCode
            End If
            Return "True"
        Else
            Return "False"
        End If
    End Function

    Public Function SelectFromDB()
        Dim ds As DataSet = ws.WSWorkOrder.PopulateInValidSupplierPostCodes()
        gvSupplierPostCode.DataSource = ds.Tables(0)
        gvSupplierPostCode.DataBind()
        lblTotalCount.Text = ds.Tables(1).Rows(0)("TotalCount")
        lblValidPostCode.Text = ViewState("count")
        Return Nothing
    End Function

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If Not IsNothing(ViewState("PostCode")) Then
            pnlPostCodeData.Visible = False
            pnlConfirmation.Visible = True
            lblProcessing.Text = "Data is processing"
            Dim PostCode As String
            Dim i As Integer
            Dim arrList As ArrayList = New ArrayList
            Dim count As Integer
            count = 0
            arrList.AddRange(ViewState("PostCode").ToString.Split(","))
            For i = 0 To arrList.ToArray.Length - 1
                PostCode = arrList(i).ToString
                'OrderWork.Process.GetPostCodeAndInsertInDB(PostCode)
                Dim details As Newtonsoft.Json.Linq.JObject = CommonFunctions.GetPostcodeLatLong(PostCode)
                count = count + 1
            Next i
            lblProcessing.Text = "Data update of " & count & " postcodes are successful"

        End If
    End Sub
End Class