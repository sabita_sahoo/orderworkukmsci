<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Admin Accepted Send Mail" CodeBehind="AcceptedSendMail.aspx.vb" Inherits="Admin.AcceptedSendMail" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            <div id="divValidationMain" visible="false" class="divValidation" runat="server" >
              <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" title="" width="5" height="5" class="corner" style="display: none" /></div>
              <table width="100%"  border="0" cellspacing="0" cellpadding="0">
	            <tr>
	              <td height="15" align="center">&nbsp;</td>
	              <td height="15">&nbsp;</td>
	              <td height="15">&nbsp;</td>
	            </tr>
	            <tr valign="middle">
	              <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
	              <td class="validationText"><div  id="divValidationMsg"></div>
		              <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
		              <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
	              </td>
	              <td width="20">&nbsp;</td>
	            </tr>
	            <tr>
	              <td height="15" align="center">&nbsp;</td>
	              <td height="15">&nbsp;</td>
	              <td height="15">&nbsp;</td>
	            </tr>
              </table>
              <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" title="" width="5" height="5" class="corner" style="display: none" /></div>
            </div>
            <div>
                <input type="hidden" id="hdnValue" runat="server" name="hdnValue">
                <input type="hidden" id="hdnSpecialistID" runat="server" name="hdnSpecialistID">
               <asp:Panel ID="pnlWorkOrder" runat="server">
                <table id="DGWorkOrders" width="100%" border="0" cellpadding="5" cellspacing="0">
                  <tr valign="top" >
                    <td width="80px" class="gridHdr gridBorderRB">WO No</td>
                    <td width="70px" class="gridHdr gridBorderRB"><asp:Label runat="server"  ID="lblHdrCreated" Text="Submitted"></asp:Label></td>
                    <td width="70px" class="gridHdr gridBorderRB" runat="server" id="tdDateModified"><asp:Label runat="server" ID="lblHdrModified" Text="Last Response"></asp:Label></td>
                    <td width="65px" class="gridHdr gridBorderRB">Location</td>
	                <td width="75px" class="gridHdr gridBorderRB" runat="server" id="tdBuyerCompany">Client</td>	                
	                <td  class="gridHdr gridBorderRB">WO Title</td>
	                <td width="60px" class="gridHdr gridBorderRB">WO Start</td>
	                <td width="80px" class="gridHdr gridBorderRB"><asp:Label runat="server" ID="lblHdrWholesalePrice" Text="WholeSale Price"></asp:Label></td>
	                <td width="75px" class="gridHdr gridBorderRB"><asp:Label runat="server" ID="lblHdrPlatformPrice" Text="Portal Price"></asp:Label></td>
	                <td width="57px" class="gridHdrHighlight gridBorderRB">Status</td>
	               
                  </tr>
                 <tr>
                    <td width="80px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label></td>
	                <td width="70px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblCreated"></asp:Label></td>
	                <td width="70px" class="gridRow gridBorderRB" runat="server" id="tdDateModified1"><asp:Label runat="server" CssClass="formTxt" ID="lblModified"></asp:Label></td>
	                <td width="65px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblLocation"></asp:Label></td>
	                <td width="75px" class="gridRow gridBorderRB" runat="server" id="tdBuyerCompany1"><asp:Label runat="server" CssClass="formTxt" ID="lblBuyerCompany"></asp:Label></td>
	                <td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblTitle"></asp:Label></td>
	                <td width="60px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOStart"></asp:Label></td>
	                <td width="80px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWholesalePrice"></asp:Label></td>
	                <td width="75px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblPlatformPrice"></asp:Label></td>
	                <td width="57px" class="gridRowHighlight gridBorderRB"><asp:Label runat="server" CssClass="gridRowHighlight" ID="lblStatus"></asp:Label></td>
	               </tr>
                </table>
              </asp:Panel>
              <asp:Panel ID="pnlSupplierResponse" runat="server" Visible="True">
			     <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="28" align="left" valign="middle" class="SubMenuTxtBoldSelected"><asp:Label CssClass="gridText" ID="lblDLHeader" runat="server"></asp:Label></td>
                            <td width="20">&nbsp;</td>
                          </tr>
                        </table>
					     <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr>
                           
                            <td align="left" valign="top">
    						
    						
    											
					    <asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLSupplierResponse" Runat="server" Width="100%" Visible="tRUE" >
				            <HeaderTemplate>
					            <table width="100%"  cellspacing="0" cellpadding="0">
						             <tr id="tblDGYourResponseHdr" align="left">
						                  <td width="20"><input id='chkAll' onClick=CheckAll(this,'Check') type='checkbox' name='chkAll' /></td>
                                          <td width="50" style=" border-left-width:0px; " >ID</td>
                                          <td width="78">Action Date</td>
                                          <td width="75">Start</td>
                                          
                                          <td runat="server" id="tdDLCompany" visible="True">Company</td>
                                          <td width="80">Price</td>
							              <td width="50" id="tdRatingHdr" VISIBLE="True" runat="server" >Rating</td>
                                          
                                        </tr>
					            </table>
				            </HeaderTemplate>
				            
				            <ItemTemplate>
					            <table width="100%" cellspacing="0" cellpadding="0">
						            <tr id="tblDLRow" align="left">
						                  <td width="20"> <asp:CheckBox ID="Check" runat="server" ></asp:CheckBox>
				                                <input type=hidden runat=server id="hdnSuppliers"   value='<%#Container.DataItem("CompanyID")%>'/></td>
                                          <td width="50" style=" border-left-width:0px; " ><%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%></td>
                                          <td width="78"><%#Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateModified"), DateFormat.ShortDate)%></td>
                                          <td width="75" align="left"><asp:label id="lblStartDate" CssClass="gridRow" runat="server" Text='<%# Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateStart"),DateFormat.ShortDate) %>'></asp:label></td>
                                          <td id="tdDLCompany1" runat="server"  visible="True" style=" border-left-width:0px; " >
                                          
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" >
							            <tr>
								            <td width="160" id="td1" visible="true" runat="server" style="border:0;" >
									            <div style='visibility:hidden;position:absolute;margin-left:100px;' id=divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%> >
											            <table width="250" height="100" bgcolor="#D6D7D6" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;" >
												            <tr>
												              <td style="background-color:#D6D7D6; border:0; font-size:12;"  align='left'><strong>Admin:</strong> <%# iif(Container.DataItem("Name") = " ","--",Container.DataItem("Name"))%>  </td>
												            </tr>
												            <tr>
												              <td style="background-color:#D6D7D6;border:0;font-size:12;" align='left'><strong>Phone No:</strong> <%# iif(Container.DataItem("Phone") = " ","--",Container.DataItem("Phone"))%>  </td>
												            </tr>
												            <tr>
												              <td style="background-color:#D6D7D6;border:0;font-size:12;" align='left'><strong>Mobile No:</strong> <%# iif(Container.DataItem("Mobile") = " ","--",Container.DataItem("Mobile"))%>  </td>
												            </tr>
												            <tr>
												              <td style="background-color:#D6D7D6;border:0;font-size:12;" align='left'><strong>Email Id:</strong> <%# iif(Container.DataItem("Email") = " ","--",Container.DataItem("Email"))%>  </td>
												            </tr>
											            </table>
									            </div>
									            <span style=" border-left-width:0px; vertical-align:bottom; cursor:hand; font-family: Geneva, Arial, Helvetica, sans-serif; font-size:11px; color:#1D85BA " onMouseOut=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%>','visible')>
							                          <%# DataBinder.Eval(Container.DataItem, "Company")%>
									            </span> 

								            </td>
							            </tr>
						                </table>
                                          
                                           
                                             
                                          </td>
                                          
                                          
                                          
                                          <td width="80" style="padding-left:2px; padding-right:5px; text-align:right;"><asp:label id="lblPrice" CssClass='<%# iif(FormatNumber(Container.DataItem("Value"),2, TriState.True, TriState.True, TriState.False) <> hdnValue.Value,"gridRowRed","gridRow")%>' runat="server" Text='<%#FormatCurrency(IIF(Not isDbnull(Container.DataItem("Value")) ,Container.DataItem("Value"),"0"),2,TriState.True, Tristate.true,Tristate.false)%>'></asp:label></td>
							                                                    
                                          <td width="50" id="tdRating" visible="True" runat="server" >
							 	            <div style='background-color=#FFFFFF;visibility:hidden;position:absolute;margin-left:20px; z-index:100' id=divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%> >
								              <table width='153px' height='65px' bgcolor='#FFFFFF'  border='0' cellspacing='0' cellpadding='0'>
								                <tr>
									              <td width='20' align='center'><img src='Images/Icons/Icon-Positive.gif' width='18' height='18'  align='absmiddle' /></td>
									              <td class='bgWhite' align='left'>Positive  <%# iif(ISDBNULL(Container.DataItem("PositiveRating")),0,Container.DataItem("PositiveRating"))%>  </td>
									            </tr>
									            <tr>
									              <td width='20' align='center'><img src='Images/Icons/Icon-Neutral.gif' width='13' height='5' align='absmiddle' /></td>
									              <td class='bgWhite' align='left'>Neutral  <%# iif(ISDBNULL(Container.DataItem("NeutralRating")),0,Container.DataItem("NeutralRating"))%>  </td>
									            </tr>
									            <tr>
									              <td width='20' align='center'><img src='Images/Icons/Icon-Negative.gif' width='17' height='18'  align='absmiddle' /></td>
									              <td class='bgWhite' align='left'>Negative  <%# iif(ISDBNULL(Container.DataItem("NegativeRating")),0,Container.DataItem("NegativeRating"))%>  </td>
									            </tr>
								               </table>
								            </div>
								            <span style='cursor:hand;' onMouseOut=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>','visible')> <%# Container.DataItem("TotalRating")%> </span>	
							              </td>
                                          
                                        </tr>
					                </table>
					                <div id="divCommentsDetail" runat="server" style="VISIBILITY:hidden; WIDTH: 945px; POSITION: absolute; ">
					                <table id="tblCommentDetails" runat="server" width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="#EDEDED" >
                                             <tr>
    						                    <td width="10" height="10" style="padding:0px; margin:0px"></td>
    						                    <td height="10" style="padding:0px; margin:0px"></td>
    						                    <td width="10" height="10" style="padding:0px; margin:0px"></td>
  						                    </tr>
  						                    <tr>
    						                    <td width="10">&nbsp;</td>
    						                    <td>
                							        
                									   
									                    <table id="tblDiscussion" width="100%"  border="0" cellspacing="0" cellpadding="0" bgcolor="#EDEDED">
                									    
									                           <tr>
										                    <td width="10px">&nbsp;</td>
                										    
									                      </tr>
									                      <tr>
										                    <td width="10px">&nbsp;</td>
										                    <td>
											                <div id="divSupplierSkills" class="marginB20" runat="server">
												                <asp:Literal id="ltSupSkills" runat="server" Text='<%# Container.DataItem("Specialist")%>'></asp:Literal>
												                </div>
											                <asp:DataList DataSource='<%#GetDiscussions(Container.DataItem("WOTrackingId"))%>'  RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLDiscussion" Runat="server" Width="100%">
											                    <ItemTemplate>
                												
											                      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
												                    <tr>
												                      <td width="200" valign="baseline"><%#IIf((DataBinder.Eval(Container.DataItem, "ContactClassID") = admin.Applicationsettings.RoleOWID), "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - " & ("Admin's Remarks") & "  </span>", "<span class='discussionTextSupplier'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - " & ("Supplier's Remarks") & "  </span>")%></td>
												                      <td width="20" align="center" valign="top">:</td>
												                      <td valign="baseline" class="remarksAreaCollapse"> <%# Container.DataItem("Comments") %> </td>
													                  <td width="10px"></td>
												                    </tr>
											                      </table>     									      
                                            					
											                    </ItemTemplate>
										                      </asp:DataList>
										                    </td>
									                      </tr>
									                      <tr>
										                    <td>&nbsp;</td>
										                    <td>&nbsp;</td>
									                      </tr>
									                    </table>
                									   
                								   
								                    </td>
    						                    <td width="10">&nbsp;</td>
  					                       </tr>
  						                    <tr>
  						                      <td height="10" style="padding:0px; margin:0px"></td>
  						                      <td height="10" style="padding:0px; margin:0px"></td>
  						                      <td height="10" style="padding:0px; margin:0px"></td>
						                      </tr>
                					      
					                </table></div>
					                <table width="100%" id="tblCommentsCollapse"  border="0" cellspacing="0" cellpadding="0" bgcolor="#DFDFDF"  runat="server" visible= <%#ShowHideComments(Container.DataItem("WOTrackingID"))%>>
                              	                  <tr>
                                                    <td class="smallText" height="8" style="padding:0px; margin:0px"></td>
                                	                <td height="8" align="center" class="smallText" style="padding:0px; margin:0px" id="tdCollapse" runat="server">
                                	                    <a href="" border="0" onClick='<%# "javascript:CollapseComments(" & Container.DataItem("WOTrackingID") & ", this.id)" %>' runat="server" id="collapseDetails">
                                	                        <img src="Images/Arrows/Grey-Arrow.gif" width="50" title='Expand' height="8" border="0" name='<%# "imgExp" & Container.DataItem("WOTrackingID") %>'  id='<%# "imgExp" & Container.DataItem("WOTrackingID") %>' onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow-Roll.gif')" onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow.gif');" style="position:static;" />
                    	                                    <img src="Images/Arrows/Orange-Arrow-Col.gif" width="50" title='Collapse' height="8" border="0" name='<%# "imgCol" & Container.DataItem("WOTrackingID") %>'  id='<%# "imgCol" & Container.DataItem("WOTrackingID") %>' onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col-Roll.gif')" onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col.gif');" style="visibility:hidden; position:absolute;" />
                                	                    </a>
                                	                </td>
	                                                <td class="smallText" height="8" style="padding:0px; margin:0px"></td>
                                                  </tr>
                                                 </table>
				                </ItemTemplate>
			                </asp:DataList>
			        </td>
        			
			         </tr>
			        </table>
			      </asp:Panel>
			      <asp:Panel ID="pnlMailContentArea" runat="server">
			          <div style="margin-left:20px;">
			          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      
                            <tr>
                            <td width="20">&nbsp;</td>
                             <td width="20">&nbsp;</td>
                          </tr>

                          <tr align="left">
	                        <td class="formLabelGrey" >Select an Attchment to upload: </td>
                            <td width="20">&nbsp;</td>
                            <td class="formLabelGrey" ><asp:HiddenField Visible="false" runat="server" ID="hdnFilePath" /></td>
                          </tr>

                          <tr>
                            <td>  <asp:FileUpload ID="FileUpload1" CssClass="formFieldGrey width140" runat="server" /></td>
                             <td width="20">&nbsp;</td>
                          </tr>

                          <tr>
                            <td width="20">&nbsp;</td>
                             <td width="20">&nbsp;</td>
                          </tr>

                          <tr>
	                        <td class="formLabelGrey" >Please add mail content.<asp:RequiredFieldValidator id="rqWOComments" runat="server" ErrorMessage="Please enter Comment." ForeColor="#EDEDEB"
			                        ControlToValidate="txtWOComment">*</asp:RequiredFieldValidator></td>
	                        <td width="20">&nbsp;</td>
                          </tr>
                          <tr>
	                        <td><asp:TextBox id="txtWOComment"  runat="server" CssClass="formFieldGrey width934" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" Rows="4"></asp:TextBox></td>
	                        <td width="20">&nbsp;</td>
                          </tr>
                      </table>
                      </div>
                      <div style="margin-top:20px;">
                      <table  border="0" align="right" cellpadding="0" cellspacing="0">
	                    <tr>
		                    <td align="right" valign="top">
			                      <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0" runat="server" id="tblConfirm" width="80">
				                    <TR>
				                      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
				                      <TD class="txtBtnImage">
				                      <asp:LinkButton ID="lnkConfirm" onClientClick="document.getElementById('hdnFilePath').value = document.getElementById('FileUpload1').value"  CausesValidation="False" runat="server" CssClass="txtListing" OnClick="lnkConfirm_Click"><img src='Images/Icons/icon-confirm.gif' title='Send Mail' align='absmiddle' hspace='1' vspace='2' border='0'> Send Mail</asp:LinkButton></TD>
				                      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
				                    </TR>
		                      </TABLE>
		                    </td>
		                    <td align="right">
		                      <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
			                    <TR>
			                      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                      <TD class="txtBtnImage">
			                       <a id="lnkCancel" runat="server" CausesValidation="false" class="txtListing" href="#"><img src='Images/Icons/Cancel.gif' title='Cancel' align='absmiddle' hspace='1' vspace='2' border='0'> Cancel</a></TD>
			                      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
			                    </TR>
		                      </TABLE>
		                    </td>
                            <td width="20" align="right">&nbsp;</td>
                       </tr>
                    </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSuccessMsg" runat="server" Visible="false">
                    <div style="margin-left:20px; margin-top:20px;">
                        <div style=" margin-top:10px;">
                            <asp:Label runat="server" Text="Mail Sent Successfully" runat="server" ID="lblSuccessmsg"></asp:Label>
                        </div>
                        <div style=" margin-top:20px;">
                            <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
			                    <TR>
			                      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                      <TD class="txtBtnImage">
			                       <asp:LinkButton id="lnkbtnBack" runat="server" CssClass="txtListing" OnClick="lnkCancel_ServerClick"><img src='Images/Icons/Cancel.gif' title='Cancel' align='absmiddle' hspace='1' vspace='2' border='0'> Back</asp:LinkButton></TD>
			                      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
			                    </TR>
		                    </TABLE>
                            
                        </div>
                    </div>
                </asp:Panel>
                
			    </div>
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      
      
</asp:Content>