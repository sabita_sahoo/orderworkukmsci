<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AccountsMigration.aspx.vb" Inherits="Admin.AccountsMigration" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :"%>
<%@ Register Assembly="WebLibrary" Namespace="WebLibrary" TagPrefix="lib" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

<input type="hidden" id="hdnContactID" runat="server" />



	
		
	   <div id="divContent" onLoad=";MM_preloadImages('Images/buttons/Btn-AutoMatch-Roll.gif')">
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            
            <div style="padding-left:20px;">
            <span class="HeadingRed">Accounts Migration</span><br /><br />
            <asp:Panel runat="server" ID="pnlMigrateAccount">
            <asp:ValidationSummary id="validationSummary1" CssClass="bodytxtValidationMsg" runat="server" ShowSummary="true" Enabled="true" ValidationGroup="AccountsMigrate"></asp:ValidationSummary>                  
		  	 Company Name <lib:Input ID="txtCompanyName" runat="server" OnSelect='SelectAutoSuggestRow' SelectParameters='AttributeValue' DataType="List" Method="GetCompanyName"  CssClass="formField150 marginLeft20"/>
		  	 <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;margin-top:15px;" runat="server" id="tblBtnSearch">
                <a runat="server" id="btnSearch" onclick="javascript:ValidateAutoSuggestGen('ctl00_ContentPlaceHolder1_txtCompanyName', 'ctl00_ContentPlaceHolder1_btnSearch1')" class="txtButtonRed" style="cursor:pointer;"> &nbsp;Search&nbsp;</a>
					    <input type="submit" id="btnSearch1" runat="server" style="display:none;" /> 
			 </div>
              <asp:Panel ID="pnlAccountsDetails" runat="server" Visible="false" CssClass="marginTop10">
                 VAT Registration No
                <asp:TextBox runat="server" CssClass="formField150" id="txtVATNo"></asp:TextBox>&nbsp;
                <asp:RegularExpressionValidator Display="None" ValidationGroup="AccountsMigrate" ControlToValidate="txtVATNo" ErrorMessage="Please enter correct VAT Reg. Number" ForeColor="#EDEDEB" ID="rgVatNo" runat="server" ValidationExpression="(^(([GB]|[gb])\s?)*(([1-9]\d{8})|([1-9]\d{11}))$)" >*</asp:RegularExpressionValidator>
                <br />
                <div class="marginTop10">
                 High skills area: <br />
                 <asp:CheckBox runat="server" ID="chkFreesat" Text=" Freesat and Digital Aerials" />
                 <asp:CheckBox runat="server" ID="chkElex" CssClass="marginLeft20" Text=" Consumer Electronic and TV" /><br />
	             <asp:CheckBox runat="server" ID="chkpc" Text=" Home PC Installations" />
	             <asp:CheckBox runat="server" ID="chkIT" Text=" Business IT" CssClass="marginLeft40" /> 	                                             
	            </div>                                 
                 <TABLE cellSpacing="0" cellPadding="0" width="260"  border="0" class="marginTop10">
				 <TR>
					<TD height="18">
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;margin-top:15px;" runat="server" id="Div1">
                    <a runat="server" id="btnCancel"  class="txtButtonRed"> &nbsp;Cancel&nbsp;</a>
                    </div>
                    </TD>
					<td width="158">&nbsp;</td>
					<TD height="18">
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;margin-top:15px;" runat="server" id="Div2">
                    <a onclick="javascript:ConfirmMigration()"  class="txtButtonRed" style="cursor:pointer;"> &nbsp;Confirm&nbsp;</a><input runat="server" type="button" id="btnConfirmClick" style="display:none;" validationgroup="AccountsMigrate" />
                    </div>
                    </TD>
				 </TR>
             </TABLE>  
             </asp:Panel>
             </asp:Panel>
             <asp:Panel ID="pnlMigrateSuccessful" runat="server" Visible="false">
               <span class="HeadingRed">Company account has been successfuly migrated from Client to Supplier</span>
             </asp:Panel>
             </div>
			 <!-- InstanceEndEditable -->
			 </td>
         </tr>
         </table>
      </div>
     
     </asp:Content>

