<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :" Codebehind="SearchWOS.aspx.vb" Inherits="Admin.SearchWOs" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <%@ Register Assembly="WebLibrary" Namespace="WebLibrary" TagPrefix="lib" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
	<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
	
	<%@ Import Namespace="System.Web.Script.Services" %>
    <%@ Import Namespace="System.Web.Services" %>
   
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

	<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<style type="text/css">
.HideBtn
{
display:none;
font-size:0px;
width:0px;
height:0px;
}

</style>

<script type="text/javascript">
    function ShowPopup(Flag, LinkId, WorkOrderId) {
        var strFlag = Flag;
        var DivId = "PopupDiv" + WorkOrderId;
        if (Flag == "True") {

            var div1Pos = $("#" + LinkId).offset();
            var div1X = div1Pos.left + 80;
            var div1Y = div1Pos.top;
            $('#' + DivId).css({ left: div1X, top: div1Y });
            $("#" + DivId).show();
        }
        else {
            $("#" + DivId).hide();
        }
    }

    //   Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.
    function SelectAutoSuggestRow(inp, data) {
        inp.value = data[0];
        var x = document.getElementById("ctl00_ContentPlaceHolder1_hdnSelectedCompanyName").value = inp.value;
        document.getElementById("ctl00_ContentPlaceHolder1_hdnSelectedCompanyID").value = data[1];        
        document.getElementById("ctl00_ContentPlaceHolder1_hdnbtnSelectedCompany").click();
                        }
</script>
			
	        <div id="divContent">
         
		 <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            <%--Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.--%>
         
              <asp:Button runat="server" ID="hdnbtnSelectedCompany" OnClick="hdnbtnSelectedCompany_Click"  Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

			<input id="hdnPageSize" type="hidden" name="hdnPageSize" runat="server"> <input id="hdnPageNo" type="hidden" name="hdnPageNo" runat="server"><input id="sortExpr" type="hidden" name="sortExpr" runat="server"> <input id="sortOrder" type="hidden" name="sortOrder" runat="server">
			<input id="totalRecs" type="hidden" name="totalRecs" runat="server"> <input id="pageRecs" type="hidden" name="pageRecs" runat="server"><input id="defaultSortImgId" type="hidden" name="defaultSortImgId" runat="server"> <input id="criteria" type="hidden" name="criteria" runat="server">
			<input type="hidden" id="hdnWorkorderID" name="hdnWorkorderID" runat="server"><input type="hidden" id="hdnPONumber" name="hdnPONumber" runat="server"><input type="hidden" id="hdnReceiptNumber" name="hdnReceiptNumber" runat="server">
			<input type="hidden" id="hdnKeyWord" name="hdnKeyWord" runat="server"><input type="hidden" id="hdnPostCode" name="hdnPostCode" runat="server"><input type="hidden" id="hdnCompanyName" name="hdnCompanyName" runat="server">
			 <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
             <ContentTemplate>
            
           	
				
                               
                               
			<asp:Panel ID="pnlSearchParams" runat="server" defaultbutton="btnSearch" >
			    <div id="divParentSearch" style="margin-left:20px;">
			        <div id="divrow1" style="width:100%;margin-top:20px;">
			            <div class="formTxt" style="width:150px;">
			                Keyword
			                <asp:TextBox ID="txtKeyword" CssClass="formField150" runat="server" />
			            </div>        
			        </div>
			        <div id="divForAdvanceSearch" runat="server" visible="true">
			            <div id="divrow2" style="width:100%;margin-top:20px;">
			                <div class="formTxt" style="width:150px; float:left; margin-right:10px;">
			                    Work Order ID
                               


					            <lib:Input ID="txtWorkorderID" runat="server" DataType="List"
				                    Method="GetWorkOrderID" CssClass="formField150"/>
					            
			                </div>    
			                <div class="formTxt" style="width:150px; float:left; text-align:left; margin-right:10px;">
			                    Company Name
                                  <%--Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.--%>
                                   <input id="hdnSelectedCompanyName" type="hidden" name="hdnSelectedCompanyName" runat="server" value="" />
                                   <input id="hdnSelectedCompanyID" type="hidden" name="hdnSelectedCompanyID" runat="server" value="" />
			                    <lib:Input ID="txtCompanyName" runat="server" DataType="List"
				                    Method="GetCompanyName" CssClass="formField150" OnSelect='SelectAutoSuggestRow' SelectParameters='CompanyName' />
			                    
			                </div>    
			                <div class="formTxt" style="width:150px; float:left; margin-right:10px;">
			                    PO Number
			                    <lib:Input ID="txtPONumber" runat="server" DataType="List"
				                    Method="GetPONumber" CssClass="formField150"/>
			                </div> 
                            <div class="formTxt" style="width:150px; float:left; margin-right:10px;">
			                    Receipt Number
			                    <lib:Input ID="txtReceiptNumber" runat="server" DataType="List"
				                    Method="GetReceiptNumber" CssClass="formField150"/>
			                </div>    
			                <div class="formTxt" style="width:150px; float:left; margin-right:10px;">
			                    Post Code
			                    <lib:Input ID="txtPostCode" onkeyup="makeUppercase(this.id)" MaxLength="8" runat="server" DataType="List"
				                    Method="GetPostCode" CssClass="formField150"/>		                    
			                </div>    
			                
			                <div class="formTxt" style="width:150px; float:left; ">
			                     Start Date
			                <asp:TextBox ID="txtStartDate" TabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
			                   <img alt="Click to Select" src="Images/calendar.gif" id="btnFromDate" style="cursor:pointer; vertical-align:top;" />		    	
                               <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnFromDate TargetControlID="txtStartDate" ID="calFromDate" runat="server"></cc1:CalendarExtender>
			                   <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regStartDate" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtStartDate" ErrorMessage="Start Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
			                </div>
                            <%--'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.--%>
                             <div class="formTxt" style="width:150px; float:left; ">
			                     Submitted Date From
			                <asp:TextBox ID="txtSubmittedDateFrom" TabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
			                   <img alt="Click to Select" src="Images/calendar.gif" id="btnSubmittedDateFrom" style="cursor:pointer; vertical-align:top;" />		    	
                               <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID="btnSubmittedDateFrom" TargetControlID="txtSubmittedDateFrom" ID="calSubmittedDateFrom" runat="server"></cc1:CalendarExtender>
			                   <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regSubmittedDateFrom" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtSubmittedDateFrom" ErrorMessage="Submitted Date From should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
			                </div>

                               <div class="formTxt" style="width:150px; float:left; ">
			                     Submitted Date To
			                <asp:TextBox ID="txtSubmittedDateTo" TabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
			                   <img alt="Click to Select" src="Images/calendar.gif" id="btnSubmittedDateTo" style="cursor:pointer; vertical-align:top;" />		    	
                               <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID="btnSubmittedDateTo" TargetControlID="txtSubmittedDateTo" ID="calSubmittedDateTo" runat="server"></cc1:CalendarExtender>
			                   <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regSubmittedDateTo" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtSubmittedDateTo" ErrorMessage="Submitted Date To should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
			                   <div style="width:200px;"> <asp:Label runat="server" ID="lblError" CssClass="txtOrange" ForeColor="Red" Text=""></asp:Label></div>
                            </div>
			          </div>
                   
			        </div>
                      <%--Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.--%>
                    <asp:Panel ID="pnlSelectedCompany" runat="server" Visible="false">
                      <div class="divStyleCompany" >
  <asp:Repeater ID="rptSelectedCompany" runat="server" >
    <ItemTemplate>        
        <div class="divStyle1 roundifyRefineMainBody"><span class="clsFloatLeft">
        <asp:Label ID="lblcompany" runat="server" Text=' <%#Eval("CompanyName")%> '></asp:Label></span>
        <span class="marginleft8 clsFloatLeft" >
        <asp:LinkButton ID="lnkbtnRemove" CommandName="Remove" CausesValidation="false" CommandArgument='<%#Container.DataItem("CompanyID") %>' runat="server" class="deleteButtonAccredation">
        x</asp:LinkButton></span>
      </div>
    </ItemTemplate>    
  </asp:Repeater> 
  <asp:Label runat="server" ID="lblSelectedCompany" CssClass="txtOrange" ForeColor="Red" style="clear: both;display: block;padding-left: 10px;" Text=""></asp:Label>
   
  </div>
  </asp:Panel>
			        <div id="divrow4" style="width:100%;margin-top:20px;float:left;">
                   
			            <div style="float:left;">
			            
			                <asp:Button CssClass="formtxt HideBtn" ID="btnSearch" Font-Size="8" CausesValidation="false" runat="server" Text="Search Work Orders"/>
			                <TABLE cellSpacing="0" cellPadding="0" width="260" border="0">
                          <TR>
                          <TD>
                            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:150px;">
                                <a style="cursor:pointer"  onclick='javascript:document.getElementById("ctl00_ContentPlaceHolder1_btnSearch").click()'  id="A2" class="txtButtonRed" >&nbsp;Search Work Orders&nbsp;</a></TD>
                            </div>
                          </TD>  
                          <td runat="server" id="tdExport" visible="false">
                          <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                            <a runat="server" style="cursor:pointer" target="_blank" id="btnExport" class="txtButtonRed" >&nbsp;Export to Excel&nbsp;</a>
                          </div>
                          </td>
						  </TR>
                        </TABLE>
			            </div>			            
			            <div id="divlnkAdvanceSearch" style="visibility:visible; float:left; margin-left:20px;"><a id="lnkAdvanceSearch" CausesValidation="false" visible="false" runat="server">Advanced Search</a></div>
			        </div>
			        <div id="div1" style="width:100%;margin-top:20px;float:left;">
			            <asp:Label runat="server" ID="lblSearchCriteria" CssClass="txtListing"></asp:Label>
			        </div>
			        <div id="div2" style="width:100%;margin-top:20px;">
			            <asp:ValidationSummary id="validationSummarySearch" runat="server" DisplayMode="List" ValidationGroup="ValidationTop" CssClass="bodytxtValidationMsg"></asp:ValidationSummary>
			        </div>
			    </div>
			</asp:Panel>
           
			  <asp:Panel ID="pnlSearchResult" runat=server Visible=false>
         <table width="100%"  align=Left border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td>	  <asp:GridView ID="gvWorkOrders" DataKeyNames="WOID" runat=server AllowPaging="True" AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
               PagerSettings-Mode=NextPreviousFirstLast CssClass="gridRowText"  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true  >          
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               
               <PagerTemplate>
                   
                  
                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>
							<td runat="server" visible="false" id="tdsetType1" width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">
							  <TR>
								<TD class="txtListing" align="left"> 
							<div id="divButton" style="width: 115px;  ">
									  <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
									  <a  runat="server" target="_blank" id="btnExport" tabIndex="6" class="buttonText"> <strong>Export to Excel</strong></a>
									  <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
								    </div>
							
								</TD>
								<TD width="5"></TD>
							  </TR>
							</TABLE>						
							</td>
							
							
							<td width="10">&nbsp;</td>
						
						  
						  <td id="tdsetType2" align="left" width="121">
								
							</td>
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
        </PagerTemplate> 
               <Columns>  
                 <asp:TemplateField SortExpression="WorkOrderId" HeaderText="WorkOrderId" HeaderStyle-CssClass="gridHdr gridText"> 
                <ItemStyle CssClass="gridRowText" Wrap="true"  Width="90"  HorizontalAlign="Right"/>
                <ItemTemplate>
                   <asp:Image ID="imgIsFlagged" runat="server" ImageUrl="~/Images/Icons/Flag.gif" hspace="2" vspace="0" Width="16" Height="17" style="float:left;" Visible='<%#IIf(Container.DataItem("IsFlagged") = "1" ,True,false)%>' />
                   <asp:Label ID="lblWOID" runat="server" Text='<%#Container.DataItem("RefWOID")%>'></asp:Label>         
                </ItemTemplate>
               </asp:TemplateField> 

                <%--<asp:BoundField  ItemStyle-CssClass="gridRowText" ItemStyle-Width="30"  HeaderStyle-CssClass="gridHdr"  DataField="WorkOrderId" SortExpression="WorkOrderId" HeaderText="WorkOrderId" />            --%>
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="60" HeaderStyle-CssClass="gridHdr gridText"  DataField="WOTitle" SortExpression="WOTitle" HeaderText="WOTitle" />            
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="40"  HeaderStyle-CssClass="gridHdr"  DataField="BuyerCompany" SortExpression="BuyerCompany" HeaderText="Client" />            
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="60"  HeaderStyle-CssClass="gridHdr"  DataField="BillingLocation" SortExpression="BillingLocation" HeaderText="Billing Location" />            
                
                <asp:TemplateField SortExpression="SupplierCompany" HeaderText="Supplier" HeaderStyle-CssClass="gridHdr gridText"> 
					 <ItemStyle CssClass="gridRowText" Wrap="true"  Width="80px" />
					 <ItemTemplate> 
                        
                        
						<table width="100%" cellspacing="0" cellpadding="0" >
							<tr>
                                <td width="260">
									<span style='cursor:hand;' onMouseOut=javascript:ShowPopup('False','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>); onMouseOver=javascript:ShowPopup('True','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);>
										<%#GetLinks(1, IIf(IsDBNull(Container.DataItem("SupCompID")), "", Container.DataItem("SupCompID")), IIf(IsDBNull(Container.DataItem("SupplierContactID")), "", Container.DataItem("SupplierContactID")), IIf(IsDBNull(Container.DataItem("SupplierCompany")), "", Container.DataItem("SupplierCompany")))%>
									</span> 
								</td>
                                <td id="tdRating" visible="true" valign="top" runat="server" >
									<div style="text-align:right;" id="PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>">
                                        <a style="text-align:right;" id="ShowPopupAnchor" onclick="javascript:ShowPopup('True','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);"><img id="imgTestQuickNote" src="Images/Icons/FindSuppliersforWorkOrder.gif" title="View Supplier Details" alt="View Supplier Details"  width="16" height="15" hspace="2" vspace="0" border="0" /></a></div>
                                        <div id="PopupDiv<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>" style="display: none;position:absolute; margin-left:-50px;">
                                                <asp:Panel runat="server" ID="pnlServicePartner" Width="200px" style="background-color:#FFEBDE; font-family: Tahoma, Arial, Verdana; font-size: 11px; color: #000000; text-decoration: none; line-height: 18px;	border: 1px solid #CECBCE;" >
                                                        <div style="text-align:right;"><a style="color:Black;text-align:right;vertical-align:top;cursor:pointer;" onclick="javascript:ShowPopup('False','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);"><b>Close [X]</b></a></div>
                                                        <asp:Label id="lblContactName" runat="server" Text='<%# Cstr("<b>Contact Name: </b>" + iif(ISDBNULL(Container.DataItem("SupplierContact")),"--",Container.DataItem("SupplierContact"))) %>' ></asp:Label><br />
                                                        <asp:Label id="lblPhoneNo" runat="server" Text='<%#Cstr("<b>Phone No: </b>" + iif(ISDBNULL(Container.DataItem("SupplierPhone")),"--",Container.DataItem("SupplierPhone"))) %>' ></asp:Label><br />
                                                        <asp:Label id="lblMobileNo" runat="server" Text='<%#Cstr("<b>Mobile No: </b>" + iif(ISDBNULL(Container.DataItem("SupplierMobile")),"--",Container.DataItem("SupplierMobile"))) %>' ></asp:Label><br />
                                                        <asp:Label id="lblEmail" runat="server" Text='<%#Cstr("<b>Email Id: </b>" + iif(ISDBNULL(Container.DataItem("SupplierEmail")),"--",Container.DataItem("SupplierEmail"))) %>' ></asp:Label>
                                                </asp:Panel>
                                        </div>	
                                    </div>
                                </td>
							</tr>
						</table>
					 </ItemTemplate>
               </asp:TemplateField>
                
               <asp:TemplateField SortExpression="WholesalePrice" HeaderText="WholesalePrice" HeaderStyle-CssClass="gridHdr gridText"> 
                <ItemStyle CssClass="gridRowText" Wrap="true"  Width="20"  HorizontalAlign="Right"/>
                <ItemTemplate>
                <%#FormatCurrency(IIF(Not isDbnull(Container.DataItem("WholesalePrice")) ,Container.DataItem("WholesalePrice"),"0"),2,TriState.True, Tristate.true,Tristate.false)%>             
				</ItemTemplate>
               </asp:TemplateField> 
                
                <asp:TemplateField SortExpression="PlatformPrice" HeaderText="PlatformPrice" HeaderStyle-CssClass="gridHdr gridText"> 
                <ItemStyle CssClass="gridRowText" Wrap="true"  Width="20"  HorizontalAlign="Right"/>
                <ItemTemplate>
                <%#FormatCurrency(IIF(Not isDbnull(Container.DataItem("PlatformPrice")) ,Container.DataItem("PlatformPrice"),"0"),2,TriState.True, Tristate.true,Tristate.false)%>             
				</ItemTemplate>
               </asp:TemplateField>
               
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="30"  HeaderStyle-CssClass="gridHdr"  DataField="Status" SortExpression="Status" HeaderText="Status" />
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="50"  HeaderStyle-CssClass="gridHdr"  DataField="SalesInvoice" SortExpression="SalesInvoice" HeaderText="Sales Invoice Number" />
               
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="50"  HeaderStyle-CssClass="gridHdr"  DataField="PurchaseInvoice" SortExpression="PurchaseInvoice" HeaderText="Purchase Invoice Number" />
                
                                
               <%--'Poonam modified on 31-03-2017 - Task - OA-427 - OA - Unsupported spaces in Admin users --%>  
                <asp:TemplateField ItemStyle-Width="8%">               
                <HeaderStyle  CssClass="gridHdr" />
                <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <a runat="server" id="lnkCustomerHistory" 
                          href=<%#"~/CustomerHistory.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("WorkOrderID") & "&BizDivID=1&" & "Group=" & iif(Container.DataItem("Status")="Conditional Accept","CA" ,iif(Container.DataItem("Status")="Active","Accepted",Container.DataItem("Status"))) & "&" & "CompanyID=" & Container.DataItem("BuyerCompID") & "&ContactId=" & Container.DataItem("BuyerContactID") & "&" & "SupCompID=" & Container.DataItem("SupCompID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "sender=SearchWO" & "&" & "CompanyName=" & (hdnCompanyName.Value.Replace("'", "dquotes")).ToString & "&" & "KeyWord=" & (txtKeyword.Text.Replace("'","dquotes")).ToString & "&" & "PONumber=" & (txtPONumber.text.Replace("'","dquotes")).ToString & "&" & "ReceiptNumber=" & (txtReceiptNumber.text.Replace("'","dquotes")).ToString & "&" & "txtWorkorderID=" & txtWorkorderID.text & "&" & "PostCode=" & (txtPostCode.text).ToString & "&" & "DateStart=" & (txtStartDate.text).ToString & "&" & "DateSubmittedFrom=" & (txtSubmittedDateFrom.text).ToString & "&" & "DateSubmittedTo=" & (txtSubmittedDateTo.text).ToString %> ><img src="Images/Icons/CustomerHistory.gif" hspace="2" vspace="0" border="0" title="Customer History"></a>
                        <%# getActionLinks(Container.DataItem("WOID"), Container.DataItem("StagedWO"), Container.DataItem("WorkOrderID"), Container.DataItem("BuyerCompID"), Container.DataItem("BuyerContactID"), Container.DataItem("SupCompID"), Container.DataItem("Status"), Container.DataItem("BuyerAccept"), Container.DataItem("WholesalePrice"), "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "sender=SearchWO" & "&" & "CompanyName=" & (hdnCompanyName.Value.Replace("'", "dquotes")).ToString & "&" & "KeyWord=" & (txtKeyword.Text.Replace("'", "dquotes")).ToString & "&" & "PONumber=" & (txtPONumber.Text.Replace("'", "dquotes")).ToString & "&" & "ReceiptNumber=" & (txtReceiptNumber.Text.Replace("'", "dquotes")).ToString & "&" & "txtWorkorderID=" & txtWorkorderID.Text & "&" & "PostCode=" & (txtPostCode.Text).ToString & "&" & "DateStart=" & (txtStartDate.Text).ToString & "&" & "DateSubmittedFrom=" & (txtSubmittedDateFrom.Text).ToString & "&" & "DateSubmittedTo=" & (txtSubmittedDateTo.Text).ToString)%>
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="15">               
                <HeaderStyle  CssClass="gridHdr" />
                <ItemStyle Wrap="true" HorizontalAlign="Left" CssClass="gridRowText"/>
                    <ItemTemplate>
                        <div style="float:left;"><a runat="server" id="lnkDetail" 
              href=<%#"~/AdminWODetails.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("WorkOrderID") & "&BizDivID=1&" & "Group=" & iif(Container.DataItem("Status")="Conditional Accept","CA" ,iif(Container.DataItem("Status")="Active","Accepted",Container.DataItem("Status"))) & "&" & "CompanyID=" & Container.DataItem("BuyerCompID") & "&ContactId=" & Container.DataItem("BuyerContactID") & "&" & "SupCompID=" & Container.DataItem("SupCompID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "sender=SearchWO" & "&" & "CompanyName=" & (hdnCompanyName.Value.Replace("'", "dquotes")).ToString & "&" & "KeyWord=" & (txtKeyword.Text.Replace("'","dquotes")).ToString & "&" & "PONumber=" & (txtPONumber.text.Replace("'","dquotes")).ToString & "&" & "ReceiptNumber=" & (txtReceiptNumber.text.Replace("'","dquotes")).ToString & "&" & "txtWorkorderID=" & txtWorkorderID.text & "&" & "PostCode=" & (txtPostCode.text).ToString & "&" & "DateStart=" & (txtStartDate.text).ToString & "&" & "DateSubmittedFrom=" & (txtSubmittedDateFrom.text).ToString & "&" & "DateSubmittedTo=" & (txtSubmittedDateTo.text).ToString %> ><img src="Images/Icons/ViewDetails.gif" width="16" height="16" hspace="2" vspace="0" border="0" title="View Details"></a></div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="15">               
                <HeaderStyle  CssClass="gridHdr" />
                <ItemStyle Wrap="true" HorizontalAlign="Left" CssClass="gridRowText"/>
                    <ItemTemplate>
                        <div style="float:left;"><a runat="server" id="lnkSIDetail" target="_blank" visible='<%#iif(Container.DataItem("SalesInvoice")<>"",True,False )%>' href= <%#"~/ViewSalesInvoice.aspx?invoiceNo=" & Container.DataItem("SalesInvoice") & "&" & "companyId=" & Container.DataItem("BuyerCompID") & "&ContactId=" & Container.DataItem("BuyerContactID") & "&" & "bizDivId=1"%> ><img src="~/Images/Icons/View-Sales-Invoice.gif" hspace="2" vspace="0" id="imgSI" border="0" runat="Server" title="View Sales Invoice"></a></div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="15">               
                <HeaderStyle  CssClass="gridHdr" />
                <ItemStyle Wrap="true" HorizontalAlign="Left" CssClass="gridRowText"/>
                    <ItemTemplate>
                        <div style="float:left;"><a runat="server" id="lnkPIDetail" target="_blank" visible='<%#iif(Container.DataItem("PurchaseInvoice")<>"",True,False)%>' href= <%#"~/ViewPurchaseInvoice.aspx?invoiceNo=" & Container.DataItem("PurchaseInvoice") & "&" & "companyId=" & Container.DataItem("SupCompID") & "&" & "WOID=" & Container.DataItem("WOID") & "&" & "bizDivId=1"%> ><img id="Img1" src="~/Images/Icons/View-Purchase-Invoice.gif" title="View Purchase Invoice" runat="Server" hspace="2" vspace="0" border="0"></a></div>
                    </ItemTemplate>
                </asp:TemplateField>
                
               <asp:TemplateField visible="false" HeaderStyle-CssClass="gridHdr">                
                <ItemStyle CssClass="gridRowText" Wrap=true  Width="15"/>
                <ItemTemplate> 
                    <asp:LinkButton ID="lnkbtnQuickNote" CommandName="AddNote" CommandArgument='<%#Container.dataitem("WOID") %>' runat="server">
                        <img id="imgQuickNote" runat="server" src='<%# IIF(Container.DataItem("NtCnt") = 0,"~/Images/Icons/QuickNote.gif","~/Images/Icons/Note.gif") %>' title='<%# IIF(Container.DataItem("NtCnt") = 0,"Add a Quick Note","") %>'  width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" />
                    </asp:LinkButton>
				</ItemTemplate>  
               </asp:TemplateField>
               
               <asp:TemplateField visible=false HeaderStyle-CssClass="gridHdr">                
                <ItemStyle CssClass="gridRowText" Wrap=true  Width="15"/>
                <ItemTemplate> 
                <%--'Poonam modified on 31-03-2017 - Task - OA-427 - OA - Unsupported spaces in Admin users --%> 
               	<a runat="server" visible='<%# IIF(Container.DataItem("NtCnt") = 0,False,True) %>' id="lnkNotes" href= '<%#"~/WONotes.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("WorkOrderID") & "&" & "Group=" & Container.DataItem("Status") & "&" & "CompanyID=" & Container.DataItem("BuyerCompID") & "&" & "SupCompID=" & Container.DataItem("SupCompID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "sender=SearchWO" & "&" & "CompanyName=" & (hdnCompanyName.Value.Replace("""", "dquotes")).ToString & "&" & "KeyWord=" & (txtKeyword.Text.Replace("""","dquotes")).ToString & "&" & "PONumber=" & (txtPONumber.text).ToString & "&" & "ReceiptNumber=" & (txtReceiptNumber.text).ToString & "&" & "txtWorkorderID=" & txtWorkorderID.text & "&" & "PostCode=" & (txtPostCode.text).ToString%>'><img src="Images/Icons/Note.gif" title="View Notes" width="10" height="11" hspace="2" vspace="0" border="0"></a>
				</ItemTemplate>  
               </asp:TemplateField>
               
               <asp:TemplateField HeaderStyle-CssClass="gridHdr">                
                <ItemStyle CssClass="gridRowText" Wrap=true />
               <ItemTemplate> 
                <%--Poonam added on 28/05/2015 - Task - 4474151: OWEM-EHN-ADMIN - Add details from Work Request Location in /WOForm.aspx to /SearchWOs.aspx results grid--%>
                   <table>
                       <tr>
                   <td id="tdRatingClient" visible="true" valign="top" runat="server" >
									<div style="text-align:right;" id="PAnchorClient<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>">
                                        <a style="text-align:right;" id="ShowPopupAnchorClient" onclick="javascript:ShowPopupClient('True','PAnchorClient<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);">
                                            <img id="imgTestQuickNoteClient" src="Images/Icons/FindSuppliersforWorkOrder.gif" title="View Client Details" alt="View Client Details"  width="16" height="15" hspace="2" vspace="0" border="0" /></a>
                                        </div>
                                        <div id="PopupDivClient<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>" style="display: none;position:absolute; margin-left:-50px;">
                                                <asp:Panel runat="server" ID="pnlServicePartnerClient" Width="200px" style="background-color:#FFEBDE; font-family: Tahoma, Arial, Verdana; font-size: 11px; color: #000000; text-decoration: none; line-height: 18px;	border: 1px solid #CECBCE;" >
                                                        <div style="text-align:right;"><a style="color:Black;text-align:right;vertical-align:top;cursor:pointer;" onclick="javascript:ShowPopupClient('False','PAnchorNewClient<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);"><b>Close [X]</b></a>
                                                        </div>
                                                    <asp:Label id="lblClientFName" runat="server" Text='<%# Cstr("<b>First Name: </b>" + iif(ISDBNULL(Container.DataItem("LocationFName")),"--",Container.DataItem("LocationFName"))) %>' ></asp:Label><br />
                                                    <asp:Label id="lblClientLName" runat="server" Text='<%# Cstr("<b>Last Name: </b>" + iif(ISDBNULL(Container.DataItem("LocationLName")),"--",Container.DataItem("LocationLName"))) %>' ></asp:Label><br />
                                                    <asp:Label id="lblClientPhone" runat="server" Text='<%# Cstr("<b>Phone No: </b>" + iif(ISDBNULL(Container.DataItem("LocationPhone")),"--",Container.DataItem("LocationPhone"))) %>' ></asp:Label><br />
                                                    <asp:Label id="lblClientMobile" runat="server" Text='<%# Cstr("<b>Mobile No: </b>" + iif(ISDBNULL(Container.DataItem("LocationMobile")),"--",Container.DataItem("LocationMobile"))) %>' ></asp:Label><br />
                                                    <asp:Label id="lblClientAddress" runat="server" Text='<%# Cstr("<b>Address: </b>" + iif(ISDBNULL(Container.DataItem("LocationAddress")),"--",Container.DataItem("LocationAddress"))) %>' ></asp:Label><br />
                                                     <asp:Label id="lblClientCity" runat="server" Text='<%# Cstr("<b>City: </b>" + iif(ISDBNULL(Container.DataItem("LocationCity")),"--",Container.DataItem("LocationCity"))) %>' ></asp:Label><br />
                                                     <asp:Label id="lblClientPostCode" runat="server" Text='<%# Cstr("<b>Post Code: </b>" + iif(ISDBNULL(Container.DataItem("LocationPostCode")),"--",Container.DataItem("LocationPostCode"))) %>' ></asp:Label><br />
                                                </asp:Panel>
                                        </div>	                  
                                </td>
               <td>    <a id="aRemove" style="cursor:pointer;"  onclick="javascript:xShowModalDialog('RemoveAttachments.aspx?WOID=<%#DataBinder.Eval(Container.DataItem,"WOID") %>&WOStatus=<%#DataBinder.Eval(Container.DataItem,"WOStatus") %>','Sign',320,140);"><asp:image ID="Image1" runat="server" ImageUrl="~/Images/Icons/icon1-attachement_remove.gif" AlternateText="Remove Attachment" ToolTip="Remove Attachment" hspace="2" vspace="0" border="0" Visible='<%# IIF(Container.DataItem("AttachmentCount") = 0,False,True) %>' /></a></td>
                    <td><a id="aUpload" style="cursor:pointer;"  onclick="javascript:xShowModalDialog('FileUpload.aspx?WOID=<%#DataBinder.Eval(Container.DataItem,"WOID") %>&CompanyID=<%#DataBinder.Eval(Container.DataItem,"BuyerCompID") %>&BizDivID=1&WOStatus=<%#DataBinder.Eval(Container.DataItem,"WOStatus") %>','Sign',500,170);"><asp:image runat="server" ImageUrl="~/Images/Icons/icon-attachement.gif" AlternateText="Upload Attachment" ToolTip="Upload Attachment" hspace="2" vspace="0" border="0" Visible='<%# IIF(Container.DataItem("AttachmentCount") = 0 and (Container.DataItem("WOStatus") = 9 or Container.DataItem("WOStatus") = 10),True,False) %>' /></a></td>
                    <%--<asp:LinkButton ID="lnkbtnUpdateRating" Visible='<%# IIF(Container.DataItem("WOStatus") = 10,True,False) %>' CommandName="WOID" CommandArgument='<%#Container.dataitem("WOID") %>' runat="server"><img id="imgUpdateRating" runat="server" src="~/Images/Icons/Update_rating.gif" title="Update Rating" width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" /></asp:LinkButton>--%>
                </tr>
                   </table>
                 
                 
                </ItemTemplate>
                </asp:TemplateField>   
                 <asp:TemplateField HeaderStyle-CssClass="gridHdr" visible="false" >                
                <ItemStyle CssClass="gridRowText" Wrap=true />
               <ItemTemplate> 
               <asp:LinkButton ID="lnkbtnUpdateRating" Visible='<%# IIF(Container.DataItem("WOStatus") = 6  or Container.DataItem("WOStatus") = 7 or Container.DataItem("WOStatus") = 9 or Container.DataItem("WOStatus") = 10,True,False) %>' CommandName="WOID" CommandArgument='<%#Container.dataitem("WOID") %>' runat="server"><img id="imgUpdateRating" runat="server" src="~/Images/Icons/Update_rating.gif" title="Update Rating" width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" /></asp:LinkButton>
                  <%--<asp:LinkButton ID="lnkbtnUpdateRating"  CommandName="RateWO" CommandArgument='<%#Container.dataitem("WOID") %>' runat="server"><img id="imgUpdateRating" runat="server" src="~/Images/Icons/Update_rating.gif" title="Rate WO" width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" /></asp:LinkButton>--%>
                 </ItemTemplate>
                </asp:TemplateField>    

                 <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">                
                <ItemStyle CssClass="gridRowIconsText" Wrap=true  Width="14px"/>
                <ItemTemplate> 
                <span style='cursor:hand;' onMouseOut=javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','visible')>
                    <asp:LinkButton ID="lnkbtnAction" CommandName="Action" CausesValidation="false" CommandArgument='<%#Container.dataitem("WOID") & "," & Container.DataItem("Status") %>' runat="server">
                        <img id="imgAction" runat="server" src=<%#GetActionIcon(Container.DataItem("NtCnt"),Container.DataItem("Rating"))%>  width="24" height="30" hspace="2" vspace="0" border="0" class="cursorHand" />
                    </asp:LinkButton>
                    </span>
                    <div class="divUserNotesDetail" id=divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%> onMouseOut=javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','visible')>
                         <asp:panel runat="server" style="color:black;width: 230px;max-height:320px;overflow:scroll;" id="pnlUserNotesDetail" Visible='<%# IIF(Container.DataItem("NtCnt") = 0,IIF(Container.DataItem("RatingComment") = "",False,True) ,True) %>'>
                         <div><b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b></div>
                         <div class="clsRatingInner">
                         <div id="divRatingComment" runat="server" visible='<%# IIF(Container.DataItem("RatingComment") = "",False,True) %>'>
                            <b style="color:#0C96CF; font-size:12px;">Rating Comment:</b>
                            <div style="margin-top: 0px; margin-bottom: 10px;">
                                <%# Container.DataItem("RatingComment")%>
                            </div>
                         </div>
                         <div id="divNotes" runat="server" visible='<%# IIF(Container.DataItem("NtCnt") = 0,False,True) %>'>
                          <b style="color:#0C96CF; font-size:12px;">Notes:</b>
			                <asp:Repeater ID="DLUserNotesDetail" runat="server" DataSource='<%#GetUserNotesDetail(Container.DataItem("WOID"))%>'>
                             <ItemTemplate>
                                <div style='visible: <%# iif( Trim(Container.DataItem("Comments")) = "" , "hidden", "visible") %>'>
                                <div>
                               <%--poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2 --%>
                                   <%# "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - <b>" & Container.DataItem("OWRepFName") & " " & Container.DataItem("OWRepLName") & "</b><br/><b>" & Container.DataItem("NoteCategory") & "</b></span>"%>
                                </div>
                                <div style="margin-top: 0px; margin-bottom: 10px;">
                                <%# Container.DataItem("Comments") %>
                                </div>
                                    </div>
                             </ItemTemplate>
                            </asp:Repeater>
                           </div>
			             </div>   
			             <div><b class="b4"></b><b class="b3"></b><b class="b2"></b><b class="b1"></b></div>
			             </asp:panel>
		             </div>
				</ItemTemplate>  
               </asp:TemplateField>
               <asp:TemplateField HeaderStyle-CssClass="gridHdr" >
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnWatch" CommandName="Watch" CausesValidation="false" runat="server" Visible='<%# ShowHideIsWatch(Container.DataItem("Status"),Container.DataItem("IsWatch"),"Watch")%>'
                            CommandArgument='<%#Container.DataItem("WOID") %>'>
                            <img id="imgwatch" runat="server" src="~/Images/Icons/open.png" width="24" height="20"
                                hspace="2" vspace="0" border="0" class="cursorHand" />
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnUnwatch" CommandName="Unwatch" CausesValidation="false" Visible='<%# ShowHideIsWatch(Container.DataItem("Status"),Container.DataItem("IsWatch"),"UnWatch")%>'
                            CommandArgument='<%#Container.DataItem("WOID") %>' runat="server">
                            <img id="imgUnwatch" runat="server" src="~/Images/Icons/Closed.png" width="24" height="15"
                                hspace="2" vspace="0" border="0" class="cursorHand" />
                         </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
               </Columns>
               
               <AlternatingRowStyle  CssClass=gridRowText />
               <RowStyle CssClass=gridRowText />            
               <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
               <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
               <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
           </asp:GridView>
		</td>
        </tr>
      </table>
				
	<asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.SearchWOs" SortParameterName="sortExpression">                
    </asp:ObjectDataSource>
    </asp:Panel>
           <cc1:ModalPopupExtender ID="mdlUpdateRating" runat="server" TargetControlID="btnTemp1" PopupControlID="pnlUpdateWO" OkControlID="" CancelControlID="ancCancel1" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False"></cc1:ModalPopupExtender>
               <asp:Panel runat="server" ID="pnlUpdateWO"  Width="300px" CssClass="pnlConfirm" style="display:none;">
               <div id="divModalParent">
               <b>Edit ratig of the workorder</b> <br />  
                 <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnPositive" Checked="false" runat="server" Text="Positive" />
                 <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNeutral"  Checked="false" runat="server"  Text="Neutral"/>
                 <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNegative" Checked="false" runat="server" Text="Negative" /><br /><br />
                 <b>Add comment here</b><br />
                    <asp:TextBox runat="server" ID="txtComment" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox>        
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtComment" WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        
                 <div id="divButton" class="divButton" style="width: 85px; float:left;">
                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                        <a class="buttonText cursorHand" href='javascript:validate1_required("ctl00_ContentPlaceHolder1_txtComment","Please enter a note")' id="a1"><strong>Submit</strong></a>
                        <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    </div>
                    <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                        <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                        <a class="buttonText cursorHand" runat="server" id="ancCancel1"><strong>Cancel</strong></a>
                        <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    </div> 
                    </div>
                    </asp:Panel>
                        <asp:Button runat="server" ID="hdnButton1" OnClick="hdnButton1_Click"  Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
                        <asp:Button runat="server" ID="btnTemp1" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />


                        <cc1:ModalPopupExtender ID="mdlAction" runat="server" TargetControlID="btnActionTemp" PopupControlID="pnlActionConfirm" OkControlID="" CancelControlID="ancCancelAction" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
			      
<asp:Panel runat="server" ID="pnlActionConfirm"  Width="340px" CssClass="pnlConfirmWOListingForAction" style="display:none;">
               
    <div id="div3">
               <div class="divTopActionStyle"><b>OrderWork Action Form</b> </div>

               <div class="divTopActionStyle" id="divActionRating" runat="server" style="display:none;">
                           <b>Rate workorder</b> <br />  
                             <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnActionPositive" Checked="false" runat="server" Text="Positive" />
                             <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnActionNeutral"  Checked="false" runat="server"  Text="Neutral"/>
                             <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnActionNegative" Checked="false" runat="server" Text="Negative" /><br />
                             Add comment for rating<br />
                             <asp:TextBox runat="server" ID="txtCommentAction" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox>        
                           <%-- <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtComment" WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        --%>
                </div> 
                    
                <div class="divTopActionStyle">
                            <b>Post Quick Note</b><br />
                            <asp:TextBox runat="server" ID="txtNoteAction" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox> 
                            <%--poonam modified on 15/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types--%>
                        <br />                          
                        <b>Note Type</b><br />
                         <asp:DropDownList  Width="280px" Height="25px" CssClass="bodyTextGreyLeftAligned" ID="ddlNoteType" runat="server" AutoPostBack="false"></asp:DropDownList> 
                      <br />
                            <span style="color:#e51312;font-size:11px;">*Notes entered here are NOT visible to the Suppliers/Clients.</span><br />       
                            <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtNote" WatermarkText="Enter a new note here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        --%>
                </div>

                 <div class="divTopActionStyle" id="div4" runat="server">
                            <asp:CheckBox ID="chkShowClient" runat="server" style="font-weight:bold;" Text="Show To Client"  ></asp:CheckBox>
                            <asp:CheckBox ID="chkShowSupplier" runat="server" style="font-weight:bold;" Text="Show To Supplier"  ></asp:CheckBox>
                </div>
               
                <div class="divTopActionStyle" id="divActionWatch" runat="server" style="display:none;">
                            <asp:CheckBox ID="chkMyWatch" runat="server" style="font-weight:bold;" Text="Add To MyWatch"  ></asp:CheckBox>
                </div>
                 
                <div class="divTopActionStyle" style="font-weight:bold;">
                           <asp:CheckBox ID="chkMyRedFlag" runat="server" Text="Add To MyRedFlag" ></asp:CheckBox>
                </div>
                 
                 <div style="padding-left:20px;padding-top:10px;float:left;width:305px;">
               
                           <div  class="divButtonAction" style="width: 85px; float:left;cursor:pointer;">
                           <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner"  /></div>                    
                           <a class="buttonText cursorHand" onclick='javascript:validate_ActionComment("ctl00_ContentPlaceHolder1_txtCommentAction","Please enter rating comment")' id="ancSubmitAction">Submit</a>
                           <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner"  /></div>
                           </div>

                           <div  class="divButtonAction" style="width: 85px; float:left; margin-left:20px;cursor:pointer;">
                           <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                           <a class="buttonText cursorHand" runat="server" id="ancCancelAction">Cancel</a>
                           <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                           </div> 

                </div>
                    
                    
    </div>
    
  </asp:Panel>
  
<asp:Button runat="server" ID="hdnBtnAction"  Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
<asp:Button runat="server" ID="btnActionTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

          </ContentTemplate>
          <%--Poonam modified on 2/8/2016 - Task -OA-311 : EA - Multiple search company in listing does not work in Chrome--%>
                 <Triggers>
                  <asp:AsyncPostBackTrigger EventName="Click" ControlID="hdnbtnSelectedCompany" />
                     </Triggers>
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
<cc1:ModalPopupExtender ID="mdlQuickNotes" runat="server" TargetControlID="btnTemp" PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
<asp:Panel runat="server" ID="pnlConfirm" Width="300px" CssClass="pnlConfirm" style="display:none;">
    <div id="divModalParent">
        <span style="font-family:Tahoma; font-size:14px;"><b>Add a QuickNote</b></span><br /><br />
        <asp:TextBox runat="server" ID="txtNote" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox>        
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtNote" WatermarkText="Enter a new note here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        
        <br /><br />
            <div id="divButton" class="divButton" style="width: 85px; float:left;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                    <a class="buttonText cursorHand" href='javascript:validate_required("ctl00_ContentPlaceHolder1_txtNote","Please enter a note")' id="ancSubmit"><strong>Submit</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div>
            <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>								
</asp:Panel>
<asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
<asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

<script type="text/javascript">
function validate_required(field,alerttxt)
{
    var text = document.getElementById(field).value;
      if (text==null||text==""||text=="Enter a new note here")
            {
           }
        else {            
            document.getElementById("ctl00_ContentPlaceHolder1_hdnButton").click();            
        }
}

    function validate1_required(field,alerttxt)
    {
    
        var text = document.getElementById(field).value;
          if (text==null||text==""||text=="Enter a comment here")
                {alert(alerttxt);
              }
         else {            
                document.getElementById("ctl00_ContentPlaceHolder1_hdnButton1").click();            
              }
        }

        function validate_ActionComment(field, alerttxt) {
        var NoteTypeValue = document.getElementById("ctl00_ContentPlaceHolder1_ddlNoteType").value;
        if (NoteTypeValue == "") {
            alert('Please Select Note Type');
            return 0;
        }
            if (document.getElementById("ctl00_ContentPlaceHolder1_divActionRating").style.display == "block") {
                var RatingSelected = 0;

                if (document.getElementById("ctl00_ContentPlaceHolder1_rdBtnActionPositive").checked) {
                    RatingSelected = 1;
                }
                else if (document.getElementById("ctl00_ContentPlaceHolder1_rdBtnActionNeutral").checked) {
                    RatingSelected = 1;
                }
                else if (document.getElementById("ctl00_ContentPlaceHolder1_rdBtnActionNegative").checked) {
                    RatingSelected = 1;
                }

                if (RatingSelected == 1) {
                    var text = document.getElementById(field).value;
                    if (text == null || text == "" || text == "Enter a new comment here") {
                        //alert("Testing");
                        document.getElementById("ctl00_ContentPlaceHolder1_hdnBtnAction").click();
                    }
                    else {
                        if (text.length > 500) {
                            alert('Please enter comments less than 500 characters');
                        }
                        else {
                            document.getElementById("ctl00_ContentPlaceHolder1_hdnBtnAction").click();
                        }
                    }
                }
                else {
                    document.getElementById("ctl00_ContentPlaceHolder1_hdnBtnAction").click();
                }
                
            }
            else {
                document.getElementById("ctl00_ContentPlaceHolder1_hdnBtnAction").click();
            }
        }

         <%--Poonam added on 28/05/2015 - Task - 4474151: OWEM-EHN-ADMIN - Add details from Work Request Location in /WOForm.aspx to /SearchWOs.aspx results grid--%>
   
        function ShowPopupClient(Flag, LinkId, WorkOrderId) {
            var strFlag = Flag;
            var DivId = "PopupDivClient" + WorkOrderId;
            if (Flag == "True") {

                var div1Pos = $("#" + LinkId).offset();
                var div1X = div1Pos.left + 80;
                var div1Y = div1Pos.top;
                $('#' + DivId).css({ left: div1X, top: div1Y });
                $("#" + DivId).show();
            }
            else {
                $("#" + DivId).hide();
            }
        }
</script>

</asp:Content>