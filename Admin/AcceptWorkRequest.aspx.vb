Public Partial Class AcceptWorkRequest
    Inherits System.Web.UI.Page

    Protected WithEvents UCAcceptWR1 As UCMSAcceptWorkRequest

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        UCAcceptWR1.BizdivID = Session("BizDivId")
        UCAcceptWR1.CompanyID = Session("CompanyId")
        UCAcceptWR1.ContactID = Session("UserId")
        UCAcceptWR1.ClassID = ApplicationSettings.RoleOWID
        UCAcceptWR1.Viewer = ApplicationSettings.ViewerAdmin
        If Not IsNothing(Request("WOID")) Then
            UCAcceptWR1.WOID = Request("WOID").Trim
        End If
        UCAcceptWR1.Viewer = "Admin"
    End Sub

End Class