﻿Public Class AddEditServiceQA
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            Session("ServiceQ") = Nothing

            If Not IsNothing(Request.QueryString("ProductID")) Then
                ViewState.Add("ProductID", Request.QueryString("ProductID"))
                PopulateData()
            End If
            lnkBackToService.HRef = getBackToServiceLink()
        End If
    End Sub
    Public Sub PopulateData()
        divValidationServiceQuestions.Visible = False
        lblValidationServiceQuestions.Text = ""
        Dim ds As New DataSet
        ds = ws.WSWorkOrder.populateWOFormProductDetails(CInt(ViewState("ProductID")), CInt(Request("companyid")), 1, CInt(HttpContext.Current.Session("UserID")), "ThermostatsQuestions", ViewState("ProductID").ToString)
        If ds.Tables.Count > 0 Then
            Session("ServiceQ") = ds
            If ds.Tables(0).Rows.Count > 0 Then
                PopulateServiceQ()
            End If
        End If
    End Sub
    Public Function getBackToServiceLink() As String
        Dim link As String = ""
        link = "~\AdminProductSpecific.aspx?mode=edit&productid=" & ViewState("ProductID") & "&pagename=CompanyProfile&CompanyID=" & Request("companyid") & "&bizDivId=1"
        Return link
    End Function
    Protected Sub PopulateServiceQ()
        Dim dsServiceQ As DataSet
        dsServiceQ = CType(Session("ServiceQ"), DataSet)

        Dim dvServiceQ As New DataView
        dvServiceQ = dsServiceQ.Tables(0).DefaultView
        If (dvServiceQ.ToTable.Rows.Count > 0) Then
            rptServiceTab1Q.Visible = True
            rptServiceTab1Q.DataSource = dvServiceQ.ToTable.DefaultView
            rptServiceTab1Q.DataBind()
        Else
            rptServiceTab1Q.Visible = False
            rptServiceTab1Q.DataSource = Nothing
            rptServiceTab1Q.DataBind()
        End If
    End Sub

    Private Sub rptServiceQ_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptServiceTab1Q.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptServiceAnswers As Repeater = CType(e.Item.FindControl("rptServiceQuestionAnswers"), Repeater)
            Dim hdnQuestionId As HtmlInputHidden = CType(e.Item.FindControl("hdnQuestionId"), HtmlInputHidden)
            Dim hdnSequence As HtmlInputHidden = CType(e.Item.FindControl("hdnSequence"), HtmlInputHidden)
            Dim hdnBaseQuestionSequence As HtmlInputHidden = CType(e.Item.FindControl("hdnBaseQuestionSequence"), HtmlInputHidden)
            Dim hdnBaseQuestionSequenceTab As HtmlInputHidden = CType(e.Item.FindControl("hdnBaseQuestionSequenceTab"), HtmlInputHidden)

            Dim ddQAnsCount As DropDownList = CType(e.Item.FindControl("ddQAnsCount"), DropDownList)


            Dim ds As New DataSet
            ds = CType(Session("ServiceQ"), DataSet)
            Dim dv As New DataView
            If ds.Tables.Count > 1 Then
                If ds.Tables(1).Rows.Count > 0 Then
                    dv = ds.Tables(1).DefaultView
                    dv.RowFilter = "QuestionId = " & hdnQuestionId.Value
                    rptServiceAnswers.DataSource = dv.ToTable.DefaultView
                    rptServiceAnswers.DataBind()
                End If
            End If
            ddQAnsCount.SelectedValue = dv.Item(0)("QuestionAnsCount")

        End If
    End Sub
 
    Private Sub rptServiceQ_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptServiceTab1Q.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "RemoveQuestion") Then
                Dim hdnQuestionId As HtmlInputHidden = CType(e.Item.FindControl("hdnQuestionId"), HtmlInputHidden)

                UpdateServiceQA(rptServiceTab1Q)

                RemoveQuestionAndAnswer(hdnQuestionId.Value.ToString)
            ElseIf (e.CommandName = "MoveUp") Then 'Move Up
                Dim Sequence As Integer = CInt(e.CommandArgument)
                If (Sequence <> 0) Then
                    MoveUpDownQuestion(Sequence, "Up")
                End If
            ElseIf (e.CommandName = "MoveDown") Then 'Move Down
                Dim Sequence As Integer = CInt(e.CommandArgument)
                If (Sequence <> 0) Then
                    MoveUpDownQuestion(Sequence, "Down")
                End If
            End If
        End If
    End Sub
    Public Sub MoveUpDownQuestion(ByVal Sequence As String, ByVal Mode As String)
        Dim ds As New DataSet
        ds = CType(Session("ServiceQ"), DataSet)

        For Each drow As DataRow In ds.Tables(0).Rows
            If drow.Item("Sequence") = Sequence Then
                If (Mode = "Up") Then
                    drow.Item("RowNum") = drow.Item("RowNum") - 1
                    drow.Item("Sequence") = drow.Item("Sequence") - 1
                ElseIf (Mode = "Down") Then
                    drow.Item("RowNum") = drow.Item("RowNum") + 1
                    drow.Item("Sequence") = drow.Item("Sequence") + 1
                End If
            ElseIf drow.Item("Sequence") = Sequence - 1 Then
                If (Mode = "Up") Then
                    drow.Item("RowNum") = drow.Item("RowNum") + 1
                    drow.Item("Sequence") = drow.Item("Sequence") + 1
                End If
            ElseIf drow.Item("Sequence") = Sequence + 1 Then
                If (Mode = "Down") Then
                    drow.Item("RowNum") = drow.Item("RowNum") - 1
                    drow.Item("Sequence") = drow.Item("Sequence") - 1
                End If
            End If
        Next

        ds.Tables(0).DefaultView.Sort = "Sequence"

        ds.Tables(0).AcceptChanges()
        Session("ServiceQ") = ds

        PopulateServiceQ()
    End Sub


    Public Sub RemoveQuestionAndAnswer(ByVal QuestionId As String)
        Dim ds As New DataSet
        ds = CType(Session("ServiceQ"), DataSet)

        Dim i As Integer
        i = 0
        For Each drow As DataRow In ds.Tables(0).Rows
            If drow.Item("QuestionId") <> QuestionId Then
                i = i + 1
                drow.Item("RowNum") = i
                drow.Item("Sequence") = i
                drow.Item("MaxSequence") = drow.Item("MaxSequence") - 1
            End If
        Next
        ds.Tables(0).AcceptChanges()
        Session("ServiceQ") = ds

        Dim dtQuestion As DataTable = ds.Tables(0)
        Dim dvQuestion As New DataView
        dvQuestion = dtQuestion.Copy.DefaultView

        'add primary key as QuestionId 
        Dim keysQ(1) As DataColumn
        keysQ(0) = dtQuestion.Columns("QuestionId")

        Dim dtAnswer As DataTable = ds.Tables(1)
        Dim dvAnswer As New DataView
        dvAnswer = dtAnswer.Copy.DefaultView

        'add primary key as QuestionId 
        Dim keysA(1) As DataColumn
        keysA(0) = dtAnswer.Columns("QuestionId")

        Dim foundrowQ As DataRow() = dtQuestion.Select("QuestionId = '" & QuestionId & "'")
        If Not (foundrowQ Is Nothing) Then
            dtQuestion.Rows.Remove(foundrowQ(0))
            ds.Tables(0).AcceptChanges()
            Session("ServiceQ") = ds
        End If

        Dim foundrowAns As DataRow() = dtAnswer.Select("QuestionId = '" & QuestionId & "'")
        If Not (foundrowAns Is Nothing) Then
            i = 0
            For i = 0 To foundrowAns.Length - 1
                dtAnswer.Rows.Remove(foundrowAns(i))
                ds.Tables(1).AcceptChanges()
                Session("ServiceQ") = ds
            Next
        End If
        PopulateServiceQ()
    End Sub

    Protected Sub ddQAnsCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddQAnsCount As DropDownList = DirectCast(sender, DropDownList)
        UpdateServiceQA(rptServiceTab1Q)
        Dim ds As New DataSet
        ds = CType(Session("ServiceQ"), DataSet)
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim MaxAnswerId As Integer
        Dim totalAns As Integer = CInt(ddQAnsCount.SelectedValue)

        If ds.Tables.Count > 1 Then
            If ds.Tables(1).Rows.Count > 0 Then

                For Each drow As DataRow In ds.Tables(1).Rows
                    If drow.Item("QuestionId") = ddQAnsCount.TabIndex Then
                        i = i + 1
                        drow.Item("QuestionAnsCount") = totalAns
                        drow.Item("RowNum") = i
                    End If
                Next

                ds.AcceptChanges()
                Session("ServiceQ") = ds

                If (i > totalAns) Then
                    Dim rindex As Integer
                    Dim strindexToRemove As String = ""

                    For rindex = 0 To ds.Tables(1).Rows.Count - 1
                        If ds.Tables(1).Rows(rindex)("QuestionId") = ddQAnsCount.TabIndex Then
                            j = j + 1
                            If (totalAns < j) Then
                                If (strindexToRemove = "") Then
                                    strindexToRemove = rindex
                                Else
                                    strindexToRemove = strindexToRemove & ":" & rindex
                                End If
                            End If
                        End If
                    Next

                    If (strindexToRemove <> "") Then
                        j = 0
                        Dim IdsToRemove As String() = strindexToRemove.Split(":")
                        For Each AnsId As String In IdsToRemove
                            ds.Tables(1).Rows.RemoveAt(AnsId - j)
                            ds.Tables(1).AcceptChanges()
                            Session("ServiceQ") = ds
                            j = j + 1
                        Next
                    End If
                Else
                    For i = i + 1 To totalAns
                        For Each drow As DataRow In ds.Tables(1).Rows
                            MaxAnswerId = drow.Item("MaxAnswerId") + 1
                            drow.Item("MaxAnswerId") = MaxAnswerId
                        Next
                        ds.AcceptChanges()
                        Session("ServiceQ") = ds
                        AddAnswers(CInt(ddQAnsCount.TabIndex), MaxAnswerId, totalAns, i)
                    Next
                End If
            End If
        End If

        PopulateServiceQ()
    End Sub

    Public Sub UpdateServiceQA(ByVal rptClientQuestions As Repeater)
        Dim rptServiceAnswers As Repeater
        Dim hdnQuestionId As HtmlInputHidden
        Dim hdnAnswerId As HtmlInputHidden

        Dim ds As New DataSet
        ds = CType(Session("ServiceQ"), DataSet)

        Dim dvQuestions As New DataView
        dvQuestions = ds.Tables(0).DefaultView

        Dim dvAnswers As New DataView
        dvAnswers = ds.Tables(1).DefaultView

        If Not IsNothing(rptClientQuestions) Then
            For Each row As RepeaterItem In rptClientQuestions.Items
                hdnQuestionId = CType(row.FindControl("hdnQuestionId"), HtmlInputHidden)

                dvQuestions.RowFilter = "QuestionId = " & hdnQuestionId.Value

                If dvQuestions.Count > 0 Then
                    dvQuestions.Item(0).Item("Question") = CType(row.FindControl("txtServiceQ"), TextBox).Text
                    dvQuestions.Item(0).Item("IsMandatory") = CType(row.FindControl("chkIsMandatory"), CheckBox).Checked
                End If

                rptServiceAnswers = CType(row.FindControl("rptServiceQuestionAnswers"), Repeater)

                If Not IsNothing(rptServiceAnswers) Then
                    For Each rowAns As RepeaterItem In rptServiceAnswers.Items

                        hdnAnswerId = CType(rowAns.FindControl("hdnAnswerId"), HtmlInputHidden)
                        dvAnswers.RowFilter = "AnswerId = " & hdnAnswerId.Value
                        If dvAnswers.Count > 0 Then
                            dvAnswers.Item(0).Item("Answer") = CType(rowAns.FindControl("txtAnswer"), TextBox).Text
                            dvAnswers.Item(0).Item("AnswerMessage") = CType(rowAns.FindControl("txtAnswerMessage"), TextBox).Text.Replace(Chr(10), "<BR>")
                            dvAnswers.Item(0).Item("StopBooking") = CType(rowAns.FindControl("chkStopBooking"), CheckBox).Checked
                        End If
                        dvAnswers.RowFilter = ""
                        ds.Tables(1).AcceptChanges()
                        Session("ServiceQ") = ds
                    Next
                End If
                dvQuestions.RowFilter = ""
                ds.Tables(0).AcceptChanges()
                Session("ServiceQ") = ds
            Next
        End If
    End Sub


    Public Sub AddAnswers(ByVal QuestionId As Integer, ByVal AnswerId As Integer, ByVal QuestionAnsCount As Integer, ByVal RowNum As Integer)

        Dim ds As New DataSet
        ds = CType(Session("ServiceQ"), DataSet)

        Dim dt As New DataTable

        dt = ds.Tables(1)

        Dim drow As DataRow
        drow = dt.NewRow()

        drow("QuestionId") = QuestionId
        drow("Question") = ""
        drow("AnswerId") = AnswerId
        drow("Answer") = ""
        drow("AnswerMessage") = ""
        drow("StopBooking") = 0
        drow("QuestionAnsCount") = QuestionAnsCount
        drow("MaxAnswerId") = AnswerId
        drow("RowNum") = RowNum

        dt.Rows.Add(drow)
        dt.AcceptChanges()

        ds.AcceptChanges()

        Session("ServiceQ") = ds

    End Sub

    Private Sub lnkAddServiceTab1Q_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddServiceTab1Q.Click
        AddQuestion()
    End Sub

    Public Sub AddQuestion()

        UpdateServiceQA(rptServiceTab1Q)
        Dim ds As New DataSet
        ds = CType(Session("ServiceQ"), DataSet)

        Dim MaxQuestionId As Integer
        Dim MaxAnswerId As Integer
        Dim RowNum As Integer
        RowNum = 1

        For Each drowQ As DataRow In ds.Tables(0).Rows
            MaxQuestionId = drowQ.Item("MaxQuestionId") + 1
            drowQ.Item("MaxQuestionId") = MaxQuestionId
        Next

        ds.AcceptChanges()
        Session("ServiceQ") = ds


        Dim dv As New DataView
        If ds.Tables.Count > 1 Then
            If ds.Tables(0).Rows.Count > 0 Then
                dv = ds.Tables(0).DefaultView
                RowNum = RowNum + dv.ToTable.Rows.Count

                For Each drQ As DataRow In dv.Table.Rows
                    drQ.Item("MaxSequence") = RowNum
                Next

                ds.AcceptChanges()
                Session("ServiceQ") = ds

            End If
        End If

        Dim dt As New DataTable

        dt = ds.Tables(0)

        Dim drow As DataRow
        drow = dt.NewRow()

        drow("QuestionId") = MaxQuestionId
        drow("Question") = ""
        drow("IsMandatory") = 0
        drow("Selected") = "--"
        drow("MaxQuestionId") = MaxQuestionId
        drow("RowNum") = RowNum
        drow("Sequence") = RowNum
        drow("MaxSequence") = RowNum
        
        dt.Rows.Add(drow)
        dt.AcceptChanges()

        ds.AcceptChanges()

        Session("ServiceQ") = ds

        ds.AcceptChanges()
        Session("ServiceQ") = ds

        For Each drowA As DataRow In ds.Tables(1).Rows
            MaxAnswerId = drowA.Item("MaxAnswerId") + 1
            drowA.Item("MaxAnswerId") = MaxAnswerId
        Next

        ds.AcceptChanges()
        Session("ServiceQ") = ds

        AddAnswers(MaxQuestionId, MaxAnswerId, 1, 1)

        PopulateServiceQ()
    End Sub

    Protected Sub ResetTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetTop.Click, btnResetBottom.Click
        PopulateData()
    End Sub
    Protected Sub Save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click, btnSaveBottom.Click
        UpdateServiceQA(rptServiceTab1Q)

        If ValidatingQuestionsData() = "" Then
            Dim dsServiceQ As New XSDServiceQ
            Dim rptClientQuestions As Repeater
            Dim rptServiceAnswers As Repeater

            Dim hdnQuestionId As HtmlInputHidden
            Dim hdnAnswerId As HtmlInputHidden

            Dim QuestionId As Integer
            QuestionId = 0

            Dim ds As New DataSet
            ds = CType(Session("ServiceQ"), DataSet)

            Dim dvQuestions As New DataView
            dvQuestions = ds.Tables(0).DefaultView

            Dim dvAnswers As New DataView
            dvAnswers = ds.Tables(1).DefaultView

            rptClientQuestions = rptServiceTab1Q

            If Not IsNothing(rptClientQuestions) Then
                For Each row As RepeaterItem In rptClientQuestions.Items

                    hdnQuestionId = CType(row.FindControl("hdnQuestionId"), HtmlInputHidden)

                    QuestionId = QuestionId + 1

                    dvQuestions.RowFilter = "QuestionId = " & hdnQuestionId.Value

                    If dvQuestions.Count > 0 Then
                        Dim nrow As XSDServiceQ.KnowhowThermostatQRow = dsServiceQ.KnowhowThermostatQ.NewRow
                        With nrow
                            .QuestionID = QuestionId
                            .CompanyID = CInt(Request("companyid"))
                            .ProductID = CInt(ViewState("ProductID"))
                            .Question = CType(row.FindControl("txtServiceQ"), TextBox).Text.Trim
                            .IsMandatory = CType(row.FindControl("chkIsMandatory"), CheckBox).Checked
                            .Sequence = CType(row.FindControl("hdnSequence"), HtmlInputHidden).Value
                        End With
                        dsServiceQ.KnowhowThermostatQ.Rows.Add(nrow)
                    End If

                    rptServiceAnswers = CType(row.FindControl("rptServiceQuestionAnswers"), Repeater)

                    If Not IsNothing(rptServiceAnswers) Then
                        For Each rowAns As RepeaterItem In rptServiceAnswers.Items
                            hdnAnswerId = CType(rowAns.FindControl("hdnAnswerId"), HtmlInputHidden)
                            dvAnswers.RowFilter = "AnswerId = " & hdnAnswerId.Value
                            If dvAnswers.Count > 0 Then
                                Dim nrowAnswer As XSDServiceQ.KnowhowThermostatAnswersRow = dsServiceQ.KnowhowThermostatAnswers.NewRow
                                With nrowAnswer
                                    .AnswerID = hdnAnswerId.Value
                                    .QuestionID = QuestionId
                                    .CompanyId = CInt(Request("companyid"))
                                    .ProductID = CInt(ViewState("ProductID"))
                                    .Answer = CType(rowAns.FindControl("txtAnswer"), TextBox).Text.Trim
                                    .Message = CType(rowAns.FindControl("txtAnswerMessage"), TextBox).Text.Trim.Replace(Chr(10), "<BR>")
                                    .StopBooking = CType(rowAns.FindControl("chkStopBooking"), CheckBox).Checked
                                End With
                                dsServiceQ.KnowhowThermostatAnswers.Rows.Add(nrowAnswer)
                            End If
                            dvAnswers.RowFilter = ""
                        Next
                    End If
                    dvQuestions.RowFilter = ""
                Next
            End If
            Dim xmlContent As String = dsServiceQ.GetXml
            xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)

            Dim dsSuccess As New DataSet
            dsSuccess = ws.WSWorkOrder.AddEditServiceQA(xmlContent, QuestionId, CInt(Request("companyid")), CInt(ViewState("ProductID")), Session("UserID"))
            If dsSuccess.Tables.Count > 0 Then
                If dsSuccess.Tables(0).Rows.Count > 0 Then
                    If CStr(dsSuccess.Tables(0).Rows(0)(0)) = "1" Then
                        Response.Redirect("AdminProductSpecific.aspx?mode=edit&productid=" & ViewState("ProductID") & "&pagename=CompanyProfile&CompanyID=" & Request("companyid") & "&bizDivId=1")
                    End If
                End If
            End If
        End If
    End Sub
    Private Function ValidatingQuestionsData() As String

        Dim errmsg As String = ""
        Dim flagPerQ As Boolean = True

        errmsg = ValidatingEachQuestionType(rptServiceTab1Q, flagPerQ, errmsg)
        If (errmsg <> "") Then
            flagPerQ = False
        End If
        If errmsg <> "" Then
            divValidationServiceQuestions.Visible = True
            lblValidationServiceQuestions.Text = errmsg.ToString

            PopulateServiceQ()

        Else
            divValidationServiceQuestions.Visible = False
            lblValidationServiceQuestions.Text = ""
        End If
        Return errmsg
    End Function
    Private Function ValidatingEachQuestionType(ByVal rptService As Repeater, ByVal flagPerQ As Boolean, ByVal errmsg As String) As String

        Dim errmsgPerQ As String = ""
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim hdnQuestionType As HtmlInputHidden

        If Not IsNothing(rptService) Then
            For Each row As RepeaterItem In rptService.Items
                Dim rptServiceQuestionAnswers As Repeater = CType(row.FindControl("rptServiceQuestionAnswers"), Repeater)
                i = i + 1
                j = 0
                errmsgPerQ = ""
                If CType(row.FindControl("txtServiceQ"), TextBox).Text.Trim.Length = 0 Then
                    If flagPerQ = True Then
                        errmsgPerQ = "- Please enter question to question " & i
                        flagPerQ = False
                    Else
                        errmsgPerQ = errmsgPerQ & "<br>- Please enter question to question " & i
                    End If
                End If
               If Not IsNothing(rptServiceQuestionAnswers) Then
                    For Each rowAns As RepeaterItem In rptServiceQuestionAnswers.Items
                        j = j + 1
                        If (rptServiceQuestionAnswers.Items.Count > 1) Then
                            If CType(rowAns.FindControl("txtAnswer"), TextBox).Text.Trim.Length = 0 Then
                                If flagPerQ = True Then
                                    errmsgPerQ = "- Please enter an answer to question " & i & " Answer " & j
                                    flagPerQ = False
                                Else
                                    errmsgPerQ = errmsgPerQ & "<br>- Please enter an answer to question " & i & " Answer " & j
                                End If
                            End If
                        End If
                    Next
                End If
                errmsg = errmsg & errmsgPerQ
            Next
        End If
        Return errmsg
    End Function


End Class