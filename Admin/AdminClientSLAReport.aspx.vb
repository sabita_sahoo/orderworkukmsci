Partial Public Class AdminClientSLAReport
    Inherits System.Web.UI.Page

    Public Shared ws As New WSObjs
    Protected WithEvents UCDateRange1 As UCDateRange

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Security.SecurePage(Page, True)
            PopulateParameters()
            PopulateData()
        End If

    End Sub

    Public Sub PopulateParameters()

        If hdnStartDate.Value = "" And hdnToDate.Value = "" Then
            Dim currDate As DateTime = System.DateTime.Today
            UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            UCDateRange1.txtToDate.Text = Strings.FormatDateTime(Date.Today, DateFormat.ShortDate)

            hdnStartDate.Value = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            hdnToDate.Value = Strings.FormatDateTime(Date.Today, DateFormat.ShortDate)
        End If

    End Sub

    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkView.Click
        hdnStartDate.Value = UCDateRange1.txtFromDate.Text
        hdnToDate.Value = UCDateRange1.txtToDate.Text
        PopulateData()
    End Sub
    Public Sub PopulateData()
        Dim ds As DataSet = ws.WSWorkOrder.GetClientSLAReport(7677, hdnStartDate.Value.ToString, hdnToDate.Value.ToString, "")
        If ds.Tables("tblClientSLAReport").DefaultView.Count > 0 Then
            If (ds.Tables("tblClientSLAReport").Rows(0).Item("SubmittedWO") <> "0") Then
                TdExpToExc.Visible = True
                TdPrint.Visible = True
                divMainReport.Visible = True
                tblNoRecords.Visible = False
                lblSubmittedWO.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("SubmittedWO")
                lblClosedWO.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("ClosedWO")
                lblOnTime.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("OnTime")
                lblSLAPercentageMet.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("SLAPercentageMet")
                lblFirstTimeSuccess.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("FirstTimeSuccess")
                lblCancellationNo.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("CancellationNumber")

                'Product
                If ds.Tables("tblProductSLAReport").DefaultView.Count > 0 Then
                    divProduct.Visible = True
                    DLProducts.DataSource() = ds.Tables("tblProductSLAReport")
                    DLProducts.DataBind()
                Else
                    divProduct.Visible = False
                End If

                'Billing Location
                If ds.Tables("tblBillingLocSLAReport").DefaultView.Count > 0 Then
                    divBillingLoc.Visible = True
                    DLBillingLoc.DataSource() = ds.Tables("tblBillingLocSLAReport")
                    DLBillingLoc.DataBind()
                Else
                    divBillingLoc.Visible = False
                End If

            Else
                TdExpToExc.Visible = False
                TdPrint.Visible = False
                divMainReport.Visible = False
                tblNoRecords.Visible = True
            End If

        Else
            TdExpToExc.Visible = False
            TdPrint.Visible = False
            divMainReport.Visible = False
            tblNoRecords.Visible = True
        End If
        btnPrint.HRef = "~/AdminClientSLAReportPrint.aspx?StartDate=" & hdnStartDate.Value & "&EndDate=" & hdnToDate.Value
        btnExport.HRef = "~/ExportToExcel.aspx?page=SLAReport&FDate=" & hdnStartDate.Value & "&TDate=" & hdnToDate.Value
    End Sub

End Class