﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ServiceListing.aspx.vb" Inherits="Admin.ServiceListing"  Title="OrderWork: Service Listing" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        #myModal
        {
            padding-left: 0 !important;
            }
            .modal-dialog
            {
                width: 98% !important;
                }
    </style>
</head>
<body onload="openPopup();">
    <form id="form1" runat="server">
    <div>

    <script language="javascript" type="text/javascript" src="JS/jquery1.12.js"></script> 
   <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
   <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_window();return false;" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
           <iframe id="iFrameServiceListing" runat="server" style="width:100%;height:700px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" ></iframe>           
      </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->    
<script type="text/javascript">
    var jq = jQuery.noConflict();
    function openPopup() {
        jq('#myModal').modal({ backdrop: 'static', keyboard: false })
        
    }
    function close_window() {
        close();
    }
    jq(document).ready(function () {
        var winHeight = window.innerHeight - 140;
        jq("#iFrameServiceListing").css("height", winHeight + "px");
    });
    </script>
    </div>
    </form>
</body>
</html>
