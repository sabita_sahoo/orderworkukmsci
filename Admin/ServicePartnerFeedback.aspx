<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="Supplier Feedback" CodeBehind="ServicePartnerFeedback.aspx.vb" Inherits="Admin.ServicePartnerFeedback" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


	        
			<asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server">
			<ContentTemplate>
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<input id="hdnPageSize" type="hidden" name="hdnPageSize" runat="server"> 
			<input id="hdnPageNo" type="hidden" name="hdnPageNo" runat="server">
			<input id="sortExpr" type="hidden" name="sortExpr" runat="server"> 
			<input id="sortOrder" type="hidden" name="sortOrder" runat="server">
			<input id="totalRecs" type="hidden" name="totalRecs" runat="server"> 
			<input id="pageRecs" type="hidden" name="pageRecs" runat="server">
			<input id="defaultSortImgId" type="hidden" name="defaultSortImgId" runat="server"> 			
			<input type="hidden" id="hdnDateID" name="hdnDateID" runat="server">
			<input type="hidden" id="hdnDate" name="hdnDate" runat="server">
			<input type="hidden" id="hdntxt" name="hdntxt" runat="server">
			<input type="hidden" id="hdnIsDeleted" name="hdnIsDeleted" runat="server">			
			           
             <table width="100%"><tr><td width="10">&nbsp;</td><td>
             <asp:Label ID="lblErr" CssClass="bodytxtValidationMsg" runat="server" Text=""></asp:Label>
             <asp:Panel ID="pnlListing" runat="server" style="display:block;">  
             <table style="margin-left:20px;">
        <tr>
            <td colspan="3">
                <div id="div2" style="width:100%;margin-top:20px;">
			            <asp:ValidationSummary id="validationSummarySearch" runat="server" DisplayMode="List" ValidationGroup="ValidationTop" CssClass="bodytxtValidationMsg"></asp:ValidationSummary>
			    </div>
           </td>
        </tr>
        <tr>
            <td>
                      <div id="divForAdvanceSearch" runat="server" visible="true">
			            <div id="divrow2" style="width:100%;margin-top:20px;">
			                <div class="formTxt" style="width:120px; float:left; margin-right:10px;">
			                    Feedback Status
                               <asp:DropDownList id="ddlStatus" tabIndex="1" runat="server" CssClass="formField105">
                                          <asp:ListItem Text="Unprocessed" Value="0"></asp:ListItem>
                                          <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                          <asp:ListItem Text="Processed " Value="1"></asp:ListItem>                                          
                               </asp:DropDownList>
			                </div>    
			                <div class="formTxt" style="width:120px; float:left; text-align:left; margin-right:10px;">
			                    Feedback Type
			                   <asp:DropDownList id="ddlType" tabIndex="2" runat="server" CssClass="formField105"></asp:DropDownList>
			                </div>
                             <div class="formTxt" style="width:120px; float:left; text-align:left; margin-right:10px;">
			                    Feedback Ref.No.
			                  <asp:TextBox ID="txtComplaintRefNo" TabIndex="3" runat="server" CssClass="formField105"></asp:TextBox>
			                </div>   	   		               
			                <div class="formTxt" style="width:150px; float:left; ">
			                     Created Month
                            <asp:DropDownList CssClass="formFieldGrey121" ID="ddMonth" TabIndex="4" runat="server">
										<asp:ListItem Text="January" value="1" ></asp:ListItem>
												  <asp:ListItem Text="February" value="2"></asp:ListItem>
												  <asp:ListItem Text= "March" value="3"></asp:ListItem>
												  <asp:ListItem Text= "April" value="4"></asp:ListItem>
												  <asp:ListItem Text= "May" value="5"></asp:ListItem>
												  <asp:ListItem Text= "June" value="6"></asp:ListItem>
												  <asp:ListItem Text= "July" value="7"></asp:ListItem>
												  <asp:ListItem Text= "August" value="8"></asp:ListItem>
												  <asp:ListItem Text= "September" value="9"></asp:ListItem>
												  <asp:ListItem Text= "October" value="10"></asp:ListItem>
												  <asp:ListItem Text= "November" value="11"></asp:ListItem>
												  <asp:ListItem Text= "December" value="12"></asp:ListItem>
										</asp:DropDownList>
			                <%--<asp:TextBox ID="txtDateCreatedFrom" TabIndex="4" runat="server" CssClass="formField105"></asp:TextBox>
			                   <img alt="Click to Select" src="Images/calendar.gif" id="btnDateCreatedFrom" style="cursor:pointer; vertical-align:top;" />		    	
                               <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnDateCreatedFrom TargetControlID="txtDateCreatedFrom" ID="calDateCreatedFrom" runat="server"></cc1:CalendarExtender>
			                   <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regDateCreatedFrom" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateCreatedFrom" ErrorMessage="Created From Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
                               <asp:CustomValidator ID="rngDateCreated" CssClass="txtOrange" runat="server" ErrorMessage="Created To Date should be greater than Created From Date"  ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateCreatedTo">*</asp:CustomValidator>--%>
			                </div>
                            <div class="formTxt" style="width:150px; float:left; ">
                                Created Year
                             <asp:DropDownList CssClass="formFieldGrey121" ID="ddYear" runat="server" TabIndex="5">
											  <asp:ListItem Text="2006" value="2006"></asp:ListItem>
											  <asp:ListItem Text="2007" value="2007"></asp:ListItem>
											  <asp:ListItem Text="2008" value="2008"></asp:ListItem>
											  <asp:ListItem Text="2009" value="2009"></asp:ListItem>
											  <asp:ListItem Text="2010" value="2010"></asp:ListItem>
											  <asp:ListItem Text="2011" value="2011"></asp:ListItem>
											  <asp:ListItem Text="2012" value="2012"></asp:ListItem>
                                              <asp:ListItem Text="2013" value="2013"></asp:ListItem>
                                              <asp:ListItem Text="2014" value="2014"></asp:ListItem>
                                              <asp:ListItem Text="2015" value="2015"></asp:ListItem>
                                              <%--Poonam modified on 26/10/2016 - Task - OA-350 : OA - Fix Dixons Complaints Page--%>
                                              <asp:ListItem Text="2016" value="2016"></asp:ListItem>
                                              <asp:ListItem Text="2017" value="2017"></asp:ListItem>
                                              <asp:ListItem Text="2018" value="2018"></asp:ListItem>
                                              <asp:ListItem Text="2019" value="2019"></asp:ListItem>
                                              <asp:ListItem Text="2020" value="2020"></asp:ListItem>
											  </asp:DropDownList>
                             <%--<asp:TextBox ID="txtDateCreatedTo"  runat="server" CssClass="formField105"></asp:TextBox>
                                 <img alt="Click to Select" src="Images/calendar.gif" id="btnDateCreatedTo" style="cursor:pointer; vertical-align:top;" />		    	
                                 <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnDateCreatedTo TargetControlID="txtDateCreatedTo" ID="calDateCreatedTo" runat="server"></cc1:CalendarExtender>
                                 <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regDateCreatedTo" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateCreatedTo" ErrorMessage="Created To Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>--%>
                            </div>
                             <%--<div class="formTxt" style="width:150px; float:left; ">
			                     Processed From Date
			                <asp:TextBox ID="txtDateProcessedFrom" TabIndex="5" runat="server" CssClass="formField105"></asp:TextBox>
			                   <img alt="Click to Select" src="Images/calendar.gif" id="btnDateProcessedFrom" style="cursor:pointer; vertical-align:top;" />		    	
                               <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnDateProcessedFrom TargetControlID="txtDateProcessedFrom" ID="calDateProcessedFrom" runat="server"></cc1:CalendarExtender>
			                   <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regDateProcessedFrom" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateProcessedFrom" ErrorMessage="Processed From Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
                               <asp:CustomValidator ID="rngDateProcessed" runat="server" CssClass="txtOrange" ErrorMessage="Processed To Date should be greater than Processed From Date" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateProcessedTo">*</asp:CustomValidator>
			                </div>--%>
                             <%--<div class="formTxt" style="width:150px; float:left; ">
			                     Processed To Date
			                <asp:TextBox ID="txtDateProcessedTo" TabIndex="5" runat="server" CssClass="formField105"></asp:TextBox>
			                   <img alt="Click to Select" src="Images/calendar.gif" id="btnDateProcessedTo" style="cursor:pointer; vertical-align:top;" />		    	
                               <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnDateProcessedTo TargetControlID="txtDateProcessedTo" ID="calDateProcessedTo" runat="server"></cc1:CalendarExtender>
			                   <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regDateProcessedTo" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateProcessedTo" ErrorMessage="Processed To Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
			                </div>--%>
			            </div>
			        </div>
            
            </td>
                <td align="left" width="60px" valign="bottom">
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                        <asp:LinkButton ID="lnkBtnView" causesValidation=True runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
                    </div>
                </td>

                 <td runat="server" id="tdExport" valign="bottom" visible="false">
                         <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:80px;">
                            <a runat="server" style="cursor:pointer" target="_blank" id="btnExport" class="txtButtonRed" > &nbsp;Export to Excel&nbsp;</a>
                        </div>
                 </td>
            </tr>
      
      </table>           
             <table width="400px" cellpadding="0" cellspacing="0" border="0">
					  	<tr >
					  	    <td width="15">&nbsp;</td>
							<td height="24" align="left"><asp:Label ID="lblMsg" runat="server" CssClass="bodytxtValidationMsg"></asp:Label></td>
											  <td align="left"><%--<asp:DropDownList CssClass="formFieldGrey121" ID="ddMonth" runat="server">
										<asp:ListItem Text="January" value="1"></asp:ListItem>
												  <asp:ListItem Text="February" value="2"></asp:ListItem>
												  <asp:ListItem Text= "March" value="3"></asp:ListItem>
												  <asp:ListItem Text= "April" value="4"></asp:ListItem>
												  <asp:ListItem Text= "May" value="5"></asp:ListItem>
												  <asp:ListItem Text= "June" value="6"></asp:ListItem>
												  <asp:ListItem Text= "July" value="7"></asp:ListItem>
												  <asp:ListItem Text= "August" value="8"></asp:ListItem>
												  <asp:ListItem Text= "September" value="9"></asp:ListItem>
												  <asp:ListItem Text= "October" value="10"></asp:ListItem>
												  <asp:ListItem Text= "November" value="11"></asp:ListItem>
												  <asp:ListItem Text= "December" value="12"></asp:ListItem>
										</asp:DropDownList>--%>
                                        
											  </td>
											  <td height="24" align="left" width="10">&nbsp;</td>
											  <td align="left">
											  </td>
							<td width="20"></td>
                             
							<td align="left">
								
							 </td>		 

						</tr>
					</table>						
                 <asp:GridView ID="gvFeedback" runat="server" AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
               PagerSettings-Mode="NextPreviousFirstLast" CssClass="gridrow"  PagerSettings-Position="TopAndBottom" Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               <PagerTemplate>                  
                  
                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>							
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
                                  <td runat="server" id="tdsetType1" width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">

                                            <TR>

                                                <TD class="txtListing" align="left"><strong>

                                                  <asp:CheckBox id="chkSelectAll" runat="server" onclick="CheckAll(this,'Check')" value="checkbox" Text="Select all" valign="middle"></asp:CheckBox>

                                                </strong> </TD>

                                                <TD width="5"></TD>

                                            </TR>

                                          </TABLE>                                  

                                          </td>
                                          <td runat="server"  id="tdProcessed" width="70" align="left" class="txtWelcome paddingL10"><div id="divButton" style="width: 90px;  ">

                                                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                                           

                                                <asp:LinkButton OnClick="ProcessedComplaint" class="buttonText" style="cursor:hand;" id="btnProcessed"  runat=server><strong>Processed</strong></asp:LinkButton>

                               <cc1:ConfirmButtonExtender ID="ConfirmBtnProcessed" TargetControlID="btnProcessed" ConfirmText="Do you want to mark these Feedbacks as processed?" runat="server">

                                </cc1:ConfirmButtonExtender> 

                                

                                                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>

                                          </div></td> 
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
                 </PagerTemplate>
                  <Columns>   
                  <asp:TemplateField>               

               <HeaderStyle CssClass="gridHdr" />

               <ItemStyle Wrap=true HorizontalAlign=Left Width="10px"  CssClass="gridRow"/>

               <ItemTemplate>             

               <asp:CheckBox ID="Check" runat="server" ></asp:CheckBox>

                <input type=hidden runat=server id="hdnComplaintId"  value='<%#Container.DataItem("ComplaintId")%>'/>

               </ItemTemplate>

               </asp:TemplateField>                                                    
                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="5%" HeaderStyle-CssClass="gridHdr"  DataField="ComplaintRefNo" SortExpression="ComplaintRefNo" HeaderText="Feedback Ref.No." />            
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="10%"  HeaderStyle-CssClass="gridHdr"  DataField="DateCreated" SortExpression="DateCreated" HeaderText="Posting Date" />            
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="10%"  HeaderStyle-CssClass="gridHdr"  DataField="PostedBy" SortExpression="PostedBy" HeaderText="Poster" />
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="12%"  HeaderStyle-CssClass="gridHdr"  DataField="CompanyName" SortExpression="CompanyName" HeaderText="Supplier Company" />
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="15%"  HeaderStyle-CssClass="gridHdr"  DataField="ProcessedBy" SortExpression="ProcessedBy" HeaderText="ProcessedBy" />
                    <asp:TemplateField HeaderText="IsProcessed" HeaderStyle-CssClass= "gridHdr gridText" SortExpression="IsProcessed"  >         
                  <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" Width="5%"/>
                    <ItemTemplate>             
                   <asp:Label ID="lblIsProcessed" runat="server" Text='<%# IIF(Container.DataItem("IsProcessed"), True,False)%>'>></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField> 
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="25%"  HeaderStyle-CssClass="gridHdr"  DataField="Complaint" SortExpression="Complaint" HeaderText="Feedback" HtmlEncode="False" />            
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="10%"  HeaderStyle-CssClass="gridHdr"  DataField="ComplaintType" SortExpression="ComplaintType" HeaderText="Feedback Type" HtmlEncode="False" />            
                    
                     <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">                
                        <ItemStyle CssClass="gridRowIcons" Wrap=true  Width="5%"/>
                        <ItemTemplate>
                        
                        &nbsp;<a href='mailto:<%#Container.DataItem("Email")%>' ><img src="../Images/Icons/Sendmail.gif" alt="Send Mail" width="13" height="10" border="0" align="middle" /></a>
               	        
				        </ItemTemplate>  
                     </asp:TemplateField>                                             
                </Columns>
                  <AlternatingRowStyle  CssClass="gridRow" />
                 
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
                 </asp:GridView>
             <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.ServicePartnerFeedback" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
            </asp:Panel>
            				      
                      
            
			
          				</td><td width="10">&nbsp;</td></tr></table>			 
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
         </ContentTemplate>
			</asp:UpdatePanel>
           
			 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelCreateWorkOrder" runat="server">
					<ProgressTemplate>
						<div>
							<img  align=middle src="Images/indicator.gif" />
							Please Wait...
						</div>      
					</ProgressTemplate>
			</asp:UpdateProgress>
			<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePanelCreateWorkOrder" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
     
     
			<cc1:ModalPopupExtender ID="mdlComplaintType" runat="server" TargetControlID="btnTemp" PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
            </cc1:ModalPopupExtender>
              <asp:Panel runat="server" ID="pnlConfirm" Width="300px" CssClass="pnlConfirm" style="display:none;">
                <div id="divModalParent">
                    <span style="font-family:Tahoma; font-size:14px;"><b>Feedback Type</b></span><br /><br />
                    <asp:DropDownList id="ddComplaintType" tabIndex="1" runat="server" CssClass="bodyTextGreyLeftAligned" Width="280"></asp:DropDownList>      
                    <br /><br />
                        <div id="divButton" class="divButton" style="width: 85px; float:left;cursor:pointer;">
                            <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                                <a class="buttonText cursorHand" href='javascript:validate_required("ctl00_ContentPlaceHolder1_ddComplaintType")' id="ancSubmit"><strong>Submit</strong></a>
                            <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                        </div>
                        <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;cursor:pointer;">
                            <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                                <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                            <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                        </div> 
                </div>								
            </asp:Panel>
             <asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
             <asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
     <script type="text/javascript">
         function validate_required(field) {
             // alert(document.getElementById(field).options[document.getElementById(field).selectedIndex].value);
             if (document.getElementById(field).options[document.getElementById(field).selectedIndex].value == 0) {

                 alert("Please select Feedback Type")
             }
             else {
                 document.getElementById("ctl00_ContentPlaceHolder1_hdnButton").click();
             }
         }
    
</script>
      </asp:Content>