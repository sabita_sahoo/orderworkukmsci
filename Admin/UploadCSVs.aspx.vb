Imports System
Imports System.Data
Imports System.Math
Imports Microsoft.SqlServer.Dts.Runtime
Partial Public Class UploadCSVs
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then
            tblPostUpload.Visible = False
            tblExecuteBtn.Visible = False
            tblUpload.Visible = True
            trType.Visible = True
            UCSearchContact1.BizDivID = Session("BizDivId")
            UCSearchContact1.ClassId = 0
            UCSearchContact1.Filter = "WebServiceAccess"
        End If
    End Sub

    Public Sub lnkbtnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnUpload.Click
        If (ddType.SelectedValue <> "Select") Then
            If (FileUpload1.HasFile) Then
                lblMsg.Text = ""
                Dim FilePath = ApplicationSettings.JohnLewisCSVData
                Dim FileName As String

                If (ddType.SelectedValue = "CSV") Then
                    FileName = "CS110124(1).T230001"
                Else
                    FileName = "Accepted.xls"
                End If

                FileUpload1.SaveAs(FilePath + FileName)
                tblExecuteBtn.Visible = True
                trUpload.Visible = False
                trType.Visible = False
            Else
                lblMsg.Text = "Please select file to upload"
                tblExecuteBtn.Visible = False
                trUpload.Visible = True
                trType.Visible = True
            End If
        Else
            lblMsg.Text = "Please Select File Type"
        End If

    End Sub

    Public Sub btnExecute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExecute.Click
        divLoading.Style.Add("display", "inline")
        Dim ds As DataSet
        ds = ws.WSWorkOrder.ExecuteJohnLewisJob(ddType.SelectedValue.ToString)
        If (ds.Tables(0).Rows.Count > 0) Then
            divLoading.Style.Add("display", "none")
            If (ds.Tables(0).Rows(0)("Status") = 0) Then
                lblMsg.Text = ""
                CsvValidRecords.HRef = ApplicationSettings.AttachmentDisplayPath(0) & "Attachments/OWDocs/ValidRecordsList.xls"
                CsvInValidRecords.HRef = ApplicationSettings.AttachmentDisplayPath(0) & "Attachments/OWDocs/InValidRecordsList.xls"
                tblPostUpload.Visible = True
                tblUpload.Visible = False
            Else
                lblMsg.Text = "Execution Fail"
            End If
        Else
            divLoading.Visible = False
            lblMsg.Text = "Execution Fail"
        End If

    End Sub

    Public Sub lnkbtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnBack.Click
        tblPostUpload.Visible = False
        tblUpload.Visible = True
        tblExecuteBtn.Visible = False
        trType.Visible = True
        trUpload.Visible = False
        divLoading.Style.Add("display", "none")
        ancCSVSample.Visible = False
        ancExcelSample.Visible = False
        ddType.SelectedIndex = 0
    End Sub

    Protected Sub ddType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddType.SelectedIndexChanged
        If (ddType.SelectedValue <> "Select") Then
            lblType.Text = ddType.SelectedValue
            trUpload.Visible = True
            If (ddType.SelectedValue = "CSV") Then
                ancCSVSample.HRef = ApplicationSettings.AttachmentDisplayPath(0) & "Attachments/OWDocs/CS110124(1).T230001"
                ancCSVSample.Visible = True
                ancExcelSample.Visible = False
            Else
                ancExcelSample.HRef = ApplicationSettings.AttachmentDisplayPath(0) & "Attachments/OWDocs/Accepted.xls"
                ancExcelSample.Visible = True
                ancCSVSample.Visible = False
            End If
        Else
            tblPostUpload.Visible = False
            tblUpload.Visible = True
            tblExecuteBtn.Visible = False
            trType.Visible = True
            trUpload.Visible = False
            divLoading.Style.Add("display", "none")
            ancCSVSample.Visible = False
            ancExcelSample.Visible = False
        End If
    End Sub
End Class