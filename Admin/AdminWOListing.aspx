<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master"
    CodeBehind="AdminWOListing.aspx.vb" Inherits="Admin.AdminWOListing" Title="AdminWOListing" %>

<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="WebLibrary" Namespace="WebLibrary" TagPrefix="lib" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Timer ID="Timer1" Enabled="false" Interval='<%# admin.Applicationsettings.GetRefreshInterval%>'
        runat="server" OnTick="Timer1_Tick">
    </asp:Timer>
    <style>
        .divStyleCompany
        {
            border: 1px solid #999999;
            float: left;
            margin-bottom: 10px;
            max-width: 550px;
            min-width: 400px;
            padding-top: 10px;
            z-index: 1005;
        }
        
        #divMenu
        {
            font-family: Geneva, Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000000;
            line-height: 12px;
            font-weight: normal;
            text-decoration: none;
            border-top-width: 1px;
            border-top-style: solid;
            border-top-color: #CECECE;
        }
        
        .divContentTopRed
        {
            width: 100%;
            background-color: #993366;
            background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
            height: 5px;
        }
    </style>
    <script type="text/javascript">
        function SelectAutoSuggestCompany(inp, data) {
            inp.value = data[0];
            var x = document.getElementById("ctl00_ContentPlaceHolder1_hdnSelectedCompanyName").value = inp.value;
            document.getElementById("ctl00_ContentPlaceHolder1_hdnbtnSelectedCompany").click();
        }
    </script>
    <div id="divContent">
        <asp:Button runat="server" ID="hdnbtnSelectedCompany" CausesValidation="false" OnClick="hdnbtnSelectedCompany_Click"
            Height="0" Width="0" BorderWidth="0" Style="visibility: hidden;" />
        <div class="roundtopWhite">
            <img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner"
                style="display: none" /></div>
        <%--Poonam modified on 2/8/2016 - Task -OA-311 : EA - Multiple search company in listing does not work in Chrome--%>
        <asp:Panel runat="server" DefaultButton="lnkView">
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
                    <tr>
                        <td valign="top">
                            <!-- InstanceBeginEditable name="EdtContent" -->
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10px">
                                    </td>
                                    <td>
                                        <asp:Label ID="lblHeading" CssClass="HeadingRed" runat="server"></asp:Label>
                                    </td>
                                    <td bgcolor="#F4F5F0">
                                        <asp:Label runat="server" ID="lblMultipleWOsMsg" Visible="false" CssClass="formTxtRed"></asp:Label>
                                    </td>
                                </tr>
                                <tr height="10px">
                                    <td colspan="3">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="10px">
                                    </td>
                                    <td colspan="2">
                                        <div id="divButton" style="width: 115px; height: 26px;" runat="server">
                                            <div class="bottonTopGrey">
                                                <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                                    height="4" class="btnCorner" style="display: none" /></div>
                                            <a class="buttonText" runat="server" id="lnkBackToListing">
                                                <img src="Images/Arrows/Arrow-Back.gif" title="Back to Listing" width="4" height="8"
                                                    hspace="0" vspace="0" border="0" class="paddingL10R8"><strong>Back to Listing</strong></a>
                                            <div class="bottonBottomGrey">
                                                <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                                    height="4" class="btnCorner" style="display: none" /></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr height="10px">
                                    <td colspan="3">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" id="tblAllActionButtons"
                                runat="server">
                                <tr id="trFilterTypleLabel" runat="server">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="padding-left: 7px;">
                                        <strong>
                                            <asp:Label ID="lblDateFilterType" runat="server"></asp:Label></strong>
                                    </td>
                                    <td colspan="6" id="tdRadioButtonCompany" runat="server">
                                        <asp:RadioButton GroupName="SelectedCompany" ID="rdBtnSingleCompany" OnCheckedChanged="rdbtnCompanySel_CheckedChanged"
                                            AutoPostBack="true" runat="server" CausesValidation="false" Checked="true" Text="Single Company" />
                                        <asp:RadioButton GroupName="SelectedCompany" ID="rdBtnMultiCompany" OnCheckedChanged="rdbtnCompanySel_CheckedChanged"
                                            AutoPostBack="true" runat="server" CausesValidation="false" Text="Multi Company" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="10px" align="left">
                                    </td>
                                    <td id="tdDateRange" runat="server" width="285px;" style="padding-right: 5px; padding-top: 7px;"
                                        align="left">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    <%@ register tagprefix="uc1" tagname="UCDateRange" src="~/UserControls/UK/UCDateRangeUK.ascx" %>
                                                    <uc1:UCDateRange ID="UCDateRange1" runat="server"></uc1:UCDateRange>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="left" width="80px;" style="padding-right: 10px;">
                                        <%@ register tagprefix="uc1" tagname="UCSearchContact" src="~/UserControls/Admin/UCSearchContact.ascx" %>
                                        <uc1:UCSearchContact ID="UCSearchContact1" runat="server"></uc1:UCSearchContact>
                                        <input id="hdnSelectedCompanyName" type="hidden" name="hdnSelectedCompanyName" runat="server"
                                            value="" />
                                        <lib:Input ID="txtCompanyName" runat="server" DataType="List" Method="GetMultiCompanyName"
                                            CssClass="formField150" OnSelect='SelectAutoSuggestCompany' SelectParameters='CompanyName' />
                                        <cc2:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtCompanyName"
                                            WatermarkText="Select Contact" WatermarkCssClass="formField150">
                                        </cc2:TextBoxWatermarkExtender>
                                    </td>
                                    <td align="left" width="80px;" style="padding-right: 10px;">
                                        <lib:Input ID="txtWorkorderID" runat="server" DataType="List" Method="GetWorkOrderID"
                                            CssClass="formField150" />
                                        <cc2:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtWorkorderID"
                                            WatermarkText="Work Order ID" WatermarkCssClass="formField150">
                                        </cc2:TextBoxWatermarkExtender>
                                    </td>
                                      <td align="left" width="80px;" style="padding-right: 10px;">
                                        <asp:TextBox ID="txtCustomerMobile" TextMode ="Phone" MaxLength = "12"  Visible="true" runat="server" 
                                        CssClass="formFieldGrey150" EnableViewState="True">
                                        </asp:TextBox>

                                           <cc2:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtCustomerMobile"
                                            WatermarkText="Customer Mobile" WatermarkCssClass="formField150">
                                        </cc2:TextBoxWatermarkExtender>
                                    </td>
                                    <td align="left" width="80px;" style="padding-right: 10px;">
                                        <asp:DropDownList ID="ddlBillLoc" Visible="true" runat="server" CssClass="formFieldGrey150"
                                            DataTextField="BillingLocation" DataValueField="BillingLocID" EnableViewState="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td id="tdViewBtn" runat="server" width="50px;" align="left" style="padding-right: 10px;">
                                        <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                            color: #FFFFFF; text-align: center; width: 60px;">
                                            <asp:LinkButton ID="lnkView" CausesValidation="true" OnClick="lnkView_Click" runat="server"
                                                CssClass="txtButtonRed" Text="View"></asp:LinkButton>
                                        </div>
                                    </td>
                                    <td align="left" style="padding-right: 10px;" width="110px;">
                                        <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                            color: #FFFFFF; text-align: center; width: 100px;">
                                            <a runat="server" target="_blank" id="btnExport" class="txtButtonRed">&nbsp;Export to
                                                Excel&nbsp;</a>
                                        </div>
                                    </td>
                                    <td align="left" id="tdMultipleRight" runat="server">
                                        <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                            color: #FFFFFF; text-align: center; width: 150px;">
                                            <asp:LinkButton runat="server" ID="lnkMultipleWOs" OnClick="lnkMultipleWOs_Click"
                                                CssClass="txtButtonRed"></asp:LinkButton>
                                        </div>
                                    </td>
                                    <td align="left">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td colspan="7" align="right" style="padding-right: 20px;">
                                        <table>
                                            <tr>
                                                <td align="left" id="tdGenDepot" runat="server" visible="false">
                                                    <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                                        color: #FFFFFF; text-align: center; width: 150px;">
                                                        <a runat="server" id="lnkbtnGenDepot" class="txtButtonRed">Generate Depot Sheet</a>
                                                    </div>
                                                </td>
                                                <td align="left" runat="server" id="tdFastClose" visible="false" style="cursor: pointer;">
                                                    <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                                        color: #FFFFFF; text-align: center; width: 80px;">
                                                        <a runat="server" id="btnfastClose" class="txtButtonRed">Fast Close</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr height="30px">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td colspan="2">
                                        <asp:Panel ID="pnlSelectedCompany" runat="server" Visible="false">
                                            <div class="divStyleCompany">
                                                <asp:Repeater ID="rptSelectedCompany" runat="server">
                                                    <ItemTemplate>
                                                        <div class="divStyle1 roundifyRefineMainBody">
                                                            <span class="clsFloatLeft">
                                                                <asp:Label ID="lblcompany" runat="server" Text=' <%#Eval("CompanyName")%> '></asp:Label></span>
                                                            <span class="marginleft8 clsFloatLeft">
                                                                <asp:LinkButton ID="lnkbtnRemove" CommandName="Remove" CausesValidation="false" CommandArgument='<%#Container.DataItem("CompanyID") %>'
                                                                    runat="server" class="deleteButtonAccredation">
                x</asp:LinkButton></span>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:Label runat="server" ID="lblSelectedCompany" CssClass="txtOrange" ForeColor="Red"
                                                    Style="clear: both; display: block; padding-left: 10px;" Text=""></asp:Label>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td id="tblwatch" runat="server" style="padding-right: 20px;" align="right">
                                        <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                            color: #FFFFFF; text-align: center; width: 60px; margin-right: 20px; margin-top: 20px;">
                                            <a runat="server" id="btnWatch" class="txtButtonRed">Watch</a>
                                            <div>
                                    </td>
                                    <td colspan="3">
                                        <asp:CheckBox runat="server" ID="chkHideTestAcc" Checked="false" Text="Hide Test Accounts"
                                            Visible="false" CssClass="formTxt" AutoPostBack="true" />&nbsp;
                                        <asp:CheckBox runat="server" ID="chkNextDay" Checked="false" Text="View jobs for next business day"
                                            Visible="false" CssClass="formTxt" AutoPostBack="true" />
                                        <asp:CheckBox runat="server" ID="chkMainCat" Checked="true" Text="Show Default View"
                                            CssClass="formTxt" AutoPostBack="true" />
                                    </td>
                                </tr>
                            </table>
                             <table border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F5F0" style="cursor: pointer;
                                margin: 10px;" height="18" id="tblRemoveWatch" runat="server">
                                <tr>
                                    <td colspan="3">
                                        <asp:Label runat="server" ID="lblRemoveMsg" CssClass="formTxtRed"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr bgcolor="#F4F5F0">
                                    <td>
                                        <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                            color: #FFFFFF; text-align: center; width: 150px;">
                                            <a runat="server" id="btnRemoveWatch" class="txtButtonRed">Remove Watch</a>
                                        </div>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:CheckBox runat="server" ID="chkWatchHideTestAcc" Checked="true" Text="Hide Test Accounts"
                                            CssClass="formTxt" AutoPostBack="true" />&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <hr style="background-color: #993366; height: 5px; margin: 0px;" />
                            <%@ register tagprefix="uc1" tagname="UCAdminWOsListing" src="~/UserControls/Admin/UCAdminWOsListingUK.ascx" %>
                            <uc1:UCAdminWOsListing ID="UCAdminWOsListing1" runat="server"></uc1:UCAdminWOsListing>
                            <!-- InstanceEndEditable -->
                        </td>
                    </tr>
                </table>
                </div>
            </ContentTemplate>
            <%--Poonam modified on 2/8/2016 - Task -OA-311 : EA - Multiple search company in listing does not work in Chrome--%>
            <Triggers>
                <asp:AsyncPostBackTrigger EventName="Click" ControlID="hdnbtnSelectedCompany" />
            </Triggers>
        </asp:UpdatePanel>
        </asp:Panel>
        <asp:UpdateProgress AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                <div class="gridText">
                    <img align="middle" src="Images/indicator.gif" />
                    <b>Fetching Data... Please Wait</b>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1"
            CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
</asp:Content>
