Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Linq
Imports System.Collections.Generic
Imports Admin.AT800

Partial Public Class AT800RolloutAuthorise
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim latestDate As DateTime
        latestDate = ws.WSWorkOrder.GetMaxAT800Rollout()
        For DateIterator As Integer = 1 To (DateTime.Today - latestDate).Days
            ddlRolloutDate.Items.Add(New ListItem(latestDate.AddDays(DateIterator).ToShortDateString()))
            DateIterator += 1
        Next
    End Sub

    Protected Sub btnAuthorise_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAuthorise.Click
        Dim ds As DataSet
        ds = ws.WSWorkOrder.AuthoriseAT800Rollout(DateTime.Parse(ddlRolloutDate.SelectedValue))
    End Sub

End Class