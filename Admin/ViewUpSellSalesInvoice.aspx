<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewUpSellSalesInvoice.aspx.vb" Inherits="Admin.ViewUpSellSalesInvoice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" style="height:100%;">
<script type="text/javascript">
<asp:Literal id="ltCountry" runat="server"></asp:Literal>
</script>
<script type="text/javascript" language="javascript" src="JS/Scripts.js"></script>
<head id="Head1" runat="server">
    <title>Invoice</title>
</head>
<body style="height:100%;">
    <form id="form1" runat="server" style="height:100%;">
    


    <asp:Panel ID="pnlUKInvoice" runat="server">
			 <%@ Register TagPrefix="uc1" TagName="UCViewInvoice" Src="~/UserControls/UK/UCMSViewUpSellSalesInvoiceUK.ascx" %>
			 <uc1:UCViewInvoice id="UCMSViewUpSellSalesInvoice1" runat="server"></uc1:UCViewInvoice> 
   </asp:Panel>
   </form>
	
	<script language="javascript" type="text/javascript">			
		if (country == "DE")
			{
				document.getElementById("pnlUKInvoice").style.visibility = "hidden";
				document.getElementById("pnlUKInvoice").style.height = "0";
				document.getElementById("pnlUKInvoice").style.position = "absolute";
				document.getElementById("pnlDEInvoice").style.visibility = "visible";				
			}			
	</script>



    </form>
	
	
</body>
</html>
