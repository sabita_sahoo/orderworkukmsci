<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Cancel WO" CodeBehind="WOCancellation.aspx.vb" Inherits="Admin.WOCancellation" %>


 <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
              <table width="100%" cellpadding="0" cellspacing="0">
			    <tr>
			    <td width="10px"></td>
			    <td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Cancel Work Order"  runat="server"></asp:Label></td>
			    </tr>
			    </table>
				
				<div id="divValidationMain" class="divValidation" runat="server" visible="false" style="margin:15px">
				<div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  <table width="96%"  border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					</tr>
					<tr valign="middle">
					  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
					  <td width="565" class="validationText"><div  id="divValidationMsg"></div>
					   <span class="validationText">
					  	<asp:ValidationSummary ID="validationSummarySubmit"  runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
					  </span>					   
					  </td>
					  <td width="20">&nbsp;</td>
					</tr>
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15"><input type="button" id="btnFocus" name="btnFocus" class="hdnClass"  /></td>
					  <td height="15">&nbsp;</td>
					</tr>
				  </table>  
				  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				</div>
				<p class="txtHeading">&nbsp;                </p>
				
				<table height="54" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server" id="tblWorkOrderId" >
                <tr valign="top">
                  <td class="formTxt" valign="top" align="left" width="33">&nbsp;</td>
                  <td class="formTxt" valign="top" align="left" width="307" height="14"><span class="formLabelGrey">Enter WorkOrderId to be cancelled&nbsp;</span><span class="bodytxtValidationMsg">*</span>
				   <asp:RequiredFieldValidator id="rqWorkOrderId" runat="server" ErrorMessage="Please enter WorkOrderId" ForeColor="#EDEDEB"
									ControlToValidate="txtWorkOrderId">*</asp:RequiredFieldValidator></td>
                  <td width="645" align="left" class="formTxt" >&nbsp;</td>
                  </tr>
                
                <tr valign="top">
                  <td align="left">&nbsp;</td>
                  <td align="left" height="20"><asp:TextBox id="txtWorkOrderId" TabIndex="1" runat="server" CssClass="formFieldGrey150"></asp:TextBox>
                  </td>
                  <td align="left">
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                        <asp:LinkButton id="btnView" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;View&nbsp;</asp:LinkButton>
                    </div>
                  </td>
                  </tr>
				  <tr valign="top">
                  <td align="left">&nbsp;</td>
                  <td align="left" height="20">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
              </table>
              
              <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="3%"  align="left">&nbsp;  
                    		
		        </td>                
                <td width="97%"  align="left"><asp:Label ID="lblMsg" runat="server" class="bodytxtValidationMsg" ></asp:Label>	</td>
              </tr>
              </table>
              
			  
			  <asp:Panel ID="pnlCancel" runat="server" Visible="false">
                <table id="DGWorkOrders" width="100%" border="0" cellpadding="5" cellspacing="0" >
				  <tr align="left" valign="top" >
					<td  class="gridHdr gridBorderRB">WO No</td>
					<td  class="gridHdr gridBorderRB">Submitted</td>
					<td  class="gridHdr gridBorderRB">Location</td>
					<td  class="gridHdr gridBorderRB">Contact</td>
					<td  class="gridHdr gridBorderRB">WO Title</td>
					<td  class="gridHdr gridBorderRB">WO Start</td>
					<td  class="gridHdr gridBorderRB">Wholesale Price</td>
					<td  class="gridHdr gridBorderRB">Portal Price</td>
					<td  class="gridHdrHighlight gridBorderB">Status</td>					
				  </tr>
				 <tr align="left">
					<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label></td>
					<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOSubmitted"></asp:Label></td>
					<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOLoc"></asp:Label></td>
					<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOContact"></asp:Label></td>
					<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOTitle"></asp:Label></td>
					<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOStart"></asp:Label></td>
					<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWPPrice"></asp:Label></td>
					<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblPPPrice"></asp:Label></td>
					<td  class="gridRow gridBorderB"><asp:Label runat="server" CssClass="gridRowHighlight" ID="lblWOStatus"></asp:Label></td>
				  </tr>
				</table>
				
				<p>&nbsp;</p>
				  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td class="HeadingRed">Client Cancellation Charges:</td>
                      <td width="10">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="250"  class="formLabelGrey">&nbsp;Wholesale Price: </td>
                          <td width="10">&nbsp;</td>
                          <td>&pound; <asp:TextBox ID="txtWPPrice" runat="server" CssClass="formFieldGrey" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                          <td width="250"  class="formLabelGrey">Cancellation Charges: 
                          <asp:RequiredFieldValidator id="RqValWholesalePrice" runat="server" ErrorMessage="Please enter a positive whole number for Wholesale Price." ForeColor="#EDEDEB"
									ControlToValidate="txtWPCancel">*</asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator id="RgValPositiveNumber" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtWPCancel"
                        	ErrorMessage="Please enter a positive whole number for Wholesale Price." ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator>
						</td>
                          <td width="10">&nbsp;</td>
                          <td>&pound; <asp:TextBox ID="txtWPCancel" runat="server" CssClass="formFieldGrey"></asp:TextBox></td>
                        </tr>
                      </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="10" class="smallText">&nbsp;</td>
                      <td  class="smallText">&nbsp;</td>
                      <td width="10"  class="smallText">&nbsp;</td>
                    </tr>
                    <tr id="trSuppTitle" runat="server">
                      <td width="10" >&nbsp;</td>
                      <td class="HeadingRed">Supplier Cancellation Charges:</td>
                      <td width="10">&nbsp;</td>
                    </tr>
                    <tr id="trSuppDetails" runat="server">
                      <td width="10">&nbsp;</td>
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="250"  class="formLabelGrey">Portal Price: </td>
                          <td width="10">&nbsp;</td>
                          <td>&pound; <asp:TextBox ID="txtPPPrice" runat="server" CssClass="formFieldGrey" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                          <td width="250"  class="formLabelGrey">Cancellation Charges:
                          <asp:RequiredFieldValidator id="RqValplatformPrice" runat="server" ErrorMessage="Please enter a positive whole number for Portal Price." ForeColor="#EDEDEB"
									ControlToValidate="txtPPCancel">*</asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator id="RgValPositiveNumber1" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtPPCancel"
                        	ErrorMessage="Please enter a positive whole number for Portal Price." ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator></td>
                          <td width="10">&nbsp;</td>
                          <td>&pound; <asp:TextBox ID="txtPPCancel" runat="server" CssClass="formFieldGrey"></asp:TextBox></td>
                        </tr>
                      </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
					 <tr>
                      <td width="10" height="15"  >&nbsp;</td>
                      <td height="15"  >&nbsp;</td>
                      <td width="10" height="15">&nbsp;</td>
                    </tr>
                    <tr id="TrRating" runat="server">
                     <td width="3%" valign="bottom" class="formLabelGrey" >&nbsp;</td>
                      <td>
                        <table>
                            <tr><td>&nbsp;</td></tr>
                             <tr>
	                       <td height="20" valign="bottom" class="formLabelGrey" >Select a Rating</td>
	                     </tr>
	                     <tr>
	                       <td style="height:30px;" valign="top"><asp:RadioButton id="rdoRatingPositive" style="vertical-align:baseline; " runat="server" CssClass="formLabelGrey" Text="Positive" TextAlign="right" GroupName="rdoRating"></asp:RadioButton>
			                    &nbsp;&nbsp;
			                    <asp:RadioButton id="rdoRatingNeutral" runat="server" CssClass="formLabelGrey" Text="Neutral" TextAlign="right" GroupName="rdoRating"></asp:RadioButton>
			                    &nbsp;&nbsp;
			                    <asp:RadioButton id="rdoRatingNegative" runat="server" CssClass="formLabelGrey" Text="Negative"	TextAlign="right" GroupName="rdoRating"></asp:RadioButton>
	                       </td>
	                     </tr>
                        






                        </table>
                      </td>
                      <td width="3%" valign="bottom" class="formLabelGrey" >&nbsp;</td>
                    </tr>                    
                    <tr>
					  <td width="3%" valign="bottom" class="formLabelGrey" >&nbsp;</td>
					  <td width="94%" height="15" valign="bottom" class="formLabelGrey" >
					  <asp:RegularExpressionValidator id="RegExComments" runat="server" ValidationExpression="(.|[\r\n]){1,500}"  ErrorMessage="Please enter comments less than 500 characters" ForeColor="#EDEDEB"
					   ControlToValidate="txtCompleteComments">*</asp:RegularExpressionValidator>
                     
					  </td>
					  <td width="3%" valign="bottom" class="formLabelGrey" >&nbsp;</td>
					</tr>
                    <tr>
                     <td width="3%" valign="bottom" class="formLabelGrey" >&nbsp;</td>
	                       <td style="height:30px;" valign="top"> 
                           Please Select Reason:<br />
                          <asp:DropDownList ID="drpdwncancelreason" TabIndex="24" runat="server"  CssClass="formFieldGrey" Width="250" DataTextField="StandardValue"></asp:DropDownList>
				  			<asp:RequiredFieldValidator id="reqdrpdwncancelreason" runat="server" ForeColor="#EDEDEB" ErrorMessage="Please select the Reason for Cancel" ControlToValidate="drpdwncancelreason">*</asp:RequiredFieldValidator>

                                </td>
                                <td width="3%" valign="bottom" class="formLabelGrey" >&nbsp;</td>
	                     </tr>
                        


					<tr>
					  <td>&nbsp;</td>
					  <td><asp:TextBox id="txtCompleteComments"  runat="server" CssClass="formFieldGreyW470H58" style="width:100%;" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" Rows="4"></asp:TextBox></td>
					  <td>&nbsp;</td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					</tr>
                    <tr>
                      <td width="10">&nbsp;
					  </td>
					  
                      <td width="920">
                      <table cellspacing="0" cellpadding="0" width="90" bgcolor="#F0F0F0" border="0" style="float:left;" >
                              <tr>
                                <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                                <td width="110" class="txtBtnImage"><asp:Linkbutton id="btnCancel" runat="server" CausesValidation="false" CssClass="txtListing" TabIndex="6" > <img src="Images/Icons/Cancel.gif" width="11" height="11" hspace="5"  align="absmiddle" border="0" />&nbsp;Back</asp:Linkbutton></td>
                                <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                              </tr>
                          </table>
                      
                        <table border="0" style="float:right;" cellpadding="0" cellspacing="0" >
                        <tr>                                                  
                          <TD width="10"></TD>
                          <TD vAlign="top" align="right" id="TdBtnCopy" runat="server"><TABLE cellSpacing="0" cellPadding="0" width="150" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD width="150" class="txtBtnImage"><a id="AnchorCopy" onclick="Javascript:DisableButton(this.id,'ctl00_ContentPlaceHolder1_btnCopy')" style="cursor:pointer;text-decoration:none;"  runat="server" TabIndex="2" class="txtListing"><img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5"  align="absmiddle" border="0">&nbsp;Cancel & Copy WO</a>
                                <asp:button id="btnCopy" runat="server" Height="0px" Width="0px"  Font-Size="0px" style="display:none;cursor:pointer;" CausesValidation="false" />
                                </TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                          </TABLE></TD>
                          <TD width="10"></TD>
                            <TD vAlign="top" align="right" id="TdBtnRematch" runat="server"><TABLE cellSpacing="0" cellPadding="0" width="200" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD width="200" class="txtBtnImage"><a id="AnchorRematch" onclick="Javascript:DisableButton(this.id,'ctl00_ContentPlaceHolder1_btnRematch')"  style="cursor:pointer;text-decoration:none;"   runat="server" TabIndex="2" class="txtListing"> <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5"  align="absmiddle" border="0">&nbsp;Cancel, Copy & Rematch WO</a>
                                <asp:button id="btnRematch" runat="server" Height="0px" Width="0px"  Font-Size="0px" style="display:none;cursor:pointer;" CausesValidation="false" />
                                </TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                          </TABLE></TD>
                          <TD width="10"></TD>
                          <TD vAlign="top" align="right"><TABLE cellSpacing="0" cellPadding="0" width="110" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD width="110" class="txtBtnImage"><a id="AnchorSubmit" onclick="Javascript:DisableButton(this.id,'ctl00_ContentPlaceHolder1_btnSubmit')"  style="cursor:pointer;text-decoration:none;"   runat="server" TabIndex="2" class="txtListing"> <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5"  align="absmiddle" border="0">&nbsp;Cancel WO</a>
                                <asp:button id="btnSubmit" runat="server" Height="0px" Width="0px"  Font-Size="0px" style="display:none;cursor:pointer;" CausesValidation="false" />
                                </TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                          </TABLE></TD>
                          
                          </tr>
                      </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
                  </table>
				  </asp:Panel>
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
</asp:Content>