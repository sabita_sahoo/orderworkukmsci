Public Partial Class PrintDepotSheets
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Public objEmail As New Emails
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim WOID As String

        'Security Check for Enhanced URL Security
        Dim errValue As String = ""
        If Not IsNothing(Request("err")) Then
            errValue = Request("err")
        Else
            errValue = 0
        End If
        Select Case errValue
            Case ApplicationSettings.ErrorCode.AccessDenied
                lblErrMsg.Text = ResourceMessageText.GetString("ErrorPrintAccessDenied")
                pnlErrMsg.Visible = True
                pnlPrintContent.Visible = False
            Case ApplicationSettings.ErrorCode.None
                pnlErrMsg.Visible = False
                pnlPrintContent.Visible = True
                If Not IsNothing(Request.QueryString("WOID")) Then
                    WOID = Request.QueryString("WOID").ToString
                    Dim i As Integer
                    Dim count As Integer
                    For i = 0 To WOID.Length
                        If Right(WOID, 1) = "," Then
                            WOID = Left(WOID, Len(WOID) - 1)
                            count = 1
                        End If
                        If Left(WOID, 1) = "," Then
                            WOID = WOID.Remove(0, 1)
                            count = count + 1
                        End If
                        If count > 0 Then
                            Exit For
                        End If
                    Next i
                    If WOID <> "" Then
                        Dim ds As DataSet = ws.WSWorkOrder.WO_PrintDepotSheets(WOID, CInt(Session("UserId")), "")
                        If ds.Tables.Count > 1 Then
                            If ds.Tables(0).Rows.Count > 0 Then
                                If ds.Relations("LinksByHeader") Is Nothing Then
                                    Dim ParentTablecol() As DataColumn
                                    Dim ChildTablecol() As DataColumn
                                    ParentTablecol = New DataColumn() {ds.Tables(0).Columns("DepotId"), ds.Tables(0).Columns("SupplierCompanyId"), ds.Tables(0).Columns("WorkOrderID")}
                                    ChildTablecol = New DataColumn() {ds.Tables(1).Columns("DepotId"), ds.Tables(1).Columns("SupplierCompanyId"), ds.Tables(1).Columns("WorkOrderID")}
                                    ds.Relations.Add("LinksByHeader", ParentTablecol, ChildTablecol, False)
                                End If
                            End If
                            dlPrint.DataSource = ds.Tables(0).Rows
                            dlPrint.DataBind()
                        End If
                    End If
                End If
        End Select
    End Sub

    Private Sub dlPrint_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlPrint.ItemDataBound
        'Getting WOID for each SI
        Dim dlPrintWorkOrder As DataList = DirectCast(e.Item.FindControl("dlPrintWorkOrder"), DataList)
        dlPrintWorkOrder.DataSource = (DirectCast(e.Item.DataItem, DataRow)).GetChildRows("LinksByHeader")
        dlPrintWorkOrder.DataBind()
    End Sub
    Public Function GetCompleteString(ByVal ProductsPurchased As String, ByVal AdditionalProductsPurchased As String) As String
        If ProductsPurchased = "" Then
            Return AdditionalProductsPurchased
        Else
            Return (ProductsPurchased + AdditionalProductsPurchased)
        End If
    End Function
End Class