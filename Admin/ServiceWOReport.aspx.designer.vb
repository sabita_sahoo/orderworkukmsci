﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ServiceWOReport
    
    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''txtSubmittedDateFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSubmittedDateFrom As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''calSubmittedDateFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calSubmittedDateFrom As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''txtSubmittedDateTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSubmittedDateTo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''calSubmittedDateTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calSubmittedDateTo As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''txtStartDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtStartDate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''calStartDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calStartDate As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''txtEndDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEndDate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''calEndDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calEndDate As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''txtPONumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPONumber As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''drpStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpStatus As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''hdnSelectedCompanyName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSelectedCompanyName As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''txtCompanyName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCompanyName As Global.WebLibrary.Input
    
    '''<summary>
    '''lnkBtnGenerateReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBtnGenerateReport As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lnkBtnExportToExcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBtnExportToExcel As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''pnlSelectedCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSelectedCompany As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''rptSelectedCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptSelectedCompany As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''lblSelectedCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSelectedCompany As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlShowRecords control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlShowRecords As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''rptShowRecords control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptShowRecords As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''pnlNoRecords control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlNoRecords As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''pnlInitialize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlInitialize As Global.System.Web.UI.WebControls.Panel
End Class
