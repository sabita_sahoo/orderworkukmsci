﻿<%@ Page Title="OrderWork : Account History" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="AccountHistory.aspx.vb" Inherits="Admin.AccountHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


              <div id="divContent">

         

            

        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>

        

             <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">

         

         <tr>

            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->

            <input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />                          

            <input id="hdnPageNo"  value="1" type="hidden" name="hdnPageNo" runat="server">

                  <input id="hdnPageSize" value="25" type="hidden" name="hdnPageSize" runat="server">

              <table width="100%" cellpadding="0" cellspacing="0" style="margin-bottom:20px">

                                    <tr>

                                    <td width="10px"></td>

                                    <td><asp:Label ID="lblMessage" CssClass="HeadingRed"  runat="server" Text="Account History"></asp:Label></td>

                                    </tr>

                                    </table>
                                                                   

<table cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
            <tr>
              <td width="165" class="txtWelcome paddingL10"><div id="divButton" style="width: 170px;  ">
                  <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                  <a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" title="Back to Profile" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8" /><strong><asp:Label id= "lblBacktoText" runat="server"></asp:Label></strong></a>
                  <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
              </div></td>                  
            </tr>
    </table>
                  <hr style="background-color:#993366;height:5px;margin:0px;" />

                  

                    <asp:Panel ID="pnlListing" runat="server" >    

                                <asp:GridView ID="gvAccountHistory"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'

                         BorderColor="White"  dataKeyNames="RowNum" PagerSettings-Mode=NextPreviousFirstLast   PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          

               <EmptyDataTemplate>

               <table  width="100%" border="0" cellpadding="10" cellspacing="0">                            

                                    <tr>

                                          <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.

                                          </td>

                                    </tr>                   

                              </table>

                  

               </EmptyDataTemplate>

               <PagerTemplate>

                   

                  

                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">

                                    <tr>        

                                          <td>

                                                <table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">

                                                  <tr>

                                                      <td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >

                                                            <TR>

                                                                  <td align="right" valign="middle">

                                                                    <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>

                                                                  </td>

                                                                  <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">

                                                                  <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                                                        <tr>

                                                                        <td align="right" width="36">

                                                                        <div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />

                                                                              </div>      

                                                                              <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />

                                                                              </div>                                                                                                                                             

                                                                        </td>

                                                                        <td width="50" align="center" style="margin-right:3px; ">                                                                              

                                                                              <asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">

                                                    </asp:DropDownList>

                                                                        </td>

                                                                        <td width="30" valign="bottom">

                                                                              <div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />

                                                                              </div>

                                                                              <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />

                                                                              </div>

                                                                        </td>

                                                                        </tr>

                                                                  </table>

                                                                  </td>

                                                            </TR>

                                                      </TABLE></td>

                                                  </tr>

                                                </table>

                                      </td>

                                    </tr>

                              </table> 

                   

        </PagerTemplate> 

               <Columns>                 
                            
                 <asp:TemplateField SortExpression="ModifiedBy" ItemStyle-Width="20%" HeaderText="Modified By" HeaderStyle-CssClass="gridHdr" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="20%"/>
                <ItemTemplate>             
                    <%# (Container.DataItem("ModifiedBy"))%>                    
				</ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="ChangeType" ItemStyle-Width="20%" HeaderText="Change Type" HeaderStyle-CssClass="gridHdr" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="20%"/>
                <ItemTemplate>             
                    <%# (Container.DataItem("ChangeType"))%>                    
				</ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="ChangeDetails" ItemStyle-Width="20%" HeaderText="ChangeDetails" HeaderStyle-CssClass="gridHdr" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="40%"/>
                <ItemTemplate>            
                    <%#GetChangeDetails(Container.DataItem("ChangeDetails"), Container.DataItem("BankAccountNameOldValue"), Container.DataItem("BankAccountNameNewValue"), Container.DataItem("BankAccountNoOldValue"), Container.DataItem("BankAccountNoNewValue"), Container.DataItem("BankSortCodeOldValue"), Container.DataItem("BankSortCodeNewValue"))%>                             
				</ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="ModifiedDate" ItemStyle-Width="20%" HeaderText="Modified Date" HeaderStyle-CssClass="gridHdr" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="20%"/>
                <ItemTemplate>             
                    <%# (Container.DataItem("ModifiedDate"))%>                    
				</ItemTemplate>
                </asp:TemplateField>
                </Columns>

                 <AlternatingRowStyle CssClass=gridRow />

                 <RowStyle CssClass=gridRow />            

                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />

                <HeaderStyle  CssClass="gridHdr" HorizontalAlign=Left VerticalAlign=Top  Height="40px" />

                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />

        

              </asp:GridView>

              </asp:Panel>

              


             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"

                SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.AccountHistory" SortParameterName="sortExpression">                

            </asp:ObjectDataSource>

                                            

                              

            <!-- InstanceEndEditable --></td>

                     

             

         </tr>

         </table>

      </div>


 


</asp:Content>
