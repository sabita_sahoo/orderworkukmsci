'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FavouriteSupplierList

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''hdnCompName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCompName As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnContactID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnContactID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnBillingLoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBillingLoc As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnBillingLocId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBillingLocId As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''LocType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LocType As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnMode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnMode As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''divAutoSuggest control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divAutoSuggest As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''txtContact control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtContact As Global.System.Web.UI.HtmlControls.HtmlInputText

    '''<summary>
    '''ddlBillLoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlBillLoc As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtBillingLoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBillingLoc As Global.System.Web.UI.HtmlControls.HtmlInputText

    '''<summary>
    '''trFixedCapacity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trFixedCapacity As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''txtFixedCapacity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFixedCapacity As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''drptxtFixedCapacityTimeSlot control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drptxtFixedCapacityTimeSlot As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''chkAllowJobAcceptance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkAllowJobAcceptance As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''tdAddSP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdAddSP As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''btnAddsupplier control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddsupplier As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearch As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''lblSearchType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSearchType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnReset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnReset As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''txtComments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtComments As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''PnlConfirm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PnlConfirm As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblConfirm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblConfirm As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lnkConfirm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkConfirm As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkCancel As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''pnlContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlContent As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''tdAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdAdd As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''pnlListing control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlListing As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''UpdateProgress1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdateProgress1 As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''UpdateProgressOverlayExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdateProgressOverlayExtender1 As Global.Flan.Controls.UpdateProgressOverlayExtender
End Class
