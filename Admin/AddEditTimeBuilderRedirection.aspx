﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddEditTimeBuilderRedirection.aspx.vb" Inherits="Admin.AddEditTimeBuilderRedirection" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Add/Edit TimeBuilder Redirection" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanelTBQ" runat="server" >
<ContentTemplate>
<script>
    function CheckIsNumeric(id,Mode) {
        var lblMsg
        lblMsg = id.replace("txtTimeSlot" + Mode, "lblMsg" + Mode);

        if (document.getElementById(id).value != "") {
            if (IsNumeric(document.getElementById(id).value) == false) {
                document.getElementById(id).value = "";
                document.getElementById(lblMsg).innerHTML = "Please enter only positive numeric values in " + Mode;
            }
            else {
                document.getElementById(lblMsg).innerHTML = "";                    
            }
        }
        else {
            document.getElementById(lblMsg).innerHTML = "";            
        }
    }
    function IsNumeric(sText) {

        var IsNumber
        var isInteger_re = /^[0-9]+[0-9]*$/;
        if (sText.match(isInteger_re)) {
            IsNumber = true;
        }
        else {
            IsNumber = false;
        }
        return IsNumber;
    }       
</script>
    <div id="divContent">
 		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
         
         <tr>
            <td valign="top">
             <table cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
                <tr>
                  <td width="165" class="txtWelcome paddingL10"><div id="divButton" style="width: 120px;  ">
                      <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                      <a class="buttonText" runat="server" id="lnkBackToService"><img src="Images/Arrows/Arrow-Back.gif" title="Back to Service" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8" /><strong>Back to Service</strong></a>
                      <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                  </div></td>                  
                </tr>
             </table>
            </td>
         </tr>
         </table>
         <div style="margin:0px 5px 0px 5px;" id="divMainTab">
            <div class="paddedBox"  id="divProfile" >
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="left" class="HeadingRed"><strong>TimeBuilder Redirection</strong></td>
                    <td align="left" class="padTop17">
                     <asp:Panel ID="pnlRemoveallTimeBuilderQ" runat="server" Visible="false">                            
                                    <TABLE cellSpacing="0" cellPadding="0" width="140" bgColor="#f0f0f0" border="0" style="float:left;">
		                                <TR>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                                <TD class="txtBtnImage" width="120">
				                                <asp:Linkbutton id="lnkBtnRemoveallTimeBuilderQ"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Remove All Questions&nbsp;</asp:LinkButton>
			                                </TD>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                                </TR>
	                                 </TABLE>
                    </asp:Panel>       
                    <table id="tblSaveTop" runat="server" cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                <tr>
                  <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveTop" ValidationGroup="VGAcc" CssClass="txtListing" runat="server" TabIndex="67"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                  <td width="10"></td>
                  <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetTop" CausesValidation="false" runat="server" TabIndex="68"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                </tr>
            </table>            
                    </td>
                  </tr>
             </table>

              <div class="divWorkOrder" style="margin-top:17px;float:left;">	      
                    <div id="divValidationTBRedirection" class="divValidation" runat=server visible="false" >
	                    <div class="roundtopVal">
	                      <blockquote>
	                        <p><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	                        </blockquote>
	                    </div>
		                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
			                    <tr valign="middle">
				                    <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				                    <td class="validationText"><div  id="divValidationMsg"></div>
				                        <asp:Label ID="lblValidationTBRedirection"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>				                    
				                    </td>
				                    <td width="20">&nbsp;</td>
			                    </tr>
		                    </table>
	                    <div class="roundbottomVal">
	                      <blockquote>
	                        <p><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	                        </blockquote>
	                    </div>
                </div>                                              
	             <table width="100%" cellspacing="0" cellpadding="0" border="0">	
                     <tr>
				        <td>				       				       
				          <div id = "divOuterpnlTimeBuilderRedirection" class="divOuterpnlTimeBuilderQuestions">
                           <div id="divOuterRedirection" runat="server">
                                <table width="100%">
                           <tr>
                           <td><b><asp:Label ID="lblMsg"  CssClass="bodytxtValidationMsg" Text="From/To fields are numeric fields" runat="server"></asp:Label></b></td>
                            <td>                           
                             <TABLE cellSpacing="0" cellPadding="0" width="92" bgColor="#f0f0f0" border="0" style="float:right;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkAddNew"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Add New&nbsp;</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
                            </td>
                           </tr>
                           </table>
                                <asp:Repeater ID="rptTimeBuilderRedirection" runat="server" >
                                <ItemTemplate>
                                     <table style="padding:5px;margin-top:10px;border:solid 1px black;">
                                     <tr>
                                      <td>Redirection - <%# Container.DataItem("RowNum")%>                               
                                       <input type="hidden" id="hdnRedirectionId" runat="server" value=<%#Container.dataitem("RedirectionId")%> />       
                                       <input type="hidden" id="hdnSelectedProductId" runat="server" value=<%#Container.dataitem("ProductId")%> />                                                                                  
                                      &nbsp;&nbsp;From&nbsp;<asp:TextBox ID="txtTimeSlotFrom" runat="server" CssClass="formField" Width="100" Height="20" Text=<%#Container.dataitem("TimeSlotFrom")%>></asp:TextBox>
                                      &nbsp;&nbsp;To&nbsp;<asp:TextBox ID="txtTimeSlotTo" runat="server" CssClass="formField" Width="100" Height="20" Text=<%#Container.dataitem("TimeSlotTo")%>></asp:TextBox>                                                                            
                                      &nbsp;&nbsp;Service&nbsp;<asp:DropDownList id="drpdwnProduct" runat="server" CssClass="formFieldGrey" Width="300"  Height="22"></asp:DropDownList>    
	                               <TABLE cellSpacing="0" cellPadding="0" width="60" bgColor="#f0f0f0" border="0" style="float:right;margin-right:140px;margin-left:20px;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkBtnRemove"  runat="server" CausesValidation="false" CssClass="txtListing" CommandName="RemoveRedirection" CommandArgument='<%#Container.DataItem("RedirectionId") %>'>&nbsp;Remove&nbsp;</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
                                      </td>
					              </tr>                                                         
				                    </table>
                             </ItemTemplate>
                            </asp:Repeater>
                            </div>                           
                            </div>
				        </td>
				      </tr>				      

                 </table>		
		     </div>       
             
             <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
              <td align="left" class="HeadingRed">&nbsp;</td>
              <td align="left" class="padTop17"><table id="tblSaveBottom" runat="server" cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                  <tr>
                    <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveBottom" ValidationGroup="VGAcc"  CssClass="txtListing" runat="server" TabIndex="63"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                        </tr>
                    </table></td>
                    <td width="10"></td>
                    <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetBottom"  CausesValidation="false" runat="server" TabIndex="64"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                        </tr>
                    </table></td>
                  </tr>
              </table></td>
            </tr>
          </table>       
            </div>          
               
         </div>
      </div>
</ContentTemplate>								
</asp:UpdatePanel>
</asp:Content>