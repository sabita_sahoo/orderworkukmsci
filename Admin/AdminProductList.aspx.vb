Public Partial Class AdminProductList
    Inherits System.Web.UI.Page
#Region "This is a declaration region"

    '''<summary>
    '''UCSwitchAdmin1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCSwitchAdmin1 As UCMSSwitchAdmin

    '''<summary>
    '''UCTopMSgLabel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCTopMSgLabel1 As UCTopMSgLabel

    '''<summary>
    '''UCMenuMainAdmin1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin

    '''<summary>
    '''UCMenuSubAdmin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin

    '''<summary>
    '''UCProductListing1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCProductListing1 As UCProductList
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
    End Sub

End Class