Partial Public Class AdminWOHistory
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lnkBackToListing.HRef = getBackToListingLink()
    End Sub

    Public Function getBackToListingLink() As String
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender")
                Case "UCWOsListing", "CompanyProfile", "companyprofile"
                    If Not IsNothing(Request("Group")) Then
                        If Request("Group") = ApplicationSettings.WOGroupIssue Then
                            link = "~\AdminWOIssueListing.aspx?"
                        Else
                            link = "~\AdminWOListing.aspx?"
                        End If
                    Else
                        link = "~\AdminWOListing.aspx?"
                    End If
                    link &= "WOID=" & Request("WOID")
                    link &= "&ContactID=" & Request("ContactID")
                    link &= "&CompanyID=" & Request("CompanyID")
                    If Not IsNothing(Request("IsWatched")) Then
                        If Request("IsWatched").ToString = "1" Then
                            link &= "&Group=Watched"
                            link &= "&mode=Watched"
                        Else
                            link &= "&Group=" & Request("Group")
                            link &= "&mode=" & Request("Group")
                        End If
                    Else
                        link &= "&Group=" & Request("Group")
                        link &= "&mode=" & Request("Group")
                    End If

                    If Not IsNothing(Request("BizDivID")) Then
                        If Request("BizDivID") <> "" Then
                            link &= "&BizDivID=" & Request("BizDivID")
                        Else
                            link &= "&BizDivID=" & ApplicationSettings.OWUKBizDivId
                        End If
                    Else
                        link &= "&BizDivID=" & ApplicationSettings.OWUKBizDivId
                    End If
                    If Not IsNothing(Request("WorkorderID")) Then
                        link &= "&WorkorderID=" & Request("WorkorderID")
                    End If
                    If Not IsNothing(Request("SearchWorkorderID")) Then
                        link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
                    End If
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&CompID=" & Request("CompID")
                    link &= "&sender=" & "UCWOsListing"

                    'Case "CompanyProfile", "companyprofile"
                    '    link = "~\AdminWoListing.aspx?Sender=AdminWOHistory"
                    '    link &= "&mode=" & Request("mode")
                    '    link &= "&Group=" & Request("Group")
                    '    link &= "&BizDivID=" & Request("BizDivID")
                    '    link &= "&PS=" & Request("PS")
                    '    link &= "&PN=" & Request("PN")
                    '    link &= "&SC=" & Request("SC")
                    '    link &= "&SO=" & Request("SO")
                    '    link &= "&CompID=" & Request("CompID")
                    '    link &= "&companyid=" & Request("companyid")
                    '    link &= "&FromDate=" & Request("FromDate")
                    '    link &= "&ToDate=" & Request("ToDate")
                    'link &= "&WOID=" & Request("WOID")
                    'link &= "&WorkorderID=" & Request("WorkorderID")

                Case "SearchWO"
                    link = "~\SearchWOs.aspx?"
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&SrcWorkorderID=" & Request("txtWorkorderID")
                    link &= "&SrcCompanyName=" & Request("CompanyName")
                    link &= "&SrcKeyword=" & Request("KeyWord")
                    link &= "&SrcPONumber=" & Request("PONumber")
                    link &= "&SrcReceiptNumber=" & Request("ReceiptNumber")
                    link &= "&SrcPostCode=" & Request("PostCode")
                    link &= "&SrcDateStart=" & Request("DateStart")
                    link &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
                    link &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")

                    If Not IsNothing(Request("BizDivID")) Then
                        If Request("BizDivID") <> "" Then
                            link &= "&BizDivID=" & Request("BizDivID")
                        Else
                            link &= "&BizDivID=" & ApplicationSettings.OWUKBizDivId
                        End If
                    Else
                        link &= "&BizDivID=" & ApplicationSettings.OWUKBizDivId
                    End If
                Case "CustomerHistory"
                    link = "CustomerHistory.aspx?"
                    link &= "WOID=" & Request("WOID")
                    link &= "&txtWorkorderID=" & Request("txtWorkorderID")
                    link &= "&ContactID=" & Request("ContactID")
                    link &= "&CompanyID=" & Request("CompanyID")
                    link &= "&Group=" & Request("Group")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&mode=" & Request("Group")
                    link &= "&CompID=" & Request("CompID")
                    link &= "&sender=" & "UCWODetails"
                Case "Rating"
                    link = "~\Rating.aspx?"
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    If Not IsNothing(Request("WorkorderID")) Then
                        link &= "&WorkorderID=" & Request("WorkorderID")
                    End If
                    If Not IsNothing(Request("SearchWorkorderID")) Then
                        link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
                    End If
                    link &= "&SelectedContact=" & Request("SelectedContact")
                    link &= "&Rating=" & Request("Rating")
                    If Not IsNothing(Request("BizDivID")) Then
                        If Request("BizDivID") <> "" Then
                            link &= "&BizDivID=" & Request("BizDivID")
                        Else
                            link &= "&BizDivID=" & ApplicationSettings.OWUKBizDivId
                        End If
                    Else
                        link &= "&BizDivID=" & ApplicationSettings.OWUKBizDivId
                    End If
                Case Else
                    Return ""
            End Select
        End If
        Return link
    End Function

End Class