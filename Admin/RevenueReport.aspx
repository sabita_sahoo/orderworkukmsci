﻿<%@ Page Title="OrderWork :" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="RevenueReport.aspx.vb" Inherits="Admin.RevenueReport" %>

<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div id="divContent">
        <div class="roundtopWhite">
            <img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" />
        </div>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
            <tr>
                <td valign="top">
                    <!-- InstanceBeginEditable name="EdtContent" -->
                    <table width="100%" cellpadding="0" cellspacing="0">

                        <tr>
                            <td>&nbsp;</td>
                            <td class="HeadingRed"><strong>Revenue Report</strong></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="formTxt" valign="top" height="18">
                                <asp:Label ID="ErrLabel" runat="server" Visible="false" CssClass="HeadingRed"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">

                                <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top: 15px;">
                                    <tr>
                                        <td width="10"></td>
                                        <td width="250">
                                            <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                <tr valign="top">
                                                    <td class="formTxt" valign="top" align="left" width="18">&nbsp;</td>
                                                    <td class="formTxt" valign="top" align="left" width="156">Report Type</td>
                                                    <td class="formTxt" valign="top" align="left" width="18">&nbsp;</td>
                                                    <td class="formTxt" valign="top" width="156" align="left" height="14">Month </td>
                                                    <td class="formTxt" align="left" width="156">&nbsp;</td>
                                                    <td class="formTxt" align="left" height="14">Year </td>

                                                </tr>
                                                <tr valign="top">
                                                    <td height="24" align="left">&nbsp;</td>
                                                    <td align="left">
                                                        <select id="ddReportType" runat="server" name="ddReportType" class="formFieldGrey121">
                                                            <option value="Consolidated">Consolidated</option>
                                                            <option value="Retail">Retail</option>
                                                            <option value="IT">IT</option>
                                                        </select>
                                                    </td>
                                                    <td height="24" align="left">&nbsp;</td>
                                                    <td align="left">
                                                        <select id="ddMonth" runat="server" name="ddMonth" class="formFieldGrey121">
                                                            <option value="1">January</option>
                                                            <option value="2">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">August</option>
                                                            <option value="9">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </td>
                                                    <td height="24" align="left" width="10">&nbsp;</td>
                                                    <td align="left">
                                                        <select id="ddYear" runat="server" name="ddYear" class="formFieldGrey121">
                                                            <option value="2006">2006</option>
                                                            <option value="2007">2007</option>
                                                            <option value="2008">2008</option>
                                                            <option value="2009">2009</option>
                                                            <option value="2010">2010</option>
                                                            <option value="2011">2011</option>
                                                            <option value="2012">2012</option>
                                                            <option value="2013">2013</option>
                                                            <option value="2014">2014</option>
                                                            <option value="2015">2015</option>
                                                            <option value="2016">2016</option>
                                                            <option value="2017">2017</option>
                                                            <option value="2018">2018</option>
                                                            <option value="2019">2019</option>
                                                            <option value="2020">2020</option>
                                                        </select>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                        <td align="left"></td>
                                        <td width="20">
                                            <table cellspacing="0" cellpadding="0" width="350" border="0">
                                                <tr valign="bottom">
                                                    <td width="350" style="padding-top: 16px;">
                                                        <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr align="left" valign="top">
                                                                <td width="6"></td>
                                                                <td class="formTxt" valign="top" height="16">From Date (dd/mm/yyyy) <span class="bodytxtValidationMsg"></span>

                                                                </td>
                                                                <td width="10">&nbsp;</td>
                                                                <td class="formTxt" height="16">To Date (dd/mm/yyyy)  <span class="bodytxtValidationMsg"></span>
                                                                </td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td width="6"></td>
                                                                <td height="24" align="left" style="cursor: pointer; vertical-align: top;">
                                                                    <asp:TextBox ID="txtFromDate" TabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
                                                                    <img alt="Click to Select" src="Images/calendar.gif" id="btnFromDate" style="cursor: pointer; vertical-align: top;" />
                                                                    <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnFromDate" TargetControlID="txtFromDate" ID="calFromDate" runat="server">
                                                                    </cc2:CalendarExtender>

                                                                </td>
                                                                <td width="10">&nbsp;</td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtToDate" TabIndex="3" runat="server" CssClass="formField105"></asp:TextBox>
                                                                    <img alt="Click to Select" src="Images/calendar.gif" id="btnToDate" style="cursor: pointer; vertical-align: top;" />
                                                                    <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnToDate" TargetControlID="txtToDate" ID="calToDate" runat="server">
                                                                    </cc2:CalendarExtender>

                                                                </td>
                                                            </tr>
                                                            <tr valign="top">
                                                                <td width="6"></td>
                                                                <td class="formTxt" colspan="3">
                                                                    <asp:ValidationSummary ID="valSumm" runat="server"></asp:ValidationSummary>
                                                                    <asp:CustomValidator ID="rngDate" runat="server" ErrorMessage="To Date should be greater than From Date" ForeColor="#EDEDEB" ControlToValidate="txtToDate">*</asp:CustomValidator>
                                                                    <asp:RegularExpressionValidator ID="regExpFromDate" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtFromDate" ErrorMessage="From Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
                                                                    <asp:RegularExpressionValidator ID="regExpToDate" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtToDate" ErrorMessage="To Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="left" style="padding-top: 15px;">
                                            <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important; color: #FFFFFF; text-align: center; width: 100px;">
                                                <asp:LinkButton ID="lnkBtnGenerateReport" CausesValidation="True" runat="server" CssClass="txtButtonRed" Text="Generate Report"></asp:LinkButton>
                                            </div>
                                        </td>

                                        <td align="left" style="padding-top: 15px;">
                                            <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important; color: #FFFFFF; text-align: center; width: 100px;">
                                                <asp:LinkButton ID="lnkBtnExportToExcel" Visible="false" runat="server" CssClass="txtButtonRed" Text="Export To Excel"></asp:LinkButton>
                                            </div>
                                        </td>
                                        <td width="60" align="left"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <%--End of header--%>


                    <asp:Panel ID="pnlShowRecords" runat="server" Visible="false">

                        <%--Revenue--%>
                        <asp:Panel ID="pnlRevenueReport" runat="server" Visible="true">
                            <div style="margin: 15px">
                                <table>
                                    <tr>
                                        <td colspan="6"><strong style="text-decoration: underline">Revenue:</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="100" style="font-weight: bold">CompanyID</td>
                                        <td width="150" style="font-weight: bold">Company Name</td>
                                        <td width="150" style="font-weight: bold;">Account Manager</td>
                                        <td width="80" style="font-weight: bold; text-align: right">Sales</td>
                                        <td width="80" style="font-weight: bold; text-align: right">Cost of Sales</td>
                                        <td width="80" style="font-weight: bold; text-align: right">Total Profit</td>
                                        <td width="80" style="font-weight: bold; text-align: right">Margin</td>
                                    </tr>
                                </table>
                                <asp:Repeater ID="rptRevenue" runat="server">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td width="100"><%#Container.DataItem("CompanyID")%></td>
                                                <td width="150"><%#Container.DataItem("CompanyName")%></td>
                                                <td width="150"><%#Container.DataItem("Manager")%></td>
                                                <td width="80" style="text-align: right">&pound; <%#Container.DataItem("Total Net Invoice value")%></td>
                                                <td width="80" style="text-align: right">&pound; <%#Container.DataItem("Total Net Platform value")%></td>
                                                <td width="80" style="text-align: right">&pound; <%#Container.DataItem("Total Profit")%></td>
                                                <td width="80" style="text-align: right"><%#Container.DataItem("Margin")%>&nbsp;%</td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <table>
                                            <tr>
                                                <td width="100">&nbsp;</td>
                                                <td width="150">&nbsp;</td>
                                                <td width="150" align="center"><b>Total</b></td>
                                                <td width="80" style="text-align: right">&pound;<asp:Label ID="lblTotalSales" runat="server" /></td>
                                                <td width="80" style="text-align: right">&pound;<asp:Label ID="lblTotalCostofSales" runat="server" /></td>
                                                <td width="80" style="text-align: right">&pound;<asp:Label ID="lblTotalProfit" runat="server" /></td>
                                                <td width="80" style="text-align: right"><asp:Label ID="lblTotalMargin" runat="server" />&nbsp;%</td>
                                            </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Panel ID="pnlRevenueNoRecords" runat="server" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: left; padding-top: 10px;" align="center">No records Available
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <hr style="width: 55%; position: absolute;" />
                                <br />
                            </div>
                        </asp:Panel>

                        <%--Credit note--%>
                        <asp:Panel ID="pnlCreditNote" runat="server" Visible="true">
                            <div style="margin: 15px">
                                <table>
                                    <tr>
                                        <td colspan="6"><strong style="text-decoration: underline">Credit Notes:</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="100" style="font-weight: bold">CompanyID</td>
                                        <td width="150" style="font-weight: bold">Company Name</td>
                                        <td width="100" style="font-weight: bold; text-align: right">Account Manager</td>
                                        <td width="100" style="font-weight: bold; text-align: right">Total Credit Value</td>

                                    </tr>
                                </table>
                                <asp:Repeater ID="rptCreditNote" runat="server">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td width="100"><%#Container.DataItem("CompanyID")%></td>
                                                <td width="150"><%#Container.DataItem("CompanyName")%></td>
                                                <td width="100"><%#Container.DataItem("Manager")%></td>
                                                <td width="100" style="text-align: right">&pound; <%#Container.DataItem("Total Net Invoice value")%></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <table>
                                            <tr>
                                                <td width="100"></td>
                                                <td width="150"></td>
                                                <td width="100"><b>Total</b></td>
                                                <td width="100" style="text-align: right">&pound;<asp:Label ID="lblTotalCreditValue" runat="server" /></td>
                                            </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Panel ID="pnlCreditNoteNoRecords" runat="server" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: left; padding-top: 10px;" align="center">No records Available
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <hr style="width: 55%; position: absolute;" />
                                <br />
                            </div>
                        </asp:Panel>

                        <%--Debit note--%>
                        <asp:Panel ID="pnlDebitNote" runat="server" Visible="true">
                            <div style="margin: 15px">
                                <table>
                                    <tr>
                                        <td colspan="6"><strong style="text-decoration: underline">Debit Notes:</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="100" style="font-weight: bold">CompanyID</td>
                                        <td width="150" style="font-weight: bold">Company Name</td>
                                        <td width="100" style="font-weight: bold; text-align: right">Total Debit Value</td>

                                    </tr>
                                </table>
                                <asp:Repeater ID="rptDebitNote" runat="server">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td width="100"><%#Container.DataItem("CompanyID")%></td>
                                                <td width="150"><%#Container.DataItem("CompanyName")%></td>
                                                <td width="100" style="text-align: right">&pound; <%#Container.DataItem("Total Net Invoice value")%></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <table>
                                            <tr>
                                                <td width="150"></td>
                                                <td width="100"><b>Total</b></td>
                                                <td width="100" style="text-align: right">&pound;<asp:Label ID="lblTotalDebitValue" runat="server" /></td>
                                            </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Panel ID="pnlDebitNoteNoRecords" runat="server" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: left; padding-top: 10px;" align="center">No records Available
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <hr style="width: 55%; position: absolute;" />
                                <br />
                            </div>
                        </asp:Panel>


                        <%--Upsell--%>
                        <asp:Panel ID="pnlUpsell" runat="server" Visible="true">
                            <div style="margin: 15px">
                                <table>
                                    <tr>
                                        <td colspan="6"><strong style="text-decoration: underline">Upsell:</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="100" style="font-weight: bold">CompanyID</td>
                                        <td width="150" style="font-weight: bold">Account Name</td>
                                        <td width="100" style="font-weight: bold; text-align: right">Upsell Amount</td>

                                    </tr>
                                </table>
                                <asp:Repeater ID="rptUpsell" runat="server">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td width="100"><%#Container.DataItem("CompanyID")%></td>
                                                <td width="150"><%#Container.DataItem("CompanyName")%></td>
                                                <td width="100" style="text-align: right">&pound; <%#Container.DataItem("UpSellPrice")%></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <table>
                                            <tr>
                                                <td width="150"></td>
                                                <td width="100"><b>Total</b></td>
                                                <td width="100" style="text-align: right">&pound;<asp:Label ID="lblTotalUpsellAmount" runat="server" /></td>
                                            </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Panel ID="pnlUpsellNoRecords" runat="server" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: left; padding-top: 10px;" align="center">No records Available
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <hr style="width: 55%; position: absolute;" />
                                <br />
                            </div>
                        </asp:Panel>

                        <%--Discounts--%>
                        <asp:Panel ID="pnlDiscount" runat="server" Visible="true">
                            <div style="margin: 15px">
                                <table>
                                    <tr>
                                        <td colspan="6"><strong style="text-decoration: underline">Discounts:</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="100" style="font-weight: bold">CompanyID</td>
                                        <td width="150" style="font-weight: bold">Account Name</td>
                                        <td width="100" style="font-weight: bold; text-align: right">Discount Amount</td>

                                    </tr>
                                </table>
                                <asp:Repeater ID="rptDiscount" runat="server">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td width="100"><%#Container.DataItem("CompanyID")%></td>
                                                <td width="150"><%#Container.DataItem("CompanyName")%></td>
                                                <td width="100" style="text-align: right">&pound; <%#Container.DataItem("Discount")%></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                     <FooterTemplate>
                                        <table>
                                            <tr>
                                                <td width="150"></td>
                                                <td width="100"><b>Total</b></td>
                                                <td width="100" style="text-align: right">&pound;<asp:Label ID="lblTotalDsicountAmount" runat="server" /></td>
                                            </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Panel ID="pnlDiscountNoRecords" runat="server" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: left; padding-top: 10px;" align="center">No records Available
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <hr style="width: 55%; position: absolute;" />
                                <br />
                            </div>
                        </asp:Panel>

                        <%--Manual Adjustments--%>
                        <asp:Panel ID="pnlManualAdjustment" runat="server" Visible="true">
                            <div style="margin: 15px">
                                <table>
                                    <tr>
                                        <td colspan="6"><strong style="text-decoration: underline">Manual Adjustments:</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="100" style="font-weight: bold">CompanyID</td>
                                        <td width="150" style="font-weight: bold">Account Name</td>
                                        <td width="120" style="font-weight: bold; text-align: right">Adjustment Amount</td>

                                    </tr>
                                </table>
                                <asp:Repeater ID="rptManualAdjustments" runat="server">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td width="100"><%#Container.DataItem("CompanyID")%></td>
                                                <td width="150"><%#Container.DataItem("CompanyName")%></td>
                                                <td width="120" style="text-align: right">&pound; <%#Container.DataItem("SumDebit")%></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                     <FooterTemplate>
                                        <table>
                                            <tr>
                                                <td width="150"></td>
                                                <td width="100"><b>Total</b></td>
                                                <td width="100" style="text-align: right">&pound;<asp:Label ID="lblTotalAdjustmentAmount" runat="server" /></td>
                                            </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Panel ID="pnlManualAdjustmentsNoRecords" runat="server" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: left; padding-top: 10px;" align="center">No records Available
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <hr style="width: 55%; position: absolute;" />
                                <br />
                            </div>
                        </asp:Panel>

                    </asp:Panel>

                    <asp:Panel ID="pnlNoRecords" runat="server" Visible="false">
                        <table width="100%">
                            <tr>
                                <td style="text-align: center; height: 250px; padding-top: 150px;" align="center">No records Available
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnlInitialize" runat="server" Visible="true">
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

