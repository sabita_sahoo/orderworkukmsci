﻿Public Class ServiceDetails
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then
            If Not IsNothing(Request("ServiceId")) Then
                iFrameServiceDetails.Attributes.Add("src", OrderWorkLibrary.ApplicationSettings.NewPortalURL.ToString & "/service/createservice/" & Session("UserID").ToString & "/" & Request("ServiceId").ToString)
            End If

        End If
    End Sub

End Class