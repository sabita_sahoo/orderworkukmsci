<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Receivables.aspx.vb" Inherits="Admin.Receivables" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Receivables"%>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
	<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
        <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<script language="JavaScript" type="text/JavaScript">
function PayEarlyDiscountInvoices(objid)
{
     var index = objid.lastIndexOf('_');  
    var identify = objid.substr(0,index); 
    if ((document.getElementById(identify+"_rdoPayInclDiscTotal").checked == false) && (document.getElementById(identify+"_rdoPayExclDiscTotal").checked == false))
    {
        alert("Please select any one of the options to proceed")
    }
    else
    {
        if (document.getElementById(identify+"_rdoPayInclDiscTotal").checked == true)
        {
            document.getElementById("hdnOnPageInvoiceNo").value = document.getElementById(identify+"_hdnInvoiceNo").value;
            document.getElementById("btnPayInclDisc").click() 
        }
        if(document.getElementById(identify+"_rdoPayExclDiscTotal").checked == true)
        {
            document.getElementById("hdnOnPageInvoiceNo").value = document.getElementById(identify+"_hdnInvoiceNo").value;
            document.getElementById("btnPayExclDisc").click()
        }
    }
}
function CallFuncToPayExclDisc()
{
     document.getElementById("btnPayInvoiceExclDisc").click()
 }
 function ShowAmountTotal(Type, Id) {
     var lableName;
     var Total;
     var HidePaymentBtn = false;
     if (Type == "Invoices") {
         lableName = "ctl00_ContentPlaceHolder1_lblInvoicesTotal";
         Total = Id.replace("Check", "hdnSIAmount");
         if (document.getElementById(Id).checked == true)
             document.getElementById(lableName).innerHTML = parseFloat(parseFloat(document.getElementById(lableName).innerHTML) + parseFloat(document.getElementById(Total).value)).toFixed(2);                       
         else
             document.getElementById(lableName).innerHTML = parseFloat(parseFloat(document.getElementById(lableName).innerHTML) - parseFloat(document.getElementById(Total).value)).toFixed(2);
     }
     else if (Type == "UnpaidCN") {
         lableName = "ctl00_ContentPlaceHolder1_lblUnpaidCNTotal";
         Total = Id.replace("Chk2", "hdnCNAmount");
         if (document.getElementById(Id).checked == true) {
             HidePaymentBtn = true;
             document.getElementById(lableName).innerHTML = parseFloat(parseFloat(document.getElementById(lableName).innerHTML) + parseFloat(document.getElementById(Total).value)).toFixed(2);
         }
         else {
             document.getElementById(lableName).innerHTML = parseFloat(parseFloat(document.getElementById(lableName).innerHTML) - parseFloat(document.getElementById(Total).value)).toFixed(2);
         }
         
     }
     else if (Type == "UnallocatedCR") {
         lableName = "ctl00_ContentPlaceHolder1_lblUnallocatedCRTotal";
         Total = Id.replace("Chk1", "hdnCRAmount");
         if (document.getElementById(Id).checked == true) {
             HidePaymentBtn = true;
             document.getElementById(lableName).innerHTML = parseFloat(parseFloat(document.getElementById(lableName).innerHTML) + parseFloat(document.getElementById(Total).value)).toFixed(2);
         }
         else {
             document.getElementById(lableName).innerHTML = parseFloat(parseFloat(document.getElementById(lableName).innerHTML) - parseFloat(document.getElementById(Total).value)).toFixed(2);
         }
     }

     if (document.getElementById("ctl00_ContentPlaceHolder1_lblUnpaidCNTotal").innerHTML != "0.00") {
         HidePaymentBtn = true;
     }
     else if (document.getElementById("ctl00_ContentPlaceHolder1_lblUnallocatedCRTotal").innerHTML != "0.00") {
         HidePaymentBtn = true;
     }
     else {
         HidePaymentBtn = false;
     }
     //alert(HidePaymentBtn);
     if (HidePaymentBtn == true) {
         document.getElementById("ctl00_ContentPlaceHolder1_gvInvoices_ctl01_tblPaymentReceivedBtn").style.display = "none";
     }
     else {
         document.getElementById("ctl00_ContentPlaceHolder1_gvInvoices_ctl01_tblPaymentReceivedBtn").style.display = "block";
     }

     document.getElementById("ctl00_ContentPlaceHolder1_lblCalculatedTotal").innerHTML = parseFloat(parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblInvoicesTotal").innerHTML) - (parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblUnallocatedCRTotal").innerHTML) + parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblUnpaidCNTotal").innerHTML))).toFixed(2);
     document.getElementById("ctl00_ContentPlaceHolder1_lblCreditAndCashTotal").innerHTML = parseFloat(parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblUnallocatedCRTotal").innerHTML) + parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblUnpaidCNTotal").innerHTML)).toFixed(2);
     pause(1);
 }
 function pause(milliseconds) {
     var dt = new Date();
     while ((new Date()) - dt <= milliseconds) { /* Do nothing */ }
 }
</script>

	        <div id="divContent">
         
		<input type="hidden" runat="server" id="hdnEtoE"/>  
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            

			<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10px"></td>
			<td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Receivables" runat="server"></asp:Label></td>
			</tr>
			</table>	
		
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
        <asp:Panel ID="pnlOuter" runat="server">						
		<table width="570" cellpadding="0" cellspacing="0" border="0">
		<tr>
		<td width="10px"></td>
		<td width="240"><%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
					<uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact></td>
		
		<td width="80" align="left"><asp:CheckBox ID="chkShowAll" runat="server" Text='Show All' class="formTxt" /> </td>
		
		<td align="left" width="80">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                <asp:LinkButton ID="lnkView" causesValidation=False OnClick="lnkView_Click" runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
            </div>
		</td>
		 			
		 <td align="left" width="190">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                <asp:linkButton ID="lnkExport" CausesValidation="false" CssClass="txtButtonRed" runat="server" style="cursor:hand" >&nbsp;Export to Excel&nbsp;</asp:linkButton>
            </div>
		</td>
       <%-- <td align="left" width="215"><TABLE cellSpacing="0" cellPadding="0" width="200" bgColor="#993366" border="0">
                          <TR>
                            <TD height="18"><IMG height="18" src="Images/Curves/Red-BtnLeft.gif" width="5"></TD>
                            <TD><a runat="server" target="_blank" id="btnExportUnPaidCN" class="txtButtonRed"> &nbsp;Export to Excel Unpaid Credit Notes&nbsp;</a></TD>
                            <TD width="5"><IMG height="18" src="Images/Curves/Red-BtnRight.gif" width="5"></TD>
                          </TR>
                        </TABLE>
		</td>
        <td align="left" ><TABLE cellSpacing="0" cellPadding="0" width="235" bgColor="#993366" border="0">
                          <TR>
                            <TD height="18"><IMG height="18" src="Images/Curves/Red-BtnLeft.gif" width="5"></TD>
                            <TD><a runat="server" target="_blank" id="btnExportUnallocatedCR" class="txtButtonRed"> &nbsp;Export to Excel Unallocated Cash Receipts&nbsp;</a></TD>
                            <TD width="5"><IMG height="18" src="Images/Curves/Red-BtnRight.gif" width="5"></TD>
                          </TR>
                        </TABLE>
		</td>--%>
		<%--<td style="text-align:right;padding-right:20px;">
                                    <strong>Selected Invoice Total - �<label id="lblInvoicesTotal" runat="server">0.00</label></strong><br />
                                    <strong>Selected Unpaid Credit Notes Total - �<label id="lblUnpaidCNTotal" runat="server">0.00</label></strong><br />
                                    <strong>Selected Unallocated Cash Receipts Total - �<label id="lblUnallocatedCRTotal" runat="server">0.00</label></strong><br />
                                    <strong>Calculated Total [Invoice Total - (Cash Receipts Total + Credit Notes Total)] - �<label id="lblCalculatedTotal" runat="server">0.00</label></strong><br />
                                 </td>--%>
		</tr>
			  
		</table>	
		<div style="border:1px solid black; position:fixed;right: 20%; background-color:white;padding:10px; width:10%;">
                <strong><div style="float:left;width:70%">Invoice:</div> <div style="text-align:right;float:left;width:30%">�<label id="lblInvoicesTotal" runat="server">0.00</label></div></strong><br />
                <strong><div style="float:left;width:70%">Credit & Cash:</div> <div style="text-align:right;float:left;width:30%">�<label id="lblCreditAndCashTotal" runat="server">0.00</label></div></strong><br />               
                <strong><div style="float:left;width:70%">Total:</div> <div style="text-align:right;float:left;width:30%">�<label id="lblCalculatedTotal" runat="server">0.00</label></div></strong><br />
                <strong style="display:none;">Credit & Cash: �<label id="lblUnpaidCNTotal" runat="server">0.00</label></strong>
                <strong style="display:none;">Selected Unallocated Cash Receipts Total - �<label id="lblUnallocatedCRTotal" runat="server">0.00</label></strong>
                <div style="clear:both"></div>
        </div>		
		
		<table width="100%">
				<tr>
				  <td></td>
				  <td><span class="formTxt">Payment Date (dd/mm/yyyy) <span class="bodytxtValidationMsg"></span></span></td>
				  <td>&nbsp;</td>
				  <td align="left">&nbsp;</td>
			    </tr>
				
				
				<tr>
		  			<td width="10"></td>
		  			<td width="135" valign="top"><asp:TextBox ID="txtPaymentDate" TabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
	                    <img alt="Click to Select" src="Images/calendar.gif" id="btnInvoiceDate" style="cursor:pointer; vertical-align:middle;" />						
					  <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnInvoiceDate TargetControlID="txtPaymentDate" ID="calFromDate" runat="server">
					  </cc1:CalendarExtender>
					</td>
		  			<td width="200">
		  			    <table id="tblAllocate" visible=false runat=server cellspacing="0" cellpadding="0" width="100%" border="0">
                 			<tr valign="top">
                 			    <td align="left">&nbsp;</td>
                  				<td width="70" align="left" id="tdSubmitData" runat=server visible=true>
									<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:80px;">	
                                        <asp:LinkButton id="btnAllocate" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Allocate&nbsp;</asp:LinkButton>
                                    </div>
								</td>
				  				<td align="left" width="3">&nbsp;</td>
								<td width="70" align="left" id="tdCancelData" runat=server visible=true>
								    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:80px;">
                                        <asp:LinkButton id="btnCancel" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Cancel&nbsp;</asp:LinkButton>
                                    </div>
								 </td>
								 
			  				</tr>
			  			</table>		  			
					</td>
		  			<td>&nbsp;
                       
					</td>
		  		</tr>
		  		<tr>
		  		    <td colspan="4"><asp:RegularExpressionValidator ID="regExpInvoiceDate" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtPaymentDate"
											ErrorMessage="Invoice Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator></td>
		  		</tr>

			</table>
           
		
           <table>
		   			<tr>
             			<td class="paddingL30 padB21" valign="top">
             
							 <div id="divValidationMain" visible=false class="divValidation" runat=server >
								  <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<tr>
										  <td height="15" align="center">&nbsp;</td>
										  <td height="15">&nbsp;</td>
										  <td height="15">&nbsp;</td>
									</tr>
									<tr valign="middle">
					  						<td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
					  						<td class="validationText"><div  id="divValidationMsg"></div>
						  							<asp:Label ID="lblMsg" runat="server"  CssClass="bodytxtValidationMsg"></asp:Label>		  	
					  						</td>
					  						<td width="20">&nbsp;</td>
									</tr>
									<tr>
											  <td height="15" align="center">&nbsp;</td>
											  <td height="15">&nbsp;</td>
											  <td height="15">&nbsp;</td>
									</tr>
				  			</table>
								  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
								</div>   
		
							</td>
					</tr>
             </table>

        
        
        <div id="divEarlyDiscValidationGrid" runat="server">
			 <hr style="background-color:#993366;height:5px;margin:0px;" />
			 <asp:GridView ID="gvEarlyDiscInvoices" runat=server AllowPaging="False"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
						   Width="100%"  AllowSorting="false" PagerSettings-Visible="false" >          
						   
									   <Columns>  
												 
															
										<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="80px"   HeaderStyle-CssClass="gridHdr" DataField="InvoiceNo" HeaderText="Sales Invoice Number" />                
										<asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="80px" HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="Date Issued" />                                    
									
										<asp:TemplateField HeaderText="Net Amount" >               
									   <HeaderStyle CssClass="gridHdr" Width="150px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
									   <ItemTemplate>  
									   <%#FormatCurrency(Container.DataItem("InvoiceNetAmnt"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   	<asp:TemplateField HeaderText="Discount" >               
									   <HeaderStyle CssClass="gridHdr" Width="150px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
									   <ItemTemplate>  
									   <%#FormatCurrency(Container.DataItem("Discount"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   <asp:TemplateField HeaderText="Total Amt Incl. Discount" >               
									   <HeaderStyle CssClass="gridHdr" Width="200px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
									   <ItemTemplate>  
									   <asp:RadioButton runat="server" ID="rdoPayInclDiscTotal" GroupName="PayInvoice" />
									   <%#FormatCurrency(Container.DataItem("InclDiscTotal"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   <asp:TemplateField HeaderText="Total Amt Excl. Discount" >               
									   <HeaderStyle CssClass="gridHdr" Width="200px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
									   <ItemTemplate>  
									   <asp:RadioButton runat="server" ID="rdoPayExclDiscTotal" GroupName="PayInvoice" />
									   <%#FormatCurrency(Container.DataItem("ExclDiscTotal"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   <asp:TemplateField >               
									   <HeaderStyle CssClass="gridHdr" Width="100px"   Wrap=true /> 
									  <ItemStyle Wrap=true HorizontalAlign=Left   CssClass="gridRow"/>
									   <ItemTemplate>   
                                            <input type=hidden runat=server id='hdnInvoiceNo'  value='<%#Container.DataItem("InvoiceNo") %>'/>
                                            <a id="lnkPayEarlyDisc" class="footerTxtSelected" runat="server" onclick='javascript:PayEarlyDiscountInvoices(this.id)' href="#" >Pay</a>
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   </Columns>
										 <AlternatingRowStyle    CssClass=gridRow />
										 <RowStyle CssClass=gridRow />            

										<HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
										<FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
								
						    </asp:GridView>
			                    <asp:Button Height="0" Width="0" BorderWidth="0px" ID="btnPayInclDisc" runat="server" OnClick="PayInclDisc" />
                                <asp:Button Height="0" Width="0" BorderWidth="0px" ID="btnPayExclDisc" runat="server" OnClick="PayInvoiceExclDiscount" />
                                <asp:Button Height="0" Width="0" BorderWidth="0px" ID="btnPayInvoiceExclDisc" runat="server" OnClick="PayInvoiceExclDiscount" />
                                <asp:textbox style="height:0px; width:0px; border:none 0px;" runat="server" id='hdnOnPageInvoiceNo'></asp:textbox>
</div>
        <div runat="server" id="divListings">
		 
                     
				   <hr style="background-color:#993366;height:5px;margin:0px;" />
           	<table width="100%" align=center border="0" cellspacing="0" cellpadding="0">               
           		 <tr>
            		<td>
						  <asp:GridView ID="gvInvoices" runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
						   PagerSettings-Mode=NextPreviousFirstLast PagerSettings-Position=Top Width="100%" DataSourceID="ObjectDataSource1" AllowSorting="true" PagerSettings-Visible="true" >          
						   <EmptyDataTemplate>
               				<table  width="100%" border="0" cellpadding="10" cellspacing="0">					
									<tr>
											<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
											</td>
									</tr>				
							</table>
                  
               				</EmptyDataTemplate>
               
               				
							<PagerTemplate>
                   
            						<table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
										<tr>																				
												<td width="10">&nbsp;</td>												  
						  						<td runat="server" id="tdSelectAll" enableviewstate=true width="100%">
						  						            <table>
						  						               <tr>
						  						                <td >
                                                                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:120px;" ID="tblPaymentReceivedBtn" runat="server">
						  						                        <asp:LinkButton ID="lnkPaymentReceived" causesValidation=False OnClick="PaymentReceived" runat="server" CssClass="txtButtonRed" Text="Payment received" ></asp:LinkButton>
                                                                    </div>
          							                            </td>
          							                           <%-- <td width="10">&nbsp;</td>
          							                            <td width="143">
          							                            <table cellspacing="0" cellpadding="0" width="143" bgcolor="#993366" border="0" id="Table1" runat="server" visible="true">
            							                            <tr>
              								                            <td height="18"><img height="18" src="Images/Curves/Red-BtnLeft.gif" width="5" /></td>
              								                            <td><asp:LinkButton ID="lnkButtonSendMail" causesValidation=False OnClick="SendEmailNotification" runat="server" CssClass="txtButtonRed" Text="Send Email Notification" ></asp:LinkButton></td>
              								                            <td width="5"><img height="18" src="Images/Curves/Red-BtnRight.gif" width="5" /></td>
            							                            </tr>
          							                            </table>						
          							                            </td>--%>                                                               
          							                           </tr>
          							                         </table>
												</td>				
												<td>
														<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  							<tr>
																<td align="right" >		
																		<TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
																				<TR>
																					<td align="right" valign="middle">
											 											 <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right" runat="server" class="formLabelGrey"></div>
																					</td>
																					<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
																						<table width="100%" border="0" cellpadding="0" cellspacing="0">
																							<tr>
																								<td align="right" width="36">
																										<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																											<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
																										  </div>	
																											<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																												<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
																											</div>																							   
																								</td>
																								<td width="50" align="center" style="margin-right:3px; ">													
																									<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField" ID="ddlPageSelector" runat="server" AutoPostBack="true">
																									</asp:DropDownList>
																								</td>
																								<td width="30" valign="bottom">
																									<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																									 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
																									</div>
																									<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																									 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
																									</div>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</TR>
																			</TABLE>
																	</td>
								  								</tr>
															</table>
						  								</td>
												</tr>
											</table> 
                   
        						</PagerTemplate> 
									   <Columns>  
												 <asp:TemplateField HeaderText="<input id='chkAll' onClick=CheckAllReceivables(this,'Check','Invoices') type='checkbox' name='chkAll' />">               
									   <HeaderStyle CssClass="gridHdr" Width="30px"    Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left   Width="15px"  CssClass="gridRow"/>
									   <ItemTemplate>   
											<input type=hidden runat=server id='hdnInvoiceNo'  value='<%#Container.DataItem("InvoiceNO") %>'/>
											<input type=hidden runat=server id='hdnSIBusinessArea'  value='<%#Container.DataItem("BusinessArea") %>'/>
											<asp:CheckBox ID='Check'  runat='server' Onclick="Javascript:ShowAmountTotal('Invoices',this.id);" Visible='<%#IIF(Container.DataItem("OnHold")=0,1,0)%>'></asp:CheckBox>
											<asp:Image runat="server" ImageUrl ="Images/Icons/alertsmall.gif" id="imgMarker" AlternateText="Withheld" ToolTip="Withheld" Width="20" Height="14" Visible='<%#IIF(Container.DataItem("OnHold")=0,0,1)%>' runat='server'/>   
									   </ItemTemplate>
									   </asp:TemplateField>  
															
										<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="150px"   HeaderStyle-CssClass="gridHdr" DataField="InvoiceNo" SortExpression="InvoiceNo" HeaderText="Sales Invoice Number" />                
										<asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="100px" HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="Date Issued" />                                    
										<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="150px"   HeaderStyle-CssClass="gridHdr" DataField="AgedDebt" SortExpression="AgedDebt" HeaderText="Aged Debt" /> 
										<asp:BoundField ItemStyle-CssClass="gridRow"   HeaderStyle-CssClass="gridHdr" DataField="BillingLocation" SortExpression="BillingLocation" HeaderText="Billing Location" /> 
										
										
										<asp:TemplateField SortExpression="Total" ItemStyle-Width="100px" HeaderText="Invoice Amount" >               
									   <HeaderStyle CssClass="gridHdr"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left   CssClass="gridRow"/>
									   <ItemTemplate>  
									   <input type=hidden runat=server id='hdnSIAmount'  value='<%#Container.DataItem("Total") %>'/> 
										<%#FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   <%--<asp:TemplateField SortExpression="Discount" HeaderText="Discount" >               
									   <HeaderStyle CssClass="gridHdr" Width="90px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="90px"  CssClass="gridRow"/>
									   <ItemTemplate>  
                                                <%#IIf(Container.DataItem("Discount"), "Y", "N")%>
									   </ItemTemplate>
									   </asp:TemplateField>--%>
														
										<asp:TemplateField >               
									   <HeaderStyle CssClass="gridHdr" Width="55px"   Wrap=true /> 
									  <ItemStyle Wrap=true HorizontalAlign=Left  Width="55px" CssClass="gridRow"/>
									   <ItemTemplate>   
												 
											   <%# GetLinks(Container.DataItem("InvoiceNo"), Container.DataItem("CompanyId"), Container.DataItem("OnHold"))%>
									   </ItemTemplate>
									   </asp:TemplateField>  
										</Columns>
									   
										 <AlternatingRowStyle    CssClass=gridRow />
										 <RowStyle CssClass=gridRow />            
										<PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
										<HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
										<FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
								
						    </asp:GridView>
				 		</td>
            		</tr>
            </table>
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr height="10">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr id="trHeadings1" runat=server>					
					<td width="100%" Class="HeadingRed" style="padding:0px 0px 0px 10px;"><strong>Unpaid Credit Notes</strong></td>                    
				</tr>
				<tr>
				<td width="100%" valign="top">
						<hr style="background-color:#993366;height:5px;margin:0px;" id="div3" runat="server" />
							
										<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            						<tr>
            							<td>
										 <asp:GridView ID="gvCreditNotes"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=10 BorderColor="White"  
									   PagerSettings-Mode=NextPreviousFirstLast  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource3"   AllowSorting=true>          
									   <EmptyDataTemplate>
												   <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
															<tr>
																<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
																</td>
															</tr>				
														</table>
										  
									   </EmptyDataTemplate>
									   
									   <PagerTemplate>
										   
														<table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTopCreditNotes" style="visibility:visible " runat="server">
																	<tr>																				
																		<td width="10">&nbsp;</td>												  
																		<td runat="server" id="tdSelectAllCreditNotes" enableviewstate=true width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">
																		  <TR>
																			<TD class="txtListing" align="left"><strong>
																			  
																			</strong> </TD>
																			<TD width="5"></TD>
																		  </TR>
																		</TABLE>						
																		</td>				
																		<td>
																			<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
																			  <tr>
																				<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
																					<TR>
																						<td align="right" valign="middle">
																						  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right" runat="server" class="formLabelGrey"></div>
																						</td>
																						<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
																						<table width="100%" border="0" cellpadding="0" cellspacing="0">
																							<tr>
																							<td align="right" width="36">
																							<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																								<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
																							  </div>	
																								<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																								<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
																								</div>																							   
																							</td>
																							<td width="50" align="center" style="margin-right:3px; ">													
																								<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged2" CssClass="formField" ID="ddlPageSelector" runat="server" AutoPostBack="true">
																								</asp:DropDownList>
																							</td>
																							<td width="30" valign="bottom">
																								<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																								 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
																								</div>
																								<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																								 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
																								</div>
																							</td>
																							</tr>
																						</table>
																						</td>
																					</TR>
																				</TABLE></td>
																			  </tr>
																			</table>
																	  </td>
																	</tr>
																</table> 
										   
								</PagerTemplate> 
									   <Columns>  
									   <asp:TemplateField HeaderText="<input id='chkAll' onClick=CheckAllReceivables(this,'Chk2','UnpaidCN') type='checkbox' name='chkAll' />">               
									   <HeaderStyle CssClass="gridHdr" Width="30px"    Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left   Width="15px"  CssClass="gridRow"/>
									   <ItemTemplate>   
											<input type=hidden runat=server id='hdnInvoiceNo2'  value='<%#Container.DataItem("InvoiceNO") %>'/>
											<input type=hidden runat=server id='hdnCNBusinessArea'  value='<%#Container.DataItem("BusinessArea") %>'/>
											<asp:CheckBox ID='Chk2'  runat='server'  Onclick="Javascript:ShowAmountTotal('UnpaidCN',this.id);" ></asp:CheckBox>
											   
									   </ItemTemplate>
									   </asp:TemplateField>  
															
										<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="150px"   HeaderStyle-CssClass="gridHdr" DataField="InvoiceNo" SortExpression="InvoiceNo" HeaderText="Credit Note Invoice No." />                
										<asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="100px" HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="Date Issued" />                                    
										<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="150px"   HeaderStyle-CssClass="gridHdr" DataField="SalesInvoiceNo" HeaderText="Sales Invoice No" /> 
									  <asp:BoundField ItemStyle-CssClass="gridRow"   HeaderStyle-CssClass="gridHdr" DataField="BillingLocation" HeaderText="Billing Location" />                         	
										<asp:TemplateField SortExpression="Total" ItemStyle-Width="100px" HeaderText="Credit Amount" >               
									   <HeaderStyle CssClass="gridHdr"   Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow"/>
									   <ItemTemplate> 
									   <input type=hidden runat=server id='hdnCNAmount'  value='<%#Container.DataItem("Total") %>'/>  
										<%#FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>    
									 
									   <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">     
									    <HeaderStyle CssClass="gridHdr" Width="50px"   Wrap=true />            
                                       <ItemStyle CssClass="gridRowIcons" Wrap=true  Width="50px"/>
                                            <ItemTemplate> 
				                            <a runat=server target="_blank" id="lnkViewCreditNote" href=<%#"~/ViewCreditNote.aspx?invoiceNo=" & Container.DataItem("InvoiceNO") & "&bizDivId=" & Session("BizDivId") %>><img src='Images/Icons/View-All-WO-SI.gif' alt='View Credit Notes' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>
				                            </ItemTemplate>  
                                        </asp:TemplateField>
										</Columns>
									   
										 <AlternatingRowStyle    CssClass=gridRow />
										 <RowStyle CssClass=gridRow />            
										<PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
										<HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
										<FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
								
									  </asp:GridView>
				 			</td>
           				 </tr>
            			</table>
					</td>
				</tr>			
				
			</table>
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr height="10">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr id="trHeadings" runat=server>
					<td width="100%" Class="HeadingRed" style="padding:0px 0px 0px 10px;"><strong>Unallocated Cash Receipts</strong></td>					
				</tr>
				<tr>
					<td width="100%" valign="top">
                            <hr id="div2" runat="server" style="background-color:#993366;height:5px;margin:0px;" />
           						<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            						<tr>
            							<td>
										  <asp:GridView ID="gvCashReceipts"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=10 BorderColor="White"  
										   PagerSettings-Mode=NextPreviousFirstLast  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource2"   AllowSorting=true>          
               								<EmptyDataTemplate>
               										<table  width="100%" border="0" cellpadding="10" cellspacing="0">					
														<tr>
																	<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
																	</td>
														</tr>				
													</table>
                  
               								</EmptyDataTemplate>
               
               					<PagerTemplate>
                   
										<table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTopCashReceipts" style="visibility:visible " runat="server">
													<tr>																				
														<td width="10">&nbsp;</td>												  
														<td runat="server" id="tdSelectAllCashReceipts" enableviewstate=true width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">
														  <TR>
															<TD class="txtListing" align="left"><strong>
															  
															</strong> </TD>
															<TD width="5"></TD>
														  </TR>
														</TABLE>						
														</td>				
														<td>
															<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
															  <tr>
																<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
																	<TR>
																		<td align="right" valign="middle">
																		  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right" runat="server" class="formLabelGrey"></div>
																		</td>
																		<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
																		<table width="100%" border="0" cellpadding="0" cellspacing="0">
																			<tr>
																			<td align="right" width="36">
																			<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																				<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
																			  </div>	
																				<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																				<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
																				</div>																							   
																			</td>
																			<td width="50" align="center" style="margin-right:3px; ">													
																				<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged1" CssClass="formField" ID="ddlPageSelector" runat="server" AutoPostBack="true">
																				</asp:DropDownList>
																			</td>
																			<td width="30" valign="bottom">
																				<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																				 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
																				</div>
																				<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																				 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
																				</div>
																			</td>
																			</tr>
																		</table>
																		</td>
																	</TR>
																</TABLE></td>
															  </tr>
															</table>
													  </td>
													</tr>
												</table> 
                   
        							</PagerTemplate> 
									   <Columns>
									   <asp:TemplateField HeaderText="<input id='chkAll' onClick=CheckAllReceivables(this,'Chk1','UnallocatedCR') type='checkbox' name='chkAll' />">               
									   <HeaderStyle CssClass="gridHdr" Width="30px"    Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left   Width="15px"  CssClass="gridRow"/>
									   <ItemTemplate>   
											<input type=hidden runat=server id='hdnInvoiceNo1'  value='<%#Container.DataItem("InvoiceNO") %>'/>
											<input type=hidden runat=server id='hdnCRBusinessArea'  value='<%#Container.DataItem("BusinessArea") %>'/>
											<asp:CheckBox ID='Chk1'  runat='server'  Onclick="Javascript:ShowAmountTotal('UnallocatedCR',this.id);"  ></asp:CheckBox>
											   
									   </ItemTemplate>
									   </asp:TemplateField>  
															
										<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="150px"   HeaderStyle-CssClass="gridHdr" DataField="InvoiceNo" SortExpression="InvoiceNo" HeaderText="Cash Receipt No." />                
										<asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="100px" HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="DateCreated" SortExpression="DateCreated" HeaderText="Date Issued" />                                    
										<%--<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="90px"   HeaderStyle-CssClass="gridHdr" DataField="BuyerName" SortExpression="BuyerName" HeaderText="Client Name" /> --%>
										<asp:BoundField ItemStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr" DataField="Comments" SortExpression="Comments" HeaderText="Comments" /> 
										 
										<asp:TemplateField SortExpression="Total" HeaderText="Payment Amount" >               
									   <HeaderStyle CssClass="gridHdr" Width="100px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="100px"  CssClass="gridRow"/>
									   <ItemTemplate>  
									   <input type=hidden runat=server id='hdnCRAmount'  value='<%#Container.DataItem("Total") %>'/>  
										<%#FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>                            
									  <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">      
									   <HeaderStyle CssClass="gridHdr" Width="50px"   Wrap=true />           
                                        <ItemStyle CssClass="gridRowIcons" Wrap=true  Width="50px"/>
                                        <ItemTemplate> 
				                        <a runat=server target="_blank" id="lnkViewAllocatedSalesInvoice" href=<%#"~/ViewCashReceipt.aspx?invoiceNo=" & Container.DataItem("InvoiceNO") & "&bizDivId=" & Session("BizDivId") & "&ReceiptType=UA Receipt" %>>
				                        <img id="Img1" src='Images/Icons/View-Cash-Payment.gif' alt='View Allocation' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'' ></a>
				                        </ItemTemplate>  
                                       </asp:TemplateField>
               </Columns>  
								 <AlternatingRowStyle    CssClass=gridRow />
								 <RowStyle CssClass=gridRow />            
								<PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
								<HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
								<FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
        
              				</asp:GridView>
				 			</td>
           				 </tr>
            			</table>
						
						
						
					</td>				
				</tr>			
				
			</table>
			
	</div>		
		  				
					 
						<table width="100%" cellpadding="0" cellspacing="0">
						 <tr align="center">
						  <td><asp:Label ID="lblMsgCheckBox" CssClass="HeadingRed" runat="server" Visible="false"></asp:Label></td>
						 </tr>
						</table>
				
				 <asp:ObjectDataSource   ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.Receivables" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
			<asp:ObjectDataSource   ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB1"  EnablePaging="True" SelectCountMethod="SelectCount1" TypeName="Admin.Receivables" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
			<asp:ObjectDataSource   ID="ObjectDataSource3" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB2"  EnablePaging="True" SelectCountMethod="SelectCount2" TypeName="Admin.Receivables" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>

          </asp:Panel>
          </ContentTemplate>
            <Triggers>
                            <asp:AsyncPostBackTrigger   ControlID="lnkView" EventName="Click"/> 
                            <asp:PostBackTrigger   ControlID="lnkExport" />                                         
                         </Triggers>
      </asp:UpdatePanel>
         

            <!-- InstanceEndEditable --></td>
			     <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                  <b>Fetching Data... Please Wait</b>
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
		 
         </tr>
         </table>
      </div>
</asp:Content>