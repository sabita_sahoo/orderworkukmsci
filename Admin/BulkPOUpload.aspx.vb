﻿Imports System.IO
Imports System.Drawing
Imports System.Web
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports System.Data.OleDb
Imports System.Data
Imports System.Collections.Generic
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet
Public Class BulkPOUpload
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then
            tblPostUpload.Visible = False
            tblProcess.Visible = False
            tblUpload.Visible = True
        End If
    End Sub

    Public Sub lnkbtnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnUpload.Click
        If (FileUpload1.HasFile) Then
            Dim Extension As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString
            ViewState("Extension") = Extension
            If (Extension = ".xls" Or Extension = ".xlsx") Then
                lblMsg.Text = ""
                'Dim FilePath As String = ApplicationSettings.AttachmentUploadPath(0).ToString
                'FilePath = FilePath + "BulkPOUpload\"

                Dim FilePath As String = Server.MapPath(Convert.ToString("~/BulkPOUpload/"))

                Dim FileName As String = "BulkPOUpload_" & DateTime.Now.ToString("ddMMyyyyhhmmss") & Extension

                FileUpload1.SaveAs(FilePath + FileName)

                ViewState("FilePath") = FilePath + FileName
                tblProcess.Visible = True
                trUpload.Visible = False
            Else
                lblMsg.Text = "File format must be .xls or .xlsx"
                tblProcess.Visible = False
                trUpload.Visible = True
            End If
        Else
            lblMsg.Text = "Please select file to upload"
            tblProcess.Visible = False
            trUpload.Visible = True
        End If
    End Sub

    Public Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim filePath As String = ""
        Try
            divLoading.Style.Add("display", "inline")
            Dim dt As DataTable
            dt = ImportUploadedFile(ViewState("FilePath").ToString, ViewState("Extension").ToString)
            filePath = ViewState("FilePath").ToString
            Dim dv As DataView
            dv = dt.DefaultView

            dv.Table.Columns(0).ColumnName = "Workorderid"
            dv.Table.Columns(1).ColumnName = "PONumber"

            Dim dsUpload As New DataSet("BulkPOUpload")
            dsUpload.Tables.Add(dt)
            Dim LoggedInUserID As Integer = Session("UserId")
            Dim ds As DataSet
            ds = ws.WSWorkOrder.UpdateBulkPONumbers(dsUpload.GetXml, LoggedInUserID)
            If (ds.Tables(0).Rows.Count > 0) Then
                divLoading.Style.Add("display", "none")
                If (ds.Tables(0).Rows(0)("Success") = 1) Then
                    lblSuccess.Text = "Update Done"
                    tblPostUpload.Visible = True
                    tblUpload.Visible = False
                Else
                    lblSuccess.Text = "Update Fail"
                End If
            Else
                divLoading.Visible = False
                lblSuccess.Text = "Update Fail"
            End If
        Catch ex As Exception
        Finally
            If File.Exists(filePath) Then
                File.Delete(filePath)
            End If
        End Try
    End Sub

    Public Sub lnkbtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnBack.Click
        tblPostUpload.Visible = False
        tblUpload.Visible = True
        tblProcess.Visible = False
        trUpload.Visible = True
        divLoading.Style.Add("display", "none")
    End Sub

    Private Function ImportUploadedFile(ByVal FilePath As String, ByVal Extension As String) As DataTable
        Dim dt As New DataTable("BulkPOUpload")
        'Dim conStr As String = ""
        'Select Case Extension
        '    Case ".xls"
        '        'Excel 97-03
        '        conStr = ConfigurationManager.ConnectionStrings("Excel03ConString") _
        '                   .ConnectionString
        '        Exit Select
        '    Case ".xlsx"
        '        'Excel 07
        '        conStr = ConfigurationManager.ConnectionStrings("Excel07ConString") _
        '                  .ConnectionString
        '        Exit Select
        'End Select
        'conStr = String.Format(conStr, FilePath, "true")

        'Dim connExcel As New OleDbConnection(conStr)
        'Dim cmdExcel As New OleDbCommand()
        'Dim oda As New OleDbDataAdapter()

        'cmdExcel.Connection = connExcel

        ''Get the name of First Sheet
        'connExcel.Open()
        'Dim dtExcelSchema As DataTable
        'dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        'Dim SheetName As String = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
        'connExcel.Close()

        ''Read Data from First Sheet
        'connExcel.Open()
        'cmdExcel.CommandText = "SELECT * From [" & SheetName & "]"
        'oda.SelectCommand = cmdExcel
        'oda.Fill(dt)
        'connExcel.Close()

        'Open the Excel file in Read Mode using OpenXml.
        Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(FilePath, False)
            'Read the first Sheet from Excel file.
            Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild(Of Sheet)()

            'Get the Worksheet instance.
            Dim worksheet As Worksheet = TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart).Worksheet

            'Fetch all the rows present in the Worksheet.
            Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()

            'Loop through the Worksheet rows.
            For Each row As Row In rows
                'Use the first row to add columns to DataTable.
                If row.RowIndex.Value = 1 Then
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        dt.Columns.Add(GetValue(doc, cell))
                    Next
                Else
                    'Add rows to DataTable.
                    dt.Rows.Add()
                    Dim i As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
                        i = i + 1
                    Next
                End If
            Next
        End Using

        'Delete blank rows
        For i As Integer = dt.Rows.Count - 1 To 0 Step -1
            If IsDBNull(dt.Rows(i)(0)) Or Convert.ToString(dt.Rows(i)(0)) = "" Then
                dt.Rows(i).Delete()
            End If
        Next
        Return dt
    End Function

    Private Function GetValue(doc As SpreadsheetDocument, cell As Cell) As String
        Try
            Dim value As String = ""
            value = cell.CellValue.Text
            If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = CellValues.SharedString Then
                Return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(Integer.Parse(value)).InnerText
            End If
            Return value
        Catch ex As Exception
            Return ""
        End Try
    End Function

End Class