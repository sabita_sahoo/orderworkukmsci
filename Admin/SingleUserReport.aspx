﻿<%@ Page Title="OrderWork : Single User Report" Language="vb" AutoEventWireup="false"
    MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="SingleUserReport.aspx.vb"
    Inherits="Admin.SingleUserReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="divContent">
    <div id="divValidationMain" class="PopUpTailRegisterDiv" runat=server visible="false"  style="width:830px;">			                
  <img src="Images/Note_Image.jpg" align="left">
  <div style="margin-left:80px;">
    <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
    <asp:ValidationSummary ValidationGroup="VG"  id="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" ></asp:ValidationSummary>									
    </div>
</div>
        <div class="roundtopWhite">
            <img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner"
                style="display: none" /></div>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
            <tr>
                <td valign="top">
                    <!-- InstanceBeginEditable name="EdtContent" -->
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="HeadingRed">
                                <strong>Single User Report</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="formTxt" valign="top" height="18">
                                <asp:Label ID="ErrLabel" runat="server" Visible="false" CssClass="HeadingRed"></asp:Label>
                            </td>
                        </tr>
                      
                    </table>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                            <td width="20">
                            </td>
                            <td width="220" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr valign="top">
                                        <td class="formTxt" valign="top" height="20">
                                            Select Company
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%@ register src="~/UserControls/Admin/UCSearchContact.ascx" tagname="UCSearchContact"
                                                tagprefix="uc1" %>
                                            <uc1:UCSearchContact ID="UCSearchContact1" runat="server"></uc1:UCSearchContact>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                             <td width="220" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr valign="top">
                                        <td class="formTxt" valign="top" height="20">
                                            Email
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                             <asp:RegularExpressionValidator ID="rgEmail" Display="None" runat="server" ValidationGroup="VG" ControlToValidate="txtEmail" ErrorMessage="Please enter Email Address in correct format" ValidationExpression="^.+@[^\.].*\.[a-z]{2,}$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="5">
                            </td>
                            <td align="left" width="370" valign="top">
                                <table cellspacing="0" cellpadding="0" width="350" border="0">
                                    <tr valign="top">
                                        <td width="350" style="padding-top: 5px;">
                                            <%@ register src="~/UserControls/UK/UCDateRangeUK.ascx" tagname="UCDateRange" tagprefix="uc1" %>
                                            <uc1:UCDateRange ID="UCDateRange1" runat="server"></uc1:UCDateRange>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="padding-top: 23px;">
                                <asp:CheckBox ID="chkToDate" runat="server" CausesValidation="false" Text="To Date"
                                    TextAlign="Right" class="formTxt" valign="top" Height="24" />
                            </td>
                            <td width="20">
                            </td>
                            <td align="left" style="padding-top: 23px;">
                                <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                    color: #FFFFFF; text-align: center; width: 100px;">
                                    <asp:LinkButton ID="lnkBtnGenerateReport"  runat="server"
                                        CssClass="txtButtonRed" Text="Generate Report" CausesValidation="false" ValidationGroup="VG"></asp:LinkButton>
                                </div>
                            </td>
                         
                            <td align="left" style="padding-top: 23px;">
                                <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                    color: #FFFFFF; text-align: center; width: 100px;">
                                    <asp:LinkButton ID="lnkBtnExportToExcel" Visible="false" runat="server" CssClass="txtButtonRed"
                                        Text="Export To Excel"></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="pnlShowRecords" runat="server" Visible="false">
                        <div style="margin: 15px">
                            <p style="font-size: 16px; text-align: center;">
                                <strong>Single User Report for
                                    <asp:Label ID="lblCompanyName" runat="server"></asp:Label></strong>
                                <br />
                                <br />
                                <span style="font-size: 14px;"><strong>Date Range : </strong>
                                    <asp:Label ID="lblFromDate" runat="server"></asp:Label>
                                    To
                                    <asp:Label ID="lblToDate" runat="server"></asp:Label></span>
                            </p>
                        </div>
                        <div style="margin: 15px">
                            <p style="text-decoration: underline; font-size: 14px;">
                                <strong>Summary</strong></p>
                            <div style="margin: 15px">
                                <table>
                                    <tr>
                                        <td colspan="6">
                                            <strong style="text-decoration: underline">WorkOrders</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="140" style="font-weight: bold">
                                            Type
                                        </td>
                                        <td width="100" style="font-weight: bold">
                                            No. of Orders
                                        </td>
                                        <td width="80" style="font-weight: bold; text-align: right">
                                            WP
                                        </td>
                                        <td width="80" style="font-weight: bold; text-align: right">
                                            PP
                                        </td>
                                        <td width="80" style="font-weight: bold; text-align: right">
                                            Margin
                                        </td>
                                        <td width="80" style="font-weight: bold; text-align: right">
                                            Margin (%)
                                        </td>
                                    </tr>
                                </table>
                                <asp:Repeater ID="rptShowRecords" runat="server">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td width="140">
                                                    <%#Container.DataItem("label")%>
                                                </td>
                                                <td width="100">
                                                    <%#Container.DataItem("TotalWorkOrders")%>
                                                </td>
                                                <td width="80" style="text-align: right">
                                                    &pound;
                                                    <%#Container.DataItem("TotalWholesalePrice")%>
                                                </td>
                                                <td width="80" style="text-align: right">
                                                    &pound;
                                                    <%#Container.DataItem("TotalPlatformPrice")%>
                                                </td>
                                                <td width="80" style="text-align: right">
                                                    &pound;
                                                    <%#Container.DataItem("Margin")%>
                                                </td>
                                                <td width="80" style="text-align: right">
                                                    <%#Container.DataItem("PercentMargin")%>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <p></p>
			    <div>
			   <%--  <table>
			            <tr>
			                <td colspan="3"><strong style="text-decoration:underline">Invoices</strong></td>
			            </tr>
                        <tr>
                            <td width="100" style="font-weight:bold">Type</td>
                            <td width="120" style="font-weight:bold">No. of Invoices</td>
                            <td width="80" style="font-weight:bold;text-align:right">Net Value</td>                            
                        </tr>
                    </table>
                <asp:Repeater ID="rptInvoices" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="100"><%#Container.DataItem("InvoiceType")%></td>
                            <td width="120"><%#Container.DataItem("NoOfInvoices")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalAmount")%></td>                           
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:Repeater>
                </div>--%>
                </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlNoRecords" runat="server" Visible="false">
                        <table width="100%">
                            <tr>
                                <td style="text-align: center; height: 250px; padding-top: 150px;" align="center">
                                    No records Available
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnlInitialize" runat="server" Visible="true">
                    </asp:Panel>
                    <!-- InstanceEndEditable -->
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
