Public Partial Class GenerateWebServicesAccessKey
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not Page.IsPostBack Then

            UCSearchContact1.BizDivID = Session("BizDivId")
            UCSearchContact1.ClassId = 0
            UCSearchContact1.Filter = ""
            pnlCredentials.Visible = False
            pnlGetCredentials.Visible = False
            ViewState("AccountID") = ""
        End If
    End Sub

    Protected Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkView.Click
        If UCSearchContact1.ddlContact.SelectedValue <> "" Then
            lblError.Text = ""
            Dim ds As DataSet = ws.WSWorkOrder.MS_GetWebServiceAccessKey(UCSearchContact1.ddlContact.SelectedValue, Session("BizDivId"))
            If ds.Tables("tblWebServiceAccessKey").Rows.Count > 0 Then
                pnlCredentials.Visible = True
                pnlGetCredentials.Visible = False
                lblAccountID.Text = ds.Tables("tblWebServiceAccessKey").Rows(0)("AccountID")
                ViewState("AccountID") = ds.Tables("tblWebServiceAccessKey").Rows(0)("AccountID")
                lblPassword.Text = Encryption.Decrypt(ds.Tables("tblWebServiceAccessKey").Rows(0)("Password"))
                chkIsEnable.Checked = CBool(ds.Tables("tblWebServiceAccessKey").Rows(0)("ISEnable"))
            Else
                pnlCredentials.Visible = False
                pnlGetCredentials.Visible = True
            End If

        Else
            lblError.Text = "Please select a company"
            pnlCredentials.Visible = False
            pnlGetCredentials.Visible = False
        End If
    End Sub
    Protected Sub SaveForm(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ws.WSWorkOrder.MS_UpdateWebServiceAccessKeyDetails(ViewState("AccountID"), chkIsEnable.Checked)
    End Sub
    Protected Sub CreateWSAccessCredentials(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetWSAccessKey.Click
        If UCSearchContact1.ddlContact.SelectedValue <> "" Then
            lblError.Text = ""
            Dim ds As DataSet = ws.WSWorkOrder.MS_CreateWebSrviceAccessKeyEntry(UCSearchContact1.ddlContact.SelectedValue, Session("BizDivId"), Encryption.Encrypt(GetRandomPasswordUsingGUID(10)))
            If ds.Tables("tblWebServiceAccessKey").Rows.Count > 0 Then
                pnlCredentials.Visible = True
                pnlGetCredentials.Visible = False
                lblAccountID.Text = ds.Tables("tblWebServiceAccessKey").Rows(0)("AccountID")
                ViewState("AccountID") = ds.Tables("tblWebServiceAccessKey").Rows(0)("AccountID")
                lblPassword.Text = Encryption.Decrypt(ds.Tables("tblWebServiceAccessKey").Rows(0)("Password"))
                chkIsEnable.Checked = CBool(ds.Tables("tblWebServiceAccessKey").Rows(0)("ISEnable"))
            End If
        Else
            lblError.Text = "Please select a company"
            pnlCredentials.Visible = False
            pnlGetCredentials.Visible = False
        End If


    End Sub
    Public Function GetRandomPasswordUsingGUID(ByVal length As Integer) As String
        'Get the GUID
        Dim guidResult As String = System.Guid.NewGuid().ToString()

        'Remove the hyphens
        guidResult = guidResult.Replace("-", String.Empty)

        'Make sure length is valid
        If length <= 0 OrElse length > guidResult.Length Then
            Throw New ArgumentException("Length must be between 1 and " & guidResult.Length)
        End If

        'Return the first length bytes
        Return guidResult.Substring(0, length)
    End Function

End Class