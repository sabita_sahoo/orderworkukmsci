﻿Imports System.IO
Imports System.Data.Common

Public Class UploadFileForBulkCancellation
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click

        Dim oFile As StreamReader = New StreamReader(FileUpload1.PostedFile.InputStream)
        Dim strWOIDStringBuilder As New StringBuilder

        Dim fileBasePath As String = Server.MapPath("~/")
        Dim fileName As String = Path.GetFileName(FileUpload1.FileName)

        Dim storePath As String = Server.MapPath("~/")
        storePath = storePath + "Attachments\WorkOrderCancellation\" + "CancellationWorkOrder.csv"
        FileUpload1.SaveAs(storePath)

        oFile = File.OpenText(storePath)
        Dim i As Integer
        i = 0

        Dim refwoid As String
        Dim inputContent As String

        Try

            For Each sLine As String In File.ReadAllLines(storePath)
                If i > 0 Then

                    refwoid = sLine

                    If strWOIDStringBuilder.ToString.Length > 0 Then
                        strWOIDStringBuilder.Append(",")
                        strWOIDStringBuilder.Append(refwoid)
                    Else
                        strWOIDStringBuilder.Append(refwoid)
                    End If
                   
                End If
                i += 1
            Next

            ws.WSWorkOrder.WOBulkCancellation(strWOIDStringBuilder.ToString)

            oFile.Close()

        Catch ex As Exception
            oFile.Close()
            lblMessage.Text = "Oops! Something went wrong. Please contact your administrator or try again."
            'File.Delete(storePath)
            Exit Sub
        End Try

        oFile.Close()
        lblMessage.Text = "Sucessfully Cancelled the WorkOrders from the file."
        File.Delete(storePath)

    End Sub
End Class