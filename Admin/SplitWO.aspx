<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :" CodeBehind="SplitWO.aspx.vb" Inherits="Admin.SplitWO" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


	        
		
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
              <asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server">
			<ContentTemplate>
			<div id="divValidationMain" class="divValidation" runat="server" visible="false" >
            <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
              <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
	            <tr>
	              <td height="15" align="center">&nbsp;</td>
	              <td width="565" height="15">&nbsp;</td>
	              <td height="15">&nbsp;</td>
	            </tr>
	            <tr valign="middle">
	              <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
	              <td width="565" class="validationText"><div  id="divValidationMsg"></div>
	              <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
	              <span class="validationText">
	              <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
  	              </span>
	              </td>
	              <td width="20">&nbsp;</td>
	            </tr>
	            <tr>
	              <td height="15" align="center">&nbsp;</td>
	              <td width="565" height="15">&nbsp;</td>
	              <td height="15">&nbsp;</td>
	            </tr>
              </table>
              
              <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
            </div>
            <table width="940"  border="0" align="center" cellpadding="0" cellspacing="0">
			     <tr>
                    <td height="35" class="HeadingRed"><strong> &nbsp;Generate Stages of a Work Order</strong></td>
                  </tr>
            </table> 
			<input id="hdnPricingMethod" type="hidden" runat=server>     
            <asp:Panel ID="pnlSetPrice" runat="server" >
			    <table width="940"  border="0" align="center" cellpadding="0" cellspacing="0">
                   			<tr style="padding:0px 0px 0px 10px; " valign="top">
							  <td>
							     <table>								  
									 <tr>
									 	<td width="195" class="formLabelGrey" valign="bottom">No. of stages<SPAN class="bodytxtValidationMsg">*</SPAN>
										<asp:RequiredFieldValidator id="rqNoOfStages" runat="server" ErrorMessage="Please enter No. of Stages."
														                            ForeColor="#EDEDEB" ControlToValidate="txtNoOfStages">*</asp:RequiredFieldValidator> </td>   
										<td id="tdEstimatedStageTimeReq" runat=server width="200" class="formLabelGrey" valign="bottom">Estimated Stage Time Required<SPAN class="bodytxtValidationMsg">*</SPAN>
										<asp:RequiredFieldValidator id="rqEstimatedInDays" runat="server" ErrorMessage="Please enter Estimated Time in Days."
														                            ForeColor="#EDEDEB" ControlToValidate="txtEstimatedTimeInDays">*</asp:RequiredFieldValidator> 									
										</td>      
									 </tr>
									  <tr style="padding:0px 0px 0px 10px; " valign="top">
							  				<td width="148" class="formLabelGrey" valign="top"><asp:TextBox ID="txtNoOfStages" runat="server" onChange="javascript:CalculatePrice(this.id)" CssClass="formLabelGrey width120" style="text-align:right" Text="2"></asp:TextBox></td>
                                			<td id="tdEstimatedStageTimeReqTxtBox" runat="server" width="200" class="formLabelGrey" valign="top"><asp:TextBox id="txtEstimatedTimeInDays" runat="server" CssClass="formFieldGrey width150" TabIndex="19" onChange="javascript:CalculatePrice(this.id)" Text="1"></asp:TextBox> Days</td>                               
                              		</tr>
								</table>
							</td>
							
							  	                     
                              </tr>
                             
				   <tr><td>&nbsp;</td></tr>
				   <tr><td>&nbsp;</td></tr>
				   <tr style="padding:0px 0px 0px 10px; " valign="top">
							  <td>
							     <table>								  
									 <tr>
									 	<td width="195" class="formLabelGrey" valign="bottom">Date Deferal<SPAN class="bodytxtValidationMsg">*</SPAN>
										<asp:RequiredFieldValidator id="ReqFieldDateDeferal"  runat="server" ErrorMessage="Please enter Date Deferal."
										 ForeColor="#EDEDEB" ControlToValidate="txtDateDeferal">*</asp:RequiredFieldValidator> 
										 <asp:RangeValidator id="RangeDateDeferal" ControlToValidate="txtDateDeferal" MinimumValue="1" MaximumValue="30" Type="Integer" EnableClientScript="false" ForeColor="#EDEDEB"  ErrorMessage = "Date Deferal value must be from 1 to 30" runat="server"/>
    									 </td>   
										 
									 </tr>
									  <tr style="padding:0px 0px 0px 10px; " valign="top">
							  			<td width="148" class="formLabelGrey" valign="top"><asp:TextBox ID="txtDateDeferal" runat="server"  CssClass="formLabelGrey width120" style="text-align:right" Text="2"></asp:TextBox></td>
                                			
                              		</tr>
								</table>
							</td>
							
							  	                     
                              </tr>
				   <tr><td>&nbsp;</td></tr>
				   <tr><td>&nbsp;</td></tr>
				  <tr>
                    <td> <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr valign="top">
                              <tr valign="top">
							  	<td width="148" class="formLabelGrey" valign="bottom">&nbsp;&nbsp;&nbsp;&nbsp;Wholesale Price 
								<asp:RegularExpressionValidator id="RgValPositiveNumber1" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtWholesalePrice"
                        			ErrorMessage="Please enter a positive whole number for Wholesale Price." ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator>						 							
								</td>
                                <td width="165" class="formLabelGrey" valign="bottom">&nbsp;WO Wholesale Stage Price</td>
                                <td id="tdWOWholesaleDayRatePrice" runat=server width="163" class="formLabelGrey" valign="bottom">WO Wholesale Day Rate Price</td>
                                <td width="180" class="formLabelGrey" valign="bottom"></td>
								
                                
                              </tr>
                              <tr valign="top">
							  	<td width="148" class="formLabelGrey" valign="top">&pound; <asp:TextBox ID="txtWholesalePrice" runat="server" CssClass="formLabelGrey width120" style="text-align:right" Enabled="false"></asp:TextBox></td>
                                <td width="165" class="formLabelGrey" valign="top">&pound; <asp:TextBox id="txtWholesaleStagePrice" runat="server" CssClass="formFieldGrey width150" TabIndex="19"></asp:TextBox></td>
                                <td id="tdWOWholesaleDayRatePriceTxtBox" runat=server width="163" class="formLabelGrey" valign="top">&pound; <asp:TextBox id="txtWholesaleDayRatePrice" runat="server" CssClass="formFieldGrey width150" TabIndex="20" Enabled="false"></asp:TextBox></td>
                                <td width="180" class="formLabelGrey" valign="top"></td>
								
                               
                              </tr>
							  <tr><td>&nbsp;</td></tr>
							  							  <tr><td>&nbsp;</td></tr>
							  
							   <tr valign="top">
							  	<td width="148" class="formLabelGrey" valign="bottom">&nbsp;&nbsp;&nbsp;&nbsp;Portal Price 
								<asp:RegularExpressionValidator id="RgValPositiveNumber2" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtWholesalePrice"
                        			ErrorMessage="Please enter a positive whole number for Wholesale Price." ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator>						 							
								</td>
                                <td width="165" class="formLabelGrey" valign="bottom">&nbsp;WO Platform Stage Price</td>
                                <td id="tdWOPlatformDayRatePrice" runat=server width="163" class="formLabelGrey" valign="bottom">WO Platform Day Rate Price</td>
                                <td width="180" class="formLabelGrey" valign="bottom"></td>
								
                                
                              </tr>
                              <tr valign="top">
							  	<td width="148" class="formLabelGrey" valign="top">&pound; <asp:TextBox ID="txtPlatformPrice" runat="server" CssClass="formLabelGrey width120" style="text-align:right" Enabled="false"></asp:TextBox></td>
                                <td width="165" class="formLabelGrey" valign="top">&pound; <asp:TextBox id="txtPlatformStagePrice" runat="server" CssClass="formFieldGrey width150" TabIndex="19"></asp:TextBox></td>
                                <td id="tdWOPlatformDayRatePriceTxtBox" runat=server width="163" class="formLabelGrey" valign="top">&pound; <asp:TextBox id="txtPlatformDayRatePrice" runat="server" CssClass="formFieldGrey width150" TabIndex="20" Enabled="false"></asp:TextBox></td>
                                <td width="180" class="formLabelGrey" valign="top"></td>
								
                               
                              </tr>
							  
							 
                            </table>  </td>
                  </tr>
				  <tr>
                    <td>&nbsp;</td>
                  </tr>
				   
				  <tr>
                    <td>&nbsp;</td>
                  </tr>
                 
                </table>	
			    <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
				    <tr>
					    <td width="475">&nbsp;</td>
					    <td>&nbsp;</td>
					   
				    </tr>
				    <tr>
						    
					    <td width="545" align="right" valign="top">
						      <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0" runat="server" id="tblConfirm">
							    <TR>
							      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
							      <TD class="txtBtnImage" width="60">
							      <asp:LinkButton ID="lnkSubmit" CausesValidation="false" runat="server" CssClass="txtListing" Text="<img src='Images/Icons/Icon-Confirm.gif' title='Submit' width='12' height='12' align='absmiddle' hspace='1' vspace='2' border='0'> Submit"></asp:LinkButton></TD>
							      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
							    </TR>
					      </TABLE>
					    </td>
						
					    <td align="left">
					      <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
						    <TR>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
						      <TD class="txtBtnImage">
						      <asp:LinkButton ID="lnkCancel" CausesValidation="false" runat="server" CssClass="txtListing" Text="<img src='Images/Icons/Cancel.gif' title='Cancel' width='11' height='11' align='absmiddle' hspace='1' vspace='2' border='0'> Cancel"></asp:LinkButton></TD>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
						    </TR>
					      </TABLE>
					    </td>
					   
			       </tr>
			    </table>	
			</asp:Panel>	
			<asp:Panel ID="pnlWarning" runat="server" Visible="false">
			    <table   border="0" cellspacing="0" cellpadding="10">
                  <tr>
				  <td width="5">&nbsp;</td>
                    <td colspan="2"><asp:Label ID="lblWarning" runat="server" CssClass="BodyTxtValidationMsg" Text="Warning � this step to generate stages is irreversible.  Click �Next� to continue or �Cancel� to return back to the WO Stage Generation listing"></asp:Label></td>
					
					</tr>
					<tr>
					<td width="5">&nbsp;</td>
                     <td align="right" style="padding:0px 0px 0px 350px;">
					      <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
						    <TR>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
						      <TD width="60" class="txtBtnImage">
    						      <asp:LinkButton ID="lnkNext" CausesValidation="false" runat="server" CssClass="txtListing" Text="<img src='Images/Icons/Icon-Confirm.gif' title='Submit' width='12' height='12' align='absmiddle' hspace='1' vspace='2' border='0'> Next"></asp:LinkButton></TD>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
						    </TR>
					      </TABLE>
					    </td>
						
						<td align="right">
					      <TABLE cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
						    <TR>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
						      <TD width="60" class="txtBtnImage">
    						      <asp:LinkButton ID="lnkCancel2" CausesValidation="false" runat="server" CssClass="txtListing" Text="<img src='Images/Icons/Cancel.gif' title='Cancel' width='11' height='11' align='absmiddle' hspace='1' vspace='2' border='0'> Cancel"></asp:LinkButton></TD>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
						    </TR>
					      </TABLE>
					    </td>
                  </tr>
                </table>
			    
			</asp:Panel> 
			<asp:Panel ID="pnlSuccess" runat="server" Visible="false">
			    <table   border="0" cellspacing="0" cellpadding="10">
                  <tr>
				  <td width="10">&nbsp;</td>
                    <td><asp:Label ID="lblSuccess" runat="server" CssClass="formLabelGrey" Text="Stages for the Work Order are succesfully generated"></asp:Label></td>
                     <td align="right">
					      <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
						    <TR>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
						      <TD width="50" class="txtBtnImage" align="center">
    						      <asp:LinkButton ID="lnkBack" CausesValidation="false" runat="server" CssClass="txtListing" Text="Back"></asp:LinkButton></TD>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
						    </TR>
					      </TABLE>
					    </td>
                  </tr>
                </table>
			    
			</asp:Panel> 
			</ContentTemplate>
			  <Triggers>  	
				<asp:AsyncPostBackTrigger EventName="Click" ControlID="lnkSubmit" />	
			  </Triggers>
			</asp:UpdatePanel>
			
			 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelCreateWorkOrder" runat="server">
					<ProgressTemplate>
						<div>
							<img  align=middle src="Images/indicator.gif" />
							Please Wait...
						</div>      
					</ProgressTemplate>
			</asp:UpdateProgress>
			<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePanelCreateWorkOrder" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />

<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
         </asp:Content>