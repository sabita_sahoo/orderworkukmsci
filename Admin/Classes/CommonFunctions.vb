Imports System.Net
Imports System.Web.Mail
Imports System.Xml
Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.tool.xml
Imports Newtonsoft.Json.Linq
'Imports System.Linq
Imports System.Linq
Imports System.Security.Policy

''' <summary>
''' Common utility functions class shared by all projects
''' </summary>
''' <remarks></remarks>
Public Class CommonFunctions

    ''' <summary>
    ''' Shared wobject to access web service webmethods
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs


    ''' <summary>
    ''' Validate mail exist and migration
    ''' </summary>
    ''' <param name="mailID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ValidateMailAndMigration(ByVal mailID As String) As String
        Dim str As String = ""
        Dim Msg1 As String = ""
        Dim migrationStatusDisabled As Boolean = False
        Dim responseText = CommonFunctions.CheckAccountExistAndMigration(mailID, True)
        If responseText = "" Then
            Return ""
        End If
        Dim action As String = CommonFunctions.getXMLElementValue(responseText, "action")
        Select Case action
            Case "Exists"
                str = ResourceMessageText.GetString("EmailAlreadyExist")
            Case "nonMsSupplier"
                Select Case ApplicationSettings.OWUKBizDivId
                    Case ApplicationSettings.SFUKBizDivId
                        str = ResourceMessageText.GetString("SupplierAccExist")
                        str += ResourceMessageText.GetString("DontHaveMSCertForComp")
                        str += ResourceMessageText.GetString("ContactOWRep")
                End Select
            Case "typeBuyer"
                Select Case ApplicationSettings.OWUKBizDivId
                    Case ApplicationSettings.SFUKBizDivId
                        str = ResourceMessageText.GetString("BuyerAccAlreadyExist")
                        str += ResourceMessageText.GetString("ContactOW")
                        str = str.Replace("<MigrateSite>", ApplicationSettings.OtherSiteName)
                End Select
            Case "userNotActive", "companySuspended"
                str = ResourceMessageText.GetString("AccAlreadyExistOnSite")
                str = str.Replace("<siteName>", ApplicationSettings.OtherSiteName)
                str = str.Replace("<siteURL>", ApplicationSettings.OtherWebPath)
                str += ResourceMessageText.GetString("AccInactive")
            Case "nonAdmin"
                str = ResourceMessageText.GetString("AccAlreadyExistOnSite")
                str = str.Replace("<siteURL>", ApplicationSettings.OtherWebPath)
                str += ResourceMessageText.GetString("NoAdminPrivilegeToMigrate")
                str = str.Replace("<siteName>", ApplicationSettings.OtherSiteName)
                str += ResourceMessageText.GetString("ContactOWRep")
            Case "Migrate"
                If CommonFunctions.getXMLElementValue(responseText, "disabled") = "true" Then
                    migrationStatusDisabled = True
                End If
                str = ResourceMessageText.GetString("AccAlreadyExistOnSite")
                str = str.Replace("<siteName>", ApplicationSettings.OtherSiteName)
                str = str.Replace("<siteURL>", ApplicationSettings.OtherWebPath)

                If migrationStatusDisabled = True Then
                    str += ResourceMessageText.GetString("ContactOW")
                    str = str.Replace("<MigrateSite>", ApplicationSettings.OtherSiteName)
                Else
                    str += "Account Details:</b>"
                    str += "Company Name:</b>&nbsp;&nbsp;" + CommonFunctions.getXMLElementValue(responseText, "companyName") + "</br>"
                    str += "Admin:</b>&nbsp;&nbsp;" + CommonFunctions.getXMLElementValue(responseText, "admin") + "</br>"
                    str += "Admin Email:</b>&nbsp;&nbsp;" + CommonFunctions.getXMLElementValue(responseText, "adminMail") + "</br>"
                    If CommonFunctions.getXMLElementValue(responseText, "noRegLoc") <> "" Then
                        str += "No of registered locations:</b>&nbsp;&nbsp;" + CommonFunctions.getXMLElementValue(responseText, "noRegLoc") + "</br>"
                    End If
                    str += "No of registered users:</b>&nbsp;&nbsp;" + CommonFunctions.getXMLElementValue(responseText, "noRegUser") + "</br><br/>"
                    str += ResourceMessageText.GetString("ClickToConfirmA") + "' href='" + CommonFunctions.getXMLElementValue(responseText, "migrateLink") + "'>confirm</a> " + ResourceMessageText.GetString("ClickToConfirmB")  '"'>confirm</a> your identity to migrate your Skills Finder account to Order Work.<br/>"
                    str += ResourceMessageText.GetString("ContinueWithDiffEmail")
                    str = str.Replace("<site>", ApplicationSettings.SiteName)
                    str = str.Replace("<fromSite>", ApplicationSettings.SiteName)
                    str = str.Replace("<toSite>", ApplicationSettings.OtherSiteName)
                End If

                Return str
            Case Else
                Return ""
        End Select
        Return str
    End Function

    ''' <summary>
    ''' Function to check for Account Exist before migration
    ''' </summary>
    ''' <param name="Email"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CheckAccountExistAndMigration(ByVal Email As String, ByVal isRegisterPage As Boolean) As String
        Dim str As String = ""
        Dim adminName As String = ""
        Dim ds As DataSet
        Dim dv As DataView
        Dim statusId As String

        If Trim(Email) = "" Then
            Return ""
        End If
        ds = ws.WSContact.GetAccountMigrationDetails(Email, isRegisterPage)

        ' If email already exist
        If ds.Tables("contactBizDivLinkage").DefaultView.Count > 0 Then

            If isRegisterPage Then
                ds.Tables("contactBizDivLinkage").DefaultView.RowFilter = "BizDivID =" & ApplicationSettings.OWUKBizDivId
            Else

                Select Case ApplicationSettings.Country
                    Case ApplicationSettings.CountryDE
                        Select Case ApplicationSettings.OWUKBizDivId
                            Case ApplicationSettings.OWDEBizDivId
                                ds.Tables("contactBizDivLinkage").DefaultView.RowFilter = "BizDivID =" & ApplicationSettings.OWDEPeer1BizDivId
                        End Select
                    Case ApplicationSettings.CountryUK
                        Select Case ApplicationSettings.OWUKBizDivId
                            Case ApplicationSettings.OWUKBizDivId
                                ds.Tables("contactBizDivLinkage").DefaultView.RowFilter = "BizDivID =" & ApplicationSettings.OWUKPeer1BizDivId
                            Case ApplicationSettings.SFUKBizDivId
                                ds.Tables("contactBizDivLinkage").DefaultView.RowFilter = "BizDivID =" & ApplicationSettings.SFUKPeer1BizDivId
                        End Select
                End Select

            End If



            If ds.Tables("contactBizDivLinkage").DefaultView.Count > 0 Then
                str = "<action>Exists</action>"
                Return str
            End If
        End If
        'If ApplicationSettings.MigrationSwitchStatus = "1" Then
        str += "<disabled>false</disabled>"
        'Else ' Migration is disabled
        'str += "<disabled>true</disabled>"
        'End If
        If ds.Tables("user").DefaultView.Count > 0 Then


            ' Check for User type and certifications this check is implemented for Skills Finder Sites only
            ' Check for buyer if user is buyer then dnt allow to register
            If isRegisterPage Then
                If ApplicationSettings.OWUKBizDivId = ApplicationSettings.SFUKBizDivId Then
                    ds.Tables("contactClasses").DefaultView.RowFilter = "ClassID=" & ApplicationSettings.RoleSupplierID
                    If ds.Tables("contactClasses").DefaultView.Count > 0 Then
                        ' check if user has MS certification                 
                        dv = ds.Tables("ContactAccreditations").Copy.DefaultView
                        If dv.Count = 0 Then
                            str = "<action>nonMsSupplier</action>"
                            Return str
                        End If
                    Else
                        str = "<action>typeBuyer</action>"
                        Return str
                    End If
                End If
            Else ' for home page Migration UC Check
                If ApplicationSettings.OWUKBizDivId = ApplicationSettings.OWUKBizDivId Or ApplicationSettings.OWUKBizDivId = ApplicationSettings.OWDEBizDivId Then
                    ds.Tables("contactClasses").DefaultView.RowFilter = "ClassID=" & ApplicationSettings.RoleSupplierID
                    If ds.Tables("contactClasses").DefaultView.Count > 0 Then
                        ' check if user has MS certification                 
                        dv = ds.Tables("ContactAccreditations").Copy.DefaultView
                        If dv.Count = 0 Then
                            str = "<action>nonMsSupplier</action>"
                            Return str
                        End If
                    Else
                        str = "<action>typeBuyer</action>"
                        Return str
                    End If
                End If
            End If

            statusId = ApplicationSettings.StatusActive

            'Check for account Status- contact
            If Not ds.Tables("contactBizDivLinkage") Is Nothing Then
                If ds.Tables("contactBizDivLinkage").Rows(0)("Status") <> statusId Then
                    'User account is not active
                    str = "<action>userNotActive</action>"
                    Return str
                End If
            End If
            statusId = ApplicationSettings.StatusSuspendedCompany
            'Check for account Status- company
            If Not ds.Tables("companyBizDivLinkage") Is Nothing Then
                If ds.Tables("companyBizDivLinkage").DefaultView.Count > 0 Then
                    If ds.Tables("companyBizDivLinkage").Rows(0)("Status") = statusId Then
                        ' Company account is suspended
                        str = "<action>companySuspended</action>"
                        Return str
                    End If
                End If
            End If

            Dim dsRollgrp As DataSet = ws.WSSecurity.GetRolesGroups("")
            dsRollgrp.Tables(0).DefaultView.RowFilter = "RoleGroupName = 'Administrator'"
            dv = dsRollgrp.Tables(0).DefaultView
            Dim adminFlag As Boolean = False
            Dim i As Integer = 0
            'Check for administrative privileges
            For i = 0 To dv.Count - 1
                If dv.Item(i)("RoleGroupID") = ds.Tables("contactRoleLinkage").Rows(0)("RoleGroupID") Then
                    adminFlag = True
                    Exit For
                End If
            Next
            ' User account dont have administrative privileges
            If adminFlag = False Then
                str = "<action>nonAdmin</action>"
                Return str
            End If

            'Action
            str &= "<action>Migrate</action>"
            'Company Name
            ds.Tables("companyAttributes").DefaultView.RowFilter = "AttributeLabel = 'CompanyName'"
            If Not IsDBNull(ds.Tables("companyAttributes").DefaultView.Item(0)("AttributeValue")) Then
                str &= "<companyName>" & ds.Tables("companyAttributes").DefaultView.Item(0)("AttributeValue") & "</companyName>"
            Else
                str &= "<companyName>" & "" & "</companyName>"
            End If

            'Admin Name
            ds.Tables("contactAttributes").DefaultView.RowFilter = "AttributeLabel = 'FName'"
            If Not IsDBNull(ds.Tables("contactAttributes").DefaultView.Item(0)("AttributeValue")) Then
                adminName = ds.Tables("contactAttributes").DefaultView.Item(0)("AttributeValue")
            End If
            ds.Tables("contactAttributes").DefaultView.RowFilter = "AttributeLabel = 'LName'"
            If Not IsDBNull(ds.Tables("contactAttributes").DefaultView.Item(0)("AttributeValue")) Then
                adminName &= "&nbsp;&nbsp;" & ds.Tables("contactAttributes").DefaultView.Item(0)("AttributeValue")
            End If
            str &= "<admin>" & adminName & "</admin>"
            'Admin Email            
            str &= "<adminMail>" & Email & "</adminMail>"
            ' No of registered location
            ds.Tables("companyAttributes").DefaultView.RowFilter = "AttributeLabel = 'NoOfLocations'"
            If ds.Tables("companyAttributes").DefaultView.Count > 0 Then
                If Not IsDBNull(ds.Tables("companyAttributes").DefaultView.Item(0)("AttributeValue")) Then
                    str &= "<noRegLoc>" & ds.Tables("companyAttributes").DefaultView.Item(0)("AttributeValue") & "</noRegLoc>"
                Else
                    str &= "<noRegLoc>" & "" & "</noRegLoc>"
                End If
            Else
                str &= "<noRegLoc>" & "" & "</noRegLoc>"
            End If

            'No of registered users    
            If Not IsDBNull(ds.Tables("spcialistCount").Rows(0)("TotalSpecialists")) Then
                str &= "<noRegUser>" & ds.Tables("spcialistCount").Rows(0)("TotalSpecialists") & "</noRegUser>"
            Else
                str &= "<noRegUser>" & "" & "</noRegUser>"
            End If
            'migrateLink
            str &= "<migrateLink>" & ApplicationSettings.WebPath & ApplicationSettings.OuterMigratePage & "?accountId=" & Email & "</migrateLink>"

            Return str
        Else
        End If
        Return str
    End Function

    ''' <summary>
    ''' Extracts the values from trhe XML tags
    ''' </summary>
    ''' <param name="XMLStr"></param>
    ''' <param name="atrName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getXMLElementValue(ByVal XMLStr As String, ByVal atrName As String)
        Try
            If XMLStr.IndexOf("<" + atrName + ">") > -1 Then
                Return XMLStr.Substring(XMLStr.IndexOf("<" + atrName + ">") + atrName.Length + 2, XMLStr.IndexOf("</" + atrName + ">") - XMLStr.IndexOf("<" + atrName + ">") - atrName.Length - 2)
            Else
                Return ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Function to get the standards for the RAQ form and store them in cache
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRAQWorkRequestStandards(ByRef page As Page, ByVal BizDivId As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        Dim cacheKey As String = ""
        cacheKey = "Standard-RAQ-Session" & "-" & page.Session.SessionID
        If killcache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            ds = ws.WSWorkOrder.woRAQGetStandards(BizDivId)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Common function to get the standards for the create workorder form
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCreateWorkRequestStandards(ByRef page As Page, ByVal BizDivId As Integer, ByVal ContactID As Integer, ByVal WOID As Integer, Optional ByVal CompanyId As Integer = 0, Optional ByVal killcache As Boolean = False) As DataSet
        Dim cacheKey As String = ""
        cacheKey = "Standard-WOID" & WOID & "-Session" & "-" & page.Session.SessionID

        If killcache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            ds = ws.WSWorkOrder.MS_WOCreateWOGetStandards(BizDivId, ContactID, WOID, CompanyId)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function


    ''' <summary>
    ''' Common function to get the standards for the create workorder form    
    ''' </summary>
    ''' <param name="ContactID"></param>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCreateWorkRequestDetails(ByVal ContactID As Integer, ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        Dim ds As DataSet
        ds = ws.WSWorkOrder.MS_WOCreateWOGetDetails(ContactID, WOID, CompanyID)
        Return ds
    End Function

    ''' <summary>
    ''' Common function to get locations for the workorder form
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="companyId"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetLocations(ByRef page As Page, ByVal companyId As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        Dim cacheKey As String = ""
        cacheKey = "Location-Company" & "-" & "-" & companyId & "-SessionID-" & page.Session.SessionID

        If killcache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            ws.WSContact.Timeout = 300000
            ds = ws.WSWorkOrder.woGetContactsLocations(companyId)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Common function to get Billing locations for the workorder form
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="companyId"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetBillingLocations(ByRef page As Page, ByVal companyId As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        Dim ds As DataSet
        ds = ws.WSWorkOrder.woGetContactsBillingLocations(companyId)
        Return ds
    End Function
#Region "AOE"
    ''' <summary>
    ''' To fetch  all category types defined in table "tblStandardsWOTypesLinkage"
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="BizDivID"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetMainCategoriesTypes(ByRef page As Page, ByVal BizDivID As Integer, Optional ByVal KillCache As Boolean = False) As DataSet
        Dim CacheKey As String = "MainCategoriesTypes" + CStr(BizDivID)
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = ws.WSStandards.GetMainCategoryTypes(BizDivID)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function
    ''' <summary>
    ''' To fetch  all category types defined in table "tblStandardsWOTypesLinkage"
    ''' </summary> 
    ''' <param name="page"></param>
    ''' <param name="BizDivID"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSubCategoriesTypes(ByRef page As Page, ByVal mainCatId As Integer, ByVal BizDivId As Integer, Optional ByVal KillCache As Boolean = False) As DataSet
        Dim CacheKey As String = "SubCategoriesTypes" + CStr(mainCatId) + CStr(BizDivId)
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = ws.WSStandards.GetSubCategoryTypes(mainCatId, BizDivId)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function
    ''' <summary>
    ''' To fetch  all category for a contact in bizdiv from tblcontacts attributes as combid
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="BizDivID"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetContactsCategories(ByRef page As Page, ByVal combIds As String, ByVal paramBizDivId As Integer, Optional ByVal KillCache As Boolean = False) As DataSet
        Dim CacheKey As String = "ContactsCategories" + page.Session.SessionID + CStr(paramBizDivId)
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = ws.WSContact.GetContactsCategories(combIds, paramBizDivId)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function
    Public Shared Function GetContactsAOE(ByVal combIds As String, ByVal paramBizDivId As Integer) As DataSet
        Dim ds As DataSet
        ds = ws.WSContact.GetContactsAOE(combIds, paramBizDivId)
        Return ds
    End Function
    ''' <summary>
    ''' To fetch   maincat ,subcat ,maincatname,subcatname for a combid in a bizdiv
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCategoryCombDetails(ByRef page As Page, ByVal combId As Integer, ByVal paramBizDivId As Integer, Optional ByVal KillCache As Boolean = False) As DataView
        Dim CacheKey As String = "BizDivCategories" + CStr(paramBizDivId)
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = ws.WSContact.GetCategoriesForBizDivId(paramBizDivId)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Dim dv As DataView = ds.Tables("tblCombIds").DefaultView
        dv.RowFilter = "CombID=" & combId
        Return dv
    End Function
#End Region

    ''' <summary>
    ''' Get the standard data for Ordermatch form: Regions and AOE
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="cacheKey"></param>
    ''' <param name="bizDivId"></param>
    ''' <param name="countryID"></param>
    ''' <param name="killCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function OrderMatch_GetStandards(ByRef page As Page, ByVal cacheKey As String, ByVal bizDivId As Integer, Optional ByVal countryID As Integer = 0, Optional ByVal killCache As Boolean = False) As DataSet
        If killCache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            ds = ws.WSWorkOrder.woOrderMatchGetStandards(countryID, bizDivId)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function


    ''' <summary>
    ''' Common function to iterate through the grid and get the list of the selected ids...
    ''' </summary>
    ''' <param name="gv"></param>
    ''' <param name="checkboxId"></param>
    ''' <param name="hdnId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getSelectedIdsOfGrid(ByVal gv As GridView, ByVal checkboxId As String, ByVal hdnId As String) As String
        Dim selectedIds As String = ""
        Dim status As String = ""
        For Each row As GridViewRow In gv.Rows
            Dim chkBox As CheckBox = CType(row.FindControl(checkboxId), CheckBox)
            If Not (chkBox Is Nothing) Then
                If chkBox.Checked Then
                    If selectedIds.Contains(CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value)) = False Then
                        If selectedIds = "" Then
                            selectedIds += "" & CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value)
                        Else
                            selectedIds += " ," & CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value)
                        End If
                    End If
                End If
            End If

        Next
        Return selectedIds
    End Function
    ''' <summary>
    ''' Common function to iterate through the grid and get the list of the selected ids...
    ''' </summary>
    ''' <param name="gv"></param>
    ''' <param name="checkboxId"></param>
    ''' <param name="hdnId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getSelectedIdsOfGridFavSP(ByVal gv As GridView, ByVal checkboxId As String, ByVal hdnId As String, ByVal hdnBillLocId As String) As String
        Dim selectedIds As String = ""
        Dim status As String = ""
        For Each row As GridViewRow In gv.Rows
            Dim chkBox As CheckBox = CType(row.FindControl(checkboxId), CheckBox)
            If Not (chkBox Is Nothing) Then
                If chkBox.Checked Then
                    'If selectedIds.Contains(CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value)) = False Then
                    If selectedIds = "" Then
                        selectedIds += "" & CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value) & "#" & CStr(CType(row.FindControl(hdnBillLocId), HtmlInputHidden).Value)

                    Else
                        selectedIds += " ," & CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value) & "#" & CStr(CType(row.FindControl(hdnBillLocId), HtmlInputHidden).Value)
                    End If
                    'End If
                End If
            End If

        Next
        Return selectedIds
    End Function
    Public Shared Function getSelectedIdsOfGridForPIUnPaid(ByVal gv As GridView, ByVal checkboxId As String, ByVal hdnId As String, ByVal hdnStatus As String, ByVal action As String) As String
        Dim selectedIds As String = ""
        Dim status As String = ""
        For Each row As GridViewRow In gv.Rows
            Dim chkBox As CheckBox = CType(row.FindControl(checkboxId), CheckBox)
            If Not (chkBox Is Nothing) Then
                If chkBox.Checked Then
                    If selectedIds.Contains(CStr(CType(row.FindControl(hdnStatus), HtmlInputHidden).Value)) = False Then
                        status = CType(row.FindControl(hdnStatus), HtmlInputHidden).Value
                        If (action = "available") Then
                            If status = "Unpaid" Or status = "On Hold" Then
                                If selectedIds.Contains(CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value)) = False Then
                                    If selectedIds = "" Then
                                        selectedIds += "" & CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value)
                                    Else
                                        selectedIds += " ," & CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value)
                                    End If
                                End If
                            Else
                                selectedIds = "error"
                                Exit For
                            End If
                        ElseIf (action = "hold") Then
                            If status = "Available" Or status = "Requested" Or status = "Unpaid" Then
                                If selectedIds.Contains(CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value)) = False Then
                                    If selectedIds = "" Then
                                        selectedIds += "" & CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value)
                                    Else
                                        selectedIds += " ," & CStr(CType(row.FindControl(hdnId), HtmlInputHidden).Value)
                                    End If
                                End If
                            Else
                                selectedIds = "error"
                                Exit For
                            End If
                        End If
                    End If
                End If
            End If
        Next
        Return selectedIds
    End Function

    ''' <summary>
    ''' Function GetCommonStandards to fetch standards for no of employees ,Distance,Response time,partner level,Skills And certificate,Regions,Security Question,Country ,skills
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetGeneralStandards(ByRef page As Page, Optional ByVal killcache As Boolean = False) As DataSet
        Dim CacheKey As String
        CacheKey = "Lookup_GeneralStandards"
        If killcache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As New DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = ws.WSStandards.GetGeneralStandards()
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Function to populate security question
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlQuestion"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateSecurityQues(ByVal page As Page, ByRef ddlQuestion As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel = 'SecurityQuest'"
        ddlQuestion.DataSource = dsView
        ddlQuestion.DataTextField = "StandardValue"
        ddlQuestion.DataValueField = "StandardID"
        ddlQuestion.DataBind()
        ddlQuestion.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectQuestion"), ""))
    End Sub
    ''' <summary>
    ''' Function to populate Dixons Complaint Type
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlDixonsComplaintType"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateDixonsComplaintType(ByVal page As Page, ByRef ddlDixonsComplaintType As DropDownList, ByRef ShowIsActive As Boolean)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        If (ShowIsActive = True) Then
            dsView.RowFilter = "StandardLabel = 'Client Complaints' AND IsActive = 1"
        Else
            dsView.RowFilter = "StandardLabel = 'Client Complaints'"
        End If
        ddlDixonsComplaintType.DataSource = dsView
        ddlDixonsComplaintType.DataTextField = "StandardValue"
        ddlDixonsComplaintType.DataValueField = "StandardID"
        ddlDixonsComplaintType.DataBind()

    End Sub

    Public Shared Sub PopulateFeedbackType(ByVal page As Page, ByRef ddlFeedbackType As DropDownList, ByRef ShowIsActive As Boolean)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        If (ShowIsActive = True) Then
            dsView.RowFilter = "StandardLabel = 'Service Partner Feedback' AND IsActive = 1"
        Else
            dsView.RowFilter = "StandardLabel = 'Service Partner Feedback'"
        End If
        ddlFeedbackType.DataSource = dsView
        ddlFeedbackType.DataTextField = "StandardValue"
        ddlFeedbackType.DataValueField = "StandardID"
        ddlFeedbackType.DataBind()

    End Sub

    ''' <summary>
    ''' Procedure to populate AOE Main Cat drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlAOE"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateAOEMainCat(ByVal page As Page, ByRef ddlAOE As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(3).Copy.DefaultView
        dsView.RowFilter = "BizDivId=" & ApplicationSettings.BizDivId
        ddlAOE.DataSource = dsView
        ddlAOE.DataTextField = "Name"
        ddlAOE.DataValueField = "MainCatID"
        ddlAOE.DataBind()
        ddlAOE.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Skills Category", ""))
    End Sub

    ''' <summary>
    ''' Procedure to populate AOE Main Cat drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlSkills"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateRegions(ByVal page As Page, ByRef ddlSkills As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(2).Copy.DefaultView
        ddlSkills.DataSource = dsView
        ddlSkills.DataTextField = "RegionName"
        ddlSkills.DataValueField = "RegionID"
        ddlSkills.DataBind()
        ddlSkills.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Region", ""))
    End Sub

    ''' <summary>
    ''' Class having methods to add and post the parameters to destination page
    ''' </summary>
    ''' <remarks></remarks>
    Public Class RemotePost
        Private Inputs As System.Collections.Specialized.NameValueCollection = New System.Collections.Specialized.NameValueCollection

        Public Url As String = ""
        Public Method As String = "post"
        Public FormName As String = "form1"
        ''' <summary>
        ''' Add parameters to post
        ''' </summary>
        ''' <param name="name"></param>
        ''' <param name="value"></param>
        ''' <remarks></remarks>
        Public Sub Add(ByVal name As String, ByVal value As String)
            Inputs.Add(name, value)
        End Sub
        ''' <summary>
        ''' senfds a post request to specified page
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Post()
            System.Web.HttpContext.Current.Response.Clear()
            System.Web.HttpContext.Current.Response.Write("<html><head>")
            System.Web.HttpContext.Current.Response.Write(String.Format("</head><body onload=""setTimeout('document.{0}.submit()', 1000)"">", FormName))
            System.Web.HttpContext.Current.Response.Write(String.Format("<form name=""{0}"" method=""{1}"" action=""{2}"" >", FormName, Method, Url))
            Dim i As Integer = 0
            Do While i < Inputs.Keys.Count
                System.Web.HttpContext.Current.Response.Write(String.Format("<input name=""{0}"" type=""hidden"" value=""{1}"">", Inputs.Keys(i), Inputs(Inputs.Keys(i))))
                i += 1
            Loop
            System.Web.HttpContext.Current.Response.Write(" Loading...<img height='16' width='16' src='" & ApplicationSettings.OrderWorkMyURL & "/Images/indicator.gif'/>")
            System.Web.HttpContext.Current.Response.Write("</form>")
            System.Web.HttpContext.Current.Response.Write("</body></html>")
            System.Web.HttpContext.Current.Response.End()
        End Sub
    End Class


    ''' <summary>
    ''' This functions accepts the destination site and page to login and redirects user
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="loginTo"></param>
    ''' <param name="redirectURL"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function AutoLogin(ByVal page As Page, ByVal loginTo As Integer, ByVal redirectURL As String) As Boolean
        Dim myremotepost As RemotePost = New RemotePost
        Select Case ApplicationSettings.SiteType

            Case ApplicationSettings.siteTypes.admin
                Select Case ApplicationSettings.Country
                    Case ApplicationSettings.CountryUK
                        Select Case loginTo
                            Case ApplicationSettings.OWUKBizDivId
                                myremotepost.Url = ApplicationSettings.OrderWorkUKAdminURL & redirectURL
                            Case ApplicationSettings.SFUKBizDivId
                                myremotepost.Url = ApplicationSettings.SkillsFinderUKAdminURL & redirectURL

                        End Select
                    Case ApplicationSettings.CountryDE
                End Select

            Case ApplicationSettings.siteTypes.site
                Select Case ApplicationSettings.Country
                    Case ApplicationSettings.CountryUK
                        Select Case loginTo
                            Case ApplicationSettings.OWUKBizDivId
                                myremotepost.Url = ApplicationSettings.OrderWorkMyURL & "SecurePages/" + redirectURL
                            Case ApplicationSettings.SFUKBizDivId
                                myremotepost.Url = ApplicationSettings.SkillsFinderUKURL & "SecurePages/" + redirectURL
                        End Select
                    Case ApplicationSettings.CountryDE
                        Select Case loginTo
                            Case ApplicationSettings.OWDEBizDivId
                                myremotepost.Url = ApplicationSettings.OrderWorkDEURL & "SecurePages/" + redirectURL
                        End Select
                End Select
        End Select


        myremotepost.Add("userName", page.Session("UserName"))
        myremotepost.Add("password", page.Session("Password"))
        page.Session.Abandon()
        page.Session.Clear()
        myremotepost.Post()

    End Function


    ''' <summary>
    ''' Procedure to populate communication preferences
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlCommunication"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateCommunicationPref(ByVal page As Page, ByRef ddlCommunication As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel = 'CommunicationPref'"
        ddlCommunication.DataSource = dsView
        ddlCommunication.DataTextField = "StandardValue"
        ddlCommunication.DataValueField = "StandardID"
        ddlCommunication.DataBind()
        ddlCommunication.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectPreference"), ""))
    End Sub

    ''' <summary>
    ''' Procedure to populate Distance drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlTravelDistance"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateDistance(ByVal page As Page, ByRef ddlTravelDistance As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel = 'TravelDistance'"
        ddlTravelDistance.DataSource = dsView
        ddlTravelDistance.DataTextField = "StandardValue"
        ddlTravelDistance.DataValueField = "StandardID"
        ddlTravelDistance.DataBind()
        ddlTravelDistance.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectTravelDistance"), ""))
    End Sub



    ''' <summary>
    ''' Procedure to populate role type i.e Administrator, Manager , Specialist , User.
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="dropDownListID"></param>
    ''' <param name="classID"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateUserType(ByVal page As Page, ByRef dropDownListID As DropDownList, ByVal classID As Integer, Optional ByVal filter As String = "")
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(6).Copy.DefaultView
        dsView.RowFilter = "RoleCategory='" & IIf(classID = ApplicationSettings.RoleSupplierID, "Supplier", "Client") & "'"
        If filter <> "" Then
            Dim dt As New DataTable
            dt = dsView.ToTable
            dsView = dt.DefaultView
            dsView.RowFilter = "RoleGroupID not in (1,4)"
        End If
        If Not IsNothing(HttpContext.Current.Session("BusinessArea")) Then
            If HttpContext.Current.Session("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                dropDownListID.DataSource = dsView
                dropDownListID.DataTextField = "RoleGroupName"
                dropDownListID.DataValueField = "RoleGroupID"
                dropDownListID.DataBind()
                dropDownListID.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectUserType"), 0))
            Else
                Dim dsUserType As DataView
                dsUserType = dsView.ToTable.DefaultView
                dsUserType.RowFilter = "RoleGroupID not in (" & ApplicationSettings.RoleClientDepotID & "," & ApplicationSettings.RoleSupplierDepotID & ")"
                dropDownListID.DataSource = dsUserType
                dropDownListID.DataTextField = "RoleGroupName"
                dropDownListID.DataValueField = "RoleGroupID"
                dropDownListID.DataBind()
                dropDownListID.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectUserType"), 0))
                For Each Item As System.Web.UI.WebControls.ListItem In dropDownListID.Items
                    If Item.Text.ToString.ToLower = "retail" Then
                        Item.Text = "Support staff"
                    End If
                Next
            End If
        Else
            Dim dsUserType As DataView
            dsUserType = dsView.ToTable.DefaultView
            dsUserType.RowFilter = "RoleGroupID not in (" & ApplicationSettings.RoleClientDepotID & "," & ApplicationSettings.RoleSupplierDepotID & ")"
            dropDownListID.DataSource = dsUserType
            dropDownListID.DataTextField = "RoleGroupName"
            dropDownListID.DataValueField = "RoleGroupID"
            dropDownListID.DataBind()
            dropDownListID.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectUserType"), 0))
            For Each Item As System.Web.UI.WebControls.ListItem In dropDownListID.Items
                If Item.Text.ToString.ToLower = "retail" Then
                    Item.Text = "Support staff"
                End If
            Next
        End If

    End Sub

    ''' <summary>
    ''' Procedure to populate locations of a particular company.
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="dropDownListID"></param>
    ''' <param name="companyID"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateLocationsofCompany(ByVal page As Page, ByRef dropDownListID As DropDownList, ByVal companyID As Integer, Optional ByVal killcache As Boolean = False)
        'Cache is deleted from location form
        Dim cachekey As String = "LocationNamesList-" & companyID
        If killcache = True Then
            page.Cache.Remove(cachekey)
        End If
        Dim ds As DataSet
        If page.Cache(cachekey) Is Nothing Then
            ds = ws.WSContact.GetLocationName(companyID)
            page.Cache.Add(cachekey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cachekey), DataSet)
        End If

        dropDownListID.DataSource = ds
        dropDownListID.DataTextField = "Name"
        dropDownListID.DataValueField = "AddressID"
        dropDownListID.DataBind()
        dropDownListID.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectLocation"), 0))
    End Sub

    ''' <summary>
    ''' Procedure to populate other locations of a particular company.
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="dropDownListID"></param>
    ''' <param name="companyID"></param>
    ''' <remarks></remarks>
    Public Shared Function GetOtherLocationsofCompany(ByVal page As Page, ByVal companyID As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        'Cache is deleted from location form
        Dim cachekey As String = "LocationNamesList-" & companyID
        If killcache = True Then
            page.Cache.Remove(cachekey)
        End If
        Dim ds As DataSet
        If page.Cache(cachekey) Is Nothing Then
            ds = ws.WSContact.GetLocationName(companyID)
            page.Cache.Add(cachekey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cachekey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Procedure to populate user status drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="dropDownListID"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateUserStatus(ByVal page As Page, ByRef dropDownListID As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='Status'"
        dropDownListID.DataSource = dsView
        dropDownListID.DataTextField = "StandardValue"
        dropDownListID.DataValueField = "StandardID"
        dropDownListID.DataBind()
    End Sub

    ''' <summary>
    ''' Get contact ds - for storing in cache (for a single contact - returns all tables)
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="sessionid"></param>
    ''' <param name="ContactID"></param>
    ''' <param name="IsMainContact"></param>
    ''' <param name="CacheKey"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetContactsDS(ByRef page As Page, ByVal sessionid As String, Optional ByVal ContactID As Integer = 0, Optional ByVal IsMainContact As Boolean = False, Optional ByVal CacheKey As String = "", Optional ByVal KillCache As Boolean = True) As DataSet
        If CacheKey = "" Then
            CacheKey = "Contact" & "-" & CStr(ContactID) & "-" & sessionid
        End If
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ws.WSContact.Timeout = 300000
            ds = ws.WSContact.GetContact(ContactID, IsMainContact, ApplicationSettings.OWUKBizDivId)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Function to return Certification and qualification dataview
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="bizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCertQual(ByVal page As Page, ByVal bizDivID As Integer) As DataView
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(1).Copy.DefaultView
        dsView.RowFilter = "BizDivID = " & bizDivID
        Return dsView
    End Function

    ''' <summary>
    ''' Procedure to populate No Of Employees drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlNoOfEmployees"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateNoOfEmployees(ByVal page As Page, ByRef ddlNoOfEmployees As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='CompanyStrength'"
        ddlNoOfEmployees.DataSource = dsView
        ddlNoOfEmployees.DataTextField = "StandardValue"
        ddlNoOfEmployees.DataValueField = "StandardID"
        ddlNoOfEmployees.DataBind()
        ddlNoOfEmployees.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectNoOfEmployees"), ""))
    End Sub

    Public Shared Sub PopulateWoCreationFlow(ByVal page As Page, ByRef ddlWoCreatioFlow As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(9).Copy.DefaultView
        ddlWoCreatioFlow.DataSource = dsView
        ddlWoCreatioFlow.DataTextField = "StandardValue"
        ddlWoCreatioFlow.DataValueField = "StandardID"
        ddlWoCreatioFlow.DataBind()
        ddlWoCreatioFlow.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Select Work Flow", "0"))
    End Sub


    ''' <summary>
    ''' Procedure to populate Vehicle Type control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="cblVehicleTypeEdit"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateVehicleTypeEdit(ByVal page As Page, ByRef cblVehicleTypeEdit As CheckBoxList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel = 'Vehicle Type'"
        cblVehicleTypeEdit.DataSource = dsView
        cblVehicleTypeEdit.DataTextField = "StandardValue"
        cblVehicleTypeEdit.DataValueField = "StandardID"
        cblVehicleTypeEdit.DataBind()
        'cblVehicleType.DataSource = dsView
        'cblVehicleType.DataTextField = "StandardValue"
        'cblVehicleType.DataValueField = "StandardID"
        'cblVehicleType.DataBind()
    End Sub

    ''' <summary>
    ''' Procedure to populate Service Response time  drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlResponseTime"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateServiceResponseTime(ByVal page As Page, ByRef ddlResponseTime As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='ServiceResponseTime'"
        ddlResponseTime.DataSource = dsView
        ddlResponseTime.DataTextField = "StandardValue"
        ddlResponseTime.DataValueField = "StandardID"
        ddlResponseTime.DataBind()
        ddlResponseTime.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("ServiceResponseTime"), ""))

    End Sub

    ''' <summary>
    ''' Procedure to populate No Of Employees drop down control 
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlpartner"></param>
    ''' <param name="ofPartner"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulatePartnerLevel(ByVal page As Page, ByRef ddlpartner As DropDownList, ByVal ofPartner As String)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='" & ofPartner & "PartnerLevel'"
        ddlpartner.DataSource = dsView
        ddlpartner.DataTextField = "StandardValue"
        ddlpartner.DataValueField = "StandardID"
        ddlpartner.DataBind()
        ddlpartner.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectLevel"), ""))
    End Sub
    Public Shared Sub PopulateAdminUsers(ByVal page As Page, ByRef ddlOWAccountManager As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, True).Tables(7).Copy.DefaultView
        ddlOWAccountManager.DataSource = dsView
        ddlOWAccountManager.DataTextField = "Name"
        ddlOWAccountManager.DataValueField = "ContactID"
        ddlOWAccountManager.DataBind()
    End Sub
    Public Shared Sub PopulateBusinessArea(ByVal page As Page, ByRef ddlBusinessArea As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel ='BusinessArea'"
        ddlBusinessArea.DataSource = dsView
        ddlBusinessArea.DataTextField = "StandardValue"
        ddlBusinessArea.DataValueField = "StandardID"
        ddlBusinessArea.DataBind()
        ddlBusinessArea.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Select Business Area", ""))
    End Sub

    ''' <summary>
    ''' Procedure to populate credit terms
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub PopulateCreditTerms(ByVal page As Page, ByRef ddlCreditTerms As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='CreditTerms'"
        ddlCreditTerms.DataSource = dsView
        ddlCreditTerms.DataTextField = "StandardValue"
        ddlCreditTerms.DataValueField = "StandardID"
        ddlCreditTerms.DataBind()
        ddlCreditTerms.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Select Payment Term", ""))
    End Sub
    'OA-622 - discount changes
    Public Shared Sub PopulateSupplierDiscountPaymentTerm(ByVal page As Page, ByRef ddlSuppDiscountPaymentsTerms As DropDownList, ByVal source As String)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(8).Copy.DefaultView
        dsView.RowFilter = "source='" + source + " '"
        ddlSuppDiscountPaymentsTerms.DataSource = dsView
        ddlSuppDiscountPaymentsTerms.DataTextField = "paymentTerm"
        ddlSuppDiscountPaymentsTerms.DataValueField = "discountPaymentTermId"
        ddlSuppDiscountPaymentsTerms.DataBind()
        'ddlSuppDiscountPaymentsTerms.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Select Payment Term", ""))
    End Sub
    Public Shared Sub GetStandardDiscount(ByVal page As Page, ByRef txtDiscount As TextBox, ByVal paymentTermId As Integer, ByVal source As String)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(8).Copy.DefaultView
        dsView.RowFilter = "source='" + source + " '"
        dsView.RowFilter = "discountPaymentTermId=" & paymentTermId
        Dim discount As Decimal
        discount = CDec(dsView(0)("discount").ToString())
        txtDiscount.Text = discount
    End Sub



    ''' <summary>
    ''' Function to return regions dataview from standards
    ''' </summary>
    ''' <param name="page"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRegions(ByVal page As Page) As DataView
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(2).Copy.DefaultView
        Return dsView
    End Function

    Public Shared Function GetContactsDSAdmin(ByRef page As Page, ByVal ContactID As Integer, ByVal IsMainContact As Boolean, ByVal CacheKey As String, Optional ByVal KillCache As Boolean = False) As DataSet
        If KillCache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ws.WSContact.Timeout = 300000

            ds = ws.WSContact.AdminGetContactDetails(ContactID, IIf(IsMainContact, 1, 0), page.Session("BizDivId"))
            'ds = ws.WSAdmin.AdminGetContactDetails(ContactID, 1, BizDivID)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Populate industries in drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlIndustry"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateIndustries(ByVal page As Page, ByRef ddlIndustry As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(5).Copy.DefaultView
        ddlIndustry.DataSource = dsView
        ddlIndustry.DataTextField = "Name"
        ddlIndustry.DataValueField = "IndustryID"
        ddlIndustry.DataBind()
        ddlIndustry.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectIndustry"), ""))
    End Sub

    Public Shared Function GetStandardsForLabel(ByVal page As Page, ByVal label As String) As DataView
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='" & label & "'"
        Return dsView
    End Function

    ''' <summary>
    ''' Procedure to populate Countries drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlCompanyCountry"></param>
    ''' <param name="defaultCountry"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateCountry(ByVal page As Page, ByRef ddlCompanyCountry As DropDownList, ByVal defaultCountry As String)

        Dim dv_country As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(4).Copy.DefaultView
        dv_country.Sort = "CountryName"
        ddlCompanyCountry.DataSource = dv_country
        ddlCompanyCountry.DataTextField = "CountryName"
        ddlCompanyCountry.DataValueField = "CountryID"
        ddlCompanyCountry.DataBind()

        'To show the default selected country
        dv_country.RowFilter = "CountryName = '" & defaultCountry & "'"
        Dim UKcountryId As String
        UKcountryId = dv_country.Item(0).Item("CountryID")
        ddlCompanyCountry.SelectedValue = CInt(UKcountryId)

    End Sub

    ''' <summary>
    ''' Procedure to populate AutoMatch Settings
    ''' </summary>
    ''' <param name="CompanyID"></param>
    ''' <remarks></remarks>
    Public Shared Function PopulateAutoMatchSettings(ByVal CompanyID As Integer)

        Dim ds As DataSet = ws.WSContact.GetAutoMatchSettings(CompanyID)
        Return ds
    End Function

    ''' <summary>
    '''  Procedure to save AutoMatch Settings
    ''' </summary>
    ''' <param name="CompanyID"></param>
    ''' <param name="ReferenceCheck"></param>
    ''' <param name="SecurityCheck"></param>
    ''' <param name="CRBCheck"></param>
    ''' <param name="NearestDistance"></param>
    ''' <param name="AutoMatchStatus"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SaveAutoMatchSettings(ByVal CompanyID As Integer, ByVal ReferenceCheck As Boolean, ByVal SecurityCheck As Boolean _
            , ByVal CRBCheck As Boolean, ByVal NearestDistance As String, ByVal AutoMatchStatus As Boolean, ByVal VersionNo As Byte(), ByVal LoggedInUserID As Integer _
            , ByVal EngineerCRBCheck As Boolean, ByVal EngineerCSCSCheck As Boolean, ByVal EngineerUKSecurityCheck As Boolean, ByVal EngineerRightToWorkInUK As Boolean) As Integer
        Dim Success As Integer = ws.WSContact.SaveAutomatchSettings(CompanyID, ReferenceCheck, SecurityCheck, CRBCheck, NearestDistance, AutoMatchStatus, VersionNo, LoggedInUserID, EngineerCRBCheck, EngineerCSCSCheck, EngineerUKSecurityCheck, EngineerRightToWorkInUK)
        Return Success
    End Function


    Public Shared Function ColumnEqual(ByVal A As Object, ByVal B As Object) As Boolean
        ' Compares two values to determine if they are equal. Also compares DBNULL.Value.

        ' NOTE: If your DataTable contains object fields, you must extend this
        ' function to handle the fields in a meaningful way if you intend to group on them.

        If A Is DBNull.Value And B Is DBNull.Value Then Return True ' Both are DBNull.Value.
        If A Is DBNull.Value Or B Is DBNull.Value Then Return False ' Only one is DBNull.Value.
        Return A = B                                                ' Value type standard comparison
    End Function

    Public Shared Function SelectDistinct(ByVal TableName As String, ByVal SourceTable As DataTable, ByVal FieldName As String) As DataTable
        Dim dt As New DataTable(TableName)
        dt.Columns.Add(FieldName, SourceTable.Columns(FieldName).DataType)
        Dim dr As DataRow, LastValue As Object
        For Each dr In SourceTable.Select("", FieldName)
            If LastValue Is Nothing OrElse Not ColumnEqual(LastValue, dr(FieldName)) Then
                LastValue = dr(FieldName)
                dt.Rows.Add(New Object() {LastValue})
            End If
        Next
        Return dt
    End Function

    ''' <summary>
    ''' This is a common function which gets the notes from the db and returns them as a dataset.
    ''' </summary>
    ''' <returns>Dataset</returns>
    ''' <remarks>Pratik Trivedi - 14 Nov, 2008</remarks>
    Public Shared Function GetNotes(ByVal WOID As Integer)
        Dim ds As DataSet = ws.WSWorkOrder.GetWONotes(WOID)
        Return ds
    End Function
    ''' <summary>
    ''' This is a common function which moves the product seq to new seq.
    ''' </summary>
    ''' <returns>Success bit</returns>
    ''' <remarks>Sumit Kale</remarks>
    Public Shared Function MoveSwapSequence(ByVal OldSeq As Integer, ByVal NewSeq As Integer, ByVal CompanyId As Integer, ByVal Mode As String) As DataSet
        Dim Success As DataSet = ws.WSWorkOrder.MoveSwapSequence(OldSeq, NewSeq, CompanyId, Mode)
        Return Success
    End Function
    ''' <summary>
    ''' This is a common function which saves the notes to the db and returns the success result bit.
    ''' </summary>
    ''' <returns>Success bit</returns>
    ''' <remarks>Pratik Trivedi - 14 Nov, 2008</remarks>
    'poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2
    Public Shared Function SaveNotes(ByVal WOID As Integer, ByVal ContactID As Integer, ByVal Note As String, ByVal ReturnData As Boolean, Optional ByVal NoteCategory As Integer = 0) As DataSet
        Dim Success As DataSet = ws.WSWorkOrder.UpdateWONotes(WOID, ContactID, Note, ReturnData, NoteCategory)
        Return Success
    End Function


    ''' <summary>
    ''' This is a common function which saves the attachments in the db and returns the success result bit.
    ''' </summary>
    ''' <returns>Success bit</returns>
    ''' <remarks>Gautam M - 27 Jan, 2010</remarks>
    Public Shared Function SaveAttachments(ByVal xmlContent As String, ByVal WOID As Integer, ByVal Source As String, ByVal LinkageSource As String, ByVal WOUploadFlag As Integer, ByVal LoggedInUserID As Integer)
        Dim Success As DataSet = ws.WSWorkOrder.SaveAttachments(xmlContent, WOID, Source, LinkageSource, WOUploadFlag, LoggedInUserID)

    End Function

    Public Shared Function GetJulianDate(ByVal Gdate As Date)
        Dim JDate As String
        Dim Year, Days As String
        Year = CStr(Gdate.Year.ToString.Substring(2)).PadLeft(2, "0")
        Dim FirstDayOfYear As Date = "1/1/" & Gdate.Year
        Days = CStr(DateDiff(DateInterval.DayOfYear, FirstDayOfYear, Gdate) + 1).PadLeft(3, "0")
        JDate = Year & Days
        Return JDate
    End Function

    ''' <summary>
    ''' Created by Snehal on 11 March 2010: This is a Common function to get the workorder rating 
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWORating(ByVal WOID As Integer, Optional ByVal LoggedInUserID As Integer = 0, Optional ByVal Mode As String = "") As DataSet
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetWorkorderRating(WOID, LoggedInUserID, Mode)
        Return ds
    End Function

    ''' <summary>
    ''' This is a common function which saves the comment for workorder to the db and returns the success result bit.
    ''' </summary>
    ''' <returns>Success bit</returns>
    ''' <remarks>Pratik Trivedi - 14 Nov, 2008</remarks>
    Public Shared Function SaveRatingComment(ByVal WOID As String, ByVal AdminCompID As Integer, ByVal AdminContactID As Integer, ByVal Comments As String, ByVal rating As Integer) As DataSet
        Dim Success As DataSet = ws.WSWorkOrder.UpdateWorkorderRating(WOID, AdminCompID, AdminContactID, Comments.Replace(Chr(13), "<BR>"), rating)
        Return Success
    End Function

    ''' <summary>
    ''' Common function to get the standards for the create workorder form    
    ''' </summary>
    ''' <returns>Default Menu</returns>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetUserTypes(ByVal CompanyId As Integer) As DataSet
        Dim ds As DataSet
        ds = ws.WSContact.GetUserType(CompanyId)
        Return ds
    End Function

    ''' <summary>
    ''' Common function to get the standards for the create workorder form    
    ''' </summary>
    ''' <returns>Default Menu</returns>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function UpdateDefaultBookingForm(ByVal CompanyId As Integer, ByVal RoleGroupId As Integer, ByVal DefaultBookingForm As String) As DataSet
        Dim ds As DataSet
        ds = ws.WSContact.UpdateDefaultBookingForm(CompanyId, RoleGroupId, DefaultBookingForm)
        Return ds
    End Function

#Region "WebContent"


    ''' <summary>
    ''' To fetch web content standards from database , it also fetch division
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="StandardLabel"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStandardsForWebContent(ByRef page As Page, Optional ByVal StandardLabel As String = "", Optional ByVal killcache As Boolean = False) As DataSet
        Dim CacheKey As String
        CacheKey = "GetStandardsAndBusiness"
        If killcache = True Then
            page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(CacheKey) Is Nothing Then
            ds = ws.WSStandards.GetStandardsForWebContent(StandardLabel)
            page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function
    Public Shared Function GetStandards(ByRef page As Page, Optional ByVal StandardLabel As String = "") As DataSet
        Dim ds As DataSet
        ds = ws.WSStandards.GetStandards(StandardLabel)
        Return ds
    End Function

    ''' <summary>
    ''' Populate webcontent type in drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlWebContent"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateWebContentType(ByVal page As Page, ByRef ddlWebContent As DropDownList, ByVal divid As Integer)
        Dim dvStandard As DataView = GetStandardsForWebContent(page).Tables(0).Copy.DefaultView

        '''Requirement By client to remove Careers As a field from the Content type Drop Down
        ''' For that we pass the Session("bizdivId") As a Parameter and apply the appropriate filter
        ''' Code Added By Ambar
        If divid = ApplicationSettings.OWUKBizDivId Then
            dvStandard.RowFilter = "StandardLabel='WebContent' AND StandardValue <> 'Careers'"
        Else
            dvStandard.RowFilter = "StandardLabel='WebContent'"
        End If

        ddlWebContent.DataSource = dvStandard
        ddlWebContent.DataTextField = "StandardValue"
        ddlWebContent.DataValueField = "StandardID"
        ddlWebContent.DataBind()
        ddlWebContent.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectAllWebContent"), ""))
    End Sub

    ''' <summary>
    ''' Procedure to populate business per country
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlBusiness"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateBusiness(ByVal page As Page, ByRef ddlBusiness As DropDownList)
        Dim ds As DataSet
        ds = GetStandardsForWebContent(page)
        Dim dvBusiness As DataView = ds.Tables(1).Copy.DefaultView
        ddlBusiness.DataSource = dvBusiness
        ddlBusiness.DataTextField = "BusinessName"
        ddlBusiness.DataValueField = "BusinessID"
        ddlBusiness.DataBind()
        ddlBusiness.Items.Insert(0, New System.Web.UI.WebControls.ListItem(ResourceMessageText.GetString("SelectSite"), ""))
        ddlBusiness.Items.Insert(1, New System.Web.UI.WebControls.ListItem("All Site", "All"))
    End Sub


    ''' <summary>
    ''' Populate Account type in drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlAccount"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateAccount(ByVal page As Page, ByRef ddlAccountType As DropDownList)

        Dim dvAccount As DataView = GetStandardsForWebContent(page).Tables(0).Copy.DefaultView
        dvAccount.RowFilter = "StandardLabel='AccountType'"
        ddlAccountType.DataSource = dvAccount
        ddlAccountType.DataTextField = "StandardValue"
        ddlAccountType.DataValueField = "StandardID"
        ddlAccountType.DataBind()
        ddlAccountType.Items.Insert(0, New System.Web.UI.WebControls.ListItem("None", ""))


    End Sub

    ''' <summary>
    ''' Populate Job type in drop down control
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlJobType"></param>
    ''' <remarks></remarks>
    Public Shared Sub PopulateJobType(ByVal page As Page, ByRef ddlJobType As DropDownList)

        Dim dvAccount As DataView = GetStandardsForWebContent(page).Tables(0).Copy.DefaultView
        dvAccount.RowFilter = "StandardLabel='JobType'"
        ddlJobType.DataSource = dvAccount
        ddlJobType.DataTextField = "StandardValue"
        ddlJobType.DataValueField = "StandardID"
        ddlJobType.DataBind()
        ddlJobType.Items.Insert(0, New System.Web.UI.WebControls.ListItem("None", ""))


    End Sub
    ''' <summary>
    ''' Function to get the standards for the RAQ form and store them in cache
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetContentForSiteForm(ByVal type As String, ByVal showOnSite As Boolean, ByVal BizDivId As Integer) As DataSet
        Dim ds As DataSet
        ds = ws.WSContact.GetContentForSiteForm(type, showOnSite, BizDivId)
        Return ds
    End Function

#End Region

#Region "DECommonFunctions"
    ''' <summary>
    ''' Common function to populate the workorder types
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlWOType"></param>
    ''' <param name="cacheKey"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function PopulateRAQWorkOrderType(ByVal page As Page, ByRef ddlWOType As DropDownList, ByVal cacheKey As String, ByVal BizDivId As Integer)

        Dim ds As DataSet
        ds = GetRAQStandards(page, cacheKey, BizDivId)
        Dim dv_WOType As DataView = ds.Tables("tblWorkOrderTypes").Copy.DefaultView
        dv_WOType.Sort = "Name"

        ddlWOType.DataTextField = "Name"
        ddlWOType.DataValueField = "CombId"
        ddlWOType.DataSource = dv_WOType
        ddlWOType.DataBind()

        Dim li As New System.Web.UI.WebControls.ListItem
        li = New System.Web.UI.WebControls.ListItem
        li.Text = ResourceMessageText.GetString("WOCategoryList")
        li.Value = ""
        ddlWOType.Items.Insert(0, li)
    End Function

    ''' <summary>
    ''' common function to get the security question and answers.
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="ddlQuestion"></param>
    ''' <param name="cacheKey"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function PopulateRAQSecurityQuestions(ByVal page As Page, ByRef ddlQuestion As DropDownList, ByVal cacheKey As String, ByVal BizDivId As Integer)

        Dim ds As DataSet
        ds = GetRAQStandards(page, cacheKey, BizDivId)
        Dim dv_QA As DataView = ds.Tables("tblSecurityQuestion").Copy.DefaultView

        ddlQuestion.DataTextField = "StandardValue"
        ddlQuestion.DataValueField = "StandardID"
        ddlQuestion.DataSource = dv_QA
        ddlQuestion.DataBind()

        Dim li As New System.Web.UI.WebControls.ListItem
        li = New System.Web.UI.WebControls.ListItem
        li.Text = ResourceMessageText.GetString("SelectQuestion")
        li.Value = ""
        ddlQuestion.Items.Insert(0, li)
    End Function

    ''' <summary>
    ''' Common function to get the standards for the RAQ form
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="cacheKey"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRAQStandards(ByRef page As Page, ByVal cacheKey As String, ByVal BizDivId As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        If killcache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            ds = ws.WSWorkOrder.woRAQGetStandards(BizDivId)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Common function to get the standards for the create workorder form
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="cacheKey"></param>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWorkOrderStandards(ByRef page As Page, ByVal cacheKey As String, ByVal BizDivId As Integer, ByVal ContactID As Integer, ByVal WOID As Integer, Optional ByVal killcache As Boolean = False) As DataSet
        If killcache = True Then
            page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If page.Cache(cacheKey) Is Nothing Then
            ds = ws.WSWorkOrder.woCreateWOGetStandards(BizDivId, ContactID, WOID)
            page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

#End Region

    ''' <summary>
    ''' create Log using Log4Net
    ''' </summary>
    ''' <param name="message"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function createLog(ByVal message As String)
        Dim logger As log4net.ILog
        Dim str As String
        str = "" & Chr(13) & "" & Chr(10) & "Log Entry : "
        str = str & ApplicationSettings.WebPath & "" & Chr(13)
        str = str & DateTime.Now.ToLongTimeString() & DateTime.Now.ToLongDateString() & "" & Chr(13)
        str = str & "Log Message " & message & Chr(9) & "" & Chr(13)
        str = str & "--------------------------------------------------------------------------"
        logger = log4net.LogManager.GetLogger("File")
        logger.Info(str)
        Return Nothing
    End Function

#Region "PostcodeLookup"

    ''' <summary>
    ''' Get the list of addresses for the Given Postcode/Keyword
    ''' </summary>
    ''' <param name="keyword"></param>
    ''' <param name="lblErr"></param>
    ''' <param name="lstProperties"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Shared Function GetAddressList(ByVal keyword As String, ByRef lblErr As Label, ByRef lstProperties As ListBox)
    '    Dim IsValid As Boolean
    '    IsValid = False
    '    If ApplicationSettings.ValidatePostcodeUK = "True" Then
    '        If Regex.IsMatch(keyword.ToUpper, ApplicationSettings.ValidateExpressionPC) Then
    '            IsValid = True
    '        End If
    '    Else
    '        IsValid = True
    '    End If

    '    If IsValid Then
    '        Dim objLookup 'As New WSAddressLookup.LookupUK
    '        Dim objInterimResults ' As WSAddressLookup.InterimResults
    '        Dim objInterimResult ' As WSAddressLookup.InterimResult

    '        'Make the request
    '        Dim AccountKey As String = ApplicationSettings.AccountKey
    '        Dim LicenceKey As String = ApplicationSettings.LicenceKey
    '        objInterimResults = objLookup.ByPostcode(keyword, AccountKey, LicenceKey, "")

    '        'Tidy up
    '        objLookup.Dispose()


    '        'Copy the results into the list box
    '        If objInterimResults.IsError Then
    '            'Disable the List box
    '            lstProperties.Visible = False

    '            'Write the error message to the address label
    '            lblErr.Text = ResourceMessageText.GetString("PostcodeLookupError")

    '        Else
    '            'Pass blank error string
    '            lblErr.Text = ""

    '            'Enable the List box
    '            lstProperties.Visible = True

    '            'Clear the items in the list
    '            lstProperties.Items.Clear()

    '            'Add the new items to the list
    '            If Not objInterimResults.Results Is Nothing Then
    '                For Each objInterimResult In objInterimResults.Results
    '                    lstProperties.Items.Add(New  _
    '                         System.Web.UI.WebControls.ListItem(objInterimResult.Description, objInterimResult.Id))
    '                Next
    '            End If

    '        End If
    '    Else
    '        lstProperties.Visible = False
    '        lblErr.Text = ResourceMessageText.GetString("PostcodeLookupError")
    '    End If
    '    Return Nothing
    'End Function

    ''' <summary>
    ''' Fetch the details of selected and populate the addresss in the form.
    ''' </summary>
    ''' <param name="lblErr"></param>
    ''' <param name="lstProperties"></param>
    ''' <param name="txtCompanyName"></param>
    ''' <param name="txtCompanyAddress"></param>
    ''' <param name="txtCity"></param>
    ''' <param name="txtCounty"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Shared Function PopulateAddress(ByRef lblErr As Label, ByVal PostCodeToCheck As String, ByRef lstProperties As ListBox, ByRef txtPostcode As TextBox, ByRef txtCompanyName As TextBox, ByRef txtCompanyAddress As TextBox, ByRef txtCity As TextBox, ByRef txtCounty As TextBox)
    '    Dim objLookup As New WSAddressLookup.LookupUK
    '    Dim objAddressResults As WSAddressLookup.AddressResults
    '    Dim objAddress As WSAddressLookup.Address

    '    'Make the request
    '    Dim AccountKey As String = ApplicationSettings.AccountKey
    '    Dim LicenceKey As String = ApplicationSettings.LicenceKey

    '    Dim PostCodeExists As Boolean = ws.WSWorkOrder.DoesPostCodeExists(PostCodeToCheck)

    '    If PostCodeExists Then
    '        objAddressResults = objLookup.FetchAddress(lstProperties.SelectedValue, _
    '           WSAddressLookup.enLanguage.enLanguageEnglish, _
    '           WSAddressLookup.enContentType.enContentStandardAddress, AccountKey, LicenceKey, "")
    '    Else
    '        objAddressResults = objLookup.FetchAddress(lstProperties.SelectedValue, _
    '           WSAddressLookup.enLanguage.enLanguageEnglish, _
    '           WSAddressLookup.enContentType.enContentGeographicAddress, AccountKey, LicenceKey, "")
    '    End If


    '    'Tidy up
    '    objLookup.Dispose()


    '    'Get the address
    '    If objAddressResults.IsError Then

    '        'Write the error message to the address label
    '        lblErr.Text = ResourceMessageText.GetString("PostcodeLookupError")

    '        'Clear the labels
    '        txtCompanyAddress.Text = String.Empty
    '        txtCity.Text = String.Empty
    '        txtCounty.Text = String.Empty

    '    Else
    '        'Pass blank error string
    '        lblErr.Text = ""

    '        'Get the address
    '        objAddress = objAddressResults.Results(0)

    '        'Write the labels
    '        If objAddress.OrganisationName <> String.Empty Then
    '            txtCompanyName.Text = String.Empty
    '            txtCompanyName.Text = objAddress.OrganisationName
    '        End If
    '        txtCompanyAddress.Text = objAddress.Line1

    '        'Append Line 2 with a comma only if it exists
    '        If objAddress.Line2.ToString <> "" Then
    '            txtCompanyAddress.Text = txtCompanyAddress.Text & " ," & Chr(13) & objAddress.Line2
    '        End If

    '        'Append Line 3 with a comma only if it exists
    '        If objAddress.Line3.ToString <> "" Then
    '            txtCompanyAddress.Text = txtCompanyAddress.Text & " ," & Chr(13) & objAddress.Line3
    '        End If

    '        txtCity.Text = objAddress.PostTown
    '        txtCounty.Text = objAddress.County
    '        txtPostcode.Text = objAddress.Postcode

    '        'Clear any previous error messages
    '        lblErr.Text = String.Empty

    '        'Get PostCode GeoCode of the location
    '        Dim PostCode As String = objAddress.Postcode
    '        Dim PostCodeGeoCode As WSAddressLookup.AddressGeographicData = objAddress.GeographicData
    '        If Not PostCodeExists Then
    '            getWOPostCodeGeoCode(PostCode, PostCodeGeoCode, objAddress)
    '        End If

    '    End If
    '    Return Nothing
    'End Function

    ''' <summary>
    ''' Extract the GeoCode info from the lookup done.
    ''' </summary>
    ''' <param name="PostCodeToCheck"></param>
    ''' <param name="PostCodeGeoCode"></param>
    ''' <remarks></remarks>
    'Public Shared Sub getWOPostCodeGeoCode(ByVal PostCodeToCheck As String, ByVal PostCodeGeoCode As WSAddressLookup.AddressGeographicData, ByVal objAddress As WSAddressLookup.Address)

    '    'DO NOT DELETE - FOR FUTURE USE - pratik
    '    'Dim PostCodeExists As Boolean = ws.WSWorkOrder.DoesPostCodeExists(PostCodeToCheck)
    '    Dim PostCodeExists As Boolean = False 'CURRENTLY, WE CHECK IN SP WHICH SAVES GEOCODES - pratik

    '    '*****************************************************************************************************************************
    '    'The following code checks the postcode associated with the address. If the geocode info of the postcode in present in the DB
    '    'then it is collected in the viewstate variables and can be used later. If the geocode info is not there with us, the code 
    '    'uses the saved coordinates which were fetched from the web service to get the addresses lookup.
    '    '*****************************************************************************************************************************

    '    If PostCodeExists = False Then
    '        Dim results As WSAddressLookup.AddressGeographicData = PostCodeGeoCode
    '        Dim GridEastM As String = String.Empty
    '        Dim GridNorthM As String = String.Empty

    '        If Not IsNothing(results.GridEastM) Then
    '            If results.GridEastM <> 0 Then
    '                GridEastM = results.GridEastM
    '            Else
    '                GridEastM = "Invalid"
    '            End If
    '        Else
    '            GridEastM = "Invalid"
    '        End If

    '        If Not IsNothing(results.GridNorthM) Then
    '            If results.GridNorthM <> 0 Then
    '                GridNorthM = results.GridNorthM
    '            Else
    '                GridNorthM = "Invalid"
    '            End If
    '        Else
    '            GridNorthM = "Invalid"
    '        End If

    '        Dim dt As New DataTable("tblGeoCode")
    '        Dim drow As DataRow
    '        Dim col1 As New DataColumn("PostCode")
    '        Dim col2 As New DataColumn("GridEast")
    '        Dim col3 As New DataColumn("GridNorth")

    '        Dim col4 As New DataColumn("CompanyName")
    '        Dim col5 As New DataColumn("Address")
    '        Dim col6 As New DataColumn("City")
    '        Dim col7 As New DataColumn("County")

    '        dt.Columns.Add(col1)
    '        dt.Columns.Add(col2)
    '        dt.Columns.Add(col3)
    '        If objAddress.Line1 <> String.Empty Then
    '            dt.Columns.Add(col4)
    '            dt.Columns.Add(col5)
    '            dt.Columns.Add(col6)
    '            dt.Columns.Add(col7)
    '        End If
    '        drow = dt.NewRow
    '        drow("PostCode") = PostCodeToCheck
    '        drow("GridEast") = GridEastM
    '        drow("GridNorth") = GridNorthM

    '        'Insert Address details in the XML only if available
    '        If objAddress.Line1 <> String.Empty Then
    '            drow("CompanyName") = objAddress.OrganisationName
    '            drow("Address") = objAddress.Line1 & " " & objAddress.Line2 & " " & objAddress.Line3
    '            drow("City") = objAddress.PostTown
    '            drow("County") = objAddress.County
    '        End If

    '        dt.Rows.Add(drow)
    '        Dim dsWOGeoCode As New DataSet
    '        dt.TableName = "tblGeoCodes"
    '        dsWOGeoCode.Tables.Add(dt)
    '        InsertPostCodeCoordinates(dsWOGeoCode)
    '    End If

    'End Sub

    ''' <summary>
    ''' insert the coordinates of the postcode in the table tblGeoCodes
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <remarks></remarks>
    Public Shared Sub InsertPostCodeCoordinates(ByVal ds As DataSet)
        ds.DataSetName = "GeoCodes"
        Dim xmlContent As String = ds.GetXml
        ws.WSWorkOrder.InsertPostCodeCoordinates(xmlContent)
    End Sub

#End Region

    ''' <summary>
    ''' Leena T. 12-Nov-2009
    ''' This function will save the version no in the session for that page
    ''' </summary>
    ''' <param name="dtVer"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Shared Sub StoreVerNum(ByVal dtVer As DataTable)
        Dim VerdvGet As DataView
        If Not IsNothing(dtVer) Then
            VerdvGet = dtVer.DefaultView
            HttpContext.Current.Session("VerNumArray") = VerdvGet.Item(0)("VersionNo")
        End If
    End Sub

    ''' <summary>
    ''' Leena T. 12-Nov-2009
    ''' This fucntion will retrieve the version no from the session for that page 
    ''' </summary>
    ''' <param name="page"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FetchVerNum() As Byte()
        Dim VerNum As Byte()
        If Not IsNothing(HttpContext.Current.Session("VerNumArray")) Then
            VerNum = CType(HttpContext.Current.Session("VerNumArray"), Byte())
        End If
        Return VerNum
    End Function

    ''' <summary>
    ''' Leena T. 18-Nov-2009
    ''' This function will save the version no for the WO Tracking in the session for that page
    ''' </summary>
    ''' <param name="dtVer"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Shared Sub StoreWOTrackingVerNum(ByVal dvVer As DataView)
        ' Dim VerdvGet As DataView
        If Not IsNothing(dvVer) Then
            'VerdvGet = dvVer.DefaultView
            HttpContext.Current.Session("WOTrackingVerNumArray") = dvVer.Item(0)("WOTrackingVersionNo")
        End If
    End Sub
    ''' <summary>
    ''' Leena T. 18-Nov-2009
    ''' This fucntion will retrieve the version no from the session for that page 
    ''' </summary>
    ''' <param name="page"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FetchWOTrackingVerNum() As Byte()
        Dim VerNum As Byte()
        If Not IsNothing(HttpContext.Current.Session("WOTrackingVerNumArray")) Then
            VerNum = CType(HttpContext.Current.Session("WOTrackingVerNumArray"), Byte())
        End If
        Return VerNum
    End Function
    Public Shared Function GetDSFromXML(ByVal strXML As String) As DataSet
        Dim ds As New DataSet
        Dim xmlread1 As New System.Xml.XmlTextReader(strXML, XmlNodeType.Document, Nothing)
        ds.ReadXml(xmlread1)
        Return ds
    End Function
    Public Shared Function GetSPHoliday(ByVal CompanyId As Integer, ByVal WorkingDays As String, ByVal Mode As String)
        Dim ds As DataSet = ws.WSContact.GetSPHoliday(CompanyId, WorkingDays, Mode)
        Return ds
    End Function
    Public Shared Function AddUpdateBlockedDate(ByVal CompanyID As Integer, ByVal BillingId As Integer, ByVal BlockedDate As String, ByVal IsBlocked As Boolean, ByVal IsBlockedByAdmin As Boolean, ByVal ServiceListValue As String, ByVal TimeSlot As String, ByVal mode As String) As DataSet
        Dim ds As DataSet
        ds = ws.WSWorkOrder.AddUpdateBlockedDate(CompanyID, BillingId, BlockedDate, IsBlocked, IsBlockedByAdmin, ServiceListValue, TimeSlot, mode)
        Return ds
    End Function
    Public Shared Function RemoveDuplicates(ByVal items As String()) As String()
        Dim noDupsArrList As New ArrayList()
        For i As Integer = 0 To items.Length - 1
            If Not noDupsArrList.Contains(items(i).Trim()) Then
                noDupsArrList.Add(items(i).Trim())
            End If
        Next

        Dim uniqueItems As String() = New String(noDupsArrList.Count - 1) {}
        noDupsArrList.CopyTo(uniqueItems)
        Return uniqueItems
    End Function
    ''' <summary>
    ''' This is a common function which saves the notes to the db and returns the success result bit.
    ''' </summary>
    ''' <returns>Success bit</returns>
    ''' <remarks>Chandank - 30 July, 2012</remarks>

    'poonam modified on 8/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
    Public Shared Function SaveActionCommentNotes(ByVal WOID As String, ByVal LoggedInUserID As Integer, ByVal Note As String, ByVal Comments As String, ByVal CompanyID As Integer, ByVal Rating As Integer, ByVal IsFlagged As Boolean, ByVal IsWatched As Boolean, ByVal ShowClient As Boolean, ByVal ShowSupplier As Boolean, Optional ByVal NoteCategory As Integer = 0) As DataSet
        Dim Success As DataSet = ws.WSWorkOrder.UpdateWorkorderAction(WOID, LoggedInUserID, Note, Comments, CompanyID, Rating, IsFlagged, IsWatched, ShowClient, ShowSupplier, NoteCategory)
        Return Success
    End Function

    Public Shared Function ConvertXMLToDataSet(ByVal xmlData As String) As DataSet
        Dim stream As StringReader = Nothing
        Dim reader As XmlTextReader = Nothing
        Try
            Dim xmlDS As New DataSet()
            stream = New StringReader(xmlData)
            ' Load the XmlTextReader from the stream
            reader = New XmlTextReader(stream)
            xmlDS.ReadXml(reader)
            Return xmlDS
        Catch
            Return Nothing
        Finally
            If reader IsNot Nothing Then
                reader.Close()
            End If
        End Try
    End Function

    Public Shared Function SendStatusUpdateToSONY(ByVal RefWOID As String, ByVal PONumber As String, ByVal Status As String, ByVal StartDate As String)

        Dim ds_WOSuccessForSONY As DataSet
        Dim strStatusUpdateURL As String = ""

        Dim dt As DateTime = StartDate
        Dim strDate As String
        strDate = dt.ToString("yyy-MM-dd")

        If Status.ToLower = "appmd" Then
            strStatusUpdateURL = ApplicationSettings.SONYStatusUpdateURL & PONumber & "/" & Status & "?remark=" & strDate

        ElseIf Status.ToLower = "execu" Then
            strStatusUpdateURL = ApplicationSettings.SONYStatusUpdateURL & PONumber & "/" & Status & "?remark=" & strDate

        ElseIf Status.ToLower = "claim" Then
            strStatusUpdateURL = ApplicationSettings.SONYStatusUpdateURL & PONumber & "/CLAIM"
        End If

        Try
            Dim myHttpWebRequest As HttpWebRequest
            Dim myHttpWebResponse As HttpWebResponse
            myHttpWebRequest = HttpWebRequest.Create(strStatusUpdateURL)
            myHttpWebRequest.Credentials = New System.Net.NetworkCredential(ApplicationSettings.SONYUserName, ApplicationSettings.SONYPassword)
            myHttpWebRequest.Method = "GET"

            myHttpWebResponse = myHttpWebRequest.GetResponse()

            If myHttpWebResponse.StatusCode = HttpStatusCode.OK Then
                ds_WOSuccessForSONY = ws.WSWorkOrder.InsertURLforSONY(RefWOID, strStatusUpdateURL, "Success")
            Else
                ds_WOSuccessForSONY = ws.WSWorkOrder.InsertURLforSONY(RefWOID, strStatusUpdateURL, "Failed")
            End If

            myHttpWebResponse.Close()
        Catch ex As Exception
            ds_WOSuccessForSONY = ws.WSWorkOrder.InsertURLforSONY(RefWOID, strStatusUpdateURL, "Failed")
        End Try
    End Function

    'Poonam added on 5/6/2015 - 4492527 - OW -- NEW -- Professional Service WO reports 
    Public Shared Function GetWoStandards(ByRef page As Page, Optional ByVal StandardLabel As String = "") As DataSet
        Dim ds As DataSet
        ds = ws.WSStandards.GetWoStandards(StandardLabel)
        Return ds
    End Function
    'poonam modified on 4/12/2015 - Task - OA-131 :OA/OM - Strengthen automatically and manually generated passwords
    'To Gnerate random number containing atleast one upper case,atleast one lower case,atleast one digit
    Public Shared Function GenerateOTP() As String
        Try
            'uppercase
            Dim UpperCase As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            Dim OtpUpperCase As String = String.Empty
            For i As Integer = 0 To 2
                Dim Character As String = String.Empty
                Do
                    Dim Index As Integer = New Random().Next(0, UpperCase.Length)
                    Character = UpperCase.ToCharArray()(Index).ToString()
                Loop While OtpUpperCase.IndexOf(Character) <> -1
                OtpUpperCase += Character
            Next

            'lower case
            Dim LowerCase As String = "abcdefghijklmnopqrstuvwxyz"
            Dim OtpLowerCase As String = String.Empty
            For i As Integer = 0 To 2
                Dim Character As String = String.Empty
                Do
                    Dim Index As Integer = New Random().Next(0, LowerCase.Length)
                    Character = LowerCase.ToCharArray()(Index).ToString()
                Loop While OtpLowerCase.IndexOf(Character) <> -1
                OtpLowerCase += Character
            Next

            'digits
            Dim Digit As String = "1234567890"
            Dim OtpDigit As String = String.Empty
            For i As Integer = 0 To 1
                Dim Character As String = String.Empty
                Do
                    Dim Index As Integer = New Random().Next(0, Digit.Length)
                    Character = Digit.ToCharArray()(Index).ToString()
                Loop While OtpDigit.IndexOf(Character) <> -1
                OtpDigit += Character
            Next

            'final random otp           
            Dim InputString As String
            InputString = Convert.ToString(OtpUpperCase & OtpLowerCase & OtpDigit)
            Dim RandomNo As New Random
            Dim FinalOtp As String = ""
            While InputString.Length > 0
                Dim i As Integer
                i = RandomNo.Next(0, InputString.Length)
                FinalOtp &= InputString.Substring(i, 1)
                InputString = InputString.Remove(i, 1)
            End While
            Return FinalOtp
        Catch ex As Exception
        End Try
    End Function

    'Poonam modified on 30/12/2015 - Task - OA-112 : OA - Add Emergency Contact fields in User detail page
    Public Shared Sub PopulateRelationType(ByVal page As Page, ByRef dropDownListID As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='Relationship Type'"
        dropDownListID.DataSource = dsView
        dropDownListID.DataTextField = "StandardValue"
        dropDownListID.DataValueField = "StandardID"
        dropDownListID.DataBind()
        dropDownListID.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Select Relation type", ""))
    End Sub

    'poonam modified on 8/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
    Public Shared Sub PopulateNoteCategory(ByVal page As Page, ByRef dropDownListID As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='Note Category'"
        dropDownListID.DataSource = dsView
        dropDownListID.DataTextField = "StandardValue"
        dropDownListID.DataValueField = "StandardID"
        dropDownListID.DataBind()
        dropDownListID.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Select Note type", ""))
    End Sub
    'Poonam added on 12/8/2016 - Task - OA-300 : EA - Make add comment mandatory via pop-up when Deleting or Suspending a company
    Public Shared Sub PopulateCommentReason(ByVal page As Page, ByRef dropDownListID As DropDownList)
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(page, False).Tables(0).Copy.DefaultView
        dsView.RowFilter = "StandardLabel='Comment Reason'"
        dropDownListID.DataSource = dsView
        dropDownListID.DataTextField = "StandardValue"
        dropDownListID.DataValueField = "StandardID"
        dropDownListID.DataBind()
        dropDownListID.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Select Comment Reason", ""))

    End Sub

    Public Shared Function CreatePDFUsingAspxURL(ByVal InvoiceNo As String, ByVal FolderName As String, ByVal InvoicePath As String, ByVal SaveLocation As String) As String
        Dim strHtml As String
        Dim memStream As New MemoryStream()
        Dim strWriter As New StringWriter()
        System.Web.HttpContext.Current.Server.Execute(InvoicePath, strWriter)
        strHtml = strWriter.ToString().Replace("<br>", "<br />")
        strWriter.Close()
        strWriter.Dispose()

        'Dim strHtml As New StringWriter()
        'Dim hw As New HtmlTextWriter(strHtml)
        'pnlInvoiceDetails.RenderControl(hw)

        Dim DirPath As String = SaveLocation + FolderName
        Dim FilePath As String = String.Empty

        'check if directory exists
        'if not found the create one
        If Not Directory.Exists(DirPath) Then
            Directory.CreateDirectory(DirPath)
        End If

        FilePath = DirPath + "\" + InvoiceNo + ".pdf"

        'check if file exists
        'if file not found then create one
        'if file already exists then append an integer to file name to make it unique.
        Dim i As Integer = 0
        While File.Exists(FilePath)
            i += 1
            FilePath = DirPath + "\" + InvoiceNo + "_" + i.ToString + ".pdf"
        End While

        'Dim strFileShortName As String = InvoiceNo & ".pdf"
        'Dim strFileName As String = HttpContext.Current.Server.MapPath("Attachments\" & FolderName & "\" & strFileShortName)
        Dim docWorkingDocument As iTextSharp.text.Document = New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10.0F, 10.0F, 0.0F, 0.0F)
        Dim srdDocToString As StringReader = Nothing
        Try
            Dim pdfWrite As PdfWriter
            pdfWrite = PdfWriter.GetInstance(docWorkingDocument, New FileStream(FilePath, FileMode.Create))
            srdDocToString = New StringReader(strHtml.ToString())
            docWorkingDocument.Open()
            XMLWorkerHelper.GetInstance().ParseXHtml(pdfWrite, docWorkingDocument, srdDocToString)
        Catch ex As Exception
            System.Web.HttpContext.Current.Response.Write(ex.Message)
        Finally
            If Not docWorkingDocument Is Nothing Then
                docWorkingDocument.Close()
                docWorkingDocument.Dispose()
            End If
            If Not srdDocToString Is Nothing Then
                srdDocToString.Close()
                srdDocToString.Dispose()
            End If
        End Try
        Return FilePath
    End Function

#Region "GetAddressAPI"

    Public Shared Function GetAddressUsage()

        Dim result As String = ""
        Dim URL = "https://api.getAddress.io/v2/usage" + "?api-key=" + Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("GetAddressAPIAdminKey").Trim)

        'Create the web request  
        Dim request As HttpWebRequest = TryCast(WebRequest.Create(URL), HttpWebRequest)
        request.Method = "GET"
        request.ContentLength = 0

        Try
            'Get response  
            ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
            Using response As HttpWebResponse = TryCast(request.GetResponse(), HttpWebResponse)
                ' Get the response stream  
                Dim reader As New StreamReader(response.GetResponseStream())

                ' Read the whole contents and return as a string  
                result = reader.ReadToEnd()
                'result = result.Substring(1, result.Length - 2)

            End Using

        Catch ex As Exception
            result = ""
            Return 0
        End Try
        Try
            If result <> "" Then
                Dim details As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.JsonConvert.DeserializeObject(result)
                Dim Count As Integer = Convert.ToString(details.Property("count").Value)
                If Count > System.Configuration.ConfigurationManager.AppSettings("GetAddressUsageCount") Then
                    SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.GetAddressUsageAlertEmail, "GetAddress Usages count reached: " & Count & ". API Calls out of 5000. Between 5000 and 7500, API calls will have a 2 second delay.", "GetAddressUsages Alert")
                End If
            End If
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function callGetAddressList(ByVal PostCode As String, ByRef lblErr As Label, ByRef lstProperties As ListBox, ByRef txtPostCode As TextBox)

        Dim IsValid As Boolean
        IsValid = False

        If ApplicationSettings.ValidatePostcodeUK = "True" Then
            If Regex.IsMatch(PostCode.ToUpper, ApplicationSettings.ValidateExpressionPC) Then
                IsValid = True
            End If
        Else
            IsValid = False
        End If

        If IsValid Then
            Dim result As String = ""
            Dim URL = System.Configuration.ConfigurationManager.AppSettings("GetAddressAPI").ToString + PostCode + "?api-key=" + System.Configuration.ConfigurationManager.AppSettings("GetAddressAPIAccessKey").ToString().Trim()

            'Create the web request  
            Dim request As HttpWebRequest = TryCast(WebRequest.Create(URL), HttpWebRequest)
            request.Method = "GET"
            request.ContentLength = 0

            Try
                'Get response  
                ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
                Using response As HttpWebResponse = TryCast(request.GetResponse(), HttpWebResponse)
                    ' Get the response stream  
                    Dim reader As New StreamReader(response.GetResponseStream())

                    ' Read the whole contents and return as a string  
                    result = reader.ReadToEnd()
                    'result = result.Substring(1, result.Length - 2)

                End Using

            Catch ex As Exception
                lblErr.Text = ResourceMessageText.GetString("PostcodeLookupError")
                lstProperties.Visible = False
                result = ""
                Return 0
            End Try

            'Copy the results into the list box
            If result = "" Then
                'Disable the List box
                lstProperties.Visible = False

                'Write the error message to the address label
                lblErr.Text = ResourceMessageText.GetString("PostcodeLookupError")

            Else
                'Pass blank error string
                lblErr.Text = ""

                'Enable the List box
                lstProperties.Visible = True

                'Clear the items in the list
                lstProperties.Items.Clear()

                ' Deserialize the Json data into an object
                Dim details As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.JsonConvert.DeserializeObject(result)

                Dim latitude As String = Convert.ToString(details.Property("latitude").Value)
                Dim longitude As String = Convert.ToString(details.Property("longitude").Value)


                'HttpContext.Current.Session("latitude") = latitude
                'HttpContext.Current.Session("longitude") = longitude


                'If PostCode <> "" Then
                '    If Not PostCode.Contains(" ") Then
                '        '        PostCode = PostCode.Substring(0, 2) + " " + PostCode.Substring(2)
                '        lstProperties.Visible = False
                '        lblErr.Text = "Please ensure that you have entered your postcode in the correct format (with a space in the middle)"
                '        Return Nothing
                '    End If
                '    PostCode = PostCode.ToUpper()
                'End If

                If PostCode <> "" Then
                    If Not PostCode.Contains(" ") Then
                        '        PostCode = PostCode.Substring(0, 2) + " " + PostCode.Substring(2)
                        'PostCode = PostCode.Insert(PostCode.Length - 3, " ")
                        txtPostCode.Text = PostCode
                        'lblErr.Text = "Please ensure that you have entered your postcode in the correct format (with a space in the middle)"
                        'Return Nothing
                    End If
                    PostCode = PostCode.ToUpper()
                End If


                HttpContext.Current.Session("PostCode") = PostCode

                Dim ds As DataSet = ws.WSStandards.UpdatePostCodeLatLong(PostCode, latitude, longitude)

                Dim addresses = details.Value(Of JArray)("addresses").Values(Of String)()

                'Add the new items to the list
                For Each obj As String In addresses
                    Dim location() As String = obj.ToString().Split(",")
                    Dim text As String = ""
                    For Each value As String In location
                        If value.Trim() <> "" Then
                            If text.Trim() = "" Then
                                text = value
                            Else
                                text = text + "," + value
                            End If

                        End If
                    Next
                    lstProperties.Items.Add(New  _
                            System.Web.UI.WebControls.ListItem(text, obj))
                Next
            End If
        Else
            lstProperties.Visible = False
            lblErr.Text = ResourceMessageText.GetString("PostcodeLookupError")
        End If
        Return Nothing
    End Function

    Public Shared Function callPopulateAddress(ByRef lblErr As Label, ByVal strSelectedItem As String, ByRef lstProperties As ListBox, ByRef txtPostcode As TextBox, ByRef txtCompanyName As TextBox, ByRef txtCompanyAddress As TextBox, ByRef txtCity As TextBox, ByRef txtCounty As TextBox)
        Dim SelectedAddress() As String = strSelectedItem.Trim().Split(",")
        txtCompanyAddress.Text = String.Empty
        txtCity.Text = String.Empty
        txtCounty.Text = String.Empty
        lblErr.Text = ""

        'If SelectedAddress(0).Trim() <> "" Then
        '    txtCompanyName.Text = SelectedAddress(0)
        'End If

        'If SelectedAddress(1).Trim() <> "" And SelectedAddress(2).Trim() <> "" Then
        '    txtCompanyAddress.Text = SelectedAddress(1).Trim() & "," & Chr(13) & SelectedAddress(2).Trim()
        'ElseIf SelectedAddress(1).Trim() <> "" Then
        '    txtCompanyAddress.Text = SelectedAddress(1).Trim()
        'Else
        '    txtCompanyAddress.Text = SelectedAddress(0).Trim()
        'End If

        txtCompanyAddress.Text = ""
        For index As Integer = 0 To 4
            If SelectedAddress(index).Trim() <> "" Then
                If txtCompanyAddress.Text = "" Then
                    txtCompanyAddress.Text = SelectedAddress(index).Trim()
                Else
                    txtCompanyAddress.Text = txtCompanyAddress.Text + "," + SelectedAddress(index)
                End If
            End If
        Next

        txtPostcode.Text = HttpContext.Current.Session("PostCode")
        txtCity.Text = SelectedAddress(5)
        txtCounty.Text = SelectedAddress(6)

        Return Nothing
    End Function

    Public Shared Function GetPostcodeLatLong(ByVal PostCode As String)
        Dim IsValid As Boolean
        IsValid = False

        If ApplicationSettings.ValidatePostcodeUK = "True" Then
            If Regex.IsMatch(PostCode.ToUpper, ApplicationSettings.ValidateExpressionPC) Then
                IsValid = True
            End If
        Else
            IsValid = True
        End If

        If IsValid Then
            Dim result As String = ""
            Dim URL = System.Configuration.ConfigurationManager.AppSettings("GetAddressAPI").ToString + PostCode + "?api-key=" + System.Configuration.ConfigurationManager.AppSettings("GetAddressAPIAccessKey").ToString().Trim()

            'Create the web request  
            Dim request As HttpWebRequest = TryCast(WebRequest.Create(URL), HttpWebRequest)
            request.Method = "GET"
            request.ContentLength = 0


            'Get response  
            ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
            Using response As HttpWebResponse = TryCast(request.GetResponse(), HttpWebResponse)
                ' Get the response stream  
                Dim reader As New StreamReader(response.GetResponseStream())

                ' Read the whole contents and return as a string  
                result = reader.ReadToEnd()
                'result = result.Substring(1, result.Length - 2)

            End Using

            ' Deserialize the Json data into an object
            Dim details As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.JsonConvert.DeserializeObject(result)

            Dim latitude As String = Convert.ToString(details.Property("latitude").Value)
            Dim longitude As String = Convert.ToString(details.Property("longitude").Value)


            PostCode = PostCode.ToUpper()
            'End If
            HttpContext.Current.Session("PostCode") = PostCode

            Dim ds As DataSet = ws.WSStandards.UpdatePostCodeLatLong(PostCode, latitude, longitude)

            Return details

        Else
            Return Nothing
        End If
    End Function

    Public Shared Function FormatPostcode(ByVal fullString As String) As String
        Dim postcode As String
        postcode = fullString
        'Remove whitespaces
        If postcode <> "" Then
            postcode = Convert.ToString(fullString.Where(Function(x) Not Char.IsWhiteSpace(x)).ToArray())
            'Insert space before 3rd last char
            postcode = postcode.Insert(postcode.Length - 3, " ")
        End If
        Return postcode
    End Function

    Public Shared Function IsValidLatLong(ByVal Postcode As String, ByVal RecordType As String, ByVal RecordID As String) As String
        Try
            Dim str As String = RecordID
            'Remove whitespaces
            If RecordID <> "" Then
                Dim latitude As String = ""
                Dim longitude As String = ""
                Dim dt As DataTable

                dt = ws.WSWorkOrder.WO_GetPostCodeCoordinates(Postcode).Tables(0)

                If Not IsNothing(dt) Then
                    If (dt.Rows.Count > 0) Then
                        latitude = dt.Rows(0).Item("Latitude")
                        longitude = dt.Rows(0).Item("Longitude")
                    End If
                End If


                If latitude = "" Or longitude = "" Or latitude = "0.0" Or longitude = "0.0" Then
                    'Emails.SendInvalidLatLongEmail(Postcode, RecordType, RecordID)
                    CallLatLongAPI(Postcode, RecordType, RecordID)
                End If

            End If
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function CallLatLongAPI(ByVal Postcode As String, ByVal RecordType As String, ByVal RecordID As String) As Integer
        Try
            Dim result As String = ""
            Postcode = FormatPostcode(Postcode)
            Dim URL = "https://api.postcodes.io/postcodes/" + Postcode


            Dim request As HttpWebRequest = TryCast(WebRequest.Create(URL), HttpWebRequest)
            request.Method = "GET"
            request.ContentLength = 0

            Try

                ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
                CommonFunctions.createLog("Orderwork � Calling postcodes.io API for Incomplete location coordinates for :" + Postcode + "-" + RecordID)
                Using response As HttpWebResponse = TryCast(request.GetResponse(), HttpWebResponse)

                    Dim reader As New StreamReader(response.GetResponseStream())

                    result = reader.ReadToEnd()

                    If result <> "" Then
                        Dim LatLongData As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.JsonConvert.DeserializeObject(result)
                        Dim status As String = Convert.ToString(LatLongData.Property("status").Value)

                        If status = 200 Then
                            Dim objDetails As Newtonsoft.Json.Linq.JObject = JObject.Parse(Convert.ToString(LatLongData.Property("result").Value))
                            Dim latitude As String = Convert.ToString(objDetails.Property("latitude").Value)
                            Dim longitude As String = Convert.ToString(objDetails.Property("longitude").Value)

                            'Insert/update geocodes in our table
                            CommonFunctions.createLog("Orderwork � UpdatePostCodeLatLong for :" + Postcode + "-" + RecordID)
                            Dim ds As DataSet = ws.WSStandards.UpdatePostCodeLatLong(Postcode, latitude, longitude)
                            CommonFunctions.createLog("Orderwork � Success UpdatePostCodeLatLong for :" + Postcode + "-" + RecordID)

                            'add postcode to getaddress
                            If latitude <> "" And longitude <> "" Then
                                Postcode = Postcode.Replace(" ", "")
                                URL = "https://api.getAddress.io/private-address/" + Postcode.Trim() + "?api-key=" + Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("GetAddressAPIAdminKey").Trim)

                                Dim objGetaddressAPI As New GetaddressAPI
                                Dim dsAddress As DataSet = ws.WSStandards.GetAddress(RecordType, RecordID)
                                If dsAddress.Tables.Count <> 0 Then
                                    objGetaddressAPI.line1 = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("Address"))
                                    objGetaddressAPI.line2 = ""
                                    objGetaddressAPI.line3 = ""
                                    objGetaddressAPI.line4 = ""
                                    objGetaddressAPI.locality = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("City"))
                                    objGetaddressAPI.townOrCity = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("City"))
                                    objGetaddressAPI.county = ""

                                    Dim objResult As New APIResult
                                    CommonFunctions.createLog("Orderwork � Calling GetaddressAPI for :" + Postcode + "-" + RecordID)
                                    objResult = GenerateJSONandCallAPI(objGetaddressAPI, URL)

                                    If objResult.id = 1 Then
                                        ' successfully added to getaddress list, now update excel accordingly
                                        CommonFunctions.createLog("Orderwork-Potal- completed location coordinates for :" + Postcode + "-" + RecordID)
                                    End If
                                End If
                            End If
                        End If
                    Else
                        CommonFunctions.createLog("Orderwork � Calling doogle API email for Incomplete location coordinates for :" + Postcode + "-" + RecordID)
                        CalldoogalLatLongAPI(Postcode, RecordType, RecordID)
                    End If
                    Return 1
                End Using

            Catch ex As Exception
                CommonFunctions.createLog("Orderwork � Calling doogle API email for Incomplete location coordinates for :" + Postcode + "-" + RecordID)
                CalldoogalLatLongAPI(Postcode, RecordType, RecordID)
                'result = ""
                'Return 0
            End Try
        Catch ex As Exception
            CommonFunctions.createLog("Orderwork-Potal- Error occured while completing location coordinates for :" + Postcode + "-" + RecordID)
        End Try
    End Function

    Public Shared Function CalldoogalLatLongAPI(ByVal Postcode As String, ByVal RecordType As String, ByVal RecordID As String) As Integer
        Try
            Dim result As String = ""
            Dim URL = "https://www.doogal.co.uk/GetPostcode.ashx?postcode=" + Postcode

            Dim request As HttpWebRequest = TryCast(WebRequest.Create(URL), HttpWebRequest)
            request.Method = "GET"
            request.ContentLength = 0

            Try

                ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
                Using response As HttpWebResponse = TryCast(request.GetResponse(), HttpWebResponse)

                    Dim reader As New StreamReader(response.GetResponseStream())

                    result = reader.ReadToEnd()

                    If result <> "" Then
                        result = result.Replace(Postcode, "")
                        result = result.Substring(0, 21).Trim()

                        Dim latitude As String = result.Substring(0, 9)
                        result = result.Replace(latitude, "")
                        Dim longitude As String = result.Trim()

                        'Insert/update geocodes in our table
                        CommonFunctions.createLog("Orderwork � upadating incomplete location coordinates from doogal for :" + Postcode + "-" + RecordID)
                        Dim ds As DataSet = ws.WSStandards.UpdatePostCodeLatLong(Postcode, latitude, longitude)
                        CommonFunctions.createLog("Orderwork � successfully updated coordinates from doogal for :" + Postcode + "-" + RecordID)

                        'add postcode to getaddress
                        If latitude <> "" And longitude <> "" Then
                            Postcode = Postcode.Replace(" ", "")
                            URL = "https://api.getAddress.io/private-address/" + Postcode.Trim() + "?api-key=" + Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("GetAddressAPIAdminKey").Trim)

                            Dim objGetaddressAPI As New GetaddressAPI
                            Dim dsAddress As DataSet = ws.WSStandards.GetAddress(RecordType, RecordID)
                            If dsAddress.Tables.Count <> 0 Then
                                objGetaddressAPI.line1 = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("Address"))
                                objGetaddressAPI.line2 = ""
                                objGetaddressAPI.line3 = ""
                                objGetaddressAPI.line4 = ""
                                objGetaddressAPI.locality = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("City"))
                                objGetaddressAPI.townOrCity = Convert.ToString(dsAddress.Tables(0).Rows(0).Item("City"))
                                objGetaddressAPI.county = ""

                                Dim objResult As New APIResult
                                objResult = GenerateJSONandCallAPI(objGetaddressAPI, URL)

                                If objResult.id = 1 Then
                                    ' successfully added to getaddress list, now update excel accordingly
                                    CommonFunctions.createLog("Orderwork � successfully added coordinates to getaddress list :" + Postcode + "-" + objGetaddressAPI.line1 + ", " + objGetaddressAPI.locality + ", " + objGetaddressAPI.townOrCity)
                                End If
                            End If
                        End If

                    Else
                        CommonFunctions.createLog("Orderwork � Lat long update failed from both API(co-ordinates not found), details :" + Postcode + "-" + RecordID)
                        Emails.SendInvalidLatLongEmail(Postcode, RecordType, RecordID)
                    End If

                    Return 1
                End Using

            Catch ex As Exception
                CommonFunctions.createLog("Orderwork � Lat long update failed from both API(co-ordinates not found), details :" + Postcode + "-" + RecordID)
                Emails.SendInvalidLatLongEmail(Postcode, RecordType, RecordID)
                result = ""
                Return 0
            End Try
        Catch ex As Exception

        End Try
    End Function


    'Generate the JSON Object and Call the respective API
    Public Shared Function GenerateJSONandCallAPI(ByVal objAPI As Object, ByVal Uri As String) As APIResult
        Dim objResult As New APIResult
        Dim JsonObject As String = Newtonsoft.Json.JsonConvert.SerializeObject(objAPI)

        ' Request
        Dim request As System.Net.HttpWebRequest = TryCast(System.Net.HttpWebRequest.Create(Uri), System.Net.HttpWebRequest)
        request.Method = "POST"
        request.ContentType = "application/json"

        Using writer As New System.IO.StreamWriter(request.GetRequestStream())
            writer.Write(JsonObject)
            writer.Flush()
            writer.Close()
        End Using

        ''Get response  
        Using response As System.Net.HttpWebResponse = TryCast(request.GetResponse(), System.Net.HttpWebResponse)

            Dim reader As New System.IO.StreamReader(response.GetResponseStream())
            Dim result As String = ""
            result = reader.ReadToEnd()
            ' Deserialize the Json data into an object
            objResult = Newtonsoft.Json.JsonConvert.DeserializeObject(Of APIResult)(Convert.ToString(result))
        End Using
        Return objResult
    End Function
#End Region

    Public Shared Function AzureStorageAccessKey() As String
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim AccessKey As String
        WebRequest = HttpWebRequest.Create((HttpContext.Current.Server.MapPath("~/AccessData/") & "AzureStorageAccessKey.txt"))
        WebResponse = WebRequest.GetResponse()
        SR = New StreamReader(WebResponse.GetResponseStream())
        AccessKey = sr.ReadToEnd
        Return AccessKey
    End Function

End Class

Public Class GetaddressAPI
    Private _line1 As String
    Private _line2 As String
    Private _line3 As String
    Private _line4 As String
    Private _locality As String
    Private _townOrCity As String
    Private _county As String


    Public Property line1() As String
        Get
            Return _line1
        End Get
        Set(ByVal value As String)
            _line1 = value
        End Set
    End Property
    Public Property line2() As String
        Get
            Return _line2
        End Get
        Set(ByVal value As String)
            _line2 = value
        End Set
    End Property
    Public Property line3() As String
        Get
            Return _line3
        End Get
        Set(ByVal value As String)
            _line3 = value
        End Set
    End Property
    Public Property line4() As String
        Get
            Return _line4
        End Get
        Set(ByVal value As String)
            _line4 = value
        End Set
    End Property
    Public Property locality() As String
        Get
            Return _locality
        End Get
        Set(ByVal value As String)
            _locality = value
        End Set
    End Property
    Public Property townOrCity() As String
        Get
            Return _townOrCity
        End Get
        Set(ByVal value As String)
            _townOrCity = value
        End Set
    End Property
    Public Property county() As String
        Get
            Return _county
        End Get
        Set(ByVal value As String)
            _county = value
        End Set
    End Property
End Class

Public Class APIResult
    Private _id As Integer
    Private _message As String
    Public Property message() As String
        Get
            Return _message
        End Get
        Set(ByVal value As String)
            _message = value
        End Set
    End Property

    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
End Class



Public Module ResponseHelper
    Sub New()
    End Sub
    Public Sub Redirect(ByVal url As String, ByVal target As String, ByVal windowFeatures As String)
        Dim context As HttpContext = HttpContext.Current
        If ([String].IsNullOrEmpty(target) OrElse target.Equals("_self", StringComparison.OrdinalIgnoreCase)) AndAlso [String].IsNullOrEmpty(windowFeatures) Then
            context.Response.Redirect(url)
        Else
            Dim page As Page = DirectCast(context.Handler, Page)
            If page Is Nothing Then
                Throw New InvalidOperationException("Cannot redirect to new window outside Page context.")
            End If
            url = page.ResolveClientUrl(url)
            Dim script As String
            If Not [String].IsNullOrEmpty(windowFeatures) Then
                script = "window.open(""{0}"", ""{1}"", ""{2}"");"
            Else
                script = "window.open(""{0}"", ""{1}"");"
            End If
            script = [String].Format(script, url, target, windowFeatures)
            ScriptManager.RegisterStartupScript(page, GetType(Page), "Redirect", script, True)
        End If
    End Sub
End Module
