Public Class Paging

    Public Sub InitializePager(ByRef pageObj As Page, ByVal totitems As Integer)
        Dim i, pagesize As Integer
        pagesize = ApplicationSettings.GridViewPageSize
        If pagesize <> 0 Then
            i = totitems Mod pagesize
            Dim pages As Integer
            If totitems > pagesize Then
                pages = Math.Floor(totitems / pagesize)
                If i > 0 Then
                    pages = pages + 1
                End If
                Dim intSelectedIndex As Integer = CType(pageObj.FindControl("ddPageNumberUp"), DropDownList).SelectedIndex
                CType(pageObj.FindControl("ddPageNumberUp"), DropDownList).Items.Clear()
                CType(pageObj.FindControl("ddPageNumberDown"), DropDownList).Items.Clear()
                Dim j As Integer = pages - 1
                For i = 0 To (pages - 1)
                    Dim item As New ListItem
                    item.Value = j
                    item.Text = j + 1
                    j = j - 1
                    CType(pageObj.FindControl("ddPageNumberUp"), DropDownList).Items.Insert(0, item)
                    CType(pageObj.FindControl("ddPageNumberDown"), DropDownList).Items.Insert(0, item)
                Next
                CType(pageObj.FindControl("ddPageNumberUp"), DropDownList).SelectedIndex = intSelectedIndex
                CType(pageObj.FindControl("ddPageNumberDown"), DropDownList).SelectedIndex = intSelectedIndex
                If pages > 1 Then
                    CType(pageObj.FindControl("tblTop"), HtmlTable).Visible = True
                    CType(pageObj.FindControl("tblDown"), HtmlTable).Visible = True
                Else
                    CType(pageObj.FindControl("tblTop"), HtmlTable).Visible = False
                    CType(pageObj.FindControl("tblDown"), HtmlTable).Visible = False
                End If
            Else
                CType(pageObj.FindControl("tblTop"), HtmlTable).Visible = False
                CType(pageObj.FindControl("tblDown"), HtmlTable).Visible = False
            End If
        Else
            CType(pageObj.FindControl("tblTop"), HtmlTable).Visible = False
            CType(pageObj.FindControl("tblDown"), HtmlTable).Visible = False
        End If
    End Sub

    Public Sub PopulateRecordsLabel(ByRef pageObj As Page, ByVal PageIndex As Integer, ByVal totitems As Integer)
        Dim pagesize As Integer = ApplicationSettings.GridViewPageSize
        Dim str As String = "Showing "
        If PageIndex = 0 Then
            str &= "1 to " & CStr(pagesize)
        Else
            If (PageIndex * pagesize) + pagesize > totitems Then
                str &= CStr((PageIndex * pagesize) + 1) & " to " & CStr(totitems)
            Else
                str &= CStr((PageIndex * pagesize) + 1) & " to " & CStr((PageIndex * pagesize) + pagesize)
            End If
        End If
        str &= " of " & CStr(totitems)
        CType(pageObj.FindControl("tdTotalCount"), HtmlTableCell).InnerText = str
        CType(pageObj.FindControl("tdTotalCountDown"), HtmlTableCell).InnerText = str
    End Sub

End Class
