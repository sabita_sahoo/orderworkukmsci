﻿
Module OW_Whitelist
    Public Function ReturnStringWhitelist(val) As String
        Dim k As New Text.StringBuilder()
        k.Append(val)

        k.Replace("&#60;p&#62;", "<p>")
        k.Replace("&#60;&#47;p&#62;", "</p>")

        k.Replace("&#60;h1&#62;", "<h1>")
        k.Replace("&#60;&#47;h1&#62;", "</h1>")
        k.Replace("&#60;h2&#62;", "<h2>")
        k.Replace("&#60;&#47;h2&#62;", "</h2>")
        k.Replace("&#60;h3&#62;", "<h3>")
        k.Replace("&#60;&#47;h3&#62;", "</h3>")
        k.Replace("&#60;h4&#62;", "<h4>")
        k.Replace("&#60;&#47;h4&#62;", "</h4>")

        k.Replace("&#60;em&#62;", "<em>")
        k.Replace("&#60;&#47;em&#62;", "</em>")

        k.Replace("&#60;u&#62;", "<u>")
        k.Replace("&#60;&#47;u&#62;", "</u>")

        k.Replace("&#60;li&#62;", "<li>")
        k.Replace("&#60;&#47;li&#62;", "</li>")
        k.Replace("&#60;ul&#62;", "<ul>")
        k.Replace("&#60;&#47;ul&#62;", "</ul>")
        k.Replace("&#60;ol&#62;", "<ol>")
        k.Replace("&#60;&#47;ol&#62;", "</ol>")

        k.Replace("&#60;strong&#62;", "<strong>")
        k.Replace("&#60;&#47;strong&#62;", "</strong>")
        k.Replace("&#38;nbsp&#59;", " ")

        k.Replace("&#60;b&#62;", "<b>")
        k.Replace("&#60;&#47;b&#62;", "</b>")

        k.Replace("&#60;br&#62;", "<br>")
        k.Replace("&#60;br&#47;&#62;", "<br/>")
        k.Replace("&#60;br &#47;&#62;", "<br />")

        k.Replace("&#60;BR&#62;", "<BR>")
        k.Replace("&#60;BR&#47;&#62;", "<BR/>")
        k.Replace("&#60;BR &#47;&#62;", "<BR />")

        'k.Replace("&#146;", Chr(146))
        'k.Replace("&#147;", Chr(147))
        'k.Replace("&#148;", Chr(148))
        'k.Replace("&#150;", Chr(150))
        'k.Replace("&#151;", Chr(151))
        Return k.ToString()
    End Function
End Module