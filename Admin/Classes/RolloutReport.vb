﻿Imports System.ComponentModel.DataAnnotations

Namespace AT800

    Public Class RolloutReport

        <Display(Name:="Sign Off Sheet ID")>
        Public Property SignOffSheetID As Integer

        <Display(Name:="WO ID")>
        Public Property WOID As Integer

        <Display(Name:="Region")>
        Public Property Region As String

        <Display(Name:="Job Submitted")>
        Public Property JobSubmitted As DateTime?

        <Display(Name:="WO Ref No")>
        Public Property WORefNo As String

        <Display(Name:="TCC Ref No")>
        Public Property TCCRefNo As String

        <Display(Name:="Post Code")>
        Public Property PostCode As String

        <Display(Name:="Visit Date")>
        Public Property VisitDate As DateTime?

        <Display(Name:="RC")>
        Public Property RC As String

        <Display(Name:="Issue (Y/N) See Notes")>
        Public Property IsIssue As String

        <Display(Name:="Job Cancelled")>
        Public Property JobCancelled As DateTime?

        <Display(Name:="Cancellation Reason")>
        Public Property CancellationReason As String

        <Display(Name:="Sign Off Sheet Received")>
        Public Property SignOffSheetReceived As DateTime?

        <Display(Name:="Job Closed")>
        Public Property JobClosed As DateTime?

        <Display(Name:="Original Job Type")>
        Public Property OriginalJobType As String

        <Display(Name:="Final Job Type")>
        Public Property FinalJobType As String

        <Display(Name:="Job Completion Reason")>
        Public Property JobCompletionReason As String

        <Display(Name:="Notes")>
        Public Property Notes As String

        <Display(Name:="File Path")>
        Public Property FilePath As String

        <Display(Name:="Customer Name")>
        Public Property CustomerName As String

        <Display(Name:="Customer Address")>
        Public Property CustomerAddress As String

        <Display(Name:="Customer Phone")>
        Public Property CustomerPhone As String

        <Display(Name:="Installation Time")>
        Public Property InstallationTime As String

        <Display(Name:="Engineer Instructions")>
        Public Property EngineerInstructions As String

        <Display(Name:="OW Instructions")>
        Public Property OWInstructions As String

        Sub New()
        End Sub

    End Class

    Public Class RegionEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.Region = b2.Region Then
                Return True
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As String = bx.Region
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class JobSubmittedEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.JobSubmitted.HasValue = b2.JobSubmitted.HasValue Then
                If Not b1.JobSubmitted.HasValue Then
                    Return True
                Else
                    If b1.JobSubmitted.Value.Date = b2.JobSubmitted.Value.Date Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As DateTime?
            If bx.JobSubmitted.HasValue Then
                hCode = bx.JobSubmitted.Value.Date
            Else
                hCode = bx.JobSubmitted
            End If
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class WORefNoEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.WORefNo = b2.WORefNo Then
                Return True
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As String = bx.WORefNo
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class TCCRefNoEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.TCCRefNo = b2.TCCRefNo Then
                Return True
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As String = bx.TCCRefNo
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class PostCodeEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.PostCode = b2.PostCode Then
                Return True
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As String = bx.PostCode
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class VisitDateEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals
            If b1.VisitDate.HasValue = b2.VisitDate.HasValue Then
                If Not b1.VisitDate.HasValue Then
                    Return True
                Else
                    If b1.VisitDate.Value.Date = b2.VisitDate.Value.Date Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As DateTime?
            If bx.VisitDate.HasValue Then
                hCode = bx.VisitDate.Value.Date
            Else
                hCode = bx.VisitDate
            End If
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class RCEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.RC = b2.RC Then
                Return True
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As String = bx.RC
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class IsIssueEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.IsIssue = b2.IsIssue Then
                Return True
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As String = bx.IsIssue
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class JobCancelledEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.JobCancelled.HasValue = b2.JobCancelled.HasValue Then
                If Not b1.JobCancelled.HasValue Then
                    Return True
                Else
                    If b1.JobCancelled.Value.Date = b2.JobCancelled.Value.Date Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As DateTime?
            If bx.JobCancelled.HasValue Then
                hCode = bx.JobCancelled.Value.Date
            Else
                hCode = bx.JobCancelled
            End If
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class CancellationReasonEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.CancellationReason = b2.CancellationReason Then
                Return True
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As String = bx.CancellationReason
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class SignOffSheetReceivedEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.SignOffSheetReceived.HasValue = b2.SignOffSheetReceived.HasValue Then
                If Not b1.SignOffSheetReceived.HasValue Then
                    Return True
                Else
                    If b1.SignOffSheetReceived.Value.Date = b2.SignOffSheetReceived.Value.Date Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As DateTime?
            If bx.SignOffSheetReceived.HasValue Then
                hCode = bx.SignOffSheetReceived.Value.Date
            Else
                hCode = bx.SignOffSheetReceived
            End If
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class JobClosedEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.JobClosed.HasValue = b2.JobClosed.HasValue Then
                If Not b1.JobClosed.HasValue Then
                    Return True
                Else
                    If b1.JobClosed.Value.Date = b2.JobClosed.Value.Date Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As DateTime?
            If bx.JobClosed.HasValue Then
                hCode = bx.JobClosed.Value.Date
            Else
                hCode = bx.JobClosed
            End If
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class OriginalJobTypeEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.OriginalJobType = b2.OriginalJobType Then
                Return True
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As String = bx.OriginalJobType
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class FinalJobTypeEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.FinalJobType = b2.FinalJobType Then
                Return True
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As String = bx.FinalJobType
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class JobCompletionReasonEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.JobCompletionReason = b2.JobCompletionReason Then
                Return True
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As String = bx.JobCompletionReason
            Return hCode.GetHashCode()
        End Function

    End Class

    Public Class NotesEqualityComparer
        Implements IEqualityComparer(Of RolloutReport)

        Public Overloads Function Equals(ByVal b1 As RolloutReport, ByVal b2 As RolloutReport) _
                       As Boolean Implements IEqualityComparer(Of RolloutReport).Equals

            If b1.Notes = b2.Notes Then
                Return True
            Else
                Return False
            End If
        End Function


        Public Overloads Function GetHashCode(ByVal bx As RolloutReport) _
                    As Integer Implements IEqualityComparer(Of RolloutReport).GetHashCode
            Dim hCode As String = bx.Notes
            Return hCode.GetHashCode()
        End Function

    End Class

End Namespace