﻿Imports System.Net
Imports System.IO
Public Class SendSMS

    Public Shared Function PostSMS(ByVal MobileTo As String, ByVal MessageBody As String) As Boolean
        Dim apikey As String = "4yQi6rzdU+s-Lb0wynsl2BflCYdDuJGjwSLkurqJzN"
        Dim SenderName As String = "OrderWork"
        Dim Number As String = MobileTo
        Dim Message As String = "Thank you for choosing to use our installation services, we would really appreciate your feedback! Just click on the link " + MessageBody
        Dim URL As String = "https://api.txtlocal.com/send/?"
        Dim PostData As String = "apikey=" & apikey & "&sender=" & SenderName & "&numbers=" & Number & "&message=" & Message
        Dim req As HttpWebRequest = WebRequest.Create(URL)
        req.Method = "POST"
        Dim encoding As New ASCIIEncoding()
        Dim byte1 As Byte() = encoding.GetBytes(PostData)
        req.ContentType = "application/x-www-form-urlencoded"
        req.ContentLength = byte1.Length
        Dim newStream As Stream = req.GetRequestStream()
        newStream.Write(byte1, 0, byte1.Length)

        Try
            Dim resp As HttpWebResponse = req.GetResponse()
            Dim sr As New StreamReader(resp.GetResponseStream())
            Dim results As String = sr.ReadToEnd()
            sr.Close()
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Sending SMS Survey Successful to Mobile Number : " & MobileTo, "Sending SMS Survey Successful")
            Return True

            'Dim html As String = results
        Catch wex As WebException
            Dim ex1 As New Exception("SOMETHING WENT AWRY!Status: " & wex.Status & "Message: " & wex.Message & "")
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error occurred while sending SMS Survey (" & "Exception details" & ex1.ToString, "Error in UCCloseComplete")
            Throw (ex1)
        End Try
        Return True
    End Function

End Class
