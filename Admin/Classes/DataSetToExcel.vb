Imports Admin.AT800
Imports System.Reflection
Imports System.ComponentModel.DataAnnotations

Public Class DataSetToExcel

    ''' <summary>
    ''' Converts Dataset to excel format
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <param name="response"></param>
    ''' <remarks></remarks>
    Public Shared Sub Convert1(ByVal ds As DataSet, ByVal response As HttpResponse, Optional ByVal pagename As String = "")

        Dim dt As New DataTable

        'Modified by PB for the issue: Export to Excel exports the bank account details in encrypted format.
        Dim encryptedColumnSortCode As Integer = -1
        Dim encryptedColumnAccNo As Integer = -1
        Dim encryptedColumnAccName As Integer = -1

        dt = ds.Tables(0)
        'dt.Columns.Remove(dt.Columns(0))
        Dim strData As New System.Text.StringBuilder
        strData.Append("<table cellspacing=0 cellpadding=0 border=1;border-collapse:collapse;>")
        strData.Append("<tr>")
        Dim colCount As Integer
        For colCount = 0 To dt.Columns.Count - 1
            'Modified by PB for the issue: Export to Excel exports the bank account details in encrypted format.
            If dt.Columns(colCount).ColumnName = "SortCode" Then
                encryptedColumnSortCode = colCount
            End If
            If dt.Columns(colCount).ColumnName = "AccountNo" Then
                encryptedColumnAccNo = colCount
            End If
            If dt.Columns(colCount).ColumnName = "AccountName" Then
                encryptedColumnAccName = colCount
            End If
            '****************************************For the German Headers***************************
            If ApplicationSettings.BizDivId = ApplicationSettings.OWDEBizDivId And ApplicationSettings.SiteType <> ApplicationSettings.siteTypes.admin Then
                'FOR LOCATION LISTING
                If dt.Columns(colCount).ColumnName = "ContactID" Then
                    dt.Columns(colCount).ColumnName = "Kontakt ID"
                ElseIf dt.Columns(colCount).ColumnName = "LocationName" Then
                    dt.Columns(colCount).ColumnName = "Standort Name"
                ElseIf dt.Columns(colCount).ColumnName = "Address" Then
                    dt.Columns(colCount).ColumnName = "Strasse"
                ElseIf dt.Columns(colCount).ColumnName = "City" Then
                    dt.Columns(colCount).ColumnName = "Ort"
                ElseIf dt.Columns(colCount).ColumnName = "State" Then
                    dt.Columns(colCount).ColumnName = "Bundesland"
                ElseIf dt.Columns(colCount).ColumnName = "PostCode" Then
                    dt.Columns(colCount).ColumnName = "Postleitzahl"
                ElseIf dt.Columns(colCount).ColumnName = "CountryID" Then
                    dt.Columns(colCount).ColumnName = "Land ID"
                ElseIf dt.Columns(colCount).ColumnName = "Main" Then
                    dt.Columns(colCount).ColumnName = "Hauptstandort"
                ElseIf dt.Columns(colCount).ColumnName = "Fax" Then
                    dt.Columns(colCount).ColumnName = "Fax"
                ElseIf dt.Columns(colCount).ColumnName = "Phone" Then
                    dt.Columns(colCount).ColumnName = "Telefon"
                ElseIf dt.Columns(colCount).ColumnName = "Country" Then
                    dt.Columns(colCount).ColumnName = "Land"
                ElseIf dt.Columns(colCount).ColumnName = "Users" Then
                    dt.Columns(colCount).ColumnName = "Benutzer"
                ElseIf dt.Columns(colCount).ColumnName = "Contact" Then
                    dt.Columns(colCount).ColumnName = "Kontakt"
                End If
                'FOR USERS LISTING
                If dt.Columns(colCount).ColumnName = "Email" Then
                    dt.Columns(colCount).ColumnName = "E-mail"
                ElseIf dt.Columns(colCount).ColumnName = "Location" Then
                    dt.Columns(colCount).ColumnName = "Standort"
                ElseIf dt.Columns(colCount).ColumnName = "Mobile" Then
                    dt.Columns(colCount).ColumnName = "Mobiltelefon"
                ElseIf dt.Columns(colCount).ColumnName = "Name" Then
                    dt.Columns(colCount).ColumnName = "Name"
                ElseIf dt.Columns(colCount).ColumnName = "Type" Then
                    dt.Columns(colCount).ColumnName = "Benutzerprofil"
                ElseIf dt.Columns(colCount).ColumnName = "Status" Then
                    dt.Columns(colCount).ColumnName = "Status"
                ElseIf dt.Columns(colCount).ColumnName = "SpendLimit" Then
                    dt.Columns(colCount).ColumnName = "Ausgabenlimit"
                ElseIf dt.Columns(colCount).ColumnName = "ReceiveWONotification" Then
                    dt.Columns(colCount).ColumnName = "Auftragsankündigung erhalten"
                End If
                'FOR BUYER INVOICE LISTING - ADDED BY PB
                If dt.Columns(colCount).ColumnName = "InvoiceNo" Then
                    dt.Columns(colCount).ColumnName = "Rechnung-ID Nr."
                ElseIf dt.Columns(colCount).ColumnName = "InvoiceType" Then
                    dt.Columns(colCount).ColumnName = "Rechnungsart"
                ElseIf dt.Columns(colCount).ColumnName = "VAT" Then
                    dt.Columns(colCount).ColumnName = "UST"
                ElseIf dt.Columns(colCount).ColumnName = "InvoiceNetAmnt" Then
                    dt.Columns(colCount).ColumnName = "Netto Betrag"
                ElseIf dt.Columns(colCount).ColumnName = "Total" Then
                    dt.Columns(colCount).ColumnName = "Brutto Betrag"
                ElseIf dt.Columns(colCount).ColumnName = "InvoiceDate" Then
                    If pagename = "MyInvoices" Then
                        dt.Columns(colCount).ColumnName = "Beendet Datum"
                    Else
                        dt.Columns(colCount).ColumnName = "Rechnung Datum"
                    End If
                ElseIf dt.Columns(colCount).ColumnName = "BuyerName" Or dt.Columns(colCount).ColumnName = "SupplierName" Then
                    dt.Columns(colCount).ColumnName = "Name"
                ElseIf dt.Columns(colCount).ColumnName = "CreditTerms" Then
                    dt.Columns(colCount).ColumnName = "Zahlungsbedingungen"
                ElseIf dt.Columns(colCount).ColumnName = "workorderid" Then
                    dt.Columns(colCount).ColumnName = "Arbeitsauftragsnummer"
                ElseIf dt.Columns(colCount).ColumnName = "Price" Then
                    dt.Columns(colCount).ColumnName = "Preis"
                ElseIf dt.Columns(colCount).ColumnName = "Discount" Then
                    dt.Columns(colCount).ColumnName = "Rabatt"
                ElseIf dt.Columns(colCount).ColumnName = "DatePaid" Then
                    dt.Columns(colCount).ColumnName = "Bezahlt Datum"
                ElseIf dt.Columns(colCount).ColumnName = "PaymentNo" Then
                    dt.Columns(colCount).ColumnName = "Zahlungseingang"
                End If

            End If
            '*****************************************************************************************
            If dt.Columns(colCount).ColumnName <> "RowNum" Then
                strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'>" & dt.Columns(colCount).ColumnName & "</td>")
            End If
        Next
        strData.Append("</tr>")

        Dim HtmlRow As DataRow
        For Each HtmlRow In dt.Rows
            strData.Append("<tr>")
            For colCount = 0 To dt.Columns.Count - 1
                'Modified by PB for the issue: Export to Excel exports the bank account details in encrypted format.
                If ((encryptedColumnSortCode <> -1) And (encryptedColumnSortCode = colCount)) Or ((encryptedColumnAccNo <> -1) And (encryptedColumnAccNo = colCount)) Or ((encryptedColumnAccName <> -1) And (encryptedColumnAccName = colCount)) Then
                    If Not IsDBNull(HtmlRow(dt.Columns(colCount))) Then
                        strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & Encryption.Decrypt(HtmlRow(dt.Columns(colCount))) & "</td>")
                    Else
                        strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>&nbsp; </td>")
                    End If
                Else
                    If dt.Columns(colCount).ColumnName <> "RowNum" Then
                        strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & HtmlRow(dt.Columns(colCount)) & "</td>")
                    End If
                End If
            Next
        Next
        strData.Append("</tr>")
        strData.Append("</table>")

        'first let's clean up the response.object
        response.Clear()
        response.Charset = ""
        'set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("content-disposition", "attachment;filename=ExportToExcel.xls")
        response.Write(strData)
        response.End()
    End Sub
    Public Shared Sub ConvertNew(ByVal ds As DataSet, ByVal response As HttpResponse)
        'first let's clean up the response.object
        response.Clear()
        response.Charset = ""
        'set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("content-disposition", "attachment;filename=ExportToExcel.xls")

        'create a string writer
        Dim stringWrite As New System.IO.StringWriter
        'create an htmltextwriter which uses the stringwriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        'instantiate a datagrid
        Dim dg As New DataGrid
        'ds.Tables(0).Columns("VoucherNumber").DataType = System.Type.GetType("string")
        'set the datagrid datasource to the dataset passed in
        dg.DataSource = ds.Tables(0)
        'bind the datagrid
        dg.DataBind()
        'tell the datagrid to render itself to our htmltextwriter
        dg.RenderControl(htmlWrite)
        'all that's left is to output the html
        response.Write(stringWrite.ToString)
        response.End()
    End Sub
    Public Shared Sub ConvertNew(ByVal ds As DataSet, ByVal TableName As String, ByVal response As HttpResponse)
        'lets make sure a table actually exists at the passed in value
        'if it is not call the base method
        'If TableIndex > ds.Tables.Count - 1 Then
        '    Convert(ds, response)
        'End If
        'we've got a good table so
        'let's clean up the response.object
        response.Clear()
        response.Charset = ""
        'set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("content-disposition", "attachment;filename=ExportToExcel.xls")

        'create a string writer
        Dim stringWrite As New System.IO.StringWriter
        'create an htmltextwriter which uses the stringwriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        'instantiate a datagrid
        Dim dg As New DataGrid
        'set the datagrid datasource to the dataset passed in
        dg.DataSource = ds.Tables(TableName)
        'bind the datagrid
        dg.DataBind()
        'tell the datagrid to render itself to our htmltextwriter
        dg.RenderControl(htmlWrite)
        'all that's left is to output the html
        response.Write(stringWrite.ToString)
        response.End()
    End Sub
    ''' <summary>
    ''' Overloaded procedure to convert dataset to excel with additional parameter specify which column to not have in exported data
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <param name="response"></param>
    ''' <param name="colNameToDelete"></param>
    ''' <remarks></remarks>
    Public Shared Sub Convert(ByVal ds As DataSet, ByVal response As HttpResponse, ByVal colNameToDelete As ArrayList, Optional ByVal pagename As String = "")
        Dim dt As New DataTable
        Dim colCount As Integer

        dt = ds.Tables(0)

        'Modified by PB for the issue: Export to Excel exports the bank account details in encrypted format.
        Dim encryptedColumnSortCode As Integer = -1
        Dim encryptedColumnAccNo As Integer = -1
        Dim encryptedColumnAccName As Integer = -1


        For colCount = 0 To colNameToDelete.Count - 1
            If Not IsNothing(dt.Columns(colNameToDelete.Item(colCount))) Then
                dt.Columns.Remove(dt.Columns(colNameToDelete.Item(colCount)))
            End If
        Next


        Dim strData As New System.Text.StringBuilder
        strData.Append("<table cellspacing=0 cellpadding=0 border=1;border-collapse:collapse;>")
        strData.Append("<tr>")

        For colCount = 0 To dt.Columns.Count - 1
            'Modified by PB for the issue: Export to Excel exports the bank account details in encrypted format.
            If dt.Columns(colCount).ColumnName = "SortCode" Then
                encryptedColumnSortCode = colCount
            End If
            If dt.Columns(colCount).ColumnName = "AccountNo" Then
                encryptedColumnAccNo = colCount
            End If
            If dt.Columns(colCount).ColumnName = "AccountName" Then
                encryptedColumnAccName = colCount
            End If
            '****************************************For the German Headers***************************
            If ApplicationSettings.BizDivId = ApplicationSettings.OWDEBizDivId And ApplicationSettings.SiteType <> ApplicationSettings.siteTypes.admin Then
                'FOR LOCATION LISTING
                If dt.Columns(colCount).ColumnName = "ContactID" Then
                    dt.Columns(colCount).ColumnName = "Kontakt ID"
                ElseIf dt.Columns(colCount).ColumnName = "LocationName" Then
                    dt.Columns(colCount).ColumnName = "Standort Name"
                ElseIf dt.Columns(colCount).ColumnName = "Address" Then
                    dt.Columns(colCount).ColumnName = "Strasse"
                ElseIf dt.Columns(colCount).ColumnName = "City" Then
                    dt.Columns(colCount).ColumnName = "Ort"
                ElseIf dt.Columns(colCount).ColumnName = "State" Then
                    dt.Columns(colCount).ColumnName = "Bundesland"
                ElseIf dt.Columns(colCount).ColumnName = "PostCode" Then
                    dt.Columns(colCount).ColumnName = "Postleitzahl"
                ElseIf dt.Columns(colCount).ColumnName = "CountryID" Then
                    dt.Columns(colCount).ColumnName = "Land ID"
                ElseIf dt.Columns(colCount).ColumnName = "Main" Then
                    dt.Columns(colCount).ColumnName = "Hauptstandort"
                ElseIf dt.Columns(colCount).ColumnName = "Fax" Then
                    dt.Columns(colCount).ColumnName = "Fax"
                ElseIf dt.Columns(colCount).ColumnName = "Phone" Then
                    dt.Columns(colCount).ColumnName = "Telefon"
                ElseIf dt.Columns(colCount).ColumnName = "Country" Then
                    dt.Columns(colCount).ColumnName = "Land"
                ElseIf dt.Columns(colCount).ColumnName = "Users" Then
                    dt.Columns(colCount).ColumnName = "Benutzer"
                ElseIf dt.Columns(colCount).ColumnName = "Contact" Then
                    dt.Columns(colCount).ColumnName = "Kontakt"
                End If
                'FOR USERS LISTING
                If dt.Columns(colCount).ColumnName = "Email" Then
                    dt.Columns(colCount).ColumnName = "E-mail"
                ElseIf dt.Columns(colCount).ColumnName = "Location" Then
                    dt.Columns(colCount).ColumnName = "Standort"
                ElseIf dt.Columns(colCount).ColumnName = "Mobile" Then
                    dt.Columns(colCount).ColumnName = "Mobiltelefon"
                ElseIf dt.Columns(colCount).ColumnName = "Name" Then
                    dt.Columns(colCount).ColumnName = "Name"
                ElseIf dt.Columns(colCount).ColumnName = "Type" Then
                    dt.Columns(colCount).ColumnName = "Benutzerprofil"
                ElseIf dt.Columns(colCount).ColumnName = "Status" Then
                    dt.Columns(colCount).ColumnName = "Status"
                ElseIf dt.Columns(colCount).ColumnName = "SpendLimit" Then
                    dt.Columns(colCount).ColumnName = "Ausgabenlimit"
                ElseIf dt.Columns(colCount).ColumnName = "ReceiveWONotification" Then
                    dt.Columns(colCount).ColumnName = "Auftragsankündigung erhalten"
                End If
                'FOR BUYER INVOICE LISTING - ADDED BY PB
                If dt.Columns(colCount).ColumnName = "InvoiceNo" Then
                    dt.Columns(colCount).ColumnName = "Rechnung-ID Nr."
                ElseIf dt.Columns(colCount).ColumnName = "InvoiceType" Then
                    dt.Columns(colCount).ColumnName = "Rechnungsart"
                ElseIf dt.Columns(colCount).ColumnName = "VAT" Then
                    dt.Columns(colCount).ColumnName = "UST"
                ElseIf dt.Columns(colCount).ColumnName = "InvoiceNetAmnt" Then
                    dt.Columns(colCount).ColumnName = "Netto Betrag"
                ElseIf dt.Columns(colCount).ColumnName = "Total" Then
                    dt.Columns(colCount).ColumnName = "Brutto Betrag"
                ElseIf dt.Columns(colCount).ColumnName = "InvoiceDate" Then
                    If pagename = "MyInvoices" Then
                        dt.Columns(colCount).ColumnName = "Beendet Datum"
                    Else
                        dt.Columns(colCount).ColumnName = "Rechnung Datum"
                    End If
                ElseIf dt.Columns(colCount).ColumnName = "BuyerName" Or dt.Columns(colCount).ColumnName = "SupplierName" Then
                    dt.Columns(colCount).ColumnName = "Name"
                ElseIf dt.Columns(colCount).ColumnName = "CreditTerms" Then
                    dt.Columns(colCount).ColumnName = "Zahlungsbedingungen"
                ElseIf dt.Columns(colCount).ColumnName = "workorderid" Then
                    dt.Columns(colCount).ColumnName = "Arbeitsauftragsnummer"
                ElseIf dt.Columns(colCount).ColumnName = "Price" Then
                    dt.Columns(colCount).ColumnName = "Preis"
                ElseIf dt.Columns(colCount).ColumnName = "Discount" Then
                    dt.Columns(colCount).ColumnName = "Rabatt"
                ElseIf dt.Columns(colCount).ColumnName = "DatePaid" Then
                    dt.Columns(colCount).ColumnName = "Bezahlt Datum"
                ElseIf dt.Columns(colCount).ColumnName = "PaymentNo" Then
                    dt.Columns(colCount).ColumnName = "Zahlungseingang"
                End If

            End If
            '*****************************************************************************************
            strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'>" & dt.Columns(colCount).ColumnName & "</td>")
        Next
        strData.Append("</tr>")

        Dim HtmlRow As DataRow
        For Each HtmlRow In dt.Rows
            strData.Append("<tr>")
            For colCount = 0 To dt.Columns.Count - 1
                'Modified by PB for the issue: Export to Excel exports the bank account details in encrypted format.
                If ((encryptedColumnSortCode <> -1) And (encryptedColumnSortCode = colCount)) Or ((encryptedColumnAccNo <> -1) And (encryptedColumnAccNo = colCount)) Or ((encryptedColumnAccName <> -1) And (encryptedColumnAccName = colCount)) Then
                    If Not IsDBNull(HtmlRow(dt.Columns(colCount))) Then
                        strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & CStr(Chr(39) & Encryption.Decrypt(HtmlRow(dt.Columns(colCount))) & Chr(39)) & "</td>")
                    Else
                        strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>&nbsp; </td>")
                    End If
                Else
                    'Change Added by PB: For Invoice listing in OrderWorkDEMS
                    If ApplicationSettings.BizDivId = ApplicationSettings.OWDEBizDivId And ApplicationSettings.SiteType <> ApplicationSettings.siteTypes.admin Then
                        If HtmlRow(dt.Columns(colCount)).ToString = "Sales" Then
                            strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & "Faktura" & "</td>")
                        ElseIf HtmlRow(dt.Columns(colCount)).ToString = "Receipt" Then
                            strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & "Geldeingang" & "</td>")
                        Else
                            strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & HtmlRow(dt.Columns(colCount)) & "</td>")
                        End If
                    Else
                        If dt.Columns(colCount).ColumnName = "VoucherNumber" Then
                            strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;mso-style-parent:style0; mso-number-format:0;'>" & HtmlRow(dt.Columns(colCount)) & "</td>")
                        Else
                            strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>")
                            strData.Append(HtmlRow(dt.Columns(colCount)))
                            strData.Append("</td>")
                        End If

                    End If
                End If
            Next
        Next
        strData.Append("</tr>")
        strData.Append("</table>")

        'first let's clean up the response.object
        response.Clear()
        response.Charset = ""
        'set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("content-disposition", "attachment;filename=ExportToExcel.xls")
        response.Write(strData)
        response.End()

    End Sub



    ''' <summary>
    ''' Overloaded procedure to convert dataset to excel with additional parameter specify which column to not have in exported data
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <param name="response"></param>
    ''' <param name="colNameToDelete"></param>
    ''' <remarks></remarks>
    Public Shared Sub Convert(ByVal rolloutReport As List(Of RolloutReport), ByVal response As HttpResponse, ByVal colNameToDelete As ArrayList, Optional ByVal pagename As String = "")
        Dim objectType As Type = Type.GetType("Admin.AT800.RolloutReport", True, False)
        Dim colCount As Integer = 0
        Dim colCount2 As Integer = 0
        Dim strData As New System.Text.StringBuilder
        strData.Append("<table cellspacing=0 cellpadding=0 border=1;border-collapse:collapse;>")
        strData.Append("<tr>")
        For Each propertyInfo As PropertyInfo In objectType.GetProperties()
            Dim isMatch As Boolean = False
            For colCount = 0 To colNameToDelete.Count - 1
                If colNameToDelete.Item(colCount) = propertyInfo.Name Then
                    isMatch = True
                End If
            Next
            If Not isMatch Then
                Dim columnName As String = propertyInfo.GetCustomAttributesData()(0).NamedArguments(0).TypedValue.Value
                strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'>" & columnName & "</td>")
            End If
        Next
        strData.Append("</tr>")
        strData.Append("<tr>")
        For colCount = 0 To rolloutReport.Count - 1
            For Each propertyInfo As PropertyInfo In objectType.GetProperties()
                Dim isMatch As Boolean = False
                For colCount2 = 0 To colNameToDelete.Count - 1
                    If colNameToDelete.Item(colCount2) = propertyInfo.Name Then
                        isMatch = True
                    End If
                Next
                If Not isMatch Then
                    Dim indexArgs() As Object = {0}
                    strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;width:auto;text-align: left;'>" & propertyInfo.GetValue(rolloutReport(colCount), Nothing) & "</td>")
                End If
            Next
            strData.Append("</tr>")
        Next
        
        strData.Append("</table>")

        response.Clear()
        response.Charset = ""
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("content-disposition", "attachment;filename=ExportToExcel.xls")
        response.Write(strData)
        response.End()

    End Sub



    ''' <summary>
    ''' Function handles special case like
    ''' AMSpecialistVNValidated:
    '''     Inserts a column called Skills for each specialist in one table which is obtained by other table.
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <param name="specialCase"></param>
    ''' <param name="response"></param>
    ''' <remarks></remarks>
    Public Shared Sub Convert(ByVal ds As DataSet, ByVal specialCase As String, ByVal response As HttpResponse)

        Select Case specialCase
            Case "AMSpecialistVNValidated"
                Dim dtSpecialists As New DataTable
                Dim dtSpecialistsSkills As New DataTable
                dtSpecialists = ds.Tables("SpecialistList")
                dtSpecialistsSkills = ds.Tables("tblSpecialistSkills")
                dtSpecialists.Columns.Remove(dtSpecialists.Columns("IsDefault"))
                dtSpecialists.Columns.Remove(dtSpecialists.Columns("CompanyID"))
                Dim strData As New System.Text.StringBuilder
                strData.Append("<table cellspacing=0 cellpadding=0 border=1;border-collapse:collapse;>")
                strData.Append("<tr>")
                Dim colCount As Integer
                For colCount = 1 To dtSpecialists.Columns.Count - 1
                    strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'>" & dtSpecialists.Columns(colCount).ColumnName & "</td>")
                    If dtSpecialists.Columns(colCount).ColumnName = "Email" Then
                        strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'> Skills </td>")
                    End If
                Next
                strData.Append("</tr>")
                Dim HtmlRow As DataRow
                For Each HtmlRow In dtSpecialists.Rows
                    strData.Append("<tr>")
                    For colCount = 1 To dtSpecialists.Columns.Count - 1

                        If dtSpecialists.Columns(colCount).ColumnName = "Email" Then
                            strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & HtmlRow(dtSpecialists.Columns(colCount)) & "</td>")
                            Dim Skills As String = ""
                            dtSpecialistsSkills.DefaultView.RowFilter = "ContactId = " & CStr(HtmlRow(dtSpecialists.Columns("ContactID")))
                            For Each drv As DataRowView In dtSpecialistsSkills.DefaultView
                                If drv.Item("Name") <> "" Then
                                    If Skills <> "" Then
                                        Skills &= ", "
                                    End If
                                    Skills &= drv.Item("Name")
                                End If
                            Next
                            strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & Skills & "</td>")
                        Else
                            strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & HtmlRow(dtSpecialists.Columns(colCount)) & "</td>")
                        End If

                    Next
                Next
                strData.Append("</tr>")
                strData.Append("</table>")

                'first let's clean up the response.object
                response.Clear()
                response.Charset = ""
                'set the response mime type for excel
                response.ContentType = "application/vnd.ms-excel"
                response.AddHeader("content-disposition", "attachment;filename=ExportToExcel.xls")
                response.Write(strData)
                response.End()

            Case "CurrentSalesReport"

                Dim dt As New DataTable
                Dim strData As New System.Text.StringBuilder
                strData.Append("<table cellspacing=0 cellpadding=0 border=1;border-collapse:collapse;>")

                'Heading
                strData.Append("<tr>")
                strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'>All Oustanding Work Orders to date:  </td></tr>")

                If Not IsNothing(ds) Then


                    'Submit row
                    dt = ds.Tables("OSubmit")
                    DisplayRowCol(dt, strData, "Submitted: ", "number")

                    'Send row
                    dt = ds.Tables("OCA")
                    DisplayRowCol(dt, strData, "Conditionally Accepted: ", "number")

                    'Active row
                    dt = ds.Tables("OActive")
                    DisplayRowCol(dt, strData, "Active: ", "number")


                    'Issue row
                    dt = ds.Tables("OIssue")
                    DisplayRowCol(dt, strData, "Issue: ", "number")


                    'Completed row
                    dt = ds.Tables("OCompleted")
                    DisplayRowCol(dt, strData, "Completed: ", "number")


                    strData.Append("<tr></tr>")
                    'main heading for "All Submitted during the Current Month: "
                    strData.Append("<tr>")
                    strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'>All Submitted during the Current Month: </td></tr>")



                    'Total no. of Submitted Work Orders
                    dt = ds.Tables("Submitted")
                    DisplayRowCol(dt, strData, "Total no. of Submitted Work Orders:", "number")


                    'Total value of Submitted Work Orders
                    dt = ds.Tables("SubmittedVal")
                    DisplayRowCol(dt, strData, "Total value of Submitted Work Orders: ", "value")

                    'Total no. of CA Work Orders
                    dt = ds.Tables("CA")
                    DisplayRowCol(dt, strData, "Total no. of Conditionally Accepted Work Orders: ", "number")


                    'Total value of CA Work Orders
                    dt = ds.Tables("CAVal")
                    DisplayRowCol(dt, strData, "Total value of Conditionally Accepted Work Orders: ", "value")

                    'Total no. of Active Work Orders
                    dt = ds.Tables("Active")
                    DisplayRowCol(dt, strData, "Total no. of Active Work Orders: ", "number")


                    'Total value of Active Work Orders
                    dt = ds.Tables("ActiveVal")
                    DisplayRowCol(dt, strData, "Total value of Active Work Orders: ", "value")


                    'Total no. of Closed Work Orders
                    dt = ds.Tables("Closed")
                    DisplayRowCol(dt, strData, "Total no. of Closed Work Orders: ", "number")


                    'Total value of Closed Work Orders
                    dt = ds.Tables("ClosedVal")
                    DisplayRowCol(dt, strData, "Total value of Closed Work Orders: ", "value")


                    'Total no. of Cancelled Work Orders
                    dt = ds.Tables("Cancelled")
                    DisplayRowCol(dt, strData, "Total no. of Cancelled Work Orders: ", "number")

                    'Total value of Cancelled Work Orders
                    dt = ds.Tables("CancelledVal")
                    DisplayRowCol(dt, strData, "Total value of Cancelled Work Orders: ", "value")

                    strData.Append("<tr></tr>")
                    'main heading for "Monthly Revenues:  "
                    strData.Append("<tr>")
                    strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'>Monthly Revenues: </td></tr>")

                    'Total Listing Fee 
                    dt = ds.Tables("Listing")
                    DisplayRowCol(dt, strData, "Total Listing Fee: ", "value")

                    'VAT on Total Listing Fee 
                    dt = ds.Tables("VATonListingFee")
                    ''''''''''''''''''need to change
                    DisplayRowCol(dt, strData, "VAT on Total Listing Fee: ", "value")

                    'Total Commission Charge 
                    dt = ds.Tables("Comm")
                    DisplayRowCol(dt, strData, "Total Commission Charge: ", "value")

                    'VAT on Total Commission Charge 
                    dt = ds.Tables("VATonComm")
                    DisplayRowCol(dt, strData, "VAT on Total Commission Charge: ", "value")



                    strData.Append("<tr></tr>")
                    'main heading
                    strData.Append("<tr>")
                    strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'>Orderwork Account Balance: </td></tr>")

                    'funds to be received
                    dt = ds.Tables("FundsToRecvd")
                    DisplayRowCol(dt, strData, "Total 'Funds to be Received' amount: ", "value")

                    'funds received
                    dt = ds.Tables("FundsRecvd")
                    DisplayRowCol(dt, strData, "Total 'Funds Received' amount: ", "value")

                    'funds to be received outstanding
                    dt = ds.Tables("OutFundsToRecv")
                    DisplayRowCol(dt, strData, "Total 'Funds to be Received' outstanding amount: ", "value")

                    'completed payments
                    dt = ds.Tables("SuppPaym")
                    DisplayRowCol(dt, strData, "Total 'Completed Payments' amount:", "value")

                    'client balance
                    dt = ds.Tables("ClientBal")
                    DisplayRowCol(dt, strData, "Client Account Balance: ", "value")

                    strData.Append("</table>")
                    response.ContentType = "application/vnd.ms-excel"
                    response.AddHeader("content-disposition", "attachment;filename=ExportToExcel.xls")
                    response.Charset = ""
                    response.Write(strData)
                    response.Flush()
                    response.End()
                End If


            Case "CompletedSalesReport"


                Dim dt As New DataTable
                Dim strData As New System.Text.StringBuilder
                strData.Append("<table cellspacing=0 cellpadding=0 border=1;border-collapse:collapse;>")

                'main heading
                strData.Append("<tr>")
                strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'>Closed Work Orders: </td></tr>")

                If Not IsNothing(ds) Then
                    'closed wo no
                    dt = ds.Tables("Closed")
                    DisplayRowCol(dt, strData, "Total no. of Closed Work Orders: ", "number")

                    'closed wo val
                    dt = ds.Tables("ClosedVal")
                    DisplayRowCol(dt, strData, "Total value of Closed Work Orders: ", "value")

                    'listing fee
                    dt = ds.Tables("Listing")
                    DisplayRowCol(dt, strData, "Total Listing Fee: ", "value")

                    'commission
                    dt = ds.Tables("Comm")
                    DisplayRowCol(dt, strData, "Total Commission Charge: ", "value")

                    'cancelled wo no
                    dt = ds.Tables("Cancelled")
                    DisplayRowCol(dt, strData, "Total no. of Cancelled Work Orders: ", "number")

                    'cancelled wo val
                    dt = ds.Tables("CancelledVal")
                    DisplayRowCol(dt, strData, "Total value of Cancelled Work Orders: ", "value")

                    strData.Append("<tr></tr>")
                    'main heading
                    strData.Append("<tr>")
                    strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'>Orderwork Account Balance: </td></tr>")

                    'funds to be received
                    dt = ds.Tables("FundsToRecvd")
                    DisplayRowCol(dt, strData, "Total Funds to be Received: ", "value")

                    'funds received
                    dt = ds.Tables("FundsRecvd")
                    DisplayRowCol(dt, strData, "Total Funds Received: ", "value")

                    'funds to be received outstanding
                    dt = ds.Tables("OutFundsToRecv")
                    DisplayRowCol(dt, strData, "Total Funds to be Received Outstanding Amount: ", "value")

                    'completed payments
                    dt = ds.Tables("SuppPaym")
                    DisplayRowCol(dt, strData, "Total amount of Completed Payments: ", "value")

                    'client balance
                    dt = ds.Tables("ClientBal")
                    DisplayRowCol(dt, strData, "Client Account Balance: ", "value")

                    strData.Append("</table>")
                    response.ContentType = "application/vnd.ms-excel"
                    response.AddHeader("content-disposition", "attachment;filename=ExportToExcel.xls")
                    response.Charset = ""
                    response.Write(strData)
                    response.Flush()
                    response.End()
                End If




        End Select
    End Sub


    Public Shared Sub ConvertWOListing(ByVal ds As DataSet, ByVal response As HttpResponse, ByVal colNameToDelete As ArrayList, ByVal Group As String)
        Dim dt As New DataTable
        Dim colCount As Integer

        dt = ds.Tables(0)

        For colCount = 0 To colNameToDelete.Count - 1
            If Not IsNothing(dt.Columns(colNameToDelete.Item(colCount))) Then
                dt.Columns.Remove(dt.Columns(colNameToDelete.Item(colCount)))
            End If
        Next


        Dim strData As New System.Text.StringBuilder
        strData.Append("<table cellspacing=0 cellpadding=0 border=1;border-collapse:collapse;>")
        strData.Append("<tr>")

        For colCount = 0 To dt.Columns.Count - 1

            '****************************************For the Headers***************************
            If Group = ApplicationSettings.WOGroupSubmitted Then
                If dt.Columns(colCount).ColumnName = "DateCreated" Then
                    dt.Columns(colCount).ColumnName = "Submitted"
                ElseIf dt.Columns(colCount).ColumnName = "DateModified" Then
                    dt.Columns(colCount).ColumnName = "Modified"
                End If
            ElseIf Group = ApplicationSettings.WOGroupSent Then
                If dt.Columns(colCount).ColumnName = "DateCreated" Then
                    dt.Columns(colCount).ColumnName = "Submitted"
                ElseIf dt.Columns(colCount).ColumnName = "DateModified" Then
                    dt.Columns(colCount).ColumnName = "LastResponse"
                End If
            ElseIf Group = ApplicationSettings.WOGroupCA Then
                If dt.Columns(colCount).ColumnName = "DateCreated" Then
                    dt.Columns(colCount).ColumnName = "Submitted"
                ElseIf dt.Columns(colCount).ColumnName = "DateModified" Then
                    dt.Columns(colCount).ColumnName = "LastResponse"
                End If
            ElseIf Group = ApplicationSettings.WOGroupAccepted Or Group = "Activeelapsed" Or Group = "Staged" Then
                If dt.Columns(colCount).ColumnName = "DateCreated" Then
                    dt.Columns(colCount).ColumnName = "Accepted"
                ElseIf dt.Columns(colCount).ColumnName = "DateModified" Then
                    dt.Columns(colCount).ColumnName = "LastResponse"
                End If
            ElseIf Group = ApplicationSettings.WOGroupIssue Then
                If dt.Columns(colCount).ColumnName = "DateCreated" Then
                    dt.Columns(colCount).ColumnName = "IssueRaised"
                ElseIf dt.Columns(colCount).ColumnName = "DateModified" Then
                    dt.Columns(colCount).ColumnName = "LastResponse"
                End If
            ElseIf Group = ApplicationSettings.WOGroupCompleted Then
                If dt.Columns(colCount).ColumnName = "DateCreated" Then
                    dt.Columns(colCount).ColumnName = "Accepted"
                ElseIf dt.Columns(colCount).ColumnName = "DateModified" Then
                    dt.Columns(colCount).ColumnName = "Completed"
                End If
            ElseIf Group = ApplicationSettings.WOGroupClosed Then
                If dt.Columns(colCount).ColumnName = "DateCreated" Then
                    dt.Columns(colCount).ColumnName = "Accepted"
                ElseIf dt.Columns(colCount).ColumnName = "DateModified" Then
                    dt.Columns(colCount).ColumnName = "Closed"
                End If
            ElseIf Group = ApplicationSettings.WOGroupCancelled Then
                If dt.Columns(colCount).ColumnName = "DateCreated" Then
                    dt.Columns(colCount).ColumnName = "Submitted"
                ElseIf dt.Columns(colCount).ColumnName = "DateModified" Then
                    dt.Columns(colCount).ColumnName = "Cancelled"
                End If
            ElseIf Group = ApplicationSettings.WOGroupInvoiced Then
                If dt.Columns(colCount).ColumnName = "DateCreated" Then
                    dt.Columns(colCount).ColumnName = "Created"
                ElseIf dt.Columns(colCount).ColumnName = "DateModified" Then
                    dt.Columns(colCount).ColumnName = "InvoicedDate"
                End If
            Else
                If dt.Columns(colCount).ColumnName = "DateCreated" Then
                    dt.Columns(colCount).ColumnName = "Created"
                ElseIf dt.Columns(colCount).ColumnName = "DateModified" Then
                    dt.Columns(colCount).ColumnName = "Modified"
                End If
            End If
            '*****************************************************************************************
            strData.Append("<td style='font-weight: bold;font-size: 11px;color: #ffffff;font-family: Tahoma;background-color: #2650a6;text-decoration: none;height: 25px;text-align: left;'>" & dt.Columns(colCount).ColumnName & "</td>")
        Next
        strData.Append("</tr>")

        Dim HtmlRow As DataRow
        For Each HtmlRow In dt.Rows
            strData.Append("<tr>")
            For colCount = 0 To dt.Columns.Count - 1
                If ApplicationSettings.BizDivId = ApplicationSettings.OWDEBizDivId And ApplicationSettings.SiteType <> ApplicationSettings.siteTypes.admin Then
                    If HtmlRow(dt.Columns(colCount)).ToString = "Sales" Then
                        strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & "Faktura" & "</td>")
                    ElseIf HtmlRow(dt.Columns(colCount)).ToString = "Receipt" Then
                        strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & "Geldeingang" & "</td>")
                    Else
                        strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & HtmlRow(dt.Columns(colCount)) & "</td>")
                    End If
                Else
                    If dt.Columns(colCount).ColumnName = "VoucherNumber" Then
                        strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;mso-style-parent:style0; mso-number-format:0;'>" & HtmlRow(dt.Columns(colCount)) & "</td>")
                    Else
                        strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>")
                        strData.Append(HtmlRow(dt.Columns(colCount)))
                        strData.Append("</td>")
                    End If

                End If

            Next
        Next
        strData.Append("</tr>")
        strData.Append("</table>")

        'first let's clean up the response.object
        response.Clear()
        response.Charset = ""
        'set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("content-disposition", "attachment;filename=ExportToExcel.xls")
        response.Write(strData)
        response.End()

    End Sub

    Public Shared Function DisplayRowCol(ByVal dt As DataTable, ByVal strData As System.Text.StringBuilder, ByVal ColText As String, ByVal Type As String)
        Dim HtmlCol As DataColumn
        HtmlCol = dt.Columns(0)
        strData.Append("<tr>")
        strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: left;'>" & ColText & "</td>")

        Dim HtmlRow As DataRow
        For Each HtmlRow In dt.Rows
            Dim HtmlCol2 As DataColumn
            HtmlCol2 = dt.Columns(0)
            strData.Append("<td style='font-weight: normal;font-size: 11px;color:black;font-family: Tahoma;background-color: #ffffff;text-decoration: none;height: 25px;text-align: right;'>")

            If IsDBNull(HtmlRow(HtmlCol2.ColumnName)) Then
                If Type = "number" Then
                    strData.Append("0")
                Else
                    strData.Append(FormatCurrency(0.0, 2, TriState.True, TriState.True, TriState.False))
                End If
            Else
                If Type = "number" Then
                    strData.Append(HtmlRow(HtmlCol2.ColumnName))
                Else
                    strData.Append(FormatCurrency(HtmlRow(HtmlCol2.ColumnName), 2, TriState.True, TriState.True, TriState.False))
                End If
            End If
            strData.Append("</td>")
            strData.Append("</tr>")
        Next
    End Function


End Class
