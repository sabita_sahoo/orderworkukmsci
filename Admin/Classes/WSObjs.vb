Imports System.IO

Public Class WSObjs
    Inherits System.Web.UI.Page
    Private _siteCountry As String
    Private _bizDivId As Integer
    Private _siteType As String
  
#Region "WS Calls"

    Public Sub New()
        MyBase.New()
        country = ApplicationSettings.Country
        bizDivId = ApplicationSettings.BizDivId
        siteType = ApplicationSettings.SiteType
    End Sub
    Public Property country() As String
        Get
            Return _siteCountry
        End Get
        Set(ByVal value As String)
            _siteCountry = value
        End Set
    End Property
    Public Property bizDivId() As Integer
        Get
            Return _bizDivId
        End Get
        Set(ByVal value As Integer)
            _bizDivId = value
        End Set
    End Property

    Public Property siteType() As String
        Get
            Return _siteType
        End Get
        Set(ByVal value As String)
            _siteType = value
        End Set
    End Property


    Public ReadOnly Property WSWorkOrder() As WSWorkOrder.WSWorkOrder
        Get
            Try
                Static ws As WSWorkOrder.WSWorkOrder

                If ws Is Nothing Then
                    Try
                        ws = New WSWorkOrder.WSWorkOrder
                        ws.Url = Path.Combine(ApplicationSettings.WSRoot, "WSWorkOrder.asmx")
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If


                Dim wsAuthHeader As New WSWorkOrder.AuthenticationHeader
                wsAuthHeader.User = getUserName()
                wsAuthHeader.Password = getPassword()
                ws.AuthenticationHeaderValue = wsAuthHeader
                ws.IntializeProperties(country, bizDivId, siteType)
                Return ws
            Catch ex As Exception
                Throw ex
            Finally : End Try
        End Get
    End Property

    'Public ReadOnly Property WSiPhoneNotification() As WSiPhoneNotification.iPhoneNotificationService
    '    Get
    '        Try
    '            Static ws As WSiPhoneNotification.iPhoneNotificationService

    '            If ws Is Nothing Then
    '                Try
    '                    ws = New WSiPhoneNotification.iPhoneNotificationService
    '                    ws.Url = Path.Combine(ApplicationSettings.WSiPhoneRoot, "iPhoneNotificationService.asmx")
    '                Catch ex As Exception
    '                    Throw ex
    '                End Try
    '            End If
    '            Return ws
    '        Catch ex As Exception
    '            Throw ex
    '        Finally : End Try
    '    End Get
    'End Property


    Public ReadOnly Property WSSecurity() As WSSecurity.WSSecurity
        Get
            Try
                Static ws As WSSecurity.WSSecurity

                If ws Is Nothing Then
                    Try
                        ws = New WSSecurity.WSSecurity
                        ws.Url = Path.Combine(ApplicationSettings.WSRoot, "WSSecurity.asmx")
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If

                Dim cookieContainer As System.Net.CookieContainer = CType(Session.Item("SIOCookieContainer"), System.Net.CookieContainer)
                If cookieContainer Is Nothing Then
                    cookieContainer = New System.Net.CookieContainer
                End If

                ws.CookieContainer = cookieContainer
                Dim wsAuthHeader As New WSSecurity.AuthenticationHeader
                wsAuthHeader.User = getUserName()
                wsAuthHeader.Password = getPassword()
                ws.AuthenticationHeaderValue = wsAuthHeader
                ws.IntializeProperties(country, bizDivId, siteType)
                Return ws
            Catch ex As Exception
                Throw ex
            Finally
                ' do something smart here todo
            End Try
        End Get


    End Property

    Public ReadOnly Property WSContact() As WSContacts.WSContacts
        Get
            Try
                Static ws As WSContacts.WSContacts
                If ws Is Nothing Then
                    Try
                        ws = New WSContacts.WSContacts
                        ws.Url = Path.Combine(ApplicationSettings.WSRoot, "WSContacts.asmx")
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If

                Dim cookieContainer As System.Net.CookieContainer = CType(Session.Item("SIOCookieContainer"), System.Net.CookieContainer)
                If cookieContainer Is Nothing Then
                    cookieContainer = New System.Net.CookieContainer
                End If
                ws.CookieContainer = cookieContainer

                Dim wsAuthHeader As New WSContacts.AuthenticationHeader
                wsAuthHeader.User = getUserName()
                wsAuthHeader.Password = getPassword()
                ws.AuthenticationHeaderValue = wsAuthHeader
                ws.IntializeProperties(country, bizDivId, siteType)
                Return ws
            Catch ex As Exception
                Throw ex
            Finally : End Try
        End Get
    End Property

    Public ReadOnly Property WSStandards() As WSStandards.WSStandards
        Get
            Try
                Static ws As WSStandards.WSStandards

                If ws Is Nothing Then
                    Try
                        ws = New WSStandards.WSStandards
                        ws.Url = Path.Combine(ApplicationSettings.WSRoot, "WSStandards.asmx")
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If

                Dim cookieContainer As System.Net.CookieContainer = CType(Session.Item("SIOCookieContainer"), System.Net.CookieContainer)
                If cookieContainer Is Nothing Then
                    cookieContainer = New System.Net.CookieContainer
                End If

                ws.CookieContainer = cookieContainer
                Dim wsAuthHeader As New WSStandards.AuthenticationHeader
                wsAuthHeader.User = getUserName()
                wsAuthHeader.Password = getPassword()
                ws.AuthenticationHeaderValue = wsAuthHeader
                ws.IntializeProperties(country, bizDivId, siteType)
                Return ws
            Catch ex As Exception
                Throw ex
            Finally
                ' do something smart here todo
            End Try
        End Get
    End Property

    Public ReadOnly Property WSFinance() As WSFinance.WSFinance
        Get
            Try
                Static ws As WSFinance.WSFinance

                If ws Is Nothing Then
                    Try
                        ws = New WSFinance.WSFinance
                        ws.Url = Path.Combine(ApplicationSettings.WSRoot, "WSFinance.asmx")
                    Catch ex As Exception
                        Throw ex
                    End Try
                End If

                Dim cookieContainer As System.Net.CookieContainer = CType(Session.Item("SIOCookieContainer"), System.Net.CookieContainer)
                If cookieContainer Is Nothing Then
                    cookieContainer = New System.Net.CookieContainer
                End If

                ws.CookieContainer = cookieContainer
                Dim wsAuthHeader As New WSFinance.AuthenticationHeader
                wsAuthHeader.User = getUserName()
                wsAuthHeader.Password = getPassword()
                ws.AuthenticationHeaderValue = wsAuthHeader
                ws.IntializeProperties(country, bizDivId, siteType)
                Return ws
            Catch ex As Exception
                Throw ex
            Finally
                ' do something smart here todo
            End Try
        End Get
    End Property

    'Public ReadOnly Property WSPostCode() As WSPostCode.LookupUK
    '    Get
    '        Try
    '            Static ws As WSPostCode.LookupUK

    '            If ws Is Nothing Then
    '                Try
    '                    ws = New WSPostCode.LookupUK
    '                Catch ex As Exception
    '                    Throw ex
    '                End Try
    '            End If

    '            Dim cookieContainer As System.Net.CookieContainer = CType(Session.Item("SIOCookieContainer"), System.Net.CookieContainer)
    '            If cookieContainer Is Nothing Then
    '                cookieContainer = New System.Net.CookieContainer
    '            End If
    '            ws.CookieContainer = cookieContainer
    '            Return ws
    '        Catch ex As Exception
    '            Throw ex
    '        Finally
    '            ' do something smart here todo
    '        End Try
    '    End Get
    'End Property
#End Region

#Region "Generic Function"
    Public Function getUserName() As String
        
        Return ApplicationSettings.GuestLogin

    End Function
    Public Function getPassword() As String
       
        Return Encryption.Encrypt(ApplicationSettings.GuestPassword)

    End Function

#End Region
End Class
