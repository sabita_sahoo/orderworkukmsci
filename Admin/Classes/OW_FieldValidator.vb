﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Text.RegularExpressions
Imports Microsoft.Security.Application

Namespace OrderWorkUK.Security

    Public Enum ValidatorType
        CheckBox
        Number
        PhoneNumber
        PostCode
        Characters
        CharactersWithPunctuation
        Email
        HTML
        URL
        CustomRegEx
    End Enum

    <DefaultProperty("TypeOfValidator"), ToolboxData("<{0}:ServerControl1 runat=server></{0}:ServerControl1>")>
    Public Class OWFormFieldValidator
        Inherits CustomValidator


        Private Shared _PostCodeRegEx As New Regex("^(GIR ?0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)|[0-9][A-HJKPS-UW]) ?[0-9][ABD-HJLNP-UW-Z]{2})$", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Private Shared _CharactersRegEx As New Regex("^([ \u00c0-\u01ffa-zA-Z'])+$", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Private Shared _EmailRegEx As New Regex("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Private Shared _URLRegEx As New Regex("^((ht|f)tp(s?)\:\/\/|~/|/)?([\w]+:\w+@)?([a-zA-Z]{1}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?((/?\w+/)+|/?)(\w+\.[\w]{3,4})?((\?\w+=\w+)?(&\w+=\w+)*)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled)

        Private Shared _CharacterWithPunctuation As New Regex("", RegexOptions.Compiled)
        Private Shared _PhoneNumberRegEx As New Regex("^[0-9\s\(\)\+\-]+$", RegexOptions.IgnoreCase)

        Private _RegEx As Regex = Nothing
        Private _TypeOfValidator As ValidatorType = ValidatorType.Characters
        Private _ValueMandatory As Boolean = 1
        Private _ControlPlaceHolder As Control = Nothing
        Private _ErrorMessageNormal As String = Nothing

        Public Sub New()
            MyBase.new()
            _ControlPlaceHolder = Me.Parent
        End Sub

        Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
            _ControlPlaceHolder = Me.Parent.FindControl(Me.ControlToValidate)
            _ErrorMessageNormal = ErrorMessage
            MyBase.OnInit(e)
        End Sub


        <Bindable(True), Category("Appearance"), DefaultValue(True)>
        Public Property DataMandatory() As Boolean
            Get
                Return _ValueMandatory
            End Get
            Set(ByVal value As Boolean)
                _ValueMandatory = value
            End Set
        End Property

        <Bindable(True), Category("Appearance")>
        Public Property TypeOfValidator() As ValidatorType
            Get
                Return _TypeOfValidator
            End Get

            Set(ByVal Value As ValidatorType)
                _TypeOfValidator = Value
            End Set
        End Property

        <Bindable(True), Category("Appearance")>
        Public Property RegularExpression() As String
            Get
                Return _RegEx.ToString
            End Get
            Set(ByVal value As String)
                _RegEx = New Regex(value, RegexOptions.Compiled)
            End Set
        End Property

        <Bindable(True), Category("Appearance")>
        Protected Overrides Function ControlPropertiesValid() As Boolean
            If Me.ControlToValidate.Length = 0 Then
                Throw New HttpException(String.Format("The ControlToValidate property of '{0}' cannot be blank.", Me.ID))
            End If
            Return True
        End Function

        Protected Overrides Sub Render(ByVal output As HtmlTextWriter)
            If Me.DesignMode Then
                output.Write("<span>" & ErrorMessage & " with:(" & Me._TypeOfValidator.ToString & ") IsRequired:'" & _ValueMandatory & "'</span>")
            Else
                MyBase.Render(output)
            End If

        End Sub

        Protected Shadows Function GetControlValidationValue(ByVal name As String) As String
            Dim k As CheckBox = TryCast(_ControlPlaceHolder, CheckBox)
            If k IsNot Nothing Then
                _TypeOfValidator = ValidatorType.CheckBox
                If _ValueMandatory And k.Checked = False Then Return ""
                Return k.Checked
            Else
                Return MyBase.GetControlValidationValue(name)
            End If
        End Function

        Protected Overrides Function EvaluateIsValid() As Boolean
            Dim v As String = GetControlValidationValue(Me.ControlToValidate).Trim()
            If _ValueMandatory = True And v.Length = 0 Then
                ErrorMessage = "Field is required"
                Return False
            Else
                ErrorMessage = _ErrorMessageNormal
            End If

            Dim regToUse As Regex = Nothing

            Select Case _TypeOfValidator
                Case ValidatorType.Characters
                    regToUse = _CharactersRegEx
                Case ValidatorType.CharactersWithPunctuation
                    regToUse = _CharacterWithPunctuation
                Case ValidatorType.Email
                    regToUse = _EmailRegEx
                Case ValidatorType.Number
                    Return IsNumeric(v)
                Case ValidatorType.PhoneNumber
                    regToUse = _PhoneNumberRegEx
                Case ValidatorType.PostCode
                    regToUse = _PostCodeRegEx
                Case ValidatorType.URL
                    regToUse = _URLRegEx
                Case ValidatorType.CustomRegEx
                    regToUse = _RegEx
                Case ValidatorType.HTML
                    'If AntiXss.GetSafeHtmlFragment(v).Replace("&nbsp;&nbsp;", " ").Replace("&#145;", Chr(145)).Replace("&#146;", Chr(146)).Replace("&#147;", Chr(147)).Replace("&#148;", Chr(148)).Replace("&#150;", Chr(150)).Replace("&#151;", Chr(151)) = AntiXss.GetSafeHtmlFragment(ReturnStringWhitelist(AntiXss.HtmlEncode(v))) Then
                    If AntiXss.GetSafeHtmlFragment(v).Replace("&nbsp;", "").Replace("&", "&amp;") = AntiXss.GetSafeHtmlFragment(ReturnStringWhitelist(AntiXss.HtmlEncode(v))) Then

                        Return True
                    Else
                        Return False
                    End If
                Case ValidatorType.CheckBox
                    If v = "True" Or v = "False" Then
                        Return True
                    Else
                        Return False
                    End If
            End Select
            If regToUse IsNot Nothing Then
                Return regToUse.IsMatch(v)
            End If
            Return False
        End Function
    End Class
End Namespace
