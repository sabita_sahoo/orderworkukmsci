'------------------------------------------------------------------------------
' <Summary>
'   <ProjectName>Orderwork</ProjectName>
'   <FileName>EMails.vb</FileName>
'   <Module>Compose Mail</Module>
'   <Description> Contains Function to compose mails as per request</Description>
'   <CreatedDate>1/03/2007</CreatedDate>
'   <References>
'   </References>
' </Summary>
'------------------------------------------------------------------------------

'import the namespace
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports System.IO

Imports System.Collections


Public Class Emails   

    Shared ws As New WSObjs()

    ''' <summary>
    ''' Fields to populate the Workorder mail. Worko order ID
    ''' </summary>
    ''' <remarks></remarks>
    Private _WorkOrderID As String
    Public Property WorkOrderID() As String
        Get
            Return _WorkOrderID
        End Get
        Set(ByVal value As String)
            _WorkOrderID = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder title
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOTitle As String
    Public Property WOTitle() As String
        Get
            Return _WOTitle
        End Get
        Set(ByVal value As String)
            _WOTitle = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder location
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOLoc As String
    Public Property WOLoc() As String
        Get
            Return _WOLoc
        End Get
        Set(ByVal value As String)
            _WOLoc = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder category
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOCategory As String
    Public Property WOCategory() As String
        Get
            Return _WOCategory
        End Get
        Set(ByVal value As String)
            _WOCategory = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder Price
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOPrice As Decimal
    Public Property WOPrice() As Decimal
        Get
            Return _WOPrice
        End Get
        Set(ByVal value As Decimal)
            _WOPrice = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder Price
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOCustomerPrice As Decimal
    Public Property WOCustomerPrice() As Decimal
        Get
            Return _WOCustomerPrice
        End Get
        Set(ByVal value As Decimal)
            _WOCustomerPrice = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder Wholesale Price
    ''' </summary>
    ''' <remarks></remarks>
    Private _WPPrice As Decimal
    Public Property WPPrice() As Decimal
        Get
            Return _WPPrice
        End Get
        Set(ByVal value As Decimal)
            _WPPrice = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder Platform Price
    ''' </summary>
    ''' <remarks></remarks>
    Private _PPPrice As Decimal
    Public Property PPPrice() As Decimal
        Get
            Return _PPPrice
        End Get
        Set(ByVal value As Decimal)
            _PPPrice = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder Start Date
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOStartDate As String
    Public Property WOStartDate() As String
        Get
            Return _WOStartDate
        End Get
        Set(ByVal value As String)
            _WOStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder End Date
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOEndDate As String
    Public Property WOEndDate() As String
        Get
            Return _WOEndDate
        End Get
        Set(ByVal value As String)
            _WOEndDate = value
        End Set
    End Property

    ''' <summary>
    ''' New Workorder Price
    ''' </summary>
    ''' <remarks></remarks>
    Private _NewWOPrice As Decimal
    Public Property NewWOPrice() As Decimal
        Get
            Return _NewWOPrice
        End Get
        Set(ByVal value As Decimal)
            _NewWOPrice = value
        End Set
    End Property

    ''' <summary>
    ''' New Workorder Supply parts
    ''' </summary>
    ''' <remarks></remarks>
    Private _NewWOSupplyParts As String
    Public Property NewWOSupplyParts() As String
        Get
            Return _NewWOSupplyParts
        End Get
        Set(ByVal value As String)
            _NewWOSupplyParts = value
        End Set
    End Property


    ''' <summary>
    ''' New Workorder Start Date
    ''' </summary>
    ''' <remarks></remarks>
    Private _NewWOStartDate As String
    Public Property NewWOStartDate() As String
        Get
            Return _NewWOStartDate
        End Get
        Set(ByVal value As String)
            _NewWOStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' New Workorder End Date
    ''' </summary>
    ''' <remarks></remarks>
    Private _NewWOEndDate As String
    Public Property NewWOEndDate() As String
        Get
            Return _NewWOEndDate
        End Get
        Set(ByVal value As String)
            _NewWOEndDate = value
        End Set
    End Property
    ''' <summary>
    ''' Estimated Time in days
    ''' </summary>
    ''' <remarks></remarks>
    Private _EstimatedTimeInDays As String
    Public Property EstimatedTimeInDays() As String
        Get
            Return _EstimatedTimeInDays
        End Get
        Set(ByVal value As String)
            _EstimatedTimeInDays = value
        End Set
    End Property
    ''' <summary>
    ''' Estimated Time in days
    ''' </summary>
    ''' <remarks></remarks>
    Private _EstimatedTimeInDaysNew As String
    Public Property EstimatedTimeInDaysNew() As String
        Get
            Return _EstimatedTimeInDaysNew
        End Get
        Set(ByVal value As String)
            _EstimatedTimeInDaysNew = value
        End Set
    End Property
    ''' <summary>
    ''' Work Order Platform Day Rate
    ''' </summary>
    ''' <remarks></remarks>
    Private _WorkOrderDayRate As String
    Public Property WorkOrderDayRate() As String
        Get
            Return _WorkOrderDayRate
        End Get
        Set(ByVal value As String)
            _WorkOrderDayRate = value
        End Set
    End Property
    ''' <summary>
    ''' Work Order Wholesale Day Rate
    ''' </summary>
    ''' <remarks></remarks>
    Private _WorkOrderWholesaleDayRate As String
    Public Property WorkOrderWholesaleDayRate() As String
        Get
            Return _WorkOrderWholesaleDayRate
        End Get
        Set(ByVal value As String)
            _WorkOrderWholesaleDayRate = value
        End Set
    End Property
    ''' <summary>
    ''' Work Order Day Rate
    ''' </summary>
    ''' <remarks></remarks>
    Private _WorkOrderDayRateNew As String
    Public Property WorkOrderDayRateNew() As String
        Get
            Return _WorkOrderDayRateNew
        End Get
        Set(ByVal value As String)
            _WorkOrderDayRateNew = value
        End Set
    End Property
    ''' <summary>
    ''' Staged WO or not
    ''' </summary>
    ''' <remarks></remarks>
    Private _StagedWO As String
    Public Property StagedWO() As String
        Get
            Return _StagedWO
        End Get
        Set(ByVal value As String)
            _StagedWO = value
        End Set
    End Property

    ''' <summary>
    ''' QuoteRequired
    ''' </summary>
    ''' <remarks></remarks>
    Private _QuoteRequired As Boolean = False
    Public Property QuoteRequired() As Boolean
        Get
            Return _QuoteRequired
        End Get
        Set(ByVal value As Boolean)
            _QuoteRequired = value
        End Set
    End Property

    ''' <summary>
    ''' Pass WorkOrder related date depending on WO Status e.g. Submitted Date, Accepted Date etc.
    ''' </summary>
    ''' <remarks></remarks>
    Private _WODate As String = ""
    Public Property WODate() As String
        Get
            Return _WODate
        End Get
        Set(ByVal value As String)
            _WODate = value
        End Set
    End Property
    Private _EstimatedTime As String = ""
    Public Property EstimatedTime() As String
        Get
            Return _EstimatedTime
        End Get
        Set(ByVal value As String)
            _EstimatedTime = value
        End Set
    End Property

    ''' <summary>
    ''' Pass WorkOrder description
    ''' </summary>
    ''' <remarks></remarks>
    Private _WODesc As String = ""
    Public Property WODesc() As String
        Get
            Return _WODesc
        End Get
        Set(ByVal value As String)
            _WODesc = value
        End Set
    End Property



    Private _Accreditation As String = ""
    Public Property Accreditation() As String
        Get
            Return _Accreditation
        End Get
        Set(ByVal value As String)
            _Accreditation = value
        End Set
    End Property

    Private _IsNearestToUsed As Boolean = False
    Public Property IsNearestToUsed() As Boolean
        Get
            Return _IsNearestToUsed
        End Get
        Set(ByVal value As Boolean)
            _IsNearestToUsed = value
        End Set
    End Property

    Private _CRBChecked As Boolean = False
    Public Property CRBChecked() As Boolean
        Get
            Return _CRBChecked
        End Get
        Set(ByVal value As Boolean)
            _CRBChecked = value
        End Set
    End Property
    Private _UKSecurityChecked As Boolean = False
    Public Property UKSecurityChecked() As Boolean
        Get
            Return _UKSecurityChecked
        End Get
        Set(ByVal value As Boolean)
            _UKSecurityChecked = value
        End Set
    End Property
    Private _CSCSChecked As Boolean = False
    Public Property CSCSChecked() As Boolean
        Get
            Return _CSCSChecked
        End Get
        Set(ByVal value As Boolean)
            _CSCSChecked = value
        End Set
    End Property
    Private _EngCRBChecked As Boolean = False
    Public Property EngCRBChecked() As Boolean
        Get
            Return _EngCRBChecked
        End Get
        Set(ByVal value As Boolean)
            _EngCRBChecked = value
        End Set
    End Property
    Private _EngUKSecurity As Boolean = False
    Public Property EngUKSecurity() As Boolean
        Get
            Return _EngUKSecurity
        End Get
        Set(ByVal value As Boolean)
            _EngUKSecurity = value
        End Set
    End Property
    Private _EngCSCSChecked As Boolean = False
    Public Property EngCSCSChecked() As Boolean
        Get
            Return _EngCSCSChecked
        End Get
        Set(ByVal value As Boolean)
            _EngCSCSChecked = value
        End Set
    End Property
    Private _EngRightToWorkChecked As Boolean = False
    Public Property EngRightToWorkChecked() As Boolean
        Get
            Return _EngRightToWorkChecked
        End Get
        Set(ByVal value As Boolean)
            _EngRightToWorkChecked = value
        End Set
    End Property
    Private _ThermostatsQuestions As String = ""
    Public Property ThermostatsQuestions() As String
        Get
            Return _ThermostatsQuestions
        End Get
        Set(ByVal value As String)
            _ThermostatsQuestions = value
        End Set
    End Property

    Private _ppcancel As Decimal
    Public Property ppcancel() As Decimal
        Get
            Return _ppcancel
        End Get
        Set(ByVal value As Decimal)
            _ppcancel = value
        End Set
    End Property

    Private _wpcancel As Decimal
    Public Property wpcancel() As Decimal
        Get
            Return _wpcancel
        End Get
        Set(ByVal value As Decimal)
            _wpcancel = value
        End Set
    End Property

    Public Shared Sub SendWorkOrderMail(ByVal objEmail As Emails, ByVal ActionBy As String, ByVal WOAction As String, Optional ByVal Comments As String = "", Optional ByVal dvEmail As DataView = Nothing, Optional ByVal WorkOrderStatus As Integer = 0, Optional ByVal addRowFilter As String = "")
        Dim strSubjectB, strSubjectS, strSubjectA As String
        Dim strContentB, strContentS, strContentA As String
        Dim SendMailToA As Boolean = False
        Dim SendMailToB As Boolean = False
        Dim SendMailToS As Boolean = False
        Dim BusinessArea As Integer
        'Supplier & Buyer Information
        Dim BName, BEmail As String
        Dim SName, SEmail As String
        If Not IsNothing(dvEmail) Then
            dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'To'" & "   " & addRowFilter
            If dvEmail.Count > 0 Then
                BusinessArea = dvEmail.Item(0).Item("BusinessArea")
                If Not IsDBNull(dvEmail.Item(0).Item("Name")) Then
                    BName = Trim(dvEmail.Item(0).Item("Name"))
                    BEmail = Trim(dvEmail.Item(0).Item("Email"))
                End If
            End If
            dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'To'" & "   " & addRowFilter
            If dvEmail.Count > 0 Then
                If Not IsDBNull(dvEmail.Item(0).Item("Name")) Then
                    SName = Trim(dvEmail.Item(0).Item("Name"))
                    SEmail = Trim(dvEmail.Item(0).Item("Email"))
                End If
            End If
        End If


        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String

        'Submit WO
        If WOAction = ApplicationSettings.WOAction.SubmitWO Then
            CommonFunctions.createLog("SubmitWO")

            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectB = "Submission of work request"
            Else
                'Put German text here
                strSubjectB = "Submission of work request"
            End If
            strSubjectA = strSubjectB
            'Email Content
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOSubmission.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd

            strContentB = Buffer
            sr.Close()
            strContentA = strContentB
            'Send Mail to Buyer & Admin
            SendMailToA = ControlMail.IsMailOnSubmitWO


            SendMailToB = ControlMail.IsMailOnSubmitWOToBuyer
        End If
        If WOAction = ApplicationSettings.WOAction.WOSPAccept Then
            CommonFunctions.createLog("WOSPAccept")
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "Acceptance of Work Order " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "Acceptance of Work Order " & objEmail.WorkOrderID
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOSPAccept.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            If (objEmail.ThermostatsQuestions <> "") Then
                strContentS = strContentS.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
                strContentA = strContentA.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
            Else
                strContentS = strContentS.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
                strContentA = strContentA.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
            End If
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnSPAccept
            SendMailToS = ControlMail.IsMailOnSPAcceptToSP
            'Check if WO is Active after Supplier Accept or Accept of CA and send Buyer Active Email
            If WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                'Email Subject
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectB = "Work Request " & objEmail.WorkOrderID & " is now Active"
                Else
                    'Put German text here
                    strSubjectB = "Work Request " & objEmail.WorkOrderID & " is now Active"
                End If
                'Email Content
                WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOActiveBuyer.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentB = Buffer
                sr.Close()
                If (objEmail.ThermostatsQuestions <> "") Then
                    strContentB = strContentB.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
                Else
                    strContentB = strContentB.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
                End If
                'Send Mail to Buyer
                SendMailToB = ControlMail.IsMailOnSPAcceptToBuyer
            End If
        End If
        If WOAction = ApplicationSettings.WOAction.WOSPCAccept Then
            CommonFunctions.createLog("WOSPCAccept")
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "Conditional Acceptance of Work Order " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "Conditional Acceptance of Work Order " & objEmail.WorkOrderID
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOCA.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnSPCAccept
            SendMailToS = ControlMail.IsMailOnSPCAcceptToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WOSPChangeCA Then
            CommonFunctions.createLog("WOSPChangeCA")
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "You have revised the terms of your conditional acceptance relating to work order ID " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "You have revised the terms of your conditional acceptance relating to work order ID " & objEmail.WorkOrderID
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOChangeCATerms.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnSPChangeCA
            SendMailToS = ControlMail.IsMailOnSPChangeCAToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WODiscussCA Then
            CommonFunctions.createLog("WODiscussCA")
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "DISCUSS JOB TERMS - " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "Conditional Acceptance discussion details relating to Work Order " & objEmail.WorkOrderID
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WODiscussCA.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnDiscussCA
            SendMailToS = True
        End If
        If WOAction = ApplicationSettings.WOAction.WOAcceptCA Then
            CommonFunctions.createLog("WOAcceptCA")
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "ACCEPT JOB TERMS - " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "The terms of your Conditional Acceptance for work order " & objEmail.WorkOrderID & " have been accepted."
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOAcceptCA.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            If (objEmail.ThermostatsQuestions <> "") Then
                strContentS = strContentS.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
                strContentA = strContentA.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
            Else
                strContentS = strContentS.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
                strContentA = strContentA.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
            End If
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnAcceptCA
            SendMailToS = ControlMail.IsMailOnAcceptCAToSP
            'Check if WO is Active after Supplier Accept or Accept of CA and send Buyer Active Email
            If WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                'Email Subject
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectB = "Work Request " & objEmail.WorkOrderID & " is now Active."
                Else
                    'Put German text here
                    strSubjectB = "Work Request " & objEmail.WorkOrderID & " is now Active."
                End If
                'Email Content
                WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOActiveBuyer.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentB = Buffer
                sr.Close()
                If (objEmail.ThermostatsQuestions <> "") Then
                    strContentB = strContentB.Replace("<ThermostatsQuestions>", objEmail.ThermostatsQuestions)
                Else
                    strContentB = strContentB.Replace("<ThermostatsQuestions>", "").Replace("Questionnaire:", "")
                End If
                'Send Mail to Buyer
                SendMailToB = ControlMail.IsMailOnAcceptCAToBuyer
            End If
        End If
        If WOAction = ApplicationSettings.WOAction.WORaiseIssue Then
            CommonFunctions.createLog("WORaiseIssue")
            If ActionBy = ApplicationSettings.ViewerSupplier Then
                'Email Subject
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectS = "An Issue has been raised relating to Work Order " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectS = "You have raised an Issue relating to Work Order " & objEmail.WorkOrderID
                End If
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectA = "An Issue has been raised relating to Work Order " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectA = "An Issue has been raised relating to Work Order " & objEmail.WorkOrderID
                End If
                'Email Content
                WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOSPRaiseIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentS = Buffer
                sr.Close()

                WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOOWRaiseIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentA = Buffer
                sr.Close()
            Else
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectS = "JOB HAS AN ISSUE - " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectS = "An Issue has been raised relating to Work Order " & objEmail.WorkOrderID
                End If
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectA = "You have raised an Issue relating to Work Order " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectA = "You have raised an Issue relating to Work Order " & objEmail.WorkOrderID
                End If
                'Email Content
                WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOOWRaiseIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentS = Buffer
                sr.Close()

                WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOSPRaiseIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentA = Buffer
                sr.Close()
            End If
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailRaiseIssue
            SendMailToS = ControlMail.IsMailRaiseIssueToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WOChangeIssue Then
            CommonFunctions.createLog("WOChangeIssue")
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "JOB TERMS CHANGED - " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "Issue terms relating to Work Order " & objEmail.WorkOrderID & " have been revised"
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOChangeIssueTerms.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnChangeIssue
            SendMailToS = ControlMail.IsMailOnChangeIssueToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WODiscuss Then
            CommonFunctions.createLog("WODiscuss")
            If ActionBy = ApplicationSettings.ViewerSupplier Then
                'Email Subject
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    strSubjectS = "Discussion details of Issue relating to Work Order " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectS = "Discussion details of Issue relating to Work Order " & objEmail.WorkOrderID
                End If
                strSubjectA = strSubjectS

                'Email Content
                WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOSPDiscussIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentS = Buffer
                sr.Close()
                WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOOWDiscussIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentA = Buffer
                sr.Close()
            Else
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then

                    strSubjectS = "JOB ISSUE UPDATE - " & objEmail.WorkOrderID
                Else
                    'Put German text here
                    strSubjectS = "Discussion details of Issue relating to Work Order " & objEmail.WorkOrderID
                End If
                strSubjectA = strSubjectS

                'Email Content
                WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOOWDiscussIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentS = Buffer
                sr.Close()
                WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOSPDiscussIssue.txt")
                WebResponse = WebRequest.GetResponse()
                sr = New StreamReader(WebResponse.GetResponseStream())
                Buffer = sr.ReadToEnd
                strContentA = Buffer
                sr.Close()
            End If            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnDiscuss
            SendMailToS = ControlMail.IsMailOnDiscussToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WOAcceptIssue Then
            CommonFunctions.createLog("WOAcceptIssue")
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "ISSUE HAS BEEN ACCEPTED - " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = "Issue terms relating to Work Order " & objEmail.WorkOrderID & " have been accepted"
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOAcceptIssueTerms.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnAcceptIssue
            SendMailToS = ControlMail.IsMailOnAcceptIssueToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WORejectIssue Then
            CommonFunctions.createLog("WORejectIssue")
            'Email Subject
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "ISSUE REJECTED " & " - " & objEmail.WorkOrderID
            Else
                'Put German text here
                strSubjectS = objEmail.WorkOrderID & " issue has been rejected by OrderWork"
            End If
            strSubjectA = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WORejectIssueTerms.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnRejectIssue
            SendMailToS = ControlMail.IsMailOnRejectIssueToSP
        End If
        If WOAction = ApplicationSettings.WOAction.WOComplete Then
            CommonFunctions.createLog("WOComplete")
            'Email Subject

            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "Work Order " & objEmail.WorkOrderID & " has been marked as Complete"
            Else
                'Put German text here
                strSubjectS = "Work Order " & objEmail.WorkOrderID & " has been marked as Complete"
            End If
            strSubjectA = strSubjectS
            strSubjectB = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOComplete.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            strContentB = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnComplete
            SendMailToS = ControlMail.IsMailOnCompleteToSP
            SendMailToB = ControlMail.IsMailOnCompleteToBuyer
        End If
        If WOAction = ApplicationSettings.WOAction.WOClose Then
            CommonFunctions.createLog("WOClose")
            'Email Subject

            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubjectS = "Work Order " & objEmail.WorkOrderID & " has been marked as Closed"
            Else
                'Put German text here
                strSubjectS = "Work Order " & objEmail.WorkOrderID & " has been marked as Closed"
            End If
            strSubjectA = strSubjectS
            strSubjectB = strSubjectS
            'Email Content
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOClose.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            strContentS = Buffer
            sr.Close()
            strContentA = strContentS
            strContentB = strContentS
            'Send Mail to Supplier & Admin
            SendMailToA = ControlMail.IsMailOnClose
            SendMailToS = ControlMail.IsMailOnCloseToSP
            SendMailToB = ControlMail.IsMailOnCloseToBuyer
        End If

        Dim strPrice As String = ""
        'Populate WO Details
        If Not IsNothing(strContentS) Then
            strContentS = strContentS.Replace("<Name>", SName)
            strContentS = strContentS.Replace("<WorkOrderID>", objEmail.WorkOrderID)
            strContentS = strContentS.Replace("<WOTitle>", objEmail.WOTitle)
            strContentS = strContentS.Replace("<WOCategory>", objEmail.WOCategory)
            strContentS = strContentS.Replace("<Location>", objEmail.WOLoc)
            If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
                strContentS = strContentS.Replace("<StartDate>", objEmail.WOStartDate)
                strContentS = strContentS.Replace("Start Date:", "Appointment Date:")
                strContentS = strContentS.Replace("Proposed End Date:", "").Replace("End Date:", "")
                strContentS = strContentS.Replace("<EndDate>", "")
            Else
                strContentS = strContentS.Replace("<StartDate>", objEmail.WOStartDate & Chr(10))
                strContentS = strContentS.Replace("<EndDate>", objEmail.WOEndDate)
            End If
            strContentS = strContentS.Replace("<Price>", FormatCurrency(objEmail.PPPrice, 2, TriState.True, TriState.True, TriState.False))
            strContentS = strContentS.Replace("<Comment>", Trim(Comments))
            strContentS = strContentS.Replace("<Date>", objEmail.WODate)
        End If
        If Not IsNothing(strContentB) Then
            strContentB = strContentB.Replace("<Name>", BName)
            strContentB = strContentB.Replace("<WorkOrderID>", objEmail.WorkOrderID)
            strContentB = strContentB.Replace("<WOTitle>", objEmail.WOTitle)
            strContentB = strContentB.Replace("<WOCategory>", objEmail.WOCategory)
            strContentB = strContentB.Replace("<Location>", objEmail.WOLoc)
            If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
                strContentB = strContentB.Replace("<StartDate>", objEmail.WOStartDate)
                strContentB = strContentB.Replace("Start Date:", "Appointment Date:")
                strContentB = strContentB.Replace("Proposed End Date:", "").Replace("End Date:", "")
                strContentB = strContentB.Replace("<EndDate>", "")
            Else
                strContentB = strContentB.Replace("<StartDate>", objEmail.WOStartDate & Chr(10))
                strContentB = strContentB.Replace("<EndDate>", objEmail.WOEndDate)
            End If
            If objEmail.QuoteRequired = True Then
                strPrice = "Quotation Requested"
            Else
                If Not IsDBNull(objEmail.WOCustomerPrice) Or objEmail.WOCustomerPrice > 0 Then
                    strPrice = FormatCurrency(objEmail.WOCustomerPrice, 2, TriState.True, TriState.True, TriState.False) & " (Customer Price)"
                Else
                    strPrice = FormatCurrency(objEmail.WPPrice, 2, TriState.True, TriState.True, TriState.False) & " (WholeSale Price)"
                End If

            End If
            strContentB = strContentB.Replace("<Price>", strPrice)
            strContentB = strContentB.Replace("<Comment>", Trim(Comments))
            strContentB = strContentB.Replace("<Date>", objEmail.WODate)
        End If
        If Not IsNothing(strContentA) Then
            strContentA = strContentA.Replace("<Name>", "OrderWork Rep")
            strContentA = strContentA.Replace("<WorkOrderID>", objEmail.WorkOrderID)
            strContentA = strContentA.Replace("<WOTitle>", objEmail.WOTitle)
            strContentA = strContentA.Replace("<WOCategory>", objEmail.WOCategory)
            strContentA = strContentA.Replace("<Location>", objEmail.WOLoc)
            If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
                strContentA = strContentA.Replace("<StartDate>", objEmail.WOStartDate)
                strContentA = strContentA.Replace("Start Date:", "Appointment Date:")
                strContentA = strContentA.Replace("Proposed End Date:", "").Replace("End Date:", "")
                strContentA = strContentA.Replace("<EndDate>", "")
            Else
                strContentA = strContentA.Replace("<StartDate>", objEmail.WOStartDate & Chr(10))
                strContentA = strContentA.Replace("<EndDate>", objEmail.WOEndDate)
            End If
            strPrice = "WholeSale Price - " & FormatCurrency(objEmail.WPPrice, 2, TriState.True, TriState.True, TriState.False) & ", Portal Price - " & FormatCurrency(objEmail.PPPrice, 2, TriState.True, TriState.True, TriState.False)
            strContentA = strContentA.Replace("<Price>", strPrice)
            strContentA = strContentA.Replace("<Comment>", Trim(Comments))
            strContentA = strContentA.Replace("<Date>", objEmail.WODate)
        End If

        'Populate Proposed WO Terms
        If WOAction = ApplicationSettings.WOAction.WOSPCAccept Or WOAction = ApplicationSettings.WOAction.WODiscussCA _
            Or WOAction = ApplicationSettings.WOAction.WOSPChangeCA Or WOAction = ApplicationSettings.WOAction.WORaiseIssue _
            Or WOAction = ApplicationSettings.WOAction.WODiscuss Or WOAction = ApplicationSettings.WOAction.WOChangeIssue Then
            'Proposed Start Date and Proposed End Date by Biz
            If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
                strContentS = strContentS.Replace("<ProposedStartDate>", objEmail.NewWOStartDate)
                strContentA = strContentA.Replace("<ProposedStartDate>", objEmail.NewWOStartDate)
                strContentS = strContentS.Replace("<ProposedEndDate>", "")
                strContentA = strContentA.Replace("<ProposedEndDate>", "")
            Else
                strContentS = strContentS.Replace("<ProposedStartDate>", objEmail.NewWOStartDate & Chr(10))
                strContentA = strContentA.Replace("<ProposedStartDate>", objEmail.NewWOStartDate & Chr(10))
                strContentS = strContentS.Replace("<ProposedEndDate>", objEmail.NewWOEndDate)
                strContentA = strContentA.Replace("<ProposedEndDate>", objEmail.NewWOEndDate)
            End If

            'Proposed Price
            strContentS = strContentS.Replace("<ProposedPrice>", FormatCurrency(objEmail.NewWOPrice, 2, TriState.True, TriState.True, TriState.False))
            strContentA = strContentA.Replace("<ProposedPrice>", "Portal Price - " & FormatCurrency(objEmail.NewWOPrice, 2, TriState.True, TriState.True, TriState.False))
        End If
        Dim dserr As DataSet = Nothing
        If SendMailToA = True Then
            Try
                If (WOAction = ApplicationSettings.WOAction.WOSPAccept Or WOAction = ApplicationSettings.WOAction.WOAcceptCA) And WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                    SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), strContentA, strSubjectA, False, "", True)
                Else
                    SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), strContentA, strSubjectA)
                End If
            Catch ex As Exception
                dserr = dvEmail.Container
                SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error occurred in wocloseComplete (" & dserr.GetXml & "<br> Exception details" & ex.ToString, "Error in UCCloseComplete")
            End Try
        End If
        Dim strCC As String = ""
        If SendMailToB = True Then
            dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'CC'" & "   " & addRowFilter
            'Get the Email id list for the CC field
            strCC = GetCCRecipients(dvEmail)
            dvEmail.RowFilter = ""

            If strCC <> "" Then
                Try
                    If (WOAction = ApplicationSettings.WOAction.WOSPAccept Or WOAction = ApplicationSettings.WOAction.WOAcceptCA) And WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), BEmail, strContentB, strSubjectB, True, strCC, True)
                    Else
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), BEmail, strContentB, strSubjectB, True, strCC)
                    End If
                Catch ex As Exception
                    dserr = dvEmail.Container
                    SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error occurred in wocloseComplete (" & dserr.GetXml & "<br> Exception details" & ex.ToString, "Error in UCCloseComplete")
                End Try
            Else
                Try
                    If (WOAction = ApplicationSettings.WOAction.WOSPAccept Or WOAction = ApplicationSettings.WOAction.WOAcceptCA) And WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), BEmail, strContentB, strSubjectB, False, "", True)
                    Else
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), BEmail, strContentB, strSubjectB)
                    End If
                Catch ex As Exception
                    dserr = dvEmail.Container
                    SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error occurred in wocloseComplete (" & dserr.GetXml & "<br> Exception details" & ex.ToString, "Error in UCCloseComplete")
                End Try
            End If
        End If
        If SendMailToS = True Then
            strCC = ""
            dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'CC'" & "   " & addRowFilter
            'Get the Email id list for the CC field
            strCC = GetCCRecipients(dvEmail)
            dvEmail.RowFilter = ""
            If strCC <> "" Then
                Try
                    If (WOAction = ApplicationSettings.WOAction.WOSPAccept Or WOAction = ApplicationSettings.WOAction.WOAcceptCA) And WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), SEmail, strContentS, strSubjectS, True, strCC, True)
                    Else
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), SEmail, strContentS, strSubjectS, True, strCC)
                    End If
                Catch ex As Exception
                    SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error occurred in wocloseComplete (" & dserr.GetXml & "<br> Exception details" & ex.ToString, "Error in UCCloseComplete")
                End Try

            Else
                Try
                    If (WOAction = ApplicationSettings.WOAction.WOSPAccept Or WOAction = ApplicationSettings.WOAction.WOAcceptCA) And WorkOrderStatus = ApplicationSettings.WOStatusID.Accepted Then
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), SEmail, strContentS, strSubjectS, False, "", True)
                    Else
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), SEmail, strContentS, strSubjectS)
                    End If
                Catch ex As Exception
                    SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error occurred in wocloseComplete (" & dserr.GetXml & "<br> Exception details" & ex.ToString, "Error in UCCloseComplete")
                End Try

            End If
        End If
    End Sub

    ''' <summary>
    ''' Send mail to the suppliers to whom the workorder is ordermatched
    ''' </summary>
    ''' <param name="objEmail"></param>
    ''' <param name="dsEmail"></param>
    ''' <remarks></remarks>
    Public Shared Sub SendOrderMatchMail(ByVal objEmail As Emails, ByVal dsEmail As DataSet, ByVal BizDivId As Integer, ByVal PricingMethod As String, Optional ByVal BuyerCompanyId As Integer = 0)
        CommonFunctions.createLog("SendOrderMatchMail")
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            If Not IsNothing(objEmail.WorkOrderID) Then
                strsubject = "NEW JOB - " & objEmail.WorkOrderID.ToString
            Else
                strsubject = "NEW JOB - " & dsEmail.Tables("tblSuppliers").Rows(0)("RefWOID").ToString
            End If
            If CDec(objEmail.PPPrice) > 0 Then
                strsubject = strsubject & " (Postcode: " & dsEmail.Tables(1).Rows(0)("Location").ToString & ") Proposed Price is �" & objEmail.PPPrice
                'If (dsEmail.Tables(1).Rows(0)("PPPerRate").ToString <> objEmail.PPPrice) Then
                If (CInt(dsEmail.Tables(1).Rows(0)("PPPerRate")) <> 0) Then
                    strsubject = strsubject & " (�" & dsEmail.Tables(1).Rows(0)("PPPerRate").ToString & " " & dsEmail.Tables(1).Rows(0)("PerRate").ToString & ")"
                Else
                    strsubject = strsubject & " " & dsEmail.Tables(1).Rows(0)("PerRate").ToString
                End If
                'Else
                '    strsubject = strsubject & " Per Job"
                'End If
            End If
        Else
            'Put German text here
            strsubject = "A new Work Order has been routed to you"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        Dim BusinessArea As Integer
        Dim AptTime As String

        BusinessArea = dsEmail.Tables("tblSuppliers").Rows(0)("BusinessArea")
        AptTime = dsEmail.Tables("tblSuppliers").Rows(0)("AptTime")
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "OrderMatch.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()


        Dim MsgBodyUnApproved As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "OrderMatchUnApproved.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBodyUnApproved = Buffer
        sr.Close()


        If Not IsNothing(objEmail.WorkOrderID) Then
            MsgBody = MsgBody.Replace("<WorkOrderID>", objEmail.WorkOrderID)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<WorkOrderID>", objEmail.WorkOrderID)
        Else
            MsgBody = MsgBody.Replace("<WorkOrderID>", dsEmail.Tables("tblSuppliers").Rows(0)("RefWOID").ToString)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<WorkOrderID>", dsEmail.Tables("tblSuppliers").Rows(0)("RefWOID").ToString)
        End If

        Dim WOCAWOlink As String = ""
        Dim WODiscardWOLink As String = ""

        If (dsEmail.Tables(1).Rows.Count > 0) Then

            Dim WODetailslink As String = ""

            Dim appendLink As New StringBuilder
            Dim encWOID As String = Encryption.EncryptURL_DES(dsEmail.Tables(1).Rows(0)("WOID").ToString)
            Dim encWorkOrderID As String = Encryption.EncryptURL_DES(dsEmail.Tables(1).Rows(0)("WorkOrderID").ToString)
            Dim encSupContactId As String = Encryption.EncryptURL_DES(dsEmail.Tables(1).Rows(0)("SupplierContactID").ToString)
            'Dim encSupCompId As String = Encryption.EncryptURL_DES(dsEmail.Tables(0).Rows(0)("MainContactId").ToString)
            appendLink.Append("WOID=" & encWOID)
            appendLink.Append("&WorkOrderID=" & encWorkOrderID)
            appendLink.Append("&SupContactId=" & encSupContactId)
            appendLink.Append("&SupCompId=")
            appendLink.Append("&FromDate=&ToDate=&SearchWorkOrderID=&PS=10&PN=0&SC=DateStart&SO=1&Viewer=Supplier&BizDivID=1&Rating=")
            appendLink.Append("&FromMailer=1")

            WODetailslink &= ApplicationSettings.OrderWorkMyURL & "SecurePages/SupplierWODetails.aspx?" & appendLink.ToString & "&sender=UCWOsListing"
            MsgBody = MsgBody.Replace("<ViewDetailsLink>", WODetailslink)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<ViewDetailsLink>", WODetailslink)

            WODiscardWOLink = WODetailslink.Replace("&sender=UCWOsListing", "&sender=UCWODetails") & "&MailerDiscard=1"

            MsgBody = MsgBody.Replace("<DiscardWO>", WODiscardWOLink)


            appendLink.Append("&ContactID=" & Encryption.EncryptURL_DES(0))
            appendLink.Append("&CompanyID=" & Encryption.EncryptURL_DES(0))

            WOCAWOlink &= ApplicationSettings.OrderWorkMyURL & "SecurePages/SupplierWODetails.aspx?" & appendLink.ToString & "&sender=UCWODetails" & "&MailerCAccept=1"
            MsgBody = MsgBody.Replace("<CAWO>", WOCAWOlink)

        End If


        If (objEmail.Accreditation <> "") Then
            Dim Accreditation As String = ""

            Dim strAccreditation() As String = Split(objEmail.Accreditation, ",")

            Dim i As Integer

            Dim AccreditationId As String
            Dim AccreditationName As String
            For i = 0 To strAccreditation.GetLength(0) - 1
                Dim strCombineAccreditation() As String = Split(CStr(strAccreditation.GetValue(i)), "#")

                Accreditation &= "<tr><td style='width:1%;border-bottom:1px solid #f4f5f0;padding-top:3px;padding-bottom:3px;'>&nbsp;</td><td style='width:16.8%;border-bottom:1px solid #f4f5f0;padding-top:3px;padding-bottom:3px;font-family: verdana;font-size: 12px;'>Accreditation</td><td style='width:46.84%;border-bottom:1px solid #f4f5f0;padding-top:3px;padding-bottom:3px;font-family: verdana;font-size: 12px;'>" & CStr(strCombineAccreditation.GetValue(1)) & "</td><td style='border-bottom:1px solid #f4f5f0;padding-top:3px;padding-bottom:3px;'><a class='ancStyle' style='color:#086faa;text-decoration:none;cursor:pointer;font-family: verdana;font-size: 12px;' href='<AccreditationLink>AccreditationId=" & Encryption.EncryptURL_DES(CStr(strCombineAccreditation.GetValue(0))).ToString & "'  target='_blank'>- Delete/Disable this Accreditation</a></td></tr>"
            Next
            MsgBody = MsgBody.Replace("<Accreditation>", Accreditation)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Accreditation>", Accreditation)
        Else
            MsgBody = MsgBody.Replace("<Accreditation>", "")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Accreditation>", "")
        End If




        MsgBody = MsgBody.Replace("<WOTitle>", objEmail.WOTitle)
        MsgBody = MsgBody.Replace("<WOCategory>", objEmail.WOCategory)
        MsgBody = MsgBody.Replace("<Location>", objEmail.WOLoc)

        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<WOTitle>", objEmail.WOTitle)
        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<WOCategory>", objEmail.WOCategory)
        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Location>", objEmail.WOLoc)

        If (BuyerCompanyId = 17657) Then
            MsgBody = MsgBody.Replace("Start Date:", "Preferred Date:")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("Start Date:", "Preferred Date:")
        End If
        If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
            MsgBody = MsgBody.Replace("Start Date:", "Appointment Date:")
            MsgBody = MsgBody.Replace("<StartDate>", IIf(IsDBNull(objEmail.WOStartDate), "", objEmail.WOStartDate))
            MsgBody = MsgBody.Replace("Proposed End Date:", "Appointment Time:").Replace("End Date:", "Appointment Time:")
            MsgBody = MsgBody.Replace("<EndDate>", IIf(AptTime = "All Day", "", AptTime))

            MsgBodyUnApproved = MsgBodyUnApproved.Replace("Start Date:", "Appointment Date:")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<StartDate>", IIf(IsDBNull(objEmail.WOStartDate), "", objEmail.WOStartDate))
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("Proposed End Date:", "Appointment Time:").Replace("End Date:", "Appointment Time:")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<EndDate>", IIf(AptTime = "All Day", "", AptTime))
        Else
            MsgBody = MsgBody.Replace("<StartDate>", IIf(IsDBNull(objEmail.WOStartDate), "", objEmail.WOStartDate) & Chr(10))
            MsgBody = MsgBody.Replace("<EndDate>", IIf(IsDBNull(objEmail.WOEndDate), "", objEmail.WOEndDate))

            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<StartDate>", IIf(IsDBNull(objEmail.WOStartDate), "", objEmail.WOStartDate) & Chr(10))
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<EndDate>", IIf(IsDBNull(objEmail.WOEndDate), "", objEmail.WOEndDate))
        End If

        MsgBody = MsgBody.Replace("<Date>", objEmail.WODate)
        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Date>", objEmail.WODate)
        'MsgBody = MsgBody.Replace("<Date>", FormatDateTime(objEmail.WODate, DateFormat.ShortDate))
        MsgBody = MsgBody.Replace("<Description>", objEmail.WODesc)
        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Description>", objEmail.WODesc)
        If CDec(objEmail.PPPrice) > 0 Then
            Dim ProposedPrice As String = ""
            ProposedPrice = FormatCurrency(objEmail.PPPrice, 2, TriState.True, TriState.True, TriState.False)

            'If (dsEmail.Tables(1).Rows(0)("PPPerRate").ToString <> objEmail.PPPrice) Then
            If (CInt(dsEmail.Tables(1).Rows(0)("PPPerRate")) <> 0) Then
                ProposedPrice = ProposedPrice & " (Rate of �" & dsEmail.Tables(1).Rows(0)("PPPerRate").ToString & " " & dsEmail.Tables(1).Rows(0)("PerRate").ToString & ")"
            Else
                ProposedPrice = ProposedPrice & " " & dsEmail.Tables(1).Rows(0)("PerRate").ToString
            End If
            'Else
            '    ProposedPrice = ProposedPrice & " Per Job"
            'End If

            MsgBody = MsgBody.Replace("<ProposedPrice>", ProposedPrice)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<ProposedPrice>", ProposedPrice)

        Else
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                MsgBody = MsgBody.Replace("<Price>", "Quotation Requested")
                MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Price>", "Quotation Requested")
            Else
                MsgBody = MsgBody.Replace("<Price>", "Quotation Requested")
                MsgBodyUnApproved = MsgBodyUnApproved.Replace("<Price>", "Quotation Requested")
            End If

        End If
        Dim dvAdminContacts, dvEmailCopied As DataView
        dvAdminContacts = dsEmail.Tables("tblSuppliers").DefaultView
        dvEmailCopied = dsEmail.Tables("tblSuppliers").Copy.DefaultView
        dvAdminContacts.RowFilter = "Email <> '' AND RoleGroupID = '" & ApplicationSettings.RoleSupplierAdminID & "'"
        Dim dvACrow, dvCCrow As DataRowView
        Dim ccEmails As String = ""
        Dim finalMailContent As String

        Dim SpecialistDetails As String = ""
        Dim LocationDetails As String = ""
        Dim CompanyProfileDetails As String = ""
        Dim EncContactID As String = ""

        CompanyProfileDetails &= ApplicationSettings.OrderWorkMyURL & "SecurePages/CompanyProfile.aspx?"

        If (objEmail.Accreditation <> "") Then
            MsgBody = MsgBody.Replace("<AccreditationLink>", CompanyProfileDetails)
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<AccreditationLink>", CompanyProfileDetails)
        End If

        If (objEmail.CRBChecked = True) Then
            MsgBody = MsgBody.Replace("id='CRBChecked' style='display:none;'", "")
            MsgBody = MsgBody.Replace("<CRBChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("CRBChecked").ToString)

            MsgBodyUnApproved = MsgBodyUnApproved.Replace("id='CRBChecked' style='display:none;'", "")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<CRBChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("CRBChecked").ToString)
        End If

        If (objEmail.UKSecurityChecked = True) Then
            MsgBody = MsgBody.Replace("id='UKSecurityChecked' style='display:none;'", "")
            MsgBody = MsgBody.Replace("<UKSecurityChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("UKSecurityChecked").ToString)

            MsgBodyUnApproved = MsgBodyUnApproved.Replace("id='UKSecurityChecked' style='display:none;'", "")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<UKSecurityChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("UKSecurityChecked").ToString)
        End If

        If (objEmail.CSCSChecked = True) Then
            MsgBody = MsgBody.Replace("id='CSCSChecked' style='display:none;'", "")
            MsgBody = MsgBody.Replace("<CSCSChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("CSCSChecked").ToString)

            MsgBodyUnApproved = MsgBodyUnApproved.Replace("id='CSCSChecked' style='display:none;'", "")
            MsgBodyUnApproved = MsgBodyUnApproved.Replace("<CSCSChecked>", CompanyProfileDetails & "Vetting=" & Encryption.EncryptURL_DES("CSCSChecked").ToString)
        End If

        MsgBody = MsgBody.Replace("<WOCategoryLink>", CompanyProfileDetails & "CategoryId=" & Encryption.EncryptURL_DES(dsEmail.Tables(1).Rows(0)("WOCategoryID").ToString))
        MsgBodyUnApproved = MsgBodyUnApproved.Replace("<WOCategoryLink>", CompanyProfileDetails & "CategoryId=" & Encryption.EncryptURL_DES(dsEmail.Tables(1).Rows(0)("WOCategoryID").ToString))


        For Each dvACrow In dvAdminContacts

            If (dvACrow("CompanyStatus") <> 51) Then
                finalMailContent = MsgBodyUnApproved
            Else
                finalMailContent = MsgBody
            End If

            dvEmailCopied.RowFilter = "MainContactID = " & dvACrow("MainContactID") & " AND RoleGroupID <> '" & ApplicationSettings.RoleSupplierAdminID & "'"
            For Each dvCCrow In dvEmailCopied
                'ccEmails
                If ccEmails <> "" Then
                    '-- space is needed for proper split in the sendemail class
                    ccEmails &= "; "
                End If
                ccEmails &= dvCCrow("Email")
            Next

            SpecialistDetails = ""
            LocationDetails = ""

            'If (dvACrow("CompanyStatus") <> 51) Then
            '    finalMailContent = finalMailContent.Replace(WOCAWOlink, "")
            '    finalMailContent = finalMailContent.Replace(WODiscardWOLink, "")
            '    finalMailContent = finalMailContent.Replace("id='tblInnerBottom'", "id='tblInnerBottom' style='display:none;'")
            '    finalMailContent = finalMailContent.Replace("id='tblInnerBottomNonApproveMsg' style='display:none;'", "id='tblInnerBottomNonApproveMsg'")
            'End If

            finalMailContent = finalMailContent.Replace("<Name>", dvACrow("Name"))
            finalMailContent = finalMailContent.Replace("&SupCompId=", "&SupCompId=" & Encryption.EncryptURL_DES(dvACrow("MainContactId")).ToString)

            SpecialistDetails &= ApplicationSettings.OrderWorkMyURL & "SecurePages/SpecialistsForm.aspx?ContactID=" & Encryption.EncryptURL_DES(dvACrow("ContactID")).ToString


            If (objEmail.IsNearestToUsed = True And dvACrow("AddressID") <> 0) Then
                LocationDetails &= ApplicationSettings.OrderWorkMyURL & "SecurePages/LocationForm.aspx?AddressID=" & Encryption.EncryptURL_DES(dvACrow("AddressID")).ToString
                finalMailContent = finalMailContent.Replace("<Distance>", dvACrow("Distance"))
                finalMailContent = finalMailContent.Replace("id='Location' style='display:none;'", "")
                finalMailContent = finalMailContent.Replace("<LocationLink>", LocationDetails & "&FromMailer=" & Encryption.EncryptURL_DES("1").ToString)
            End If

            If (objEmail.EngCRBChecked = True) Then
                finalMailContent = finalMailContent.Replace("id='EngCRBChecked' style='display:none;'", "")
                'finalMailContent = finalMailContent.Replace("<EngCRBChecked>", SpecialistDetails & "&Vetting=" & Encryption.EncryptURL_DES("EngCRBChecked").ToString)
            End If

            If (objEmail.EngUKSecurity = True) Then
                finalMailContent = finalMailContent.Replace("id='EngUKSecurity' style='display:none;'", "")
                'finalMailContent = finalMailContent.Replace("<EngUKSecurity>", SpecialistDetails & "&Vetting=" & Encryption.EncryptURL_DES("EngUKSecurity").ToString)
            End If

            If (objEmail.EngCSCSChecked = True) Then
                finalMailContent = finalMailContent.Replace("id='EngCSCSChecked' style='display:none;'", "")
                finalMailContent = finalMailContent.Replace("<EngCSCSChecked>", SpecialistDetails & "&Vetting=" & Encryption.EncryptURL_DES("EngCSCSChecked").ToString)
            End If

            If (objEmail.EngRightToWorkChecked = True) Then
                finalMailContent = finalMailContent.Replace("id='EngRightToWorkChecked' style='display:none;'", "")
                'finalMailContent = finalMailContent.Replace("<EngRightToWorkChecked>", SpecialistDetails & "&Vetting=" & Encryption.EncryptURL_DES("EngRightToWorkChecked").ToString)
            End If

            SendMail.SendMail(ApplicationSettings.EmailInfo(), dvACrow("Email"), finalMailContent, strsubject, True, ccEmails, True)

            If ccEmails <> "" Then
                ccEmails = ""
            End If
        Next
    End Sub

    ''' <summary>
    ''' get the cc recipients list
    ''' </summary>
    ''' <param name="dvnotif"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCCRecipients(ByVal dvnotif As DataView) As String
        Dim drowNotif As DataRowView
        'this string has all the cc of the email to be sent
        Dim recipients As String = ""
        For Each drowNotif In dvnotif
            'recipients &= drowNotif.Item("Name")
            'recipients &= " <"
            recipients &= drowNotif.Item("Email")
            'recipients &= ">"
            If recipients <> "" Then
                recipients &= "; "
            End If
        Next

        Return recipients
    End Function

    ''' <summary>
    ''' Fucntion to send mail to the Supplier who have lost the workorder
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="drEmail"></param>
    ''' <param name="dvCC"></param>
    ''' <param name="ActionBy"></param>
    ''' <param name="BizDivId"></param>
    ''' <remarks></remarks>
    Public Shared Sub SendLostWOMail(ByVal WOID As String, ByVal drEmail As DataRowView, ByVal dvCC As DataView)
        Dim strSubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strSubject = "Work Order " & WOID & " has been accepted by another supplier"
        Else
            'Put German text here
            strSubject = "Work Order " & WOID & " has been accepted by another supplier"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOLost.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<WorkOrderID>", WOID)
        MsgBody = MsgBody.Replace("<Name>", drEmail.Item("Name"))
        Dim strCC As String
        strCC = GetCCRecipients(dvCC)
        Dim IsMailOnLostWOToSP As Boolean = ControlMail.IsMailOnLostWOToSP
        If IsMailOnLostWOToSP Then
            CommonFunctions.createLog("OrderworkLibrary - SendLostWOMail : " + WOID + " " + drEmail.Item("Email").ToString())
            If strCC <> "" Then
                SendMail.SendMail(ApplicationSettings.EmailInfo(), drEmail.Item("Email"), MsgBody, strSubject, True, strCC)
            Else
                SendMail.SendMail(ApplicationSettings.EmailInfo(), drEmail.Item("Email"), MsgBody, strSubject)
            End If
        End If
    End Sub

    Public Shared Sub SendUserStatusMail(ByVal AccountStatus As String, ByVal user As Hashtable, ByVal subject As String, ByVal topmessage As String, ByVal BizDivId As Integer)

        Dim strSubject As String = ""
        Dim msgBody As String = ""
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        Dim strEmail As String = Trim(user.Item("UserEmail"))
        Dim strName As String = Trim(user.Item("UserFName")) & " " & Trim(user.Item("UserLName"))
        If AccountStatus = "Activate" Or AccountStatus = "EnableLogin" Then
            CommonFunctions.createLog("Activate")
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubject = "OrderWork user account registration details"
            Else
                'Put German text here
                strSubject = "OrderWork user account registration details"
            End If
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "UserActivation.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Dim activationlink As String
            activationlink = ApplicationSettings.OrderWorkMyURL & "ActivateAccount.aspx?Email=" & System.Web.HttpContext.Current.Server.UrlEncode(strEmail) & "&FName=" & System.Web.HttpContext.Current.Server.UrlEncode(user.Item("UserFName")) & "&LName=" & System.Web.HttpContext.Current.Server.UrlEncode(user.Item("UserLName")) & "&CreatorFName=" &
                                   System.Web.HttpContext.Current.Server.UrlEncode(user.Item("AdminFName")) & "&CreatorLName=" & System.Web.HttpContext.Current.Server.UrlEncode(user.Item("AdminLName")) & "&User=AddedUser"

            Buffer = sr.ReadToEnd
            msgBody = Buffer
            sr.Close()
            msgBody = msgBody.Replace("<Email>", strEmail)
            msgBody = msgBody.Replace("<Password>", user.Item("UserPassword"))
            msgBody = msgBody.Replace("<AdminName>", user.Item("AdminFName") & " " & user.Item("AdminLName"))
            msgBody = msgBody.Replace("<ActivationLink>", activationlink)
        End If
        If AccountStatus = "ReActivate" Then
            CommonFunctions.createLog("ReActivate")
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubject = "Welcome to OrderWork - User Account activation"
            Else
                'Put German text here
                strSubject = "Welcome to OrderWork - User Account Re-activation"
            End If
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "UserReActivation.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Dim activationlink As String
            activationlink = ApplicationSettings.OrderWorkMyURL & "OWForgotPassword.aspx"
            Buffer = sr.ReadToEnd
            msgBody = Buffer
            sr.Close()
            msgBody = msgBody.Replace("<Email>", strEmail)
            msgBody = msgBody.Replace("<Password>", user.Item("UserPassword"))
            msgBody = msgBody.Replace("<AdminName>", user.Item("AdminFName") & " " & user.Item("AdminLName"))
            msgBody = msgBody.Replace("<UserSecurityQues>", user.Item("UserSecurityQues"))
            msgBody = msgBody.Replace("<UserSecurityAns>", user.Item("UserSecurityAns"))
            msgBody = msgBody.Replace("<ActivationLink>", activationlink)
        End If
        If AccountStatus = "InActivate" Then
            CommonFunctions.createLog("InActivate")
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubject = "Your OrderWork user account has been deactivated"
            Else
                'Put German text here
                strSubject = "Your OrderWork user account has been deactivated"
            End If
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "UserDeActivation.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            msgBody = Buffer
            sr.Close()

            msgBody = msgBody.Replace("<CompanyName>", user.Item("CompanyName"))
        End If

        If AccountStatus = "ReActivateWOLogin" Then
            CommonFunctions.createLog("ReActivateWOLogin")
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                strSubject = "Welcome to OrderWork - User Account Re-activation"
            Else
                'Put German text here
                strSubject = "Welcome to OrderWork - User Account Re-activation"
            End If
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "UserReActivateWOLogin.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            msgBody = Buffer
            sr.Close()

            msgBody = msgBody.Replace("<AdminName>", user.Item("AdminFName") & " " & user.Item("AdminLName"))
        End If

        msgBody = msgBody.Replace("<Name>", strName)

        SendMail.SendMail(ApplicationSettings.EmailInfo(), strEmail, msgBody, strSubject)
    End Sub

    Public Shared Sub SendCompanyStatusMail(ByVal eMail As String, ByVal name As String, ByVal message As String, ByVal status As String)
        CommonFunctions.createLog("SendCompanyStatusMail")
        Select Case status
            Case "Approve"
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    status = "Approved"
                Else
                    'Put German text here
                    status = "Approved"
                End If
            Case "Suspend"
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    status = "Suspended"
                Else
                    'Put German text here
                    status = "Suspended"
                End If
            Case "Unapprove"
                If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                    status = "UnApproved"
                Else
                    'Put German text here
                    status = "UnApproved"
                End If
        End Select
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "OrderWork - Account Status Change"
        Else
            'Put German text here
            strsubject = "Your OrderWork account status has changed"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "AccountStatus.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Name>", name)
        MsgBody = MsgBody.Replace("<Message>", message.Trim)
        MsgBody = MsgBody.Replace("<AccountStatus>", status)
        SendMail.SendMail(ApplicationSettings.EmailInfo(), eMail, MsgBody, strsubject)
    End Sub

    'poonam modified on 14/8/2015 - Task - OM-13:OW - ENH - Add Create Supplier account Page in OW 
    Public Shared Sub SendCompanyApproveMail(ByVal UserName As String, ByVal Name As String, ByVal Password As String)
        CommonFunctions.createLog("SendCompanyApproveMail")
        Dim strsubject As String
        strsubject = "OrderWork - Account Status"
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "AccountRegistration.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Name>", Name)
        MsgBody = MsgBody.Replace("<UserName>", UserName)
        MsgBody = MsgBody.Replace("<Password>", Password)
        SendMail.SendMail(ApplicationSettings.EmailInfo(), UserName, MsgBody, strsubject)
    End Sub

    ''' <summary>
    ''' Function to send mail to the client/supplier on cancellation of the workorder 
    ''' Modified by Pankaj Malav on 23 Feb 2008 as part of case 388
    ''' </summary>
    ''' <param name="WOAction"></param>
    ''' <param name="objEmail"></param>
    ''' <param name="BuyerCharge"></param>
    ''' <param name="SupplierCharge"></param>
    ''' <param name="dvEmail"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="Comments"></param>
    ''' <param name="SupplierID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Sub SendWOCancelMail(ByVal objEmail As Emails, ByVal dvEmail As DataView, ByVal SendMailToSupplier As Boolean, Optional ByVal Comments As String = "", Optional ByVal dsEmailSupp As DataSet = Nothing)
        CommonFunctions.createLog("SendWOCancelMail")
        Dim strsubject As String
        Dim BusinessArea As Integer
        BusinessArea = dvEmail.Item(0).Item("BusinessArea")
        Dim strBcc As String
        strBcc = ApplicationSettings.CancelWoBcc

        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "CANCELLED JOB - " & objEmail.WorkOrderID
        Else
            'Put German text here
            strsubject = "Work Order " & objEmail.WorkOrderID & " has been marked as Cancelled"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOCancel.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<WorkOrderID>", objEmail.WorkOrderID)
        MsgBody = MsgBody.Replace("<WOTitle>", objEmail.WOTitle)
        MsgBody = MsgBody.Replace("<WOCategory>", objEmail.WOCategory)
        MsgBody = MsgBody.Replace("<Location>", objEmail.WOLoc)
        If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
            MsgBody = MsgBody.Replace("Start Date:", "Appointment Date:")
            MsgBody = MsgBody.Replace("<StartDate>", objEmail.WOStartDate)
            MsgBody = MsgBody.Replace("Proposed End Date:", "").Replace("End Date:", "")
            MsgBody = MsgBody.Replace("<EndDate>", "")
        Else
            MsgBody = MsgBody.Replace("<StartDate>", objEmail.WOStartDate & Chr(10))
            MsgBody = MsgBody.Replace("<EndDate>", objEmail.WOEndDate)
        End If
        MsgBody = MsgBody.Replace("<Comment>", Comments)
        If objEmail.WODate = "" Then
            MsgBody = MsgBody.Replace("<Date>", "")
        Else
            MsgBody = MsgBody.Replace("<Date>", FormatDateTime(objEmail.WODate, DateFormat.ShortDate))
        End If

        Dim EmailContent As String = ""
        'Send Mail to Buyer
        EmailContent = MsgBody
        EmailContent = EmailContent.Replace("<Price>", FormatCurrency(objEmail.WPPrice, 2, TriState.True, TriState.True, TriState.False))
        Dim strName, Email, strCC As String
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'To'"
        strName = dvEmail.Item(0).Item("Name")
        EmailContent = EmailContent.Replace("<Name>", strName)

        Email = dvEmail.Item(0).Item("Email")
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'CC'"
        strCC = GetCCRecipients(dvEmail)
        If (ControlMail.IsMailOnCancelToBuyer) Then


            If Email <> "" Then
                If strCC <> "" Then
                    SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, EmailContent, strsubject, True, strCC, False, True, strBcc)
                Else
                    SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, EmailContent, strsubject, False, "", False, True, strBcc)
                End If
            End If
        End If
        'Send mail 

        If (ControlMail.IsMailOnCancelToSP And SendMailToSupplier) Then
            Dim drow_Mailer As DataRowView
            Dim dv_Mailer As DataView = dsEmailSupp.Tables("tblEmailRecipientsSupp").Copy.DefaultView
            dv_Mailer.RowFilter = "Recipient = 'To'"

            Dim dv_ToCC As DataView = dsEmailSupp.Tables("tblEmailRecipientsSupp").Copy.DefaultView

            For Each drow_Mailer In dv_Mailer
                dv_ToCC.RowFilter = "Recipient = 'CC' AND CompanyID = " & drow_Mailer.Item("CompanyID")
                'Contact ID is the id of the contact who either created the workorder or performed the action.
                strName = drow_Mailer.Item("Name")
                Email = drow_Mailer.Item("Email")

                EmailContent = MsgBody
                EmailContent = EmailContent.Replace("<Price>", FormatCurrency(objEmail.PPPrice, 2, TriState.True, TriState.True, TriState.False))
                EmailContent = EmailContent.Replace("<Name>", strName)

                strCC = GetCCRecipients(dv_ToCC)
                If Email <> "" Then
                    If strCC <> "" Then
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, EmailContent, strsubject, True, strCC, False, True, strBcc)
                    Else
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, EmailContent, strsubject, False, "", False, True, strBcc)
                    End If
                End If
            Next
        End If
        'if cancellation charges then make wo complete rather than cancel & send email to supplier
        If (objEmail.ppcancel > 0 Or objEmail.wpcancel > 0) Then
            If (ControlMail.IsMailOnCancelToSP And SendMailToSupplier) Then
                Dim drow_Mailer_cancellation As DataRowView
                Dim dv_Mailer_cancellation As DataView = dsEmailSupp.Tables("tblEmailRecipientsSuppCancellation").Copy.DefaultView
                dv_Mailer_cancellation.RowFilter = "Recipient = 'To'"

                Dim dv_ToCC As DataView = dsEmailSupp.Tables("tblEmailRecipientsSuppCancellation").Copy.DefaultView

                For Each drow_Mailer_cancellation In dv_Mailer_cancellation
                    dv_ToCC.RowFilter = "Recipient = 'CC' AND CompanyID = " & drow_Mailer_cancellation.Item("CompanyID")
                    'Contact ID is the id of the contact who either created the workorder or performed the action.
                    strName = drow_Mailer_cancellation.Item("Name")
                    Email = drow_Mailer_cancellation.Item("Email")

                    EmailContent = MsgBody
                    EmailContent = EmailContent.Replace("<Price>", FormatCurrency(objEmail.PPPrice, 2, TriState.True, TriState.True, TriState.False))
                    EmailContent = EmailContent.Replace("<Name>", strName)

                    strCC = GetCCRecipients(dv_ToCC)
                    If Email <> "" Then
                        If strCC <> "" Then
                            SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, EmailContent, strsubject, True, strCC, False, True, strBcc)
                        Else
                            SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, EmailContent, strsubject, False, "", False, True, strBcc)
                        End If
                    End If
                Next
            End If
        End If

    End Sub

    ''' <summary>
    ''' Function to send mail when the OW admin sets the wholesale price
    ''' </summary>
    ''' <param name="woid"></param>
    ''' <param name="oldPrice"></param>
    ''' <param name="newPrice"></param>
    ''' <param name="comments"></param>
    ''' <param name="name"></param>
    ''' <param name="email"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Sub sendWholesalePriceMail(ByVal woid As String, ByVal oldPrice As Decimal, ByVal newPrice As Decimal, ByVal comments As String, ByVal strName As String, ByVal email As String, ByVal BizDivId As Integer)
        CommonFunctions.createLog("sendWholesalePriceMail")
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Work Order Price Updated"
        Else
            'Put German text here
            strsubject = "Work Order Price Updated"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOSetWP.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Name>", strName)
        MsgBody = MsgBody.Replace("<WorkOrderID>", woid)
        MsgBody = MsgBody.Replace("<CurrentPrice>", FormatCurrency(oldPrice, 2, TriState.True, TriState.True, TriState.False))
        MsgBody = MsgBody.Replace("<RevisedPrice>", FormatCurrency(newPrice, 2, TriState.True, TriState.True, TriState.False))
        SendMail.SendMail(ApplicationSettings.EmailInfo(), email, MsgBody, strsubject)
    End Sub

    Public Shared Sub SendAddWithdrawFundsMail(ByVal dataRow As DataRowView, ByVal dvPaymentDetails As DataView, ByVal dateCompleted As String, ByVal withDrawType As String, ByVal bizDivId As Integer, Optional ByVal sendToAdmin As Boolean = False)
        CommonFunctions.createLog("SendAddWithdrawFundsMail")
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "OrderWork - Payment Processed"
        Else
            'Put German text here
            strsubject = "Withdrawal of funds successful"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WithdrawalProcessed.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<InvoiceCount>", dvPaymentDetails.Count)

        If dateCompleted = "" Then
            MsgBody = MsgBody.Replace("<Date>", Strings.FormatDateTime(Date.Now, DateFormat.ShortDate))
        Else
            MsgBody = MsgBody.Replace("<Date>", dateCompleted)
        End If
        'Invoice Details & Total Amount
        Dim strInvoiceDetails As String = ""
        Dim TotalAmt As Decimal = 0
        For Each dsViewRow As DataRowView In dvPaymentDetails
            strInvoiceDetails &= dsViewRow("InvoiceNo") & " - " & dsViewRow("WorkOrderNo") & " - " & FormatCurrency(dsViewRow("InvoiceAmt"), 2, TriState.True, TriState.True, TriState.False) & Chr(13)
            TotalAmt += dsViewRow("InvoiceAmt")
        Next
        MsgBody = MsgBody.Replace("<InvoiceDetails>", strInvoiceDetails)
        MsgBody = MsgBody.Replace("<TotalAmount>", FormatCurrency(TotalAmt, 2, TriState.True, TriState.True, TriState.False))
        Dim strName As String = dataRow.Item("FName") & " " & dataRow.Item("LName")
        MsgBody = MsgBody.Replace("<Name>", strName)

        'Send Mail to Supplier
        If (ControlMail.IsMailOnFundsProcessToSP) Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), dataRow.Item("UserName"), MsgBody, strsubject)
        End If
        'Send Mail to Admin
        'SendMail.SendMail(ApplicationSettings.EmailInfo() , "Admin" + " <" + ApplicationSettings.EmailInfo() + ">", MsgBody, strsubject)

    End Sub

    ''' <summary>
    ''' Function to send mail on Buyer Accepted
    ''' </summary>
    ''' <param name="objEmail"></param>
    ''' <param name="dvEmail"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Sub BuyerAccepted(ByVal objEmail As Emails, ByVal dvEmail As DataView, ByVal BizDivId As Integer, ByVal WorkOrderStatus As Integer)
        CommonFunctions.createLog("BuyerAccepted")
        Dim strName As String = ""
        Dim Email As String = ""
        Dim strCC As String = ""
        Dim BusinessArea As Integer
        BusinessArea = dvEmail.Item(0).Item("BusinessArea")

        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'To'"
        'Contact ID is the id of the contact who either created the workorder or performed the action.
        strName = dvEmail.Item(0).Item("Name")
        Email = dvEmail.Item(0).Item("Email")
        'Else Send mail to Admin
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'CC'"
        'Get the Email id list for the CC field
        strCC = GetCCRecipients(dvEmail)
        dvEmail.RowFilter = ""
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Acceptance of Work Order " & objEmail.WorkOrderID
        Else
            'Put German text here
            strsubject = "Acceptance of Work Order " & objEmail.WorkOrderID
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOBuyerAccept.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Name>", strName)
        MsgBody = MsgBody.Replace("<WorkOrderID>", objEmail.WorkOrderID)
        MsgBody = MsgBody.Replace("<WOTitle>", objEmail.WOTitle)
        MsgBody = MsgBody.Replace("<Location>", objEmail.WOLoc)
        If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
            MsgBody = MsgBody.Replace("Start Date:", "Appointment Date:")
            MsgBody = MsgBody.Replace("<StartDate>", objEmail.WOStartDate)
            MsgBody = MsgBody.Replace("Proposed End Date:", "").Replace("End Date:", "")
            MsgBody = MsgBody.Replace("<EndDate>", "")
        Else
            MsgBody = MsgBody.Replace("<StartDate>", objEmail.WOStartDate & Chr(10))
            MsgBody = MsgBody.Replace("<EndDate>", objEmail.WOEndDate)
        End If
        MsgBody = MsgBody.Replace("<Price>", FormatCurrency(objEmail.WOPrice, 2, TriState.True, TriState.True, TriState.False))
        Dim IsMailOnBuyerAcceptToBuyer As Boolean = ControlMail.IsMailOnBuyerAcceptToBuyer
        Dim IsMailOnBuyerAcceptToAdmin As Boolean = ControlMail.IsMailOnBuyerAcceptToAdmin
        'Admin Email
        If (IsMailOnBuyerAcceptToAdmin) Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), MsgBody, strsubject)
        End If
        'Buyer Email
        If (IsMailOnBuyerAcceptToBuyer) Then
            If strCC <> "" Then
                SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject, True, strCC)
            Else
                SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Function to send forgot mail
    ''' </summary>
    ''' <param name="email">Email of the contact</param>
    ''' <param name="firstName">First Name of the Contact</param>
    ''' <param name="password">New password</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SendMailForgotPassword(ByVal email As String, ByVal Name As String, ByVal password As String, ByVal BizDivId As Integer, Optional ByVal OWCommonEmail As String = "") As Boolean
        CommonFunctions.createLog("SendMailForgotPassword")
        Dim Success As Boolean = False
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "OrderWork - Password Assistance"
        Else
            'Put German text here
            strsubject = "OrderWork - Password Assistance"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "ForgotPassword.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Name>", Name)
        MsgBody = MsgBody.Replace("<Email>", email)
        MsgBody = MsgBody.Replace("<Password>", password)
        If (OWCommonEmail <> "") Then
            Dim MainEmail As String = ""
            Dim CCEmail As String = ""
            Dim CCs() As String = Split(OWCommonEmail, "; ")
            Dim i As Integer
            For i = 0 To CCs.GetLength(0) - 1
                If CCs.GetValue(i) <> "" Then
                    If i = 0 Then
                        MainEmail = CCs.GetValue(i)
                    Else
                        If (CCEmail = "") Then
                            CCEmail = CCs.GetValue(i)
                        Else
                            CCEmail = CCEmail & "; " & CCs.GetValue(i)
                        End If
                    End If

                End If
            Next
            If (CCEmail <> "") Then
                SendMail.SendMail(ApplicationSettings.EmailInfo(), MainEmail, MsgBody, strsubject, True, CCEmail)
            Else
                SendMail.SendMail(ApplicationSettings.EmailInfo(), MainEmail, MsgBody, strsubject)
            End If
        Else
            SendMail.SendMail(ApplicationSettings.EmailInfo(), email, MsgBody, strsubject)
        End If

        'Check whether mail to be send to admin
        Return Success
    End Function

    Public Shared Function CreateContactUsMail(EmailAddress As String, password As String) As MailMessage
        Try
            CommonFunctions.createLog("CreateContactUsMail")
            Dim ContactUsMail As New MailMessage()
            CommonFunctions.createLog("OWAdmin - Reset password CreateContactUsMail")
            ContactUsMail.To.Add(EmailAddress)
            'ContactUsMail.Bcc.Add("hdesai@orderwork.co.uk")
            'ContactUsMail.From = New MailAddress("smtp2.empowered@empowereduk.com", "My OrderWork Portal")
            ContactUsMail.From = New MailAddress("no-reply@orderwork.co.uk", "Orderwork")
            ContactUsMail.Subject = "OrderWork - Password Assistance"

            'Get content from txt file to get email content.
            Dim MsgBody As String
            Dim WebRequest As WebRequest
            Dim WebResponse As WebResponse
            Dim sr As StreamReader
            Dim Buffer As String
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "PasswordReset.txt")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            MsgBody = Buffer
            sr.Close()

            'Dim emailText As String = File.ReadAllText(ApplicationSettings.MailerFolder & "/" & "KnowHowB2BPasswordReset.txt")

            MsgBody = MsgBody.Replace("***Name***", "OrderWork User")
            MsgBody = MsgBody.Replace("***Username***", EmailAddress)
            MsgBody = MsgBody.Replace("***Password***", password)
            Dim HtmlTextMessage As AlternateView = AlternateView.CreateAlternateViewFromString(MsgBody, New System.Net.Mime.ContentType("text/html"))

            ContactUsMail.AlternateViews.Add(HtmlTextMessage)
            CommonFunctions.createLog("OWAdmin - Reset password Success")
            Return (ContactUsMail)
        Catch ex As Exception
            CommonFunctions.createLog("OWAdmin - Reset password Mail sending Issue1 : " & ex.ToString)
        End Try
    End Function

    ''' <summary>
    ''' Function to send mail to the Admin of the Buyer Company when payments are available for Withdrawal. 
    ''' </summary>
    ''' <param name="email"></param>
    ''' <param name="Name"></param>
    ''' <param name="PurchaseInvoiceNo"></param>
    ''' <returns>Success Bit</returns>
    ''' <remarks>Pratik Trivedi - 28 Aug, 2008</remarks>
    Public Shared Function SendMailPaymentAvailable(ByVal email As String, ByVal FName As String, ByVal LName As String, ByVal PurchaseInvoiceNo As String, Optional ByVal GroupInvoiceNo As String = "", Optional ByVal ccEmail As String = "", Optional ByVal FundAmount As Decimal = 0) As Boolean
        CommonFunctions.createLog("SendMailPaymentAvailable")
        Dim Success As Boolean = False
        Dim strsubject As String
        Dim IsCCEmail As Boolean = False

        Dim Name As String = FName & " " & LName

        If ccEmail <> "" Then
            IsCCEmail = True
        End If
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            If PurchaseInvoiceNo <> "" Then
                strsubject = "Funds Available - " & "�" & CStr(FundAmount)
            Else
                strsubject = "Funds Available - " & "�" & CStr(FundAmount)
            End If
        Else
            'Put German text here   If PurchaseInvoiceNo <> "" Then
            If PurchaseInvoiceNo <> "" Then
                strsubject = "Invoice number " & PurchaseInvoiceNo & " is now available for withdrawal"
            Else
                strsubject = "Invoice's are now available for withdrawal"
            End If
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        Dim emailContent As String
        If GroupInvoiceNo = "" Then
            emailContent = "PaymentAvailable.txt"
        Else
            emailContent = "PaymentAvailableInSet.txt"
        End If
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & emailContent)

        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<SupFName>", FName)
        MsgBody = MsgBody.Replace("<SupLName>", LName)
        MsgBody = MsgBody.Replace("<Purchase Invoice no.>", IIf(GroupInvoiceNo = "", PurchaseInvoiceNo, GroupInvoiceNo))
        If (ControlMail.IsMailOnFundsProcessToSP) Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), email, MsgBody, strsubject, IsCCEmail, ccEmail)
        End If

        'Check whether mail to be send to admin
        Return Success
    End Function

    Public Shared Sub SendMailPaymentAvailable(ByVal dvData As DataView)
        Dim Email, FName, Lname, PurInvNo As String
        For Each vRow As DataRowView In dvData
            Email = vRow("username")
            FName = vRow("FName")
            Lname = vRow("LName")
            PurInvNo = vRow("PurInvoiceNo")
            Emails.SendMailPaymentAvailable(Email, FName, Lname, PurInvNo)
        Next
    End Sub

    ''' <summary>
    ''' Function to sent mail of payment available to admin of the supplier company and CC to accounts
    ''' </summary>
    ''' <param name="dvData"></param>
    ''' <remarks></remarks>
    Public Shared Sub SendMailPaymentAvailableInInvoiceSet(ByVal dvData As DataView)
        Dim FundAmount As Decimal = 0
        Dim Email, FName, Lname As String
        Dim InvoiceNos As String = ""
        Dim ccEmail As String = ""
        Dim distinctEmails As New DataTable
        distinctEmails = CommonFunctions.SelectDistinct("tblDistinct", dvData.Table, "MainContactID")

        For Each vRow As DataRow In distinctEmails.Select("", "MainContactID")
            InvoiceNos = ""
            Email = ""
            FName = ""
            Lname = ""
            ccEmail = ""
            FundAmount = 0
            dvData.RowFilter = "MainContactID = " & vRow("MainContactID") & " and rolegroupid = " + ApplicationSettings.RoleSupplierAdminID
            If dvData.Count > 0 Then
                For Each dvRow As DataRowView In dvData
                    InvoiceNos = InvoiceNos & vbNewLine & dvRow("PurInvoiceNo")
                    FundAmount = FundAmount + dvRow("Total")
                Next
                Email = dvData.Item(0)("username")
                FName = dvData.Item(0)("FName")
                Lname = dvData.Item(0)("LName")
                ' Sent ccEmail 
                If Email <> "" Then
                    '''' Filter the data to get the company's Finance Roles only - so filter by mainconatctID , role and invoice no to get distinct
                    dvData.RowFilter = "MainContactID = " & vRow("MainContactID") & " and rolegroupid = " + ApplicationSettings.RoleSupplierFinanceID & " and PurInvoiceno = '" & dvData.Item(0)("PurInvoiceno") & "'"
                    For Each dvRow As DataRowView In dvData
                        If ccEmail = "" Then
                            ccEmail = dvRow("username")
                        Else
                            ccEmail = ccEmail & "; " & dvRow("username")
                        End If
                    Next
                End If
                ' Since group invoice is consdiered the fourth parameter is send as blank
                Emails.SendMailPaymentAvailable(Email, FName, Lname, "", InvoiceNos, ccEmail, FundAmount)
            End If
        Next
    End Sub

    Public Shared Function SendMailSIGeneration(ByVal dvEmail As DataView, ByVal BizDivId As Integer)
        CommonFunctions.createLog("SendMailSIGeneration")
        Dim strName As String = ""
        Dim Email As String = ""
        Dim strCC As String = ""
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'To'"
        'Contact ID is the id of the admin of the company for whom the SI is generated.
        strName = dvEmail.Item(0).Item("Name")
        Email = dvEmail.Item(0).Item("Email")
        'Else Send mail to Admin
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'CC'"
        'Get the Email id list for the CC field
        strCC = GetCCRecipients(dvEmail)
        dvEmail.RowFilter = ""
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Sales Invoice Generated " & dvEmail.Item(0)("AdviceNumber")
        Else
            'Put German text here
            strsubject = "Sales Invoice Generated " & dvEmail.Item(0)("AdviceNumber")
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "SIGeneration.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        'MsgBody = MsgBody.Replace("<Name>", strName)
        MsgBody = MsgBody.Replace("<SalesInvoiceNumber>", dvEmail.Item(0)("AdviceNumber"))
        MsgBody = MsgBody.Replace("<SalesInvoiceNetAmount>", dvEmail.Item(0)("NetAmount"))
        MsgBody = MsgBody.Replace("<SalesInvoiceVATAmount>", dvEmail.Item(0)("VATAmount"))
        MsgBody = MsgBody.Replace("<SalesInvoiceTotal>", dvEmail.Item(0)("TotalAmount"))

        'Admin Email
        SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), MsgBody, strsubject)
        'Buyer Email
        If strCC <> "" Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject, True, strCC)
        Else
            SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject)
        End If
        Return Nothing
    End Function
    ''' <summary>
    ''' Send Mail to Individual supplier as a result of CA send mail
    ''' </summary>
    ''' <param name="dvEmail">Consists of the information of all the selected Suppliers and their ccs</param>
    ''' <param name="dvWOSummary">Consists of the information related to WorkOrder</param>
    ''' <param name="dvSupplier">Consists of Supplier response</param>
    ''' <returns>Nothing</returns>
    ''' <remarks></remarks>
    Public Shared Function SendCAMail(ByVal dvEmail As DataView, ByVal dvWOSummary As DataView, ByVal dvSupplier As DataView, ByVal Comments As String)
        CommonFunctions.createLog("SendCAMail")
        'dvEmail Consists of the information of all the selected Suppliers and their ccs
        'dvWOSummary consists of the information related to WorkOrder
        'dvSupplier consists of Supplier's response

        Dim strName As String = ""
        Dim Email As String = ""
        Dim strCC As String = ""
        Dim BusinessArea As Integer
        BusinessArea = dvEmail.Item(0).Item("BusinessArea")

        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'To'"
        'Contact ID is the id of the admin of the company for whom the SI is generated.
        strName = dvEmail.Item(0).Item("Name")
        Email = dvEmail.Item(0).Item("Email")
        'Else Send mail to Admin
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'CC'"
        'Get the Email id list for the CC field
        strCC = GetCCRecipients(dvEmail)
        dvEmail.RowFilter = ""
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Conditional Accept Mail " & dvWOSummary.Item(0)("RefWOID")
        Else
            'Put German text here
            strsubject = "Conditional Accept Mail " & dvWOSummary.Item(0)("RefWOID")
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "CASendMail.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()


        'MsgBody = MsgBody.Replace("<Name>", strName)
        'Add WO Summary
        MsgBody = MsgBody.Replace("<Content>", Comments)
        MsgBody = MsgBody.Replace("<WorkOrderID>", dvWOSummary.Item(0)("RefWOID"))
        MsgBody = MsgBody.Replace("<WOTitle>", dvWOSummary.Item(0)("WOTitle"))
        MsgBody = MsgBody.Replace("<WOCategory>", dvWOSummary.Item(0)("WOCategory"))
        MsgBody = MsgBody.Replace("<DateAccepted>", dvWOSummary.Item(0)("DateModified"))
        MsgBody = MsgBody.Replace("<Location>", dvWOSummary.Item(0)("Location"))
        MsgBody = MsgBody.Replace("<ProposedPrice>", dvWOSummary.Item(0)("PlatformPrice"))
        If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
            MsgBody = MsgBody.Replace("Start Date:", "Appointment Date:")
            MsgBody = MsgBody.Replace("<StartDate>", dvWOSummary.Item(0)("DateStart"))
            MsgBody = MsgBody.Replace("Proposed End Date:", "").Replace("End Date:", "")
            MsgBody = MsgBody.Replace("<EndDate>", "")
        Else
            MsgBody = MsgBody.Replace("<StartDate>", dvWOSummary.Item(0)("DateStart") & Chr(10))
            MsgBody = MsgBody.Replace("<EndDate>", dvWOSummary.Item(0)("DateEnd"))
        End If

        'Add Supplier Response
        If dvSupplier.Count > 0 Then
            MsgBody = MsgBody.Replace("<SupPrice>", dvSupplier.Item(0)("Value"))
            If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
                MsgBody = MsgBody.Replace("<SupStartDate>", dvSupplier.Item(0)("DateStart"))
                MsgBody = MsgBody.Replace("<SupEndDate>", "")
            Else
                MsgBody = MsgBody.Replace("<SupStartDate>", dvSupplier.Item(0)("DateStart") & Chr(10))
                MsgBody = MsgBody.Replace("<SupEndDate>", dvSupplier.Item(0)("DateEnd"))
            End If
        Else
            MsgBody = MsgBody.Replace("<SupPrice>", "")
            If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
                MsgBody = MsgBody.Replace("<SupStartDate>", dvSupplier.Item(0)("DateStart"))
                MsgBody = MsgBody.Replace("<SupEndDate>", "")
            Else
                MsgBody = MsgBody.Replace("<SupStartDate>", dvSupplier.Item(0)("DateStart") & Chr(10))
                MsgBody = MsgBody.Replace("<SupEndDate>", dvSupplier.Item(0)("DateEnd"))
            End If
        End If

        'Admin Email
        If (ControlMail.IsMailCAToAdmin) Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), MsgBody, strsubject)
        End If
        'Buyer Email
        If strCC <> "" Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject, True, strCC)
        Else
            SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject)
        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' Sending mail on Undiscard of WorkOrder
    ''' </summary>
    ''' <param name="dvEmail"></param>
    ''' <param name="WorkOrderId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SendWOUnDiscardMail(ByVal dvEmail As DataView, ByVal WorkOrderId As String)
        CommonFunctions.createLog("SendWOUnDiscardMail")
        'dvEmail Consists of the information of all the selected Suppliers and their ccs
        'dvWOSummary consists of the information related to WorkOrder
        'dvSupplier consists of Supplier's response

        Dim strName As String = ""
        Dim Email As String = ""
        Dim strCC As String = ""
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'To'"
        'Contact ID is the id of the admin of the company for whom the SI is generated.
        strName = dvEmail.Item(0).Item("Name")
        Email = dvEmail.Item(0).Item("Email")
        'Else Send mail to Admin
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'CC'"
        'Get the Email id list for the CC field
        strCC = GetCCRecipients(dvEmail)
        dvEmail.RowFilter = ""
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Confirmation of Discard Work Order Reset"
        Else
            'Put German text here
            strsubject = "Confirmation of Discard Work Order Reset"
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOUndiscardSendMail.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()


        'MsgBody = MsgBody.Replace("<Name>", strName)
        'Add WO Summary

        MsgBody = MsgBody.Replace("<WorkOrderID>", WorkOrderId)

        'Admin Email
        SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), MsgBody, strsubject)
        'Supplier Email
        If strCC <> "" Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject, True, strCC)
        Else
            SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject)
        End If
        Return Nothing
    End Function

    Public Shared Sub SendDiscardWOMail(ByVal WorkOrderID As String, ByVal dvEmail As DataView)
        CommonFunctions.createLog("SendDiscardWOMail")
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Confirmation of Discarded Work Order " & WorkOrderID
        Else
            'Put German text here
            strsubject = "Confirmation of Discarded Work Order " & WorkOrderID
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WODiscard.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<WorkOrderID>", WorkOrderID)
        Dim strName As String = ""
        Dim strEmail As String = ""
        Dim strCC As String = ""
        'To Supplier
        'Dim dvEmail As New DataView(dsEmail.Tables(0))
        dvEmail.RowFilter = ""
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'To'"
        'WO Process:
        'Client: To � person who created wo, performed action (if both are different), CC � all those that have wo notif checked.
        'Supplier: To � person who accepted wo, performed action (if both are different), CC � all those that have wo notif checked.
        'Contact ID is the id of the contact who either created the workorder or performed the action.
        strName = dvEmail.Item(0).Item("Name").Replace("""", "")
        strEmail = dvEmail.Item(0).Item("Email")
        MsgBody = MsgBody.Replace("<Name>", strName)
        'Else Send mail to Admin
        dvEmail.RowFilter = ""
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'CC'"
        'Get the Email id list for the CC field
        strCC = GetCCRecipients(dvEmail)
        Dim IsMailOnDiscardToSP As Boolean = True
        If (IsMailOnDiscardToSP) Then
            If strCC <> "" Then
                SendMail.SendMail(ApplicationSettings.EmailInfo(), strEmail, MsgBody, strsubject, True, strCC)
            Else
                SendMail.SendMail(ApplicationSettings.EmailInfo(), strEmail, MsgBody, strsubject)
            End If
        End If
        'TO Admin
        'Contact ID is the id of the contact who either created the workorder or performed the action.
        Dim SendMailToA As Boolean = False
        SendMailToA = True
        If SendMailToA = True Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), MsgBody, strsubject)
        End If

    End Sub

    ''' <summary>
    ''' Sending depot sheets to corresponding buyer and depot user
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <param name="dvEmail"></param>
    ''' <remarks></remarks>
    Public Shared Sub DepotMail(ByVal ds As DataSet, ByVal dvEmail As DataView, ByVal CollectionDate As String)
        CommonFunctions.createLog("DepotMail")
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                If (ds.Tables(0).Rows(i).Item("seq") = 1) Then


                    Dim MsgBody As String
                    'Get content from txt file to get email content.
                    Dim WebRequest As WebRequest
                    Dim WebResponse As WebResponse
                    Dim sr As StreamReader
                    Dim Buffer As String

                    WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "Depot.htm")
                    WebResponse = WebRequest.GetResponse()
                    sr = New StreamReader(WebResponse.GetResponseStream())
                    Buffer = sr.ReadToEnd
                    MsgBody = Buffer
                    sr.Close()
                    MsgBody = MsgBody.Replace("<AssignedUserRoleOfDepot>", ds.Tables(0).Rows(i).Item("UserDepot"))
                    MsgBody = MsgBody.Replace("<BuyerCompanyName>", ds.Tables(0).Rows(i).Item("BuyerCompanyName"))
                    MsgBody = MsgBody.Replace("<DeptName>", ds.Tables(0).Rows(i).Item("DepotName"))
                    MsgBody = MsgBody.Replace("<DepotAddress>", ds.Tables(0).Rows(i).Item("DepotAddress"))
                    MsgBody = MsgBody.Replace("<OrderWorkUsername>", ds.Tables(0).Rows(i).Item("OrderWorkUsername"))
                    MsgBody = MsgBody.Replace("<DateDeportSheetGeneration>", Strings.FormatDateTime(ds.Tables(0).Rows(i).Item("Date").ToString(), DateFormat.ShortDate))
                    MsgBody = MsgBody.Replace("<ServicePartnerCompanyName>", ds.Tables(0).Rows(i).Item("Supplier"))
                    MsgBody = MsgBody.Replace("<NextDay>", CollectionDate)
                    Dim dvTable1 As DataView = ds.Tables(1).DefaultView
                    dvTable1.RowFilter = "DepotId = " & ds.Tables(0).Rows(i).Item("DepotId") & " and SupplierCompanyId = " & ds.Tables(0).Rows(i).Item("SupplierCompanyId") & " and WorkOrderID = " & ds.Tables(0).Rows(i).Item("WorkOrderID")
                    Dim str As New StringBuilder
                    str.Append("")
                    'For j As Integer = 0 To dvTable1.ToTable.Rows.Count - 1
                    If dvTable1.ToTable.Rows.Count > 0 Then
                        str.Append("<tr><td  width='120' style='border-top:2px solid #b7b7b7; border-left:1px solid #b7b7b7;  padding-left:10px;' height='20'>&nbsp;")
                        str.Append(dvTable1.ToTable.Rows(0)("UserDepot"))
                        str.Append("</td><td  width='120' style='border-top:2px solid #b7b7b7; border-left:1px solid #b7b7b7;  padding-left:10px;' height='20'>&nbsp;")
                        str.Append(dvTable1.ToTable.Rows(0)("PostCode"))
                        str.Append("</td><td  width='120' style='border-top:2px solid #b7b7b7; border-left:1px solid #b7b7b7;  padding-left:10px;' height='20'>&nbsp;")
                        str.Append(dvTable1.ToTable.Rows(0)("WOCRNumber"))
                        str.Append("</td><td  width='120' style='border-top:2px solid #b7b7b7; border-left:1px solid #b7b7b7;  padding-left:10px;' height='20'>&nbsp;")
                        str.Append(dvTable1.ToTable.Rows(0)("PONumber"))
                        str.Append("</td><td  width='120' style='border-top:2px solid #b7b7b7; border-left:1px solid #b7b7b7; border-right:1px solid #b7b7b7; padding-left:10px;' height='20'>&nbsp;</td></tr>")
                        str.Append("<tr><td colspan='5' width='600' style='border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7;border-right:1px solid #b7b7b7;padding-left:35px;padding-top:5px;padding-bottom:5px;'><b>Products Purchased</b>&nbsp;:&nbsp;")
                        str.Append(dvTable1.ToTable.Rows(0)("ProductsPurchased"))
                        str.Append("<br />")
                        str.Append(dvTable1.ToTable.Rows(0)("AddProductsPurchased"))
                        str.Append("<br /></td></tr>")
                    End If

                    'Next
                    MsgBody = MsgBody.Replace("<Details>", str.ToString)
                    dvEmail.RowFilter = "DepotId = " & ds.Tables(0).Rows(i).Item("DepotId") & " and SupplierCompanyId = " & ds.Tables(0).Rows(i).Item("SupplierCompanyId") & " and WorkOrderID = " & ds.Tables(0).Rows(i).Item("WorkOrderID")
                    Dim dvDepot As DataView
                    dvDepot = dvEmail.ToTable.DefaultView
                    Dim strWorkorderID As String = ""
                    If dvDepot.Count > 0 Then
                        'For dvDepotCount As Integer = 0 To dvDepot.Count - 1
                        'If (dvDepot.ToTable.Rows(0)("depotID") = ds.Tables(0).Rows(i).Item("DepotId") And dvDepot.ToTable.Rows(0)("SupplierCompanyID") = ds.Tables(0).Rows(i).Item("SupplierCompanyId") And dvDepot.ToTable.Rows(0)("ContactType") = "Supplier") And dvDepot.ToTable.Rows(0)("Recipient") <> "CC" Then
                        '    If strWorkorderID = "" Then
                        strWorkorderID = dvDepot.ToTable.Rows(0)("WorkorderID").ToString
                        '    Else
                        '        strWorkorderID = strWorkorderID & ", " & dvDepot.ToTable.Rows(0)("WorkorderID").ToString
                        '    End If
                        'End If
                        'Next
                    End If
                    Dim strsubject As String = "COLLECTION SHEET - " + strWorkorderID
                    Dim strCC As String = ""
                    dvDepot.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'To'"
                    'Contact ID is the id of the admin of the company for whom the SI is generated.
                    Dim Name As String
                    Dim Email As String
                    Name = dvDepot.ToTable.Rows(0)("Name")
                    Email = dvDepot.ToTable.Rows(0)("Email")
                    'Else Send mail to Admin
                    dvDepot.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'CC'"
                    Dim dvCCEmail As DataView
                    'Get the Email id list for the CC field
                    dvCCEmail = dvDepot.ToTable.DefaultView
                    strCC = GetCCRecipients(dvCCEmail)

                    'Dim strBCC As String = ""
                    'strBCC = ApplicationSettings.Error_Email_Contact1
                    If strCC <> "" Then
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject, True, strCC, True, False, "")
                    Else
                        SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject, False, "", True, False, "")
                    End If
                    dvDepot.RowFilter = ""
                    'Sending mail to depot user
                    dvDepot.RowFilter = "ContactType = '" & ApplicationSettings.ViewerBuyer & "' AND " & "Recipient = 'To'"
                    'start distinct
                    Dim dtUnique As DataTable
                    dtUnique = dvDepot.ToTable(True, "Email")
                    Dim dt As DataTable
                    dt = dvDepot.ToTable
                    Dim dtnew As New DataTable
                    For dtcount As Integer = 0 To dt.Columns.Count - 1
                        dtnew.Columns.Add(dt.Columns(dtcount).ColumnName)
                    Next
                    Dim WorkOrderID As String
                    Dim newRow As DataRow = dtnew.NewRow()
                    For k As Integer = 0 To dtUnique.Rows.Count - 1
                        WorkOrderID = ""
                        newRow = Nothing
                        For j As Integer = 0 To dt.Rows.Count - 1
                            If dtUnique.Rows(k)("Email") = dt.Rows(j)("Email") Then
                                'Dim dr As DataRow
                                'If WorkOrderID = "" Then
                                WorkOrderID = dt.Rows(j)("WorkOrderID")
                                dtnew.ImportRow(dt.Rows(j))
                                ''Else
                                ''    WorkOrderID = WorkOrderID & "," & dt.Rows(j)("WorkOrderID")
                                ''    For Each newRow In dtnew.Rows
                                'If newRow.Item("Email") = dt.Rows(j)("Email") Then
                                '    newRow.Item("WorkOrderID") = WorkOrderID
                                'End If
                                '    Next
                                'End If
                            End If
                        Next
                    Next
                    Dim dv As DataView
                    dv = dtnew.DefaultView
                    'end distinct
                    If dv.ToTable.Rows.Count > 0 Then
                        For dvDepotCountBuyer As Integer = 0 To dv.Count - 1
                            SendMail.SendMail(ApplicationSettings.EmailInfo(), dv.ToTable.Rows(dvDepotCountBuyer)("Email"), MsgBody, strsubject, False, "", True)
                        Next
                    End If
                    dvDepot.RowFilter = ""
                    dvEmail.RowFilter = ""
                End If
            Next

        End If
    End Sub



    ''' <summary>
    ''' Function to send mail when the OW admin updates workorder
    ''' </summary>
    ''' <param name="woid"></param>
    ''' <param name="comments"></param>
    ''' <param name="name"></param>
    ''' <param name="email"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Sub sendUpdateWorkorderMail(ByVal woid As String, ByVal portalprice As String, ByVal woTitle As String, ByVal location As String, ByVal comments As String, ByVal StartDate As String, ByVal EndDate As String, ByVal AptTime As String, ByVal GoodLocation As String, ByVal ProductsPurchased As String, ByVal BizDivId As Integer, ByVal dvEmail As DataView, ByVal dvSupplier As DataView, ByVal businessArea As String)
        CommonFunctions.createLog("sendUpdateWorkorderMail")
        Dim strName As String = ""
        Dim Email As String = ""
        Dim strCC As String = ""
        Dim strsubject As String
        'Dim BusinessArea As Integer
        'BusinessArea = dvEmail.Item(0).Item("BusinessArea")
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Update: The details of WO " & woid & " have been changed"
        Else
            'Put German text here
            strsubject = "Update: The details of WO " & woid & " have been changed"
        End If
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'To'"
        'Contact ID is the id of the admin of the company for whom the SI is generated.
        strName = dvEmail.Item(0).Item("Name")
        Email = dvEmail.Item(0).Item("Email")
        'Else Send mail to Admin
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'CC'"
        'Get the Email id list for the CC field
        strCC = GetCCRecipients(dvEmail)
        dvEmail.RowFilter = ""

        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOUpdate.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<Name>", strName)
        MsgBody = MsgBody.Replace("<WorkOrderID>", woid)
        MsgBody = MsgBody.Replace("<WOTitle>", woTitle)
        MsgBody = MsgBody.Replace("<Location>", location)
        MsgBody = MsgBody.Replace("<GoodsLocation>", GoodLocation)
        MsgBody = MsgBody.Replace("<ProductsPurchased>", ProductsPurchased)
        If businessArea = ApplicationSettings.BusinessAreaRetail Then
            MsgBody = MsgBody.Replace("Start Date:", "Appointment Date:")
            MsgBody = MsgBody.Replace("<StartDate>", StartDate)
            MsgBody = MsgBody.Replace("End Date:", "Appointment Time:")
            MsgBody = MsgBody.Replace("<EndDate>", AptTime)
        Else
            MsgBody = MsgBody.Replace("<StartDate>", StartDate)
            MsgBody = MsgBody.Replace("<EndDate>", EndDate)
        End If
        MsgBody = MsgBody.Replace("<Comment>", comments)
        MsgBody = MsgBody.Replace("<ProposedPrice>", portalprice)

        SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject)
    End Sub
    ''' <summary>
    ''' Function to send registeration mail to user as well as admin
    ''' </summary>
    ''' <param name="contacts">XSD for registering contact</param>
    ''' <param name="mailToAdmin">Boolean value to send mail to admin</param>
    ''' <param name="Country">Specify Country</param>
    ''' <param name="intuserType">Specify Type of user</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SendMailRegister(ByVal contacts As XSDContacts, Optional ByVal mailToAdmin As Boolean = True, Optional ByVal Country As String = "UK", Optional ByVal intuserType As ApplicationSettings.UserType = ApplicationSettings.UserType.buyer, Optional ByVal Password As String = "") As Boolean
        CommonFunctions.createLog("SendMailRegister")
        Dim Success As Boolean = False
        ' Fetching value from typed dataset - Contacts
        Dim dv As DataView
        dv = contacts.Contacts.Copy.DefaultView
        dv.RowFilter = "ContactID = '1'"
        Dim strName As String
        Dim UserName As String
        strName = Trim(dv.Item(0).Item("FullName")).Replace("""", "")
        Dim dvLogin As DataView
        dvLogin = contacts.tblContactsLogin.Copy.DefaultView
        dvLogin.RowFilter = "ContactID = '1'"
        UserName = Trim(dvLogin.Item(0).Item("UserName")).Replace("""", "")
        If (Password <> "") Then
            Password = Trim(Password).Replace("""", "")
        End If
        Dim Email As String = contacts.tblContactsLogin(0).UserName
        Dim strsubject As String
        strsubject = "OrderWork - Account Creation"


        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Registration.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Name>", strName)
        MsgBody = MsgBody.Replace("<UserName>", UserName)
        MsgBody = MsgBody.Replace("<Password>", Password)
        MsgBody = MsgBody.Replace("<SecureLogin>", ApplicationSettings.OrderWorkMyURL & "SecureLogin.aspx")

        SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject)
        'Check whether mail to be send to admin
        Return Success
    End Function

    Public Shared Function SendFastCloseMail(ByVal FullName As String, ByVal WorkOrderID As String, ByVal PINo As String, ByVal Email As String) As Boolean
        CommonFunctions.createLog("SendFastCloseMail")
        Dim Success As Boolean = False
        ' Fetching value from typed dataset - Contacts
        Dim strsubject As String
        strsubject = WorkOrderID & " has been closed and marked for payment"
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "FastClose.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<name>", FullName)
        MsgBody = MsgBody.Replace("<workorderid>", WorkOrderID)
        MsgBody = MsgBody.Replace("<pino>", PINo)
        MsgBody = MsgBody.Replace("<SecureLogin>", ApplicationSettings.OrderWorkMyURL & "SecureLogin.aspx")

        Success = SendMail.SendMail(ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject)
        'Check whether mail to be send to admin
        Return Success
    End Function

    Public Shared Sub SendDiscardCreateNewWOMail(ByVal WorkOrderID As String, ByVal dvEmail As DataView, ByVal pNewWorkOrderId As String, ByVal pWOTitle As String, ByVal pWOCategory As String, ByVal pLocation As String, ByVal pStartDate As DateTime, ByVal pEndDate As DateTime, ByVal pWODate As DateTime, ByVal pPrice As Decimal)

        CommonFunctions.createLog("SendDiscardCreateNewWOMail")
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Confirmation of Discarded Work Order " & WorkOrderID
        Else
            'Put German text here
            strsubject = "Confirmation of Discarded Work Order " & WorkOrderID
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOCANew.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<WorkOrderID>", WorkOrderID)
        MsgBody = MsgBody.Replace("<NewWorkOrderID>", pNewWorkOrderId)
        MsgBody = MsgBody.Replace("<WOTitle>", pWOTitle)
        MsgBody = MsgBody.Replace("<WOCategory>", pWOCategory)
        MsgBody = MsgBody.Replace("<Location>", pLocation)
        MsgBody = MsgBody.Replace("<StartDate>", pStartDate)
        MsgBody = MsgBody.Replace("<EndDate>", pEndDate)
        MsgBody = MsgBody.Replace("<Date>", pWODate)
        MsgBody = MsgBody.Replace("<Price>", pPrice)

        Dim strName As String = ""
        Dim strEmail As String = ""

        Dim strCC As String = ""
        'To Supplier
        'Dim dvEmail As New DataView(dsEmail.Tables(0))
        dvEmail.RowFilter = ""
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'To'"
        'WO Process:
        'Client: To � person who created wo, performed action (if both are different), CC � all those that have wo notif checked.
        'Supplier: To � person who accepted wo, performed action (if both are different), CC � all those that have wo notif checked.
        'Contact ID is the id of the contact who either created the workorder or performed the action.
        strName = dvEmail.Item(0).Item("Name").Replace("""", "")
        strEmail = dvEmail.Item(0).Item("Email")
        MsgBody = MsgBody.Replace("<Name>", strName)
        'Else Send mail to Admin
        dvEmail.RowFilter = ""
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'CC'"
        'Get the Email id list for the CC field
        strCC = GetCCRecipients(dvEmail)
        Dim IsMailOnDiscardToSP As Boolean = True
        If (IsMailOnDiscardToSP) Then
            If strCC <> "" Then
                SendMail.SendMail(ApplicationSettings.EmailInfo(), strEmail, MsgBody, strsubject, True, strCC)
            Else
                SendMail.SendMail(ApplicationSettings.EmailInfo(), strEmail, MsgBody, strsubject)
            End If
        End If
        'TO Admin
        'Contact ID is the id of the contact who either created the workorder or performed the action.
        Dim SendMailToA As Boolean = False
        SendMailToA = True
        If SendMailToA = True Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), MsgBody, strsubject)
        End If

    End Sub

    Public Shared Sub SendEmailToSONYonWOClose(ByVal PONumber As String, ByVal pAttachment As Attachment)
        CommonFunctions.createLog("SendEmailToSONYonWOClose")
        Dim mailSubject As String
        Dim mailTo As String = ApplicationSettings.SONYMailCloseWO
        Dim mailFrom As String = ApplicationSettings.EmailInfo
        Dim Attachment As System.Net.Mail.Attachment

        'Attachment = New System.Net.Mail.Attachment(pAttachment)

        mailSubject = "Sony Order Fulfilment Form - " & PONumber

        Dim mailBody As String

        mailBody = "Dear SONY Support, <br/ ><br />"
        mailBody += "Please find the attached SONY Order Fulfilment Form. <br /><br />"
        mailBody += "Kind Regards, <br />"
        mailBody += "OrderWork Team <br /><br />"

        Dim strName As String = ""
        Dim strEmail As String = ""

        Dim SMTPMAILSERVER As String = ""
        Try
            SMTPMAILSERVER = ApplicationSettings.SMTPServer
        Catch Ex As Exception
            Dim ex1 As New Exception("Unable to find value for key - webSite_Smtp_Mail_Server - . Unexpected error occured in server while sending email.")
            Throw (ex1)
        End Try

        Dim from As New MailAddress(mailFrom)
        Dim sendTo As New MailAddress("sonyjobs@orderwork.co.uk")
        Dim message As New MailMessage(from, sendTo)

        Dim i As Integer


        message.Bcc.Add(ApplicationSettings.Error_Email_Contact1)
        message.Attachments.Add(pAttachment)

        'message.CC.Add("hdesai@orderwork.co.uk")
        'message.CC.Add("mheath@orderwork.co.uk")

        message.Subject = mailSubject
        message.Body = mailBody
        message.Priority = MailPriority.Normal
        message.IsBodyHtml = True

        Dim client As New SmtpClient(SMTPMAILSERVER)
        client.UseDefaultCredentials = True
        'client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory
        'client.PickupDirectoryLocation = "C:\TestEmails"
        client.Send(message)

    End Sub

    Public Shared Function SendAcceptedMail(ByVal pAttachment As Attachment, ByVal dvEmail As DataView, ByVal dvWOSummary As DataView, ByVal dvSupplier As DataView, ByVal Comments As String)

        CommonFunctions.createLog("SendAcceptedMail")
        'dvEmail Consists of the information of all the selected Suppliers and their ccs
        'dvWOSummary consists of the information related to WorkOrder
        'dvSupplier consists of Supplier's response

        Dim strName As String = ""
        Dim Email As String = ""
        Dim strCC As String = ""
        Dim BusinessArea As Integer
        BusinessArea = dvEmail.Item(0).Item("BusinessArea")

        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'To'"
        'Contact ID is the id of the admin of the company for whom the SI is generated.
        strName = dvEmail.Item(0).Item("Name")
        Email = dvEmail.Item(0).Item("Email")
        'Else Send mail to Admin
        dvEmail.RowFilter = "ContactType = '" & ApplicationSettings.ViewerSupplier & "' AND " & "Recipient = 'CC'"
        'Get the Email id list for the CC field
        strCC = GetCCRecipients(dvEmail)
        dvEmail.RowFilter = ""
        Dim strsubject As String
        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
            strsubject = "Accept Mail " & dvWOSummary.Item(0)("RefWOID")
        Else
            'Put German text here
            strsubject = "Accept Mail " & dvWOSummary.Item(0)("RefWOID")
        End If
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "AcceptSendMail.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        ''MsgBody = MsgBody.Replace("<Name>", strName)
        ''Add WO Summary
        'MsgBody = MsgBody.Replace("<Content>", Comments)
        MsgBody = MsgBody.Replace("<WorkOrderID>", dvWOSummary.Item(0)("RefWOID"))
        'MsgBody = MsgBody.Replace("<WOTitle>", dvWOSummary.Item(0)("WOTitle"))
        'MsgBody = MsgBody.Replace("<WOCategory>", dvWOSummary.Item(0)("WOCategory"))
        'MsgBody = MsgBody.Replace("<DateAccepted>", dvWOSummary.Item(0)("DateModified"))
        'MsgBody = MsgBody.Replace("<Location>", dvWOSummary.Item(0)("Location"))
        'MsgBody = MsgBody.Replace("<ProposedPrice>", dvWOSummary.Item(0)("PlatformPrice"))
        'If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
        '    MsgBody = MsgBody.Replace("Start Date:", "Appointment Date:")
        '    MsgBody = MsgBody.Replace("<StartDate>", dvWOSummary.Item(0)("DateStart"))
        '    MsgBody = MsgBody.Replace("Proposed End Date:", "").Replace("End Date:", "")
        '    MsgBody = MsgBody.Replace("<EndDate>", "")
        'Else
        '    MsgBody = MsgBody.Replace("<StartDate>", dvWOSummary.Item(0)("DateStart") & Chr(10))
        '    MsgBody = MsgBody.Replace("<EndDate>", dvWOSummary.Item(0)("DateEnd"))
        'End If

        ''Add Supplier Response
        'If dvSupplier.Count > 0 Then
        '    MsgBody = MsgBody.Replace("<SupPrice>", dvSupplier.Item(0)("Value"))
        '    If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
        '        MsgBody = MsgBody.Replace("<SupStartDate>", dvSupplier.Item(0)("DateStart"))
        '        MsgBody = MsgBody.Replace("<SupEndDate>", "")
        '    Else
        '        MsgBody = MsgBody.Replace("<SupStartDate>", dvSupplier.Item(0)("DateStart") & Chr(10))
        '        MsgBody = MsgBody.Replace("<SupEndDate>", dvSupplier.Item(0)("DateEnd"))
        '    End If
        'Else
        '    MsgBody = MsgBody.Replace("<SupPrice>", "")
        '    If BusinessArea = ApplicationSettings.BusinessAreaRetail Then
        '        MsgBody = MsgBody.Replace("<SupStartDate>", dvSupplier.Item(0)("DateStart"))
        '        MsgBody = MsgBody.Replace("<SupEndDate>", "")
        '    Else
        '        MsgBody = MsgBody.Replace("<SupStartDate>", dvSupplier.Item(0)("DateStart") & Chr(10))
        '        MsgBody = MsgBody.Replace("<SupEndDate>", dvSupplier.Item(0)("DateEnd"))
        '    End If
        'End If

        'Admin Email
        If (ControlMail.IsMailCAToAdmin) Then
            SendMail.SendMailWithAttachment(pAttachment, ApplicationSettings.EmailInfo(), ApplicationSettings.EmailInfo(), MsgBody, strsubject)
        End If
        'Buyer Email
        If strCC <> "" Then
            SendMail.SendMailWithAttachment(pAttachment, ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject, True, strCC)
        Else
            SendMail.SendMailWithAttachment(pAttachment, ApplicationSettings.EmailInfo(), Email, MsgBody, strsubject)
        End If
        Return Nothing
    End Function
    Public Shared Function SendAccountInfoUpdate(ByVal UserName As String, ByVal EmailIdToSend As String, ByVal SortCode As String, ByVal AccountNumber As String, ByVal AccountName As String, ByVal Name As String, ByVal PostalCode As String, ByVal Address As String, ByVal City As String, ByVal State As String, ByVal Phone As String, ByVal FirstName As String, ByVal LastName As String, ByVal EmailAddress As String)
        CommonFunctions.createLog("SendAccountInfoUpdate")
        Dim Success As Boolean = False
        ' Fetching value from typed dataset - Contacts
        Dim strsubject As String
        strsubject = "Account Information Change"
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "AccountInfoUpdate.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        Dim Mailcontent As String
        Dim maskedSortCode As String = ""
        Dim maskedAccountNo As String = ""
        Dim maskedAccountName As String = ""


        If (SortCode <> "") Then
            maskedSortCode = SortCode.Substring(0, SortCode.Length - 5) + "XX-XX"
            Mailcontent = Mailcontent & "<br/> * Sort Code - " & maskedSortCode
        End If

        If (AccountNumber <> "") Then
            maskedAccountNo = AccountNumber.Substring(0, AccountNumber.Length - 4) + "XXXX"
            Mailcontent = Mailcontent & "<br/> * Account Number - " & maskedAccountNo
        End If

        If (AccountName <> "") Then
            maskedAccountName = AccountName.Substring(0, AccountName.Length - (AccountName.Length - 2)) + "XXX"
            Mailcontent = Mailcontent & "<br/> * Account Name - " & maskedAccountName
        End If

        If (Name <> "") Then
            Mailcontent = Mailcontent & "<br/> * Name - " & Name
        End If
        If (PostalCode <> "") Then
            Mailcontent = Mailcontent & "<br/> * PostalCode - " & PostalCode
        End If
        If (Address <> "") Then
            Mailcontent = Mailcontent & "<br/> * Address - " & Address
        End If
        If (City <> "") Then
            Mailcontent = Mailcontent & "<br/> * City - " & City
        End If
        If (State <> "") Then
            Mailcontent = Mailcontent & "<br/> * State - " & State
        End If
        If (Phone <> "") Then
            Mailcontent = Mailcontent & "<br/> * Phone - " & Phone
        End If
        If (FirstName <> "") Then
            Mailcontent = Mailcontent & "<br/> * First Name - " & FirstName
        End If
        If (LastName <> "") Then
            Mailcontent = Mailcontent & "<br/> * Last Name - " & LastName
        End If
        If (EmailAddress <> "") Then
            Mailcontent = Mailcontent & "<br/> * EmailAddress - " & EmailAddress
        End If

        MsgBody = MsgBody.Replace("<UserName>", UserName)
        MsgBody = MsgBody.Replace("<content>", Mailcontent)

        Dim strBCC As String = ""
        strBCC = ApplicationSettings.BccEmail

        Success = SendMail.SendMail(ApplicationSettings.EmailInfo(), EmailIdToSend, MsgBody, strsubject, False, "", True, True, strBCC)

        Return Nothing
    End Function


    Public Shared Sub SendPostMessageMail(ByVal strsubject As String, ByVal strMsg As String, ByVal dsEmail As DataSet)
        CommonFunctions.createLog("SendPostMessageMail")
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String

        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "PostMessage.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<Message>", strMsg)

        Dim dvAdminContacts As DataView
        dvAdminContacts = dsEmail.Tables(1).DefaultView
        dvAdminContacts.RowFilter = "Email <> '' AND RoleGroupID = '" & ApplicationSettings.RoleSupplierAdminID & "'"
        Dim dvACrow, dvCCrow As DataRowView

        Dim finalMailContent As String
        finalMailContent = MsgBody

        'Send mail to admin
        SendMail.SendMail(ApplicationSettings.EmailInfo(), "info@orderwork.co.uk", finalMailContent, strsubject, False, "", True)

        For Each dvACrow In dvAdminContacts
            If Not (dvACrow("Email").ToString.Contains(",")) Then
                Try
                    SendMail.SendMail(ApplicationSettings.EmailInfo(), dvACrow("Email"), finalMailContent, strsubject, False, "", True)
                Catch ex As Exception
                    CommonFunctions.createLog(ex.ToString())
                End Try
            End If
        Next
    End Sub


    Public Shared Sub SendUpsellInvoices(ByVal strsubject As String, ByVal CustomerEmail As String, ByVal InvoicePDF As Attachment, ByVal ClientCompany As Integer)
        CommonFunctions.createLog("SendUpsellInvoices")
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String

        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Upsell_receipt_email.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        If (ClientCompany = CInt(ApplicationSettings.SamsungElectronicsCompany) Or ClientCompany = CInt(ApplicationSettings.SamsungPRSCompany)) Then
            MsgBody = MsgBody.Replace("<PhoneNumber>", "0203 249 7604")
            MsgBody = MsgBody.Replace("<PhoneNumberwithSpace>", "0203&nbsp;249&nbsp;7604")
        Else
            MsgBody = MsgBody.Replace("<PhoneNumber>", "0203 053 0343")
            MsgBody = MsgBody.Replace("<PhoneNumberwithSpace>", "0203&nbsp;053&nbsp;0343")
        End If

        Dim finalMailContent As String
        finalMailContent = MsgBody

        'Send mail to CustomerEmail
        SendMail.SendMailWithAttachment(InvoicePDF, ApplicationSettings.EmailInfo(), CustomerEmail, finalMailContent, strsubject, True, ApplicationSettings.OWReceiptsEmail, True, False, "", ClientCompany)

    End Sub
    Public Shared Sub SendCustomerJobBookEmail(ByVal strsubject As String, ByVal CustomerEmail As String, ByVal WorkordeId As String, ByVal AppointmentDate As String, ByVal AppointmentTimeSlot As String, ByVal ScopeOfWork As String, ByVal CustomerName As String, ByVal ClientCompany As Integer, Optional ByVal CustomerRefNo As String = "")
        CommonFunctions.createLog("SendCustomerJobBookEmail")
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String

        Dim strBCC As String = ""
        Dim Attachment As System.Net.Mail.Attachment
        If (ClientCompany = CInt(ApplicationSettings.Telecare24Company)) Then
            strBCC = ApplicationSettings.BookingConfirmationTelecare24Bcc
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_Telecare24.htm")
        ElseIf (ClientCompany = CInt(ApplicationSettings.Lifeline24Company)) Then
            strBCC = ApplicationSettings.BookingConfirmationLifeline24Bcc
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_Lifeline24.htm")
        Else
            strBCC = ApplicationSettings.ArgosBccEmail
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "Booking_Confirmation_generic.htm")
            'Attachment = New System.Net.Mail.Attachment(OrderWorkLibrary.ApplicationSettings.AttachmentUploadPath(0) & "Customer Terms and Conditions.pdf")
            Attachment = New System.Net.Mail.Attachment((HttpContext.Current.Server.MapPath("~/Attachments/") & "Customer Terms and Conditions.pdf").ToString())
        End If

        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<CustomerName>", CustomerName)
        MsgBody = MsgBody.Replace("<WorkordeId>", WorkordeId)
        MsgBody = MsgBody.Replace("<AppointmentDate>", AppointmentDate)
        MsgBody = MsgBody.Replace("<AppointmentTimeSlot>", AppointmentTimeSlot)
        MsgBody = MsgBody.Replace("<ScopeOfWork>", ScopeOfWork)

        If (ClientCompany = CInt(ApplicationSettings.Telecare24Company)) Then
            MsgBody = MsgBody.Replace("<CustomerReferenceNo>", CustomerRefNo)
        End If
        If (ClientCompany = CInt(ApplicationSettings.Lifeline24Company)) Then
            MsgBody = MsgBody.Replace("<PhoneNumber>", "0800 999 0400")
            MsgBody = MsgBody.Replace("<PhoneNumberwithSpace>", "0800&nbsp;999&nbsp;0400")
        End If

        Dim finalMailContent As String
        finalMailContent = MsgBody

        'Send mail to CustomerEmail
        If (ClientCompany = CInt(ApplicationSettings.Telecare24Company)) Or (ClientCompany = CInt(ApplicationSettings.Lifeline24Company)) Then
            SendMail.SendMail(ApplicationSettings.EmailInfo(), CustomerEmail, finalMailContent, strsubject, False, "", True, True, strBCC)
        Else
            SendMail.SendMailWithAttachment(Attachment, ApplicationSettings.EmailInfo(), CustomerEmail, finalMailContent, strsubject, False, "", True, True, strBCC, ClientCompany)
        End If
    End Sub

    Public Shared Function SendWOSubmittedWithoutBillingLocation(ByVal WorkOrderID As String) As Boolean
        CommonFunctions.createLog("SendWOSubmittedWithoutBillingLocation")
        Dim Success As Boolean = False
        ' Fetching value from typed dataset - Contacts
        Dim strsubject As String
        strsubject = "WO " & WorkOrderID & " has been book with no known Billing location"
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "WOWithoutBillingLocation.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<WorkOrderID>", WorkOrderID)

        Success = SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.EmailPortalAdmin, MsgBody, strsubject)
        'Check whether mail to be send to admin
        Return Success
    End Function

    Public Shared Sub SendsupplierApproveMail(ByVal UserName As String, ByVal Name As String, ByVal Password As String)
        CommonFunctions.createLog("SendsupplierApproveMail")
        Dim strsubject As String
        strsubject = "OrderWork - Account Status"
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        'WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "AccountRegistration.txt")
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "SupplierApproval.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<name>", Name)
        MsgBody = MsgBody.Replace("<loginurl>", ApplicationSettings.OrderWorkMyURL & "securelogin.aspx")

        If (Password = "") Then
            MsgBody = MsgBody.Replace("id='PasswordInfo'", "id='PasswordInfo' style='display:none;'")
        Else
            MsgBody = MsgBody.Replace("<username>", UserName)
            MsgBody = MsgBody.Replace("<password>", Password)
        End If
        ' Dim pAttachment() As String = {"Orderwork - Supplier Portal Handbook.pdf", "Orderwork - Terms and Conditions.pdf", "Orderwork Privacy Policy.pdf"}

        SendMail.SendMailSupplierApprovalWithAttachment(ApplicationSettings.EmailInfo(), UserName, MsgBody, strsubject, False, "", True, False, "")
    End Sub

    Public Shared Sub SendUserProfileCompletionMail(ByVal UserName As String, ByVal Name As String)
        CommonFunctions.createLog("SendUserProfileCompletionMail")
        Dim strsubject As String
        strsubject = "OrderWork - Profile Completion (Minimum Requirements Met)"
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "UserProfileCompletion.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<name>", Name)
        SendMail.SendMail(ApplicationSettings.EmailInfo(), UserName, MsgBody, strsubject, False, "", True, False, "")
    End Sub

    Public Shared Sub SendRecruitmentProfileCompletionMail(ByVal dsEmailInfo As DataSet)
        CommonFunctions.createLog("SendRecruitmentProfileCompletionMail")
        Dim strsubject As String
        strsubject = "OrderWork - Profile Completion (Minimum Requirements Met) - " + dsEmailInfo.Tables(0).Rows(0)("Email").ToString
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "RecruitmentProfileCompletion.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        Dim CompanyProfileLink As String = ""
        If (dsEmailInfo.Tables(0).Rows.Count > 0) Then
            MsgBody = MsgBody.Replace("<companyname>", dsEmailInfo.Tables(0).Rows(0)("CompanyName").ToString)
            MsgBody = MsgBody.Replace("<companystatus>", dsEmailInfo.Tables(0).Rows(0)("CompanyStatus").ToString)
            MsgBody = MsgBody.Replace("<emailid>", dsEmailInfo.Tables(0).Rows(0)("Email").ToString)
            MsgBody = MsgBody.Replace("<fullname>", dsEmailInfo.Tables(0).Rows(0)("Fullname").ToString)
            MsgBody = MsgBody.Replace("<landline>", dsEmailInfo.Tables(0).Rows(0)("Phone").ToString)
            MsgBody = MsgBody.Replace("<mobile>", dsEmailInfo.Tables(0).Rows(0)("Mobile").ToString)

            CompanyProfileLink &= ApplicationSettings.WebPath & "CompanyProfile.aspx?bizDivId=1&classId=2&companyId=" & dsEmailInfo.Tables(0).Rows(0)("MainContactID").ToString

            MsgBody = MsgBody.Replace("<companyprofilelink>", CompanyProfileLink)
        End If

        SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.recruitmentEmail(), MsgBody, strsubject, False, "", True, False, "")
    End Sub
    'EmailAccountingUpdate
    Public Shared Sub SendEmailAccountingUpdate(ByVal UpdatedData As String, ByVal UpdatedValue As String, ByVal UpdatedField As String, ByVal OldUpdatedValue As String, ByVal NewUpdatedValue As String, ByVal FullName As String)
        CommonFunctions.createLog("SendEmailAccountingUpdate")
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "EmailAccountingUpdate.txt")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()

        MsgBody = MsgBody.Replace("<UpdatedData>", UpdatedData)
        MsgBody = MsgBody.Replace("<UpdatedValue>", UpdatedValue)
        MsgBody = MsgBody.Replace("<UpdatedField>", UpdatedField)
        MsgBody = MsgBody.Replace("<OldUpdatedValue>", OldUpdatedValue)
        MsgBody = MsgBody.Replace("<NewUpdatedValue>", NewUpdatedValue)
        MsgBody = MsgBody.Replace("<FullName>", FullName)

        Dim strsubject As String
        strsubject = "Orderwork - " & UpdatedData & " " & UpdatedValue & " " & UpdatedField & " has been updated from Accounting Update"

        SendMail.SendMail(ApplicationSettings.EmailInfo, ApplicationSettings.FinanceEmail, MsgBody, strsubject, False, "", False)
    End Sub
    Public Shared Sub ReSendEmail(ByVal UserName As String, ByVal Name As String, ByVal URLToRedirect As String)
        Try
            CommonFunctions.createLog("ReSendEmail")
            Dim strsubject As String
            strsubject = "OrderWork - Account Status"
            Dim MsgBody As String
            'Get content from txt file to get email content.
            Dim WebRequest As WebRequest
            Dim WebResponse As WebResponse
            Dim sr As StreamReader
            Dim Buffer As String
            'WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "AccountRegistration.txt")
            WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "ResendEmail.htm")
            WebResponse = WebRequest.GetResponse()
            sr = New StreamReader(WebResponse.GetResponseStream())
            Buffer = sr.ReadToEnd
            MsgBody = Buffer
            sr.Close()
            MsgBody = MsgBody.Replace("<name>", Name)
            MsgBody = MsgBody.Replace("<loginurl>", URLToRedirect)
            CommonFunctions.createLog("OWAdmin - ReSend Email")
            SendMail.SendMail(ApplicationSettings.EmailInfo(), UserName, MsgBody, strsubject, False, "", True, False, "")
            CommonFunctions.createLog("OWAdmin - ReSend Email Success")
        Catch ex As Exception
            CommonFunctions.createLog("ReSendEmail exception: " + ex.ToString())
        End Try
    End Sub

    'OM-125 : SendMail
    Public Shared Function SendOutsourceWOCompleteEmail(ByVal WorkOrderID As String, ByVal WOTitle As String, ByVal clientrefno As String, ByVal aptdate As String, ByVal ponumber As String, ByVal apttime As String, ByVal location As String) As Boolean
        CommonFunctions.createLog("SendOutsourceWOCompleteEmail")
        Dim Success As Boolean = False
        Dim strsubject As String
        strsubject = "Orderwork Workorder - " & WorkOrderID & " has now been marked as completed"
        Dim MsgBody As String
        'Get content from txt file to get email content.
        Dim WebRequest As WebRequest
        Dim WebResponse As WebResponse
        Dim sr As StreamReader
        Dim Buffer As String
        Dim aptdatetime As String = ""
        aptdatetime = aptdate + " - " + apttime
        WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "OutsourceWOCompleteNotification.htm")
        WebResponse = WebRequest.GetResponse()
        sr = New StreamReader(WebResponse.GetResponseStream())
        Buffer = sr.ReadToEnd
        MsgBody = Buffer
        sr.Close()
        MsgBody = MsgBody.Replace("<WorkordeId>", WorkOrderID)
        MsgBody = MsgBody.Replace("<wotitle>", WOTitle)
        MsgBody = MsgBody.Replace("<clientrefno>", clientrefno)
        MsgBody = MsgBody.Replace("<ponumber>", ponumber)
        MsgBody = MsgBody.Replace("<aptdate>", aptdatetime)
        MsgBody = MsgBody.Replace("<location>", location)

        'Success = SendMail.SendMail(ApplicationSettings.EmailInfo() , "Admin" + " <" + ApplicationSettings.EmailSoftwareAdmin + ">", MsgBody, strsubject)
        SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.OutsourceWOCompleteNotificationTO, MsgBody, strsubject, False, "", True, True, ApplicationSettings.OutsourceWOCompleteNotificationBCC)
        'Check whether mail to be send to admin
        Return Success
    End Function

    Public Shared Function SendInvalidLatLongEmail(ByVal Postcode As String, ByVal RecordType As String, ByVal ID As String) As Boolean
        CommonFunctions.createLog("SendInvalidLatLongEmail")
        Dim Success As Boolean = False
        Dim strsubject As String = "Orderwork � Incomplete location coordinates"
        Dim WebRequest As WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "InvalidLatLongEmail.htm")
        Dim WebResponse As WebResponse = WebRequest.GetResponse()
        Dim sr As StreamReader = New StreamReader(WebResponse.GetResponseStream())
        Dim Buffer As String = sr.ReadToEnd
        Dim MsgBody As String = Buffer

        sr.Close()

        If RecordType = "workorder" Then
            MsgBody = MsgBody.Replace("<iswoorlocation>", "workorder")
        Else
            MsgBody = MsgBody.Replace("<iswoorlocation>", "location")
        End If
        MsgBody = MsgBody.Replace("<postcode>", Postcode)
        MsgBody = MsgBody.Replace("<recordtype>", RecordType)
        MsgBody = MsgBody.Replace("<id>", ID)
        CommonFunctions.createLog("Orderwork � Sending email for Incomplete location coordinates for :" + Postcode + "-" + ID)
        SendMail.SendMail(ApplicationSettings.EmailInfo(), ApplicationSettings.GetAddressUsageAlertEmail, MsgBody, strsubject, False, "", True, False, "")
        CommonFunctions.createLog("Orderwork � Sent email for Incomplete location coordinates for :" + Postcode)
        'Check whether mail to be send to admin
        Return Success
    End Function
    Public Shared Function Send7dayPortalpaymentalert(ByVal WOID As String, ByVal ClientCompanyName As String, ByVal SupplierCompanyName As String, ByVal CompletedDate As String) As Boolean
        Try
            CommonFunctions.createLog("Send7dayPortalpaymentalert")
            Dim Success As Boolean = False
            Dim strsubject As String = "Orderwork - Empowered Workorder with 7-day Payment Terms"
            Dim WebRequest As WebRequest = HttpWebRequest.Create(ApplicationSettings.MailerFolder & "/" & "EmpoweredWorkorderwithPaymentAlert.htm")
            Dim WebResponse As WebResponse = WebRequest.GetResponse()
            Dim sr As StreamReader = New StreamReader(WebResponse.GetResponseStream())
            Dim Buffer As String = sr.ReadToEnd
            Dim MsgBody As String = Buffer

            sr.Close()

            MsgBody = MsgBody.Replace("<woid>", WOID)
            MsgBody = MsgBody.Replace("<client>", ClientCompanyName)
            MsgBody = MsgBody.Replace("<supplier>", SupplierCompanyName)
            MsgBody = MsgBody.Replace("<completeddate>", CompletedDate)
            CommonFunctions.createLog("Orderwork - Empowered Workorder with 7-day Payment Terms for :" + WOID)
            SendMail.SendMail(ApplicationSettings.EmailPortalAdmin, ApplicationSettings.FinanceEmail, MsgBody, strsubject, False, "", True, False, "")
            CommonFunctions.createLog("Orderwork - Empowered Workorder with 7-day Payment Terms email sent successfully for :" + WOID)
            Return Success
        Catch ex As Exception
            CommonFunctions.createLog(ex.ToString())
        End Try
    End Function
    Public Shared Sub SendAutoAcceptWOEmailToClient(ByVal clientSubject As String, ByVal clientMailbody As String, ByVal clientRecipients As String, ByVal clientCCList As String)
        Try
            CommonFunctions.createLog("SendAutoAcceptWOEmailToClient")
            SendMail.SendMail(ApplicationSettings.EmailInfo(), clientRecipients, clientMailbody, clientSubject, True, clientCCList, True, False, "")
        Catch ex As Exception
            CommonFunctions.createLog(ex.ToString())
        End Try
    End Sub
    Public Shared Sub SendAutoAcceptWOEmailToSP(ByVal SPSubject As String, ByVal SPMailbody As String, ByVal SPRecipients As String, ByVal SPCCList As String)
        Try
            CommonFunctions.createLog("SendAutoAcceptWOEmailToSP")
            SendMail.SendMail(ApplicationSettings.EmailInfo(), SPRecipients, SPMailbody, SPSubject, True, SPCCList, True, False, "")
        Catch ex As Exception
            CommonFunctions.createLog(ex.ToString())
        End Try
    End Sub
End Class
