﻿Imports Microsoft.WindowsAzure.Storage
Imports Microsoft.WindowsAzure.Storage.File
Imports System
Imports System.IO
Public Class AzureFileUpload
    Public Shared Sub UploadFileThroughFileDir(ByVal filelocation As String, ByVal newfileName As String, ByVal uploadfilelocation As String)
        Dim storageAccount As CloudStorageAccount = CloudStorageAccount.Parse(ApplicationSettings.AzureStorageConnectionString)
        Dim fileClient As CloudFileClient = storageAccount.CreateCloudFileClient()
        Dim share As CloudFileShare = fileClient.GetShareReference("attachments")
        Dim localPath As String = "D:\home\site\wwwroot\Attachments\"
        Dim sourceFile As String = ""
        Try
            If share.Exists() Then
                Dim rootDir As CloudFileDirectory = share.GetRootDirectoryReference()
                Dim sampleDir As CloudFileDirectory = rootDir.GetDirectoryReference(uploadfilelocation)

                If sampleDir.Exists() Then
                    Dim file As CloudFile = sampleDir.GetFileReference(newfileName)

                    If file.Exists() Then
                    End If


                    sourceFile = Path.Combine(localPath, newfileName)

                    Using fileStream = IO.File.OpenRead(sourceFile)
                        file.UploadFromStream(CType(fileStream, Stream))
                    End Using
                End If

            End If
        Catch ex As Exception

        Finally
            If File.Exists(sourceFile) Then
                File.Delete(sourceFile)
            End If
        End Try
    End Sub
End Class