
''' <summary>
''' Class provides shared getString() method to access resource text from site resource file or library resource file
''' </summary>
''' <author>Chandrashekhar Muradnar</author>
''' <remarks></remarks>
Public Class EmailContents
    ''' <summary>
    ''' this is the shared handler for library de site
    ''' </summary>
    ''' <remarks></remarks>    
    Private Shared _resourceManagerLibraryDE As System.Resources.ResourceManager

    ''' <summary>
    ''' this is the shared handler for site resources
    ''' </summary>
    ''' <remarks></remarks>    
    Private Shared _resourceManagerSite As System.Resources.ResourceManager

    ''' <summary>
    ''' this is the shared handler for library resources
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _resourceManagerLibrary As System.Resources.ResourceManager

    ''' <summary>
    ''' private constructor... no instance of this class is allowed
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ResourceMessageText()

    End Sub
    ''' <summary>
    ''' Initialize resources when application starts..
    ''' this function is called at Application_Start event in global.aspx file
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub InitializeResources(Optional ByVal paramResourceClassName As String = "MessageText")

        ' Site Resource Handler
        Dim resourceAssemblySite As Reflection.Assembly = Reflection.Assembly.GetCallingAssembly
        _resourceManagerSite = New System.Resources.ResourceManager(resourceAssemblySite.GetName().Name & "." & paramResourceClassName, resourceAssemblySite)
        _resourceManagerSite.IgnoreCase = True

        Dim resourceAssemblyLibrary As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly

        'Library Resource Handler
        Dim libraryResourceClassName As String = ""
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryUK
                libraryResourceClassName = "EmailContentsUK"
            Case ApplicationSettings.CountryDE
                If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                    libraryResourceClassName = "EmailContentsUK"
                    _resourceManagerLibraryDE = New System.Resources.ResourceManager(resourceAssemblyLibrary.GetName().Name & ".EmailContentsDE", resourceAssemblyLibrary)
                    _resourceManagerLibraryDE.IgnoreCase = True
                Else
                    libraryResourceClassName = "EmailContentsDE"
                End If

        End Select

        _resourceManagerLibrary = New System.Resources.ResourceManager(resourceAssemblyLibrary.GetName().Name & "." & libraryResourceClassName, resourceAssemblyLibrary)
        _resourceManagerLibrary.IgnoreCase = True
    End Sub
    ''' <summary>
    ''' static function returning value for the provided  key
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetString(ByVal key As String, Optional ByVal from As ApplicationSettings.MessageSource = ApplicationSettings.MessageSource.library) As String
        Try
            Dim s As String = ""
            Select Case from
                'the library UK is used as default for ow site, sf site, admin uk, admin de
                'in some cases ow site and sf site use site specific file (5%)
                'in some cases admin de site uses the library de file, hence u need to pass the messagesource as library de
                'the library DE is used as default for de site
                Case ApplicationSettings.MessageSource.library
                    s = _resourceManagerLibrary.GetString(key)
                Case ApplicationSettings.MessageSource.site
                    s = _resourceManagerSite.GetString(key)
                Case ApplicationSettings.MessageSource.libraryde
                    If ApplicationSettings.Country = ApplicationSettings.CountryDE And ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                        s = _resourceManagerLibraryDE.GetString(key)
                    Else
                        s = _resourceManagerLibrary.GetString(key)
                    End If
            End Select

            If ("" = s) Then
                Throw (New Exception())
            End If

            Return s
        Catch ex As Exception
            Return String.Format("[?:{0}]", key)
        End Try
    End Function
End Class

