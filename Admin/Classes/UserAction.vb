Imports System
Imports System.IO
Imports System.Data
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.Security
Imports System.Security.Principal
Imports System.Runtime.InteropServices
Imports System.Diagnostics

'Option Strict On

''' <summary>
''' Class to have methods to respond to user action as logout
''' </summary>
''' <remarks></remarks>
Public Class UserAction
    Shared ws As New WSObjs

    ''' <summary>
    ''' Method to be called when user logout the system
    ''' </summary>
    ''' <param name="Page"></param>
    ''' <remarks></remarks>
    Public Shared Sub userLogout(ByVal Page As Page)
        Security.SecurePageSSL(Page, False)
        Page.Session.Abandon()
        Page.Session.Clear()

        Page.Cache.Remove("LoggedInfo-" & Page.Session("UserName") & Page.Session.SessionID)

        'Code for securing Dev Site
        If ApplicationSettings.SecureSandbox = 1 Then
            Page.Response.Redirect("~/Login.aspx?login=true")
        Else
            Page.Response.Redirect("~/Home.aspx")
            'Page.Response.Redirect("~/securelogin.aspx")
        End If
    End Sub

    ''' <summary>
    ''' Method to be called for welcome 
    ''' </summary>
    ''' <param name="Page"></param>
    ''' <param name="postBack"></param>
    ''' <remarks></remarks>
    Public Shared Sub userWelcome(ByVal Page, ByVal postBack)
        'Put user code to initialize the page here
        'Page.Response.Cache.SetNoStore()
        'Open an secure socket using SSL
        Security.SecurePage(Page)
        If Page.Session("NoAccess") = True Then
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryUK
                    Page.FindControl("lblMsg").text = "You do not have access to this page."

                Case ApplicationSettings.CountryDE
                    Page.FindControl("lblMsg").text = "Sie haben nicht Zugang zu dieser Seite."
            End Select
        Else
            Page.FindControl("lblMsg").text = ""
        End If

        If postBack = True Then
            Dim dsChkUser As DataSet = ws.WSContact.GetAttributesForContact(Page.Session("CompanyID"), "CompanyStrength")
            If dsChkUser.Tables(0).Rows.Count <> 0 Then
                If dsChkUser.Tables(0).Rows(0).Item("AttributeValue") = ApplicationSettings.ContractorSelfEmployed Then
                    Page.FindControl("lnkAddUsers").Visible = False
                End If
            End If

            Exit Sub
        End If

        'Try
        '    Security.GetPageControls(Page)
        'Catch ex As Exception
        'End Try

    End Sub

    Public Shared Sub userAutoLoginAccept(ByVal Page)

        If Not (Page.Request.Form("userName") Is Nothing And Page.Request.Form("password") Is Nothing) Then
            Dim ds As New DataSet
            'Fetch Dataset from authentication user SP which will contain LoginInfo, Menu Table, Child Menu table and privilege table
            ds = ws.WSSecurity.AuthenticateUser(Page.Request.Form("userName"), Page.Request.Form("password"), ApplicationSettings.BizDivId, ApplicationSettings.BusinessId)


            'If user does not exit then login failed
            If ds.Tables(0).Rows.Count > 0 Then
                'Maintain session for the following with the value as in dataset
                Page.Session("CompanyId") = ds.Tables(0).Rows(0).Item("CompanyId")
                Page.Session("CompanyName") = ds.Tables(0).Rows(0).Item("CompanyName")
                Page.Session("UserId") = ds.Tables(0).Rows(0).Item("UserID")
                Page.Session("UserName") = Page.Request.Form("userName")
                Page.Session("FirstName") = ds.Tables(0).Rows(0).Item("FName")
                Page.Session("LastName") = ds.Tables(0).Rows(0).Item("LName")
                Page.Session("SecQues") = ds.Tables(0).Rows(0).Item("SecurityQues")
                Page.Session("SecAns") = ds.Tables(0).Rows(0).Item("SecurityAns")
                Page.Session("Password") = ds.Tables(0).Rows(0).Item("Password")
                If Not IsDBNull(ds.Tables(0).Rows(0).Item("AcceptWOValueLimit")) Then
                    Page.Session("AcceptWOValueLimit") = ds.Tables(0).Rows(0).Item("AcceptWOValueLimit")
                End If
                If Not IsDBNull(ds.Tables(0).Rows(0).Item("SpendLimit")) Then
                    Page.Session("SpendLimit") = ds.Tables(0).Rows(0).Item("SpendLimit")
                End If
                'Update tblContactsLogin table to store recent login information
                ws.WSSecurity.UpdateLoginDate(Page.Session("UserId"))
                Page.Cache.Remove("LoggedInfo-" & Page.Session("UserName") & Page.Session.SessionID)
                'Page.Cache.Add("LoggedInfo-" & Session("UserName") & Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
                SyncLock (GetType(UCLogin))
                    Dim objPageClass As New PageClass
                    Page.Cache.Insert("LoggedInfo-" & Page.Session("UserName") & Page.Session.SessionID, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf objPageClass.RemoveCallback))
                End SyncLock
                Select Case ApplicationSettings.SiteType
                    Case ApplicationSettings.siteTypes.admin
                        'Set the BizDivId in the session for Admin
                        Select Case ApplicationSettings.Country
                            Case ApplicationSettings.CountryUK
                                Page.Session("BizDivId") = ApplicationSettings.OWUKBizDivId
                            Case ApplicationSettings.CountryDE
                        End Select
                        Page.Session("RoleGroupID") = ds.Tables(0).Rows(0).Item("RoleGroupID")
                    Case ApplicationSettings.siteTypes.site
                        Page.Session("RoleGroupID") = ds.Tables(0).Rows(0).Item("RoleGroupID")
                        Page.Session("ContactClassID") = ds.Tables(0).Rows(0).Item("ClassID")
                        If ds.Tables(0).Rows.Count > 1 Then
                            Page.Session("SecondaryClassID") = ds.Tables(0).Rows(1).Item("ClassID")
                        End If
                        Page.Session("SubClassID") = ds.Tables(0).Rows(0).Item("Status")
                End Select
            End If
        End If

    End Sub

   
    'Public Sub openAttachment(ByVal Page)

    '    Dim inp As IO.FileStream
    '    Try


    '        Dim context As WindowsImpersonationContext
    '        Try
    '            context = Impersonation.StartImpersonatingUser(ApplicationSettings.adSystem.Configuration.ConfigurationSettings.AppSettings("AdminUser"), System.Configuration.ConfigurationSettings.AppSettings("AdminDomain"), System.Configuration.ConfigurationSettings.AppSettings("AdminPassword"))
    '        Catch
    '        End Try



    '        Dim FilePath As String = Server.MapPath(Request.QueryString("path"))




    '        Dim info As IO.FileInfo = New IO.FileInfo(FilePath)
    '        If Not info.Exists Then
    '            Exit Sub
    '        End If

    '        Dim b() As Byte = Array.CreateInstance(GetType(Byte), info.Length)
    '        inp = info.OpenRead()
    '        inp.Read(b, 0, info.Length)
    '        Response.Buffer = False
    '        Dim ext As String = FilePath.Split(".")(FilePath.Split(".").Length - 1)

    '        Dim tempFilePath As String = FilePath.Replace("\", "/")
    '        Dim attachFilename As String = tempFilePath.Split("/")(tempFilePath.Split("/").Length - 1)

    '        If LCase(ext) = "pdf" Then

    '            Response.Expires = 0
    '            Response.ContentType = "application/pdf"
    '            Response.AddHeader("Content-Length", info.Length.ToString())
    '        ElseIf LCase(ext) = "xls" Then
    '            Response.Expires = 0
    '            Response.ContentType = "application/vnd.ms-excel"
    '            Response.AddHeader("content-disposition", "attachment; filename=Temp.xls")
    '        ElseIf LCase(ext) = "doc" Then
    '            Response.ContentType = "application/vnd.ms-word"
    '            Response.AddHeader("content-disposition", "attachment; filename=Temp.doc")
    '        ElseIf LCase(ext) = "bmp" Then
    '            Response.ContentType = "image/bmp"
    '            Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
    '        ElseIf LCase(ext) = "gif" Then
    '            Response.ContentType = "image/gif"
    '            Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
    '        ElseIf LCase(ext) = "jpe" Or LCase(ext) = "jpeg" Or LCase(ext) = "jpg" Then
    '            Response.ContentType = "image/jpeg"
    '            Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
    '        ElseIf LCase(ext) = "tif" Or LCase(ext) = "tiff" Then
    '            Response.ContentType = "image/tiff"
    '            Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
    '        ElseIf LCase(ext) = "png" Then
    '            Response.ContentType = "image/png"
    '            Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
    '        ElseIf LCase(ext) = "jfif" Then
    '            Response.ContentType = "image/pjpeg"
    '            Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
    '        Else
    '            Response.ContentType = "application/x-unknown"
    '            Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
    '        End If
    '        Response.Charset = ""
    '        Response.BinaryWrite(b)

    '        Response.Flush()
    '        Response.End()
    '        Try
    '            Impersonation.StopImpersonatingUser(context)
    '        Catch
    '        End Try


    '    Catch ex As Exception
    '        Try
    '            inp.Close()
    '        Catch
    '        End Try
    '        Throw ex
    '    End Try
    'End Sub



End Class
Public Class PageClass
    Inherits System.Web.UI.Page
    Public Sub RemoveCallback(ByVal tag As String, ByVal obj As Object, ByVal reason As CacheItemRemovedReason)
        If (CacheItemRemovedReason.Underused = reason) Then
            HttpContext.Current.Cache.Remove(tag)
            If Not IsNothing(obj) Then
                Cache.Insert(tag, obj, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            End If
        Else
            HttpContext.Current.Cache.Remove(tag)
        End If
    End Sub
End Class



