Public Class ControlMail
    Private Shared _IsMailOnBuyerAcceptToBuyer As Boolean = False
    Public Shared ReadOnly Property IsMailOnBuyerAcceptToBuyer() As Boolean
        Get
            Return _IsMailOnBuyerAcceptToBuyer And ApplicationSettings.SendMailBuyer
        End Get
    End Property
    ''' <summary>
    ''' _IsMailOnBuyerAcceptToAdmin
    ''' </summary>
    ''' <remarks></remarks>

    Private Shared _IsMailOnBuyerAcceptToAdmin As Boolean = False
    Public Shared ReadOnly Property IsMailOnBuyerAcceptToAdmin() As Boolean
        Get
            Return _IsMailOnBuyerAcceptToAdmin And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' _IsMailOnLostWOToAdmin
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnLostWOToAdmin As Boolean = False
    Public Shared ReadOnly Property IsMailOnLostWOToAdmin() As Boolean
        Get
            Return _IsMailOnLostWOToAdmin And ApplicationSettings.SendMailAdmin
        End Get
    End Property

    ''' <summary>
    ''' _IsMailCAToAdmin
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailCAToAdmin As Boolean = False
    Public Shared ReadOnly Property IsMailCAToAdmin() As Boolean
        Get
            Return _IsMailCAToAdmin And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' _IsMailOnLostWOToSP
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnLostWOToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnLostWOToSP() As Boolean
        Get
            Return _IsMailOnLostWOToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on submit of WO
    ''' </summary>
    ''' <remarks></remarks>

    Private Shared _IsMailOnSubmitWO As Boolean = True
    Public Shared ReadOnly Property IsMailOnSubmitWO() As Boolean
        Get
            Return _IsMailOnSubmitWO And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Buyer on submit of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSubmitWOToBuyer As Boolean = True
    Public Shared ReadOnly Property IsMailOnSubmitWOToBuyer() As Boolean
        Get
            Return _IsMailOnSubmitWOToBuyer And ApplicationSettings.SendMailBuyer
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on SPAccept of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPAccept As Boolean = False
    Public Shared ReadOnly Property IsMailOnSPAccept() As Boolean
        Get
            Return _IsMailOnSPAccept And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on SPAccept of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPAcceptToSP As Boolean = False
    Public Shared ReadOnly Property IsMailOnSPAcceptToSP() As Boolean
        Get
            Return _IsMailOnSPAcceptToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on SPAccept of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPAcceptToBuyer As Boolean = True
    Public Shared ReadOnly Property IsMailOnSPAcceptToBuyer() As Boolean
        Get
            Return _IsMailOnSPAcceptToBuyer And ApplicationSettings.SendMailBuyer
        End Get
    End Property


    ''' <summary>
    ''' Send mail to Admin on SPCAccept of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPCAccept As Boolean = True
    Public Shared ReadOnly Property IsMailOnSPCAccept() As Boolean
        Get
            Return _IsMailOnSPCAccept And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' IsMailOnSPCAcceptToSP
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPCAcceptToSP As Boolean = False
    Public Shared ReadOnly Property IsMailOnSPCAcceptToSP() As Boolean
        Get
            Return _IsMailOnSPCAcceptToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property

    ''' <summary>
    ''' IsMailOnSPCAcceptToBuyer
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPCAcceptToBuyer As Boolean = False
    Public Shared ReadOnly Property IsMailOnSPCAcceptToBuyer() As Boolean
        Get
            Return _IsMailOnSPCAcceptToBuyer And ApplicationSettings.SendMailBuyer
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on SPChangeCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPChangeCA As Boolean = True
    Public Shared ReadOnly Property IsMailOnSPChangeCA() As Boolean
        Get
            Return _IsMailOnSPChangeCA And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Admin on SPChangeCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnSPChangeCAToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnSPChangeCAToSP() As Boolean
        Get
            Return _IsMailOnSPChangeCAToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on DiscussCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnDiscussCA As Boolean = False
    Public Shared ReadOnly Property IsMailOnDiscussCA() As Boolean
        Get
            Return _IsMailOnDiscussCA And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on DiscussCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnDiscussCAToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnDiscussCAToSP() As Boolean
        Get
            Return _IsMailOnDiscussCAToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property


    ''' <summary>
    ''' Send mail to Admin on AcceptCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnAcceptCA As Boolean = False
    Public Shared ReadOnly Property IsMailOnAcceptCA() As Boolean
        Get
            Return _IsMailOnAcceptCA And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on AcceptCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnAcceptCAToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnAcceptCAToSP() As Boolean
        Get
            Return _IsMailOnAcceptCAToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Buyer on AcceptCA of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnAcceptCAToBuyer As Boolean = True
    Public Shared ReadOnly Property IsMailOnAcceptCAToBuyer() As Boolean
        Get
            Return _IsMailOnAcceptCAToBuyer And ApplicationSettings.SendMailBuyer
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on RaiseIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailRaiseIssue As Boolean = True
    Public Shared ReadOnly Property IsMailRaiseIssue() As Boolean
        Get
            Return _IsMailRaiseIssue And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on RaiseIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailRaiseIssueToSP As Boolean = True
    Public Shared ReadOnly Property IsMailRaiseIssueToSP() As Boolean
        Get
            Return _IsMailRaiseIssueToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on ChangeIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnChangeIssue As Boolean = True
    Public Shared ReadOnly Property IsMailOnChangeIssue() As Boolean
        Get
            Return _IsMailOnChangeIssue And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Admin on ChangeIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnChangeIssueToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnChangeIssueToSP() As Boolean
        Get
            Return _IsMailOnChangeIssueToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on ChangeIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnDiscuss As Boolean = True
    Public Shared ReadOnly Property IsMailOnDiscuss() As Boolean
        Get
            Return _IsMailOnDiscuss And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on ChangeIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnDiscussToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnDiscussToSP() As Boolean
        Get
            Return _IsMailOnDiscussToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property


    ''' <summary>
    ''' Send mail to Admin on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnAcceptIssue As Boolean = True
    Public Shared ReadOnly Property IsMailOnAcceptIssue() As Boolean
        Get
            Return _IsMailOnAcceptIssue And ApplicationSettings.SendMailAdmin
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Supplier on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnAcceptIssueToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnAcceptIssueToSP() As Boolean
        Get
            Return _IsMailOnAcceptIssueToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Admin on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnComplete As Boolean = False
    Public Shared ReadOnly Property IsMailOnComplete() As Boolean
        Get
            Return _IsMailOnComplete And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Supplier on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnCompleteToSP As Boolean = False
    Public Shared ReadOnly Property IsMailOnCompleteToSP() As Boolean
        Get
            Return _IsMailOnCompleteToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Buyer on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnCompleteToBuyer As Boolean = False
    Public Shared ReadOnly Property IsMailOnCompleteToBuyer() As Boolean
        Get
            Return _IsMailOnCompleteToBuyer And ApplicationSettings.SendMailBuyer
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on Close of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnClose As Boolean = False
    Public Shared ReadOnly Property IsMailOnClose() As Boolean
        Get
            Return _IsMailOnClose And ApplicationSettings.SendMailAdmin
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Admin on Close of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnCloseToSP As Boolean = False
    Public Shared ReadOnly Property IsMailOnCloseToSP() As Boolean
        Get
            Return _IsMailOnCloseToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property ''' <summary>
    ''' Send mail to Admin on Close of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnCloseToBuyer As Boolean = False
    Public Shared ReadOnly Property IsMailOnCloseToBuyer() As Boolean
        Get
            Return _IsMailOnCloseToBuyer And ApplicationSettings.SendMailBuyer
        End Get
    End Property


    ''' <summary>
    ''' Send mail to Admin on Close of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailWithdrawFunds As Boolean = False
    Public Shared ReadOnly Property IsMailWithdrawFunds() As Boolean
        Get
            Return _IsMailWithdrawFunds And ApplicationSettings.SendMailAdmin
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Admin on Cancel of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnCancelToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnCancelToSP() As Boolean
        Get
            Return _IsMailOnCancelToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property ''' <summary>
    ''' Send mail to Admin on Cancel of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnCancelToBuyer As Boolean = False
    Public Shared ReadOnly Property IsMailOnCancelToBuyer() As Boolean
        Get
            Return _IsMailOnCancelToBuyer And ApplicationSettings.SendMailBuyer
        End Get
    End Property
    ''' Send mail to Admin on FundsProcess 
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnFundsProcessToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnFundsProcessToSP() As Boolean
        Get
            Return _IsMailOnFundsProcessToSP And ApplicationSettings.SendMailBuyer
        End Get
    End Property
    ''' Send mail to supplier on Payment Available 
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnPaymentAvailableToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnPaymentAvailableToSP() As Boolean
        Get
            Return _IsMailOnPaymentAvailableToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property
    ''' <summary>
    ''' Send mail to Admin on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnRejectIssue As Boolean = False
    Public Shared ReadOnly Property IsMailOnRejectIssue() As Boolean
        Get
            Return _IsMailOnRejectIssue And ApplicationSettings.SendMailAdmin
        End Get
    End Property

    ''' <summary>
    ''' Send mail to Supplier on AcceptIssue of WO
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared _IsMailOnRejectIssueToSP As Boolean = True
    Public Shared ReadOnly Property IsMailOnRejectIssueToSP() As Boolean
        Get
            Return _IsMailOnRejectIssueToSP And ApplicationSettings.SendMailSupplier
        End Get
    End Property
End Class
