'------------------------------------------------------------------------------
''' <Summary>
'''   <ProjectName>Orderwork</ProjectName>
'''   <FileName>Security.vb</FileName>
'''   <Module>Security Mail</Module>
'''   <Description> Contains Function related to security as get security question of user ..etc </Description>
'''   <CreatedDate>1/03/2007</CreatedDate>
'''  <References>
'''        
'''   </References>
''' </Summary>
'------------------------------------------------------------------------------

Imports System.Web

''' <summary>
''' Security Class implements the security for the project. This is page level, control priveliges security.
''' </summary>
''' <remarks></remarks>
Public Class Security

    ''' <summary>
    ''' Shared WSobject to access web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs()

    ''' <summary>
    ''' Function to process Use of SSL
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="enableSSL"></param>
    ''' <param name="SSLSecure"></param>
    ''' <param name="RedirectLink"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SecurePageSSL(ByVal page As Page, ByVal enableSSL As Integer, Optional ByVal SSLSecure As Boolean = True, Optional ByVal RedirectLink As String = "")
        'Check if enableSSL is enables via web.config file, if not then skip the process
        If enableSSL = 1 Then
            Dim redirectSSLURL As String
            Dim temp As String
            If RedirectLink = "" Then
                temp = LCase(CType(page.Request.Url, System.Uri).AbsoluteUri)
            Else
                temp = RedirectLink
            End If
            'Replace the url with secure HTTPS
            If SSLSecure = True Then
                If temp.IndexOf("https://") < 0 Then
                    redirectSSLURL = temp.Replace("http://", "https://")
                    page.Response.Redirect(redirectSSLURL)
                End If
            Else
                If temp.IndexOf("http://") < 0 Then
                    redirectSSLURL = temp.Replace("https://", "http://")
                    page.Response.Redirect(redirectSSLURL)
                End If
            End If

        End If
    End Function


    ''' <summary>
    ''' Function to secure access to each pages of the system
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="SSLSecure"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Sub SecurePage(ByVal page As Page, Optional ByVal SSLSecure As Boolean = True)
        Dim RedirectLink As String = ""

        If IsNothing(CType(page.Cache("LoggedInfo-" & page.Session("UserName") & page.Session.SessionID), DataSet)) Then
            'If Cache has expired then throw user out of the system
            page.Session("SessionExpired") = True
            Select Case ApplicationSettings.SiteType
                Case ApplicationSettings.siteTypes.admin
                    Dim Url As String = page.Request.RawUrl
                    Dim query As String = Url.Substring(Url.LastIndexOf("/") + 1)
                    RedirectLink = ApplicationSettings.WebPath & "Login.aspx?preURL=" & query
                Case ApplicationSettings.siteTypes.site
                    RedirectLink = ApplicationSettings.WebPath & "Home.aspx"
            End Select
            page.Response.Redirect(RedirectLink)
        End If

        Dim NoAccess As Boolean = True

        'Get Pagename from requesting url
        Dim filepath As String = page.Request.Url.PathAndQuery.ToLower
        Dim filepathNoQry As String = page.Request.FilePath.ToLower
        Dim pagewithqry, pagenoqry As String
        pagewithqry = filepath.Substring(filepathNoQry.LastIndexOf("/") + 1)
        pagenoqry = filepathNoQry.Substring(filepathNoQry.LastIndexOf("/") + 1)

        Dim ds As DataSet = CType(page.Cache("LoggedInfo-" & page.Session("UserName") & page.Session.SessionID), DataSet)

        'Creating a copy of table "Menu"
        Dim dv As DataView = ds.Tables(1).Copy.DefaultView
        dv.RowFilter = "MenuLink = '" & pagewithqry & "' or MenuLink = '" & pagenoqry & "'"
        'Creating a copy of table "ChildMenu"
        Dim dvSub As DataView = ds.Tables(2).Copy.DefaultView
        dvSub.RowFilter = "MenuLink = '" & pagewithqry & "' or MenuLink = '" & pagenoqry & "'"

        If dv.Count <> 0 Or dvSub.Count <> 0 Then
            NoAccess = False
        End If

        If NoAccess = True Then
            page.Session("NoAccess") = True
            If ds.Tables(0).Rows(0).Item("ClassID") = ApplicationSettings.RoleSupplierID Then
                RedirectLink = ApplicationSettings.WebPath & "SecurePages/Welcome.aspx"
            End If
            If ds.Tables(0).Rows(0).Item("ClassID") = ApplicationSettings.RoleClientID Then
                RedirectLink = ApplicationSettings.WebPath & "SecurePages/Welcome.aspx"
            End If
        End If

        If ApplicationSettings.enableSSL = 1 Then
            If RedirectLink = "" Then
                RedirectLink = page.Request.Url.AbsoluteUri.ToLower
            End If
            SecurePageSSL(page, ApplicationSettings.enableSSL, SSLSecure, RedirectLink)
        Else
            If RedirectLink <> "" Then
                page.Response.Redirect(RedirectLink)
            End If
        End If

        Dim dvpriv As DataView = ds.Tables(3).DefaultView
        dvpriv.RowFilter = "PageName Like('" & pagenoqry & "') "
        If dvpriv.Count <> 0 Then
            GetPageControls(page, dvpriv)
        End If
    End Sub

    ''' <summary>
    ''' To provide security to the page controls
    ''' </summary>
    ''' <param name="page"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Sub GetPageControls(ByVal page As Page, ByVal dvpriv As DataView)
        Dim drv As DataRowView
        For Each drv In dvpriv
            Try
                If drv.Item("ControlName").IndexOf(".") = -1 Then
                    If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                        page.FindControl(drv.Item("ControlName")).Visible = CType(drv.Item("ControlPropertyValue"), Boolean)
                    End If
                    If CStr(drv.Item("ControlPropertyLabel")).ToLower = "enabled" Then
                        CType(page.FindControl(drv.Item("ControlName")), WebControl).Enabled = CType(drv.Item("ControlPropertyValue"), String)
                    End If
                Else
                    Dim obj() As String = Split(drv.Item("ControlName"), ".")

                    If page.FindControl((obj(0).ToString)).GetType.ToString.ToLower = "system.web.ui.webcontrols.gridview" Then
                        If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                            CType(page.FindControl(obj(0).ToString), GridView).Columns(CInt(obj(1))).Visible = CType(drv.Item("ControlPropertyValue"), String)
                        End If
                    ElseIf page.FindControl((obj(0).ToString)).TemplateSourceDirectory.ToLower.IndexOf("usercontrols") <> -1 Then
                        If obj.Length = 2 Then
                            If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                                CType(page.FindControl(obj(0)), UserControl).FindControl(obj(1)).Visible = CType(drv.Item("ControlPropertyValue"), String)
                            End If
                            If CStr(drv.Item("ControlPropertyLabel")).ToLower = "enabled" Then
                                CType(CType(page.FindControl(obj(0)), UserControl).FindControl(obj(1)), WebControl).Enabled = CType(drv.Item("ControlPropertyValue"), String)
                            End If
                        Else
                            If CType(page.FindControl(obj(0)), UserControl).FindControl((obj(1).ToString)).GetType.ToString.ToLower = "system.web.ui.webcontrols.gridview" Then
                                If IsNumeric(obj(2)) Then
                                    If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                                        CType(CType(page.FindControl(obj(0)), UserControl).FindControl(obj(1)), GridView).Columns(obj(2)).Visible = CType(drv.Item("ControlPropertyValue"), String)
                                    End If
                                Else
                                    If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                                        CType(CType(page.FindControl(obj(0)), UserControl).FindControl(obj(1)), GridView).FindControl(obj(2)).Visible = CType(drv.Item("ControlPropertyValue"), String)
                                    End If
                                End If
                            ElseIf CType(page.FindControl(obj(0)), UserControl).FindControl((obj(1).ToString)).TemplateSourceDirectory.ToLower.IndexOf("usercontrols") <> -1 Then
                                If CStr(drv.Item("ControlPropertyLabel")).ToLower = "visible" Then
                                    CType(CType(page.FindControl(obj(0)), UserControl).FindControl(obj(1)), UserControl).FindControl(obj(2)).Visible = CType(drv.Item("ControlPropertyValue"), String)
                                End If
                                If CStr(drv.Item("ControlPropertyLabel")).ToLower = "enabled" Then
                                    CType(CType(CType(page.FindControl(obj(0)), UserControl).FindControl(obj(1)), UserControl).FindControl(obj(2)), WebControl).Enabled = CType(drv.Item("ControlPropertyValue"), String)
                                End If
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
            End Try
        Next
    End Sub
    ''' <summary>
    ''' Function to get security question for the user to retrieve forgot password
    ''' </summary>
    ''' <param name="Email"></param>
    ''' <param name="Country"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getSecurityQuestion(ByVal Email As String, ByVal Country As String) As String
        Dim str, question, answer As String

        If Trim(Email) = "" Then
            Select Case Country
                Case "UK"
                    str = "Please enter an email address."
                Case "DE"
                    str = "Tragen Sie bitte ein email address ein."
            End Select
        Else
            Dim ds As DataSet
            'Get dataset of the string
            ds = ws.WSSecurity.GetSecurityQuesAns(Trim(Email))

            If ds.Tables(0).Rows.Count = 0 Then
                'If there is no record return then show message that user does not exist
                Select Case Country
                    Case "UK"
                        str = "We're sorry, this email address does not exist in our records. Please try again"
                    Case "DE"
                        str = "Es tut uns leid, aber dieses email address besteht nicht in unseren Aufzeichnungen. Bitte Versuch wieder"
                End Select
            Else
                'If record exist then return its value as string to the caller
                question = ds.Tables(0).Rows(0).Item("standardvalue")
                answer = ds.Tables(0).Rows(0).Item("SecurityAns")
                str = "success" + "*^*" + question + "*^*" + answer
            End If
        End If
        'Return security question
        Return str
    End Function

    ''' <summary>
    ''' Function to get Roles Groups Name as per Role Category
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="RoleCategory"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetRolesGroups(ByVal page As Page, Optional ByVal RoleCategory As String = "") As DataSet
        Dim ds As DataSet

        If page.Cache("RolesGroups" & page.Session("UserName") & page.Session.SessionID) Is Nothing Then
            ds = ws.WSSecurity.GetRolesGroups(RoleCategory)
            page.Cache.Add("RolesGroups" & page.Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache("RolesGroups" & page.Session.SessionID), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Function to get status of the contact
    ''' </summary>
    ''' <param name="page"></param>
    ''' <param name="RoleGroupID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStatuses(ByVal page As Page, Optional ByVal RoleGroupID As Integer = 0) As DataSet
        Dim ds As DataSet
        If page.Cache("Statuses for : " & RoleGroupID & page.Session("UserName") & page.Session.SessionID) Is Nothing Then
            ds = ws.WSSecurity.GetStatuses(RoleGroupID)
            page.Cache.Add("Statuses for : " & RoleGroupID & page.Session("UserName") & page.Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(page.Cache("Statuses for : " & RoleGroupID & page.Session("UserName") & page.Session.SessionID), DataSet)
        End If
        Return ds
    End Function





End Class
