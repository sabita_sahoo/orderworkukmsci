<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuyerAgedDebtReport.aspx.vb" Inherits="Admin.BuyerAgedDebtReport" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :"%>
    
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
    





	        
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
						<td>&nbsp;</td>
							<td class="HeadingRed" ><strong>Client Statement of Account</strong></td>
						</tr>
					  </table>
			      <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
			      <ContentTemplate>

					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					  	<tr >
							<td width="10"></td>
							<td width="220" valign="top" >
					    		<table width="100%" cellpadding="0" cellspacing="0" border="0">
					    			   <tr>
										 <td class="formTxt" valign="top" height="24"><asp:Label ID="ErrLabel" runat="server" Visible="false"></asp:Label></td>
									   </tr>
									   <tr>
										 <td class="formTxt" valign="top" height="20">Select Company</td>
									   </tr>

									   <tr >
										 <td >
												<%@ register src="~/UserControls/Admin/UCSearchContact.ascx" tagname="UCSearchContact" tagprefix="uc1" %>
												<uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact> 
										 </td>
									   </tr>
								</table>
							</td>
							<td width="20"></td>
							 <td align="left" width="70" valign="top" >
								<TABLE cellSpacing="0" cellPadding="0" width="70" border="0" >
								<tr>&nbsp;</tr>
									<TR>
										<TD valign="top" class="formTxt" height="20" >Company No.</TD>
									</TR>
		
									<TR>
										<TD valign="top">
											<asp:TextBox ID="txtCompanyNo" runat="server" class="formTxt" ></asp:TextBox>
										</TD>
									</TR>
								</TABLE>
							 </td>
							<td width="20"></td>
							<td align="left"  style="padding-top:30px; " >
								<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                    <asp:LinkButton ID="lnkBtnGenerateReport" causesValidation=True runat="server" CssClass="txtButtonRed" Text="Generate Report"></asp:LinkButton>
                                </div>
							 </td>
							 <td width="60" align="left" ></td>
						</tr>
					</table>
				  </ContentTemplate>
					<Triggers>
						<asp:PostBackTrigger   ControlID="lnkBtnGenerateReport"   /> 
				    </Triggers>
					 </asp:UpdatePanel>
					  <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
						<ProgressTemplate >
				   <div class="gridText">
						<img  align=middle src="Images/indicator.gif" />
						<b>Fetching Data... Please Wait</b>
					</div>     
				</ProgressTemplate>
						</asp:UpdateProgress>
						<cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />					
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
					  <tr>
						<td style="padding-top:20px; ">
							 <rsweb:ReportViewer ID="ReportViewerOW" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="830px" ProcessingMode="Remote" Width="100%" bgColor="#FFFFFF">
									<ServerReport ReportServerUrl="" />
							 </rsweb:ReportViewer>
						</td>
					  </tr>						  
					  </table>
			<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>

</asp:Content>
