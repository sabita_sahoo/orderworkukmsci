﻿
Public Class SignOffSheetAT800
    Inherits System.Web.UI.Page

    Public Shared ws As New WSObjs

    Private Sub WebForm1_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit
        wzd.ActiveStepIndex = 0
        wzd.DisplaySideBar = False
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            OrderWorkLibrary.Security.SecurePage(Page, True)
        Else

            If txtWorkOrderDetails.Text.IndexOf(":") > 0 Then
                Dim IdString As String = Left(txtWorkOrderDetails.Text, txtWorkOrderDetails.Text.IndexOf(":"))
                Select Case IdString
                    Case "1", "2", "3", "4", "5", "6", "7"
                        Installer_Return_Code.Items(4).Enabled = False
                        Installer_Return_Code.Items(5).Enabled = False
                    Case "8", "17"
                        Installer_Return_Code.Items(2).Enabled = False
                        Installer_Return_Code.Items(3).Enabled = False
                        Installer_Return_Code.Items(4).Enabled = False
                        Installer_Return_Code.Items(5).Enabled = False
                    Case "9", "10", "11", "12", "18", "19", "20", "21"
                        Installer_Return_Code.Items(2).Enabled = False
                        Installer_Return_Code.Items(3).Enabled = False
                        'Installer_Return_Code.Items(5).Enabled = False
                    Case "13", "14", "15", "16"
                        Installer_Return_Code.Items(2).Enabled = False
                        Installer_Return_Code.Items(4).Enabled = False
                        Installer_Return_Code.Items(5).Enabled = False
                    Case "22"
                        Installer_Return_Code.Items(2).Enabled = False
                        Installer_Return_Code.Items(3).Enabled = False
                        Installer_Return_Code.Items(4).Enabled = False
                        Installer_Return_Code.Items(5).Enabled = False
                End Select
            End If

            '<asp:ListItem Value="0">Yes</asp:ListItem>
            '<asp:ListItem Value="1">No: Householder not present</asp:ListItem>
            '<asp:ListItem Value="2">No: Specialist installer team required</asp:ListItem>
            '<asp:ListItem Value="3">No: Installer unable to resolve</asp:ListItem>
            '<asp:ListItem Value="4">No: Platform change required</asp:ListItem>
            '<asp:ListItem Value="5">No: Bespoke solution required</asp:ListItem>
            End If
            'OrderWorkLibrary.Security.SecurePage(Page, True)
            '    ''''''''''''''
            If txtPostCode.Text = "" Then txtPostCode.Text = txtPostCode.ToolTip
            If txtWorkOrder.Text = "" Then txtWorkOrder.Text = txtWorkOrder.ToolTip
            If txtInstaller_Notes.Text = "" Then txtInstaller_Notes.Text = txtInstaller_Notes.ToolTip

            txtWorkOrderDetails.Attributes.Add("readonly", "true")

    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function SearchWorkOrder(ByVal txtWorkOrder As String, ByVal txtPostcode As String) As String
        Dim strReturnString As String = ""
        strReturnString = ws.WSWorkOrder.SearchWorkOrder(txtWorkOrder, txtPostcode)
        Return strReturnString
    End Function

    Private Function ValidateWOAndPostCode() As Boolean

        Dim CheckWOAgain As String = ""
        CheckWOAgain = ws.WSWorkOrder.SearchWorkOrder(txtWorkOrder.Text, txtPostCode.Text)
        Dim PageValid As Boolean = True
        If txtWorkOrderDetails.Text <> CheckWOAgain Then
            PageValid = False
            If Page.Validators.Count > 0 Then
                Page.Validators(0).IsValid = False
                Page.Validators(1).IsValid = False
            End If
        End If
        Return PageValid
    End Function
    Private Sub wzd_NextButtonClick(sender As Object, e As System.Web.UI.WebControls.WizardNavigationEventArgs) Handles wzd.NextButtonClick
        Page.Validate()
        Dim PageValid As Boolean = IsValid

        If PageValid Then
            If e.CurrentStepIndex = 0 Then
                PageValid = ValidateWOAndPostCode()
            End If
        End If
        If IsValid = False Then
            e.Cancel = True
            txtWorkOrderDetails.Text = ""
        End If
    End Sub

    Protected Sub wzd_PreRender(sender As Object, e As System.EventArgs) Handles wzd.PreRender
        Dim SideBarList As Repeater = wzd.FindControl("HeaderContainer").FindControl("SideBarList")
        SideBarList.DataSource = wzd.WizardSteps
        SideBarList.DataBind()
    End Sub

    Public Function GetClassForWizardStep(wizardStep As WizardStep) As String
        If IsNothing(wizardStep) Then
            Return ""
        End If
        Dim stepIndex As Integer = wzd.WizardSteps.IndexOf(wizardStep)
        If (stepIndex < wzd.ActiveStepIndex) Then
            Return "StepNoComplete"
        ElseIf (stepIndex > wzd.ActiveStepIndex) Then
            Return "StepNoToDo"
        Else
            Return "StepNoCurrent"
        End If
    End Function

    Protected Sub wzd_FinishButtonClick(sender As Object, e As System.Web.UI.WebControls.WizardNavigationEventArgs) Handles wzd.FinishButtonClick
        Try
            If txtInstaller_Notes.Text = txtInstaller_Notes.ToolTip Then
                txtInstaller_Notes.Text = ""
            End If
            Page.Validate()

            If IsValid Then
                If ValidateWOAndPostCode() Then
                    ws.WSWorkOrder.AT800InsertSignOffSheet(txtWorkOrder.Text, Installer_Return_Code.SelectedValue, Filt_Type.SelectedValue, Filt_Manuf.SelectedValue, ProblemNot4G.Checked, txtInstaller_Notes.Text)
                    litWorkOrder1.Text = txtWorkOrder.Text
                    wzd.Visible = False
                    CompletePanel.Visible = True
                    pnlBackListing.Style.Add("display", "block")
                    'pnlBackListing.Visible = True
                Else
                    wzd.ActiveStepIndex = 0
                End If
            End If

        Catch ex As Exception
            litWorkOrder2.Text = txtWorkOrder.Text
            wzd.Visible = False
            FailedPanel.Visible = True
        End Try

        'Reset the page 
        txtPostCode.Text = ""
        txtWorkOrder.Text = ""
        txtWorkOrderDetails.Text = ""
        Installer_Return_Code.ClearSelection()
        Filt_Manuf.ClearSelection()
        Filt_Type.ClearSelection()
        ProblemNot4G.Checked = False
        txtInstaller_Notes.Text = ""
    End Sub


    Private Sub btnHome_Click(sender As Object, e As System.EventArgs) Handles btnHome.Click
        Response.Redirect("SignoffSheetAT800.aspx")
    End Sub

    Private Sub btnDashBoard_Click(sender As Object, e As System.EventArgs) Handles btnDashBoard.Click
        Response.Redirect("AdminWOListing.aspx?mode=Submitted")
    End Sub
End Class