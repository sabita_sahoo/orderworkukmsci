﻿<%@ Page Title="OrderWork : Contractor" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="ContractorListing.aspx.vb" Inherits="Admin.ContractorListing" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
    
    
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript" language="javascript">
    function onMainCatChange(ddlMainCat) {
        var hdnsubcatval = document.getElementById("ctl00_ContentPlaceHolder1_hdnSubCategoryVal");
        var hdnsubcattxt = document.getElementById("ctl00_ContentPlaceHolder1_hdnSubCategorytxt");
        var subcat = document.getElementById("ctl00_ContentPlaceHolder1_ddlWOSubType");
        hdnsubcatval.value = "";
        hdnsubcattxt.value = "";
        if (document.getElementById(ddlMainCat).value != "") {
            subcat.disabled = false;
            clearOptions(subcat);
            
            populateSubCat(document.getElementById(ddlMainCat).value);
            hdnsubcattxt.value = subcat.options[subcat.selectedIndex].text;
            hdnsubcatval.value = subcat.value;
        }
        else {
            clearOptions(subcat);
            subcat.disabled = true;
        }
    }
    function populateSubCat(ddlMainCatValue) {
        addOption(document.getElementById("ctl00_ContentPlaceHolder1_ddlWOSubType"), "Select Sub Category", "");
        var browserName = navigator.appName;
        xmlDoc = loadXMLDoc("XML/WOFormStandards.xml");
        if (browserName == "Netscape") {
            for (i = 1; i < xmlDoc.childNodes[0].childNodes.length; i++) {

                if (xmlDoc.childNodes[0].childNodes[i].tagName == 'MainCat') {

                    var selectedCombID = xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue;

                    if (xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue == ddlMainCatValue) {
                        for (j = 1; i < xmlDoc.childNodes[0].childNodes.length; j++) {
                            if (xmlDoc.childNodes[0].childNodes[j].tagName == 'SubCat') {

                                if (xmlDoc.childNodes[0].childNodes[j].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue == selectedCombID) {
                                    addOption(document.getElementById("ctl00_ContentPlaceHolder1_ddlWOSubType"), xmlDoc.childNodes[0].childNodes[j].getElementsByTagName("Name")[0].childNodes[0].nodeValue, xmlDoc.childNodes[0].childNodes[j].getElementsByTagName("CombID")[0].childNodes[0].nodeValue);
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (browserName == "Microsoft Internet Explorer") {
       
            for (i = 1; i < xmlDoc.childNodes[1].childNodes.length; i++) {
       
                if (xmlDoc.childNodes[1].childNodes[i].tagName == 'MainCat') {
                    var selectedCombID = xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue;
                    if (xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue == ddlMainCatValue) {
                        for (j = 1; i < xmlDoc.childNodes[1].childNodes.length; j++) {
                            if (xmlDoc.childNodes[1].childNodes[j].tagName == 'SubCat') {

                                if (xmlDoc.childNodes[1].childNodes[j].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue == selectedCombID) {
                                    addOption(document.getElementById("ctl00_ContentPlaceHolder1_ddlWOSubType"), xmlDoc.childNodes[1].childNodes[j].getElementsByTagName("Name")[0].childNodes[0].nodeValue, xmlDoc.childNodes[1].childNodes[j].getElementsByTagName("CombID")[0].childNodes[0].nodeValue);
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    function onSubCatChange(ddlSubCat) {
        var subcat = document.getElementById(ddlSubCat).value;
        var subcattxt = document.getElementById(ddlSubCat).options[document.getElementById(ddlSubCat).selectedIndex].text;
        document.getElementById("ctl00_ContentPlaceHolder1_hdnSubCategoryVal").value = subcat;
        document.getElementById("ctl00_ContentPlaceHolder1_hdnSubCategorytxt").value = subcattxt;
    }


</script>
    <input type="hidden" id="hdnSubCategoryVal" runat="Server" value="" />
<input type="hidden" id="hdnSubCategorytxt" runat="Server" value="" />
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			 
			 <div style="padding:10px;"><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Contractors" runat="server"></asp:Label></div>
			 <table cellpadding="0" cellspacing="0" style="float:left; width:550px">
			<tr>
			<td width="10px" height="30px" valign="bottom"></td>			
            <td height="28" width="350px">
				<asp:Panel ID="pnlWOType" runat="server">
				    <table border="0" cellspacing="0" cellpadding="0">
				        <TR>
				            <td width="165" height="28" valign="top"><asp:DropDownList id="ddlWOType" runat="server" onkeydown='javascript:onMainCatChange(this.id);' onkeyup='javascript:onMainCatChange(this.id);' onchange='javascript:onMainCatChange(this.id);' CssClass="formFieldGrey" Width="160" TabIndex="4" autopostback="false"></asp:DropDownList></td>
				            <td width="163" height="28" valign="top"><asp:DropDownList id="ddlWOSubType"  runat="server" CssClass="formFieldGrey" Width="160" onchange='javascript:onSubCatChange(this.id)' TabIndex="5" ></asp:DropDownList></td>
				        </TR>
				    </table>
				</asp:Panel>
			</td>	
            <td allign="left"><asp:CheckBox ID="chkHideDeleted" Checked="true" runat="server" Text='Hide deleted Contractors' class="formTxt"  AutoPostBack="true" /> </td>
            </tr>
			</table>     
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
             <ContentTemplate>
			<div style="float:left; width:200px;">
                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                    <asp:LinkButton ID="lnkSearch" causesValidation=False runat="server" CssClass="txtButtonRed" Text="Search"></asp:LinkButton>
                </div>  
            </div>           
			
			<div style="clear:both"></div>
                               
			<hr style="background-color:#993366;height:5px;margin:0px;" />
			
			
           	<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td><asp:GridView ID="gvContractor"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>' BorderColor="White"  
               PagerSettings-Mode=NextPreviousFirstLast  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               
               <PagerTemplate>
                   
                  
                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>
							<td runat="server" id="tdsetType1" width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">
							  <TR>
								<TD class="txtListing" align="left"> 
							
							
								</TD>
								<TD width="5"></TD>
							  </TR>
							</TABLE>						
							</td>
							
							
							<td width="10">&nbsp;</td>
						
						  
						  <td id="tdsetType2" align="left" width="121">
								
							</td>
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
        </PagerTemplate>
               <Columns>  
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="8%" HeaderStyle-CssClass="gridHdr gridText"  DataField="FName" SortExpression="FName" HeaderText="First Name" />            
				<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="8%" HeaderStyle-CssClass="gridHdr gridText"  DataField="LName" SortExpression="LName" HeaderText="Last Name"/> 
				 <asp:TemplateField HeaderText="Email" HeaderStyle-CssClass= "gridHdr gridText" SortExpression= "Email" >         
                  <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" Width="5%"/>
                    <ItemTemplate>             
                   <a href='mailto:<%#Container.DataItem("Email")%>' class="footerTxtSelected"><%#Container.DataItem("Email")%></a>
                    </ItemTemplate>
                </asp:TemplateField> 
                            
				<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="Phone" SortExpression="Phone" HeaderText="Phone"/> 
				<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="Mobile" SortExpression="Mobile" HeaderText="Mobile"/> 
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="PostCode" SortExpression="PostCode" HeaderText="PostCode"/> 
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"  DataField="DateCreated" SortExpression="DateCreated" HeaderText="Created Date"/> 
                <asp:TemplateField HeaderText="IsDeleted" HeaderStyle-CssClass= "gridHdr gridText"  >         
                  <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" Width="5%"/>
                    <ItemTemplate>             
                   <asp:Label ID="lblIsDeleted" runat="server" Text='<%# IIF(Container.DataItem("IsDeleted"), True,False)%>'>></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="CV" HeaderStyle-CssClass= "gridHdr gridText"  >               
                <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Center CssClass="gridRow"/>
                    <ItemTemplate>             
                   <span class='txtGreySmallSelected'>
                              <a class='footerTxtSelected' href='<%# getlink(Container.DataItem("FilePath")) %>' target='_blank'>
                                                            <%# Container.DataItem("Name")%></a>
                                                    
                        </span>
                    </ItemTemplate>
                </asp:TemplateField>  
                 <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass= "gridHdr gridText"  >         
                  <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Center  CssClass="gridRow" Width="5%"/>
                    <ItemTemplate>     
                    <asp:ImageButton ImageUrl="Images/Icons/Delete.gif" CommandName="Del" CommandArgument='<%# Container.DataItem("ContractorId")%>' Runat="server" ID="ImgBtnDelete" AlternateText="Delete Contractor" ToolTip="Delete Contractor" CausesValidation="False" Visible='<%#IIF((Container.DataItem("IsDeleted")) = "1", False, True)%>' ></asp:ImageButton>        
                    <asp:ImageButton ImageUrl="Images/Icons/undodelete.png" CommandName="Show" CommandArgument='<%# Container.DataItem("ContractorId")%>' Runat="server" ID="ImgBtnShow" AlternateText="Show Contractor" ToolTip="Show Contractor" CausesValidation="False" Visible='<%#IIF((Container.DataItem("IsDeleted")) = "0", False, True)%>' ></asp:ImageButton>                            
                    </ItemTemplate>
                </asp:TemplateField>  
               
				</Columns>
               
                 <AlternatingRowStyle    CssClass=gridRow />
                 <RowStyle CssClass=gridRow />            
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
        
              </asp:GridView>
				 </td>
            </tr>
            </table>
				
				 <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.ContractorListing" SortParameterName="sortExpression" >                
            </asp:ObjectDataSource>
  
          </ContentTemplate>
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>

</asp:Content>

