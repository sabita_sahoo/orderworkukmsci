﻿Public Class PhotoUpload
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            ViewState("ContactId") = Request("PhotoUploadContact")
            hdnAttachmentDisplayPath.Value = ApplicationSettings.AttachmentDisplayPath(1).ToString
        End If
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpload.Click
        Page.Validate()
        If Page.IsValid() Then
            Dim ImageName As String
            If Not IsNothing(ViewState("ContactId")) Then
                If (ViewState("ContactId") <> "") Then
                    ImageName = (ViewState("ContactId").ToString).Replace(" ", "_") & DateTime.UtcNow.Ticks & ".jpg"
                Else
                    ImageName = DateTime.UtcNow.Ticks & ".jpg"
                End If
            Else
                ImageName = DateTime.UtcNow.Ticks & ".jpg"
            End If

            If FileUpload1.HasFile = False Then
                ScriptManager.RegisterStartupScript(Me, btnUpload.GetType, "JS", "alert('Please select file to upload!!!');", True)
            Else
                SaveImage(135, 135, ImageName)
            End If
        End If
    End Sub

    Public Sub SaveImage(ByVal Width As Integer, ByVal Height As Integer, ByVal ImageName As String)
        If OrderWorkLibrary.PhotoUpload.CheckFileType(FileUpload1.FileName, FileUpload1.PostedFile.ContentType) = False Then
            ScriptManager.RegisterStartupScript(Me, btnUpload.GetType(), "JS", "alert('Incorrect File Type!!!');", True)
            Exit Sub
        Else
            Dim ImageWidth As Integer = Width
            Dim ImageHeight As Integer = Height
            Dim ImagePath As String = ""

            ImagePath = "D:\home\site\wwwroot\Attachments\"

            Dim Success As Boolean
            Success = OrderWorkLibrary.PhotoUpload.UploadImage(ImageWidth, ImageHeight, ImageName.ToString(), ImagePath.ToString(), "jpg", FileUpload1)
            If Success = True Then
                Dim ds As DataSet = ws.WSContact.UpdateContactImage(CInt(ViewState("ContactId")), ImageName)

                'Send Email to user and Recruiter if all the profile info is filled
                Dim DSProfileCompleteData As DataSet
                DSProfileCompleteData = ws.WSContact.GetProfileCompleteData(ViewState("ContactId"))
                If (DSProfileCompleteData.Tables(0).Rows.Count > 0) Then
                    If (DSProfileCompleteData.Tables(0).Rows(0)("IsProfileCompleted") = 1) Then
                        'Send Email to user and Recruiter if all the profile info is filled
                        Emails.SendUserProfileCompletionMail(DSProfileCompleteData.Tables(0).Rows(0)("Email").ToString, DSProfileCompleteData.Tables(0).Rows(0)("FName").ToString)
                        Emails.SendRecruitmentProfileCompletionMail(DSProfileCompleteData)
                        Dim dsInfo As DataSet = ws.WSContact.UpdateUserProfileCompletionMailInfo(ViewState("ContactId"), 1, Session("UserID"))
                    End If
                End If

                hdnImageName.Value = ImageName
                ScriptManager.RegisterStartupScript(Me, btnUpload.GetType, "JS", "callPrevPage();", True)
            Else
                ScriptManager.RegisterStartupScript(Me, btnUpload.GetType, "JS", "alert('Fail!!!');", True)
            End If
        End If

    End Sub

End Class