
Partial Public Class AgedDebtReport
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryDE
                    ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
                Case ApplicationSettings.CountryUK
                    ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
            End Select
            generateReport()
        End If
    End Sub

   
    Private Sub generateReport()
        Dim languageCode As String = ""
        'pnlExportReport.Visible = True
        ReportViewerOW.ShowParameterPrompts = False
        ReportViewerOW.ShowToolBar = True
        Dim reportSErverPath As Uri = New Uri(ApplicationSettings.ReportServerPath)
        ReportViewerOW.ServerReport.ReportServerUrl = reportSErverPath
        ReportViewerOW.ServerReport.ReportServerCredentials = New CustomReportCredentials.CustomReportCredentials(ApplicationSettings.ReportServerUserName, ApplicationSettings.ReportServerPassword, ApplicationSettings.ReportServerDomain)
        Dim objParameter(2) As Microsoft.Reporting.WebForms.ReportParameter
        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/AllCompanyAgedDebtReport"
        objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)
        objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "False")
        'Language parameters
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                languageCode = "de-DE"
            Case ApplicationSettings.CountryUK
                languageCode = "en-GB"
        End Select
        objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)


        ReportViewerOW.ServerReport.SetParameters(objParameter)
        ReportViewerOW.ServerReport.Refresh()

    End Sub

    'Private Sub lnkExportReportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportReportExcel.Click

    '    Dim warnings As Microsoft.Reporting.WebForms.Warning()
    '    Dim streamids As String()
    '    Dim mimeType As String
    '    Dim encoding As String
    '    Dim fileType As String = "EXCEL"
    '    Dim extension As String = "xls"
    '    Dim fileName As String = "OWReport"
    '    Dim languageCode As String = ""

    '    Dim objParameter(2) As Microsoft.Reporting.WebForms.ReportParameter
    '    ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/AllCompanyAgedDebtReport"
    '    objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)
    '    objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "False")
    '    'Language parameters
    '    Select Case ApplicationSettings.Country
    '        Case ApplicationSettings.CountryDE
    '            languageCode = "de-DE"
    '        Case ApplicationSettings.CountryUK
    '            languageCode = "en-GB"
    '    End Select
    '    objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)

    '    ReportViewerOW.ServerReport.SetParameters(objParameter)

    '    Dim bytes As Byte() = ReportViewerOW.ServerReport.Render(fileType, Nothing, mimeType, encoding, extension, streamids, warnings)
    '    Response.Buffer = True
    '    Response.Clear()
    '    Response.ContentType = mimeType
    '    Response.AddHeader("content-disposition", "attachment; filename=AllCompanyAgedDebtReport" + Date.Now + "." + extension)
    '    Response.BinaryWrite(bytes)
    '    Response.Flush()
    '    Response.End()
    'End Sub

    'Private Sub lnkExportReportPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportReportPdf.Click

    '    Dim warnings As Microsoft.Reporting.WebForms.Warning()
    '    Dim streamids As String()
    '    Dim mimeType As String
    '    Dim encoding As String
    '    Dim fileType As String = "PDF"
    '    Dim extension As String = "pdf"
    '    Dim fileName As String = "OWReport"
    '    Dim languageCode As String = ""

    '    Dim objParameter(2) As Microsoft.Reporting.WebForms.ReportParameter
    '    ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/AllCompanyAgedDebtReport"
    '    objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)
    '    objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "False")
    '    'Language parameters
    '    Select Case ApplicationSettings.Country
    '        Case ApplicationSettings.CountryDE
    '            languageCode = "de-DE"
    '        Case ApplicationSettings.CountryUK
    '            languageCode = "en-GB"
    '    End Select
    '    objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)
    '    ReportViewerOW.ServerReport.SetParameters(objParameter)

    '    Dim bytes As Byte() = ReportViewerOW.ServerReport.Render(fileType, Nothing, mimeType, encoding, extension, streamids, warnings)
    '    Response.Buffer = True
    '    Response.Clear()
    '    Response.ContentType = mimeType
    '    Response.AddHeader("content-disposition", "attachment; filename=AllCompanyAgedDebtReport" + Date.Now + "." + extension)
    '    Response.BinaryWrite(bytes)
    '    Response.Flush()
    '    Response.End()
    'End Sub

End Class