

Partial Public Class WONotes
    Inherits System.Web.UI.Page

    ''' <summary>
    ''' This method handles the page load of this page.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Pratik Trivedi. 13 Nov, 2008</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsNothing(Request.QueryString("WOID")) And Request.QueryString("WOID") <> "" Then
            ViewState("WOID") = Request.QueryString("WOID")
            
        End If
        lnkBackToListing.HRef = getBackToListingLink()

        Dim ds As DataSet
        ds = CommonFunctions.GetNotes(ViewState("WOID"))
        populateNotes(ds)
        'poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2
        divValidationMain.Visible = False
        If Not Page.IsPostBack Then
            'populate note type ddl
            CommonFunctions.PopulateNoteCategory(Page, ddlNoteType)

        End If
       
    End Sub

    ''' <summary>
    ''' This method populates the Data. Can be called from anywhere in this page.
    ''' </summary>
    ''' <remarks>Pratik Trivedi. 14 Nov, 2008</remarks>
    Protected Sub populateNotes(ByVal ds As DataSet)
        If ds.Tables("Notes").Rows.Count = 0 Then
            pnlNoNotesMsg.Visible = True
            pnlNotesList.Visible = False
        Else
            pnlNoNotesMsg.Visible = False
            pnlNotesList.Visible = True
            rptNotes.DataSource = ds.Tables("Notes")
            rptNotes.DataBind()

        End If
    End Sub

    ''' <summary>
    ''' This method saves the Notes into the Database
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Pratik Trivedi. 14 Nov, 2008</remarks>
    Private Sub ancSubmit_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ancSubmit.ServerClick
        'poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2
        Page.Validate()
        If (Page.IsValid = True) Then
            divValidationMain.Visible = False
            Dim message As String = txtNote.Text.Trim.Replace(Chr(13), "<BR>")
            'poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2
            Dim ds As DataSet = CommonFunctions.SaveNotes(ViewState("WOID"), Session("UserId"), message, True, ddlNoteType.SelectedItem.Value)
            populateNotes(ds)
            txtNote.Text = ""
            ddlNoteType.SelectedIndex = -1
        Else
            divValidationMain.Visible = True
        End If
        
    End Sub

    ''' <summary>
    ''' This method prepares the back-to-listing link.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Pratik Trivedi. 14 Nov, 2008</remarks>
    Public Function getBackToListingLink() As String
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender")
                Case "UCWOsListing", "CompanyProfile", "ucwoslisting", "companyprofile"
                    If Request("Group") = ApplicationSettings.WOGroupIssue Then
                        link = "~\AdminWOIssueListing.aspx?"
                    Else
                        link = "~\AdminWOListing.aspx?"
                    End If
                    link &= "WOID=" & Request("WOID")
                    link &= "&ContactID=" & Request("ContactID")
                    link &= "&CompanyID=" & Request("CompanyID")
                    link &= "&Group=" & Request("Group")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&mode=" & Request("Group")
                    If Not IsNothing(Request("SearchWorkorderID")) Then
                        link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
                    End If
                    link &= "&CompID=" & Request("CompID")
                    link &= "&sender=" & "UCWOsListing"
                Case "UCWODetails"
                    link = "~\AdminWODetails.aspx?"
                    link &= "WOID=" & Request("WOID")
                    link &= "&ContactID=" & Request("ContactID")
                    link &= "&CompanyID=" & Request("CompanyID")
                    link &= "&Group=" & Request("Group")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&mode=" & Request("Group")
                    link &= "&CompID=" & Request("CompID")
                    link &= "&sender=" & "UCWOsListing"
                Case "SalesInvoiceGeneration"
                    link = "~\AdminWODetails.aspx?"
                    link &= "&companyId=" & Request("companyId")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&bizDivId=" & Request("bizDivId")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=" & "SalesInvoiceGeneration"
                Case "PIUnpaidAvailable"
                    link = "AdminWODetails.aspx?"
                    link &= "invoiceNo=" & Request("invoiceNo")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&CompanyId=" & Request("CompanyId")
                    link &= "&Over30Days=" & Request("Over30Days")
                    link &= "&sender=" & "PIUnpaidAvailable"
                Case "UpSellSalesInvoicePaid", "upsellsalesinvoicepaid"
                    link = "~\AdminWODetails.aspx?"
                    link &= "WOID=" & Request("WOID")
                    'link &= "&ContactID=" & Request("ContactID")
                    link &= "&CompanyID=" & Request("CompanyID")
                    'link &= "&Group=" & Request("Group")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    'link &= "&mode=" & Request("Group")
                    'link &= "&CompID=" & Request("CompID")
                    link &= "&sender=" & "UpSellSalesInvoicePaid"

                Case "SearchWO"
                    link = "AdminWODetails.aspx?"
                    link &= "WOID=" & Request("WOID")
                    link &= "&WorkOrderID=" & Request("WorkOrderID")
                    link &= "&CompanyID=" & Request("CompanyID")
                    link &= "&ContactId=" & Request("ContactId")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&Group=" & Request("Group")
                    link &= "&SupCompID=" & Request("SupCompID")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=" & "SearchWO"
                    link &= "&txtWorkorderID=" & Request("txtWorkorderID")
                    link &= "&txtCompanyName=" & Request("txtCompanyName")
                    link &= "&Keyword=" & Request("txtKeyword")
                    link &= "&PONumber=" & Request("txtPONumber")
                    link &= "&PostCode=" & Request("txtPostCode")
                Case "SearchFinance"
                    link = "~\AdminWODetails.aspx?"
                    link &= "WOID=" & Request("WOID")
                    link &= "&CompanyID=" & Request("CompanyID")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&txtPONumber=" & Request("txtPONumber")
                    link &= "&txtInvoiceNo=" & Request("txtInvoiceNo")
                    link &= "&txtKeyword=" & Request("txtKeyword")
                    link &= "&txtWorkorderID=" & Request("txtWorkorderID")
                    link &= "&txtCompanyName=" & Request("txtCompanyName")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=SearchFinance"
                Case "SalesInvoiceWOListing"
                    link = "~\AdminWODetails.aspx?"
                    link &= "WOID=" & Request("WOID")
                    link &= "&CompanyID=" & Request("CompanyID")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=" & Request("SalesInvoiceWOListing")
                    link &= "&InvoiceNo=" & Request("InvoiceNo")
                    link &= "&tab=" & Request("tab")
                    link &= "&HideBACKTOLISTING=" & Request("HideBACKTOLISTING")
                Case Else
                    Return ""
            End Select
        End If
        Return link
    End Function



    
End Class