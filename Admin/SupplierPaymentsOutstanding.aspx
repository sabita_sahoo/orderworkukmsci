<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" CodeBehind="SupplierPaymentsOutstanding.aspx.vb" Inherits="Admin.SupplierPaymentsOutstanding" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Supplier Payment Outstanding"%>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
	        <div id="divContent">
	        <style type="text/css">
	        .HighlightTab
{text-decoration:none;float:left;background-color:#993366;font-family:verdana;font-weight:bold;font-size:8pt;color:#ffffff;width:150;height:21;text-align:center;}
.NormalTab
{text-decoration:none;float:left;background-color:#a0a0a0;font-family:verdana;font-weight:bold;font-size:8pt;color:#ffffff;width:150;height:21;text-align:center;}
</style>
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10px"></td>
			<td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Supplier Payments" runat="server"></asp:Label></td>
			</tr>
			</table>
			
            <input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />
           
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>		
                <asp:Panel ID="pnlExcel" runat=server Visible=true>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
					   <td width="10px"></td>
					    <td width="0"><table width="100%" cellpadding="0" cellspacing="0" border="0">
						     <tr>
						       <td >
							  <%@ Register TagPrefix="uc1" TagName="UCDateRange" Src="~/UserControls/UK/UCDateRangeUK.ascx" %>
							     <uc1:UCDateRange id="UCDateRange1" runat="server"></uc1:UCDateRange> 
						       </td>
						      </tr>
						    </table>
					    </td>
                        <td align="left"  width="150px">
                         <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:115px;" ID="divExportToExcel" runat="server">
                                <asp:linkButton ID="btnExportToExcel" CssClass="txtButtonRed" runat="server" style="cursor:hand" >&nbsp;Export to Excel&nbsp;</asp:linkButton>                                
                            </div>
                         </td>   
					    <td runat=server id="tdShowAll" visible=false width="80" align="left"><asp:CheckBox ID="chkShowAll" runat="server" Text='Show 100' class="formTxt"  AutoPostBack="true" /> </td>
					    				
					    <td align="left" runat="server" id="tdReqExportToExcel" >&nbsp;
    					    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:150px;" ID="tblExport" runat="server">
                                <asp:linkButton ID="lnkExport" CssClass="txtButtonRed" runat="server" style="cursor:hand" >&nbsp;Process Invoices&nbsp;</asp:linkButton>
                                <input type=hidden id="hdnStatus" name="hdnStatus" runat=server /> 
                            </div>
                            
                            <div id="divConfirm" style="visibility:hidden;" runat="server"><b> you want to mark the exported payments as "In Process"? </b>
                             <asp:button id="btnUpdateStatus" runat="server" Text="Update"></asp:button> 
                             <input type=button onclick="cancelUpdate()" value="Cancel" />
                             </div>
    					<input id="hdnEtoE" type="hidden" runat="server"></input>
                           
					    </td>
                        		                         		    
					</tr>
				</table>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0" id="tblHoldPayment" runat="server">
                  <tr>      
                  <td runat="server" id="tdMakeAvailable" width="120" align="left" class="txtWelcome paddingL10" visible="false"><div id="divButton" style="width: 120px;  ">
											<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>								
											<asp:LinkButton OnClick="MakeAvailable" class="buttonText" style="cursor:hand;" id="btnMakeAvailable"  runat=server><strong>Unhold Payment</strong></asp:LinkButton>
										   <cc1:ConfirmButtonExtender ID="ConfirmBtnAvailable" TargetControlID="btnMakeAvailable" ConfirmText="Do you want to make selected Invoices payment unhold?" runat="server">
										  </cc1:ConfirmButtonExtender> 
											
											<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
										</div></td>	
										         
				    <td id="tdHold" runat="server" align="left" class="txtWelcome paddingL10"><div id="divButton" style="width: 120px;  ">
					    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>								
					    <asp:LinkButton OnClick="WithHoldPayment" class="buttonText" style="cursor:hand;" id="btnHold"  runat=server><strong>Withhold Payment</strong></asp:LinkButton>
					    <cc1:ConfirmButtonExtender ID="ConfirmBtnHold" TargetControlID="btnHold" ConfirmText="Do you want to hold the selected Invoices payment?" runat="server">
					    </cc1:ConfirmButtonExtender> 
					    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
					    </div>
					</td>									
                  </tr>
				  <tr> <td height="15">&nbsp;</td></tr>
                </table>	
		
					
					<asp:panel id="pnlComplete" runat="server">
					  <table id="tblComplete" runat="server"  cellspacing="0" cellpadding="0" width="100%" border="0">
                     
                          <tr valign="top">
                            <td class="formTxt" valign="top" align="left" width="18">&nbsp;</td>
                            <TD width="316" height="14" align="left" vAlign="top" class="formTxt">Date when payment will hit Supplier Bank Account (dd/mm/yyyy)</TD>
                            <td class="formTxt" align="left">&nbsp;</td>
                          </tr>
                          <tr valign="top">
                            <td height="24" align="left">&nbsp;</td>
                            <td align="left"><asp:TextBox id="txtDateCompleted" runat="server" CssClass="formField105"></asp:TextBox>						    
						    <img alt="Click to Select" src="Images/calendar.gif" id="btnBeginDate" style="cursor:pointer; vertical-align:middle;" />
              <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnBeginDate TargetControlID="txtDateCompleted" ID="calBeginDate" runat="server">
              </cc1:CalendarExtender>
						    </td>
                            <td align="left">
                                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:150px;">
                                    <asp:LinkButton runat=server ID="btnComplete"  CssClass="txtButtonRed" >&nbsp;Mark Invoices As Paid&nbsp;</asp:LinkButton>

                                    <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender1" TargetControlID="btnComplete" ConfirmText="Do you want to mark selected Invoices as Processed?" runat="server">
										  </cc1:ConfirmButtonExtender> 
                                </div>
                            </td>

                            <td align="left">
                                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:120px;" id="tblBOSExport" runat="server" visible="false">
                                    <a style="cursor:hand" id="btnBOSExport" runat="server" tabindex="6" class="txtButtonRed">&nbsp;HSBC Export&nbsp;</a>
                                    <input id="hdnBOS" type="hidden" runat="server"></input>
                                </div>
                            </td>
                            
                         </tr>
                      </table>                      
    			        
					    
					 </asp:panel>
					 
		</asp:Panel>
		
					  <asp:ValidationSummary   id="ValidationSummary1" runat="server" CssClass="bodytxtValidationMsg"></asp:ValidationSummary>
			   <div id="divValidationMsg" class="bodytxtValidationMsg paddingL28"></div>
					<div id="ValFromDateMsg" class="bodytxtValidationMsg" style="padding:10px 0px 0px 25px;"></div>
					<div id="ValToDateMsg" class="bodytxtValidationMsg" style="padding:0px 0px 10px 25px;"></div>		
			  <asp:Label  CssClass="bodytxtValidationMsg paddingL23" runat="server" ID="lblMsg"></asp:Label>
									  
						
						  
		   <table border="0" cellspacing="0" cellpadding="0" width="100" id="tblTabInner" runat="server">
            <tr>
            <td height="15px"></td><td height="15px"></td></tr>
				<tr>				
				<td class="paddingL10" >
				<asp:LinkButton ID="lnkbtn1" runat="server" CssClass="HighlightTab" OnClick="Tab1Click" Width="150" >Requested</asp:LinkButton></td>
				<td class="paddingL10" >
				<asp:LinkButton ID="lnkbtn2" runat="server" CssClass="NormalTab" OnClick="Tab2Click" Width="150">Inprocess</asp:LinkButton>
                  </td>                  
                  <td class="paddingL10" >
				<asp:LinkButton ID="lnkbtn3" runat="server" CssClass="NormalTab" OnClick="Tab3Click" Width="150">WithHeld</asp:LinkButton>
                  </td>	
				</tr>
			</table>
			<hr style="background-color:#993366;height:5px;margin:0px;" />
			
		      <asp:GridView ID="gvContacts"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
                  DataKeyNames="Date" PagerSettings-Mode=NextPreviousFirstLast   PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
                  
                   <EmptyDataTemplate>
                   <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						    <tr>
							    <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							    </td>
						    </tr>				
					    </table>
                      
                   </EmptyDataTemplate>
               
                       <PagerTemplate>
                           
                          
                         <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						        <tr>
							        <td runat="server" id="tdsetType1" width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">
							          <TR>
								        <TD class="txtListing" align="left">&nbsp;
								            
								        </TD>
								        <TD width="5"></TD>
							          </TR>
							        </TABLE>						
							        </td>
        							
							        <td runat="server"  id="tdApprove" width="70" align="left" class="txtWelcome paddingL10">&nbsp;
							        
							        </td>
        							
							        <td width="10">&nbsp;</td>
							        <td runat="server"  id="tdSuspend" width="75" align="right" class="txtWelcome paddingL10">&nbsp;
							            
							        </td>
						          <td width="10">&nbsp;</td>
						          <td runat="server" id="tdNew" width="75" align="right" class="txtWelcome paddingL10">&nbsp;
						            
        						  
						          </td>
						          <td id="tdsetType2" align="left" width="121">
        								
							        </td>
							        <td>
								        <table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								          <tr>
									        <td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										        <TR>
											        <td align="right" valign="middle">
											          <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
											        </td>
											        <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											        <table width="100%" border="0" cellpadding="0" cellspacing="0">
												        <tr>
												        <td align="right" width="36">
												        <div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													        <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													        </div>	
													        <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													        <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													        </div>																							   
												        </td>
												        <td width="50" align="center" style="margin-right:3px; ">													
													        <asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
												        </td>
												        <td width="30" valign="bottom">
													        <div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													         <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													        </div>
													        <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													         <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													        </div>
												        </td>
												        </tr>
											        </table>
											        </td>
										        </TR>
									        </TABLE></td>
								          </tr>
								        </table>
						          </td>
						        </tr>
					        </table> 
                           
                </PagerTemplate> 
                           <Columns>  
                                        
                           <asp:TemplateField HeaderText="<input id='chkAll' onClick=CheckAll(this,'Check') type='checkbox' name='chkAll' />">
                                           
                           <HeaderStyle CssClass="gridHdr" />
                           <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="gridRow"/>
                           <ItemTemplate>             
                            <asp:CheckBox ID="Check" runat="server" ></asp:CheckBox>
                            <input type=hidden runat=server id="hdnID"   value='<%#Container.DataItem("AdviceNumber") %>'/>
                            
                           </ItemTemplate>
                           </asp:TemplateField>   
                           
						    
                            <asp:TemplateField HeaderText="Purchase Invoice Number" SortExpression="AdviceNo" >
                            <HeaderStyle CssClass="gridHdr gridText" />
                            <ItemStyle Wrap="true" HorizontalAlign="Left" Width="200px" Height="40px"  CssClass="gridRow"/>
                            <ItemTemplate> <%#  Container.DataItem("AdviceNumber")%>
                            <asp:Image runat="server" ImageUrl ="~/Images/Icons/alertsmall.gif" id="imgMarker" AlternateText="Payment Withheld" ToolTip="Payment Withheld" Width="20" Height="14" Visible='<%#IIF(Container.DataItem("OnHold")=0,0,1)%>' runat='server'/>
                            </ItemTemplate>
                           </asp:TemplateField>
                          
                            <asp:TemplateField HeaderText="Supplier Company" SortExpression="SupplierName" >
                            <HeaderStyle CssClass="gridHdr" />
                            <ItemStyle Wrap=true HorizontalAlign=Left  CssClass="gridRow"/>
                            <ItemTemplate> <%#  Container.DataItem("SupplierName")%>
                            <%--<asp:Image runat="server" ImageUrl ="~/Images/Icons/alertsmall.gif" id="imgMarker" AlternateText="Payment Withheld" ToolTip="Payment Withheld" Width="20" Height="14" Visible='<%#IIF(Container.DataItem("OnHold")=0,0,1)%>' runat='server'/>--%>
                            </ItemTemplate>
                           </asp:TemplateField> 
                               
                            <asp:TemplateField HeaderText="Withdrawal Request Date" SortExpression="Date" >
                            <HeaderStyle CssClass="gridHdr" />
                            <ItemStyle Wrap=true HorizontalAlign=Left  CssClass="gridRow"/>
                            <ItemTemplate>             
                            <%# Container.DataItem("Date")  %>
                            </ItemTemplate>
                           </asp:TemplateField> 
                             
                               <asp:TemplateField HeaderText="Withdrawal Amount" SortExpression="Total" >
                                <HeaderStyle CssClass="gridHdr" />
                                <ItemStyle Wrap=true HorizontalAlign=Left  CssClass="gridRow"/>
                                <ItemTemplate>             
                                    <%#  Container.DataItem("Total")%>
                                </ItemTemplate>
                               </asp:TemplateField> 
							   
							   <asp:TemplateField >               
							   <HeaderStyle CssClass="gridHdr"   Wrap=true /> 
							  <ItemStyle Wrap=true HorizontalAlign=Left Width="50px"   CssClass="gridRow"/>
							   <ItemTemplate>   
									   <%#GetLinks(Container.DataItem("AdviceNumber"), Container.DataItem("SupplierName"),  Container.DataItem("WOID"))%>
							   </ItemTemplate>
							   </asp:TemplateField> 
               				</Columns>
                
							
							   
                             <AlternatingRowStyle CssClass=gridRow />
                             <RowStyle CssClass=gridRow />            
                            <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                            <HeaderStyle  CssClass="gridHdr" HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                            <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
        
               </asp:GridView>
               
               
				 </ContentTemplate>
				
            
            </asp:UpdatePanel>
            
                <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                             
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="gvContacts" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
                
             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.SupplierPaymentsOutstanding" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
</asp:Content>      
