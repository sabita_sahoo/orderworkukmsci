<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AccountingUpdate.aspx.vb" Inherits="Admin.AccountingUpdate" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Accounting Update" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<!-- InstanceBeginEditable name="head" -->
<style type="text/css">
#divMenu {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	line-height: 12px;
	font-weight: normal;
	text-decoration: none;
	border-top-width: 1px;
	border-top-style: solid; 
	border-top-color: #CECECE;
}

.divContentTopRed{
	width: 100%;
	background-color: #993366;
	background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
	height: 5px;
	
}
    #div1
    {
        height: 13px;
    }
</style>
<!-- InstanceEndEditable --><!-- InstanceParam name="opAccountSumm" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddUser" type="boolean" value="false" --><!-- InstanceParam name="OpRgCLAddLOcation" type="boolean" value="false" --><!-- InstanceParam name="OPWOSummary" type="boolean" value="false" --><!-- InstanceParam name="RgOpBtnNewWO" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddFunds" type="boolean" value="false" --><!-- InstanceParam name="opMenuRegion" type="boolean" value="true" --><!-- InstanceParam name="opContentTopRedCurve" type="boolean" value="false" --><!-- InstanceParam name="opUsersProfile" type="boolean" value="false" --><!-- InstanceParam name="opCompanyProfile" type="boolean" value="true" --><!-- InstanceParam name="opUsersListing" type="boolean" value="false" --><!-- InstanceParam name="opLocations" type="boolean" value="false" --><!-- InstanceParam name="opReferences" type="boolean" value="false" --><!-- InstanceParam name="opPreferences" type="boolean" value="false" --><!-- InstanceParam name="opUsers" type="boolean" value="false" --><!-- InstanceParam name="width" type="text" value="985" --><!-- InstanceParam name="opComments" type="boolean" value="false" --><!-- InstanceParam name="OpRgScriptManager" type="boolean" value="true" --><!-- InstanceParam name="OpFavSuppliers" type="boolean" value="false" --><!-- InstanceParam name="OpFavSuppliersLink" type="boolean" value="false" -->
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
        <asp:UpdatePanel runat="Server" ID="UpdatePanel1">
        <ContentTemplate>

		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            <div style="margin-left:20px;" class="formTxt">
              <p class="paddingB4 HeadingRed"><strong>Welcome to OrderWork! Accounting Update</strong> </p>
               <asp:ValidationSummary id="validationSummarySubmit" runat="server" ShowSummary="true" Enabled="true"></asp:ValidationSummary>
			    <asp:Panel ID="pnlMsg" Visible="false" runat="server">
                    <div style="height:30px; margin-top:10px;">
                        <asp:Label ID="lblMsg" runat="server" Text="Label" CssClass="HeadingRed"></asp:Label>
                       
                    </div>
                </asp:Panel>    
                <asp:DropDownList ID="drpdwnAccountingUpdate" runat="server" AutoPostBack="true" CssClass="formField width240">
                    <asp:ListItem Value="0" Selected="True">Select Operation to perform</asp:ListItem>
                    <asp:ListItem Value="UpdateDate">Update Invoice date</asp:ListItem>
                    <asp:ListItem Value="UpdateInvoicePO">Update Invoice PO</asp:ListItem>
                    <asp:ListItem Value="UpdateVAT">Include VAT Amount</asp:ListItem>
                    <asp:ListItem Value="UpdateWPPrice">Update Wholesale Price</asp:ListItem>
                    <asp:ListItem Value="UpdatePFPrice">Update Portal Price</asp:ListItem>
                    <asp:ListItem Value="UpdatePONum">Update PO Number</asp:ListItem>
                     <asp:ListItem Value="UpdateRatings">Update Rating</asp:ListItem>
                     <asp:ListItem Value="UpdateReceipt">Change Cash Receipt value</asp:ListItem>
                     <asp:ListItem Value="UpdateUpsell">Change Upsell value</asp:ListItem>
                     <asp:ListItem Value="UpdateInvoiceTitle">Change Invoice Title</asp:ListItem>
                     <asp:ListItem Value="UpdateUpsellInvoice">Create Upsell Invoice</asp:ListItem>
                     <asp:ListItem Value="UpdateQuantity">Change Quantity</asp:ListItem>
                </asp:DropDownList>
                <asp:Panel ID="pnlUpdatePrice" Visible="false" runat="server">  
                    <asp:ValidationSummary id="validationSummary1" CssClass="bodytxtValidationMsg" runat="server" ShowSummary="true" Enabled="true" ValidationGroup="PriceChange"></asp:ValidationSummary>                  
                    <div>
                        <div>
                            <div style="height:30px; margin-top:10px;">
                                <div style="width:90px; float:left;">
                                <asp:Label ID="lbltxtWOID" runat="server" Text="WOID"></asp:Label><asp:RequiredFieldValidator
                                    ID="rqdFieldWOID" runat="server" ErrorMessage="Please Enter Work Order ID" ControlToValidate="txtWOID" ValidationGroup="PriceChange">*</asp:RequiredFieldValidator></div>
                                <asp:TextBox ID="txtWOID" runat="server" CssClass="formField width150" AutoPostBack="true" MaxLength="15"></asp:TextBox>
                            </div>
	                        <div style="height:30px; margin-top:10px;" runat="server" id="divOriginalPrice" visible="false">
                                <div style="width:90px; float:left;">
                                <asp:Label ID="lbltxtOrigPrice" runat="server" Text="Original Price"></asp:Label></div>
                                <asp:TextBox ID="txtOrigPrice" runat="server" CssClass="formField width150" Enabled="false" MaxLength="8"></asp:TextBox>
                            </div>
                            <div style="margin-bottom:10px;">
                                <div style="width:90px; float:left;">
                                <asp:Label ID="lblPriceText" runat="server" Text="Label"></asp:Label><asp:RequiredFieldValidator
                                    ID="rqdFieldPrice" runat="server" ErrorMessage="Please Enter Price To Be modified" ControlToValidate="txtPrice" ValidationGroup="PriceChange">*</asp:RequiredFieldValidator></div>
                                <asp:TextBox ID="txtPrice" runat="server" CssClass="formField width150" MaxLength="8"></asp:TextBox>
                            </div>
                        </div>
                        <asp:Button ID="btnUpdatePrice" runat="server" Text="Update" ValidationGroup="PriceChange" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlUpdateUpsellInvoice" Visible="false" runat="server">  
                    <asp:ValidationSummary id="validationSummary10" CssClass="bodytxtValidationMsg" runat="server" ShowSummary="true" Enabled="true" ValidationGroup="UpdateUpsellInvoice"></asp:ValidationSummary>                  
                    <div>
                        <div>
                            <div style="height:30px; margin-top:10px;">
                                <div style="width:90px; float:left;">
                                WorkOrderId<asp:RequiredFieldValidator
                                    ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please Enter Work Order ID" ControlToValidate="txtUpdateUpsellInvoiceWOID" ValidationGroup="UpdateUpsellInvoice">*</asp:RequiredFieldValidator></div>
                                <asp:TextBox ID="txtUpdateUpsellInvoiceWOID" runat="server" CssClass="formField width150" AutoPostBack="true" MaxLength="15"></asp:TextBox>
                            </div>	                        
                            <div style="margin-bottom:10px;">
                                <div style="width:90px; float:left;">
                                UpSell Price<asp:RequiredFieldValidator
                                    ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please Enter Price To Be modified" ControlToValidate="txtUpSellPrice" ValidationGroup="UpdateUpsellInvoice">*</asp:RequiredFieldValidator></div>
                                <asp:TextBox ID="txtUpSellPrice" runat="server" CssClass="formField width150" MaxLength="8"></asp:TextBox>
                            </div>
                        </div>
                        <asp:Button ID="btnUpdateUpsellInvoice" runat="server" Text="Update" ValidationGroup="UpdateUpsellInvoice" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlUpdateInvoiceDate" Visible="false" runat="server">
                    <asp:ValidationSummary id="validationSummary2" runat="server" CssClass="bodytxtValidationMsg" ShowSummary="true" Enabled="true" ValidationGroup="DateChange"></asp:ValidationSummary>                  
                    <div>
                        <div style="height:30px; margin-top:10px;">
                            <div style="width:110px; float:left;">
                            Invoice Number<asp:RequiredFieldValidator ID="rqdInvoiceNo" runat="server" ErrorMessage="Please Enter Invoice Number whose Date Details needs to be Updated" 
                            ControlToValidate="txtInvoiceNumber" ValidationGroup="DateChange">*</asp:RequiredFieldValidator></div>
                            <asp:TextBox ID="txtInvoiceNumber" runat="server" CssClass="formField width150" MaxLength="20"></asp:TextBox>
                        </div>
                        <div style="margin-bottom:10px;">
                            <div style="width:110px; float:left;">
                            Invoice Updated Date <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ErrorMessage="Please Enter Date to be Updated" ControlToValidate="txtNewInvoiceDate" ValidationGroup="DateChange">*</asp:RequiredFieldValidator></div>
                            <asp:TextBox ID="txtNewInvoiceDate" runat="server" Width="130" CssClass="formField" MaxLength="10"></asp:TextBox>
                            <img alt="Click to Select" src="Images/calendar.gif" id="btnEndDate" style="cursor:pointer; vertical-align:middle;" />
                             
                                      <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnEndDate TargetControlID="txtNewInvoiceDate" ID="calEndDate" runat="server">
                                      </cc1:CalendarExtender>
                        </div>
                        <asp:Button ID="btnUpdateInvoiceDate" runat="server" Text="Update" ValidationGroup="DateChange"/>
                    </div>
                </asp:Panel>
               <asp:Panel ID="PnlUpdateInvoicePO" Visible="false" runat="server">
                    <asp:ValidationSummary id="validationSummary13" runat="server" CssClass="bodytxtValidationMsg" ShowSummary="true" Enabled="true" ValidationGroup="DateChange"></asp:ValidationSummary>                  
                     <div>                                                	  
			         <div style="height:30px; margin-top:10px;">
                            <div style="width:110px; float:left;">
                            Invoice Number<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Please Enter Invoice Number whose PO Details needs to be Updated" 
                            ControlToValidate="txtInvoiceNumber1" ValidationGroup="DateChange">*</asp:RequiredFieldValidator></div>
                            <asp:TextBox ID="txtInvoiceNumber1" runat="server" CssClass="formField width150" MaxLength="20"></asp:TextBox>
                        </div>                           
                                 <div id="div1" runat="server" visible="true">
                                     Invoice PO Number&nbsp;&nbsp;&nbsp;
                                     <asp:TextBox ID="txtNewInvoicePO" runat="server"  MaxLength="30" 
                                          CssClass="formField width150"></asp:TextBox> <br />
                                     <br /><br />                          
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Please enter a PO Number" 
                                     ControlToValidate="txtNewInvoicePO" ValidationGroup="PONumUpdate" ForeColor="#EDEDEB" >*</asp:RequiredFieldValidator>
                                     <asp:Button ID="btnUpdateInvoicePO" runat="server" Text="Update" ValidationGroup="PONumUpdate" />
                                     <asp:Button ID="btnCancelInvoicePO" runat="server" Text="Cancel" />
                                </div>
                            </div>                        
                </asp:Panel>
                <asp:Panel ID="pnlUpdateVATDetails" Visible="false" runat="server">
                     <asp:ValidationSummary id="validationSummary3" runat="server" CssClass="bodytxtValidationMsg" ShowSummary="true" Enabled="true" ValidationGroup="VATUpdate"></asp:ValidationSummary>                  
                     <div>
                        <div style="height:30px; margin-top:10px; margin-bottom:10px;">
                            <div style="width:90px; float:left;">
                            Invoice Number<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Enter Purchase Invoice Number whose VAT Details needs to be Updated" 
                            ControlToValidate="txtPurchaseInvoiceNumber" ValidationGroup="VATUpdate">*</asp:RequiredFieldValidator></div>
                            <asp:TextBox ID="txtPurchaseInvoiceNumber" runat="server" CssClass="formField width150" MaxLength="20"></asp:TextBox>
                        </div>
                        <asp:Button ID="btnUpdateVAT" runat="server" Text="Update" ValidationGroup="VATUpdate" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlUpdatePONum" Visible="false" runat="server">
                     <asp:ValidationSummary id="validationSummary5" runat="server" CssClass="bodytxtValidationMsg" ShowSummary="true" Enabled="true" ValidationGroup="PONumFetch"></asp:ValidationSummary>
                     <asp:ValidationSummary id="validationSummary4" runat="server" CssClass="bodytxtValidationMsg" ShowSummary="true" Enabled="true" ValidationGroup="PONumUpdate"></asp:ValidationSummary>
                     <asp:Label runat="server" ID="lblPOError" Text="<br /> * Invalid Work Order ID" Visible="false" CssClass="bodytxtValidationMsg"></asp:Label>
                     <div>
                        <div style="margin-top:10px; margin-bottom:10px;">
                            <div>                                 
                                 Work Order ID <br />
                                 <asp:TextBox ID="txtWOIDPONum" runat="server" Width="180" MaxLength="30"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter a Work Order ID" 
                                     ControlToValidate="txtWOIDPONum" ValidationGroup="PONumFetch" ForeColor="#EDEDEB">*</asp:RequiredFieldValidator>
                                 <asp:Button ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="PONumFetch" /><br /><br />                                 
                                 <div id="divUpdatePONum" runat="server" visible="false">
                                     Current PO Number <br />
                                     <asp:TextBox runat="server" ID="txtCurrPONum" Enabled="false" Width="245"></asp:TextBox><br /><br />
                                     New PO Number <br />
                                     <asp:TextBox ID="txtNewPONum" runat="server" Width="245" MaxLength="30"></asp:TextBox><br /><br />                          
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter a PO Number" 
                                     ControlToValidate="txtNewPONum" ValidationGroup="PONumUpdate" ForeColor="#EDEDEB" >*</asp:RequiredFieldValidator>
                                     <asp:Button ID="btnUpdatePONum" runat="server" Text="Update" ValidationGroup="PONumUpdate" />
                                     <asp:Button ID="btnCancelPONum" runat="server" Text="Cancel" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                 <asp:Panel ID="pnlUpdateQuantity" Visible="false" runat="server">
                     <asp:ValidationSummary id="validationSummary11" runat="server" CssClass="bodytxtValidationMsg" ShowSummary="true" Enabled="true" ValidationGroup="QuantityFetch"></asp:ValidationSummary>
                     <asp:ValidationSummary id="validationSummary12" runat="server" CssClass="bodytxtValidationMsg" ShowSummary="true" Enabled="true" ValidationGroup="QuantityUpdate"></asp:ValidationSummary>
                     <asp:Label runat="server" ID="lblQuantityError" Text="<br /> * Invalid Work Order ID" Visible="false" CssClass="bodytxtValidationMsg"></asp:Label>
                     <div>
                        <div style="margin-top:10px; margin-bottom:10px;">
                            <div>                                 
                                 Work Order ID <br />
                                 <asp:TextBox ID="txtWOIDQuantity" runat="server" Width="180" MaxLength="30"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Please enter a Work Order ID" 
                                     ControlToValidate="txtWOIDQuantity" ValidationGroup="QuantityFetch" ForeColor="#EDEDEB">*</asp:RequiredFieldValidator>
                                 <asp:Button ID="btnSubmitQuantity" runat="server" Text="Submit" ValidationGroup="QuantityFetch" /><br /><br />                                 
                                 <div id="divUpdateQuantity" runat="server" visible="false">
                                     Current Quantity <br />
                                     <asp:TextBox runat="server" ID="txtCurrQuantity" Enabled="false" Width="245"></asp:TextBox><br /><br />
                                     New Quantity <br />
                                     <asp:TextBox ID="txtNewQuantity" runat="server" Width="245" MaxLength="30"></asp:TextBox><br /><br />                          
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Please enter a Quantity" 
                                     ControlToValidate="txtNewQuantity" ValidationGroup="QuantityUpdate" ForeColor="#EDEDEB" >*</asp:RequiredFieldValidator>
                                     <asp:Button ID="btnUpdateQuantity" runat="server" Text="Update" ValidationGroup="QuantityUpdate" />
                                     <asp:Button ID="btnCancelQuantity" runat="server" Text="Cancel" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlUpdateRatings" Visible="false">
                  <asp:ValidationSummary id="validationSummary6" CssClass="bodytxtValidationMsg" runat="server" ShowSummary="true" Enabled="true" ValidationGroup="UpdateRatings"></asp:ValidationSummary>                  
                  
                  <div id="divRatingBox" runat="server">
                      <div style="height:30px; margin-top:10px;">
                          <div style="width:90px; float:left;">
                            &nbsp;<asp:Label ID="lblWOIDUpdateRatings" runat="server" Text="WOID"></asp:Label><asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please Enter Work Order ID" ControlToValidate="txtWOIDRatings" ValidationGroup="UpdateRatings">*</asp:RequiredFieldValidator></div>
                            <asp:TextBox ID="txtWOIDRatings" runat="server" CssClass="formField width150" AutoPostBack="true" MaxLength="13"></asp:TextBox>
                       </div>   
                  
                  <div runat="server" visible="false" id="divRating">
                      Current Rating&nbsp;
                      <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnPositive" Enabled="false"  runat="server" Text="Positive" />
                      <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNeutral" Enabled="false" runat="server"  Text="Neutral"/>
                       <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNegative" Enabled="false" runat="server" Text="Negative" /><br /><br />
                  </div>
                  <div runat="server" visible="false" id="divSelectRatings">
                       Update Rating&nbsp;
                      <asp:RadioButton GroupName="Ratings" ID="rdoPositive" runat="server" Text="Positive" />
                      <asp:RadioButton GroupName="Ratings" ID="rdoNeutral" runat="server"  Text="Neutral"/>
                      <asp:RadioButton GroupName="Ratings" ID="rdoNegative" runat="server" Text="Negative" /><br /><br />
                  </div>
                  
                  <asp:Button ID="btnUpdateRatings" runat="server" Text="Update" ValidationGroup="UpdateRatings" /><br />
                  </div>
                  <br />
                  <asp:Label runat="server" CssClass="paddingB4 HeadingRed" Visible="false" Text="Your ratings has been updated" ID="lblRatingText"></asp:Label>
                </asp:Panel>
                 <asp:Panel ID="pnlChangeCashReceipt" Visible="false" runat="server">  
                    <asp:ValidationSummary id="validationSummary7" CssClass="bodytxtValidationMsg" runat="server" ShowSummary="true" Enabled="true" ValidationGroup="CashReceipt"></asp:ValidationSummary>                  
                    <div>
                        <div>
                            <div style="height:30px; margin-top:10px;">
                                <div style="width:120px; float:left;">
                                <asp:Label ID="lbl" runat="server" Text="Cash Receipt Number"></asp:Label><asp:RequiredFieldValidator
                                    ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter a sales cash receipt" ControlToValidate="txtbxReceiptNo" ValidationGroup="CashReceipt">*</asp:RequiredFieldValidator></div>
                                <asp:TextBox ID="txtbxReceiptNo" runat="server" CssClass="formField width150" AutoPostBack="true" MaxLength="16"></asp:TextBox>
                            </div>
	                        <div style="height:30px; margin-top:10px;" runat="server" id="divCashReceiptVal1" visible="false">
                                <div style="width:120px; float:left;">
                                <asp:Label ID="Label2" runat="server" Text="Original Value"></asp:Label></div>
                                <asp:TextBox ID="txtbxReceiptVal1" runat="server" CssClass="formField width150" Enabled="false" MaxLength="8"></asp:TextBox>
                            </div>
                            <div style="margin-bottom:10px;" runat="server" id="divCashReceiptVal2" visible="false">
                                <div style="width:120px; float:left;">
                                <asp:Label ID="Label3" runat="server" Text="New Value"></asp:Label><asp:RequiredFieldValidator
                                    ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please Enter Value To Be modified" ControlToValidate="txtbxReceiptVal2" ValidationGroup="CashReceipt">*</asp:RequiredFieldValidator></div>
                                <asp:TextBox ID="txtbxReceiptVal2" runat="server" CssClass="formField width150" MaxLength="8"></asp:TextBox>
                            </div>
                        </div>
                        <asp:Button ID="btnChangeCashReceipt" runat="server" Text="Update" ValidationGroup="CashReceipt" visible="false" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlUpdateInvoiceTitle" Visible="false" runat="server">
                     <asp:ValidationSummary id="validationSummary8" runat="server" CssClass="bodytxtValidationMsg" ShowSummary="true" Enabled="true" ValidationGroup="InvoiceTitleFetch"></asp:ValidationSummary>
                     <asp:ValidationSummary id="validationSummary9" runat="server" CssClass="bodytxtValidationMsg" ShowSummary="true" Enabled="true" ValidationGroup="UpdateInvoiceTitle"></asp:ValidationSummary>
                     <asp:Label runat="server" ID="lblInvoiceTitle" Text="<br /> * Invalid Work Order ID" Visible="false" CssClass="bodytxtValidationMsg"></asp:Label>
                     <div>
                        <div style="margin-top:10px; margin-bottom:10px;">
                            <div>                                 
                                 Work Order ID <br />
                                 <asp:TextBox ID="txtWOIDInvTitle" AutoPostBack="true" runat="server" Width="180" MaxLength="15"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please enter a Work Order ID" 
                                     ControlToValidate="txtWOIDInvTitle" ValidationGroup="UpdateInvoiceTitle" ForeColor="#EDEDEB">*</asp:RequiredFieldValidator>                                 
                                 <div id="divTitle" runat="server" visible="false">
                                     Current Invoice Title <br />
                                     <asp:TextBox runat="server" ID="txtCurTitle" Enabled="false" Width="245"></asp:TextBox><br /><br />
                                     New Invoice Title <br />
                                     <asp:TextBox ID="txtInvoiceTitleNew" runat="server" Width="245" MaxLength="600"></asp:TextBox><br /><br />                          
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Please enter an Invoice Title " 
                                     ControlToValidate="txtInvoiceTitleNew" ValidationGroup="UpdateInvoiceTitle" ForeColor="#EDEDEB" >*</asp:RequiredFieldValidator>
                                     <asp:Button ID="btnUpdateInvoiceTitle" runat="server" Text="Update" ValidationGroup="UpdateInvoiceTitle" />
                                     <asp:Button ID="btnCancelInvoiceTitle" runat="server" Text="Cancel" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>                    
              </div>          
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
                 </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
        <ProgressTemplate >
            <div>
                <img  align="middle" src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
        </asp:UpdateProgress>
        <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="Server" />
                
      </div>
</asp:Content>
