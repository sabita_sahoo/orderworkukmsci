

Partial Public Class PostMessages
    Inherits System.Web.UI.Page
    Protected WithEvents UCDateRange1 As UCDateRange
    Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not IsNothing(Request("MsgID")) Then
                ViewState("MsgID") = Request("MsgID")
            Else
                ViewState("MsgID") = 0
            End If

            'If ViewState("MsgID") <> 0 Then
            PopulateMessageDetails()
            'End If
        End If
        UCDateRange1.valSumm.Visible = False
        'CType(UCDateRange1.FindControl("reqFronDate"), RequiredFieldValidator).Enabled = False
        'CType(UCDateRange1.FindControl("reqToDate"), RequiredFieldValidator).Enabled = False
        divValidationMain.Visible = False
    End Sub
    Private Sub PopulateMessageDetails()
        Dim dsMsgDetails As DataSet

        dsMsgDetails = ws.WSContact.GetMessageDetails(ViewState("MsgID"))

        If dsMsgDetails.Tables(0).Rows.Count <> 0 Then
            Dim dvAOE As New DataView
            dvAOE = dsMsgDetails.Tables(0).DefaultView
            rptMessagesAOE.DataSource = dvAOE
            rptMessagesAOE.DataBind()
        End If
        If ViewState("MsgID") <> 0 Then
            If dsMsgDetails.Tables(1).Rows.Count <> 0 Then
                With dsMsgDetails.Tables(1).Rows(0)
                    txtSubject.Text = .Item("Subject")
                    txtMessage.Text = .Item("Message").ToString.Replace("<BR>", Chr(13))
                    If .Item("Status") = ApplicationSettings.WOStatusID.Cancelled Then
                        chkMarkDeleted.Checked = True
                    Else
                        chkMarkDeleted.Checked = False
                    End If
                   
                    If .Item("ISImp") = True Then
                        chkIsImp.Checked = True
                    Else
                        chkIsImp.Checked = False
                    End If
                    If .Item("IsDisplayLogin") = True Then
                        chkIsDisplayLogin.Checked = True
                    Else
                        chkIsDisplayLogin.Checked = False
                    End If

                    UCDateRange1.txtFromDate.Text = dsMsgDetails.Tables(1).Rows(0)("FromDisplayDate")
                    UCDateRange1.txtToDate.Text = dsMsgDetails.Tables(1).Rows(0)("EndDisplayDate")

                End With

            End If
        End If
        
    End Sub

    Private Sub btnConfirmPostMessage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmPostMessage.Click
        If chkMsgServicePartners.Checked = True Then
            'Dim SkillSet As String = GetSeletcedSkills()
            'If SkillSet = "" Then
            '    divValidationMain.Visible = True
            '    lblErr.Visible = True
            '    lblErr.Text = "Please select atleast one skill set for Supplier"
            'Else
            '    Page.Validate()
            '    SaveMessages()
            'End If
            Page.Validate()
            SaveMessages()
        Else
            divValidationMain.Visible = True
            lblErr.Visible = True
            lblErr.Text = "Please select either Suppliers to post a message"
        End If
    End Sub
  
    Public Function GetSeletcedSkills()
        Dim selectedIds As String = ""
        For Each Item As RepeaterItem In rptMessagesAOE.Items
            Dim chkBox As CheckBox = CType(Item.FindControl("Check"), CheckBox)
            If Not (chkBox Is Nothing) Then
                If chkBox.Checked Then
                    If selectedIds.Contains(CStr(CType(Item.FindControl("hdnWOProductID"), HtmlInputHidden).Value)) = False Then
                        If selectedIds = "" Then
                            selectedIds += "" & CStr(CType(Item.FindControl("hdnWOProductID"), HtmlInputHidden).Value)
                        Else
                            selectedIds += " ," & CStr(CType(Item.FindControl("hdnWOProductID"), HtmlInputHidden).Value)
                        End If
                    End If
                End If
            End If
        Next
        Return selectedIds
    End Function

    Private Sub SaveMessages()
        If Page.IsValid Then
            divValidationMain.Visible = False
            Dim Success As Integer
            Dim Status As Integer
            If chkMarkDeleted.Checked = True Then
                Status = ApplicationSettings.WOStatusID.Cancelled
            Else
                Status = ApplicationSettings.WOStatusID.Accepted
            End If

            'Check for posting messages
            If chkMsgServicePartners.Checked = True Then
                ViewState("ForContactClassID") = 2
            Else
                ViewState("ForContactClassID") = 2
            End If
            Dim SkillSet As String = GetSeletcedSkills()
            Dim ds As DataSet
            Dim message As String = txtMessage.Text.Trim.Replace(Chr(13), "<BR>")
            ds = ws.WSContact.PostMessage(ViewState("MsgID"), txtSubject.Text.Trim, message, ViewState("ForContactClassID"), Status, ApplicationSettings.CommentType.Messages, SkillSet, chkIsImp.Checked, chkIsDisplayLogin.Checked, UCDateRange1.txtFromDate.Text.Trim, UCDateRange1.txtToDate.Text.Trim)
            Success = ds.Tables("tblSuccess").Rows(0).Item("Success")
            If Success = 1 Then
                If (chkSendMail.Checked) Then
                    Emails.SendPostMessageMail(txtSubject.Text.Trim, message.Replace(ControlChars.Lf, "<BR>"), ds)
                End If
                Response.Redirect("ListMessages.aspx")
            Else
                lblErrMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
            End If
        Else
            divValidationMain.Visible = True
        End If
    End Sub

    Private Sub lnkbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnReset.Click
        chkSendMail.Checked = False
        If ViewState("MsgID") <> 0 Then
            PopulateMessageDetails()
        Else
            txtSubject.Text = ""
            txtMessage.Text = ""
            chkMarkDeleted.Checked = False
        End If
    End Sub

    Private Sub lnkbtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnBack.Click
        Response.Redirect("ListMessages.aspx")
    End Sub

    Private Sub chkMsgServicePartners_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMsgServicePartners.CheckedChanged
        If chkMsgServicePartners.Checked = True Then
            pnlSkillsArea.Visible = True
        Else
            pnlSkillsArea.Visible = False
        End If
    End Sub
End Class