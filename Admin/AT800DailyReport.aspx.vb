

Partial Public Class AT800DailyReport
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            For newDate As Integer = 0 To 365
                ddlDate.Items.Add(DateTime.Today.AddDays(newDate - 365).ToShortDateString())
            Next
            ddlDate.SelectedValue = DateTime.Today.ToShortDateString()
            ShowDailyReport()
        End If
    End Sub

    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged
        ShowDailyReport()
    End Sub

    Protected Function ShowDailyReport()
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetAT800DailyReport(DateTime.Parse(ddlDate.SelectedValue))
        lblTotalJobsSubmittedDaily.Text = ds.Tables(0).Rows(0)("TotalJobsSubmittedDaily")
        lblTotalJobsSubmittedCumulative.Text = ds.Tables(0).Rows(0)("TotalJobsSubmittedCumulative")
        lblJobsLiveDaily.Text = ds.Tables(0).Rows(0)("JobsLiveDaily")
        lblJobsLiveCumulative.Text = ds.Tables(0).Rows(0)("JobsLiveCumulative")
        lblJobsClosedDaily.Text = ds.Tables(0).Rows(0)("JobsClosedDaily")
        lblJobsClosedCumulative.Text = ds.Tables(0).Rows(0)("JobsClosedCumulative")
        lblJobsCancelledDaily.Text = ds.Tables(0).Rows(0)("JobsCancelledDaily")
        lblJobsCancelledCumulative.Text = ds.Tables(0).Rows(0)("JobsCancelledCumulative")
        ds = ws.WSWorkOrder.GetAT800DailyReportCancellations(DateTime.Parse(ddlDate.SelectedValue))
        lblCancelledJobsViewerDaily.Text = ds.Tables(0).Rows(0)("CancelledJobsViewerDaily")
        lblCancelledJobsViewerCumulative.Text = ds.Tables(0).Rows(0)("CancelledJobsViewerCumulative")
        lblCancelledJobsViewer2HoursDaily.Text = ds.Tables(0).Rows(0)("CancelledJobsViewer2HoursDaily")
        lblCancelledJobsViewer2HoursCumulative.Text = ds.Tables(0).Rows(0)("CancelledJobsViewer2HoursCumulative")
        lblCancelledJobsInstallerDaily.Text = ds.Tables(0).Rows(0)("CancelledJobsInstallerDaily")
        lblCancelledJobsInstallerCumulative.Text = ds.Tables(0).Rows(0)("CancelledJobsInstallerCumulative")
        Dim CompletionReasons As New Dictionary(Of String, String)
        CompletionReasons.Add("-7", "No: Fixed non 4G interference by alternative methods")
        CompletionReasons.Add("-6", "No: 4G issue identified and rectified")
        CompletionReasons.Add("-5", "No: Confirmed first visit installation is correct, further issues not 4G")
        CompletionReasons.Add("-4", "No: Filter Fitted - Interference remains but not 4G")
        CompletionReasons.Add("-3", "No: Filter Fitted - no interference (4G or Non 4G) was visible")
        CompletionReasons.Add("-2", "No: Filter Fitted - Non 4G interference resolved via alternative methods")
        CompletionReasons.Add("-1", "No: Filter Fitted - Resolved 4G Interference")
        CompletionReasons.Add("0", "No: Work completed successfully")
        CompletionReasons.Add("1", "Yes: Householder not present")
        CompletionReasons.Add("2", "Yes: Specialist installer team required")
        CompletionReasons.Add("3", "Yes: Installer unable to resolve")
        CompletionReasons.Add("4", "Yes: Platform change required")
        CompletionReasons.Add("5", "Yes: Bespoke solution required")
        CompletionReasons.Add("9", "No: Other (Please include details on the next page)")
        rptCompletionReasonClosedJobs.DataSource = CompletionReasons
        rptCompletionReasonClosedJobs.DataBind()
    End Function

    Protected Sub rptCompletionReasonClosedJobs_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptCompletionReasonClosedJobs.ItemDataBound
        Dim lblCompletionReasonsDaily As Label
        Dim lblCompletionReasonsCumulative As Label
        Dim hidKey As HiddenField
        lblCompletionReasonsDaily = CType(e.Item.FindControl("lblCompletionReasonsDaily"), Label)
        lblCompletionReasonsCumulative = CType(e.Item.FindControl("lblCompletionReasonsCumulative"), Label)
        hidKey = CType(e.Item.FindControl("hidKey"), HiddenField)
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetAT800DailyReportCompletions(DateTime.Parse(ddlDate.SelectedValue), hidKey.Value)
        lblCompletionReasonsDaily.Text = ds.Tables(0).Rows(0)("CompletionReasonsDaily")
        lblCompletionReasonsCumulative.Text = ds.Tables(0).Rows(0)("CompletionReasonsCumulative")
    End Sub
End Class