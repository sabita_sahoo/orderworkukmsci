Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Linq
Imports System.Collections.Generic
Imports Admin.AT800

Partial Public Class AT800RolloutReport
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs

    Shared IDColumn As String

    Public Property SortExpression As String
        Get
            If Not ViewState("SortExpression") Is Nothing Then
                Return ViewState("SortExpression").ToString()
            Else
                Return IDColumn
            End If
        End Get
        Private Set(ByVal value As String)
            ViewState("SortExpression") = value
        End Set
    End Property

    Public Property SortDirection As SortDirection
        Get
            If Not ViewState("SortDirection") Is Nothing Then
                Return ViewState("SortDirection")
            Else
                Return SortDirection.Ascending
            End If
        End Get
        Private Set(ByVal value As SortDirection)
            ViewState("SortDirection") = value
        End Set
    End Property

    Private Sub Page_Init1(sender As Object, e As System.EventArgs) Handles Me.Init

    End Sub


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            IDColumn = "Region"
            Dim StartRolloutReport As New RolloutReport()
            StartRolloutReport.CancellationReason = ""
            StartRolloutReport.FinalJobType = ""
            StartRolloutReport.IsIssue = ""
            StartRolloutReport.JobCancelled = Nothing
            StartRolloutReport.JobClosed = Nothing
            StartRolloutReport.JobCompletionReason = ""
            StartRolloutReport.JobSubmitted = Nothing
            StartRolloutReport.Notes = ""
            StartRolloutReport.OriginalJobType = ""
            StartRolloutReport.PostCode = ""
            StartRolloutReport.RC = ""
            StartRolloutReport.Region = ""
            lblSignOffSheetDate.Text = DateTime.Today.ToShortDateString()
            StartRolloutReport.SignOffSheetReceived = DateTime.Today.ToString()
            StartRolloutReport.TCCRefNo = ""
            StartRolloutReport.VisitDate = Nothing
            StartRolloutReport.WORefNo = ""
            Session("DefaultRolloutReport") = StartRolloutReport
            Bind(SortDirection.Ascending, "Region")
        End If
    End Sub

    Public Function Bind(ByVal selectedSortDirection As SortDirection, ByVal selectedSortExpression As String)
        Dim DefaultRolloutReport As RolloutReport
        DefaultRolloutReport = CType(Session("DefaultRolloutReport"), RolloutReport)
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetAT800RolloutReport(DefaultRolloutReport.Region, DefaultRolloutReport.JobSubmitted, DefaultRolloutReport.WORefNo, DefaultRolloutReport.TCCRefNo, DefaultRolloutReport.PostCode, DefaultRolloutReport.VisitDate, DefaultRolloutReport.RC, DefaultRolloutReport.IsIssue, DefaultRolloutReport.JobCancelled, DefaultRolloutReport.CancellationReason, DefaultRolloutReport.SignOffSheetReceived, DefaultRolloutReport.JobClosed, DefaultRolloutReport.OriginalJobType, DefaultRolloutReport.FinalJobType, DefaultRolloutReport.JobCompletionReason, DefaultRolloutReport.Notes)
        Dim RolloutReport As New List(Of RolloutReport)
        For Each dr As DataRow In ds.Tables(0).Rows
            Dim tempRolloutReport As New RolloutReport
            If Not IsDBNull(dr("SignOffSheetID")) Then
                tempRolloutReport.SignOffSheetID = Integer.Parse(dr("SignOffSheetID").ToString())
            End If
            If Not IsDBNull(dr("WOID")) Then
                tempRolloutReport.WOID = Integer.Parse(dr("WOID").ToString())
            End If
            tempRolloutReport.Region = dr("Region").ToString()
            If Not dr("Job Submitted") Is System.DBNull.Value Then
                tempRolloutReport.JobSubmitted = DateTime.Parse(dr("Job Submitted"))
            End If
            tempRolloutReport.WORefNo = dr("WO Ref No").ToString()
            tempRolloutReport.TCCRefNo = dr("TCC Ref No").ToString()
            tempRolloutReport.PostCode = dr("Postcode").ToString()
            If Not dr("Visit Date") Is System.DBNull.Value Then
                tempRolloutReport.VisitDate = DateTime.Parse(dr("Visit Date").ToString())
            End If
            tempRolloutReport.RC = dr("RC").ToString()
            tempRolloutReport.IsIssue = dr("Issue").ToString()
            If Not dr("Job Cancelled") Is System.DBNull.Value Then
                tempRolloutReport.JobCancelled = DateTime.Parse(dr("Job Cancelled").ToString())
            End If
            tempRolloutReport.CancellationReason = dr("Cancellation Reason").ToString()
            If Not dr("Sign off sheet Received") Is System.DBNull.Value Then
                tempRolloutReport.SignOffSheetReceived = DateTime.Parse(dr("Sign off sheet Received").ToString())
            End If
            If Not dr("Job Closed") Is System.DBNull.Value Then
                tempRolloutReport.JobClosed = DateTime.Parse(dr("Job Closed").ToString())
            End If
            tempRolloutReport.OriginalJobType = dr("Original Job Type").ToString()
            tempRolloutReport.FinalJobType = dr("Final Job Type").ToString()
            tempRolloutReport.JobCompletionReason = dr("Job Completion Reason").ToString()
            tempRolloutReport.Notes = dr("Notes").ToString()
            tempRolloutReport.FilePath = dr("FilePath").ToString()
            tempRolloutReport.CustomerName = dr("Customer Name").ToString()
            tempRolloutReport.CustomerAddress = dr("Customer Address").ToString()
            tempRolloutReport.CustomerPhone = dr("Customer Phone").ToString()
            tempRolloutReport.InstallationTime = dr("Installation Time").ToString()
            tempRolloutReport.EngineerInstructions = dr("Engineer Instructions").ToString()
            tempRolloutReport.OWInstructions = dr("OW Instructions").ToString()
            RolloutReport.Add(tempRolloutReport)
        Next
        grdView.DataSource = Sort(RolloutReport)
        grdView.DataBind()
        txtTotalDisplay.Text = RolloutReport.Count.ToString() + " Records Found"
    End Function

    Protected Sub grdView_Export(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim DefaultRolloutReport As RolloutReport
        DefaultRolloutReport = CType(Session("DefaultRolloutReport"), RolloutReport)
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetAT800RolloutReport(DefaultRolloutReport.Region, DefaultRolloutReport.JobSubmitted, DefaultRolloutReport.WORefNo, DefaultRolloutReport.TCCRefNo, DefaultRolloutReport.PostCode, DefaultRolloutReport.VisitDate, DefaultRolloutReport.RC, DefaultRolloutReport.IsIssue, DefaultRolloutReport.JobCancelled, DefaultRolloutReport.CancellationReason, DefaultRolloutReport.SignOffSheetReceived, DefaultRolloutReport.JobClosed, DefaultRolloutReport.OriginalJobType, DefaultRolloutReport.FinalJobType, DefaultRolloutReport.JobCompletionReason, DefaultRolloutReport.Notes)
        Dim RolloutReport As New List(Of RolloutReport)
        For Each dr As DataRow In ds.Tables(0).Rows
            Dim tempRolloutReport As New RolloutReport
            If Not IsDBNull(dr("WOID")) Then
                tempRolloutReport.WOID = Integer.Parse(dr("WOID").ToString())
            End If
            tempRolloutReport.Region = dr("Region").ToString()
            If Not dr("Job Submitted") Is System.DBNull.Value Then
                tempRolloutReport.JobSubmitted = DateTime.Parse(dr("Job Submitted"))
            End If
            tempRolloutReport.WORefNo = dr("WO Ref No").ToString()
            tempRolloutReport.TCCRefNo = dr("TCC Ref No").ToString()
            tempRolloutReport.PostCode = dr("Postcode").ToString()
            If Not dr("Visit Date") Is System.DBNull.Value Then
                tempRolloutReport.VisitDate = DateTime.Parse(dr("Visit Date").ToString())
            End If
            tempRolloutReport.RC = dr("RC").ToString()
            tempRolloutReport.IsIssue = dr("Issue").ToString()
            If Not dr("Job Cancelled") Is System.DBNull.Value Then
                tempRolloutReport.JobCancelled = DateTime.Parse(dr("Job Cancelled").ToString())
            End If
            tempRolloutReport.CancellationReason = dr("Cancellation Reason").ToString()
            If Not dr("Sign off sheet Received") Is System.DBNull.Value Then
                tempRolloutReport.SignOffSheetReceived = DateTime.Parse(dr("Sign off sheet Received").ToString())
            End If
            If Not dr("Job Closed") Is System.DBNull.Value Then
                tempRolloutReport.JobClosed = DateTime.Parse(dr("Job Closed").ToString())
            End If
            tempRolloutReport.OriginalJobType = dr("Original Job Type").ToString()
            tempRolloutReport.FinalJobType = dr("Final Job Type").ToString()
            tempRolloutReport.JobCompletionReason = dr("Job Completion Reason").ToString()
            tempRolloutReport.Notes = dr("Notes").ToString()
            tempRolloutReport.FilePath = dr("FilePath").ToString()
            tempRolloutReport.CustomerName = dr("Customer Name").ToString()
            tempRolloutReport.CustomerAddress = dr("Customer Address").ToString()
            tempRolloutReport.CustomerPhone = dr("Customer Phone").ToString()
            tempRolloutReport.InstallationTime = dr("Installation Time").ToString()
            tempRolloutReport.EngineerInstructions = dr("Engineer Instructions").ToString()
            tempRolloutReport.OWInstructions = dr("OW Instructions").ToString()
            RolloutReport.Add(tempRolloutReport)
        Next
        Dim arr As New ArrayList
        arr.Add("SignOffSheetID")
        arr.Add("WOID")
        arr.Add("FilePath")
        DataSetToExcel.Convert(Sort(RolloutReport), Response, arr)
    End Sub

    Protected Sub grdView_Filter(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList
        Dim txt As TextBox
        Dim defaultRolloutReport As RolloutReport
        defaultRolloutReport = CType(Session("DefaultRolloutReport"), RolloutReport)
        If sender.GetType() Is GetType(DropDownList) Then
            ddl = CType(sender, DropDownList)
            If ddl.ID = "ddlRegion" Then
                defaultRolloutReport.Region = ddl.SelectedValue
            ElseIf ddl.ID = "ddlJobSubmitted" Then
                If ddl.SelectedValue = "" Then
                    defaultRolloutReport.JobSubmitted = Nothing
                Else
                    defaultRolloutReport.JobSubmitted = ddl.SelectedValue
                End If
            ElseIf ddl.ID = "ddlWORefNo" Then
                defaultRolloutReport.WORefNo = ddl.SelectedValue
            ElseIf ddl.ID = "ddlTCCRefNo" Then
                defaultRolloutReport.TCCRefNo = ddl.SelectedValue
            ElseIf ddl.ID = "ddlVisitDate" Then
                If ddl.SelectedValue = "" Then
                    defaultRolloutReport.VisitDate = Nothing
                Else
                    defaultRolloutReport.VisitDate = ddl.SelectedValue
                End If
            ElseIf ddl.ID = "ddlRC" Then
                defaultRolloutReport.RC = ddl.SelectedValue
            ElseIf ddl.ID = "ddlIsIssue" Then
                defaultRolloutReport.IsIssue = ddl.SelectedValue
            ElseIf ddl.ID = "ddlJobCancelled" Then
                If ddl.SelectedValue = "" Then
                    defaultRolloutReport.JobCancelled = Nothing
                Else
                    defaultRolloutReport.JobCancelled = ddl.SelectedValue
                End If
            ElseIf ddl.ID = "ddlCancellationReason" Then
                defaultRolloutReport.CancellationReason = ddl.SelectedValue
            ElseIf ddl.ID = "ddlSignOffSheetReceived" Then
                If ddl.SelectedValue = "" Then
                    defaultRolloutReport.SignOffSheetReceived = Nothing
                    lblSignOffSheetDate.Text = "ALL Dates"
                Else
                    defaultRolloutReport.SignOffSheetReceived = ddl.SelectedValue
                    lblSignOffSheetDate.Text = ddl.SelectedValue
                End If
            ElseIf ddl.ID = "ddlJobClosed" Then
                If ddl.SelectedValue = "" Then
                    defaultRolloutReport.JobClosed = Nothing
                Else
                    defaultRolloutReport.JobClosed = ddl.SelectedValue
                End If
            ElseIf ddl.ID = "ddlOriginalJobType" Then
                defaultRolloutReport.OriginalJobType = ddl.SelectedValue
            ElseIf ddl.ID = "ddlFinalJobType" Then
                defaultRolloutReport.FinalJobType = ddl.SelectedValue
            ElseIf ddl.ID = "ddlJobCompletionReason" Then
                defaultRolloutReport.JobCompletionReason = ddl.SelectedValue
            ElseIf ddl.ID = "ddlNotes" Then
                defaultRolloutReport.Notes = ddl.SelectedValue
            End If
        ElseIf sender.GetType() Is GetType(TextBox) Then
            txt = CType(sender, TextBox)
            If txt.ID = "txtPostCode" Then
                defaultRolloutReport.PostCode = txt.Text
            ElseIf txt.ID = "txtWORefNo" Then
                defaultRolloutReport.WORefNo = txt.Text
            ElseIf txt.ID = "txtTCCRefNo" Then
                defaultRolloutReport.TCCRefNo = txt.Text
            End If
        End If
        Session("DefaultRolloutReport") = defaultRolloutReport
        Bind(SortDirection, SortExpression)
    End Sub

    Protected Function Sort(ByVal preSortedRolloutReport As List(Of RolloutReport)) As List(Of RolloutReport)
        Dim sortedRolloutReport As IEnumerable(Of RolloutReport)
        If SortDirection = SortDirection.Ascending Then
            If SortExpression = "Region" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.Region)
            End If
            If SortExpression = "JobSubmitted" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.JobSubmitted)
            End If
            If SortExpression = "WORefNo" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.WORefNo)
            End If
            If SortExpression = "TCCRefNo" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.TCCRefNo)
            End If
            If SortExpression = "PostCode" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.PostCode)
            End If
            If SortExpression = "VisitDate" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.VisitDate)
            End If
            If SortExpression = "RC" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.RC)
            End If
            If SortExpression = "IsIssue" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.IsIssue)
            End If
            If SortExpression = "JobCancelled" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.JobCancelled)
            End If
            If SortExpression = "CancellationReason" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.CancellationReason)
            End If
            If SortExpression = "SignOffSheetReceived" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.SignOffSheetReceived)
            End If
            If SortExpression = "JobClosed" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.JobClosed)
            End If
            If SortExpression = "OriginalJobType" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.OriginalJobType)
            End If
            If SortExpression = "FinalJobType" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.FinalJobType)
            End If
            If SortExpression = "JobCompletionReason" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.JobCompletionReason)
            End If
            If SortExpression = "Notes" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.Notes)
            End If
            If SortExpression = "CustomerName" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.CustomerName)
            End If
            If SortExpression = "CustomerAddress" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.CustomerAddress)
            End If
            If SortExpression = "CustomerPhone" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.CustomerPhone)
            End If
            If SortExpression = "InstallationTime" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.InstallationTime)
            End If
            If SortExpression = "EngineerInstructions" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.EngineerInstructions)
            End If
            If SortExpression = "OWInstructions" Then
                sortedRolloutReport = preSortedRolloutReport.OrderBy(Function(x) x.OWInstructions)
            End If
        Else
            If SortExpression = "Region" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.Region)
            End If
            If SortExpression = "JobSubmitted" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.JobSubmitted)
            End If
            If SortExpression = "WORefNo" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.WORefNo)
            End If
            If SortExpression = "TCCRefNo" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.TCCRefNo)
            End If
            If SortExpression = "PostCode" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.PostCode)
            End If
            If SortExpression = "VisitDate" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.VisitDate)
            End If
            If SortExpression = "RC" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.RC)
            End If
            If SortExpression = "IsIssue" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.IsIssue)
            End If
            If SortExpression = "JobCancelled" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.JobCancelled)
            End If
            If SortExpression = "CancellationReason" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.CancellationReason)
            End If
            If SortExpression = "SignOffSheetReceived" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.SignOffSheetReceived)
            End If
            If SortExpression = "JobClosed" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.JobClosed)
            End If
            If SortExpression = "OriginalJobType" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.OriginalJobType)
            End If
            If SortExpression = "FinalJobType" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.FinalJobType)
            End If
            If SortExpression = "JobCompletionReason" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.JobCompletionReason)
            End If
            If SortExpression = "Notes" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.Notes)
            End If
            If SortExpression = "CustomerName" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.CustomerName)
            End If
            If SortExpression = "CustomerAddress" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.CustomerAddress)
            End If
            If SortExpression = "CustomerPhone" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.CustomerPhone)
            End If
            If SortExpression = "InstallationTime" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.InstallationTime)
            End If
            If SortExpression = "EngineerInstructions" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.EngineerInstructions)
            End If
            If SortExpression = "OWInstructions" Then
                sortedRolloutReport = preSortedRolloutReport.OrderByDescending(Function(x) x.OWInstructions)
            End If
        End If
        Return sortedRolloutReport.ToList()
    End Function

    Protected Sub grdView_DataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then
            Dim ds As DataSet
            ds = ws.WSWorkOrder.GetAT800RolloutValues()
            'Dim MinimumValues As DataTable
            Dim Dates As DataTable
            'MinimumValues = ds.Tables(0)
            Dates = ds.Tables(0)
            Dim CancellationReasons As DataTable
            CancellationReasons = ds.Tables(3)
            Dim JobTypes As DataTable
            JobTypes = ds.Tables(1)
            Dim JobCompletionReasons As DataTable
            JobCompletionReasons = ds.Tables(2)
            Dim Regions As DataTable
            Regions = ds.Tables(4)
            Dim RCs As DataTable
            RCs = ds.Tables(5)
            Dim RolloutReportList As List(Of RolloutReport)
            RolloutReportList = CType(grdView.DataSource, List(Of RolloutReport))
            Dim defaultRolloutReport As RolloutReport
            defaultRolloutReport = CType(Session("DefaultRolloutReport"), RolloutReport)
            Dim ddlRegion As DropDownList
            ddlRegion = CType(e.Row.FindControl("ddlRegion"), DropDownList)
            ddlRegion.DataSource = Regions
            ddlRegion.DataBind()
            ddlRegion.SelectedValue = defaultRolloutReport.Region
            Dim ddlJobSubmitted As DropDownList
            ddlJobSubmitted = CType(e.Row.FindControl("ddlJobSubmitted"), DropDownList)
            'If Not MinimumValues.Rows(0).Item("MinJobSubmitted").GetType() = System.DBNull.Value.GetType() Then
            'For newDate As Integer = -(DateTime.Today - DateTime.Parse(MinimumValues.Rows(0).Item("MinJobSubmitted").ToString())).Days - 1 To 0
            '    ddlJobSubmitted.Items.Add(DateTime.Today.AddDays(newDate).ToShortDateString())
            'Next
            ddlJobSubmitted.DataSource = Dates
            ddlJobSubmitted.DataBind()
            If defaultRolloutReport.JobSubmitted.HasValue Then
                ddlJobSubmitted.SelectedValue = defaultRolloutReport.JobSubmitted.Value.ToShortDateString()
            End If
            'End If
            Dim txtWORefNo As TextBox
            txtWORefNo = CType(e.Row.FindControl("txtWORefNo"), TextBox)
            txtWORefNo.Text = defaultRolloutReport.WORefNo
            Dim txtTCCRefNo As TextBox
            txtTCCRefNo = CType(e.Row.FindControl("txtTCCRefNo"), TextBox)
            txtTCCRefNo.Text = defaultRolloutReport.TCCRefNo
            Dim txtPostCode As TextBox
            txtPostCode = CType(e.Row.FindControl("txtPostCode"), TextBox)
            txtPostCode.Text = defaultRolloutReport.PostCode
            Dim ddlVisitDate As DropDownList
            ddlVisitDate = CType(e.Row.FindControl("ddlVisitDate"), DropDownList)
            Dim visitDate As DateTime
            'If Not MinimumValues.Rows(0).Item("MinVisitDate").GetType() = System.DBNull.Value.GetType() Then
            '    visitDate = DateTime.Parse(MinimumValues.Rows(0).Item("MinVisitDate").ToString())
            'Else
            '    visitDate = DateTime.Today
            'End If
            'For newDate As Integer = -(DateTime.Today - visitDate).Days - 1 To 90
            '    ddlVisitDate.Items.Add(DateTime.Today.AddDays(newDate).ToShortDateString())
            'Next
            ddlVisitDate.DataSource = Dates
            ddlVisitDate.DataBind()
            If defaultRolloutReport.VisitDate.HasValue Then
                ddlVisitDate.SelectedValue = defaultRolloutReport.VisitDate.Value.ToShortDateString()
            End If
            Dim ddlRC As DropDownList
            ddlRC = CType(e.Row.FindControl("ddlRC"), DropDownList)
            ddlRC.DataSource = RCs
            ddlRC.DataBind()
            ddlRC.SelectedValue = defaultRolloutReport.RC
            Dim ddlIsIssue As DropDownList
            ddlIsIssue = CType(e.Row.FindControl("ddlIsIssue"), DropDownList)
            ddlIsIssue.Items.Add("Y")
            ddlIsIssue.Items.Add("N")
            ddlIsIssue.SelectedValue = defaultRolloutReport.IsIssue
            Dim ddlJobCancelled As DropDownList
            ddlJobCancelled = CType(e.Row.FindControl("ddlJobCancelled"), DropDownList)
            'If Not MinimumValues.Rows(0).Item("MinJobCancelled").GetType() = System.DBNull.Value.GetType() Then
            '    For newDate As Integer = -(DateTime.Today - DateTime.Parse(MinimumValues.Rows(0).Item("MinJobCancelled").ToString())).Days - 1 To 0
            '        ddlJobCancelled.Items.Add(DateTime.Today.AddDays(newDate).ToShortDateString())
            '    Next
            'End If
            ddlJobCancelled.DataSource = Dates
            ddlJobCancelled.DataBind()
            If defaultRolloutReport.JobCancelled.HasValue Then
                ddlJobCancelled.SelectedValue = defaultRolloutReport.JobCancelled.Value.ToShortDateString()
            End If
            Dim ddlCancellationReason As DropDownList
            ddlCancellationReason = CType(e.Row.FindControl("ddlCancellationReason"), DropDownList)
            ddlCancellationReason.DataSource = CancellationReasons
            ddlCancellationReason.DataBind()
            ddlCancellationReason.SelectedValue = defaultRolloutReport.CancellationReason
            Dim ddlSignOffSheetReceived As DropDownList
            ddlSignOffSheetReceived = CType(e.Row.FindControl("ddlSignOffSheetReceived"), DropDownList)
            'If Not MinimumValues.Rows(0).Item("MinSignOffSheetReceived").GetType() = System.DBNull.Value.GetType() Then
            'For newDate As Integer = -(DateTime.Today - DateTime.Parse(MinimumValues.Rows(0).Item("MinSignOffSheetReceived").ToString())).Days - 1 To 0
            '    ddlSignOffSheetReceived.Items.Add(DateTime.Today.AddDays(newDate).ToShortDateString())
            'Next
            ddlSignOffSheetReceived.DataSource = Dates
            ddlSignOffSheetReceived.DataBind()
            If defaultRolloutReport.SignOffSheetReceived.HasValue Then
                ddlSignOffSheetReceived.SelectedValue = defaultRolloutReport.SignOffSheetReceived.Value.ToShortDateString()
            End If
            'End If
            Dim ddlJobClosed As DropDownList
            ddlJobClosed = CType(e.Row.FindControl("ddlJobClosed"), DropDownList)
            'If Not MinimumValues.Rows(0).Item("MinSignOffSheetReceived").GetType() = System.DBNull.Value.GetType() Then
            'For newDate As Integer = -(DateTime.Today - DateTime.Parse(MinimumValues.Rows(0).Item("MinJobClosed").ToString())).Days - 1 To 0
            '    ddlJobClosed.Items.Add(DateTime.Today.AddDays(newDate).ToShortDateString())
            'Next
            ddlJobClosed.DataSource = Dates
            ddlJobClosed.DataBind()
            If defaultRolloutReport.JobClosed.HasValue Then
                ddlJobClosed.SelectedValue = defaultRolloutReport.JobClosed.Value.ToShortDateString()
            End If
            'End If
            Dim ddlOriginalJobType As DropDownList
            ddlOriginalJobType = CType(e.Row.FindControl("ddlOriginalJobType"), DropDownList)
            ddlOriginalJobType.DataSource = JobTypes
            ddlOriginalJobType.DataBind()
            ddlOriginalJobType.SelectedValue = defaultRolloutReport.OriginalJobType
            Dim ddlFinalJobType As DropDownList
            ddlFinalJobType = CType(e.Row.FindControl("ddlFinalJobType"), DropDownList)
            ddlFinalJobType.DataSource = JobTypes
            ddlFinalJobType.DataBind()
            ddlFinalJobType.SelectedValue = defaultRolloutReport.FinalJobType
            Dim ddlJobCompletionReason As DropDownList
            ddlJobCompletionReason = CType(e.Row.FindControl("ddlJobCompletionReason"), DropDownList)
            ddlJobCompletionReason.DataSource = JobCompletionReasons
            ddlJobCompletionReason.DataBind()
            ddlJobCompletionReason.SelectedValue = defaultRolloutReport.JobCompletionReason
            Session("CancellationReasons") = CancellationReasons
            Session("JobTypes") = JobTypes
            Session("JobCompletionReasons") = JobCompletionReasons
        ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            Dim txtVisitDate As TextBox
            'Dim ddlVisitDate As DropDownList
            'Dim ddlJobCancelled As DropDownList
            Dim ddlCancellationReason As DropDownList
            Dim ddlOriginalJobType As DropDownList
            Dim ddlFinalJobType As DropDownList
            Dim ddlJobCompletionReason As DropDownList
            Dim imgFilePath As HyperLink
            imgFilePath = CType(e.Row.FindControl("imgFilePath"), HyperLink)
            If String.IsNullOrEmpty(imgFilePath.NavigateUrl) Then
                imgFilePath.Visible = False
            Else
                imgFilePath.NavigateUrl = getlink(imgFilePath.NavigateUrl)
            End If

            txtVisitDate = CType(e.Row.FindControl("txtVisitDate"), TextBox)
            'ddlVisitDate = CType(e.Row.FindControl("ddlVisitDate"), DropDownList)
            'ddlJobCancelled = CType(e.Row.FindControl("ddlJobCancelled"), DropDownList)
            ddlCancellationReason = CType(e.Row.FindControl("ddlCancellationReason"), DropDownList)
            ddlOriginalJobType = CType(e.Row.FindControl("ddlOriginalJobType"), DropDownList)
            ddlFinalJobType = CType(e.Row.FindControl("ddlFinalJobType"), DropDownList)
            ddlJobCompletionReason = CType(e.Row.FindControl("ddlJobCompletionReason"), DropDownList)
            For newDate As Integer = -(DateTime.Today - DateTime.Parse("2013-01-01")).Days To 365
                'ddlVisitDate.Items.Add(DateTime.Today.AddDays(newDate).ToShortDateString())
                'ddlJobCancelled.Items.Add(DateTime.Today.AddDays(newDate).ToShortDateString())
            Next
            'For Each tempListItem As ListItem In ddlVisitDate.Items
            '    If Not tempListItem.Value = "" Then
            '        tempListItem.Text = DateTime.Parse(tempListItem.Text).ToShortDateString()
            '        tempListItem.Value = DateTime.Parse(tempListItem.Text).ToShortDateString()
            '    End If
            'Next
            'For Each tempListItem As ListItem In ddlJobCancelled.Items
            '    If Not tempListItem.Value = "" Then
            '        tempListItem.Text = DateTime.Parse(tempListItem.Text).ToShortDateString()
            '        tempListItem.Value = DateTime.Parse(tempListItem.Text).ToShortDateString()
            '    End If
            'Next
            'If CType(e.Row.DataItem, RolloutReport).VisitDate.HasValue Then
            '    ddlVisitDate.SelectedValue = CType(e.Row.DataItem, RolloutReport).VisitDate.Value.ToShortDateString()
            'End If
            txtVisitDate.Text = CType(e.Row.DataItem, RolloutReport).VisitDate.Value.ToShortDateString()
            'If CType(e.Row.DataItem, RolloutReport).JobCancelled.HasValue Then
            '    ddlJobCancelled.SelectedValue = CType(e.Row.DataItem, RolloutReport).JobCancelled.Value.ToShortDateString()
            'End If
            ddlCancellationReason.DataSource = Session("CancellationReasons")
            ddlCancellationReason.DataBind()
            ddlCancellationReason.SelectedValue = CType(e.Row.DataItem, RolloutReport).CancellationReason
            If ddlCancellationReason.SelectedValue = "" Then
                For listItemIndex As Integer = ddlCancellationReason.Items.Count To 2 Step -1
                    ddlCancellationReason.Items.RemoveAt(listItemIndex - 1)
                Next
            Else
                ddlCancellationReason.Items.RemoveAt(0)
            End If
            ddlOriginalJobType.DataSource = Session("JobTypes")
            ddlOriginalJobType.DataBind()
            ddlOriginalJobType.SelectedValue = CType(e.Row.DataItem, RolloutReport).OriginalJobType
            ddlFinalJobType.DataSource = Session("JobTypes")
            ddlFinalJobType.DataBind()
            ddlFinalJobType.SelectedValue = CType(e.Row.DataItem, RolloutReport).FinalJobType
            ddlJobCompletionReason.DataSource = Session("JobCompletionReasons")
            ddlJobCompletionReason.DataBind()
            ddlJobCompletionReason.SelectedValue = CType(e.Row.DataItem, RolloutReport).JobCompletionReason
            If ddlJobCompletionReason.SelectedValue = "" Then
                For listItemIndex As Integer = ddlJobCompletionReason.Items.Count To 2 Step -1
                    ddlJobCompletionReason.Items.RemoveAt(listItemIndex - 1)
                Next
            Else
                ddlJobCompletionReason.Items.RemoveAt(0)
            End If

        End If
    End Sub

    Public Function getlink(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachment")
                Dim str As String = filePath.Substring(index)
                str = ApplicationSettings.AttachmentDisplayPath(4) + str
                Return str.Replace("\", "/")
            End If
        End If
        Return ""
    End Function

    Protected Sub AddPleaseSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList
        ddl = CType(sender, DropDownList)
        ddl.Items.Insert(0, New ListItem("ALL", ""))
    End Sub

    Protected Sub AddEmpty(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList
        ddl = CType(sender, DropDownList)
        'If ddl.ID = "ddlCancellationReason" Or ddl.ID = "ddlJobCompletionReason" 
        ddl.Items.Insert(0, New ListItem("", ""))
    End Sub

    Protected Sub grdView_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs)
        If SortExpression = e.SortExpression Then
            If SortDirection = WebControls.SortDirection.Ascending Then
                SortDirection = WebControls.SortDirection.Descending
            Else
                SortDirection = WebControls.SortDirection.Ascending
            End If
        Else
            SortDirection = WebControls.SortDirection.Ascending
        End If
        SortExpression = e.SortExpression
        Bind(SortDirection, SortExpression)
    End Sub

    <System.Web.Script.Services.ScriptMethod(), _
    System.Web.Services.WebMethod()>
    Public Shared Function GetAt800PostCodes(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetAT800PostCodes(prefixText)
        Dim postCodes As New List(Of String)
        For Each dr As DataRow In ds.Tables(0).Rows
            postCodes.Add(dr(0))
        Next
        Return postCodes
    End Function

    <System.Web.Script.Services.ScriptMethod(), _
    System.Web.Services.WebMethod()>
    Public Shared Function GetAt800WORefNo(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetAT800WORefNo(prefixText)
        Dim woRefNo As New List(Of String)
        For Each dr As DataRow In ds.Tables(0).Rows
            woRefNo.Add(dr(0))
        Next
        Return woRefNo
    End Function

    <System.Web.Script.Services.ScriptMethod(), _
    System.Web.Services.WebMethod()>
    Public Shared Function GetAt800TCCRefNo(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetAT800TCCRefNo(prefixText)
        Dim tccRefNo As New List(Of String)
        For Each dr As DataRow In ds.Tables(0).Rows
            tccRefNo.Add(dr(0))
        Next
        Return tccRefNo
    End Function

    Public Sub AddJS(ByVal sender As Object, ByVal e As EventArgs)
        Dim ctrl As WebControl
        ctrl = CType(sender, WebControl)
        ctrl.Attributes.Add("onchange", "update(this,'" + ctrl.ID + "');")
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function AT800RolloutUpdate(WOID As Integer, TCCRefNo As String, VisitDate As String, CancellationReason As String, OriginalJobType As String, FinalJobType As String, JobCompletionReason As String, Notes As String, InputID As String) As String
        Dim ds As DataSet
        'ds = ws.WSWorkOrder.AT800RolloutUpdate(SignOffSheetID, WORefNo, TCCRefNo, VisitDate, JobCancelled, CancellationReason, OriginalJobType, FinalJobType, JobCompletionReason, Notes)
        'ds = ws.WSWorkOrder.AT800RolloutUpdate(SignOffSheetID, WORefNo, TCCRefNo, VisitDate, CancellationReason, OriginalJobType, FinalJobType, JobCompletionReason, Notes)
        ds = ws.WSWorkOrder.AT800RolloutUpdate(WOID, TCCRefNo, VisitDate, CancellationReason, OriginalJobType, FinalJobType, JobCompletionReason, Notes)
        Return InputID
    End Function

    Public Function GetFileIcon(ByVal fileName As String) As String

        Dim extn = fileName.Substring(fileName.LastIndexOf(".") + 1)
        Dim imgsrc As String
        Select Case extn.ToString.ToLower
            Case "doc"
                imgsrc = "images/icons/icon-word.jpg"
            Case "jpg", "jpeg", "gif", "png"
                imgsrc = "images/icons/icon-image.jpg"
            Case "pdf"
                imgsrc = "images/icons/icon-pdf.jpg"
            Case "xls"
                imgsrc = "images/icons/icon-excel.jpg"
            Case Else
                imgsrc = "images/icons/icon-attachement.gif"
        End Select
        Return imgsrc

    End Function

End Class