﻿Public Class SingleUserReport
    Inherits System.Web.UI.Page
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents lnkExportReportExcel As Global.System.Web.UI.WebControls.LinkButton
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Set property of UC to select only users with business area defined
            UCSearchContact1.ContactType = "BusinessAreaDefined"

            'lnkBackToListing.HRef = getBackToListingLink()

            UCDateRange1.txtToDate.Text = Date.Today.Date
            UCDateRange1.txtFromDate.Text = CType((DateTime.Now.Year & "/" & DateTime.Now.Month & "/" & 1), Date)
            '[ lnkBackToListing.HRef = getBackToListingLink()
            txtEmail.Text = ""
        End If
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                UCSearchContact1.BizDivID = ApplicationSettings.OWDEBizDivId
                ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
            Case ApplicationSettings.CountryUK
                UCSearchContact1.BizDivID = ApplicationSettings.OWUKBizDivId
                ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
        End Select


        If Not IsNothing(Request("companyId")) Then
            If Request("companyId") <> "" Then
                If UCDateRange1.DateValidate() Then
                    generateReport()
                End If
            End If
        End If
    End Sub
    Private Sub lnkBtnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnGenerateReport.Click
        If UCDateRange1.DateValidate() Then
            generateReport()
        End If
    End Sub
    Private Sub generateReport()
        Dim languageCode As String = ""
        ErrLabel.Visible = False
        If txtEmail.Text <> "" Then
            Page.Validate()
            If (Page.IsValid = False) Then
                divValidationMain.Visible = True
            Else
                divValidationMain.Visible = False

                Dim dsEmail As New DataSet

                dsEmail = ws.WSStandards.GetSingleUserReport(0, txtEmail.Text.Trim, UCDateRange1.txtFromDate.Text.ToString.Trim, UCDateRange1.txtToDate.Text.ToString.Trim)

                Session("SingleUserReportEmailwise") = dsEmail

                If dsEmail.Tables.Count > 0 Then
                    lnkBtnExportToExcel.Visible = True
                    pnlShowRecords.Visible = True
                    pnlNoRecords.Visible = False
                    pnlInitialize.Visible = False
                    If dsEmail.Tables(0).Rows.Count > 0 Then
                        rptShowRecords.DataSource = dsEmail.Tables(0)
                        rptShowRecords.DataBind()
                        lblCompanyName.Text = txtEmail.Text.Trim

                        lblFromDate.Text = UCDateRange1.txtFromDate.Text.ToString.Trim
                        lblToDate.Text = UCDateRange1.txtToDate.Text.ToString.Trim
                    Else
                        pnlNoRecords.Visible = True
                        pnlShowRecords.Visible = False
                        lnkBtnExportToExcel.Visible = False
                        pnlInitialize.Visible = False
                    End If
                    'If dsEmail.Tables.Count > 1 Then
                    '    If dsEmail.Tables(1).Rows.Count > 0 Then
                    '        rptInvoices.DataSource = dsEmail.Tables(1)
                    '        rptInvoices.DataBind()
                    '    End If
                    'End If

                Else
                    pnlNoRecords.Visible = True
                    pnlShowRecords.Visible = False
                    lnkBtnExportToExcel.Visible = False
                    pnlInitialize.Visible = False
                End If
                End If

        ElseIf (UCSearchContact1.ddlContact.SelectedIndex > 0) Or Not IsNothing(Request("companyId")) Then
        Dim ds As New DataSet
        If Request("companyId") <> "" Then
                ds = ws.WSStandards.GetSingleUserReport(Convert.ToInt32(Request("companyId")), "", UCDateRange1.txtFromDate.Text.ToString.Trim, UCDateRange1.txtToDate.Text.ToString.Trim)
            Else
                ds = ws.WSStandards.GetSingleUserReport(Convert.ToInt32(UCSearchContact1.ddlContact.SelectedValue), "", UCDateRange1.txtFromDate.Text.ToString.Trim, UCDateRange1.txtToDate.Text.ToString.Trim)

        End If
        Session("SingleUserReport") = ds

        If ds.Tables.Count > 0 Then

            lnkBtnExportToExcel.Visible = True

            pnlShowRecords.Visible = True
            pnlNoRecords.Visible = False
            pnlInitialize.Visible = False
            If ds.Tables(0).Rows.Count > 0 Then
                rptShowRecords.DataSource = ds.Tables(0)
                rptShowRecords.DataBind()
                lblCompanyName.Text = ds.Tables(0).Rows(0)("CompanyName").ToString
                lblFromDate.Text = UCDateRange1.txtFromDate.Text.ToString.Trim
                lblToDate.Text = UCDateRange1.txtToDate.Text.ToString.Trim
            Else
                pnlNoRecords.Visible = True
                pnlShowRecords.Visible = False

                lnkBtnExportToExcel.Visible = False

                pnlInitialize.Visible = False
            End If
                'If ds.Tables.Count > 1 Then
                '    If ds.Tables(1).Rows.Count > 0 Then
                '        rptInvoices.DataSource = ds.Tables(1)
                '        rptInvoices.DataBind()
                '    End If
                'End If
        Else
            pnlNoRecords.Visible = True
            pnlShowRecords.Visible = False

            lnkBtnExportToExcel.Visible = False

            pnlInitialize.Visible = False
        End If


            Else

        ErrLabel.Visible = True
        ErrLabel.Text = "* Please select a Company or Enter an Email"
            End If
    End Sub
    Private Sub lnkBtnExportToExcel_Click(sender As Object, e As EventArgs) Handles lnkBtnExportToExcel.Click
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=RepeaterExport.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New System.IO.StringWriter
        Dim hw As New HtmlTextWriter(sw)
        Dim tb As New Table()
        Dim tr1 As New TableRow()
        Dim cell1 As New TableCell()
        cell1.Controls.Add(pnlShowRecords)
        tr1.Cells.Add(cell1)
        Dim cell2 As New TableCell()
        cell2.Text = "&nbsp;"
        Dim tr2 As New TableRow()
        tr2.Cells.Add(cell2)
        tb.Rows.Add(tr1)
        tb.Rows.Add(tr2)
        tb.RenderControl(hw)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub
End Class