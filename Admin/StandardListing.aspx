﻿<%@ Page Title="OrderWork : Default Texts Listing" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="StandardListing.aspx.vb" Inherits="Admin.StandardListing" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


              <div id="divContent">

         

            

        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>

        

             <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">

         

         <tr>

            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->

            <input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />                          

            <input id="hdnPageNo"  value="1" type="hidden" name="hdnPageNo" runat="server">

                  <input id="hdnPageSize" value="25" type="hidden" name="hdnPageSize" runat="server">

                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate> 

                                <table width="100%" cellpadding="0" cellspacing="0">

                                    <tr>

                                    <td width="10px"></td>

                                    <td><asp:Label ID="lblMessage" CssClass="HeadingRed"  runat="server" Text="Default Texts Listing"></asp:Label></td>

                                    </tr>

                                    </table>

              <table style="margin-left:20px;">
      
        <tr>
            <td>
                      <div id="divForAdvanceSearch" runat="server" visible="true">
			            <div id="divrow2" style="width:100%;margin-top:20px;">
			                <div class="formTxt" style="width:150px; float:left; margin-right:10px;">
			                    Type
                               <asp:DropDownList id="ddlStdType" tabIndex="2" runat="server" Width="140"  CssClass="formField105">
                                   <asp:ListItem Text="All" Value=""></asp:ListItem>
                                   <asp:ListItem Text="Cancellation Reasons" Value="Cancellation Reasons" ></asp:ListItem>
                                   <asp:ListItem Text="Client Complaints" Value="Client Complaints" ></asp:ListItem>
                                   <asp:ListItem Text="Supplier Feedback" Value="Supplier Feedback" ></asp:ListItem>
                               </asp:DropDownList>
			                </div>    
			               <div class="formTxt" style="width:70px; float:left;margin-top:15px; ">
                                <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" Checked="true" />
			                 </div>
			            </div>
			        </div>
            
            </td>
                <td align="left" width="60px" valign="bottom">
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                        <asp:LinkButton ID="btnView" causesValidation=False runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
                    </div>
                </td>                
            </tr>
      
      </table>

            <table >

            <tr>

            <td class="paddingL30 padB21" valign="top" padB21 paddingTop1>

            <asp:Label ID="lblMsg" runat="server" CssClass="bodytxtValidationMsg"></asp:Label>                

            </td>

            </tr>

            </table>                      

                        

                 

                                          

                  

                        

                               

                               

                  <hr style="background-color:#993366;height:5px;margin:0px;" />

                  

                    <asp:Panel ID="pnlListing" runat="server">    

                                <asp:GridView ID="gvStandards"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'

                         BorderColor="White"  dataKeyNames="StandardID" PagerSettings-Mode=NextPreviousFirstLast   PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          

               <EmptyDataTemplate>

               <table  width="100%" border="0" cellpadding="10" cellspacing="0">                            

                                    <tr>

                                          <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.

                                          </td>

                                    </tr>                   

                              </table>

                  

               </EmptyDataTemplate>

               <PagerTemplate>

                   

                  

                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">

                                    <tr>                                                                                   

                                          <td runat="server"  id="tdAddNewStandard" align="left" class="txtWelcome paddingL10">
                                            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;width:80px;"> 
                                                <asp:LinkButton OnClick="AddNewStandard" class="buttonText" style="cursor:hand;vertical-align:middle; line-height:20px;" id="btnProcessed"  runat=server><strong>Add New</strong></asp:LinkButton>                           
                                          </div></td>                                      

                                

                                          <td>

                                                <table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">

                                                  <tr>

                                                      <td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >

                                                            <TR>

                                                                  <td align="right" valign="middle">

                                                                    <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>

                                                                  </td>

                                                                  <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">

                                                                  <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                                                        <tr>

                                                                        <td align="right" width="36">

                                                                        <div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />

                                                                              </div>      

                                                                              <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />

                                                                              </div>                                                                                                                                             

                                                                        </td>

                                                                        <td width="50" align="center" style="margin-right:3px; ">                                                                              

                                                                              <asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">

                                                    </asp:DropDownList>

                                                                        </td>

                                                                        <td width="30" valign="bottom">

                                                                              <div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />

                                                                              </div>

                                                                              <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />

                                                                              </div>

                                                                        </td>

                                                                        </tr>

                                                                  </table>

                                                                  </td>

                                                            </TR>

                                                      </TABLE></td>

                                                  </tr>

                                                </table>

                                      </td>

                                    </tr>

                              </table> 

                   

        </PagerTemplate> 

               <Columns>                 
                            
               <asp:TemplateField SortExpression="StandardValue" ItemStyle-Width="60%" HeaderText="Text" HeaderStyle-CssClass="gridHdr" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="20%"/>
                <ItemTemplate>             
                    <%# (Container.DataItem("StandardValue"))%>               
                    <input type=hidden runat=server id="hdnStandardID"  value='<%#Container.DataItem("StandardID")%>'/>    
				</ItemTemplate>
                </asp:TemplateField> 

               <asp:TemplateField SortExpression="StandardLabel" ItemStyle-Width="20%" HeaderText="Type" HeaderStyle-CssClass="gridHdr" ItemStyle-Height="30px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="20%"/>
                <ItemTemplate>             
                    <%# (Container.DataItem("StandardLabel"))%>                    
				</ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="IsActive" HeaderStyle-CssClass= "gridHdr gridText" SortExpression="IsActive"  >         
                  <HeaderStyle  CssClass="gridHdr gridText"  />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" Width="5%"/>
                    <ItemTemplate>             
                   <asp:Label ID="lblIsActive" runat="server" Text='<%# IIF(Container.DataItem("IsActive"), True,False)%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>   
                   <asp:TemplateField ItemStyle-Width="6%">               
                <HeaderStyle  CssClass="gridHdr" />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow"/>
                    <ItemTemplate>    
                    <asp:LinkButton ID="lnkBtnEdit" CausesValidation="false"   CommandName="EditStd" CommandArgument='<%#Container.DataItem("StandardID") & "#" & Container.DataItem("StandardValue") & "#" & Container.DataItem("StandardLabel") & "#" & Container.DataItem("IsActive") %>' runat="server">
                           <img src="Images/Icons/Edit.gif" title='Edit' width="12" height="11" hspace="2" vspace="0" border="0">
                    </asp:LinkButton>                           
                    </ItemTemplate>
                </asp:TemplateField>  


                </Columns>

                 <AlternatingRowStyle CssClass=gridRow />

                 <RowStyle CssClass=gridRow />            

                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />

                <HeaderStyle  CssClass="gridHdr" HorizontalAlign=Left VerticalAlign=Top  Height="40px" />

                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />

        

              </asp:GridView>

              </asp:Panel>

      <cc1:ModalPopupExtender ID="mdlStandard" runat="server" TargetControlID="btnTemp" PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
<asp:Panel runat="server" ID="pnlConfirm" Width="300px" CssClass="pnlConfirm" style="display:none;">
    <div id="divModalParent">
        <span style="font-family:Tahoma; font-size:14px;"><b>Standard Type</b></span><br /><br />
        <asp:DropDownList id="ddlMdlStdType" tabIndex="1" runat="server" CssClass="bodyTextGreyLeftAligned" Width="280">
           <asp:ListItem Text="Select Type" Value=""></asp:ListItem>
            <asp:ListItem Text="Cancellation Reasons" Value="Cancellation Reasons" ></asp:ListItem>
            <asp:ListItem Text="Client Complaints" Value="Client Complaints" ></asp:ListItem>
            <asp:ListItem Text="Supplier Feedback" Value="Supplier Feedback" ></asp:ListItem>
        </asp:DropDownList>      
        <br /><br />
            <asp:TextBox runat="server" ID="txtStandardValue" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned" MaxLength="500" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox>        
            <br /><br />
          <asp:CheckBox ID="chkIsActivePopup" runat="server" Text="IsActive" />
        <br /><br />
            <div id="divButton" class="divButton" style="width: 85px; float:left;cursor:pointer;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                    <a class="buttonText cursorHand" href='javascript:validate_required("ctl00_ContentPlaceHolder1_ddlMdlStdType")' id="ancSubmit"><strong>Submit</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div>
            <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;cursor:pointer;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>								
</asp:Panel>
<asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
<asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

              

                         </ContentTemplate>

                         <Triggers>

                            <asp:PostBackTrigger   ControlID="btnView"   />                       

                         </Triggers>

            

            </asp:UpdatePanel>

            

                <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">

                <ProgressTemplate >

                        <div class="gridText">

                               <img  align=middle src="Images/indicator.gif" />

                               <b>Fetching Data... Please Wait</b>

                        </div>   

                        </ProgressTemplate>

                </asp:UpdateProgress>

                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />

             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"

                SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.StandardListing" SortParameterName="sortExpression">                

            </asp:ObjectDataSource>

                                            

                              

            <!-- InstanceEndEditable --></td>

                     

             

         </tr>

         </table>

      </div>


<script type="text/javascript">
    function validate_required(field) {
        // alert(document.getElementById(field).options[document.getElementById(field).selectedIndex].value);
        if (document.getElementById(field).options[document.getElementById(field).selectedIndex].value == "") {

            alert("Please select Standard Type")
        }
        else if (document.getElementById("ctl00_ContentPlaceHolder1_txtStandardValue").value == "") {

            alert("Please enter Standard value")
        }
        else {
            document.getElementById("ctl00_ContentPlaceHolder1_hdnButton").click();
        }
    }
    
</script>


</asp:Content>
