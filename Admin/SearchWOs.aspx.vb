
Imports WebLibrary
Partial Public Class SearchWOs
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Public WithEvents hdnButton As Button
    Public WithEvents pnlConfirm As Panel


    Dim getCount As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            gvWorkOrders.PageSize = ApplicationSettings.GridViewPageSize
            If Not IsNothing(Request("SrcWorkorderID")) And Not IsNothing(Request("SrcCompanyName")) And Not IsNothing(Request("SrcKeyword")) And Not IsNothing(Request("SrcPONumber")) And Not IsNothing(Request("SrcReceiptNumber")) And Not IsNothing(Request("SrcPostCode")) Then
                processBackToListing()
            Else
                InitializeForm()
                'Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.
                Session("dsSelectedCompany") = Nothing
            End If

            ''Added code for default showing of fields
            'If (Request("SrcWorkorderID") <> "") Or (Request("SrcCompanyName") <> "") Or (Request("SrcKeyword") <> "") Or (Request("SrcPONumber") <> "") Or (Request("SrcPostCode") <> "") Then
            '    divForAdvanceSearch.Visible = True
            '    lnkAdvanceSearch.Visible = False
            'Else
            '    divForAdvanceSearch.Visible = False
            '    lnkAdvanceSearch.Visible = True
            'End If

            'poonam modified on 9/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
            'populate note type ddl
            CommonFunctions.PopulateNoteCategory(Page, ddlNoteType)
        End If

    End Sub

    Public Sub InitializeForm(Optional ByVal resetFields As Boolean = True)

        'Initialize the Bizdivid with OrderWrok ID
        If IsNothing(ViewState("BizDivId")) Then
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryUK
                    ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
                Case ApplicationSettings.CountryDE
                    ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
            End Select
        End If

        If resetFields Then
            txtWorkorderID.Text = ""
            txtCompanyName.Text = ""
            txtKeyword.Text = ""
            txtPONumber.Text = ""
            txtReceiptNumber.Text = ""
            txtPostCode.Text = ""
            txtStartDate.Text = ""
            'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
            txtSubmittedDateFrom.Text = ""
            txtSubmittedDateTo.Text = ""
            ViewState!SortExpression = "WorkOrderId"
            gvWorkOrders.Sort(ViewState!SortExpression, SortDirection.Ascending)
        End If
    End Sub

    Private Function PrepareSearchCriteria() As String
        lblSearchCriteria.Text = "<strong>Search Work Orders Criteria:</strong>"
        'keyword
        If txtKeyword.Text <> "" Then
            lblSearchCriteria.Text &= " Keyword - <span class='txtOrange'>" & txtKeyword.Text & "</span>"
        End If

        'WorkOrderID
        If txtWorkorderID.Text <> "" Then
            lblSearchCriteria.Text &= " Work Order ID - <span class='txtOrange'>" & txtWorkorderID.Text & "</span>"
        End If

        'PO Number
        If txtPONumber.Text <> "" Then
            lblSearchCriteria.Text &= " PO Number - <span class='txtOrange'>" & txtPONumber.Text & "</span>"
        End If

        'Receipt Number
        If txtReceiptNumber.Text <> "" Then
            lblSearchCriteria.Text &= " Receipt Number - <span class='txtOrange'>" & txtReceiptNumber.Text & "</span>"
        End If

        'company name
        If txtCompanyName.Text <> "" Then
            lblSearchCriteria.Text &= " Company Name - <span class='txtOrange'>" & txtCompanyName.Text & "</span>"
        End If

        'Post Code
        If txtPostCode.Text <> "" Then
            lblSearchCriteria.Text &= " Post Code - <span class='txtOrange'>" & txtPostCode.Text & "</span>"
        End If

        'Post Code
        If txtStartDate.Text <> "" Then
            lblSearchCriteria.Text &= " Start Date - <span class='txtOrange'>" & txtStartDate.Text & "</span>"
        End If
        'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
        If txtSubmittedDateFrom.Text <> "" Then
            lblSearchCriteria.Text &= " Submitted Date From - <span class='txtOrange'>" & txtSubmittedDateFrom.Text & "</span>"
        End If

        'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
        If txtSubmittedDateTo.Text <> "" Then
            lblSearchCriteria.Text &= " Submitted Date To - <span class='txtOrange'>" & txtSubmittedDateTo.Text & "</span>"
        End If

        Return lblSearchCriteria.Text

    End Function

    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvWorkOrders.DataBind()
        End If
    End Sub

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim rowCount As Integer
        Dim BizDivId As Integer = HttpContext.Current.Items("BizDivId").ToString.Trim
        Dim Keyword As String = HttpContext.Current.Items("Keyword").ToString.Trim
        Dim PONumber As String = HttpContext.Current.Items("PONumber").ToString.Trim
        Dim ReceiptNumber As String = HttpContext.Current.Items("ReceiptNumber").ToString.Trim
        Dim PostCode As String = HttpContext.Current.Items("PostCode").ToString.Trim
        Dim WorkOrderId As String = HttpContext.Current.Items("WorkOrderId").ToString.Trim
        Dim CompanyName As String = HttpContext.Current.Items("CompanyName").ToString.Trim
        Dim DateStart As String = HttpContext.Current.Items("DateStart").ToString.Trim
        Dim DateSubmittedFrom As String = HttpContext.Current.Items("DateSubmittedFrom").ToString.Trim
        Dim DateSubmittedTo As String = HttpContext.Current.Items("DateSubmittedTo").ToString.Trim
        Dim ds As DataSet = Nothing
        If PONumber <> "" Or ReceiptNumber <> "" Or PostCode <> "" Or CompanyName <> "" Or Keyword <> "" Or WorkOrderId <> "" Or DateStart <> "" Or DateSubmittedFrom <> "" Or DateSubmittedTo <> "" Then
            ds = ws.WSWorkOrder.GetSearchWorkOrders(BizDivId, PONumber, ReceiptNumber, PostCode, CompanyName, Keyword, WorkOrderId, sortExpression, startRowIndex, maximumRows, rowCount, DateStart, DateSubmittedFrom, DateSubmittedTo, Session("UserID"))
            HttpContext.Current.Items("rowCount") = rowCount
            ViewState("totalRecords") = rowCount

            HttpContext.Current.Items("tblWOUserNotes") = ds.Tables(2)
        Else
            HttpContext.Current.Items("rowCount") = 0
            ViewState("totalRecords") = 0
        End If
        Return ds
    End Function

    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function
    Public Function ShowHideUserNotes(ByVal WOID As Integer) As String
        If Not IsNothing(HttpContext.Current.Items("tblWOUserNotes")) Then
            Dim dt As DataTable = HttpContext.Current.Items("tblWOUserNotes")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "WOID = " & WOID & " and Comments <> ''"
            If dv.Count = 0 Then
                Return "False"
            Else
                Return "True"
            End If
        Else
            Return "False"
        End If

    End Function
    Public Function GetUserNotesDetail(ByVal WOID As Integer) As DataView
        Dim dv As DataView
        If Not IsNothing(HttpContext.Current.Items("tblWOUserNotes")) Then
            Dim dt As DataTable = HttpContext.Current.Items("tblWOUserNotes")
            dv = dt.DefaultView
            dv.RowFilter = "WOID = " & WOID
            Return dv
        Else
            Return dv
        End If
    End Function

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        gvWorkOrders.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

    Private Sub gvContacts_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvWorkOrders.PreRender
        Me.gvWorkOrders.Controls(0).Controls(Me.gvWorkOrders.Controls(0).Controls.Count - 1).Visible = True
        If ViewState("rowCount") < 1 Then
            Return
        End If
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

            End If
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvWorkOrders.RowDataBound
        MakeGridViewHeaderClickable(gvWorkOrders, e.Row)

        If e.Row.RowType = DataControlRowType.DataRow Then
            If (DataBinder.Eval(e.Row.DataItem, "IsWatch") = "1") Then

                Dim status As String = DataBinder.Eval(e.Row.DataItem, "Status")
                If status.Trim() <> ApplicationSettings.WOGroupCancelled Then
                    Dim col As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#993366")
                    e.Row.BackColor = col
                    e.Row.ForeColor = Drawing.Color.White
                    For Each tableCell As TableCell In e.Row.Cells
                        tableCell.ForeColor = Drawing.Color.White
                    Next
                End If

                'Dim col As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#993366")
                'e.Row.BackColor = col
                'e.Row.ForeColor = Drawing.Color.White
                'For Each tableCell As TableCell In e.Row.Cells
                '    tableCell.ForeColor = Drawing.Color.White
                'Next

            Else
                Dim OriginalCol As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#EDEDED")
                e.Row.BackColor = OriginalCol
                For Each tableCell As TableCell In e.Row.Cells
                    tableCell.ForeColor = Drawing.Color.Black
                Next
            End If
        End If
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvWorkOrders.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvWorkOrders, e.Row, Me)
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)
        'Dim btnExport As HtmlAnchor = CType(gvPagerRow.FindControl("btnExport"), HtmlAnchor)
        If pageCount > 0 Then
            tdExport.Visible = True
            btnExport.HRef = prepareExportToExcelLink()
        Else
            tdExport.Visible = False
        End If
        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    Public Sub processBackToListing()
        'Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.
        Dim dsSelectedCompany As DataSet
        Dim ds As DataSet
        Dim dv As DataView
        dsSelectedCompany = CType(Session("dsSelectedCompany"), DataSet)

        If Not IsNothing(dsSelectedCompany) Then
            ds = CType(Session("dsSelectedCompany"), DataSet)
            dv = ds.Tables(0).DefaultView


            rptSelectedCompany.DataSource = dv.ToTable.DefaultView
            rptSelectedCompany.DataBind()

            pnlSelectedCompany.Visible = True
        End If
        If Not IsNothing(Request("SrcWorkorderID")) Then
            txtWorkorderID.Text = Request("SrcWorkorderID")
        End If
        If Not IsNothing(Request("SrcCompanyName")) Then

            Dim CompanyName As String = ""
            Dim i As Integer = 0
            CompanyName = Server.UrlDecode(Request("SrcCompanyName").Replace("dquotes", "'")).ToString
            Dim strCompanyName() As String = Split(CompanyName, ",")
            If Not IsNothing(dsSelectedCompany) Then
                For i = 0 To strCompanyName.GetLength(0) - 1
                    dv.RowFilter = "CompanyName IN ('" & CStr(strCompanyName.GetValue(i)) & "')"
                    If Not (dv.Count > 0) Then
                        txtCompanyName.Text = CStr(strCompanyName.GetValue(i))
                    End If
                Next
            Else
                txtCompanyName.Text = strCompanyName(0).ToString
            End If

        End If
        If Not IsNothing(Request("SrcKeyword")) Then
            txtKeyword.Text = Server.UrlDecode(Request("SrcKeyword").Replace("dquotes", "'")).ToString
        End If
        If Not IsNothing(Request("SrcPONumber")) Then
            txtPONumber.Text = Server.UrlDecode(Request("SrcPONumber").Replace("dquotes", "'")).ToString
        End If
        If Not IsNothing(Request("SrcReceiptNumber")) Then
            txtReceiptNumber.Text = Server.UrlDecode(Request("SrcReceiptNumber").Replace("dquotes", "'")).ToString
        End If
        If Not IsNothing(Request("SrcPostCode")) Then
            txtPostCode.Text = Server.UrlDecode(Request("SrcPostCode")).ToString
        End If
        If Not IsNothing(Request("SrcDateStart")) Then
            txtStartDate.Text = Server.UrlDecode(Request("SrcDateStart")).ToString
        End If
        'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
        If Not IsNothing(Request("SrcDateSubmittedFrom")) Then
            txtSubmittedDateFrom.Text = Server.UrlDecode(Request("SrcDateSubmittedFrom")).ToString
        End If
        If Not IsNothing(Request("SrcDateSubmittedTo")) Then
            txtSubmittedDateTo.Text = Server.UrlDecode(Request("SrcDateSubmittedTo")).ToString
        End If
        ViewState("BizDivId") = Request("bizDivId")

        ViewState("BizDivId") = Request("BizDivId")

        ViewState("SrcKeyword") = Server.UrlDecode(Request("SrcKeyword").Replace("dquotes", "'")).ToString
        ViewState("SrcPONumber") = Server.UrlDecode(Request("SrcPONumber").Replace("dquotes", "'")).ToString
        ViewState("SrcReceiptNumber") = Server.UrlDecode(Request("SrcReceiptNumber").Replace("dquotes", "'")).ToString
        ViewState("SrcPostCode") = Server.UrlDecode(Request("SrcPostCode")).ToString
        ViewState("SrcWorkOrderId") = Request("SrcWorkorderID")
        ViewState("SrcCompanyName") = Server.UrlDecode(Request("SrcCompanyName").Replace("dquotes", "'")).ToString
        hdnCompanyName.Value = ViewState("SrcCompanyName")
        ViewState("SrcDateStart") = Server.UrlDecode(Request("SrcDateStart")).ToString
        'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
        ViewState("SrcDateSubmittedFrom") = Server.UrlDecode(Request("SrcDateSubmittedFrom")).ToString
        ViewState("SrcDateSubmittedTo") = Server.UrlDecode(Request("SrcDateSubmittedTo")).ToString
        gvWorkOrders.PageSize = Request("PS")

        'Sort Expe
        ViewState!SortExpression = ""
        If Trim(Request("SC")) <> "" Then
            ViewState!SortExpression = Trim(Request("SC"))
        Else
            'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
            ViewState!SortExpression = "SubmittedDateFrom"
        End If

        'Sort direction
        Dim sd As SortDirection
        sd = SortDirection.Descending
        If Trim(Request("SO")) <> "" Then
            If Trim(Request("SO")).ToLower = "0" Then
                sd = SortDirection.Ascending
            End If
        End If

        InitializeForm(False)
        lblSearchCriteria.Text = PrepareSearchCriteria()
        pnlSearchResult.Visible = True
        gvWorkOrders.Sort(ViewState!SortExpression, sd)
        PopulateGrid()
        gvWorkOrders.PageIndex = Request("PN")
    End Sub

    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    Private Sub ObjectDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles ObjectDataSource1.Selecting
        InitializeGridParameters()
    End Sub

    Private Sub InitializeGridParameters()
        HttpContext.Current.Items("BizDivId") = ViewState("BizDivId")
        HttpContext.Current.Items("Keyword") = ViewState("SrcKeyword")
        HttpContext.Current.Items("PONumber") = ViewState("SrcPONumber")
        HttpContext.Current.Items("ReceiptNumber") = ViewState("SrcReceiptNumber")
        HttpContext.Current.Items("PostCode") = ViewState("SrcPostCode")
        HttpContext.Current.Items("WorkOrderId") = ViewState("SrcWorkOrderId")
        HttpContext.Current.Items("CompanyName") = ViewState("SrcCompanyName")
        HttpContext.Current.Items("DateStart") = ViewState("SrcDateStart")
        HttpContext.Current.Items("DateSubmittedFrom") = ViewState("SrcDateSubmittedFrom")
        HttpContext.Current.Items("DateSubmittedTo") = ViewState("SrcDateSubmittedTo")
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
        If txtSubmittedDateFrom.Text.Trim = "" And txtSubmittedDateTo.Text.Trim <> "" Then
            lblError.Text = "Please enter Submitted Date From"
            lblSearchCriteria.Text = "<strong> <span class='txtOrange'>Please Enter Search Criteria</span></strong>"
            pnlSearchResult.Visible = False
        Else
            lblError.Text = ""
            If txtWorkorderID.Text.Trim <> "" Or txtKeyword.Text.Trim <> "" Or txtCompanyName.Text.Trim <> "" Or txtPONumber.Text.Trim <> "" Or txtReceiptNumber.Text.Trim <> "" Or txtPostCode.Text.Trim <> "" Or txtStartDate.Text.Trim <> "" Or txtSubmittedDateFrom.Text.Trim <> "" Or txtSubmittedDateTo.Text.Trim <> "" Or rptSelectedCompany.Items.Count <> 0 Then
                lblSearchCriteria.Text = PrepareSearchCriteria()
                pnlSearchResult.Visible = True
                ViewState("SrcWorkOrderId") = txtWorkorderID.Text.Trim
                'Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.
                Dim dsSelectedCompany As DataSet
                Dim ds As DataSet
                dsSelectedCompany = CType(Session("dsSelectedCompany"), DataSet)
                Dim splitCompanyName As String
                Dim strCompanyName As String = ""
                Dim SPCompanyName As String = ""
                If Not IsNothing(dsSelectedCompany) And txtCompanyName.Text.Trim <> "" Then
                    ds = CType(Session("dsSelectedCompany"), DataSet)
                    Dim SelectedCompanyName As String = ""

                    Dim i As Integer
                    i = 0
                    For Each drow As DataRow In ds.Tables(0).Rows
                        SelectedCompanyName += drow.Item("ContactID").ToString + ","
                        i = i + 1
                        ' strCompanyName = strCompanyName + SelectedCompanyName.Trim
                        strCompanyName = SelectedCompanyName
                    Next
                    strCompanyName = strCompanyName + txtCompanyName.Text.Trim + ","
                    splitCompanyName = strCompanyName.Substring(0, strCompanyName.LastIndexOf(","))
                    ViewState("SrcCompanyName") = splitCompanyName
                ElseIf Not IsNothing(dsSelectedCompany) Then
                    ds = CType(Session("dsSelectedCompany"), DataSet)
                    Dim SelectedCompanyName As String = ""

                    Dim i As Integer
                    i = 0
                    For Each drow As DataRow In ds.Tables(0).Rows
                        SelectedCompanyName += drow.Item("ContactID").ToString + ","
                        i = i + 1
                        ' strCompanyName = strCompanyName + SelectedCompanyName.Trim
                        strCompanyName = SelectedCompanyName
                    Next
                    splitCompanyName = strCompanyName.Substring(0, strCompanyName.LastIndexOf(","))
                    ViewState("SrcCompanyName") = splitCompanyName
                ElseIf txtCompanyName.Text.Trim <> "" Then
                    strCompanyName = strCompanyName + txtCompanyName.Text.Trim + ","
                    splitCompanyName = strCompanyName.Substring(0, strCompanyName.LastIndexOf(","))
                    ViewState("SrcCompanyName") = splitCompanyName
                Else
                    ViewState("SrcCompanyName") = ""
                End If
                hdnCompanyName.Value = ViewState("SrcCompanyName")

                ''Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.
                'Dim dsSelectedCompany As DataSet
                'Dim ds As DataSet
                'dsSelectedCompany = CType(Session("dsSelectedCompany"), DataSet)
                'If Not IsNothing(dsSelectedCompany) Then
                '    ds = CType(Session("dsSelectedCompany"), DataSet)
                '    Dim SelectedCompanyName As String
                '    Dim i As Integer
                '    i = 0
                '    For Each drow As DataRow In ds.Tables(0).Rows
                '        SelectedCompanyName += drow.Item("CompanyName").ToString + ","
                '        i = i + 1
                '    Next
                '    Dim firstpart As String = SelectedCompanyName.Substring(0, SelectedCompanyName.LastIndexOf(","))
                'Else
                '    ViewState("SrcCompanyName") = ""
                'End If




                'ViewState("SrcCompanyName") = txtCompanyName.Text.Trim
                ViewState("SrcKeyword") = txtKeyword.Text.Trim
                ViewState("SrcPONumber") = txtPONumber.Text.Trim
                ViewState("SrcReceiptNumber") = txtReceiptNumber.Text.Trim
                ViewState("SrcPostCode") = txtPostCode.Text.Trim
                ViewState("SrcDateStart") = txtStartDate.Text.Trim
                'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
                ViewState("SrcDateSubmittedFrom") = txtSubmittedDateFrom.Text.Trim
                ViewState("SrcDateSubmittedTo") = txtSubmittedDateTo.Text.Trim
                PopulateGrid()
            Else
                lblSearchCriteria.Text = "<strong> <span class='txtOrange'>Please Enter Search Criteria</span></strong>"
            End If
        End If

    End Sub

    'Private Sub lnkAdvanceSearch_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceSearch.ServerClick
    '    divForAdvanceSearch.Visible = True
    '    lnkAdvanceSearch.Visible = False
    'End Sub


    Public Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click
        Dim Success As DataSet = CommonFunctions.SaveNotes(ViewState("SelectedWorkOrderId"), Session("UserId"), txtNote.Text.Trim, False)
        txtNote.Text = ""
        'Removed Re-Population as per Chris. See Case 320 (Double click to view case)
        'PopulateGrid()
    End Sub

    Private Sub gvWorkOrders_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvWorkOrders.RowCommand
        If (e.CommandName = "AddNote") Then
            Dim WorkOrderId As String
            WorkOrderId = e.CommandArgument.ToString
            ViewState.Add("SelectedWorkOrderId", WorkOrderId)
            mdlQuickNotes.Show()
            'poonam modified on 15/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
            ddlNoteType.SelectedIndex = -1
        End If

        If (e.CommandName = "WOID") Then
            Dim WorkOrderId As String
            WorkOrderId = e.CommandArgument.ToString
            ViewState.Add("SelectedWorkOrderId", WorkOrderId)
            mdlUpdateRating.Show()
            rdBtnNegative.Checked = False
            rdBtnNeutral.Checked = False
            rdBtnPositive.Checked = False
            txtComment.Text = ""

            Dim ds As DataSet
            ds = CommonFunctions.GetWORating(WorkOrderId)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
                        Dim Result As Integer = ds.Tables(0).Rows(0)(0)
                        Select Case Result
                            Case -1
                                rdBtnNegative.Checked = True
                            Case 0
                                rdBtnNeutral.Checked = True
                            Case 1
                                rdBtnPositive.Checked = True
                        End Select
                    End If
                    txtComment.Text = ds.Tables("tblRating").Rows(0).Item("Comments")
                End If
            End If
        End If
        If (e.CommandName = "Action") Then
            Dim WorkOrderId As String
            Dim Group As String
            Dim arrList As ArrayList = New ArrayList
            Dim str As String = e.CommandArgument.ToString
            arrList.AddRange(str.Split(","))


            WorkOrderId = arrList(0)
            Group = arrList(1)
            ViewState.Add("SelectedWorkOrderId", WorkOrderId)
            mdlAction.Show()
            'CType(FindControl("mdlAction"), AjaxControlToolkit.ModalPopupExtender).Show()
            'poonam modified on 15/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
            ddlNoteType.SelectedIndex = -1
            rdBtnActionPositive.Checked = False
            rdBtnActionNeutral.Checked = False
            rdBtnActionNegative.Checked = False
            txtCommentAction.Text = ""
            divActionRating.Style.Add("display", "none")
            divActionWatch.Style.Add("display", "block")
            chkMyWatch.Checked = False
            chkMyRedFlag.Checked = False

            If (Group = ApplicationSettings.WOGroupAccepted Or Group = "Activeelapsed" Or Group = "Active" Or Group = "Issue" Or Group = ApplicationSettings.WOGroupCompleted Or Group = ApplicationSettings.WOGroupClosed) Then
                divActionRating.Style.Add("display", "block")
            End If
            If (Group = ApplicationSettings.WOGroupClosed) Or (Group = ApplicationSettings.WOGroupCancelled) Or (Group = ApplicationSettings.WOGroupInvoiced) Then
                divActionWatch.Style.Add("display", "none")
            End If

            Dim ds As DataSet
            ds = CommonFunctions.GetWORating(WorkOrderId, CInt(Session("UserID")), "WOAction")
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
                        Dim Result As Integer = ds.Tables(0).Rows(0)(0)
                        Select Case Result
                            Case -1
                                rdBtnActionNegative.Checked = True
                            Case 0
                                rdBtnActionNeutral.Checked = True
                            Case 1
                                rdBtnActionPositive.Checked = True
                        End Select
                    End If
                    txtCommentAction.Text = ds.Tables("tblRating").Rows(0).Item("Comments")
                End If
                If ds.Tables(1).Rows.Count > 0 Then
                    chkMyWatch.Checked = ds.Tables(1).Rows(0).Item("IsWatched")
                    chkMyRedFlag.Checked = ds.Tables(1).Rows(0).Item("IsFlagged")
                End If
            End If
        End If

        If (e.CommandName = "Watch") Then
            Dim Param As String
            Param = e.CommandArgument.ToString
            If Param <> "" Then
                ws.WSWorkOrder.AddDeleteWatchedWO(Param, Session("UserID"), "Add")
                ' Me.Page.GetType.InvokeMember("Populate", System.Reflection.BindingFlags.InvokeMethod, Nothing, Me.Page, New Object() {})
                PopulateGrid()
            End If
        End If

        If (e.CommandName = "Unwatch") Then
            Dim Param As String
            Param = e.CommandArgument.ToString
            If Param <> "" Then
                ws.WSWorkOrder.AddDeleteWatchedWO(Param, Session("UserID"), "Remove")
                'Me.Page.GetType.InvokeMember("Populate", System.Reflection.BindingFlags.InvokeMethod, Nothing, Me.Page, New Object() {})
                PopulateGrid()
            End If
        End If
    End Sub
    ''' <summary>
    ''' Used for showing and hiding of Option Set wholesale price
    ''' </summary>
    ''' <param name="Status"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ShowWholesalePrice(ByVal Status As String) As Boolean
        If Status = ApplicationSettings.WOGroupSubmitted Or Status = ApplicationSettings.WOGroupAccepted Or Status = ApplicationSettings.WOGroupCompleted Or Status = "Conditional Accept" Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function ShowHideIsWatch(ByVal Status As String, ByVal ISWatch As String, ByVal Mode As String) As Boolean
        If Status = ApplicationSettings.WOGroupCancelled Then
            Return False
        Else
            If (Mode = "Watch" And ISWatch = 0) Then
                Return True
            ElseIf (Mode = "UnWatch" And ISWatch = 1) Then
                Return True
            Else
                Return False
            End If
            Return True
        End If
    End Function
    Public Function getActionLinks(ByVal WOID As String, ByVal WOStaged As String, ByVal WorkOrderID As String, ByVal BuyerCompID As Integer, ByVal BuyerContactID As Integer, ByVal SupCompID As Integer, ByVal Status As String, ByVal BuyerAccept As Integer, ByVal WholesalePrice As Integer, ByVal param As String) As String
        Dim lnk As String
        lnk = ""

        If Status <> "" Then
            Select Case Status
                Case "Active"
                    'Accepted will be having set wholesale price
                    lnk = lnk & "<a href=WOSetWholesalePrice.aspx?WOID=" & WOID & "&Group=" & ApplicationSettings.WOGroupAccepted & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='SetWP'><img src='Images/Icons/SetWholesalePrice.gif' alt='Set Wholesale Price' hspace='2' vspace='0' border='0'></a>"
                    'Accepted will be having set Raise Issue
                    lnk = lnk & "<a href=WORaiseIssue.aspx?WOID=" & WOID & "&Action=RaiseIssue&Group=" & ApplicationSettings.WOGroupAccepted & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='CancelWO'><img src='Images/Icons/RaiseIssue.gif' alt='Raise Issue' hspace='2' vspace='0' border='0'></a>"
                    'Accepted will be having set Complete WO 
                    If WOStaged <> "Yes" Then
                        lnk = lnk & "<a href=WOComplete.aspx?WOID=" & WOID & "&Group=" & ApplicationSettings.WOGroupAccepted & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='CompleteWO'><img src='Images/Icons/Complete.gif' alt='Complete Work Order' hspace='2' vspace='0' border='0'></a>"
                    End If
                    lnk = lnk & "<a href=AdminWOHistory.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & ApplicationSettings.WOGroupAccepted & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='WOHistory'><img src='Images/Icons/ViewHistory.gif' alt='Work Order History' hspace='2' vspace='0' border='0'></a>"
                    'Accepted will be having set Cancel WO
                    lnk = lnk & "<a href=WOCancellation.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & ApplicationSettings.WOGroupAccepted & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='CancelWO'><img src='Images/Icons/Cancel.gif' alt='Cancel Work Order' hspace='2' vspace='0' border='0'></a>"
                Case ApplicationSettings.WOGroupSubmitted
                    'Submitted will be having set wholesale price
                    lnk = lnk & "<a href=WOSetWholesalePrice.aspx?WOID=" & WOID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='SetWP'><img src='Images/Icons/SetWholesalePrice.gif' alt='Set Wholesale Price' hspace='2' vspace='0' border='0'></a>"
                    If WholesalePrice > 0 And BuyerAccept = 0 Then
                        lnk = lnk & "<a href=AcceptWorkRequest.aspx?WOID=" & WOID & "&Group=" & Status & "&FromDate=''&ToDate=''&ContactID=" & BuyerContactID & "&CompanyID=" & BuyerCompID & "&" & param & " runat='server' id='SetBuyerAccept'><img src='Images/Icons/AcceptedBuyerWorkRequest.gif' alt='Set Buyer Accept' hspace='2' vspace='0' border='0'></a>"
                    End If
                    'Submitted will be having set Cancel WO
                    lnk = lnk & "<a href=AdminWOHistory.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='WOHistory'><img src='Images/Icons/ViewHistory.gif' alt='Work Order History' hspace='2' vspace='0' border='0'></a>"
                    lnk = lnk & "<a href=WOCancellation.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='CancelWO'><img src='Images/Icons/Cancel.gif' alt='Cancel Work Order' hspace='2' vspace='0' border='0'></a>"
                Case ApplicationSettings.WOGroupSent
                    'Submitted will be having set wholesale price
                    lnk = lnk & "<a href=WOSetWholesalePrice.aspx?WOID=" & WOID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='SetWP'><img src='Images/Icons/SetWholesalePrice.gif' alt='Set Wholesale Price' hspace='2' vspace='0' border='0'></a>"
                    If WholesalePrice > 0 And BuyerAccept = 0 Then
                        lnk = lnk & "<a href=AcceptWorkRequest.aspx?WOID=" & WOID & "&Group=" & Status & "&FromDate=''&ToDate=''&ContactID=" & BuyerContactID & "&CompanyID=" & BuyerCompID & "&" & param & " runat='server' id='SetBuyerAccept'><img src='Images/Icons/AcceptedBuyerWorkRequest.gif' alt='Set Buyer Accept' hspace='2' vspace='0' border='0'></a>"
                    End If
                    'Submitted will be having set Cancel WO
                    lnk = lnk & "<a href=AdminWOHistory.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='WOHistory'><img src='Images/Icons/ViewHistory.gif' alt='Work Order History' hspace='2' vspace='0' border='0'></a>"
                    lnk = lnk & "<a href=WOCancellation.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='CancelWO'><img src='Images/Icons/Cancel.gif' alt='Cancel Work Order' hspace='2' vspace='0' border='0'></a>"
                Case ApplicationSettings.WOGroupIssue
                    'Issue will be having set wholesale price
                    lnk = lnk & "<a href=WOSetWholesalePrice.aspx?WOID=" & WOID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='SetWP'><img src='Images/Icons/SetWholesalePrice.gif' alt='Set Wholesale Price' hspace='2' vspace='0' border='0'></a>"
                    lnk = lnk & "<a href=AdminWOHistory.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='WOHistory'><img src='Images/Icons/ViewHistory.gif' alt='Work Order History' hspace='2' vspace='0' border='0'></a>"
                    'Issue will be having set Cancel WO
                    lnk = lnk & "<a href=WOCancellation.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='CancelWO'><img src='Images/Icons/Cancel.gif' alt='Cancel Work Order' hspace='2' vspace='0' border='0'></a>"
                Case ApplicationSettings.WOGroupCompleted
                    'Completed will be having set wholesale price
                    lnk = lnk & "<a href=WOSetWholesalePrice.aspx?WOID=" & WOID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='SetWP'><img src='Images/Icons/SetWholesalePrice.gif' alt='Set Wholesale Price' hspace='2' vspace='0' border='0'></a>"
                    'Completed will be having set Raise Issue
                    lnk = lnk & "<a href=WORaiseIssue.aspx?WOID=" & WOID & "&Action=RaiseIssue&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='CancelWO'><img src='Images/Icons/RaiseIssue.gif' alt='Raise Issue' hspace='2' vspace='0' border='0'></a>"
                    'Completed will be having set Close WO 
                    lnk = lnk & "<a href=WOClose.aspx?WOID=" & WOID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='CompleteWO'><img src='Images/Icons/Close.gif' alt='Close Work Order' hspace='2' vspace='0' border='0'></a>"
                    lnk = lnk & "<a href=AdminWOHistory.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='WOHistory'><img src='Images/Icons/ViewHistory.gif' alt='Work Order History' hspace='2' vspace='0' border='0'></a>"
                    'Completed will be having set Cancel WO
                    lnk = lnk & "<a href=WOCancellation.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='CancelWO'><img src='Images/Icons/Cancel.gif' alt='Cancel Work Order' hspace='2' vspace='0' border='0'></a>"
                Case "Conditional Accept"
                    'CA will be having set wholesale price
                    lnk = lnk & "<a href=WOSetWholesalePrice.aspx?WOID=" & WOID & "&Group=" & ApplicationSettings.WOGroupCA & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='SetWP'><img src='Images/Icons/SetWholesalePrice.gif' alt='Set Wholesale Price' hspace='2' vspace='0' border='0'></a>"
                    lnk = lnk & "<a href=AdminWOHistory.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & ApplicationSettings.WOGroupCA & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='WOHistory'><img src='Images/Icons/ViewHistory.gif' alt='Work Order History' hspace='2' vspace='0' border='0'></a>"
                    If WholesalePrice > 0 And BuyerAccept = 0 Then
                        lnk = lnk & "<a href=AcceptWorkRequest.aspx?WOID=" & WOID & "&Group=" & Status & "&FromDate=''&ToDate=''&ContactID=" & BuyerContactID & "&CompanyID=" & BuyerCompID & "&" & param & " runat='server' id='SetBuyerAccept'><img src='Images/Icons/AcceptedBuyerWorkRequest.gif' alt='Set Buyer Accept' hspace='2' vspace='0' border='0'></a>"
                    End If
                    'CA will be having set Cancel WO
                    lnk = lnk & "<a href=WOCancellation.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & ApplicationSettings.WOGroupCA & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='CancelWO'><img src='Images/Icons/Cancel.gif' alt='Cancel Work Order' hspace='2' vspace='0' border='0'></a>"
                Case ApplicationSettings.WOGroupClosed
                    lnk = lnk & "<a href=AdminWOHistory.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='WOHistory'><img src='Images/Icons/ViewHistory.gif' alt='Work Order History' hspace='2' vspace='0' border='0'></a>"
                Case Else
                    'Rest all other statuses will be having no action links
                    lnk = ""
            End Select


        End If
        Return lnk
    End Function

    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetWorkOrderID(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("WorkOrderID", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function
    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetCompanyName(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("SearchWorkorderCompanyName", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function
    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetPONumber(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("PONumber", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function
    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetReceiptNumber(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("ReceiptNumber", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function
    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetPostCode(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("PostCode", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function
    Public Sub hdnButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton1.Click

        Dim Rating As Integer
        If rdBtnNegative.Checked = True Then
            Rating = -1
        ElseIf rdBtnPositive.Checked = True Then
            Rating = 1
        Else
            Rating = 0
        End If
        Dim Success As DataSet = CommonFunctions.SaveRatingComment(ViewState("SelectedWorkOrderId"), Session("CompanyID"), Session("UserID"), txtComment.Text.Trim, Rating)
        txtComment.Text = ""

        PopulateGrid()
    End Sub
    Public Function prepareExportToExcelLink() As String
        Dim BizDivId As Integer = ViewState("BizDivId")
        Dim Keyword As String = ViewState("SrcKeyword")
        Dim PONumber As String = ViewState("SrcPONumber")
        Dim ReceiptNumber As String = ViewState("SrcReceiptNumber")
        Dim PostCode As String = ViewState("SrcPostCode")
        Dim WorkOrderId As String = ViewState("SrcWorkOrderId")
        Dim CompanyName As String = ViewState("SrcCompanyName")
        Dim DateStart As String = ViewState("SrcDateStart")
        'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
        Dim DateSubmittedFrom As String = ViewState("SrcDateSubmittedFrom")
        Dim DateSubmittedTo As String = ViewState("SrcDateSubmittedTo")
        Dim linkParams As String = ""
        Dim mode As String
        mode = Request.QueryString("mode")
        If mode <> ApplicationSettings.WOGroupCA Then
            mode = StrConv(mode, VbStrConv.ProperCase)
        End If
        linkParams &= "BizDivId=" & BizDivId
        linkParams &= "&Keyword=" & Keyword
        linkParams &= "&PONumber=" & PONumber
        linkParams &= "&ReceiptNumber=" & ReceiptNumber
        linkParams &= "&PostCode=" & PostCode
        linkParams &= "&WorkOrderId=" & WorkOrderId
        linkParams &= "&DateStart=" & DateStart
        'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
        linkParams &= "&DateSubmittedFrom=" & DateSubmittedFrom
        linkParams &= "&DateSubmittedTo=" & DateSubmittedTo
        linkParams &= "&CompanyName=" & Server.UrlEncode(CompanyName).ToString
        linkParams &= "&sortExpression=WorkOrderId"
        linkParams &= "&page=" & "searchwo"
        Return "ExportToExcel.aspx?" & linkParams
    End Function

    Private Sub hdnBtnAction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnBtnAction.Click
        Dim Note As String = txtNoteAction.Text.Trim.Replace(Chr(13), "<BR>")
        Dim Comments As String = txtCommentAction.Text.Trim.Replace(Chr(13), "<BR>")
        Dim IsFlagged As Boolean = chkMyRedFlag.Checked
        Dim IsWatched As Boolean = chkMyWatch.Checked

        Dim Rating As Integer

        If (divActionRating.Style.Item("display") <> "none") Then
            If rdBtnActionNegative.Checked = True Then
                Rating = -1
            ElseIf rdBtnActionPositive.Checked = True Then
                Rating = 1
            ElseIf rdBtnActionNeutral.Checked = True Then
                Rating = 0
            Else
                Rating = 2
            End If
        Else
            Rating = 2
        End If
        'poonam modified on 15/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
        Dim Success As DataSet = CommonFunctions.SaveActionCommentNotes(ViewState("SelectedWorkOrderId"), Session("UserID"), Note, Comments, Session("CompanyID"), Rating, IsFlagged, IsWatched, chkShowClient.Checked, chkShowSupplier.Checked, ddlNoteType.SelectedItem.Value)
        txtNoteAction.Text = ""
        txtCommentAction.Text = ""
        PopulateGrid()
    End Sub
    Public Function GetActionIcon(ByVal NtCnt As Integer, ByVal Rating As Integer) As String
        Dim ActionIcon As String
        ActionIcon = "~/Images/Icons/Action.gif"
        If (NtCnt > 0 And Rating = 2) Then
            ActionIcon = "~/Images/Icons/Action-note.gif"
        ElseIf (NtCnt > 0 And Rating = 0) Then
            ActionIcon = "~/Images/Icons/Action-note-neutral.gif"
        ElseIf (NtCnt > 0 And Rating = 1) Then
            ActionIcon = "~/Images/Icons/Action-note-positive.gif"
        ElseIf (NtCnt > 0 And Rating = -1) Then
            ActionIcon = "~/Images/Icons/Action-note-negative.gif"
        ElseIf (NtCnt = 0 And Rating = 0) Then
            ActionIcon = "~/Images/Icons/Action-neutral.gif"
        ElseIf (NtCnt = 0 And Rating = 1) Then
            ActionIcon = "~/Images/Icons/Action-positive.gif"
        ElseIf (NtCnt = 0 And Rating = -1) Then
            ActionIcon = "~/Images/Icons/Action-negative.gif"
        End If
        Return ActionIcon
    End Function

    Public Function GetLinks(ByVal parambizDivId As Object, ByVal companyId As Object, ByVal contactId As Object, ByVal SupplierCompanyName As String) As String
        Dim link As String = ""
        Dim contactType As ApplicationSettings.ContactType = ApplicationSettings.ContactType.approvedSupplier
        Dim classId As String
        Dim statusId As String
        classId = ApplicationSettings.RoleSupplierID
        statusId = ApplicationSettings.StatusApprovedCompany

        Dim bizDivId = parambizDivId

        link &= "<a class='footerTxtSelected ' href='companyprofile.aspx?sender=SearchWO&bizDivId=" & bizDivId & "&contactType=" & contactType & "&WorkorderID=" & txtWorkorderID.Text.Trim & "&companyid=" & companyId & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&mode=" & Request("mode") & "&ps=" & gvWorkOrders.PageSize & "&pn=" & gvWorkOrders.PageIndex & "&sc=" & gvWorkOrders.SortExpression & "&so=" & gvWorkOrders.SortDirection & "&CompID=" & companyId & "&FromDate=''" & "&ToDate=''" & "'>" & SupplierCompanyName & "</a>"

        Return link

    End Function
    'Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.
    Public Sub hdnbtnSelectedCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnbtnSelectedCompany.Click
        Dim SelectedCompany As String = hdnSelectedCompanyName.Value  'hdnSelectedCompanyName.Value
        Dim SelectedCompanyID As String = hdnSelectedCompanyID.Value

        Dim dsSelectedCompany As DataSet
        dsSelectedCompany = CType(Session("dsSelectedCompany"), DataSet)
        Dim ds As New DataSet
        Dim dsExists As DataSet
        Dim dt As New DataTable
        Dim dv As DataView
        Dim drow As DataRow


        If Not IsNothing(dsSelectedCompany) Then
            ds = CType(Session("dsSelectedCompany"), DataSet)
            dv = ds.Tables(0).DefaultView
            dv.RowFilter = "ContactID IN ('" & SelectedCompanyID & "')"
            If (dv.Count > 0) Then

                lblSelectedCompany.Text = "Selected item already exists in your selection."
            Else
                lblSelectedCompany.Text = ""
                dt = ds.Tables(0)

                drow = dt.NewRow()
                drow("CompanyID") = (dt.Rows.Count + 1).ToString
                drow("CompanyName") = SelectedCompany
                drow("ContactID") = SelectedCompanyID
                dt.Rows.Add(drow)
                dt.AcceptChanges()


                ds.AcceptChanges()
            End If


        Else


            Dim dColCompanyID As New DataColumn
            dColCompanyID.DataType = Type.[GetType]("System.String")
            dColCompanyID.ColumnName = "CompanyID"
            dt.Columns.Add(dColCompanyID)
            Dim dColCompanyName As New DataColumn
            dColCompanyName.DataType = Type.[GetType]("System.String")
            dColCompanyName.ColumnName = "CompanyName"
            dt.Columns.Add(dColCompanyName)
            Dim dColContactID As New DataColumn
            dColContactID.DataType = Type.[GetType]("System.String")
            dColContactID.ColumnName = "ContactID"
            dt.Columns.Add(dColContactID)


            drow = dt.NewRow()
            drow("CompanyID") = "1"
            drow("CompanyName") = SelectedCompany
            drow("ContactID") = SelectedCompanyID

            dt.Rows.Add(drow)
            dt.AcceptChanges()

            ds.Tables.Add(dt)
            ds.AcceptChanges()

        End If

        dv = ds.Tables(0).DefaultView
        dv.RowFilter = ""
        rptSelectedCompany.DataSource = dv.ToTable.DefaultView
        rptSelectedCompany.DataBind()


        Session("dsSelectedCompany") = ds
        txtCompanyName.Text = ""
        pnlSelectedCompany.Visible = True
    End Sub
    'Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.
    Private Sub rptSelectedCompany_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSelectedCompany.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "Remove") Then
                RemoveSelectedCompany(e.CommandArgument.ToString)
            End If
        End If
    End Sub
    'Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.
    Public Sub RemoveSelectedCompany(ByVal CompanyId As String)
        Dim ds As DataSet = CType(Session("dsSelectedCompany"), DataSet)
        Dim dt As DataTable = ds.Tables(0)
        Dim dv As New DataView
        dv = dt.Copy.DefaultView

        'add primary key as TagId 
        Dim keys(1) As DataColumn
        keys(0) = dt.Columns("CompanyID")

        Dim foundrow As DataRow() = dt.Select("CompanyID = '" & CompanyId & "'")
        If Not (foundrow Is Nothing) Then
            dt.Rows.Remove(foundrow(0))
        End If

        Dim i As Integer
        i = 0
        For Each drow As DataRow In ds.Tables(0).Rows
            drow.Item("CompanyID") = (i + 1).ToString
            i = i + 1
        Next

        dv = ds.Tables(0).DefaultView
        rptSelectedCompany.DataSource = dv.ToTable.DefaultView
        rptSelectedCompany.DataBind()
        Session("dsSelectedCompany") = ds
        pnlSelectedCompany.Visible = True
        If (ds.Tables(0).Rows.Count <= 0) Then
            pnlSelectedCompany.Visible = False
            Session("dsSelectedCompany") = Nothing
        End If




    End Sub
End Class