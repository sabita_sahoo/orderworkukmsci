

Partial Public Class PrintTescoReport
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Month As Integer
        Dim Year As Integer
        Dim Mode As String

        Month = Request("Month")
        Year = Request("Year")
        Mode = Request("Mode")

        Dim ds As DataSet = ws.WSWorkOrder.MS_TescoReport(Month, Year, Mode)
        Session.Add("PrintTescoReport" & Session.SessionID, ds)
        dlTescoReport.DataSource = ds.Tables("Locations").DefaultView
        dlTescoReport.DataBind()
    End Sub
    Public Sub dlTescoReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlTescoReport.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim dlJobsBreakUp As DataList = CType(e.Item.FindControl("dlJobsBreakUp"), DataList)

            Dim hdnGoodsLoc As HtmlInputHidden = CType(e.Item.FindControl("hdnGoodsLocation"), HtmlInputHidden)
            Dim ds As New DataSet
            ds = CType(Session("PrintTescoReport" & Session.SessionID), DataSet)
            Dim dv As New DataView
            If ds.Tables.Count > 2 Then
                If ds.Tables("JobDetails").Rows.Count > 0 Then
                    dlJobsBreakUp.Visible = True
                    dv = ds.Tables("JobDetails").DefaultView
                    dv.RowFilter = "GoodsLocation = " & hdnGoodsLoc.Value

                    dlJobsBreakUp.DataSource = dv.ToTable.DefaultView

                    dlJobsBreakUp.DataBind()
                End If
                tblTotal.Visible = True
                lblTotalJobs.Text = ds.Tables("TotalJobs").Rows(0).Item("TotalWOs")
                TotalEngAvail.Text = ds.Tables("JobDetails").Compute("sum(EngrAvail)", "").ToString
                TotalSuppliersUsed.Text = ds.Tables("TotalJobs").Rows(0).Item("cntDistinctSupplier").ToString
                TotalJobsReceived.Text = ds.Tables("JobDetails").Compute("sum(JobsReceived)", "").ToString
                TotalJobsInvoiced.Text = ds.Tables("JobDetails").Compute("sum(JobsInvoiced)", "").ToString
                TotalJobsSLA.Text = ds.Tables("JobDetails").Compute("sum(JobsSLA)", "").ToString
                TotalJobsActive.Text = ds.Tables("JobDetails").Compute("sum(JobsActive)", "").ToString

                If ds.Tables("JobDetails").Compute("sum(AverTime)", "") <> 0 Then
                    TotalAvgJobTime.Text = Strings.FormatNumber((ds.Tables("JobDetails").Compute("sum(AverTime)", "") / ds.Tables("JobDetails").Compute("sum(JobsInvoiced)", "")), 2, TriState.False, TriState.False, TriState.False)
                Else
                    TotalAvgJobTime.Text = "0"
                End If
                If ds.Tables("JobDetails").Compute("sum(JobsSLA)", "") <> 0 Then
                    TotalEngrOnTime.Text = Strings.FormatNumber(((ds.Tables("JobDetails").Compute("sum(JobsSLA)", "") / ds.Tables("JobDetails").Compute("sum(JobsInvoiced)", "")) * 100), 2, TriState.False, TriState.False, TriState.False) & " %"
                Else
                    TotalEngrOnTime.Text = "0.00 %"
                End If
            End If

        End If
    End Sub
End Class
