
Partial Public Class ChangeBillingLocation
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not IsNothing(Request("companyid")) Then
                If Request("companyid") <> "" Then
                    If Not IsNothing(Request("woids")) Then
                        If Request("woids") <> "" Then
                            ViewState.Add("woids", Request("woids"))
                        End If
                    End If
                    ViewState.Add(("companyid"), Request("companyid"))
                    PopulateBillingLocation(ViewState("companyid"))
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Populate Billing Location in the dropdown
    ''' </summary>
    ''' <param name="CompanyId"></param>
    ''' <remarks></remarks>
    Public Sub PopulateBillingLocation(ByVal CompanyId As Integer)
        Dim dsLoc As DataSet
        drpdwnBillingLocation.Visible = True
        Dim killCache As Boolean = True
        dsLoc = CommonFunctions.GetBillingLocations(Me.Page, CompanyId, killCache)
        Dim dv_loc As New DataView(dsLoc.Tables(0))
        dv_loc.RowFilter = "Type = 'Business'"
        drpdwnBillingLocation.DataSource = dv_loc
        drpdwnBillingLocation.DataBind()
        ViewState.Add("NewAddrId", drpdwnBillingLocation.SelectedValue)
    End Sub

    ''' <summary>
    ''' Handles Submit button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim ds As DataSet
        ds = ws.WSFinance.UpdateBillingLocation(ViewState("woids"), ViewState("companyid"), ViewState("NewAddrId"))
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("Success") = 1 Then
                    Response.Redirect("SalesInvoiceGeneration.aspx?sender=ChangeBilling&companyId=" & ViewState("companyid") & "&PN=" & Request("PN") & "&SC=" & Request("SC") & "&SO" & Request("SO") & "")
                Else
                    lblBillingLocation.Text = "Not able to update. Please try again"
                End If
            Else
                lblBillingLocation.Text = "Not able to update. Please try again"
            End If
        Else
            lblBillingLocation.Text = "Not able to update. Please try again"
        End If
    End Sub

    ''' <summary>
    ''' Handles Cancel button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Server.Transfer("SalesInvoiceGeneration.aspx?sender=ChangeBilling&companyId=" & ViewState("companyid") & "&PN=" & Request("PN") & "&SC=" & Request("SC") & "&SO" & Request("SO") & "")
    End Sub

    ''' <summary>
    ''' Stores the changed value of billing location in viewstate
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub drpdwnBillingLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpdwnBillingLocation.SelectedIndexChanged
        ViewState("NewAddrId") = drpdwnBillingLocation.SelectedValue
    End Sub
End Class