

Partial Public Class FastCloseWO
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Public objEmail As New Emails
    Private Delegate Sub AfterWorkOrderClosure(ByVal SelectedIDs As String)
    Private Delegate Sub AfterInvoiceGeneration(ByVal dsSendMail As DataSet)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack = True Then
            If Request.QueryString("mode") <> Nothing Then
                ViewState.Add("mode", Request.QueryString("mode"))
                ViewState.Add("WOID", Request.QueryString("WOID"))
            End If
            SetPageSettings()
            GetWOListing()
        End If
    End Sub
    ''' <summary>
    ''' Set the page settings i.e. accord to mode what is to be shown and whta is to be hidden
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub SetPageSettings()
        Select Case ViewState("mode").ToString.ToLower
            Case ApplicationSettings.WOGroupAccepted.ToString.ToLower, "ActiveElapsed".ToString.ToLower
                pnlGridListings.Visible = True
                pnlConfirm.Visible = False
                divInvalidWOIDs.Visible = False
                divValidWOIDs.Visible = True
                lblValidWO.Text = ResourceMessageText.GetString("FastClose")
            Case Else
                pnlGridListings.Visible = False
                pnlConfirm.Visible = False
        End Select
    End Sub
    ''' <summary>
    ''' Show confirmation panel and hide next and back button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        pnlGridListings.Visible = True
        pnlConfirm.Visible = True
        btnNext.Visible = False
        btnBack.Visible = False
        divInvalidWOIDs.Visible = False
    End Sub
    ''' <summary>
    ''' Should redirect to the previous listing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
   Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        'Poonam : OA - 483- Back button of AdminWOListing.aspx does not return to filtered listing
        'Dim lnk As String
        'lnk = "AdminWOListing.aspx?mode=" & ViewState("mode")
        'Response.Redirect(lnk)
        'Dim lnk As String
        'If ViewState("mode") = ApplicationSettings.WOGroupIssue Then
        '    lnk = "AdminWOIssueListing.aspx?mode=" & ViewState("mode") & "&from=multiplewo"
        'Else
        '    lnk = "AdminWOListing.aspx?mode=" & ViewState("mode")
        'End If
        Dim link As String = ""
        If ViewState("mode") = ApplicationSettings.WOGroupIssue Then
            link = "AdminWOIssueListing.aspx?mode=" & ViewState("mode") & "&from=multiplewo"
        Else
            link = "AdminWOListing.aspx?"
            link &= "&Group=" & ViewState("mode")
            link &= "&mode=" & ViewState("mode")
            link &= "&BizDivID=" & Request("BizDivID")
            link &= "&FromDate=" & Request("FromDate")
            link &= "&ToDate=" & Request("ToDate")
            If Not IsNothing(Request("SearchWorkorderID")) Then
                link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
            End If

            link &= "&sender=" & "fastclose"
        End If
        Response.Redirect(link)
    End Sub
    ''' <summary>
    ''' On click of Cancel it should be again goes into the same position as in before clicking on next
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        SetPageSettings()
        btnNext.Visible = True
        btnBack.Visible = True
    End Sub
    ''' <summary>
    ''' Updating the Status on clicking of confirm
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        'Dim executiontime As New Stopwatch
        'executiontime.Start()
        Dim WOID As String
        Dim Success As Integer = 0
        Dim ds As New DataSet
        WOID = ViewState("ValidWOID")
        divInvalidWOIDs.Visible = False
        ds = ws.WSWorkOrder.WOFastClose(WOID, Session("UserID"), "workorder")
        GenerateInvoices(WOID)
        'Dim dGenerateInvoices As [Delegate] = New AfterWorkOrderClosure(AddressOf GenerateInvoices)
        'ThreadUtil.FireAndForget(dGenerateInvoices, New Object() {WOID})
        'executiontime.Stop()
        'Dim OMTime As String
        'OMTime = executiontime.ElapsedMilliseconds.ToString

        'CommonFunctions.createLog("Total Time for Validating,Updating and auditing Data to the database for the Fast Close Workorders " & WOID & ", is" & OMTime & " ms.")
        'If ds.Tables(0).Rows.Count > 0 Then
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        sendMailer(ds)
        '    End If
        'End If
        Dim appendstr As String = ""
        If Not IsNothing(Request.QueryString("mode")) Then
            appendstr = "mode=" & Request.QueryString("mode")
        End If
        If Not IsNothing(Request.QueryString("CompanyID")) Then
            appendstr = appendstr + "&CompID=" & Request.QueryString("CompanyID")
        End If
        If Not IsNothing(Request("WorkorderID")) Then
            appendstr = appendstr + "&WorkorderID=" & Request.QueryString("WorkorderID")
        End If
        Dim lnk As String
        lnk = "AdminWOListing.aspx?" & appendstr
        Response.Redirect(lnk)
    End Sub
    Public Sub GenerateInvoices(ByVal SelectedIDs As String)
        Dim dsSendmail As DataSet = ws.WSWorkOrder.WOFastClose(SelectedIDs, Session("UserID"), "")
        Dim dvWODetails As New DataView
        Dim i As Integer
        If dsSendmail.Tables(1).Rows.Count > 0 Then
            dvWODetails = dsSendmail.Tables(1).DefaultView
        End If
        If dvWODetails.Count > 0 Then
            For i = 0 To dvWODetails.Count - 1
                With dvWODetails(i)
                    If (CInt(dvWODetails.Item(i)("upsellprice")) > 0 And dvWODetails.Item(i)("UpSellSalesInvoice").ToString <> "" And dvWODetails.Item(i)("CustomerEmail").ToString <> "") Then
                        'First Send Upsell invoices
                        'Create PDF and send email
                        Dim FileName As String = CommonFunctions.CreatePDFUsingAspxURL(dvWODetails.Item(i)("UpSellSalesInvoice").ToString, "UpsellInvoices", "ViewUpSellSalesInvoice.aspx?BizDivId=" & ApplicationSettings.OWUKBizDivId & "&InvoiceNo=" & dvWODetails.Item(i)("UpSellSalesInvoice").ToString, HttpContext.Current.Server.MapPath("~/Attachments/"))
                        Dim Attachment As System.Net.Mail.Attachment
                        Attachment = New System.Net.Mail.Attachment(FileName)
                        Emails.SendUpsellInvoices("Orderwork - Receipt details for your installation service and/or materials purchased - " & dvWODetails.Item(i)("WorkOrderID").ToString, dvWODetails.Item(i)("CustomerEmail").ToString, Attachment, dvWODetails.Item(i)("ClientCompanyId"))
                    End If
                End With
            Next
        End If
        If dsSendmail.Tables(0).Rows.Count > 0 Then
            
            Dim dSendMail As [Delegate] = New AfterInvoiceGeneration(AddressOf sendMailer)
            ThreadUtil.FireAndForget(dSendMail, New Object() {dsSendmail})
            
        End If

        
    End Sub
    ''' <summary>
    ''' Getting valid and Invalid Work Orders in case of submitted and 
    ''' in case of others only displaying Valid Work Orders
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetWOListing()
        Dim ds As DataSet
        Dim dvWOID As DataView
        ds = ws.WSWorkOrder.GetValidInvaildWOList(ViewState("WOID").ToString, "FastClose")
        dvWOID = ds.Tables(0).DefaultView
        Dim csvWOID As New StringBuilder
        For Each dRow As DataRowView In dvWOID
            If csvWOID.ToString = "" Then
                csvWOID.Append(dRow("WOID"))
            Else
                csvWOID.Append("," & dRow("WOID"))
            End If
        Next
        ViewState("ValidWOID") = csvWOID.ToString
        Select Case ViewState("mode").ToString.ToLower
            Case ApplicationSettings.WOGroupAccepted.ToString.ToLower, "ActiveElapsed".ToString.ToLower
                If ds.Tables(0).Rows.Count <> 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        dlValidWO.DataSource = ds.Tables(0)
                        dlValidWO.DataBind()
                    Else
                        divValidWOIDs.Visible = False
                    End If

                Else
                    btnNext.Visible = False
                End If
                If ds.Tables(1).Rows.Count <> 0 Then
                    divInvalidWOIDs.Visible = True
                    lblInValidWO.Text = "List of WorkOrders cannot be Updated."
                    dlInvalidWO.DataSource = ds.Tables(1)
                    dlInvalidWO.DataBind()
                End If
            Case Else

        End Select
    End Sub



    ''' <summary>
    ''' Sends mail to the respective contactid
    ''' </summary>
    ''' <param name="ds_WOSuccess"></param>
    ''' <param name="ContactId"></param>
    ''' <remarks></remarks>
    Private Sub sendMailer(ByVal ds_WOSuccess As DataSet)
        'send mailer to respective party
        Dim success As Boolean
        If ds_WOSuccess.Tables("SendMail").Rows.Count > 0 Then
            For i As Integer = 0 To ds_WOSuccess.Tables("SendMail").Rows.Count - 1
                'no email is sent to the supplier (Smart tech)
                If (ds_WOSuccess.Tables("SendMail").Rows(i).Item("SupplierCompanyID") <> ApplicationSettings.SmartTechCompany) Then
                    success = Emails.SendFastCloseMail(ds_WOSuccess.Tables("SendMail").Rows(i).Item("FullName"), ds_WOSuccess.Tables("SendMail").Rows(i).Item("WorkOrderId"), ds_WOSuccess.Tables("SendMail").Rows(i).Item("PurInvoiceNo"), ds_WOSuccess.Tables("SendMail").Rows(i).Item("Email"))
                End If
            Next i
        End If

    End Sub
End Class