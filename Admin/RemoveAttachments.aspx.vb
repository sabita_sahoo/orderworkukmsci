
Imports WebLibrary
Imports System.IO
Partial Public Class RemoveAttachments
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)

        If Not IsPostBack Then
            ' Code to Show/Hide control for OW Admin
            If Not IsNothing(Request.QueryString("WOID")) Then
                Dim WOID As String = (Request.QueryString("WOID")).ToString
                ViewState("SelectedWorkOrderId") = WOID
                ViewState("WOStatus") = CInt(Request.QueryString("WOStatus"))
                SelectFromDB(WOID)
            End If
        End If
    End Sub

    Public Function SelectFromDB(ByVal WOID As String)
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetAttachments(WOID, "", "GetAttachments", "", "AttachmentId", 1, 25)
        dlAttList.DataSource = ds
        dlAttList.DataBind()
        Return ds
    End Function

    '''' <summary>
    '''' To return dispplay path information of the attached file
    '''' </summary>
    '''' <param name="filePath"></param>
    '''' <returns></returns>
    '''' <remarks></remarks>
    Public Function getlink(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachments")

                Dim str As String = filePath.Substring(index)
                'str = ApplicationSettings.AttachmentDisplayPath(1) + str
                str = ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/" + str + CommonFunctions.AzureStorageAccessKey().ToString
                Return str
            End If
        End If
        Return ""
    End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim woid As Integer
        If Not IsNothing(ViewState("SelectedWorkOrderId")) Then
            woid = CInt(ViewState("SelectedWorkOrderId"))
            Dim ds As DataSet
            ds = ws.WSWorkOrder.GetAttachments("", "", "RemoveAttachments", woid, "", 0, 0)
            Dim filePath As String
            For Each drow As DataRow In ds.Tables(0).Rows
                filePath = drow.Item("FilePath")
                Try
                    Dim theFile As FileInfo = New FileInfo(filePath)
                    If (theFile.Exists) Then
                        File.Delete(filePath)
                    Else
                        Throw New FileNotFoundException()
                    End If
                Catch ex As FileNotFoundException
                Catch ex As Exception
                End Try
            Next
        End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "js", "CloseWindowForAttchments(" & ViewState("WOStatus") & ");", True)
    End Sub
End Class