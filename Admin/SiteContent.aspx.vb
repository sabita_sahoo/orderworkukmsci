

Partial Public Class SiteContent
    Inherits System.Web.UI.Page

    ''' <summary>
    ''' Shared wsobject to access web service webmethods
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            CommonFunctions.PopulateWebContentType(Page, ddlType, Session("BizDivID"))

            If Not IsNothing(Request("sender")) Then
                If Request("sender") = "SiteContentForm" Then
                    processBackToListing()

                Else
                    gvContacts.Sort("DateCreated", SortDirection.Descending)
                End If
            Else
                gvContacts.Sort("DateCreated", SortDirection.Descending)
            End If

            Dim currDate As DateTime = System.DateTime.Today
            txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            If Trim(Request("fromDate")) <> "" Then
                txtFromDate.Text = Request("fromDate")
            End If
            txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
            If Trim(Request("toDate")) <> "" Then
                txtToDate.Text = Request("toDate")
            End If



       
        End If

    End Sub



    Private Sub gvContacts_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvContacts.PreRender

    End Sub


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowDataBound
        MakeGridViewHeaderClickable(gvContacts, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvContacts, e.Row, Me)
        End If

    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex

    End Sub


    ''' <summary>
    ''' Event handler to handle page index change
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvContacts.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

    ''' <summary>
    ''' Selected Event handler for objectdatasource to set rowcount
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub


    ''' <summary>
    ''' To return dataset for listing
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim rowCount As Integer

        Dim showOnSite As Boolean = HttpContext.Current.Items("chkShowOnSite")

        Dim fromDate As String = HttpContext.Current.Items("fromDate")
        Dim toDate As String = HttpContext.Current.Items("toDate")
        Dim typeID As Integer
        If HttpContext.Current.Items("type") = "" Then
            typeID = 0
        Else
            typeID = CInt(HttpContext.Current.Items("type"))
        End If

        Dim ds As DataSet = ws.WSContact.GetAllWebContent(showOnSite, typeID, fromDate, toDate, sortExpression, startRowIndex, maximumRows, rowCount)
        HttpContext.Current.Items("rowCount") = rowCount
        Return ds
    End Function

    ''' <summary>
    ''' ObjectDataSource Controls select Count Method which returns the row count for the listing 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function


    ''' <summary>
    ''' populates the list of contacts
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnView.Click
        Page.Validate()
        If Page.IsValid Then
            PopulateGrid()
            gvContacts.PageIndex = 0
        End If
    End Sub

    ''' <summary>
    ''' Populate function to rebind data to the grid
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvContacts.DataBind()
        End If
    End Sub


    ''' <summary>
    ''' intializes the paramaters for ODC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ObjectDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles ObjectDataSource1.Selecting
        InitializeGridParameters()
    End Sub

    ''' <summary>
    ''' Initialise the SelectDB function parameter
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InitializeGridParameters()
        
        'If HttpContext.Current.Items("fromDate") <> "" Then
        '    HttpContext.Current.Items("fromDate") = Strings.FormatDateTime(txtFromDate.Text, DateFormat.ShortDate)

        'Else
        '    HttpContext.Current.Items("fromDate") = ""
        'End If

        HttpContext.Current.Items("fromDate") = Strings.FormatDateTime(txtFromDate.Text, DateFormat.ShortDate)
        HttpContext.Current.Items("toDate") = Strings.FormatDateTime(txtToDate.Text, DateFormat.ShortDate)
        HttpContext.Current.Items("chkShowOnSite") = chkShowOnSite.Checked
        HttpContext.Current.Items("type") = ddlType.SelectedValue
    End Sub


    ''' <summary>
    ''' Function to return date
    ''' </summary>
    ''' <param name="dateValue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getDate(ByVal dateValue As Object) As String

        If Not IsNothing(dateValue) And Not IsDBNull(dateValue) Then
            Return Strings.FormatDateTime(dateValue, DateFormat.ShortDate)
        End If

        Return ""
    End Function


    Public Function SetLinks(ByVal contentID As Integer) As String
     
        Dim showOnSite As Boolean = HttpContext.Current.Items("chkShowOnSite")
        Dim type As String = HttpContext.Current.Items("type")
        Dim fromDate As String = Strings.FormatDateTime(HttpContext.Current.Items("fromDate"), DateFormat.ShortDate)
        Dim toDate As String = Strings.FormatDateTime(HttpContext.Current.Items("toDate"), DateFormat.ShortDate)



        Dim appendLink As String = ""

        appendLink &= "contentID=" & contentID
        appendLink &= "&type=" & type
        appendLink &= "&OnSite=" & showOnSite
        appendLink &= "&FromDate=" & fromDate
        appendLink &= "&ToDate=" & toDate
        appendLink &= "&PS=" & gvContacts.PageSize
        appendLink &= "&PN=" & gvContacts.PageIndex
        appendLink &= "&SC=" & gvContacts.SortExpression
        appendLink &= "&SO=" & gvContacts.SortDirection
        appendLink &= "&sender=" & "SiteContent"

        Return appendLink
    End Function


    Public Sub processBackToListing()

        If Request("OnSite") = "" Then
            chkShowOnSite.Checked = True
        Else
            chkShowOnSite.Checked = CType(Request("OnSite"), Boolean)
        End If
        'chkShowOnSite.Checked = CType(Request("OnSite"), Boolean)
        ddlType.SelectedValue = Request("type")
        'txtFromDate.Text = Request("fromDate")
        'txtToDate.Text = Request("toDate")

        Dim currDate As DateTime = System.DateTime.Today


        If Trim(Request("fromDate")) <> "" Then
            txtFromDate.Text = Request("fromDate")
        Else
            txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
        End If

        If Trim(Request("toDate")) <> "" Then
            txtToDate.Text = Request("toDate")
        Else
            txtToDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
        End If

        If Trim(Request("PS")) <> "" Then
            gvContacts.PageSize = Request("PS")
        Else
            gvContacts.PageSize = 10
        End If

        ViewState!SortExpression = ""
        If Trim(Request("SC")) <> "" Then
            ViewState!SortExpression = Trim(Request("SC"))
        Else
            ViewState!SortExpression = "ContentID"
        End If
        'Sort direction
        Dim sd As SortDirection
        sd = SortDirection.Descending
        If Trim(Request("SO")) <> "" Then
            If Trim(Request("SO")).ToLower = "0" Then
                sd = SortDirection.Ascending
            End If
        Else
            sd = SortDirection.Ascending
        End If




        gvContacts.Sort(ViewState!SortExpression, sd)
        gvContacts.DataBind()

        If Trim(Request("PN")) <> "" Then
            gvContacts.PageIndex = Request("PN")
        Else
            gvContacts.PageIndex = 0
        End If


    End Sub


End Class