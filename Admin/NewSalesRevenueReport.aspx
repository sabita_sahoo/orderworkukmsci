<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewSalesRevenueReport.aspx.vb" Inherits="Admin.NewSalesRevenueReport" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :"%>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %> 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
   




			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td>&nbsp;</td>
							<td class="HeadingRed" ><strong>Revenue Report</strong></td>
						</tr>

					  </table>
			      <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
			      <ContentTemplate>

					  <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top:15px; " >
					  	<tr >
							<td width="10"></td>
							<td width="250" >
									<table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr valign="top">
											  <td class="formTxt" valign="top" align="left" width="18">&nbsp;</td>
											  <td class="formTxt" valign="top" align="left" width="156">Report Type</td>
											  <td class="formTxt" valign="top" align="left" width="18">&nbsp;</td>
											  <TD class="formTxt" vAlign="top"  width="156" align="left" height="14">Month </TD>
											  <td class="formTxt" align="left" width="156">&nbsp;</td>
											  <TD class="formTxt" align="left" height="14">Year </TD>

                                            </tr>
											<tr valign="top">
											  <td height="24" align="left">&nbsp;</td>
											  <td align="left"><select id="ddReportType" runat="server" name="ddReportType" class="formFieldGrey121">
												  <option value="Consolidated" >Consolidated</option>
                                                  <option value="Retail">Retail</option>
                                                  <option value="IT">IT</option>												 
												</select>
											  </td>
											  <td height="24" align="left">&nbsp;</td>
											  <td align="left"><select id="ddMonth" runat="server" name="ddMonth" class="formFieldGrey121">
												  <option value=1>January</option>
												  <option value=2>February</option>
												  <option value=3>March</option>
												  <option value=4>April</option>
												  <option value=5>May</option>
												  <option value=6>June</option>
												  <option value=7>July</option>
												  <option value=8>August</option>
												  <option value=9>September</option>
												  <option value=10>October</option>
												  <option value=11>November</option>
												  <option value=12>December</option>
												</select>
											  </td>
											  <td height="24" align="left" width="10">&nbsp;</td>
											  <td align="left"><select id="ddYear" runat="server" name="ddYear" class="formFieldGrey121">
												  <option value="2006">2006</option>
												  <option value="2007">2007</option>
												  <option value="2008">2008</option>
												  <option value="2009">2009</option>
                                                  <option value="2010">2010</option>
                                                  <option value="2011">2011</option>
                                                  <option value="2012">2012</option>
                                                  <option value="2013">2013</option>
                                                  <option value="2014">2014</option>
                                                  <option value="2015">2015</option>
                                                  <option value="2016">2016</option>
                                                  <option value="2017">2017</option>
                                                  <option value="2018">2018</option>
                                                  <option value="2019">2019</option>
                                                  <option value="2020">2020</option>
												</select>
											  </td>
											</tr>
                                        
										  </table>
							</td>
                             <td align="left">
                                                </td>
							<td width="20"> <TABLE cellSpacing="0" cellPadding="0" width="350" border="0" >
									                <TR valign="bottom">
										                <td width="350" style="padding-top:16px; ">
											                <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">  
  <tr align="left" valign="top">
  <td width="6"></td>
    <td class="formTxt" valign="top" height="16">From Date (dd/mm/yyyy) <span class="bodytxtValidationMsg"></span>
       
    </td>
    <td width="10">&nbsp;</td>
    <td class="formTxt" height="16">To Date (dd/mm/yyyy)  <span class="bodytxtValidationMsg"></span>
    </td>
  </tr>
  <tr valign="top">
  <td width="6"></td>
    <td height="24" align="left" style="cursor:pointer; vertical-align:top;" ><asp:TextBox ID="txtFromDate" TabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
	            <img alt="Click to Select" src="Images/calendar.gif" id="btnFromDate" style="cursor:pointer; vertical-align:top;" />		    	
              <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnFromDate TargetControlID="txtFromDate" ID="calFromDate" runat="server">
              </cc2:CalendarExtender>
			  
	</td>
	<td width="10">&nbsp;</td>
    <td align="left"><asp:TextBox ID="txtToDate" TabIndex="3" runat="server" CssClass="formField105"></asp:TextBox>            
            <img alt="Click to Select" src="Images/calendar.gif" id="btnToDate" style="cursor:pointer; vertical-align:top;" />
              <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnToDate TargetControlID="txtToDate" ID="calToDate" runat="server">
              </cc2:CalendarExtender>
			  
	</td>
  </tr>
  <tr valign="top">
  <td width="6"></td>
  <td class="formTxt" colspan="3">
      <asp:ValidationSummary ID="valSumm" runat="server"></asp:ValidationSummary>
      <asp:CustomValidator ID="rngDate" runat="server" ErrorMessage="To Date should be greater than From Date" ForeColor="#EDEDEB" ControlToValidate="txtToDate">*</asp:CustomValidator>
      <asp:RegularExpressionValidator ID="regExpFromDate" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtFromDate" ErrorMessage="From Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
      <asp:RegularExpressionValidator ID="regExpToDate" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtToDate" ErrorMessage="To Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
 </td>    
  </tr>  
</table> 
									                 </td>
									                </TR>
								                </TABLE></td>
							<td align="left"  style="padding-top:15px; " >
								<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                    <asp:LinkButton ID="lnkBtnGenerateReport" causesValidation=True runat="server" CssClass="txtButtonRed" Text="Generate Report"></asp:LinkButton>
                                </div>
							 </td>
							 <td width="60" align="left" ></td>
						</tr>
					</table>
				  </ContentTemplate>
					<Triggers>
						<asp:PostBackTrigger   ControlID="lnkBtnGenerateReport"   /> 
				    </Triggers>
					 </asp:UpdatePanel>
					  <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
						<ProgressTemplate >
				   <div class="gridText">
						<img  align=middle src="Images/indicator.gif" />
						<b>Fetching Data... Please Wait</b>
					</div>     
				</ProgressTemplate>
						</asp:UpdateProgress>
						<cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />					
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
					  <tr>
						<td style="padding-top:20px; ">
							 <rsweb:ReportViewer ID="ReportViewerOW" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" ProcessingMode="Remote" Width="100%" bgColor="#FFFFFF">
									<ServerReport ReportServerUrl="" />
							 </rsweb:ReportViewer>
						</td>
					  </tr>						  
					  </table>
			<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>


</asp:Content>
