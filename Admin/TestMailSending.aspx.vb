﻿Public Class TestMailSending
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
    End Sub

    Private Sub btnSendEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendEmail.Click
        SendMail.SendMail(ApplicationSettings.MailFrom + " <" + ApplicationSettings.EmailInfo() + ">", Trim(txtEmail.Text), "OW Test Email Body.", "OW Test Email", True, "sumitk@iniquus.com", False, False, "")
        txtEmail.Text = ""
        lblMsg.Text = "Email sent to user"
    End Sub
End Class