<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewSalesInvoice.aspx.vb" Inherits="Admin.ViewSalesInvoice" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" style="height:100%;">
<script type="text/javascript">
<asp:Literal id="ltCountry" runat="server"></asp:Literal>
</script>
<script type="text/javascript" language="javascript" src="JS/Scripts.js"></script>
<head id="Head1" runat="server">
    <title>Invoice</title>
</head>
<%--Poonam modified on 29-03-2017 Task - OA-430 : OA - Print button throw issue when Sales Invoice has �0 Wholsale Price --%>
<body style="height:100%;" onload="javascript:this.print()">
    <form id="form1" runat="server" style="height:100%;">
    
			 <%@ Register TagPrefix="uc1" TagName="UCViewInvoice" Src="~/UserControls/UK/UCMSViewSalesInvoiceUK.ascx" %>
			 <uc1:UCViewInvoice id="UCMSViewSalesInvoice1" runat="server"></uc1:UCViewInvoice> 

    </form>
	
	
</body>
</html>

