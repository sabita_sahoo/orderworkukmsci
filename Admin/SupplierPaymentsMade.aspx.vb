
Partial Public Class SupplierPaymentsMade
    Inherits System.Web.UI.Page
    Protected WithEvents UCSupplierPayments1 As UCMSSupplierPayments
    Protected WithEvents UCDateRange1 As UCDateRange
    Protected WithEvents UCSearchContact1 As UCSearchContact

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            UCSupplierPayments1.SubAccount = 0
            UCSupplierPayments1.Status = "Made"
            UCSupplierPayments1.Viewer = ApplicationSettings.ViewerAdmin
            UCSupplierPayments1.BizDiv = Session("BizDivId")
            UCSearchContact1.BizDivID = Session("BizDivId")
            UCSearchContact1.Filter = "PIProcessed"
            Dim currDate As DateTime = System.DateTime.Today
            UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
            UCSupplierPayments1.FromDate = UCDateRange1.txtFromDate.Text
            UCSupplierPayments1.ToDate = UCDateRange1.txtToDate.Text
            UCSearchContact1.populateContact()
            btnExport.HRef = "ExportToExcel.aspx?page=SupplierPaymentsMade&bizDivId=" & Session("BizDivId") & "&subAccount=" & 0 & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & _
                               UCDateRange1.txtToDate.Text & "&status=Made&sortExpression=DateCompleted&startRowIndex=0&maximumRows=0"
        End If
    End Sub

    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkView.Click
        Page.Validate()
        If Page.IsValid Then
            Dim subaccount As Integer
            If UCSearchContact1.ddlContact.SelectedValue = "" Then
                UCSupplierPayments1.SubAccount = 0
                subaccount = 0
            Else
                UCSupplierPayments1.SubAccount = UCSearchContact1.ddlContact.SelectedValue
                subaccount = UCSearchContact1.ddlContact.SelectedValue
            End If
            btnExport.HRef = "ExportToExcel.aspx?page=SupplierPaymentsMade&bizDivId=" & Session("BizDivId") & "&subAccount=" & subaccount & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & _
                                           UCDateRange1.txtToDate.Text & "&status=Made&sortExpression=DateCompleted&startRowIndex=0&maximumRows=0"
            Populate()
            UCSupplierPayments1.PopulateGrid()
        End If
    End Sub

    Public Sub Populate()
        UCSupplierPayments1.BizDiv = Session("BizDivId")
        UCSearchContact1.BizDivID = Session("BizDivId")
        UCSearchContact1.Filter = "PIProcessed"
        UCSupplierPayments1.FromDate = UCDateRange1.txtFromDate.Text
        UCSupplierPayments1.ToDate = UCDateRange1.txtToDate.Text
    End Sub

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
End Class