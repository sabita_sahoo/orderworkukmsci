﻿Public Class InvoiceApproval
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("RoleGroupID") = ApplicationSettings.RoleOWFinanceID Or Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
            tdApproveTop.Visible = True
            tdApproveBottom.Visible = True
        Else
            tdApproveTop.Visible = False
            tdApproveBottom.Visible = False
        End If
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            Session("InvoiceToEdit") = Nothing
            Dim vat As Decimal
            vat = ApplicationSettings.VATPercentage
            hdnvatvalue.Value = vat / 100
            If Not IsNothing(Request.QueryString("InvoiceNo")) Then
                ViewState.Add("InvoiceNo", Request.QueryString("InvoiceNo"))
                lblInvoiceNo.Text = Request.QueryString("InvoiceNo")
                PopulateData()
            End If
            lnkBackToListing.HRef = getBackToListingLink()
        End If
    End Sub
    Public Function getBackToListingLink() As String
        Dim link As String = ""
        If Not IsNothing(Request("Sender")) Then
            Select Case Request("sender")
                Case "InvoiceApprovalList"
                    link &= "~\InvoiceApprovalListing.aspx"
                Case "PIUnpaidAvailable"
                    link = "PIUnpaidAvailable.aspx?"
                    link &= "invoiceNo=" & Request("invoiceNo")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&CompanyId=" & Request("CompanyId")
                    link &= "&Over30Days=" & Request("Over30Days")
                    link &= "&sender=" & "PIUnpaidAvailable"
                Case "SuppPayOutstanding"
                    link = "SupplierPaymentsOutstanding.aspx?"
                    link &= "invoiceNo=" & Request("invoiceNo")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&CompanyId=" & Request("CompanyId")
                    link &= "&status=" & Request("status")
                    link &= "&sender=" & "SuppPayOutstanding"
                Case "Receivables"
                    link = "~\Receivables.aspx?"
                    link &= "&companyId=" & Request("companyId")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    If (Request("SC") = "CloseDate") Then
                        link &= "&SC=" & "InvoiceDate"
                    Else
                        link &= "&SC=" & Request("SC")
                    End If
                    link &= "&SO=" & Request("SO")
                    link &= "&tab=" & Request("tab")
                    link &= "&sender=" & "Receivables"
                Case Else
                    Return "~\PIUnpaidAvailable.aspx"
            End Select
        Else
            link = "~\PIUnpaidAvailable.aspx"
        End If
        Return link
    End Function
    Public Sub PopulateData()
        divValidationApproveData.Visible = False
        lblValidationApproveData.Text = ""
        Dim ds As New DataSet
        ds = ws.WSFinance.MS_GetInvoiceToEdit(ViewState("InvoiceNo").ToString)
        If ds.Tables.Count > 0 Then
            Session("InvoiceToEdit") = ds
            If ds.Tables(0).Rows.Count > 0 Then
                PopulateInvoiceToEdit()
            End If
        End If
    End Sub
    Protected Sub PopulateInvoiceToEdit()
        Dim dsInvoiceToEdit As DataSet
        dsInvoiceToEdit = CType(Session("InvoiceToEdit"), DataSet)
        If dsInvoiceToEdit.Tables.Count > 0 Then
            If dsInvoiceToEdit.Tables(0).Rows.Count > 0 Then
                Dim dvInvoiceToEdit As New DataView
                dvInvoiceToEdit = dsInvoiceToEdit.Tables(0).DefaultView
                DLInvoiceToEdit.Visible = True
                DLInvoiceToEdit.DataSource = dvInvoiceToEdit.ToTable.DefaultView
                DLInvoiceToEdit.DataBind()

                dvInvoiceToEdit.RowFilter = "RowNum = " & dsInvoiceToEdit.Tables(0).Rows(0).Item("MaxSequence")
                lblGrandTotal.Text = dvInvoiceToEdit.ToTable.Rows(0).Item("GrandTotal")
                dvInvoiceToEdit.RowFilter = ""

                If dsInvoiceToEdit.Tables.Count > 1 Then
                    tblOriginlaInvoice.Visible = True
                    Dim dvOriginalInvoice As New DataView
                    dvOriginalInvoice = dsInvoiceToEdit.Tables(1).DefaultView
                    DLOriginalInvoice.Visible = True
                    DLOriginalInvoice.DataSource = dvOriginalInvoice.ToTable.DefaultView
                    DLOriginalInvoice.DataBind()

                    dvOriginalInvoice.RowFilter = "RowNum = " & dsInvoiceToEdit.Tables(1).Rows(0).Item("MaxSequence")
                    lblOriginalGrandTotal.Text = dvOriginalInvoice.ToTable.Rows(0).Item("GrandTotal")
                    dvOriginalInvoice.RowFilter = ""
                Else
                    tblOriginlaInvoice.Visible = False
                End If

                If dsInvoiceToEdit.Tables.Count > 2 Then
                    Dim dvInvoiceNotes As New DataView
                    dvInvoiceNotes = dsInvoiceToEdit.Tables(2).DefaultView
                    If (dvInvoiceNotes.Table.Rows.Count > 0) Then
                        divNotes.Visible = True
                        gvNotes.DataSource = dvInvoiceNotes.ToTable.DefaultView
                        gvNotes.DataBind()
                    Else
                        divNotes.Visible = False
                    End If
                Else
                    divNotes.Visible = False
                End If

            Else
                DLInvoiceToEdit.Visible = False
                DLInvoiceToEdit.DataSource = Nothing
                DLInvoiceToEdit.DataBind()
            End If
        End If
    End Sub

    Protected Sub ResetTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetTop.Click, btnResetBottom.Click
        PopulateData()
    End Sub

    Private Sub lnkAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddNew.Click
        AddApproveData()
    End Sub

    Public Sub AddApproveData()

        UpdateApproveData()

        Dim ds As New DataSet
        ds = CType(Session("InvoiceToEdit"), DataSet)

        Dim MaxFinanceApprovalId As Integer
        Dim GrandTotal As Decimal
        GrandTotal = 0

        Dim RowNum As Integer
        RowNum = 1

        For Each drowFinanceApproval As DataRow In ds.Tables(0).Rows
            MaxFinanceApprovalId = drowFinanceApproval.Item("MaxFinanceApprovalId") + 1
            drowFinanceApproval.Item("MaxFinanceApprovalId") = MaxFinanceApprovalId
            GrandTotal = GrandTotal + drowFinanceApproval.Item("Total")
        Next

        ds.AcceptChanges()
        Session("InvoiceToEdit") = ds


        Dim dv As New DataView
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                dv = ds.Tables(0).DefaultView
                RowNum = RowNum + dv.ToTable.Rows.Count

                For Each drQ As DataRow In dv.Table.Rows
                    drQ.Item("MaxSequence") = RowNum
                Next

                ds.AcceptChanges()
                Session("InvoiceToEdit") = ds

            End If
        End If


        Dim dt As New DataTable

        dt = ds.Tables(0)

        Dim drow As DataRow
        drow = dt.NewRow()

        drow("FinanceApprovalId") = MaxFinanceApprovalId
        drow("InvoiceNo") = ViewState("InvoiceNo").ToString
        drow("InvoiceDate") = Date.Today.ToString("dd/MM/yyyy")
        drow("WOID") = 0
        drow("WorkOrderId") = ""
        drow("Discount") = 0
        drow("AmountAfterDisc") = 0
        drow("VAT") = 0
        drow("Total") = 0
        drow("Description") = ""
        drow("WOVal") = 0
        drow("MaxFinanceApprovalId") = MaxFinanceApprovalId
        drow("GrandTotal") = GrandTotal
        drow("RowNum") = RowNum
        drow("MaxSequence") = RowNum

        dt.Rows.Add(drow)
        dt.AcceptChanges()

        ds.AcceptChanges()

        Session("InvoiceToEdit") = ds

        ds.AcceptChanges()
        Session("InvoiceToEdit") = ds

        PopulateInvoiceToEdit()

    End Sub
    Private Sub DLInvoiceToEdit_ItemCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles DLInvoiceToEdit.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            UpdateApproveData()
            If (e.CommandName = "RemoveLineItem") Then
                Dim hdnFinanceApprovalId As HtmlInputHidden = CType(e.Item.FindControl("hdnFinanceApprovalId"), HtmlInputHidden)
                RemoveApproveData(hdnFinanceApprovalId.Value.ToString)
            ElseIf (e.CommandName = "MoveUp") Then 'Move Up
                Dim Sequence As Integer = CInt(e.CommandArgument)
                If (Sequence <> 0) Then
                    MoveUpDown(Sequence, "Up")
                End If
            ElseIf (e.CommandName = "MoveDown") Then 'Move Down
                Dim Sequence As Integer = CInt(e.CommandArgument)
                If (Sequence <> 0) Then
                    MoveUpDown(Sequence, "Down")
                End If
            End If
        End If
    End Sub
    Public Sub MoveUpDown(ByVal Sequence As String, ByVal Mode As String)
        Dim ds As New DataSet
        ds = CType(Session("InvoiceToEdit"), DataSet)

        Dim GrandTotal As Decimal
        Dim dvInvoiceToEdit As New DataView
        dvInvoiceToEdit = ds.Tables(0).DefaultView
        dvInvoiceToEdit.RowFilter = "RowNum = " & ds.Tables(0).Rows(0).Item("MaxSequence")
        GrandTotal = dvInvoiceToEdit.ToTable.Rows(0).Item("GrandTotal")
        dvInvoiceToEdit.RowFilter = ""

        For Each drow As DataRow In ds.Tables(0).Rows
            If drow.Item("RowNum") = Sequence Then
                If (Mode = "Up") Then
                    drow.Item("RowNum") = drow.Item("RowNum") - 1
                ElseIf (Mode = "Down") Then
                    drow.Item("RowNum") = drow.Item("RowNum") + 1
                End If
            ElseIf drow.Item("RowNum") = Sequence - 1 Then
                If (Mode = "Up") Then
                    drow.Item("RowNum") = drow.Item("RowNum") + 1
                End If
            ElseIf drow.Item("RowNum") = Sequence + 1 Then
                If (Mode = "Down") Then
                    drow.Item("RowNum") = drow.Item("RowNum") - 1
                End If
            End If
            drow.Item("GrandTotal") = GrandTotal
        Next

        ds.Tables(0).DefaultView.Sort = "RowNum"

        ds.Tables(0).AcceptChanges()
        Session("InvoiceToEdit") = ds

        PopulateInvoiceToEdit()

    End Sub
    Public Sub RemoveApproveData(ByVal FinanceApprovalId As String)
        Dim ds As New DataSet
        ds = CType(Session("InvoiceToEdit"), DataSet)

        Dim i As Integer
        i = 0
        Dim GrandTotal As Decimal
        GrandTotal = 0

        For Each drow As DataRow In ds.Tables(0).Rows
            If drow.Item("FinanceApprovalId") <> FinanceApprovalId Then
                i = i + 1
                drow.Item("RowNum") = i
                drow.Item("MaxSequence") = drow.Item("MaxSequence") - 1
                GrandTotal = GrandTotal + drow.Item("Total")
                drow.Item("GrandTotal") = GrandTotal
            End If
        Next
        ds.Tables(0).AcceptChanges()
        Session("InvoiceToEdit") = ds

        Dim dtFinanceApproval As DataTable = ds.Tables(0)
        Dim dvFinanceApproval As New DataView
        dvFinanceApproval = dtFinanceApproval.Copy.DefaultView

        'add primary key as RedirectionId 
        Dim keysRedirection(1) As DataColumn
        keysRedirection(0) = dtFinanceApproval.Columns("RedirectionId")

        Dim foundrowFinanceApproval As DataRow() = dtFinanceApproval.Select("FinanceApprovalId = '" & FinanceApprovalId & "'")
        If Not (foundrowFinanceApproval Is Nothing) Then
            dtFinanceApproval.Rows.Remove(foundrowFinanceApproval(0))
            ds.Tables(0).AcceptChanges()
            Session("InvoiceToEdit") = ds
        End If

        PopulateInvoiceToEdit()
    End Sub

    Public Sub UpdateApproveData()

        divValidationApproveData.Visible = False
        lblValidationApproveData.Text = ""

        Dim hdnFinanceApprovalId As HtmlInputHidden

        Dim ds As New DataSet
        ds = CType(Session("InvoiceToEdit"), DataSet)

        Dim dvFinanceApproval As New DataView
        dvFinanceApproval = ds.Tables(0).DefaultView

        If Not IsNothing(DLInvoiceToEdit) Then

            Dim GrandTotal As Decimal
            Dim i As Integer
            i = 0

            For Each row As DataListItem In DLInvoiceToEdit.Items
            
                hdnFinanceApprovalId = CType(row.FindControl("hdnFinanceApprovalId"), HtmlInputHidden)

                dvFinanceApproval.RowFilter = "FinanceApprovalId = " & hdnFinanceApprovalId.Value

                i = i + 1
                If (i = 1) Then
                    GrandTotal = 0
                End If

                If dvFinanceApproval.Count > 0 Then
                    If (CType(row.FindControl("txtDescription"), TextBox).Text <> "") Then
                        dvFinanceApproval.Item(0).Item("Description") = CType(row.FindControl("txtDescription"), TextBox).Text
                    Else
                        dvFinanceApproval.Item(0).Item("Description") = ""
                    End If

                    If (CType(row.FindControl("txtWOVal"), TextBox).Text <> "") Then
                        If (IsNumeric(CType(row.FindControl("txtWOVal"), TextBox).Text.Trim)) Then
                            dvFinanceApproval.Item(0).Item("WOVal") = CType(row.FindControl("txtWOVal"), TextBox).Text
                        End If
                    Else
                        dvFinanceApproval.Item(0).Item("WOVal") = 0
                    End If
                    If (CType(row.FindControl("txtDiscount"), TextBox).Text <> "") Then
                        If (IsNumeric(CType(row.FindControl("txtDiscount"), TextBox).Text.Trim)) Then
                            dvFinanceApproval.Item(0).Item("Discount") = CType(row.FindControl("txtDiscount"), TextBox).Text
                        End If
                    Else
                        dvFinanceApproval.Item(0).Item("Discount") = 0
                    End If
                    If (CType(row.FindControl("txtVat"), TextBox).Text <> "") Then
                        If (IsNumeric(CType(row.FindControl("txtVat"), TextBox).Text.Trim)) Then
                            dvFinanceApproval.Item(0).Item("Vat") = CType(row.FindControl("txtVat"), TextBox).Text
                        End If
                    Else
                        dvFinanceApproval.Item(0).Item("Vat") = 0
                    End If

                    dvFinanceApproval.Item(0).Item("AmountAfterDisc") = (dvFinanceApproval.Item(0).Item("WOVal") - dvFinanceApproval.Item(0).Item("Discount"))
                    dvFinanceApproval.Item(0).Item("Total") = (dvFinanceApproval.Item(0).Item("WOVal") - dvFinanceApproval.Item(0).Item("Discount")) + dvFinanceApproval.Item(0).Item("Vat")
                    GrandTotal = GrandTotal + dvFinanceApproval.Item(0).Item("Total")

                    dvFinanceApproval.Item(0).Item("GrandTotal") = GrandTotal

                End If

                dvFinanceApproval.RowFilter = ""
                ds.Tables(0).AcceptChanges()
                Session("InvoiceToEdit") = ds
            Next
        End If
    End Sub

    Protected Sub Save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click, btnSaveBottom.Click
        SaveApprove("Edit")
    End Sub
    Private Function ValidatingApproveData() As String


        Dim flag As Boolean = True
        Dim errmsg As String = ""
        Dim i As Integer = 0
        Dim j As Integer = 0

        If Not IsNothing(DLInvoiceToEdit) Then
            For Each row As DataListItem In DLInvoiceToEdit.Items
                i = i + 1
                j = 0
                'Validating TimeSlotFrom is not blank
                If CType(row.FindControl("txtDescription"), TextBox).Text.Trim.Length = 0 Then
                    If flag = True Then
                        errmsg = "- Please Enter Description for Line Item " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Enter Description for Line Item " & i
                    End If
                End If
                If CType(row.FindControl("txtWOVal"), TextBox).Text.Trim.Length = 0 Then
                    If flag = True Then
                        errmsg = "- Please Enter WOVal for Line Item " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Enter WOVal for Line Item " & i
                    End If
                End If
                If CType(row.FindControl("txtVat"), TextBox).Text.Trim.Length = 0 Then
                    If flag = True Then
                        errmsg = "- Please Enter Vat for Line Item " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Enter Vat for Line Item " & i
                    End If
                End If
            Next
        End If

        If Not flag Then
            divValidationApproveData.Visible = True
            lblValidationApproveData.Text = errmsg.ToString
        End If
        Return errmsg
    End Function

    Protected Sub ApproveTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApproveTop.Click, btnApproveBottom.Click
        SaveApprove("Approve")
    End Sub

    Public Sub SaveApprove(ByVal Mode As String)
        UpdateApproveData()
        If ValidatingApproveData() = "" Then

            Dim xmlContent As String = CType(Session("InvoiceToEdit"), DataSet).GetXml
            Dim dsSuccess As New DataSet
            dsSuccess = ws.WSFinance.EditInvoice(xmlContent, ViewState("InvoiceNo").ToString, Mode, txtNote.Text.Trim, Session("UserID"))
            If dsSuccess.Tables.Count > 0 Then
                If dsSuccess.Tables(0).Rows.Count > 0 Then
                    If CStr(dsSuccess.Tables(0).Rows(0)(0)) = "1" Then
                        Dim link As String = getBackToListingLink()
                        Response.Redirect(link)
                    End If
                End If
            End If
        Else
            PopulateInvoiceToEdit()
        End If
    End Sub
End Class