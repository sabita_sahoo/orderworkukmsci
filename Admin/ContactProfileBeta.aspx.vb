﻿Public Class CompanyProfileBeta
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'OD-804 - .Net - Skills, Accreditation And Location coverage changes To make it Live
        Dim loggedInId As Integer = Convert.ToInt32(Session("UserId"))
        Dim userId As Integer = Convert.ToInt32(Session("ContactIDForSkills")) 'Session(ContactIDForSkills) will be either company id or user id depending upon the form iframe is being called
        iFrameContactProfile.Attributes.Add("src", ApplicationSettings.NewPortalURL.ToString & "/profile/" & loggedInId & "/" & userId)
    End Sub

End Class