<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ListMessages.aspx.vb" Inherits="Admin.ListMessages" MasterPageFile="~/MasterPage/OWAdmin.Master" title="List Messages"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>






	
	        
			<asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server">
			<ContentTemplate>
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<input id="hdnPageSize" type="hidden" name="hdnPageSize" runat="server"> 
			<input id="hdnPageNo" type="hidden" name="hdnPageNo" runat="server">
			<input id="sortExpr" type="hidden" name="sortExpr" runat="server"> 
			<input id="sortOrder" type="hidden" name="sortOrder" runat="server">
			<input id="totalRecs" type="hidden" name="totalRecs" runat="server"> 
			<input id="pageRecs" type="hidden" name="pageRecs" runat="server">
			<input id="defaultSortImgId" type="hidden" name="defaultSortImgId" runat="server"> 			
			<input type="hidden" id="hdnDateID" name="hdnDateID" runat="server">
			<input type="hidden" id="hdnDate" name="hdnDate" runat="server">
			<input type="hidden" id="hdntxt" name="hdntxt" runat="server">
			<input type="hidden" id="hdnIsDeleted" name="hdnIsDeleted" runat="server">			
			           
             <table width="100%"><tr><td width="10">&nbsp;</td><td>
             <asp:Label ID="lblErr" CssClass="bodytxtValidationMsg" runat="server" Text=""></asp:Label>
             <asp:Panel ID="pnlListing" runat="server" style="display:block;">             
             <table border="0" cellPadding="0" cellSpacing="0" width="440">
							  <TR>
                                <td width="80"><asp:CheckBox CssClass="formTxt"  ID="chkbxHideDeleted" runat="server" AutoPostBack="true" Text="Hide Deleted" Checked="True"  /></td>
								<td width="100">
                                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                        <asp:LinkButton id="lnkbtnPostMessage" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Post Message&nbsp;</asp:LinkButton>
                                    </div>
                                </td>
							  </TR>
							</table>						
                 <asp:GridView ID="gvMessages" runat="server" AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
               PagerSettings-Mode="NextPreviousFirstLast" CssClass="gridrow"  PagerSettings-Position="TopAndBottom" Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               <PagerTemplate>                  
                  
                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>							
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
                 </PagerTemplate>
                  <Columns>                                       
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="22%"  HeaderStyle-CssClass="gridHdr"  DataField="DateModified" SortExpression="DateModified" HeaderText="Date Modified" />            
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="22%"  HeaderStyle-CssClass="gridHdr"  DataField="Subject" SortExpression="Subject" HeaderText="Subject" />            
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="22%"  HeaderStyle-CssClass="gridHdr"  DataField="IsDeleted" SortExpression="Status" HeaderText="IsDeleted" />
                     <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">                
                        <ItemStyle CssClass="gridRowIcons" Wrap=true  Width="5%"/>
                        <ItemTemplate>
                        <a href='<%#"PostMessages.aspx?MsgID=" & Container.DataItem("MsgID") & "&ForContactClassID=" & Container.DataItem("ForContactClassID") %>'><img src="Images/Icons/Edit.gif" title="Edit" width="12" height="11" hspace="2" vspace="0" border="0"></a> 
               	        
				        </ItemTemplate>  
                     </asp:TemplateField>                                             
                </Columns>
                  <AlternatingRowStyle  CssClass="gridRow" />
                 
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
                 </asp:GridView>
             <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.ListMessages" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
            </asp:Panel>
            				      
                      
            
			
          				</td><td width="10">&nbsp;</td></tr></table>			 
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
         </ContentTemplate>
			</asp:UpdatePanel>
			
			 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelCreateWorkOrder" runat="server">
					<ProgressTemplate>
						<div>
							<img  align=middle src="Images/indicator.gif" />
							Please Wait...
						</div>      
					</ProgressTemplate>
			</asp:UpdateProgress>
			<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePanelCreateWorkOrder" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
      
</asp:Content>
