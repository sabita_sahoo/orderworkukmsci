<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddMultipleUsers.aspx.vb" Inherits="Admin.AddMultipleUsers" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Admin Add Multiple Users"%>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>






    
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            <table><tr><td width="10">&nbsp;</td><td>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>                
                <div id="divValidationMain" class="divValidation" runat="server" style="display:none;" >
	                <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
			                <tr>
				                <td height="15" align="center">&nbsp;</td>
				                <td height="15">&nbsp;</td>
				                <td height="15">&nbsp;</td>
			                </tr>
			                <tr valign="middle">
				                <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				                <td class="validationText"><div  id="divValidationMsg"></div>
				                <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
				                <asp:ValidationSummary id="validationSummarySubmit"  runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="true"></asp:ValidationSummary>
				                </td>
				                <td width="20">&nbsp;</td>
			                </tr>
			                <tr>
				                <td height="15" align="center">&nbsp;</td>
				                <td height="15">&nbsp;</td>
				                <td height="15">&nbsp;</td>
			                </tr>
		                </table>
	                <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                </div>  
                <asp:Label ID="lblHeading" CssClass="HeadingRed" runat="server" Text="Create Multiple Users"></asp:Label>
                <asp:RequiredFieldValidator id="rqNoUsers" runat="server" ErrorMessage="Please enter a value greater than 0" ForeColor="#CBC9CA"
		        ControlToValidate="txtbxNoUsers">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rgxNoUsers" ControlToValidate="txtbxNoUsers" runat="server" ForeColor="#CBC9CA" ValidationExpression="^([1-9]|10)$"
                    ErrorMessage="Please enter a value between 1 to 10">*</asp:RegularExpressionValidator>    	  
                    	  
                
                <asp:Panel ID="pnlMultipleUser" runat="server" Visible="true">
                <div style="float:left; width:100%; height:24px; margin-top:15px;">	                    
	                 <asp:TextBox ID="txtbxNoUsers" runat="server" Width="150" CssClass="formField"  MaxLength="2"></asp:TextBox>
	                 <cc1:TextBoxWatermarkExtender TargetControlID="txtbxNoUsers" ID="TextBoxWatermarkExtender1" WatermarkText="Enter no. of Users"  runat="server"/>
                </div><br />
                <div style="float:left; width:100%; margin-top:20px;" >
                <TABLE runat="server" id="TABLE1" visible="true" cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD width="110" class="txtBtnImage"><a id="lnkEnterUser" onclick="noUsers()" runat="server" TabIndex="2" class="txtListing" style="cursor:pointer;"  > <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Submit&nbsp;</a>
                                <%--<asp:LinkButton ID="lnkbtnEnter" runat="server" Width="0" Height="0">&nbsp;</asp:LinkButton>--%>
                                <asp:button id="lnkbtnEnter" runat="server" Height="0px" Width="0px"  Font-Size="0px" style="display:none;" />
                                </TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                            </TABLE></div>
                </asp:Panel>
                <asp:Panel ID="pnlInpUsers" runat="server" Visible="false" >
                    <div style="margin-top:10px; margin-bottom:15px;">
                       <strong><asp:Label ID="lblTopHeading" runat="server" Text="Insert user details to be added" CssClass="HeadingRed"></asp:Label></strong>
                    </div>
                    <table width="1200" style="background-color:#D7D7D7; color:#ffffff;">
                        <tr>
                            <td width="70">&nbsp;</td>
                            <td width="100"><b>First Name</b></td>
                            <td width="100"><b>Last Name</b></td>
                            <td width="120"><b>Email Address</b></td>
                            <td width="100"><b>Phone</b></td>
                            <td width="140"><b>Location</b></td>
                            <td width="130"><b>User Type</b></td>
                            <td width="200"><b>Security Question</b></td>
                            <td width="100"><b>Security Answer</b></td>
                            <td width="120"><b>Enable Login</b></td>
                        </tr>
                    </table>
                    <asp:Repeater ID="rptUsers" runat="server" >
                    <ItemTemplate>
                        <table width="1200" class="contentListing">
                        <tr>
                            <td width="70" ><b>User <%#Container.dataitem("ContactID")%>:</b> <input type="hidden" id="hdnContactID" runat="server" value=<%#Container.dataitem("ContactID")%> /></td>
                            <td width="100" ><asp:TextBox ID="txtbxFName" runat="server" Width="90" CssClass="formField" MaxLength="30"></asp:TextBox></td>
                            <td width="100" ><asp:TextBox ID="txtbxLName" runat="server" Width="90" CssClass="formField" MaxLength="30"></asp:TextBox></td>
                            <td width="120"><asp:TextBox ID="txtbxEmailAddr" runat="server" Width="110" CssClass="formField" MaxLength="50"></asp:TextBox></td>
                            <td width="100"><asp:TextBox ID="txtPhone" runat="server" Width="90" CssClass="formField" MaxLength="20"></asp:TextBox></td>
                            <td width="140"><asp:DropDownList ID="drpdwnLocation" runat="server" Width="130" CssClass="formField">
                                </asp:DropDownList></td>
                            <td width="130"><asp:DropDownList ID="drpdwnUserType" runat="server" Width="120" CssClass="formField" onkeydown='javascript:disableLogin(this.id);' onkeyup='javascript:disableLogin(this.id);' onchange='javascript:disableLogin(this.id);' >
                                </asp:DropDownList></td>
                                <td width="200"><asp:DropDownList ID="drpdwnSecQue" runat="server" Width="200" CssClass="formField">
                                </asp:DropDownList></td>
                                <td width="100"><asp:TextBox ID="txtbxSecAns" runat="server" Width="100" CssClass="formField" MaxLength="50"></asp:TextBox></td>
                            <td width="120"><asp:CheckBox ID="chkbxEnableLogin" runat="server" CssClass="formTxt" Text="Enable Login" /></td>
                        </tr>
                    </table>
                    </ItemTemplate>
                    </asp:Repeater>
                    <div style="margin-top:20px;">
                    <table>
                         <tr><td>
                              <TABLE runat="server" id="tblconfSubmit" visible="true" cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD width="110" class="txtBtnImage"><a id="btnSubmit" runat="server" TabIndex="2" class="txtListing" > <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Submit&nbsp;</a>
                                <asp:LinkButton ID="lnkhdnbtnConf" runat="server" Height="0" Width="0">&nbsp;</asp:LinkButton>
                                </TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                            </TABLE>
                            <TABLE runat="server" id="tblconfConfirm" visible="false" cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD width="110" class="txtBtnImage"><a id="AnchorConfirm" onclick="this.disabled=1;document.getElementById('ctl00_ContentPlaceHolder1_btnConfirm').click()" style="cursor:pointer;"   runat="server" TabIndex="2" class="txtListing"> <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Confirm&nbsp;</a>                                
                                <asp:button id="btnConfirm" runat="server" Height="0px" Width="0px"  Font-Size="0px" style="display:none;cursor:pointer;" />
                                </TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                            </TABLE>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                             <TABLE runat="server" id="tblUserCancel" visible="True"  cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD width="110" class="txtBtnImage"><a id="btnUersCancel" runat="server" TabIndex="3" class="txtListing"> <img src="Images/Icons/Cancel.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Cancel&nbsp;</a></TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                            </TABLE>
                            <TABLE runat="server" id="tblConfCancel" visible="false"  cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD width="110" class="txtBtnImage"><a id="btnConfCancel" runat="server" TabIndex="3" class="txtListing"> <img src="Images/Icons/Cancel.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Cancel&nbsp;</a></TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                            </TABLE>
                            </td></tr>
                    </table>
                    </div>
                </asp:Panel>
               
            </ContentTemplate>
            </asp:UpdatePanel>
             <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
           
            </td><td width="10">&nbsp;</td></tr>
            </table>
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>

 </asp:Content>
