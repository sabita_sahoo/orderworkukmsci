
Partial Public Class ServicePartnerFeedback
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then
            Dim startDate As String
            startDate = "01" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
            Dim endDate As String
            If ddMonth.SelectedValue = 2 Then
                endDate = "28" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
            ElseIf ddMonth.SelectedValue = 1 Or ddMonth.SelectedValue = 3 Or ddMonth.SelectedValue = 5 Or ddMonth.SelectedValue = 7 Or ddMonth.SelectedValue = 8 Or ddMonth.SelectedValue = 10 Or ddMonth.SelectedValue = 12 Then
                endDate = "31" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
            ElseIf ddMonth.SelectedValue = 4 Or ddMonth.SelectedValue = 6 Or ddMonth.SelectedValue = 9 Or ddMonth.SelectedValue = 11 Then
                endDate = "30" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
            End If
            ddMonth.SelectedValue = Month(Date.Now)
            ddYear.SelectedValue = Year(Date.Now)
            CommonFunctions.PopulateFeedbackType(Page, ddlType, False)
            ddlType.Items.Insert(0, New ListItem("All", 0))
            CommonFunctions.PopulateFeedbackType(Page, ddComplaintType, True)
            ddComplaintType.Items.Insert(0, New ListItem("Select Type", 0))
            gvFeedback.PageSize = ApplicationSettings.GridViewPageSize
            ViewState!SortExpression = "DateCreated DESC"
            gvFeedback.Sort(ViewState!SortExpression, SortDirection.Ascending)
            PopulateGrid()
        End If
        lblMsg.Text = ""
    End Sub
    Public Sub PopulateGrid()

        gvFeedback.DataBind()

    End Sub
    Private Sub gvFeedback_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvFeedback.PreRender
        Me.gvFeedback.Controls(0).Controls(Me.gvFeedback.Controls(0).Controls.Count - 1).Visible = True
        If ViewState("rowCount") < 1 Then
            Return
        End If
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

            End If
        End If
    End Sub
    Protected Sub gvFeedback_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvFeedback.RowDataBound
        MakeGridViewHeaderClickable(gvFeedback, e.Row)
    End Sub
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvFeedback.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvFeedback, e.Row, Me)
        End If
    End Sub
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)
        'Dim btnExport As HtmlAnchor = CType(gvPagerRow.FindControl("btnExport"), HtmlAnchor)

        'btnExport.HRef = prepareExportToExcelLink()

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvFeedback.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub
    Public Sub ProcessedComplaint(ByVal sender As Object, ByVal e As System.EventArgs)

        lblMsg.Text = ""
        Dim ComplaintIds As String = CommonFunctions.getSelectedIdsOfGrid(gvFeedback, "Check", "hdnComplaintId") 'getSelectedComplaintIds()
        If ComplaintIds = "" Then
            lblMsg.Text = "Please select Feedback to mark as Processed"
            Return
        End If
        ddlType.SelectedValue = 0
        mdlComplaintType.Show()
    End Sub
    Public Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click
        Dim ds As DataSet
        Dim ComplaintIds As String = CommonFunctions.getSelectedIdsOfGrid(gvFeedback, "Check", "hdnComplaintId") 'getSelectedComplaintIds()
        Dim ComplaintType As Integer = CInt(ddComplaintType.SelectedValue)
        'ds = ws.WSContact.UpdateDixonsComplaints(ComplaintIds, 1, CInt(ddComplaintType.SelectedValue))
        ds = ws.WSContact.UpdateDixonsComplaints(ComplaintIds, 1, ComplaintType, Session("UserId"))
        If ds.Tables.Count <> 0 Then
            If ds.Tables("tblStatus").Rows(0).Item("Success") <> 0 Then
                ' For populating the Textbox with the original contents of the mail
                lblMsg.Text = "Feedback Marked Processed Successfully."
                PopulateGrid()
            End If
        End If
    End Sub

    'Public Function SelectFromDB1(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
    '    Dim ds As DataSet
    '    Dim rowCount As Integer
    '    ds = ws.WSContact.GetDixonsComplaintsListing(CInt(ddlStatus.SelectedValue), sortExpression, startRowIndex, maximumRows, rowCount, txtComplaintRefNo.Text.Trim, CInt(ddlType.SelectedValue),'', '', '', '')
    '    ViewState("rowCount") = rowCount
    '    If (ds.Tables(1).Rows.Count > 0) Then
    '        If (ds.Tables(1).Rows(0).Item("TotalRecs") > 0) Then
    '            tdExport.Visible = True
    '            btnExport.HRef = prepareExportToExcelLink()
    '        Else
    '            tdExport.Visible = False
    '        End If
    '    Else
    '        tdExport.Visible = False
    '    End If
    '    Return ds
    'End Function
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        Dim rowCount As Integer
        Dim IsProcessed As Integer
        Dim feedback As String
        feedback = "Feedback"
        IsProcessed = 0
        Dim startDate As String
        startDate = "01" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
        Dim endDate As String
        If ddMonth.SelectedValue = 2 Then
            endDate = "28" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
        ElseIf ddMonth.SelectedValue = 1 Or ddMonth.SelectedValue = 3 Or ddMonth.SelectedValue = 5 Or ddMonth.SelectedValue = 7 Or ddMonth.SelectedValue = 8 Or ddMonth.SelectedValue = 10 Or ddMonth.SelectedValue = 12 Then
            endDate = "31" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
        ElseIf ddMonth.SelectedValue = 4 Or ddMonth.SelectedValue = 6 Or ddMonth.SelectedValue = 9 Or ddMonth.SelectedValue = 11 Then
            endDate = "30" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
        End If
        ds = ws.WSContact.GetDixonsComplaintsListing(CInt(ddlStatus.SelectedValue), sortExpression, startRowIndex, maximumRows, 25, txtComplaintRefNo.Text.Trim, CInt(ddlType.SelectedValue), startDate, endDate, "", "", "Feedback")
        HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0)("TotalRecs")
        ViewState("rowCount") = ds.Tables(1).Rows(0)("TotalRecs")
        If (ds.Tables(1).Rows.Count > 0) Then
            If (ds.Tables(1).Rows(0).Item("TotalRecs") > 0) Then
                tdExport.Visible = True
                btnExport.HRef = prepareExportToExcelLink()
            Else
                tdExport.Visible = False
            End If
        Else
            tdExport.Visible = False
        End If
        Return ds
    End Function
    Public Function prepareExportToExcelLink() As String
        Dim linkParams As String = ""
        Dim startDate As String
        startDate = "01" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
        Dim endDate As String
        If ddMonth.SelectedValue = 2 Then
            endDate = "28" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
        ElseIf ddMonth.SelectedValue = 1 Or ddMonth.SelectedValue = 3 Or ddMonth.SelectedValue = 5 Or ddMonth.SelectedValue = 7 Or ddMonth.SelectedValue = 8 Or ddMonth.SelectedValue = 10 Or ddMonth.SelectedValue = 12 Then
            endDate = "31" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
        ElseIf ddMonth.SelectedValue = 4 Or ddMonth.SelectedValue = 6 Or ddMonth.SelectedValue = 9 Or ddMonth.SelectedValue = 11 Then
            endDate = "30" & "/" & ddMonth.SelectedValue.ToString() & "/" & ddYear.SelectedValue.ToString()
        End If
        linkParams &= "IsProcessed=" & ddlStatus.SelectedValue
        linkParams &= "&sortExpression=DateCreated DESC"
        linkParams &= "&startRowIndex=0"
        linkParams &= "&ComplaintRefNo=" & txtComplaintRefNo.Text.Trim
        linkParams &= "&ComplaintType=" & ddlType.SelectedValue
        linkParams &= "&DateCreatedFrom=" & startDate
        linkParams &= "&DateCreatedTo=" & endDate
        'linkParams &= "&DateProcessedFrom=" & txtDateProcessedFrom.Text.Trim
        'linkParams &= "&DateProcessedTo=" & txtDateProcessedTo.Text.Trim
        linkParams &= "&page=" & "ServicePartnerFeedback"
        Return "ExportToExcel.aspx?" & linkParams
    End Function
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub
    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    Private Sub lnkBtnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnView.Click
        PopulateGrid()
    End Sub
End Class