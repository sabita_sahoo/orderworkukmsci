﻿Public Class ServicePreview
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then
            If Not IsNothing(Request("ServiceId")) Then
                iFrameServicePreview.Attributes.Add("src", OrderWorkLibrary.ApplicationSettings.NewPortalURL.ToString & "/workorder/" & Session("UserID").ToString & "/customiseservice/" & Request("ServiceId").ToString & "/1")
            End If
        End If
    End Sub

End Class