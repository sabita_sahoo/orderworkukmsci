Imports System.Web.Mail
Imports System.IO
Imports System.Net.Mail
Imports System.Net

Partial Public Class ResetPassword
    Inherits System.Web.UI.Page

    Shared ws As New WSObjs
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        UserAction.userAutoLoginAccept(Page)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
    End Sub

    Public Sub btnResetPassword_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        mdlPONumber.Show()
        lblMsg.Text = ""
    End Sub

    Private Sub ancSubmit_ServerClick(sender As Object, e As System.EventArgs) Handles ancSubmit.ServerClick
        Try

            CommonFunctions.createLog("OWAdmin - Reset password ancSubmit_ServerClick")
            If txtEmailtoReset.Text.Trim = "" Then
                lblMsg.Text = "Please enter an email address."
                Exit Sub
            End If
            'poonam modified on 14/6/2016 - Task - OA-276 : OA - Change SP to record Name of password changer/requester
            Dim LoggedInUserID As Integer = 0
            If Not IsNothing(Session("UserId")) Then
                LoggedInUserID = Session("UserId")
            End If
            Dim password As String = ""
            password = txtPasswordReset.Text.Trim

            Dim RandomPassword As String
            'poonam modified on 4/12/2015 - Task - OA-131 :OA/OM - Strengthen automatically and manually generated passwords
            RandomPassword = CommonFunctions.GenerateOTP()

            If (txtEmailtoReset.Text.Trim <> ApplicationSettings.OWCommonUserAccountEmail) Then

                Dim success As Integer

                If password.Length = 0 Then
                    'poonam modified on 14/6/2016 - Task - OA-276 : OA - Change SP to record Name of password changer/requester
                    success = ws.WSSecurity.ChangeLoginInfo(ViewState("ContactID"), txtEmailtoReset.Text.Trim, "", Encryption.EncryptToMD5Hash(RandomPassword), 0, 0, LoggedInUserID)

                    If success <> -1 Then
                        If success <> 0 Then
                            If success = 1173 Then
                                lblMsg.Text = "You cannot reset an OrderWork user's password."
                            Else
                                Emails.SendMailForgotPassword(txtEmailtoReset.Text.Trim, "Orderwork User", Trim(RandomPassword), success)
                                lblMsg.Text = "The password for the user has been reset and a mail has been sent to the user informing them of the new password. "
                            End If
                        Else
                            lblMsg.Text = "This email address does not exist in our records. Please try again"
                        End If
                    Else
                        lblMsg.Text = "The password could not be reset. Please try again."
                    End If

                Else
                    'poonam modified on 14/6/2016 - Task - OA-276 : OA - Change SP to record Name of password changer/requester
                    success = ws.WSSecurity.ChangeLoginInfo(ViewState("ContactID"), txtEmailtoReset.Text.Trim, "OLDCheck", Encryption.EncryptToMD5Hash(password), 0, 0, LoggedInUserID)

                    If success <> -1 Then
                        If success <> 0 Then
                            If success = 11111 Then
                                lblMsg.Text = "You cannot set password for this user."
                                mdlPONumber.Hide()
                                txtPasswordReset.Text = ""
                                txtEmailtoReset.Text = ""
                                Exit Sub
                            End If

                            If success = 1173 Then
                                lblMsg.Text = "You cannot reset an OrderWork user's password."
                                mdlPONumber.Hide()
                                txtPasswordReset.Text = ""
                                txtEmailtoReset.Text = ""
                                Exit Sub
                            Else
                                'Dim client As New Net.Mail.SmtpClient("smtp.emailsrvr.com")
                                'client.Credentials = New Net.NetworkCredential("hdesai@orderwork.co.uk", "")
                                'client.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.SpecifiedPickupDirectory
                                'client.PickupDirectoryLocation = "C:\TestEmails"

                                'Dim client As New Net.Mail.SmtpClient("localhost")
                                'client.Send(Emails.CreateContactUsMail(txtEmailtoReset.Text.Trim, Trim(password)))

                                'Dim client As New System.Net.Mail.SmtpClient("smtp.office365.com", 587)
                                'client.DeliveryMethod = SmtpDeliveryMethod.Network
                                'client.UseDefaultCredentials = False
                                'client.Credentials = New NetworkCredential(ApplicationSettings.SMTPServerUserId.ToString, ApplicationSettings.SMTPServerPassword.ToString)
                                'client.TargetName = "STARTTLS/smtp.office365.com"
                                'client.EnableSsl = True
                                'client.Send(Emails.CreateContactUsMail(txtEmailtoReset.Text.Trim, Trim(password)))


                                'Dim client As New System.Net.Mail.SmtpClient("smtp.1and1.com", 587)
                                'client.DeliveryMethod = SmtpDeliveryMethod.Network
                                'client.UseDefaultCredentials = False
                                ''client.Credentials = New NetworkCredential("owrearch@iniquus.com", "iniquus@1234")
                                'client.Credentials = New NetworkCredential("owrearch@iniquus.com", "iniquus@1234")
                                ''client.TargetName = "starttls/smtp.office365.com"
                                'client.EnableSsl = True
                                'client.Send(Emails.CreateContactUsMail(txtEmailtoReset.Text.Trim, Trim(password)))
                                'Emails.CreateContactUsMail(txtEmailtoReset.Text.Trim, Trim(password))

                                Dim client As New SmtpClient("smtp.office365.com", 587)
                                client.DeliveryMethod = SmtpDeliveryMethod.Network
                                client.UseDefaultCredentials = False
                                client.Credentials = New NetworkCredential(ApplicationSettings.SMTPServerUserId.ToString, ApplicationSettings.SMTPServerPassword.ToString)
                                client.TargetName = "STARTTLS/smtp.office365.com"
                                client.EnableSsl = True
                                client.Send(Emails.CreateContactUsMail(txtEmailtoReset.Text.Trim, Trim(password)))

                                lblMsg.Text = "The password for the user has been reset and a mail has been sent to the user informing them of the new password. "
                                mdlPONumber.Hide()
                            End If
                        Else
                            lblMsg.Text = "This email address does not exist in our records. Please try again"
                            mdlPONumber.Hide()
                            txtPasswordReset.Text = ""
                            txtEmailtoReset.Text = ""
                            Exit Sub
                        End If
                    Else
                        lblMsg.Text = "The password could not be reset. Please try again."
                        mdlPONumber.Hide()
                        txtPasswordReset.Text = ""
                        txtEmailtoReset.Text = ""
                        Exit Sub
                    End If

                End If
            Else
                lblMsg.Text = "You cannot reset an OrderWork Common user's password."
                mdlPONumber.Hide()
                txtPasswordReset.Text = ""
                txtEmailtoReset.Text = ""
                Exit Sub
            End If

            mdlPONumber.Hide()
            txtPasswordReset.Text = ""
            txtEmailtoReset.Text = ""
        Catch ex As Exception
            CommonFunctions.createLog("OWAdmin - Reset password Mail sending Issue1 : " & ex.ToString)
        End Try
    End Sub

    Private Sub ancCancel_ServerClick(sender As Object, e As System.EventArgs) Handles ancCancel.ServerClick
        mdlPONumber.Hide()
        txtPasswordReset.Text = ""
        txtEmailtoReset.Text = ""
    End Sub
End Class