﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PhotoUpload.aspx.vb" Inherits="Admin.PhotoUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
   <title>Image Upload</title>
   <%-- <link rel="stylesheet" href="../SDCLibrary/Styles/Admin.css" type="text/css" />      --%>   
    <%--<script language="javascript" type="text/javascript" src="<%=SDCLibrary.ApplicationSettings.Webpath & "SDCLibrary/Scripts/Admin.js"%>" ></script>--%>
    <script>
        function callPrevPage() {
            var AttachmentDisplayPath
            alert("Updated successfully!");
            //alert(window.opener.location.href);
            window.opener.location.href = window.opener.location.href;
            if (window.opener.progressWindow) {
                window.opener.progressWindow.close()
            }
            window.close();
//            AttachmentDisplayPath = document.getElementById("hdnAttachmentDisplayPath").value + "AttachmentsUK/ContactPhoto/";
//            window.opener.document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_imgProfile").src = AttachmentDisplayPath.toString() + document.getElementById("hdnImageName").value + "?v=" + Date.parse(new Date().toString());
//            window.close();
        }
    </script>
    <style>
    .tblImgUpload
{
    margin-top:25px;
    margin-left:25px;
}
.txtClose
{
	color: #82B0EE;
	text-decoration: none;
	font-family: Arial;
	font-size: 12px;
}
.divCloseWindow
{
	float: right;		
}
.fileupload {
font-family:Arial;
color:#7B797B;
font-size:12px;
text-decoration:none;
width:350px;
}
.cursorPointer
{
 cursor:pointer;	
}
.Title
{
	color:#FF6600;
	font-family:'Arial';
	font-size:20px;
	font-weight:bold;
	text-decoration:none;	
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
       <asp:Panel ID="pnlUpload" runat="server">
        <table cellpadding="0" cellspacing="0"  border="0" class="tblImgUpload">
           <tr>	                    
	        <td colspan=2 class="Title" >	        
	        Add / Edit Image
	        <div class="divCloseWindow"><a class="txtClose" id="lnkCancel" onclick="javascript:window.close();" href="#">Close</a></div>   
	        </td>	                    
	      </tr>	
	       <tr>	                    
	        <td >&nbsp;</td>	                    
	      </tr>	
	      <tr height=30>	                    
	        <td colspan=2>
	             <asp:FileUpload ID="FileUpload1"  runat="server" CssClass="fileupload" size="51px" />
	        </td>	                    
	      </tr>	        
	      <tr>	                    
	        <td >&nbsp;</td>	                    
	      </tr>	
	      <tr height=30>	                    
	        <td colspan=2>
	             <asp:Button ID="btnUpload"  runat="server" Text="Upload" ToolTip="Upload"  CssClass="cursorPointer" ></asp:Button>
	        </td>	                    
	      </tr>	  
	    </table>
    </asp:Panel>
     <input ID="hdnImageName" runat="server" type="hidden" />
     <input ID="hdnAttachmentDisplayPath" runat="server" type="hidden" />
    </form>
</body>
</html>

