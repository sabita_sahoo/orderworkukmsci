<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FileUpload.aspx.vb" Inherits="Admin.FileUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Sign Off Sheet Upload</title>
    <link href="Styles/OrderWork.css" rel="stylesheet" type="text/css">
<link href="Styles/OrderWork.css" rel="stylesheet" type="text/css">
<link href="Styles/Menu.css" rel="stylesheet" type="text/css">
<style>
.clsDim{
width:30px;
height:15px;
padding:10px;
}
</style> 
<script language="JavaScript" type="text/JavaScript" src="JS/Scripts.js"></script>
</head>
<body style="background-color:#F4F5EF;" >
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
     <%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>
	<uc2:UCFileUpload id="UCFileUpload1" runat="server" Control="UCFileUpload1" 
	Type="WorkOrder" AttachmentForSource="WOComplete"  UploadCount="1" 
	ShowExistAttach="False" ShowNewAttach="True" ShowUpload="True" MaxFiles="3"></uc2:UCFileUpload> <br />
	<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF !important;text-align:center;width:60px;float:left;padding:4px;">
        <asp:linkbutton CausesValidation="false" id="btnSave" runat="server" style="color:#FFFFFF;text-decoration:none;font-size:11px;font-family:Geneva, Arial, Helvetica, sans-serif;">Save</asp:linkbutton>
    </div>
    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;float:left;margin-left:20px;padding:4px;">
		<a href=""  style="color:#FFFFFF;text-decoration:none;font-size:11px;font-family:Geneva, Arial, Helvetica, sans-serif;" onclick="window.close();">Cancel</a>
    </div>	        
    </form>
</body>
</html>
