
Partial Public Class RevenueAndForecastReport
	Inherits System.Web.UI.Page
	Protected WithEvents UCMenuMainAdmin As UCMenuMainAdmin
	Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
	Protected WithEvents UCSearchContact1 As UCSearchContact
	Protected WithEvents lnkBackToListing As System.Web.UI.HtmlControls.HtmlAnchor
	Protected WithEvents tdBackToListing As System.Web.UI.HtmlControls.HtmlTableCell
	Protected WithEvents ddMonth As System.Web.UI.HtmlControls.HtmlSelect
	Protected WithEvents ddYear As System.Web.UI.HtmlControls.HtmlSelect
	Protected WithEvents lnkExportReportExcel As System.Web.UI.WebControls.LinkButton
	Protected WithEvents lnkExportReportPdf As System.Web.UI.WebControls.LinkButton

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		Select Case ApplicationSettings.Country
			Case ApplicationSettings.CountryDE
				ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
			Case ApplicationSettings.CountryUK
				ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
		End Select

        'If Not IsPostBack Then
        '    ddMonth.SelectedIndex = ddMonth.Items.IndexOf(ddMonth.Items.FindByValue(CType(Now.Month, String)))
        '    ddYear.SelectedIndex = ddYear.Items.IndexOf(ddYear.Items.FindByValue(CType(Now.Year, String)))
        'End If

	End Sub

	Private Sub lnkBtnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnGenerateReport.Click
		generateReport()
		ReportViewerOW.ServerReport.Refresh()
	End Sub

	Private Sub lnkExportReportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportReportExcel.Click

		Dim warnings As Microsoft.Reporting.WebForms.Warning()
		Dim streamids As String()
		Dim mimeType As String
		Dim encoding As String
		Dim fileType As String = "EXCEL"
		Dim extension As String = "xls"
		Dim fileName As String = "OWReport"

		generateReport()

		Dim bytes As Byte() = ReportViewerOW.ServerReport.Render(fileType, Nothing, mimeType, encoding, extension, streamids, warnings)
		Response.Buffer = True
		Response.Clear()
		Response.ContentType = mimeType
		Response.AddHeader("content-disposition", "attachment; filename=NewSalesRevenueReports." + extension)
		Response.BinaryWrite(bytes)
		Response.Flush()
		Response.End()
	End Sub

	Private Sub lnkExportReportPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportReportPdf.Click

		Dim warnings As Microsoft.Reporting.WebForms.Warning()
		Dim streamids As String()
		Dim mimeType As String
		Dim encoding As String
		Dim fileType As String = "PDF"
		Dim extension As String = "pdf"
		Dim fileName As String = "OWReport"

		generateReport()

		Dim bytes As Byte() = ReportViewerOW.ServerReport.Render(fileType, Nothing, mimeType, encoding, extension, streamids, warnings)
		Response.Buffer = True
		Response.Clear()
		Response.ContentType = mimeType
		Response.AddHeader("content-disposition", "attachment; filename=RevenueAndForecastReport" + Date.Now + "." + extension)
		Response.BinaryWrite(bytes)
		Response.Flush()
		Response.End()
	End Sub

	Private Sub generateReport()
		ReportViewerOW.ShowParameterPrompts = False
		ReportViewerOW.ShowToolBar = True
		Dim reportSErverPath As Uri = New Uri(ApplicationSettings.ReportServerPath)
		ReportViewerOW.ServerReport.ReportServerUrl = reportSErverPath
		ReportViewerOW.ServerReport.ReportServerCredentials = New CustomReportCredentials.CustomReportCredentials(ApplicationSettings.ReportServerUserName, ApplicationSettings.ReportServerPassword, ApplicationSettings.ReportServerDomain)


		Dim objParameter As New List(Of Microsoft.Reporting.WebForms.ReportParameter)

		ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/RevenueAndForecastReport"

		objParameter.Add(New Microsoft.Reporting.WebForms.ReportParameter("paramStartDate", ddYearFrom.Value & "-" & ddMonthFrom.Value & "-01"))
		objParameter.Add(New Microsoft.Reporting.WebForms.ReportParameter("paramAdditionalMonths", ddMonthAdditional.Value.ToString))
		objParameter.Add(New Microsoft.Reporting.WebForms.ReportParameter("paramCompareWithLastYear", CStr(0)))
		objParameter.Add(New Microsoft.Reporting.WebForms.ReportParameter("paramIncludeSubmittedSend", CStr(1)))
		objParameter.Add(New Microsoft.Reporting.WebForms.ReportParameter("paramIncludeForecast", CStr(1)))
		objParameter.Add(New Microsoft.Reporting.WebForms.ReportParameter("paramBusinessArea", ddReportType.Value.ToString))
        objParameter.Add(New Microsoft.Reporting.WebForms.ReportParameter("paramForecastPercent", CDbl(txtForecastPercent.Text) / 100))
        objParameter.Add(New Microsoft.Reporting.WebForms.ReportParameter("paramConditionalPercent", CDbl(txtConditionalPercent.Text) / 100))

		ReportViewerOW.ServerReport.SetParameters(objParameter)

	End Sub


End Class
