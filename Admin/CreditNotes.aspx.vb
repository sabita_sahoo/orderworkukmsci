
Partial Public Class CreditNotes
    Inherits System.Web.UI.Page
    Protected WithEvents UCDateRange1 As UCDateRange
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents ImgBtnPay As System.Web.UI.WebControls.ImageButton
    Protected WithEvents PnlListing, PnlConfirm, PnlSuccess As System.Web.UI.WebControls.Panel
    Protected WithEvents hdnButtonCN As System.Web.UI.WebControls.Button
    Protected WithEvents txtCNDate As System.Web.UI.WebControls.TextBox
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)

            Dim currDate As DateTime = System.DateTime.Today
            UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            ViewState("fromDate") = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
            ViewState("toDate") = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
            ViewState!SortExpression = "InvoiceDate"
            gvInvoices.Sort(ViewState!SortExpression, SortDirection.Descending)
            'btnExport.HRef = "ExportToExcel.aspx?page=UpSellSalesInvoicePaid&bizDivId=" & Session("BizDivId") & "&status=Paid" & "&fromDate=" & ViewState("fromDate") & "&toDate=" & ViewState("toDate") & "&sortExpression=" & gvInvoices.SortExpression
            UCSearchContact1.BizDivID = Session("BizDivId")

            UCSearchContact1.populateContact()
            If Not IsNothing(Request.QueryString("Mode")) Then
                If Request.QueryString("Mode") = "US" Then
                    lblHeading.Text = "UpSell Credit Notes"
                    UCSearchContact1.Filter = "UpSellCreditNoteListing"
                Else
                    lblHeading.Text = "Credit Notes"
                    UCSearchContact1.Filter = "CreditNoteListing"
                End If
            Else
                lblHeading.Text = "Credit Notes"
                UCSearchContact1.Filter = "CreditNoteListing"
            End If
        End If
    End Sub
    
    Public Sub PopulateGrid()
        gvInvoices.DataBind()

        'btnExport.HRef = "ExportToExcel.aspx?page=UpSellSalesInvoicePaid&bizDivId=" & Session("BizDivId") & "&status=Paid" & "&fromDate=" & ViewState("fromDate") & "&toDate=" & ViewState("toDate") & "&sortExpression=" & gvInvoices.SortExpression

    End Sub
    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then
            ViewState("fromDate") = UCDateRange1.txtFromDate.Text
            ViewState("toDate") = UCDateRange1.txtToDate.Text
            If UCSearchContact1.ddlContact.SelectedValue = "" Then
                ViewState("CompanyID") = -1
            ElseIf UCSearchContact1.ddlContact.SelectedValue = "ALL" Then
                ViewState("CompanyID") = 0
            Else
                ViewState("CompanyID") = UCSearchContact1.ddlContact.SelectedValue
            End If
            If ViewState("CompanyID") <> -1 Then
                lblMsg.Text = ""
                PopulateGrid()
            Else
                lblMsg.Text = "* Please select a contact to continue"
            End If

        End If
    End Sub


    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvInvoices.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvInvoices.DataBind()
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                       ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        Dim status As String = "Paid"
        Dim bizDivId As Integer = Session("BizDivId")
        If Not IsNothing(ViewState("CompanyID")) Then
            If ViewState("CompanyID") <> -1 Then
                If Not IsNothing(Request.QueryString("Mode")) Then
                    If Request.QueryString("Mode") = "CN" Then
                        ds = ws.WSFinance.MS_GetCreditNotesAdviceListing(ViewState("CompanyID"), bizDivId, "CN", ViewState("fromDate"), ViewState("toDate"), sortExpression, startRowIndex, maximumRows)
                    Else
                        ds = ws.WSFinance.MS_GetCreditNotesAdviceListing(ViewState("CompanyID"), bizDivId, "US", ViewState("fromDate"), ViewState("toDate"), sortExpression, startRowIndex, maximumRows)
                    End If
                    ViewState("rowCount") = ds.Tables(1).Rows(0)(0)
                End If
            Else
                ViewState("rowCount") = 0
            End If
        Else
            ViewState("rowCount") = 0
        End If
        Return ds
    End Function

    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvInvoices.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvInvoices, e.Row, Me)
        Else
            If Not IsNothing(Request.QueryString("Mode")) Then
                If Request.QueryString("Mode") = "US" Then
                    gvInvoices.Columns(5).HeaderText = "Upsell Invoice Number"
                    gvInvoices.Columns(2).Visible = True
                    gvInvoices.Columns(1).HeaderText = "Invoice Date"
                Else
                    gvInvoices.Columns(2).Visible = False
                End If
            Else
                gvInvoices.Columns(2).Visible = False
            End If
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            If Not IsNothing(Request.QueryString("Mode")) Then
                If Request.QueryString("Mode") = "US" Then
                    Dim imgView As Image = CType(e.Row.FindControl("imgView"), Image)
                    imgView.AlternateText = "View Upsell Credit Note"
                    Dim ImgBtnPay As ImageButton = CType(e.Row.FindControl("ImgBtnPay"), ImageButton)
                    ImgBtnPay.AlternateText = "Pay Upsell Credit Note"
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex



    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvInvoices.RowDataBound
        MakeGridViewHeaderClickable(gvInvoices, e.Row)
    End Sub

    Public Sub ImgBtnPay_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim arrList As ArrayList = New ArrayList
        Dim str As String = CType(sender, ImageButton).CommandArgument.ToString
        arrList.AddRange(str.Split(","))
        Session("arrList(1)") = arrList(1)
        Session("arrList(0)") = arrList(0)
        Session("arrList(2)") = arrList(2)
        'ds = ws.WSFinance.MS_PayCreditNote(Session("BizDivId"), arrList(1), arrList(0))
        'gvInvoices.DataBind()
        mdlCNDatePaid.Show()
    End Sub

    ''' <summary>
    ''' Called when Invoice type is Credit Note and status is unpaid
    ''' Copied code from CreditNotes
    ''' Create by Pankaj on 20 Nov''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub hdnButtonCN_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButtonCN.Click
        'Dim arrList As ArrayList = New ArrayList
        'Dim str As String = CType(sender, ImageButton).CommandArgument.ToString
        'arrList.AddRange(str.Split(","))
        ''Invoice No
        'Dim InvoiceNo As String
        ''Company Id
        'Dim CompanyId As Integer
        'CompanyId = arrList(1)
        'InvoiceNo = arrList(0)
        Dim ds As DataSet = New DataSet
        ds = ws.WSFinance.MS_PayCreditNote(Session("BizDivId"), Session("arrList(1)"), Session("arrList(0)"), Strings.FormatDateTime(txtCNDate.Text), Session("UserId"))
        'Dim ds As New DataSet
        'ds = ws.WSFinance.MSAccounts_GenerateCashReceipt(Session("BizDivId"), Session("arrList(0)"), Session("arrList(1)"), Strings.FormatDateTime(txtDate.Text), 0)
        Session.Remove("arrList(0)")
        Session.Remove("arrList(1)")
        PopulateGrid()
    End Sub

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
End Class