<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Web Content" CodeBehind="SiteContent.aspx.vb" Inherits="Admin.SiteContent" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>

<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>

			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
    
        <!-- start of code -->
        
                
		    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>		
					 <table width="100%"  border="0" cellpadding="0" cellspacing="0">
             
                                <tr>
                                  <td class="paddingL23" height="5" align="left" valign="top"><asp:Label  CssClass="HeadingRed" runat="server" ID="lblMessage"></asp:Label>

				                  </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top">
				                    <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
                                      <tr valign="top">
                                        <td width="18" height="14" align="left" valign="top" class="formTxt">&nbsp;</td>
                                        <td class="formTxt" align="left" width="170">&nbsp;</td>
                                        <td class="formTxt" align="left" width="120">&nbsp;</td>
                                        <td class="formTxt" align="left" width="135">&nbsp;</td>
                                        <td class="formTxt" align="left" width="150">&nbsp;</td>
                                        <td class="formTxt" align="left"  width="73" >&nbsp;</td>
                                        <td width="346" align="left" class="formTxt" >&nbsp;</td>
                                      </tr>
                                      <tr valign="top">
                                        <td width="18" height="14" align="left" valign="top" class="formTxt">&nbsp;</td>
                                        <td class="formTxt" valign="top" align="left" width="170">&nbsp;</td>
                                        <td class="formTxt" valign="top" align="left" width="120">&nbsp;</td>
                                        <TD class="formTxt" vAlign="top" align="left" height="14" >From Date (dd/mm/yyyy)
                                           
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1"  ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$" ControlToValidate="txtFromDate" runat="server" ErrorMessage="From Date should be in DD/MM/YYYY Format">*</asp:RegularExpressionValidator>
                                        </TD>
                                        <TD class="formTxt" align="left" height="14">To Date (dd/mm/yyyy) 
                                          
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$" runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date should be in DD/MM/YYYY Format">*</asp:RegularExpressionValidator>
						                </TD>
                                        <td class="formTxt" align="left" width="73">&nbsp;</td>
                                        <td class="formTxt" align="left" >&nbsp;</td>
                                      </tr>
                                      <tr valign="top">
                                        <td width="18" height="14" align="left" valign="top" class="formTxt">&nbsp;</td>
                                        <td height="24" align="left" width="170">
											 <asp:DropDownList ID="ddlType" TabIndex="14" runat="server" CssClass="formField width150" ></asp:DropDownList>
										</td>
										<td class="formTxt" valign="top" align="left" width="180">
                                            <asp:CheckBox id="chkShowOnSite" runat="server" value="checkbox" Text="Display Live Content Only" Checked="true" valign="Top"></asp:CheckBox>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox  id="txtFromDate" tabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
                                            <img alt="Click to Select" src="Images/calendar.gif" id="btnFromDate" style="cursor:pointer; vertical-align:middle;" />                                            	
                                  <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnFromDate TargetControlID="txtFromDate" ID="calFromDate" runat="server">
                                  </cc2:CalendarExtender>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox  id="txtToDate" tabIndex="3" runat="server" CssClass="formField105" ></asp:TextBox>
                                            <img alt="Click to Select" src="Images/calendar.gif" id="btnToDate" style="cursor:pointer; vertical-align:middle;" />                                            	
                                  <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnToDate TargetControlID="txtToDate" ID="calToDate" runat="server">
                                  </cc2:CalendarExtender>
                                        </td>
                                        <td align="left">
                                            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                                <asp:LinkButton runat=server ID="btnView" causesvalidation="false"  CssClass="txtButtonRed" >&nbsp;View&nbsp;</asp:LinkButton>
						  	                </div>
                                        </td>
                                        <td align="left">&nbsp;</td>
                                      </tr>
                                    </table>
                                  </td>
                       </tr>
                    </table>
                    
            
                    <table cellspacing="0" cellpadding="0" width="200"  border="0">
                        <tr>
                            <td width="19" height="14" align="left" valign="top" >&nbsp;</td>
                            <td width="181"  height="14" align="left" valign="top" >&nbsp;</td>
                           
                            
                        </tr>
                        <tr>
                            <td width="19" height="14" align="left" valign="top" >&nbsp;</td>
                            <td>
                                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                    <a id="btnNewContent" class="txtButtonRed" tabindex="11" href="SiteContentForm.aspx" runat="server" ><strong>&nbsp;New Content&nbsp;</strong></a>
                                </div>
                            </td>
                        </tr>
                    </table>
						
				
                 <asp:ValidationSummary   id="ValidationSummary1" runat="server" CssClass="bodytxtValidationMsg"></asp:ValidationSummary>
					               <div id="divValidationMsg" class="bodytxtValidationMsg paddingL28"></div>
		<div class="bodytxtValidationMsg" style="padding:10px 0px 0px 25px;"></div>
					<div class="bodytxtValidationMsg" style="padding:0px 0px 10px 25px;"></div>
									  <asp:Label  CssClass="bodytxtValidationMsg paddingL23" runat="server" ID="lblMsg"></asp:Label>
									  
							  
	                           
			<hr style="background-color:#993366;height:5px;margin:0px;" />
					  <asp:GridView ID="gvContacts"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333"
					   BorderWidth="1px" PageSize=25 BorderColor="White"   DataKeyNames="ContentDate" PagerSettings-Mode=NextPreviousFirstLast 
					    PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
               
               
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
				</table>
                  
               </EmptyDataTemplate>
               <PagerTemplate>
                   
                  
                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>
							<td runat="server" id="tdsetType1" width="20">
							      		
							</td>
							
							<td runat="server"  id="tdApprove" width="150" align="left" class="txtWelcome paddingL10">&nbsp;
							  
							</td>
							<td width="10">&nbsp;</td>
							<td runat="server" id="tdNew" width="" align="right" class="txtWelcome paddingL10">
							 
						    </td>
						    <td id="tdsetType2" align="left" width="121">
								
						    </td>
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
												  </div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
				</table> 
                   
               </PagerTemplate> 
               
               
               <Columns>  
                        
                 
                    <asp:TemplateField SortExpression="TypeID" HeaderText="Type" HeaderStyle-CssClass="headerListing">
                        <HeaderStyle CssClass="gridHdr" /> 
                        <ItemStyle Wrap=true  Width="90px"  CssClass="gridRow" />
                        <ItemTemplate>             
                            <%#  IIF(not isdbnull(Container.DataItem("TypeID")), Container.DataItem("ContentType"),"" ) %>
                        </ItemTemplate>
                    </asp:TemplateField> 
                    
                    <asp:TemplateField SortExpression="CompanyName" HeaderText="Company" HeaderStyle-CssClass="headerListing">
                        <HeaderStyle CssClass="gridHdr" /> 
                        <ItemStyle Wrap=true  Width="90px"  CssClass="gridRow" />
                        <ItemTemplate>             
                            <%#  IIF(not isdbnull(Container.DataItem("CompanyName")), Container.DataItem("CompanyName"),"" ) %>
                        </ItemTemplate>
                    </asp:TemplateField> 
                    
               
                    <asp:TemplateField SortExpression="Title" HeaderText="Title" HeaderStyle-CssClass="headerListing"> 
                        <HeaderStyle CssClass="gridHdr" /> 
                        <ItemStyle Wrap=true    CssClass="gridRow" />
                        <ItemTemplate>             
                            <%#  IIF(not isdbnull(Container.DataItem("Title")), Container.DataItem("Title"),"" ) %>
                        </ItemTemplate>
                    </asp:TemplateField> 
                    
                      
                    <asp:TemplateField SortExpression="Name" HeaderText="Name" HeaderStyle-CssClass="headerListing"> 
                        <HeaderStyle CssClass="gridHdr" /> 
                        <ItemStyle Wrap=true    CssClass="gridRow" />
                        <ItemTemplate>             
                            <%#  IIF(not isdbnull(Container.DataItem("Name")), Container.DataItem("Name"),"" ) %>
                        </ItemTemplate>
                    </asp:TemplateField> 
                    
                   <asp:TemplateField SortExpression="ShowOnStage" HeaderText="Publish On Stage" HeaderStyle-CssClass="headerListing"> 
                          <HeaderStyle CssClass="gridHdr" /> 
                        <ItemStyle Wrap=true  Width="100px"  CssClass="gridRow" />
                        <ItemTemplate>             
                            <%#IIf(Container.DataItem("ShowOnStage") = True, "Yes", "No")%>
                        </ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField SortExpression="ShowOnLive" HeaderText="Publish On Live" HeaderStyle-CssClass="headerListing"> 
                          <HeaderStyle CssClass="gridHdr" /> 
                        <ItemStyle Wrap=true  Width="100px"  CssClass="gridRow" />
                        <ItemTemplate>             
                            <%#IIf(Container.DataItem("ShowOnLive") = True, "Yes", "No")%>
                        </ItemTemplate>
                   </asp:TemplateField> 
                        <asp:TemplateField SortExpression="ContentDate" HeaderText="Content Date" HeaderStyle-CssClass="headerListing"> 
                              <HeaderStyle CssClass="gridHdr" /> 
                            <ItemStyle Wrap=true  Width="115px"  CssClass="gridRow" />
                            <ItemTemplate>             
                            <%#IIf(Strings.FormatDateTime(IIf(Not IsDBNull(Container.DataItem("ContentDate")), Container.DataItem("ContentDate"), ""), DateFormat.ShortDate) = "01/01/1900", "NA", Strings.FormatDateTime(IIf(Not IsDBNull(Container.DataItem("ContentDate")), Container.DataItem("ContentDate"), ""), DateFormat.ShortDate))%>
                        </ItemTemplate>
                    </asp:TemplateField> 
       
          
                    <asp:TemplateField SortExpression="DateCreated" HeaderText="Created Date" HeaderStyle-CssClass="headerListing"> 
                        <HeaderStyle CssClass="gridHdr" /> 
                        <ItemStyle Wrap=true  Width="115px"  CssClass="gridRow" />
                        <ItemTemplate>             
                            <%#  strings.formatdatetime(IIF(not isdbnull(Container.DataItem("DateCreated")), Container.DataItem("DateCreated"),"" ),DateFormat.ShortDate )%>
                        </ItemTemplate>
                    </asp:TemplateField> 
                    
                    
                    <asp:TemplateField HeaderText="Edit" HeaderStyle-CssClass="headerListing">               
                        <HeaderStyle CssClass="gridHdr" /> 
                       <ItemStyle Wrap=true HorizontalAlign=Left Width="30" CssClass="gridRow" />
                       <ItemTemplate >             
                           <%#"<a class=footerTxtSelected href=SiteContentForm.aspx?"   & SetLinks(Container.DataItem("ContentId")) & "><img src='Images/Icons/Icon-Pencil.gif' border='0'>"  & "</a>"%>
                       </ItemTemplate>
                    </asp:TemplateField> 
               
               
                  
                </Columns>
                 <AlternatingRowStyle CssClass=gridRow />
                 <RowStyle CssClass=gridRow />            
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr" HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
        
              </asp:GridView>
              
              
			    </ContentTemplate>
				 <Triggers>
				 <asp:AsyncPostBackTrigger   ControlID="btnView"   /> 				 
				 </Triggers>
            
            </asp:UpdatePanel>
            
                <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="gvContacts" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.SiteContent" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
        
        
        
        
        <!-- END of code -->


		
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      
      
            </asp:Content>
