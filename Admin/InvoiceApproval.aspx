﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InvoiceApproval.aspx.vb" Inherits="Admin.InvoiceApproval" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Invoice Approval" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style type="text/css">
.gridRow{
paddin-right:0px;
}
.clsBorderTbl{
 border-left-width:0px;
 border-right: solid 1px #000000;
 border-bottom: solid 1px #000000;
 padding:5px;
 margin:0px;
 font-family:Arial;font-size:11px;
 }
 .gridRow{
 padding:0px;
 }
</style>
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanelTBQ" runat="server" >
<ContentTemplate>
<script language="JavaScript" type="text/JavaScript">
    function GetTotalAmount(Type, Id) {
        if (document.getElementById(Id).value != "") {
            if (IsFloat(document.getElementById(Id).value) == false) {
                document.getElementById(Id).value = "";
                alert("Please enter only positive numeric values");
            }
            else {
                if (Type == 'WOVal')
                    Id = Id.replace("txtWOVal", "");
                else if (Type == 'Discount')
                    Id = Id.replace("txtDiscount", "");
                else
                    Id = Id.replace("txtVat", "");

                var preAmount

                preAmount = parseFloat(document.getElementById(Id + "lblAmount").innerHTML);

                var vatValue = parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_hdnvatvalue").value);
                

                document.getElementById(Id + "lblAmountAfterDisc").innerHTML = parseFloat((parseFloat(document.getElementById(Id + "txtWOVal").value) - parseFloat(document.getElementById(Id + "txtDiscount").value))).toFixed(2);

                if (Type != 'Vat')
                    document.getElementById(Id + "txtVat").value = parseFloat(parseFloat(document.getElementById(Id + "lblAmountAfterDisc").innerHTML) * vatValue).toFixed(2);

                document.getElementById(Id + "lblAmount").innerHTML = parseFloat((parseFloat(document.getElementById(Id + "txtWOVal").value) - parseFloat(document.getElementById(Id + "txtDiscount").value)) + parseFloat(document.getElementById(Id + "txtVat").value)).toFixed(2);

                document.getElementById("ctl00_ContentPlaceHolder1_lblGrandTotal").innerHTML = parseFloat((parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblGrandTotal").innerHTML) - preAmount) + parseFloat(document.getElementById(Id + "lblAmount").innerHTML)).toFixed(2);
            }
        }
        else {
            alert("Please enter only positive numeric values");
        }
    } 
    function IsFloat(sText) {
        return parseFloat(sText.match(/^-?\d*(\.\d+)?$/)) >= 0;
    }    
</script>
    <div id="divContent">
 		<input type="hidden" runat="server" id="hdnvatvalue" ></input>
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
         
         <tr>
            <td valign="top">
             <table cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
                <tr>
                  <td width="165" class="txtWelcome paddingL10"><div id="divButton" style="width: 120px;  ">
                      <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                      <a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" title="Back to Service" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8" /><strong>Back to Listing</strong></a>
                      <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                  </div></td>                  
                </tr>
             </table>
            </td>
         </tr>
         </table>
         <div style="margin:0px 5px 0px 5px;" id="divMainTab">
            <div class="paddedBox"  id="divProfile" >
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="left" class="HeadingRed"><strong>Invoice Update</strong></td>
                    <td align="left" class="padTop17">                        
                    <table id="tblSaveTop" runat="server" cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                <tr>
                  <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveTop" ValidationGroup="VGAcc" CssClass="txtListing" runat="server" TabIndex="67"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                  <td width="10"></td>
                  <td width="140" align="right" valign="top" style="padding:0px 0px 0px 0px;" id="tdApproveTop" runat="server"><table cellspacing="0" cellpadding="0" width="140" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td width="130" class="txtBtnImage"><asp:LinkButton ID="btnApproveTop" CausesValidation="false" runat="server" TabIndex="68"  class="txtListing"> <img src="Images/Icons/Complete.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Approve Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                  <td width="10"></td>
                  <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetTop" CausesValidation="false" runat="server" TabIndex="68"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                </tr>
            </table>            
                    </td>
                  </tr>
             </table>
              <table width="100%" cellspacing="0" cellpadding="0" border="0" id="tblOriginlaInvoice" runat="server">	
                     <tr>
				        <td>				       				       
				          <div id = "divOuterpnlOriginalInvoice" class="divOuterpnlTimeBuilderQuestions">
                           <div id="divOuterOriginlaInvoice" runat="server">
                                <table width="100%">
                           <tr>                         
                            <td>              
                            <b>Origianl Invoice Details</b>
                            </td>
                           </tr>
                           </table>                                
                                 <asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLOriginalInvoice" Runat="server" ForeColor="#555354">
                                 <HeaderTemplate>
					                    <table cellspacing="0" cellpadding="0" class="gridHdr gridText" style="color:#000; text-align:left;height:25px;padding-left:0px; margin-left:0px;margin-top:10px;">
						                     <tr align="left" >						                          
                                                  <td width="60" class="clsBorderTbl" style="border-top: solid 1px #000000;border-left: solid 1px #000000;"><b>Date</b></td>
                                                  <td width="80" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>WorkOrderId</b></td>
                                                  <td width="320" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Description</b></td>                                          
                                                  <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Price</b></td>
                                                  <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Discount</b></td>
                                                  <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Amt after Discount</b></td>
                                                  <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Vat</b></td>
							                      <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Total</b></td>                                                  
                                                </tr>
					                    </table>
				                    </HeaderTemplate>
				                    <ItemTemplate>
                                        <table cellspacing="0" cellpadding="0" >
                                         <tr>
                                          <td width="60" class="clsBorderTbl" style="word-wrap:break-word; border-left: solid 1px #000000;">                                                                        
                                            <asp:Label ID="lblDate" runat="server" Text=<%#Container.dataitem("InvoiceDate")%>></asp:Label>
                                          </td>
                                          <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;">
                                          <asp:Label ID="lblWorkOrderId" runat="server" Text=<%#Container.dataitem("WorkOrderId")%>></asp:Label>
                                          </td>
                                          <td width="320" class="clsBorderTbl" style="word-wrap:break-word ;">                                          
                                            <asp:Label ID="lblDescription" runat="server" Text=<%#Container.dataitem("Description")%>></asp:Label>
                                          </td>
                                          <td width="115" class="clsBorderTbl" style="word-wrap:break-word ;">                                          
                                            <asp:Label ID="lblWOVal" runat="server" Text=<%#Container.dataitem("WOVal")%>></asp:Label>
                                          </td>
                                           <td width="115" class="clsBorderTbl" style="word-wrap:break-word ;">                                          
                                            <asp:Label ID="lblDiscount" runat="server" Text=<%#Container.dataitem("Discount")%>></asp:Label>
                                          </td>
                                          <td width="115" class="clsBorderTbl" style="word-wrap:break-word ;">
                                          <asp:Label ID="lblAmountAfterDisc" runat="server" Text=<%#Container.dataitem("AmountAfterDisc")%>></asp:Label>
                                          </td>
                                          <td width="115" class="clsBorderTbl" style="word-wrap:break-word ;">                                          
                                           <asp:Label ID="lblVat" runat="server" Text=<%#Container.dataitem("Vat")%>></asp:Label>
                                          </td>
                                          <td width="115" class="clsBorderTbl" style="word-wrap:break-word ;">
                                          <asp:Label ID="lblAmount" runat="server" Text=<%#Container.dataitem("Total")%>></asp:Label>
                                          </td>
                                        </tr>                                                         
				                        </table>
                                    </ItemTemplate>                                   
                                 </asp:DataList>  
                                  <table cellspacing="0" cellpadding="0"  style="color:#000; text-align:left;height:25px;padding-left:0px; margin-left:0px;margin-top:10px;">
						                     <tr align="left" >						                          
                                                  <td width="60">&nbsp;</td>
                                                  <td width="80">&nbsp;</td>
                                                  <td width="320">&nbsp;</td>                                          
                                                  <td width="115">&nbsp;</td>
                                                  <td width="115">&nbsp;</td>
                                                  <td width="115">&nbsp;</td>
                                                  <td width="195">&nbsp;</td>
							                      <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;border-left: solid 1px #000000;"><b><asp:Label ID="lblOriginalGrandTotal" runat="server"></asp:Label></b></td>
                                                  <td width="135">&nbsp;</td>
                                                </tr>
					                    </table>				                                                                
                            </div>                           
                            </div>
				        </td>
				      </tr>				      

                 </table>		
              <div class="divWorkOrder" style="margin-top:17px;float:left;">	      
                    <div id="divValidationApproveData" class="divValidation" runat=server visible="false" >
	                    <div class="roundtopVal">
	                      <blockquote>
	                        <p><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	                        </blockquote>
	                    </div>
		                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
			                    <tr valign="middle">
				                    <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				                    <td class="validationText"><div  id="divValidationMsg"></div>
				                        <asp:Label ID="lblValidationApproveData"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>				                    
				                    </td>
				                    <td width="20">&nbsp;</td>
			                    </tr>
		                    </table>
	                    <div class="roundbottomVal">
	                      <blockquote>
	                        <p><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	                        </blockquote>
	                    </div>
                </div>        
                 
                                                      
	             <table width="100%" cellspacing="0" cellpadding="0" border="0">	
                     <tr>
				        <td>				       				       
				          <div id = "divOuterpnlInvoiceToEdit" class="divOuterpnlTimeBuilderQuestions">
                           <div id="divOuterRedirection" runat="server">
                                <table width="100%">
                           <tr>                         
                            <td>              
                            <b>Invoice No. -</b> <asp:Label ID="lblInvoiceNo" runat="server" class="HeadingRed"></asp:Label>             
                             <TABLE cellSpacing="0" cellPadding="0" width="92" bgColor="#f0f0f0" border="0" style="float:right;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkAddNew"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Add New&nbsp;</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
                            </td>
                           </tr>
                           </table>                                
                                 <asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLInvoiceToEdit" Runat="server" ForeColor="#555354">
                                 <HeaderTemplate>
					                    <table cellspacing="0" cellpadding="0" class="gridHdr gridText" style="color:#000; text-align:left;height:25px;padding-left:0px; margin-left:0px;margin-top:10px;">
						                     <tr align="left" >						                          
                                                  <td width="60" class="clsBorderTbl" style="border-top: solid 1px #000000;border-left: solid 1px #000000;"><b>Date</b></td>
                                                  <td width="80" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>WorkOrderId</b></td>
                                                  <td width="320" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Description</b></td>                                          
                                                  <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Price</b></td>
                                                  <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Discount</b></td>
                                                  <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Amt after Discount</b></td>
                                                  <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Vat</b></td>
							                      <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Total</b></td>
                                                  <td width="135" class="clsBorderTbl" style="border-top: solid 1px #000000;"><b>Actions</b></td>
                                                </tr>
					                    </table>
				                    </HeaderTemplate>
				                    <ItemTemplate>
                                        <table cellspacing="0" cellpadding="0" >
                                         <tr>
                                          <td width="60" class="clsBorderTbl" style="word-wrap:break-word; border-left: solid 1px #000000;">                             
                                            <input type="hidden" id="hdnFinanceApprovalId" runat="server" value=<%#Container.dataitem("FinanceApprovalId")%> />                                             
                                            <asp:Label ID="lblDate" runat="server" Text=<%#Container.dataitem("InvoiceDate")%>></asp:Label>
                                          </td>
                                          <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;">
                                          <asp:Label ID="lblWorkOrderId" runat="server" Text=<%#Container.dataitem("WorkOrderId")%>></asp:Label>
                                          </td>
                                          <td width="320" class="clsBorderTbl" style="word-wrap:break-word ;">
                                          <asp:TextBox ID="txtDescription" runat="server"  CssClass="formField" Width="300" Height="20" Text=<%#Container.dataitem("Description")%>></asp:TextBox>
                                          </td>
                                          <td width="115" class="clsBorderTbl" style="word-wrap:break-word ;">
                                          <asp:TextBox ID="txtWOVal" runat="server" Onblur="Javascript:GetTotalAmount('WOVal',this.id);"  CssClass="formField" Width="100" Height="20" Text=<%#Container.dataitem("WOVal")%>></asp:TextBox>                                                                            
                                          </td>
                                           <td width="115" class="clsBorderTbl" style="word-wrap:break-word ;">
                                          <asp:TextBox ID="txtDiscount" runat="server" Onblur="Javascript:GetTotalAmount('Discount',this.id);"  CssClass="formField" Width="100" Height="20" Text=<%#Container.dataitem("Discount")%>></asp:TextBox>                                                                            
                                          </td>
                                          <td width="115" class="clsBorderTbl" style="word-wrap:break-word ;">
                                          <asp:Label ID="lblAmountAfterDisc" runat="server" Text=<%#Container.dataitem("AmountAfterDisc")%>></asp:Label>
                                          </td>
                                          <td width="115" class="clsBorderTbl" style="word-wrap:break-word ;">
                                          <asp:TextBox ID="txtVat" runat="server" Onblur="Javascript:GetTotalAmount('Vat',this.id);"  CssClass="formField" Width="100" Height="20" Text=<%#Container.dataitem("Vat")%>></asp:TextBox>                                                                            
                                          </td>
                                          <td width="115" class="clsBorderTbl" style="word-wrap:break-word ;">
                                          <asp:Label ID="lblAmount" runat="server" Text=<%#Container.dataitem("Total")%>></asp:Label>
                                          </td>
                                          
                                          <td width="135" class="clsBorderTbl" style="word-wrap:break-word ;">
                                          <div style="float:left;">
                                                <div style="float:left;width:69px;">
                                               <table cellspacing="0" cellpadding="0" >
                                                <tr>
                                                    <TD>
                                                    <div style="width:50px; height:26px;float:left;margin-right:10px;margin-left:-20px;">
                                                        <div style="float:left; width:24px; height:26px; margin-right:7px;"><asp:LinkButton ID="lnkBtnMoveUp" Visible='<%#IIF(Container.DataItem("RowNum") = 1 , False, IIF(Container.DataItem("RowNum") = 0 , False, True))%>' CommandName="MoveUp" CommandArgument='<%#Container.dataitem("RowNum") %>' runat="server" style="margin-left:35px;"><img id="imgMoveUp" runat="server" src="~/Images/Arrows/arrow-up.jpg" title="Move Up" border="0" class="cursorHand" /></asp:LinkButton></div>
                                                        <div style="float:left; width:10px; height:26px;"><asp:LinkButton ID="lnkBtnMoveDown" Visible='<%#IIF(Container.DataItem("RowNum") = Container.DataItem("MaxSequence"), False, IIF(Container.DataItem("RowNum") = 0 , False, True))%>'  CommandName="MoveDown" CommandArgument='<%#Container.dataitem("RowNum") %>' runat="server" style="margin-left:20px;"><img id="imgMoveDown" runat="server" src="~/Images/Arrows/arrow-down.jpg" title="Move Down" border="0" class="cursorHand" /></asp:LinkButton></div>
                                                    </div>                                        
                                                    </TD>
                                                 </tr>
                                                 </table>
                                                   </div>
                                               <div style="float:left;width:64px;">
	                                           <TABLE cellSpacing="0" cellPadding="0" width="60" bgColor="#f0f0f0" border="0" style="float:right;margin-right:20px;margin-left:20px;">
		                                        <TR>
			                                        <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                                        <TD class="txtBtnImage" width="120">
				                                        <asp:Linkbutton id="lnkBtnRemove"  runat="server" CausesValidation="false" CssClass="txtListing" CommandName="RemoveLineItem" CommandArgument='<%#Container.DataItem("FinanceApprovalId") %>'>&nbsp;Remove&nbsp;</asp:LinkButton>
			                                        </TD>
			                                        <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                                        </TR>
	                                        </TABLE>
                                              </div>
                                            </div>

                                          </td>
					                    </tr>                                                         
				                        </table>
                                    </ItemTemplate>                                   
                                 </asp:DataList>  
                                  <table cellspacing="0" cellpadding="0"  style="color:#000; text-align:left;height:25px;padding-left:0px; margin-left:0px;margin-top:10px;">
						                     <tr align="left" >						                          
                                                  <td width="60">&nbsp;</td>
                                                  <td width="80">&nbsp;</td>
                                                  <td width="320">&nbsp;</td>                                          
                                                  <td width="115">&nbsp;</td>
                                                  <td width="115">&nbsp;</td>
                                                  <td width="115">&nbsp;</td>
                                                  <td width="195">&nbsp;</td>
							                      <td width="115" class="clsBorderTbl" style="border-top: solid 1px #000000;border-left: solid 1px #000000;"><b><asp:Label ID="lblGrandTotal" runat="server"></asp:Label></b></td>
                                                  <td width="135">&nbsp;</td>
                                                </tr>
					                    </table>				                                                                
                            </div>                           
                            </div>
				        </td>
				      </tr>		                     
                 </table>		
		     </div>       
             
    <div class="divContainerStyle floatLeft" id="divNotes" runat="server" visible="false" >
	<div class="WOProcessTopHeader" id="divPaymentMethod"><strong class="HeadingRed">Previous Notes:</strong></div>
	<div id="divMiddleBand" class="padding10">
	<asp:GridView ID="gvNotes" runat="server" AllowPaging="False" AutoGenerateColumns="False" BorderColor="#E2E1DD" BorderWidth="1px" 
       Width="1000" AllowSorting="true" GridLines="Both" RowStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr">
            
                               
              
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               
               
        
               <Columns>  
                   
               <asp:TemplateField HeaderText="Date Created" ItemStyle-Width="20%" HeaderStyle-CssClass="txtGrid" ItemStyle-CssClass="txtGrid" ItemStyle-Height="40px">
               <ItemTemplate>
                            <%# Strings.Format(Container.DataItem("DateCreated"), "dd/MM/yyyy <br /> hh:mm tt")%>
               </ItemTemplate>
               
               </asp:TemplateField>
               
                <asp:TemplateField HeaderText="Written By" ItemStyle-Width="30%" HeaderStyle-CssClass="txtGrid" ItemStyle-CssClass="txtGrid" ItemStyle-Height="40px">
               <ItemTemplate>
                            <%#Container.DataItem("OWRepFname")%> <%#Container.DataItem("OWRepLname")%>
               </ItemTemplate>
               
               </asp:TemplateField>
               
               
                <asp:TemplateField HeaderText="Notes" ItemStyle-Width="50%" HeaderStyle-CssClass="txtGrid" ItemStyle-CssClass="txtGrid" ItemStyle-Height="40px">
               <ItemTemplate>
                            <%# (Container.DataItem("Note"))%>
               </ItemTemplate>
               
               </asp:TemplateField>
              
                  
                </Columns>
               
                           
        </asp:GridView>
        <br />            
       
		
    </div>
	</div>
     <div style="margin-left:10px;float:left;">
        <b class="HeadingRed">Notes:</b><br />
            <asp:TextBox runat="server" TextMode="MultiLine"    onblur="javascript:removeHTML(this.id);" ID="txtNote" Height="50" Width="500" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
       </div>
		<div id="divClearBoth"></div>



             <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
              <td align="left" class="HeadingRed">&nbsp;</td>
              <td align="left" class="padTop17"><table id="tblSaveBottom" runat="server" cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                  <tr>
                    <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveBottom" ValidationGroup="VGAcc"  CssClass="txtListing" runat="server" TabIndex="63"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                        </tr>
                    </table></td>
                    <td width="10"></td>
                    <td width="140" align="right" valign="top" style="padding:0px 0px 0px 0px;" id="tdApproveBottom" runat="server"><table cellspacing="0" cellpadding="0" width="140" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td width="130" class="txtBtnImage"><asp:LinkButton ID="btnApproveBottom"  CausesValidation="false" runat="server" TabIndex="64"  class="txtListing"> <img src="Images/Icons/Complete.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Approve Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                        </tr>
                    </table></td>
                    <td width="10"></td>
                    <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetBottom"  CausesValidation="false" runat="server" TabIndex="64"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                        </tr>
                    </table></td>
                  </tr>
              </table></td>
            </tr>
          </table>       
            </div>          
               
         </div>
      </div>
</ContentTemplate>								
</asp:UpdatePanel>
</asp:Content>