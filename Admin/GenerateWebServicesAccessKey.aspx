<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :" CodeBehind="GenerateWebServicesAccessKey.aspx.vb" Inherits="Admin.GenerateWebServicesAccessKey" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>





<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <style type="text/css">

.style1 {font-weight: bold}
.style2 {font-weight: bold}
</style>

 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>

		
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
             <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
             <ContentTemplate>
           			
            <%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
     <table width="100%"  border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td width="7%">&nbsp;</td>
         <td width="30%" class="HeadingRed">Generate Web Service Access Key </td>
         <td width="63%">&nbsp;</td>
       </tr>
       <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
       </tr>
       <tr>
         <td>&nbsp;</td>
         <td><uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact></td>
         <td>
             <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                <asp:LinkButton ID="lnkView" causesValidation=False OnClick="lnkView_Click" runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
             </div>
        </td>
       </tr>
       <tr>
         <td>&nbsp;</td>
         <td><asp:Label ID="lblError"   runat="server" class="bodytxtValidationMsg" /></td>
         <td>&nbsp;</td>
       </tr>
       <tr>
         <td>&nbsp;</td>
         <td><asp:Panel runat="server" id="pnlCredentials">
		 <table width="102%" height="125"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="189" height="37" style="width: 62px">&nbsp;</td>
    <td width="186">&nbsp;</td>
  </tr>
  <tr>
    <td height="23" align="left" >
        AccountId</td>
    <td >: 
      <span class="style1">
      <asp:Label ID="lblAccountID" runat="server" /> 
      </span></td>
  </tr>
  <tr>
    <td align="left" >Password</td>
    <td>: 
      <span class="style2">
      <asp:Label ID="lblPassword" runat="server" /> 
      </span></td>
  </tr>
  <tr>
    <td align="left" >Enable Account </td>
    <td>:
      <asp:CheckBox ID="chkIsEnable" runat="server" /></td>
  </tr>
  <tr>
    <td align="left" >&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" >&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" >&nbsp;</td>
    <td align="left"><TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0">
      <TR>
        <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
        <TD width="80" class="txtBtnImage"><asp:LinkButton ID="btnSave" runat="server" class="txtListing" CausesValidation="false"  > <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save&nbsp;</asp:LinkButton>
        </TD>
        <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
      </TR>
    </TABLE></td>
  </tr>
</table>

		 </asp:Panel>

  <asp:Panel runat="server" id="pnlGetCredentials">
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="4%">&nbsp;</td>
    <td width="96%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0">
      <TR>
        <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
        <TD width="100" class="txtBtnImage"><asp:LinkButton ID="btnGetWSAccessKey" runat="server" class="txtListing" CausesValidation="false"  > <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Get WS Key &nbsp;</asp:LinkButton>
        </TD>
        <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
      </TR>
    </TABLE></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

  </asp:Panel>
		 </td>
         <td valign="bottom">&nbsp;</td>
       </tr>
       <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
       </tr>
     </table>
        </ContentTemplate>        
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
         
               <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
</asp:Content>