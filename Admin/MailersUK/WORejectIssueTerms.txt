<p>Dear <Name>,</p>

<p>The current issue relating to Work Order <WorkOrderID> has been rejected because of following reason:</p>

<p>Reason: <Comment></p>

<p>Kind regards, <br />
The OrderWork Team </p>

<p>Telephone: 0203 053 0343 <br />
E-mail: info@orderwork.co.uk<br />
My OrderWork: http://orderwork.co.uk/login.php <br />
Website: www.orderwork.co.uk</p>

<p>Note: This email was automatically generated following a Work Order action performed on the OrderWork site.</p>