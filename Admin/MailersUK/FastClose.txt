<p>Dear <name>,</p>

<p>The following work order <workorderid> has been marked as closed and a self billed invoice has been raised on your behalf - <pino>.<br />
Payment in relation to this invoice will be included in our upcoming payment run.</p>

<p>If you have any questions, please feel free to contact us. </p>

<p>Kind regards, <br />
The OrderWork Team </p>

<p>Telephone: 0203 053 0343 <br />
E-mail: info@orderwork.co.uk <br />
My OrderWork: http://orderwork.co.uk/login.php<br />
Website: www.orderwork.co.uk</p>

<p>Note: This email was automatically generated following your registration on the OrderWork website.</p>