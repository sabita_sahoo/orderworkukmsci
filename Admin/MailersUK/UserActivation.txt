<p>Dear <Name>,</p>

<p>Welcome to OrderWork - a new way to find on-site IT service!</p>

<p><AdminName> has created an account for you to access the OrderWork portal. </p>

<p>Your registration details are:</p>

<p>Login: <Email><br />
Password <Password></p>

<p>You will need to activate your account. To do this please click the following link (or copy/paste this link into your Web browser) and enter the password you provided during registration:</p>

<p><ActivationLink></p>

<p>For security reasons, please change your password after successful account activation</p>

<p>If you have any questions, please feel free to contact us. </p>

<p>Kind regards, <br />
The OrderWork Team </p>

<p>Telephone: 0203 053 0343 <br />
E-mail: info@orderwork.co.uk <br />
My OrderWork: http://orderwork.co.uk/login.php<br />
Website: www.orderwork.co.uk</p>