<p>Dear ***Name***, <br/><br/></p>

<p>Welcome to the OrderWork Portal! <br/><br/></p>

<p>Your login Details are as follows: <br/><br/></p>

<p><b>Username:</b>***Username*** <br/><br />
<b>Password:</b>***Password*** <br/><br/></p>

<p>To Logon to Orderwork please visit this link http://orderwork.co.uk/login.php <br/><br/></p>

<p>If you have any questions, please feel free to contact us. <br/><br/></p>

<p>Kind Regards,<br/><br />
The OrderWork Team <br/><br/> </p>

<p><b>Telephone:</b> 0203 053 0343 <br/><br />
<b>E-mail:</b> info@orderwork.co.uk <br/><br />
<b>My OrderWork:</b> http://orderwork.co.uk/login.php <br/><br />
<b>Website:</b> www.orderwork.co.uk <br/><br/></p>

<p><b>Note: This email was automatically generated following your registration on the OrderWork website.</b><br />
</p>