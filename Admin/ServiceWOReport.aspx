﻿<%@ Page Title="ServiceWOReport" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="ServiceWOReport.aspx.vb" Inherits="Admin.ServiceWOReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register TagPrefix="lib" Namespace="WebLibrary" Assembly="WebLibrary" %>
<%@ Register TagPrefix="cc1" Namespace="Flan.Controls" Assembly="Flan.Controls" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <style type="text/css">
        select {
            width: 138px;
        }

        .auto-style1 {
            width: 164px;
        }
    </style>
    <script type="text/javascript">
        
        function SelectAutoSuggestRow(inp, data) {
            inp.value = data[0];
            var x = document.getElementById("ctl00_ContentPlaceHolder1_hdnSelectedCompanyName").value = inp.value;
            document.getElementById("ctl00_ContentPlaceHolder1_hdnbtnSelectedCompany").click();
        }
    </script>
    <div id="divContent">
        <div class="roundtopWhite">
            <img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" />
        </div>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">

            <tr>
                <td valign="top">
                    <!-- InstanceBeginEditable name="EdtContent" -->
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>&nbsp;</td>
                            <td class="HeadingRed"><strong>Service WO Report</strong></td>
                        </tr>
                    </table>

                  
                    

                    <table width="100%" cellpadding="0" cellspacing="5" border="0" style="padding-top: 15px;">
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td width="100px"></td>
                            <td></td>
                            <td class="auto-style1"></td>
                            <td align="left"></td>
                        </tr>
                        <tr>
                            <td class="formTxt" valign="top" align="left" width="185px">Submitted Date From (dd/mm/yyyy)</td>
                            <td class="formTxt" valign="top" align="left" width="170px">Submitted Date To (dd/mm/yyyy)</td>
                            <td class="formTxt" valign="top" align="left" width="145px">Start Date (dd/mm/yyyy)</td>
                            <td class="formTxt" valign="top" align="left" width="145px">End Date (dd/mm/yyyy)</td>
                            <td class="formTxt" valign="top" align="left" width="120px">PO Number </td>

                            <td class="formTxt" valign="top" align="left" width="138px">Status </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtSubmittedDateFrom" TabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
                                <img alt="Click to Select" src="Images/calendar.gif" id="btnSubmittedDateFrom" style="cursor: pointer; vertical-align: top;" />
                                <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnSubmittedDateFrom" TargetControlID="txtSubmittedDateFrom" ID="calSubmittedDateFrom" runat="server">
                                </cc2:CalendarExtender>

                            </td>
                            <td>
                                <asp:TextBox ID="txtSubmittedDateTo" TabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
                                <img alt="Click to Select" src="Images/calendar.gif" id="btnSubmittedDateTo" style="cursor: pointer; vertical-align: top;" />
                                <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnSubmittedDateTo" TargetControlID="txtSubmittedDateTo" ID="calSubmittedDateTo" runat="server">
                                </cc2:CalendarExtender>


                            </td>
                            <td>
                                <asp:TextBox ID="txtStartDate" TabIndex="2" runat="server" CssClass="formField105"></asp:TextBox>
                                <img alt="Click to Select" src="Images/calendar.gif" id="btnStartDate" style="cursor: pointer; vertical-align: top;" />
                                <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnStartDate" TargetControlID="txtStartDate" ID="calStartDate" runat="server">
                                </cc2:CalendarExtender>

                            </td>
                            <td>
                                <asp:TextBox ID="txtEndDate" TabIndex="3" runat="server" CssClass="formField105"></asp:TextBox>
                                <img alt="Click to Select" src="Images/calendar.gif" id="btnEndDate" style="cursor: pointer; vertical-align: top;" />
                                <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnEndDate" TargetControlID="txtEndDate" ID="calEndDate" runat="server">
                                </cc2:CalendarExtender>


                            </td>

                            <td width="100px">
                                <asp:TextBox ID="txtPONumber" TabIndex="4" runat="server" CssClass="formField105"></asp:TextBox>

                            </td>
                            <td width="128px">
                                <asp:DropDownList ID="drpStatus" runat="server" CssClass="formFieldGrey121"></asp:DropDownList>
                            </td>

                        </tr>
                      

                        <tr>
                            <td class="formTxt" valign="top" align="left" width="160px">Company Name</td>
                        </tr>

                        <tr>
                            <td>
                                <input id="hdnSelectedCompanyName" type="hidden" name="hdnSelectedCompanyName" runat="server" value="" />
                                <lib:Input ID="txtCompanyName" runat="server" DataType="List"
                                    Method="GetCompanyName" CssClass="formField150" OnSelect='SelectAutoSuggestRow' SelectParameters='CompanyName' />
                            </td>
                            <%-- <td >
                                       <%@ register src="~/UserControls/Admin/UCSearchContact.ascx" tagname="UCSearchContact" tagprefix="uc1" %>
												<uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact> 
                                    </td>--%>

                            <td align="left">
                                <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important; color: #FFFFFF; text-align: center; width: 105px;">
                                    <asp:LinkButton ID="lnkBtnGenerateReport" CausesValidation="True" runat="server" CssClass="txtButtonRed" Text="Generate Report"></asp:LinkButton>
                                </div>
                            </td>

                            <td align="left">
                                <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important; color: #FFFFFF; text-align: center; width: 105px;">
                                    <asp:LinkButton ID="lnkBtnExportToExcel" Visible="False" runat="server" CssClass="txtButtonRed" Text="Export To Excel"></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="pnlSelectedCompany" runat="server" Visible="false">
                        <div class="divStyleCompany">
                            <asp:Repeater ID="rptSelectedCompany" runat="server">
                                <ItemTemplate>
                                    <div class="divStyle1 roundifyRefineMainBody">
                                        <span class="clsFloatLeft">
                                            <asp:Label ID="lblcompany" runat="server" Text=' <%#Eval("CompanyName")%> '></asp:Label></span>
                                        <span class="marginleft8 clsFloatLeft">
                                            <asp:LinkButton ID="lnkbtnRemove" CommandName="Remove" CausesValidation="false" CommandArgument='<%#Container.DataItem("CompanyID") %>' runat="server" class="deleteButtonAccredation">
                                                            x</asp:LinkButton></span>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Label runat="server" ID="lblSelectedCompany" CssClass="txtOrange" ForeColor="Red" Style="clear: both; display: block; padding-left: 10px;" Text=""></asp:Label>
                        </div>
                    </asp:Panel>


                    <tr>
                        <td align="right" colspan="7">

                            <asp:Panel ID="pnlShowRecords" runat="server" Visible="False">

                                <table width="1500">

                                    <tr>
                                        <td width="100" style="font-weight: bold;">WOID</td>
                                        <td width="150" style="font-weight: bold;">Ref WOID</td>
                                        <td width="150" style="font-weight: bold;">Start Date</td>
                                        <td width="150" style="font-weight: bold;">End Date</td>
                                        <td width="150" style="font-weight: bold;">Submitted Date</td>
                                        <td width="150" style="font-weight: bold;">Client Account</td>
                                        <td width="150" style="font-weight: bold;">Supplier Account</td>
                                        <td width="150" style="font-weight: bold;">WOTitle</td>
                                        <td width="150" style="font-weight: bold;">WP</td>
                                        <td width="150" style="font-weight: bold;">PP</td>
                                        <td width="150" style="font-weight: bold;">Closed Date</td>
                                        <td width="150" style="font-weight: bold;">Invoice Date</td>
                                        <td width="150" style="font-weight: bold;">Sales Invoice</td>
                                        <td width="150" style="font-weight: bold;">Purchase Invoice</td>
                                        <td width="150" style="font-weight: bold;">Work Order Status</td>
                                        <td width="150" style="font-weight: bold;">PONumber</td>
                                        <td width="150" style="font-weight: bold;">Created By</td>

                                    </tr>
                                    <tr>
                                        <asp:Repeater ID="rptShowRecords" runat="server">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td width="100"><%#Container.DataItem("WOID")%></td>
                                                        <td width="150"><%#Container.DataItem("RefWOID")%></td>
                                                        <td width="150"><%#Container.DataItem("Start Date")%></td>
                                                        <td width="150"><%#Container.DataItem("End Date")%></td>
                                                        <td width="150"><%#Container.DataItem("Submitted Date")%></td>
                                                        <td width="150"><%#Container.DataItem("ClientAccount")%></td>
                                                        <td width="150"><%#Container.DataItem("SupplierAccount")%></td>
                                                        <td width="150"><%#Container.DataItem("WOTitle")%></td>
                                                        <td width="150"><%#Container.DataItem("WP")%></td>
                                                        <td width="150"><%#Container.DataItem("PP")%></td>
                                                        <td width="150"><%#Container.DataItem("Closed Date")%></td>
                                                        <td width="150"><%#Container.DataItem("Invoice Date")%></td>
                                                        <td width="150"><%#Container.DataItem("SalesInvoice")%></td>
                                                        <td width="150"><%#Container.DataItem("PurchaseInvoice")%></td>
                                                        <td width="150"><%#Container.DataItem("WorkOrder Status")%></td>
                                                        <td width="150"><%#Container.DataItem("PONumber")%></td>
                                                        <td width="150"><%#Container.DataItem("CreatedBy")%></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                </table>





                            </asp:Panel>
                            <asp:Panel ID="pnlNoRecords" runat="server" Visible="false">
                                <table width="100%">
                                    <tr>
                                        <td style="text-align: center; height: 250px; padding-top: 150px;" align="center">No records Available
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlInitialize" runat="server" Visible="true">
                            </asp:Panel>
                        </td>
                    </tr>
            </td>
            </tr>
        </table>
    </div>
</asp:Content>
