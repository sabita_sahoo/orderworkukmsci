<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CurrentSalesReport.aspx.vb" Inherits="Admin.CurrentSalesReport" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :"%>

<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    
 <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>






	
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
              <p class="paddingB4 HeadingRed"><strong>Current Month </strong></p>
			  
			  
			<br>
			  
			  
              <table cellspacing="0" cellpadding="0" width="500" border="0">
                <tr>
                  <td><p class="formTxt" style="padding:0px 0px 5px 0px; margin:0px; "><b>All Oustanding Work Orders to date: </b></p></td>
                  <td align="right">
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                        <a  href="ExportToExcel.aspx?page=bizDivId" target="_blank" id="btnExport" runat="server" tabIndex="6" class="txtButtonRed">&nbsp;Export to Excel&nbsp;</a>
                    </div>
                  </td>
                </tr>
              </table>
              <table width="500" class="tblBdrBlack"  cellspacing="0" cellpadding="0">
                <tr>
                  <td><table width="500"  border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td width="240" class="gridHdrNotBold"><a href="AdminWOListing.aspx?mode=Submitted&calledfrom=CurrentSales" class="footerTxtSelected">Submitted:</a></td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblOSubmitted"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridRow"><a href="AdminWOListing.aspx?mode=CA&calledfrom=CurrentSales" class="footerTxtSelected">Conditionally Accepted:</a></td>
                        <td width="20" class="gridRow">:</td>
                        <td class="gridRow"><asp:Label runat="server" ID="lblOCA"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridHdrNotBold"><a href="AdminWOListing.aspx?mode=Accepted&calledfrom=CurrentSales" class="footerTxtSelected">Active:</a></td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblOActive"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridRow"><a href="AdminWOListing.aspx?mode=Issue&calledfrom=CurrentSales" class="footerTxtSelected">Issue:</a></td>
                        <td width="20" class="gridRow">:</td>
                        <td class="gridRow"><asp:Label runat="server" ID="lblOIssue"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridHdrNotBold"><a href="AdminWOListing.aspx?mode=Completed&calledfrom=CurrentSales" class="footerTxtSelected">Completed:</a></td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblOCompleted"></asp:Label></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
              <br>
              <br>
              <p class="formTxt" style="padding:0px 0px 5px 0px; margin:0px; "><b>All Submitted during the Current Month: </b></p>
              <table width="500" class="tblBdrBlack"  cellspacing="0" cellpadding="0">
                <tr>
                  <td><table width="500"  border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td width="240" class="gridHdrNotBold">Total no. of Submitted Work Orders</td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblSubmittedWOS"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridRow">Total value of Submitted Work Orders</td>
                        <td width="20" class="gridRow">:</td>
                        <td class="gridRow"><asp:Label runat="server" ID="lblSubmittedWOSVal"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridHdrNotBold">Total no. of Conditionally Accepted Work Orders</td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblCAWOS"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridRow">Total value of Conditionally Accepted Work Orders</td>
                        <td width="20" class="gridRow">:</td>
                        <td class="gridRow"><asp:Label runat="server" ID="lblCAWOSVal"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridHdrNotBold">Total no. of Active Work Orders</td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblActiveWOS"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridRow">Total value of Active Work Orders</td>
                        <td width="20" class="gridRow">:</td>
                        <td class="gridRow"><asp:Label runat="server" ID="lblActiveWOVal"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridHdrNotBold">Total no. of Closed Work Orders</td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblClosedWOS"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridRow">Total value of Closed Work Orders</td>
                        <td width="20" class="gridRow">:</td>
                        <td class="gridRow"><asp:Label runat="server" ID="lblClosedWOVal"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridHdrNotBold">Total no. of Cancelled Work Orders</td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblCancelledWOS"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridRow">Total value of Cancelled Work Orders</td>
                        <td width="20" class="gridRow">:</td>
                        <td class="gridRow"><asp:Label runat="server" ID="lblCancelledWOVal"></asp:Label></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
              <br>
              <br>
              <p class="formTxt" style="padding:0px 0px 5px 0px; margin:0px; "><b>Monthly Revenues: </b></p>
              <table width="500" class="tblBdrBlack"  cellspacing="0" cellpadding="0">
                <tr>
                  <td><table width="500" border="0" bordercolor="#FFFFFF" cellspacing="0" cellpadding="5">
                      <tr>
                        <td width="251"  class="gridHdrNotBold">Total Listing Fee </td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblListingFee"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="251"  class="gridRow">VAT on Total Listing Fee </td>
                        <td width="20" class="gridRow">:</td>
                        <td class="gridRow"><asp:Label runat="server" ID="lblVATonListingFee"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="251"  class="gridHdrNotBold">Total Commission Charge </td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblCommission"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="251"  class="gridRow">VAT on Total Commission Charge </td>
                        <td width="20" class="gridRow">:</td>
                        <td class="gridRow"><asp:Label runat="server" ID="lblVATonCommission"></asp:Label></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
              <br>
              <br>
              <p class="formTxt" style="padding:0px 0px 5px 0px; margin:0px; "><b>Orderwork Account Balance: </b></p>
              <table width="500" class="tblBdrBlack"  cellspacing="0" cellpadding="0">
                <tr>
                  <td><table width="500"  border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td width="240" class="gridHdrNotBold">Total 'Funds to be Received' amount</td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblFTR"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridRow">Total 'Funds Received' amount</td>
                        <td width="20" class="gridRow">:</td>
                        <td class="gridRow"><asp:Label runat="server" ID="lblFR"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridHdrNotBold">Total 'Funds to be Received' outstanding amount</td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblOFTR"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridRow">Total 'Completed Payments' amount</td>
                        <td width="20" class="gridRow">:</td>
                        <td class="gridRow"><asp:Label runat="server" ID="lblCompletedPayments"></asp:Label></td>
                      </tr>
                      <tr>
                        <td width="240" class="gridHdrNotBold">Client Account Balance</td>
                        <td width="20" class="gridHdrNotBold">:</td>
                        <td class="gridHdrNotBold"><asp:Label runat="server" ID="lblAccountBal"></asp:Label></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
              <br>
<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
</asp:Content>
