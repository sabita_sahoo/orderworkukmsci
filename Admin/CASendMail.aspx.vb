Public Partial Class CASendMail
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected WithEvents lblStartDate As Global.System.Web.UI.WebControls.Label
    Protected WithEvents lblEndDate As Global.System.Web.UI.WebControls.Label
    Protected WithEvents hdnSuppliers As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetNoStore()
        Dim ds As New DataSet
        If Not IsPostBack Then
            ViewState("WOID") = Request("WOID")
            ViewState("CompanyID") = Request("CompanyID")
            ViewState("Group") = Request("Group")
            Dim BizdivID As Integer
            BizdivID = Request("BizdivID")
            'Cache.Remove("WODetails" & Session.SessionID)
            ds = getWODetails(True)
            populateData(ds)
        End If

    End Sub
    ''' <summary>
    ''' Populating Data for WO Summary i.e. WorkOrder Details
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <remarks>Pankajm-16-Dec, 2008</remarks>

    Public Sub populateData(ByVal ds As DataSet)
        'Populate WO Summary
        If (ds.Tables("tblSummary").Rows.Count <> 0) Then
            'Buyer Contact ID
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("ContactId")) Then
                ViewState("BuyerContID") = ds.Tables("tblSummary").Rows(0).Item("ContactId")
            End If
            'Buyer Company Name
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("BuyerCompany")) Then
                lblBuyerCompany.Text = ds.Tables("tblSummary").Rows(0).Item("BuyerCompany")
            End If
            'Buyer Company ID
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("CompanyId")) Then
                ViewState("BuyerCompID") = ds.Tables("tblSummary").Rows(0).Item("CompanyId")
            End If

            'WO No
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("RefWOID")) Then
                lblWONo.Text = ds.Tables("tblSummary").Rows(0).Item("RefWOID")
            End If
            'Created
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateCreated")) Then
                lblCreated.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateCreated"), DateFormat.ShortDate)
            End If
            'Modified
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateModified")) Then
                lblModified.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateModified"), DateFormat.ShortDate)
            End If
            'Location
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Location")) Then
                lblLocation.Text = ds.Tables("tblSummary").Rows(0).Item("Location")
            End If
            'WO Title
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("WOTitle")) Then
                lblTitle.Text = ds.Tables("tblSummary").Rows(0).Item("WOTitle")
            End If
            'WO Start
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateStart")) Then
                lblWOStart.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
            End If
            'Wholesale Price
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice")) Then
                lblWholesalePrice.Text = FormatCurrency(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.True, TriState.False)
                hdnValue.Value = FormatNumber(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice"), 2)
            End If
            'Platform Price
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("PlatformPrice")) Then
                lblPlatformPrice.Text = FormatCurrency(ds.Tables("tblSummary").Rows(0).Item("PlatformPrice"), 2, TriState.True, TriState.True, TriState.False)
                hdnValue.Value = FormatNumber(ds.Tables("tblSummary").Rows(0).Item("PlatformPrice"), 2)
            End If
            'Status
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Status")) Then
                lblStatus.Text = ds.Tables("tblSummary").Rows(0).Item("Status")
            End If
        End If
        If Not IsNothing(ds.Tables("tblCADetails")) Then
            If ds.Tables("tblCADetails").Rows.Count > 0 Then
                'Added By Pankaj Malav on 16 Dec 2008 as part of added functionality of CA Send Mail
                'This below code stores the count in view state and later used for showing or hiding the link.
                ViewState.Add("CASupplierCount", ds.Tables("tblCADetails").Rows.Count)
                Dim dv As DataView
                dv = ds.Tables("tblCADetails").DefaultView
                Session("BindDataList") = dv
                BindDataList(dv)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Function to get the data from database populating WorkOrder Details, Response of Suppliers and their comments
    ''' </summary>
    ''' <param name="killcache"></param>
    ''' <returns>Dataset for reuse</returns>
    ''' <remarks>Pankajm-16-Dec, 2008</remarks>
    Public Function getWODetails(Optional ByVal killcache As Boolean = False) As DataSet
        If killcache = True Then
            Session.Remove("AdminCADetails" & Session.SessionID)
        End If
        Dim ds As DataSet
        If Session("AdminCADetails" & Session.SessionID) Is Nothing Then
            ds = ws.WSWorkOrder.GetCADetails(ViewState("WOID"), ViewState("CompanyID"))
            Session.Add("AdminCADetails" & Session.SessionID, ds)
        Else
            ds = CType(Session("AdminCADetails" & Session.SessionID), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' Populating Supplier Response
    ''' </summary>
    ''' <param name="dv"></param>
    ''' <remarks>Pankajm-16-Dec, 2008</remarks>
    Public Sub BindDataList(ByVal dv As DataView)
        Dim dvSource As DataView = dv
        DLSupplierResponse.DataSource = dvSource
        DLSupplierResponse.DataBind()
    End Sub

    ''' <summary>
    ''' Populating Comments by Suppliers
    ''' </summary>
    ''' <param name="WOTrackingId"></param>
    ''' <returns>Returns Dataview as called from aspx page</returns>
    ''' <remarks>Pankajm-16-Dec, 2008</remarks>
    Public Function getDiscussions(ByVal WOTrackingId As Integer) As DataView
        Dim ds As DataSet = getWODetails(False)
        Dim dv As DataView = ds.Tables("tblComments").DefaultView
        dv.RowFilter = "WOTrackingId = " & WOTrackingId
        Return dv
    End Function

    ''' <summary>
    ''' Show Hide Comments i.e. if there are no comments then Hide the Arrow
    ''' Called from aspx page
    ''' </summary>
    ''' <param name="WOTrackingid"></param>
    ''' <returns>True or False</returns>
    ''' <remarks>Pankajm-16-Dec, 2008</remarks>
    Public Function ShowHideComments(ByVal WOTrackingid As Integer) As Boolean
        Dim ds As DataSet = getWODetails()
        Dim dv As DataView = ds.Tables("tblComments").DefaultView
        dv.RowFilter = "WOTrackingId = " & WOTrackingid & " and Comments <> ''"
        If dv.Count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' On click of cancel redirect admin to AdminWOListing as done in other action pages
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Pankajm-16-Dec, 2008</remarks>
    Public Sub lnkCancel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.ServerClick
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            If Request("sender") = "UCWODetails" Or Request("sender") = "UCWOsListing" Then
                link = "~\AdminWODetails.aspx?"
            End If
            link &= "WOID=" & ViewState("WOID")
            link &= "&ContactID=" & Request("ContactID")
            link &= "&CompanyID=" & Request("CompanyID")
            link &= "&CompID=" & Request("CompanyID")
            link &= "&Viewer=" & Request("Viewer")
            link &= "&Group=" & Request("Group")
            link &= "&mode=" & Request("Group")
            link &= "&BizDivID=" & Request("BizDivID")
            link &= "&FromDate=" & Request("FromDate")
            link &= "&ToDate=" & Request("ToDate")
            link &= "&PS=" & Request("PS")
            link &= "&PN=" & Request("PN")
            link &= "&SC=" & Request("SC")
            link &= "&SO=" & Request("SO")
            link &= "&sender=" & "UCWOsDetails"
            Response.Redirect(link)
        End If

    End Sub

    ''' <summary>
    ''' On click of confirm get the selected servicepartners, save info and then send mail
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Pankajm-16-Dec, 2008</remarks>
    Public Sub lnkConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkConfirm.Click
        Dim SelectedSuppliers As String
        SelectedSuppliers = GetSelectedSuppliers()

        If SelectedSuppliers.Length > 0 Then
            Dim ds As New DataSet
            ds = ws.WSWorkOrder.SaveCACommentsSendMail(SelectedSuppliers, ViewState("WOID"), txtWOComment.Text.Trim)
            If ds.Tables(0).Rows(0).Item(0) <> -1 Then
                'Run loop For sending mail to each supplier
                'Find distinct SupplierIDs and accordingly filter table for mail sending and for supplier response
                Dim dtDistinct As New DataTable
                Dim dvWOSummary As New DataView
                Dim dvEmailInfo As New DataView
                Dim dvSupplier As New DataView
                Dim CompanyID As Integer

                ' Store only distinct supplierIS in dtDistinct
                ds.Tables(3).TableName = "tblMail"
                dtDistinct = CommonFunctions.SelectDistinct("tblMail", ds.Tables("tblMail"), "CompanyID")

                dvWOSummary = ds.Tables(1).Copy.DefaultView
                dvSupplier = ds.Tables(2).Copy.DefaultView
                dvEmailInfo = ds.Tables(3).Copy.DefaultView

                For Each row As DataRow In dtDistinct.Rows
                    CompanyID = row("CompanyID")
                    dvSupplier.RowFilter = "CompanyID = '" & CompanyID & "'"
                    dvEmailInfo.RowFilter = "CompanyID = '" & CompanyID & "'"
                    Emails.SendCAMail(dvEmailInfo.ToTable.DefaultView, dvWOSummary, dvSupplier.ToTable.DefaultView, txtWOComment.Text.Trim)
                    pnlSuccessMsg.Visible = True
                    pnlMailContentArea.Visible = False
                    pnlSupplierResponse.Visible = False
                Next
            Else
                lblError.Text = "Not Able to Send Mail. Please Try Later"
            End If
        Else
            'Show Validation Message if no Supplier is selected
            divValidationMain.Visible = True
            validationSummarySubmit.Visible = True
            lblError.Text = "Please Select atleast one Supplier"
        End If

    End Sub

    ''' <summary>
    ''' Method to get the Selected WOIDs
    ''' </summary>
    ''' <remarks>Pankajm-16-Dec, 2008</remarks>
    Public Function GetSelectedSuppliers() As String
        Dim selectedIds As String = ""
        For Each Item As DataListItem In DLSupplierResponse.Items
            Dim chkBox As CheckBox = CType(Item.FindControl("Check"), CheckBox)
            If Not (chkBox Is Nothing) Then
                If chkBox.Checked Then
                    If selectedIds = "" Then
                        selectedIds = CType(Item.FindControl("hdnSuppliers"), HtmlInputHidden).Value
                    Else
                        selectedIds = selectedIds & "," & CType(Item.FindControl("hdnSuppliers"), HtmlInputHidden).Value
                    End If
                End If
            End If
        Next
        Return selectedIds
    End Function

End Class