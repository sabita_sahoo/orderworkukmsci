﻿Public Class BookWorkorder
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then
            iFrameBookWorkorder.Attributes.Add("src", ApplicationSettings.NewPortalURL.ToString & "/workorder/" & Session("UserID").ToString & "/createworkorder")
        End If
    End Sub
End Class