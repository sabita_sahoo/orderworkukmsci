<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :" CodeBehind="HolidayListing.aspx.vb" Inherits="Admin.HolidayListing" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>

			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<input id="hdnPageSize" type="hidden" name="hdnPageSize" runat="server"> 
			<input id="hdnPageNo" type="hidden" name="hdnPageNo" runat="server">
			<input id="sortExpr" type="hidden" name="sortExpr" runat="server"> 
			<input id="sortOrder" type="hidden" name="sortOrder" runat="server">
			<input id="totalRecs" type="hidden" name="totalRecs" runat="server"> 
			<input id="pageRecs" type="hidden" name="pageRecs" runat="server">
			<input id="defaultSortImgId" type="hidden" name="defaultSortImgId" runat="server"> 			
			<input type="hidden" id="hdnDateID" name="hdnDateID" runat="server">
			<input type="hidden" id="hdnDate" name="hdnDate" runat="server">
			<input type="hidden" id="hdntxt" name="hdntxt" runat="server">
			<input type="hidden" id="hdnIsDeleted" name="hdnIsDeleted" runat="server">			
			           
             <table width="100%"><tr><td width="10">&nbsp;</td><td>
             <asp:Label ID="lblErr" CssClass="bodytxtValidationMsg" runat="server" Text=""></asp:Label>
             <asp:Panel ID="pnlListing" runat="server" >             
             <TABLE border="0" cellPadding="0" cellSpacing="0" width="440">
							  <TR>
							    <td width="30">&nbsp;</td>
								<TD align="left" id="btnAddHol" runat="server">
                                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;width:120px;" onclick="javascript:AddHoliday('AddEdit')"> 
							            <a runat="server" id="btnAddHoliday" tabIndex="6" class="buttonText cursorHand" style="line-height:20px !important;vertical-align:middle !important;" ><strong>Add New Holiday</strong></a>
                                    </div>	
								</TD>
								<TD width="15"></TD>
								<td width="200"><asp:CheckBox ID="chkbxShowDeleted" runat="server" AutoPostBack="true" Text="Hide Deleted" Checked="True"  /></td>
							  </TR>
							</TABLE>						
                 <asp:GridView ID="gvHolidays" runat="server" AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
               PagerSettings-Mode=NextPreviousFirstLast CssClass="gridrow"  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               <PagerTemplate>                  
                  
                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>							
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
                 </PagerTemplate>
                  <Columns>                                       
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="22%"  HeaderStyle-CssClass="gridHdr"  DataField="HolidayDate" SortExpression="HolidayDate" HeaderText="Holiday Date" />            
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="22%"  HeaderStyle-CssClass="gridHdr"  DataField="HolidayTxt" SortExpression="HolidayTxt" HeaderText="Holiday" />            
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="22%"  HeaderStyle-CssClass="gridHdr"  DataField="IsDeleted" SortExpression="IsDeleted" HeaderText="IsDeleted" />
                     <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">                
                        <ItemStyle CssClass="gridRowIcons" Wrap=true  Width="14px"/>
                        <ItemTemplate> 
               	        <asp:LinkButton ID="lnkEditbtn" runat="server" CommandName="Edit1" Visible='<% #iif(Session("RoleGroupID")<>admin.Applicationsettings.RoleOWRepID,true,false)%>' CommandArgument='<%#Container.DataItem("DateID")& "," & Container.DataItem("HolidayDate") & "," & Container.DataItem("HolidayTxt") & "," & Container.DataItem("IsDeleted") %>'><img src="Images/Icons/Edit.gif" title="Edit" width="12" height="11" hspace="2" vspace="0" border="0"></asp:LinkButton>
				        </ItemTemplate>  
                     </asp:TemplateField>                                             
                </Columns>
                  <AlternatingRowStyle  CssClass="gridRow" />
                 
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
                 </asp:GridView>
             <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.HolidayListing" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
            </asp:Panel>
            				      
            <asp:Panel runat="server" ID="pnlConfirm" Width="750px" style="display:none;">
                <input type="hidden" id="hdnAddEditDateID" runat="server" value="" />
                <div>
                    <table>
                        <tr>
                            <td width="170" height="18" class="formTxt" valign="bottom">Holiday Date</td>
                            <td width="170" height="18" class="formTxt" valign="bottom">Holiday Caption</td>
                            <td width="250" height="18" class="formTxt" valign="bottom">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="168" height="18" valign="top"><asp:TextBox runat="server" ID="txtDate" CssClass="formFieldGrey width120" ></asp:TextBox>                                
                                <img alt="Click to Select" src="Images/calendar.gif" id="btnBeginDate" style="cursor:pointer; vertical-align:top;" />		                               
                                <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnBeginDate TargetControlID="txtDate" ID="calBeginDate" runat="server" ></cc1:CalendarExtender></td>
                            <td height="18" valign="top"><asp:TextBox runat="server" ID="txtHolidayTxt"  CssClass="formFieldGrey width150" ></asp:TextBox></td>
                            <td height="18" valign="top"><asp:CheckBox ID="chkbxIsDeleted" runat="server" Text="Is Deleted" CssClass="formTxt" /></td>
                        </tr>
                    </table>
                    
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;width:60px;float:left;"> 
                            <a class="buttonText cursorHand" onclick='javascript:validate_required_Holiday("ctl00_ContentPlaceHolder1_txtDate","Please enter a date")' id="ancSubmit" style="line-height:20px; vertical-align:middle;"><strong>Submit</strong></a>
                        </div>
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;width:60px;float:left;margin-left:20px;"> 
                                <a class="buttonText cursorHand" runat="server" id="ancCancel" onclick="javascript:AddHoliday('Listing')" style="vertical-align:middle;line-height:20px;"><strong>Cancel</strong></a>
                        </div> 
                       <div style="width:0px; height:0px; visibility:hidden"> <asp:Button ID="hdnButton" runat="server"   Height="0" Width="0" CausesValidation="false" />
                        <asp:Button ID="hdnButtonCancel" runat="server" Height="0" Width="0" CausesValidation="false" />
                        </div>
                </div>								
            </asp:Panel>          
            
			
          				</td><td width="10">&nbsp;</td></tr></table>			 
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      
      
      </asp:Content>