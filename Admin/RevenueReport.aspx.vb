﻿Imports System.IO
Imports System.Linq

Public Class RevenueReport
    Inherits System.Web.UI.Page
    Protected WithEvents UCMenuMainAdmin As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents lnkBackToListing As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents tdBackToListing As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lnkExportReportExcel As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lnkExportReportPdf As System.Web.UI.WebControls.LinkButton

    Public Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
            Case ApplicationSettings.CountryUK
                ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
        End Select

        If Not IsPostBack Then
            ddMonth.SelectedIndex = ddMonth.Items.IndexOf(ddMonth.Items.FindByValue(CType(Now.Month, String)))
            ddYear.SelectedIndex = ddYear.Items.IndexOf(ddYear.Items.FindByValue(CType(Now.Year, String)))
        End If
    End Sub
    Private Sub lnkBtnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnGenerateReport.Click
        generateReport()
    End Sub
    Private Sub generateReport()
        Dim languageCode As String = ""
        ErrLabel.Visible = False
        Dim bizDivId As Integer
        Dim dayinm As Integer
        dayinm = Date.DaysInMonth(CType(ddYear.Items(ddYear.SelectedIndex).Value, Integer), CType(ddMonth.Items(ddMonth.SelectedIndex).Value, Integer))
        Dim SelMonth, SelYear, EndDate, ReportType, dtStart, dtEnd As String
        SelMonth = ddMonth.Items(ddMonth.SelectedIndex).Value.ToString
        SelYear = ddYear.Items(ddYear.SelectedIndex).Value.ToString
        EndDate = dayinm.ToString
        bizDivId = Convert.ToInt32(ViewState("BizDivId"))
        ReportType = ddReportType.Items(ddReportType.SelectedIndex).Value.ToString
        dtStart = txtFromDate.Text.ToString.Trim
        dtEnd = txtToDate.Text.ToString.Trim

        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                languageCode = "de-DE"
            Case ApplicationSettings.CountryUK
                languageCode = "en-GB"
        End Select

        Dim ds As New DataSet
        ds = ws.WSStandards.GetRevenueReport(SelMonth, SelYear, EndDate, ReportType, bizDivId, dtStart, dtEnd)
        Session("RevenueReport") = ds

        If ds.Tables.Count > 0 Then

            lnkBtnExportToExcel.Visible = True

            pnlShowRecords.Visible = True
            pnlNoRecords.Visible = False
            pnlInitialize.Visible = False

            pnlRevenueNoRecords.Visible = False
            pnlCreditNoteNoRecords.Visible = False
            pnlDebitNoteNoRecords.Visible = False
            pnlUpsellNoRecords.Visible = False
            pnlDiscountNoRecords.Visible = False
            pnlManualAdjustmentsNoRecords.Visible = False


            If ds.Tables(0).Rows.Count > 0 Then
                rptRevenue.DataSource = ds.Tables(0)
                rptRevenue.DataBind()

                Dim dt As DataTable
                dt = ds.Tables(0)

                Dim sumTotalSales As Decimal = Math.Round(dt.AsEnumerable().Sum(Function(row) row.Field(Of Decimal)("Total Net Invoice value")), 2)
                Dim sumTotalCostofSales As Decimal = Math.Round(dt.AsEnumerable().Sum(Function(row) row.Field(Of Decimal)("Total Net Platform value")), 2)
                Dim sumTotalProfit As Decimal = Math.Round(dt.AsEnumerable().Sum(Function(row) row.Field(Of Decimal)("Total Profit")), 2)
                Dim sumMargin As Decimal
                If sumTotalProfit > 0.00 And sumTotalProfit > 0.00 Then
                    sumMargin = Math.Round((sumTotalProfit * 100) / sumTotalSales, 2)
                Else
                    sumMargin = 0.00
                End If

                Dim FooterTemplate As Control = rptRevenue.Controls(rptRevenue.Controls.Count - 1).Controls(0)

                Dim lblTotalSales As Label = TryCast(FooterTemplate.FindControl("lblTotalSales"), Label)
                Dim lblTotalCostofSales As Label = TryCast(FooterTemplate.FindControl("lblTotalCostofSales"), Label)
                Dim lblTotalProfit As Label = TryCast(FooterTemplate.FindControl("lblTotalProfit"), Label)
                Dim lblTotalMargin As Label = TryCast(FooterTemplate.FindControl("lblTotalMargin"), Label)

                lblTotalSales.Text = sumTotalSales
                lblTotalCostofSales.Text = sumTotalCostofSales
                lblTotalProfit.Text = sumTotalProfit
                lblTotalMargin.Text = sumMargin

            Else
                pnlRevenueNoRecords.Visible = True

            End If

            If ds.Tables.Count > 1 Then
                If ds.Tables(1).Rows.Count > 0 Then
                    rptCreditNote.DataSource = ds.Tables(1)
                    rptCreditNote.DataBind()

                    Dim dt As DataTable
                    dt = ds.Tables(1)
                    Dim sumCreditValue As Decimal = Math.Round(dt.AsEnumerable().Sum(Function(row) row.Field(Of Decimal)("Total Net Invoice value")), 2)
                    Dim FooterTemplate As Control = rptCreditNote.Controls(rptCreditNote.Controls.Count - 1).Controls(0)
                    Dim lblTotalCreditValue As Label = TryCast(FooterTemplate.FindControl("lblTotalCreditValue"), Label)
                    lblTotalCreditValue.Text = sumCreditValue

                Else
                    pnlCreditNoteNoRecords.Visible = True

                End If
            End If

            If ds.Tables.Count > 2 Then
                If ds.Tables(2).Rows.Count > 0 Then
                    rptDebitNote.DataSource = ds.Tables(2)
                    rptDebitNote.DataBind()

                    Dim dt As DataTable
                    dt = ds.Tables(2)
                    Dim sumDebitValue As Decimal = Math.Round(dt.AsEnumerable().Sum(Function(row) row.Field(Of Decimal)("Total Net Invoice value")), 2)
                    Dim FooterTemplate As Control = rptDebitNote.Controls(rptDebitNote.Controls.Count - 1).Controls(0)
                    Dim lblTotalDebitValue As Label = TryCast(FooterTemplate.FindControl("lblTotalDebitValue"), Label)
                    lblTotalDebitValue.Text = sumDebitValue

                Else
                    pnlDebitNoteNoRecords.Visible = True

                End If
            End If

            If ds.Tables.Count > 3 Then
                If ds.Tables(3).Rows.Count > 0 Then
                    rptUpsell.DataSource = ds.Tables(3)
                    rptUpsell.DataBind()

                    Dim dt As DataTable
                    dt = ds.Tables(3)
                    Dim sumUpSellPrice As Decimal = Math.Round(dt.AsEnumerable().Sum(Function(row) row.Field(Of Decimal)("UpSellPrice")), 2)
                    Dim FooterTemplate As Control = rptUpsell.Controls(rptUpsell.Controls.Count - 1).Controls(0)
                    Dim lblTotalUpsellAmount As Label = TryCast(FooterTemplate.FindControl("lblTotalUpsellAmount"), Label)
                    lblTotalUpsellAmount.Text = sumUpSellPrice

                Else
                    pnlUpsellNoRecords.Visible = True

                End If
            End If

            If ds.Tables.Count > 4 Then
                If ds.Tables(4).Rows.Count > 0 Then
                    rptDiscount.DataSource = ds.Tables(4)
                    rptDiscount.DataBind()

                    Dim dt As DataTable
                    dt = ds.Tables(4)
                    Dim sumDiscount As Decimal = Math.Round(dt.AsEnumerable().Sum(Function(row) row.Field(Of Decimal)("Discount")), 2)
                    Dim FooterTemplate As Control = rptDiscount.Controls(rptDiscount.Controls.Count - 1).Controls(0)
                    Dim lblTotalDsicountAmount As Label = TryCast(FooterTemplate.FindControl("lblTotalDsicountAmount"), Label)
                    lblTotalDsicountAmount.Text = sumDiscount

                Else
                    pnlDiscountNoRecords.Visible = True

                End If
            End If

            If ds.Tables.Count > 5 Then
                If ds.Tables(5).Rows.Count > 0 Then
                    rptManualAdjustments.DataSource = ds.Tables(5)
                    rptManualAdjustments.DataBind()

                    Dim dt As DataTable
                    dt = ds.Tables(5)
                    Dim sumSumDebit As Decimal = Math.Round(dt.AsEnumerable().Sum(Function(row) row.Field(Of Decimal)("SumDebit")), 2)
                    Dim FooterTemplate As Control = rptManualAdjustments.Controls(rptManualAdjustments.Controls.Count - 1).Controls(0)
                    Dim lblTotalAdjustmentAmount As Label = TryCast(FooterTemplate.FindControl("lblTotalAdjustmentAmount"), Label)
                    lblTotalAdjustmentAmount.Text = sumSumDebit

                Else
                    pnlManualAdjustmentsNoRecords.Visible = True
                End If
            End If

        Else
            'No records at all
            pnlNoRecords.Visible = True
            pnlShowRecords.Visible = False
            lnkBtnExportToExcel.Visible = False
            pnlInitialize.Visible = False
        End If


    End Sub

    Private Sub lnkBtnExportToExcel_Click(sender As Object, e As EventArgs) Handles lnkBtnExportToExcel.Click
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=RepeaterExport.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        Dim tb As New Table()
        Dim tr1 As New TableRow()
        Dim cell1 As New TableCell()
        cell1.Controls.Add(pnlShowRecords)
        tr1.Cells.Add(cell1)
        Dim cell2 As New TableCell()
        cell2.Text = "&nbsp;"
        Dim tr2 As New TableRow()
        tr2.Cells.Add(cell2)
        tb.Rows.Add(tr1)
        tb.Rows.Add(tr2)
        tb.RenderControl(hw)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub
End Class