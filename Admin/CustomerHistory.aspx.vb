﻿Public Class CustomerHistory
    Inherits System.Web.UI.Page

    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then

            If Not IsNothing(Request("WOID")) Then
                ViewState("WOID") = Request("WOID")
            Else
                ViewState("WOID") = 0
            End If
            gvCustomerHistory.PageSize = ApplicationSettings.GridViewPageSize

            ViewState!SortExpression = "WOID ASC"
            gvCustomerHistory.Sort(ViewState!SortExpression, SortDirection.Ascending)
            PopulateGrid()
        End If
    End Sub
    Public Function GetLinks(ByVal parambizDivId As Object, ByVal companyId As Object, ByVal contactId As Object, ByVal SupplierCompanyName As String, ByVal Status As String, ByVal WorkOrderID As String) As String
        Dim link As String = ""
        Dim contactType As ApplicationSettings.ContactType = ApplicationSettings.ContactType.approvedSupplier
        Dim classId As String
        Dim statusId As String
        classId = ApplicationSettings.RoleSupplierID
        statusId = ApplicationSettings.StatusApprovedCompany

        Dim bizDivId = parambizDivId

        link &= "<a class='footerTxtSelected ' href='companyprofile.aspx?sender=CustomerHistory&bizDivId=" & bizDivId & "&WorkOrderID=" & WorkOrderID & "&WOID=" & ViewState("WOID") & "&contactType=" & contactType & "&companyid=" & companyId & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&mode=" & Status & "&txtWorkorderID=" & Request("txtWorkorderID") & "&ps=" & gvCustomerHistory.PageSize & "&pn=" & gvCustomerHistory.PageIndex & "&sc=" & gvCustomerHistory.SortExpression & "&so=" & gvCustomerHistory.SortDirection & "&CompID=" & companyId & "&FromDate=''" & "&ToDate=''" & "'>" & SupplierCompanyName & "</a>"

        Return link

    End Function
    Public Function getHistoryLinks(ByVal WOID As String, ByVal WOStaged As String, ByVal WorkOrderID As String, ByVal BuyerCompID As Integer, ByVal BuyerContactID As Integer, ByVal SupCompID As Integer, ByVal Status As String, ByVal BuyerAccept As Integer, ByVal WholesalePrice As Integer, ByVal param As String) As String
        Dim lnk As String
        lnk = ""
        lnk = lnk & "<a href=AdminWOHistory.aspx?WOID=" & WOID & "&WorkOrderID=" & WorkOrderID & "&txtWorkorderID=" & Request("txtWorkorderID") & "&Group=" & Status & "&CompanyID=" & BuyerCompID & "&ContactID=" & BuyerContactID & "&" & param & " runat='server' id='WOHistory'><img src='Images/Icons/ViewHistory.gif' alt='Work Order History' hspace='2' vspace='0' border='0'></a>"
        Return lnk
    End Function
    Public Function GetActionIcon(ByVal NtCnt As Integer, ByVal Rating As Integer) As String
        Dim ActionIcon As String
        ActionIcon = "~/Images/Icons/Action.gif"
        If (NtCnt > 0 And Rating = 2) Then
            ActionIcon = "~/Images/Icons/Action-note.gif"
        ElseIf (NtCnt > 0 And Rating = 0) Then
            ActionIcon = "~/Images/Icons/Action-note-neutral.gif"
        ElseIf (NtCnt > 0 And Rating = 1) Then
            ActionIcon = "~/Images/Icons/Action-note-positive.gif"
        ElseIf (NtCnt > 0 And Rating = -1) Then
            ActionIcon = "~/Images/Icons/Action-note-negative.gif"
        ElseIf (NtCnt = 0 And Rating = 0) Then
            ActionIcon = "~/Images/Icons/Action-neutral.gif"
        ElseIf (NtCnt = 0 And Rating = 1) Then
            ActionIcon = "~/Images/Icons/Action-positive.gif"
        ElseIf (NtCnt = 0 And Rating = -1) Then
            ActionIcon = "~/Images/Icons/Action-negative.gif"
        End If
        Return ActionIcon
    End Function
    Public Function GetUserNotesDetail(ByVal WOID As Integer) As DataView
        Dim dv As DataView
        If Not IsNothing(HttpContext.Current.Items("tblWOUserNotes")) Then
            Dim dt As DataTable = HttpContext.Current.Items("tblWOUserNotes")
            dv = dt.DefaultView
            dv.RowFilter = "WOID = " & WOID
            Return dv
        Else
            Return dv
        End If
    End Function
    Private Sub gvCustomerHistory_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCustomerHistory.PreRender
        Me.gvCustomerHistory.Controls(0).Controls(Me.gvCustomerHistory.Controls(0).Controls.Count - 1).Visible = True
        If ViewState("rowCount") < 1 Then
            Return
        End If
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

            End If
        End If
    End Sub
    Protected Sub gvCustomerHistory_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvCustomerHistory.RowDataBound
        MakeGridViewHeaderClickable(gvCustomerHistory, e.Row)
        If e.Row.RowType = DataControlRowType.DataRow Then
            If (DataBinder.Eval(e.Row.DataItem, "OriginalWOID") = "1") Then
                Dim col As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#FFFFBC")
                e.Row.BackColor = col
            Else
                Dim OriginalCol As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#EDEDED")
                e.Row.BackColor = OriginalCol
                For Each tableCell As TableCell In e.Row.Cells
                    tableCell.ForeColor = Drawing.Color.Black
                Next
            End If
        End If
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvCustomerHistory.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvCustomerHistory, e.Row, Me)
        End If
    End Sub
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)
        'Dim btnExport As HtmlAnchor = CType(gvPagerRow.FindControl("btnExport"), HtmlAnchor)

        'btnExport.HRef = prepareExportToExcelLink()

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    Private Sub gvCustomerHistory_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCustomerHistory.RowCommand

    End Sub

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvCustomerHistory.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet

        ds = ws.WSWorkOrder.GetCustomerHistory(sortExpression, startRowIndex, maximumRows, ViewState("WOID"))


        HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0)("TotalRecords")
        ViewState("rowCount") = ds.Tables(1).Rows(0)("TotalRecords")
        HttpContext.Current.Items("tblWOUserNotes") = ds.Tables(2)
        lblLocation.Text = ds.Tables(0).Rows(0)("Location")
        lblPostcode.Text = ds.Tables(0).Rows(0)("Postcode")
        lblCustomer.Text = ds.Tables(0).Rows(0)("CustomerName")
        Return ds
    End Function
    Public Sub PopulateGrid()

        gvCustomerHistory.DataBind()

    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub
    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function
    Private Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        lnkBackToListing.HRef = getBackToListingLink()
    End Sub
    Public Function getBackToListingLink() As String
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender")
                Case "UCWODetails"
                    link = "~\AdminWODetails.aspx?"
                    link &= "WOID=" & Request("WOID")
                    link &= "&ContactID=" & Request("ContactID")
                    link &= "&CompanyID=" & Request("CompanyID")
                    link &= "&Group=" & Request("Group")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&mode=" & Request("Group")
                    link &= "&CompID=" & Request("CompID")
                    link &= "&sender=" & "UCWOsListing"
               Case "SearchWO"
                    link = "SearchWOs.aspx?"
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&SrcWorkorderID=" & Request("txtWorkorderID")
                    link &= "&SrcCompanyName=" & Request("CompanyName")
                    link &= "&SrcKeyword=" & Request("KeyWord")
                    link &= "&SrcPONumber=" & Request("PONumber")
                    link &= "&SrcReceiptNumber=" & Request("ReceiptNumber")
                    link &= "&SrcPostCode=" & Request("PostCode")
                    link &= "&SrcDateStart=" & Request("DateStart")
                    link &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
                    link &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")
                    link &= "&sender=AdminWODetails"
                Case Else
                    Return ""
            End Select
        End If
        Return link
    End Function
End Class