<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UpdateRating.aspx.vb" Inherits="Admin.UpdateRating" %>
<%@ Register Assembly="WebLibrary" Namespace="WebLibrary" TagPrefix="lib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">  
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Admin/Templates/AdminTemplate.dwt" codeOutsideHTMLIsLocked="false" -->
<head id="Head1" runat=server>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>OrderWork :</title>
<!-- InstanceEndEditable --><!-- InstanceParam name="opRgNewsletter" type="boolean" value="true" --><!-- InstanceParam name="opRgClient" type="boolean" value="false" --><!-- InstanceParam name="opRgSupplier" type="boolean" value="false" --><!-- InstanceParam name="opLogo" type="boolean" value="true" --><!-- InstanceParam name="opHomeFlash" type="boolean" value="false" -->
<script language="JavaScript" type="text/JavaScript" src="JS/Scripts.js"></script>
<script language="javascript" src="JS/Scripts.js"  type="text/javascript"></script>
<!-- InstanceParam name="OpMenuRedTopCurves" type="boolean" value="true" --><!-- InstanceParam name="OpMenuRedBtmCurve" type="boolean" value="true" --><!-- InstanceParam name="OpMenuOrangeTopCurve" type="boolean" value="false" --><!-- InstanceParam name="OpMenuBtmOrangeCurve" type="boolean" value="false" --><!-- InstanceParam name="onLoad" type="text" value="" --><!-- InstanceParam name="OpQLRegion" type="boolean" value="false" --><!-- InstanceParam name="OpContentTopCurve" type="boolean" value="true" --><!-- InstanceParam name="OpUCMenu" type="boolean" value="false" --><!-- InstanceParam name="OpContentTopMargin" type="boolean" value="false" --><!-- InstanceParam name="OpAddSpecialist" type="boolean" value="true" --><!-- InstanceParam name="OpAddLocations" type="boolean" value="true" --><!-- InstanceParam name="OpLogoMessage" type="boolean" value="false" --><!-- InstanceParam name="SpacNavAdminHome" type="boolean" value="false" --><!-- InstanceParam name="OpUCSubMenu" type="boolean" value="false" --><!-- InstanceParam name="OpRgLogin" type="boolean" value="false" --><!-- InstanceParam name="OpRgSupplierLogin" type="boolean" value="false" --><!-- InstanceParam name="OpRgClientLogin" type="boolean" value="false" -->
<link href="Styles/OrderWork.css" rel="stylesheet" type="text/css">
<link href="Styles/OrderWork.css" rel="stylesheet" type="text/css">
<link href="Styles/Menu.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<style>

#divMenu {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	line-height: 12px;
	font-weight: normal;
	text-decoration: none;
	width: 100%;
	background-position: right top;
	background-repeat: no-repeat;
	border-top-width: 1px;
	border-top-style: solid; 
	border-top-color: #CECECE;
	background-color: #993366;
}
.divContentTopRed{
	width: 100%;
	background-color: #993366;
	background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
	height: 5px;	
}

</style>
<!-- InstanceEndEditable --><!-- InstanceParam name="opAccountSumm" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddUser" type="boolean" value="false" --><!-- InstanceParam name="OpRgCLAddLOcation" type="boolean" value="false" --><!-- InstanceParam name="OPWOSummary" type="boolean" value="true" --><!-- InstanceParam name="RgOpBtnNewWO" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddFunds" type="boolean" value="false" --><!-- InstanceParam name="opMenuRegion" type="boolean" value="true" --><!-- InstanceParam name="opContentTopRedCurve" type="boolean" value="false" --><!-- InstanceParam name="opUsersProfile" type="boolean" value="false" --><!-- InstanceParam name="opCompanyProfile" type="boolean" value="true" --><!-- InstanceParam name="opUsersListing" type="boolean" value="false" --><!-- InstanceParam name="opLocations" type="boolean" value="false" --><!-- InstanceParam name="opReferences" type="boolean" value="false" --><!-- InstanceParam name="opPreferences" type="boolean" value="false" --><!-- InstanceParam name="opUsers" type="boolean" value="false" --><!-- InstanceParam name="width" type="text" value="985" --><!-- InstanceParam name="opComments" type="boolean" value="false" --><!-- InstanceParam name="OpRgScriptManager" type="boolean" value="true" --><!-- InstanceParam name="OpFavSuppliers" type="boolean" value="false" --><!-- InstanceParam name="OpFavSuppliersLink" type="boolean" value="false" -->
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

<!-- InstanceParam name="OpAutoMatch" type="boolean" value="false" -->
</head>
<body style="background-color:#F4F5EF;" >
<form id="Form1" method="post" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<div style="font-size:12px;font-family:Arial;padding-left:17px;"><b>Rating of workorder:</b><br />
    <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnPositive" Enabled="false"  runat="server" Text="" />Positive
    <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNeutral" Enabled="false" runat="server"  Text="Neutral"/>
    <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNegative" Enabled="false" runat="server" Text="Negative" /><br /><br />
</div>                       
</form>
</body>
<!-- InstanceEnd --></html>