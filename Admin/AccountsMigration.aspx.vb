
Imports WebLibrary
Partial Public Class AccountsMigration
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)

        If Not IsPostBack Then
            ' Code to Show/Hide control for OW Admin
            'Dim pagename As String
            'pagename = CType(CType(Page.Request.Url, Object), System.Uri).AbsolutePath()
            'pagename = pagename.Substring(pagename.LastIndexOf("/") + 1, (pagename.Length - 1) - pagename.LastIndexOf("/"))
            'Dim dvpriv As DataView = UCMSCompanyProfile.GetContact(Me, Session("ContactBizDivID"), True, True, Session("ContactCompanyID")).Tables(8).Copy.DefaultView
            'dvpriv.RowFilter = "PageName Like('" & pagename & "') "
            'Security.GetPageControls(Page, dvpriv)

        End If
    End Sub

    Private Sub btnCancel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.ServerClick
        pnlAccountsDetails.Visible = False
        tblBtnSearch.Visible = True
        txtCompanyName.Text = ""
        txtCompanyName.Enabled = True
        chkFreesat.Checked = False
        chkElex.Checked = False
        chkIT.Checked = False
        chkpc.Checked = False
        txtVATNo.Text = ""
    End Sub

    <Services.WebMethod(), Script.Services.ScriptMethod()> _
      Public Shared Function GetCompanyName(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("ClientCompanyName", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function

    Public Sub btnConfirmClick_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmClick.ServerClick
        Dim CompanyName As String = txtCompanyName.Text.Trim
        Dim VATNo As String = txtVATNo.Text.Trim
        Dim BizID As Integer = ApplicationSettings.OWUKBizDivId
        Dim CompanyId = hdnContactID.Value

        Dim Freesat As Boolean
        Dim Elex As Boolean
        Dim IT As Boolean
        Dim pc As Boolean

        If chkFreesat.Checked = True Then
            Freesat = 1
        Else
            Freesat = 0
        End If

        If chkElex.Checked = True Then
            Elex = 1
        Else
            Elex = 0
        End If

        If chkIT.Checked = True Then
            IT = 1
        Else
            IT = 0
        End If

        If chkpc.Checked = True Then
            pc = 1
        Else
            pc = 0
        End If


        Dim Success As Integer
        Success = ws.WSContact.MigrateAccounts(BizID, CompanyId, CompanyName, VATNo, Freesat, Elex, IT, pc, Session("UserID"))

        If Success = 1 Then
            pnlMigrateAccount.Visible = False
            pnlMigrateSuccessful.Visible = True
        Else
            pnlMigrateAccount.Visible = True
            pnlMigrateSuccessful.Visible = False
        End If
    End Sub

    Private Sub btnSearch1_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch1.ServerClick
        pnlAccountsDetails.Visible = True
        tblBtnSearch.Visible = False
        txtCompanyName.Enabled = False
    End Sub
End Class