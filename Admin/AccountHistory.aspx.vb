﻿Public Class AccountHistory
    Inherits System.Web.UI.Page

    Protected WithEvents ObjectDataSource1 As ObjectDataSource

    ''' <summary>
    ''' Shared wsobject to access web service webmethods
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)

            ViewState!SortExpression = "ModifiedDate"
            gvAccountHistory.PageSize = hdnPageSize.Value
            gvAccountHistory.Sort(ViewState!SortExpression, SortDirection.Descending)
            PopulateGrid()
            gvAccountHistory.PageIndex = 0
            lnkBackToListing.HRef = getBackToListingLink()
        End If
    End Sub
    Public Function getBackToListingLink() As String
        Dim link As String = ""
        Dim ds As DataSet
        Dim ContactId As Integer
        ContactId = 0
        If (Not IsNothing(Request("ContactId"))) Then
            ContactId = Request("ContactId")
        End If
        If (ContactId <> 0) Then
            lblBacktoText.Text = "Back to User Profile"
            link = "~\SpecialistsForm.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&ContactId=" & Request("ContactId")
        Else
            lblBacktoText.Text = "Back to Company Profile"
            If ((Request("sender") = "buyer") Or (Request("classid") = ApplicationSettings.RoleClientID)) Then
                link = "~\CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            Else
                link = "~\CompanyProfile.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            End If
        End If
        Return link
    End Function
    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvAccountHistory.DataBind()
        End If
    End Sub



#Region "Grid Functions"

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        Dim ContactId As Integer
        ContactId = 0
        If (Not IsNothing(Request("ContactId"))) Then
            ContactId = Request("ContactId")
        End If
        If (ContactId <> 0) Then
            ds = ws.WSContact.GetUserAccountHistory(sortExpression, startRowIndex, maximumRows, ContactId)
        Else
            ds = ws.WSContact.GetAccountHistory(sortExpression, startRowIndex, maximumRows, Request("CompanyID"))
        End If
        If (ds.Tables(0).Rows.Count > 0) Then
            ViewState("rowCount") = ds.Tables(0).Rows(0).Item("TotalRecs")
        Else
            ViewState("rowCount") = 0
        End If
        Return ds
    End Function

    ''' <summary>
    ''' ObjectDataSource Controls select Count Method which returns the row count for the listing 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvAccountHistory.RowDataBound
        MakeGridViewHeaderClickable(gvAccountHistory, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvAccountHistory.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvAccountHistory, e.Row, Me)
        End If
    End Sub

    Private Sub gvAccountHistory_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAccountHistory.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True


            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True


            End If
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)



        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        gvAccountHistory.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

#End Region

    Public Function GetChangeDetails(ByVal ChangeDetails As String, ByVal BankAccountNameOldValue As String, ByVal BankAccountNameNewValue As String, ByVal BankAccountNoOldValue As String, ByVal BankAccountNoNewValue As String, ByVal BankSortCodeOldValue As String, ByVal BankSortCodeNewValue As String) As String
        Dim link As String = ""

        If (BankAccountNameOldValue <> "") Then
            If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                ' Display plain text
                ChangeDetails = ChangeDetails.Replace(BankAccountNameOldValue, Encryption.Decrypt(BankAccountNameOldValue))
            Else
                ' Show masked data
                Dim maskedAccountName As String = Encryption.Decrypt(BankAccountNameOldValue)
                If maskedAccountName <> "" Then
                    'maskedAccountName = maskedAccountName.Substring(0, maskedAccountName.Length - 5) + "XX-XX"
                    maskedAccountName = maskedAccountName.Substring(0, maskedAccountName.Length - (maskedAccountName.Length - 2)) + "XXX"
                End If
                ChangeDetails = ChangeDetails.Replace(BankAccountNameOldValue, maskedAccountName)
            End If
        End If
        If (BankAccountNameNewValue <> "") Then
            If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                ' Display plain text
                ChangeDetails = ChangeDetails.Replace(BankAccountNameNewValue, Encryption.Decrypt(BankAccountNameNewValue))
            Else
                ' Show masked data
                Dim maskedAccountName As String = Encryption.Decrypt(BankAccountNameNewValue)
                If maskedAccountName <> "" Then
                    'maskedAccountName = maskedAccountName.Substring(0, maskedAccountName.Length - 5) + "XX-XX"
                    maskedAccountName = maskedAccountName.Substring(0, maskedAccountName.Length - (maskedAccountName.Length - 2)) + "XXX"
                End If
                ChangeDetails = ChangeDetails.Replace(BankAccountNameNewValue, maskedAccountName)
            End If

        End If

        If (BankAccountNoOldValue <> "") Then
            If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                ' Display plain text
                ChangeDetails = ChangeDetails.Replace(BankAccountNoOldValue, Encryption.Decrypt(BankAccountNoOldValue))
            Else
                ' Show masked data
                Dim maskedAccountNo As String = Encryption.Decrypt(BankAccountNoOldValue)
                If maskedAccountNo <> "" Then
                    'maskedAccountName = maskedAccountName.Substring(0, maskedAccountName.Length - 5) + "XX-XX"
                    maskedAccountNo = maskedAccountNo.Substring(0, maskedAccountNo.Length - 4) + "XXXX"
                End If
                ChangeDetails = ChangeDetails.Replace(BankAccountNoOldValue, maskedAccountNo)
            End If
        End If

        If (BankAccountNoNewValue <> "") Then
            If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                ' Display plain text
                ChangeDetails = ChangeDetails.Replace(BankAccountNoNewValue, Encryption.Decrypt(BankAccountNoNewValue))
            Else
                ' Show masked data
                Dim maskedAccountNo As String = Encryption.Decrypt(BankAccountNoNewValue)
                If maskedAccountNo <> "" Then
                    'maskedAccountName = maskedAccountName.Substring(0, maskedAccountName.Length - 5) + "XX-XX"
                    maskedAccountNo = maskedAccountNo.Substring(0, maskedAccountNo.Length - 4) + "XXXX"
                End If
                ChangeDetails = ChangeDetails.Replace(BankAccountNoNewValue, maskedAccountNo)
            End If
        End If

        If (BankSortCodeOldValue <> "") Then
            If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                ' Display plain text
                ChangeDetails = ChangeDetails.Replace(BankSortCodeOldValue, Encryption.Decrypt(BankSortCodeOldValue))
            Else
                ' Show masked data
                Dim maskedSortCode As String = Encryption.Decrypt(BankSortCodeOldValue)
                If maskedSortCode <> "" Then
                    maskedSortCode = maskedSortCode.Substring(0, maskedSortCode.Length - 5) + "XX-XX"
                End If
                ChangeDetails = ChangeDetails.Replace(BankSortCodeOldValue, maskedSortCode)
            End If
        End If

        If (BankSortCodeNewValue <> "") Then
            If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                ' Display plain text
                ChangeDetails = ChangeDetails.Replace(BankSortCodeNewValue, Encryption.Decrypt(BankSortCodeNewValue))
            Else
                ' Show masked data
                Dim maskedSortCode As String = Encryption.Decrypt(BankSortCodeNewValue)
                If maskedSortCode <> "" Then
                    maskedSortCode = maskedSortCode.Substring(0, maskedSortCode.Length - 5) + "XX-XX"
                End If
                ChangeDetails = ChangeDetails.Replace(BankSortCodeNewValue, maskedSortCode)
            End If
        End If

        link &= ChangeDetails

        Return link

    End Function

End Class