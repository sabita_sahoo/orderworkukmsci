﻿<%@ Page Title="OrderWork : List of Engineers" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="UnapprovedEngineers.aspx.vb" Inherits="Admin.UnapprovedEngineers" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


              <div id="divContent">

         

            

        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>

        

             <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">

         

         <tr>

            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->

            <input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />

            <input id="hdnContactType" type="hidden" name="hdnContactType" runat="server">                   

            <input id="hdnPageNo"  value="1" type="hidden" name="hdnPageNo" runat="server">

                  <input id="hdnPageSize" value="25" type="hidden" name="hdnPageSize" runat="server">

                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate> 

                                <table width="100%" cellpadding="0" cellspacing="0">

                                    <tr>

                                    <td width="10px"></td>

                                    <td><asp:Label ID="lblMessage" CssClass="HeadingRed"  runat="server" Text="List of Engineers"></asp:Label></td>

                                    </tr>

                                    </table>

                              <table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:10px;>

                                <tr>

                                 <td width="10px"></td>

                                 <td width="300"><table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-left:10px;">

                                     <tr>

                                       <td width="350">

                                       <%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
						            <uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact>

                                       </td>

                                      </tr>

                                    </table>

                                 </td>

                                 <td align="left" width="60px">
                                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                        <asp:LinkButton ID="btnView" causesValidation=False runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
                                    </div>

                               </td>

                               <td width="20px"></td>

                               <td align="left"></td>

            </tr>

         </table>

            <table >

            <tr>

            <td class="paddingL30 padB21" valign="top" padB21 paddingTop1>

            <asp:Label ID="lblMsg" runat="server" CssClass="bodytxtValidationMsg"></asp:Label>                

            </td>

            </tr>

            </table>                      

                        

                 

                                          

                  

                        

                               

                               

                  <hr style="background-color:#993366;height:5px;margin:0px;" />

                  

                    <asp:Panel ID="pnlListing" runat="server">    

                                <asp:GridView ID="gvContacts"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'

                         BorderColor="White"  dataKeyNames="ContactID" PagerSettings-Mode=NextPreviousFirstLast   PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          

               <EmptyDataTemplate>

               <table  width="100%" border="0" cellpadding="10" cellspacing="0">                            

                                    <tr>

                                          <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.

                                          </td>

                                    </tr>                   

                              </table>

                  

               </EmptyDataTemplate>

               <PagerTemplate>

                   

                  

                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">

                                    <tr>

                                          <%--<td runat="server" id="tdsetType1" width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">

                                            <TR>

                                                <TD class="txtListing" align="left"><strong>

                                                  <asp:CheckBox id="chkSelectAll" runat="server" onclick="CheckAll(this,'Check')" value="checkbox" Text="Select all" valign="middle"></asp:CheckBox>

                                                </strong> </TD>

                                                <TD width="5"></TD>

                                            </TR>

                                          </TABLE>                                  

                                          </td>

                                          

                                          <td runat="server"  id="tdApprove" width="70" align="left" class="txtWelcome paddingL10"><div id="divButton" style="width: 70px;  ">

                                                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                                           

                                                <asp:LinkButton OnClick="ApproveContact" class="buttonText" style="cursor:hand;" id="btnApprove"  runat=server><strong>Approve</strong></asp:LinkButton>

                               <cc2:ConfirmButtonExtender ID="ConfirmBtnApprove" TargetControlID="btnApprove" ConfirmText="Do you want to approve engineer?" runat="server">

                                </cc2:ConfirmButtonExtender> 

                                

                                                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>

                                          </div></td>               --%>                       

                                

                                          <td>

                                                <table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">

                                                  <tr>

                                                      <td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >

                                                            <TR>

                                                                  <td align="right" valign="middle">

                                                                    <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>

                                                                  </td>

                                                                  <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">

                                                                  <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                                                        <tr>

                                                                        <td align="right" width="36">

                                                                        <div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />

                                                                              </div>      

                                                                              <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />

                                                                              </div>                                                                                                                                             

                                                                        </td>

                                                                        <td width="50" align="center" style="margin-right:3px; ">                                                                              

                                                                              <asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">

                                                    </asp:DropDownList>

                                                                        </td>

                                                                        <td width="30" valign="bottom">

                                                                              <div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />

                                                                              </div>

                                                                              <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />

                                                                              </div>

                                                                        </td>

                                                                        </tr>

                                                                  </table>

                                                                  </td>

                                                            </TR>

                                                      </TABLE></td>

                                                  </tr>

                                                </table>

                                      </td>

                                    </tr>

                              </table> 

                   

        </PagerTemplate> 

               <Columns>  

               
              <%-- <asp:TemplateField>               

               <HeaderStyle CssClass="gridHdr" />

               <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="gridRow"/>

               <ItemTemplate>             

               <asp:CheckBox ID="Check" runat="server" ></asp:CheckBox>

                <input type=hidden runat=server id="hdnContactId"  value='<%#Container.DataItem("ContactID")%>'/>

               </ItemTemplate>

               </asp:TemplateField>               --%> 
                              
                <asp:TemplateField HeaderText="Name" HeaderStyle-CssClass= "gridHdr gridText" SortExpression="Name"  >         
                  <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" Width="8%"/>
                    <ItemTemplate>             
                        <%# "<a class=footerTxtSelected href=SpecialistsForm.aspx?ContactId=" & Container.DataItem("ContactID") & "&CompanyID=" & Container.DataItem("CompanyID") & "&ClassId=" & Container.DataItem("ClassId") & "&bizDivId=1&sender=UnapprovedEngineers>" & Container.DataItem("Name") & "</a>"%>
                    </ItemTemplate>
                </asp:TemplateField>  	
                		         
				<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="8%" HeaderStyle-CssClass="gridHdr gridText"  DataField="CompanyName" SortExpression="CompanyName" HeaderText="Company Name"/> 
                <asp:TemplateField HeaderText="Email" HeaderStyle-CssClass= "gridHdr gridText" SortExpression="Email"  >         
                  <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" Width="14%"/>
                    <ItemTemplate>             
                        <%#"<a class=footerTxtSelected href=mailto:" & Container.DataItem("Email") & ">" & Container.DataItem("Email") & "</a>"%>
                    </ItemTemplate>
                </asp:TemplateField>  	
                		
                <asp:TemplateField HeaderText="CV" HeaderStyle-CssClass= "gridHdr gridText"  >               
                <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Center CssClass="gridRow" Width="12%"/>
                    <ItemTemplate>             
                   <span class='txtGreySmallSelected'>
                              <a class='footerTxtSelected' href='<%# getlink(Container.DataItem("CVFilePath")) %>' target='_blank'>
                                                            <%# Container.DataItem("CVName")%></a>
                                                    
                        </span>
                    </ItemTemplate>
                </asp:TemplateField> 
                 <asp:TemplateField HeaderText="Proof of meeting" HeaderStyle-CssClass= "gridHdr gridText"  >               
                <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Center CssClass="gridRow" Width="12%"/>
                    <ItemTemplate>             
                   <span class='txtGreySmallSelected'>
                              <a class='footerTxtSelected' href='<%# getlink(Container.DataItem("POMFilePath")) %>' target='_blank'>
                                                            <%# Container.DataItem("POMName")%></a>
                                                    
                        </span>
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="CRC" HeaderStyle-CssClass= "gridHdr gridText"  >         
                  <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" Width="5%"/>
                    <ItemTemplate>             
                   <asp:Label ID="lblCRC" runat="server" Text='<%# IIF(Container.DataItem("CRC"), True,False)%>'>></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>  
                <asp:TemplateField HeaderText="UK Security Clearance" HeaderStyle-CssClass= "gridHdr gridText"  >         
                  <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" Width="5%"/>
                    <ItemTemplate>             
                   <asp:Label ID="lblUKSecurityClearance" runat="server" Text='<%# IIF(Container.DataItem("UKSecurityClearance"), True,False)%>'>></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>  
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="8%" HeaderStyle-CssClass="gridHdr gridText"  DataField="UKRightToWorkExpDate" HeaderText="Right to work in UK"/> 
                  <asp:TemplateField HeaderText="Right to work in UK" HeaderStyle-CssClass= "gridHdr gridText"  >               
                <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Center CssClass="gridRow" Width="12%"/>
                    <ItemTemplate>             
                   <span class='txtGreySmallSelected'>
                              <a class='footerTxtSelected' href='<%# getlink(Container.DataItem("RWUKFilePath")) %>' target='_blank'>
                                                            <%# Container.DataItem("RWUKName")%></a>
                                                    
                        </span>
                    </ItemTemplate>
                </asp:TemplateField> 
                  <asp:TemplateField HeaderText="CSCS" HeaderStyle-CssClass= "gridHdr gridText"  >         
                  <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" Width="5%"/>
                    <ItemTemplate>             
                   <asp:Label ID="lblCSCS" runat="server" Text='<%# IIF(Container.DataItem("CSCS"), True,False)%>'>></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>  
                 <asp:TemplateField HeaderText="CSCS" HeaderStyle-CssClass= "gridHdr gridText"  >               
                <HeaderStyle  CssClass="gridHdr"  />
                <ItemStyle Wrap=true HorizontalAlign=Center CssClass="gridRow" />
                    <ItemTemplate>             
                   <span class='txtGreySmallSelected'>
                              <a class='footerTxtSelected' href='<%# getlink(Container.DataItem("CSCSFilePath")) %>' target='_blank'>
                                                            <%# Container.DataItem("CSCSName")%></a>
                                                    
                        </span>
                    </ItemTemplate>
                </asp:TemplateField> 

                </Columns>

                 <AlternatingRowStyle CssClass=gridRow />

                 <RowStyle CssClass=gridRow />            

                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />

                <HeaderStyle  CssClass="gridHdr" HorizontalAlign=Left VerticalAlign=Top  Height="40px" />

                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />

        

              </asp:GridView>

              </asp:Panel>

      

              

                         </ContentTemplate>

                         <Triggers>

                            <asp:PostBackTrigger   ControlID="btnView"   />                       

                         </Triggers>

            

            </asp:UpdatePanel>

            

                <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">

                <ProgressTemplate >

                        <div class="gridText">

                               <img  align=middle src="Images/indicator.gif" />

                               <b>Fetching Data... Please Wait</b>

                        </div>   

                        </ProgressTemplate>

                </asp:UpdateProgress>

                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />

             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"

                SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.UnapprovedEngineers" SortParameterName="sortExpression">                

            </asp:ObjectDataSource>

                                            

                              

            <!-- InstanceEndEditable --></td>

                     

             

         </tr>

         </table>

      </div>
</asp:Content>

 
