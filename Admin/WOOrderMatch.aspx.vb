

Imports System
Imports System.Data
Imports System.Threading
Partial Public Class WOOrderMatch
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected WithEvents chkShowAll As CheckBox
    Protected WithEvents chkFavSupplier As CheckBox
    Protected WithEvents chkNearestTo As System.Web.UI.WebControls.CheckBox
    Protected WithEvents CRBCheckedYes As RadioButton
    Protected WithEvents UKSecurityClearanceYes As RadioButton

    Protected WithEvents ddlNoOfEmployees As DropDownList
    Protected WithEvents ddlSortBy As DropDownList
    Protected WithEvents ddlNearestTo As DropDownList

    Protected WithEvents lblInvalidPostCode As Label
    Private Delegate Sub PostCodeUpdate(ByVal dsPostCode As DataSet)
    'Protected WithEvents UCCompanyAccreditations As UCDualListBox
    Protected WithEvents UCAccreditations As UCAccreditationForSearch
    Protected WithEvents divNearestTo As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents trFindNearest, trAreaOfExp As System.Web.UI.HtmlControls.HtmlTableRow
    Private Delegate Sub AfterOrderMatch(ByVal SelectedIDs As String, ByVal WOID As Integer, ByVal dsEmailContacts As DataSet)

#Region "Declaration"

    ''' <summary>
    ''' Region Usercontrol
    ''' </summary>
    ''' <remarks></remarks>
    Protected WithEvents UCRegions As UCDualListBox
    '''<summary>
    '''UCAOE1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCAOE1 As UCTableAOE

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "" Then
            If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "0" Then
                hdnProductIds.Value = CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value
            Else
                hdnProductIds.Value = ""
            End If
        Else
            hdnProductIds.Value = ""
        End If
        ' Populate Selected value of Area of expertise
        UCAOE1.CombIds = hdnProductIds.Value
        UCAOE1.PopulateAOE()
        UCAOE1.PageName = "NoMainCatSel"

        If Not Page.IsPostBack Then
            ViewState("Country") = ApplicationSettings.Country
            ViewState("cacheKey") = "OrderMatch" & "-" & Me.Page.Session.SessionID
            Security.SecurePage(Page)
            UCAccreditations.Type = "AllAccreditations"
            UCAccreditations.Mode = "WOAccreditations"
            'Workorder ID
            If ApplicationSettings.Country = ApplicationSettings.CountryDE Then
                trFindNearest.Visible = False
                trAreaOfExp.Visible = False
            ElseIf ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                trFindNearest.Visible = True
                trAreaOfExp.Visible = True
            End If
            If Not IsNothing(Request("WOID")) Then
                ViewState("WOID") = Request("WOID")
                If Not IsNothing(Request("sender")) Then
                    If Request("sender") = "CompanyProfile" Or Request("sender") = "AdminWOListing" Then
                        processBackToListing()
                    Else
                        If (Request("WOID") <> "") Then
                            InitializeForm(Request("WOID").Trim)
                        Else
                            'If the query string does not have WOID then redirect to the listing page
                            Response.Redirect("AMContacts.aspx")
                        End If
                    End If
                Else
                    If (Request("WOID") <> "") Then
                        InitializeForm(Request("WOID").Trim)
                    Else
                        'If the query string does not have WOID then redirect to the listing page
                        Response.Redirect("AMContacts.aspx")
                    End If
                End If
            Else
                'If the query string does not have WOID then redirect to the listing page
                Response.Redirect("AMContacts.aspx")
            End If
        End If
    End Sub

#Region "Populate & Initialise"

    Public Sub PopulateGrid(Optional ByVal PageIndex As Integer = 0, Optional ByVal SortExpression As String = "", Optional ByVal SortDirection As String = "")
        Page.Validate()
        If Page.IsValid Then
            'setGridSettings()            
            If PageIndex > 0 Then
                gvSupplier.PageIndex = PageIndex
            Else
                gvSupplier.PageIndex = 0
            End If
            'Dim ds As New DataSet
            SelectFromDB(SortExpression, PageIndex)
            'gvSupplier.DataSource = ds.Tables(0)
            'gvSupplier.DataBind()
        End If
    End Sub

    ''' <summary>
    ''' Initialise the ordermatch form.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <remarks></remarks>
    Private Sub InitializeForm(ByVal WOID As Integer)
        ViewState("cacheKey") = "OrderMatch-Standards-WOID" & WOID & "-Session" & "-" & Page.Session.SessionID
        Dim dsLookup As DataSet
        Dim dsWorkOrder As DataSet
        Dim dvRegion As DataView

        'Populate WO Summary
        dsWorkOrder = ws.WSWorkOrder.MS_WOOrderMatchGetSummary(WOID)


        If dsWorkOrder.Tables("tblWOSummary").Rows.Count > 0 Then
            If Not IsNothing(Request("product")) Then
                UCAOE1.CombIds = hdnProductIds.Value
            Else
                If dsWorkOrder.Tables("tblWOSummary").Rows(0)("WOCategoryId") <> 0 Then
                    UCAOE1.CombIds = dsWorkOrder.Tables("tblWOSummary").Rows(0)("WOCategoryId")
                End If
            End If
            populateWOSummary(dsWorkOrder)
        End If
        UCAOE1.PopulateAOE()
        'Populate No of Employees drop down 
        CommonFunctions.PopulateNoOfEmployees(Page, ddlNoOfEmployees)
        If Not Request("noOfEmployees") Is Nothing Then
            Dim lItem As ListItem
            lItem = ddlNoOfEmployees.Items.FindByValue(Request("noOfEmployees"))
            If Not lItem Is Nothing Then
                ddlNoOfEmployees.SelectedValue = Request("noOfEmployees")
            End If
        End If
        'Populate the SkillsArea
        Dim dsXML As New DataSet()
        dsXML.ReadXml(MapPath("~/XML/WOFormStandards.xml"))
        'Dim dv_skills As DataView = dsXML.Tables("skillsarea").DefaultView
        'ddlSkillsArea.DataSource = dv_skills
        'ddlSkillsArea.DataBind()
        'Get Standards for the ordermatch page
        If Not IsNothing(Request("BizdivId")) Then
            If (Request("BizdivId") <> "") Then
                ViewState("BizDivId") = Request("BizdivId").Trim
                UCAOE1.BizDivId = getAOEBizDivIdFromRadioListBtn()
            End If
        End If
        dsLookup = CommonFunctions.OrderMatch_GetStandards(Page, ViewState("cacheKey"), ViewState("BizDivId"))

        'Populate Area Of Expertise

        'Populate regions
        'Read Querystring to fill up the search criteria - Pratik Trivedi
        If Not IsNothing(Request("region")) Then
            UCRegions.populateListBox(Nothing, dsLookup.Tables("tblRegionCodes").Copy.DefaultView, "RegionName", "SettingValue", "RegionID")
            'UCRegions.showSelected(Request("region").ToString)
        ElseIf (dsWorkOrder.Tables().Count > 1) Then
            'check if the table 1 has region id column. if yes then populate else donot. - PB
            If dsWorkOrder.Tables(1).Columns.Contains("RegionId") Then
                dvRegion = New DataView(dsWorkOrder.Tables(1))
                UCRegions.populateListBox(Nothing, dsLookup.Tables("tblRegionCodes").Copy.DefaultView, "RegionName", "SettingValue", "RegionID")
            Else
                UCRegions.populateListBox(Nothing, dsLookup.Tables("tblRegionCodes").Copy.DefaultView, "RegionName", "SettingValue", "RegionID")
            End If
        Else
            UCRegions.populateListBox(Nothing, dsLookup.Tables("tblRegionCodes").Copy.DefaultView, "RegionName", "SettingValue", "RegionID")
        End If

        'Populate Accreditations
        'Dim dsVendors As DataSet = GetVendors(False)
        'Dim dvVendor As DataView
        'dvVendor = New DataView(dsVendors.Tables("tblAllVendors"))
        'UCCompanyAccreditations.populateListBox(Nothing, dvVendor, "VendorName", "VendorID", "VendorID")

        UCAccreditations.BizDivId = ViewState("BizDivId")
        UCAccreditations.CommonID = Request("WOID").Trim
        UCAccreditations.PopulateSelectedGrid(True)


        If Not IsNothing(Request("crbChecked")) Then
            If Request("crbChecked") = "Yes" Then
                chkCRBChecked.Checked = True
            ElseIf Request("crbChecked") = "No" Then
                chkCRBChecked.Checked = False
            End If
        Else
            chkCRBChecked.Checked = False
        End If
        If Not IsNothing(Request("ukSecurity")) Then
            If Request("ukSecurity") = "Yes" Then
                chkUKSecurity.Checked = True
            ElseIf Request("ukSecurity") = "No" Then
                chkUKSecurity.Checked = False
            End If
        Else
            chkUKSecurity.Checked = False
        End If

        If Not IsNothing(Request("CSCS")) Then
            If Request("CSCS") = "Yes" Then
                chkCSCS.Checked = True
            ElseIf Request("CSCS") = "No" Then
                chkCSCS.Checked = False
            End If
        Else
            chkCSCS.Checked = False
        End If
        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        If Not IsNothing(Request("ExcelPartner")) Then
            If Request("ExcelPartner") = "Yes" Then
                chkExcelPartner.Checked = True
            ElseIf Request("ExcelPartner") = "No" Then
                chkExcelPartner.Checked = False
            End If
        Else
            chkExcelPartner.Checked = False
        End If
        If Not IsNothing(Request("EPCP")) Then
            If Request("EPCP") = "Yes" Then
                chkEPCP.Checked = True
            ElseIf Request("EPCP") = "No" Then
                chkEPCP.Checked = False
            End If
        Else
            chkEPCP.Checked = False
        End If

        If IsNothing(ViewState!SortExpression) Then
            ViewState!SortExpression = "Email"
            ' gvSupplier.Sort(ViewState!SortExpression, SortDirection.Ascending)
        End If
    End Sub

    ''' <summary>
    ''' Function to populate the workorder summary displayed on to of the ordermatch form
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <remarks></remarks>
    Private Sub populateWOSummary(ByVal ds As DataSet)
        'Caching the ds to use later to calculate the Geocode of the WO Postcode.
        'Dim cacheSummaryDS As String = "cacheSummaryDS"
        'Cache.Remove("cacheSummaryDS")
        'Cache.Add(cacheSummaryDS, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        If Not IsNothing(ds.Tables("tblGeoCode")) Then
            If (ds.Tables("tblGeoCode").Rows.Count > 0) Then
                ViewState("WOLatitude") = ds.Tables("tblGeoCode").Rows(0).Item("Latitude")
                ViewState("WOLongitude") = ds.Tables("tblGeoCode").Rows(0).Item("Longitude")
                ViewState("WLPostCode") = ds.Tables("tblGeoCode").Rows(0).Item("PostCode")
            End If
        End If
        Dim dv As New DataView(ds.Tables("tblWOSummary"))
        'Workorder id
        If Not IsDBNull(dv.Item(0).Item("RefWOID")) Then
            'lblWONo.Text = dv.Item(0).Item("WorkOrderId")
            lblWONo.Text = dv.Item(0).Item("RefWOID")
        End If
        'Submitted date
        If Not IsDBNull(dv.Item(0).Item("DateCreated")) Then
            lblWOSubmitted.Text = Strings.FormatDateTime(dv.Item(0).Item("DateCreated"), DateFormat.ShortDate)
        End If
        'Last sent
        If IsDBNull(dv.Item(0).Item("DateModified")) Then
            lblWOLastSent.Text = ""
        Else
            lblWOLastSent.Text = Strings.FormatDateTime(dv.Item(0).Item("DateModified"), DateFormat.ShortDate)
        End If
        'location
        If Not IsDBNull(dv.Item(0).Item("Location")) Then
            lblWOLoc.Text = dv.Item(0).Item("Location")
        End If
        'Contact
        If Not IsDBNull(dv.Item(0).Item("Contact")) Then
            lblWOContact.Text = dv.Item(0).Item("Contact")
        End If
        'Title
        If Not IsDBNull(dv.Item(0).Item("WOTitle")) Then
            lblWOTitle.Text = dv.Item(0).Item("WOTitle")
        End If
        'Title
        ViewState("WOLongDesc") = ""
        If Not IsDBNull(dv.Item(0).Item("WOLongDesc")) Then
            ViewState("WOLongDesc") = Trim(dv.Item(0).Item("WOLongDesc")).Replace("<br>", Chr(13)).Replace("<BR>", Chr(13))
        End If
        'Start Date
        If Not IsDBNull(dv.Item(0).Item("DateStart")) Then
            lblWOStart.Text = Strings.FormatDateTime(dv.Item(0).Item("DateStart"), DateFormat.ShortDate)
            ViewState("DateStart") = dv.Item(0).Item("DateStart")
        End If
        'Wholesale Price 
        If Not IsDBNull(dv.Item(0).Item("WholesalePrice")) Then
            lblWOPriceWP.Text = FormatCurrency(dv.Item(0).Item("WholesalePrice"), 2, TriState.True, TriState.True, TriState.False)
            ViewState("WholesalePrice") = dv.Item(0).Item("WholesalePrice")
        End If
        'Platform Price
        If Not IsDBNull(dv.Item(0).Item("PlatformPrice")) Then
            lblWOPricePP.Text = FormatCurrency(dv.Item(0).Item("PlatformPrice"), 2, TriState.True, TriState.True, TriState.False)
            ViewState("PlatformPrice") = dv.Item(0).Item("PlatformPrice")
        End If
        '*********************************Staged WO Code*****************************
        If Not IsDBNull(dv.Item(0).Item("EstimatedTimeInDays")) Then
            ViewState("EstimatedTimeInDays") = dv.Item(0).Item("EstimatedTimeInDays")
        End If
        If Not IsDBNull(dv.Item(0).Item("PlatformDayJobRate")) Then
            ViewState("PlatformDayJobRate") = dv.Item(0).Item("PlatformDayJobRate")
        End If
        If Not IsDBNull(dv.Item(0).Item("StagedWO")) Then
            ViewState("StagedWO") = dv.Item(0).Item("StagedWO")
        End If
        '*********************************Staged WO Code*****************************
        'Status
        lblWOStatus.Text = IIf(IsDBNull(dv.Item(0).Item("WOStatusSent")) = True, "Submitted", "Sent")


        ViewState("DateEnd") = dv.Item(0).Item("DateEnd")
        ViewState("EstimatedTime") = dv.Item(0).Item("EstimatedTime")
        ViewState("SpecialistSuppliesParts") = dv.Item(0).Item("SpecialistSuppliesParts")
        ViewState("ActualTime") = dv.Item(0).Item("ActualTime")
        ViewState("WOCategory") = dv.Item(0).Item("WOCategory")
        ViewState("Location") = dv.Item(0).Item("Location")
        ViewState("WOTitle") = dv.Item(0).Item("WOTitle")
        ViewState("WorkOrderId") = dv.Item(0).Item("WorkOrderId")
        ViewState("RefWOID") = dv.Item(0).Item("RefWOID")
        ViewState("PricingMethod") = dv.Item(0).Item("PricingMethod")
        ViewState("BuyerCompanyId") = dv.Item(0).Item("CompanyID")
        WODetails.HRef = "AdminWODetails.aspx?WOID=" & ViewState("WOID") & "&Viewer=Admin&WorkOrderID=" & ViewState("WorkOrderId") & "&ContactID=" & dv.Item(0).Item("ContactID") & "&CompanyID=" & dv.Item(0).Item("CompanyID") & "&Group=" & Request("Group") & "&SearchWorkorderID=" & Request("SearchWorkorderID") & "&BizDivID=" & Request("BizDivID") & "&FromDate=" & Request("FromDate") & "&ToDate=" & Request("ToDate") & "&PS=" & Request("PS") & "&PN=" & Request("PN") & "&SC=" & Request("SC") & "&SO=" & Request("SO") & "&sender=" & Request("sender") & "&CompID=" & Request("CompID")


    End Sub

    ''' <summary>
    ''' Function to prepare the sercha criteria string.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function PrepareSearchCriteria()
        ' display search criteria
        hdnRegionName.Value = getSelectedRegions("Text")
        hdnRegionValue.Value = getSelectedRegions("Value")
        hdnVendorName.Value = GetAccredinCSV("Text")
        hdnVendorValue.Value = GetAccredinCSV("Value")

        lblSearchCriteria.Text = "<strong>OrderMatch Search Criteria:</strong>"

        'region name
        If hdnRegionName.Value.EndsWith(",") Then
            hdnRegionName.Value = hdnRegionName.Value.Substring(0, hdnRegionName.Value.Length - 1)
        End If
        If hdnRegionName.Value <> "" Then
            lblSearchCriteria.Text &= " Region - <span class='txtOrange'>" & hdnRegionName.Value & "</span>"
        End If

        'vendor name
        If hdnVendorName.Value.EndsWith(",") Then
            hdnVendorName.Value = hdnVendorName.Value.Substring(0, hdnVendorName.Value.Length - 1)
        End If
        If hdnVendorName.Value <> "" Then
            lblSearchCriteria.Text &= " Vendors - <span class='txtOrange'>" & hdnVendorName.Value & "</span>"
        End If
        'Company Name
        If txtCompanyName.Text <> "" Then
            lblSearchCriteria.Text &= " Company Name - <span class='txtOrange'>" & txtCompanyName.Text & "</span>"
        End If

        'keyword
        If txtKeyword.Text <> "" Then
            lblSearchCriteria.Text &= " Keyword - <span class='txtOrange'>" & txtKeyword.Text & "</span>"
        End If

        'postcode
        If txtPostCode.Text <> "" Then
            lblSearchCriteria.Text &= " PostCode - <span class='txtOrange'>" & txtPostCode.Text & "</span>"
        End If
        'No Of Employees
        If ddlNoOfEmployees.SelectedValue <> "" Then
            lblSearchCriteria.Text &= " Company Name - <span class='txtOrange'>" & ddlNoOfEmployees.SelectedItem.Text & "</span>"
        End If

        'min wo value
        If chkMinWOValue.Checked = True Then
            lblSearchCriteria.Text &= " Exclude suppliers with min WO Value >= Price - <span class='txtOrange'>Yes</span>"
        Else
            lblSearchCriteria.Text &= " Exclude suppliers with min WO Value >= Price - <span class='txtOrange'>No</span>"
        End If
        'Show suppliers with min one closed WO
        If chkShowSupplierWithCW.Checked = True Then
            lblSearchCriteria.Text &= " Show suppliers with min one closed WO - <span class='txtOrange'>Yes</span>"
        Else
            lblSearchCriteria.Text &= " Show suppliers with min one closed WO - <span class='txtOrange'>No</span>"
        End If
        'ref checked
        If chkRefChecked.Checked = True Then
            lblSearchCriteria.Text &= " Reference Checked - <span class='txtOrange'>Yes</span>"
        Else
            lblSearchCriteria.Text &= " Reference Checked - <span class='txtOrange'>No</span>"
        End If
        'lblSearchCriteria.Text &= " Skills Area - <span class='txtOrange'>" & ddlSkillsArea.SelectedItem.Text & "</span>"
        Return Nothing
    End Function

    Public Function getWODetailLink() As String
        Dim strWODetails As String = ""
        strWODetails = "WODetails.apsx?WOID=" & ViewState("WOID") & "&Viewer=Admin&WorkOrderID=" & Request("WorkOrderID")
        Return strWODetails
    End Function

    Public Function getWOListingLink(ByVal BizDivID As Integer, ByVal CompanyID As Integer, ByVal ContactID As Integer, ByVal ClassID As Integer, ByVal StatusID As Integer, ByVal RoleGroupID As Integer) As String
        'Company profile page link
        Dim link As String = ""
        Dim linkParams As String = ""
        linkParams &= "bizDivId=" & BizDivID
        linkParams &= "&bizdiv=" & ViewState("Country")
        linkParams &= "&companyId=" & CompanyID
        linkParams &= "&contactId=" & ContactID
        linkParams &= "&classId=" & ClassID
        linkParams &= "&statusId=" & StatusID
        linkParams &= "&roleGroupId=" & RoleGroupID
        linkParams &= "&WOID=" & ViewState("WOID")
        linkParams &= "&WorkOrderID=" & ViewState("WorkOrderId")
        If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "" Then
            If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "0" Then
                linkParams &= "&product=" & CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value
            End If
        End If
        linkParams &= "&region=" & ViewState("Regions")
        linkParams &= "&PostCode=" & txtPostCode.Text
        linkParams &= "&Keyword=" & txtKeyword.Text
        linkParams &= "&Group=" & Request("Group")
        linkParams &= "&refcheck=" & chkRefChecked.Checked
        linkParams &= "&MinWOValue=" & IIf((chkMinWOValue.Checked = True), 1, 0)
        linkParams &= "&FavSupplier=" & IIf((chkFavSupplier.Checked = True), 1, 0)

        linkParams &= "&companyName=" & txtCompanyName.Text
        linkParams &= "&noOfEmployees=" & ddlNoOfEmployees.SelectedValue
        linkParams &= "&ShowSupplierWithCW=" & chkShowSupplierWithCW.Checked
        linkParams &= "&vendorIds=" & ViewState("vendorIds")

        linkParams &= "&mode=" & "Accepted"
        linkParams &= "&sender=" & "OrderMatch"
        linkParams &= "&PS=" & gvSupplier.PageSize
        linkParams &= "&PN=" & gvSupplier.PageIndex
        linkParams &= "&SC=" & gvSupplier.SortExpression
        linkParams &= "&SO=" & gvSupplier.SortDirection
        linkParams &= "&NearestTo=" & chkNearestTo.Checked
        linkParams &= "&Distance=" & ddlNearestTo.SelectedValue
        If chkCRBChecked.Checked = True Then
            linkParams &= "&crbChecked=Yes"
        ElseIf chkCRBChecked.Checked = False Then
            linkParams &= "&crbChecked=No"
        End If
        If chkUKSecurity.Checked = True Then
            linkParams &= "&ukSecurity=Yes"
        ElseIf chkUKSecurity.Checked = False Then
            linkParams &= "&ukSecurity=No"
        End If

        If chkCSCS.Checked = True Then
            linkParams &= "&CSCS=Yes"
        ElseIf chkCSCS.Checked = False Then
            linkParams &= "&CSCS=No"
        End If
        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        If chkExcelPartner.Checked = True Then
            linkParams &= "&ExcelPartner=Yes"
        ElseIf chkExcelPartner.Checked = False Then
            linkParams &= "&ExcelPartner=No"
        End If
        If chkEPCP.Checked = True Then
            linkParams &= "&EPCP=Yes"
        ElseIf chkEPCP.Checked = False Then
            linkParams &= "&EPCP=No"
        End If

        Return linkParams
    End Function


#End Region

#Region "Action"

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        pnlSearchResult.Visible = True
        lblMsgSent.Text = ""
        PrepareSearchCriteria()
        SelectFromDB("")
        'Sumit - Populate Selected value of Area of expertise
        UCAOE1.CombIds = hdnProductIds.Value
        UCAOE1.PopulateAOE()
    End Sub

    ''' <summary>
    ''' Function to send mail to the selected suppliers from the listing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Dim flagSuppliers As Boolean = False
        Dim selectedSupplierIDs As String = ""

        'Get the status before sending the notifications to the suppliers as by this time some supplier may have accepted the wo.
        Dim woStatus As String = ""
        Dim dsNew As DataSet = ws.WSWorkOrder.woGetWOStatus(ViewState("WOID"), "short")
        If dsNew.Tables("tblWOStaus").Rows.Count > 0 Then
            If dsNew.Tables("tblWOStaus").Rows(0)("WOStatus") <> "2" And dsNew.Tables("tblWOStaus").Rows(0)("WOStatus") <> "3" And dsNew.Tables("tblWOStaus").Rows(0)("WOStatus") <> "5" Then
                lblMsg.Text = "Sorry, the work order is accepted by some supplier. You can not send work order notification to other suppliers."
                Return
            End If
        End If
        selectedSupplierIDs = CommonFunctions.getSelectedIdsOfGrid(gvSupplier, "CheckSupplier", "hdnID")

        If selectedSupplierIDs = "" Then
            lblMsg.Text = "Please select atleast one supplier to send this WorkOrder to."
        Else
            lblMsg.Text = ""

            'Make Entries in DB for each Supplier to whom WO is sent
            Dim dsEmailContacts As DataSet
            '0 as the second last parameter says that AutoMatch is not to be applied. - Pratik Trivedi - 27-Aug, 2008
            '0 as the last parameter says that Buyer id is not required. - Pratik Trivedi - 27-Aug, 2008
            Dim NearestTo As String = "No"
            If Not IsNothing(ViewState("NearestTo")) Then
                NearestTo = ViewState("NearestTo").ToString()
            End If

            dsEmailContacts = ws.WSWorkOrder.SendWO(selectedSupplierIDs, ViewState("WOID"), Strings.FormatDateTime(ViewState("DateStart"), DateFormat.ShortDate), Strings.FormatDateTime(ViewState("DateEnd"), DateFormat.ShortDate), ViewState("EstimatedTime"), ViewState("PlatformPrice"), ViewState("SpecialistSuppliesParts"), ViewState("ActualTime"), Session("CompanyID"), Session("UserID"), 0, 0, NearestTo)

            If dsEmailContacts.Tables.Count > 2 Then
                If dsEmailContacts.Tables(1).Rows(0)("Status") = -1 Then
                    lblMsg.Text = ResourceMessageText.GetString("WOVersionControlMsg").Replace("<Link>", "WOOrdermatch.aspx?WOID=" & Request("WOID") & "&Group=" & Request("Group") & "&SupContactId=" & Request("SupContactId") & "&SupCompId=" & Request("SupCompId") & "&ContactID=" & Request("ContactID") & "&CompanyID=" & Request("CompanyID") & "&FromDate=&ToDate=&PS=" & Request("PS") & "&PN=0&SC=DateStart&SO=0&CompID=" & Request("CompID") & "&sender=UCWOsListing&BizDivID=" & ApplicationSettings.OWUKBizDivId)
                    Return
                End If
            End If
            'Send WO Notification Emails to Supplier Accounts (Administrator + All those that have Receive WO Notification = True)
            If dsEmailContacts.Tables("tblSuppliers").Rows.Count <> 0 Then

                Dim dSendNotification As [Delegate] = New AfterOrderMatch(AddressOf IPhoneNotification)
                ThreadUtil.FireAndForget(dSendNotification, New Object() {selectedSupplierIDs, Convert.ToInt32(ViewState("WOID")), dsEmailContacts})

                lblSearchCriteria.Text = ""
                lblMsgSent.Text = "<p>The WorkOrder has been successfully sent to the Suppliers you selected.</p><p>To send this same work order to more suppliers you can perform the search again. </p><p>The suppliers to whom the Work Order has already been sent will not show up in the search results.</p>"
                pnlSearchResult.Visible = False
            Else
                lblSearchCriteria.Text = ""
                lblMsgSent.Text = ResourceMessageText.GetString("DBUpdateFail")
                pnlSearchResult.Visible = False
            End If


        End If
    End Sub
    Private Sub btnSendAndAutoaccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendAndAutoaccept.Click
        'Get the status before sending the notifications to the suppliers as by this time some supplier may have accepted the wo.
        Dim woStatus As String = ""
        Dim selectedSupplierIDs As String = ""

        Dim Success As Integer = 0
        Dim dsNew As DataSet = ws.WSWorkOrder.woGetWOStatus(ViewState("WOID"), "short")
        If dsNew.Tables("tblWOStaus").Rows.Count > 0 Then
            If dsNew.Tables("tblWOStaus").Rows(0)("WOStatus") <> "2" And dsNew.Tables("tblWOStaus").Rows(0)("WOStatus") <> "3" And dsNew.Tables("tblWOStaus").Rows(0)("WOStatus") <> "5" Then
                lblMsg.Text = "Sorry, the work order is accepted by some supplier. You can not send work order notification to other suppliers."
                Return
            End If
        End If
        Dim selectedSupplierCount As Integer = 0
        Dim AllowJobAcceptance As Boolean = False
        For Each row As GridViewRow In gvSupplier.Rows
            Dim chkBox As CheckBox = CType(row.FindControl("CheckSupplier"), CheckBox)
            If Not (chkBox Is Nothing) Then
                If chkBox.Checked Then
                    If selectedSupplierIDs.Contains(CStr(CType(row.FindControl("hdnCompanyID"), HtmlInputHidden).Value)) = False Then
                        selectedSupplierCount = selectedSupplierCount + 1
                        AllowJobAcceptance = CType(row.FindControl("hdnAllowJobAcceptance"), HtmlInputHidden).Value
                        If selectedSupplierIDs = "" Then
                            selectedSupplierIDs += "" & CStr(CType(row.FindControl("hdnCompanyID"), HtmlInputHidden).Value)
                        Else
                            selectedSupplierIDs += " ," & CStr(CType(row.FindControl("hdnCompanyID"), HtmlInputHidden).Value)
                        End If
                    End If
                End If
            End If
        Next

        If selectedSupplierIDs = "" Then
            lblMsg.Text = "Please select supplier to send And Auto Accept this WorkOrder."
        Else
            If (selectedSupplierCount = 1 And AllowJobAcceptance = True) Then
                SendAndAutoaccept(selectedSupplierIDs)
            Else
                lblMsg.Text = "Please select single supplier with Allow Job Acceptance set to True to send And Auto Accept this WorkOrder."
            End If
        End If
    End Sub
    Public Sub SendAndAutoaccept(ByVal selectedSupplierIDs As String)
        Try
            Dim ds As New DataSet
            lblMsg.Text = ""
            ds = ws.WSWorkOrder.SendAndAutoaccept(CInt(ViewState("WOID")), CInt(selectedSupplierIDs), Session("UserID"), Session("BizDivId"))
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("Success") = 1 Then

                    'To Client
                    If (ds.Tables(0).Rows(0)("clientMailbody") <> "" And ds.Tables(0).Rows(0)("clientRecipients") <> "") Then
                        Emails.SendAutoAcceptWOEmailToClient(ds.Tables(0).Rows(0)("clientSubject"), ds.Tables(0).Rows(0)("clientMailbody"), ds.Tables(0).Rows(0)("clientRecipients"), ds.Tables(0).Rows(0)("ClientCCList"))
                    End If

                    'To SP
                    If (ds.Tables(0).Rows(0)("SPMailbody") <> "" And ds.Tables(0).Rows(0)("SPRecipients") <> "") Then
                        Emails.SendAutoAcceptWOEmailToSP(ds.Tables(0).Rows(0)("SPSubject"), ds.Tables(0).Rows(0)("SPMailbody"), ds.Tables(0).Rows(0)("SPRecipients"), ds.Tables(0).Rows(0)("SPCCList"))
                    End If

                    lblSearchCriteria.Text = ""
                    lblMsgSent.Text = "<p>The WorkOrder has been successfully sent and auto accepted on behalf of the Suppliers you selected.</p>"
                    pnlSearchResult.Visible = False
                ElseIf ds.Tables(0).Rows(0)("Success") = 0 Then
                    lblSearchCriteria.Text = ""
                    lblMsgSent.Text = "Selected supplier's capacity is reached for this day/time slot and billing location."
                    pnlSearchResult.Visible = False
                Else
                    lblSearchCriteria.Text = ""
                    lblMsgSent.Text = ResourceMessageText.GetString("DBUpdateFail")
                    pnlSearchResult.Visible = False
                End If
            End If
        Catch ex As Exception
            CommonFunctions.createLog(ex.ToString())
        End Try
    End Sub
    Public Sub IPhoneNotification(ByVal SelectedIDs As String, ByVal WOID As Integer, ByVal dsEmailContacts As DataSet)

        Try
            If ApplicationSettings.SendIphoneNotification Then
                Dim dsNotification As DataSet = ws.WSWorkOrder.IPhoneNotification(SelectedIDs, WOID)
                If dsNotification.Tables.Count > 0 Then
                    If dsNotification.Tables("TokenDetails").Rows.Count > 0 Then
                        For i As Integer = 0 To dsNotification.Tables("TokenDetails").Rows.Count - 1
                            '   ws.WSiPhoneNotification.SendAlert(dsNotification.Tables("TokenDetails").Rows(i)("Token"), "New JOB -" & dsNotification.Tables("TokenDetails").Rows(i)("RefWOID") & " arrived", "default", 1)
                        Next
                    End If
                End If
            End If

            'Send WO Notification Emails to Supplier Accounts (Administrator + All those that have Receive WO Notification = True)
            If dsEmailContacts.Tables("tblSuppliers").Rows.Count <> 0 Then
                Dim objEmail As New Emails

                'Add data to the Email object
                objEmail.WOCategory = ViewState("WOCategory")
                objEmail.WOEndDate = ViewState("DateEnd")
                objEmail.WOLoc = ViewState("Location")
                objEmail.PPPrice = ViewState("PlatformPrice")
                objEmail.WorkOrderID = ViewState("RefWOID")
                objEmail.WOStartDate = ViewState("DateStart")
                objEmail.WOTitle = ViewState("WOTitle")
                objEmail.EstimatedTimeInDays = ViewState("EstimatedTimeInDays")
                objEmail.WorkOrderDayRate = ViewState("PlatformDayJobRate")
                objEmail.StagedWO = ViewState("StagedWO")
                objEmail.WODate = Trim(lblWOSubmitted.Text)
                objEmail.WODesc = ViewState("WOLongDesc")

                If hdnVendorName.Value <> "" Then
                    objEmail.Accreditation = GetAccredinCSV("Both")
                End If

                If chkNearestTo.Checked = True Then
                    objEmail.IsNearestToUsed = True
                End If

                objEmail.CRBChecked = chkCRBChecked.Checked
                objEmail.UKSecurityChecked = chkUKSecurity.Checked
                objEmail.CSCSChecked = chkCSCS.Checked
                'objEmail.EngCRBChecked = chkEngCRBChecked.Checked
                If (CRBCheckedEnhanced.Checked = True Or CRBCheckedYes.Checked) Then
                    objEmail.EngCRBChecked = True
                Else
                    objEmail.EngCRBChecked = False
                End If

                objEmail.EngUKSecurity = chkEngUKSecurity.Checked
                objEmail.EngCSCSChecked = chkEngCSCS.Checked
                objEmail.EngRightToWorkChecked = chkEngRightToWork.Checked



                Emails.SendOrderMatchMail(objEmail, dsEmailContacts, ViewState("BizDivId"), ViewState("PricingMethod"), CInt(ViewState("BuyerCompanyId")))

            End If
        Catch ex As Exception
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, " Error in IPhoneNotification")
        End Try

    End Sub
    ''' <summary>
    ''' back to listing - from company profile page
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub processBackToListing()

        ViewState("BizDivId") = Session("BizDivId")
        UCAOE1.BizDivId = getAOEBizDivIdFromRadioListBtn()
        ViewState("WOID") = Request("WOID")
        hdnProductIds.Value = Request("product")
        txtKeyword.Text = Request("Keyword")
        txtPostCode.Text = Request("PostCode")
        chkMinWOValue.Checked = IIf((Request("MinWOValue").Trim = "1"), True, False)
        chkRefChecked.Checked = IIf((Request("refcheck").Trim = "True"), True, False)
        txtCompanyName.Text = Request("companyName")
        'If regionCode = Request("region") Then
        '    regionCode = Request("region")
        'End If

        If Not IsNothing(Request("FavSupplier")) Then
            chkFavSupplier.Checked = Request("FavSupplier")
        Else
            chkFavSupplier.Checked = False
        End If
        If Not IsNothing(Request("refcheck")) Then
            chkRefChecked.Checked = Request("refcheck")
        Else
            chkRefChecked.Checked = False
        End If

        If Not Request("ShowSupplierWithCW") Is Nothing Then
            chkShowSupplierWithCW.Checked = Request("ShowSupplierWithCW")
        End If

        gvSupplier.PageSize = Request("PS")

        'Sort Expe
        ViewState!SortExpression = ""
        If Trim(Request("SC")) <> "" Then
            ViewState!SortExpression = Trim(Request("SC"))
        End If

        ''Sort direction
        Dim sd As SortDirection
        sd = SortDirection.Descending
        If Trim(Request("SO")) <> "" Then
            If Trim(Request("SO")).ToLower = "0" Then
                sd = SortDirection.Ascending
            End If
        End If
        If Request("NearestTo") = "True" Then
            chkNearestTo.Checked = True
            divNearestTo.Visible = True
            ddlNearestTo.SelectedValue = Request("Distance")
        Else
            chkNearestTo.Checked = False
        End If

        InitializeForm(Request("WOID").Trim)


        'UCAOE1.CombIds = Request("product")
        lblSearchCriteria.Text = PrepareSearchCriteria()
        pnlSearchResult.Visible = True
        'Dim ds As New DataSet
        SelectFromDB(ViewState!SortExpression)
        'gvSupplier.DataSource = ds.Tables(0)
        'gvSupplier.DataBind()
    End Sub


#End Region

#Region "GridView"

    Private Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        If Not IsNothing(ViewState("PagingClick")) Then
            Select Case ViewState("PagingClick")
                Case "Prev"
                    ViewState("drpPageIndex") = ViewState("drpPageIndex") - 1
                Case "Next"
                    ViewState("drpPageIndex") = ViewState("drpPageIndex") + 1
                Case "First"
                    ViewState("drpPageIndex") = 0
                Case "Last"
                    ViewState("drpPageIndex") = ViewState("pagecount") - 1
            End Select
        Else
            ViewState("drpPageIndex") = 0
        End If
        gvSupplier.PageIndex = ViewState("drpPageIndex")
        ViewState("pageindex") = (gvSupplier.PageIndex * gvSupplier.PageSize) + 1
        PopulateGrid(ViewState("pageindex"), ViewState("SortExpression"))
    End Sub

    Protected Sub gvSupplier_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvSupplier.RowDataBound
        MakeGridViewHeaderClickable(gvSupplier, e.Row)
        If (DataBinder.Eval(e.Row.DataItem, "TrackSP") = "1") Then
            Dim col As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#FFFFBC")
            e.Row.BackColor = col
            'e.Row.Attributes.Add("onMouseOver", "style.background='#FFEBC6';")
            'e.Row.Attributes.Add("onMouseOut", "style.background='#FFFFBC';")
        Else
            Dim OriginalCol As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#EDEDED")
            e.Row.BackColor = OriginalCol
        End If
    End Sub

    Protected Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim DDLPage As DropDownList = CType(gvSupplier.TopPagerRow.FindControl("ddlPageSelector"), DropDownList)
        gvSupplier.PageIndex = DDLPage.SelectedIndex
        ViewState("drpPageIndex") = DDLPage.SelectedIndex
        ViewState("pageindex") = (DDLPage.SelectedIndex * gvSupplier.PageSize) + 1
        PopulateGrid(ViewState("pageindex"), ViewState("SortExpression"))
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub


    ''' <summary>
    ''' Object Datasource function which calls the webmethod to get the list of suppliers.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectFromDB(ByVal sortExpression As String, Optional ByVal PageIndex As Integer = 0)

        If chkNearestTo.Checked = True Then
            If sortExpression = "" Then
                sortExpression = "Distance"

            End If
            ViewState("NearestTo") = "Yes"
        Else
            If sortExpression = "" Then
                sortExpression = "CompanyName"
            End If
            ViewState("NearestTo") = "No"
        End If
        ViewState("SortExpression") = sortExpression
        Dim woid As Integer
        Dim Keyword As String
        Dim skills As String
        Dim regionCode As String
        Dim vendorIds As String
        Dim Postcode As String
        Dim minWOValue As Integer
        Dim statusID As String
        Dim companyName As String
        Dim categoryType As String
        Dim noOfEmployees As String
        Dim ShowSupplierWithCW As Boolean
        Dim CRBChecked As String = "No"
        Dim UKSecurity As String = "No"
        Dim CSCS As String = "No"
        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        Dim ExcelPartner As String = "No"
        Dim EPCP As String = "No"
        Dim FavSupplier As Boolean = False
        Dim NearestTo As String = "No"
        Dim distance As Integer = 0
        Dim hidetestaccount As Boolean
        Dim EngineerCRBCheck As Integer
        Dim EngineerCSCSCheck As Boolean
        Dim EngineerUKSecurityCheck As Boolean
        Dim EngineerRightToWorkInUK As Boolean
        Dim EngProofOfMeeting As Boolean
        'Dim skillsArea As String = "All"
        'If Not IsNothing(Request("sender")) Then
        '    If Request("sender") = "CompanyProfile" Or Request("sender") = "AdminWOListing" Then
        '        woid = Request("WOID")
        '        Keyword = Request("Keyword").Trim
        '        companyName = Request("companyName")
        '        noOfEmployees = Request("noOfEmployees")
        '        ShowSupplierWithCW = Request("ShowSupplierWithCW")
        '        skills = ""
        '        If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "" Then
        '            If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "0" Then
        '                skills = CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value
        '            End If
        '        End If
        '        Postcode = Request("PostCode")
        '        regionCode = Request("region")
        '        vendorIds = Request("vendorIds")
        '        minWOValue = Request("MinWOValue")
        '        If Not IsNothing(Request("FavSupplier")) Then
        '            FavSupplier = Request("FavSupplier")
        '        Else
        '            FavSupplier = False
        '        End If
        '        hidetestaccount = True
        '        statusID = IIf((Request("refcheck").Trim = "True"), ApplicationSettings.StatusApprovedCompany, "")
        '    Else
        '        woid = ViewState("WOID")
        '        Keyword = txtKeyword.Text.Trim
        '        companyName = txtCompanyName.Text.Trim
        '        noOfEmployees = ddlNoOfEmployees.SelectedValue
        '        ShowSupplierWithCW = chkShowSupplierWithCW.Checked
        '        skills = ""
        '        If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "" Then
        '            If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "0" Then
        '                skills = CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value
        '            End If
        '        End If
        '        Postcode = txtPostCode.Text
        '        regionCode = getSelectedRegions("Value")
        '        vendorIds = getSelectedVendors("Value")
        '        minWOValue = IIf((chkMinWOValue.Checked = True), 1, 0)
        '        statusID = IIf((chkRefChecked.Checked = True), ApplicationSettings.StatusApprovedCompany, "")
        '        FavSupplier = IIf((chkFavSupplier.Checked = True), 1, 0)
        '        'skillsArea = ddlSkillsArea.SelectedItem.Value
        '        hidetestaccount = IIf((chkbxHideTestAccount.Checked = True), True, False)
        '    End If
        'Else
        woid = ViewState("WOID")
        Keyword = txtKeyword.Text.Trim
        companyName = txtCompanyName.Text.Trim
        noOfEmployees = ddlNoOfEmployees.SelectedValue
        categoryType = drpCategoryType.SelectedValue
        ShowSupplierWithCW = chkShowSupplierWithCW.Checked
        skills = ""
        If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "" Then
            If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "0" Then
                skills = CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value
                If (skills.Substring(0, 2) = "0,") Then
                    skills = skills.Remove(0, 2)
                End If
            End If
        End If
        Postcode = txtPostCode.Text
        regionCode = getSelectedRegions("Value")
        vendorIds = GetAccredinCSV("Value")
        minWOValue = IIf((chkMinWOValue.Checked = True), 1, 0)
        statusID = IIf((chkRefChecked.Checked = True), ApplicationSettings.StatusApprovedCompany, "")
        FavSupplier = IIf((chkFavSupplier.Checked = True), 1, 0)
        hidetestaccount = IIf((chkbxHideTestAccount.Checked = True), True, False)
        'skillsArea = ddlSkillsArea.SelectedItem.Value
        'End If
        hdnProductIds.Value = skills
        If Not IsNothing(Request("crbChecked")) Then
            CRBChecked = Request("crbChecked")
        ElseIf chkCRBChecked.Checked = True Then
            CRBChecked = "Yes"
        ElseIf chkCRBChecked.Checked = False Then
            CRBChecked = "No"
        End If
        If Not IsNothing(Request("ukSecurity")) Then
            UKSecurity = Request("ukSecurity")
        ElseIf chkUKSecurity.Checked = True Then
            UKSecurity = "Yes"
        ElseIf chkUKSecurity.Checked = False Then
            UKSecurity = "No"
        End If

        If Not IsNothing(Request("CSCS")) Then
            CSCS = Request("CSCS")
        ElseIf chkCSCS.Checked = True Then
            CSCS = "Yes"
        ElseIf chkCSCS.Checked = False Then
            CSCS = "No"
        End If
        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        If Not IsNothing(Request("ExcelPartner")) Then
            ExcelPartner = Request("ExcelPartner")
        ElseIf chkExcelPartner.Checked = True Then
            ExcelPartner = "Yes"
        ElseIf chkExcelPartner.Checked = False Then
            ExcelPartner = "No"
        End If
        If Not IsNothing(Request("EPCP")) Then
            EPCP = Request("EPCP")
        ElseIf chkEPCP.Checked = True Then
            EPCP = "Yes"
        ElseIf chkEPCP.Checked = False Then
            EPCP = "No"
        End If

        Dim maxRows As Integer = 0
        If chkShowAll.Checked Then
            maxRows = 0
        Else
            maxRows = gvSupplier.PageSize
        End If
        Dim ApprovedServicePartner As Boolean
        If chkApproved.Checked Then
            ApprovedServicePartner = True
        Else
            ApprovedServicePartner = False
        End If
        ViewState("AOE") = skills
        ViewState("Regions") = regionCode
        ViewState("vendorIds") = vendorIds
        'EngineerCRBCheck = chkEngCRBChecked.Checked
        If (CRBCheckedEnhanced.Checked = True) Then
            EngineerCRBCheck = 137
        ElseIf (CRBCheckedYes.Checked = True) Then
            EngineerCRBCheck = 1
        Else
            EngineerCRBCheck = 0
        End If
        EngineerCSCSCheck = chkEngCSCS.Checked
        EngineerUKSecurityCheck = chkEngUKSecurity.Checked
        EngineerRightToWorkInUK = chkEngRightToWork.Checked
        EngProofOfMeeting = chkEngProofOfMeeting.Checked

        ' If Find Nearest to is checked then Selected regions shoudl not be considered in the Match Supplier fetch logic hence 
        ' sending region code as blank

        If chkNearestTo.Checked = True Then
            regionCode = ""
            NearestTo = "Yes"
            distance = ddlNearestTo.SelectedValue
            If CStr(ViewState("WOLatitude")) <> "Invalid" Then
                lblInvalidPostCode.Visible = False
            ElseIf CStr(ViewState("WOLatitude")) = "Invalid" Then
                lblInvalidPostCode.Visible = True
            End If
        Else
            lblInvalidPostCode.Visible = False
        End If

        'Added one more parameter to the procedurre call which will say if the chkNearestTo check box is checked or not.
        Dim executiontime As New Stopwatch
        executiontime.Start()
        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        Dim ds As DataSet = ws.WSWorkOrder.woOrderMatchGetSuppliers(companyName, Keyword, Postcode, noOfEmployees, ShowSupplierWithCW, skills, woid, statusID, regionCode, vendorIds, minWOValue, sortExpression, CRBChecked, CSCS, UKSecurity, FavSupplier, maxRows, PageIndex, NearestTo, distance, ApprovedServicePartner, hidetestaccount, EngineerCRBCheck, EngineerCSCSCheck, EngineerUKSecurityCheck, EngineerRightToWorkInUK, EngProofOfMeeting, txtSkillKeywords.Text.Trim, ExcelPartner, EPCP, categoryType)
        executiontime.Stop()
        Dim OMTime As String
        OMTime = executiontime.ElapsedMilliseconds.ToString

        CommonFunctions.createLog("Total Time for fetching Data from database for OrderMatch with parameters " & companyName & "," & Keyword & "," & Postcode & "," & noOfEmployees & "," & ShowSupplierWithCW & "," & skills & "," & woid & "," & statusID & "," & regionCode & "," & vendorIds & "," & minWOValue & "," & sortExpression & "," & chkNearestTo.Checked & "," & CRBChecked & "," & CSCS & "," & ExcelPartner & "," & EPCP & "," & UKSecurity & "," & FavSupplier & " is " & OMTime & " ms.")
        If ds.Tables.Count > 1 Then
            ViewState("rowCount") = ds.Tables("tblCount").Rows(0)(0)
        End If

        '*************************************************************************************************************************
        'Following code is used to decide if we need to show the distance column or not, depending on the status of the NearestTo Checkbox
        'Also, when ordermatch is clicked and suppliers are searched, the web service will only be used to get the geocode of the supplier
        'postcodes if the NearestTo checkbox is checked.

        'The following code is written so that grid will be sortable by distance column.(Distance column is not physically present)
        'If sortExpression = "Distance" Then
        '    ds.Tables(0).DefaultView.Sort = sortExpression & " ASC"
        'Else
        '    ds.Tables(0).DefaultView.Sort = sortExpression
        'End If
        '*************************************************************************************************************************
        If ViewState("rowCount") = 0 Then
            tblSendMail.Visible = False
            '    tdSendAndAutoaccept.Visible = False
            'ElseIf ViewState("rowCount") = 1 Then
            '    tblSendMail.Visible = True
            '    If (ds.Tables(0).Rows(0).Item("AllowJobAcceptance")) Then
            '        tdSendAndAutoaccept.Visible = True
            '    Else
            '        tdSendAndAutoaccept.Visible = False
            '    End If
        Else
            If ViewState("rowCount") > gvSupplier.PageSize Then
                If chkShowAll.Checked Then
                    gvSupplier.AllowPaging = False
                Else
                    gvSupplier.AllowPaging = True
                End If
            End If
            tblSendMail.Visible = True
            'tdSendAndAutoaccept.Visible = False
        End If

        gvSupplier.DataSource = ds.Tables(0)
        gvSupplier.DataBind()
        Return Nothing

    End Function

    Private Sub gvSupplier_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
                Dim tblTop As HtmlTable = CType(pagerRowTop.FindControl("tblTop"), HtmlTable)
                If chkShowAll.Checked Then
                    tblTop.Visible = False
                End If
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True
                Dim tblBottom As HtmlTable = CType(pagerRowBottom.FindControl("tblTop"), HtmlTable)
                If chkShowAll.Checked Then
                    tblBottom.Visible = False
                End If
            End If
        End If
        If CStr(ViewState("WOLatitude")) <> "Invalid" Then
            gvSupplier.Columns(6).Visible = True 'Distance Column
        ElseIf CStr(ViewState("WOLatitude")) = "Invalid" Then
            gvSupplier.Columns(6).Visible = False 'Distance Column
        End If
        If chkNearestTo.Checked = False Then
            gvSupplier.Columns(6).Visible = False 'Distance Column
        End If
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvSupplier.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvSupplier, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' Paging function for the datagrid
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer
        Dim pageCount As Integer
        If ViewState("rowCount") > 0 Then
            If ViewState("rowCount") > gridView.PageSize Then
                pageCount = CInt(Math.Ceiling(ViewState("rowCount") / gridView.PageSize))
            Else
                pageCount = 1
            End If
        Else
            pageCount = 1
        End If
        If Not IsNothing(ViewState("drpPageIndex")) Then
            pageIndex = ViewState("drpPageIndex")
        Else
            pageIndex = gvSupplier.PageIndex
        End If
        ViewState("pagecount") = pageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")

        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()

        For i As Integer = 1 To pageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.ClearSelection()
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    ''' <summary>
    ''' function to populate the Reference check column in the data grid.
    ''' returns date if refernce check is yes else returns No.
    ''' </summary>
    ''' <param name="refChkDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SetRefCheckedStatus(ByVal refChkDate As Object) As String
        If Not IsDBNull(refChkDate) Then
            If CStr(refChkDate) <> "" Then
                Return CDate(refChkDate).ToShortDateString
            End If
        End If
        Return "No"
    End Function

    ''' <summary>
    ''' Function to populate the link column of the grid. Link to the company profile page of the supplier
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="ContactID"></param>
    ''' <param name="ClassID"></param>
    ''' <param name="StatusID"></param>
    ''' <param name="RoleGroupID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLinks(ByVal BizDivID As Integer, ByVal CompanyID As Integer, ByVal ContactID As Integer, ByVal ClassID As Integer, ByVal StatusID As Integer, ByVal RoleGroupID As Integer) As String
        'Company profile page link
        Dim link As String = ""
        Dim linkParams As String = ""
        linkParams &= "bizDivId=" & BizDivID
        linkParams &= "&bizdiv=" & ViewState("Country")
        linkParams &= "&companyId=" & CompanyID
        linkParams &= "&contactId=" & ContactID
        linkParams &= "&classId=" & ClassID
        linkParams &= "&statusId=" & StatusID
        linkParams &= "&roleGroupId=" & RoleGroupID
        linkParams &= "&vendorIDs=" & GetAccredinCSV("Value")
        linkParams &= "&WOID=" & ViewState("WOID")
        linkParams &= "&WorkOrderID=" & ViewState("WorkOrderId")
        linkParams &= "&product=" & hdnProductIds.Value.Trim
        linkParams &= "&region=" & ViewState("Regions")
        linkParams &= "&PostCode=" & txtPostCode.Text
        linkParams &= "&Keyword=" & txtKeyword.Text
        linkParams &= "&Group=" & Request("Group")
        linkParams &= "&refcheck=" & chkRefChecked.Checked
        linkParams &= "&MinWOValue=" & IIf((chkMinWOValue.Checked = True), 1, 0)
        linkParams &= "&FavSupplier=" & IIf((chkFavSupplier.Checked = True), 1, 0)

        linkParams &= "&companyName=" & txtCompanyName.Text
        linkParams &= "&noOfEmployees=" & ddlNoOfEmployees.SelectedValue
        linkParams &= "&ShowSupplierWithCW=" & chkShowSupplierWithCW.Checked
        linkParams &= "&vendorIds=" & ViewState("vendorIds")

        linkParams &= "&mode=" & "edit"
        linkParams &= "&sender=" & "OrderMatch"
        linkParams &= "&PS=" & gvSupplier.PageSize
        linkParams &= "&PN=" & gvSupplier.PageIndex
        linkParams &= "&SC=" & gvSupplier.SortExpression
        linkParams &= "&SO=" & gvSupplier.SortDirection
        linkParams &= "&NearestTo=" & chkNearestTo.Checked
        linkParams &= "&Distance=" & ddlNearestTo.SelectedValue
        If chkCRBChecked.Checked = True Then
            linkParams &= "&crbChecked=Yes"
        ElseIf chkCRBChecked.Checked = False Then
            linkParams &= "&crbChecked=No"
        End If
        If chkUKSecurity.Checked = True Then
            linkParams &= "&ukSecurity=Yes"
        ElseIf chkUKSecurity.Checked = False Then
            linkParams &= "&ukSecurity=No"
        End If
        If chkCSCS.Checked = True Then
            linkParams &= "&CSCS=Yes"
        ElseIf chkCSCS.Checked = False Then
            linkParams &= "&CSCS=No"
        End If

        If chkExcelPartner.Checked = True Then
            linkParams &= "&ExcelPartner=Yes"
        ElseIf chkExcelPartner.Checked = False Then
            linkParams &= "&ExcelPartner=No"
        End If
        If chkEPCP.Checked = True Then
            linkParams &= "&EPCP=Yes"
        ElseIf chkEPCP.Checked = False Then
            linkParams &= "&EPCP=No"
        End If

        link &= "<a class=footerTxtSelected href='CompanyProfile.aspx?" & linkParams & "'><img src='Images/Icons/ViewDetails.gif' alt='View Details' width='16' height='16' hspace='2' vspace='0' border='0'></a>"
        Return link
    End Function

    ''' <summary>
    ''' Get the selected regions id
    ''' </summary>
    ''' <param name="columnName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getSelectedRegions(ByVal columnName As String) As String
        Dim strRegions = ""

        Dim dt As DataTable = UCRegions.saveListBox()
        For Each dr As DataRow In dt.Rows
            strRegions &= dr(columnName) & ","
        Next
        Return strRegions
    End Function

    Public Function GetAccredinCSV(ByVal columnName As String) As String
        Dim strAccred = ""

        Dim ds As DataSet = UCAccreditations.GetSelectedAccred()
        Dim dsContacts As New OWContacts()
        For Each drowAccreds As DataRow In ds.Tables(0).Rows
            If (columnName = "Text") Then
                If strAccred = "" Then
                    strAccred = drowAccreds("TagName").ToString
                Else
                    strAccred = strAccred & "," & drowAccreds("TagName").ToString
                End If
            ElseIf (columnName = "Both") Then
                If strAccred = "" Then
                    strAccred = drowAccreds("TagId").ToString & "#" & drowAccreds("TagName").ToString
                Else
                    strAccred = strAccred & "," & drowAccreds("TagId").ToString & "#" & drowAccreds("TagName").ToString
                End If
            Else
                If strAccred = "" Then
                    strAccred = drowAccreds("TagId").ToString
                Else
                    strAccred = strAccred & "," & drowAccreds("TagId").ToString
                End If
            End If
        Next
        Return strAccred
    End Function
    ''' <summary>
    ''' Get the selected vendor id
    ''' </summary>
    ''' <param name="columnName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Function getSelectedVendors(ByVal columnName As String) As String
    '    Dim strVendors = ""

    '    Dim dt As DataTable = UCCompanyAccreditations.saveListBox()
    '    For Each dr As DataRow In dt.Rows
    '        strVendors &= dr(columnName) & ","
    '    Next
    '    Return strVendors
    'End Function

    Public Function GetVendors(Optional ByVal killCache As Boolean = False)
        Dim ds As New DataSet

        ' this gets all vendors and selected vendors
        Dim CacheKey As String = "VendorsList" & Session.SessionID & Session("BizDivId")
        If killCache Then
            Cache.Remove(CacheKey)
        End If

        If Cache(CacheKey) Is Nothing Then
            ds = ws.WSContact.GetCompanyAccreditations(Session("BizDivId"), 0)
            Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        Else
            ds = CType(Page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function

    Public Sub RemoveCallback(ByVal tag As String, ByVal obj As Object, ByVal reason As CacheItemRemovedReason)
        If (CacheItemRemovedReason.Underused = reason) Then
            Cache.Remove(tag)
            If Not IsNothing(obj) Then
                Cache.Insert(tag, obj, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            End If
        Else
            Cache.Remove(tag)
        End If
    End Sub

    Public Sub Sort(ByVal sender As Object, ByVal e As System.EventArgs)
        If (ddlSortBy.SelectedValue <> "") Then
            SelectFromDB(ddlSortBy.SelectedValue, gvSupplier.PageIndex)
        End If
    End Sub
#End Region

    ''' <summary>
    ''' First, ShowHideElement() javascript funciton was used to implement this functionality. 
    ''' But as it had few issue, the following server side show hide code is written.
    ''' on check of the checkbox, the ddl would always get hidden irrespective of the status of checkbox. In short
    ''' the javascript function was not getting called on click of the ordermatch button. Tried calling the funciton onload of the aspx page
    ''' tried calling the funciton from the code like javascript:ShowDDLNearest('',''), but dint work.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub chkNearestTo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNearestTo.CheckedChanged
        If chkNearestTo.Checked = True Then
            divNearestTo.Visible = True
        ElseIf chkNearestTo.Checked = False Then
            divNearestTo.Visible = False
        End If
    End Sub

    Public Function getAOEBizDivIdFromRadioListBtn() As Integer
        Select Case rdAOEList.SelectedValue
            Case 1
                Return ApplicationSettings.OWUKBizDivId
            Case 2
                Return ApplicationSettings.SFUKBizDivId
        End Select
    End Function
    Public Sub rdAOEList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdAOEList.SelectedIndexChanged
        UCAOE1.BizDivId = getAOEBizDivIdFromRadioListBtn()
        UCAOE1.PopulateAOE()
    End Sub

    Public Function GetPostCodeString(ByVal postCode As Decimal) As String
        If postCode = 999999 Then
            Return "Invalid PostCode"
        Else
            Return Convert.ToString(postCode)
        End If
    End Function

    Private Sub gvSupplier_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvSupplier.Sorting
        If String.Compare(e.SortExpression, ViewState("_SortExp_"), True) = 0 Then
            If ViewState("_SortExp_") = e.SortExpression Then
                If ViewState("_Direction_") = "ASC" Then
                    ViewState("_Direction_") = "DESC"
                Else
                    ViewState("_Direction_") = "ASC"
                End If
            Else
                ViewState("_Direction_") = "ASC"
            End If
        Else
            ViewState("_Direction_") = "ASC"
        End If

        ViewState("_SortExp_") = e.SortExpression
        SelectFromDB(ViewState("_SortExp_") & " " & ViewState("_Direction_"))
    End Sub

    Private Sub gvSupplier_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSupplier.RowCommand
        If e.CommandName.Equals("Page") Then
            ViewState("PagingClick") = e.CommandArgument.ToString()
        End If
    End Sub

    Private Sub chkbxHideTestAccount_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkbxHideTestAccount.CheckedChanged
        SelectFromDB("")
    End Sub
End Class