Partial Public Class AdminWODetails
    Inherits System.Web.UI.Page

    Protected WithEvents UCAdminWODetails1 As UCAdminWODetails

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'lnkBackToListing.HRef = getBackToListingLink()
    End Sub

    Public Function getBackToListingLink() As String
        'If page is opened in a new page i.e. target=_BLANK then hide back to listing button
        If Not Request("HideBackToListing") Is Nothing Then
            tdBackToListing.Visible = False
            Return Nothing
        End If

        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender")
                Case "UCWOsListing", "UCWOsDetails", "CompanyProfile", "ucwoslisting", "ucwosdetails", "companyprofile", "UCWOsIssueListing"
                    If Not IsNothing(Request("Group")) Then
                        If Request("Group") = ApplicationSettings.WOGroupIssue Then
                            link = "~\AdminWOIssueListing.aspx?"
                        Else
                            link = "~\AdminWOListing.aspx?"
                        End If
                    Else
                        link = "~\AdminWOListing.aspx?"
                    End If
                    link &= "WOID=" & Request("WOID")
                    link &= "&ContactID=" & Request("ContactID")
                    link &= "&CompanyID=" & Request("CompanyID")

                    If Not IsNothing(Request("IsWatched")) Then
                        If Request("IsWatched").ToString = "1" Then
                            link &= "&Group=Watched"
                            link &= "&mode=Watched"
                        Else
                            If Not IsNothing(Request("Group")) Then
                                link &= "&Group=" & Request("Group")
                                link &= "&mode=" & Request("Group")
                            Else
                                link &= "&Group=" & Session("Group")
                                link &= "&mode=" & Session("Group")
                            End If
                        End If
                    Else
                        If Not IsNothing(Request("Group")) Then
                            link &= "&Group=" & Request("Group")
                            link &= "&mode=" & Request("Group")
                        Else
                            link &= "&Group=" & Session("Group")
                            link &= "&mode=" & Session("Group")
                        End If
                    End If
                    If Not IsNothing(Request("WorkorderID")) Then
                        link &= "&WorkorderID=" & Request("WorkorderID")
                    End If
                    If Not IsNothing(Request("SearchWorkorderID")) Then
                        link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
                    End If
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&CompID=" & Request("CompID")
                    link &= "&sender=" & "UCWOsListing"
                    If Not IsNothing(Request("IsNextDay")) Then
                        If Request("IsNextDay") = "Yes" Then
                            link &= "&IsNextDay=" & "Yes"
                        Else
                            link &= "&IsNextDay=" & "No"
                        End If
                    End If
                Case "SalesInvoiceGeneration"
                    link = "~\SalesInvoiceGeneration.aspx?"
                    link &= "&companyId=" & Request("companyId")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&bizDivId=" & Request("bizDivId")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=" & "SalesInvoiceGeneration"
                Case "SalesInvoiceWOListing"
                    link = "~\SalesInvoiceWOListing.aspx?"
                    link &= "invoiceNo=" & Request("invoiceNo")
                    link &= "&companyId=" & Request("companyId")
                    link &= "&bizDivId=" & Request("bizDivId")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&tab=" & Request("tab")
                    link &= "&sender=" & "Receivables"
                Case "PIUnpaidAvailable"
                    link = "PIUnpaidAvailable.aspx?"
                    link &= "invoiceNo=" & Request("invoiceNo")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&CompanyId=" & Request("CompanyId")
                    link &= "&Over30Days=" & Request("Over30Days")
                    link &= "&sender=" & "PIUnpaidAvailable"
                Case "UpSellSalesInvoicePaid", "upsellsalesinvoicepaid"
                    link = "UpSellSalesInvoicePaid.aspx?"
                    link &= "BizDivId=" & Request("BizDivId")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&mode=" & Request("Group")
                    link &= "&sender=" & "AdminWODetails"
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                Case "SearchWO"
                    link = "SearchWOs.aspx?"
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&SrcWorkorderID=" & Request("txtWorkorderID")
                    link &= "&SrcCompanyName=" & Request("CompanyName")
                    link &= "&SrcKeyword=" & Request("KeyWord")
                    link &= "&SrcPONumber=" & Request("PONumber")
                    link &= "&SrcReceiptNumber=" & Request("ReceiptNumber")
                    link &= "&SrcPostCode=" & Request("PostCode")
                    link &= "&SrcDateStart=" & Request("DateStart")
                    link &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
                    link &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")
                    link &= "&sender=AdminWODetails"
                Case "CustomerHistory"
                    link = "CustomerHistory.aspx?"
                    link &= "WOID=" & Request("WOID")
                    link &= "&txtWorkorderID=" & Request("txtWorkorderID")
                    link &= "&ContactID=" & Request("ContactID")
                    link &= "&CompanyID=" & Request("CompanyID")
                    link &= "&Group=" & Request("Group")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&mode=" & Request("Group")
                    link &= "&CompID=" & Request("CompID")
                    link &= "&sender=" & "UCWODetails"

                Case "SearchFinance"
                    link = "~\SearchFinance.aspx?"
                    link &= "SrcPONumber=" & Request("txtPONumber")
                    link &= "&SrcInvoiceNo=" & Request("txtInvoiceNo")
                    link &= "&SrcKeyword=" & Request("txtKeyword")
                    link &= "&SrcWorkOrderId=" & Request("txtWorkorderID")
                    link &= "&SrcCompanyName=" & Request("txtCompanyName")
                    link &= "&SrcInvoiceDate=" & Request("txtInvoiceDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=WorkOrderDetails"
                Case "AllInvoices"
                    link = "~\ViewAllInvoices.aspx?"
                    link &= "CompanyId=" & Request("CompanyId")
                    link &= "&sender=WorkOrderDetails"
                    'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                    link &= "&classId=" & Request("ClassID")
                Case "Rating"
                    link = "Rating.aspx?"
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    If Not IsNothing(Request("WorkorderID")) Then
                        link &= "&WorkorderID=" & Request("WorkorderID")
                    End If
                    If Not IsNothing(Request("SearchWorkorderID")) Then
                        link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
                    End If
                    link &= "&SelectedContact=" & Request("SelectedContact")
                    link &= "&Rating=" & Request("Rating")
                    If Not IsNothing(Request("BizDivID")) Then
                        If Request("BizDivID") <> "" Then
                            link &= "&BizDivID=" & Request("BizDivID")
                        Else
                            link &= "&BizDivID=" & ApplicationSettings.OWUKBizDivId
                        End If
                    Else
                        link &= "&BizDivID=" & ApplicationSettings.OWUKBizDivId
                    End If
                Case Else
                    Return ""
            End Select
        End If
        Return link
    End Function

    Private Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        lnkBackToListing.HRef = getBackToListingLink()
    End Sub
End Class