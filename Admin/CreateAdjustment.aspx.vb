
Partial Public Class CreateAdjustment
    Inherits System.Web.UI.Page
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents txtAdjustmentAmount As Global.System.Web.UI.WebControls.TextBox

    Protected WithEvents txtAdjustmntDate As Global.System.Web.UI.WebControls.TextBox

    Protected WithEvents ddlAdjustmentCategory As Global.System.Web.UI.WebControls.DropDownList

    Protected WithEvents ddlAdjustmentType As Global.System.Web.UI.WebControls.DropDownList

    Protected WithEvents chkIncludeVAT As Global.System.Web.UI.HtmlControls.HtmlInputCheckBox

    Shared ws As New WSObjs
    Protected WithEvents Check As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkShowAllMy As System.Web.UI.WebControls.CheckBox


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)


        If Not IsPostBack Then
            UCSearchContact1.BizDivID = Session("BizDivId")
            UCSearchContact1.ClassId = 0
            UCSearchContact1.Filter = "SalesPaidWithDiscount"
            UCSearchContact1.populateContact()
            ViewState!SortExpression = "InvoiceDate"
            gvInvoices.Sort(ViewState!SortExpression, SortDirection.Ascending)

            If Not IsNothing(Request("ContactID")) Then
                ViewState("ContactId") = Request("ContactID")
                UCSearchContact1.SelectedContact = Request("ContactID")
                UCSearchContact1.populateContact()
            Else
                ViewState("ContactId") = UCSearchContact1.ddlContact.SelectedValue
            End If

            If Not IsNothing(Request("InvoiceNo")) Then
                ViewState("InvoiceNo") = Request("InvoiceNo")
            Else
                ViewState("InvoiceNo") = ""
            End If

            lblMsgCheckBox.Visible = False

            tblAdjustmentFields.Visible = False
            ResetAdjustmentFields()
            PopulateGrid()
        End If
        If UCSearchContact1.ddlContact.SelectedValue = "" Then
            gvInvoices.Visible = False
        Else
            gvInvoices.Visible = True
        End If
    End Sub
    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then
            lblMsg.Text = ""
            ViewState("ContactId") = UCSearchContact1.ddlContact.SelectedValue
            ViewState("InvoiceNo") = ""
            ResetAdjustmentFields()
            lblMsgCheckBox.Visible = False
            PopulateGrid()
            divValidationMain.Visible = False
        End If
    End Sub

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                       ByVal maximumRows As System.Nullable(Of Integer)) As DataSet

        Dim ds As DataSet
        Dim bizDivId As Integer = Session("BizDivId")
        Dim contactid As String = ""
        Dim AdviceType As String = "Sales"
        Dim status As String = "Paid"
        If CStr(ViewState("ContactId")) = "" Then
            contactid = "0"
        Else
            contactid = ViewState("ContactId")
        End If
        Dim maxRows As Integer = 0
        If chkShowAllMy.Checked Then
            maxRows = 0
        Else
            maxRows = maximumRows
        End If

        ds = ws.WSFinance.MS_GetSalesReceiptAdviceListing(Session("BizDivId"), AdviceType, contactid, status, "", "", "ManualAdjustment", sortExpression, startRowIndex, maxRows, ViewState("InvoiceNo"), "")
        ViewState("rowCount") = ds.Tables("tblCount").Rows(0)(0)

        Return ds
    End Function
    Public Sub PopulateGrid()
        gvInvoices.DataBind()
        If gvInvoices.Rows.Count > 0 Then
            tblAdjustmentFields.Visible = True
        Else
            tblAdjustmentFields.Visible = False
        End If
    End Sub
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvInvoices.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvInvoices.DataBind()
    End Sub
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex

        ShowHideGrid()


    End Sub
    Public Sub ShowHideGrid()
        If UCSearchContact1.ddlContact.SelectedValue = "" Then
            gvInvoices.Visible = False
        Else
            gvInvoices.Visible = True

        End If
    End Sub
    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvInvoices.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvInvoices, e.Row, Me)
        End If
    End Sub
    Private Sub gvInvoices_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvInvoices.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True
            End If
        End If

    End Sub
    Private Sub gvInvoices_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvInvoices.DataBound
        'If Session("RoleGroupID") = 7 Then
        '    tblAdjustmentFields.Visible = True
        'Else
        '    tblAdjustmentFields.Visible = False
        'End If
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub

    Public Sub ManualAdjustment(ByVal sender As Object, ByVal e As EventArgs)
        If txtAdjustmentAmount.Text = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please enter a an adjustment amount."
            gvInvoices.DataBind()
            Return
        ElseIf txtAdjustmentAmount.Text = 0.0 Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please enter a non-zero adjustment amount."
            gvInvoices.DataBind()
            Return
        Else
            If txtAdjustmntDate.Text = "" Then
                divValidationMain.Visible = True
                lblMsg.Text = "Please enter date for Payment."
                gvInvoices.DataBind()
                Return
            Else
                Try
                    Dim tmpFromDate As DateTime = DateTime.Parse(txtAdjustmntDate.Text.Trim)
                Catch ex As Exception
                    divValidationMain.Visible = True
                    lblMsg.Text = "Please enter valid date for adjustment."
                    gvInvoices.DataBind()
                    Return
                End Try
                divValidationMain.Visible = False
                lblMsg.Text = ""
            End If
        End If

        lblMsg.Text = ""
        divValidationMain.Visible = False
        Dim invoices As String = CommonFunctions.getSelectedIdsOfGrid(gvInvoices, "Check", "hdnInvoiceNo")
        If invoices = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please Select atleast one unpaid sales invoice."
            gvInvoices.DataBind()
            Return
        End If
        If UCSearchContact1.ddlContact.SelectedValue = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please Select a Contact."
            gvInvoices.DataBind()
            Return
        Else
            ViewState("ContactId") = UCSearchContact1.ddlContact.SelectedValue
            lblMsg.Text = ""
            divValidationMain.Visible = False
        End If
        Dim ds As New DataSet
        ds = ws.WSFinance.MS_ManualAdjustment(Session("BizDivId"), invoices, ViewState("ContactId"), txtAdjustmntDate.Text, ddlAdjustmentType.SelectedValue, ddlAdjustmentCategory.SelectedValue, txtAdjustmentAmount.Text, chkIncludeVAT.Checked)
        If ds.Tables.Count > 0 Then
            If ds.Tables("tblSuccess").Rows(0).Item("Success") = 0 Then
                divValidationMain.Visible = True
                lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
                PopulateGrid()
            Else
                lblMsgCheckBox.Visible = True
                lblMsgCheckBox.Text = ResourceMessageText.GetString("AdjustmentSuccessful")
                lblMsgCheckBox.Text = lblMsgCheckBox.Text.Replace("<invoices>", invoices)
                ViewState("InvoiceNo") = ""
                PopulateGrid()
                ResetAdjustmentFields()
            End If
        End If
    End Sub

    Private Sub ResetAdjustmentFields()
        txtAdjustmentAmount.Text = ""
        txtAdjustmntDate.Text = ""
        chkIncludeVAT.Checked = False
    End Sub

    Private Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click
        ResetAdjustmentFields()
        gvInvoices.DataBind()
        lblMsgCheckBox.Visible = False
    End Sub
End Class