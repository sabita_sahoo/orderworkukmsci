Public Partial Class AutoMatchSettings
    Inherits System.Web.UI.Page

    Protected WithEvents UCTopMSgLabel1 As UCTopMSgLabel
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents UCAutoMatchSettings1 As UCAutoMatchSettings


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        'Security.SetPageControlsByAdmin(Me, GetContact(True, True, Session("ContactCompanyID")).Tables(15).Copy.DefaultView)

        'Security.GetPageControls(Page)
        If Not IsPostBack Then

            If Not IsNothing(Request("CompanyID")) Then
                If Request("CompanyID") <> "" Then
                    Session("CompanyID") = Request("CompanyID")
                End If
            End If

            ' Code to Show/Hide control for OW Admin
            'Dim pagename As String
            'pagename = CType(CType(Page.Request.Url, Object), System.Uri).AbsolutePath()
            ''pagename = pagename.ToLower.Substring(pagename.LastIndexOf("/") + 1, (pagename.Length - 1) - pagename.LastIndexOf("/"))
            'pagename = pagename.Substring(pagename.LastIndexOf("/") + 1, (pagename.Length - 1) - pagename.LastIndexOf("/"))
            'Dim dvpriv As DataView = UCMSCompanyProfile.GetContact(Me, Session("ContactBizDivID"), True, True, Session("ContactCompanyID")).Tables(15).Copy.DefaultView
            'dvpriv.RowFilter = "PageName Like('" & pagename & "') "
            'Security.GetPageControls(Page, dvpriv)

        End If

        UCAutoMatchSettings1.CompanyID = Request("CompanyID")

    End Sub

End Class