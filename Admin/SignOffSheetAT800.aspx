﻿<%@ Page Language="VB" AutoEventWireup="true" CodeBehind="SignOffSheetAT800.aspx.vb" Inherits="Admin.SignOffSheetAT800" ClientIDMode="Static" %>

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>OrderWork's AT800 Sign-Off Sheet</title>
    <meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes" />
    <link rel="stylesheet" media="all" type="text/css" href="~/Styles/JQUI.css" />
    <script type="text/javascript">

        var NoWOFoundMessage = "WorkOrder Not found. Check the WorkOrder Number Postcode and try again";
        window.onload = function () {
            var tmp = document.getElementById("StartNextButton");
            var tmp2 = document.getElementById("txtWorkOrderDetails");
            if (tmp != null) {
                if (tmp2 != null) {
                    if (tmp2.value == NoWOFoundMessage || tmp2.value == "") {
                        tmp.style.display = "none";
                    } else {
                        tmp.style.display = "block";
                    }
                }

            }
        };

        function SearchJob() {
            var txtWorkOrder = document.getElementById("txtWorkOrder").value;
            var txtPostcode = document.getElementById("txtPostCode").value;
            //OrderWorkLibrary.DBWorkOrder.SearchWorkOrder(txtWorkOrder.Text, txtPostCode.Text, Session("PortalCompanyId"));
            PageMethods.SearchWorkOrder(txtWorkOrder, txtPostcode, SuccessFunction, FailedFunction);

        }
        function SuccessFunction(ReturnValue) {
            if (ReturnValue == "") {
                ReturnValue = NoWOFoundMessage
                document.getElementById('StartNextButton').style.display = "none";
            } else {
                document.getElementById('StartNextButton').style.display = "block";
            }
            document.getElementById("txtWorkOrderDetails").value = ReturnValue;
            var pnlBackListing = document.getElementById("pnlBackListing");

            if (pnlBackListing != null) {
                if (ReturnValue.indexOf("workorder not found") != -1)
                    pnlBackListing.style.display = "block";
            }
        }
        function FailedFunction(ReturnValue) {
            document.getElementById("txtWorkOrderDetails").value = ReturnValue;
            var pnlBackListing = document.getElementById("pnlBackListing");
            if (pnlBackListing != null) {
                if (ReturnValue.indexOf("workorder not found") == -1)
                    pnlBackListing.style.visibility = 'visible';
            }
        }
        function ClearAndDisableText(TextElement) {
            var txtReason = TextElement;
            if (txtReason) {
                txtReason.value = "If No, please enter the reason why:";
                txtReason.disabled = true;
            }
        }
        function EnableText(TextElement) {
            var txtReason = TextElement;
            if (txtReason) {
                txtReason.disabled = false;
            }
        }
        function TextBoxLabel(theTextControl) {
            var txtLabel = theTextControl.getAttribute("title");
            if (txtLabel) {
                if (theTextControl.value == txtLabel) {
                    theTextControl.value = "";
                }
            }
        }
        function TextBoxLabelOut(theTextControl) {
            var txtLabel = theTextControl.value;
            if (txtLabel == "") {
                theTextControl.value = theTextControl.getAttribute("title");

            }
        }
    </script>
    <style type="text/css">
        .wzdStyle
        {
            padding: 1px 1px 1px 1px;
            border: 1px solid black;
            background-color: #EFF8FF;
            max-width: 700px;
            max-height: 500px;
            width: 100%;
            height: 100%;
        }
        #wzd_StartNavigationTemplateContainerID_StartNextButton, #wzd_StepNavigationTemplateContainerID_StepNextButton, #wzd_StepNavigationTemplateContainerID_StepPreviousButton, #wzd_FinishNavigationTemplateContainerID_FinishPreviousButton, #wzd_StepNavigationTemplateContainerID_FinishPreviousButton, #wzd_FinishNavigationTemplateContainerID_FinishButton
        {
            font-weight: bold;
            font-size: 1.1em;
            margin: 5px 0px 5px 0px;
        }
        .StepPanel
        {
            border: 1px solid black;
            background-color: #F9F9F9;
            width: 97%;
            height: 80%;
            font-size: 1.0em;
            font-weight: normal;
            padding-left: 1%;
            margin-left: 1%;
        }
        .StepHeader
        {
            font-weight: bold;
            font-size: 1.1em;
            margin: 5px 0px 5px 0px;
            padding-left: 10px;
            display: inline-block;
        }
        .StepNoCurrent
        {
            display: inline-block;
            background-color: #FF0000;
            font-weight: bold;
            border: 1px solid black;
            padding: 0px 5px 0px 5px;
            margin: 5px 5px 5px 5px;
            border-radius: 100%;
        }
        .StepNoComplete
        {
            display: inline-block;
            background-color: Black;
            color: White;
            font-weight: bold;
            border: 1px solid black;
            padding: 0px 5px 0px 5px;
            margin: 5px 5px 5px 5px;
            border-radius: 100%;
        }
        .StepNoToDo
        {
            display: inline-block;
            background-color: #F9F9F9;
            border: 1px solid black;
            padding: 0px 5px 0px 5px;
            margin: 5px 5px 5px 5px;
            border-radius: 100%;
        }
        .SectionLabel
        {
            color: white;
            font-weight: bold;
            background-color: Black;
            margin: 5px 0px 3px 0px;
            width: 98%;
            padding-left: 5px;
            border: 1px solid black;
        }
        .lblAbove
        {
            padding-left: 5px;
            display: block;
            margin-top: 6px;
        }
        .SingleLineText
        {
            display: block;
            padding-left: 5px;
            font-weight: lighter;
            width: 80%;
            font-size: 1.0em;
            color: Gray;
            margin-bottom: 5px;
        }
        .MoreHeight
        {
            height: 100px;
        }
        .EvenMoreHeight
        {
            height: 200px;
        }
        .ErrorCss
        {
            display: inline-block;
            font-weight: bold;
            color: Red;
            padding-left: 10px;
        }
    </style>
</head>
<body>
    <form runat="server" id="RegisterationForm">
    <asp:Panel ID="pnlBackListing" runat="server" style="display:none;">
        <table width="54%">
            <tr>
                <td align="left">
                    <asp:Button runat="server" ID="btnHome" Text="Another Sign Off Sheet"></asp:Button>
                </td>
                <td align="right">
                    <asp:Button runat="server" ID="btnDashBoard" Text="Go to DashBoard"></asp:Button>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True">
    </asp:ScriptManager>
    <asp:Wizard ID="wzd" runat="server" ActiveStepIndex="1" CssClass="wzdStyle">
        <HeaderStyle CssClass="SideBarList" />
        <HeaderTemplate>
            <div style="float: left">
                <%= wzd.ActiveStep.Title%>
            </div>
            <div style="float: right">
                <asp:Repeater ID="SideBarList" runat="server">
                    <ItemTemplate>
                        <span class="<%# GetClassForWizardStep(Container.DataItem) %>">
                            <%# wzd.WizardSteps.IndexOf(Container.DataItem) + 1%></span>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div style="clear: both">
            </div>
        </HeaderTemplate>
        <WizardSteps>
            <asp:WizardStep ID="WizardStep0" runat="server" Title="<span class='StepHeader'>Sign-Off Sheet</span>">
                <div class="StepPanel">
                    <div class="SectionLabel">
                        WorkOrder Search</div>
                    <div>
                        <div class="lblAbove">
                            <asp:Label runat="server" AssociatedControlID="txtWorkOrder" ID="lblWorkOrder">WorkOrder Number:</asp:Label>
                            <OWFormFieldValidator runat="server" ID="OWFormFieldValidator1" CssClass="ErrorCss" ControlToValidate="txtWorkOrder" TypeOfValidator="Number" ErrorMessage="&nbsp;&nbsp;* WorkOrder number incorrect"/>
                        </div>
                        <asp:TextBox runat="server" CssClass="SingleLineText" onfocus="TextBoxLabel(this);"
                            onblur="TextBoxLabelOut(this);" ID="txtWorkOrder" ToolTip="Enter WorKOrder No:"></asp:TextBox>
                    </div>
                    <div>
                        <div class="lblAbove">
                            <asp:Label runat="server" AssociatedControlID="txtPostCode" ID="lblPostCode">PostCode of Job:</asp:Label>
                            <OWFormFieldValidator runat="server" ID="OWFormFieldValidator2" CssClass="ErrorCss" ControlToValidate="txtPostCode" TypeOfValidator="PostCode" ErrorMessage="&nbsp;&nbsp;* PostCode incorrect"/>
                        </div>
                        <asp:TextBox runat="server" CssClass="SingleLineText" onfocus="TextBoxLabel(this);"
                            onblur="TextBoxLabelOut(this);" ID="txtPostCode" ToolTip="Enter PostCode"></asp:TextBox>
                    </div>
                    <div>
                        <asp:Button runat="server" ID="btnJobSearch" OnClientClick="SearchJob(); return false;"
                            Text="Click to Search for Job" />
                    </div>
                    <div class="SectionLabel">
                        WorkOrder Details:</div>
                    <div>
                        <asp:TextBox runat="server" CssClass="SingleLineText MoreHeight" ID="txtWorkOrderDetails"
                            TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStep1" runat="server" Title="<span class='StepHeader'>Details</span>">
                <div class="StepPanel">
                    <div class="SectionLabel">
                        Problem</div>
                    <div>
                        <asp:Label runat="server" CssClass="lblAbove" AssociatedControlID="Installer_Return_Code"
                            ID="lblInstaller_Return_Code">Has the Problem been fixed?</asp:Label>
                        <asp:DropDownList runat="server" CssClass="SingleLineText" ID="Installer_Return_Code">
                            <asp:ListItem Value="0">Yes</asp:ListItem>
                            <asp:ListItem Value="1">No: Householder not present</asp:ListItem>
                            <asp:ListItem Value="2">No: Specialist installer team required</asp:ListItem>
                            <asp:ListItem Value="3">No: Installer unable to resolve</asp:ListItem>
                            <asp:ListItem Value="4">No: Platform change required</asp:ListItem>
                            <asp:ListItem Value="5">No: Bespoke solution required</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div>
                        <asp:CheckBox runat="server" class="lblAbove" ID="ProblemNot4G" Text="Check this if the Problem was not 4G related?"
                            TextAlign="Left"></asp:CheckBox>
                    </div>
                    <div class="SectionLabel">
                        Filter Fitted</div>
                    <div>
                        <asp:Label runat="server" CssClass="lblAbove" AssociatedControlID="Filt_Type" ID="lblFilt_Type">What type if filter was fitted?</asp:Label>
                        <asp:DropDownList runat="server" CssClass="SingleLineText" ID="Filt_Type">
                            <asp:ListItem Value="0">None</asp:ListItem>
                            <asp:ListItem Value="1">Consumer 59 Indoor</asp:ListItem>
                            <asp:ListItem Value="2">Consumer 59E Indoor</asp:ListItem>
                            <asp:ListItem Value="3">Consumer 59 Outdoor</asp:ListItem>
                            <asp:ListItem Value="4">Consumer 60 Indoor</asp:ListItem>
                            <asp:ListItem Value="5">Consumer 60 Outdoor</asp:ListItem>
                            <asp:ListItem Value="6">Communal 59 Outdoor</asp:ListItem>
                            <asp:ListItem Value="7">Communal 60 Outdoor</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div>
                        <asp:Label runat="server" CssClass="lblAbove" AssociatedControlID="Filt_Manuf" ID="lblFilt_Manuf">What brand of filter was fitted?</asp:Label>
                        <asp:DropDownList runat="server" CssClass="SingleLineText" ID="Filt_Manuf">
                            <asp:ListItem Value="0">None</asp:ListItem>
                            <asp:ListItem Value="1">Filtronic</asp:ListItem>
                            <asp:ListItem Value="2">Link Microtek</asp:ListItem>
                            <asp:ListItem Value="3">Philex - Labgear</asp:ListItem>
                            <asp:ListItem Value="4">Radio Design</asp:ListItem>
                            <asp:ListItem Value="5">Televes</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStep2" runat="server" Title="<span class='StepHeader'>Additional information</span>">
                <div class="StepPanel">
                    <div class="SectionLabel">
                        Your Notes</div>
                    <OWFormFieldValidator runat="server" ID="OWFormFieldValidator3" CssClass="ErrorCss" ControlToValidate="txtInstaller_Notes" TypeOfValidator="CharactersWithPunctuation" ErrorMessage="&nbsp;&nbsp;* Please remove any special characters"/>
                    <asp:TextBox runat="server" CssClass="SingleLineText EvenMoreHeight" ID="txtInstaller_Notes"
                        TextMode="MultiLine" onfocus="TextBoxLabel(this);" onblur="TextBoxLabelOut(this);"
                        ToolTip="Enter any notes here"></asp:TextBox>
                </div>
            </asp:WizardStep>
        </WizardSteps>
    </asp:Wizard>
    </form>
    <asp:Panel runat="server" CssClass="wzdStyle" ID="CompletePanel" Visible="false">
        <span class="StepHeader">Complete</span>
        <div class="StepPanel">
            <div style="padding: 10px 10px 10px 10px; font-size: 1.2em;">
                <p>
                    Thank you for completing the online sign-off sheet for job Number:</p>
                <asp:Literal runat="server" ID="litWorkOrder1"></asp:Literal>
                <p>
                    OrderWork will now process this and close the job as soon as possible</p>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" CssClass="wzdStyle" ID="FailedPanel" Visible="false">
        <span class="StepHeader">Failed!</span>
        <div class="StepPanel">
            <div style="padding: 10px 10px 10px 10px; font-size: 1.2em;">
                <p>
                    Unfortunately an error has occurred with updating the sign-off sheet for</p>
                <asp:Literal runat="server" ID="litWorkOrder2"></asp:Literal>
                <p>
                    Please try again later, if it continues to produce this error message, please contact
                    OrderWork on:
                </p>
            </div>
        </div>
    </asp:Panel>
</body>
</html>
