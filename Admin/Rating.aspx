<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Rating" Codebehind="Rating.aspx.vb" Inherits="Admin.Rating" %>

<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <%@ Register Assembly="WebLibrary" Namespace="WebLibrary" TagPrefix="lib" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
	<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
	
	<%@ Import Namespace="System.Web.Script.Services" %>
    <%@ Import Namespace="System.Web.Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript">
    function validate_required_Rating(field, alerttxt)
    {
        var text = document.getElementById(field).value;
        if (text==null || text=="" || text=="Enter a comment here")
          
                {alert(alerttxt);
               }
            else {
                document.getElementById("ctl00_ContentPlaceHolder1_hdnButton").click();            
            }
    }
</script>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      
                    <div id="divContent">
                        <div class="roundtopWhite">
                            <img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner"
                                style="display: none" /></div>
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
                            <tr>
                                <td valign="top">
                                    <!-- InstanceBeginEditable name="EdtContent" -->
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                                        <ContentTemplate>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10px" height="30px" valign="bottom">
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Ratings" runat="server"></asp:Label></td>
                                                </tr>
                                            </table>
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td width="10px">
                                                    </td>
                                                    <td width="300">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td width="350">
                                                                    <%@ register tagprefix="uc1" tagname="UCDateRange" src="~/UserControls/UK/UCDateRangeUK.ascx" %>
                                                                    <uc1:UCDateRange id="UCDateRange1" runat="server">
                                                                    </uc1:UCDateRange>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="180">
                                                        <%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
						                                <uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact>
                                                    </td>
                                                     <td align="left" width="80px;" style="padding-right:10px;">
		                                                <lib:Input ID="txtWorkorderID" runat="server" DataType="List" Method="GetWorkOrderID" CssClass="formField150"/>
                                                        <cc2:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtWorkorderID" WatermarkText="Work Order ID" WatermarkCssClass="formField150"></cc2:TextBoxWatermarkExtender>        
			                                         </td>	
                                                    <td width="150">
                                                        <asp:DropDownList runat="server" ID="ddFilterRating" CssClass="formFieldGrey215" style="width:120px;">
                                                            <asp:ListItem Selected="True" Value="1">Show all</asp:ListItem>
                                                            <asp:ListItem Value="0">Show all neutral</asp:ListItem>
                                                            <asp:ListItem Value="-1">Show all negative</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" width="60px">
                                                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                                            <asp:LinkButton ID="lnkView" CausesValidation="False" OnClick="lnkView_Click" runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
                                                        </div>
                                                    </td>
                                                    <td width="20px">
                                                    </td>
                                                    <td align="left">
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                            <hr style="background-color:#993366;height:5px;margin:0px;" />
                                            <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                <asp:GridView ID="gvRating" runat="server" AllowPaging="True" AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize="25" BorderColor="White" PagerSettings-Mode="NextPreviousFirstLast" PagerSettings-Position="TopAndBottom" Width="100%" DataSourceID="ObjectDataSource1" AllowSorting="true">
                                                    <EmptyDataTemplate>
                                                        <table width="100%" border="0" cellpadding="10" cellspacing="0">
                                                            <tr>
                                                                <td align="center" style="text-align: center" valign="middle" height="300" class="txtWelcome">
                                                                    Sorry! There are no records in this category.
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </EmptyDataTemplate>
                                                    <PagerTemplate>
                                                        <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9"
                                                            id="tblMainTop" style="visibility: visible" runat="server">
                                                            <tr>
                                                                <td runat="server" id="tdsetType1" width="90">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="90">
                                                                        <tr>
                                                                            <td class="txtListing" align="left">
                                                                                <div id="divButton" style="width: 115px;">
                                                                                    <div class="bottonTopGrey">
                                                                                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                                                                            height="4" class="btnCorner" style="display: none" /></div>
                                                                                    <a runat="server" target="_blank" id="btnExport" tabindex="6" class="buttonText"><strong>
                                                                                        Export to Excel</strong></a>
                                                                                    <div class="bottonBottomGrey">
                                                                                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                                                                            height="4" class="btnCorner" style="display: none" /></div>
                                                                                </div>
                                                                            </td>
                                                                            <td width="5">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td width="10">
                                                                    &nbsp;</td>
                                                                <td id="tdsetType2" align="left" width="121">
                                                                </td>
                                                                <td>
                                                                    <table width="100%" id="tblTop" runat="server" border="0" cellpadding="0" cellspacing="0"
                                                                        bgcolor="#DAD8D9">
                                                                        <tr>
                                                                            <td align="right">
                                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td align="right" valign="middle">
                                                                                            <div id="tdTotalCount" style="width: 200px; padding-right: 20px; text-align: right"
                                                                                                runat="server" class="formLabelGrey">
                                                                                            </div>
                                                                                        </td>
                                                                                        <td align="right" width="136" valign="middle" id="tdPagerUp" runat="server">
                                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td align="right" width="36">
                                                                                                        <div id="divDoubleBackUp" runat="server" style="float: left; vertical-align: bottom;  padding-top: 2px;">
                                                                                                            <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
                                                                                                        </div>
                                                                                                        <div id="divBackUp" runat="server" style="float: right; vertical-align: bottom; padding-top: 2px;">
                                                                                                            <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                    <td width="50" align="center" style="margin-right: 3px;">
                                                                                                        <asp:DropDownList Style="margin-left: 4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true"></asp:DropDownList>
                                                                                                    </td>
                                                                                                    <td width="30" valign="bottom">
                                                                                                        <div id="divNextUp" runat="server" style="float: left; vertical-align: bottom; padding-top: 2px;">
                                                                                                            <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
                                                                                                        </div>
                                                                                                        <div id="divDoubleNextUp" runat="server" style="float: right; vertical-align: bottom;  padding-top: 2px;">
                                                                                                            <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                    <Columns>
                                                        <asp:TemplateField SortExpression="Rating" HeaderText="Rating" HeaderStyle-CssClass="gridHdr gridText" ItemStyle-Width="5%" ItemStyle-Height="20px">
                                                            <ItemStyle HorizontalAlign="Center" CssClass="gridRow" Wrap="true" Width="5%" />
                                                            <ItemTemplate>
                                                                <img id="ratingIcon" align="middle" runat="server" src='<%#ShowRatingIcon(Container.dataitem("Rating"))%>' width="15" height="19" border="0"  />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="8%" HeaderStyle-CssClass="gridHdr gridText" DataField="RefWOID" SortExpression="RefWOID" HeaderText="WO No" />
                                                        
                                                        <asp:TemplateField SortExpression="DateSubmitted" HeaderText="Submitted" ItemStyle-Width="10%"  HeaderStyle-CssClass="gridHdr gridText" ItemStyle-Height="30px">
                                                            <ItemStyle CssClass="gridRow" Wrap="true" />
                                                            <ItemTemplate>
                                                                <%#Strings.FormatDateTime(Container.DataItem("DateSubmitted"), DateFormat.ShortDate)%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField SortExpression="DateClosed" HeaderText="Closed" ItemStyle-Width="10%"  HeaderStyle-CssClass="gridHdr gridText" ItemStyle-Height="30px">
                                                            <ItemStyle CssClass="gridRow" Wrap="true" />
                                                            <ItemTemplate>
                                                                <%# strings.formatDateTime(Container.DataItem("DateClosed"), DateFormat.ShortDate)%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText" DataField="Client" SortExpression="Client" HeaderText="Client" />
                                                        <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText" DataField="Supplier" SortExpression="Supplier" HeaderText="Supplier" />
                                                        

                                                        
                                                        <asp:TemplateField   SortExpression="Comment" HeaderStyle-CssClass="gridHdr gridText" HeaderText="Comments">
                                                            <ItemStyle CssClass="gridRow" Wrap="true"  />
                                                            <ItemTemplate>
                                                                 
                                                                  <asp:panel runat="server" ID="pnlCommentFromClient" visible='<%#IIf(Container.DataItem("Comments") = "", False, True)%>'>
                                                                 <span class='discussionTextClient'> <%# Strings.FormatDateTime(Container.DataItem("DateClosed"), DateFormat.ShortDate) & " - " & (Container.DataItem("CommentBy")) & "'s Comment"%>  </span> : <%# Container.DataItem("Comments") %> <br />   
                                                                 </asp:panel>
                                                                <asp:DataList ID="rptDiscussion" Runat="server">
											                            <ItemTemplate>
												                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
												                            <tr>
												                              <td width="200" valign="baseline"><span class='discussionTextSupplier'><%#Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - " & (Container.DataItem("CommentBy")) & "'s Comment"%>  </span> </td>
												                              <td width="20" align="center" valign="top">:</td>
												                              <td valign="baseline" class="remarksAreaCollapse"> <%# Container.DataItem("Comments") %> </td>
												                            </tr>
											                              </table>     									      
                            				                        </ItemTemplate>
										                         </asp:DataList>
                                                             </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        
                                                        <asp:TemplateField  HeaderStyle-CssClass="gridHdr gridText" ItemStyle-Height="30px" ItemStyle-Width="8%" >
                                                            <ItemStyle CssClass="gridRow" Wrap="true" />
                                                            <ItemTemplate>
                                                            <a runat=server id="lnkDetail" href= '<%#"~/AdminWODetails.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("WorkOrderID") & "&" & "SearchWorkorderID=" & txtWorkorderID.Text & "&" & "Group=Closed&Rating=" & ddFilterRating.SelectedValue & "&FromDate=" & UCDateRange1.txtFromDate.Text &"&ToDate=" & UCDateRange1.txtToDate.Text & "&CompanyID=" & Container.DataItem("BuyerCompanyId") & "&ContactId=" & Container.DataItem("BuyerContactId")   & "&SupCompId=&SelectedContact=" &  UCSearchContact1.ddlContact.SelectedValue & "&PS=25&PN=0&SC=DateCreated&SO=DateCreated&BizDivID=1&sender=Rating" %>' ><img src="Images/Icons/ViewDetails.gif" title="View Details" width="16" height="16" hspace="2" vspace="0" border="0"></a>
                                                            <a runat=server id="lnkViewHistory" href= '<%#"~/AdminWOHistory.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("WorkOrderID") & "&" & "SearchWorkorderID=" & txtWorkorderID.Text & "&" & "Group=Closed&Rating=" & ddFilterRating.SelectedValue & "&FromDate=" & UCDateRange1.txtFromDate.Text &"&ToDate=" & UCDateRange1.txtToDate.Text & "&CompanyID=&SupCompId=&SelectedContact=" &  UCSearchContact1.ddlContact.SelectedValue & "&PS=25&PN=0&SC=DateCreated&SO=DateCreated&BizDivID=1&sender=Rating"  %>'><img src="Images/Icons/ViewHistory.gif" alt="View Details" title="View History" hspace="2" vspace="0" border="0"></a> 
                                                            <asp:LinkButton ID="lnkbtnUpdateRating" CommandName="WOID" CommandArgument='<%#Container.dataitem("WOID") %>' runat="server"><img id="imgUpdateRating" runat="server" src="~/Images/Icons/Update_rating.gif" title="Edit Rating" width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" /></asp:LinkButton>
                                                        </ItemTemplate>
                                                         </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="gridRow" />
                                                    <RowStyle CssClass="gridRow" />
                                                    <PagerStyle BorderWidth="0px" BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top" Height="40px" />
                                                    <FooterStyle BackColor="#D7D7D7" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
                                                SelectMethod="SelectFromDB" EnablePaging="True" SelectCountMethod="SelectCount"
                                                TypeName="Admin.Rating" SortParameterName="sortExpression"></asp:ObjectDataSource>
                                                
                                                        <cc2:ModalPopupExtender ID="mdlUpdateRating" runat="server" TargetControlID="btnTemp" PopupControlID="pnlUpdateWO" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False"></cc2:ModalPopupExtender>
                                                            <asp:Panel runat="server" ID="pnlUpdateWO"  Width="300px" CssClass="pnlPopUp" style="display:none;">
                                                            <div><b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b></div>
                                                            <div id="divModalPopUpParent">
                                                                     <b>Edit ratig of the workorder </b><br />  
                                                                     <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnPositive" runat="server" Text="Positive" />
                                                                     <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNeutral"  runat="server"  Text="Neutral"/>
                                                                     <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNegative" runat="server" Text="Negative" /><br /><br />
                                                                     
                                                                     <b>Add comment here</b><br />
                                                                     <asp:TextBox runat="server" ID="txtComment" Height="50" Width="275" CssClass="bodyTextGreyLeftAligned" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox>        
                                                                    <cc2:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtComment" WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc2:TextBoxWatermarkExtender>        
                                                                 <div id="divButton" class="divButton" style="width: 85px; float:left;">
                                                                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                                                                        <a class="buttonText cursorHand" href='javascript:validate_required_Rating("ctl00_ContentPlaceHolder1_txtComment","Please enter a comment")' id="ancSubmit"><strong>Submit</strong></a>
                                                                    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                                                                </div>
                                                                <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                                                                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                                                                        <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                                                                    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                                                                </div> 
                                                                <div style="clear:both;"></div>
                                                            </div>
                                                             <div><b class="b4"></b><b class="b3"></b><b class="b2"></b><b class="b1"></b></div>
                                                            </asp:Panel>
                                                            <asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click"  Height="0" Width="0" BorderWidth="0" />
                                                            <asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                                        <ProgressTemplate>
                                            <div>
                                                <img align="middle" src="Images/indicator.gif" />
                                                Loading ...
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />    
                                        
                                    
                                    <!-- InstanceEndEditable -->
                                    
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                    </asp:Content>
                

