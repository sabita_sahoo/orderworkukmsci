﻿<%@ Page  Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="SageExportData.aspx.vb" Inherits="Admin.SagePayExport" Title="Sage Pay Export" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
   <style  type="text/css">
   .colorwhitered
   {
       color:White;
       text-decoration:none;
   }
    .colorwhitered:hover
   {
       color:#F6CF1C;
       text-decoration:none;
   }
   </style>
    <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
       
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10px"></td>
			<td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Sage Pay Export" runat="server"></asp:Label></td>
			</tr>
            <tr style="height:20px">
			<td width="10px"></td>
			<td > </td>
			</tr>
            </tr>
            <tr style="margin-left:5px">
			<td width="10px"></td>
			<td><asp:DropDownList CssClass="formField"  ID="ddlsagepaymode" runat="server">
             <asp:ListItem Text="Customers" Value="Customers"> </asp:ListItem>
             <asp:ListItem Text="Suppliers" Value="ServicePartners"> </asp:ListItem>
             <asp:ListItem Text="Sales Invoices" Value="SalesInvoices"> </asp:ListItem>
             <asp:ListItem Text="Credit Notes" Value="CreditNotes"> </asp:ListItem>
             <asp:ListItem Text="Purchase Invoices" Value="PurchaseInvoices"> </asp:ListItem>
             <asp:ListItem Text="Debit Notes" Value="DebitNotes"> </asp:ListItem>
             <asp:ListItem Text="Payments" Value="Payments"> </asp:ListItem>
            
            
            </asp:DropDownList></td>
			</tr>
			</table>			
		<div style=" margin-top:20px;"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
		<td width="10px"></td>
		<td width="300"><table width="100%" cellpadding="0" cellspacing="0" border="0">
			 <tr>
			   <td width="300">
			     <%@ Register TagPrefix="uc1" TagName="UCDateRange" Src="~/UserControls/UK/UCDateRangeUK.ascx" %>
                 <uc1:UCDateRange id="UCDateRange1" runat="server"></uc1:UCDateRange> 
			   </td>
			  </tr>
			</table>
		</td>
		<td align="left">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                <a runat="server" style=" cursor:pointer; text-decoration:none;"     CssClass="txtButtonRed " OnClick="ExportToexcel" id="btnExport">&nbsp;<span class="colorwhitered">Export to Excel</span>&nbsp;</a>
            </div>
		</td>
			 <td align="left">
		</td>
		</tr>
		</table></div>
           </tr>
         </table>
      </div>
      </asp:Content>

