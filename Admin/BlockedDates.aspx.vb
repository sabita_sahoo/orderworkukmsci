﻿Public Class BlockedDates
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        If hdnContactID.Value <> "" Then
            PopulateDetails()
        End If
    End Sub

    Private Sub PopulateDetails()

        Dim ds As DataSet
        ds = CommonFunctions.GetCreateWorkRequestDetails(0, 0, CInt(hdnContactID.Value))
        Dim dv_loc As New DataView(ds.Tables("tblLocDetails"))

        hdnServiceSelected.Value = "0"
        drpdwnBillingLocation.Visible = True
        lblBillingLocation.Visible = True
        lblLocationMandatory.Visible = True
        lblServiceMandatory.Visible = True
        lnkViewBlockDates.Visible = True
        tblViewDates.Visible = True



        dv_loc.RowFilter = "IsBilling = 1"
        drpdwnBillingLocation.DataSource = dv_loc.ToTable
        drpdwnBillingLocation.DataBind()

        Dim liBillLoc As New ListItem
        liBillLoc = New ListItem
        liBillLoc.Text = "Select billing location"
        liBillLoc.Value = ""
        drpdwnBillingLocation.Items.Insert(0, liBillLoc)


        PopulateServices()

    End Sub

    Private Sub PopulateServices()
        Dim dsProductsList As DataSet
        dsProductsList = ws.WSWorkOrder.GetProductListing((hdnContactID.Value), 1, "Sequence", 0, 0, 0, False)

        If dsProductsList.Tables(0).Rows.Count > 0 Then
            'drpdwnProduct.Visible = True
            'lblService.Visible = True
            'Dim liProducts As New ListItem
            'liProducts = New ListItem
            'liProducts.Text = "Select Service"
            'liProducts.Value = "0"

            'drpdwnProduct.DataSource = dsProductsList.Tables(0)
            'drpdwnProduct.DataTextField = "ProductName"
            'drpdwnProduct.DataValueField = "ProductID"
            'drpdwnProduct.DataBind()
            'drpdwnProduct.Items.Insert(0, liProducts)

            tblServices.Visible = True

            If (hdnServiceSelected.Value Is Nothing Or hdnServiceSelected.Value = "") Then
                hdnServiceSelected.Value = "0"
            End If

            'Populating other selected product listbox
            Dim dvProductsSel As DataView = dsProductsList.Tables(0).Copy.DefaultView
            dvProductsSel.RowFilter = "ProductID IN (" & hdnServiceSelected.Value & ")"
            listServiceSel.DataSource = dvProductsSel
            listServiceSel.DataBind()

            Dim dvProducts As DataView = dsProductsList.Tables(0).Copy.DefaultView
            dvProducts.RowFilter = "ProductID NOT IN (" & hdnServiceSelected.Value & ")"
            dvProducts.Sort = "ProductName"
            listService.DataSource = dvProducts
            listService.DataBind()
        Else
            'drpdwnProduct.Visible = False
            tblServices.Visible = False
        End If
    End Sub

    Private Sub btnViewBlockDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewBlockDate.Click
        Dim BillingId As Integer
        Dim ServiceListValue As String
        Dim TimeSlot As String

        BillingId = CInt(drpdwnBillingLocation.SelectedValue)
        If tblServices.Visible = False Then
            ServiceListValue = ""
        Else
            ServiceListValue = hdnServiceSelected.Value
        End If


        PopulateCalender(CInt(hdnContactID.Value), BillingId, ServiceListValue)
        PopulateServices()
        calendar.Visible = True
    End Sub
    Private Sub PopulateCalender(ByVal ContactId As Integer, ByVal BillingId As Integer, ByVal ServiceListValue As String)
        divDummy.Visible = False
        Dim currentdate As String
        currentdate = "test"
        Dim ds As DataSet
        ds = Admin.CommonFunctions.AddUpdateBlockedDate(ContactId, BillingId, currentdate, True, True, ServiceListValue, "", currentdate)

        'Populate JS for Locations Data
        Dim strarr As New StringBuilder
        Dim i As Integer
        Dim dvBlockDate As DataView = ds.Tables("Success").DefaultView

        strarr.Append("BlockDateArr = new Array();" & Chr(13))
        For i = 0 To dvBlockDate.Count - 1
            strarr.Append("BlockDateArr[" & i + 1 & "] = new Array();" & Chr(13))
            strarr.Append("BlockDateArr[" & i + 1 & "][0] = '" & (dvBlockDate.Item(i).Item("Title")).ToString.Replace("'", "") & "';")
            strarr.Append("BlockDateArr[" & i + 1 & "][1] = '" & (dvBlockDate.Item(i).Item("BlockDay")).ToString.Replace("'", "") & "';")
            strarr.Append("BlockDateArr[" & i + 1 & "][2] = '" & (dvBlockDate.Item(i).Item("BlockMonth")).ToString.Replace("'", "") & "';")
            strarr.Append("BlockDateArr[" & i + 1 & "][3] = '" & (dvBlockDate.Item(i).Item("BlockYear")).ToString.Replace("'", "") & "';")
            strarr.Append("BlockDateArr[" & i + 1 & "][4] = '" & (dvBlockDate.Item(i).Item("Description")).ToString.Replace("'", "") & "';")
            strarr.Append("BlockDateArr[" & i + 1 & "][5] = '" & (dvBlockDate.Item(i).Item("PartialServices")).ToString.Replace("'", "") & "';")
            strarr.Append("BlockDateArr[" & i + 1 & "][6] = '" & i + 1 & "';")
            strarr.Append("BlockDateArr[" & i + 1 & "][7] = '" & (dvBlockDate.Item(i).Item("DateBlockedTimeSlot")).ToString.Replace("'", "") & "';")
        Next

        litBlockDate.Text = strarr.ToString
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "js", "setCalender();", True)
    End Sub
    Private Sub AddUpdateBlockedDate(ByVal TimeSlot As String, ByVal IsBlocked As Boolean, ByVal BillingId As Integer, ByVal blockdate As String, ByVal ServiceListValue As String)

        Dim dsBlockedDate As DataSet = Admin.CommonFunctions.AddUpdateBlockedDate(CInt(hdnContactID.Value), BillingId, blockdate, IsBlocked, True, ServiceListValue, TimeSlot, "AddRemove")

    End Sub

    Private Sub btnSaveBlockDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveBlockDate.Click
        Dim blockdate As String
        blockdate = appointdate.Value.Replace("-", "/")
        Dim IsBlocked As Boolean

        Dim BillingId As Integer
        Dim ServiceListValue As String


        BillingId = CInt(drpdwnBillingLocation.SelectedValue)
        If tblServices.Visible = False Then
            ServiceListValue = ""
        Else
            ServiceListValue = hdnServiceSelected.Value
        End If
        If (chkMorningSlot.Checked) Then
            'If (hdnTimeSlot.Value <> "Both" And hdnTimeSlot.Value <> "8am - 1pm") Then
            '    AddUpdateBlockedDate("8am - 1pm", True, BillingId, blockdate, ServiceListValue)
            'End If
            AddUpdateBlockedDate("8am - 1pm", True, BillingId, blockdate, ServiceListValue)
        Else
            AddUpdateBlockedDate("8am - 1pm", False, BillingId, blockdate, ServiceListValue)
        End If
        If (chkEveningSlot.Checked) Then
            'If (hdnTimeSlot.Value <> "Both" And hdnTimeSlot.Value <> "1pm - 6pm") Then
            '    AddUpdateBlockedDate("1pm - 6pm", True, BillingId, blockdate, ServiceListValue)
            'End If
            AddUpdateBlockedDate("1pm - 6pm", True, BillingId, blockdate, ServiceListValue)
        Else
            AddUpdateBlockedDate("1pm - 6pm", False, BillingId, blockdate, ServiceListValue)
        End If
        PopulateCalender(CInt(hdnContactID.Value), BillingId, ServiceListValue)
    End Sub
End Class