

Partial Public Class AMContacts
    Inherits System.Web.UI.Page
    Protected WithEvents ObjectDataSource1 As ObjectDataSource

    ''' <summary>
    ''' Shared wsobject to access web service webmethods
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared ws As New WSObjs

    Protected WithEvents UCDateRange1 As UCDateRange

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Following session variable added to fix the issue: Admin Company profile back to listing does not work
        Session("CompanyProfileQS") = ""
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)

            Dim bizDivId As Integer
            Dim contactType As String

            If Not IsNothing(Request("bizDivId")) Then
                If Request("bizDivId") <> "" Then
                    bizDivId = Request("bizDivId")
                Else

                    bizDivId = getDefaultBizDiv()
                End If
            Else
                bizDivId = getDefaultBizDiv()
            End If
            ViewState("bizDivId") = bizDivId


            If Not IsNothing(Request("contactType")) Then
                If Request("contactType") <> "" Then
                    contactType = Request("contactType")
                Else
                    contactType = ApplicationSettings.ContactType.approvedBuyer
                End If
            Else
                contactType = ApplicationSettings.ContactType.approvedBuyer
            End If

            '   this session is used to know , what is clicked by admin. so that it can be used on other pages.
            Session("contactType") = contactType

            hdnContactType.Value = contactType
            If Trim(Request("message")) <> "" Then
                lblMsg.Text = Trim(Request("message"))
            End If


            'Show hide colums in the data grid as per the listing
            Select Case contactType
                Case ApplicationSettings.ContactType.approvedBuyer
                    'ConfirmBtnSuspend.ConfirmText = "Do you want to change the buyer type?"
                    lblMessage.Text = "Approved Cients"
                    gvContacts.Columns(5).Visible = True 'contact info
                    gvContacts.Columns(6).Visible = True 'location
                    gvContacts.Columns(9).Visible = False  'rating
                    gvContacts.Columns(10).Visible = True 'Source   
                    ViewState("userType") = "Buyer"
                Case ApplicationSettings.ContactType.suspendedBuyer
                    ' ConfirmBtnApprove.ConfirmText = "Do you want to change the buyer type?"
                    lblMessage.Text = "Suspended Cients"
                    gvContacts.Columns(5).Visible = True 'contact info
                    gvContacts.Columns(6).Visible = True 'location
                    gvContacts.Columns(9).Visible = False  'rating
                    gvContacts.Columns(10).Visible = True 'Source
                    ViewState("userType") = "Buyer"
                Case ApplicationSettings.ContactType.newSupplier
                    lblMessage.Text = "Non Reference Checked Suppliers"
                    gvContacts.Columns(5).Visible = True 'contact info
                    gvContacts.Columns(6).Visible = True 'location
                    gvContacts.Columns(9).Visible = True 'rating
                    gvContacts.Columns(10).Visible = True 'rating
                    ViewState("userType") = "Supplier"
                Case ApplicationSettings.ContactType.approvedSupplier
                    lblMessage.Text = "Approved Suppliers"
                    gvContacts.Columns(8).HeaderText = " Approved On"
                    gvContacts.Columns(5).Visible = True 'contact info
                    gvContacts.Columns(6).Visible = True 'location
                    gvContacts.Columns(8).Visible = True ' ActionDate
                    gvContacts.Columns(9).Visible = True ' rating
                    gvContacts.Columns(10).Visible = True 'Source
                    ViewState("userType") = "Supplier"
                Case ApplicationSettings.ContactType.suspendedSupplier
                    lblMessage.Text = "Suspended Suppliers"
                    gvContacts.Columns(8).HeaderText = "Suspended On"
                    gvContacts.Columns(5).Visible = True 'contact info
                    gvContacts.Columns(6).Visible = True 'location
                    gvContacts.Columns(8).Visible = True 'ActionDate
                    gvContacts.Columns(9).Visible = True 'rating
                    gvContacts.Columns(10).Visible = True 'Source
                    ViewState("userType") = "Supplier"
                Case ApplicationSettings.ContactType.deletedAccount
                    lblMessage.Text = "Deleted Accounts"
                    gvContacts.Columns(0).Visible = False
                    gvContacts.Columns(1).Visible = False
                    gvContacts.Columns(2).Visible = True
                    gvContacts.Columns(3).Visible = True 'Company name
                    gvContacts.Columns(4).Visible = True 'Name
                    gvContacts.Columns(5).Visible = True 'Phone
                    gvContacts.Columns(7).Visible = True 'Type
                    gvContacts.Columns(8).Visible = True 'Action Date
                    gvContacts.Columns(8).HeaderText = "Deleted On"
                    gvContacts.Columns(10).Visible = False 'Source
                Case Else

            End Select

            If Trim(Request("PS")) <> "" Then
                If IsNumeric(Trim(Request("PS"))) = True Then
                    If CType(Trim(Request("PS")), Integer) <> 0 Then
                        hdnPageSize.Value = CType(Trim(Request("PS")), Integer)
                    End If
                End If
            End If
            hdnPageNo.Value = 0
            If Trim(Request("PN")) <> "" Then
                If IsNumeric(Trim(Request("PN"))) = True Then
                    If CType(Trim(Request("PN")), Integer) <> 0 Then
                        hdnPageNo.Value = CType(Trim(Request("PN")), Integer)
                    End If
                End If
            End If

            ViewState!SortExpression = ""
            If Trim(Request("SC")) <> "" Then
                ViewState!SortExpression = Trim(Request("SC"))
            Else
                ViewState!SortExpression = "DateCreated"
            End If
            Dim sd As SortDirection
            sd = SortDirection.Descending
            If Trim(Request("SO")) <> "" Then
                If Trim(Request("SO")).ToLower = "true" Then
                    sd = SortDirection.Ascending
                End If
            End If

            Dim currDate As DateTime = System.DateTime.Today
            UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(DateTime.Now.AddDays(-30), DateFormat.ShortDate)'Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            If Trim(Request("fromDate")) <> "" Then
                UCDateRange1.txtFromDate.Text = Request("fromDate")
            End If
            UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
            If Trim(Request("toDate")) <> "" Then
                UCDateRange1.txtToDate.Text = Request("toDate")
            End If
            'Initialize dates
            ViewState("fromDate") = UCDateRange1.txtFromDate.Text
            ViewState("toDate") = UCDateRange1.txtToDate.Text
            gvContacts.PageSize = hdnPageSize.Value
            gvContacts.Sort(ViewState!SortExpression, sd)
            PopulateGrid()
            gvContacts.PageIndex = hdnPageNo.Value
        End If

        lblMsg.Text = ""
    End Sub

    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvContacts.DataBind()
            btnExport.HRef = "ExportToExcel.aspx?page=AMContacts&bizDivId=" & ViewState("bizDivId") & "&fromDate=" & ViewState("fromDate") & "&toDate=" & ViewState("toDate") & "&mainClassId=" & ViewState("mainClassId") & "&statusId=" & ViewState("statusId") & "&status=" & ViewState("status") & "&dateCriteriaField=" & ViewState("dateCriteriaField") & "&sortExpression=" & gvContacts.SortExpression & "&startRowIndex=0&maximumRows=0"
        End If

    End Sub

    ''' <summary>
    ''' sets the text for the confirm button. 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub setConfirmButtonText(ByVal gvPagerRow As GridViewRow)
        CType(gvPagerRow.FindControl("ConfirmBtnApprove"), AjaxControlToolkit.ConfirmButtonExtender).ConfirmText = "Do you want to change the " & ViewState("userType") & " type?"
        CType(gvPagerRow.FindControl("ConfirmBtnSuspend"), AjaxControlToolkit.ConfirmButtonExtender).ConfirmText = "Do you want to change the " & ViewState("userType") & " type?"
        CType(gvPagerRow.FindControl("ConfirmBtnNew"), AjaxControlToolkit.ConfirmButtonExtender).ConfirmText = "Do you want to change the " & ViewState("userType") & " type?"
    End Sub

#Region "Grid Functions"

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet

        Dim rowCount As Integer
        Dim bizDivId As Integer = ViewState("bizDivId")
        Dim fromDate As String = Strings.FormatDateTime(ViewState("fromDate"), DateFormat.ShortDate)
        Dim toDate As String = Strings.FormatDateTime(ViewState("toDate"), DateFormat.ShortDate)

        Dim mainClassId As String
        Dim statusId As String
        Dim status As String
        Dim dateCriteriaField As String

        Dim contactType As ApplicationSettings.ContactType = CType(Master.FindControl("ContentPlaceHolder1").FindControl("hdncontactType"), HtmlInputHidden).Value


        Select Case contactType

            Case ApplicationSettings.ContactType.approvedBuyer
                mainClassId = ApplicationSettings.RoleClientID
                statusId = ApplicationSettings.StatusApprovedCompany
                dateCriteriaField = ""
                status = ""
            Case ApplicationSettings.ContactType.suspendedBuyer
                mainClassId = ApplicationSettings.RoleClientID
                statusId = ApplicationSettings.StatusSuspendedCompany
                dateCriteriaField = ""
                status = ""
            Case ApplicationSettings.ContactType.newSupplier
                mainClassId = ApplicationSettings.RoleSupplierID
                statusId = ApplicationSettings.StatusNewCompany
                dateCriteriaField = ""
                status = ""
            Case ApplicationSettings.ContactType.approvedSupplier
                mainClassId = ApplicationSettings.RoleSupplierID
                statusId = ApplicationSettings.StatusApprovedCompany
                dateCriteriaField = "ApprovedDate"
                status = ""
            Case ApplicationSettings.ContactType.suspendedSupplier
                mainClassId = ApplicationSettings.RoleSupplierID
                statusId = ApplicationSettings.StatusSuspendedCompany
                dateCriteriaField = "SuspendedDate"
                status = ""
            Case ApplicationSettings.ContactType.deletedAccount
                mainClassId = 0
                statusId = ApplicationSettings.StatusDeletedCompany
                dateCriteriaField = "DeletedDate"
                status = ""
            Case Else
                mainClassId = ApplicationSettings.RoleClientID
                statusId = ApplicationSettings.StatusApprovedCompany
                dateCriteriaField = ""
                status = ""
        End Select
        ViewState("mainClassId") = mainClassId
        ViewState("fromDate") = fromDate
        ViewState("toDate") = toDate
        ViewState("statusId") = statusId
        ViewState("status") = status
        ViewState("dateCriteriaField") = dateCriteriaField
        Dim ds As DataSet = ws.WSContact.GetContactListing(bizDivId, mainClassId, fromDate, toDate, statusId, dateCriteriaField, status, sortExpression, startRowIndex, maximumRows, rowCount)
        ViewState("rowCount") = rowCount
        HttpContext.Current.Items("tblWOUserNotes") = ds.Tables(2)
        Return ds
    End Function

    ''' <summary>
    ''' ObjectDataSource Controls select Count Method which returns the row count for the listing 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Private Sub gvContacts_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvContacts.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
                'top pager buttons
                Dim tdApproveTop As HtmlTableCell = CType(pagerRowTop.FindControl("tdApprove"), HtmlTableCell)
                Dim tdSuspendTop As HtmlTableCell = CType(pagerRowTop.FindControl("tdSuspend"), HtmlTableCell)
                Dim tdNewTop As HtmlTableCell = CType(pagerRowTop.FindControl("tdNew"), HtmlTableCell)
                Select Case CType(Master.FindControl("ContentPlaceHolder1").FindControl("hdncontactType"), HtmlInputHidden).Value
                    Case ApplicationSettings.ContactType.approvedBuyer
                        tdApproveTop.Visible = False
                        tdSuspendTop.Visible = True
                        tdNewTop.Visible = False
                    Case ApplicationSettings.ContactType.suspendedBuyer
                        tdApproveTop.Visible = True
                        tdSuspendTop.Visible = False
                        tdNewTop.Visible = False
                    Case ApplicationSettings.ContactType.newSupplier

                        tdApproveTop.Visible = False
                        tdSuspendTop.Visible = True
                        tdNewTop.Visible = False
                    Case ApplicationSettings.ContactType.approvedSupplier
                        tdApproveTop.Visible = False
                        tdSuspendTop.Visible = True
                        tdNewTop.Visible = True
                    Case ApplicationSettings.ContactType.suspendedSupplier
                        tdApproveTop.Visible = False
                        tdSuspendTop.Visible = False
                        tdNewTop.Visible = True
                    Case ApplicationSettings.ContactType.deletedAccount
                        tdApproveTop.Visible = False
                        tdSuspendTop.Visible = False
                        tdNewTop.Visible = False
                        Dim chkSelectAll As CheckBox = CType(pagerRowTop.FindControl("chkSelectAll"), CheckBox)
                        chkSelectAll.Visible = False
                    Case Else
                End Select

            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

                'bottom pager buttons
                Dim tdApproveBottom As HtmlTableCell = CType(pagerRowBottom.FindControl("tdApprove"), HtmlTableCell)
                Dim tdSuspendBottom As HtmlTableCell = CType(pagerRowBottom.FindControl("tdSuspend"), HtmlTableCell)
                Dim tdNewBottom As HtmlTableCell = CType(pagerRowBottom.FindControl("tdNew"), HtmlTableCell)
                Dim chkSelectAll As CheckBox = CType(pagerRowBottom.FindControl("chkSelectAll"), CheckBox)

                tdApproveBottom.Visible = False
                tdSuspendBottom.Visible = False
                tdNewBottom.Visible = False
                chkSelectAll.Visible = False
            End If
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowDataBound
        MakeGridViewHeaderClickable(gvContacts, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvContacts, e.Row, Me)
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)



        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
        setConfirmButtonText(gvPagerRow)
    End Sub

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        gvContacts.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

#End Region

#Region "Get Functions"

    Public Function GetUserType(ByVal paramClassId As Object) As String
        Dim classId As Integer = -1
        If Not IsNothing(paramClassId) Then
            If IsNumeric(paramClassId) Then
                classId = paramClassId
            End If
        End If

        If classId = ApplicationSettings.RoleClientID Then
            Return "Buyer"
        End If
        If classId = ApplicationSettings.RoleSupplierID Then
            Return "Supplier"
        End If
        Return ""
    End Function

    ''' <summary>
    ''' Returns the links column which has different images and links associated with it
    ''' </summary>
    ''' <param name="parambizDivId"></param>
    ''' <param name="companyId"></param>
    ''' <param name="contactId"></param>
    ''' <param name="classId"></param>
    ''' <param name="paramstatusId"></param>
    ''' <param name="roleGroupId"></param>
    ''' <param name="Status"></param>
    ''' <param name="paramactionDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLinks(ByVal parambizDivId As Integer, ByVal companyId As Integer, ByVal contactId As Integer, ByVal classId As Integer, ByVal paramstatusId As Object, ByVal roleGroupId As Integer, ByVal Status As String, ByVal Email As String, ByVal CompanyName As String, ByVal Name As String, ByVal paramActionDate As Object) As String
        Dim link As String = ""
        Dim contactType As ApplicationSettings.ContactType = CType(Master.FindControl("ContentPlaceHolder1").FindControl("hdncontactType"), HtmlInputHidden).Value
        Dim actionDate As String = ""
        Dim bizDivId = ViewState("bizDivId")
        Dim pg As String
        Dim statusId As Integer = -1
        If Not IsDBNull(paramstatusId) Then
            If IsNumeric(paramstatusId) Then
                statusId = paramstatusId
            End If
        End If
        If Not IsDBNull(paramActionDate) Then
            actionDate = paramActionDate
        End If

        Select Case contactType
            Case ApplicationSettings.ContactType.deletedAccount
                If classId = ApplicationSettings.RoleClientID Then
                    link &= "<a href='companyprofilebuyer.aspx?sender=AdminContactsListing&bizDivId=" & bizDivId & "&bizdiv=uk" & "&contactType=" & contactType & "&companyid=" & companyId & "&userid=" & contactId & "&classid=" & classId & "&rolegroupid=" & roleGroupId & "&mode=edit&ps=" & gvContacts.PageSize & "&pn=" & gvContacts.PageIndex & "&sc=" & gvContacts.SortExpression & "&so=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & "'><img src='images/icons/viewdetails.gif' alt='view details' width='16' height='16' hspace='2' vspace='0' border='0'></a>"
                End If
                If classId = ApplicationSettings.RoleSupplierID Then
                    link &= "<a href='companyprofile.aspx?sender=AdminContactsListing&bizDivId=" & bizDivId & "&contactType=" & contactType & "&companyid=" & companyId & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&rolegroupid=" & roleGroupId & "&mode=edit&ps=" & gvContacts.PageSize & "&pn=" & gvContacts.PageIndex & "&sc=" & gvContacts.SortExpression & "&so=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & "'><img src='images/icons/viewdetails.gif' alt='view details' width='16' height='16' hspace='2' vspace='0' border='0'></a>"
                End If
            Case ApplicationSettings.ContactType.approvedBuyer, ApplicationSettings.ContactType.suspendedBuyer
                link &= "<a href='companyprofilebuyer.aspx?sender=AdminContactsListing&bizDivId=" & bizDivId & "&bizdiv=uk" & "&contactType=" & contactType & "&companyid=" & companyId & "&userid=" & contactId & "&classid=" & classId & "&rolegroupid=" & roleGroupId & "&mode=edit&ps=" & gvContacts.PageSize & "&pn=" & gvContacts.PageIndex & "&sc=" & gvContacts.SortExpression & "&so=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & "'><img src='images/icons/viewdetails.gif' alt='view details' width='16' height='16' hspace='2' vspace='0' border='0'></a>"
                'link &= "<a href='addfunds.aspx?sender=AdminContactsListing&companyname=" & CompanyName & "&companyId=" & companyId & "&contactname=" & Name & "&contactId=" & companyId & " & bizDivId = " & bizDivId & " & contactType = " & contactType & " & ps = " & gvContacts.PageSize & " & pn = " & gvContacts.PageIndex & " & sc = " & gvContacts.SortExpression & " & so = " & gvContacts.SortDirection & " & fromDate = " & UCDateRange1.txtFromDate.Text & " & toDate = " & UCDateRange1.txtToDate.Text & " '><img src='images/icons/addfunds.gif' alt='add funds' width='11' height='16' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
                'link &= "<a href='AccountStatement.aspx?sender=AdminContactsListing&bizDivId=" & bizDivId & "&contactType=" & contactType & "&companyid=" & companyId & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&rolegroupid=" & roleGroupId & "&mode=edit&ps=" & gvContacts.PageSize & "&pn=" & gvContacts.PageIndex & "&sc=" & gvContacts.SortExpression & "&so=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & "'><img src='Images/Icons/AccountStatement.gif' alt='Account Statement' width='15' height='14' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
                link &= "<a href='Invoices.aspx?sender=AdminContactsListing&CompanyID=" & companyId & "&contactType=" & contactType & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&adviceFor=" & ApplicationSettings.ViewerBuyer & "&ps=" & gvContacts.PageSize & "&pn=" & gvContacts.PageIndex & "&sc=" & gvContacts.SortExpression & "&so=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & "'><img src='images/icons/View-Sales-Invoice.gif' alt='View Sales Invoices' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
            Case ApplicationSettings.ContactType.newSupplier, ApplicationSettings.ContactType.approvedSupplier, ApplicationSettings.ContactType.suspendedSupplier

                link &= "<a href='ReferenceCheckForm.aspx?sender=AdminContactsListing&CompanyID=" & companyId & "&UserId=" & contactId & "&statusId=" & statusId & "&bizDivId=" & bizDivId & "&contactType=" & contactType & "&ClassID=" & classId & "&Email=" & Email & "&Name=" & Name & "&PS=" & gvContacts.PageSize & "&PN=" & gvContacts.PageIndex & "&SC=" & gvContacts.SortExpression & "&SO=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & " '><img src='Images/Icons/Icon-RefCheck.gif' alt='Reference Check' align='Top' width='14' height='13' hspace='2' vspace='0' border='0'></a>"

                link &= "<a href='companyprofile.aspx?sender=AdminContactsListing&bizDivId=" & bizDivId & "&contactType=" & contactType & "&companyid=" & companyId & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&rolegroupid=" & roleGroupId & "&mode=edit&ps=" & gvContacts.PageSize & "&pn=" & gvContacts.PageIndex & "&sc=" & gvContacts.SortExpression & "&so=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & "'><img src='images/icons/viewdetails.gif' alt='view details' width='16' height='16' hspace='2' vspace='0' border='0'></a>"
                If Status.ToLower = "aktiv" Or Status.ToLower = "active" Then
                    If contactType = ApplicationSettings.ContactType.newSupplier Or contactType = ApplicationSettings.ContactType.suspendedSupplier Then
                        link &= "<a href='ReferenceCheckForm.aspx?sender=AdminContactsListing&CompanyID=" & companyId & "&UserId=" & contactId & "&statusId=" & statusId & "&bizDivId=" & bizDivId & "&contactType=" & contactType & "&ClassID=" & classId & "&Email=" & Email & "&Name=" & Name & "&PS=" & gvContacts.PageSize & "&PN=" & gvContacts.PageIndex & "&SC=" & gvContacts.SortExpression & "&SO=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & " '><img src='Images/Icons/Icon-RefCheck.gif' alt='Reference Check' align='Top' width='14' height='13' hspace='2' vspace='0' border='0'></a>"
                    Else
                        link &= "<a href='ReferenceCheckForm.aspx?sender=AdminContactsListing&CompanyID=" & companyId & "&UserId=" & contactId & "&statusId=" & statusId & "&ApprovedOn=" & actionDate & "&bizDivId=" & bizDivId & "&contactType=" & contactType & "&ClassID=" & classId & "&Email=" & Email & "&Name=" & Name & "&PS=" & gvContacts.PageSize & "&PN=" & gvContacts.PageIndex & "&SC=" & gvContacts.SortExpression & "&SO=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & " '><img src='Images/Icons/Icon-RefCheck.gif' alt='Reference Check' align='Top' width='14' height='13' hspace='2' vspace='0' border='0'></a>"
                    End If
                End If
                'If contactType = ApplicationSettings.ContactType.newSupplier Or contactType = ApplicationSettings.ContactType.approvedSupplier Then
                '    link &= "<a href='AddFunds.aspx?sender=AdminContactsListing&companyname=" & CompanyName & "&companyId=" & companyId & "&contactname=" & Name & "&ContactID=" & companyId & "&bizDivId=" & bizDivId & "&contactType=" & contactType & "&PS=" & gvContacts.PageSize & "&PN=" & gvContacts.PageIndex & "&SC=" & gvContacts.SortExpression & "&SO=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & "'><img src='Images/Icons/AddFunds.gif' alt='Add Funds' width='11' height='16' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
                'End If

                Dim dc As Decimal
                dc = ws.WSWorkOrder.CheckBalance("Supplier", companyId, bizDivId)
                If dc > 0 Then

                    If contactType = ApplicationSettings.ContactType.newSupplier Then
                        pg = "NonApp"
                    ElseIf contactType = ApplicationSettings.ContactType.approvedSupplier Then
                        pg = "App"
                    ElseIf contactType = ApplicationSettings.ContactType.suspendedSupplier Then
                        pg = "Suspend"
                    End If
                    link &= "<a href='WithdrawFunds.aspx?sender=AdminContactsListing&companyname=" & CompanyName & "&companyId=" & companyId & "&contactname=" & Name & "&ContactID=" & companyId & "&bizDivId=" & bizDivId & "&contactType=" & contactType & "&pg=" & pg & "&PS=" & gvContacts.PageSize & "&PN=" & gvContacts.PageIndex & "&SC=" & gvContacts.SortExpression & "&SO=" & gvContacts.SortDirection & "'><img src='Images/Icons/WithdrawFunds.gif' alt='Withdraw Funds' width='11' height='14' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
                End If
                link &= "<a href='Invoices.aspx?sender=AdminContactsListing&CompanyID=" & companyId & "&contactType=" & contactType & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&adviceFor=" & ApplicationSettings.ViewerBuyer & "&ps=" & gvContacts.PageSize & "&pn=" & gvContacts.PageIndex & "&sc=" & gvContacts.SortExpression & "&so=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & "'><img src='images/icons/View-Sales-Invoice.gif' alt='View Sales Invoices' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
                link &= "<a href='SupplierInvoices.aspx?sender=AdminContactsListing&bizDivId=" & bizDivId & "&contactType=" & contactType & "&CompanyID=" & companyId & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&adviceFor=" & ApplicationSettings.ViewerSupplier & "&ps=" & gvContacts.PageSize & "&pn=" & gvContacts.PageIndex & "&sc=" & gvContacts.SortExpression & "&so=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & "'><img src='Images/Icons/View-Purchase-Invoice.gif' alt='View Purchase Invoices' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        End Select

        'Create work request link
        Select Case contactType
            Case ApplicationSettings.ContactType.approvedBuyer
                link &= "<a href='woform.aspx?sender=AdminContactsListing&bizDivId=" & bizDivId & "&bizdiv=uk" & "&contactType=" & contactType & "&companyid=" & companyId & "&contactid=" & contactId & "&classid=" & classId & "&rolegroupid=" & roleGroupId & "&mode=edit&ps=" & gvContacts.PageSize & "&pn=" & gvContacts.PageIndex & "&sc=" & gvContacts.SortExpression & "&so=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & "'><img src='images/icons/Create-New-WO.gif' alt='Create New Work Request' width='14' height='14' hspace='2' vspace='0' border='0'></a>"
            Case ApplicationSettings.ContactType.newSupplier, ApplicationSettings.ContactType.approvedSupplier
                link &= "<a href='woform.aspx?sender=AdminContactsListing&bizDivId=" & bizDivId & "&bizdiv=uk" & "&contactType=" & contactType & "&companyid=" & companyId & "&contactid=" & contactId & "&classid=" & classId & "&rolegroupid=" & roleGroupId & "&mode=edit&ps=" & gvContacts.PageSize & "&pn=" & gvContacts.PageIndex & "&sc=" & gvContacts.SortExpression & "&so=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text & "'><img src='images/icons/Create-New-WO.gif' alt='Create New Work Request' width='14' height='14' hspace='2' vspace='0' border='0'></a>"
        End Select

        Return link
    End Function

    Public Function GetSource(ByVal source As Object) As String
        If Not IsDBNull(source) Then
            Return CStr(source)
        Else
            Return "NA"
        End If
    End Function

    Private Function getDefaultBizDiv() As Integer
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryUK
                Return ApplicationSettings.OWUKBizDivId
            Case ApplicationSettings.CountryDE
                Return ApplicationSettings.OWDEBizDivId
        End Select
    End Function

#End Region

#Region "Actions"

    ''' <summary>
    ''' populates the list of contacts
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnView.Click
        Page.Validate()
        If Page.IsValid Then
            ViewState("fromDate") = UCDateRange1.txtFromDate.Text
            ViewState("toDate") = UCDateRange1.txtToDate.Text
            lblMsg.Text = ""
            PopulateGrid()
            gvContacts.PageIndex = 0
        End If
    End Sub

#End Region

#Region "Status Update"

    ''' <summary>
    ''' Function to set the company status to approve - applicable only to buyers
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ApproveContact(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMsg.Text = ""
        Dim mainContactIds As String = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnMainContactId") 'getSelectedMainContactIds()
        If mainContactIds = "" Then
            lblMsg.Text = ResourceMessageText.GetString("SelectContact") '"Please select  contact to change the status"
            Return
        End If
        Dim strcontactType As String = ""
        Dim str As String = ""
        strcontactType = hdnContactType.Value
        str = ResourceMessageText.GetString("SendMailTo")
        If strcontactType = ApplicationSettings.ContactType.approvedBuyer Or strcontactType = ApplicationSettings.ContactType.suspendedBuyer Then
            str.Replace("<type>", "Buyer(s)")   ' "Do you wish to send email to the following Buyers?"
        Else
            str.Replace("<type>", "Supplier(s)")     '"Do you wish to send email to the following Suppliers?"
        End If
        litMessage.Text = str

        ConfirmMailSend(mainContactIds, ApplicationSettings.StatusApprovedCompany)

        Session("page") = "AMContacts.aspx"

    End Sub

    ''' <summary>
    ''' Function to set the company status to suspended
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub SuspendContact(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMsg.Text = ""
        Dim mainContactIds As String = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnMainContactId")  'getSelectedMainContactIds()
        If mainContactIds = "" Then
            lblMsg.Text = ResourceMessageText.GetString("SelectContact")
            Return
        End If
        Dim strcontactType As String = ""
        Dim str As String = ""
        strcontactType = hdnContactType.Value
        str = ResourceMessageText.GetString("SendMailTo")
        If strcontactType = ApplicationSettings.ContactType.approvedBuyer Or strcontactType = ApplicationSettings.ContactType.suspendedBuyer Then
            str.Replace("<type>", "Buyer(s)")   ' "Do you wish to send email to the following Buyers?"
        Else
            str.Replace("<type>", "Supplier(s)")     '"Do you wish to send email to the following Suppliers?"
        End If
        litMessage.Text = str

        ConfirmMailSend(mainContactIds, ApplicationSettings.StatusSuspendedCompany)

        Session("page") = "AMContacts.aspx"
        'Response.Redirect("ConfirmMailSend.aspx?bizDivId=" & ViewState("bizDivId") & "&contactType=" & hdnContactType.Value & "&SelectedSupp=" & mainContactIds & "&ClassID=" & ApplicationSettings.StatusSuspendedCompany & "&PN=" & hdnPageNo.Value & "&PS=" & hdnPageSize.Value & "&SC=" & gvContacts.SortExpression & "&SO=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text)

    End Sub

    ''' <summary>
    ''' Function to set the company status to unapprove
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub NewContact(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMsg.Text = ""
        Dim mainContactIds As String = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnMainContactId")
        If mainContactIds = "" Then
            lblMsg.Text = ResourceMessageText.GetString("SelectContact")
            Return
        End If
        Dim strcontactType As String = ""
        strcontactType = hdnContactType.Value
        Dim str As String = ""
        str = ResourceMessageText.GetString("SendMailTo")
        If strcontactType = ApplicationSettings.ContactType.approvedBuyer Or strcontactType = ApplicationSettings.ContactType.suspendedBuyer Then
            str.Replace("<type>", "Buyer(s)")   ' "Do you wish to send email to the following Buyers?"
        Else
            str.Replace("<type>", "Supplier(s)")     '"Do you wish to send email to the following Suppliers?"
        End If
        litMessage.Text = str

        ConfirmMailSend(mainContactIds, ApplicationSettings.StatusNewCompany)

        Session("page") = "AMContacts.aspx"
        'Response.Redirect("ConfirmMailSend.aspx?bizDivId=" & ViewState("bizDivId") & "&contactType=" & hdnContactType.Value & "&SelectedSupp=" & mainContactIds & "&ClassID=" & ApplicationSettings.StatusNewCompany & "&PN=" & hdnPageNo.Value & "&PS=" & hdnPageSize.Value & "&SC=" & gvContacts.SortExpression & "&SO=" & gvContacts.SortDirection & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & UCDateRange1.txtToDate.Text)

    End Sub

    ''' <summary>
    ''' Update the status - and show confirmation screen for send mail
    ''' </summary>
    ''' <param name="SelectedSupp"></param>
    ''' <param name="ClassID"></param>
    ''' <remarks></remarks>
    Private Sub ConfirmMailSend(ByVal SelectedSupp As String, ByVal Status As Integer)
        ViewState("ActionStatus") = Status
        Dim selectedSuppliers() As String = Split(SelectedSupp, ",")
        Dim i As Integer
        Dim ds As DataSet

        Dim strContactType As String = ""
        strContactType = hdnContactType.Value.Trim

        If strContactType = ApplicationSettings.ContactType.approvedBuyer Or strContactType = ApplicationSettings.ContactType.suspendedBuyer Then
            'ds = ws.WSStandards.GetContactsforCompanies(ApplicationSettings.RoleClientID, SelectedSupp, ViewState("bizDivId"))
            ds = ws.WSContact.UpdateCompanyType(ApplicationSettings.RoleClientID, Status, SelectedSupp.Trim, ViewState("bizDivId"), Session("CompanyId"), Session("UserId"))  ' Session("CompanyId"), Session("UserId")
        Else
            'ds = ws.WSStandards.GetContactsforCompanies(ApplicationSettings.RoleSupplierID, SelectedSupp, ViewState("bizDivId"))
            ds = ws.WSContact.UpdateCompanyType(ApplicationSettings.RoleSupplierID, Status, SelectedSupp.Trim, ViewState("bizDivId"), Session("CompanyId"), Session("UserId"))  ' Session("CompanyId"), Session("UserId")
        End If

        If ds.Tables.Count <> 0 Then
            If ds.Tables("tblContacts").Rows.Count > 0 Then
                If ds.Tables("tblContacts").Rows(0).Item("ContactLinkId") <> 0 Then
                    DGSuppliers.DataSource = ds
                    DGSuppliers.DataBind()

                    pnlConfirmSendMail.Visible = True
                    pnlListing.Visible = False

                    Dim Stat As String = ""
                    Dim tempClassid As String = Status
                    If tempClassid = ApplicationSettings.StatusApprovedCompany Then
                        Stat = "Approved"
                    ElseIf tempClassid = ApplicationSettings.StatusNewCompany Then
                        Stat = "Un Approved"
                    ElseIf tempClassid = ApplicationSettings.StatusSuspendedCompany Then
                        Stat = "Suspended"
                    End If

                    ' For populating the Textbox with the original contents of the mail
                    txtMessage.Text = (EmailContents.GetString("AccountStatusMessage"))
                Else
                    lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
                End If
            Else
                lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
            End If
        Else
            lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
        End If


    End Sub

    ''' <summary>
    ''' Send mail to the selected contacts in the list
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSendMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendMail.Click

        Dim str As String = ""
        Dim mailSendFlag As Boolean = False
        Dim anitem As DataGridItem
        Dim isChecked As Boolean = False
        Dim status As String = ""
        If ViewState("ActionStatus") = ApplicationSettings.StatusApprovedCompany Then
            status = "Approve"
        ElseIf ViewState("ActionStatus") = ApplicationSettings.StatusDeletedCompany Then
            status = "Delete"
        ElseIf ViewState("ActionStatus") = ApplicationSettings.StatusNewCompany Then
            status = "Unapprove"
        ElseIf ViewState("ActionStatus") = ApplicationSettings.StatusSuspendedCompany Then
            status = "Suspend"
        End If
        ViewState("ActionStatus") = ""
        For Each anitem In DGSuppliers.Items
            isChecked = CType(anitem.FindControl("Check"), CheckBox).Checked
            If isChecked Then
                mailSendFlag = True
                Emails.SendCompanyStatusMail(CType(anitem.FindControl("Email"), HtmlInputHidden).Value, CType(anitem.FindControl("Name"), HtmlInputHidden).Value, txtMessage.Text, status)
            End If
        Next

        pnlConfirmSendMail.Visible = False
        pnlListing.Visible = True
        If mailSendFlag Then
            lblMsg.Text = ResourceMessageText.GetString("MailSentToSupplierMsg")
        End If
        PopulateGrid()
    End Sub

    ''' <summary>
    ''' donot send mail to anyone - only status updated
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlConfirmSendMail.Visible = False
        pnlListing.Visible = True
        PopulateGrid()
    End Sub

    Public Function getSelectedMainContactIds() As String
        Dim mainContactIds As String = ""
        For Each row As GridViewRow In gvContacts.Rows
            Dim chkBox As HtmlInputCheckBox = CType(row.FindControl("Check"), HtmlInputCheckBox)
            If chkBox.Checked Then
                If mainContactIds = "" Then
                    mainContactIds = CType(row.FindControl("hdnMainContactId"), HtmlInputHidden).Value
                Else
                    mainContactIds += "," & CType(row.FindControl("hdnMainContactId"), HtmlInputHidden).Value
                End If
            End If
        Next
        Return mainContactIds
    End Function

#End Region

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
    Public Function GetUserNotesDetail(ByVal ContactId As Integer) As DataView
        Dim dv As DataView
        If Not IsNothing(HttpContext.Current.Items("tblWOUserNotes")) Then
            Dim dt As DataTable = HttpContext.Current.Items("tblWOUserNotes")
            dv = dt.DefaultView
            dv.RowFilter = "ContactID = " & ContactId
            Return dv
        Else
            Return dv
        End If
    End Function

End Class



