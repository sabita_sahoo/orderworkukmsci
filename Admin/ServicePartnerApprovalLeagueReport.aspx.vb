
Partial Public Class ServicePartnerApprovalLeagueReport
    Inherits System.Web.UI.Page
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents lnkBtnGenerateReport As System.Web.UI.WebControls.LinkButton
    



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ddMonth.SelectedValue = Month(Date.Now)
            ddYear.SelectedValue = Year(Date.Now)
        End If
    End Sub

    Private Sub lnkBtnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnGenerateReport.Click
        generateReport()
    End Sub

    Private Sub generateReport()
        Dim languageCode As String = ""


        ReportViewerOW.ShowParameterPrompts = False
        ReportViewerOW.ShowToolBar = True
        Dim reportSErverPath As Uri = New Uri(ApplicationSettings.ReportServerPath)
        ReportViewerOW.ServerReport.ReportServerUrl = reportSErverPath
        ReportViewerOW.ServerReport.ReportServerCredentials = New CustomReportCredentials.CustomReportCredentials(ApplicationSettings.ReportServerUserName, ApplicationSettings.ReportServerPassword, ApplicationSettings.ReportServerDomain)
        Dim objParameter(3) As Microsoft.Reporting.WebForms.ReportParameter
        Dim StrCompanyID As String = ""

        'Give path to report            
        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/ServicePartnerApprovalLeagueReport"
        objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "True")

        'Language parameters
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                languageCode = "de-DE"
            Case ApplicationSettings.CountryUK
                languageCode = "en-GB"
        End Select
        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/ServicePartnerApprovalLeagueReport"
        objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("Year", ddYear.SelectedValue.ToString)
        objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("Month", ddMonth.SelectedValue.ToString)
        objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("MonthText", ddMonth.SelectedItem.Text)
        ViewState("Year") = ddYear.SelectedValue.ToString
        ViewState("Month") = ddMonth.SelectedValue.ToString

        ReportViewerOW.ServerReport.SetParameters(objParameter)
        ReportViewerOW.ServerReport.Refresh()

    End Sub

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
End Class