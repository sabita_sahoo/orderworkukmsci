﻿<%@ Page Title="OrderWork : Bulk WO Upload" Language="vb" AutoEventWireup="false"
    MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="BulkWOUpload.aspx.vb"
    Inherits="Admin.BulkWOUpload" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" src="JS/json2.js" type="text/javascript"></script>
    <script language="javascript" src="JS/Standards.js" type="text/javascript"></script>
    <script language="javascript" src="JS/jquery.js" type="text/javascript"></script>
    <script language="javascript" src="JS/JQuery.jsonSuggestCustom.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var input = $('input#txtContact').attr("autocomplete", "off");
        if ($.browser.mozilla)
            input.keypress(processKey); // onkeypress repeats arrow keys in Mozilla/Opera
        else
            input.keydown(processKey);
    </script>
    <script type="text/javascript">
        function validate() {

            var Status = document.getElementById('ctl00_ContentPlaceHolder1_UCSearchContact1_ddlContact').value;

            if (Status == '') {
                alert("Please Select Contact Name");
                return false;
            }

            var array = ['xls', 'xlsx'];

            var xyz = document.getElementById('<%= fileuploader.ClientID %>');

            var Extension = xyz.value.substring(xyz.value.lastIndexOf('.') + 1).toLowerCase();

            if (array.indexOf(Extension) <= -1) {

                alert("Please Upload a File with Valid Format");

                return false;
            }
        }      
    </script>
    <style>
        .BookHead {
            background-color: #EFEBEF;
            border-radius: 8px 8px 0px 0px;
            behavior: url(../Styles/PIE.htc);
            position: relative;
            width: 668px;
            width: 467px\9;
            height: 16px;
            *margin-top: -5px;
            padding: 8px 0px 15px 10px;
            *margin-left: 0px;
            font-family: Tahoma;
            font-size: 14px;
            font-weight: bolder;
        }

        .textField {
            width: 280px;
        }

        .BookTable {
            margin-top: 10px;
            background-color: white;
            border-radius: 8px;
            behavior: url(../Styles/PIE.htc);
            position: relative;
            width: 668px;
            height: 400px;
            padding-bottom: 10px;
            border: solid 1px #c0c0c0;
            text-align: left;
            overflow: hidden;
        }

         .OTP {
            margin-top: 10px;
            background-color: white;
            border-radius: 8px;
            behavior: url(../Styles/PIE.htc);
            position: relative;
            width: 668px;
            height: 400px;
            padding-bottom: 10px;
            border: solid 1px #c0c0c0;
            text-align: left;
            overflow: hidden;
        }

        .BookTableContent {
            font-family: tahoma;
            font-size: 12px;
            padding: 0 15px;
        }

        .Authentication {
            float: left;
            margin-bottom: 10px;
            width: 100%;
        }

        .AuthenticationLeft {
            float: left;
            width: 150px;
        }

        .AuthenticationRight {
            float: left;
        }

        .DateOfBirthFormat {
            color: #7f7f7f;
            float: left;
            font-family: tahoma;
            font-size: 10px;
            margin-left: 150px;
        }

        body {
        }

        .mGrid {
            border-style: solid;
            border-color: inherit;
            border-width: 0;
            border-top: 0px;
            border-bottom: 0px;
        }

            .mGrid td {
                border-color: -moz-use-text-color #FFFFFF #F7EBF7 -moz-use-text-color;
                border-style: none solid solid none;
                border-width: 0 1px 1px 0;
                color: #717171;
                border-right: 1px solid #FFFFFF\9;
                border-bottom: 1px solid #F7EBF7\9;
                border-left: 0px\9;
                border-top: 0px\9;
            }

        @media screen and (-webkit-min-device-pixel-ratio:0) {
            .mGrid td {
                border-bottom: 1px solid #F7EBF7;
                border-top: 0px;
                border-right: 1px solid #FFFFFF;
                border-left: 0px;
            }
        }

        * + html .mGrid td {
            border-right: 1px solid #FFFFFF;
            border-bottom: 1px solid #F7EBF7;
            border-left: 0px;
            border-top: 0px;
        }

        .mGrid th {
            background-color: #DEDFDE;
            border-color: -moz-use-text-color #FFFFFF -moz-use-text-color -moz-use-text-color;
            border-right-color: #FFFFFF\9;
            border-style: none solid none none;
            border-width: 0 1px 0 0;
            color: #636163;
            font-family: Tahoma;
            font-size: 12px;
            padding: 6px 2px;
        }

        @media screen and (-webkit-min-device-pixel-ratio:0) {
            .mGrid th {
                border-bottom: Opx;
                border-top: 0px;
                border-right: 1px solid;
                border-right-color: #FFFFFF;
                border-left: 0px;
            }
        }

        * + html .mGrid th {
            border-bottom: 0px;
            border-top: 0px;
            border-right: 1px solid #FFFFFF;
            border-left: 0px;
        }

        .one {
            font-family: tahoma;
            font-size: 12px;
            padding: 8px 2px 8px 10px;
            width: 159px;
        }

        .two {
            font-family: tahoma;
            font-size: 12px;
            padding: 8px 2px 8px 10px;
            width: 77px;
            text-align: center;
        }

        .three {
            font-family: tahoma;
            font-size: 12px;
            padding: 8px 2px 8px 10px;
            width: 71px;
            text-align: center;
        }

        .four {
            font-family: tahoma;
            font-size: 12px;
            padding: 8px 2px 8px 10px;
            width: 55px;
            text-align: center;
        }

        .five {
            font-family: tahoma;
            font-size: 12px;
            padding: 8px 2px 8px 10px;
            width: 20px;
            text-align: center;
        }

        .mGrid .alt {
            background: #F7F7F7;
        }

        .mGrid .pgr td {
            padding: 0px;
        }


        .gridSubContainer {
            width: 480px;
                      border: 1px solid #BDBEBD;
            border-top: 0px;
            border-bottom: 0px;
        }

        .beneficiaryTopGrid {
            width: 480px;
            background-color: #EFEBEF;
            float: left;
           
            border: 1px solid #BDBEBD;
            border-bottom: 0px;
            behavior: url(../Styles/PIE.htc);
        }

        .beneTopGridLeft {
            float: left;
            font-family: Tahoma;
            font-size: 14px;
            padding: 16px 0 18px 10px;
            border-radius: 8px 0 0 0;
            behavior: url(../Styles/PIE.htc);
        }

        .beneTopGridRight {
            float: right;
            font-family: Tahoma;
            font-size: 12px;
            margin-right: 10px;
            padding: 16px 0 16px 0;
            border-radius: 0 8px 0 0;
            behavior: url(../Styles/PIE.htc);
        }

        .beneficiaryBottomGrid {
            width: 480px;
            float: left;
            border-radius: 0 0 8px 8px;
            behavior: url(../Styles/PIE.htc);
            border: 1px solid #BDBEBD;
            border-top: 0px;
        }

        .beneBottomGridLeft {
            float: left;
            font-family: Tahoma;
            font-size: 14px;
            padding: 16px 0 18px 10px;
            border-radius: 0 0 0 8px;
            behavior: url(../Styles/PIE.htc);
        }

        .beneBottomGridRight {
            float: right;
            font-family: Tahoma;
            font-size: 12px;
            margin-right: 10px;
            padding: 16px 0;
            border-radius: 0 0 8px 0;
            behavior: url(../Styles/PIE.htc);
        }

        .Top {
            background-color: #EFEBEF;
            border-radius: 8px 8px 0 0;
            behavior: url(../Styles/PIE.htc);
        }

        .Bottom {
            background-color: #FFFFFF;
            border-radius: 0 0 8px 8px;
            behavior: url(../Styles/PIE.htc);
        }

        .paging {
            width: 480px;
            color: Black;
            float: left;
        }

        .lblPagingTop {
            float: left;
            font-family: Tahoma;
            font-size: 14px;
            padding: 16px 0 18px 10px;
            border-radius: 8px 0 0 0;
            behavior: url(../Styles/PIE.htc);
        }

        .pagingRightTop {
            float: right;
            font-family: Tahoma;
            font-size: 12px;
            margin-right: 10px;
            padding: 18px 0 16px 0;
            border-radius: 0 8px 0 0;
            behavior: url(../Styles/PIE.htc);
        }

        .lblShowPaging {
            float: left;
            margin-right: 5px;
        }

        #prevPagingImageTop {
            float: left;
            margin-right: 5px;
            margin-top: -2px;
        }

        .firstPaging {
            float: left;
            border: 1px solid #DEDBDE;
            background-color: #FFFFFF;
            margin-right: 5px;
            margin-top: -2px;
            padding: 2px 5px;
        }

        .secondPaging {
            float: left;
            border: 1px solid #DEDBDE;
            background-color: #FFFFFF;
            margin-right: 5px;
            margin-top: -2px;
            padding: 2px 5px;
        }

        .thirdPaging {
            float: left;
            border: 1px solid #DEDBDE;
            background-color: #FFFFFF;
            margin-right: 5px;
            margin-top: -2px;
            padding: 2px 5px;
        }

        #nextPagingImageTop {
            float: left;
            margin-top: -2px;
        }

        .pagingRightBtn {
            float: right;
            font-family: Tahoma;
            font-size: 12px;
            margin-right: 10px;
            padding: 18px 0 16px 0;
            border-radius: 0 0 8px 0;
            behavior: url(../Styles/PIE.htc);
        }

        #prevPagingImageBtn {
            float: left;
            margin-right: 5px;
            margin-top: -2px;
        }

        #nextPagingImageBtn {
            float: left;
            margin-top: -2px;
        }



        #divProgress {
            background-color: #eeeeeb;
            position: absolute;
            font-family: Tahoma;
            font-size: 13px;
            font-weight: bold;
            filter: alpha(opacity=70);
            MozOpacity: 0.7;
            opacity: 0.7;
            padding: 15% 0 0;
            margin: 0px auto;
            height: 1920px;
            width: 1020px;
            text-align: center;
        }

        .benestyl {
            margin-left: 16px;
        }
    </style>
    <style>
        table.bgWhite tr:nth-child(even) table
        {
            background: #f7f7f7;
        }
        
        body
        {
            font-family: tahoma;
            font-size: 12px;
            overflow-y: scroll;
        }
    </style>
    <style type="text/css">
        #HideDetails1, #HideDetails2, #HideDetails3
        {
            width: 98%;
            float: left;
            margin-bottom: 25px;
        }
        
        .Hide
        {
            display: none;
        }
        
        .Show
        {
            display: block;
        }
    </style>
    <input type="hidden" id="hdnContactID" runat="Server" value="" />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div style="margin-left: 15px; margin-top: 5px; font-size: 12px; min-height: 550px;">
        <p class="paddingB4 HeadingRed">
            <strong>Bulk WO Upload</strong>
        </p>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divUpload" runat="server">
                    <div class="container containerLeft">
                        <div>
                            <div class="beneficiaryTopGrid">
                                <div class="paddingB4 HeadingRed">
                                    &nbsp;&nbsp;<b>Upload File</b></div>
                            </div>
                            <div class="gridSubContainer">
                                <div style="padding: 12px 0;">
                                    <asp:RadioButtonList ID="rbl_typeofClient" runat="server" RepeatDirection="Horizontal"
                                        Style="margin-left: 12px;">
                                        <asp:ListItem Value="0">IT</asp:ListItem>
                                        <asp:ListItem Value="1" Selected="True">Retail</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div style="padding: 12px 0; margin-left: 15px;">
                                    <%@ register src="~/UserControls/Admin/UCSearchContact.ascx" tagname="UCSearchContact"
                                        tagprefix="uc1" %>
                                    <uc1:UCSearchContact ID="UCSearchContact1" runat="server"></uc1:UCSearchContact>
                                </div>
                                <div style="border-bottom: 1px solid #c6c4c5; padding: 12px 0;">
                                    <asp:FileUpload ID="fileuploader" runat="server" Style="margin-left: 15px;" size="50"
                                        class="formText" />&nbsp;&nbsp;
                                    <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                        color: #FFFFFF; text-align: center; width: 60px; margin: 17px 370px;">
                                        <asp:LinkButton ID="btn_upload_excel" runat="server" CssClass="txtButtonRed" OnClientClick="return validate();">&nbsp;Upload&nbsp;</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="divView" runat="server" class="Hide">
                    <div id="" class=" containerLeft">
                        <div id="divViewForRetailClient" runat="server" class="Hide">
                            <asp:GridView ID="DataList1" runat="server" ForeColor="#333333" BorderWidth="1px"
                                PageSize="25" BorderColor="White" CssClass="gridrow" Width="99%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sr No" SortExpression="Sr No" HeaderText="Sr No" HeaderStyle-Width="10px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Title" SortExpression="Work Request Title" HeaderText="Work Request Title"
                                        HeaderStyle-Width="300px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataFormatString="{0:dd/MM/yy}" DataField="Appointment Date"
                                        SortExpression="Appointment Date" HeaderText="Appointment Date" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataField="Appointment Time" SortExpression="Appointment Time"
                                        HeaderText="Appointment Time" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Type" SortExpression="Work Request Type" HeaderText="Work Request Type" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sub Category For Work Request Type" SortExpression="Sub Category For Work Request Type"
                                        HeaderText="Sub Category" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sales Invoice Title" SortExpression="Sales Invoice Title" HeaderText="Sales Invoice Title" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Scope of Work" SortExpression="Scope of Work" HeaderText="Scope of Work" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataField="Contact First Name" SortExpression="Contact First Name"
                                        HeaderText="First Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Contact Last Name" SortExpression="Contact Last Name" HeaderText="Last Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Postal Code" SortExpression="Postal Code" HeaderText="Postal Code" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Address" SortExpression="Address" HeaderText="Address" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="City" SortExpression="City" HeaderText="City" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Phone" SortExpression="Phone" HeaderText="Phone" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Estimated Time Required" SortExpression="Estimated Time Required"
                                        HeaderText="Estimated Time Required" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Product Purchased" SortExpression="Product Purchased" HeaderText="Product Purchased" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="WP" SortExpression="WP" HeaderText="WP" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="PP" SortExpression="PP" HeaderText="PP" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="PO Number" SortExpression="PO Number" HeaderText="PO Number" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Engineer Scope of work" SortExpression="Engineer Scope of work" HeaderText="Engineer Scope of work" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Allow bid review" SortExpression="Allow bid review" HeaderText="Allow bid review" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Force Sign off sheet" SortExpression="Force Sign off sheet" HeaderText="Force Sign off sheet" />
                                </Columns>
                                <AlternatingRowStyle CssClass="gridRow" />
                                <PagerStyle BorderWidth="0px" BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top" Height="40px" />
                                <FooterStyle BackColor="#D7D7D7" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </div>
                        <div id="divViewForITClient" runat="server" class="Hide">
                            <asp:GridView ID="dlst_beneficiaries" runat="server" ForeColor="#333333" BorderWidth="1px"
                                PageSize="25" BorderColor="White" CssClass="gridrow" Width="99%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sr No" SortExpression="Sr No" HeaderText="Sr No" HeaderStyle-Width="10px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Title" SortExpression="Work Request Title" HeaderText="Work Request Title"
                                        HeaderStyle-Width="300px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataFormatString="{0:dd/MM/yy}" DataField="Schedule Window Begin"
                                        SortExpression="Schedule Window Begin" HeaderText="Schedule Window Begin" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataFormatString="{0:dd/MM/yy}" DataField="Schedule Window End"
                                        SortExpression="Schedule Window End" HeaderText="Schedule Window End" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Type" SortExpression="Work Request Type" HeaderText="Work Request Type" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sub Category For Work Request Type" SortExpression="Sub Category For Work Request Type"
                                        HeaderText="Sub Category" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sales Invoice Title" SortExpression="Sales Invoice Title" HeaderText="Sales Invoice Title" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Scope of Work" SortExpression="Scope of Work" HeaderText="Scope of Work" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataField="Contact First Name" SortExpression="Contact First Name"
                                        HeaderText="First Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Contact Last Name" SortExpression="Contact Last Name" HeaderText="Last Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Postal Code" SortExpression="Postal Code" HeaderText="Postal Code" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Address" SortExpression="Address" HeaderText="Address" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="City" SortExpression="City" HeaderText="City" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Phone" SortExpression="Phone" HeaderText="Phone" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Equipment Details" SortExpression="Equipment Details" HeaderText="Equipment Details" />
                                </Columns>
                                <AlternatingRowStyle CssClass="gridRow" />
                                <PagerStyle BorderWidth="0px" BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top" Height="40px" />
                                <FooterStyle BackColor="#D7D7D7" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div style="float: right; margin: 15px -11px;">
                        <table>
                            <tr>
                                <td align="right" valign="top">
                                    <table class="marginL20" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0"
                                        runat="server" id="tblConfirm">
                                        <tr>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                            </td>
                                            <td class="txtBtnImage">
                                                <asp:LinkButton ID="btn_confirm" CausesValidation="false" runat="server" CssClass="txtListing"
                                                    Text="<img src='Images/Icons/Icon-Confirm.gif' title='Cancel' width='12' height='12' align='absmiddle' hspace='1' vspace='2' border='0'> Confirm"></asp:LinkButton>
                                            </td>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="right">
                                    <table class="marginL20" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                        <tr>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                            </td>
                                            <td class="txtBtnImage">
                                                <asp:LinkButton ID="btn_cancel" CausesValidation="false" runat="server" CssClass="txtListing"
                                                    Text="<img src='Images/Icons/Cancel.gif' title='Cancel' width='11' height='11' align='absmiddle' hspace='1' vspace='2' border='0'> Cancel"></asp:LinkButton>
                                            </td>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="20" align="right">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="divConfirm" runat="server" class="Hide">
                    <div id="Div2" class=" containerLeft">
                        <div id="divConfirmSuccessfullRecordsForRetailClient" runat="server" class="Hide">
                            <b>Valid Records</b>
                            <asp:GridView ID="DataList2" runat="server" ForeColor="#333333" BorderWidth="1px"
                                PageSize="25" BorderColor="White" CssClass="gridrow" Width="99%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sr No" SortExpression="Sr No" HeaderText="Sr No" HeaderStyle-Width="10px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Title" SortExpression="Work Request Title" HeaderText="Work Request Title"
                                        HeaderStyle-Width="300px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataFormatString="{0:dd/MM/yy}" DataField="Appointment Date"
                                        SortExpression="Appointment Date" HeaderText="Appointment Date" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataField="Appointment Time" SortExpression="Appointment Time"
                                        HeaderText="Appointment Time" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Type" SortExpression="Work Request Type" HeaderText="Work Request Type" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sub Category For Work Request Type" SortExpression="Sub Category For Work Request Type"
                                        HeaderText="Sub Category" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sales Invoice Title" SortExpression="Sales Invoice Title" HeaderText="Sales Invoice Title" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Scope of Work" SortExpression="Scope of Work" HeaderText="Scope of Work" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataField="Contact First Name" SortExpression="Contact First Name"
                                        HeaderText="First Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Contact Last Name" SortExpression="Contact Last Name" HeaderText="Last Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Postal Code" SortExpression="Postal Code" HeaderText="Postal Code" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Address" SortExpression="Address" HeaderText="Address" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="City" SortExpression="City" HeaderText="City" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Phone" SortExpression="Phone" HeaderText="Phone" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Estimated Time Required" SortExpression="Estimated Time Required"
                                        HeaderText="Estimated Time Required" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Product Purchased" SortExpression="Product Purchased" HeaderText="Product Purchased" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="WP" SortExpression="WP" HeaderText="WP" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="PP" SortExpression="PP" HeaderText="PP" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="PO Number" SortExpression="PO Number" HeaderText="PO Number" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Engineer Scope of work" SortExpression="Engineer Scope of work" HeaderText="Engineer Scope of work" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Allow bid review" SortExpression="Allow bid review" HeaderText="Allow bid review" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Force Sign off sheet" SortExpression="Force Sign off sheet" HeaderText="Force Sign off sheet" />
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <img src="BulkWOFiles/ActiveIcon.PNG" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="gridRow" />
                                <PagerStyle BorderWidth="0px" BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top" Height="40px" />
                                <FooterStyle BackColor="#D7D7D7" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </div>
                        <br />
                        <br />
                        <div id="divConfirmInvalidRecordsForRetailClient" runat="server" class="Hide">
                            <b>Invalid Records</b>
                            <asp:GridView ID="DataList3" runat="server" ForeColor="#333333" BorderWidth="1px"
                                PageSize="25" BorderColor="White" CssClass="gridrow" Width="99%" AutoGenerateColumns="false"
                                EmptyDataText="No Invalid Records" EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-HorizontalAlign="Center">
                                <Columns>
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sr No" SortExpression="Sr No" HeaderText="Sr No" HeaderStyle-Width="10px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Title" SortExpression="Work Request Title" HeaderText="Work Request Title"
                                        HeaderStyle-Width="300px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataFormatString="{0:dd/MM/yy}" DataField="Appointment Date"
                                        SortExpression="Appointment Date" HeaderText="Appointment Date" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataField="Appointment Time" SortExpression="Appointment Time"
                                        HeaderText="Appointment Time" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Type" SortExpression="Work Request Type" HeaderText="Work Request Type" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sub Category For Work Request Type" SortExpression="Sub Category For Work Request Type"
                                        HeaderText="Sub Category" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sales Invoice Title" SortExpression="Sales Invoice Title" HeaderText="Sales Invoice Title" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Scope of Work" SortExpression="Scope of Work" HeaderText="Scope of Work" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataField="Contact First Name" SortExpression="Contact First Name"
                                        HeaderText="First Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Contact Last Name" SortExpression="Contact Last Name" HeaderText="Last Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Postal Code" SortExpression="Postal Code" HeaderText="Postal Code" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Address" SortExpression="Address" HeaderText="Address" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="City" SortExpression="City" HeaderText="City" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Phone" SortExpression="Phone" HeaderText="Phone" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Estimated Time Required" SortExpression="Estimated Time Required"
                                        HeaderText="Estimated Time Required" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Product Purchased" SortExpression="Product Purchased" HeaderText="Product Purchased" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Reason" SortExpression="Reason" HeaderText="Reason" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="WP" SortExpression="WP" HeaderText="WP" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="PP" SortExpression="PP" HeaderText="PP" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="PO Number" SortExpression="PO Number" HeaderText="PO Number" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Engineer Scope of work" SortExpression="Engineer Scope of work" HeaderText="Engineer Scope of work" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Allow bid review" SortExpression="Allow bid review" HeaderText="Allow bid review" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Force Sign off sheet" SortExpression="Force Sign off sheet" HeaderText="Force Sign off sheet" />
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <img src="BulkWOFiles/in-activeIcon.png" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="gridRow" />
                                <PagerStyle BorderWidth="0px" BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top" Height="40px" />
                                <FooterStyle BackColor="#D7D7D7" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </div>
                        <div id="divConfirmSuccessfullRecordsForITClient" runat="server" class="Hide">
                            <b>Valid Records </b>
                            <asp:GridView ID="dlst_bene" runat="server" ForeColor="#333333" BorderWidth="1px"
                                PageSize="25" BorderColor="White" CssClass="gridrow" Width="99%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sr No" SortExpression="Sr No" HeaderText="Sr No" HeaderStyle-Width="10px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Title" SortExpression="Work Request Title" HeaderText="Work Request Title"
                                        HeaderStyle-Width="300px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataFormatString="{0:dd/MM/yy}" DataField="Schedule Window Begin"
                                        SortExpression="Schedule Window Begin" HeaderText="Schedule Window Begin" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataFormatString="{0:dd/MM/yy}" DataField="Schedule Window End"
                                        SortExpression="Schedule Window End" HeaderText="Schedule Window End" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Type" SortExpression="Work Request Type" HeaderText="Work Request Type" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sub Category For Work Request Type" SortExpression="Sub Category For Work Request Type"
                                        HeaderText="Sub Category" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sales Invoice Title" SortExpression="Sales Invoice Title" HeaderText="Sales Invoice Title" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Scope of Work" SortExpression="Scope of Work" HeaderText="Scope of Work" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataField="Contact First Name" SortExpression="Contact First Name"
                                        HeaderText="First Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Contact Last Name" SortExpression="Contact Last Name" HeaderText="Last Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Postal Code" SortExpression="Postal Code" HeaderText="Postal Code" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Address" SortExpression="Address" HeaderText="Address" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="City" SortExpression="City" HeaderText="City" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Phone" SortExpression="Phone" HeaderText="Phone" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Equipment Details" SortExpression="Equipment Details" HeaderText="Equipment Details" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="WP" SortExpression="WP" HeaderText="WP" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="PP" SortExpression="PP" HeaderText="PP" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="PO Number" SortExpression="PO Number" HeaderText="PO Number" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Engineer Scope of work" SortExpression="Engineer Scope of work" HeaderText="Engineer Scope of work" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Allow bid review" SortExpression="Allow bid review" HeaderText="Allow bid review" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Force Sign off sheet" SortExpression="Force Sign off sheet" HeaderText="Force Sign off sheet" />
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <img src="BulkWOFiles/ActiveIcon.PNG" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="gridRow" />
                                <PagerStyle BorderWidth="0px" BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top" Height="40px" />
                                <FooterStyle BackColor="#D7D7D7" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </div>
                        <br />
                        <br />
                        <div id="divConfirmInvalidRecordsForITClient" runat="server" class="Hide">
                            <b>Invalid Records</b>
                            <asp:GridView ID="DataList4" runat="server" ForeColor="#333333" BorderWidth="1px"
                                PageSize="25" BorderColor="White" CssClass="gridrow" Width="99%" AutoGenerateColumns="false"
                                EmptyDataText="No Invalid Records">
                                <Columns>
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sr No" SortExpression="Sr No" HeaderText="Sr No" HeaderStyle-Width="10px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Title" SortExpression="Work Request Title" HeaderText="Work Request Title"
                                        HeaderStyle-Width="300px" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataFormatString="{0:dd/MM/yy}" DataField="Schedule Window Begin"
                                        SortExpression="Schedule Window Begin" HeaderText="Schedule Window Begin" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="300px" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataFormatString="{0:dd/MM/yy}" DataField="Schedule Window End"
                                        SortExpression="Schedule Window End" HeaderText="Schedule Window End" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Work Request Type" SortExpression="Work Request Type" HeaderText="Work Request Type" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sub Category For Work Request Type" SortExpression="Sub Category For Work Request Type"
                                        HeaderText="Sub Category" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Sales Invoice Title" SortExpression="Sales Invoice Title" HeaderText="Sales Invoice Title" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Scope of Work" SortExpression="Scope of Work" HeaderText="Scope of Work" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-Width="300px"
                                        HeaderStyle-CssClass="gridHdr" DataField="Contact First Name" SortExpression="Contact First Name"
                                        HeaderText="First Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Contact Last Name" SortExpression="Contact Last Name" HeaderText="Last Name" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Postal Code" SortExpression="Postal Code" HeaderText="Postal Code" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Address" SortExpression="Address" HeaderText="Address" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="City" SortExpression="City" HeaderText="City" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Phone" SortExpression="Phone" HeaderText="Phone" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Equipment Details" SortExpression="Equipment Details" HeaderText="Equipment Details" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Reason" SortExpression="Reason" HeaderText="Reason" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="WP" SortExpression="WP" HeaderText="WP" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="PP" SortExpression="PP" HeaderText="PP" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="PO Number" SortExpression="PO Number" HeaderText="PO Number" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Engineer Scope of work" SortExpression="Engineer Scope of work" HeaderText="Engineer Scope of work" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Allow bid review" SortExpression="Allow bid review" HeaderText="Allow bid review" />
                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="22%" HeaderStyle-CssClass="gridHdr"
                                        DataField="Force Sign off sheet" SortExpression="Force Sign off sheet" HeaderText="Force Sign off sheet" />
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <img src="BulkWOFiles/in-activeIcon.png" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="gridRow" />
                                <PagerStyle BorderWidth="0px" BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top" Height="40px" />
                                <FooterStyle BackColor="#D7D7D7" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div style="float: right; margin: 15px 25px; width: 200px;">
                        <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                            color: #FFFFFF; text-align: center; width: 60px; margin: 0px 82px;">
                            <asp:LinkButton ID="btn_upload" runat="server" CssClass="txtButtonRed" Visible="false">&nbsp;Upload&nbsp;</asp:LinkButton>
                        </div>
                        <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                            color: #FFFFFF; text-align: center; width: 60px; float: left; margin: -18px 150px;">
                            <asp:LinkButton ID="btn_cancel_upload" runat="server" CssClass="txtButtonRed">&nbsp;Cancel&nbsp;</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div id="divResult" runat="server" class="Hide">
                    <div id="Div3" class=" containerLeft">
                        <table id="Table4" style="border: 1px solid #cccdcc; border-radius: 5px; color: #524d4a;"
                            width="974px" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="" style="padding: 5px 10px;
                                            width: 970px; border-bottom: 1px solid #cccdcc;">
                                            <tbody>
                                                <tr>
                                                    <td class=" headerText" style="width: 150px;" align="left">
                                                        <h1 style="color: #FE0100; font-weight: normal;">
                                                            Thank You</h1>
                                                        <asp:Label runat="server" ID="lbl_success_count" Style="font-weight: bold; display: block;
                                                            padding-bottom: 15px; border-bottom: 1px solid #cccdcc;"></asp:Label>
                                                        <asp:Label runat="server" ID="lbl_error_count" Style="font-weight: bold; display: block;
                                                            margin: 15px 0;"> </asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="" style="background: #efebef;
                                            padding: 5px 0; width: 974px; border-bottom: 1px solid #cccdcc; font-weight: bold;">
                                            <tbody>
                                                <tr>
                                                    <td class=" headerText" style="width: 150px;" align="left">
                                                        &nbsp;Country
                                                    </td>
                                                    <td class=" headerText" style="width: 150px;" align="left">
                                                        &nbsp;Currency (3 char code)
                                                    </td>
                                                    <td class=" headerText" style="width: 150px;" align="left">
                                                        &nbsp;IBAN
                                                    </td>
                                                    <td class=" headerText" style="width: 150px;" align="left">
                                                        &nbsp;Sort Code
                                                    </td>
                                                    <td class="headerText" style="width: 150px;" align="left">
                                                        &nbsp;Swift Code
                                                    </td>
                                                    <td class="headerText" style="width: 150px;" align="left">
                                                        &nbsp;Account Number
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table style="width: 974px; border-bottom: 2px solid #bdbebd; background: #cefbce;
                                            padding: 5px 0; font-weight: bold;" border="0" cellpadding="0" cellspacing="0"
                                            width="970px">
                                            <tbody>
                                                <tr>
                                                    <td class="headerText " style="width: 150px; border-right: 1px solid #ccc;" align="left">
                                                        &nbsp;Beneficiary Name
                                                    </td>
                                                    <td class="headerText " style="width: 150px; border-right: 1px solid #ccc;" align="left">
                                                        &nbsp;Beneficiary Address
                                                    </td>
                                                    <%--<td class="headerText " style="width: 150px; border-right: 1px solid #ccc;" align="left">&nbsp;Is bene third party</td>
                                                        <td class="headerText " style="width: 150px; border-right: 1px solid #ccc;" align="left">&nbsp;Auto Send Payment</td>--%>
                                                    <td class="headerText " style="width: 150px; border-right: 1px solid #ccc;" align="left">
                                                        &nbsp;Beneficiary Email id
                                                    </td>
                                                    <%--<td class="headerText " style="width: 150px;" align="left">&nbsp;Personal Reference</td>--%>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <%--inserting datalist--%>
                                        <asp:DataList ID="dlst_result" DataKeyField="SrNo" runat="server" class="bgWhite"
                                            Style="background-color: White; border-collapse: collapse;" border="0" CellSpacing="0"
                                            Width="970px">
                                            <EditItemTemplate>
                                                <table class="" style="border-collapse: collapse;" border="0" cellspacing="0" width="970px">
                                                    <tbody>
                                                        <tr>
                                                            <td class="bgYellow#FFFFD6">
                                                                <table width="950px" cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="970px" cellspacing="0" cellpadding="0" border="0" style="padding: 4px 0;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style="">
                                                                                                    <%-- <asp:DropDownList ID="ddl_country"  runat="server" OnSelectedIndexChanged="ddl_country_SelectedIndexChanged" AutoPostBack="true">
                                                                                                        <asp:ListItem>IN</asp:ListItem>
                                                                                                        <asp:ListItem>GB</asp:ListItem>
                                                                                                    </asp:DropDownList>--%></span>
                                                                                            </td>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style="">
                                                                                                    <%--<asp:DropDownList ID="ddl_currency" runat="server" OnSelectedIndexChanged="ddl_currency_SelectedIndexChanged">
                                                                                                        <asp:ListItem>INR</asp:ListItem>
                                                                                                        <asp:ListItem>EUR</asp:ListItem>
                                                                                                    </asp:DropDownList>--%></span>
                                                                                            </td>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style=""><asp:TextBox ID="TextBox2" runat="server" Text='<%# Eval("IBAN") %>'>
                                                                                                </asp:TextBox></span>
                                                                                            </td>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style=""><asp:TextBox ID="TextBox3" runat="server" Text='<%# Eval("SortCode") %>'>
                                                                                                </asp:TextBox></span>
                                                                                            </td>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style=""><asp:TextBox ID="TextBox4" runat="server" Text='<%# Eval("SwiftCode") %>'>
                                                                                                </asp:TextBox></span>
                                                                                            </td>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style=""><asp:TextBox ID="TextBox5" runat="server" Text='<%# Eval("Accountnumber") %>'>
                                                                                                </asp:TextBox></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table width="970px" cellspacing="0" cellpadding="0" border="0" style="width: 970px;
                                                                                    background: #cefbce; padding: 3px 0;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText ">
                                                                                                &nbsp;<asp:TextBox ID="TextBox6" runat="server" Text='<%# Eval("Name") %>'>
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                            <%-- <td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText   ">&nbsp;<asp:TextBox ID="TextBox7" runat="server"
                                                                                                    Text='<%# Eval("Address") %>'>
                                                                                                </asp:TextBox>
                                                                                                </td>--%>
                                                                                            <%--   <td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText   ">&nbsp;<asp:TextBox ID="TextBox8" runat="server"
                                                                                                    Text='<%# Eval("ThirdParty") %>'>
                                                                                                </asp:TextBox></td>--%>
                                                                                            <td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText   ">
                                                                                                &nbsp;<asp:TextBox ID="TextBox9" runat="server" Text='<%# Eval("AutoSendPayment") %>'>
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                            <td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText   ">
                                                                                                &nbsp;<asp:TextBox ID="TextBox10" runat="server" Text='<%# Eval("EmailID") %>'>
                                                                                                </asp:TextBox>
                                                                                            </td>
                                                                                            <%-- <td align="left" style="width: 150px;" class=" formText ">&nbsp;<asp:TextBox ID="TextBox11" runat="server"
                                                                                                    Text='<%# Eval("PersonalReference") %>'>
                                                                                                </asp:TextBox></td>--%>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table width="970px" cellspacing="0" cellpadding="0" border="0" style="width: 970px;
                                                                                    background: #FEFFD7; padding: 3px 0;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText "
                                                                                                valign="middle">
                                                                                                &nbsp;<b>Status : </b>&nbsp;
                                                                                                <img src="../BulkWOFiles/in-activeIcon.png" />
                                                                                                &nbsp;
                                                                                                <asp:Label ID="Label14" Text='<%# DataBinder.Eval(Container.DataItem, "Reason") %>'
                                                                                                    runat="server" />
                                                                                                <div style="float: right">
                                                                                                    <asp:ImageButton ID="btn_save" CommandName="update" ImageUrl="~/BulkWOFiles/save-btn.png"
                                                                                                        runat="server" />
                                                                                                    &nbsp;<asp:ImageButton ID="btn_cancel_edit" ImageUrl="~/BulkWOFiles/cancel.jpg" CommandName="cancel"
                                                                                                        runat="server" />
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table width="970px" cellspacing="0" cellpadding="0" border="0" height="" style="border-bottom: 2px solid #bdbebd;
                                                                                    height: 8px;">
                                                                                    <tbody>
                                                                                        <tr class="smallText" style="height: ">
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <table id="dlClientList" class="" style="border-collapse: collapse;" border="0" cellspacing="0"
                                                    width="970px">
                                                    <tbody>
                                                        <tr>
                                                            <td class="bgYellow#FFFFD6">
                                                                <table width="950px" cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="970px" cellspacing="0" cellpadding="0" border="0" style="padding: 4px 0;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <%--<td>
                                                                                                    <asp:Label ID="Label22" Text='<%# DataBinder.Eval(Container.DataItem, "SrNo") %>' runat="server" /></td>--%>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style=""><asp:Label ID="Label21" Text='<%# DataBinder.Eval(Container.DataItem, "CountryName") %>'
                                                                                                    runat="server" /></span>
                                                                                            </td>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style=""><asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem, "Currency") %>'
                                                                                                    runat="server" /></span>
                                                                                            </td>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style=""><asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem, "IBAN") %>'
                                                                                                    runat="server" /></span>
                                                                                            </td>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style=""><asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem, "SortCode") %>'
                                                                                                    runat="server" /></span>
                                                                                            </td>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style=""><asp:Label ID="Label4" Text='<%# DataBinder.Eval(Container.DataItem, "SwiftCode") %>'
                                                                                                    runat="server" /></span>
                                                                                            </td>
                                                                                            <td style="width: 150px;" align="left" class="formText ">
                                                                                                &nbsp;<span style=""><asp:Label ID="Label5" Text='<%# DataBinder.Eval(Container.DataItem, "Accountnumber") %>'
                                                                                                    runat="server" /></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table width="970px" cellspacing="0" cellpadding="0" border="0" style="width: 970px;
                                                                                    background: #cefbce; padding: 3px 0;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText ">
                                                                                                &nbsp;<asp:Label ID="Label6" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'
                                                                                                    runat="server" />
                                                                                            </td>
                                                                                            <td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText   ">
                                                                                                &nbsp;<asp:Label ID="Label7" Text='<%# DataBinder.Eval(Container.DataItem, "Address") %>'
                                                                                                    runat="server" />
                                                                                            </td>
                                                                                            <%--<td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText   ">&nbsp;<asp:Label ID="Label8" Text='<%# DataBinder.Eval(Container.DataItem, "ThirdParty") %>' runat="server" /></td>--%>
                                                                                            <%--                                                                                                <td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText   ">&nbsp;<asp:Label ID="Label9" Text='<%# DataBinder.Eval(Container.DataItem, "AutoSendPayment") %>' runat="server" /></td>--%>
                                                                                            <td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText   ">
                                                                                                &nbsp;<asp:Label ID="Label10" Text='<%# DataBinder.Eval(Container.DataItem, "EmailID") %>'
                                                                                                    runat="server" />
                                                                                            </td>
                                                                                            <%--                                                                                                <td align="left" style="width: 150px;" class=" formText ">&nbsp;<asp:Label ID="Label11" Text='<%# DataBinder.Eval(Container.DataItem, "PersonalReference") %>' runat="server" /></td>--%>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table width="970px" cellspacing="0" cellpadding="0" border="0" style="width: 970px;
                                                                                    background: #FEFFD7; padding: 3px 0;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" style="width: 150px; border-right: 1px solid #ccc;" class="formText "
                                                                                                valign="middle">
                                                                                                &nbsp;<b>Status : </b>&nbsp;
                                                                                                <img src="../BulkWOFiles/in-activeIcon.png" />
                                                                                                &nbsp;
                                                                                                <asp:Label ID="Label14" Text='<%# DataBinder.Eval(Container.DataItem, "Reason") %>'
                                                                                                    runat="server" />
                                                                                                <div style="float: right">
                                                                                                    <asp:ImageButton Visible="false" ID="btn_edit" runat="server" CommandName="edit"
                                                                                                        ImageUrl="~/BulkWOFiles/edit-btn.png" AlternateText="Edit" />
                                                                                                    <%--OnClick="javascript:getdrpdwnid(this.id);" --%>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table width="970px" cellspacing="0" cellpadding="0" border="0" height="" style="border-bottom: 2px solid #bdbebd;
                                                                                    height: 8px;">
                                                                                    <tbody>
                                                                                        <tr class="smallText" style="height: ">
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                        <%--datalist ends here--%>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="float: right; margin: 15px 25px;">
                            <asp:ImageButton ID="btn_download" runat="server" AlternateText="Download" ImageUrl="~/BulkWOFiles/download.jpg"
                                CausesValidation="False" />
                        </div>
                    </div>
                    <%--</asp:Panel>--%>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btn_upload_excel" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdateProgress AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                <div>
                    <img align="middle" src="Images/indicator.gif" />
                    Loading ...
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1"
            CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
</asp:Content>
