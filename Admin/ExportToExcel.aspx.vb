Partial Public Class ExportToExcel
    Inherits System.Web.UI.Page

    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        Dim BillLoc As Integer = Session("BillLoc")
        Select Case (Request("page"))
            Case "AMContacts", "amcontacts"
                Dim bizDivId As Integer = Request("bizDivId")
                Dim fromDate As String = Request("fromDate")
                Dim toDate As String = Request("toDate")
                Dim mainClassId As String = Request("mainClassId")
                Dim subClassId As String = Request("subClassId")
                Dim status As String = Request("status")
                Dim statusId As String = Request("statusId")

                Dim dateCriteriaField As String = ""
                If Not IsNothing(Request("dateCriteriaField")) Then
                    dateCriteriaField = Request("dateCriteriaField") 'HttpContext.Current.Items("dateCriteriaField")                
                End If
                Dim sortExpression As String = Request("sortExpression")
                Dim startRowIndex As String = Request("startRowIndex")
                Dim maximumRows As String = Request("maximumRows")
                Dim rowCount As Integer = 0
                Dim ds As DataSet = ws.WSContact.GetContactExporttoExcel(bizDivId, mainClassId, fromDate, toDate, statusId, dateCriteriaField)
                DataSetToExcel.ConvertNew(ds, Response)
            Case "Location", "location"
                Dim arr As New ArrayList
                Dim strFilterString As String = Request.QueryString("FilterString")
                arr.Add("AddressID")
                arr.Add("ContactID")
                DataSetToExcel.ConvertNew(ws.WSContact.GetLocationListing(Request("CompanyID"), "admin", "AddressID", 0, 0, 1000, strFilterString), Response)
            Case "Product", "product"
                Dim arr As New ArrayList
                arr.Add("WOID")
                DataSetToExcel.ConvertNew(ws.WSWorkOrder.GetProductListing(Request("CompanyID"), Request("bizDivId"), "WOID", 0, 100, 1000, True), Response)
            Case "specialist"
                Dim siteType As String = "site"
                Dim strFilterString As String = Request.QueryString("FilterString")
                Select Case ApplicationSettings.SiteType
                    Case ApplicationSettings.siteTypes.admin
                        siteType = "admin"
                    Case ApplicationSettings.siteTypes.site
                        siteType = "site"
                End Select

                Dim ds As DataSet = ws.WSContact.GetSpecialistsListing(Request("CompanyID"), Request("bizDivId"), siteType, "ContactID", 0, 0, 1000, strFilterString)
                Dim colsToDelete As New ArrayList()
                Dim ContactID As String = ds.Tables(0).Columns(0).ColumnName
                Dim RowNum As String = ds.Tables(0).Columns(11).ColumnName
                colsToDelete.Add("ContactID")
                colsToDelete.Add("RowNum")
                DataSetToExcel.ConvertNew(ds, Response)
            Case "AdminSearchAccounts", "adminsearchaccounts"
                Dim TrackSp As Boolean
                Dim EngineerCRBCheck As Boolean
                Dim EngineerCSCSCheck As Boolean
                Dim EngineerUKSecurityCheck As Boolean
                Dim EngineerRightToWorkInUK As Boolean
                Dim EngProofOfMeeting As Boolean
                If Request("TrackSp") = 0 Then
                    TrackSp = False
                Else
                    TrackSp = True
                End If
                If Request("EngineerCRBCheck") = 0 Then
                    EngineerCRBCheck = False
                Else
                    EngineerCRBCheck = True
                End If
                If Request("EngineerCSCSCheck") = 0 Then
                    EngineerCSCSCheck = False
                Else
                    EngineerCSCSCheck = True
                End If
                If Request("EngineerUKSecurityCheck") = 0 Then
                    EngineerUKSecurityCheck = False
                Else
                    EngineerUKSecurityCheck = True
                End If
                If Request("EngineerRightToWorkInUK") = 0 Then
                    EngineerRightToWorkInUK = False
                Else
                    EngineerRightToWorkInUK = True
                End If
                If Request("EngProofOfMeeting") = 0 Then
                    EngProofOfMeeting = False
                Else
                    EngProofOfMeeting = True
                End If
                'Dim ds As New DataSet
                'ds = ws.WSContact.GetMSSearchAccounts(Request("bizDivId"), Request("skillSet"), Request("region"), Request("postCode"), Request("keyword"), statusId, Request("fName"), Request("lName"), Request("companyName"), Request("eMail"), Request("vendorIDs"), Request("CRB"), Request("CSCS"), Request("UKSec"), Request("NearestTo"), Request("sortExpression"), 0, 0, 0, Request("TrackSp"), EngineerCRBCheck, EngineerCSCSCheck, EngineerUKSecurityCheck, EngineerRightToWorkInUK, EngProofOfMeeting)
                'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
                DataSetToExcel.ConvertNew(ws.WSContact.GetMSSearchAccounts(Request("bizDivId"), Request("skillSet"), Request("region"), Request("postCode"), Request("keyword"), Request("statusId"), Request("fName"), Request("lName"), Request("companyName"), Request("eMail"), Request("vendorIDs"), Request("CRB"), Request("CSCS"), Request("UKSec"), Request("NearestTo"), Request("sortExpression"), 0, 0, 0, TrackSp, EngineerCRBCheck, EngineerCSCSCheck, EngineerUKSecurityCheck, EngineerRightToWorkInUK, EngProofOfMeeting, 0, Request("ExcelPartner"), Request("EPCP"), Request("CategoryType")), Response)
            Case "SearchWO", "searchwo"
                'Poonam modified on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
                DataSetToExcel.ConvertNew(ws.WSWorkOrder.GetSearchWorkOrders(Request("BizDivId"), Request("PONumber"), Request("ReceiptNumber"), Request("PostCode"), Server.UrlDecode(Request("CompanyName")).ToString, Request("Keyword"), Request("WorkOrderId"), Request("sortExpression"), 0, 0, 0, Request("DateStart"), Request("DateSubmittedFrom"), Request("DateSubmittedTo"), Session("UserID")), Response)
            Case "AdminWOListing", "adminwolisting"
                Dim ds As DataSet

                ds = ws.WSWorkOrder.GetAdminWOListing(Request("sortExpression"), 0, 0, Request("Group"), Request("CompID"), Request("FromDate"), Request("ToDate"), Request("BizDivId"), Request("hideTestWOs"), "", False, BillLoc, Request("MainCat"), Request("Watched"), Request("WorkorderID"), Session("UserID"), Request("CompanyName"), Request("CustomerMobile"))
                Dim colsToDelete As New ArrayList()
                Dim woStatus As String = ds.Tables(0).Columns(3).ColumnName
                Dim companyID As String = ds.Tables(0).Columns(4).ColumnName
                Dim contactID As String = ds.Tables(0).Columns(5).ColumnName
                Dim supplierCompanyID As String = ds.Tables(0).Columns(6).ColumnName
                Dim dataRefStatus As String = ds.Tables(0).Columns(15).ColumnName
                colsToDelete.Add("woStatus")
                colsToDelete.Add("companyID")
                colsToDelete.Add("contactID")
                colsToDelete.Add("supplierCompanyID")
                colsToDelete.Add("dataRefStatus")
                'DataSetToExcel.ConvertNew(ds, Response)
                DataSetToExcel.ConvertWOListing(ds, Response, colsToDelete, Request("Group"))
            Case "AdminWOListingNoSupp", "adminwolistingnosupp"
                Dim ds As DataSet
                ds = ws.WSWorkOrder.GetAdminWOListing(Request("sortExpression"), 0, 0, Request("Group"), Request("CompID"), Request("FromDate"), Request("ToDate"), Request("BizDivId"), Request("hideTestWOs"), "", False, BillLoc, Request("MainCat"), Request("Watched"), Request("WorkorderID"), Session("UserID"), Request("CompanyName"), Request("CustomerMobile"))
                Dim colsToDelete As New ArrayList()
                Dim woStatus As String = ds.Tables(0).Columns(3).ColumnName
                Dim companyID As String = ds.Tables(0).Columns(4).ColumnName
                Dim contactID As String = ds.Tables(0).Columns(5).ColumnName
                Dim supplierCompanyID As String = ds.Tables(0).Columns(6).ColumnName
                Dim dataRefStatus As String = ds.Tables(0).Columns(15).ColumnName
                colsToDelete.Add("woStatus")
                colsToDelete.Add("companyID")
                colsToDelete.Add("contactID")
                colsToDelete.Add("supplierCompanyID")
                colsToDelete.Add("dataRefStatus")
                colsToDelete.Add("SupplierContactID")
                colsToDelete.Add("SupplierCompany")
                colsToDelete.Add("SupplierPhone")
                colsToDelete.Add("SupplierMobile")
                colsToDelete.Add("SupplierEmail")
                colsToDelete.Add("SupplierContact")
                'DataSetToExcel.ConvertNew(ds, Response)
                DataSetToExcel.ConvertWOListing(ds, Response, colsToDelete, Request("Group"))
            Case "AdminWORatings", "adminworatings"
                DataSetToExcel.ConvertNew(ws.WSWorkOrder.GetWORatings(Request("FromDate"), Request("ToDate"), Request("BizDivID"), Request("sortExpression"), 0, 0, Request("CompanyId"), 1, Request("WorkorderID")), Response)
            Case "AMSpecialistVNValidated", "amspecialistvnvalidated"
                'DataSetToExcel.Convert(ws.WSAdmin.GetSpecialistListing(Request("bizDivId"), Request("validationType"), Request("sortExpression"), 0, 0, 0), "AMSpecialistVNValidated", Response)
            Case "SupplierPaymentsMade", "supplierpaymentsmade"
                Dim arr As New ArrayList
                arr.Add("RowNum")
                arr.Add("BuyerName")
                arr.Add("WorkOrderId")
                Dim ds As DataSet
                ds = ws.WSFinance.MSAccounts_GetSupplierPaymentDetails(Request("subAccount"), Request("status"), Request("fromDate"), Request("toDate"), Request("bizDivId"), True, Request("sortExpression"), 0, 0, "")
                DataSetToExcel.ConvertNew(ds, Response)
            Case "SupplierPaymentsOutstanding", "supplierpaymentsoutstanding"
                Dim arr As New ArrayList
                arr.Add("RowNum")
                arr.Add("DateModified")
                arr.Add("AdviceNo")
                arr.Add("Status")
                arr.Add("VoucherNumber")
                arr.Add("VoucherSerialNo")
                arr.Add("BizDivId")
                arr.Add("AccountVoucherID")
                arr.Add("DateCompleted")
                arr.Add("WOID")
                Dim ds As DataSet
                ds = ws.WSFinance.MSAccounts_GetSupplierPaymentDetails(Request("subAccount"), Request("status"), Request("fromDate"), Request("toDate"), Request("bizDivId"), True, Request("sortExpression"), 0, 0, Request("Invoices"))
                DataSetToExcel.Convert(ds, Response, arr)
            Case "AccountStatement", "accountstatement"
                Dim ds As DataSet
                'ds = ws.WSFinance.GetAccountVoucherDetails(Request("Account"), Request("subAccount"), Request("Viewer"), Request("FromDate"), Request("ToDate"), Request("bizDivId"), Request("sortExpression"), Request("startRowIndex"), Request("maximumRows"))
                'Code added by PB: to hide the balance field (temporarily). 
                Dim colsToDelete As New ArrayList()
                colsToDelete.Add("Balance")
                DataSetToExcel.ConvertNew(ds, Response)
            Case "OrderWorkPayments", "orderworkpayments"
                Dim ds As DataSet
                'ds = ws.WSFinance.GetOWPayments(Request("status"), Request("fromDate"), Request("toDate"), Request("bizDivId"), Request("sortExpression"), Request("startRowIndex"), Request("maximumRows"))
                DataSetToExcel.ConvertNew(ds, Response)
            Case "CurrentSalesReport", "currentsalesreport"
                Dim ds As DataSet
                'ds = ws.WSAdmin.CurrentSalesReport(Request("bizDivId"))
                DataSetToExcel.ConvertNew(ds, "CurrentSalesReport", Response)
            Case "CompletedSalesReport", "completedsalesreport"
                Dim ds As DataSet
                'ds = ws.WSAdmin.CompletedSalesReport(Request("Month"), Request("Year"), Request("Days"), Request("bizDivId"))
                DataSetToExcel.ConvertNew(ds, "CompletedSalesReport", Response)
            Case "OWControlAccounts", "owcontrolaccounts"
                'Dim arr As New ArrayList
                'arr.Add("Balance")
                'arr.Add("AccountVoucherId")
                'arr.Add("Account")
                'arr.Add("SubAccount")
                'arr.Add("WorkOrderId")
                'arr.Add("VoucherSerialNo")
                'arr.Add("VoucherTypeId")
                'arr.Add("VAT")
                'arr.Add("RowNum")

                Dim ds As DataSet
                Dim contactid As String
                If Request("ContactId") = "" Then
                    contactid = "0"
                Else
                    contactid = Request("ContactId")
                End If
                ds = ws.WSFinance.MS_GetAccountVoucherDetails(Request("bizDivId"), "'" & Request("AccountType") & "'", contactid, Request("fromDate"), Request("toDate"), Request("sortExpression"), 0, 0)
                DataSetToExcel.ConvertNew(ds, Response)
            Case "PurchaseInvoicePaid", "purchaseinvoicepaid"
                Dim arr As New ArrayList
                arr.Add("AdviceNo")
                arr.Add("BizDivId")
                arr.Add("CompanyId")
                arr.Add("InvoiceNetAmnt")
                arr.Add("InvoiceType")
                arr.Add("Price")
                arr.Add("Discount")
                arr.Add("VAT")
                arr.Add("XSrc")
                arr.Add("WOID")
                arr.Add("PaymentNo")
                arr.Add("Refunded")
                Dim ds As DataSet
                Dim contactid As String
                If Request("ContactId") = "" Then
                    contactid = "0"
                Else
                    contactid = Request("ContactId")
                End If
                ds = ws.WSFinance.MS_GetPurchasePaymentAdviceListing(Request("bizDivId"), Request("adviceType"), contactid, Request("status"), Request("fromDate"), Request("toDate"), "admin", Request("sortExpression"), 0, 0)
                DataSetToExcel.ConvertNew(ds, Response)
            Case "PIUnpaidReport"
                Dim ds As DataSet
                ds = ws.WSFinance.MS_GetUnpaidPIReport(1) 'BizDivID passed as 1
                DataSetToExcel.ConvertNew(ds, Response)
            Case "ViewAllInvoices"
                Dim ds As DataSet
                ds = ws.WSFinance.MS_GetAllInvoices(Request("CompanyID"), Request("BizDivId"), Request("FromDate"), Request("ToDate"), Request("sortExpression"), Request("startRowIndex"), 0)
                Dim arr As New ArrayList
                arr.Add("discount")
                DataSetToExcel.Convert(ds, Response, arr)
            Case "SalesInvoiceWO", "salesinvoicewo"
                DataSetToExcel.ConvertNew(ws.WSFinance.MS_GetWOsForSalesInvoice(Request("BizDivId"), Request("invoiceNo"), Request("sortExpression"), 0, 0), Response)
            Case "DixonsComplaints"
                DataSetToExcel.ConvertNew(ws.WSContact.GetDixonsComplaintsListing(Request("IsProcessed"), Request("sortExpression"), Request("startRowIndex"), 0, 0, Request("ComplaintRefNo"), Request("ComplaintType"), Request("DateCreatedFrom"), Request("DateCreatedTo"), Request("DateProcessedFrom"), Request("DateProcessedTo"), ""), Response)
            Case "ServicePartnerFeedback"
                DataSetToExcel.ConvertNew(ws.WSContact.GetDixonsComplaintsListing(Request("IsProcessed"), Request("sortExpression"), Request("startRowIndex"), 0, 0, Request("ComplaintRefNo"), Request("ComplaintType"), Request("DateCreatedFrom"), Request("DateCreatedTo"), "", "", "Feedback"), Response)
            Case "Receivables", "receivables"
                Dim ds As DataSet
                Dim contactid As String
                Dim arr As New ArrayList
                arr.Add("Refunded")
                arr.Add("Comments")
                arr.Add("DateCreated")
                arr.Add("WOID")
                arr.Add("WOTitle")
                arr.Add("PONumber")
                arr.Add("RefundAmount")

                If Request("ContactId") = "" Then
                    contactid = "0"
                Else
                    contactid = Request("ContactId")
                End If
                ds = ws.WSFinance.MS_GetSalesReceiptAdviceListing(Request("bizDivId"), Request("adviceType"), contactid, Request("status"), "", "", "admin", Request("sortExpression"), 0, 0, "", Request("Invoices"))
                DataSetToExcel.ConvertNew(ds, Response)
            Case "SalesInvoicePaid", "salesinvoicepaid"

                Dim ds As DataSet
                Dim contactid As String
                If Request("ContactId") = "" Then
                    contactid = "0"

                Else
                    contactid = Request("ContactId")
                End If
                ds = ws.WSFinance.MS_GetPaidSalesListing(Request("bizDivId"), Request("adviceType"), contactid, Request("status"), Request("fromDate"), Request("toDate"), "admin", Request("sortExpression"), 0, 0)
                DataSetToExcel.ConvertNew(ds, Response)
            Case "SalesInvoiceGeneration", "salesinvoicegeneration"
                Dim ds As DataSet
                Dim contactid As String
                If Request("ContactId") = "" Then
                    Return

                Else
                    contactid = Request("ContactId")
                End If
                ds = ws.WSWorkOrder.MS_GetClosedWOUnInvoiced(Request("bizDivId"), Request("ContactId"), "", "", Request("sortExpression"), 0, 0)
                Try
                    DataSetToExcel.ConvertNew(ds, Response)
                Catch ex As Exception

                End Try
            Case "SupplierInvoices", "supplierinvoices"
                DataSetToExcel.ConvertNew(ws.WSFinance.MS_GetPurchasePaymentAdviceListing(Request("bizDivId"), Request("adviceType"), Request("CompanyID"), Request("status"), Request("fromDate"), Request("toDate"), "admin", Request("sortExpression"), 0, 0), Response)
            Case "Invoices", "invoices"
                DataSetToExcel.ConvertNew(ws.WSFinance.MS_GetSalesReceiptAdviceListing(Request("bizDivId"), Request("adviceType"), Request("CompanyID"), Request("status"), Request("fromDate"), Request("toDate"), "admin", Request("sortExpression"), 0, 0, "", ""), Response)
            Case "FavouriteSupplier", "favouritesupplier"
                DataSetToExcel.ConvertNew(ws.WSContact.MS_GetFavouriteSuppliers(Request("bizDivId"), Request("BuyerCompanyId"), Request("sortExpression"), Request("startRowIndex"), Request("maximumRows"), ViewState("RowCount"), Request("mode"), Request("SearchContactId"), Request("BillingLocId"), Request("Comment"), Request("FixedCapacity"), Request("FixedCapacityTimeSlot"), Request("AllowJobAcceptance")), Response)
            Case "AdminSearchSuppliers", "adminsearchsuppliers"
                DataSetToExcel.ConvertNew(ws.WSContact.MS_GetSuppliers(Request("buyerCompanyId"), Request("bizDivId"), Request("skillSet"), Request("region"), Request("postCode"), Request("keyword"), Request("statusId"), Request("fName"), Request("lName"), Request("companyName"), Request("eMail"), Request("sortExpression"), 0, 0, 0, Request("mode")), Response)
            Case "SingleCompanyReports", "singlecompanyreports"
                DataSetToExcel.ConvertNew(ws.WSWorkOrder.woMSGenerateReportCompanyWO(Request("bizDivId"), Request("StrCompanyID"), Request("fromDate"), Request("toDate")), Response)
            Case "UpSellSalesInvoicePaid", "upsellsalesinvoicepaid"
                Dim ds As DataSet
                Dim arr As New ArrayList
                arr.Add("RowNum")
                arr.Add("rn")
                arr.Add("InvoiceType")
                arr.Add("BizDivId")
                arr.Add("RefWOID")
                arr.Add("WOID")
                arr.Add("InvoiceDate")
                arr.Add("InvoiceTotal")
                arr.Add("CashReceiptNo")

                ds = ws.WSFinance.MS_GetUpSellSalesReceiptAdviceListing(Request("bizDivId"), Request("status"), Request("fromDate"), Request("toDate"), Request("sortExpression"), 0, 0)
                DataSetToExcel.ConvertNew(ds, Response)
            Case "WebLeads", "webleads"
                DataSetToExcel.ConvertNew(ws.WSContact.GetRegistrantListing(Request("SortExp"), Request("StartIndex"), Request("PageSize")), Response)
            Case "TescoTechSupport", "tescotechsupport"
                Dim arr As New ArrayList
                arr.Add("WOID")
                Dim ds As DataSet
                ds = ws.WSWorkOrder.MS_TescoReport(Request("Month"), Request("Year"), Request("Mode"))
                DataSetToExcel.ConvertNew(ds, Response)
            Case "SLAReport"
                If Not IsNothing(Request("FDate")) And Not IsNothing(Request("TDate")) Then
                    DataSetToExcel.ConvertNew(ws.WSWorkOrder.GetClientSLAReport(7677, Request("FDate"), Request("TDate"), "Excel"), Response)
                End If
            Case "IE", "ie"
                Dim ds As DataSet
                Dim bizDivId As Integer = Session("BizDivId")
                ds = ws.WSContact.GetInsuranceDetails(Request("fromDate"), Request("toDate"), bizDivId, Request("sortExpression"), 0, 0)
                DataSetToExcel.ConvertNew(ds, Response)
            Case "SagePayExport", "sagepayexport"
                Dim ds As DataSet
                ds = ws.WSFinance.SagePayExport(Request("mode"), Request("FromDate"), Request("ToDate"))
                DataSetToExcel.ConvertNew(ds, Response)
            Case "PIUnpaidAvailable"
                Dim ds As DataSet
                ds = ws.WSFinance.MS_GetPurchaseInvoicesForStatusUA(Request("ContactId").ToString, Request("BizDivId"), Request("Over30Day"), Request("HideAvailable"), Request("sortExpression"), Request("startRowIndex"), 0, Request("HideDemo"), Request("mode"))
                DataSetToExcel.ConvertNew(ds, Response)
            Case "AutoUnapprovedSuppliers"
                Dim bizDivId As Integer = Request("bizDivId")
                Dim fromDate As String = Request("fromDate")
                Dim toDate As String = Request("toDate")
                Dim mainClassId As String = Request("mainClassId")
                Dim subClassId As String = Request("subClassId")
                Dim status As String = Request("status")
                Dim statusId As String = Request("statusId")

                Dim dateCriteriaField As String = ""
                If Not IsNothing(Request("dateCriteriaField")) Then
                    dateCriteriaField = Request("dateCriteriaField") 'HttpContext.Current.Items("dateCriteriaField")                
                End If
                Dim sortExpression As String = Request("sortExpression")
                Dim startRowIndex As String = Request("startRowIndex")
                Dim maximumRows As String = Request("maximumRows")
                Dim rowCount As Integer = 0
                Dim ds As DataSet = ws.WSContact.GetAutoUnapprovedSuppliersListing(bizDivId, fromDate, toDate, sortExpression, startRowIndex, maximumRows, rowCount)
                DataSetToExcel.ConvertNew(ds, Response)
        End Select
    End Sub

End Class