<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintDepotSheets.aspx.vb" Inherits="Admin.PrintDepotSheets" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Print Depot Sheets</title>
    <link href="Styles/OrderWork.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#F4F5F0" onload="javascript:this.print()">
    <form id="form1" runat="server">
    <div style="margin-left:auto; margin-right:auto; background-color:#ffffff; width:730px;">
    <asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
			<table height="500" width="700" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FEFFD3">
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" align="center">
						  <tr>
							<td>&nbsp;</td>
							<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#CC3300; font-weight:bold;"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
						  </tr>
						</table>
					</td>
				</tr>
			</table>	
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlPrintContent" Visible="true">
		
    <asp:DataList ID="dlPrint" runat="server">
		<ItemTemplate>
            <div style="width:723px; border-bottom:2px solid #CCCCCC; padding-bottom:30px;">
            <div id="divPrintWOHeader" style="border-bottom:1px solid #CCCCCC; height:135px;  ">
                    <div style="width:300px; float:left">
			        <div id="divPrintWOHeaderTopBand"></div>
			        <div id="divPrintWOHeaderMiddleBand">
			        <img id="Img1" src="~/Images/Invoice_Logo_New.gif" runat="server" align="left" class="imgLogo" style="margin-top:25px" alt="OrderWork - Services Reinvented" border="0"/></div>
			        <div id="divPrintWOHeaderBtmBand"></div>
			        </div>
			        <div style="text-align:right;padding-right:20px; padding-top:10px; width:300px; float:right;  ">
				        <b>Orderwork Ltd<br />
                            Montpelier Chambers<br />
                             61-63 High Street South<br />
                            114 Power Road<br />
                            Dunstable, LU6 3SF.<br />
				            Tel:  0203 053 0343<br />
				            Fax:  0203 006 8542</b>
				    <div id="divClearBoth"></div>       		
		        </div>
                <div id="divClearBoth"></div>
		        </div>
	        <div style="height:10px;"></div>
            <h1 align="center">Depot Collection</h1>
            <div style="height:10px; "></div>
            <table width="720" border="0" cellpadding="0" cellspacing="0" align="center" style=" font-size:10px; font-family:Verdana ">
              <tr>
  	            <td width="10">&nbsp;</td>
                <td  width="180" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7; padding-left:10px" height="20">
		            <b>To:</b>
	            </td>
	            <td width="540" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7;border-right:1px solid #b7b7b7; padding-left:10px;" height="20">
		            <%#Container.DataItem("UserDepot")%>, <%#Container.DataItem("BuyerCompanyName")%>
	            </td>
              </tr>
              <tr>
              <td width="10">&nbsp;</td>
                <td  width="180" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7; padding-left:10px" height="20" >
		            <b>Depot Address:</b>
	            </td>
	            <td width="540" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7;border-right:1px solid #b7b7b7; padding-left:10px;" height="20">
		            <%#Container.DataItem("DepotName")%>, <%#Container.DataItem("DepotAddress")%>
	            </td>
              </tr>
              <tr>
              <td width="10">&nbsp;</td>
                <td width="180" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7; padding-left:10px" height="20" >
		            <b>From:</b>
	            </td>
	            <td width="540" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7;border-right:1px solid #b7b7b7; padding-left:10px;" height="20">
		            &nbsp;<%#Container.DataItem("OrderWorkUsername")%>
	            </td>
              </tr>
              <tr>
              <td width="10">&nbsp;</td>
                <td  width="180" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7; padding-left:10px" height="20">
		            <b>Date:</b>
	            </td>
	            <td width="540" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7;border-right:1px solid #b7b7b7; padding-left:10px;" height="20">
		            <%#Strings.FormatDateTime(Container.DataItem("Date").ToString(), DateFormat.ShortDate)%>
	            </td>
              </tr>
                <tr>
	            <td width="10">&nbsp;</td>
                <td  width="180" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7; border-bottom:1px solid #b7b7b7;padding-left:10px" height="20">
		            <b>Subject:</b>
	            </td>
	            <td width="540" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7;border-right:1px solid #b7b7b7; padding-left:10px; border-bottom:1px solid #b7b7b7;" height="20">
		            Items to be collected by <%#Container.DataItem("Supplier")%> on <%#Request.QueryString("CDate")%>
	            </td>
              </tr>
            </table>
            
            <div style="height:50px; "></div>
            <table width="720" border="0" cellpadding="0" cellspacing="0" align="center" >
            <tr>
                <td width="10">&nbsp;</td>
                <td  width="120" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7; padding-left:10px; " height="20">
		            <b>Surname</b>
	            </td>
	             <td  width="120" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7; padding-left:10px; " height="20">
		            <b>PostCode</b>
	            </td>
	             <td  width="120" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7; padding-left:10px; " height="20">
		            <b>CR Number</b>
	            </td>
	             <td  width="120" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7; padding-left:10px; " height="20">
		            <b>Call Num </b>
	            </td>	             
	             <td  width="119" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7; padding-left:10px;border-right:1px solid #b7b7b7;" height="20">
		            <b>Signature</b>
	            </td>
            </tr> 
            </table>
            <div style="border-bottom:1px solid #b7b7b7;margin-left:10px">
            
           <asp:DataList ID="dlPrintWorkOrder" runat="server">
		<ItemTemplate>
            <table width="710" border="0" cellpadding="0" cellspacing="0" align="center" >        
            <tr>
          
                <td  width="120" style="border-top:3px solid #b7b7b7; border-left:1px solid #b7b7b7;  padding-left:10px;" height="20">
		            &nbsp;<%#Container.DataItem("UserDepot")%>
	            </td>
	             <td  width="120" style="border-top:3px solid #b7b7b7; border-left:1px solid #b7b7b7;  padding-left:10px;" height="20">
		            &nbsp;<%#Container.DataItem("PostCode")%>
	            </td>
	             <td  width="120" style="border-top:3px solid #b7b7b7; border-left:1px solid #b7b7b7;  padding-left:10px;" height="20">
		            &nbsp;<%# Container.DataItem("WOCRNumber") %>
	            </td>
	             <td  width="120" style="border-top:3px solid #b7b7b7; border-left:1px solid #b7b7b7;  padding-left:10px;" height="20">
		            &nbsp;<%# Container.DataItem("PONumber") %>
	            </td>	            
	             <td  width="120" style="border-top:3px solid #b7b7b7; border-left:1px solid #b7b7b7;border-right:1px solid #b7b7b7;  padding-left:10px;" height="20">
		            &nbsp;
	            </td>
            </tr>
            <tr>
                <td colspan="6" width="100%" style="border-top:1px solid #b7b7b7; border-left:1px solid #b7b7b7;border-right:1px solid #b7b7b7;padding-left:35px;padding-top:5px;padding-bottom:5px;">
                    <b>Products Purchased</b>&nbsp;:&nbsp;<%#GetCompleteString(Container.DataItem("ProductsPurchased"),Container.DataItem("AddProductsPurchased"))%><br />
                    
                </td>
            </tr>            
        </table>	
        </ItemTemplate></asp:DataList>
        
        </div>
    </div>
    
    <div style="page-break-after:always"></div>
    </ItemTemplate></asp:DataList>
    </asp:Panel>
    </div>
    </form>
</body>
</html>
