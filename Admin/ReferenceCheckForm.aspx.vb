

Partial Public Class ReferenceCheckForm
    Inherits System.Web.UI.Page

    Shared ws As New WSObjs
    Dim totalrating As Integer = 0


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsNothing(Request("contactType")) Then
            If Request("contactType") = "3" Or Request("contactType") = "4" Then
                tblApprovedSupp.Visible = False
                tblApprovedSuppTop.Visible = False
                divMailMessage.Visible = False
                tblSendMail.Visible = False
            Else
                tblApprovedSupp.Visible = True
                tblApprovedSuppTop.Visible = True
                divMailMessage.Visible = True
                tblSendMail.Visible = True
            End If
        End If
        divValidationMain.Visible = False
        lblErrorMsg.Text = ""

        If Not IsPostBack Then

            ViewState("CompanyId") = Request("CompanyID")

            If Not IsNothing(Request("sender")) Then
                If Request("sender") = "AdminContactsListing" Then
                    ViewState("ContactId") = Request("UserId")
                ElseIf Request("sender") = "AdminSearchAccounts" Then
                    ViewState("ContactId") = Request("contactId")
                End If
                Dim strBackLink As String = prepareBackToListing()
                lnkBackToListingTop.HRef = strBackLink
                lnkBackToListing.HRef = strBackLink
            End If
            If ApplicationSettings.Country = ApplicationSettings.CountryDE Then
                rgVatNo.Enabled = False
            Else
                rgVatNo.Enabled = True
            End If

            'Populate No of Employees drop down 
            'CommonFunctions.PopulateNoOfEmployees(Page, ddlNoOfEmployees)

            populatedata(True)
        End If


    End Sub
    Public Function prepareBackToListing() As String
        Select Case Request("sender")
            Case "AdminContactsListing"
                Return "AMContacts.aspx?CompanyID=" & ViewState("CompanyId") & "&UserId=" & ViewState("ContactId") & "&BizDivId=" & Request("BizDivId") & "&contactType=" & Request("contactType") & "&PS=" & Request("PS") & "&PN=" & Request("PN") & "&SC=" & Request("SC") & "&SO=" & Request("SO") & "&FromDate=" & Request("FromDate") & "&ToDate=" & Request("ToDate")
            Case "AdminSearchAccounts"
                Return "SearchAccounts.aspx?sender=AdminSearchAccounts&companyId=" & ViewState("CompanyId") & "&contactId=" & ViewState("ContactId") & "&bizDivId=" & Request("BizDivId") & "&classId=" & Request("classId") & "&PS=" & Request("PS") & "&PN=" & Request("PN") & "&SC=" & Request("SC") & "&SO=" & Request("SO") & "&roleGroupId=" & Request("roleGroupId") & "&statusId=" & Request("statusId") & "&product=" & Request("product") & "&region=" & Request("region") & "&txtPostCode=" & Request("txtPostCode") & "&txtKeyword=" & Request("txtKeyword") & "&txtFName=" & Request("txtFName") & "&txtLName=" & Request("txtLName") & "&txtCompanyName=" & Request("txtCompanyName") & "&txtEmail=" & Request("txtEmail") & "&refcheck=" & Request("refcheck")
        End Select
        Return ""
    End Function

    Public Sub populatedata(Optional ByVal killcache As Boolean = False)

        Dim dv As DataView
        Dim i As Integer

        Dim dsRefCheck As DataSet = GetRefInfo(True)
        dv = dsRefCheck.Tables("ContactAttributes").Copy.DefaultView
        If dv.Count > 0 Then

            ViewState("SupplierCreatedCompanyID") = dv.Item(0).Item("SupplierCreatedCompanyID")

            lblCompanyName.Text = dv.Item(0).Item("CompanyName")
            Dim strVAT As String = Trim(dv.Item(0).Item("VATRegistrationNumber"))
            If strVAT <> "" Then
                Select Case ApplicationSettings.Country
                    Case ApplicationSettings.CountryUK
                        If strVAT.IndexOf("gb") = -1 And strVAT.IndexOf("GB") = -1 And strVAT.IndexOf("Gb") = -1 And strVAT.IndexOf("gB") = -1 Then
                            strVAT = "GB " & strVAT
                        End If
                    Case ApplicationSettings.CountryDE
                        If strVAT.IndexOf("de") = -1 And strVAT.IndexOf("DE") = -1 And strVAT.IndexOf("De") = -1 And strVAT.IndexOf("dE") = -1 Then
                            strVAT = "DE " & strVAT
                        End If

                End Select

                strVAT = StrConv(strVAT, VbStrConv.Uppercase)
            End If
            txtVATRegNo.Text = strVAT
            txtCompRegNo.Text = dv.Item(0).Item("CompanyRegNo")
            'If dv.Item(0).Item("CompanyStrength") <> 0 Then
            '    ddlNoOfEmployees.SelectedValue = dv.Item(0).Item("CompanyStrength")
            'Else
            '    ddlNoOfEmployees.SelectedValue = ""
            'End If
            If Not IsDBNull(dv.Item(0).Item("NoOfEngineers")) Then
                If (dv.Item(0).Item("NoOfEngineers") <> 0) Then
                    txtNoOfEngineers.Text = dv.Item(0).Item("NoOfEngineers")
                End If
            End If

            'populateLabels(dv, lblEmpLiability, "EmployersLiability")
            If dv.Item(0).Item("EmployersLiability") = True Then
                lblEmpLiability.Text = "Yes"
                txtCoverAmountEmpLiability.Text = dv.Item(0).Item("EmpLiabilityCoverAmt")
                txtEmployeeLiabilityDate.Text = dv.Item(0).Item("EmpLiabilityExpDate")
                'OA-531:Sagar
                calEmpLDate.Enabled = True
            Else
                lblEmpLiability.Text = ""
                txtCoverAmountEmpLiability.Enabled = False
                txtEmployeeLiabilityDate.Enabled = False
                'OA-531:Sagar
                calEmpLDate.Enabled = False
            End If

            If dv.Item(0).Item("PublicLiability") = True Then
                lblPublicLiability.Text = "Yes"
                txtCoverAmountPubLiability.Text = dv.Item(0).Item("PublicLiabilityCoverAmt")
                txtPublicLiabilityDate.Text = dv.Item(0).Item("PublicLiabilityExpDate")
                'OA-531:Sagar
                calPubLDate.Enabled = True
            Else
                lblPublicLiability.Text = ""
                txtCoverAmountPubLiability.Enabled = False
                txtPublicLiabilityDate.Enabled = False
                'OA-531:Sagar
                calPubLDate.Enabled = False
            End If

            If dv.Item(0).Item("ProfessionalIndemnity") = True Then
                lblProfIndemnity.Text = "Yes"
                txtCoverAmountProfIndemnity.Text = dv.Item(0).Item("ProfIdemnityCoverAmt")
                txtProfessionalIndemnityDate.Text = dv.Item(0).Item("ProfIndemnityExpDate")
                'OA-531:Sagar
                calIndemDate.Enabled = True
            Else
                lblProfIndemnity.Text = ""
                txtCoverAmountProfIndemnity.Enabled = False
                txtProfessionalIndemnityDate.Enabled = False
                'OA-531:Sagar
                calIndemDate.Enabled = False
            End If
            txtGeneralComments.Text = dv.Item(0).Item("GeneralComments")
        End If

        Dim path As String
        If dsRefCheck.Tables("EmployeeLiabiliyAttachment").Rows.Count <> 0 Then   '  for EmployeeLiability Attachment
            path = "OpenAttachment.aspx?path=" & dsRefCheck.Tables("EmployeeLiabiliyAttachment").Rows(0).Item(1) & ""
            'poonam modified on - 29/9/2015 - Task - OA-72:OA - Broken Icons in ReferenceCheckForm.aspx
            anchorEmployee.InnerHtml = "<img src='Images/Icons/Icon-word.jpg' width='19' height='19' hspace='5' border='0' align='absmiddle'>" & dsRefCheck.Tables("EmployeeLiabiliyAttachment").Rows(0).Item(0)
            anchorEmployee.HRef = path

        End If
        If dsRefCheck.Tables("PublicLiabilityAttachment").Rows.Count <> 0 Then   '  for Public  Liability Attachment
            path = "OpenAttachment.aspx?path=" & dsRefCheck.Tables("PublicLiabilityAttachment").Rows(0).Item(1) & ""
            'poonam modified on - 29/9/2015 - Task - OA-72:OA - Broken Icons in ReferenceCheckForm.aspx
            anchorPublic.InnerHtml = "<img src='Images/Icons/Icon-word.jpg' width='19' height='19' hspace='5' border='0' align='absmiddle'>" & dsRefCheck.Tables("PublicLiabilityAttachment").Rows(0).Item(0)
            anchorPublic.HRef = path
        End If
        If dsRefCheck.Tables("ProfIndemnityAttachment").Rows.Count <> 0 Then   '  for Professional Indemnity Attachment
            path = "OpenAttachment.aspx?path=" & dsRefCheck.Tables("ProfIndemnityAttachment").Rows(0).Item(1) & ""
            'poonam modified on - 29/9/2015 - Task - OA-72:OA - Broken Icons in ReferenceCheckForm.aspx
            anchorProfIndemnity.InnerHtml = "<img src='Images/Icons/Icon-word.jpg' width='19' height='19' hspace='5' border='0' align='absmiddle'>" & dsRefCheck.Tables("ProfIndemnityAttachment").Rows(0).Item(0)
            anchorProfIndemnity.HRef = path
        End If

        'for attachments
        dv = dsRefCheck.Tables("ContactAttachInsur").Copy.DefaultView
        'for references
        dv = dsRefCheck.Tables("ContactReferences").Copy.DefaultView
        dv.RowFilter = "Sequence = 1"
        If dv.Count <> 0 Then
            lblCompName1.Text = dv.Item(0).Item("Company")
            lblContact1.Text = dv.Item(0).Item("Name")
            lblEmail1.Text = dv.Item(0).Item("Email")
            lblPhone1.Text = dv.Item(0).Item("Phone")

            If Not IsDBNull(dv.Item(0).Item("WorkDoneDate")) Then
                lblWorkDate1.Text = dv.Item(0).Item("WorkDoneDate")
            End If

            lblDesc1.Text = dv.Item(0).Item("WorkDone")

        End If

        dv.RowFilter = "Sequence = 2"
        If dv.Count <> 0 Then
            lblCompName2.Text = dv.Item(0).Item("Company")
            lblContact2.Text = dv.Item(0).Item("Name")
            lblEmail2.Text = dv.Item(0).Item("Email")
            lblPhone2.Text = dv.Item(0).Item("Phone")

            If Not IsDBNull(dv.Item(0).Item("WorkDoneDate")) Then
                lblWorkDate2.Text = dv.Item(0).Item("WorkDoneDate")
            End If


            lblDesc2.Text = dv.Item(0).Item("WorkDone")

        End If

        dv.RowFilter = "Sequence = 3"
        If dv.Count <> 0 Then
            lblCompName3.Text = dv.Item(0).Item("Company")
            lblContact3.Text = dv.Item(0).Item("Name")
            lblEmail3.Text = dv.Item(0).Item("Email")
            lblPhone3.Text = dv.Item(0).Item("Phone")

            If Not IsDBNull(dv.Item(0).Item("WorkDoneDate")) Then
                lblWorkDate3.Text = dv.Item(0).Item("WorkDoneDate")
            End If


            lblDesc3.Text = dv.Item(0).Item("WorkDone")

        End If

        ' for Approval date
        dv = dsRefCheck.Tables("ContactApproval").Copy.DefaultView
        Dim approvalDate As String = ""
        Dim FullName As String = ""

        If dv.Count <> 0 Then
            If Not IsDBNull(dv.Item(0).Item("ApprovalDate")) Then
                approvalDate = CType(dv.Item(0).Item("ApprovalDate"), Date).ToShortDateString
            End If
            FullName = dv.Item(0).Item("FullName")


        End If

        '   for Rating and Comments 
        dv = dsRefCheck.Tables("ContactRatingsRC").Copy.DefaultView
        dv.Sort = "LinkID ASC"
        If dv.Count <> 0 Then

            'to display date at top of the page - neetu
            If Request("statusId") = ApplicationSettings.StatusApprovedCompany Then   ' ConfigUtil.GetAppSetting("Status_ApprovedCompany")
                ' lblTimeStamp.Text = "Reference Checked on " & CType(Request("ApprovedOn"), Date).ToShortDateString
                lblTimeStamp.Text = "Approved by " & FullName & " on " & approvalDate & "<br>Modified by " & dv.Item(0).Item("Name") & " on " & CType(dv.Item(0).Item("DateModified"), Date).ToShortDateString

            Else
                lblTimeStamp.Text = "Modified by " & dv.Item(0).Item("Name") & " on " & CType(dv.Item(0).Item("DateModified"), Date).ToShortDateString
            End If

            Dim str As String
            For i = 1 To dv.Count
                CType(Master.FindControl("ContentPlaceHolder1").FindControl("rdoRatingNeutral" & i), RadioButton).Checked = False
                CType(Master.FindControl("ContentPlaceHolder1").FindControl("txtMessage" & i), TextBox).Text = dv.Item(i - 1).Item("Comments")

                If dv.Item(i - 1).Item("Rating") = 0 Then
                    CType(Master.FindControl("ContentPlaceHolder1").FindControl("rdoRatingNeutral" & i), RadioButton).Checked = True
                End If
                If dv.Item(i - 1).Item("Rating") = 1 Then
                    CType(Master.FindControl("ContentPlaceHolder1").FindControl("rdoRatingPositive" & i), RadioButton).Checked = True
                End If
                If dv.Item(i - 1).Item("Rating") = -1 Then
                    CType(Master.FindControl("ContentPlaceHolder1").FindControl("rdoRatingNegative" & i), RadioButton).Checked = True
                End If
            Next
        End If
        'poonam modified on 14/8/2015 - Task - OM-13:OW - ENH - Add Create Supplier account Page in OW
        If dsRefCheck.Tables("ContactEmail").Rows.Count <> 0 Then
            ViewState("UserFirstName") = dsRefCheck.Tables("ContactEmail").Rows(0)("AttributeValue")
        End If

        'If dsRefCheck.Tables("ContactEmail").Rows.Count <> 0 Then
        '    Session("ApproveUserName") = dsRefCheck.Tables("ContactEmail").Rows(0)("UserName")
        '    Session("ApproveFName") = dsRefCheck.Tables("ContactEmail").Rows(0)("AttributeValue")
        'End If


        ' ''If Not IsNothing(Request("contactType")) Then
        ' ''    If Request("contactType") = "3" Or Request("contactType") = "4" Then
        ' ''        'Set Track supplier checkbox 
        ' ''        If dsRefCheck.Tables("TrackDetailsSP").Rows.Count <> 0 Then
        ' ''            chkTrackSP.Checked = dsRefCheck.Tables("TrackDetailsSP").Rows(0)("TrackSP")
        ' ''        End If
        ' ''    Else
        ' ''        chkTrackSP.Checked = True
        ' ''    End If
        ' ''End If




        If Session("EmailMsg") = "" Then
            txtMailMessage.Text = EmailContents.GetString("AccountStatusMessage")
        End If


    End Sub

    Private Sub btnSaveTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click
        Page.Validate()
        If Page.IsValid Then
            Session("action") = "Save"
            saveData()
        Else
            divValidationMain.Visible = True

        End If
        ViewState("WarningCount") = 0
    End Sub
    Private Sub btnSaveTopNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveTopNew.Click
        Page.Validate()
        If Page.IsValid Then
            Session("action") = "Save"
            saveData()

        Else
            divValidationMain.Visible = True
        End If
        ViewState("WarningCount") = 0
    End Sub

    Public Sub saveData()
        Page.Validate()
        If Page.IsValid Then
            'Dim ds As DataSet = GetRefInfo(False)
            Dim dsContacts As New XSDContacts
            Dim contactid As Integer = ViewState("CompanyId")



            'For updating tblContactsAttributes

            'Save General Comments
            Dim nrowGC As XSDContacts.ContactsAttributesRow = dsContacts.ContactsAttributes.NewRow
            With nrowGC
                .ContactID = contactid
                .GeneralComments = txtGeneralComments.Text.Trim
                If txtVATRegNo.Text.Trim <> "" Then
                    .VATRegistrationNumber = txtVATRegNo.Text.Trim
                    .VATRegistered = True
                Else
                    .VATRegistrationNumber = ""
                End If
                .CompanyRegNo = txtCompRegNo.Text.Trim
                'If ddlNoOfEmployees.SelectedValue.ToString() <> "" Then
                '    .CompanyStrength = ddlNoOfEmployees.SelectedValue.ToString()
                'End If
                If (txtNoOfEngineers.Text.Trim <> "") Then
                    .NoOfEngineers = txtNoOfEngineers.Text.Trim
                End If
                If txtCoverAmountEmpLiability.Text.Trim <> "" Then
                    If Convert.ToDecimal(txtCoverAmountEmpLiability.Text) <> 0 Then
                        .EmpLiabilityCoverAmt = txtCoverAmountEmpLiability.Text.Trim
                        .EmpLiabilityExpDate = txtEmployeeLiabilityDate.Text.Trim
                        .EmployersLiability = True
                    End If
                End If
                If txtCoverAmountProfIndemnity.Text.Trim <> "" Then
                    If Convert.ToDecimal(txtCoverAmountProfIndemnity.Text) <> 0 Then
                        .ProfIdemnityCoverAmt = txtCoverAmountProfIndemnity.Text.Trim
                        .ProfIndemnityExpDate = txtProfessionalIndemnityDate.Text.Trim
                        .ProfessionalIndemnity = True
                    End If
                End If
                If txtCoverAmountPubLiability.Text.Trim <> "" Then
                    If Convert.ToDecimal(txtCoverAmountPubLiability.Text) <> 0 Then
                        .PublicLiabilityCoverAmt = txtCoverAmountPubLiability.Text.Trim
                        .PublicLiabilityExpDate = txtPublicLiabilityDate.Text.Trim
                        .PublicLiability = True
                    End If
                End If
            End With
            dsContacts.ContactsAttributes.Rows.Add(nrowGC)

            'For updating tblContactsRating
            Dim rating As Integer = 0
            totalrating = 0
            'for rating1
            If rdoRatingPositive1.Checked = True Then
                rating = 1
                totalrating += 1
            ElseIf rdoRatingNeutral1.Checked = True Then
                rating = 0
            ElseIf rdoRatingNegative1.Checked = True Then
                rating = -1
            End If

            Dim nrowRating1 As XSDContacts.tblContactsRatingRow = dsContacts.tblContactsRating.NewRow
            With nrowRating1
                .RatedCompany = ViewState("CompanyId")
                .RatedContact = ViewState("ContactId")
                .RatingCompany = Session("CompanyId")
                .RatingContact = Session("UserId")
                .Rating = rating
                .Comments = txtMessage1.Text.Trim
                .LinkID = 1
                .LinkSource = "ReferenceCheck"
            End With
            dsContacts.tblContactsRating.Rows.Add(nrowRating1)


            'for rating2
            If rdoRatingPositive2.Checked = True Then
                rating = 1
                totalrating += 1
            ElseIf rdoRatingNeutral2.Checked = True Then
                rating = 0
            ElseIf rdoRatingNegative2.Checked = True Then
                rating = -1
            End If
            Dim nrowRating2 As XSDContacts.tblContactsRatingRow = dsContacts.tblContactsRating.NewRow
            With nrowRating2
                .RatedCompany = ViewState("CompanyId")
                .RatedContact = ViewState("ContactId")
                .RatingCompany = Session("CompanyId")
                .RatingContact = Session("UserId") 'The logged in user is rating.
                .Rating = rating
                .Comments = txtMessage2.Text.Trim
                .LinkID = 2
                .LinkSource = "ReferenceCheck"
            End With
            dsContacts.tblContactsRating.Rows.Add(nrowRating2)

            'for rating3
            If rdoRatingPositive3.Checked = True Then
                rating = 1
                totalrating += 1
            ElseIf rdoRatingNeutral3.Checked = True Then
                rating = 0
            ElseIf rdoRatingNegative3.Checked = True Then
                rating = -1
            End If
            Dim nrowRating3 As XSDContacts.tblContactsRatingRow = dsContacts.tblContactsRating.NewRow
            With nrowRating3
                .RatedCompany = ViewState("CompanyId")
                .RatedContact = ViewState("ContactId")
                .RatingCompany = Session("CompanyId")
                .RatingContact = Session("UserId")
                .Rating = rating
                .Comments = txtMessage3.Text.Trim
                .LinkID = 3
                .LinkSource = "ReferenceCheck"
            End With
            dsContacts.tblContactsRating.Rows.Add(nrowRating3)


            'For updating the ContactsSettings table trackSP field
            Dim nrowCS As XSDContacts.ContactsSettingsRow = dsContacts.ContactsSettings.NewRow
            With nrowCS
                .ContactID = contactid
                '.TrackSP = chkTrackSP.Checked
            End With
            dsContacts.ContactsSettings.Rows.Add(nrowCS)

            Session("dsContacts") = dsContacts.GetXml
            If Session("action") <> "Approve" Then
                pnlSubmitForm.Visible = False
                pnlConfirm.Visible = True
            End If
        Else
            divValidationMain.Visible = True
            ViewState("WarningCount") = 0

        End If





    End Sub

    ''' <summary>
    ''' To handle confirm action
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Dim flagGoToApprovedList As Boolean = False
        Dim IsApprove As Boolean = False
        If Not IsNothing(Session("action")) Then
            If Session("action") = "Approve" Then

                IsApprove = True


            End If
        End If

        Dim xmlContent As String = Session("dsContacts")
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)
        'Dim Success As Boolean = ws.WSContact.AddUpdateReferenceCheck(xmlContent, ViewState("CompanyId"), ViewState("CompanyId"), ViewState("ContactId"), Session("CompanyId"), Session("UserId"))

        Dim Success As Integer
        Dim RandomPassword As String
        RandomPassword = ""

        If (ViewState("SupplierCreatedCompanyID") = 1173) Then
            'poonam modified on 14/8/2015 - Task - OM-13:OW - ENH - Add Create Supplier account Page in OW
            'poonam modified on 4/12/2015 - Task - OA-131 :OA/OM - Strengthen automatically and manually generated passwords
            RandomPassword = CommonFunctions.GenerateOTP()

            Success = ws.WSContact.AddUpdateReferenceCheck(xmlContent, ViewState("CompanyId"), ViewState("CompanyId"), ViewState("ContactId"), Session("CompanyId"), Session("UserId"), ApplicationSettings.StatusApprovedCompany, Request("BizDivId"), IsApprove, Session("UserID"), Encryption.EncryptToMD5Hash(RandomPassword))

        Else
            Success = ws.WSContact.AddUpdateReferenceCheck(xmlContent, ViewState("CompanyId"), ViewState("CompanyId"), ViewState("ContactId"), Session("CompanyId"), Session("UserId"), ApplicationSettings.StatusApprovedCompany, Request("BizDivId"), IsApprove, Session("UserID"), "")
        End If
       

        If Success = 1 Then
            If Not IsNothing(Session("action")) Then

                If Session("action") = "Approve" Then

                    If chkSendMail.Checked = True Then
                        ' Emails.SendCompanyStatusMail(Request("Email"), Request("Name"), Server.HtmlDecode(txtMailMessage.Text), "Approve")
                        'poonam modified on 14/8/2015 - Task - OM-13:OW - ENH - Add Create Supplier account Page in OW
                        'Emails.SendCompanyApproveMail(Convert.ToString(Request("Email")), Convert.ToString(ViewState("UserFirstName")), Convert.ToString(RandomPassword))
                        Emails.SendsupplierApproveMail(Convert.ToString(Request("Email")), Convert.ToString(ViewState("UserFirstName")), Convert.ToString(RandomPassword))
                    End If
                    flagGoToApprovedList = True
                End If

            End If


            Cache.Remove("Reference" & "-" & CStr(ViewState("CompanyId")) & "-" & Session.SessionID)

            If flagGoToApprovedList = True Then

                Dim redirectStr As String = ""
                Select Case Request("sender")
                    Case "AdminContactsListing"
                        redirectStr = "AMContacts.aspx?CompanyID=" & ViewState("CompanyId") & "&UserId=" & ViewState("ContactId") & "&BizDivId=" & Request("BizDivId") & "&contactType=" & Request("contactType") & "&PS=" & Request("PS") & "&PN=" & Request("PN") & "&SC=" & Request("SC") & "&SO=" & Request("SO") & "&FromDate=" & Request("FromDate") & "&ToDate=" & Request("ToDate")
                    Case "AdminSearchAccounts"
                        redirectStr = "SearchAccounts.aspx?sender=AdminSearchAccounts&companyId=" & ViewState("CompanyId") & "&contactId=" & ViewState("ContactId") & "&bizDivId=" & Request("BizDivId") & "&classId=" & Request("classId") & "&vendorIDs=" & Request("vendorIDs") & "&PS=" & Request("PS") & "&PN=" & Request("PN") & "&SC=" & Request("SC") & "&SO=" & Request("SO") & "&roleGroupId=" & Request("roleGroupId") & "&statusId=" & Request("statusId") & "&product=" & Request("product") & "&region=" & Request("region") & "&txtPostCode=" & Request("txtPostCode") & "&txtKeyword=" & Request("txtKeyword") & "&txtFName=" & Request("txtFName") & "&txtLName=" & Request("txtLName") & "&txtCompanyName=" & Request("txtCompanyName") & "&txtEmail=" & Request("txtEmail") & "&refcheck=" & Request("refcheck")
                End Select

                Response.Redirect(redirectStr)
            End If

            pnlSubmitForm.Visible = True
            populatedata()
            pnlConfirm.Visible = False
        ElseIf Success = 2 Then
            pnlSubmitForm.Visible = True
            pnlConfirm.Visible = False
            lblErrorMsg.Text = "Operation failed. Please make sure Bank details have been specified for this account and try again"
            divValidationMain.Visible = True
        Else
            pnlSubmitForm.Visible = True
            pnlConfirm.Visible = False
            lblErrorMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
            divValidationMain.Visible = True

        End If

    End Sub

    ''' <summary>
    ''' To handle cancel action on confirm
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlSubmitForm.Visible = True
        pnlConfirm.Visible = False
        ViewState("WarningCount") = 0
    End Sub

    Public Sub populateLabels(ByVal dv As DataView, ByVal lbl As Label, ByVal AttribLabel As String)
        dv.RowFilter = "AttributeLabel = '" & AttribLabel & "'"
        If dv.Count <> 0 Then
            lbl.Text = dv.Item(0).Item("AttributeValue")
        Else
            lbl.Text = ""
        End If

    End Sub

    Public Sub populateText(ByVal dv As DataView, ByVal txt As TextBox, ByVal AttribLabel As String)
        dv.RowFilter = "AttributeLabel = '" & AttribLabel & "'"
        If dv.Count <> 0 Then
            txt.Text = dv.Item(0).Item("AttributeValue")
        Else
            txt.Text = ""
        End If
    End Sub

    Public Function GetRefInfo(Optional ByVal KillCache As Boolean = False) As DataSet
        Dim CacheKey As String = "ReferenceCheck" & ViewState("CompanyId") & Session.SessionID

        If KillCache = True Then
            Page.Cache.Remove(CacheKey)
        End If
        Dim ds As DataSet
        If Cache(CacheKey) Is Nothing Then
            ds = ws.WSContact.GetRefCheckData(ViewState("CompanyId"), ViewState("ContactId"), Request("BizDivId"))
        Else
            ds = CType(Page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function

    Private Sub btnApprovedSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovedSupp.Click
        Page.Validate()
        If Page.IsValid Then
            Session("action") = "Approve"
            saveData()
            checkForApproveSupp()
        Else
            divValidationMain.Visible = True
            ViewState("WarningCount") = 0

        End If
    End Sub

    Private Sub btnApprovedSuppTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovedSuppTop.Click
        Page.Validate()
        If Page.IsValid Then
            Session("action") = "Approve"
            saveData()
            checkForApproveSupp()
        Else
            divValidationMain.Visible = True
            ViewState("WarningCount") = 0

        End If
    End Sub

    Public Sub checkForApproveSupp()
        'check if at least 2 ratings are positive, else cannot approve - neetu
        If totalrating > 1 Then

            'buliding string to display in the confirm action page, checks if insurance is selected and attachment not there - then display warning message 

            ' Dim WarningCount As Integer = 0
            If IsNothing(ViewState("WarningCount")) Then
                ViewState("WarningCount") = 0
            End If



            Dim InsuranceCoverage As String = ""
            If lblEmpLiability.Text = "Yes" And anchorEmployee.InnerHtml = "" Then
                InsuranceCoverage = "Employee Liability"
            End If
            If lblPublicLiability.Text = "Yes" And anchorPublic.InnerHtml = "" Then
                If InsuranceCoverage = "" Then
                    InsuranceCoverage = "Public Liability"
                Else
                    InsuranceCoverage &= " and Public Liability"
                End If
            End If
            If lblProfIndemnity.Text = "Yes" And anchorProfIndemnity.InnerHtml = "" Then
                If InsuranceCoverage = "" Then
                    InsuranceCoverage = "Professional Indemnity"
                Else
                    InsuranceCoverage &= " and Professional Indemnity"
                End If
            End If

            Dim WarningMessage As String = ""

            If InsuranceCoverage <> "" Then
                If InsuranceCoverage.IndexOf("and", 0) = -1 Then
                    WarningMessage = "Are you aware that the insurance " & InsuranceCoverage & " does not have an attachment. <BR> Do you still want to approve this supplier?"
                Else
                    WarningMessage = "Are you aware that the insurance " & InsuranceCoverage & " do not have attachments. <BR> Do you still want to approve this supplier?"
                End If

                '  lblErrorMsg.Text = WarningMessage
            End If


            If WarningMessage = "" Then
                pnlSubmitForm.Visible = False
                pnlConfirm.Visible = True
            Else
                If ViewState("WarningCount") = 0 Then
                    divValidationMain.Visible = True
                    lblErrorMsg.Text = "<ul><li>" + WarningMessage + "</li></ul>"
                    ViewState("WarningCount") = 1
                Else
                    pnlSubmitForm.Visible = False
                    pnlConfirm.Visible = True
                End If
            End If
        Else
            divValidationMain.Visible = True
            ViewState("WarningCount") = 0
            lblErrorMsg.Text = "<ul><li>A minimum of 2 Positive Ratings are required to Approve a Supplier</li></ul>"

        End If
    End Sub

    Public Sub checkFutureDate1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles checkFutureDate1.ServerValidate
        If Page.IsValid Then
            Dim EnteredDate = New DateTime
            Dim Todaysdate = New DateTime

            If txtEmployeeLiabilityDate.Text <> "" And txtEmployeeLiabilityDate.Text <> "Enter expiry date" Then

                EnteredDate = Strings.FormatDateTime(txtEmployeeLiabilityDate.Text, DateFormat.ShortDate)
                Todaysdate = Strings.FormatDateTime(DateTime.Now, DateFormat.ShortDate)

                If Date.Compare(EnteredDate, Todaysdate) < 0 Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            End If
        End If
    End Sub

    Public Sub checkFutureDate2_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles checkFutureDate2.ServerValidate

        If Page.IsValid Then
            Dim EnteredDate = New DateTime
            Dim Todaysdate = New DateTime

            If txtPublicLiabilityDate.Text = "" And txtPublicLiabilityDate.Text = "Enter expiry date" Then

                EnteredDate = Strings.FormatDateTime(txtPublicLiabilityDate.Text, DateFormat.ShortDate)
                Todaysdate = Strings.FormatDateTime(DateTime.Now, DateFormat.ShortDate)


                If Date.Compare(EnteredDate, Todaysdate) < 0 Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            End If
        End If

    End Sub

    Public Sub checkFutureDate3_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles checkFutureDate3.ServerValidate
        If Page.IsValid Then
            Dim EnteredDate = New DateTime
            Dim Todaysdate = New DateTime


            If txtProfessionalIndemnityDate.Text = "" And txtProfessionalIndemnityDate.Text = "Enter expiry date" Then

                EnteredDate = Strings.FormatDateTime(txtProfessionalIndemnityDate.Text, DateFormat.ShortDate)
                Todaysdate = Strings.FormatDateTime(DateTime.Now, DateFormat.ShortDate)
                ' DateTime.Now

                If Date.Compare(EnteredDate, Todaysdate) < 0 Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            End If
        End If

    End Sub

    Public Sub rqCoverAmount1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rqCoverAmount1.ServerValidate

        If IsNumeric(txtCoverAmountEmpLiability.Text.Trim) Then
            If txtCoverAmountEmpLiability.Text.IndexOf("-") < 0 Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Else
            args.IsValid = False
        End If

    End Sub

    Public Sub rqCoverAmount2_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rqCoverAmount2.ServerValidate
        If IsNumeric(txtCoverAmountPubLiability.Text.Trim) Then
            If txtCoverAmountPubLiability.Text.IndexOf("-") < 0 Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Else
            args.IsValid = False
        End If
    End Sub

    Public Sub rqCoverAmount3_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rqCoverAmount3.ServerValidate
        If IsNumeric(txtCoverAmountProfIndemnity.Text.Trim) Then
            If txtCoverAmountProfIndemnity.Text.IndexOf("-") < 0 Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Else
            args.IsValid = False
        End If
    End Sub


    Public Sub CustValWOBeginDate1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustValWOBeginDate1.ServerValidate
        Try
            If txtEmployeeLiabilityDate.Text <> "" Then

                If txtEmployeeLiabilityDate.Text <> ResourceMessageText.GetString("EnterExpiryDate") Then

                    'prepare regular expression
                    Dim str As String = "(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                    'match input value against regular expression 
                    Dim myMatch As Match = System.Text.RegularExpressions.Regex.Match(txtEmployeeLiabilityDate.Text.Trim, str)
                    If Not myMatch.Success Then
                        'if format is not correct
                        args.IsValid = False
                    Else
                        'if format is  correct
                        '  chkInsurance4.Checked = True
                        args.IsValid = True
                    End If

                End If
            End If
        Catch ex As Exception
        End Try
    End Sub



    Public Sub CustValWOBeginDate2_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustValWOBeginDate2.ServerValidate
        Try
            If txtPublicLiabilityDate.Text <> "" Then
                If txtPublicLiabilityDate.Text <> ResourceMessageText.GetString("EnterExpiryDate") Then
                    'prepare regular expression
                    Dim str As String = "(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                    'match input value against regular expression 
                    Dim myMatch As Match = System.Text.RegularExpressions.Regex.Match(txtPublicLiabilityDate.Text.Trim, str)
                    If Not myMatch.Success Then
                        'if format is not correct
                        args.IsValid = False
                    Else
                        'if format is  correct

                        args.IsValid = True
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub CustValWOBeginDate3_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustValWOBeginDate3.ServerValidate
        Try
            If txtProfessionalIndemnityDate.Text <> "" Then
                If txtProfessionalIndemnityDate.Text <> ResourceMessageText.GetString("EnterExpiryDate") Then
                    'prepare regular expression
                    Dim str As String = "(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                    'match input value against regular expression 
                    Dim myMatch As Match = System.Text.RegularExpressions.Regex.Match(txtProfessionalIndemnityDate.Text.Trim, str)
                    If Not myMatch.Success Then
                        'if format is not correct
                        args.IsValid = False
                    Else
                        'if format is  correct

                        args.IsValid = True
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub



End Class