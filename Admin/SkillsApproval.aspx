﻿<%@ Page Title="OrderWork : Accreditation Tags" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="SkillsApproval.aspx.vb" Inherits="Admin.SkillsApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <script language="javascript" type="text/javascript" src="JS/jquery.js"></script>
    <script type = "text/javascript">
        function SetContextKey() {
            $find('<%=AutoCompleteExtender1.ClientID%>').set_contextKey($("#ddlTagFor option:selected").text() + "AccreditationsSkillsApproval" + $get("<%=chkIsLocked.ClientID %>").checked);
            $find('<%=AutoCompleteExtender2.ClientID%>').set_contextKey($("#ddlTagFor option:selected").text() + "CompanyEngineer");
        }
    </script>
    <style type="text/css">
   /*AutoComplete flyout */
    .completionList 
    {
        border:solid 1px #444444;
        margin:0px;
        padding:2px;
        background-color: #FFFFFF;
        width:200px !important;
        font-size:10pt;
        overflow:auto;
        list-style-type:none
    }

    .listItem {
    color: #1C1C1C;
    width:200px !important;
    }

    .itemHighlighted {
    background-color: #ffc0c0;
    width:200px !important;
    }
       </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="divContent">
        <div class="roundtopWhite">
            <img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner"
                style="display: none" /></div>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
            <tr>
                <td valign="top">
                    <!-- InstanceBeginEditable name="EdtContent" -->
                    <input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />
                    <input id="hdnPageNo" value="1" type="hidden" name="hdnPageNo" runat="server" />
                    <input id="hdnPageSize" value="25" type="hidden" name="hdnPageSize" runat="server" />
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10px">
                                    </td>
                                    <td>
                                        <asp:Label ID="lblMessage" CssClass="HeadingRed" runat="server" Text="Accreditation Tags"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table style="margin-left: 10px;">
                                <tr>
                                    <td colspan="3">
                                        <div id="div2" style="width: 100%; margin-top: 20px;">
                                            <asp:ValidationSummary ID="validationSummarySearch" runat="server" DisplayMode="List"
                                                ValidationGroup="ValidationTop" CssClass="bodytxtValidationMsg"></asp:ValidationSummary>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="divForAdvanceSearch" runat="server" visible="true">
                                            <div id="divrow2" style="width: 100%; margin-top: 20px;">
                                                <div class="formTxt" style="width: 100px; float: left; margin-right: 10px;">
                                                    Tag For
                                                    <asp:DropDownList ID="ddlTagFor" ClientIDMode="Static" TabIndex="1" runat="server" CssClass="formField105">
                                                        <asp:ListItem Text="All" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="Company" Value="Company"></asp:ListItem>
                                                        <asp:ListItem Text="Engineer" Value="Engineer"></asp:ListItem>
                                                        <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="formTxt" style="width: 110px; float: left; text-align: left; margin-right: 10px;">
                                                    Tag Name
                                                    <asp:TextBox ID="txtTagName" TabIndex="2" runat="server" CssClass="formField105" onkeyup="SetContextKey()"></asp:TextBox>
                                                    <cc1:AutoCompleteExtender ServiceMethod="GetTagName" CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted" CompletionListItemCssClass="listItem"
                                                        MinimumPrefixLength="3"
                                                        CompletionInterval="100" EnableCaching="false" CompletionSetCount="10"
                                                        TargetControlID="txtTagName" UseContextKey = "true"
                                                        ID="AutoCompleteExtender1" runat="server" FirstRowSelected = "false">
                                                    </cc1:AutoCompleteExtender>
                                                </div>
                                                <div class="formTxt" style="width: 120px; float: left; text-align: left; margin-right: 10px;">
                                                    Company/Engineer Name
                                                    <asp:TextBox ID="txtName" TabIndex="2" runat="server" CssClass="formField105" onkeyup="SetContextKey()"></asp:TextBox>
                                                    <cc1:AutoCompleteExtender ServiceMethod="GetName" CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted" CompletionListItemCssClass="listItem"
                                                        MinimumPrefixLength="3"
                                                        CompletionInterval="100" EnableCaching="false" CompletionSetCount="10"
                                                        TargetControlID="txtName" UseContextKey = "true"
                                                        ID="AutoCompleteExtender2" runat="server" FirstRowSelected = "false">
                                                    </cc1:AutoCompleteExtender>
                                                </div>
                                                <div class="formTxt" style="width: 150px; float: left;">
                                                    Created From Date
                                                    <asp:TextBox ID="txtDateCreatedFrom" TabIndex="4" runat="server" CssClass="formField105"></asp:TextBox>
                                                    <img alt="Click to Select" src="Images/calendar.gif" id="btnDateCreatedFrom" style="cursor: pointer;
                                                        vertical-align: top;" />
                                                    <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnDateCreatedFrom"
                                                        TargetControlID="txtDateCreatedFrom" ID="calDateCreatedFrom" runat="server">
                                                    </cc1:CalendarExtender>
                                                    <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regDateCreatedFrom"
                                                        runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateCreatedFrom"
                                                        ErrorMessage="Created From Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="rngDateCreated" CssClass="txtOrange" runat="server" ErrorMessage="Created To Date should be greater than Created From Date"
                                                        ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateCreatedTo">*</asp:CustomValidator>
                                                </div>
                                                <div class="formTxt" style="width: 150px; float: left;">
                                                    Created To Date
                                                    <asp:TextBox ID="txtDateCreatedTo" TabIndex="4" runat="server" CssClass="formField105"></asp:TextBox>
                                                    <img alt="Click to Select" src="Images/calendar.gif" id="btnDateCreatedTo" style="cursor: pointer;
                                                        vertical-align: top;" />
                                                    <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnDateCreatedTo"
                                                        TargetControlID="txtDateCreatedTo" ID="calDateCreatedTo" runat="server">
                                                    </cc1:CalendarExtender>
                                                    <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regDateCreatedTo"
                                                        runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtDateCreatedTo"
                                                        ErrorMessage="Created To Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
                                                </div>
                                                <div class="formTxt" style="width: 70px; float: left; margin-top: 15px;">
                                                    <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" Checked="true" />
                                                </div>
                                                <div class="formTxt" style="width: 70px; float: left; margin-top: 15px;">
                                                    <asp:CheckBox ID="chkIsLocked" runat="server" Text="IsLocked" />
                                                </div>
                                                <div class="formTxt" style="width: 250px; float: left;">
                                                    Approval Status
                                                    <asp:RadioButtonList ID="rblApprovalStatusHeader" runat="server" RepeatDirection="Horizontal"
                                                    CssClass="radioButtonList" DataTextField="StandardValue" DataValueField="StandardID" />
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td align="left" width="60px" valign="bottom">
                                        <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                            color: #FFFFFF; text-align: center; width: 60px;">
                                            <input type="reset" name="Reset" title="Reset" class="txtButtonRed" style="border:0px;padding:0px;" />
                                        </div>
                                    </td>
                                    <td align="left" width="60px" valign="bottom">
                                        <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                            color: #FFFFFF; text-align: center; width: 60px;">
                                            <asp:LinkButton ID="btnView" CausesValidation="False" runat="server" CssClass="txtButtonRed"
                                                Text="View"></asp:LinkButton>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td class="paddingL30 padB21" valign="top" padb21 paddingtop1>
                                        <asp:Label ID="lblMsg" runat="server" CssClass="bodytxtValidationMsg"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <hr style="background-color: #993366; height: 5px; margin: 0px;" />
                            <asp:Panel ID="pnlListing" runat="server">

                                <div style="float:right;">
                                    <asp:Button ID="btnUpdateTop" runat="server" Text="Update" CausesValidation="false" CommandArgument='<%#Eval("TagContactLinkageID") %>' />
                                </div>
                                <asp:GridView ID="gvAccreditationTags" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'
                                    BorderColor="White" DataKeyNames="TagId" PagerSettings-Mode="NextPreviousFirstLast"
                                    PagerSettings-Position="TopAndBottom" Width="100%" DataSourceID="ObjectDataSource1"
                                    AllowSorting="true" OnRowDataBound="gvAccreditationTags_RowDataBound">
                                    <EmptyDataTemplate>
                                        <table width="100%" border="0" cellpadding="10" cellspacing="0">
                                            <tr>
                                                <td align="center" style="text-align: center" valign="middle" height="300" class="txtWelcome">
                                                    Sorry! There are no records in this category.
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <PagerTemplate>
                                        <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9"
                                            id="tblMainTop" style="visibility: visible" runat="server">
                                            <tr>
                                                <td width="70" align="left" class="txtWelcome paddingL10">

                                                </td>
                                                <td>
                                                    <table width="100%" id="tblTop" runat="server" border="0" cellpadding="0" cellspacing="0"
                                                        bgcolor="#DAD8D9">
                                                        <tr>
                                                            <td align="right">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="right" valign="middle">
                                                                            <div id="tdTotalCount" style="width: 200px; padding-right: 20px; text-align: right"
                                                                                runat="server" class="formLabelGrey">
                                                                            </div>
                                                                        </td>
                                                                        <td align="right" width="136" valign="middle" id="tdPagerUp" runat="server">
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="right" width="36">
                                                                                        <div id="divDoubleBackUp" runat="server" style="float: left; vertical-align: bottom;
                                                                                            padding-top: 2px;">
                                                                                            <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif"
                                                                                                CommandArgument="First" runat="server" />
                                                                                        </div>
                                                                                        <div id="divBackUp" runat="server" style="float: right; vertical-align: bottom; padding-top: 2px;">
                                                                                            <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif"
                                                                                                CommandArgument="Prev" runat="server" />
                                                                                        </div>
                                                                                    </td>
                                                                                    <td width="50" align="center" style="margin-right: 3px;">
                                                                                        <asp:DropDownList Style="margin-left: 4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged"
                                                                                            CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td width="30" valign="bottom">
                                                                                        <div id="divNextUp" runat="server" style="float: left; vertical-align: bottom; padding-top: 2px;">
                                                                                            <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif"
                                                                                                CommandArgument="Next" runat="server" />
                                                                                        </div>
                                                                                        <div id="divDoubleNextUp" runat="server" style="float: right; vertical-align: bottom;
                                                                                            padding-top: 2px;">
                                                                                            <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif"
                                                                                                CommandArgument="Last" runat="server" />
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tag Name" HeaderStyle-CssClass="gridHdr gridText"
                                            SortExpression="TagName">
                                            <HeaderStyle CssClass="gridHdr gridText" />
                                            <ItemStyle Wrap="true" HorizontalAlign="Left" CssClass="gridRow" Width="20%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTagName" runat="server" Text='<%# databinder.eval(container.dataitem, "TagName") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Company/Engineer Name" HeaderStyle-CssClass="gridHdr gridText"
                                            SortExpression="Name">
                                            <HeaderStyle CssClass="gridHdr gridText" />
                                            <ItemStyle Wrap="true" HorizontalAlign="Left" CssClass="gridRow" Width="20%" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hypSpecialistCompany" runat="server" Text='<%# databinder.eval(container.dataitem, "Name") %>' NavigateUrl='<%# FormatLink(Eval("ContactId"),Eval("MainContactId"),Eval("IsCompany")) %>' /> 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="20%"  HeaderStyle-CssClass="gridHdr gridText"  DataField="TagName" SortExpression="TagName" HeaderText="Tag Name" />            --%>
                                        <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"
                                            DataField="TagFor" SortExpression="TagFor" HeaderText="Tag For" />
                                        <asp:TemplateField HeaderText="IsActive" HeaderStyle-CssClass="gridHdr gridText"
                                            SortExpression="IsActive">
                                            <HeaderStyle CssClass="gridHdr gridText" />
                                            <ItemStyle Wrap="true" HorizontalAlign="Left" CssClass="gridRow" Width="5%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblIsProcessed" runat="server" Text='<%# IIF(Container.DataItem("IsActive"), True,False)%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="IsLocked" HeaderStyle-CssClass="gridHdr gridText"
                                            SortExpression="IsLocked">
                                            <HeaderStyle CssClass="gridHdr gridText" />
                                            <ItemStyle Wrap="true" HorizontalAlign="Left" CssClass="gridRow" Width="5%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblIsLocked" runat="server" Text='<%# IIF(Container.DataItem("IsLocked"), True,False)%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr gridText"
                                            DataField="DateCreated" HeaderText="Date Created" SortExpression="DateCreated" />
                                        <asp:TemplateField ItemStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr gridText"
                                            HeaderText="Approval Status" SortExpression="ApprovalStatus">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hidTagContactLinkageId" runat="server" Value='<%# Eval("TagContactLinkageID") %>' />
                                                <asp:RadioButtonList ID="rblApprovalStatus" runat="server" RepeatDirection="Horizontal"
                                                    CssClass="radioButtonList" DataTextField="StandardValue" DataValueField="StandardID" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="gridRow" />
                                    <RowStyle CssClass="gridRow" />
                                    <PagerStyle BorderWidth="0px" BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top" Height="40px" />
                                    <FooterStyle BackColor="#D7D7D7" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                                <div style="float:right;">
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CausesValidation="false" CommandArgument='<%#Eval("TagContactLinkageID") %>' />
                                </div>
                            </asp:Panel>
                            <triggers>

                            <asp:PostBackTrigger   ControlID="btnView"   />                       

                         </triggers>
                            <cc1:ModalPopupExtender ID="mdlTagInfo" runat="server" TargetControlID="btnTemp"
                                PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground"
                                Drag="False" DropShadow="False">
                            </cc1:ModalPopupExtender>
                            <asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" Style="visibility: hidden;" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <div class="gridText">
                                <img align="middle" src="Images/indicator.gif" />
                                <b>Fetching Data... Please Wait</b>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1"
                        CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="SelectFromDB" EnablePaging="True" SelectCountMethod="SelectCount"
                        TypeName="Admin.SkillsApproval" SortParameterName="sortExpression">
                    </asp:ObjectDataSource>
                    <!-- InstanceEndEditable -->
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
