Public Partial Class LocationForm
    Inherits System.Web.UI.Page

    Protected WithEvents UCLocationForm1 As UCLocationForm
    Protected WithEvents UCAccountSumm1 As UCAccountSummary
    Protected WithEvents lnkViewFavSuppliers As System.Web.UI.HtmlControls.HtmlAnchor

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        UCLocationForm1.CompanyID = Request("CompanyID")
        If Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
            UCAccountSumm1.ShowRating = True
            lnkViewBlackListClient.Visible = True
            lnkViewWorkingDays.Visible = True
        ElseIf Session("ContactClassID") = ApplicationSettings.RoleClientID Then
            UCAccountSumm1.ShowRating = False
        End If
        UCAccountSumm1.BizDivId = Request("bizDivId")

        Dim contactType As Integer
        If (Session(Request("companyid") & "_ContactClassID")) Then
            contactType = Session(Request("companyid") & "_ContactClassID")
        End If

        If Not IsNothing(Session(Request("companyid") & "_ContactClassID")) Then
            If contactType = ApplicationSettings.ContactType.newSupplier Or contactType = ApplicationSettings.ContactType.approvedSupplier Or contactType = ApplicationSettings.ContactType.suspendedSupplier Then
                lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId")
                rgnReferences.Visible = True

                'Prepare AutoMatch Link URL
                lnkAutoMatch.HRef = "AutoMatchSettings.aspx?CompanyID=" & Request("CompanyID") & "&sender=supplier" & "&bizDivId=" & Request("bizDivId")
                lnkAccSettings.HRef = "AccountSettings.aspx?CompanyID=" & Request("CompanyID") & "&sender=supplier" & "&bizDivId=" & Request("bizDivId")
                'lnkComments.HRef = "Comments.aspx?CompanyID=" & Session("ContactCompanyID")
                lnkViewFavSuppliers.HRef = "FavouriteSupplierList.aspx?mode=favourite&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
                lnkAddProduct.HRef = "AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
                lnkAddProductNew.HRef = "ServiceListing.aspx?CompanyID=" & Request("CompanyID")
                'lnkViewBlackListsupplier.HRef = "FavouriteSupplierList.aspx?mode=blacklist&CompanyID=" & Session("ContactCompanyID")
                lnkReferences.HRef = "References.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
                lnkuser.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
                lnkloc.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
                lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
                lnkAddUsers.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
                lnkViewBlackListClient.HRef = "FavouriteSupplierList.aspx?mode=ClientBlacklist&CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId")
                lnkViewWorkingDays.HRef = "WorkingDays.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId")
                lnkViewAllInvoices.HRef = "ViewAllInvoices.aspx?CompanyID=" & Request("companyid") & "&sender=supplier" & "&bizDivId=" & Request("bizDivId")
                lnkViewAccountHistory.HRef = "AccountHistory.aspx?CompanyID=" & Request("companyid") & "&sender=supplier" & "&bizDivId=" & Request("bizDivId")
            ElseIf contactType = ApplicationSettings.ContactType.approvedBuyer Or contactType = ApplicationSettings.ContactType.suspendedBuyer Then
                'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                lnkCompProfile.HRef = "CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                rgnReferences.Visible = False

                'Prepare AutoMatch Link URL
                lnkAutoMatch.HRef = "AutoMatchSettings.aspx?CompanyID=" & Request("CompanyID") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                lnkAccSettings.HRef = "AccountSettings.aspx?CompanyID=" & Request("CompanyID") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                'lnkComments.HRef = "Comments.aspx?CompanyID=" & Session("ContactCompanyID")
                lnkViewFavSuppliers.HRef = "FavouriteSupplierList.aspx?mode=favourite&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                lnkAddProduct.HRef = "AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                'lnkViewBlackListsupplier.HRef = "FavouriteSupplierList.aspx?mode=blacklist&CompanyID=" & Session("ContactCompanyID")
                lnkReferences.HRef = "References.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                lnkuser.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                lnkloc.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                ' lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
                lnkAddUsers.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                lnkViewBlackListClient.HRef = "FavouriteSupplierList.aspx?mode=ClientBlacklist&CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                lnkViewWorkingDays.HRef = "WorkingDays.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                lnkViewAllInvoices.HRef = "ViewAllInvoices.aspx?CompanyID=" & Request("companyid") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                lnkViewAccountHistory.HRef = "AccountHistory.aspx?CompanyID=" & Request("companyid") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid")
            End If
        End If

        ' ''lnkComments.HRef = "Comments.aspx?CompanyID=" & Session("ContactCompanyID")
        ''lnkViewFavSuppliers.HRef = "FavouriteSupplierList.aspx?mode=favourite&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        ''lnkAddProduct.HRef = "AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        ' ''lnkViewBlackListsupplier.HRef = "FavouriteSupplierList.aspx?mode=blacklist&CompanyID=" & Session("ContactCompanyID")
        ''lnkReferences.HRef = "References.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        ''lnkuser.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        ''lnkloc.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        ''lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")


        'Code commeneted as Suppliers can also behave as buyers
        ''Show Favourite supplier link
        'lnkViewFavSuppliers.Visible = False
        'If Session("ContactClassID") = ApplicationSettings.RoleClientID Then
        '    lnkViewFavSuppliers.Visible = True
        'End If

    End Sub

End Class