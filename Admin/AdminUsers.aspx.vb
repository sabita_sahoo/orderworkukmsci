
Partial Public Class AdminUsers
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        UserAction.userAutoLoginAccept(Page)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            gvSiteUsers.Sort("LastLogin", SortDirection.Descending)
            gvSiteUsers.PageSize = ApplicationSettings.GridViewPageSize
            HttpContext.Current.Session("HideDeleted") = chkHideDeleted.Checked
        End If
    End Sub

    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvSiteUsers.DataBind()

        End If
    End Sub

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim FromDate As String = HttpContext.Current.Items("FromDate")
        Dim ToDate As String = HttpContext.Current.Items("ToDate")
        Dim BizDivID As Integer = HttpContext.Current.Items("BizDivID")
        Dim HideDeletedAccounts As Integer
        If Not HttpContext.Current.Session("HideDeleted") Is Nothing Then
            If HttpContext.Current.Session("HideDeleted") = "True" Then
                HideDeletedAccounts = 1
            Else
                HideDeletedAccounts = 0
            End If
        End If

        Dim ds As DataSet = ws.WSContact.GetAdminSiteUsers(sortExpression, startRowIndex, maximumRows, HideDeletedAccounts)
        HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
        Return ds
    End Function

    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvSiteUsers.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

    Private Sub gvSiteUsers_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSiteUsers.PreRender
        Me.gvSiteUsers.Controls(0).Controls(Me.gvSiteUsers.Controls(0).Controls.Count - 1).Visible = True
        If ViewState("rowCount") < 1 Then
            Return
        End If
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

            End If
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvSiteUsers.RowDataBound
        MakeGridViewHeaderClickable(gvSiteUsers, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvSiteUsers.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvSiteUsers, e.Row, Me)
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)
        Dim btnExport As HtmlAnchor = CType(gvPagerRow.FindControl("btnExport"), HtmlAnchor)

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    Private Sub ObjectDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles ObjectDataSource1.Selecting
        InitializeGridParameters()
    End Sub

    Private Sub InitializeGridParameters()
        HttpContext.Current.Items("BizDivID") = ViewState("BizDivID")
        HttpContext.Current.Items("HideDeleted") = chkHideDeleted.Checked
    End Sub

    Public Function chkForNull(ByVal LastLogin As Object) As Object
        Dim lastLoginDate As Object = LastLogin
        If Not IsDBNull(lastLoginDate) Then
            Return Strings.FormatDateTime(lastLoginDate, DateFormat.ShortDate)
        Else
            Return "Not Yet Logged in "
        End If
    End Function

    Public Function GetLinks(ByVal UserName As Object, ByVal FirstName As Object, ByVal LastName As Object, ByVal RoleGroupName As Object, ByVal ContactID As Object, ByVal enableLogin As Boolean, ByVal MainCat As String, ByVal RoleGroupID As Object, ByVal MenuID As Object) As String
        Dim link As String = ""
        Dim linkParams As String = ""
        linkParams &= "UserName=" & UserName
        'Poonam modified on 22-03-2017 - Task - OA-427 - OA - Unsupported spaces in Admin users
        linkParams &= "&FirstName=" & FirstName.Replace("'", "dquotes").ToString
        linkParams &= "&LastName=" & LastName.Replace("'", "dquotes").ToString
        linkParams &= "&RoleGroupName=" & RoleGroupName
        linkParams &= "&ContactID=" & ContactID
        linkParams &= "&EnableLogin=" & enableLogin
        linkParams &= "&mode=Edit"
        linkParams &= "&PS=" & gvSiteUsers.PageSize
        linkParams &= "&PN=" & gvSiteUsers.PageIndex
        linkParams &= "&SC=" & gvSiteUsers.SortExpression
        linkParams &= "&SO=" & gvSiteUsers.SortDirection
        linkParams &= "&MainCat=" & MainCat
        linkParams &= "&sender=" & "AdminSiteUsers"
        linkParams &= "&RoleGroupID=" & RoleGroupID
        linkParams &= "&MenuID=" & MenuID
        link &= "<a href='CreateAdminUser.aspx?" & linkParams & "'><img src='Images/Icons/Edit.gif' alt='Edit' width='12' height='11' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        Return link
    End Function

    Public Sub lnkCreateAdminUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCreateAdminUser.Click
        Response.Redirect("CreateAdminUser.aspx")
    End Sub

    Private Sub chkHideDeleted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkHideDeleted.CheckedChanged
        HttpContext.Current.Session("HideDeleted") = chkHideDeleted.Checked
        gvSiteUsers.PageIndex = 0
        PopulateGrid()
    End Sub
End Class