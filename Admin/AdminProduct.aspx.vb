Public Partial Class AdminProduct
    Inherits System.Web.UI.Page
#Region "Declaration"

    '''<summary>
    '''UCTopMSgLabel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCTopMSgLabel1 As UCTopMSgLabel

    '''<summary>
    '''UCMenu1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCMenu1 As UCMenu

    '''<summary>
    '''UCSubMenu1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCSubMenu1 As UCSubMenu

    '''<summary>
    '''UCWOPlacedSummary1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCWOPlacedSummary1 As UCWOPlacedSummary

    ''' <summary>
    ''' Create Workorder user control
    ''' </summary>
    ''' <remarks></remarks>
    Protected WithEvents UCAccountSumm1 As UCAccountSummary

    ''' <summary>
    ''' Create Workorder user control
    ''' </summary>
    ''' <remarks></remarks>
    Protected WithEvents UCAdminProduct1 As UCAdminProduct


#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ADMIN

        Security.SecurePage(Page)

        If Not (IsNothing(Request("WOID"))) Then
            UCAdminProduct1.WOID = Request("WOID").Trim
        End If

        If (Request("Sender") = "TemplateWOListing") Then
            If Request("TempDoMode") = "ModifyTemplate" Then
                UCAdminProduct1.Src = "ModifyTemplate"
            Else
                UCAdminProduct1.Src = "NewTemplate"
            End If
        End If

        UCAdminProduct1.BizDivID = Session("BizDivId")
        lnkBackToListing.HRef = getBackToListingLink()
    End Sub
    ''' <summary>
    ''' generates Back to listing link for TemplateWOListing page.
    ''' </summary>
    ''' <returns>string</returns>
    ''' <remarks>This function is used for TemplateWOListing page.
    '''   If page is comming from TemplateWOListing page then the back to listing button will be visible.
    ''' </remarks>
    Public Function getBackToListingLink() As String
        'If page is opened in a new page i.e. target=_BLANK then hide back to listing button
        If Not Request("sender") Is Nothing Then
            If Not Request("sender") = "TemplateWOListing" Then
                tdBackToListing.Visible = False
                Return Nothing
            Else
                tdBackToListing.Visible = True
            End If
        Else
            tdBackToListing.Visible = False
            Return Nothing
        End If

        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender").ToLower
                Case "templatewolisting"
                    link = "~\TemplateWOListing.aspx?"
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=TemplateWOForm"
                Case Else
                    Return ""
            End Select
        End If
        Return link
    End Function
End Class