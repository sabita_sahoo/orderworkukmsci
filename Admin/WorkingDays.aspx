﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master"
    CodeBehind="WorkingDays.aspx.vb" Inherits="Admin.WorkingDays" Title="OrderWork : Working Days" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register TagPrefix="uc1" TagName="GridPager" Src="~/UserControls/Admin/GridPager.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScrptManager1" runat="server">
    </asp:ScriptManager>

    

    <style>
        .divHolidayOuter
        {
            -moz-border-radius: 8px 8px 8px 8px;
            -webkit-border-radius: 8px 8px 8px 8px;
            border: 1px solid red;
            height: 150px;
            margin-left: 20px;
            padding-left: 20px;
            margin-right: 20px;
            margin-top: 10px;
            /*background-color: #fff3de;*/
        }
        
        .divHolidayOuterHolidayList
        {
            -moz-border-radius: 8px 8px 8px 8px;
            -webkit-border-radius: 8px 8px 8px 8px;
            border: 1px solid #ffc352;
            width: "100%";
            min-height: 200px;
            margin-left: 20px;
            padding-left: 20px;
            margin-right: 20px;
            margin-top: 10px;
            background-color: #fff3de;
            position: relative;
        }
        .divHolidayInner
        {
            border: 1px solid #846963;
            height: 26px;
            margin-top: 36px;
            background-color: #ce3439;
            margin-right: 1px;
            padding-left: 6px;
            padding-right: 8px;
            float: left;
            color: #FFFFFF;
        }
        .divHolidayInnerSelected
        {
            border: 1px solid #846963;
            height: 26px;
            margin-top: 36px;
            background-color: #c6aead;
            margin-right: 1px;
            padding-left: 6px;
            padding-right: 8px;
            float: left;
            color: #000000;
        }
    </style>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="padding: 0px 0px 16px 0px;
        margin-top: 20px;">
        <tr>
            <td width="115" class="txtWelcome paddingL10">
                <div id="divButton" style="width: 115px;">
                    <div class="bottonTopGrey">
                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText" runat="server" id="lnkBackToListing">
                        <img src="Images/Arrows/Arrow-Back.gif" title="Back to Listing" width="4" height="8"
                            hspace="0" vspace="0" border="0" class="paddingL10R8"><strong>Back to Listing</strong></a>
                    <div class="bottonBottomGrey">
                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                </div>
            </td>
        </tr>
    </table>
    <div style="margin: 0px 5px 0px 5px;" id="divMainTab">
        <div class="paddedBox" id="divProfile">
            <asp:Label ID="lblMsg" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" class="HeadingRed">
                        <strong>Working Days</strong>
                    </td>
                    <td align="left" class="padTop17">
                        
                    </td>
                </tr>
            </table>
            <div id="divMiddleBand" style="border-top: 1px solid #efebef;">
                <div class="divHolidayOuter roundifyRefine8">
                
                <%--<table id="tblSaveTop" runat="server" cellspacing="0" cellpadding="0" width="100%"
                            border="0" style="padding: 0px 0px 16px 0px; margin: 0px;">
                            <tr>
                                <td align="right" valign="top">
                                    <table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                                        <tr>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                            </td>
                                            <td width="110" class="txtBtnImage">
                                                <asp:LinkButton ID="btnSaveTop" CausesValidation="false" CssClass="txtListing" runat="server"
                                                    TabIndex="67"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:LinkButton>
                                            </td>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10">
                                </td>
                                <td width="125" align="right" valign="top" style="padding: 0px 0px 0px 0px;">
                                    <table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                                        <tr>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                            </td>
                                            <td width="115" class="txtBtnImage">
                                                <asp:LinkButton ID="btnResetTop" CausesValidation="false" runat="server" TabIndex="68"
                                                    class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton>
                                            </td>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table> --%>
                    <asp:Repeater ID="rptHolidays" runat="server">
                        <ItemTemplate>
                            <div class='<%#IIF(Container.DataItem("ISSelected")="1", "divHolidayInnerSelected", "divHolidayInner")%>'>
                                <div style="float: left; margin-top: 3px;">
                                    <asp:CheckBox ID="chkHoliday" runat="server" Checked='<%#IIF(Container.DataItem("ISSelected")="1", True, False)%>'
                                        Style="float: left;" />
                                    <div style="float: left; margin-top: 2px;">
                                        <strong>
                                            <%#Container.DataItem("StandardValue")%></strong>
                                        <input type="hidden" value='<%#Container.DataItem("StandardID")%>' runat="server"
                                            id="StandardID" />
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    
<table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td align="left" class="HeadingRed">
                            &nbsp;
                        </td>
                        <td align="left" class="padTop17">
                            <table id="tblSaveBottom" runat="server" cellspacing="0" cellpadding="0" width="100%"
                                border="0" style="padding: 0px 0px 16px 0px; margin: 0px;">
                                <tr>
                                    <td align="right" valign="top">
                                        <table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                                            <tr>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                </td>
                                                <td width="110" class="txtBtnImage">
                                                    <asp:LinkButton ID="btnSaveBottom" CausesValidation="false" OnClientClick="callSetFocusValidation('UCCompanyProfile1')"
                                                        CssClass="txtListing" runat="server" TabIndex="63"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:LinkButton>
                                                </td>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="10">
                                    </td>
                                    <td width="125" align="right" valign="top" style="padding: 0px 0px 0px 0px;">
                                        <table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                                            <tr>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                </td>
                                                <td width="115" class="txtBtnImage">
                                                    <asp:LinkButton ID="btnResetBottom" CausesValidation="false" runat="server" TabIndex="64"
                                                        class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton>
                                                </td>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </div>
                
            </div>
        </div>
        <div class="floatLeft">
            <div class="clsGreyBandStyleFund marginBottom10" id="divPaymentMethod" style="width: 992px;">
                <strong>Holiday</strong>
            </div>

           <div style="margin-left:40px;width:auto;height:80%" class="divHolidayOuter roundifyRefine8" >
            <asp:UpdatePanel ID="updatePnlListMessages" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="div1" style="padding-left: 40px; clear: none;">
                        <div id="divMediumBtnImage" class="clsButtonGreyDetailsAccept " style="margin-left: 30px;
                            text-align: center;">
                            <a id="btnAddHoliday" onclick="btnAddHoliday_Click" runat="server" class="txtListing"
                                tabindex="11"><strong>
                                <%--Poonam modified on 22/4/2016 - Task - OA-245 : OA - Broken Icon link in Working days--%>
                                    <img src="Images/Icons/Icon-SpecialistSmall.gif" runat="server" id="imgbtnSpecialist"
                                        width="12" height="16" hspace="5" vspace="0" align="absMiddle" border="0">&nbsp;Add
                                    Holiday</strong></a></div>
                             </div>
        <table  style="margin-left:2px; width:100%;">          

         <tr>
                <td style="width:20%;">
                    &nbsp;
                </td>
                <td>
                    <asp:Button ID="btnClearSelection" Visible="false" runat="server" Text="Clear Calendar Selection" class="txtListing" />
                </td>
                 <td style="width:20%;">
                    &nbsp;
                </td>
            </tr>
                        <tr>
                 <td style="width:32%; text-align:right; vertical-align:top;">
                    <asp:Label ID="lblSelectDate" Visible="false" runat="server" Text="Choose Dates: "></asp:Label>
                </td>

                   <%--Poonam modified on 22/4/2016 - Task - OA-245 : OA - Broken Icon link in Working days--%>
                <td valign="top" align="left">
                    <asp:Calendar ID="Calendar1" SelectionMode="None" Width="560px" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66"
                        BorderWidth="1px" DayNameFormat="Full" Font-Names="Verdana" Font-Size="8pt" 
                        ForeColor="#663399"  ShowGridLines="True" 
                        SelectWeekText="Week <img height='10' border='0' src='Images/Arrows/Black-Arrow.gif' width='5'>"                         
                        SelectMonthText="Month <img height='10' border='0' src='Images/Icons/Icon-SpecialistSmall.gif' width='10'>" 
                        ShowNextPrevMonth="true" NextMonthText="" >
                         <%--SelectMonthText="<img height='10' border='0' src='OWLibrary/Images/Icons/Icon-SpecialistSmall.gif' width='5'>"--%>
                        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                        <SelectorStyle BackColor="#FFCC66"/>
                        <TodayDayStyle BackColor="#FFCC66" />
                        <OtherMonthDayStyle ForeColor="#CC9966" BackColor="LightGray" />
                        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />

                    </asp:Calendar>
                      <%--Poonam modified on 22/4/2016 - Task - OA-245 : OA - Broken Icon link in Working days--%>
                    <td valign="top" ><asp:Calendar Width="560px" ID="Calendar2" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66"
                        BorderWidth="1px" DayNameFormat="Full" Font-Names="Verdana" Font-Size="8pt" 
                        ForeColor="#663399" SelectionMode="None" ShowGridLines="True" 
                         SelectWeekText="Week <img height='10' border='0' src='Images/Arrows/Black-Arrow.gif' width='5'>"  

                        SelectMonthText="Month <img height='10' border='0' src='Images/Icons/Icon-SpecialistSmall.gif' width='10'>" 
                        ShowNextPrevMonth="true" PrevMonthText="" >
            
                        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                        <SelectorStyle BackColor="#FFCC66" />
                        <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                        <OtherMonthDayStyle ForeColor="#CC9966" BackColor="LightGray" />
                        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />

                    </asp:Calendar></td>
                </td>
            </tr>
             <tr>
                <td style="width:20%; text-align:right;">
                    <asp:Label ID="lblHolidatDate" Visible="false" runat="server" Text="Add Holidays Caption: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtHolidayDateTop" Visible="false" Width="60%" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="btnClick" ID="reqHolidayCaption" ControlToValidate="txtHolidayDateTop"  runat="server" ErrorMessage="Please Enter Holiday Caption" Display="Dynamic" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                </td>
                 <td style="width:20%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                  <td style="width:20%;">
                    &nbsp;
                </td>
                <td align="left">
                    <asp:Button ID="btnSelectDates" ValidationGroup="btnClick" Visible="false" runat="server" Text="Add/Remove Holidays" />
                    <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                </td>
                 <td style="width:20%;">
                    &nbsp;
                </td>
            </tr>
            
        </table>

         <asp:Panel ID="pnlHolidayListing" Visible="false" runat="server" Width="100%">
        
                             <div runat="server" visible="false" id="divGrid" style="height: auto; width: 100%;margin: 0px 5px 0px 5px;">
                                <div class="paddedBox" id="divProfile" style="height: auto; width: 100%;">
                                    <div class="divHolidayOuterHolidayList roundifyRefine8" style="height: auto; width: 100%;">
                                        <div id="tdTotalRecs" style="width: 100px; padding-top: 10px; padding-left: 0px;
                                            margin-left: 10px; text-align: left;" runat="server">
                                        </div>
                                        <div style="width: 1010px; margin-top: 2px; margin-bottom: 2px;">
                                            <div id="divPagerTop" style="float: left; clear: right; width: 300px;">
                                                <uc1:GridPager ID="GridPagerTop" runat="server"></uc1:GridPager>
                                            </div>
                                            <div id="Div2" style="padding-top: 10px; text-align: right; font-size: 11px; float: right;
                                                clear: right;" runat="server">
                                                View Records
                                                <asp:DropDownList Style="margin-left: 4px;" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                                                    CssClass="formField marginLR5" ID="ddlPageSize" runat="server" AutoPostBack="true">
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>20</asp:ListItem>
                                                    <asp:ListItem>25</asp:ListItem>
                                                </asp:DropDownList>
                                                per page
                                            </div>
                                        </div>
                                        <br />
                                        <br />

                                       <div>

        

        <br />

        <b></b>

        <asp:Label ID="LabelAction" runat="server"></asp:Label><br />      

    </div>
                                        <%--<div style="float:left;" style="height:auto;">--%>
                                        <asp:GridView Visible="false" ID="grdHolidayList" ForeColor="Maroon" Font-Size="11px" BorderWidth="1px"
                                            BorderColor="Black" runat="server" AutoGenerateColumns="false" Width="1000px">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Holidaye Date" ItemStyle-Width="250px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHolidayDate" runat="server" Text='<%#Eval("HolidayDate") %>'></asp:Label>
                                                        <asp:TextBox runat="server" Visible="false" Text='<%#Eval("HolidayDate") %>' ID="txtHolidayDate1"
                                                            Height="15" Width="120" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
                                                        <img alt="Click to Select" runat="server" visible="false" src="../OWLibrary/Images/calendar.gif"
                                                            id="imgCalHoliday" style="cursor: pointer; vertical-align: middle;" />
                                                        <asp:RegularExpressionValidator ForeColor="Red" ID="regHolidayDate1" Display="Dynamic"
                                                            ControlToValidate="txtHolidayDate1" ErrorMessage="Holiday Date should be in DD/MM/YYYY Format"
                                                            ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                            runat="server"></asp:RegularExpressionValidator>
                                                        <cc1:CalendarExtender CssClass="dddddd" Format="dd/MM/yyyy" PopupButtonID="imgCalHoliday"
                                                            TargetControlID="txtHolidayDate1" ID="CalendarExtender1" runat="server" Enabled="true">
                                                        </cc1:CalendarExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Holiday Text" ItemStyle-Width="500px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHolidayCaption" runat="server" Text='<%#Eval("HolidayTxt") %>'></asp:Label>
                                                        <asp:TextBox ID="txtHolidayCaption" Width="500px" runat="server" Text='<%#Eval("HolidayTxt") %>'
                                                            Visible="false"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="IsActive" ItemStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIsActive" runat="server" Text='<%#IIF(Eval("IsDeleted")=0,"True","False") %>'></asp:Label>
                                                        <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%#IIF(Eval("IsDeleted")=0,"True","False") %>'
                                                            Visible="false"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" CommandName="EditHoliday" CommandArgument='<%#Eval("DateID")%>'>
                                                            <img id="imgEditHoliday" runat="server" src="~/OWLibrary/Images/Icons/Icon-Pencil.gif"
                                                                title="Edit Holiday" border="0" class="cursorPointer" />
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Cancel" Visible="false" CommandName="Cancel"></asp:LinkButton>
                                                        &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkInActive" runat="server" CommandName="InActive"
                                                            CommandArgument='<%#Eval("DateID")%>'>
                                                            <img id="img1" runat="server" src="~/OWLibrary/Images/Icons/Cancel.gif" title="Delete Holiday"
                                                                border="0" class="cursorPointer" />
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <%--</div>	  --%>
                                        <div style="width: 1010px; margin-top: 2px; margin-bottom: 2px;">
                                            <div id="divPagerBtm">
                                                <uc1:GridPager ID="GridPagerBtm" runat="server"></uc1:GridPager>
                                            </div>
                                            <div id="Div3" style="padding-right: 0px; padding-top: 5px; text-align: right; font-size: 11px;
                                                float: right;" runat="server">
                                                View Records
                                                <asp:DropDownList Style="margin-left: 4px;" OnSelectedIndexChanged="ddlPageSizeBtm_SelectedIndexChanged"
                                                    CssClass="formField marginLR5" ID="ddlPageSizeBtm" runat="server" AutoPostBack="true">
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>20</asp:ListItem>
                                                    <asp:ListItem>25</asp:ListItem>
                                                </asp:DropDownList>
                                                per page
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                       <%-- <asp:Panel ID="pnlnoRecords" runat="server">
                            <div style="text-align: center;">
                                No Records Available</div>
                        </asp:Panel>--%>
              
                    <div id="divClearBoth">
                    </div>
                    <cc1:ModalPopupExtender ID="mdlHoliday" runat="server" TargetControlID="btnTemp"
                        PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground"
                        Drag="False" DropShadow="False">
                    </cc1:ModalPopupExtender>
                    <asp:Panel runat="server" ID="pnlConfirm" Width="320px" CssClass="pnlConfirm" Style="display: none;">
                        <asp:Label ID="lblError" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                        <div id="divModalParent">
                            <span style="font-family: Tahoma; font-size: 14px;"><b>Holiday</b></span>
                            <br />
                            <table width="380px">
                                <tr>
                                    <td align="right" width="140px">
                                        <label>
                                            Holiday Date From:</label>
                                    </td>
                                    <td width="220px">
                                        <asp:TextBox runat="server" ID="txtHolidayDate" Height="15" Width="130" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
                                        <img alt="Click to Select" src="../OWLibrary/Images/calendar.gif" id="btnHolidayDate"
                                            style="cursor: pointer; vertical-align: middle;" />
                                        <asp:RegularExpressionValidator ID="regHolidayDate" ControlToValidate="txtHolidayDate"
                                            ErrorMessage="Holiday Date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB"
                                            ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                            runat="server">*</asp:RegularExpressionValidator>
                                        <cc1:CalendarExtender CssClass="dddddd" Format="dd/MM/yyyy" PopupButtonID="btnHolidayDate"
                                            TargetControlID="txtHolidayDate" ID="calHolidayDate" runat="server" Enabled="true">
                                        </cc1:CalendarExtender>
                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtHolidayDate"
                                            WatermarkText="Select Holiday Date" WatermarkCssClass="bodyTextGreyLeftAligned">
                                        </cc1:TextBoxWatermarkExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <label>
                                            Holiday Date To:</label>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtHolidayDateTo" Height="15" Width="130" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
                                        <img alt="Click to Select" src="../OWLibrary/Images/calendar.gif" id="btnHolidayDateTo"
                                            style="cursor: pointer; vertical-align: middle;" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtHolidayDateTo"
                                            ErrorMessage="Holiday Date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB"
                                            ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                            runat="server">*</asp:RegularExpressionValidator>
                                        <cc1:CalendarExtender CssClass="dddddd" Format="dd/MM/yyyy" PopupButtonID="btnHolidayDateTo"
                                            TargetControlID="txtHolidayDateTo" ID="CalendarExtender2" runat="server" Enabled="true">
                                        </cc1:CalendarExtender>
                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtHolidayDateTo"
                                            WatermarkText="Select Holiday Date" WatermarkCssClass="bodyTextGreyLeftAligned">
                                        </cc1:TextBoxWatermarkExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <label>
                                            Holiday Caption:</label>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtHolidayCaption" Height="15" Width="150" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtHolidayCaption"
                                            WatermarkText="Enter Holiday Caption" WatermarkCssClass="bodyTextGreyLeftAligned">
                                        </cc1:TextBoxWatermarkExtender>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td colspan="2" width="380px">
                                        <div id="divButton" class="divButton" style="width: 85px; float: left;">
                                            <div class="bottonTopGrey">
                                                <img src="OWLibrary/Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0"
                                                    width="4" height="4" class="btnCorner" style="display: none" /></div>
                                            <a class="buttonText cursorPointer" onclick='javascript:validate_required()' id="ancSubmit">
                                                Submit</a>
                                            <div class="bottonBottomGrey">
                                                <img src="OWLibrary/Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0"
                                                    width="4" height="4" class="btnCorner" style="display: none" /></div>
                                        </div>
                                        <div id="divButton" class="divButton" style="width: 85px; float: left; margin-left: 20px;">
                                            <div class="bottonTopGrey">
                                                <img src="OWLibrary/Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0"
                                                    width="4" height="4" class="btnCorner" style="display: none" /></div>
                                            <a class="buttonText cursorPointer" onclick='javascript:clearText("ctl00_ContentPlaceHolder1_UCSupplierWOsListing1_txtNote")'
                                                runat="server" id="ancCancel">Cancel</a>
                                            <div class="bottonBottomGrey">
                                                <img src="OWLibrary/Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0"
                                                    width="4" height="4" class="btnCorner" style="display: none" /></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            
                        </div>
                    </asp:Panel>
                    <script type="text/javascript">
                        function validate_required(field, alerttxt, btnID) {
                            var text = document.getElementById("ctl00_ContentPlaceHolder1_txtHolidayDate").value;

                            if (text != "Select Holiday Date") {
                                while (text.indexOf(" ") != -1) {
                                    text = text.replace(" ", "");
                                }
                            }

                            if (text == null || text == "" || text == "Select Holiday Date") {
                                alert("Select Holiday Date");
                            }
                            else {
                                text = document.getElementById("ctl00_ContentPlaceHolder1_txtHolidayCaption").value;
                                if (text != "Enter Holiday Caption") {
                                    while (text.indexOf(" ") != -1) {
                                        text = text.replace(" ", "");
                                    }
                                }
                                if (text == null || text == "" || text == "Enter Holiday Caption") {
                                    alert("Enter Holiday Caption");
                                }
                                else {
                                    document.getElementById("ctl00_ContentPlaceHolder1_hdnButton").click();
                                }

                            }
                        }
                        function clearText() {
                            document.getElementById("ctl00_ContentPlaceHolder1_txtHolidayDate").value = "";
                            document.getElementById("ctl00_ContentPlaceHolder1_txtHolidayCaption").value = "";
                        }
                    </script>
                    <asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click" Height="0" Width="0"
                        BorderWidth="0" Style="visibility: hidden;" />
                    <asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" Style="visibility: hidden;" />
                </ContentTemplate>
                <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Calendar1" EventName="SelectionChanged" />
                        <asp:AsyncPostBackTrigger ControlID="Calendar2" EventName="SelectionChanged" />
                </Triggers>
            </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>