<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Get Password" CodeBehind="RetrievePassword.aspx.vb" Inherits="Admin.RetrievePassword" %>

			<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
	  <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<table width="100%"  border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td width="40">&nbsp;</td>
				<td>
					<P>
						Enter Password:
						<BR>
						<asp:TextBox id="TextBox1" runat="server"></asp:TextBox></P>
					<P>
						<asp:Button id="btnDecrypt" runat="server" Text="Decrypt"></asp:Button>&nbsp;
                        <asp:Button id="btnEncrypt" runat="server" Text="Encrypt"></asp:Button>&nbsp;
                        <asp:Button id="btnDecrypt_DES" runat="server" Text="Decrypt_DES"></asp:Button>&nbsp;
                        <asp:Button id="btnEncrypt_DES" runat="server" Text="Encrypt_DES"></asp:Button>&nbsp;
						<asp:Button id="btnEncryptMD5" runat="server" Text="Encrypt MD5"></asp:Button></P>
					<P>Enter Email - Get Password:<BR>
						<asp:TextBox id="txtEmail" runat="server"></asp:TextBox></P>
					<P>
						<asp:Button id="btnGetPwd" runat="server" Text="Get Password"></asp:Button><BR>
					</P>
					<P>
					<asp:Label id="Label1" runat="server"></asp:Label></P>
				</td>
			  </tr>
			</table>

              
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
         </asp:Content>