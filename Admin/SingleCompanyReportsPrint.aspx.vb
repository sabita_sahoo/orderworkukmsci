Public Partial Class SingleCompanyReportsPrint
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PopulateData()
    End Sub
    Public Sub PopulateData()
        Dim ds As DataSet = ws.WSStandards.GetSingleCompanyReport(Convert.ToInt32(Request("BizDivId")), Convert.ToInt32(Request("CompanyId")), Request("ToDate").ToString, Request("StartDate").ToString)
        Session("SingleCompanyReport") = ds
        If ds.Tables.Count > 0 Then
            pnlShowRecords.Visible = True
            If ds.Tables(0).Rows.Count > 0 Then
                Repeater1.DataSource = ds.Tables(0)
                Repeater1.DataBind()
                lblCompanyName.Text = ds.Tables(0).Rows(0)("CompanyName").ToString
                lblFromDate.Text = Request("StartDate").ToString
                lblToDate.Text = Request("ToDate").ToString
            Else
                pnlShowRecords.Visible = False
            End If
            If ds.Tables.Count > 1 Then
                If ds.Tables(1).Rows.Count > 0 Then
                    Repeater2.DataSource = ds.Tables(1)
                    Repeater2.DataBind()
                End If
                If ds.Tables.Count > 2 Then
                    If ds.Tables(2).Rows.Count > 0 Then
                        divServices.Visible = True
                        Repeater3.DataSource = ds.Tables(2)
                        Repeater3.DataBind()
                    Else
                        divServices.Visible = False
                    End If
                    If ds.Tables.Count > 3 Then
                        If ds.Tables(3).Rows.Count > 0 Then
                            Repeater7.DataSource = ds.Tables(3)
                            Repeater7.DataBind()
                        End If
                    End If
                End If
            End If
        Else
            pnlShowRecords.Visible = False
        End If
    End Sub

    Private Sub Repeater7_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles Repeater7.ItemDataBound
        Dim billingLocID As Integer
        billingLocID = CType(e.Item.FindControl("hdnBillingLocID"), HtmlInputHidden).Value
        Dim ds As New DataSet
        ds = CType(Session("SingleCompanyReport"), DataSet)
        If ds.Tables.Count > 4 Then
            If ds.Tables(4).Rows.Count > 0 Then
                Dim dv As New DataView
                dv = ds.Tables(4).DefaultView
                dv.RowFilter = "BillingLocID = " & billingLocID
                CType(e.Item.FindControl("Repeater4"), Repeater).DataSource = dv.ToTable
                CType(e.Item.FindControl("Repeater4"), Repeater).DataBind()
            End If
            If ds.Tables.Count > 5 Then
                If ds.Tables(5).Rows.Count > 0 Then
                    Dim dv As New DataView
                    dv = ds.Tables(5).DefaultView
                    dv.RowFilter = "BillingLocID = " & billingLocID
                    CType(e.Item.FindControl("Repeater5"), Repeater).DataSource = dv.ToTable
                    CType(e.Item.FindControl("Repeater5"), Repeater).DataBind()
                End If
                If ds.Tables.Count > 6 Then
                    If ds.Tables(6).Rows.Count > 0 Then
                        CType(e.Item.FindControl("divServices2"), HtmlGenericControl).Visible = True
                        Dim dv As New DataView
                        dv = ds.Tables(6).DefaultView
                        dv.RowFilter = "BillingLocID = " & billingLocID
                        If dv.ToTable.Rows.Count > 0 Then
                            CType(e.Item.FindControl("Repeater6"), Repeater).DataSource = dv.ToTable
                            CType(e.Item.FindControl("Repeater6"), Repeater).DataBind()
                        Else
                            CType(e.Item.FindControl("divServices2"), HtmlGenericControl).Visible = False
                        End If
                    Else
                        CType(e.Item.FindControl("divServices2"), HtmlGenericControl).Visible = False
                    End If
                End If
            End If

        End If
    End Sub
End Class