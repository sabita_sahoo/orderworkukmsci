<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SalesInvoiceGeneration.aspx.vb" Inherits="Admin.SalesInvoiceGeneration" ValidateRequest="false" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Sales Invoice Generation" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
	<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
           <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<script language="javascript" type="text/javascript">
function OpenNewWindowInvoice(InvoiceNo,BizDivId,CompanyId) {    
var w = 800;
var h = 800;
var left = (screen.width/2)-(w/2);
var top = (screen.height/2)-(h/2);
//window.open ('ViewSalesInvoice.aspx?invoiceNo='+ InvoiceNo + '&bizDivId='+ BizDivId + '&companyId='+ CompanyId +'', 'SalesInvoice', 'toolbar=0,location=0, directories=0, status=0, menubar=0, scrollbars=0, resizable=1, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
window.open('InvoicesPrintForm.aspx?invoiceNo=' + InvoiceNo + '&bizDivId=' + BizDivId + '&companyId=' + CompanyId + '', 'SalesInvoice', 'toolbar=0,location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}
function CheckGroup(Id) {
    var chk = document.getElementById(Id);
    var row = chk.parentNode.parentNode;
    var rowIndex = row.rowIndex;
    var grid = document.getElementById("<%= gvWorkOrders.ClientID %>");
    var hdnPO = grid.rows[rowIndex].cells[0].getElementsByTagName('input');
    cellValue = hdnPO[2].value;
    if (grid.rows.length > 0) {
         for (i = 1; i < grid.rows.length - 1; i++) {
             var tempPO = grid.rows[i].cells[0].getElementsByTagName('input');
                {if (cellValue == tempPO[2].value) {
                   var cell = grid.rows[i].cells[0];
                   for (j = 0; j < cell.childNodes.length; j++) {
                     if (cell.childNodes[j].type == "checkbox") {
                        if (chk.checked == true) 
                            {cell.childNodes[j].checked = true;}
                                else
                                    cell.childNodes[j].checked = false;
                            }
                        }
                    }
                }
            }
        }
    }
    function AddTextToClipboard(value) {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE");
        if (msie > 0) {
            window.clipboardData.setData('Text', value);
        }
        else {
            window.prompt("Copy to clipboard: Ctrl+C, Enter", value);
        }
    }
</script>
	        <div id="divContent">
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
             <input type="hidden" runat="server" id="hdnKillCache" name="hdnKillCache" value="True" />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
             <ContentTemplate>
           <asp:Panel ID="pnlSearchTop" runat="server" Visible="true" style="margin-bottom:20px;">
                
			<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10px"></td>
			<td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Sales Invoice Generation " runat="server"></asp:Label></td>
			</tr>
			</table>			
		<table width="100%" cellpadding="0" cellspacing="0" border="0" runat="server" id="tblSearchcontact">
		<tr>
		<td width="10px"></td>
		<td width="240"><%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
					<uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact></td>
		<td >
		</td>
	    <td width="80" align="left"><asp:CheckBox ID="chkShowAll" runat="server" Text='Show All' class="formTxt" Checked="true" onclick="Javascript:ShowRecords(this.id);" /> </td>        
        <td width="80" align="left"><asp:CheckBox ID="chkShow100" runat="server" Text='Show 100' class="formTxt" onclick="Javascript:ShowRecords(this.id);"/> </td>        
		<td align="left">
        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
            <asp:LinkButton ID="lnkView" causesValidation=False OnClick="lnkView_Click" runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
        </div>
		</td>
		
	 <td align="left">
     <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
        <a runat="server" target="_blank" id="btnExport" class="txtButtonRed"> &nbsp;Export to Excel&nbsp;</a>
     </div>
		</td>		
		</tr>
			  
		</table>
		</asp:Panel>	
		
              <asp:Panel ID="pnlShowClosedWorkOrders" runat="server" Visible="true">
                 
		    <table>			
				<tr>
                <td width="5"> </td>
		  <td width="160"> <asp:CheckBox ID="chkIndividualInvoice" TabIndex="1" runat="server" Text="Generate individual invoices" CssClass="formTxt"></asp:CheckBox></td>
          <td width="120"> <asp:CheckBox ID="chkGroupInvoiceByPO" TabIndex="1" runat="server" Text="Group Invoice by PO" CssClass="formTxt"></asp:CheckBox></td>
		   <td width="120" valign="bottom"> <asp:CheckBox ID="chkListingFee" TabIndex="1" runat="server" Text="Charge Listing Fees" CssClass="formTxt" AutoPostBack="true"></asp:CheckBox></td>
   			<td width="130"  valign="bottom" Class="formTxt"  id="tdListingFee"> <asp:Label id="lblPoundSign" runat="server" Visible="false" Text="&pound"></asp:Label>   <asp:TextBox ID="txtListingFee" Visible="false" TabIndex="2" runat="server" Text="8.00" TextMode="SingleLine" CssClass="formField105" style="text-align:right"></asp:TextBox></td>		          
		  <td width="143">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:150px;">
                <asp:LinkButton ID="lnkAssignInvoiceNo" causesValidation=False OnClick="AssignInvoiceNo" runat="server" TabIndex="5" CssClass="txtButtonRed" Text="Generate Invoice(s)"></asp:LinkButton>
            </div>
          </td> 
          <td width="20">&nbsp;</td>
          <td width="143">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:150px;">
                <asp:LinkButton ID="lnkchngBillLocation" causesValidation=False OnClick="ChangeBillingLocation" runat="server" TabIndex="6" CssClass="txtButtonRed" Text="Change Billing Location"></asp:LinkButton>
            </div>
          </td>         
		  <td width="20">&nbsp;</td>
          <td width="115">
              <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:150px;">
                <asp:LinkButton ID="lnkUpdatePONumber" causesValidation=False  runat="server" OnClick="UpdatePONumber" TabIndex="6" CssClass="txtButtonRed" Text="Update PONumber"></asp:LinkButton>
              </div>
          </td>   
		  </tr>

		</table>
		
           <table><tr>
             <td class="paddingL30 padB21" valign="top">
                <div id="divValidationMain" visible=false class="divValidation" runat=server >
				  <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					</tr>
					<tr valign="middle">
					  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
					  <td class="validationText"><div  id="divValidationMsg"></div>
						  <asp:Label ID="lblMsg" runat="server"  CssClass="bodytxtValidationMsg"></asp:Label>
					  </td>
					  <td width="20">&nbsp;</td>
					</tr>
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					</tr>
				  </table>
				  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				</div>   
		
		</td></tr>
             </table>
	   <hr style="background-color:#993366;height:5px;margin:0px;" />
           	<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td>
              <asp:GridView ID="gvWorkOrders"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
               PagerSettings-Mode=NextPreviousFirstLast PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               
               <PagerTemplate>
                   
            <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>																				
							<td width="10">&nbsp;</td>												  
						  	<td runat="server" id="tdSelectAll" enableviewstate=true width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">
							  <TR>
								<TD class="txtListing" align="left"><strong>
								  
								  
								</strong> </TD>
								<TD width="5"></TD>
							  </TR>
							</TABLE>						
							</td>				
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right" runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
												  </div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
        </PagerTemplate> 
               <Columns>  
                         <asp:TemplateField HeaderText="<input type=checkbox runat=server id='chkSelectAll' onclick=CheckAll(this,'Check') valign='middle' />">               
               <HeaderStyle CssClass="gridHdr" Width="30px"  Wrap=true   />
               <ItemStyle Wrap=true HorizontalAlign=Left  Width="15px"  CssClass="gridRow"/>
               <ItemTemplate>   
                    <input type=hidden runat=server id='hdnWOID'  value='<%#Container.DataItem("WOID") %>'/>                    
                    <asp:CheckBox ID='Check'  runat='server'></asp:CheckBox>
                    <input type=hidden runat=server id='hdnPO'  value='<%#Container.DataItem("ponumber") %>'/>  
                       
               </ItemTemplate>
               </asp:TemplateField>  
                                    
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="150px"   HeaderStyle-CssClass="gridHdr" DataField="RefWOID" SortExpression="RefWOID" HeaderText="WorkOrderID" />                
                <asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="120px" HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="CloseDate" SortExpression="CloseDate" HeaderText="Closed Date" />                    
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="60px"   HeaderStyle-CssClass="gridHdr" DataField="Quantity" SortExpression="Quantity" HeaderText="Quantity" /> 
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="WOTitle" SortExpression="WOTitle" HeaderText="Work Order Title" /> 
                <asp:TemplateField SortExpression="BuyerName" HeaderText="Client Name" >               
               <HeaderStyle CssClass="gridHdr" Width="200px"  Wrap=true />
               <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
               <ItemTemplate>   
                    <span style='cursor:hand;'>
						<%# GetBuyerLinks(IIf(IsDBNull(Container.DataItem("BuyerName")), "", Container.DataItem("BuyerName")), Container.DataItem("CompanyId"))%>
					</span> 
               </ItemTemplate>
               </asp:TemplateField>                
                
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="100px"   HeaderStyle-CssClass="gridHdr" DataField="Location" SortExpression="Location" HeaderText="Billing Location" />                
                
                 <asp:TemplateField SortExpression="SupplierName" HeaderText="Supplier" >               
               <HeaderStyle CssClass="gridHdr" Width="200px"  Wrap=true />
               <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
               <ItemTemplate>   
                    <span style='cursor:hand;'>
						<%# GetSupplierLinks(IIf(IsDBNull(Container.DataItem("SupplierCompanyID")), "", Container.DataItem("SupplierCompanyID")), IIf(IsDBNull(Container.DataItem("SupplierName")), "", Container.DataItem("SupplierName")), Container.DataItem("CompanyId"))%>
					</span> 
               </ItemTemplate>
               </asp:TemplateField>

                <asp:TemplateField SortExpression="PONumber" HeaderText="PONumber" >               
               <HeaderStyle CssClass="gridHdr" Width="100px"  Wrap=true />
               <ItemStyle Wrap=true HorizontalAlign=Left  Width="100px"  CssClass="gridRow"/>
               <ItemTemplate>   
                     <%#Container.DataItem("PONumber") %>      
                     <asp:LinkButton ID="lnkBtnEdit" style="float:right;" CausesValidation="false"   CommandName="EditPONumber" CommandArgument='<%#Container.DataItem("RefWOID") & "#" & Container.DataItem("PONumber")%>' runat="server">
                           <img src="Images/Icons/Edit.gif" title='Edit' width="12" height="11" hspace="2" vspace="0" border="0">
                    </asp:LinkButton>  
               </ItemTemplate>
               </asp:TemplateField>
                
                <asp:TemplateField SortExpression="WholeSalePrice" HeaderText="Whole Sale Price" >               
               <HeaderStyle CssClass="gridHdr" Width="200px"  Wrap=true />
               <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
               <ItemTemplate>   
                <%#FormatCurrency(Container.DataItem("WholeSalePrice"), 2, TriState.True, TriState.True, TriState.False)%>                       
               </ItemTemplate>
               </asp:TemplateField>
               
                                
                <asp:TemplateField >               
               <HeaderStyle CssClass="gridHdr"   Wrap=true /> 
              <ItemStyle Wrap=true HorizontalAlign=Left   CssClass="gridRow"/>
               <ItemTemplate>   
                         
                       <%#GetLinks(Container.DataItem("CompanyId"), Container.DataItem("WOID"))%>
               </ItemTemplate>
               </asp:TemplateField>  
               
				</Columns>
               
                 <AlternatingRowStyle    CssClass=gridRow />
                 <RowStyle CssClass=gridRow />            
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
         
        
              </asp:GridView>
				 </td>
            </tr>
            </table>
				
				 <asp:ObjectDataSource ID="ObjectDataSource1" 
				 runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  
				 EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.SalesInvoiceGeneration" SortParameterName="sortExpression" 
				 StartRowIndexParameterName="startRowIndex" MaximumRowsParameterName="maximumRows"  >                
				 </asp:ObjectDataSource>
          </asp:Panel>
          <asp:Panel ID="pnlShowInvoice" runat="server" Visible="false">
          <div style="margin-left:20px;">
            <p class="paddingB4 HeadingRed"><strong>Based on your work order selection, the following sales invoice(s) will be created: </strong> </p>
            <div style="margin-bottom:15px; width:100%;">
                <div style="float:left; margin-right:10px;">
                    <asp:Label ID="Label1" runat="server" Text="Invoice Date (dd/mm/yyyy)" CssClass="formTxt"></asp:Label>
                    <asp:RegularExpressionValidator ID="regExpInvoiceDate" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtInvoiceDate"
							ErrorMessage="Invoice Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
                </div>
                <div style="float:left; margin-right:30px;">
                <asp:TextBox ID="txtInvoiceDate" TabIndex="3" runat="server" CssClass="formField105"></asp:TextBox>
	            <img alt="Click to Select" src="Images/calendar.gif" id="btnInvoiceDate" style="cursor:pointer; vertical-align:middle;" />		    	
              <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnInvoiceDate TargetControlID="txtInvoiceDate" ID="calFromDate" runat="server">
              </cc1:CalendarExtender>
               </div>
               <div style=" float:left;">
                   <asp:CheckBox ID="chkbxemailnotification" runat="server" Text="Send Email Notification" CssClass="formTxt"/>
               </div>
            </div>
            <div style=" clear:both"></div>
            <div style="width:100%; font-size:11px; margin-top:10px;">
                <div style="width:100%; font-weight:bold; background-color:#993366; color:#ffffff;float:left; ">
                    <div style="width:110px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; border-top: solid 1px #ffffff; padding-left:5px; height:25px;">
                            <asp:Label ID="lblSI" runat="server" Text="Sales Invoice No."></asp:Label>
                        </div>
                        <div style="width:200px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; border-top: solid 1px #ffffff; padding-left:5px; height:25px;">
                            <asp:Label ID="lblGroupingItemName" runat="server"></asp:Label>
                        </div>
                        <div style="width:110px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; border-top: solid 1px #ffffff; padding-left:5px; height:25px;">
                            <asp:Label ID="lblNetAmnt" runat="server" Text="Net Amount" ></asp:Label>
                        </div>
                        <div style="width:80px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; border-top: solid 1px #ffffff; padding-left:5px; height:25px;">
                            <asp:Label ID="lblVATAmnt" runat="server" Text="VAT Amount" ></asp:Label>
                        </div>
                        <div style="width:100px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; border-top: solid 1px #ffffff; padding-left:5px; height:25px;">
                            <asp:Label ID="lblDiscountAmt" runat="server" Text="Discount Amt" ></asp:Label>
                        </div>
                        <div style="width:110px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; border-top: solid 1px #ffffff; padding-left:5px; height:25px;">
                            <asp:Label ID="lblVATOnDiscount" runat="server" Text="VAT on discount" ></asp:Label>
                        </div>
                        <div style="width:120px; float:left; border: solid 1px #ffffff; padding-left:5px; height:25px;">
                            <asp:Label ID="lblTotalAmnt" runat="server" Text="Total Amount" ></asp:Label>
                        </div>
                        <div style="width:120px; float:left; border: solid 1px #ffffff; padding-left:5px; height:25px;">
                            <asp:Label ID="lblBusinessArea" runat="server" Text="Business Area" ></asp:Label>
                        </div>
                        
                  </div>
                     <div style=" clear:right"></div>
                <asp:Repeater ID="rptSI" runat="server" OnItemDataBound="rptWOID_ItemDataBound">
                
                <ItemTemplate>
                    <div style="width:90%; background-color: #EDEDED;">
                        <div style=" width:100%; ">
                        <div style="width:110px; height:25px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff;padding-left:5px; ">
                            <asp:Label ID="lblSIName" runat="server" Text='<%#"Sales Invoice - " & Container.dataitem("SINo")%>'></asp:Label>
                        </div>
                        <div style="width:200px; height:25px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff;padding-left:5px;">
                            <asp:Label ID="lblGroupingItem" runat="server" Text='<%#Container.dataitem("GroupingItem")%>' ></asp:Label>
                        </div>
                        
                            <div style="width:110px; height:25px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; padding-left:5px;">
                               <div style="width:100px; text-align:right;">
                                <asp:Label ID="lblNetAmount" runat="server" Text='<%#FormatCurrency(Container.DataItem("NetAmount"), 2, TriState.True, TriState.True, TriState.False)%>' ></asp:Label>
                               </div>
                            </div>
                            <div style="width:80px; height:25px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; padding-left:5px;">
                               <div style="width:70px; text-align:right;">
                                <asp:Label ID="lblVATAmount" runat="server" Text='<%#FormatCurrency(Container.DataItem("VATAmount"), 2, TriState.True, TriState.True, TriState.False)%>' ></asp:Label>
                               </div>
                            </div>
                            <div style="width:100px; height:25px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; padding-left:5px;">
                               <div style="width:90px; text-align:right;">
                                <asp:Label ID="lblDiscountAmt" runat="server" Text='<%#FormatCurrency(Container.DataItem("DiscountAmt"), 2, TriState.True, TriState.True, TriState.False)%>' ></asp:Label>
                               </div>
                            </div>
                            <div style="width:110px; height:25px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; padding-left:5px;">
                               <div style="width:100px; text-align:right;">
                                <asp:Label ID="lblVATOnDiscount" runat="server" Text='<%#FormatCurrency(Container.DataItem("VATOnDiscount"), 2, TriState.True, TriState.True, TriState.False)%>' ></asp:Label>
                               </div>
                            </div>
                            <div style="width:120px; height:25px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; border-right:solid 1px #ffffff; padding-left:5px;">
                               <div style="width:110px; text-align:right;">
                                <asp:Label ID="lblTotalAmount" runat="server" Text='<%#FormatCurrency(Container.dataitem("TotalAmount"),2,TriState.True, TriState.True, TriState.False)%>' ></asp:Label>
                               </div>
                            </div>
                            <div style="width:120px; height:25px; float:left; border-bottom: solid 1px #ffffff; border-left:solid 1px #ffffff; border-right:solid 1px #ffffff; padding-left:5px;">
                               <div style="width:100px; text-align:right;">
                                <asp:Label ID="lblBusinessAreaValue" runat="server" Text='<%#Container.dataitem("BusinessAreaLabel")%>' ></asp:Label>
                               </div>
                            </div>                            
                        </div>
                       
                        <div id="LIDdetails" runat="server" style="width:950px;padding-left:30px;  margin-top:10px; background-color:#F4F5F0; position:static; ">
                                <div style="clear:both"></div>
                              <div style="margin-top:10px;">  
                              <div style="width:248px;background-color:#DAD8D9; ">
                                    <div style="float:left; width:110px;background-color:#DAD8D9; margin-left:0px; padding:0px; padding-left:5px;border-left: solid 1px #ffffff;">
                                        <strong><asp:Label ID="lblWOID" runat="server" Text="WorkOrderId"></asp:Label></strong>
                                    </div>          
                                    <div style="float:right;width:115px;background-color:#DAD8D9; margin-left:0px; padding:0px; padding-left:5px;border-left: solid 1px #ffffff;">
                                        <strong><asp:Label ID="lblWP" runat="server" Text="Wholesale Price"></asp:Label></strong>
                                    </div>
                                </div>
                            <asp:Repeater ID="rptWOID" runat="server">
                                <ItemTemplate>
                                    <div style="width:248px; background-color:#FFFFFF;">                         
                                        <div style="float:left; Padding-right:10px;width:110px;padding:0px; padding-left:5px; border: solid 1px #ffffff;">
                                            <asp:Label ID="lblWOID" runat="server" Text=<%#Container.dataitem("RefWOID") & "<br/>" %>></asp:Label>
                                        </div>          
                                        <div style="float:right;width:115px; margin-left:0px; padding-left:5px; border: solid 1px #ffffff;">
                                            <div style="width:80px; text-align:right;">
                                                <asp:Label ID="lblWP" runat="server" Text=<%#FormatCurrency(Container.dataitem("WholesalePrice"),2,TriState.True, TriState.True, TriState.False)%>></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>  
                            </div>        
                        </div>  
                        <table width="950" onClick='SIWODetails(this)' id="LID" border="0" cellspacing="0" cellpadding="0" bgcolor="#DFDFDF" runat="server" visible="True">
					      <tr>
						    <td class="smallText" height="8" style="padding:0px; margin:0px"></td>
						    <td height="8" align="center" class="smallText" style="padding:0px; margin:0px" id="tdCollapse" runat="server">
						            <a href="" border="0" runat="server" >
						                <img src="Images/Arrows/Grey-Arrow.gif" width="50" runat="server" title='Expand' height="8" border="0" name='LIDimgExp'  id='LIDimgExp' onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow-Roll.gif')" onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow.gif');" style="visibility:hidden; position:absolute;"  />
                                	    <img src="Images/Arrows/Orange-Arrow-Col.gif" width="50" runat="server" title='Collapse' height="8" border="0" name='LIDimgCol'  id='LIDimgCol' onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col-Roll.gif')" onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col.gif');"/>						                
						            </a></td>
						    <td class="smallText" height="8" style="padding:0px; margin:0px"></td>
					      </tr>
					 </table>                      
                    </div>
                    
                </ItemTemplate>
                </asp:Repeater>     
             </div>
            <div style="width:100%; margin-top:20px; margin-left:-10px; padding:0px;" id="divSubmitCancel" runat="server">
                <asp:Button ID="btnSubmit" runat="server" style="cursor:pointer;" Text="Submit" />
                <asp:Button ID="btnCancel" runat="server" style="cursor:pointer;" Text="Cancel" />
            </div>
           
            </div>
          </asp:Panel>
           <div id="divPostInvoiceGen" runat="server" visible="false"  style="margin-left:20px;margin-top:20px;padding:0px;">
                <TABLE cellSpacing="0" cellPadding="0"  border="0">
                 <tr><td><asp:Label ID="lblPostIvoiceMsg" runat="server" style="color:red;"></asp:Label> </td></tr>
                      <TR>
                        <TD align="left" class="HeadingRed"><TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
                      <TR>
                        <TD align="left" class="HeadingRed">&nbsp;</TD>
                        <TD align="left" class="padTop17"><TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
                          <TR>
                            <TD vAlign="bottom" align="right"><TABLE cellSpacing="0" cellPadding="0" width="80" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD width="110" class="txtBtnImage">
                                <a runat=server id="lnkPrintInvoice" target='_blank'  class="txtListing">
                                    <img src="Images/Icons/Icon-Print.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Print&nbsp;
                                </a>
                                </TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                            </TABLE></TD>
                            <TD width="10"></TD>
                            <TD width="125" align="right" vAlign="top"><TABLE  width="125" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD class="txtBtnImage">
                                <a id="lnkBack" runat="server" class="txtListing"> 
                                <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Back to Listing&nbsp;
                                </a></TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                            </TABLE></TD>					
                        </TABLE></TD>
                          </TR>
                        </TABLE></TD>
						
                      </TR>
                    </TABLE></TD>
                      </TR>
					   
                    </TABLE>
            </div>

   <cc1:ModalPopupExtender ID="mdlPONumber" runat="server" TargetControlID="btnTemp" PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
    <asp:Panel runat="server" ID="pnlConfirm" Width="300px" CssClass="pnlConfirm" style="display:none;">
        <div id="divModalParent">
            <span style="font-family:Tahoma; font-size:14px;"><b>PONumber</b></span><br /><br />           
                <asp:TextBox runat="server" ID="txtPONumber"  Width="250" CssClass="bodyTextGreyLeftAligned" ></asp:TextBox>        
            <br /><br />
                <div id="divButton" class="divButton" style="width: 85px; float:left;cursor:pointer;">
                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                        <a class="buttonText cursorHand" href='javascript:validate_required()' id="ancSubmit"><strong>Submit</strong></a>
                    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                </div>
                <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;cursor:pointer;">
                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                        <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                </div> 
        </div>								
    </asp:Panel>
    <asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
    <asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

          </ContentTemplate>
      
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
              </asp:UpdateProgress>
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
            <!-- InstanceEndEditable --></td>
         </tr>
         </table>
      </div>

      <script type="text/javascript">
          function validate_required() {
              if (document.getElementById("ctl00_ContentPlaceHolder1_txtPONumber").value == "") {

                  alert("Please enter PONumber")
              }
              else {
                  document.getElementById("ctl00_ContentPlaceHolder1_hdnButton").click();
              }
          }
          function ShowRecords(selectedCheckbox) {
              if (document.getElementById("ctl00_ContentPlaceHolder1_chkShowAll").checked == true && document.getElementById("ctl00_ContentPlaceHolder1_chkShow100").checked == true) {
                  alert("You can not select 'Show All' and 'Show 100' checkbox simultaneously.")
                  document.getElementById(selectedCheckbox).checked = false;
              }                          
              
          }
    
</script>
</asp:Content>