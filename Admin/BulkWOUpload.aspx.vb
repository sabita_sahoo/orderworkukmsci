﻿Imports System.Data.OleDb
Imports System.IO
Imports Admin.OWServicesWSWorkOrder


Imports System.Data
Imports System.Collections.Generic
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet


Public Class BulkWOUpload
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Dim dt As New DataTable()
    Dim dv As New DataView()



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            UCSearchContact1.ContactType = "BusinessAreaDefined"
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryDE
                    UCSearchContact1.BizDivID = ApplicationSettings.OWDEBizDivId
                    ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
                Case ApplicationSettings.CountryUK
                    UCSearchContact1.BizDivID = ApplicationSettings.OWUKBizDivId
                    ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
            End Select
        Catch ex As Exception
            CommonFunctions.createLog("Bulk WO Upload - Page_Load - " + ex.ToString())
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, "<br><br> Error in Bulk WO Upload - Page_Load")
        End Try
    End Sub


    Private Sub btn_upload_excel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_upload_excel.Click
        Try
            hdnContactID.Value = UCSearchContact1.ddlContact.SelectedValue
            Dim filePath As String = ""
            If fileuploader.HasFile Then
                Try
                    'Dim connectionString As String = ""

                    'Dim fileName As String = System.IO.Path.GetFileName(fileuploader.PostedFile.FileName)
                    'Dim Extension As String = System.IO.Path.GetExtension(fileuploader.FileName).ToString

                    'fileLocation = Server.MapPath(Convert.ToString("~/Templates/") & fileName)

                    'fileuploader.SaveAs(fileLocation)

                    'Select Case Extension
                    '    Case ".xls"
                    '        'Excel 97-03
                    '        connectionString = ConfigurationManager.ConnectionStrings("Excel03ConString") _
                    '                   .ConnectionString
                    '        Exit Select
                    '    Case ".xlsx"
                    '        'Excel 07
                    '        connectionString = ConfigurationManager.ConnectionStrings("Excel07ConString") _
                    '                  .ConnectionString
                    '        Exit Select

                    'End Select

                    'connectionString = String.Format(connectionString, fileLocation, "true")

                    'Dim con As New OleDbConnection(connectionString)

                    'Dim cmd As New OleDbCommand()

                    'cmd.CommandType = System.Data.CommandType.Text

                    'cmd.Connection = con

                    'Dim dAdapter As New OleDbDataAdapter(cmd)

                    'con.Open()

                    'Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)

                    'Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()

                    ''Check if the excel contain valid values

                    'cmd.CommandText = (Convert.ToString("SELECT TOP 101 * FROM [") & getExcelSheetName) + "]"

                    'dAdapter.SelectCommand = cmd

                    'dAdapter.Fill(dt)

                    'con.Close()

                    'Save the uploaded Excel file.
                    filePath = Server.MapPath("~/Templates/") + Path.GetFileName(fileuploader.PostedFile.FileName)
                    fileuploader.SaveAs(filePath)

                    'Open the Excel file in Read Mode using OpenXml.
                    Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(filePath, False)
                        'Read the first Sheet from Excel file.
                        Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild(Of Sheet)()

                        'Get the Worksheet instance.
                        Dim worksheet As Worksheet = TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart).Worksheet

                        'Fetch all the rows present in the Worksheet.
                        Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()



                        'Loop through the Worksheet rows.
                        For Each row As Row In rows
                            'Use the first row to add columns to DataTable.
                            If row.RowIndex.Value = 1 Then
                                For Each cell As Cell In row.Descendants(Of Cell)()
                                    dt.Columns.Add(GetValue(doc, cell))
                                Next
                            Else
                                'Add rows to DataTable.
                                dt.Rows.Add()
                                Dim i As Integer = 0
                                For Each cell As Cell In row.Descendants(Of Cell)()
                                    If i < 22 Then
                                        dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
                                    End If
                                    i = i + 1
                                Next
                            End If
                        Next
                    End Using

                    'Delete blank rows
                    For i As Integer = dt.Rows.Count - 1 To 0 Step -1
                        If IsDBNull(dt.Rows(i)(0)) Or Convert.ToString(dt.Rows(i)(0)) = "" Then
                            dt.Rows(i).Delete()
                        End If
                    Next

                    dt.AcceptChanges()

                    If rbl_typeofClient.SelectedValue = 1 Then
                        ViewState("Mode") = "Retail"
                        For i As Integer = dt.Rows.Count - 1 To 0 Step -1
                            If Not IsDBNull(dt.Rows(i)(2)) Or Convert.ToString(dt.Rows(i)(2)) = "" Then
                                dt.Rows(i)("Appointment Date") = DateTime.FromOADate(Convert.ToDouble(dt.Rows(i)(2))).ToString("dd/MM/yyyy")
                            End If
                        Next
                    Else
                        ViewState("Mode") = "IT"
                        For i As Integer = dt.Rows.Count - 1 To 0 Step -1
                            If Not IsDBNull(dt.Rows(i)(2)) Or Convert.ToString(dt.Rows(i)(2)) = "" Then
                                dt.Rows(i)("Schedule Window Begin") = DateTime.FromOADate(Convert.ToDouble(dt.Rows(i)(2))).ToString("dd/MM/yyyy")
                            End If
                            If Not IsDBNull(dt.Rows(i)(3)) Or Convert.ToString(dt.Rows(i)(3)) = "" Then
                                dt.Rows(i)("Schedule Window End") = DateTime.FromOADate(Convert.ToDouble(dt.Rows(i)(3))).ToString("dd/MM/yyyy")
                            End If
                        Next
                    End If


                    'Retail
                    If ViewState("Mode") = "Retail" Then
                        If dt.Columns(2).ColumnName = "Appointment Date" AndAlso dt.Columns(3).ColumnName = "Appointment Time" Then
                            dt = dt.DefaultView.ToTable(False, "Sr No", "Work Request Title", "Appointment Date", "Appointment Time", "Work Request Type", "Sub Category For Work Request Type", "Sales Invoice Title", "Scope of Work", "Contact First Name", "Contact Last Name", "Postal Code", "Address", "City", "Phone", "Estimated Time Required", "Product Purchased", "WP", "PP", "PO Number", "Engineer Scope of work", "Allow bid review", "Force Sign off sheet")
                            If dt.Rows.Count > 0 Then

                                DataList1.DataSource = dt
                                DataList1.DataBind()

                                divViewForRetailClient.Attributes.Add("class", "Show")
                                divViewForITClient.Attributes.Add("class", "Hide")
                                divUpload.Attributes.Add("class", "Hide")
                                divView.Attributes.Add("class", "Show")
                                divConfirm.Attributes.Add("class", "Hide")
                                divResult.Attributes.Add("class", "Hide")
                                Session("dt") = dt
                            Else
                                ScriptManager.RegisterStartupScript(Me, GetType(Page), "Alert", "<script>alert('Cannot Read From Empty File');</script>", False)
                            End If
                        Else
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "Alert", "<script>alert('Please Upload a Valid File for Retail Client');</script>", False)

                        End If

                    Else
                        If dt.Columns(2).ColumnName = "Schedule Window Begin" AndAlso dt.Columns(3).ColumnName = "Schedule Window End" Then
                            dt = dt.DefaultView.ToTable(False, "Sr No", "Work Request Title", "Schedule Window Begin", "Schedule Window End", "Work Request Type", "Sub Category For Work Request Type", "Sales Invoice Title", "Scope of Work", "Contact First Name", "Contact Last Name", "Postal Code", "Address", "City", "Phone", "Equipment Details", "WP", "PP", "PO Number", "Engineer Scope of work", "Allow bid review", "Force Sign off sheet")
                            If dt.Rows.Count > 0 Then

                                dlst_beneficiaries.DataSource = dt
                                dlst_beneficiaries.DataBind()

                                divViewForRetailClient.Attributes.Add("class", "Hide")
                                divViewForITClient.Attributes.Add("class", "Show")
                                divUpload.Attributes.Add("class", "Hide")
                                divView.Attributes.Add("class", "Show")
                                divConfirm.Attributes.Add("class", "Hide")
                                divResult.Attributes.Add("class", "Hide")
                                Session("dt") = dt
                            Else
                                ScriptManager.RegisterStartupScript(Me, GetType(Page), "Alert", "<script>alert('Cannot Read From Empty File');</script>", False)
                            End If
                        Else
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "Alert", "<script>alert('Please Upload a Valid File for IT Client');</script>", False)
                        End If
                    End If



                Catch ex As Exception

                Finally
                    If File.Exists(filePath) Then
                        File.Delete(filePath)
                    End If
                End Try
            End If
        Catch ex As Exception
            CommonFunctions.createLog("Bulk WO Upload - btn_upload_excel_Click - " + ex.ToString())
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, "<br><br> Error in Bulk WO Upload - btn_upload_excel_Click")
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "Error - " + ex.ToString, "<br><br> Error in Bulk WO Upload - btn_upload_excel_Click")
        End Try
    End Sub


    Private Sub btn_confirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_confirm.Click

        Try
            dt = TryCast(Session("dt"), DataTable)

            Dim newColumn As New DataColumn("IsValid", GetType(String))
            newColumn.DefaultValue = "Y"
            dt.Columns.Add(newColumn)
            dt.Columns.Add("Reason", GetType(String))

            'Validation to check if any value is missing then make this entry as invalid
            For i As Integer = dt.Rows.Count - 1 To 0 Step -1
                For j As Integer = dt.Columns.Count - 4 To 0 Step -1
                    If IsDBNull(dt.Rows(i)(j)) Or Convert.ToString(dt.Rows(i)(j)) = "" Then
                        dt.Rows(i)("IsValid") = "N"
                        dt.Rows(i)("Reason") = "Missing Certain Fields in Excel"
                    End If
                Next
            Next

            If ViewState("Mode") = "Retail" Then
                'Get the Valid Records
                dv = dt.AsDataView()
                dv.RowFilter = "IsValid='Y'"
                DataList2.DataSource = dv
                DataList2.DataBind()
                If (dv.Count > 0) Then
                    btn_upload.Visible = True
                Else
                    btn_upload.Visible = False
                End If


                'Get the Invalid Records
                dv = dt.AsDataView()
                dv.RowFilter = "IsValid='N'"
                DataList3.DataSource = dv
                DataList3.DataBind()

                divUpload.Attributes.Add("class", "Hide")
                divView.Attributes.Add("class", "Hide")
                divConfirm.Attributes.Add("class", "Show")
                divConfirmSuccessfullRecordsForRetailClient.Attributes.Add("class", "Show")
                divConfirmInvalidRecordsForRetailClient.Attributes.Add("class", "Show")
                divConfirmSuccessfullRecordsForITClient.Attributes.Add("class", "Hide")
                divConfirmInvalidRecordsForITClient.Attributes.Add("class", "Hide")
                divResult.Attributes.Add("class", "Hide")
                Session("dt") = dt
            Else

                'Get the Valid Records only
                dv = dt.AsDataView()
                dv.RowFilter = "IsValid= 'Y'"
                dlst_bene.DataSource = dv
                dlst_bene.DataBind()
                If (dv.Count > 0) Then
                    btn_upload.Visible = True
                Else
                    btn_upload.Visible = False
                End If

                'Get the Invalid Records
                dv = dt.AsDataView()
                dv.RowFilter = "IsValid='N'"
                DataList4.DataSource = dv
                DataList4.DataBind()

                divUpload.Attributes.Add("class", "Hide")
                divView.Attributes.Add("class", "Hide")
                divConfirm.Attributes.Add("class", "Show")
                divConfirmSuccessfullRecordsForRetailClient.Attributes.Add("class", "Hide")
                divConfirmInvalidRecordsForRetailClient.Attributes.Add("class", "Hide")
                divConfirmSuccessfullRecordsForITClient.Attributes.Add("class", "Show")
                divConfirmInvalidRecordsForITClient.Attributes.Add("class", "Show")
                divResult.Attributes.Add("class", "Hide")
                Session("dt") = dt
            End If

        Catch ex As Exception
            CommonFunctions.createLog("Bulk WO Upload - btn_confirm_Click - " + ex.ToString())
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, "<br><br> Error in Bulk WO Upload - btn_confirm_Click")
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "Error - " + ex.ToString, "<br><br> Error in Bulk WO Upload - btn_confirm_Click")
        End Try
    End Sub


    Private Sub btn_upload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_upload.Click
        Try
            Dim count As Integer = 0
            Dim dt_success As New DataTable()
            Dim dt_invalid As New DataTable()
            dt = TryCast(Session("dt"), DataTable)

            dv = dt.AsDataView()
            dv.RowFilter = "IsValid='Y'"
            dt_success = dv.ToTable()

            'write a code to upload dt_success records into database
            ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted
            ViewState("Comments") = "WorkOrder Created"
            For Each row As DataRow In dt_success.Rows
                ViewState("WOID") = 0
                Dim dsWorkOrder As New WorkOrderNew
                Dim Uplift As Boolean = False
                'Updating workorder tracking table
                Dim nrowWO As WorkOrderNew.tblWorkOrderRow = dsWorkOrder.tblWorkOrder.NewRow
                With nrowWO
                    .BizDivID = 1
                    .WOID = 0
                    .WorkOrderID = "" 'Created using a formula  in the table  
                    .DateCreated = Date.Now
                    .IsAMIgnored = False

                    ViewState("WOCategoryID") = GetSubCategory(row) 'row("Sub Category For Work Request Type")

                    .WOCategoryID = ViewState("WOCategoryID")

                    .LocationId = 0

                    .AlternateContactId = hdnContactID.Value
                    .WOTitle = row("Work Request Title")
                    .WOShortDesc = ""
                    .WOLongDesc = row("Scope of Work").Replace(Chr(13), "<BR>")
                    .PONumber = row("PO Number") 'txtPONumber.Text
                    .JRSNumber = "" 'txtJRSNumber.Text
                    .SpecialInstructions = "" 'txtSpecialInstructions.Text.Replace(Chr(13), "<BR>")
                    .DressCode = "" ' ddlDressCode.SelectedValue
                    .OfficeTimings = ""
                    .WOStatus = ViewState("WOStatus")
                    .DataRef = 0
                    .LastActionRef = 0
                    .SupplierCompanyID = 0
                    .SupplierContactID = 0


                    .BillingLocID = 315882 'drpdwnBillingLocation.SelectedValue
                    ViewState("BillingLocID") = .BillingLocID

                    .IsRAQ = False

                    .IsSignOffSheetReqd = IIf(IsDBNull(row("Force Sign off sheet")), True, False)

                    .InvoiceTitle = row("Sales Invoice Title")

                    'ContactId & CompanyId
                    .CompanyId = hdnContactID.Value
                    .ContactId = 0 'Session("UserId")

                    'Buyer Accepted                
                    .BuyerAccepted = True


                    'Wholesale price & Platform Price
                    .WholesalePrice = row("WP") 'CDec(txtSpendLimitWP.Text)

                    .PlatformPrice = row("PP") 'CDec(txtSpendLimitPP.Text)


                    .SupplierAccepted = False

                    'Review Bids is associated with Platform Price only. Pltaform Price can be set by OW rep only.
                    .ReviewBids = row("Allow bid review")

                    'Show loc to Supplier
                    .ShowLocToSupplier = False


                    '************************************************************
                    'Staged Work Order Code Starts
                    .StagedWO = "No"

                    If (ViewState("Mode") = "Retail") Then
                        .EstimatedTimeInDays = row("Estimated Time Required")
                    End If
                    .WholesaleDayJobRate = 0.0 'txtWholesaleDayJobRate.Text
                    .PlatformDayJobRate = 0.0 'txtPlatformDayJobRate.Text
                    .PricingMethod = "" 'ddlPricingMethod.SelectedValue
                    'Staged Work Order Code Ends
                    '************************************************************


                    .GoodsLocation = 0

                    If (ViewState("Mode") = "Retail") Then
                        .EquipmentDetails = row("Product Purchased").Replace(Chr(13), "<BR>")
                        .AptTime = row("Appointment Time")
                    Else
                        .EquipmentDetails = row("Equipment Details").Replace(Chr(13), "<BR>")
                    End If

                    .ClientSpecialInstructions = "" 'txtClientInstru.Text.Replace(Chr(13), "<BR>")
                    .SalesAgent = "" 'txtSalesAgent.Text
                    .FreesatMake = GetCategory(row) 'row("Work Request Type")

                End With
                dsWorkOrder.tblWorkOrder.Rows.Add(nrowWO)


                Dim nrowUpSell As WorkOrderNew.tblWOUpsellRow = dsWorkOrder.tblWOUpsell.NewRow
                With nrowUpSell
                    .WOID = 0

                    .UpSellPrice = 0.0

                    '.UpSellInvoiceTitle = "" 'txtUpSellInvoiceTitle.Text

                    '.BCFirstName = Convert.ToString(row("Contact First Name"))
                    '.BCLastName = Convert.ToString(row("Contact Last Name"))
                    '.BCAddress = Convert.ToString(row("Address"))

                    '.BCCity = Convert.ToString(row("City"))

                    '.BCCounty = "" 'txtBCCounty.Text

                    '.BCPostCode = Convert.ToString(row("Postal Code"))

                    '.BCPhone = Convert.ToString(row("Phone"))


                    '.BCFax = "" 'txtBCFax.Text


                End With
                dsWorkOrder.tblWOUpsell.Rows.Add(nrowUpSell)


                'Updating the work tracking table
                Dim nrowTracking As WorkOrderNew.tblWOTrackingRow = dsWorkOrder.tblWOTracking.NewRow
                With nrowTracking
                    .BizDivID = 1
                    .WOTrackingId = 0
                    .WOID = 0
                    .DateCreated = Date.Now


                    .CompanyID = hdnContactID.Value
                    .ContactID = 0 'Session("UserId")
                    .ContactClassID = ApplicationSettings.RoleOWID

                    'WOvalue = Wholesale Price
                    .Value = 0


                    .Status = ViewState("WOStatus")


                    If ViewState("Mode") = "Retail" Then
                        .DateStart = row("Appointment Date")
                        .DateEnd = CType(CDate(row("Appointment Date")).AddDays(1), Date).ToString("dd/MM/yyyy")
                        ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail
                    Else
                        .DateStart = row("Schedule Window Begin")
                        .DateEnd = row("Schedule Window End") ' Schedules End date
                        ViewState("BusinessArea") = ApplicationSettings.BusinessAreaIT
                    End If


                    .EstimatedTime = CDate(.DateEnd).Subtract(CDate(.DateStart)).Days.ToString

                    ViewState("EstimatedTime") = CDate(.DateEnd).Subtract(CDate(.DateStart)).Days.ToString
                    '.EstimatedTime = lblEstimatedTime.Text
                    .SpecialistSuppliesParts = False
                    .Comments = ViewState("Comments")

                    If Not IsNothing(ViewState("BusinessArea")) Then
                        If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                            .ActualTime = row("Appointment Time")
                        Else
                            .ActualTime = ""
                        End If
                    Else
                        .ActualTime = ""
                    End If

                    .DateModified = Date.Now
                    .SpecialistID = 0

                    'For Tracking
                    If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                        If ViewState("Mode") = "Retail" Then
                            .EstimatedTimeInDays = row("Estimated Time Required")
                        End If
                        '.DayJobRate = txtPlatformDayJobRate.Text
                        .DayJobRate = 0.0 'txtWholesaleDayJobRate.Text
                    End If

                End With
                dsWorkOrder.tblWOTracking.Rows.Add(nrowTracking)


                'Updating the addresses table
                Dim nrowLoc As WorkOrderNew.tblContactsAddressesRow = dsWorkOrder.tblContactsAddresses.NewRow
                With nrowLoc
                    .AddressID = 0
                    .ContactID = hdnContactID.Value
                    .Type = "Temporary"

                    .Name = ""
                    .Address = row("Address")
                    .City = row("City")
                    .State = "" 'txtCounty.Text
                    .PostCode = CommonFunctions.FormatPostcode(row("Postal Code"))
                    .CountryID = ApplicationSettings.DefaultCountry
                    .Sequence = 0
                    .IsDefault = False
                    .ContactFname = row("Contact First Name")
                    .ContactLname = row("Contact Last Name")
                    .ContactPhone = row("Phone")
                    .ContactFax = "" 'txtCompanyFax.Text
                    .Mobile = "" 'txtMobile.Text
                    .IsDepot = False
                    'Poonam added on 11/1/2017 - Task -  OA-385 :OA - Add Customer email field to New Workorder booking page
                    .Email = "" 'txtCustomerEmail.Text.Trim
                End With
                dsWorkOrder.tblContactsAddresses.Rows.Add(nrowLoc)

                Dim xmlContent As String = dsWorkOrder.GetXml
                Dim xmlContentMail As String = dsWorkOrder.GetXml
                xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)

                Dim PPerRate As Decimal
                PPerRate = 0.0

                Dim ClientScope As String
                ClientScope = row("Engineer Scope of work")

                Dim IsWaitingToAutomatch As Boolean
                IsWaitingToAutomatch = False

                'Save data to DB
                Dim dsSendMail As DataSet = ws.WSWorkOrder.MS_WOAddUpdateWorkOrderDetails(xmlContent, "add", "", 0, 0, "BulkUpload", "", CommonFunctions.FetchVerNum(), Session("UserID"), False, False, 0, PPerRate, 0.0, "", ClientScope, "", IsWaitingToAutomatch, "1", "")

                Dim NoOfWO As Integer
                Dim OriginalWOID As Integer
                NoOfWO = 1


            Next
            'ScriptManager.RegisterStartupScript(Me, GetType(Page), "Alert", "<script>alert('Records Successfully Uploaded');</script>", False)
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "redirect", "alert('Records Inserted Successfully'); window.location='AdminWOListing.aspx?mode=Submitted';", True)
            'Response.Redirect("AdminWOListing.aspx", False)
        Catch ex As Exception
            CommonFunctions.createLog("Bulk WO Upload - btn_upload_Click - " + ex.ToString())
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, " <br><br> Error in Bulk WO Upload - btn_upload_Click")
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "Error - " + ex.ToString, "<br><br> Error in Bulk WO Upload - btn_upload_Click")
        End Try
    End Sub


    Private Sub btn_download_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btn_download.Click

    End Sub

    Private Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancel.Click, btn_cancel_upload.Click
        Response.Redirect("~/BulkWOUpload.aspx", False)
    End Sub

    Public Function GetCategory(ByVal row As DataRow)
        Try


            Dim category As String = row("Work Request Type")
            Dim subCategory As String = ""

            Select Case category.Trim

                Case "FreesatandDigitalAerials"
                    Return 148

                Case "ConsumerElectronicandTV"
                    Return 153

                Case "HomePCInstallations"
                    Return 156

                Case "HomeAutomation"
                    Return 244

                Case "EPOS"
                    Return 228

                Case "Desktop"
                    Return 229

                Case "Tablets"
                    Return 230

                Case "NetworkInfrastructure"
                    Return 231

                Case "Serverhardware"
                    Return 232

                Case "ServerSoftware"
                    Return 233

                Case "ServerStructuredCabling"
                    Return 234

                Case "Telephony"
                    Return 235

                Case "ITServiceManagement"
                    Return 236

            End Select
        Catch ex As Exception
            CommonFunctions.createLog("Bulk WO Upload - GetCategory - " + ex.ToString())
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, " <br><br> Error in Bulk WO Upload - GetCategory")
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "Error - " + ex.ToString, " <br><br> Error in Bulk WO Upload - GetCategory")

        End Try
    End Function

    Public Function GetSubCategory(ByVal row As DataRow) As Integer
        Try
            Dim subCategory As String = row("Sub Category For Work Request Type")

            Select Case subCategory.Trim

                Case "Satellite"
                    Return 146

                Case "Digital Aerial"
                    Return 147


                Case "TV Wall Mount"
                    Return 149

                Case "Embedded Cabling"
                    Return 150

                Case "Internet TV Setup"
                    Return 151

                Case "Standard TV Set-Up"
                    Return 152


                Case "Windows"
                    Return 154
                Case "Apple Mac"
                    Return 155


                Case "Smart Lock"
                    Return 238
                Case "Thermo"
                    Return 239
                Case "Smart Starter Kits"
                    Return 240
                Case "Energy monitor"
                    Return 241
                Case "Alarm"
                    Return 242
                Case "CCTV"
                    Return 243


                Case "IBM"
                    If row("work request type") = "EPOS" Then
                        Return 169
                    ElseIf row("work request type") = "Desktop" Then
                        Return 166
                    ElseIf row("work request type") = "NetworkInfrastructure" Then
                        Return 184
                    Else
                        Return 196
                    End If


                Case "Casio"
                    Return 170
                Case "Chip and PIN"
                    Return 171
                Case "Fujitsu"
                    Return 172
                Case "NEC"
                    Return 173
                Case "NCR"
                    Return 174
                Case "Sharp"
                    Return 175
                Case "Wincor Nixdorf"
                    Return 176


                Case "Apple"
                    Return 163

                Case "Dell"
                    If row("work request type") = "Desktop" Then
                        Return 164
                    Else
                        Return 193
                    End If


                Case "HP"
                    If row("work request type") = "Desktop" Then
                        Return 165
                    ElseIf row("work request type") = "NetworkInfrastructure" Then
                        Return 183
                    Else
                        Return 195
                    End If


                Case "Printer"
                    Return 167
                Case "Microsoft"
                    If row("work request type") = "Desktop" Then
                        Return 168
                    Else
                        Return 203
                    End If



                Case "Androids"
                    Return 177
                Case "iPad"
                    Return 178

                Case "3Com"
                    Return 179
                Case "Avaya"
                    If row("work request type") = "NetworkInfrastructure" Then
                        Return 180
                    Else
                        Return 216
                    End If


                Case "Check Point"
                    Return 181
                Case "Cisco"
                    If row("work request type") = "NetworkInfrastructure" Then
                        Return 182
                    Else
                        Return 217
                    End If



                Case "Juniper"
                    Return 185
                Case "Linksys"
                    Return 186
                Case "NetApp"
                    Return 187
                Case "Netgear"
                    Return 188
                Case "Netscreen"
                    Return 189
                Case "Sophos"
                    Return 190
                Case "Symantec"
                    Return 191
                Case "Zyxel"
                    Return 192



                Case "EMC"
                    Return 194
                Case "Sun"
                    Return 197

                Case "Apache"
                    Return 198
                Case "Blackberry"
                    Return 199
                Case "Citrix"
                    Return 200
                Case "Exchange"
                    Return 201
                Case "Linux"
                    Return 202


                Case "MS SQL"
                    Return 204
                Case "MySQL"
                    Return 205
                Case "Oracle"
                    Return 206
                Case "Share Point"
                    Return 237
                Case "Unix"
                    Return 207
                Case "VMware"
                    Return 208


                Case "Cat5"
                    Return 209
                Case "Cat5e"
                    Return 210
                Case "Cat6"
                    Return 211
                Case "Cat7"
                    Return 212
                Case "Optical Fibre"
                    Return 213
                Case "Telephony Cabling"
                    Return 214

                Case "Alcatel"
                    Return 215



                Case "Meridian"
                    Return 218
                Case "Mitel"
                    Return 219
                Case "Nortel"
                    Return 220

                Case "Agile"
                    Return 221
                Case "APM"
                    Return 222
                Case "ITIL"
                    Return 223
                Case "PMI"
                    Return 224
                Case "PRINCE2"
                    Return 225
                Case "Six Sigma"
                    Return 226
                Case "TOGAF"
                    Return 227

            End Select
        Catch ex As Exception
            CommonFunctions.createLog("Bulk WO Upload - GetSubCategory - " + ex.ToString())
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, " <br><br> Error in Bulk WO Upload - GetSubCategory")
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact2, "Error - " + ex.ToString, " <br><br> Error in Bulk WO Upload - GetSubCategory")
        End Try
    End Function

    Private Function GetValue(doc As SpreadsheetDocument, cell As Cell) As String
        Try
            Dim value As String = ""
            value = cell.CellValue.Text
            If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = CellValues.SharedString Then
                Return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(Integer.Parse(value)).InnerText
            End If
            Return value
        Catch ex As Exception
            Return ""
        End Try
    End Function



End Class
