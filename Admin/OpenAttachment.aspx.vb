Imports System
Imports System.IO
Imports System.Data
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.Security
Imports System.Security.Principal
Imports System.Runtime.InteropServices
Partial Public Class OpenAttachment
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim inp As IO.FileStream
        Try
            Dim context As WindowsImpersonationContext
            Try
                '    context = Impersonation.StartImpersonatingUser(System.Configuration.ConfigurationSettings.AppSettings("AdminUser"), System.Configuration.ConfigurationSettings.AppSettings("AdminDomain"), System.Configuration.ConfigurationSettings.AppSettings("AdminPassword"))
            Catch
            End Try

            Dim FilePath As String = Request.QueryString("path")
            Dim info As IO.FileInfo = New IO.FileInfo(FilePath)
            If Not info.Exists Then
                Exit Sub
            End If

            Dim b() As Byte = Array.CreateInstance(GetType(Byte), info.Length)
            inp = info.OpenRead()
            inp.Read(b, 0, info.Length)
            Response.Buffer = False
            Dim ext As String = FilePath.Split(".")(FilePath.Split(".").Length - 1)

            Dim tempFilePath As String = FilePath.Replace("\", "/")
            Dim attachFilename As String = tempFilePath.Split("/")(tempFilePath.Split("/").Length - 1)

            If LCase(ext) = "pdf" Then
                Response.Expires = 0
                Response.ContentType = "application/pdf"
            ElseIf LCase(ext) = "xls" Then
                Response.Expires = 0
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("content-disposition", "attachment; filename=Temp.xls")
            ElseIf LCase(ext) = "doc" Then
                Response.ContentType = "application/vnd.ms-word"
                Response.AddHeader("content-disposition", "attachment; filename=Temp.doc")
            ElseIf LCase(ext) = "bmp" Then
                Response.ContentType = "image/bmp"
                Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
            ElseIf LCase(ext) = "gif" Then
                Response.ContentType = "image/gif"
                Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
            ElseIf LCase(ext) = "jpe" Or LCase(ext) = "jpeg" Or LCase(ext) = "jpg" Then
                Response.ContentType = "image/jpeg"
                Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
            ElseIf LCase(ext) = "tif" Or LCase(ext) = "tiff" Then
                Response.ContentType = "image/tiff"
                Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
            ElseIf LCase(ext) = "png" Then
                Response.ContentType = "image/png"
                Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
            ElseIf LCase(ext) = "jfif" Then
                Response.ContentType = "image/pjpeg"
                Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
            Else
                Response.ContentType = "application/x-unknown"
                Response.AddHeader("content-disposition", "attachment; filename=" & attachFilename)
            End If
            Response.Charset = ""
            Response.BinaryWrite(b)
            Response.Flush()

            Try
                '    Impersonation.StopImpersonatingUser(context)
            Catch
            End Try

            Response.End()
        Catch ex As Exception
            Try
                inp.Close()
            Catch
            End Try
            Throw ex
        End Try
    End Sub

End Class