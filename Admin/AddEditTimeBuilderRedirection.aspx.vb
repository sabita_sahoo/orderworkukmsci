﻿Public Class AddEditTimeBuilderRedirection
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            Session("TimeBuilderRedirection") = Nothing
            If Not IsNothing(Request.QueryString("ProductID")) Then
                ViewState.Add("ProductID", Request.QueryString("ProductID"))
                ViewState.Add("CompanyId", Request.QueryString("CompanyId"))
                PopulateData()
            End If
            lnkBackToService.HRef = getBackToServiceLink()
        End If
    End Sub
    Public Function getBackToServiceLink() As String
        Dim link As String = ""
        link = "~\AdminProduct.aspx?mode=edit&productid=" & ViewState("ProductID") & "&pagename=CompanyProfile&CompanyID=" & ViewState("CompanyId") & "&bizDivId=1"
        Return link
    End Function
    Public Sub PopulateData()
        divValidationTBRedirection.Visible = False
        lblValidationTBRedirection.Text = ""
        Dim ds As New DataSet
        ds = ws.WSWorkOrder.populateWOFormProductDetails(CInt(ViewState("ProductID")), CInt(ViewState("CompanyId")), 1, CInt(HttpContext.Current.Session("UserID")), "TimeBuilderRedirection", "")
        If ds.Tables.Count > 0 Then
            Session("TimeBuilderRedirection") = ds
            If ds.Tables(0).Rows.Count > 0 Then
                PopulateTimeBuilderRedirection()
            End If
        End If
    End Sub
    Protected Sub PopulateTimeBuilderRedirection()
        Dim dsTimeBuilderRedirection As DataSet
        dsTimeBuilderRedirection = CType(Session("TimeBuilderRedirection"), DataSet)
        If dsTimeBuilderRedirection.Tables.Count > 0 Then
            If dsTimeBuilderRedirection.Tables(0).Rows.Count > 0 Then
                Dim dvTimeBuilderRedirection As New DataView
                dvTimeBuilderRedirection = dsTimeBuilderRedirection.Tables(0).DefaultView
                rptTimeBuilderRedirection.Visible = True
                rptTimeBuilderRedirection.DataSource = dvTimeBuilderRedirection.ToTable.DefaultView
                rptTimeBuilderRedirection.DataBind()
            Else
                rptTimeBuilderRedirection.Visible = False
                rptTimeBuilderRedirection.DataSource = Nothing
                rptTimeBuilderRedirection.DataBind()
            End If
        End If
    End Sub
    Public Sub populateProductListDD(ByVal drpdwnProduct As DropDownList, ByVal SelectedProduct As Integer)
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetProductListing(ViewState("CompanyId"), 1, "Sequence", 0, 0, 0, False)

        If ds.Tables(0).Rows.Count > 0 Then
            drpdwnProduct.Visible = True
            Dim liProducts As New ListItem
            liProducts = New ListItem
            liProducts.Text = "Select Service"
            liProducts.Value = 0
            drpdwnProduct.DataSource = ds.Tables(0)
            drpdwnProduct.DataTextField = "ProductName"
            drpdwnProduct.DataValueField = "ProductID"
            drpdwnProduct.DataBind()
            drpdwnProduct.Items.Insert(0, liProducts)

            If (ViewState("ProductID") <> 0) Then
                Dim liProductsToRemove As New ListItem
                liProductsToRemove = New ListItem
                liProductsToRemove.Value = ViewState("ProductID")
                drpdwnProduct.Items.Remove(drpdwnProduct.Items.FindByValue(ViewState("ProductID")))
            End If
        Else
            drpdwnProduct.Visible = False
        End If

        Dim vListItem As ListItem = drpdwnProduct.Items.FindByValue(SelectedProduct)

        If Not vListItem Is Nothing Then
            drpdwnProduct.SelectedValue = SelectedProduct
        End If

    End Sub
    Private Sub rptTimeBuilderRedirection_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTimeBuilderRedirection.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim hdnSelectedProductId As HtmlInputHidden = CType(e.Item.FindControl("hdnSelectedProductId"), HtmlInputHidden)
            Dim drpdwnProduct As DropDownList = CType(e.Item.FindControl("drpdwnProduct"), DropDownList)
            populateProductListDD(drpdwnProduct, hdnSelectedProductId.Value)
        End If
    End Sub
    Private Sub rptTimeBuilderRedirection_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTimeBuilderRedirection.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "RemoveRedirection") Then
                Dim hdnRedirectionId As HtmlInputHidden = CType(e.Item.FindControl("hdnRedirectionId"), HtmlInputHidden)

                UpdateTimeBuilderRedirection()

                RemoveRedirection(hdnRedirectionId.Value.ToString)

            End If
        End If
    End Sub
    Public Sub RemoveRedirection(ByVal RedirectionId As String)
        Dim ds As New DataSet
        ds = CType(Session("TimeBuilderRedirection"), DataSet)

        Dim i As Integer
        i = 0
        For Each drow As DataRow In ds.Tables(0).Rows
            If drow.Item("RedirectionId") <> RedirectionId Then
                i = i + 1
                drow.Item("RowNum") = i
            End If
        Next
        ds.Tables(0).AcceptChanges()
        Session("TimeBuilderRedirection") = ds

        Dim dtRedirection As DataTable = ds.Tables(0)
        Dim dvRedirection As New DataView
        dvRedirection = dtRedirection.Copy.DefaultView

        'add primary key as RedirectionId 
        Dim keysRedirection(1) As DataColumn
        keysRedirection(0) = dtRedirection.Columns("RedirectionId")

        Dim foundrowRedirection As DataRow() = dtRedirection.Select("RedirectionId = '" & RedirectionId & "'")
        If Not (foundrowRedirection Is Nothing) Then
            dtRedirection.Rows.Remove(foundrowRedirection(0))
            ds.Tables(0).AcceptChanges()
            Session("TimeBuilderRedirection") = ds
        End If

        PopulateTimeBuilderRedirection()
    End Sub
    Public Sub UpdateTimeBuilderRedirection()

        divValidationTBRedirection.Visible = False
        lblValidationTBRedirection.Text = ""

        Dim hdnRedirectionId As HtmlInputHidden

        Dim ds As New DataSet
        ds = CType(Session("TimeBuilderRedirection"), DataSet)

        Dim dvRedirection As New DataView
        dvRedirection = ds.Tables(0).DefaultView

        If Not IsNothing(rptTimeBuilderRedirection) Then
            For Each row As RepeaterItem In rptTimeBuilderRedirection.Items
                hdnRedirectionId = CType(row.FindControl("hdnRedirectionId"), HtmlInputHidden)

                dvRedirection.RowFilter = "RedirectionId = " & hdnRedirectionId.Value

                If dvRedirection.Count > 0 Then

                    If (CType(row.FindControl("txtTimeSlotFrom"), TextBox).Text <> "") Then
                        If (IsNumeric(CType(row.FindControl("txtTimeSlotFrom"), TextBox).Text.Trim)) Then
                            dvRedirection.Item(0).Item("TimeSlotFrom") = CType(row.FindControl("txtTimeSlotFrom"), TextBox).Text
                        End If
                    Else
                        dvRedirection.Item(0).Item("TimeSlotFrom") = 0
                    End If
                    If (CType(row.FindControl("txtTimeSlotTo"), TextBox).Text <> "") Then
                        If (IsNumeric(CType(row.FindControl("txtTimeSlotTo"), TextBox).Text.Trim)) Then
                            dvRedirection.Item(0).Item("TimeSlotTo") = CType(row.FindControl("txtTimeSlotTo"), TextBox).Text
                        End If
                    Else
                        dvRedirection.Item(0).Item("TimeSlotTo") = 0
                    End If
                    dvRedirection.Item(0).Item("ProductID") = CType(row.FindControl("drpdwnProduct"), DropDownList).SelectedValue

                End If

                dvRedirection.RowFilter = ""
                ds.Tables(0).AcceptChanges()
                Session("TimeBuilderRedirection") = ds
            Next
        End If
    End Sub
    Private Sub lnkAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddNew.Click
        AddQuestion()
    End Sub

    Public Sub AddQuestion()

        UpdateTimeBuilderRedirection()

        Dim ds As New DataSet
        ds = CType(Session("TimeBuilderRedirection"), DataSet)

        Dim MaxRedirectionId As Integer
        Dim RowNum As Integer
        RowNum = 1

        For Each drowRedirection As DataRow In ds.Tables(0).Rows
            MaxRedirectionId = drowRedirection.Item("MaxRedirectionId") + 1
            drowRedirection.Item("MaxRedirectionId") = MaxRedirectionId
        Next

        ds.AcceptChanges()
        Session("TimeBuilderRedirection") = ds


        Dim dv As New DataView
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                dv = ds.Tables(0).DefaultView
                RowNum = RowNum + dv.ToTable.Rows.Count
            End If
        End If


        Dim dt As New DataTable

        dt = ds.Tables(0)

        Dim drow As DataRow
        drow = dt.NewRow()

        drow("RedirectionId") = MaxRedirectionId
        drow("TimeSlotFrom") = 0
        drow("TimeSlotTo") = 0
        drow("ProductID") = 0
        drow("MaxRedirectionId") = MaxRedirectionId
        drow("RowNum") = RowNum

        dt.Rows.Add(drow)
        dt.AcceptChanges()

        ds.AcceptChanges()

        Session("TimeBuilderRedirection") = ds

        ds.AcceptChanges()
        Session("TimeBuilderRedirection") = ds

        PopulateTimeBuilderRedirection()

    End Sub
    Protected Sub ResetTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetTop.Click, btnResetBottom.Click
        PopulateData()
    End Sub
    Protected Sub Save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click, btnSaveBottom.Click
        UpdateTimeBuilderRedirection()
        If ValidatingRedirectionData() = "" Then
            Dim dsTBQ As New XSDTBQ
            Dim hdnRedirectionId As HtmlInputHidden

            Dim RedirectionId As Integer
            RedirectionId = 0

            Dim ds As New DataSet
            ds = CType(Session("TimeBuilderRedirection"), DataSet)

            Dim dvRedirection As New DataView
            dvRedirection = ds.Tables(0).DefaultView

            If Not IsNothing(rptTimeBuilderRedirection) Then
                For Each row As RepeaterItem In rptTimeBuilderRedirection.Items
                    hdnRedirectionId = CType(row.FindControl("hdnRedirectionId"), HtmlInputHidden)

                    RedirectionId = RedirectionId + 1

                    dvRedirection.RowFilter = "RedirectionId = " & hdnRedirectionId.Value

                    If dvRedirection.Count > 0 Then
                        Dim nrow As XSDTBQ.TimeBuilderRedirectionRow = dsTBQ.TimeBuilderRedirection.NewRow
                        With nrow
                            .RedirectionId = RedirectionId
                            .TimeSlotFrom = CType(row.FindControl("txtTimeSlotFrom"), TextBox).Text.Trim
                            .TimeSlotTo = CType(row.FindControl("txtTimeSlotTo"), TextBox).Text.Trim
                            .ProductId = CType(row.FindControl("drpdwnProduct"), DropDownList).SelectedValue
                            .CompanyID = CInt(ViewState("CompanyId"))
                            .BaseProductID = CInt(ViewState("ProductID"))
                        End With
                        dsTBQ.TimeBuilderRedirection.Rows.Add(nrow)
                    End If
                    dvRedirection.RowFilter = ""
                Next
            End If

            Dim xmlContent As String = dsTBQ.GetXml
            xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)

            Dim dsSuccess As New DataSet
            dsSuccess = ws.WSContact.AddEditTimeBuilderQA(xmlContent, RedirectionId, "TimeBuilderRedirection", CInt(ViewState("CompanyId")), CInt(ViewState("ProductID")), Session("UserID"))
            If dsSuccess.Tables.Count > 0 Then
                If dsSuccess.Tables(0).Rows.Count > 0 Then
                    If CStr(dsSuccess.Tables(0).Rows(0)(0)) = "1" Then
                        Response.Redirect("AdminProduct.aspx?mode=edit&productid=" & ViewState("ProductID") & "&pagename=CompanyProfile&CompanyID=" & Request("companyid") & "&bizDivId=1")
                    End If
                End If
            End If
        End If
    End Sub
    Private Function ValidatingRedirectionData() As String

        
        Dim flag As Boolean = True
        Dim errmsg As String = ""
        Dim i As Integer = 0
        Dim j As Integer = 0

        If Not IsNothing(rptTimeBuilderRedirection) Then
            For Each row As RepeaterItem In rptTimeBuilderRedirection.Items
                i = i + 1
                j = 0
                'Validating TimeSlotFrom is not blank
                If CType(row.FindControl("txtTimeSlotFrom"), TextBox).Text.Trim.Length = 0 Then
                    If flag = True Then
                        errmsg = "- Please Enter TimeSlot From for Redirection " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Enter TimeSlot From for Redirection " & i
                    End If
                ElseIf (Not IsNumeric(CType(row.FindControl("txtTimeSlotFrom"), TextBox).Text.Trim)) Then
                    If flag = True Then
                        errmsg = "- Please Enter Numeric Value in TimeSlot From for Redirection " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Enter Numeric Value in TimeSlot From for Redirection " & i
                    End If
                End If
                If CType(row.FindControl("txtTimeSlotTo"), TextBox).Text.Trim.Length = 0 Then
                    If flag = True Then
                        errmsg = "- Please Enter TimeSlot To for Redirection " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Enter TimeSlot To for Redirection " & i
                    End If
                ElseIf (Not IsNumeric(CType(row.FindControl("txtTimeSlotTo"), TextBox).Text.Trim)) Then
                    If flag = True Then
                        errmsg = "- Please Enter Numeric Value in TimeSlot To for Redirection " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Enter Numeric Value in TimeSlot To for Redirection " & i
                    End If
                End If
                If CType(row.FindControl("drpdwnProduct"), DropDownList).SelectedValue = 0 Then
                    If flag = True Then
                        errmsg = "- Please Select Service for Redirection " & i
                        flag = False
                    Else
                        errmsg = errmsg & "<br>- Please Select Service for Redirection -  " & i
                    End If
                End If
            Next
        End If

        If Not flag Then
            divValidationTBRedirection.Visible = True
            lblValidationTBRedirection.Text = errmsg.ToString
        End If
        Return errmsg
    End Function

End Class