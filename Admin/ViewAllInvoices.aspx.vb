﻿Partial Public Class ViewAllInvoices
    Inherits System.Web.UI.Page
    Protected WithEvents UCDateRange1 As UCDateRange
    Protected WithEvents ImgBtnPay As System.Web.UI.WebControls.ImageButton
    Protected WithEvents PnlListing, PnlConfirm, PnlSuccess As System.Web.UI.WebControls.Panel
    Protected WithEvents hdnButtonCN As System.Web.UI.WebControls.Button
    Protected WithEvents txtCNDate As System.Web.UI.WebControls.TextBox
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)

            Dim currDate As DateTime = System.DateTime.Today
            UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            ViewState("fromDate") = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
            ViewState("toDate") = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
            ViewState!SortExpression = "InvoiceDate"
            gvInvoices.Sort(ViewState!SortExpression, SortDirection.Descending)
            'btnExport.HRef = "ExportToExcel.aspx?page=UpSellSalesInvoicePaid&bizDivId=" & Session("BizDivId") & "&status=Paid" & "&fromDate=" & ViewState("fromDate") & "&toDate=" & ViewState("toDate") & "&sortExpression=" & gvInvoices.SortExpression
            lblHeading.Text = "All Invoices"
            If Not IsNothing(Request("CompanyID")) Then
                ViewState("CompanyID") = CInt(Request("CompanyID"))
            Else
                ViewState("CompanyID") = 0
            End If
            lnkBackToListing.HRef = getBackToListingLink()

        End If
    End Sub
    Public Function getBackToListingLink() As String
        Dim link As String = ""
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        If ((Request("sender") = "buyer") Or (Request("classid") = ApplicationSettings.RoleClientID)) Then
            link = "~\CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        Else
            link = "~\CompanyProfile.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        End If
        Return link
    End Function
    Public Sub PopulateGrid()
        gvInvoices.DataBind()

        btnExport.HRef = "ExportToExcel.aspx?page=ViewAllInvoices&BizDivId=" & Session("BizDivId") & "&CompanyID=" & ViewState("CompanyID") & "&FromDate=" & ViewState("fromDate") & "&ToDate=" & ViewState("toDate") & "&sortExpression=InvoiceDate&startRowIndex=0"

    End Sub
    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then
            ViewState("fromDate") = UCDateRange1.txtFromDate.Text
            ViewState("toDate") = UCDateRange1.txtToDate.Text
            If ViewState("CompanyID") <> -1 Then
                lblMsg.Text = ""
                PopulateGrid()
            Else
                lblMsg.Text = "* Please select a contact to continue"
            End If

        End If
    End Sub


    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvInvoices.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvInvoices.DataBind()
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                       ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        Dim bizDivId As Integer = Session("BizDivId")
        If Not IsNothing(ViewState("CompanyID")) Then
            If ViewState("CompanyID") <> -1 Then
                ds = ws.WSFinance.MS_GetAllInvoices(ViewState("CompanyID"), bizDivId, ViewState("fromDate"), ViewState("toDate"), sortExpression, startRowIndex, maximumRows)
                ViewState("rowCount") = ds.Tables(1).Rows(0)(0)
            Else
                ViewState("rowCount") = 0
            End If
        Else
            ViewState("rowCount") = 0
        End If


        btnExport.HRef = "ExportToExcel.aspx?page=ViewAllInvoices&BizDivId=" & Session("BizDivId") & "&CompanyID=" & ViewState("CompanyID") & "&FromDate=" & ViewState("fromDate") & "&ToDate=" & ViewState("toDate") & "&sortExpression=InvoiceDate&startRowIndex=0"

        Return ds
    End Function

    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvInvoices.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvInvoices, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)

        If pageCount > 0 Then
            tdExport.Visible = True
        Else
            tdExport.Visible = False
        End If

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex



    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvInvoices.RowDataBound
        MakeGridViewHeaderClickable(gvInvoices, e.Row)
    End Sub

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
    Public Function GetLinks(ByVal InvoiceNo As String, ByVal CompanyID As Integer, ByVal WorkOrderId As String, ByVal InvoiceType As String, ByVal AllocatedTo As String, ByVal WOID As String, ByVal InvoiceStatus As String) As String
        Dim lnk As String
        lnk = ""
        If InvoiceType <> "" Then
            Select Case InvoiceType
                Case "Purchase"
                    'View Invoice: will open viewPurchaseInvoices.aspx in new window and Invoice No as a query string
                    lnk = lnk & "<a href=ViewPurchaseInvoice.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&WOID=" & WOID & "&bizdivid=1 runat='server' target='_blank' id='ViewInvoice'><img src='Images/Icons/View-Purchase-Invoice.gif' alt='View Purchase Invoice' hspace='2' vspace='0' border='0'></a>"
                    'View Payment: (if Invoice Status = Paid) viewCashpayment.aspx and Invoice No as a query string.
                    If InvoiceStatus = "Paid" Or InvoiceStatus = "Processed" Then
                        lnk = lnk & "<a href=ViewCashPayment.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&PaymentNo=" & AllocatedTo & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/View-Cash-Payment.gif' alt='View Payment' hspace='2' vspace='0' border='0'></a>"
                    End If
                    'View Work Order: redirect to AdminWODetails with woid and CompanyID in query string.
                    lnk = lnk & "<a href=AdminWODetails.aspx?woid=" & WOID & "&companyId=" & CompanyID & "&sender=AllInvoices&PS=" & gvInvoices.PageSize & "&PN=" & gvInvoices.PageIndex & "&SC=" & gvInvoices.SortExpression & "&SO=" & gvInvoices.SortDirection & "&Group=Closed " & "&bizdivid=1 runat='server' id='View WO'><img src='Images/Icons/ViewPIWODetails.gif' alt='View Work Orders' hspace='2' vspace='0' width='15' height='14' border='0'></a>"
                Case "Sales"
                    'View Invoice: will open viewSalesInvoices.aspx in new window and Invoice No as a query string
                    lnk = lnk & "<a href=ViewSalesInvoice.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Sales Invoice' hspace='2' vspace='0' border='0'></a>"
                    'View Payment: (if Invoice Status = Paid) ViewCashReceiptsCreditNotes.aspx and Invoice No as a query string.
                    If InvoiceStatus = "Paid" Then
                        lnk = lnk & "<a href=ViewCashReceiptsCreditNotes.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&PaymentNo=" & InvoiceNo & "&sender=AllInvoices&PS=" & gvInvoices.PageSize & "&PN=" & gvInvoices.PageIndex & "&SC=" & gvInvoices.SortExpression & "&SO=" & gvInvoices.SortDirection & "&bizdivid=1 runat='server' id='ViewInvoice'><img src='Images/Icons/View-Cash-Receipt.gif' alt='View Cash Receipts / Credit Notes' hspace='2' vspace='0' width='22' height='12' border='0'></a>"
                    End If
                    'View Related Work Orders: redirect to SalesInvoiceWOListings with InvoiceNo and CompanyID in query string.
                    lnk = lnk & "<a href=SalesInvoiceWOListing.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&PaymentNo=" & InvoiceNo & "&sender=AllInvoices&PS=" & gvInvoices.PageSize & "&PN=" & gvInvoices.PageIndex & "&SC=" & gvInvoices.SortExpression & "&SO=" & gvInvoices.SortDirection & "&bizdivid=1 runat='server' id='ViewRelatedWO'><img src='Images/Icons/View-All-WO-SI.gif' alt='View related Work Orders' hspace='2' vspace='0' width='15' height='14' border='0'></a>"
                    'Raise Credit Note: Redirect to RefundBuyer.aspx sending Sales Invoice Number in query string
                    'if user is not a representative then only he can check this option else hide it
                    If Session("RoleGroupID") <> ApplicationSettings.RoleOWRepID Then
                        lnk = lnk & "<a href=RefundBuyer.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&sender=AllInvoices&PS=" & gvInvoices.PageSize & "&PN=" & gvInvoices.PageIndex & "&SC=" & gvInvoices.SortExpression & "&SO=" & gvInvoices.SortDirection & " runat='server' id='RaiseCreditNote'><img src='Images/Icons/RaiseCreditNote.gif' alt='Raise Credit Notes' hspace='2' vspace='0' width='16' height='14' border='0'></a>"
                    End If
                Case "Credit Note"
                    'View Invoice: will open ViewCreditNote.aspx and InvoiceNo as query string
                    lnk = lnk & "<a href=ViewCreditNote.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/ViewCreditNote.gif' alt='View Credit Notes' hspace='2' vspace='0' border='0'></a>"
                    If InvoiceStatus = "Paid" Then
                        'View Invoice Allocation: If InvoiceStatus = Paid then redirect to ViewCashPayment.aspx and InvoiceNo in Query String
                        lnk = lnk & "<a href=ViewCashReceipt.aspx?invoiceNo=" & AllocatedTo & "&ReceiptType=Receipt&bizdivid=1 runat='server' id='ViewCNAlloc' target='_blank'><img src='Images/Icons/ViewInvoiceAllocation.gif' alt='View Invoice Allocation' hspace='2' vspace='0' height='14' width='12' border='0'></a>"
                    End If
                Case "Debit Note"
                    'View Invoice: will open ViewDebitNote.aspx and InvoiceNo as query string
                    lnk = lnk & "<a href=ViewDebitNote.aspx?invoiceNo=" & AllocatedTo & "&companyId=" & CompanyID & "&PaymentNo=" & InvoiceNo & "&bizDivId=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/ViewDebitNote.gif' alt='View Debit Notes' hspace='2' vspace='0' border='0'></a>"
                    ' ViewInvoiceAllocation: Redirect to ViewPurchaseInvoice.aspx and Allocatedto in query string as InvoiceNo
                    lnk = lnk & "<a href=ViewPurchaseInvoice.aspx?invoiceNo=" & AllocatedTo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewDNAlloc' target='_blank'><img src='Images/Icons/View-Purchase-Invoice.gif' alt='View Purchase Invoice' hspace='2' vspace='0' border='0'></a>"
                Case "Payment"
                    'View Invoice: will open ViewCashPayment.aspx and InvoiceNo as query string
                    lnk = lnk & "<a href=ViewCashPayment.aspx?invoiceNo=" & WOID & "&companyId=" & CompanyID & "&PaymentNo=" & InvoiceNo & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/View-Cash-Payment.gif' alt='View Payment' hspace='2' vspace='0' border='0'></a>"
                Case "Receipt"
                    'View Receipt
                    lnk = lnk & "<a href=ViewCashReceipt.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Cash Receipt' hspace='2' vspace='0' border='0'></a>"
                Case "UpSellSales"
                    'View Upsell Sales Invoice: will open ViewUpSellSales.aspx and InvoiceNo as query string
                    lnk = lnk & "<a href=ViewUpSellSalesInvoice.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Upsell Sales Invoice' hspace='2' vspace='0' border='0'></a>"
                    'View Invoice Allocation: If InvoiceStatus = Paid then redirect to ViewCashPayment.aspx and InvoiceNo in Query String
                    lnk = lnk & "<a href=ViewUpSellCashReceipt.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewCNAlloc' target='_blank'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Upsell Cash Receipt' hspace='2' vspace='0' height='14' width='12' border='0'></a>"
                Case Else
                    'Unallocated Receipts cannot be viewed in details
                    lnk = ""
            End Select
        End If
        Return lnk
    End Function
End Class