<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Portal Price" CodeBehind="WOSetPlatformPrice.aspx.vb" Inherits="Admin.WOSetPlatformPrice" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server">
			<ContentTemplate>
            <div id="divValidationMain" class="divValidation" runat="server" visible="false" >
            <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
              <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
	            <tr>
	              <td height="15" align="center">&nbsp;</td>
	              <td width="565" height="15">&nbsp;</td>
	              <td height="15">&nbsp;</td>
	            </tr>
	            <tr valign="middle">
	              <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
	              <td width="565" class="validationText"><div  id="divValidationMsg"></div>
	              <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
	              <span class="validationText">
	              <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
  	              </span>
	              </td>
	              <td width="20">&nbsp;</td>
	            </tr>
	            <tr>
	              <td height="15" align="center">&nbsp;</td>
	              <td width="565" height="15">&nbsp;</td>
	              <td height="15">&nbsp;</td>
	            </tr>
              </table>
              
              <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
            </div>
            <table width="940"  border="0" align="center" cellpadding="0" cellspacing="0">
			     <tr>
                    <td height="35" class="HeadingRed"><strong> &nbsp;Portal Price</strong></td>
                  </tr>
            </table>      
            <asp:Panel ID="pnlSetPrice" runat="server" >
			    <table width="940"  border="0" align="center" cellpadding="0" cellspacing="0">
			      <tr>
                    <td>
				    <table id="DGWorkOrders" width="100%" border="0" cellpadding="5" cellspacing="0">
				      <tr valign="top" >
					    <td  class="gridHdr gridBorderRB">WO No</td>
					    <td  class="gridHdr gridBorderRB">Submitted</td>
					    <td  class="gridHdr gridBorderRB">Location</td>
					    <td  class="gridHdr gridBorderRB">Client Company</td>
					    <td  class="gridHdr gridBorderRB">Client Contact</td>
					    <td  class="gridHdr gridBorderRB">WO Title</td>
					    <td  class="gridHdr gridBorderRB">WO Start</td>
					    <td  class="gridHdr gridBorderRB">WO End</td>
					    <td  class="gridHdrHighlight gridBorderRB">Status</td>
				      </tr>
				     <tr>
					    <td width="65px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label></td>
					    <td width="70px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblSubmitted"></asp:Label></td>
					    <td width="65px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblLocation"></asp:Label></td>
					    <td width="90px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblBuyerCompany"></asp:Label></td>
					    <td width="90px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblBuyerContact"></asp:Label></td>
					    <td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblTitle"></asp:Label></td>
					    <td width="60px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOStart"></asp:Label></td>
					    <td width="90px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOEnd"></asp:Label></td>
					    <td width="57px" class="gridRowHighlight gridBorderRB"><asp:Label runat="server" CssClass="gridRowHighlight" ID="lblStatus"></asp:Label></td>
				      </tr>
				    </table>
				    </td>
                  </tr>
                  <tr>
                    <td class="txtWelcome" height="24" valign="bottom">
					    The present Wholesale Price for the Work Request is: &pound; <asp:Label ID="lblWholesalePrice" runat="server" CssClass="formLabelGrey"></asp:Label>
				    </td>
                  </tr>
                  <tr>
                    <td class="txtWelcome" height="24" valign="bottom">
					    The present Portal Price for the Work Request is: &pound; <asp:Label ID="lblPlatformPrice" runat="server" CssClass="formLabelGrey"></asp:Label>
				    </td>
                  </tr>
                  <tr>
                    <td class="txtWelcome" height="35" valign="bottom">
					 <asp:CustomValidator ID="CustPlatformPriceValidator" runat="server" 
                     															    ControlToValidate="txtSpendLimitPP" OnServerValidate="ServerValidatePP" ErrorMessage="Please enter a positive number for Portal Price">*</asp:CustomValidator>
                     <asp:CustomValidator ID="CustPPPerRateValidator" runat="server" 
                     															    ControlToValidate="txtPPPerRate" OnServerValidate="ServerValidatePP" ErrorMessage="Please enter a positive number for PP Per Rate">*</asp:CustomValidator>
                                
					    Set the Portal Price: </td>
                  </tr>
				   <tr>
                    <td height="24" >
					 <table  height="24"  border="0" cellspacing="0" cellpadding="0">
						<tr valign="top">
						  <td width="180" class="txtWelcome"><asp:RadioButton TabIndex="15" runat="server" ID="rdo0ValuePP" Text="Require Quotation for work" CssClass="txtWelcome" GroupName="rdoValuePP"  AutoPostBack="true"></asp:RadioButton></td>
						  <td width="175" class="txtWelcome"><asp:RadioButton Checked="true" TabIndex="16" runat="server" ID="rdoNon0ValuePP" Text="Propose a price(excl. VAT)" CssClass="txtWelcome" GroupName="rdoValuePP" AutoPostBack="true"></asp:RadioButton></td>
						  <td  width="470"><span class="txtWelcome">&pound;</span>
							  <asp:TextBox id="txtSpendLimitPP" Text="0" runat="server" CssClass="txtWelcome" style="text-align:right;width:120px;" TabIndex="17"></asp:TextBox>
                              &nbsp;&nbsp;&nbsp;<b>PP Per Rate</b>&nbsp;&nbsp;<span class="txtWelcome">&pound;</span>
                              <asp:TextBox id="txtPPPerRate" Text="0" runat="server" CssClass="txtWelcome" style="text-align:right;width:120px;" TabIndex="17"></asp:TextBox>
                               &nbsp;<asp:DropDownList id="ddPPPerRate" runat="server" CssClass="formFieldGrey" Width="70">
								                                  <asp:ListItem Text="Per Job" Value="Per Job"></asp:ListItem>
								                                  <asp:ListItem Text="Per Hour" Value="Per Hour"></asp:ListItem>
                                                                  <asp:ListItem Text="Per Day" Value="Per Day"></asp:ListItem>
                                                                  <asp:ListItem Text="Per Week" Value="Per Week"></asp:ListItem>                                                      
								                                </asp:DropDownList>  
                              </td>
						  <TD width="170" align="left" class="txtWelcome"  ><asp:CheckBox TabIndex="18" runat="server" ID="chkReviewBid" Text="Allow Review Bids" CssClass="txtWelcome" ></asp:CheckBox></TD>
						</tr>						
					   </table>
					</td>
                  </tr>
               
                  <tr>
                    <td class="formLabelGrey" height="24" valign="bottom">Please add any comments you have below. Thank You.
                    <asp:RequiredFieldValidator id="rqWOComments" runat="server" ErrorMessage="Please enter Comment." ForeColor="#EDEDEB"
			        ControlToValidate="txtWOComment">*</asp:RequiredFieldValidator>
                    </td>
                  </tr>
			       <tr>
                    <td><asp:TextBox id="txtWOComment"  runat="server" CssClass="formFieldGrey width934" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" Rows="4"></asp:TextBox></td>
                  </tr>
                </table>	
			    <table  border="0" align="right" cellpadding="0" cellspacing="0">
				    <tr>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td width="20">&nbsp;</td>
				    </tr>
				    <tr>
					    <td align="right" valign="top">
						      <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0" runat="server" id="tblConfirm">
							    <TR>
							      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
							      <TD class="txtBtnImage">
							      <asp:LinkButton ID="lnkConfirm" CausesValidation="False" runat="server" CssClass="txtListing" Text="<img src='Images/Icons/Icon-Confirm.gif' title='Cancel' width='12' height='12' align='absmiddle' hspace='1' vspace='2' border='0'> Confirm"></asp:LinkButton></TD>
							      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
							    </TR>
					      </TABLE>
					    </td>
					    <td align="right">
					      <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
						    <TR>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
						      <TD class="txtBtnImage" >
						      <asp:LinkButton ID="lnkCancel" CausesValidation="false" runat="server" CssClass="txtListing" Text="<img src='Images/Icons/Cancel.gif' title='Cancel' width='11' height='11' align='absmiddle' hspace='1' vspace='2' border='0'> Cancel"></asp:LinkButton></TD>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
						    </TR>
					      </TABLE>
					    </td>
					    <td width="20" align="right">&nbsp;</td>
			       </tr>
			    </table>	
			</asp:Panel>	
			<asp:Panel ID="pnlSuccess" runat="server" Visible="false">
			    <table   border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><asp:Label ID="lblSuccess" runat="server" CssClass="formLabelGrey"></asp:Label></td>
                     <td align="right">
					      <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
						    <TR>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
						      <TD class="txtBtnImage" width="30">
    						      <asp:LinkButton ID="lnkBack" CausesValidation="false" runat="server" CssClass="txtListing" Text="Back"></asp:LinkButton></TD>
						      <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
						    </TR>
					      </TABLE>
					    </td>
                  </tr>
                </table>
			    
			</asp:Panel> 
			
			</ContentTemplate>
			  <Triggers>  	
				<asp:AsyncPostBackTrigger EventName="Click" ControlID="lnkConfirm" />	
			  </Triggers>
			</asp:UpdatePanel>
			
			 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelCreateWorkOrder" runat="server">
					<ProgressTemplate>
						<div>
							<img  align=middle src="Images/indicator.gif" />
							Please Wait...
						</div>      
					</ProgressTemplate>
			</asp:UpdateProgress>
			<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePanelCreateWorkOrder" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />

              <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
     </asp:Content>