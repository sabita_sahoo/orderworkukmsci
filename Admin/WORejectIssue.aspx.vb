Public Partial Class WORejectIssue
    Inherits System.Web.UI.Page
    Protected WithEvents UCWOProcess1 As UCMSWOProcess

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        UCWOProcess1.ParentName = "WORejectIssue"
        UCWOProcess1.CompanyID = Session("CompanyID")
        UCWOProcess1.UserID = Session("UserID")

        UCWOProcess1.Viewer = ApplicationSettings.ViewerAdmin
        UCWOProcess1.ClassID = ApplicationSettings.RoleOWID
        UCWOProcess1.RoleGroupID = Session("RoleGroupID")
        UCWOProcess1.BizDivId = Session("BizDivId")

        If Not IsNothing(Request("SupCompId")) Then
            If Request("SupCompId") <> 0 Then
                UCWOProcess1.SupCompanyID = Trim(Request("SupCompId"))
            Else
                UCWOProcess1.SupCompanyID = Nothing
            End If
        Else
            UCWOProcess1.SupCompanyID = Nothing
        End If
    End Sub

End Class