Imports WebLibrary
Partial Public Class WOForm
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs


#Region "Declaration"

    '''<summary>
    '''UCTopMSgLabel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCTopMSgLabel1 As UCTopMSgLabel

    '''<summary>
    '''UCMenu1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCMenu1 As UCMenu

    '''<summary>
    '''UCSubMenu1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCSubMenu1 As UCSubMenu

    '''<summary>
    '''UCWOPlacedSummary1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCWOPlacedSummary1 As UCWOPlacedSummary

    ''' <summary>
    ''' Create Workorder user control
    ''' </summary>
    ''' <remarks></remarks>
    Protected WithEvents UCCreateWorkOrderUK1 As UCMSWOForm
    Protected WithEvents UCAccountSumm1 As UCAccountSummary
    ''' <summary>
    ''' Company id of the user who has logged in
    ''' </summary>
    ''' <remarks></remarks>
    Private _CompanyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _CompanyID
        End Get
        Set(ByVal value As Integer)
            _CompanyID = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ADMIN

        Security.SecurePage(Page)

        If Not (IsNothing(Request("WOID"))) Then
            UCCreateWorkOrderUK1.WOID = Request("WOID").Trim
        End If
        'CompanyID of the user who is assigned the workorder
        If Not (IsNothing(Request("CompanyID"))) Then
            UCCreateWorkOrderUK1.CompanyID = Request("CompanyID").Trim
        End If

        'Contact id of the user who is assigned the workorder
        If Not (IsNothing(Request("ContactID"))) Then
            UCCreateWorkOrderUK1.ContactID = Request("ContactID").Trim
        End If

        If Not Session("BizDivId") Is Nothing Then
            If Session("BizDivId") = "" Then
                UCCreateWorkOrderUK1.BizDivID = ApplicationSettings.BizDivId
            Else
                UCCreateWorkOrderUK1.BizDivID = Session("BizDivId")
            End If
        Else
            UCCreateWorkOrderUK1.BizDivID = ApplicationSettings.BizDivId
        End If


        'Role group id - whether the logged in user is an Administrator/Manager/Specialist/User
        UCCreateWorkOrderUK1.RoleGroupID = Session("RoleGroupID")

        'If WOID <> 0 means that a workorder is being edited.    
        If UCCreateWorkOrderUK1.WOID <> 0 Then
            If Not (IsNothing(Request("Sender"))) Then
                If (Request("Sender") = "SampleWO") Then
                    UCCreateWorkOrderUK1.Src = Request("Sender")
                Else
                    If (Request("Sender") = "TemplateWOListing") Then
                        UCCreateWorkOrderUK1.Src = "CreateSimilarWOFromTemplate"
                    Else
                        UCCreateWorkOrderUK1.Src = "EditWO"
                    End If
                End If
            Else
                UCCreateWorkOrderUK1.Src = "EditWO"
            End If
        Else
            UCCreateWorkOrderUK1.Src = "NewWO"
        End If

        UCCreateWorkOrderUK1.AdminCompID = Session("CompanyId")
        UCCreateWorkOrderUK1.AdminConID = Session("UserId")

        'Viewer
        UCCreateWorkOrderUK1.Viewer = "Admin"
        UCCreateWorkOrderUK1.ShowLocCheckBox = True
        lnkBackToListing.HRef = getBackToListingLink()
    End Sub


    ''' <summary>
    ''' generates Back to listing link for TemplateWOListing page.
    ''' </summary>
    ''' <returns>string</returns>
    ''' <remarks>This function is used for TemplateWOListing page.
    '''   If page is comming from TemplateWOListing page then the back to listing button will be visible.
    ''' </remarks>
    Public Function getBackToListingLink() As String
        'If page is opened in a new page i.e. target=_BLANK then hide back to listing button
        If Not Request("sender") Is Nothing Then
            If Not Request("sender") = "TemplateWOListing" Then
                tdBackToListing.Visible = False
                Return Nothing
            Else
                tdBackToListing.Visible = True
            End If
        Else
            tdBackToListing.Visible = False
            Return Nothing
        End If

        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender").ToLower
                Case "templatewolisting"
                    link = "~\TemplateWOListing.aspx?"
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=WOForm"
                Case Else
                    Return ""
            End Select
        End If
        Return link
    End Function

    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetCompanyName(ByVal prefixText As String) As DataTable
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("CompanyName", prefixText, 0)
        Return dt
    End Function


    <System.Web.Services.WebMethod()> _
        Public Shared Function GetProductDetails(ByVal productid As String, ByVal companyid As String) As DataTable
        Dim ds As New DataSet
        companyid = CInt(companyid)
        ds = ws.WSWorkOrder.populateWOFormProductDetails(CInt(productid), CInt(companyid), 1, CInt(HttpContext.Current.Session("UserID")), "WoForm", "")
        Return ds.Tables(0)
    End Function

    <System.Web.Services.WebMethod()> _
        Public Shared Function GetHolidays() As DataTable
        Dim ds As New DataSet
        ds = ws.WSContact.GetHolidays("HolidayDate ASC", "0", "200", False, "WOForm", 0)
        Return ds.Tables(0)
    End Function

    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetGoodsLocation(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        If Not IsNothing(HttpContext.Current.Session("BuyerCompanyID")) Then
            dt = ws.WSWorkOrder.GetGoodsLocation("GoodsLocation", prefixText, CInt(HttpContext.Current.Session("BuyerCompanyID")))
        End If
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function

    <System.Web.Services.WebMethod()> _
       Public Shared Function DepotLocDetails(ByVal AddressID As String) As DataTable
        Dim dt As New DataTable
        AddressID = CInt(AddressID)
        dt = ws.WSWorkOrder.GetDepotLocDetails(AddressID)
        Return dt
    End Function
End Class