<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AgedDebtReport.aspx.vb" Inherits="Admin.AgedDebtReport" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :"%>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>  

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
  




			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
              <table width="100%" cellpadding="0" cellspacing="0">
				   <tr>
						<td width="10"></td>
						<td class="HeadingRed"><strong>OrderWork Aged Debt Report</strong></td>
				   </tr>

			  </table>

			 <table  width="100%" cellpadding="0" cellspacing="0" border="0">
					  <tr>
					  	<td style="padding-top:10px; ">
							 <rsweb:ReportViewer ID="ReportViewerOW" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="700px" ProcessingMode="Remote" Width="100%" bgColor="#FFFFFF">
									<ServerReport ReportServerUrl="" />
							 </rsweb:ReportViewer>
						</td>
					  </tr>

			  </table>
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>

</asp:Content>
