
Imports WebLibrary
Imports System.IO
Partial Public Class UpdateRating
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then
            ' Code to Show/Hide control for OW Admin
            If Not IsNothing(Request.QueryString("WOID")) Then
                Dim WOID As String = (Request.QueryString("WOID")).ToString
                ViewState("SelectedWorkOrderId") = WOID
                SelectFromDB(WOID)

                rdBtnPositive.Enabled = True
                rdBtnNeutral.Enabled = True
                rdBtnNegative.Enabled = True
            End If
        End If
    End Sub
    Public Function SelectFromDB(ByVal WOID As String)
        Dim ds As DataSet
        ds = CommonFunctions.GetWORating(WOID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
                    Dim Result As Integer = ds.Tables(0).Rows(0)(0)
                    Select Case Result
                        Case -1
                            rdBtnNegative.Checked = True
                            rdBtnNegative.Enabled = False
                        Case 0
                            rdBtnNeutral.Checked = True
                            rdBtnNeutral.Enabled = False
                        Case 1
                            rdBtnPositive.Checked = True
                            rdBtnPositive.Enabled = False
                    End Select
                End If
            End If
        End If
        Return ds
    End Function
End Class