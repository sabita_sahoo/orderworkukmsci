Public Partial Class InvoicesPrintForm
    Inherits System.Web.UI.Page


    Protected WithEvents reWo As System.Web.UI.WebControls.DataList
    Protected WithEvents lblErrMsg As Label
    Protected WithEvents pnlErrMsg As Panel
    Protected WithEvents pnlPrintContent As Panel
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Security.SecurePage(Page)
        Dim invoiceNo As String
        Dim tempArray As Array
        Dim arrindx As Int32 = 0


        'Security Check for Enhanced URL Security
        Dim errValue As String = ""
        If Not IsNothing(Request("err")) Then
            errValue = Request("err")
        Else
            errValue = 0
        End If
        Select Case errValue
            Case ApplicationSettings.ErrorCode.AccessDenied
                lblErrMsg.Text = ResourceMessageText.GetString("ErrorPrintAccessDenied")
                pnlErrMsg.Visible = True
                pnlPrintContent.Visible = False
            Case ApplicationSettings.ErrorCode.None
                pnlErrMsg.Visible = False
                pnlPrintContent.Visible = True

                invoiceNo = Request("InvoiceNo")

                tempArray = invoiceNo.Split(",")
                'For arrindx = 0 To tempArray.Length - 1
                '    If tempArray(arrindx) <> "" Then
                '        tempArray(arrindx) = tempArray(arrindx)
                '    End If
                'Next
                invoiceNo = ""
                For arrindx = 0 To tempArray.Length - 1
                    If invoiceNo = "" Then
                        invoiceNo = tempArray(arrindx).ToString
                    Else
                        invoiceNo = invoiceNo & "," & tempArray(arrindx).ToString
                    End If
                Next
                invoiceNo.Replace(",,", ",")
                Dim i As Integer
                Dim count As Integer
                For i = 0 To invoiceNo.Length
                    If Right(invoiceNo, 1) = "," Then
                        invoiceNo = Left(invoiceNo, Len(invoiceNo) - 1)
                        count = 1
                    End If
                    If Left(invoiceNo, 1) = "," Then
                        invoiceNo = invoiceNo.Remove(0, 1)
                        count = count + 1
                    End If
                    If count > 0 Then
                        Exit For
                    End If
                Next i
                If invoiceNo <> "" Then
                    Dim ds As DataSet = ws.WSFinance.MS_GetSalesAdviceDetails_PrintList(ApplicationSettings.OWUKBizDivId, invoiceNo, Request("CompanyId"))
                    Session("ds") = ds
                    'Orderworkdetails
                    ViewState("VATRegNo") = ds.Tables("tblOrderWorkDetails").Rows(0).Item("VATRegNo")
                    ViewState("BankSortCode") = ds.Tables("tblOrderWorkDetails").Rows(0).Item("BankSortCode")
                    ViewState("BankAccountNo") = ds.Tables("tblOrderWorkDetails").Rows(0).Item("BankAccountNo")
                    ViewState("BankAccountName") = ds.Tables("tblOrderWorkDetails").Rows(0).Item("BankAccountName")
                    ViewState("CompanyRegNo") = ds.Tables("tblOrderWorkDetails").Rows(0).Item("CompanyRegNo")

                    If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                        'Registration number
                        ViewState("RegistartionIN") = "Registered in England"

                        If Not (IsDBNull(ds.Tables("tblOrderWorkDetails").Rows(0)("CompanyRegNo"))) Then
                            ViewState("RegistartionNo") = "No.  " & ds.Tables("tblOrderWorkDetails").Rows(0)("CompanyRegNo")
                        Else
                            ViewState("RegistartionNo") = ""
                        End If
                    Else
                        ViewState("RegistartionIN") = ""
                        ViewState("RegistartionNo") = ""
                    End If

                    'order work address - tblOWAddress
                    ViewState("Address") = ds.Tables("tblOWAddress").Rows(0).Item("Address")
                    ViewState("City") = ds.Tables("tblOWAddress").Rows(0).Item("City")
                    ViewState("PostCode") = ds.Tables("tblOWAddress").Rows(0).Item("PostCode")
                    ViewState("State") = ds.Tables("tblOWAddress").Rows(0).Item("State")
                    ViewState("Country") = ds.Tables("tblOWAddress").Rows(0).Item("Country")
                    ViewState("CompanyName") = ds.Tables("tblOWAddress").Rows(0).Item("CompanyName")
                    ViewState("Phone") = ds.Tables("tblOWAddress").Rows(0).Item("Phone")
                    ViewState("Fax") = ds.Tables("tblOWAddress").Rows(0).Item("Fax")

                    'order work bank details - tblBankDetails
                    ViewState("bankCity") = ds.Tables("tblBankDetails").Rows(0).Item("City")
                    ViewState("bankAddress") = ds.Tables("tblBankDetails").Rows(0).Item("Address")
                    ViewState("bankPostCode") = ds.Tables("tblBankDetails").Rows(0).Item("PostCode")
                    ViewState("bankContactType") = ds.Tables("tblBankDetails").Rows(0).Item("ContactType")
                    ViewState("bankCompanyName") = ds.Tables("tblBankDetails").Rows(0).Item("CompanyName")

                    If ds.Relations("LinksByHeader") Is Nothing Then
                        ds.Relations.Add("LinksByHeader", ds.Tables("tblAdviceDetails").Columns("InvoiceNo"), ds.Tables("tblWODetails").Columns("InvoiceNo"), False)
                    End If

                    dlPrint.DataSource = ds.Tables("tblAdviceDetails").Rows
                    dlPrint.DataBind()

                End If
        End Select
    End Sub

    Public Function getOWAddress() As String
        Return ViewState("Address") & "<br>" & ViewState("State") & " " & ViewState("PostCode")
    End Function

    Public Function GetBankDetails() As String
        Dim str As String

        If Not (IsDBNull(ViewState("bankCompanyName"))) Then
            str = ViewState("bankCompanyName")
        End If
        If Not (IsDBNull(ViewState("bankAddress"))) Then
            str &= ", " & ViewState("bankAddress")
        End If
        If Not (IsDBNull(ViewState("bankCity"))) Then
            str &= ", " & ViewState("bankCity")
        End If
        If Not (IsDBNull(ViewState("bankPostCode"))) Then
            str &= ", " & ViewState("bankPostCode")
        End If

        Return str
    End Function

    Public Function GetCompanyRegNo(ByVal RegNo) As String
        Dim str As String = ""
        If Not IsDBNull(RegNo) Then
            str = "Company No : " & RegNo.ToString
        End If
        Return str
    End Function

    Public Sub reWo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles reWo.ItemDataBound
        Dim repWO As Repeater = DirectCast(e.Item.FindControl("rptList"), Repeater)
        repWO.DataSource = (DirectCast(e.Item.DataItem, DataRow)).GetChildRows("LinksByHeader")
        repWO.DataBind()
        Dim repWOReceipt As Repeater = DirectCast(e.Item.FindControl("rptListReceipt"), Repeater)
        repWOReceipt.DataSource = (DirectCast(e.Item.DataItem, DataRow)).GetChildRows("LinksByHeader")
        repWOReceipt.DataBind()
        Dim repWOCN As Repeater = DirectCast(e.Item.FindControl("rptListCN"), Repeater)
        repWOCN.DataSource = (DirectCast(e.Item.DataItem, DataRow)).GetChildRows("LinksByHeader")
        repWOCN.DataBind()
    End Sub



    Public Function ShowListFee(ByVal vat, ByVal woval, ByVal total) As Boolean
        If (vat = 0) And (woval = 0) And (total = 0) Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function GetListOfWorkorder(ByVal WO) As String
        If Not IsDBNull(WO) Then
            Return "Listing Fee for Workorder Ids - " & WO
        Else
            Return ""
        End If
    End Function
    Public Function GetBillingAddress(ByVal AdviceNumber As String) As String
        Dim str As String = ""
        Dim ds As DataSet
        Dim dv As DataView
        If Not IsNothing(Session("ds")) Then
            ds = CType(Session("ds"), DataSet)
            dv = ds.Tables(6).Copy.DefaultView
            dv.RowFilter = "AdviceNumber='" + AdviceNumber + "'"
            str = str + dv.Item(0)("CompanyName") + "<br />" + dv.Item(0)("Address") + "<br />" + dv.Item(0)("City") + "<br />" + dv.Item(0)("State") + " " + dv.Item(0)("Postcode")
        End If
        Return str
    End Function
    Public Function getInvoiceVariables(ByVal AdviceNumber As String) As DataView
        Dim str As String = ""
        Dim ds As DataSet
        Dim dv As DataView
        If Not IsNothing(Session("ds")) Then
            ds = CType(Session("ds"), DataSet)
            dv = ds.Tables(7).Copy.DefaultView
            dv.RowFilter = "AdviceNumber='" + AdviceNumber + "'"
        End If
        Return dv
    End Function
End Class