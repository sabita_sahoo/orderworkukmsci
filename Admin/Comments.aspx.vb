Public Partial Class Comments
    Inherits System.Web.UI.Page

    Protected WithEvents UCTopMSgLabel1 As UCTopMSgLabel
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents UCComments As UCComments
    Protected WithEvents UCAccountSumm1 As UCAccountSummary
    Protected WithEvents lnkViewFavSuppliers As System.Web.UI.HtmlControls.HtmlAnchor

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsNothing(Request("sender")) Then
            If Request("sender") = "SearchAccounts" Then
                UCComments.ContactID = Request("companyid")
                UCComments.BizDivID = Request("bizdivid")
                Session("ContactBizDivID") = Request("bizdivid")
            Else
                UCComments.ContactID = Request("companyid")
                UCComments.BizDivID = Request("bizdivid")
            End If
        Else
            UCComments.ContactID = Request("companyid")
            UCComments.BizDivID = Request("bizdivid")
        End If

        If Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
            UCAccountSumm1.ShowRating = True
            lnkViewBlackListClient.Visible = True
            lnkViewWorkingDays.Visible = True
        ElseIf Session("ContactClassID") = ApplicationSettings.RoleClientID Then
            UCAccountSumm1.ShowRating = False
        End If
        UCAccountSumm1.BizDivId = Request("bizDivId")

        If Session(Request("companyid") & "_ContactClassID") = ApplicationSettings.RoleClientID Then
            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            lnkCompProfile.HRef = "CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")

            'Prepare AutoMatch Link URL
            lnkAutoMatch.HRef = "AutoMatchSettings.aspx?CompanyID=" & Request("companyid") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            lnkAccSettings.HRef = "AccountSettings.aspx?CompanyID=" & Request("companyid") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            lnkViewAllInvoices.HRef = "ViewAllInvoices.aspx?CompanyID=" & Request("companyid") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            lnkViewAccountHistory.HRef = "AccountHistory.aspx?CompanyID=" & Request("companyid") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")

            'lnkViewFavSuppliers.HRef = "FavouriteSupplierList.aspx?mode=favourite&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            'lnkAddProduct.HRef = "AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            ''lnkViewBlackListsupplier.HRef = "FavouriteSupplierList.aspx?mode=blacklist&CompanyID=" & Session("ContactCompanyID")
            'lnkReferences.HRef = "References.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            'lnkuser.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            'lnkloc.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            'lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            'lnkAddUsers.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            'lnkAddLocationsSupp.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        ElseIf Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
            lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId")

            'Prepare AutoMatch Link URL
            lnkAutoMatch.HRef = "AutoMatchSettings.aspx?CompanyID=" & Request("companyid") & "&sender=supplier" & "&bizDivId=" & Request("bizDivId")
            lnkAccSettings.HRef = "AccountSettings.aspx?CompanyID=" & Request("companyid") & "&sender=supplier" & "&bizDivId=" & Request("bizDivId")
            lnkViewAllInvoices.HRef = "ViewAllInvoices.aspx?CompanyID=" & Request("companyid") & "&sender=supplier" & "&bizDivId=" & Request("bizDivId")
            lnkViewAccountHistory.HRef = "AccountHistory.aspx?CompanyID=" & Request("companyid") & "&sender=supplier" & "&bizDivId=" & Request("bizDivId")
        End If
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        lnkViewFavSuppliers.HRef = "FavouriteSupplierList.aspx?mode=favourite&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkAddProduct.HRef = "AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid")
        lnkAddProductNew.HRef = "ServiceListing.aspx?CompanyID=" & Request("CompanyID")
        'lnkViewBlackListsupplier.HRef = "FavouriteSupplierList.aspx?mode=blacklist&CompanyID=" & Session("ContactCompanyID")
        lnkReferences.HRef = "References.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkuser.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkloc.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        'lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        lnkAddUsers.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkAddLocationsSupp.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkViewBlackListClient.HRef = "FavouriteSupplierList.aspx?mode=ClientBlacklist&CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkViewWorkingDays.HRef = "WorkingDays.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")


        'Code commeneted as Suppliers can also behave as buyers
        ''Show Favourite supplier link
        'lnkViewFavSuppliers.Visible = False
        'If Session("ContactClassID") = ApplicationSettings.RoleClientID Then
        '    lnkViewFavSuppliers.Visible = True
        'End If

    End Sub

End Class