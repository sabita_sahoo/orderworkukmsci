﻿Public Class ServiceListing
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then
            If Not IsNothing(Request("CompanyID")) Then
                iFrameServiceListing.Attributes.Add("src", OrderWorkLibrary.ApplicationSettings.NewPortalURL.ToString & "/service/listing/" & Session("UserID").ToString & "/" & Request("CompanyID").ToString)
            Else
                iFrameServiceListing.Attributes.Add("src", OrderWorkLibrary.ApplicationSettings.NewPortalURL.ToString & "/service/listing/" & Session("UserID").ToString)
            End If

        End If
    End Sub

End Class