
Partial Public Class AdminWOIssueListing
    Inherits System.Web.UI.Page

    Public Shared ws As New WSObjs
    Protected WithEvents UCAdminWOsIssueListing1 As UCAdminWOsIssueListing
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents CValidatorSearchContact As System.Web.UI.WebControls.CustomValidator

    Delegate Sub DelPopulateObject(ByVal CompID As Integer, ByVal myInt As Integer, ByVal InitialCall As Boolean, ByVal PagerIndex As Integer, ByVal killcache As Boolean, ByVal IssueBy As String, ByVal WorkorderID As String)

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        UserAction.userAutoLoginAccept(Page)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsNothing(Request("Sender")) Then
            If Request("Sender") = "OrderMatch" Then
                lnkBackToListing.HRef = getBackToListingLink()
                divButton.Visible = True
            Else
                divButton.Visible = False
            End If
        Else
            divButton.Visible = False
        End If

        ''Added Code by Pankaj Malav on 11Oct 2008
        ''This code is added to fix the issue while login the url is changed to lower case so Submitted is converted to submitted which disables the proper functioning of the page
        ''The fix that is found is that we can convert the first character of the mode to upper and leave the rest
        Dim mode As String
        mode = Request.QueryString("mode")
        If mode <> ApplicationSettings.WOGroupCA Then
            mode = StrConv(mode, VbStrConv.ProperCase)
        End If

        UCAdminWOsIssueListing1.BizDiv = Session("BizDivId")
        UCSearchContact1.BizDivID = Session("BizDivId")
        UCSearchContact1.HideTestAccounts = chkHideTestAcc.Checked

        UCSearchContact1.DefaultView = chkMainCat.Checked
        UCAdminWOsIssueListing1.Group = mode
        If Not IsPostBack Then

            UCSearchContact1.Filter = mode
            UCSearchContact1.populateContact()
            Dim selVal As String
            selVal = ""
            UCAdminWOsIssueListing1.CompID = -1

            If Not IsNothing(Request("CompID")) Then
                If Request("CompID") <> "0" And Request("CompID") <> "" Then
                    UCSearchContact1.ddlContact.SelectedValue = Request("CompID").Trim
                    UCAdminWOsIssueListing1.CompID = Request("CompID")
                ElseIf Request("CompID") = "0" Then
                    UCSearchContact1.ddlContact.SelectedValue = "ALL"
                    UCAdminWOsIssueListing1.CompID = 0
                    Session("AdminSelectedCompID") = 0
                End If
            Else
                ''If UCSearchContact1.ddlBillLoc.SelectedValue <> "" Then
                ''    UCAdminWOsIssueListing1.BillLoc = UCSearchContact1.ddlBillLoc.SelectedValue
                ''Else
                ''    UCAdminWOsIssueListing1.BillLoc = 0
                ''End If
                If Not IsNothing(Session("AdminSelectedCompID")) Then
                    If Session("AdminSelectedCompID") <> 0 Then
                        Dim li As ListItem
                        li = UCSearchContact1.ddlContact.Items.FindByValue(Session("AdminSelectedCompID").ToString)
                        If Not IsNothing(li) Then
                            UCSearchContact1.ddlContact.SelectedValue = Session("AdminSelectedCompID")
                            UCAdminWOsIssueListing1.CompID = Session("AdminSelectedCompID")
                        Else
                            UCSearchContact1.ddlContact.SelectedValue = Session("AdminSelectedCompID")
                            UCAdminWOsIssueListing1.CompID = Session("AdminSelectedCompID")
                        End If
                    ElseIf Session("AdminSelectedCompID") = "0" Then
                        UCSearchContact1.ddlContact.SelectedValue = "ALL"
                        UCAdminWOsIssueListing1.CompID = Session("AdminSelectedCompID")
                    Else
                        If Not IsNothing(Request("from")) Then
                            If (Request("from").ToString = "multiplewo") Then
                                Session("AdminSelectedCompID") = 0
                                UCSearchContact1.ddlContact.SelectedValue = "ALL"
                                UCAdminWOsIssueListing1.CompID = Session("AdminSelectedCompID")
                            Else
                                Session("AdminSelectedCompID") = Nothing
                            End If
                        Else
                            Session("AdminSelectedCompID") = Nothing
                        End If
                    End If
                Else
                    If Not IsNothing(Request("from")) Then
                        If (Request("from").ToString = "multiplewo") Then
                            Session("AdminSelectedCompID") = 0
                            UCSearchContact1.ddlContact.SelectedValue = "ALL"
                            UCAdminWOsIssueListing1.CompID = Session("AdminSelectedCompID")
                        Else
                            Session("AdminSelectedCompID") = Nothing
                        End If
                    Else
                        Session("AdminSelectedCompID") = Nothing
                    End If
                End If
            End If
            Dim aObj(6) As Object
            aObj(0) = CInt(Session("AdminSelectedCompID"))
            aObj(1) = 0
            aObj(2) = False
            aObj(3) = 0
            aObj(4) = False
            aObj(5) = Session("IssueBy")
            aObj(6) = Session("IssueWorkorderID")
            Dim delPopulate As New DelPopulateObject(AddressOf UCAdminWOsIssueListing1.Populate)
            'code for showing and hiding for billing location dropdown on pageload
            If Not IsNothing(Session("BillLoc")) And Not IsNothing(Session("AdminSelectedCompID")) Then
                If Session("AdminSelectedCompID") <> 0 And Session("AdminSelectedCompID") <> -1 Then
                    ddlBillLoc.Visible = True
                    delPopulate.DynamicInvoke(aObj)
                Else
                    ddlBillLoc.Visible = False
                End If

            Else
                ddlBillLoc.Visible = False
            End If
            'code for populating data in dropdown
            Dim dvBillLoc As DataView
            If ddlBillLoc.Visible Then


                dvBillLoc = UCAdminWOsIssueListing1.FillBillingLocation()

                If Not IsNothing(dvBillLoc) Then
                    ddlBillLoc.DataSource = dvBillLoc
                    ddlBillLoc.DataBind()
                Else
                    ddlBillLoc.Visible = False
                End If
                'If dvBillLoc.Count > 0 Then

                '    Dim li1 As New ListItem
                '    li1 = New ListItem
                '    li1.Text = "All"
                '    li1.Value = 0
                '    ddlBillLoc.Items.Insert(0, li1)
                'Else
                '    ddlBillLoc.Visible = False
                'End If






                'for selected value of dropdown billing location
                If Not IsNothing(dvBillLoc) Then
                    If Not IsNothing(Session("BillLoc")) Then
                        dvBillLoc.RowFilter = "BillingLocID = " & Session("BillLoc")
                        If dvBillLoc.ToTable.Rows.Count > 0 Then
                            ddlBillLoc.SelectedValue = Session("BillLoc")
                        Else
                            Session("BillLoc") = 0
                            delPopulate.DynamicInvoke(aObj)
                        End If
                    Else
                        Session("BillLoc") = 0
                        delPopulate.DynamicInvoke(aObj)
                    End If
                Else
                    Session("BillLoc") = 0
                    delPopulate.DynamicInvoke(aObj)
                End If
            Else
                Session("BillLoc") = 0
                delPopulate.DynamicInvoke(aObj)
            End If

            'end code for billing location
            'for ddl issueby
            If Not IsNothing(Request("from")) Then
                If (Request("from").ToString = "multiplewo") Then
                    If Not IsNothing(Session("IssueBy")) Then
                        If Session("IssueBy") = ApplicationSettings.IssueByAll Then
                            ddlIssueBy.SelectedIndex = 0
                        ElseIf Session("IssueBy") = ApplicationSettings.IssueByOW Then
                            ddlIssueBy.SelectedIndex = 1
                        ElseIf Session("IssueBy") = ApplicationSettings.IssueBySupplier Then
                            ddlIssueBy.SelectedIndex = 2
                        End If
                    End If
                End If
            End If

            If Not IsNothing(Session("IssueBy")) Then
                If Session("IssueBy") = ApplicationSettings.IssueByAll Then
                    ddlIssueBy.SelectedIndex = 0
                ElseIf Session("IssueBy") = ApplicationSettings.IssueByOW Then
                    ddlIssueBy.SelectedIndex = 1
                ElseIf Session("IssueBy") = ApplicationSettings.IssueBySupplier Then
                    ddlIssueBy.SelectedIndex = 2
                End If
            End If
            If Not IsNothing(Request("SearchWorkorderID")) Then
                txtWorkorderID.Text = Request("SearchWorkorderID")
            Else
                txtWorkorderID.Text = ""
            End If

            Session("IssueWorkorderID") = txtWorkorderID.Text.Trim

            btnExport.HRef = prepareExportToExcelLink()

            'For issueby
        End If
        UCAdminWOsIssueListing1.WorkorderID = txtWorkorderID.Text.Trim
    End Sub

    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then

            If UCSearchContact1.ddlContact.SelectedValue = "" Then
                UCAdminWOsIssueListing1.CompID = -1
                Session("AdminSelectedCompID") = -1
            ElseIf UCSearchContact1.ddlContact.SelectedValue = "ALL" Then
                UCAdminWOsIssueListing1.CompID = 0
                Session("AdminSelectedCompID") = 0
            Else
                UCAdminWOsIssueListing1.CompID = UCSearchContact1.ddlContact.SelectedValue
                Session("AdminSelectedCompID") = UCSearchContact1.ddlContact.SelectedValue
            End If
            If ddlIssueBy.SelectedIndex = 0 Then
                Session("IssueBy") = ApplicationSettings.IssueByAll
            ElseIf ddlIssueBy.SelectedIndex = 1 Then
                Session("IssueBy") = ApplicationSettings.IssueByOW
            ElseIf ddlIssueBy.SelectedIndex = 2 Then
                Session("IssueBy") = ApplicationSettings.IssueBySupplier
            End If
            Session("IssueWorkorderID") = txtWorkorderID.Text.Trim
            UCAdminWOsIssueListing1.WorkorderID = txtWorkorderID.Text.Trim
            UCAdminWOsIssueListing1.Populate(Session("AdminSelectedCompID"), 0, True, True, False, Session("IssueBy"), Session("IssueWorkorderID"))
            ddlBillLoc.Visible = True
            'Else
            '    ddlBillLoc.Visible = False
            'End If
            If UCSearchContact1.ddlContact.SelectedValue = "ALL" Or UCSearchContact1.ddlContact.SelectedValue = "" Then
                'ddlBillLoc.SelectedValue = 0
                ddlBillLoc.Visible = False

            Else

                Dim dvBillLoc As DataView
                dvBillLoc = UCAdminWOsIssueListing1.FillBillingLocation()

                If Not IsNothing(dvBillLoc) Then
                    ddlBillLoc.DataSource = dvBillLoc
                    ddlBillLoc.DataBind()
                End If

                Dim li As ListItem
                li = New ListItem
                li.Text = "All"
                li.Value = 0
                ddlBillLoc.Items.Insert(0, li)
                Dim aObj(6) As Object
                aObj(0) = CInt(Session("AdminSelectedCompID"))
                aObj(1) = 0
                aObj(2) = False
                aObj(3) = 0
                aObj(4) = False
                aObj(5) = Session("IssueBy")
                aObj(6) = Session("IssueWorkorderID")
                Dim delPopulate As New DelPopulateObject(AddressOf UCAdminWOsIssueListing1.Populate)

                If Not IsNothing(dvBillLoc) Then
                    If Not IsNothing(Session("BillLoc")) Then
                        dvBillLoc.RowFilter = "BillingLocID = " & Session("BillLoc")
                        If dvBillLoc.ToTable.Rows.Count > 0 Then
                            ddlBillLoc.SelectedValue = Session("BillLoc")
                        Else
                            Session("BillLoc") = 0
                            delPopulate.DynamicInvoke(aObj)
                        End If
                    Else
                        Session("BillLoc") = 0
                        delPopulate.DynamicInvoke(aObj)
                    End If
                Else
                    Session("BillLoc") = 0
                    delPopulate.DynamicInvoke(aObj)
                End If
            End If

            btnExport.HRef = prepareExportToExcelLink()
        End If


    End Sub

    Public Function prepareExportToExcelLink() As String
        Dim linkParams As String = ""
        Dim mode As String
        mode = Request.QueryString("mode")
        If mode <> ApplicationSettings.WOGroupCA Then
            mode = StrConv(mode, VbStrConv.ProperCase)
        End If

        linkParams &= "BizDivId=" & UCAdminWOsIssueListing1.BizDiv
        linkParams &= "&Group=" & UCAdminWOsIssueListing1.Group
        If UCSearchContact1.ddlContact.SelectedValue <> "" And UCSearchContact1.ddlContact.SelectedValue.ToString.ToLower <> "all" Then
            linkParams &= "&CompID=" & UCSearchContact1.ddlContact.SelectedValue
        Else
            linkParams &= "&CompID=" & 0
        End If
        linkParams &= "&FromDate=" & UCAdminWOsIssueListing1.FromDate
        linkParams &= "&ToDate=" & UCAdminWOsIssueListing1.ToDate
        If (UCAdminWOsIssueListing1.WorkorderID = "0") Then
            linkParams &= "&WorkorderID="
        Else
            linkParams &= "&WorkorderID=" & UCAdminWOsIssueListing1.WorkorderID
        End If
        linkParams &= "&sortExpression=" & UCAdminWOsIssueListing1.sortExpression
        linkParams &= "&hideTestWOs=" & chkHideTestAcc.Checked
        linkParams &= "&MainCat=" & chkMainCat.Checked
        If (Request.QueryString("mode") = "Watched") Then
            linkParams &= "&Watched=" & True
        Else
            linkParams &= "&Watched=" & False
        End If

        Dim splitCompanyName As String = ""
        If Not IsNothing(Session("dsMultiSelectedCompany")) Then
            Dim dsSelectedCompany As DataSet
            Dim dsTemp As DataSet
            dsSelectedCompany = CType(Session("dsMultiSelectedCompany"), DataSet)

            Dim strCompanyName As String = ""
            Dim SPCompanyName As String = ""
            If Not IsNothing(dsSelectedCompany) Then
                dsTemp = CType(Session("dsMultiSelectedCompany"), DataSet)
                Dim SelectedCompanyName As String = ""
                Dim i As Integer
                i = 0
                For Each drow As DataRow In dsTemp.Tables(0).Rows
                    SelectedCompanyName += drow.Item("CompanyName").ToString + ","
                    i = i + 1
                    strCompanyName = strCompanyName + SelectedCompanyName.Trim
                Next
                splitCompanyName = strCompanyName.Substring(0, strCompanyName.LastIndexOf(","))
            End If
        End If
        linkParams &= "&CompanyName=" & splitCompanyName

        If mode = ApplicationSettings.WOGroupCA Or mode = ApplicationSettings.WOGroupSubmitted Or mode = ApplicationSettings.WOGroupDraft Then
            linkParams &= "&page=" & "AdminWOListingNoSupp"
        Else
            linkParams &= "&page=" & "AdminWOListing"
        End If
        Return "ExportToExcel.aspx?" & linkParams
    End Function

    ''' <summary>
    ''' Sub to handle click on click of Multiple selected workorders. Passes WOIDs in string
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Pratik Trivedi-19th Aug, 2008</remarks>
    Public Sub lnkMultipleWOs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMultipleWOs.Click
        UCAdminWOsIssueListing1.GetSelectedWOIDs()
        If UCAdminWOsIssueListing1.SelectedWOIDs <> "" Then
            Dim appendLink As String = ""
            appendLink = "WOID=" & UCAdminWOsIssueListing1.SelectedWOIDs
            appendLink = appendLink & "&mode=" & UCAdminWOsIssueListing1.Group
            Response.Redirect("MultipleWOHandling.aspx?" & appendLink)
            lblMultipleWOsMsg.Visible = False
        Else
            lblMultipleWOsMsg.Visible = True
            lblMultipleWOsMsg.Text = "&nbsp;*&nbsp;" & ResourceMessageText.GetString("WOMultipleSelectError")
        End If

    End Sub



    Public Function getBackToListingLink() As String
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender")
                Case "OrderMatch"
                    link = "~\WOOrderMatch.aspx?"
                    'link &= "bizDivId=" & Request("bizDivId")
                    'link &= "&bizdiv=" & Request("bizdiv")
                    link &= "&companyId=" & Request("compId")
                    If Not IsNothing(Request("vendorIDs")) Then
                        link &= "&vendorIDs=" & Request("vendorIDs")
                    End If
                    'link &= "&contactId=" & Request("contactId")
                    'link &= "&classId=" & Request("classId")
                    'link &= "&statusId=" & Request("statusId")
                    'link &= "&roleGroupId=" & Request("Request")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&product=" & Request("product")
                    link &= "&region=" & Request("region")
                    link &= "&PostCode=" & Request("PostCode")
                    link &= "&Keyword=" & Request("Keyword")
                    link &= "&Group=" & Request("Group")
                    link &= "&refcheck=" & Request("refcheck")
                    link &= "&MinWOValue=" & Request("MinWOValue")

                    link &= "&companyName=" & Request("companyName")
                    link &= "&noOfEmployees=" & Request("noOfEmployees")
                    link &= "&ShowSupplierWithCW=" & Request("ShowSupplierWithCW")
                    If Not IsNothing(Request("vendorIDs")) Then
                        link &= "&vendorIDs=" & Request("vendorIDs")
                    End If

                    'link &= "&mode=" & "edit"
                    link &= "&sender=" & "AdminWOListing"
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&NearestTo=" & Request("NearestTo")
                    link &= "&Distance=" & Request("Distance")
                    link &= "&crbChecked=" & Request("crbChecked")
                    link &= "&ukSecurity=" & Request("ukSecurity")
            End Select
        End If
        Return link
    End Function

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub


    Private Sub chkHideTestAcc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkHideTestAcc.CheckedChanged, chkMainCat.CheckedChanged
        Dim mode As String
        mode = Request.QueryString("mode")
        If mode <> ApplicationSettings.WOGroupCA Then
            mode = StrConv(mode, VbStrConv.ProperCase)
        End If
        UCSearchContact1.Filter = mode
        UCSearchContact1.BizDivID = Session("BizDivId")
        UCSearchContact1.HideTestAccounts = chkHideTestAcc.Checked
        UCSearchContact1.populateContact()
        Dim aObj(6) As Object
        aObj(0) = CInt(Session("AdminSelectedCompID"))
        aObj(1) = 0
        aObj(2) = False
        aObj(3) = 0
        aObj(4) = False
        aObj(5) = Session("IssueBy")
        aObj(6) = Session("IssueWorkorderID")
        Dim delPopulate As New DelPopulateObject(AddressOf UCAdminWOsIssueListing1.Populate)
        If Not IsNothing(Session("BillLoc")) And Not IsNothing(Session("AdminSelectedCompID")) Then
            If Session("AdminSelectedCompID") <> 0 And Session("AdminSelectedCompID") <> -1 Then
                ddlBillLoc.Visible = True
                delPopulate.DynamicInvoke(aObj)
            Else
                ddlBillLoc.Visible = False
            End If
        Else
            ddlBillLoc.Visible = False
        End If
        delPopulate.DynamicInvoke(aObj)
    End Sub

    Public Sub btnWatch_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWatch.ServerClick
        UCAdminWOsIssueListing1.GetSelectedWOIDs()
        If UCAdminWOsIssueListing1.SelectedWOIDs <> "" Then
            ws.WSWorkOrder.AddDeleteWatchedWO(UCAdminWOsIssueListing1.SelectedWOIDs, Session("UserID"), "Add")
            lblMultipleWOsMsg.Visible = True
            lblMultipleWOsMsg.Text = "&nbsp;*&nbsp;Added Successfully to watched listing."
            Dim mode As String
            mode = Request.QueryString("mode")
            If mode <> ApplicationSettings.WOGroupCA Then
                mode = StrConv(mode, VbStrConv.ProperCase)
            End If
            UCSearchContact1.Filter = mode
            UCSearchContact1.BizDivID = Session("BizDivId")
            UCSearchContact1.HideTestAccounts = chkHideTestAcc.Checked
            UCSearchContact1.populateContact()
            Dim aObj(6) As Object
            aObj(0) = CInt(Session("AdminSelectedCompID"))
            aObj(1) = 0
            aObj(2) = False
            aObj(3) = 0
            aObj(4) = False
            aObj(5) = Session("IssueBy")
            aObj(6) = Session("IssueWorkorderID")
            Dim delPopulate As New DelPopulateObject(AddressOf UCAdminWOsIssueListing1.Populate)
            If Not IsNothing(Session("BillLoc")) And Not IsNothing(Session("AdminSelectedCompID")) Then
                If Session("AdminSelectedCompID") <> 0 And Session("AdminSelectedCompID") <> -1 Then
                    ddlBillLoc.Visible = True
                    delPopulate.DynamicInvoke(aObj)
                Else
                    ddlBillLoc.Visible = False
                End If
            Else
                ddlBillLoc.Visible = False
            End If
            delPopulate.DynamicInvoke(aObj)
        Else
            lblMultipleWOsMsg.Visible = True
            lblMultipleWOsMsg.Text = "&nbsp;*&nbsp;" & ResourceMessageText.GetString("WOMultipleSelectError")
        End If
    End Sub
    

    Private Sub ddlBillLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBillLoc.SelectedIndexChanged
        Dim billloc As Integer
        billloc = ddlBillLoc.SelectedValue
        Session("BillLoc") = ddlBillLoc.SelectedValue

        UCAdminWOsIssueListing1.BillLoc = ddlBillLoc.SelectedValue
    End Sub

    Private Sub chkMainCat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMainCat.CheckedChanged
        UCSearchContact1.DefaultView = chkMainCat.Checked
    End Sub

    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetWorkOrderID(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("WorkOrderID", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function
End Class