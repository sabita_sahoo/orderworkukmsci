<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RevenueAndForecastReport.aspx.vb" Inherits="Admin.RevenueAndForecastReport" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork :" %>

<%@ Register Assembly="Admin" Namespace="Admin.OrderWorkUK.Security" TagPrefix="cc2" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:content id="Content1" contentplaceholderid="ContentPlaceHolder1" runat="server">
	<asp:scriptmanager id="ScriptManager1" runat="server">
	</asp:scriptmanager>
	<div id="divContent">
		<div class="roundtopWhite">
			<img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
			<tr>
				<td valign="top">
					<!-- InstanceBeginEditable name="EdtContent" -->
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								&nbsp;
							</td>
							<td class="HeadingRed">
								<strong>Revenue Report</strong>
							</td>
						</tr>
					</table>
					<asp:updatepanel id="UpdatePanel1" runat="server" rendermode="Inline">
						<contenttemplate>

					  <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top:15px; ">
					  	<tr>
							<td width="10px"></td>
							<td width="350px">
									<table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr valign="top">
											  <td class="formTxt" valign="top" align="left" width="18">&nbsp;</td>
											  <td class="formTxt" valign="top" align="left" width="156">Report Type</td>
											  <td class="formTxt" valign="top" align="left" width="18">&nbsp;</td>
											  <TD class="formTxt" vAlign="top"  width="156" align="left" height="14">Month </TD>
											  <td class="formTxt" align="left" width="156">&nbsp;</td>
											  <TD class="formTxt" align="left" height="14">Year </TD>
											  <td height="24" align="left" width="10">&nbsp;</td>
											  <td class="formTxt" align="left" width="156">Additional Months</td>
											  <td height="24" align="left" width="10">&nbsp;</td>
												<td>Forecast Probability</td>
												<td height="24" align="left" width="10">&nbsp;</td>
												<td>Forecast Probability</td>
											</tr>
											<tr valign="top">
											  <td height="24" align="left">&nbsp;</td>
											  <td align="left"><select id="ddReportType" runat="server" name="ddReportType" class="formFieldGrey121">
												  <option value="" >Consolidated</option>
												  <option value="Retail">Retail</option>
												  <option value="IT">IT</option>
												</select>
											  </td>
											  <td height="24" align="left">&nbsp;</td>
											  <td align="left"><select id="ddMonthFrom" runat="server" name="ddMonthFrom" class="formFieldGrey121">
												  <option value="01">January</option>
												  <option value="02">February</option>
												  <option value="03">March</option>
												  <option value="04">April</option>
												  <option value="05">May</option>
												  <option value="06">June</option>
												  <option value="07">July</option>
												  <option value="08">August</option>
												  <option value="09">September</option>
												  <option value="10">October</option>
												  <option value="11">November</option>
												  <option value="12">December</option>
												</select>
											  </td>
											  <td height="24" align="left" width="10">&nbsp;</td>
											  <td align="left"><select id="ddYearFrom" runat="server" name="ddYearFrom" class="formFieldGrey121">
												  <option value="2006">2006</option>
												  <option value="2007">2007</option>
												  <option value="2008">2008</option>
												  <option value="2009">2009</option>
                                                  <option value="2010">2010</option>
                                                  <option value="2011">2011</option>
                                                  <option value="2012">2012</option>
                                                  <option value="2013">2013</option>
                                                  <option value="2014">2014</option>
                                                  <option value="2015">2015</option>
                                                  <%--Poonam modified on 7/3/2015 - Task - OA-214 : OA - Add new fields to Payment File report exported--%>
                                                  <option value="2016">2016</option>
                                                  <option value="2017">2017</option>
                                                  <option value="2018">2018</option>
                                                  <option value="2019">2019</option>
                                                  <option value="2020">2020</option>
												</select>
											  </td>
											  <td height="24" align="left" width="10">&nbsp;</td>
											  <td align="left"><select id="ddMonthAdditional" runat="server" name="ddMonthAdditional" class="formFieldGrey121">
												  <option value="0">0</option>
												  <option value="1">1</option>
												  <option value="2">2</option>
												  <option value="3">3</option>
												  <option value="4">4</option>
												  <option value="5">5</option>
												  <option value="6">6</option>
												  <option value="7">7</option>
												  <option value="8">8</option>
												  <option value="9">9</option>
												  <option value="10">10</option>
												  <option value="11">11</option>
												  <option value="12">12</option>
												</select>
											  </td>
												<td height="24" align="left" width="10">&nbsp;</td>
												<td>
													<asp:TextBox runat="server" id="txtForecastPercent" style="width:40px;" MaxLength="3">90</asp:TextBox>
												</td>
												<td height="24" align="left" width="10">%</td>
												<td><asp:TextBox runat="server" id="txtConditionalPercent" style="width:40px;" MaxLength="3">25</asp:TextBox></td>
												<td height="24" align="left" width="10">%</td>
											</tr>
										  </table>
							</td>
							<td width="20"></td>
							<td align="left"  style="padding-top:30px; ">
								<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                    <asp:LinkButton ID="lnkBtnGenerateReport" causesValidation=True runat="server" CssClass="txtButtonRed" Text="Generate Report"></asp:LinkButton>
                                </div>
							 </td>
							<td width="60" align="left" ></td>
						</tr>
						<tr>
						<td></td>
						<td></td>
						<td></td>
						</tr>
					</table>
				  </contenttemplate>
						<triggers>
						<asp:PostBackTrigger   ControlID="lnkBtnGenerateReport" />
				    </triggers>
					</asp:updatepanel>
					<asp:updateprogress associatedupdatepanelid="UpdatePanel1" id="UpdateProgress1" runat="server">
						<progresstemplate>
				   <div class="gridText">
						<img  align=middle src="Images/indicator.gif" />
						<b>Fetching Data... Please Wait</b>
					</div>
				</progresstemplate>
					</asp:updateprogress>
					<cc1:updateprogressoverlayextender controltooverlayid="UpdatePanel1" targetcontrolid="UpdateProgress1" cssclass="updateProgress" id="UpdateProgressOverlayExtender1" runat="server" />
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td style="padding-top: 20px;">
								<rsweb:ReportViewer ID="ReportViewerOW" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" ProcessingMode="Remote" Width="100%" bgColor="#FFFFFF">
									<ServerReport ReportServerUrl="" />
								</rsweb:ReportViewer>
							</td>
						</tr>
					</table>
					<!-- InstanceEndEditable -->
				</td>
			</tr>
		</table>
	</div>
</asp:content>