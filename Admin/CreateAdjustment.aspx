<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreateAdjustment.aspx.vb" Inherits="Admin.CreateAdjustment" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Create Adjustment" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
	<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!-- InstanceEndEditable --><!-- InstanceParam name="opAccountSumm" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddUser" type="boolean" value="false" --><!-- InstanceParam name="OpRgCLAddLOcation" type="boolean" value="false" --><!-- InstanceParam name="OPWOSummary" type="boolean" value="true" --><!-- InstanceParam name="RgOpBtnNewWO" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddFunds" type="boolean" value="false" --><!-- InstanceParam name="opMenuRegion" type="boolean" value="true" --><!-- InstanceParam name="opContentTopRedCurve" type="boolean" value="false" --><!-- InstanceParam name="opUsersProfile" type="boolean" value="false" --><!-- InstanceParam name="opCompanyProfile" type="boolean" value="true" --><!-- InstanceParam name="opUsersListing" type="boolean" value="false" --><!-- InstanceParam name="opLocations" type="boolean" value="false" --><!-- InstanceParam name="opReferences" type="boolean" value="false" --><!-- InstanceParam name="opPreferences" type="boolean" value="false" --><!-- InstanceParam name="opUsers" type="boolean" value="false" --><!-- InstanceParam name="width" type="text" value="985" --><!-- InstanceParam name="opComments" type="boolean" value="false" --><!-- InstanceParam name="OpRgScriptManager" type="boolean" value="true" --><!-- InstanceParam name="OpFavSuppliers" type="boolean" value="false" --><!-- InstanceParam name="OpFavSuppliersLink" type="boolean" value="false" -->
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
 
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>

	   <div id="divContent">
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            
            
            <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10px"></td>
			<td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Create Adjustment" runat="server"></asp:Label></td>
			</tr>
			</table>
            <br />
            
             <table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
		<td width="10px"></td>
		<td width="240"><%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
					<uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact></td>
		
		<td width="80" align="left"><asp:CheckBox ID="chkShowAllMy" Checked="false" runat="server" Text="Show All" class="formTxt" /> </td>
		
		<td align="left">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                <asp:LinkButton ID="lnkView" causesValidation=False OnClick="lnkView_Click" runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
            </div>
		</td>
		 			
		 <td align="left">&nbsp;
		</td>
		<td width="40">&nbsp;</td>
		</tr>
			  
		</table>
		            
  <br />
		<table>
		   			<tr>
             			<td class="paddingL30 padB21" valign="top">
             
							 <div id="divValidationMain" visible=false class="divValidation" runat=server >
								  <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<tr>
										  <td height="15" align="center">&nbsp;</td>
										  <td height="15">&nbsp;</td>
										  <td height="15">&nbsp;</td>
									</tr>
									<tr valign="middle">
					  						<td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
					  						<td class="validationText"><div  id="divValidationMsg"></div>
						  							<asp:Label ID="lblMsg" runat="server"  CssClass="bodytxtValidationMsg"></asp:Label>		  	
					  						</td>
					  						<td width="20">&nbsp;</td>
									</tr>
									<tr>
											  <td height="15" align="center">&nbsp;</td>
											  <td height="15">&nbsp;</td>
											  <td height="15">&nbsp;</td>
									</tr>
				  			</table>
								  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
								</div>   
		
							</td>
					</tr>
             </table>
             <br />
             <table id="tblAdjustmentFields" cellspacing="0" cellpadding="0" width="600" border="0" runat="server" visible="true" style="margin-left:10px;">
						  						                    <tr>
						  						                        <td width="100" align="left" class="formTxt">Adjmt. Type</td>
						  						                        <td width="90" align="left" class="formTxt">Adjmt. Date</td>
						  						                        <td width="150" align="left" class="formTxt">Adjmt. Amount</td>
						  						                        <td width="100" align="left" class="formTxt">Adjmt. Category</td>
						  						                        
						  						                            
						  						                            <td width="70">&nbsp;</td>
						  						                            <td width="60">&nbsp;</td>
						  						                        </tr>
						  						                    <tr>
						  						                        <td width="100" align="left"><asp:DropDownList id="ddlAdjustmentType" runat="server" CssClass="formField90">
						  						                        			        <asp:ListItem Text="Trade Debtor" Value="TradeDebtor"></asp:ListItem>
						  						                        </asp:DropDownList></td>
						  						                        <td width="90" align="left"><asp:TextBox ID="txtAdjustmntDate" runat="server" CssClass="formField width60"></asp:TextBox>
										<img alt="Click to Select" src="Images/calendar.gif" id="btnAdjustmntDate" style="cursor:pointer; vertical-align:middle;" />										 
								  <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnAdjustmntDate TargetControlID="txtAdjustmntDate" ID="calAdjustmntDate" runat="server">
								  </cc2:CalendarExtender></td>
						  						                        <td width="150" align="left" valign="bottom"><asp:TextBox ID="txtAdjustmentAmount" MaxLength="18" runat="server" CssClass="formFieldRight width60" onblur='javascript:FormatttedAmount(this.id);'></asp:TextBox>&nbsp;<input runat="server" type="checkbox" id="chkIncludeVAT" style="vertical-align:bottom;"/>&nbsp;<span class="txtNavigation">Include VAT</span></td>
						  						                        <td width="100" align="left"><asp:DropDownList id="ddlAdjustmentCategory" runat="server" CssClass="formField90">
                                                                            <asp:ListItem Text="Debit" Value="Debit"></asp:ListItem>
						  						                        	<asp:ListItem Text="Credit" Value="Credit"></asp:ListItem>
						  						                        </asp:DropDownList></td>
						  						                         <td width="70">
						  						                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                                                    <asp:LinkButton ID="lnkManualAdjustment" causesValidation=False OnClick='ManualAdjustment'  runat="server" CssClass="txtButtonRed" Text="Submit" ></asp:LinkButton>
                                                                </div>
          							                            </td>
						  						                         <td width="60">
						  						                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                                                    <asp:LinkButton ID="lnkCancel" causesValidation=False runat="server" CssClass="txtButtonRed" Text="Cancel" ></asp:LinkButton>
                                                                </div>
          							                            </td>   
						  						                        </tr>
						  						                    </table>
             <br />
             <table width="100%" cellpadding="0" cellspacing="0">
						 <tr align="center">
						  <td><asp:Label ID="lblMsgCheckBox" CssClass="HeadingRed" runat="server" Visible="false"></asp:Label></td>
						 </tr>
						</table>
						<br />
		<hr style="background-color:#993366;height:5px;margin:0px;" />
						<table width="100%" align=center border="0" cellspacing="0" cellpadding="0">
           		 <tr>
            		<td>
						  <asp:GridView ID="gvInvoices" runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
						   PagerSettings-Mode=NextPreviousFirstLast PagerSettings-Position=Top Width="100%" DataSourceID="ObjectDataSource1" AllowSorting="true" PagerSettings-Visible="true" >          
						   <EmptyDataTemplate>
               				<table  width="100%" border="0" cellpadding="10" cellspacing="0">					
									<tr>
											<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
											</td>
									</tr>				
							</table>
                  
               				</EmptyDataTemplate>
               
               				
							<PagerTemplate>
                   
            						<table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
										<tr>																				
												<td width="10">&nbsp;</td>												  
						  						<td runat="server" id="tdSelectAll" enableviewstate=true width="90">
						  						            <table>
						  						               <tr>
						  						                
          							                            <td width="10">&nbsp;</td>
          							                            
          							                           </tr>
          							                         </table>
												</td>				
												<td>
														<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  							<tr>
																<td align="right" >		
																		<TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
																				<TR>
																					<td align="right" valign="middle">
											 											 <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right" runat="server" class="formLabelGrey"></div>
																					</td>
																					<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
																						<table width="100%" border="0" cellpadding="0" cellspacing="0">
																							<tr>
																								<td align="right" width="36">
																										<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																											<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
																										  </div>	
																											<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																												<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
																											</div>																							   
																								</td>
																								<td width="50" align="center" style="margin-right:3px; ">													
																									<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField" ID="ddlPageSelector" runat="server" AutoPostBack="true">
																									</asp:DropDownList>
																								</td>
																								<td width="30" valign="bottom">
																									<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																									 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
																									</div>
																									<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																									 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
																									</div>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</TR>
																			</TABLE>
																	</td>
								  								</tr>
															</table>
						  								</td>
												</tr>
											</table> 
                   
        						</PagerTemplate> 
									   <Columns>  
												 <asp:TemplateField HeaderText="<input type='checkbox' id='chkSelectAll' runat='server' onclick=CheckAll(this,'Check') value='checkbox'  valign='middle'></input>" >               
									   <HeaderStyle CssClass="gridHdr" Width="30px"    Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left   Width="15px"  CssClass="gridRow"/>
									   <ItemTemplate>   
											<input type=hidden runat=server id='hdnInvoiceNo'  value='<%#Container.DataItem("InvoiceNO") %>'/>
											<input type=hidden runat=server id='hdnSIBusinessArea'  value='<%#Container.DataItem("BusinessArea") %>'/>
											<asp:CheckBox ID='Check'  runat='server' ></asp:CheckBox>
											   
									   </ItemTemplate>
									   </asp:TemplateField>  
															
										<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="140px"   HeaderStyle-CssClass="gridHdr" DataField="InvoiceNo" SortExpression="InvoiceNo" HeaderText="Sales Invoice Number" />                
										<asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="150px" HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="Date Issued" />                                    
										<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="CreditTerms" SortExpression="CreditTerms" HeaderText="Credit Terms" /> 
										
										
										<asp:TemplateField SortExpression="Total" HeaderText="Invoice Amount" >               
									   <HeaderStyle CssClass="gridHdr" Width="200px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
									   <ItemTemplate>  
									   <input type=hidden runat=server id='hdnSIAmount'  value='<%#Container.DataItem("Total") %>'/> 
										<%#FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   <asp:TemplateField SortExpression="Discount" HeaderText="Discount" >               
									   <HeaderStyle CssClass="gridHdr" Width="90px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="90px"  CssClass="gridRow"/>
									   <ItemTemplate>  
                                                <%#IIf(Container.DataItem("Discount"), "Y", "N")%>
									   </ItemTemplate>
									   </asp:TemplateField>
														
										  
										</Columns>
									   
										 <AlternatingRowStyle    CssClass=gridRow />
										 <RowStyle CssClass=gridRow />            
										<PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
										<HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
										<FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
								
						    </asp:GridView>
				 		</td>
            		</tr>
            </table>
						<asp:ObjectDataSource   ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.CreateAdjustment" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
			 <!-- InstanceEndEditable -->
			 </td>
         </tr>
         </table>
      </div>

</asp:Content>
