
Partial Public Class ServicePartnerPaymentStatement
    Inherits System.Web.UI.Page
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents lnkBackToListing As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents tdBackToListing As System.Web.UI.HtmlControls.HtmlTableCell


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
            Case ApplicationSettings.CountryUK
                ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
        End Select

        generateReport()

        If Not IsPostBack Then
        End If

    End Sub

    ''' <summary>
    ''' Function to generate the report.
    ''' </summary>
    ''' <remarks>Pratik Trivedi - 19 Nov, 2008</remarks>
    Private Sub generateReport()
        Dim languageCode As String = ""

        'pnlExportReport.Visible = True
        ReportViewerOW.ShowParameterPrompts = False
        ReportViewerOW.ShowToolBar = True
        Dim reportSErverPath As Uri = New Uri(ApplicationSettings.ReportServerPath)
        ReportViewerOW.ServerReport.ReportServerUrl = reportSErverPath
        ReportViewerOW.ServerReport.ReportServerCredentials = New CustomReportCredentials.CustomReportCredentials(ApplicationSettings.ReportServerUserName, ApplicationSettings.ReportServerPassword, ApplicationSettings.ReportServerDomain)
        Dim objParameter(1) As Microsoft.Reporting.WebForms.ReportParameter

        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/ServicePartnerPaymentsReport"

        objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)

        'Language parameters
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                languageCode = "de-DE"
            Case ApplicationSettings.CountryUK
                languageCode = "en-GB"
        End Select
        objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)
        ReportViewerOW.ServerReport.SetParameters(objParameter)
        ReportViewerOW.ServerReport.Refresh()
    End Sub
End Class