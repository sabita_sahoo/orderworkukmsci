<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : OrderMatch" CodeBehind="WOOrderMatch.aspx.vb" Inherits="Admin.WOOrderMatch" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>




 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


	        
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->  
			
			<input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />
			<!-- WO Summary -->
			<table id="DGWorkOrders" width="100%" border="0" cellpadding="5" cellspacing="0">
			  <tr align="left" valign="top" >
				<td  class="gridHdr gridBorderRB">WO No</td>
				<td  class="gridHdr gridBorderRB">Submitted</td>
				<td  class="gridHdr gridBorderRB">Last Sent</td>
				<td  class="gridHdr gridBorderRB">Location</td>
				<td  class="gridHdr gridBorderRB">Contact</td>
				<td  class="gridHdr gridBorderRB">WO Title</td>
				<td  class="gridHdr gridBorderRB">WO Start</td>
				<td  class="gridHdr gridBorderRB">Wholesale Price</td>
				<td  class="gridHdr gridBorderRB">Portal Price</td>
				<td  class="gridHdr gridBorderRB">Status</td>
				<td  class="gridHdrHighlight gridBorderB">&nbsp;</td>
			  </tr>
			 <tr align="left">
			 	<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label></td>
				<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOSubmitted"></asp:Label></td>
				<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOLastSent"></asp:Label></td>
				<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOLoc"></asp:Label></td>
				<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOContact"></asp:Label></td>
				<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOTitle"></asp:Label></td>
				<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOStart"></asp:Label></td>
				<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOPriceWP"></asp:Label></td>
				<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOPricePP"></asp:Label></td>
				<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="gridRowHighlight" ID="lblWOStatus"></asp:Label></td>
				<td  class="gridRow gridBorderB"><a id="WODetails" runat="server" ><img src="Images/Icons/ViewDetails.gif" alt="View Details" width="16" height="16" hspace="2" vspace="0" border="0"></a> </td>				
			  </tr>
			</table>
			
            <!-- Search Panel -->   
			<asp:UpdatePanel ID="UpdatePanelOrderMatch" runat="server">
			<ContentTemplate>

			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="15" height="30">&nbsp;</td>
                <td height="30" width="63%" ><strong>Search for Suppliers </strong></td>
                <td width="15" height="30">&nbsp;</td>
                <td width="250" height="30" valign="top">&nbsp;</td>
                <td width="15" height="30">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr id="trAreaOfExp" runat="server">
                    <td> 
						<span class="formTxt">Choose Areas Of Expertise Site</span>
						<asp:RadioButtonList id="rdAOEList" RepeatDirection="Horizontal" AutoPostBack="true" CssClass="formTxt"  runat="server">
						   <asp:ListItem  Value="1" selected="true" style="display:none;" ><span class="formTxt">Order Work UK</span></asp:ListItem>
						<%--   <asp:ListItem  Value="2"><span class="formTxt">Skills Finder UK</span></asp:ListItem>--%>
						</asp:RadioButtonList>								 

					</td>
                  </tr>

                  <tr>
                    <td><%@ Register TagPrefix="uc1" TagName="UCAOE" Src="~/UserControls/Admin/UCTableAOE.ascx" %>
                                  <uc1:UCAOE id="UCAOE1" runat="server"></uc1:UCAOE> </td>
                  </tr>
                    
                  <tr style=" visibility:hidden">
                    <td>
                    <br/>
                    <span class="formTxt">Select Regions</span></td>
                  </tr>
                  <tr style=" visibility:hidden">
                    <td>
                            <%@ Register TagPrefix="uc3" TagName="UCDualListBox" Src="~/UserControls/UK/UCDualListBox.ascx" %>
								 <uc3:UCDualListBox id="UCRegions" runat="server"></uc3:UCDualListBox></td>
                  </tr>
                
				  
                  <tr>
                    <td><asp:Label runat="server" ID="lblSearchCriteria" CssClass="txtListing"></asp:Label></td>
                  </tr>
				  <tr>
                    <td><asp:Label runat="server" ID="lblInvalidPostCode" CssClass="bodytxtValidationMsg" Text="There is an issue with the postcode information provided with this work order." Visible="false"></asp:Label></td>
                  </tr>
				  <tr>
                    <td>&nbsp;</td>
                  </tr>
                </table></td>
                <td width="15" height="30">&nbsp;</td>
                <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr>
                    <td height="20" valign="bottom"><span class="formTxt">Company Name</span></td>
                  </tr>
				    <tr>
                    <td><asp:TextBox ID="txtCompanyName" CssClass="formField200" runat="server" /></td>
                  </tr>
                  <tr>
                    <td height="20" valign="bottom"><span class="formTxt">Keyword</span></td>
                  </tr>				
                  <tr>
                    <td><asp:TextBox ID="txtKeyword" CssClass="formField200" runat="server" /></td>
                  </tr>
                  <tr>
                    <td height="20" valign="bottom"><span class="formTxt">PostCode</span></td>
                  </tr>
                  <tr>
                    <td><asp:TextBox ID="txtPostCode" CssClass="formField200" runat="server" /></td>
                  </tr>
				  <tr>
                    <td height="20" valign="bottom"><span class="formTxt">No. of Employees</span></td>
                  </tr>
				   <tr style="height:25px;">
                    <td><asp:DropDownList ID="ddlNoOfEmployees" TabIndex="15" runat="server" CssClass="formField width150"></asp:DropDownList></td>
                  </tr>
                    <tr>
                    <td height="20" valign="bottom"><span class="formTxt">Category Type</span></td>
                  </tr>
				   <tr style="height:25px;">
                    <td><asp:DropDownList CssClass="formField width150" ID="drpCategoryType" runat="server">
                                  <asp:ListItem Text="Select Category Type" Selected="True" Value=""></asp:ListItem>
                                  <asp:ListItem Text="Internal" Value="Internal"></asp:ListItem>
                                  <asp:ListItem Text="External" Value="External"></asp:ListItem>
                                                          </asp:DropDownList></td>
                  </tr>
                   <tr>
                              <td>&nbsp;</td>
                            </tr>
                  <tr>
                    <td><br/>
                    <span class="formTxt">Select Company Accreditations</span></td>
                  </tr>

                   <tr>
                       <td height="20" valign="bottom"><span class="formTxt">Skills (Like to Like Search)</span></td>
                   </tr>
                   <tr>
                        <td><asp:TextBox tabIndex="19" ID="txtSkillKeywords" CssClass="formField200" runat="server" Enabled="false"></asp:TextBox></td>
                   </tr>

                  <tr>
                    <td><%--<%@ Register TagPrefix="uc3" TagName="UCDualListBox" Src="~/UserControls/UK/UCDualListBox.ascx" %>
								 <uc3:UCDualListBox id="UCCompanyAccreditations" runat="server"></uc3:UCDualListBox>--%>
                    <%@ Register TagPrefix="uc1" TagName="UCAccreds" Src="~/UserControls/Admin/UCAccreditationForSearch.ascx" %>
                                <uc1:UCAccreds id="UCAccreditations" runat="server"></uc1:UCAccreds>                    
                    </td>
                  </tr>
                   <tr>
                              <td>&nbsp;</td>
                            </tr>
                 <tr class="smallText">
                    <td style="padding-left:5px;">
                    <b>DBS:</b>&nbsp;
                    <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedEnhanced" runat="server" GroupName="CRBChecked" Text="Enhanced" TextAlign="right"></asp:RadioButton>
                    <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedYes" runat="server" GroupName="CRBChecked" Text="Basic" TextAlign="right"></asp:RadioButton>
                    <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedNo" runat="server" GroupName="CRBChecked" Text="No" Checked =true TextAlign="right"></asp:RadioButton>
                        <%-- <asp:CheckBox ID="chkEngCRBChecked" TabIndex="13" runat="server" CssClass="formTxt"
                            TextAlign="right" Text="Engineer DBS Checked"></asp:CheckBox>--%>
                    </td>
                </tr>
                  <tr >
                    <td><asp:CheckBox id="chkbxHideTestAccount" tabIndex="19" runat="server" CssClass="formTxt" TextAlign="right"
             		Text="Hide Test Account" Checked="true" AutoPostBack="True"></asp:CheckBox>		</td>
                  </tr>
                
                  <tr>
                    <td><asp:CheckBox id="chkMinWOValue" tabIndex="6" runat="server" CssClass="formTxt" TextAlign="right"
													Text="Exclude suppliers with min WO Value > Price"></asp:CheckBox></td>
                  </tr>
				   <tr>
                    <td><asp:CheckBox id="chkShowSupplierWithCW" tabIndex="7" runat="server" CssClass="formTxt" TextAlign="right"
													Text="Show suppliers with min one closed WO"></asp:CheckBox></td>
                  </tr>
                  <tr runat="server" visible="false">
                    <td><asp:CheckBox id="chkRefChecked" tabIndex="8" runat="server" CssClass="formTxt" TextAlign="right"
																					Text="Reference Checked"></asp:CheckBox></td>
                  </tr>
				    <tr>
                    <td><asp:CheckBox id="chkShowAll" tabIndex="9" runat="server" CssClass="formTxt" TextAlign="right"
													Text="Show All" Checked="true"></asp:CheckBox></td>
                  </tr>	
				   
				  <tr class="smallText" style="display:none;">
                    <td><asp:CheckBox id="chkCRBChecked" tabIndex="10" runat="server" CssClass="formTxt" TextAlign="right"
																					Text="DBS Checked"></asp:CheckBox></td>
                  </tr>
                  <tr>
                    <td><asp:CheckBox id="chkUKSecurity" tabIndex="11" runat="server" CssClass="formTxt" TextAlign="right"
																					Text="UK Security Clearance"></asp:CheckBox></td>
                  </tr>
                  <tr>
                    <td><asp:CheckBox id="chkCSCS" tabIndex="12" runat="server" CssClass="formTxt" TextAlign="right"
																					Text="CSCS"></asp:CheckBox></td>
                  </tr>
				
                  <%--'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch--%>
                  <tr class="smallText">
                    <td><asp:CheckBox id="chkExcelPartner" tabIndex="13" runat="server" CssClass="formTxt" TextAlign="right"
																					Text="Excel Partner"></asp:CheckBox></td>
                  </tr>
                    <tr class="smallText">
                    <td><asp:CheckBox id="chkEPCP" tabIndex="13" runat="server" CssClass="formTxt" TextAlign="right"
																					Text="EPCP"></asp:CheckBox></td>
                  </tr>
                  <tr>
                    <td><asp:CheckBox id="chkEngUKSecurity" tabIndex="14" runat="server" CssClass="formTxt" TextAlign="right"
																					Text="Engineer UK Security Check"></asp:CheckBox></td>
                  </tr>
                  <tr>
                    <td><asp:CheckBox id="chkEngCSCS" tabIndex="15" runat="server" CssClass="formTxt" TextAlign="right"
																					Text="Engineer CSCS"></asp:CheckBox></td>
                  </tr>
                  <tr>
                    <td><asp:CheckBox id="chkEngRightToWork" tabIndex="16" runat="server" CssClass="formTxt" TextAlign="right"
																					Text="Engineer Right to work in UK"></asp:CheckBox></td>
                  </tr>
                  <tr>
                    <td><asp:CheckBox id="chkEngProofOfMeeting" tabIndex="13" runat="server" CssClass="formTxt" TextAlign="right"
             		                Text="Engineer with Proof of meeting" Checked="false"></asp:CheckBox></td>
                  </tr>
				 <tr>
                    <td><asp:CheckBox id="chkFavSupplier" tabIndex="17" runat="server" CssClass="formTxt" TextAlign="right"
																					Text="Favourite Suppliers"></asp:CheckBox></td>
                  </tr>	
                  <tr runat="server">
                    <td><asp:CheckBox id="chkApproved" tabIndex="18" runat="server" CssClass="formTxt" TextAlign="right" Checked="false" Enabled = "false"
																					Text="Show Only Approved Supplier"></asp:CheckBox></td>
                  </tr>
                  
                  <tr id="trFindNearest" runat="server">
                    <td><asp:CheckBox id="chkNearestTo" tabIndex="20" runat="server" CssClass="formTxt" TextAlign="right"
             		Text="Find Nearest" Checked="false" AutoPostBack="True"></asp:CheckBox>		</td>
                  </tr>	
				  
				   <tr>
                    <td>&nbsp;</td>
                  </tr>			  
				  <tr style="height:25px;">
                    <td><div id="divNearestTo" runat=server visible="false" style="padding-left:4px;"><asp:DropDownList ID="ddlNearestTo" TabIndex="10" runat="server" CssClass="formField width150">
						 <asp:ListItem Text="Upto 10 Miles" Value="10"></asp:ListItem>
                          <asp:ListItem Text="Upto 20 Miles" Value="20"></asp:ListItem>
						 <asp:ListItem Text="Upto 30 Miles" Value="30"></asp:ListItem>
						 <asp:ListItem Text="Upto 50 Miles" Value="50"></asp:ListItem>
                         <asp:ListItem Text="Upto 100 Miles" Value="100"></asp:ListItem>
                         </asp:DropDownList></div></td>
                  </tr>
				   <tr class="smallText">
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td class="paddingL3"><asp:Button CssClass="formTxt12" ID="btnSearch" runat="server" Text="OrderMatch" Width="194" Height="22" /></td>
                  </tr>
                </table></td>
                <td>&nbsp;</td>
              </tr>
            </table>
			<!--  Supplier Listing: Starts here-->
				
				
				<asp:Panel ID="pnlSearchResult" runat=server Visible="false">
				
				  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="15">&nbsp;</td>
					    <td>
					  
         		<table width="100%"   border="0" cellspacing="0" cellpadding="0">
          		  <tr>
          		    <td style="padding-left:5px">
					<table width="100%" style="height:35px;"  border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblSendMail" runat="server">
					<tr>
					<td width="220" align="left" >
					<table border="0" cellpadding="0" width="100%" cellspacing="0" bgcolor="#e6e6e6">
					  <tr>
						<td width="5"><img height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5" /></td>
						<td class="txtListing">
						  <asp:LinkButton id="btnSend" class="txtListing" runat="server"> <strong><img src="Images/Icons/Icon-SpecialistSmall.gif" width="12" height="16" hspace="5" vspace="3" align="middle"  border="0" /> &nbsp;Send WorkOrder to Suppliers </strong></asp:LinkButton>
						</td>
						<td width="5"><img height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5" /></td>
					  </tr>
					</table>
                    
					</td>
                    <td  width="320" align="left" id="tdSendAndAutoaccept" runat="server">
                    <table border="0" cellpadding="0" width="100%" cellspacing="0" bgcolor="#e6e6e6">
					  <tr>
						<td width="5"><img height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5" /></td>
						<td class="txtListing">
						  <asp:LinkButton id="btnSendAndAutoaccept" class="txtListing" runat="server"> <strong><img src="Images/Icons/Accept.gif" width="12" height="16" hspace="5" vspace="3" align="middle"  border="0" /> &nbsp;Send and autoaccept WorkOrder on behalf of SP</strong></asp:LinkButton>
						</td>
						<td width="5"><img height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5" /></td>
					  </tr>
					</table>
                    </td>
						<td align="left"><asp:label runat=server id="lblMsg" cssClass="bodytxtValidationMsg"></asp:label></td>
						<td runat="server" valign="bottom"  id="tdSortBy" width="120" align="left" class="txtWelcome">
							<asp:DropDownList style="margin-left:4px;"  CssClass="formField105" ID="ddlSortBy"  runat="server" AutoPostBack="false">
									<asp:ListItem Text="Sort By" Value=""></asp:ListItem>
									<asp:ListItem Text="Favourite Supplier" Value="isFavourite"></asp:ListItem>									
                           </asp:DropDownList>
							</td>
							<td runat="server"  valign="middle" id="tdSort" width="50" align="left" class="txtWelcome"><div id="divButton" style="width: 50px;  ">
								<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>								
								<asp:LinkButton OnClick="Sort" class="buttonText" style="cursor:hand;text-align:center" id="btnSort"  runat=server><strong>Sort</strong></asp:LinkButton>							
								<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
							</div></td>
						<td width="10">&nbsp;</td>
					</tr>
					
				</table></td>
        		    </tr>
          		  <tr>
           		 <td>
           		  
				 	<asp:GridView ID="gvSupplier"  runat="server" AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" BorderColor="White"  
                CssClass="gridRow1" EnableViewState="true"  Width="100%" AllowSorting="true" AllowPaging="true" HeaderStyle-HorizontalAlign="Left" PageSize="25" PagerSettings-Mode="NextPreviousFirstLast" PagerSettings-Position="Top"   >          
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               <PagerTemplate>
                <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>
						
						<td id="tdsetType2" align="left" width="121"></td>
						<td>
						<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
  					    <tr>
							<td align="right" >
								<TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
									<TR>
									<td align="right" valign="middle">
										  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
									</td>
									<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td align="right" width="36">
										<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
											<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
										</div>	
										<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
											<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
										</div>																							   
										</td>
										<td width="50" align="center" style="margin-right:3px; ">													
                                            <asp:DropDownList style="margin-left:4px;"  CssClass="formField40 marginLR5" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" ID="ddlPageSelector" runat="server" AutoPostBack="true" EnableViewState="true">
                                            </asp:DropDownList>
                                            
										</td>
										<td width="30" valign="bottom">
											<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
												 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
											</div>
											<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
												 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
											</div>
										</td>
									</tr>
									</table>
									</td>
									</TR>
									</TABLE>
								</td>
							  </tr>
							</table>
						  </td>
						</tr>
					</table>               	  
               </PagerTemplate>
               <Columns>  
			   
			   <asp:TemplateField HeaderStyle-CssClass="gridHdr" ItemStyle-Width="15px" ItemStyle-Wrap="true"
                 ItemStyle-HorizontalAlign="left" ItemStyle-CssClass="gridRow1" HeaderText="<input id='chkUpdateAllSkill' onClick=CheckAll(this,'CheckSupplier') type='checkbox' name='chkUpdateAll' />">
				 <ItemTemplate > 
				 		<asp:CheckBox ID="CheckSupplier" Runat="server" ></asp:CheckBox> 
						<INPUT id="hdnID" type="hidden" name="hdnID" runat="server" value='<%# Container.DataItem("ContactID") %>'> 
                        <INPUT id="hdnCompanyID" type="hidden" name="hdnCompanyID" runat="server" value='<%# Container.DataItem("CompanyID") %>'> 
                        <INPUT id="hdnAllowJobAcceptance" type="hidden" name="hdnAllowJobAcceptance" runat="server" value='<%# Container.DataItem("AllowJobAcceptance") %>'> 
  				 </ItemTemplate>
               </asp:TemplateField>
			   
			    <asp:TemplateField SortExpression="CompanyName"  HeaderText="Company">               
               <HeaderStyle CssClass="gridHdr" Width="70px"   Wrap="true" />
               <ItemStyle Wrap="true" Width="70px" HorizontalAlign=Left CssClass="gridRow1 "/>
               <ItemTemplate>                           
			   			<table width="100%" cellspacing="0" cellpadding="0" >
							<tr>
								<td width="160" id="tdRating" visible="true" runat="server" >
									<div style='visibility:hidden;position:absolute;margin-left:144px;' id=divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%> >
											<table width="250" height="100" bgcolor="#D6D7D6" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;" >
												<tr>
												  <td class='bgWhite' align='left'><strong>Admin:</strong> <%# iif(Container.DataItem("Name") = " ","--",Container.DataItem("Name"))%>  </td>
												</tr>
												<tr>
												  <td class='bgWhite' align='left'><strong>Phone No:</strong> <%# iif(Container.DataItem("Phone") = " ","--",Container.DataItem("Phone"))%>  </td>
												</tr>
												<tr>
												  <td class='bgWhite' align='left'><strong>Mobile No:</strong> <%# iif(Container.DataItem("Mobile") = " ","--",Container.DataItem("Mobile"))%>  </td>
												</tr>
												<tr>
												  <td class='bgWhite' align='left'><strong>Email Id:</strong> <%# iif(Container.DataItem("Email") = " ","--",Container.DataItem("Email"))%>  </td>
												</tr>
											</table>
									</div>
									<span style='cursor:hand; font-family: Geneva, Arial, Helvetica, sans-serif; font-size:11px; color:#1D85BA ' onMouseOut=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%>','visible')>
										<%#  Container.Dataitem("CompanyName") + " " + IIF((Container.Dataitem("isFavourite")) = "True", "<img src='Images/Star.gif', alt='Favourite Supplier', title='Favourite Suppplier'>", "") %> 
									</span> 
								</td>
							</tr>
						</table>

               </ItemTemplate>
               </asp:TemplateField> 

                <asp:BoundField ItemStyle-CssClass="gridRow1" ItemStyle-Width="75px"  HeaderStyle-CssClass="gridHdr"  DataField="Location" SortExpression="Location" HeaderText="Location" />
                
                <asp:TemplateField SortExpression="RefCheckedDate"  HeaderText="Reference Checked">               
                    <HeaderStyle CssClass="gridHdr" Width="50px"  Wrap=true />
                        <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="gridRow1"/>
                            <ItemTemplate>                           
                                <%# SetRefCheckedStatus(Container.DataItem("RefCheckedDate"))%> 
                            </ItemTemplate>
                </asp:TemplateField>  
                
				<asp:BoundField ItemStyle-CssClass="gridRow1" ItemStyle-Width="50px"  HeaderStyle-CssClass="gridHdr"  DataField="Status" SortExpression="Status" HeaderText="Status" />          
               <asp:BoundField ItemStyle-CssClass="gridRow1" ItemStyle-Width="50px"  HeaderStyle-CssClass="gridHdr"  DataField="PosRatingPercentage" HeaderText="Rating" /> 
               
               
			   
			   <asp:TemplateField SortExpression="Distance"  HeaderText="Distance (Miles)">               
               <HeaderStyle CssClass="gridHdr" Width="50px"  Wrap=true />
               <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="gridRow1"/>
               <ItemTemplate>                           
                       <%#IIf(CInt(Container.DataItem("Distance")) = 99999999, "Invalid", Container.DataItem("Distance").ToString )%> 
                       <input id="hdnDist" runat="server" type="hidden" value='<%#IIf(CInt(Container.DataItem("Distance")) = 99999999, 0, Container.DataItem("Distance").ToString )%>'  />   
               </ItemTemplate>
               </asp:TemplateField>
                  
			    <asp:TemplateField SortExpression="ActiveWos"  HeaderText="No. of WOs">               
               <HeaderStyle CssClass="gridHdr" Width="50px"  Wrap=true />
               <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="gridRow1"/>
               <ItemTemplate>          
					<%#IIf(Container.DataItem("ActiveWos") = "0" , " ", "<a style='cursor:hand; font-size:11px; color:#1D85BA ' runat=server id='ancActiveWos' href='AdminWOListing.aspx?" & getWOListingLink(Container.DataItem("BizDivID"), Container.DataItem("CompanyID"), Container.DataItem("ContactID"), Container.DataItem("ClassID"), Container.DataItem("StatusID"), Container.DataItem("RoleGroupID")) & "&CompID=" & Container.DataItem("CompanyId") & "'>" & Container.DataItem("ActiveWos") & "</a>" )%>
               </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField ItemStyle-CssClass="gridRow1" ItemStyle-Width="50px"  HeaderStyle-CssClass="gridHdr"  DataField="JobsCompleted" SortExpression="JobsCompleted" HeaderText="Jobs Completed" />
               <asp:BoundField ItemStyle-CssClass="gridRow1" ItemStyle-Width="50px"  HeaderStyle-CssClass="gridHdr"  DataField="SupplierLastJobDate" HeaderText="Date of last closed WO" />
               <asp:TemplateField ItemStyle-Width="35px">               
                <HeaderStyle  CssClass="gridHdr" />
                <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow1"/>
                    <ItemTemplate> 
                    <%#GetLinks(Container.DataItem("BizDivID"), Container.DataItem("CompanyID"), Container.DataItem("ContactID"), Container.DataItem("ClassID"), Container.DataItem("StatusID"), Container.DataItem("RoleGroupID"))%> 
					</ItemTemplate>
                </asp:TemplateField>    
                </Columns>
                 
                   <AlternatingRowStyle    CssClass=gridRow1 />
                 <RowStyle CssClass=gridRow1 />            
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />                      
        
              </asp:GridView>
			 </td>
            </tr>
            </table>  </td>
                      <td width="20">&nbsp;</td>
                    </tr>
                  </table>				
          </asp:Panel>
				
				<!--  Suppiler Listing: Ends here--> 
             <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="20">&nbsp;</td>
                <td valign="top">
                    <span align="center"><b>
                    <asp:label runat="server" id="lblMsgSent" ></asp:label>
                  </b></span></td>
                <td width="20">&nbsp;</td>
              </tr>
            </table>
           
			<INPUT name="hdnRegionValue" type="hidden" id="hdnRegionValue" runat="server">
            <INPUT name="hdnProductIds" type="hidden" id="hdnProductIds" runat="server">
			<INPUT name="hdnRegionName" type="hidden" id="hdnRegionName" runat="server">
			<INPUT name="hdnVendorName" type="hidden" id="hdnVendorName" runat="server">
			<INPUT name="hdnVendorValue" type="hidden" id="hdnVendorValue" runat="server">
			</ContentTemplate>
  			
		</asp:UpdatePanel>

		 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelOrderMatch" runat="server">
        	<ProgressTemplate>
            	<div>
                	<img   src="Images/indicator.gif" />
	                Please Wait...
    	        </div>      
        	</ProgressTemplate>
		</asp:UpdateProgress>
		<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePanelOrderMatch" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />

<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      
      </asp:Content>