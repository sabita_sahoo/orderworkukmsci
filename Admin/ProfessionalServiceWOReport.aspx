﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProfessionalServiceWOReport.aspx.vb" Inherits="Admin.ProfessionalServiceWOReport"  MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Service Report" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
	<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
 <%@ Register Assembly="WebLibrary" Namespace="WebLibrary" TagPrefix="lib" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
 <%@ Import Namespace="System.Web.Script.Services" %>
    <%@ Import Namespace="System.Web.Services" %>
  

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager> 
  <style type="text/css">select{width:138px;}</style>
  <script type="text/javascript">
      //   Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range
      function SelectAutoSuggestRow(inp, data) {
          inp.value = data[0];
          var x = document.getElementById("ctl00_ContentPlaceHolder1_hdnSelectedCompanyName").value = inp.value;
          document.getElementById("ctl00_ContentPlaceHolder1_hdnbtnSelectedCompany").click();
      }
</script>
  <div id="divContent">
   <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
                 <tr>
                <td valign="top">
                    <!-- InstanceBeginEditable name="EdtContent" -->
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>&nbsp;</td>
                            <td  class="HeadingRed"><strong>Professional Service WO Report</strong></td>
                        </tr>

                    </table>
                     <%--//   Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range --%>
                     <asp:Button runat="server" ID="hdnbtnSelectedCompany" OnClick="hdnbtnSelectedCompany_Click"  Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
			      <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="5" border="0" style="padding-top: 15px;"">
                                <tr><td></td><td></td><td></td><td></td><td  width="100px"></td><td></td><td width="128px"></td><td align="left" ></td></tr>
                                <tr>
                                      <td class="formTxt" valign="top" align="left" width="185px">Submitted Date From (dd/mm/yyyy)</td>
                                    <td class="formTxt" valign="top" align="left" width="170px">Submitted Date To (dd/mm/yyyy)</td>
                                     <td class="formTxt" valign="top" align="left" width="145px">Start Date (dd/mm/yyyy)</td>                                    
                                     <td class="formTxt" valign="top" align="left" width="145px">End Date (dd/mm/yyyy)</td>
                                      <td class="formTxt" valign="top" align="left" width="120px">PO Number </td>
                                                                       
                                    <td class="formTxt" valign="top" align="left" width="138px">Status </td>
                                    </tr>
                                <tr>
                                    <td >
                                        <asp:TextBox ID="txtSubmittedDateFrom" TabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
                                                    <img alt="Click to Select" src="Images/calendar.gif" id="btnSubmittedDateFrom" style="cursor: pointer; vertical-align: top;" />
                                                    <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnSubmittedDateFrom" TargetControlID="txtSubmittedDateFrom" ID="calSubmittedDateFrom" runat="server">
                                                    </cc2:CalendarExtender>

                                    </td>
                                     <td >
                                        <asp:TextBox ID="txtSubmittedDateTo" TabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
                                                    <img alt="Click to Select" src="Images/calendar.gif" id="btnSubmittedDateTo" style="cursor: pointer; vertical-align: top;" />
                                                    <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnSubmittedDateTo" TargetControlID="txtSubmittedDateTo" ID="calSubmittedDateTo" runat="server">
                                                    </cc2:CalendarExtender>
                                          
                             </div>

                                    </td>
                                    <td >
                                        <asp:TextBox ID="txtStartDate" TabIndex="2" runat="server" CssClass="formField105"></asp:TextBox>
                                                    <img alt="Click to Select" src="Images/calendar.gif" id="btnStartDate" style="cursor: pointer; vertical-align: top;" />
                                                    <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnStartDate" TargetControlID="txtStartDate" ID="calStartDate" runat="server">
                                                    </cc2:CalendarExtender>
                                        
                                    </td>
                                    <td >
                                        <asp:TextBox ID="txtEndDate" TabIndex="3" runat="server" CssClass="formField105"></asp:TextBox>
                                                    <img alt="Click to Select" src="Images/calendar.gif" id="btnEndDate" style="cursor: pointer; vertical-align: top;" />
                                                    <cc2:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnEndDate" TargetControlID="txtEndDate" ID="calEndDate" runat="server">
                                                    </cc2:CalendarExtender>
                                      

                                    </td>
                                   
                                    <td  width="100px">
                                        <asp:TextBox ID ="txtPONumber" TabIndex="4" runat="server" CssClass="formField105"></asp:TextBox>
			                   
                                                                               </td>
                                                                                  <td width="128px">
                                        <asp:DropDownList ID="drpStatus" runat="server" CssClass="formFieldGrey121"></asp:DropDownList>
                                    </td>  </tr><%-- Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range --%>
                                    <tr>
                                     <td class="formTxt" valign="top" align="left" width="160px">Company Name</td> 
                                 </tr><tr>
                                                                                
                                                                                <td>
                                                                                 <input id="hdnSelectedCompanyName" type="hidden" name="hdnSelectedCompanyName" runat="server" value="" />
			                    <lib:Input ID="txtCompanyName" runat="server" DataType="List"
				                    Method="GetCompanyName" CssClass="formField150" OnSelect='SelectAutoSuggestRow' SelectParameters='CompanyName' />
                                    </td>
                                   <%-- <td >
                                       <%@ register src="~/UserControls/Admin/UCSearchContact.ascx" tagname="UCSearchContact" tagprefix="uc1" %>
												<uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact> 
                                    </td>--%>
                                          
                                    <td align="left" >
                                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                    <asp:LinkButton ID="lnkBtnGenerateReport" CausesValidation="True" runat="server" CssClass="txtButtonRed" Text="Generate Report"></asp:LinkButton></td>
                                </tr>
                                <tr><td align="right" colspan="7">
                                 <%-- Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range <tr>--%>
                            <asp:Panel ID="pnlSelectedCompany" runat="server" Visible="false">
                      <div class="divStyleCompany" >
  <asp:Repeater ID="rptSelectedCompany" runat="server" >
    <ItemTemplate>        
        <div class="divStyle1 roundifyRefineMainBody"><span class="clsFloatLeft">
        <asp:Label ID="lblcompany" runat="server" Text=' <%#Eval("CompanyName")%> '></asp:Label></span>
        <span class="marginleft8 clsFloatLeft" >
        <asp:LinkButton ID="lnkbtnRemove" CommandName="Remove" CausesValidation="false" CommandArgument='<%#Container.DataItem("CompanyID") %>' runat="server" class="deleteButtonAccredation">
        x</asp:LinkButton></span>
      </div>
    </ItemTemplate>    
  </asp:Repeater> 
  <asp:Label runat="server" ID="lblSelectedCompany" CssClass="txtOrange" ForeColor="Red" style="clear: both;display: block;padding-left: 10px;" Text=""></asp:Label>
   
  </div>
  </asp:Panel>
                                
                                </td></tr>
                                <tr><td align="left" colspan="8"><div> <asp:Label runat="server" ID="lblError" CssClass="txtOrange" ForeColor="Red" Text=""></asp:Label></div></td></tr>
                            </table>
                           


                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="lnkBtnGenerateReport" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <div class="gridText">
                                <img align="middle" src="Images/indicator.gif" />
                                <b>Fetching Data... Please Wait</b>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td style="padding-top: 20px;">
                                <rsweb:ReportViewer ID="ReportViewerOW" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="830px" ProcessingMode="Remote" Width="100%" bgColor="#FFFFFF">
                                    <ServerReport ReportServerUrl="" />
                                </rsweb:ReportViewer>
                            </td>
                        </tr>
                    </table>
                    <!-- InstanceEndEditable -->
                </td>


            </tr>
              </table>     

               
				     
      </div>



 </asp:Content>



 




	       
           
