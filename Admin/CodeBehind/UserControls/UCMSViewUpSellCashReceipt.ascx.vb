Partial Public Class UCMSViewUpSellCashReceipt
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs
    Protected WithEvents lblInvoiceType, lblAddress, lblCity, lblPostCode, lblCompNameInvoiceTo As Label
    Protected WithEvents lblInvoiceTo, lblCompanyName, lblInvoiceNo, lblInvoiceDate As Label
    Protected WithEvents lblDate, lblDescription As Label
    Protected WithEvents lblTotalAmount As Label
    Protected WithEvents lblAccountNo As Label
    Protected WithEvents lblTelNo, lblFaxNo, lblRefNumber, lblRegistartionNo, lblRegistartionNoLine2, lblShowFax As Label
    Protected WithEvents rptList As Repeater


#Region "Events"
    ''' <summary>
    ''' On page load a call to Ws is made to retrieve upsell receipt details for passed invoice number and bizdivid.
    ''' These details are received in a dataset. Using this dataset the fields on UpSell receipt are populated.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim dsUpSellReceiptAdviceDetails As DataSet = ws.WSFinance.MS_GetUpSellReceiptAdviceDetails(Request("BizDivId"), Request("invoiceNo"))


            If dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows.Count > 0 Then
                'Company Name
                If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("CompanyName"))) Then
                    lblCompanyName.Text = dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("CompanyName")
                End If
                'Address
                If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("Address"))) Then
                    lblAddress.Text = dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("Address")
                End If
                'OW city

                If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("City"))) Then
                    lblCity.Text = dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("City") & ","
                End If

                'OW post code
                If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("PostCode"))) Then
                    lblPostCode.Text = dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("PostCode")
                End If

                'OW Phone
                If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("Phone"))) Then
                    lblTelNo.Text = dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("Phone")
                End If
                'OW Fax
                If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("Fax"))) Then
                    If dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("Fax") <> "" Then
                        lblFaxNo.Text = dsUpSellReceiptAdviceDetails.Tables("tblOWAddress").Rows(0)("Fax")
                    Else
                        lblShowFax.Visible = False
                    End If
                End If
            End If


            If dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows.Count > 0 Then
                'Invoice To
                Dim str1 As String = ""
                Dim str2 As String = ""
                If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCFirstName"))) Then
                    str1 = dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCFirstName").ToString
                End If

                If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCLastName"))) Then
                    str2 = dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCLastName")
                End If

                str1 = str1 + " " + str2
                lblCompNameInvoiceTo.Text = str1.ToString

                'Address Invoice to
                If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCAddress"))) Then
                    If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCCounty"))) Then
                        If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCPostCode"))) Then
                            lblInvoiceTo.Text = dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCAddress") & "<br>" & IIf((dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCCity") <> ""), dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCCity") & ",", "") & " " & dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCCounty") & IIf(((dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCCounty") <> "") Or (dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCCity") <> "")), "<br>", "") & dsUpSellReceiptAdviceDetails.Tables("tblBillingAddress").Rows(0)("BCPostCode")
                        End If
                    End If
                End If

            End If


            If dsUpSellReceiptAdviceDetails.Tables("tblOrderWorkDetails").Rows.Count > 0 Then
                'Registration number
                lblRegistartionNo.Text = "Registered in England"

                If Not (IsDBNull(dsUpSellReceiptAdviceDetails.Tables("tblOrderWorkDetails").Rows(0)("CompanyRegNo"))) Then
                    lblRegistartionNoLine2.Text = "No.: " & dsUpSellReceiptAdviceDetails.Tables("tblOrderWorkDetails").Rows(0)("CompanyRegNo")
                End If
            End If

            If dsUpSellReceiptAdviceDetails.Tables("tblInvoices").Rows.Count > 0 Then
                rptList.DataSource = dsUpSellReceiptAdviceDetails.Tables("tblInvoices").DefaultView
                rptList.DataBind()

                Dim totalAmount As Decimal = 0

                Dim drow As DataRow
                For Each drow In dsUpSellReceiptAdviceDetails.Tables("tblInvoices").Rows
                    totalAmount += drow("Total")
                Next

                lblTotalAmount.Text = FormatCurrency(totalAmount, 2, TriState.True, TriState.True, TriState.False)
            End If

        End If
    End Sub
#End Region
End Class