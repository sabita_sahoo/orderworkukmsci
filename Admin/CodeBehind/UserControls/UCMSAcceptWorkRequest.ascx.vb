Partial Public Class UCMSAcceptWorkRequest
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs

#Region "Properties"
    ''' <summary>
    ''' Company id of the user performing action
    ''' </summary>
    ''' <remarks></remarks>
    Private _companyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property

    ''' <summary>
    ''' Contact id of the user performing action
    ''' </summary>
    ''' <remarks></remarks>
    Private _contactID As Integer
    Public Property ContactID() As Integer
        Get
            Return _contactID
        End Get
        Set(ByVal value As Integer)
            _contactID = value
        End Set
    End Property

    ''' <summary>
    ''' classid
    ''' </summary>
    ''' <remarks></remarks>
    Private _ClassID As Integer
    Public Property ClassID() As Integer
        Get
            Return _ClassID
        End Get
        Set(ByVal value As Integer)
            _ClassID = value
        End Set
    End Property

    ''' <summary>
    ''' Bizidvid
    ''' </summary>
    ''' <remarks></remarks>
    Private _BizdivID As Integer
    Public Property BizdivID() As Integer
        Get
            Return _BizdivID
        End Get
        Set(ByVal value As Integer)
            _BizdivID = value
        End Set
    End Property

    ''' <summary>
    ''' workorder id
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOID As Integer
    Public Property WOID() As Integer
        Get
            Return _WOID
        End Get
        Set(ByVal value As Integer)
            _WOID = value
        End Set
    End Property

    ''' <summary>
    ''' Viewer = Buyer / Admin
    ''' </summary>
    ''' <remarks></remarks>
    Private _Viewer As String
    Public Property Viewer() As String
        Get
            Return _Viewer
        End Get
        Set(ByVal value As String)
            _Viewer = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Populate the page
        If Not IsPostBack Then
            populateWoDetails()

            UCFileUpload1.BizDivID = BizdivID
            UCFileUpload1.Type = "StatementOfWork"
            UCFileUpload1.AttachmentForID = WOID
            UCFileUpload1.ClearUCFileUploadCache()
        End If
    End Sub

    ''' <summary>
    ''' Populate the wo details
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateWoDetails()
        Dim ds As DataSet
        ds = ws.WSWorkOrder.MS_GetWODetails(WOID, Viewer)

        'Insert items in Goods Collection Dropdown
        Dim li As New ListItem
        li = New ListItem
        li.Text = "Customer's Home"
        li.Value = 1
        drpdwnGoodsCollection.Items.Insert(0, li)
        Dim li1 As New ListItem
        li1 = New ListItem
        li1.Text = "None Specified"
        li1.Value = 0
        drpdwnGoodsCollection.Items.Insert(0, li1)

        'Prepare the view of page
        SetFormView(ds)

        'Populate WO Summary
        If (ds.Tables("tblSummary").Rows.Count <> 0) Then

            'WO No
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("RefWOID")) Then
                'lblWONo.Text = ds.Tables("tblSummary").Rows(0).Item("WorkOrderId")
                lblWONo.Text = ds.Tables("tblSummary").Rows(0).Item("RefWOID")
            End If
            'Created
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateCreated")) Then
                lblCreated.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateCreated"), DateFormat.ShortDate)
            End If
            'Location
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Location")) Then
                lblLocation.Text = ds.Tables("tblSummary").Rows(0).Item("Location")
            End If
            'Created By
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("BuyerContact")) Then
                lblBuyerContact.Text = ds.Tables("tblSummary").Rows(0).Item("BuyerContact")
            End If
            'WO Title
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("WOTitle")) Then
                lblTitle.Text = ds.Tables("tblSummary").Rows(0).Item("WOTitle")
            End If
            'WO Start
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateStart")) Then
                lblWOStart.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
            End If
            'Proposed Price
            If Viewer = ApplicationSettings.ViewerAdmin Then
                If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice")) Then
                    lblProposedPrice.Text = FormatCurrency(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.True, TriState.False)
                End If
            ElseIf Viewer = ApplicationSettings.ViewerBuyer Then
                If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Value")) Then
                    lblProposedPrice.Text = FormatCurrency(ds.Tables("tblSummary").Rows(0).Item("Value"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If

            ViewState("WOValue") = CDec(lblProposedPrice.Text.Trim)
            ViewState("LastActionRef") = ds.Tables("tblSummary").Rows(0).Item("LastActionRef")
            ViewState("WPPrice") = ds.Tables("tblSummary").Rows(0).Item("WholesalePrice")
            'ViewState("WorkOrderId") = ds.Tables("tblSummary").Rows(0).Item("WorkOrderId")
            ViewState("WorkOrderId") = ds.Tables("tblSummary").Rows(0).Item("RefWOID")

            'Status
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Status")) Then
                lblStatus.Text = ds.Tables("tblSummary").Rows(0).Item("Status")
            End If
            'Category
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("WOCategory")) Then
                lblCategory.Text = ds.Tables("tblSummary").Rows(0).Item("WOCategory")
            End If
            'Start Date
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateStart")) Then
                lblStartDate.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
            End If
            'End Date
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateEnd")) Then
                lblEndDate.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate)
            End If
            'Estimated Time
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("EstimatedTime")) Then
                If ds.Tables("tblSummary").Rows(0).Item("EstimatedTime") <> "" Then
                    lblEstimatedTime.Text = ds.Tables("tblSummary").Rows(0).Item("EstimatedTime")
                Else
                    lblEstimatedTime.Text = ResourceMessageText.GetString("NoneSpecified")
                End If
            Else
                lblEstimatedTime.Text = ResourceMessageText.GetString("NoneSpecified")  '    "None Specified"
            End If
            'Supply Parts
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("SpecialistSuppliesParts")) Then
                If ds.Tables("tblSummary").Rows(0).Item("SpecialistSuppliesParts") = False Then
                    lblSupplyParts.Text = ResourceMessageText.GetString("No")    '   "No"
                Else
                    lblSupplyParts.Text = ResourceMessageText.GetString("Yes")    ' "Yes"
                End If
            End If
        End If



        If (ds.Tables("tblDetails").Rows.Count <> 0) Then
            'PO Number
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("PONumber")) Then
                If ds.Tables("tblDetails").Rows(0).Item("PONumber") <> "" Then
                    lblPONumber.Text = ds.Tables("tblDetails").Rows(0).Item("PONumber")
                Else
                    txtPONumber.Visible = True
                End If
            Else
                txtPONumber.Visible = True
            End If

            'JRS Number
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("JRSNumber")) Then
                If ds.Tables("tblDetails").Rows(0).Item("JRSNumber") <> "" Then
                    lblJRSNumber.Text = ds.Tables("tblDetails").Rows(0).Item("JRSNumber")
                Else
                    txtJRSNumber.Visible = True
                End If
            Else
                txtJRSNumber.Visible = True
            End If

            'Location of Goods (previously Business Division)
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("BusinessDivision")) Then
                If ds.Tables("tblDetails").Rows(0).Item("BusinessDivision") <> "" Then
                    lblBizDiv.Text = ds.Tables("tblDetails").Rows(0).Item("BusinessDivision")
                Else
                    drpdwnGoodsCollection.Visible = True
                End If
            Else
                drpdwnGoodsCollection.Visible = True
            End If

            'Product Name / Equipment Details
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("EquipmentDetails")) Then
                If ds.Tables("tblDetails").Rows(0).Item("EquipmentDetails") <> "" Then
                    lblProductName.Text = ds.Tables("tblDetails").Rows(0).Item("EquipmentDetails")
                    'Additional Product Name 
                    If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")) Then
                        If ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts") <> "" Then
                            lblProductName.Text = lblProductName.Text + "<BR>" + ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")
                        End If
                    End If
                Else
                    'Additional Product Name 
                    If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")) Then
                        If ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts") <> "" Then
                            lblProductName.Text = ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")
                        Else
                            txtProductName.Visible = True
                        End If
                    Else
                        txtProductName.Visible = True
                    End If

                End If
            Else
                If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")) Then
                    If ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts") <> "" Then
                        lblProductName.Text = ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")
                    Else
                        txtProductName.Visible = True
                    End If
                Else
                    txtProductName.Visible = True
                End If
            End If
            'Sales agent
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("SalesAgent")) Then
                If ds.Tables("tblDetails").Rows(0).Item("SalesAgent") <> "" Then
                    lblSalesAgent.Text = ds.Tables("tblDetails").Rows(0).Item("SalesAgent")
                Else
                    lblSalesAgent.Text = ResourceMessageText.GetString("NoneSpecified")
                End If
            Else
                lblSalesAgent.Text = ResourceMessageText.GetString("NoneSpecified")
            End If

            'Dress Code

            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("DressCode")) Then
                If ds.Tables("tblDetails").Rows(0).Item("DressCode") <> "" Then
                    lblDressCode.Text = ds.Tables("tblDetails").Rows(0).Item("DressCode")
                End If
            Else
                lblDressCode.Text = ResourceMessageText.GetString("NoneSpecified")  '    "None Specified"
            End If

            'Special Instructions
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("ClientSpecialInstructions")) Then
                lblSpecialInstructions.Text = ds.Tables("tblDetails").Rows(0).Item("ClientSpecialInstructions")
            End If

            'Long Desc
            If ds.Tables("tblDetails").Rows(0).Item("ClientScope").ToString.Trim = "" Then
                If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("WOLongDesc")) Then
                    lblLongDesc.Text = ds.Tables("tblDetails").Rows(0).Item("WOLongDesc")
                End If
            Else
                If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("ClientScope")) Then
                    lblLongDesc.Text = ds.Tables("tblDetails").Rows(0).Item("ClientScope")
                End If
            End If

            'Installation Time
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("AptTime")) Then
                If ds.Tables("tblDetails").Rows(0).Item("AptTime") <> "" Then
                    lblInstallTime.Text = " - " & ds.Tables("tblDetails").Rows(0).Item("AptTime")
                Else
                    lblInstallTime.Text = ResourceMessageText.GetString("NoneSpecified")  '    "None Specified"
                End If
            Else
                lblInstallTime.Text = ResourceMessageText.GetString("NoneSpecified")  '    "None Specified"
            End If
        End If

        'Buyer Supplier Info
        Dim RowNoBuyer As Integer
        Dim RowNoSupp As Integer
        If (ds.Tables("tblContacts").Rows.Count <> 0) Then
            If ds.Tables("tblContacts").Rows(0).Item("ContactType") = "Supplier" Then
                RowNoSupp = 0
                RowNoBuyer = 1
            ElseIf ds.Tables("tblContacts").Rows(0).Item("ContactType") = "Buyer" Then
                RowNoBuyer = 0
                RowNoSupp = 1
            End If

            'BUYER INFO
            lblBuyerContactLabel.Text = "Buyer Info:"
            'Name
            If Not IsDBNull(ds.Tables("tblContacts").Rows(RowNoBuyer).Item("Name")) Then
                lblClientContact.Text = ds.Tables("tblContacts").Rows(RowNoBuyer).Item("Name")
            End If
            'Buyer Company
            If Not IsDBNull(ds.Tables("tblContacts").Rows(RowNoBuyer).Item("Company")) Then
                lblClientCompany.Text = ds.Tables("tblContacts").Rows(RowNoBuyer).Item("Company")
                lblBuyerCompany.Text = ds.Tables("tblContacts").Rows(RowNoBuyer).Item("Company")
            End If
            'Buyer Address
            If Not IsDBNull(ds.Tables("tblContacts").Rows(RowNoBuyer).Item("Address")) Then
                lblClientAddress.Text = ds.Tables("tblContacts").Rows(RowNoBuyer).Item("Address")
            End If
            'Buyer Phone  
            If Not IsDBNull(ds.Tables("tblContacts").Rows(RowNoBuyer).Item("Phone")) Then
                lblClientPhone.Text = ds.Tables("tblContacts").Rows(RowNoBuyer).Item("Phone")
            End If
        Else
            ViewState("showSupplierInfo") = "False"
        End If

        'Attachments
        '******************************************************************************
        If Not Page.IsPostBack Then
            Dim flagPnlAttachment As Boolean = False
            Dim flagSWAttachment As Boolean = False
            If Not IsNothing(ds.Tables("tblAttachments")) Then
                If ds.Tables("tblAttachments").Rows.Count > 0 Then
                    Dim dv As DataView
                    dv = ds.Tables("tblAttachments").DefaultView

                    'Workorder
                    flagPnlAttachment = True
                    litAttachments.Text &= ResourceMessageText.GetString("WOAttachments")
                    dv.RowFilter = "LinkSource = 'WorkOrder'"
                    Dim drv As DataRowView
                    For Each drv In dv
                        litAttachments.Text &= "<span class='txtGreySmallSelected'><a class='footerTxtSelected' href='" + getlink(drv("FilePath")) + "' target='_blank'>" + drv("Name") + "</a><br>"
                    Next
                    litAttachments.Text &= "</span><br>"

                    'Statement of Works Attachments
                    dv.RowFilter = ""
                    dv.RowFilter = "LinkSource = 'StatementOfWork'"
                    If dv.Count > 0 Then
                        litSWAttachments.Text &= ResourceMessageText.GetString("SWAttachments")
                        For Each drv In dv
                            litSWAttachments.Text &= "<span class='txtGreySmallSelected'><a class='footerTxtSelected' href='" + getlink(drv("FilePath")) + "' target='_blank'>" + drv("Name") + "</a><br>"
                            flagSWAttachment = True
                        Next
                    End If
                    litSWAttachments.Text &= "</span>"
                End If
            End If
            If flagPnlAttachment = False Then
                pnlAttachments.Visible = True
                pnlSWBuyerAcceptForm.Visible = True
            End If

            If flagSWAttachment = True Then
                pnlSWBuyerAcceptForm.Visible = False
            End If
        End If
        '******************************************************************************

        'Wholesale Price is zero .. cannot accept work request
        If ViewState("WPPrice") = 0 Then
            lnkSubmit.Enabled = False
        End If

        'Spend Limit
        checkSpendLimit()
    End Sub

    ''' <summary>
    ''' Function to check if wo value exceeds the Spend limit
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub checkSpendLimit()
        Dim Str As String = ""
        ' If Session Variable existis
        If (Not IsNothing(Session("SpendLimit"))) Then
            If (Session("SpendLimit") < CDec(ViewState("WOValue"))) Then
                Str = ResourceMessageText.GetString("SpendLimitWR")
                Str = Str.Replace("<value>", FormatCurrency(CDec(Session("SpendLimit")), 2, TriState.True, TriState.True, TriState.False))

                lblError.Text = Str
                divValidationMain.Visible = True

                lnkSubmit.Enabled = False
            End If
        End If
    End Sub


    ''' <summary>
    ''' Attachment Link
    ''' </summary>
    ''' <param name="filePath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getlink(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachment")
                Dim str As String = filePath.Substring(index)
                str = ApplicationSettings.AttachmentDisplayPath(BizdivID) + str
                Return str.Replace("\", "/")
            End If
        End If
        Return ""
    End Function

    ''' <summary>
    ''' Function to update the Database and make an entry for Buyer Accepted in tracking table
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSubmit.Click
        Dim ds_Status As DataSet
        Dim dvEmail As New DataView
        Dim objEmail As New Emails

        'Statement of works attachments
        Dim dsAttachment As New WorkOrderNew
        Dim PONumber As String
        Dim JRSNumber As String
        Dim CustNumber As String
        Dim BizDiv As String
        Dim ProdName As String
        Dim AddnProdName As String

        'PO Number 
        If txtPONumber.Visible = False Then
            PONumber = lblPONumber.Text.Trim
        Else
            PONumber = txtPONumber.Text.Trim
        End If

        'JRS Number
        If txtJRSNumber.Visible = False Then
            JRSNumber = lblJRSNumber.Text.Trim
        Else
            JRSNumber = txtJRSNumber.Text.Trim
        End If

        'Location of Goods (previously Business Division)
        If drpdwnGoodsCollection.Visible Then
            BizDiv = drpdwnGoodsCollection.SelectedValue
        End If

        'Product Name
        If txtProductName.Visible = False Then
            ProdName = lblProductName.Text.Trim
        Else
            ProdName = txtProductName.Text.Trim
        End If

        dsAttachment = UCFileUpload1.ReturnFilledAttachments(dsAttachment)
        Dim xmlAttachment As String = dsAttachment.GetXml
        'Remove XML namespace attribute and its value
        xmlAttachment = xmlAttachment.Remove(xmlAttachment.IndexOf("xmlns"), xmlAttachment.IndexOf(".xsd") - xmlAttachment.IndexOf("xmlns") + 5)

        ds_Status = ws.WSWorkOrder.MS_WOSetBuyerAccepted(WOID, ViewState("WOValue"), CompanyID, ContactID, ClassID, ViewState("LastActionRef"), PONumber, JRSNumber, "", BizDiv, ProdName, AddnProdName, xmlAttachment)

        If ds_Status.Tables.Count <> 0 Then
            If ds_Status.Tables("tblStatus").Rows.Count > 0 Then
                If ds_Status.Tables("tblStatus").Rows(0).Item("ContactID") = -1 Then
                    'double entry
                Else
                    'Send mail only on submission of work order
                    dvEmail = ds_Status.Tables("tblStatus").Copy.DefaultView
                    objEmail.WPPrice = ViewState("WPPrice")
                    objEmail.WorkOrderID = ViewState("WorkOrderId")
                    objEmail.WOLoc = lblLocation.Text.Trim
                    objEmail.WOTitle = lblTitle.Text.Trim
                    objEmail.WOStartDate = lblStartDate.Text.Trim
                    objEmail.WOEndDate = lblEndDate.Text.Trim
                    objEmail.WPPrice = lblProposedPrice.Text.Trim
                    Dim dvStatus As New DataView(ds_Status.Tables("WOStatus"))
                    Dim WOStatus As Integer
                    If dvStatus.Count > 0 Then
                        If Not IsDBNull(dvStatus.Item(0).Item("WOStatus")) Then
                            WOStatus = dvStatus.Item(0).Item("WOStatus")
                        End If
                    End If

                    'Mail to the client & admin
                    Emails.BuyerAccepted(objEmail, dvEmail, BizdivID, WOStatus)

                End If
            Else
                'no rows returned
                divValidationMain.Visible = True
                lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
            End If
        Else
            'no ds sent
            divValidationMain.Visible = True
            lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
        End If

        If (Viewer = "Admin") Then
            If Not IsNothing(Request("sender")) Then
                Dim link As String
                If Request("sender") = "UCWOsListing" Then
                    If Viewer = ApplicationSettings.ViewerBuyer Then
                        link = "BuyerWOListing.aspx?"
                    ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                        link = "AdminWOListing.aspx?"
                    End If
                ElseIf Request("sender") = "SearchWO" Then
                    link = "SearchWOs.aspx?"
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&SrcWorkorderID=" & Request("txtWorkorderID")
                    link &= "&SrcCompanyName=" & Request("CompanyName")
                    link &= "&SrcDateStart=" & Request("DateStart")
                    link &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
                    link &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")
                    link &= "&SrcKeyword=" & Request("KeyWord")
                    link &= "&SrcPONumber=" & Request("PONumber")
                    link &= "&SrcReceiptNumber=" & Request("ReceiptNumber")
                    link &= "&SrcPostCode=" & Request("PostCode")
                    Response.Redirect(link)
                Else
                    link = "AdminWODetails.aspx?"
                End If

                link &= "WOID=" & Request("WOID")
                link &= "&WorkOrderID=" & Request("WorkOrderID")
                link &= "&ContactId=" & Request("ContactId")
                link &= "&CompanyId=" & Request("CompanyId")
                link &= "&SupContactId=" & Request("SupContactId")
                link &= "&SupCompId=" & Request("SupCompId")
                link &= "&Group=" & Request("Group")
                link &= "&mode=" & Request("Group")
                link &= "&BizDivID=" & Request("BizDivID")
                link &= "&FromDate=" & Request("FromDate")
                link &= "&ToDate=" & Request("ToDate")
                link &= "&PS=" & Request("PS")
                link &= "&PN=" & Request("PN")
                link &= "&SC=" & Request("SC")
                link &= "&SO=" & Request("SO")
                link &= "&CompID=" & Request("CompID")
                link &= "&sender=" & Request("sender")

                Response.Redirect(link)
            Else
                If Viewer = "Admin" Then
                    Response.Redirect("AdminWOListing.aspx?mode=Submitted")
                ElseIf Viewer = "Buyer" Then
                    Response.Redirect("BuyerWOListing.aspx?mode=" & Request("Group"))
                End If
            End If
        Else
            Response.Redirect("BuyerWOListing.aspx?mode=Submitted")
        End If

    End Sub

    ''' <summary>
    ''' Function to go back to listing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            If Request("sender") = "UCWOsListing" Then
                If Viewer = ApplicationSettings.ViewerBuyer Then
                    link = "BuyerWOListing.aspx?"
                ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                    link = "AdminWOListing.aspx?"
                End If
            ElseIf Request("sender") = "SearchWO" Then
                link = "SearchWOs.aspx?"
                link &= "&PS=" & Request("PS")
                link &= "&PN=" & Request("PN")
                link &= "&SC=" & Request("SC")
                link &= "&SO=" & Request("SO")
                link &= "&SrcWorkorderID=" & Request("txtWorkorderID")
                link &= "&SrcCompanyName=" & Request("CompanyName")
                link &= "&SrcDateStart=" & Request("DateStart")
                link &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
                link &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")
                link &= "&SrcKeyword=" & Request("KeyWord")
                link &= "&SrcPONumber=" & Request("PONumber")
                link &= "&SrcReceiptNumber=" & Request("ReceiptNumber")
                link &= "&SrcPostCode=" & Request("PostCode")
                Response.Redirect(link)
            Else
                link = "AdminWODetails.aspx?"
            End If

            link &= "WOID=" & Request("WOID")
            link &= "&WorkOrderID=" & Request("WorkOrderID")
            link &= "&ContactId=" & Request("ContactId")
            link &= "&CompanyId=" & Request("CompanyId")
            link &= "&SupContactId=" & Request("SupContactId")
            link &= "&SupCompId=" & Request("SupCompId")
            link &= "&Group=" & Request("Group")
            link &= "&mode=" & Request("Group")
            link &= "&BizDivID=" & Request("BizDivID")
            link &= "&FromDate=" & Request("FromDate")
            link &= "&ToDate=" & Request("ToDate")
            link &= "&PS=" & Request("PS")
            link &= "&PN=" & Request("PN")
            link &= "&SC=" & Request("SC")
            link &= "&SO=" & Request("SO")
            link &= "&CompID=" & Request("CompID")
            link &= "&sender=" & Request("sender")

            Response.Redirect(link)
        Else
            If Viewer = "Admin" Then
                Response.Redirect("AdminWOListing.aspx?mode=Submitted")
            ElseIf Viewer = "Buyer" Then
                Response.Redirect("BuyerWOListing.aspx?mode=" & Request("Group"))
            End If
        End If
    End Sub

    Private Sub SetFormView(ByVal ds As DataSet)
        If ds.Tables("tblSummary").Rows(0).Item("BusinessArea").ToString = ApplicationSettings.BusinessAreaRetail Then
            lblInstallTime.Visible = True
            trEndDate.Visible = False
            trJobNumber.Visible = True
            lblLocOfGoods.Text = "Location of Goods"

            lblProductsLabel.Text = "Product Purchased"
            trSalesAgent.Visible = True
            lblStartDateLabel.text = "Appointment"
        ElseIf ds.Tables("tblSummary").Rows(0).Item("BusinessArea").ToString = ApplicationSettings.BusinessAreaIT Then
            lblInstallTime.Visible = False
            trEndDate.Visible = True
            trJobNumber.Visible = False
            lblLocOfGoods.Text = "Location of Equipment"

            lblProductsLabel.Text = "Equipment Details"
            trSalesAgent.Visible = False
            lblStartDateLabel.text = "Start Date"
        Else
            lblInstallTime.Visible = False
            trEndDate.Visible = True
            trJobNumber.Visible = True

            pnlProducts.Visible = False
            lblProductsLabel.Text = "Product Purchased"
            trSalesAgent.Visible = False
            lblStartDateLabel.Text = "Start Date"
            trLocOfGoods.Visible = False
        End If
    End Sub
End Class