''' <summary>
''' Class for User Control "UCDualListBox"; which contains two listbox and its associated function to move data between these listboxes
''' </summary>
''' <remarks></remarks>

Partial Public Class UCDualListBox
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Add code if needed
        End If
    End Sub

    ''' <summary>
    ''' Method to populate both the Listbox.
    ''' </summary>
    ''' <param name="dtSelected">DataTable : Items which were selected</param>
    ''' <param name="dtList">DataTable :  List of all the items(e.g. Data from the lookup table</param>
    ''' <remarks></remarks>
    Public Sub populateListBox(ByVal dvSelected As DataView, ByVal dvList As DataView, ByVal displayText As String, ByVal selIDColName As String, ByVal deSelIDName As String)

      
        Dim drv As DataRowView
        ' Clear the fields
        Do While Not (lstSelected.Items.Count = 0)
            lstSelected.Items.Remove(lstSelected.Items(0))
        Loop

        Do While Not (lstDeSelected.Items.Count = 0)
            lstDeSelected.Items.Remove(lstDeSelected.Items(0))
        Loop


        Dim i As Integer
        Dim addFlag As Integer
        If Not IsNothing(dvSelected) Then
            For i = 0 To dvList.Count - 1
                addFlag = 0
                For Each drv In dvSelected
                    If CStr(drv(selIDColName)) = CStr(dvList.Item(i).Item(deSelIDName)) Then
                        lstSelected.Items.Add(New ListItem(dvList.Item(i).Item(displayText).ToString, dvList.Item(i).Item(deSelIDName)))
                        addFlag = 1
                    End If
                Next

                If addFlag = 0 Then
                    lstDeSelected.Items.Add(New ListItem(dvList.Item(i).Item(displayText), dvList.Item(i).Item(deSelIDName)))
                End If
            Next
        Else
            For Each drv In dvList
                lstDeSelected.Items.Add(New ListItem(drv.Item(displayText), drv.Item(deSelIDName)))
            Next

        End If

    End Sub

    ''' <summary>
    ''' Public method to return the left box items as a form of tables
    ''' </summary>
    ''' <returns>DataTable with row("Values","Text") </returns>
    ''' <remarks></remarks>
    Public Function saveListBox() As DataTable
        'Create a new datatable
        Dim dtSelected As New DataTable("SelectedList")
        'Create a new row for the datatable
        Dim row As DataRow

        'Define columns of the table.
        dtSelected.Columns.Add("Value", System.Type.GetType("System.String"))
        dtSelected.Columns.Add("Text", System.Type.GetType("System.String"))

        'Filling the values and text of the items in the selected list box
        Dim count As Integer = lstSelected.Items.Count
        Dim item As ListItem
        For Each item In lstSelected.Items
            row = dtSelected.NewRow()
            row("Value") = item.Value.ToString
            row("Text") = item.Text.ToString
            dtSelected.Rows.Add(row)
        Next

        'Return the datatset
        Return dtSelected

    End Function

    ''' <summary>
    ''' Method to handle the "Add All" functionality; adds all items in the left box to the right box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkbtnAddAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkbtnAddAll.Click
        'On Add All action, fill all the value of the inital listbox to the target listbox
        Do While Not (lstDeSelected.Items.Count = 0)
            lstSelected.Items.Add(New ListItem(lstDeSelected.Items(0).Text, lstDeSelected.Items(0).Value))
            lstDeSelected.Items.Remove(lstDeSelected.Items(0))
        Loop

    End Sub

    ''' <summary>
    ''' Method to handle the single item add functionality; adds all items in the left box to the right box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkbtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkbtnAdd.Click
        ' Add one by one
        While lstDeSelected.SelectedIndex <> -1
            If Not (lstDeSelected.SelectedIndex = -1) Then
                lstSelected.Items.Add(New ListItem(lstDeSelected.SelectedItem.Text, lstDeSelected.SelectedItem.Value))
                lstDeSelected.Items.Remove(lstDeSelected.SelectedItem)

            End If
        End While


        'If Not (lstDeSelected.SelectedIndex = -1) Then
        '    lstSelected.Items.Add(New ListItem(lstDeSelected.SelectedItem.Text, lstDeSelected.SelectedItem.Value))
        '    lstDeSelected.Items.Remove(lstDeSelected.SelectedItem)

        'End If
    End Sub

    ''' <summary>
    ''' Method to clear all the items in the right box to fill in the left box.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkbtnClearAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkbtnClearAll.Click
        Do While Not (lstSelected.Items.Count = 0)
            lstDeSelected.Items.Add(New ListItem(lstSelected.Items(0).Text, lstSelected.Items(0).Value))
            lstSelected.Items.Remove(lstSelected.Items(0))
        Loop
    End Sub

    ''' <summary>
    ''' Method to clear the single item from the right box and fill in the left box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkbtnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkbtnClear.Click
        While lstSelected.SelectedIndex <> -1
            If Not (lstSelected.SelectedIndex = -1) Then
                lstDeSelected.Items.Add(New ListItem(lstSelected.SelectedItem.Text, lstSelected.SelectedItem.Value))
                lstSelected.Items.Remove(lstSelected.SelectedItem)
            End If
        End While

        'If Not (lstSelected.SelectedIndex = -1) Then
        '    lstDeSelected.Items.Add(New ListItem(lstSelected.SelectedItem.Text, lstSelected.SelectedItem.Value))
        '    lstSelected.Items.Remove(lstSelected.SelectedItem)
        'End If
    End Sub
    Public Function showSelected(ByVal commmaSeperatedValues As String)
        If commmaSeperatedValues <> "" Then
            Dim strValue As String() = commmaSeperatedValues.Split(",")
            Dim lItem As ListItem
            If strValue.Length > 0 Then
                For Each Str As String In strValue
                    If Str.Trim <> "" Then
                        lItem = lstDeSelected.Items.FindByValue(Str)
                        lstSelected.Items.Add(lItem)
                        lstDeSelected.Items.Remove(lItem)
                        lItem = Nothing
                    End If
                Next
            End If
        End If
        Return Nothing
    End Function
End Class