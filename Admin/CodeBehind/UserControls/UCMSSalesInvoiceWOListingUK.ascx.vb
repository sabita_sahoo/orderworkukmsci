

Partial Public Class UCMSSalesInvoiceWOListingUK
    Inherits System.Web.UI.UserControl    
    Protected WithEvents gvWorkOrders As GridView
    Protected WithEvents ObjectDataSource1 As ObjectDataSource
    Protected WithEvents lnkBackToListing As HtmlAnchor
    Protected WithEvents btnExport As HtmlAnchor
    Protected WithEvents lblHeading As Label


    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            lblHeading.Text = "Work Orders for Sales Invoice " & Request("invoiceNo")
            ViewState!SortExpression = "CloseDate"
            gvWorkOrders.Sort(ViewState!SortExpression, SortDirection.Ascending)
            lnkBackToListing.HRef = getBackToListingLink()
            Select Case ApplicationSettings.SiteType
                Case ApplicationSettings.siteTypes.admin
                    btnExport.HRef = "~/ExportToExcel.aspx?page=SalesInvoiceWO&bizDivId=" & Request("BizDivId") & "&invoiceNo=" & Request("invoiceNo") & "&sortExpression=" & gvWorkOrders.SortExpression
                Case ApplicationSettings.siteTypes.site
                    btnExport.HRef = "~/SecurePages/ExportToExcel.aspx?page=SalesInvoiceWO&bizDivId=" & Request("BizDivId") & "&invoiceNo=" & Request("invoiceNo") & "&sortExpression=" & gvWorkOrders.SortExpression
            End Select
        End If
    End Sub

    Public Sub PopulateGrid()
        gvWorkOrders.DataBind()
        Select Case ApplicationSettings.SiteType
            Case ApplicationSettings.siteTypes.admin
                btnExport.HRef = "~/ExportToExcel.aspx?page=SalesInvoiceWO&bizDivId=" & Request("BizDivId") & "&invoiceNo=" & Request("invoiceNo") & "&sortExpression=" & gvWorkOrders.SortExpression
            Case ApplicationSettings.siteTypes.site
                btnExport.HRef = "~/SecurePages/ExportToExcel.aspx?page=SalesInvoiceWO&bizDivId=" & Request("BizDivId") & "&invoiceNo=" & Request("invoiceNo") & "&sortExpression=" & gvWorkOrders.SortExpression
        End Select


    End Sub

    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvWorkOrders.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvWorkOrders.DataBind()
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                       ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet        
        Dim contactid As String = ""

        ds = ws.WSFinance.MS_GetWOsForSalesInvoice(Request("BizDivId"), Request("invoiceNo"), sortExpression, startRowIndex, maximumRows)

        ViewState("rowCount") = ds.Tables(1).Rows(0)(0)
        Return ds
    End Function

    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function
    Public Function GetLinks(ByVal WOID As String, ByVal CompanyId As String) As String
        Dim link As String = ""
        Dim linkParams As String = ""
        linkParams &= "WOID=" & WOID

        If Not Request("sender") Is Nothing Then
            Select Case Request("sender")
                Case "Receivables"
                    linkParams &= "&CompanyId=" & Request("companyId")
                Case Else
                    linkParams &= "&CompanyId=" & CompanyId
            End Select
        Else
            linkParams &= "&CompanyId=" & CompanyId
        End If


        linkParams &= "&BizDivId=" & Request("BizDivId")
        linkParams &= "&invoiceNo=" & Request("invoiceNo")
        linkParams &= "&FromDate=" & Request("FromDate")
        linkParams &= "&ToDate=" & Request("ToDate")
        linkParams &= "&tab=" & Request("tab")
        linkParams &= "&PN=" & gvWorkOrders.PageIndex
        linkParams &= "&SC=" & gvWorkOrders.SortExpression
        linkParams &= "&SO=" & gvWorkOrders.SortDirection
        linkParams &= "&Group=Invoiced"
        'linkParams &= "&HideBackToListing=1"
        'linkParams &= "&companyId=" & Request("companyId")
        linkParams &= "&sender=SalesInvoiceWOListing"


        Select Case ApplicationSettings.SiteType
            Case ApplicationSettings.siteTypes.admin
                link &= "<a href='AdminWODetails.aspx?" & linkParams & "'><img src='Images/Icons/ViewDetails.gif' alt='view Work Order details' width='16' height='16' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
            Case ApplicationSettings.siteTypes.site
                Select Case Session("RoleGroupID")
                    'Commenting the following lines coz supplier will also act as a buyer on this page. 
                    'Case ApplicationSettings.RoleClientAdminID
                    '    link &= "<a target='_blank' href='BuyerWODetails.aspx?" & linkParams & "'><img src='../Images/Icons/ViewDetails.gif' alt='view Work Order details' width='16' height='16' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
                    'Case ApplicationSettings.RoleSupplierAdminID
                    '    link &= "<a target='_blank' href='SupplierWODetails.aspx?" & linkParams & "'><img src='../Images/Icons/ViewDetails.gif' alt='view Work Order details' width='16' height='16' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
                End Select
                link &= "<a target='_blank' href='BuyerWODetails.aspx?" & linkParams & "'><img src='../Images/Icons/ViewDetails.gif' alt='view Work Order details' width='16' height='16' hspace='2' vspace='0' border='0' align='absmiddle'></a>"

        End Select

        Return link


    End Function
    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvWorkOrders.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvWorkOrders, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex



    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvWorkOrders.RowDataBound
        MakeGridViewHeaderClickable(gvWorkOrders, e.Row)
    End Sub
    Public Function getBackToListingLink() As String
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender")          
                Case "Invoices"
                    Select Case ApplicationSettings.SiteType
                        Case ApplicationSettings.siteTypes.site
                            link = "~\SecurePages\Invoices.aspx?"
                        Case ApplicationSettings.siteTypes.admin
                            link = "~\Invoices.aspx?"
                    End Select
                    link &= "&companyId=" & Request("companyId")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&tab=" & Request("tab")
                    link &= "&sender=" & "Invoices"               
                Case "Receivables"
                    link = "~\Receivables.aspx?"
                    link &= "&companyId=" & Request("companyId")                    
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    If (Request("SC") = "CloseDate") Then
                        link &= "&SC=" & "InvoiceDate"
                    Else
                        link &= "&SC=" & Request("SC")
                    End If
                    link &= "&SO=" & Request("SO")
                    link &= "&tab=" & Request("tab")
                    link &= "&sender=" & "Receivables"
                Case "SalesInvoicePaid"
                    link = "~\SalesInvoicePaid.aspx?"
                    link &= "&invoiceNo=" & Request("invoiceNo")
                    link &= "&bizDivId=" & Request("bizDivId")
                    link &= "&companyId=" & Request("companyId")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=SalesInvoicePaid"
                Case "SearchFinance"
                    link = "~\SearchFinance.aspx?"
                    link &= "SrcPONumber=" & Request("txtPONumber")
                    link &= "&SrcInvoiceNo=" & Request("txtInvoiceNo")
                    link &= "&SrcKeyword=" & Request("txtKeyword")
                    link &= "&SrcWorkOrderId=" & Request("txtWorkorderID")
                    link &= "&SrcInvoiceDate=" & Request("txtInvoiceDate")
                    link &= "&SrcCompanyName=" & Request("txtCompanyName")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=RelatedWorkOrders"
                Case Else
                    Return ""
            End Select
        End If
        Return link
    End Function
    Public Sub ProcessBackToListing()
   
        gvWorkOrders.PageIndex = Request("PN")
        gvWorkOrders.Sort(Request("SC"), Request("SO"))
        PopulateGrid()       
    End Sub
End Class
