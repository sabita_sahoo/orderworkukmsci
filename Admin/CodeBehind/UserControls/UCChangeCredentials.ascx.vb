
''' <summary>
''' Class to handle change credentials User Control functionality.
''' </summary>
''' <remarks></remarks>
Partial Public Class UCChangeCredentials
    Inherits System.Web.UI.UserControl

    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            'Hide validation region 
            divValidationMain.Visible = False
            'Hide Success region which shows success message after change login process
            pnlSuccess.Visible = False

            'Populate Security question
            CommonFunctions.PopulateSecurityQues(Page, ddlQuestion)

            'Call Populate method
            PopulateChangeLogin(False)
        End If
    End Sub

    ''' <summary>
    ''' To populate the security question and answer on the change login form
    ''' </summary>
    ''' <param name="killcache"></param>
    ''' <remarks></remarks>
    Protected Sub PopulateChangeLogin(Optional ByVal killcache As Boolean = False)
        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
            tblEmail.Visible = False
            tblSecurity.Visible = False
        End If


        'Hide validation region 
        divValidationMain.Visible = False

        'Clear the fields
        txtNewEmail.Text = ""
        txtEmailConfirm.Text = ""
        'If session variable for security is avaiable then
        If Not IsNothing(Session("SecQues")) Then
            If Not IsDBNull(Session("SecQues")) Then
                ddlQuestion.SelectedValue = CInt(Session("SecQues"))
                txtAnswer.Text = Session("SecAns")
            End If
        End If
    End Sub

    ''' <summary>
    ''' To save changes made in the change login form
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub SaveChangeLogin()
        Page.Validate()
        If (Page.IsValid = False) Then
            divValidationMain.Visible = True
        Else
            'Page is Valid, hide validation region
            divValidationMain.Visible = False

            'Check for server side validation
            Dim Status As Boolean
            'Check if password is correct and both password supplied are same
            Status = PasswordCheckValid()
            'Check id new username supplied by user already exist in the system
            Status = Status And (Not CheckEmailAlreadyExist())

            Session("NewPassword") = Session("Password")

            If Status = True Then
                'If the field is not empty then store encrypted password in session variable
                If txtPassword.Text.Trim <> "" Then
                    Session("NewPassword") = Encryption.EncryptToMD5Hash(Trim(CStr(txtPassword.Text)))
                End If


                If ddlQuestion.SelectedValue <> "" Then
                    Session("NewSecQues") = CInt(ddlQuestion.SelectedValue)
                Else
                    Session("NewSecQues") = 0
                End If

                Session("NewSecAns") = Trim(txtAnswer.Text)

                pnlSubmitForm.Visible = False
                pnlConfirm.Visible = True

            End If
        End If

    End Sub


    Protected Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click

        'If new email field is not empty then change the cachekey accordingly and session variable also
        If txtNewEmail.Text.Trim <> "" Then
            Dim ds As DataSet = CType(Cache("LoggedInfo-" & Session("UserName") & Session.SessionID), DataSet)
            Cache.Remove("LoggedInfo-" & Session("UserName") & Session.SessionID)
            Session("UserName") = Trim(txtNewEmail.Text)
            Cache.Add("LoggedInfo-" & Session("UserName") & Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        End If
        'poonam modified on 14/6/2016 - Task - OA-276 : OA - Change SP to record Name of password changer/requester
        Dim LoggedInUserID As Integer = 0
        If Not IsNothing(Session("UserId")) Then
            LoggedInUserID = Session("UserId")
        End If
        Dim success As Integer
        success = ws.WSSecurity.ChangeLoginInfo(CInt(Session("UserId")), Session("UserName"), Session("Password"), Session("NewPassword"), Session("NewSecQues"), Session("NewSecAns"), LoggedInUserID)
        If success <> -1 Then
            Session("SecQues") = Session("NewSecQues")
            Session("SecAns") = Session("NewSecAns")
            Session("Password") = Session("NewPassword")
            Page.Session.Remove("NewPassword")
            Page.Session.Remove("NewSecQues")
            Page.Session.Remove("NewSecAns")
            pnlSubmitForm.Visible = False
            pnlSuccess.Visible = True
            pnlConfirm.Visible = False
            lblChangeLoginMsg.Text = ResourceMessageText.GetString("ChangeLoginSuccess")

        Else
            lblChangeLoginMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
            pnlSuccess.Visible = True

        End If

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlSubmitForm.Visible = True
        pnlSuccess.Visible = False
        pnlConfirm.Visible = False
        PopulateChangeLogin()
    End Sub

    ''' <summary>
    ''' Check for password condition
    ''' </summary>
    ''' <returns>boolean status</returns>
    ''' <remarks></remarks>
    Private Function PasswordCheckValid() As Boolean
        Dim status As Boolean
        If txtOldPassword.Text.Trim <> "" Then

            If CStr(Session("Password")) <> Trim(Encryption.EncryptToMD5Hash(txtOldPassword.Text)) Then
                'Check if old password is correct
                divValidationMain.Visible = True
                lblError.Text = ResourceMessageText.GetString("ChangeLoginIncorrectPassword")
                status = False
            Else
                'Check if old password is same as new password, if yes then prompt user
                If txtOldPassword.Text.Trim = txtPassword.Text.Trim Then
                    divValidationMain.Visible = True
                    lblError.Text = "<li>" & ResourceMessageText.GetString("ChangeLoginNoSamePassword") & "</li>"
                    status = False
                Else
                    status = True
                End If

            End If
        Else
            status = True
        End If
        Return status
    End Function

    ''' <summary>
    ''' Check if email already exist in system; if yes then show appropriate message
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckEmailAlreadyExist() As Boolean
        Dim loginexists As Boolean
        If txtNewEmail.Text <> "" Then
            loginexists = ws.WSContact.DoesLoginExist(txtNewEmail.Text.Trim)
            If loginexists = True Then
                'Email already exist, show validation message
                divValidationMain.Visible = True
                lblError.Text = ResourceMessageText.GetString("ChangeLoginEmailExist")
            End If
        End If
        Return loginexists
    End Function

    ''' <summary>
    ''' To handle save change login action triggerd by the Save LinkButton at the top
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub SaveProfileTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click
        SaveChangeLogin()
    End Sub

    ''' <summary>
    ''' To handle save  change login action triggerd by the Save LinkButton at the bottom
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub SaveProfileBottom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveBottom.Click
        SaveChangeLogin()
    End Sub

    ''' <summary>
    ''' To handle reset  change login action triggerd by the Save LinkButton at the top
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ResetProfileTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetTop.Click
        PopulateChangeLogin()
    End Sub

    ''' <summary>
    ''' To handle reset  change login action triggerd by the Save LinkButton at the bottom
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ResetProfileBottom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetBottom.Click
        PopulateChangeLogin()
    End Sub

    ''' <summary>
    ''' Custom validation to check for enter of old password if user enteres new password.
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Public Sub OldPassword_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles custChangePassword.ServerValidate
        If txtPassword.Text.Trim <> "" Then
            If txtOldPassword.Text.Trim <> "" Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        End If
    End Sub

    Public Sub custNewEmail_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles custNewEmail.ServerValidate
        If txtNewEmail.Text.Trim <> "" Then
            If txtEmailConfirm.Text.Trim <> "" Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        End If
    End Sub


End Class