''' <summary>
''' Class to implement the Placed Work Order Summary user control functionality.
''' </summary>
''' <remarks></remarks>
Partial Public Class UCWOPlacedSummary
    Inherits System.Web.UI.UserControl


    Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Session("CompanyId") Is Nothing Then
                Dim ds As DataSet = ws.WSWorkOrder.MS_woBuyWorkSummary(ApplicationSettings.BizDivId, Session("CompanyId"))
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count = 0 Then
                        tblNoWO.Visible = True
                    Else
                        DLSummary.DataSource = ds.Tables(0)
                        DLSummary.DataBind()
                    End If
                    lblTotal.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                    If ds.Tables(1).Rows.Count > 0 Then
                        If Not IsDBNull(ds.Tables(1).Rows(0).Item(0)) Then
                            lblTotal.Text = FormatCurrency(ds.Tables(1).Rows(0).Item(0), 2, TriState.True, TriState.True, TriState.False)
                        End If
                    End If
                End If
            End If
        End If
        lnkPlacedWO.HRef = "~/SecurePages/BuyerWOListing.aspx?mode=Draft"
    End Sub

    Public Function GetWOLink(ByVal StatusID As Integer) As String
        Select Case StatusID
            Case ApplicationSettings.WOStatusID.Draft, ApplicationSettings.WOStatusID.EnquiryDraft
                Return "BuyerWOListing.aspx?mode=Draft"
            Case ApplicationSettings.WOStatusID.Submitted, ApplicationSettings.WOStatusID.EnquirySubmitted
                Return "BuyerWOListing.aspx?mode=Submitted"
            Case ApplicationSettings.WOStatusID.CA
                Return "BuyerWOListing.aspx?mode=CA"
            Case ApplicationSettings.WOStatusID.Accepted
                Return "BuyerWOListing.aspx?mode=Accepted"
            Case ApplicationSettings.WOStatusID.Issue
                Return "BuyerWOListing.aspx?mode=Issue"
            Case ApplicationSettings.WOStatusID.Completed
                Return "BuyerWOListing.aspx?mode=Completed"
            Case ApplicationSettings.WOStatusID.Closed
                Return "BuyerWOListing.aspx?mode=Closed"
            Case ApplicationSettings.WOStatusID.Cancelled
                Return "BuyerWOListing.aspx?mode=Cancelled"
        End Select
    End Function

End Class