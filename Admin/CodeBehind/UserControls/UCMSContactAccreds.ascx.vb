Partial Public Class UCMSContactAccreds
    Inherits System.Web.UI.UserControl

    Shared ws As New WSObjs

    Public Property BizDivId() As Integer
        Get
            If Not IsNothing(ViewState("AccredsBizDivId")) Then
                Return ViewState("AccredsBizDivId")
            Else
                Return ApplicationSettings.BizDivId
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("AccredsBizDivId") = value
        End Set
    End Property
    Public Property ContactId() As Integer
        Get
            If Not IsNothing(ViewState("AccredsContactID")) Then
                Return ViewState("AccredsContactID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("AccredsContactID") = value
        End Set
    End Property
    Private _companyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            PopulateVendors(True)
            PopulateCertificates(0)
            PopulateSelectedGrid()
            lstBoxCerts.Visible = False
            tdCertID.Visible = False
            tdCertAccessSubmit.Visible = False
            trFileUpload.Visible = False
            tdCerts.Visible = False
        End If
    End Sub

    Public Function GetAccreds(Optional ByVal killCache As Boolean = False)
        Dim ds As New DataSet

        ' this gets all vendors and selected vendors
        Dim CacheKey As String = "AccredsList" & Session.SessionID & BizDivId
        If killCache Then
            Cache.Remove(CacheKey)
        End If

        If Cache(CacheKey) Is Nothing Then
            ds = ws.WSContact.GetContactAccreditations(BizDivId, ContactId)
            Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        Else
            ds = CType(Page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function

    Public Function PopulateVendors(Optional ByVal killCache As Boolean = False)
        Dim ds As New DataSet
        Dim dtAllVendors As New DataTable
        Dim dtSelectedVendors As New DataTable
        Dim dvSelectedVendors As New DataView

        ds = GetAccreds(killCache)
        'this dt has list of all vendors
        dtAllVendors = ds.Tables("tblAllVendors").Copy
        'this dt has list of all selected vendors
        dtSelectedVendors = ds.Tables("tblSelectedAccreds").Copy

        If Not IsNothing(dtSelectedVendors) Then
            dvSelectedVendors = dtSelectedVendors.Copy.DefaultView
        End If

        ' this cache stores the list of unselectd vendors
        Dim CacheKey As String = "UnSelectedVendors" & Session.SessionID & BizDivId

        If killCache Then
            Page.Cache.Remove(CacheKey)
            'also save All vendors in Cache
            Dim CacheAllVendorsKey As String = "AllVendors" & Session.SessionID & BizDivId
            Page.Cache.Remove(CacheAllVendorsKey)
            Page.Cache.Add(CacheAllVendorsKey, dtAllVendors, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        End If

        'this ds has unselected vendors
        Dim dtUnSelectedVendors As DataTable = CType(Page.Cache(CacheKey), DataTable)

        'if unselected vendors ds has something, then use tht ds
        If IsNothing(dtUnSelectedVendors) Or killCache = False Then
            'else use all vendors ds
            dtUnSelectedVendors = dtAllVendors.Copy
        End If

        'this iterates thru the list of unselected vendors (which may be all vendors) and deletes any that are selected
        'coz - this needs to be shown in the listbox of unselected vendors
iterateVendor:
        For Each drow As DataRow In dtUnSelectedVendors.Rows
            dvSelectedVendors.RowFilter = "VendorID = " & drow("VendorID")
            If dvSelectedVendors.Count <> 0 Then
                If dvSelectedVendors.Count = drow("CertCount") Then
                    dtUnSelectedVendors.Rows.Remove(drow)
                    GoTo iterateVendor
                End If
            End If
        Next

        'if there are a few recs in the list of unselected vendors, then show the vendors box
        If dtUnSelectedVendors.Rows.Count = 0 Then
            pnlAccredListBoxes.Visible = False
        Else
            pnlAccredListBoxes.Visible = True
        End If

        Dim dvUnSelectedVendors As DataView = dtUnSelectedVendors.Copy.DefaultView
        dvUnSelectedVendors.Sort = "VendorName"
        lstBoxVendor.DataSource = dvUnSelectedVendors
        lstBoxVendor.DataBind()
        Page.Cache.Remove(CacheKey)
        Page.Cache.Add(CacheKey, dtUnSelectedVendors, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
    End Function

    Public Sub PopulateSelectedGrid()
        Dim ds As DataSet = GetAccreds()
        Dim dtSelectedVendors As DataTable = ds.Tables("tblSelectedAccreds")
        Dim dvSelectedVendors As New DataView
        If Not IsNothing(dtSelectedVendors) Then
            If Not IsDBNull(dtSelectedVendors) Then
                If dtSelectedVendors.Rows.Count > 0 Then
                    dvSelectedVendors = dtSelectedVendors.DefaultView
                    dvSelectedVendors.RowFilter = ""
                    pnlSelectedAccreds.Visible = True
                    gvSelectedAccreds.DataSource = dvSelectedVendors
                    gvSelectedAccreds.DataBind()
                    If dtSelectedVendors.Rows.Count > 10 Then
                        divAccreds.Attributes.Add("style", "height:208px")
                    Else
                        divAccreds.Attributes.Remove("style")
                    End If
                Else
                    pnlSelectedAccreds.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub lstBoxVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstBoxVendor.SelectedIndexChanged
        PopulateCertificates(lstBoxVendor.SelectedValue, False)
        lstBoxCerts.Visible = True
        tdCertID.Visible = True
        tdCertAccessSubmit.Visible = True
        tdCerts.Visible = True
        txtAccessCode.Text = ""
        txtCertificateCode.Text = ""
        trFileUpload.Visible = False
        UCFileUpload7.ClearUCFileUploadCache()
        UCFileUpload7.ShowFileUpload()
    End Sub

    Private Sub lstBoxCerts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstBoxCerts.SelectedIndexChanged
        UCFileUpload7.ClearUCFileUploadCache()
        trFileUpload.Visible = True
        ViewState("BizDivID") = ApplicationSettings.OWUKBizDivId
        UCFileUpload7.BizDivID = ApplicationSettings.OWUKBizDivId
        UCFileUpload7.AttachmentForID = lstBoxCerts.SelectedValue
        UCFileUpload7.CompanyID = lstBoxCerts.SelectedValue
        UCFileUpload7.AttachmentForSource = "UserSkillSet"
        UCFileUpload7.Type = "UserSkillSet"
        UCFileUpload7.ShowExistAttach = False
        UCFileUpload7.ClearUCFileUploadCache()
        UCFileUpload7.ShowFileUpload()
    End Sub


    Public Sub PopulateCertificates(ByVal VendorID As Integer, Optional ByVal killCache As Boolean = False)
        Dim ds As New DataSet
        ds = GetAccreds(killCache)
        Dim dtSelectedCerts As New DataTable
        Dim dtAllCerts As New DataTable
        Dim dvSelectedCerts As New DataView

        'this dt has list of all certs
        dtAllCerts = ds.Tables("tblAllCerts")
        'this dt has list of all selected certs
        dtSelectedCerts = ds.Tables("tblSelectedAccreds")
        If Not IsNothing(dtSelectedCerts) Then
            dvSelectedCerts = dtSelectedCerts.DefaultView
        End If


        ' this cache stores the list of unselectd certs
        Dim CacheKey As String = "UnSelectedCerts" & Session.SessionID & BizDivId

        If killCache Then
            Page.Cache.Remove(CacheKey)
        End If

        'this ds has unselected certs
        Dim dtUnSelectedCerts As DataTable = CType(Page.Cache(CacheKey), DataTable)

        'if unselected certs dt has something, then use tht dt
        If IsNothing(dtUnSelectedCerts) Or killCache = False Then
            'else use all vendors dt
            dtUnSelectedCerts = dtAllCerts
        End If

        'this iterates thru the list of unselected certs (which may be all certs) and deletes any that are selected
        'coz - this needs to be shown in the listbox of unselected certs
iterateCert:
        For Each drow As DataRow In dtUnSelectedCerts.Rows
            dvSelectedCerts.RowFilter = "VendorID = " & drow("VendorID") & " AND CertID = " & drow("CertID")
            If dvSelectedCerts.Count <> 0 Then
                dtUnSelectedCerts.Rows.Remove(drow)
                GoTo iterateCert
            End If
        Next

        Page.Cache.Remove(CacheKey)
        Page.Cache.Add(CacheKey, dtUnSelectedCerts, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))

        If VendorID <> -1 And VendorID <> 0 Then
            lstBoxVendor.SelectedValue = VendorID
        End If

        dtUnSelectedCerts.DefaultView.RowFilter = "VendorID = " & VendorID
        lstBoxCerts.DataSource = dtUnSelectedCerts.DefaultView
        lstBoxCerts.DataBind()
    End Sub

    Private Sub gvSelectedAccreds_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSelectedAccreds.PreRender
        If Not IsNothing(gvSelectedAccreds) Then
            If Not IsNothing(gvSelectedAccreds.HeaderRow) Then
                gvSelectedAccreds.HeaderRow.Visible = False
            End If
        End If
    End Sub

    Private Sub lnkSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSelect.Click
        If lstBoxCerts.SelectedIndex < 0 Then
            lblMessage.Text = ResourceMessageText.GetString("SelectCertification")
            Return
        Else
            lblMessage.Text = ""
            Dim ds As DataSet = GetAccreds()
            Dim dtSelectedVendors As DataTable = ds.Tables("tblSelectedAccreds")
            Dim dscontacts As New XSDContacts
            Dim dsattachments As New DataSet

            dsattachments = UCFileUpload7.ReturnFilledAttachments(dscontacts)
            'this adds partner to the list
            For Each lItem As ListItem In lstBoxCerts.Items
                If lItem.Selected Then
                    Dim dRow As DataRow = dtSelectedVendors.NewRow
                    dRow("CertID") = lItem.Value.Trim
                    dRow("CertName") = lItem.Text.Trim
                    dRow("AccessCode") = Trim(txtAccessCode.Text)
                    dRow("CertCode") = Trim(txtCertificateCode.Text)
                    dRow("VendorID") = lstBoxVendor.SelectedItem.Value
                    dRow("VendorName") = lstBoxVendor.SelectedItem.Text.ToString.Trim
                    dRow("Date") = Date.Today
                    Dim dv As New DataView
                    dv = dsattachments.Tables("tblAttachmentInfo").DefaultView
                    If dv.ToTable.Rows.Count > 0 Then
                        dRow("Source") = dv.ToTable.Rows(0)("LinkSource")
                        dRow("FilePath") = dv.ToTable.Rows(0)("FilePath")
                        dRow("FileName") = dv.ToTable.Rows(0)("Name")
                        dRow("AttachmentID") = 0
                    Else
                        dRow("Source") = ""
                        dRow("FilePath") = ""
                        dRow("FileName") = ""
                        dRow("AttachmentID") = 0
                    End If

                    dtSelectedVendors.Rows.Add(dRow)
                End If
            Next

            Dim CacheKey As String = "VendorsList" & Session.SessionID & BizDivId
            Page.Cache.Remove(CacheKey)
            Page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            PopulateVendors()
            PopulateSelectedGrid()

            'hide the controls
            lstBoxCerts.Visible = False
            tdCertID.Visible = False
            tdCertAccessSubmit.Visible = False
            trFileUpload.Visible = False
            tdCerts.Visible = False
            UCFileUpload7.ClearUCFileUploadCache()
            UCFileUpload7.ShowFileUpload()
        End If
    End Sub

    Public Sub RemoveCallback(ByVal tag As String, ByVal obj As Object, ByVal reason As CacheItemRemovedReason)
        If (CacheItemRemovedReason.Underused = reason) Then
            Cache.Remove(tag)
            If Not IsNothing(obj) Then
                Cache.Insert(tag, obj, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            End If
        Else
            Cache.Remove(tag)
        End If
    End Sub

    Private Sub lnkRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemove.Click
        lstBoxCerts.Visible = False
        tdCertID.Visible = False
        tdCertAccessSubmit.Visible = False
        trFileUpload.Visible = False
        tdCerts.Visible = False
        Dim ds As DataSet = GetAccreds()
        Dim dtSelectedCerts As DataTable = ds.Tables("tblSelectedAccreds")
        Dim dvSelectedCerts As New DataView
        dvSelectedCerts = dtSelectedCerts.Copy.DefaultView

        Dim flagCheck As Boolean = False
        'add primary key as certid 
        Dim keys(1) As DataColumn
        keys(0) = dtSelectedCerts.Columns("CertID")

        'unselected certs
        Dim CacheKey As String = "UnSelectedCerts" & Session.SessionID & BizDivId
        Dim dtUnSelectedCerts As DataTable = CType(Page.Cache(CacheKey), DataTable)

        'Iteration through grid
        For Each row As GridViewRow In gvSelectedAccreds.Rows
            Dim chkBox As CheckBox = CType(row.FindControl("CheckAccreds"), CheckBox)
            If chkBox.Checked Then
                dtSelectedCerts.PrimaryKey = keys
                flagCheck = True
                If dtSelectedCerts.Rows.Count > 0 Then
                    dtSelectedCerts.Rows.Remove(dtSelectedCerts.Rows.Find(CType(row.FindControl("hdnCertId"), HtmlInputHidden).Value))
                    'Add to Unselected certs
                    If Not IsNothing(dtUnSelectedCerts) Then
                        Dim dRow As DataRow = dtUnSelectedCerts.NewRow
                        dvSelectedCerts.RowFilter = "CertID = '" & CType(row.FindControl("hdnCertId"), HtmlInputHidden).Value & "'"
                        dRow("VendorID") = dvSelectedCerts(0)("VendorID")
                        dRow("CertID") = dvSelectedCerts(0)("CertID")
                        dRow("CertName") = dvSelectedCerts(0)("CertName")
                        dtUnSelectedCerts.Rows.Add(dRow)
                    End If
                End If

            End If
        Next
        'Add cache for subcats
        Page.Cache.Remove(CacheKey)
        Page.Cache.Add(CacheKey, dtUnSelectedCerts, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))

        'Show validation message
        If flagCheck Then
            lblMessage.Text = ""
        Else
            lblMessage.Text = ResourceMessageText.GetString("SelectCertDel")
        End If

        AdjustVendors()
        PopulateVendors()
        PopulateSelectedGrid()
    End Sub

    Public Sub AdjustVendors()
        Dim dtAllVendors As DataTable = CType(Page.Cache("AllVendors" & Session.SessionID & BizDivId), DataTable).Copy
        Dim CacheKey As String = "UnSelectedVendors" & Session.SessionID & BizDivId
        Dim dtUnSelectedVendors As DataTable = CType(Page.Cache(CacheKey), DataTable)
        Dim dtUnSelectedCerts As DataTable = CType(Page.Cache("UnSelectedCerts" & Session.SessionID & BizDivId), DataTable)

        Dim dvUnSelectedCerts As DataView = dtUnSelectedCerts.DefaultView

iterateVendor:
        For Each drow As DataRow In dtAllVendors.Rows
            dvUnSelectedCerts.RowFilter = "VendorID=" & drow("VendorID")
            If dvUnSelectedCerts.Count = drow("CertCount") Then
                dtUnSelectedVendors.DefaultView.RowFilter = "VendorID=" & drow("VendorID")
                If dtUnSelectedVendors.DefaultView.Count < 1 Then
                    Dim newRow As DataRow = dtUnSelectedVendors.NewRow
                    newRow("VendorID") = drow("VendorID")
                    newRow("VendorName") = drow("VendorName")
                    newRow("CertCount") = drow("CertCount")
                    dtUnSelectedVendors.Rows.Add(newRow)
                    GoTo iterateVendor
                End If
            End If
        Next

        Page.Cache.Remove(CacheKey)
        Page.Cache.Add(CacheKey, dtUnSelectedVendors, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
    End Sub

    Public Function getAttachmentLinkage(ByVal FilePath As String, ByVal Name As String) As String
        Dim returnString As String = ""
        If (FilePath <> "" And Name <> "") Then
            returnString = "<span class='txtGreySmallSelected'><a class='footerTxtSelected' href='" + getlink(FilePath) + "' target='_blank'>" + Name + "</a></span>"
        End If
        Return returnString
    End Function
    Public Function getlink(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachment")
                Dim str As String = filePath.Substring(index)
                'str = ApplicationSettings.AttachmentDisplayPath(4) + str
                str = ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/" + str + CommonFunctions.AzureStorageAccessKey().ToString
                Return str.Replace("\", "/")
            End If
        End If
        Return ""
    End Function
End Class