Imports System.IO
Imports System.Net
Partial Public Class UCMSWOProcess
    Inherits System.Web.UI.UserControl

    Shared ws As New WSObjs
    Public objEmail As New Emails
    Private Delegate Sub AfterWOSave(ByVal dsSendMail As DataSet, ByVal ContactId As Integer, ByVal ds_WO As DataSet)
    Protected WithEvents UCFileUpload1 As UCFileUpload_Outer

    Private _parentName As String
    Public Property ParentName() As String
        Get
            Return _parentName
        End Get
        Set(ByVal value As String)
            _parentName = value
        End Set
    End Property

    Private _companyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property

    Private _supCompanyID As Integer
    Public Property SupCompanyID() As Integer
        Get
            Return _supCompanyID
        End Get
        Set(ByVal value As Integer)
            _supCompanyID = value
        End Set
    End Property

    Private _supContactID As Integer
    Public Property SupContactID() As Integer
        Get
            Return _supContactID
        End Get
        Set(ByVal value As Integer)
            _supContactID = value
        End Set
    End Property

    Private _userID As Integer
    Public Property UserID() As Integer
        Get
            Return _userID
        End Get
        Set(ByVal value As Integer)
            _userID = value
        End Set
    End Property

    Private _roleGroupID As Integer
    Public Property RoleGroupID() As Integer
        Get
            Return _roleGroupID
        End Get
        Set(ByVal value As Integer)
            _roleGroupID = value
        End Set
    End Property

    Private _classID As Integer
    Public Property ClassID() As Integer
        Get
            Return _classID
        End Get
        Set(ByVal value As Integer)
            _classID = value
        End Set
    End Property


    Private _BizDivId As Integer
    Public Property BizDivId() As Integer
        Get
            Return _BizDivId
        End Get
        Set(ByVal value As Integer)
            _BizDivId = value
        End Set
    End Property

    Private _viewer As String
    Public Property Viewer() As String
        Get
            Return _viewer
        End Get
        Set(ByVal value As String)
            _viewer = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetNoStore()
        'rngWOEndDate.MaximumValue = Date.MaxValue.Date
        'rngWOEndDate.MinimumValue = Date.Now.Date
        If Not Page.IsPostBack Then
            If Not IsNothing(Request("WOID")) Then
                If Request("WOID") <> "" Then
                    populateData(True)
                End If
            End If
        End If
    End Sub

    Private Sub populateData(Optional ByVal KillCache As Boolean = False)
        Dim str As String = ""
        Dim strViewer As String = ""
        'fetches the data for the wo
        Dim ds_WO As New DataSet
        Dim tblName As String

        ds_WO = GetActionDetails(KillCache)

        Dim CompanyId As String = ""
        CompanyId = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("CompanyId")

        If CompanyId = ApplicationSettings.OutsourceCompany Or CompanyId = ApplicationSettings.ExponentialECompany Or CompanyId = ApplicationSettings.ITECHSOCompany Or CompanyId = ApplicationSettings.DemoCompany Then
            ddlAptTime.Items.Clear()
            'ddlAptTime.Items.Insert(0, New ListItem("Please Select", ""))
            ddlAptTime.Items.Insert(0, New ListItem("Arrival 0 - 1", "Arrival 0 - 1"))
            ddlAptTime.Items.Insert(1, New ListItem("Arrival 1 - 2", "Arrival 1 - 2"))
            ddlAptTime.Items.Insert(2, New ListItem("Arrival 2 - 3", "Arrival 2 - 3"))
            ddlAptTime.Items.Insert(3, New ListItem("Arrival 3 - 4", "Arrival 3 - 4"))
            ddlAptTime.Items.Insert(4, New ListItem("Arrival 4 - 5", "Arrival 4 - 5"))
            ddlAptTime.Items.Insert(5, New ListItem("Arrival 5 - 6", "Arrival 5 - 6"))
            ddlAptTime.Items.Insert(6, New ListItem("Arrival 6 - 7", "Arrival 6 - 7"))
            ddlAptTime.Items.Insert(7, New ListItem("Arrival 7 - 8", "Arrival 7 - 8"))
            ddlAptTime.Items.Insert(8, New ListItem("Arrival 8 - 9", "Arrival 8 - 9"))
            ddlAptTime.Items.Insert(9, New ListItem("Arrival 9 - 10", "Arrival 9 - 10"))
            ddlAptTime.Items.Insert(10, New ListItem("Arrival 10 - 11", "Arrival 10 - 11"))
            ddlAptTime.Items.Insert(11, New ListItem("Arrival 11 - 12", "Arrival 11 - 12"))
            ddlAptTime.Items.Insert(12, New ListItem("Arrival 12 - 13", "Arrival 12 - 13"))
            ddlAptTime.Items.Insert(13, New ListItem("Arrival 13 - 14", "Arrival 13 - 14"))
            ddlAptTime.Items.Insert(14, New ListItem("Arrival 14 - 15", "Arrival 14 - 15"))
            ddlAptTime.Items.Insert(15, New ListItem("Arrival 15 - 16", "Arrival 15 - 16"))
            ddlAptTime.Items.Insert(16, New ListItem("Arrival 16 - 17", "Arrival 16 - 17"))
            ddlAptTime.Items.Insert(17, New ListItem("Arrival 17 - 18", "Arrival 17 - 18"))
            ddlAptTime.Items.Insert(18, New ListItem("Arrival 18 - 19", "Arrival 18 - 19"))
            ddlAptTime.Items.Insert(19, New ListItem("Arrival 19 - 20", "Arrival 19 - 20"))
            ddlAptTime.Items.Insert(20, New ListItem("Arrival 20 - 21", "Arrival 20 - 21"))
            ddlAptTime.Items.Insert(21, New ListItem("Arrival 21 - 22", "Arrival 21 - 22"))
            ddlAptTime.Items.Insert(22, New ListItem("Arrival 22 - 23", "Arrival 22 - 23"))
            ddlAptTime.Items.Insert(23, New ListItem("Arrival 23 - 24", "Arrival 23 - 24"))
            'OM-153 Added by Sabita New Slot for Amazon
        ElseIf CompanyId = ApplicationSettings.AmazonCompany Then
            ddlAptTime.Items.Clear()
            ddlAptTime.Items.Insert(0, New ListItem("Arrival 8 - 11", "Arrival 8 - 11"))
            'drpTime.Items.Insert(1, New ListItem("Arrival 9 - 12", "Arrival 9 - 12"))
            'drpTime.Items.Insert(2, New ListItem("Arrival 10 - 13", "Arrival 10 - 13"))
            'drpTime.Items.Insert(3, New ListItem("Arrival 11 - 14", "Arrival 11 - 14"))
            ddlAptTime.Items.Insert(1, New ListItem("Arrival 12 - 15", "Arrival 12 - 15"))
            'drpTime.Items.Insert(5, New ListItem("Arrival 13 - 16", "Arrival 13 - 16"))
            'drpTime.Items.Insert(6, New ListItem("Arrival 14 - 17", "Arrival 14 - 17"))
            'drpTime.Items.Insert(7, New ListItem("Arrival 15 - 18", "Arrival 15 - 18"))
            ddlAptTime.Items.Insert(2, New ListItem("Arrival 16 - 19", "Arrival 16 - 19"))
        Else
            ddlAptTime.Items.Clear()
            ddlAptTime.Items.Insert(0, New ListItem("Please Select", "0"))
            ddlAptTime.Items.Insert(1, New ListItem("8am - 1pm", "8am - 1pm"))
            ddlAptTime.Items.Insert(2, New ListItem("1pm - 6pm", "1pm - 6pm"))
            ddlAptTime.Items.Insert(3, New ListItem("All Day", "All Day"))
        End If

        If Not IsNothing(Request("sender")) Then
            If Request("sender") <> "UCWODetails" Then
                'Store version number
                CommonFunctions.StoreVerNum(ds_WO.Tables("tblWorkOrderSummary"))
                If ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "worejectissue" Or ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Or ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "wochangeissue" Then
                    Dim dvVerNo As DataView
                    dvVerNo = ds_WO.Tables("tblLastAction").DefaultView
                    CommonFunctions.StoreWOTrackingVerNum(dvVerNo)
                End If
            End If
        Else
            CommonFunctions.StoreVerNum(ds_WO.Tables("tblWorkOrderSummary"))
            If ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "worejectissue" Or ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Or ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "wochangeissue" Then
                Dim dvVerNo As DataView
                dvVerNo = ds_WO.Tables("tblLastAction").DefaultView
                CommonFunctions.StoreWOTrackingVerNum(dvVerNo)
            End If

        End If

        Dim dtAptTime As DataTable

        If ParentName.ToLower = "woraiseissue" Or ParentName.ToLower = "woacceptissue" Then
            'Populate the appointment time
            If ds_WO.Tables.Count - 1 = 3 Then
                dtAptTime = ds_WO.Tables(1).Copy
            ElseIf ds_WO.Tables.Count - 1 = 4 Then
                dtAptTime = ds_WO.Tables(2).Copy
            ElseIf ds_WO.Tables.Count - 1 = 5 Then
                dtAptTime = ds_WO.Tables(3).Copy
            ElseIf ds_WO.Tables.Count - 1 = 6 Then
                dtAptTime = ds_WO.Tables(4).Copy
            Else
                dtAptTime = Nothing
            End If
        Else
            'Populate the appointment time
            If ds_WO.Tables.Count = 3 Then
                dtAptTime = ds_WO.Tables(1).Copy
            ElseIf ds_WO.Tables.Count = 4 Then
                dtAptTime = ds_WO.Tables(2).Copy
            ElseIf ds_WO.Tables.Count = 5 Then
                dtAptTime = ds_WO.Tables(3).Copy
            ElseIf ds_WO.Tables.Count = 6 Then
                dtAptTime = ds_WO.Tables(4).Copy
            Else
                dtAptTime = Nothing
            End If
        End If

        If Not IsNothing(dtAptTime) Then
            If dtAptTime.Rows.Count > 0 Then
                If dtAptTime.Rows(0).Item("TimeSlot1Enabled") <> 0 Or dtAptTime.Rows(0).Item("TimeSlot2Enabled") <> 0 Or dtAptTime.Rows(0).Item("TimeSlot3Enabled") <> 0 Then
                    ddlAptTime.Items.Clear()
                    Dim itemcount As Integer = 0

                    If dtAptTime.Rows(0).Item("TimeSlot1Enabled") <> 0 Then
                        ddlAptTime.Items.Insert(itemcount, New ListItem(dtAptTime.Rows(0).Item("TimeSlot1"), dtAptTime.Rows(0).Item("TimeSlot1")))
                        itemcount = itemcount + 1
                    End If
                    If dtAptTime.Rows(0).Item("TimeSlot2Enabled") <> 0 Then
                        ddlAptTime.Items.Insert(itemcount, New ListItem(dtAptTime.Rows(0).Item("TimeSlot2"), dtAptTime.Rows(0).Item("TimeSlot2")))
                        itemcount = itemcount + 1
                    End If
                    If dtAptTime.Rows(0).Item("TimeSlot3Enabled") <> 0 Then
                        ddlAptTime.Items.Insert(itemcount, New ListItem(dtAptTime.Rows(0).Item("TimeSlot3"), dtAptTime.Rows(0).Item("TimeSlot3")))
                        itemcount = itemcount + 1
                    End If
                    If dtAptTime.Rows(0).Item("TimeSlot4Enabled") <> 0 Then
                        ddlAptTime.Items.Insert(itemcount, New ListItem(dtAptTime.Rows(0).Item("TimeSlot4"), dtAptTime.Rows(0).Item("TimeSlot4")))
                        itemcount = itemcount + 1
                    End If
                End If
            End If
        End If

        If IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("StagedWO")) Then
            ViewState("StagedWO") = "No"
        Else
            ViewState("StagedWO") = Trim(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("StagedWO"))
        End If

        If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PricingMethod")) Then
            ViewState("PricingMethod") = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PricingMethod")
        End If


        If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
            'Hide the Proposed day rate and estimated time cols in the summary if the wo is not staged and pricing method is Fixed
            If ViewState("StagedWO") = "No" Then
                If ViewState("PricingMethod") = "DailyRate" Then
                    tdColNameProposedDayRate.Visible = True
                    tdColNameEstimatedTimeInDays.Visible = True
                    tdlblProposedDayRate.Visible = True
                    tdlblEstimatedTimeInDays.Visible = True
                Else
                    tdColNameProposedDayRate.Visible = False
                    tdColNameEstimatedTimeInDays.Visible = False
                    tdlblProposedDayRate.Visible = False
                    tdlblEstimatedTimeInDays.Visible = False

                End If
            ElseIf ViewState("StagedWO") = "Yes" Then
                If ViewState("PricingMethod") = "DailyRate" Then
                    tdColNameProposedDayRate.Visible = True
                    tdColNameEstimatedTimeInDays.Visible = True
                    tdlblProposedDayRate.Visible = True
                    tdlblEstimatedTimeInDays.Visible = True

                Else
                    tdColNameProposedDayRate.Visible = False
                    tdColNameEstimatedTimeInDays.Visible = False
                    tdlblProposedDayRate.Visible = False
                    tdlblEstimatedTimeInDays.Visible = False
                End If
            End If
        End If




        'if i try to accept - a RAQ i.e. status = EnquirySubmitted or WOValue = 0 or ReviewBids = 0
        'then user should be redirected to CA page
        If ParentName.ToLower = "wospaccept" Then
            If Not IsNothing(ds_WO.Tables("tblWorkOrderSummary")) Then
                If ds_WO.Tables("tblWorkOrderSummary").Rows.Count <> 0 Then
                    'Removing the condition for platform price for the validation removal task in Q2 Phase1 2008 Dev
                    'If ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("ReviewBids") = True Or ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("WOStatus") = ApplicationSettings.WOStatusID.EnquirySubmitted Or ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformPrice") = 0 Then
                    If ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("ReviewBids") = True Or ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("WOStatus") = ApplicationSettings.WOStatusID.EnquirySubmitted Then
                        'redirect to CA page
                        Response.Redirect("~/SecurePages/WOSPCAccept.aspx?WOID=" & Request("WOID") & "&WorkOrderID=" & Request("WorkOrderID") & "&ContactID=" & Request("ContactID") & "&CompanyID=" & Request("CompanyID") & "&Viewer=" & Request("Viewer") & "&Group=" & Request("Group") & "&BizDivID=" & Request("BizDivID") & "&FromDate=" & Request("FromDate") & "&ToDate=" & Request("ToDate") & "&PS=" & Request("PS") & "&PN=" & Request("PN") & "&SC=" & Request("SC") & "&SO=" & Request("SO") & "&sender=" & Request("sender"))
                    End If
                End If
            End If
        End If

        ' Discuss comment is compulsory
        If ParentName.ToLower = "wospcaccept" Or ParentName.ToLower = "wospaccept" Or ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "wospchangeca" Then
            rqWOComments.Enabled = False
        ElseIf ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Or ParentName.ToLower = "woraiseissue" Or ParentName.ToLower = "wochangeissue" Then
            rqWOComments.Enabled = True
        ElseIf ParentName.ToLower = "worejectissue" Then
            rqWOComments.Enabled = True
            rqWOComments.ErrorMessage = "Please enter the reason for rejecting the issue"
            lblcommentsHdg.Text = "Reason"
        Else
            rqWOComments.Enabled = False
        End If


        ' in case of cond accept, raise issue, accept - last action grid is tblwosummary only
        If ParentName.ToLower = "wospcaccept" Or ParentName.ToLower = "wospaccept" Or ParentName.ToLower = "woraiseissue" Then
            tblName = "tblWorkOrderSummary"
            ' in case of discuss ca, discuss issue, accept ca, change terms issue, change ca, accept issue - grid is tbllastaction
        ElseIf ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Or ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "wochangeissue" Or ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "worejectissue" Then
            tblName = "tblLastAction"
        End If

        'populate the wo summary
        If Not IsNothing(ds_WO.Tables("tblWorkOrderSummary")) Then
            If ds_WO.Tables("tblWorkOrderSummary").Rows.Count <> 0 Then

                ' work order id
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("RefWOID")) Then
                    'lblWONo.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("WorkOrderId")
                    lblWONo.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("RefWOID")
                End If

                'submitted date
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("DateCreated")) Then
                    lblCreated.Text = Strings.FormatDateTime(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("DateCreated"), DateFormat.ShortDate)
                End If

                'location
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Location")) Then
                    lblLocation.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Location")
                End If

                'contact
                If Viewer = ApplicationSettings.ViewerSupplier Then
                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContact")) Then
                        lblContact.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContact")
                    End If
                ElseIf Viewer = ApplicationSettings.ViewerBuyer Then
                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BuyerContact")) Then
                        lblContact.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BuyerContact")
                    End If
                ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BuyerContact")) Then
                        lblContact.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BuyerContact")
                        If ParentName.ToLower = "woacceptissue" Then
                            populateProductListDD(CInt(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("CompanyId")), CInt(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("ProductUsed")))
                            populateSpecialistListDD()
                        End If
                    End If
                End If

                'title
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("WOTitle")) Then
                    lblTitle.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("WOTitle")
                End If

                ViewState("BusinessArea") = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BusinessArea").ToString
                'change label to Appointment Date and Appointment Time for Retail
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BusinessArea")) Then
                    If (ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BusinessArea") = ApplicationSettings.BusinessAreaRetail) Then
                        lblStart.Text = "Apt. Date"
                        lblEnd.Text = "Apt. Time"
                    End If
                End If
                'date start
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart")) Then
                    lblWOStart.Text = Strings.FormatDateTime(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
                End If

                'proposed price
                If Viewer = ApplicationSettings.ViewerSupplier Then
                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Value")) Then
                        lblProposedPrice.Text = FormatCurrency(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Value"), 2, TriState.True, TriState.True, TriState.False)
                    End If

                ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformPrice")) Then
                        lblProposedPrice.Text = FormatCurrency(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformPrice"), 2, TriState.True, TriState.True, TriState.False)
                    End If
                End If



                '**************************Staged WO Code To Summary Grid*****************************************
                If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                    'Proposed Day Rate
                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformDayJobRate")) Then
                        lblProposedDayRate.Text = FormatCurrency(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformDayJobRate"), 2, TriState.True, TriState.True, TriState.False)
                        ViewState("ProposedDayRate") = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformDayJobRate")
                    End If
                    'Estimated Time
                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("EstimatedTimeInDays")) Then
                        lblEstimatedTimeInDays.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("EstimatedTimeInDays")
                        ViewState("EstimatedTimeInDays") = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("EstimatedTimeInDays")
                    End If
                End If

                '**************************Staged WO Code*****************************************


                'status
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Status")) Then
                    lblStatus.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Status")
                End If
            End If
        End If

        'populate the last action grid 
        'cond accept, raise issue
        'discuss ca, discuss issue, accept ca, change terms issue, change ca, accept issue
        If ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "worejectissue" Or ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Or ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "wospcaccept" Or ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "woraiseissue" Or ParentName.ToLower = "wochangeissue" Then
            With ds_WO.Tables(tblName).Rows(0)
                'modifiable grid
                If ParentName.ToLower = "wospcaccept" Or ParentName.ToLower = "woraiseissue" Or ParentName.ToLower = "wochangeissue" Or ParentName.ToLower = "wospchangeca" Then
                    txtDateStart.Text = .Item("DateStart")
                    txtDateEnd.Text = .Item("DateEnd")

                    If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                        If ViewState("StagedWO") = "No" Then
                            If ViewState("PricingMethod") = "DailyRate" Then
                                txtValue.Enabled = False
                                tdHdrPropDayRate.Visible = True
                                tdHdrEstTime.Visible = True
                                tdPropDayRate.Visible = True
                                tdEstTime.Visible = True
                            Else
                                txtValue.Enabled = True
                                tdHdrPropDayRate.Visible = False
                                tdHdrEstTime.Visible = False
                                tdPropDayRate.Visible = False
                                tdEstTime.Visible = False
                            End If
                        Else
                            If ViewState("PricingMethod") = "DailyRate" Then
                                txtValue.Enabled = False
                                tdHdrPropDayRate.Visible = True
                                tdHdrEstTime.Visible = True
                                tdPropDayRate.Visible = True
                                tdEstTime.Visible = True
                            Else
                                txtValue.Enabled = True
                                tdHdrPropDayRate.Visible = False
                                tdHdrEstTime.Visible = False
                                tdPropDayRate.Visible = False
                                tdEstTime.Visible = False
                            End If
                        End If
                    Else
                        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                            txtValue.Enabled = True
                            tdHdrPropDayRate.Visible = False
                            tdHdrEstTime.Visible = False
                            tdPropDayRate.Visible = False
                            tdEstTime.Visible = False
                        End If
                    End If


                    'assing values of rate and days to the modifiable grid textboxes.
                    'If Not IsDBNull(.Item("PlatformDayJobRate")) Then
                    '    txtProposedRate.Text = FormatNumber(.Item("PlatformDayJobRate"), 2, TriState.True, TriState.False, TriState.False)
                    'End If
                    If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                        If Not IsDBNull(.Item("EstimatedTimeInDays")) Then
                            txtEstimatedTimeInDays.Text = .Item("EstimatedTimeInDays")
                        Else
                            txtEstimatedTimeInDays.Text = "1"
                        End If
                    End If


                    If Viewer = ApplicationSettings.ViewerSupplier Then
                        'Supplier does - accept, ca , Change Ca terms, discuss ca, raise issue, discuss issue, change terms issue, accept issue
                        txtValue.Text = FormatNumber(.Item("Value"), 2, TriState.True, TriState.False, TriState.False)
                        hdnPrice.Value = FormatNumber(.Item("Value"), 2, TriState.True, TriState.False, TriState.False)
                        If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                            If tblName = "tblWorkOrderSummary" Then
                                If Not IsDBNull(.Item("PlatformDayJobRate")) Then
                                    txtProposedRate.Text = FormatNumber(.Item("PlatformDayJobRate"), 2, TriState.True, TriState.False, TriState.False)
                                Else
                                    txtProposedRate.Text = "0.0"
                                End If
                            Else
                                If Not IsDBNull(.Item("DayJobRate")) Then
                                    txtProposedRate.Text = FormatNumber(.Item("DayJobRate"), 2, TriState.True, TriState.False, TriState.False)
                                Else
                                    txtProposedRate.Text = "0.0"
                                End If
                            End If
                        End If


                    ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                        'Admin does - discuss ca, accept ca, raise issue, discuss issue, change terms issue, accept issue
                        If tblName = "tblWorkOrderSummary" Then
                            txtValue.Text = FormatNumber(.Item("PlatformPrice"), 2, TriState.True, TriState.False, TriState.False)
                            hdnPrice.Value = FormatNumber(.Item("PlatformPrice"), 2, TriState.True, TriState.False, TriState.False)
                            If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                                If Not IsDBNull(.Item("PlatformDayJobRate")) Then
                                    txtProposedRate.Text = FormatNumber(.Item("PlatformDayJobRate"), 2, TriState.True, TriState.False, TriState.False)
                                Else
                                    txtProposedRate.Text = "0.0"
                                End If
                            End If

                        ElseIf tblName = "tblLastAction" Then
                            txtValue.Text = FormatNumber(.Item("Value"), 2, TriState.True, TriState.False, TriState.False)
                            If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                                If Not IsDBNull(.Item("DayJobRate")) Then
                                    txtProposedRate.Text = FormatNumber(.Item("DayJobRate"), 2, TriState.True, TriState.False, TriState.False)
                                Else
                                    txtProposedRate.Text = "0.0"
                                End If
                            End If

                        End If
                    End If
                End If

                'non modifiable grid
                If ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "worejectissue" Or ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Then
                    lblSubmitted.Text = Strings.FormatDateTime(.Item("DateCreated"), DateFormat.ShortDate)
                    lblStartD.Text = Strings.FormatDateTime(.Item("DateStart"), DateFormat.ShortDate)

                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BusinessArea")) Then
                        If (ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BusinessArea") = ApplicationSettings.BusinessAreaRetail) Then
                            lblAptTime.Text = ds_WO.Tables("tblLastAction").Rows(0).Item("AptTime")
                            ViewState("AptTime") = lblAptTime.Text
                            lblEndD.Text = ""
                        Else
                            lblEndD.Text = Strings.FormatDateTime(.Item("DateEnd"), DateFormat.ShortDate)
                        End If
                    Else
                        lblEndD.Text = Strings.FormatDateTime(.Item("DateEnd"), DateFormat.ShortDate)
                    End If




                    'assing values of rate and days to the non modifiable grid labels.
                    If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                        If Not IsDBNull(.Item("EstimatedTimeInDays")) Then
                            lblTimeInDays.Text = .Item("EstimatedTimeInDays")
                        Else
                            lblTimeInDays.Text = "1"
                        End If
                        If Not IsDBNull(.Item("DayJobRate")) Then
                            lblDayRate.Text = FormatCurrency(.Item("DayJobRate"), 2, TriState.True, TriState.False, TriState.False)
                        Else
                            lblDayRate.Text = "0.0"
                        End If
                    End If


                    If Viewer = ApplicationSettings.ViewerSupplier Then
                        'Supplier does - accept, ca , Change Ca terms, discuss ca, raise issue, discuss issue, change terms issue, accept issue
                        lblNewPrice.Text = FormatCurrency(.Item("Value"), 2, TriState.True, TriState.False, TriState.False)

                    ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                        'Admin does - discuss ca, accept ca, raise issue, discuss issue, change terms issue, accept issue
                        If tblName = "tblWorkOrderSummary" Then
                            lblNewPrice.Text = FormatCurrency(.Item("PlatformPrice"), 2, TriState.True, TriState.False, TriState.False)
                        ElseIf tblName = "tblLastAction" Then
                            lblNewPrice.Text = FormatCurrency(.Item("Value"), 2, TriState.True, TriState.False, TriState.False)
                        End If
                    End If
                    If Not IsDBNull(.Item("SpecialistName")) Then
                        tdLblSpecialistName.Visible = True
                        tdSpecialistName.Visible = True
                        lblSpecialistName.Text = .Item("SpecialistName")
                    Else
                        tdLblSpecialistName.Visible = False
                        tdSpecialistName.Visible = False
                    End If

                    lblCurrentStatus.Text = .Item("Status")

                    'highlight changed terms
                    If lblStartD.Text <> lblWOStart.Text Then
                        lblStartD.CssClass = "bodytxtValidationMsg"
                    Else
                        lblStartD.CssClass = "gridRow"
                    End If
                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BusinessArea")) Then
                        If (ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BusinessArea") = ApplicationSettings.BusinessAreaRetail) Then
                            If lblEndD.Text <> ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime") Then
                                lblAptTime.CssClass = "bodytxtValidationMsg"
                                lblEndD.CssClass = "bodytxtValidationMsg"
                            Else
                                lblAptTime.CssClass = "gridRow"
                                lblEndD.CssClass = "gridRow"
                            End If
                        Else
                            If lblEndD.Text <> ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("DateEnd").ToString Then
                                lblEndD.CssClass = "bodytxtValidationMsg"
                            Else
                                lblEndD.CssClass = "gridRow"
                            End If
                        End If
                    Else
                        If lblEndD.Text <> ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("DateEnd").ToString Then
                            lblEndD.CssClass = "bodytxtValidationMsg"
                        Else
                            lblEndD.CssClass = "gridRow"
                        End If
                    End If



                    If Viewer = ApplicationSettings.ViewerSupplier Then
                        'Supplier does - accept, ca , Change Ca terms, discuss ca, raise issue, discuss issue, change terms issue, accept issue
                        If ds_WO.Tables(tblName).Rows(0).Item("Value") <> ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Value") Then
                            lblNewPrice.CssClass = "bodytxtValidationMsg"
                            If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                                lblTimeInDays.CssClass = "bodytxtValidationMsg"
                                lblDayRate.CssClass = "bodytxtValidationMsg"
                            End If
                        Else
                            lblNewPrice.CssClass = "gridRow"
                            If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                                lblTimeInDays.CssClass = "gridRow"
                                lblDayRate.CssClass = "gridRow"
                            End If
                        End If
                    ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                        'Admin does - discuss ca, accept ca, raise issue, discuss issue, change terms issue, accept issue
                        If tblName = "tblWorkOrderSummary" Then
                            If ds_WO.Tables(tblName).Rows(0).Item("PlatformPrice") <> ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformPrice") Then
                                lblNewPrice.CssClass = "bodytxtValidationMsg"
                                If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                                    lblTimeInDays.CssClass = "bodytxtValidationMsg"
                                    lblDayRate.CssClass = "bodytxtValidationMsg"
                                End If
                            Else
                                lblNewPrice.CssClass = "gridRow"
                                If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                                    lblTimeInDays.CssClass = "gridRow"
                                    lblDayRate.CssClass = "gridRow"
                                End If
                            End If
                        ElseIf tblName = "tblLastAction" Then
                            If ds_WO.Tables(tblName).Rows(0).Item("Value") <> ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformPrice") Then
                                lblNewPrice.CssClass = "bodytxtValidationMsg"
                                If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                                    lblTimeInDays.CssClass = "bodytxtValidationMsg"
                                    lblDayRate.CssClass = "bodytxtValidationMsg"
                                End If
                            Else
                                lblNewPrice.CssClass = "gridRow"
                                If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                                    lblTimeInDays.CssClass = "gridRow"
                                    lblDayRate.CssClass = "gridRow"
                                End If
                            End If
                        End If
                    End If

                    If ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "worejectissue" Or ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Then
                        If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("SpecialistID")) Then
                            If ds_WO.Tables(tblName).Rows(0).Item("SpecialistID") <> ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("SpecialistID") Then
                                lblSpecialistName.CssClass = "bodytxtValidationMsg"
                            Else
                                lblSpecialistName.CssClass = "gridRow"
                            End If
                        Else
                            lblSpecialistName.CssClass = "gridRow"
                        End If
                    End If
                End If
            End With
        End If

        'populate the message, the wo summary labels, the confirm click button
        Dim DateCreatedLabel, PriceLabel, ConfirmLabel, Msg1 As String

        If Viewer.ToLower = "buyer" Then
            strViewer = ResourceMessageText.GetString("Buyer")
        ElseIf Viewer.ToLower = "supplier" Then
            strViewer = ResourceMessageText.GetString("Supplier")
        End If

        'Hardcoding the following two strings as this functionality (Stagd WO) needs to be only implemented for the UK site.
        'Will have to pick the text from the resourse file to implement this in DE
        If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
            lblColNameProposedDayRate.Text = "Proposed Day Rate"
            lblColNameEstimatedTimeInDays.Text = "Estimated Time (Days)"
        End If

        If ParentName.ToLower = "wospaccept" Then
            pnlSpecialist.Visible = True
            pnlActionModifyGrid.Visible = False
            pnlActionNonModifyGrid.Visible = False
            DateCreatedLabel = ResourceMessageText.GetString("Received")
            PriceLabel = ResourceMessageText.GetString("ProposedPrice")
            ConfirmLabel = ResourceMessageText.GetString("AcceptLnk")
            Msg1 = ResourceMessageText.GetString("ConfirmAcceptWO")
            Msg1 = Msg1.Replace("<WONo>", lblWONo.Text)

        ElseIf ParentName.ToLower = "wospcaccept" Then
            pnlSpecialist.Visible = True
            pnlActionNonModifyGrid.Visible = False
            pnlActionModifyGrid.Visible = True
            DateCreatedLabel = ResourceMessageText.GetString("Received")
            PriceLabel = ResourceMessageText.GetString("ProposedPrice")
            ConfirmLabel = ResourceMessageText.GetString("CAcceptLnk")
            Msg1 = ResourceMessageText.GetString("ConfirmCAcceptWO")
            Msg1 = Msg1.Replace("<WONo>", lblWONo.Text)
            lblSpecialist.Text = ResourceMessageText.GetString("SelectSpecialist")


        ElseIf ParentName.ToLower = "wospchangeca" Then
            pnlSpecialist.Visible = True
            pnlActionNonModifyGrid.Visible = False
            pnlActionModifyGrid.Visible = True
            DateCreatedLabel = ResourceMessageText.GetString("Received")
            PriceLabel = ResourceMessageText.GetString("ProposedPrice")
            ConfirmLabel = ResourceMessageText.GetString("ChangeCALnk")
            Msg1 = ResourceMessageText.GetString("ConfirmChangeCATerms")
            Msg1 = Msg1.Replace("<WONo>", lblWONo.Text)

        ElseIf ParentName.ToLower = "woraiseissue" Then

            pnlAttachments.Visible = True
            UCFileUpload1.BizDivID = BizDivId
            UCFileUpload1.AttachmentForID = Request("WOID")
            UCFileUpload1.CompanyID = CompanyID
            UCFileUpload1.AttachmentForSource = "AdminWOIssue"
            UCFileUpload1.Type = "AdminWOIssue"
            UCFileUpload1.ClearUCFileUploadCache()
            UCFileUpload1.PopulateAttachedFiles()
            UCFileUpload1.PopulateExistingFiles()


            If Viewer = ApplicationSettings.ViewerSupplier Then
                pnlSpecialist.Visible = True
            Else
                pnlSpecialist.Visible = False
            End If
            pnlActionNonModifyGrid.Visible = False
            pnlActionModifyGrid.Visible = True
            DateCreatedLabel = ResourceMessageText.GetString("Accepted")
            PriceLabel = ResourceMessageText.GetString("Price")
            ConfirmLabel = ResourceMessageText.GetString("IssueLnk")
            Msg1 = ResourceMessageText.GetString("ConfirmRaiseIssue")
            Msg1 = Msg1.Replace("<WONo>", lblWONo.Text)
            'hide the column end date and show the apt time column
            hdnEvngInstTime.Value = ds_WO.Tables("tblWorkOrderSummary").Rows(0)("EvengInstTime")
            If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BusinessArea")) Then
                If (ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BusinessArea") = ApplicationSettings.BusinessAreaRetail) Then
                    tdEndDHdr.Visible = False
                    tdEndDTxt.Visible = False
                    tdAptTime.Visible = True
                    tdAptTimeHdr.Visible = True
                    rqWODateRange.Enabled = False
                    regExEndDate.Enabled = False
                    lblstrtHdrIssue.Text = "Apt. Date"
                    hdnPrice.Value = txtValue.Text
                    If (ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("IsEveningInstallationEnabled")) Then
                        'ddlAptTime.Items.Remove(CStr(ApplicationSettings.AllDaySlot))
                        Dim itemcount As Integer
                        itemcount = ddlAptTime.Items.Count

                        Dim liInstallationTime As New ListItem
                        liInstallationTime.Text = hdnEvngInstTime.Value
                        liInstallationTime.Value = hdnEvngInstTime.Value
                        ddlAptTime.Items.Insert(itemcount, liInstallationTime)
                        'itemcount = itemcount + 1
                        'Dim liAllDayTime As New ListItem
                        'liAllDayTime.Text = ApplicationSettings.AllDaySlot
                        'liAllDayTime.Value = ApplicationSettings.AllDaySlot
                        'ddlAptTime.Items.Insert(itemcount, liAllDayTime)
                        hdnPPUp.Value = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PPUplift")

                        If ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PPUpliftPercent") = "0" Then
                            hdnPPUpliftPercent.Value = "False"
                        Else
                            hdnPPUpliftPercent.Value = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PPUpliftPercent")
                        End If
                    Else
                        'Dim liAllDayTime As New ListItem
                        'liAllDayTime.Text = ApplicationSettings.AllDaySlot
                        'liAllDayTime.Value = ApplicationSettings.AllDaySlot
                        'ddlAptTime.Items.Insert(2, liAllDayTime)
                    End If

                    If Not ddlAptTime.Items.FindByText(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime").ToString) Is Nothing Then
                        ddlAptTime.SelectedValue = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime").ToString
                    Else
                        Dim itemcount As Integer = 0
                        itemcount = ddlAptTime.Items.Count
                        ddlAptTime.Items.Insert(itemcount, New ListItem(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime").ToString, ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime").ToString))
                        ddlAptTime.SelectedValue = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime").ToString
                    End If
                    ViewState("AptTime") = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime").ToString
                    hdnAptTM.Value = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime").ToString
                Else
                    tdEndDHdr.Visible = True
                    tdEndDTxt.Visible = True
                    tdAptTime.Visible = False
                    tdAptTimeHdr.Visible = False
                    rqWODateRange.Enabled = True
                    regExEndDate.Enabled = True
                End If
            End If


        ElseIf ParentName.ToLower = "wochangeissue" Then
            pnlAttachments.Visible = True
            UCFileUpload1.BizDivID = BizDivId
            UCFileUpload1.AttachmentForID = Request("WOID")
            UCFileUpload1.CompanyID = CompanyID
            UCFileUpload1.AttachmentForSource = "AdminWOIssue"
            UCFileUpload1.Type = "AdminWOIssue"
            UCFileUpload1.ClearUCFileUploadCache()
            UCFileUpload1.PopulateAttachedFiles()
            'UCFileUpload1.PopulateExistingFiles()

            If Viewer = ApplicationSettings.ViewerSupplier Then
                pnlSpecialist.Visible = True
            Else
                pnlSpecialist.Visible = False
            End If
            pnlActionNonModifyGrid.Visible = False
            pnlActionModifyGrid.Visible = True
            DateCreatedLabel = ResourceMessageText.GetString("IssueRaised")
            PriceLabel = ResourceMessageText.GetString("Price")
            ConfirmLabel = ResourceMessageText.GetString("EditIssueLnk")
            Msg1 = ResourceMessageText.GetString("ConfirmChangeIssueTerms")
            Msg1 = Msg1.Replace("<WONo>", lblWONo.Text)
            If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BusinessArea")) Then
                If (ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BusinessArea") = ApplicationSettings.BusinessAreaRetail) Then
                    tdEndDHdr.Visible = False
                    tdEndDTxt.Visible = False
                    tdAptTime.Visible = True
                    tdAptTimeHdr.Visible = True
                    rqWODateRange.Enabled = False
                    regExEndDate.Enabled = False
                    lblstrtHdrIssue.Text = "Apt. Date"
                    hdnPrice.Value = txtValue.Text
                    If (ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("IsEveningInstallationEnabled")) Then
                        'ddlAptTime.Items.Remove(CStr(ApplicationSettings.AllDaySlot))
                        Dim itemcount As Integer
                        itemcount = ddlAptTime.Items.Count

                        hdnEvngInstTime.Value = ds_WO.Tables("tblWorkOrderSummary").Rows(0)("EvengInstTime")

                        Dim liInstallationTime As New ListItem
                        liInstallationTime.Text = hdnEvngInstTime.Value
                        liInstallationTime.Value = hdnEvngInstTime.Value
                        ddlAptTime.Items.Insert(itemcount, liInstallationTime)
                        'itemcount = itemcount + 1
                        'Dim liAllDayTime As New ListItem
                        'liAllDayTime.Text = ApplicationSettings.AllDaySlot
                        'liAllDayTime.Value = ApplicationSettings.AllDaySlot
                        'ddlAptTime.Items.Insert(itemcount, liAllDayTime)
                        hdnPPUp.Value = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PPUplift")

                        If ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PPUpliftPercent") = "0" Then
                            hdnPPUpliftPercent.Value = "False"
                        Else
                            hdnPPUpliftPercent.Value = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PPUpliftPercent")
                        End If
                    Else
                        'Dim itemcount As Integer
                        'itemcount = ddlAptTime.Items.Count
                        'Dim liAllDayTime As New ListItem
                        'liAllDayTime.Text = ApplicationSettings.AllDaySlot
                        'liAllDayTime.Value = ApplicationSettings.AllDaySlot
                        'ddlAptTime.Items.Insert(itemcount, liAllDayTime)
                    End If
                    ddlAptTime.SelectedValue = ds_WO.Tables("tblLastAction").Rows(0).Item("AptTime")
                Else
                    tdEndDHdr.Visible = True
                    tdEndDTxt.Visible = True
                    tdAptTime.Visible = False
                    tdAptTimeHdr.Visible = False
                    rqWODateRange.Enabled = True
                    regExEndDate.Enabled = True
                End If
            End If

        ElseIf ParentName.ToLower = "woacceptca" Then
            pnlSpecialist.Visible = False
            pnlActionNonModifyGrid.Visible = True
            pnlActionModifyGrid.Visible = False
            lblDLHeader.Text = ResourceMessageText.GetString("ConditionalAcceptanceTerms")
            DateCreatedLabel = ResourceMessageText.GetString("Submitted")
            PriceLabel = ResourceMessageText.GetString("ProposedPrice")
            ConfirmLabel = ResourceMessageText.GetString("AcceptCALnk")
            Msg1 = ResourceMessageText.GetString("ConfirmAcceptCA")
            Msg1 = Msg1.Replace("<WONo>", lblWONo.Text)
        ElseIf ParentName.ToLower = "woacceptissue" Then
            pnlAttachments.Visible = True
            UCFileUpload1.BizDivID = BizDivId
            UCFileUpload1.AttachmentForID = Request("WOID")
            UCFileUpload1.CompanyID = CompanyID
            UCFileUpload1.AttachmentForSource = "AdminWOIssue"
            UCFileUpload1.Type = "AdminWOIssue"
            UCFileUpload1.ClearUCFileUploadCache()
            UCFileUpload1.PopulateAttachedFiles()
            'UCFileUpload1.PopulateExistingFiles()
            tblConfirmAndBack.Visible = True


            pnlSpecialist.Visible = False
            pnlActionNonModifyGrid.Visible = True
            pnlActionModifyGrid.Visible = False
            DateCreatedLabel = ResourceMessageText.GetString("IssueRaised")
            PriceLabel = ResourceMessageText.GetString("Price")
            ConfirmLabel = ResourceMessageText.GetString("AcceptIssueLnk")
            Msg1 = ResourceMessageText.GetString("ConfirmAcceptIssue")
            Msg1 = Msg1.Replace("<WONo>", lblWONo.Text)
            If Viewer = ApplicationSettings.ViewerSupplier Then
                Msg1 = Msg1.Replace("<issueLbl>", ResourceMessageText.GetString("SuppIssue"))       '   "supplier's issue"
            ElseIf Viewer = ApplicationSettings.ViewerBuyer Then
                Msg1 = Msg1.Replace("<issueLbl>", ResourceMessageText.GetString("BuyerIssue"))          '       "buyer's issue"
            End If
        ElseIf ParentName.ToLower = "worejectissue" Then
            pnlSpecialist.Visible = False
            pnlActionNonModifyGrid.Visible = True
            pnlActionModifyGrid.Visible = False
            DateCreatedLabel = ResourceMessageText.GetString("IssueRaised")
            PriceLabel = ResourceMessageText.GetString("Price")
            ConfirmLabel = ResourceMessageText.GetString("RejectIssueLnk")
            Msg1 = ResourceMessageText.GetString("ConfirmRejectIssue")
            Msg1 = Msg1.Replace("<WONo>", lblWONo.Text)
            tblConfirmAndBack.Visible = True
        ElseIf ParentName.ToLower = "wodiscussca" Then
            pnlSpecialist.Visible = False
            pnlActionNonModifyGrid.Visible = True
            pnlActionModifyGrid.Visible = False
            DateCreatedLabel = ResourceMessageText.GetString("Received")
            PriceLabel = ResourceMessageText.GetString("ProposedPrice")
            str = ResourceMessageText.GetString("DiscussLnk")
            ConfirmLabel = str.Replace("<viewer>", strViewer)
            Msg1 = ResourceMessageText.GetString("ConfirmDiscussCA")
            Msg1 = Msg1.Replace("<WONo>", lblWONo.Text)
        ElseIf ParentName.ToLower = "wodiscuss" Then
            pnlAttachments.Visible = True
            UCFileUpload1.BizDivID = BizDivId
            UCFileUpload1.AttachmentForID = Request("WOID")
            UCFileUpload1.CompanyID = CompanyID
            UCFileUpload1.AttachmentForSource = "AdminWOIssue"
            UCFileUpload1.Type = "AdminWOIssue"
            UCFileUpload1.ClearUCFileUploadCache()
            UCFileUpload1.PopulateAttachedFiles()
            'UCFileUpload1.PopulateExistingFiles()


            pnlSpecialist.Visible = False
            pnlActionNonModifyGrid.Visible = True
            pnlActionModifyGrid.Visible = False
            DateCreatedLabel = ResourceMessageText.GetString("IssueRaised")
            PriceLabel = ResourceMessageText.GetString("Price")
            str = ResourceMessageText.GetString("DiscussLnk")

            ConfirmLabel = str.Replace("<viewer>", strViewer)
            Msg1 = ResourceMessageText.GetString("ConfirmDiscussIssue")
            Msg1 = Msg1.Replace("<WONo>", lblWONo.Text)
            Msg1 = Msg1.Replace("<viewer>", strViewer)
        End If


        If ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "worejectissue" Or ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Then
            '****************************Staged Code*******************
            If ViewState("StagedWO") = "Yes" Or ViewState("StagedWO") = "No" Then
                If ViewState("PricingMethod") = "DailyRate" Then
                    If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                        tdDLDayRate.Visible = True
                        tdDLEstimatedTime.Visible = True
                        tdDLDayRate1.Visible = True
                        tdDLEstimatedTime1.Visible = True
                        tdDLEstimatedTime.Width = "307PX"
                        tdDLDayRate1.Width = "307PX"
                        tdEndDateHdr.Visible = False
                        tdStatusHdr.Visible = False
                        tdEndDateValue.Visible = False
                        tdStatusValue.Visible = False
                    End If

                    tdLblSpecialistName.Visible = False
                    tdSpecialistName.Visible = False
                Else
                    If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                        tdDLDayRate.Visible = False
                        tdDLEstimatedTime.Visible = False
                        tdDLDayRate1.Visible = False
                        tdDLEstimatedTime1.Visible = False
                        tdEndDateHdr.Visible = True
                        tdStatusHdr.Visible = True
                        tdEndDateValue.Visible = True
                        tdStatusValue.Visible = True
                    End If

                    tdLblSpecialistName.Visible = True
                    tdSpecialistName.Visible = True
                End If
            End If
            '**********************************************************
        End If

        'wo summary column names
        lblColNameDateCreated.Text = DateCreatedLabel
        lblColNamePrice.Text = PriceLabel
        lblNonModifyPrice.Text = PriceLabel

        ' confirm button text
        lnkConfirm.Text = ConfirmLabel
        lnkConfirmAndBack.Text = ConfirmLabel & " & go back to issue list"

        'message
        lblMsg1.Text = Msg1

        'populate specialists
        If Viewer = ApplicationSettings.ViewerSupplier Then
            If Not IsNothing(ds_WO.Tables("tblSpecialists")) Then
                If ds_WO.Tables("tblSpecialists").Rows.Count <> 0 Then
                    'filter for isdefault = true
                    pnlSpecialist.Visible = True
                    Dim dv_Specialists As DataView = ds_WO.Tables("tblSpecialists").Copy.DefaultView

                    If Not IsNothing(ds_WO.Tables(tblName).Rows(0).Item("SpecialistID")) Then
                        If ds_WO.Tables(tblName).Rows(0).Item("SpecialistID") = 0 Then
                            dv_Specialists.RowFilter = "IsDefault = 'true'"
                        End If
                    End If


                    'populate the specialist name

                    If dv_Specialists.Count <> 0 Then
                        'cond accept - select 1 specialist, and show msg please select a specialist
                        If ParentName.ToLower = "wospcaccept" Then
                            ViewState("SpecialistID") = dv_Specialists.Item(0).Item("ContactID")
                        ElseIf ParentName.ToLower = "wospaccept" Or ParentName.ToLower = "woraiseissue" Or ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "wochangeissue" Then
                            'here, show the specialist tht is actually assigned
                            If ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "wochangeissue" Or ParentName.ToLower = "woraiseissue" Then
                                If Not IsDBNull(ds_WO.Tables(tblName).Rows(0).Item("SpecialistID")) Then
                                    ViewState("SpecialistID") = ds_WO.Tables(tblName).Rows(0).Item("SpecialistID")
                                    dv_Specialists.RowFilter = "ContactID = " & ViewState("SpecialistID")
                                End If
                                ' here show the first specialist in the list as default selected
                            ElseIf ParentName.ToLower = "wospaccept" Then
                                If Not IsDBNull(ds_WO.Tables(tblName).Rows(0).Item("SupplierContactID")) Then
                                    ViewState("SpecialistID") = ds_WO.Tables(tblName).Rows(0).Item("SupplierContactID")
                                    dv_Specialists.RowFilter = "ContactID = " & ViewState("SpecialistID")
                                End If
                            End If

                            If dv_Specialists.Count <> 0 Then
                                str = ResourceMessageText.GetString("MsgAssignedSpecialist")
                                str = str.Replace("<SpecialistName>", dv_Specialists.Item(0).Item("Name"))
                                lblSpecialist.Text = str
                            Else
                                lblSpecialist.Text = ResourceMessageText.GetString("SelectSpecialist")
                            End If
                        End If
                    End If

                    DGSpecialists.DataSource = ds_WO.Tables("tblSpecialists")
                    DGSpecialists.DataBind()
                Else
                    pnlSpecialist.Visible = False
                End If
            Else
                pnlSpecialist.Visible = False
            End If
        End If

        Dim strComment As String = ""
        Dim intFlagApp As Boolean = False
        Dim intFlagGoods As Boolean = False

        ' Added by Hemisha on 12/03/2014 for SONY
        If Not IsNothing(Request("CompanyID")) Then
            If CInt(Request("CompanyID")) = 16363 And (ParentName.ToLower = "woraiseissue") Then
                If Not IsNothing(ds_WO.Tables("tblCommentsRaiseIssue")) Then
                    If ds_WO.Tables("tblCommentsRaiseIssue").Rows.Count <> 0 Then
                        For Each objDataRow As DataRow In ds_WO.Tables("tblCommentsRaiseIssue").Rows
                            strComment = objDataRow("Comments").ToString
                            If strComment.ToLower.Contains("goods") Then
                                intFlagGoods = True
                                Exit For
                            End If
                        Next

                        For Each objDataRow As DataRow In ds_WO.Tables("tblCommentsRaiseIssue").Rows
                            strComment = objDataRow("Comments").ToString
                            If strComment.ToLower.Contains("*appointment*") Then
                                intFlagApp = True
                                Exit For
                            End If
                        Next
                    End If
                End If

                ' Check for the autopopulation of the comments
                If intFlagGoods = True And intFlagApp = True Then
                    txtWOComment.Text = ""
                ElseIf intFlagApp = False Then
                    txtWOComment.Text = "*appointment* "
                ElseIf intFlagGoods = False And intFlagApp = False Then
                    txtWOComment.Text = "Goods are received on - " & Date.Today.ToShortDateString
                End If

            End If
        End If

        'populate comments
        If Not IsNothing(ds_WO.Tables("tblComments")) Then
            If ds_WO.Tables("tblComments").Rows.Count <> 0 Then
                pnlRemarks.Visible = True
                DLDiscussion.DataSource = ds_WO.Tables("tblComments")
                DLDiscussion.DataBind()
            End If
        End If

        '''''end of new code


    End Sub

    Public Sub populateProductListDD(ByVal CompanyId As Integer, ByVal SelectedProduct As Integer)
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetProductListing(CompanyId, 1, "Sequence", 0, 0, 0, False)

        If ds.Tables(0).Rows.Count > 0 Then
            trChangeService.Visible = True
            Dim liProducts As New ListItem
            liProducts = New ListItem
            liProducts.Text = "Select Service"
            liProducts.Value = 0
            drpdwnProduct.DataSource = ds.Tables(0)
            drpdwnProduct.DataTextField = "ProductName"
            drpdwnProduct.DataValueField = "ProductID"
            drpdwnProduct.DataBind()
            drpdwnProduct.Items.Insert(0, liProducts)

            'If (SelectedProduct <> 0) Then
            '    Dim liProductsToRemove As New ListItem
            '    liProductsToRemove = New ListItem
            '    liProductsToRemove.Value = SelectedProduct
            '    drpdwnProduct.Items.Remove(drpdwnProduct.Items.FindByValue(SelectedProduct))
            'End If

            Dim vListItem As ListItem = drpdwnProduct.Items.FindByValue(SelectedProduct)

            If Not vListItem Is Nothing Then
                drpdwnProduct.SelectedValue = SelectedProduct
            End If
        Else
            trChangeService.Visible = False
        End If
    End Sub

    Public Sub populateSpecialistListDD()
        Dim ds As DataSet
        Dim dt As DataTable
        ds = GetActionDetails(True)
        If Not IsNothing(ds.Tables("tblSpecialists")) Then
            dt = ds.Tables("tblSpecialists")
        End If

        If dt.Rows.Count > 0 Then
            Dim tblName As String
            If ParentName.ToLower = "wospcaccept" Or ParentName.ToLower = "wospaccept" Or ParentName.ToLower = "woraiseissue" Then
                tblName = "tblWorkOrderSummary"
                ' in case of discuss ca, discuss issue, accept ca, change terms issue, change ca, accept issue - grid is tbllastaction
            ElseIf ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Or ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "wochangeissue" Or ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "woacceptissue" Then
                tblName = "tblLastAction"
                '--
            End If
            Dim dv_Specialists As DataView = dt.Copy.DefaultView
            If Not IsNothing(ds.Tables(tblName).Rows(0).Item("SpecialistID")) Then
                If ds.Tables(tblName).Rows(0).Item("SpecialistID") = 0 Then
                    dv_Specialists.RowFilter = "IsDefault = 'true'"
                End If
            End If


            If dv_Specialists.Count <> 0 Then
                'cond accept - select 1 specialist, and show msg please select a specialist
                If ParentName.ToLower = "wospcaccept" Then
                    ViewState("SpecialistID") = dv_Specialists.Item(0).Item("ContactID")
                ElseIf ParentName.ToLower = "wospaccept" Or ParentName.ToLower = "woraiseissue" Or ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "wochangeissue" Then
                    'here, show the specialist tht is actually assigned
                    If ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "wochangeissue" Or ParentName.ToLower = "woraiseissue" Then
                        If Not IsDBNull(ds.Tables(tblName).Rows(0).Item("SpecialistID")) Then
                            ViewState("SpecialistID") = ds.Tables(tblName).Rows(0).Item("SpecialistID")
                            dv_Specialists.RowFilter = "ContactID = " & ViewState("SpecialistID")
                        End If
                        ' here show the first specialist in the list as default selected
                    ElseIf ParentName.ToLower = "wospaccept" Then
                        If Not IsDBNull(ds.Tables(tblName).Rows(0).Item("SupplierContactID")) Then
                            ViewState("SpecialistID") = ds.Tables(tblName).Rows(0).Item("SupplierContactID")
                            dv_Specialists.RowFilter = "ContactID = " & ViewState("SpecialistID")
                        End If
                    End If
                ElseIf ParentName.ToLower = "woacceptissue" Then
                    If Not IsDBNull(ds.Tables(tblName)) Then
                        ViewState("SpecialistID") = ds.Tables(tblName).Rows(0).Item("SpecialistID")
                    End If

                End If
            End If


            trChangeSpecialist.Visible = True
            Dim liSpecialists As New ListItem
            liSpecialists = New ListItem
            liSpecialists.Text = "Select Specialist"
            liSpecialists.Value = 0
            drpdwnSpecialist.DataSource = dt
            drpdwnSpecialist.DataTextField = "Name"
            drpdwnSpecialist.DataValueField = "ContactID"
            drpdwnSpecialist.DataBind()
            drpdwnSpecialist.Items.Insert(0, liSpecialists)

            Dim vListItem As ListItem = drpdwnSpecialist.Items.FindByValue(ViewState("SpecialistID"))

            If Not vListItem Is Nothing Then
                drpdwnSpecialist.SelectedValue = ViewState("SpecialistID")
            End If
        Else
            trChangeSpecialist.Visible = False
        End If
    End Sub

    Public Function GetActionDetails(Optional ByVal KillCache As Boolean = False) As DataSet
        pnlChangedData.Visible = False
        Dim ds As DataSet
        If KillCache = True Then
            Cache.Remove("WorkOrderProcess" & Session.SessionID)
        End If
        If Cache("WorkOrderProcess" & Session.SessionID) Is Nothing Then
            Dim chkValid As Integer
            Select Case ApplicationSettings.BizDivId
                Case ApplicationSettings.OWUKBizDivId
                    chkValid = 0
                Case ApplicationSettings.SFUKBizDivId
                    chkValid = 1
                Case ApplicationSettings.OWDEBizDivId
                    chkValid = 0
            End Select

            Dim PopForCompanyID As Integer
            If ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "worejectissue" Or ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Then
                If (ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "wodiscussca") And Viewer = ApplicationSettings.ViewerSupplier Then
                    PopForCompanyID = CompanyID
                Else
                    PopForCompanyID = SupCompanyID
                End If
            Else
                PopForCompanyID = CompanyID
            End If

            ds = ws.WSWorkOrder.MS_GetWO(Request("WOID"), ParentName.ToLower, PopForCompanyID, BizDivId, chkValid, Viewer)

            Cache.Add("WorkOrderProcess" & Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(Cache("WorkOrderProcess" & Session.SessionID), DataSet)
        End If
        Return ds
    End Function

    Private Sub lnkConfirmAndBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkConfirmAndBack.Click
        ViewState("ConfirmAndBack") = "True"
        Confirm()
    End Sub

    Private Sub lnkConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkConfirm.Click
        ViewState("ConfirmAndBack") = "False"
        Confirm()
    End Sub

    Private Sub Confirm()
        Page.Validate()
        If Page.IsValid Then
            Dim str As String
            divValidationMain.Visible = False

            Dim ds As DataSet = GetActionDetails()

            Dim ContactId As Integer
            ContactId = 0

            '' setting values used all over
            Dim AcceptWOLimit_Value, NewValue, NewProposedDayRate As Decimal
            Dim NewDateStart, NewDateEnd, NewAptTime As String

            Dim NewEstimatedTimeInDays As Integer
            ''in new wo creation, no (wp and pp rates)proposed rate, estimated time in days textboxes
            ''for old recs, these 3 fields are null
            ''if daily rate selected, then these 3 fields will have value
            If ParentName.ToLower = "wospaccept" Then
                AcceptWOLimit_Value = ds.Tables("tblWorkOrderSummary").Rows(0).Item("Value")
                NewDateStart = Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
                NewDateEnd = Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate)
                NewValue = ds.Tables("tblWorkOrderSummary").Rows(0).Item("Value")
                If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                    If Not IsDBNull(ds.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformDayJobRate")) Then
                        NewProposedDayRate = ds.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformDayJobRate")
                    End If
                    If Not IsDBNull(ds.Tables("tblWorkOrderSummary").Rows(0).Item("EstimatedTimeInDays")) Then
                        NewEstimatedTimeInDays = ds.Tables("tblWorkOrderSummary").Rows(0).Item("EstimatedTimeInDays")
                    End If
                End If
            ElseIf ParentName.ToLower = "wospcaccept" Or ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "woraiseissue" Or ParentName.ToLower = "wochangeissue" Then
                AcceptWOLimit_Value = txtValue.Text
                'Validation Removal Code starts
                If txtDateStart.Text <> "" Then
                    NewDateStart = txtDateStart.Text
                Else
                    NewDateStart = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
                End If
                If txtDateEnd.Text <> "" Then
                    NewDateEnd = txtDateEnd.Text
                Else
                    NewDateEnd = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
                End If

                If (ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail) Then
                    ViewState("AptTime") = ddlAptTime.SelectedValue
                End If

                'NewDateStart = txtDateStart.Text
                'NewDateEnd = txtDateEnd.Text
                'Validation Removal Code ends
                NewValue = txtValue.Text
                If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                    If Not IsDBNull(txtProposedRate.Text) Then
                        NewProposedDayRate = CDec(txtProposedRate.Text)
                    Else
                        NewProposedDayRate = 0.0
                    End If
                    NewEstimatedTimeInDays = CInt(txtEstimatedTimeInDays.Text)
                End If
            ElseIf ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "worejectissue" Or ParentName.ToLower = "worejectissue" Or ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Then
                AcceptWOLimit_Value = CDec(lblNewPrice.Text)
                NewDateStart = lblStartD.Text
                If lblEndD.Text = "" Then
                    NewDateEnd = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
                Else
                    NewDateEnd = lblEndD.Text
                End If
                NewValue = lblNewPrice.Text

                If trChangeSpecialist.Visible = True Then
                    ContactId = Integer.Parse(drpdwnSpecialist.SelectedValue)
                Else
                    ContactId = ds.Tables("tblLastAction").Rows(0).Item("SpecialistID")
                End If

                ViewState("WOContactID") = ContactId
                'new apt time
                '***
                If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                    NewProposedDayRate = CDec(lblDayRate.Text)
                    NewEstimatedTimeInDays = CInt(lblTimeInDays.Text)
                End If
                '***
            End If

            'specialist handling - only in case of suppliers
            If Viewer = ApplicationSettings.ViewerSupplier Then
                'check only in accept, caccept, raise issue, change ca, change issue
                If ParentName.ToLower = "wospaccept" Or ParentName.ToLower = "wospcaccept" Or ParentName.ToLower = "woraiseissue" Or ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "wochangeissue" Then
                    Dim anitem As DataGridItem
                    Dim isChecked As Boolean
                    For Each anitem In DGSpecialists.Items
                        isChecked = CType(anitem.FindControl("rdoValidSpecialist"), HtmlInputRadioButton).Checked
                        If isChecked Then
                            ContactId = CType(anitem.FindControl("SpecialistID"), HtmlInputHidden).Value
                            ViewState("WOContactID") = ContactId
                            If ParentName.ToLower = "woraiseissue" Then
                                Dim dv_Specialist As DataView = ds.Tables("tblSpecialists").Copy.DefaultView
                                dv_Specialist.RowFilter = "ContactID = " & ContactId
                                ViewState("SpecialistName") = dv_Specialist.Item(0).Item("Name")
                            End If
                            Exit For
                        End If
                    Next

                    ' if not even one specialist is selected then show message and exit sub
                    If Not isChecked And DGSpecialists.Items.Count <> 0 Then
                        lblError.Text = ResourceMessageText.GetString("SelectSpecialist")
                        divValidationMain.Visible = True
                        Exit Sub
                    End If
                End If

                'accept wo limit handle - in case of accept wo, caccept wo, change terms of ca, accept issue, change issue terms, discuss issue
                If ParentName.ToLower = "wospcaccept" Or ParentName.ToLower = "wospaccept" Or ParentName.ToLower = "wospchangeca" Or ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "worejectissue" Or ParentName.ToLower = "woraiseissue" Or ParentName.ToLower = "wochangeissue" Then
                    'while accepting a wo, check is performed if the supplier's accept wo limit is greater than or equal to the value of wo
                    ' if no, msg - The value of this work order exceeds your authorised spend limit. Please contact your Account Administrator for further details.
                    Dim acceptWOLimit As Decimal = 0

                    If Not IsNothing(Session("AcceptWOValueLimit")) Then
                        acceptWOLimit = Session("AcceptWOValueLimit")
                    End If

                    'The value of this work order exceeds your authorised spend limit. Please contact your Account Administrator for further details.
                    If acceptWOLimit <> 0 Then
                        If acceptWOLimit < AcceptWOLimit_Value Then
                            divValidationMain.Visible = True
                            lblError.Text = ResourceMessageText.GetString("AcceptWOLimit")
                            Exit Sub
                        End If
                    End If
                End If
            End If
            If ParentName.ToLower = "woraiseissue" Then
                ViewState("AptTime") = ddlAptTime.SelectedValue
            End If
            saveData(NewDateStart, NewDateEnd, NewValue, ContactId, NewProposedDayRate, NewEstimatedTimeInDays)
        Else
            divValidationMain.Visible = True
        End If

    End Sub

    Private Sub saveData(ByVal NewDateStart As String, ByVal NewDateEnd As String, ByVal NewValue As Decimal, ByVal ContactId As Integer, ByVal NewProposedDayRate As Decimal, ByVal NewEstimatedTimeInDays As Integer, Optional ByVal TransferFunds As Boolean = False, Optional ByVal TransferValue As Decimal = 0.0)

        Dim ds As DataSet = GetActionDetails()

        Dim RefWOID As String = ds.Tables("tblWorkOrderSummary").Rows(0).Item("RefWOID")
        Dim PONumber As String = ds.Tables("tblWorkOrderSummary").Rows(0).Item("PONumber")
        Dim CompanyId As String = ds.Tables("tblWorkOrderSummary").Rows(0).Item("CompanyId")
        Dim DateStart As String = ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart")
        Dim AptTime As String = ds.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime")
        Dim StartDate As DateTime

        If (CompanyId = OrderWorkLibrary.ApplicationSettings.ITECHSOCompany And (PONumber = "tbc" Or PONumber = "TBC")) Then
            OrderWorkLibrary.Emails.SendITECHSOJobAcceptEmail(RefWOID, DateStart, AptTime)
        End If

        Dim ds_WOSuccess As DataSet
        Dim WOTrackID As Integer
        If Not IsNothing(ds.Tables("tblLastAction")) Then
            If Not IsDBNull(ds.Tables("tblLastAction").Rows(0).Item("WOTrackingId")) Then
                WOTrackID = ds.Tables("tblLastAction").Rows(0).Item("WOTrackingId")
            Else
                WOTrackID = 0
            End If
        Else
            WOTrackID = 0
        End If
        If IsNothing(ViewState("AptTime")) Then
            ViewState("AptTime") = ""
        End If

        Dim WOStatusText As String = ""
        Dim AttachmentXSD As String = ""

        If ParentName.ToLower = "woraiseissue" Or ParentName.ToLower = "wochangeissue" Or ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "wodiscuss" Or ParentName.ToLower = "worejectissue" Then
            Dim dsWorkOrderForAttach As New WorkOrderNew
            dsWorkOrderForAttach = UCFileUpload1.ReturnFilledAttachments(dsWorkOrderForAttach)
            AttachmentXSD = dsWorkOrderForAttach.GetXml
            'Remove XML namespace attribute and its value
            AttachmentXSD = AttachmentXSD.Remove(AttachmentXSD.IndexOf("xmlns"), AttachmentXSD.IndexOf(".xsd") - AttachmentXSD.IndexOf("xmlns") + 5)
        End If

        Dim ChangeService As Integer = 0
        If ParentName.ToLower = "woacceptissue" Then
            If (trChangeService.Visible = True) Then
                If (drpdwnProduct.SelectedValue <> ds.Tables("tblWorkOrderSummary").Rows(0).Item("ProductUsed")) Then
                    ChangeService = CInt(drpdwnProduct.SelectedValue)
                End If
            End If
        End If

        If Viewer = ApplicationSettings.ViewerSupplier Then
            ds_WOSuccess = ws.WSWorkOrder.MS_PerformWOProcess(Request("WOID"), CompanyID, UserID, UserID, CompanyID, ParentName.ToLower, BizDivId, NewDateStart, NewDateEnd, NewValue, txtWOComment.Text, ContactId, ClassID, WOTrackID, ds.Tables("tblWorkOrderSummary").Rows(0).Item("DataRef"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("LastActionRef"), 0, "", AttachmentXSD, 0, NewProposedDayRate, NewEstimatedTimeInDays, ViewState("AptTime").ToString, CommonFunctions.FetchVerNum(), CommonFunctions.FetchWOTrackingVerNum(), "", 0, Session("UserId"), ChangeService)
        ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
            If Not trChangeSpecialist.Visible Then
                ds_WOSuccess = ws.WSWorkOrder.MS_PerformWOProcess(Request("WOID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompanyID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContactID"), UserID, CompanyID, ParentName.ToLower, BizDivId, NewDateStart, NewDateEnd, NewValue, txtWOComment.Text, ContactId, ClassID, WOTrackID, ds.Tables("tblWorkOrderSummary").Rows(0).Item("DataRef"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("LastActionRef"), 0, "", AttachmentXSD, 0, NewProposedDayRate, NewEstimatedTimeInDays, ViewState("AptTime"), CommonFunctions.FetchVerNum(), CommonFunctions.FetchWOTrackingVerNum(), "", 0, Session("UserId"), ChangeService)
            Else
                ds_WOSuccess = ws.WSWorkOrder.MS_PerformWOProcess(Request("WOID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompanyID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContactID"), UserID, CompanyID, ParentName.ToLower, BizDivId, NewDateStart, NewDateEnd, NewValue, txtWOComment.Text, Integer.Parse(drpdwnSpecialist.SelectedValue), ClassID, WOTrackID, ds.Tables("tblWorkOrderSummary").Rows(0).Item("DataRef"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("LastActionRef"), 0, "", AttachmentXSD, 0, NewProposedDayRate, NewEstimatedTimeInDays, ViewState("AptTime"), CommonFunctions.FetchVerNum(), CommonFunctions.FetchWOTrackingVerNum(), "", 0, Session("UserId"), ChangeService)
            End If
        End If
        If ds_WOSuccess.Tables.Count = 0 Then
            divValidationMain.Visible = True
            lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
        ElseIf ds_WOSuccess.Tables(0).Rows.Count <> 0 Then
            If ds_WOSuccess.Tables(0).Rows(0).Item("ContactID") = -1 Then
                divValidationMain.Visible = True
                If ParentName.ToLower = "wospaccept" Or ParentName.ToLower = "wospcaccept" Or ParentName.ToLower = "wospchangeca" Then
                    lblError.Text = ResourceMessageText.GetString("NoAcceptWO")
                ElseIf ParentName.ToLower = "wochangeissue" Then
                    lblError.Text = ResourceMessageText.GetString("NoChangeTermsIssue")
                ElseIf ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Then
                    lblError.Text = ResourceMessageText.GetString("NoDiscuss")
                ElseIf ParentName.ToLower = "woraiseissue" Then
                    lblError.Text = ResourceMessageText.GetString("NoRaiseIssue")
                ElseIf ParentName.ToLower = "woacceptissue" Then
                    lblError.Text = ResourceMessageText.GetString("NoAcceptIssue")
                ElseIf ParentName.ToLower = "worejectissue" Then
                    lblError.Text = ResourceMessageText.GetString("NoRejectIssue")
                ElseIf ParentName.ToLower = "woacceptca" Then
                    lblError.Text = ResourceMessageText.GetString("NoAcceptCA")
                End If
            ElseIf ds_WOSuccess.Tables(0).Rows(0).Item("ContactID") = -2 Then
                Dim link As String
                link = getBackToListingLink("Cancel")
                Response.Redirect(link)
                Exit Sub
                'If the ContactID is -10 i.e. version number mismatch
            ElseIf ds_WOSuccess.Tables(0).Rows(0).Item("ContactID") = -10 Then 'Or ds_WOSuccess.Tables(0).Rows(0).Item("ContactID") = -11 Then
                divValidationMain.Visible = True
                'Now set the status message according to the status of the WOrkorder
                Select Case ds_WOSuccess.Tables(0).Rows(0).Item("WOTrackingStatus")
                    Case ApplicationSettings.WOStatusID.Accepted
                        'lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                        WOStatusText = ApplicationSettings.WOGroupAccepted
                    Case ApplicationSettings.WOStatusID.Cancelled
                        lblError.Text = ResourceMessageText.GetString("WOVNoWOCancelled")
                        WOStatusText = ApplicationSettings.WOGroupCancelled
                        'Case ApplicationSettings.WOStatusID.Issue
                        '    lblError.Text = ResourceMessageText.GetString("WOVNoIssueRaised")
                        '    WOStatusText = "Issue"
                    Case ApplicationSettings.WOStatusID.Completed
                        'lblError.Text = ResourceMessageText.GetString("WOVNoCompleted")
                        WOStatusText = ApplicationSettings.WOGroupCompleted
                    Case ApplicationSettings.WOStatusID.Closed
                        'lblError.Text = ResourceMessageText.GetString("WOVNoClosed")
                        WOStatusText = ApplicationSettings.WOGroupClosed
                    Case ApplicationSettings.WOStatusID.CA
                        'lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                        WOStatusText = ApplicationSettings.WOGroupCA
                    Case Else
                        WOStatusText = Request.QueryString("Group")
                        'lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                End Select
                lblError.Text = ResourceMessageText.GetString("WOVersionControlMsg").Replace("<Link>", "AdminWODetails.aspx?WOID=" & Request("WOID") & "&WorkOrderID=" & Request("WorkOrderID") & "&SupContactId=" & Request("SupContactId") & "&SupCompId=" & Request("SupCompId") & "&Group=" & WOStatusText & "&CompID=" & Request("CompID") & "&FromDate=&ToDate=&PS=" & Request("PS") & "&PN=0&SC=DateCreated&SO=1&Viewer=" & ViewState("Viewer") & "&sender=UCWOsListing&BizDivID=" & ApplicationSettings.OWUKBizDivId)
                If (ds_WOSuccess.Tables.Count > 1) Then
                    If (ds_WOSuccess.Tables(1).Rows.Count > 0) Then
                        pnlChangedData.Visible = True
                        repChangedData.DataSource = ds_WOSuccess.Tables(1)
                        repChangedData.DataBind()
                    End If
                End If

            ElseIf ds_WOSuccess.Tables(0).Rows(0).Item("ContactID") = -11 Then

                divValidationMain.Visible = True

                Select Case ds_WOSuccess.Tables(0).Rows(0).Item("WOStatus")
                    Case ApplicationSettings.WOStatusID.Accepted
                        'lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                        WOStatusText = "Accepted"
                    Case ApplicationSettings.WOStatusID.Cancelled
                        'lblError.Text = ResourceMessageText.GetString("WOVNoWOCancelled")
                        WOStatusText = "Cancelled"
                    Case ApplicationSettings.WOStatusID.Issue
                        'lblError.Text = ResourceMessageText.GetString("WOVNoIssueRaised")
                        WOStatusText = "Issue"
                    Case ApplicationSettings.WOStatusID.Completed
                        'lblError.Text = ResourceMessageText.GetString("WOVNoCompleted")
                        WOStatusText = "Completed"
                    Case ApplicationSettings.WOStatusID.Closed
                        'lblError.Text = ResourceMessageText.GetString("WOVNoClosed")
                        WOStatusText = "Closed"
                    Case ApplicationSettings.WOStatusID.CA
                        'lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                        WOStatusText = "CA"
                    Case Else
                        WOStatusText = Request.QueryString("Group")
                        'lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                End Select
                lblError.Text = ResourceMessageText.GetString("WOVersionControlMsg").Replace("<Link>", "AdminWODetails.aspx?WOID=" & Request("WOID") & "&WorkOrderID=" & Request("WorkOrderID") & "&SupContactId=" & Request("SupContactId") & "&SupCompId=" & Request("SupCompId") & "&Group=" & WOStatusText & "&CompID=" & Request("CompID") & "&FromDate=&ToDate=&PS=" & Request("PS") & "&PN=0&SC=DateCreated&SO=1&Viewer=" & ViewState("Viewer") & "&sender=UCWOsListing&BizDivID=" & ApplicationSettings.OWUKBizDivId)
                If (ds_WOSuccess.Tables.Count > 1) Then
                    If (ds_WOSuccess.Tables(1).Rows.Count > 0) Then
                        pnlChangedData.Visible = True
                        repChangedData.DataSource = ds_WOSuccess.Tables(1)
                        repChangedData.DataBind()
                    End If
                End If
            Else

                'Added by Hemisha Desai on 24/02/2014 to call the webservice of SONY to update the status

                Dim strComment As String = ""
                Dim intFlag As Boolean = False
                'StartDate = ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart")
                StartDate = CDate(NewDateStart)

                If Not IsNothing(ds.Tables("tblCommentsRaiseIssue")) Then
                    If ds.Tables("tblCommentsRaiseIssue").Rows.Count <> 0 Then
                        For Each objDataRow As DataRow In ds.Tables("tblCommentsRaiseIssue").Rows
                            strComment = objDataRow("Comments").ToString
                            If strComment.ToLower.Contains("*appointment*") Then
                                intFlag = True
                                Exit For
                            End If
                        Next
                    End If
                End If
                If CInt(Request("CompanyID")) = 16363 And (ParentName.ToLower = "woacceptissue") Then
                    If intFlag = True Then
                        CommonFunctions.SendStatusUpdateToSONY(RefWOID, PONumber, "APPMD", StartDate)
                    End If
                End If

                Dim ds_WO As New DataSet
                ds_WO = GetActionDetails(True)
                Dim dSave As [Delegate] = New AfterWOSave(AddressOf sendMailer)
                ThreadUtil.FireAndForget(dSave, New Object() {ds_WOSuccess, ContactId, ds_WO})
                'sendMailer(ds_WOSuccess, ContactId)
                Dim link, GroupName As String
                If ParentName.ToLower = "wospcaccept" Then
                    GroupName = "CA"
                ElseIf ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "wospaccept" Then
                    GroupName = "Accepted"
                ElseIf ParentName.ToLower = "woraiseissue" Then
                    GroupName = "Issue"
                ElseIf ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "worejectissue" Then
                    If ds_WOSuccess.Tables("tblWOStatus").Rows(0).Item("WOStatus") = ApplicationSettings.WOStatusID.Completed Then
                        GroupName = "Completed"
                    Else
                        GroupName = "Accepted"
                    End If
                Else
                    GroupName = ""
                End If

                If (ViewState("ConfirmAndBack") = "True") Then
                    GroupName = "Issue"
                End If

                link = getBackToListingLink("Confirm", GroupName)
                Response.Redirect(link)
            End If
        Else

            Dim strComment As String = ""
            Dim intFlag As Boolean = False
            'StartDate = ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart")
            StartDate = CDate(NewDateStart)

            If Not IsNothing(ds.Tables("tblCommentsRaiseIssue")) Then
                If ds.Tables("tblCommentsRaiseIssue").Rows.Count <> 0 Then
                    For Each objDataRow As DataRow In ds.Tables("tblCommentsRaiseIssue").Rows
                        strComment = objDataRow("Comments").ToString
                        If strComment.ToLower.Contains("*appointment*") Then
                            intFlag = True
                            Exit For
                        End If
                    Next
                End If
            End If

            If CInt(Request("CompanyID")) = 16363 And (ParentName.ToLower = "woacceptissue") Then
                If intFlag = True Then
                    CommonFunctions.SendStatusUpdateToSONY(RefWOID, PONumber, "APPMD", StartDate)
                End If
            End If
            Dim ds_WO As New DataSet
            ds_WO = GetActionDetails(True)
            Dim dSave As [Delegate] = New AfterWOSave(AddressOf sendMailer)
            ThreadUtil.FireAndForget(dSave, New Object() {ds_WOSuccess, ContactId, ds_WO})
            'sendMailer(ds_WOSuccess, ContactId)
            Dim link, GroupName As String
            If ParentName.ToLower = "wospcaccept" Then
                GroupName = "CA"
            ElseIf ParentName.ToLower = "woacceptca" Or ParentName.ToLower = "wospaccept" Then
                GroupName = "Accepted"
            ElseIf ParentName.ToLower = "woraiseissue" Then
                GroupName = "Issue"
            ElseIf ParentName.ToLower = "woacceptissue" Or ParentName.ToLower = "worejectissue" Then
                If ds_WOSuccess.Tables("tblWOStatus").Rows(0).Item("WOStatus") = ApplicationSettings.WOStatusID.Completed Then
                    GroupName = "Completed"
                Else
                    GroupName = "Accepted"
                End If
            Else
                GroupName = ""
            End If

            If (ViewState("ConfirmAndBack") = "True") Then
                GroupName = "Issue"
            End If

            link = getBackToListingLink("Confirm", GroupName)
            Response.Redirect(link)
        End If

    End Sub

    Public Sub sendMailer(ByVal ds_WOSuccess As DataSet, ByVal ContactId As Integer, ByVal ds_WO As DataSet)
        'send mailer to respective party
        Dim strViewer As String = ""
        'Dim ds_WO As New DataSet
        'ds_WO = GetActionDetails(True)
        Try

            With ds_WO.Tables("tblWorkOrderSummary").Rows(0)
                If IsDBNull(.Item("DateAccepted")) Then
                    objEmail.WODate = FormatDateTime(Now.Date, DateFormat.ShortDate)
                Else
                    objEmail.WODate = FormatDateTime(.Item("DateAccepted"), DateFormat.ShortDate)
                End If
                objEmail.WorkOrderID = .Item("RefWOID")
                objEmail.WOLoc = .Item("Location")
                objEmail.WOTitle = .Item("WOTitle")
                objEmail.WOCategory = .Item("WOCategory")
                objEmail.ThermostatsQuestions = .Item("ThermostatsQuestions")
                If ParentName.ToLower <> "woacceptca" Then
                    If Viewer = ApplicationSettings.ViewerSupplier Then
                        objEmail.WOPrice = .Item("Value")

                    ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                        objEmail.WOPrice = .Item("PlatformPrice")
                    End If
                    If Not IsDBNull(.Item("EstimatedTimeInDays")) Then
                        objEmail.EstimatedTimeInDays = .Item("EstimatedTimeInDays")
                    End If

                    If Not IsDBNull(.Item("PlatformDayJobRate")) Then
                        objEmail.WorkOrderDayRate = .Item("PlatformDayJobRate")
                    End If

                    objEmail.WOStartDate = Strings.FormatDateTime(.Item("DateStart"), DateFormat.ShortDate)
                    objEmail.WOEndDate = Strings.FormatDateTime(.Item("DateEnd"), DateFormat.ShortDate)
                End If
                objEmail.WPPrice = .Item("WholesalePrice")
                objEmail.PPPrice = .Item("PlatformPrice")
                If Not IsDBNull(.Item("WholesaleDayJobRate")) Then
                    objEmail.WorkOrderWholesaleDayRate = .Item("WholesaleDayJobRate")
                    ViewState("WorkOrderWholesaleDayRate") = .Item("WholesaleDayJobRate")
                End If
            End With

            'Dim dv_CCBuyer As New DataView
            'Dim dv_CCSupplier As New DataView
            Dim dvEmail As New DataView
            Dim status As String
            objEmail.StagedWO = ViewState("StagedWO")
            Select Case ParentName.ToLower
                Case "wospcaccept"
                    status = ApplicationSettings.WOAction.WOSPCAccept
                Case "wospchangeca"
                    status = ApplicationSettings.WOAction.WOSPChangeCA
                Case "woraiseissue"
                    status = ApplicationSettings.WOAction.WORaiseIssue
                Case "wochangeissue"
                    status = ApplicationSettings.WOAction.WOChangeIssue
                Case "wospaccept"
                    status = ApplicationSettings.WOAction.WOSPAccept
                Case "woacceptca"
                    status = ApplicationSettings.WOAction.WOAcceptCA
                    'in the mail, the new price, new dates shud be sent
                    objEmail.WOPrice = lblNewPrice.Text
                    objEmail.WOStartDate = lblStartD.Text
                    objEmail.WOEndDate = lblEndD.Text
                    objEmail.EstimatedTimeInDays = lblTimeInDays.Text
                    objEmail.WorkOrderDayRate = lblDayRate.Text
                    If Not IsNothing(ViewState("WorkOrderWholesaleDayRate")) Then
                        objEmail.WorkOrderWholesaleDayRate = ViewState("WorkOrderWholesaleDayRate") 'For Buyer
                    End If
                    objEmail.StagedWO = ViewState("StagedWO")
                Case "woacceptissue"
                    status = ApplicationSettings.WOAction.WOAcceptIssue
                Case "wodiscussca"
                    status = ApplicationSettings.WOAction.WODiscussCA
                Case "wodiscuss"
                    status = ApplicationSettings.WOAction.WODiscuss
                Case "worejectissue"
                    status = ApplicationSettings.WOAction.WORejectIssue
            End Select

            Select Case ParentName.ToLower
                Case "wospaccept", "woacceptca"
                    dvEmail = ds_WOSuccess.Tables("Mailers").Copy.DefaultView
                    Dim dvStatus As New DataView(ds_WOSuccess.Tables("WOStatus"))
                    Dim WOStatus As Integer
                    If dvStatus.Count > 0 Then
                        If Not IsDBNull(dvStatus.Item(0).Item("WOStatus")) Then
                            WOStatus = dvStatus.Item(0).Item("WOStatus")
                        End If
                    End If
                    Emails.SendWorkOrderMail(objEmail, Viewer, status, txtWOComment.Text, dvEmail, WOStatus)
                Case "woacceptissue"
                    dvEmail = ds_WOSuccess.Tables("Mailers").Copy.DefaultView
                    Emails.SendWorkOrderMail(objEmail, Viewer, status, txtWOComment.Text, dvEmail)
                Case "worejectissue"
                    dvEmail = ds_WOSuccess.Tables("Mailers").Copy.DefaultView
                    Emails.SendWorkOrderMail(objEmail, Viewer, status, txtWOComment.Text, dvEmail)
                Case "wospcaccept", "wospchangeca", "woraiseissue", "wochangeissue", "wodiscussca", "wodiscuss"
                    If ParentName.ToLower = "wodiscussca" Or ParentName.ToLower = "wodiscuss" Then
                        objEmail.NewWOStartDate = lblStartD.Text
                        objEmail.NewWOEndDate = lblEndD.Text
                        objEmail.NewWOPrice = lblNewPrice.Text
                        If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                            objEmail.WorkOrderDayRateNew = lblDayRate.Text
                            objEmail.EstimatedTimeInDaysNew = lblTimeInDays.Text
                        End If
                        objEmail.StagedWO = ViewState("StagedWO")
                    Else
                        objEmail.NewWOStartDate = txtDateStart.Text
                        objEmail.NewWOEndDate = txtDateEnd.Text
                        objEmail.NewWOPrice = txtValue.Text
                        If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                            objEmail.WorkOrderDayRateNew = txtProposedRate.Text
                            objEmail.EstimatedTimeInDaysNew = txtEstimatedTimeInDays.Text
                        End If
                        objEmail.StagedWO = ViewState("StagedWO")
                    End If
                    dvEmail = ds_WOSuccess.Tables("Mailers").Copy.DefaultView
                    Emails.SendWorkOrderMail(objEmail, Viewer, status, txtWOComment.Text, dvEmail)
            End Select

            'send mailer to supplier who loose work order
            If ParentName.ToLower = "wospaccept" Or ParentName.ToLower = "woacceptca" Then
                Dim drow_LostMailer As DataRowView
                Dim dv_LostMailer As DataView = ds_WOSuccess.Tables("LostDiscardSupp").Copy.DefaultView
                dv_LostMailer.RowFilter = "Recipient = 'To' and CA =1"
                Dim dv_LostToCC As DataView = ds_WOSuccess.Tables("LostDiscardSupp").Copy.DefaultView
                For Each drow_LostMailer In dv_LostMailer
                    dv_LostToCC.RowFilter = "Recipient = 'CC' AND CompanyID = " & drow_LostMailer.Item("CompanyID")
                    Emails.SendLostWOMail(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("WorkOrderId"), drow_LostMailer, dv_LostToCC)
                Next
            End If
        Catch ex As Exception
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error occurred in wocloseComplete (" & " Exception details" & ex.ToString, "Error in UCCloseComplete")
        End Try
    End Sub

    Private Sub lnkCancel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.ServerClick
        Dim link As String
        link = getBackToListingLink("Cancel")
        Response.Redirect(link)
    End Sub
    'Added Code by Pankaj Malav on 23 Dec 2008
    'To cater for Search WO Raise Issue Processing on Back to listing and on confirm
    Public Function getBackToListingLink(ByVal BtnIdentify As String, Optional ByVal GroupName As String = "") As String
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            If Request("sender") = "UCWOsListing" Or BtnIdentify = "Confirm" Or Request("sender") = "UCWODetails" Then
                If Request("sender") = "SearchWO" Then
                    link = "SearchWOs.aspx?"
                    link &= "&SrcWorkorderID=" & Request("txtWorkorderID")
                    link &= "&SrcCompanyName=" & Request("CompanyName")
                    link &= "&SrcKeyword=" & Request("KeyWord")
                    link &= "&SrcPONumber=" & Request("PONumber")
                    link &= "&SrcReceiptNumber=" & Request("ReceiptNumber")
                    link &= "&SrcPostCode=" & Request("PostCode")
                    link &= "&SrcDateStart=" & Request("DateStart")
                    link &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
                    link &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")

                    Return (link)
                ElseIf (Request("sender") = "UCWODetails" Or Request("Action").ToLower = "acceptissue" Or Request("Action").ToLower = "rejectissue") And ViewState("ConfirmAndBack") <> "True" Then
                    link = "AdminWODetails.aspx?"
                Else
                    If Viewer = ApplicationSettings.ViewerSupplier Then
                        link = "~\SecurePages\SupplierWOListing.aspx?"
                    ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                        If GroupName <> "" Then
                            If GroupName = ApplicationSettings.WOGroupIssue Then
                                link = "AdminWOIssueListing.aspx?"
                            Else
                                link = "AdminWOListing.aspx?"
                            End If
                        Else
                            If Request("Group") = ApplicationSettings.WOGroupIssue Then
                                link = "AdminWOIssueListing.aspx?"
                            Else
                                link = "AdminWOListing.aspx?"
                            End If
                        End If

                    End If
                End If
            ElseIf Request("sender") = "SearchWO" Then
                link = "SearchWOs.aspx?"
                link &= "&SrcWorkorderID=" & Request("txtWorkorderID")
                link &= "&SrcCompanyName=" & Request("CompanyName")
                link &= "&SrcKeyword=" & Request("KeyWord")
                link &= "&SrcPONumber=" & Request("PONumber")
                link &= "&SrcReceiptNumber=" & Request("ReceiptNumber")
                link &= "&SrcPostCode=" & Request("PostCode")
                link &= "&SrcDateStart=" & Request("DateStart")
                link &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
                link &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")
                link &= "&PS=" & Request("PS")
                link &= "&PN=" & Request("PN")
                link &= "&SC=" & Request("SC")
                link &= "&SO=" & Request("SO")
                Return (link)
            Else
                If Viewer = ApplicationSettings.ViewerSupplier Then
                    link = "SupplierWODetails.aspx?"
                ElseIf Viewer = ApplicationSettings.ViewerBuyer Then
                    link = "BuyerWODetails.aspx?"
                ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                    link = "AdminWODetails.aspx?"
                End If
            End If
            link &= "WOID=" & Request("WOID")
            If (Request("Action").ToLower <> "acceptissue" And Request("Action").ToLower <> "rejectissue") Then
                link &= "&ContactID=" & Request("ContactID")
                link &= "&CompanyID=" & Request("CompanyID")

            End If
            link &= "&Viewer=" & Request("Viewer")
            If GroupName <> "" Then
                link &= "&Group=" & GroupName
                link &= "&mode=" & GroupName
            Else
                link &= "&Group=" & Request("Group")
                link &= "&mode=" & Request("Group")
            End If
            link &= "&BizDivID=" & Request("BizDivID")
            link &= "&FromDate=" & Request("FromDate")
            link &= "&ToDate=" & Request("ToDate")
            link &= "&PS=" & Request("PS")
            link &= "&PN=" & Request("PN")
            link &= "&SC=" & Request("SC")
            link &= "&SO=" & Request("SO")
            link &= "&sender=" & "UCWOsListing"
            If Not IsNothing(Request("IsNextDay")) Then
                If Request("IsNextDay") = "Yes" Then
                    link &= "&IsNextDay=" & "Yes"
                Else
                    link &= "&IsNextDay=" & "No"
                End If
            End If
            Return link
        End If
        Return link
    End Function

    Public Sub custValiDateRange_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rqWODateRange.ServerValidate
        If Page.IsValid Then
            If txtDateStart.Text.Trim <> "" And txtDateEnd.Text.Trim <> "" Then
                Try
                    Dim tmpStartDate As DateTime = DateTime.Parse(txtDateStart.Text.Trim)
                    Dim tmpEndDate As DateTime = DateTime.Parse(txtDateEnd.Text.Trim)


                    Dim DateStart As New Date
                    Dim DateEnd As New Date
                    DateStart = tmpStartDate
                    DateEnd = tmpEndDate
                    If DateStart <= DateEnd Then
                        args.IsValid = True
                    Else
                        args.IsValid = False
                        rqWODateRange.ErrorMessage = "End Date should be greater than Start Date"
                    End If
                Catch ex As Exception
                    args.IsValid = False
                    rqWODateRange.ErrorMessage = ResourceMessageText.GetString("errInvalidDate")
                End Try
            End If
        End If
    End Sub


    Public Function getlink(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachment")
                Dim str As String = filePath.Substring(index)
                'str = ApplicationSettings.AttachmentDisplayPath(4) + str
                str = ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/" + str + CommonFunctions.AzureStorageAccessKey().ToString
                Return str.Replace("\", "/")
            End If
        End If
        Return ""
    End Function
End Class