Partial Public Class UCMSCompAccreds
    Inherits System.Web.UI.UserControl

    Shared ws As New WSObjs

    Public Property BizDivId() As Integer
        Get
            If Not IsNothing(ViewState("AccredBizDivId")) Then
                Return ViewState("AccredBizDivId")
            Else
                Return ApplicationSettings.BizDivId
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("AccredBizDivId") = value
        End Set
    End Property
    Public Property CompanyID() As Integer
        Get
            If Not IsNothing(ViewState("CompanyID")) Then
                Return ViewState("CompanyID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("CompanyID") = value
        End Set
    End Property

    Private _showMemberno As Boolean = True
    Public Property ShowMemberNo() As Boolean
        Get
            Return _showMemberno
        End Get
        Set(ByVal value As Boolean)
            _showMemberno = value
        End Set
    End Property

    Private _showColMemberno As Boolean = True
    Public Property ShowColMemberNo() As Boolean
        Get
            Return _showColMemberno
        End Get
        Set(ByVal value As Boolean)
            _showColMemberno = value
        End Set
    End Property

    Private _showColDate As Boolean = True
    Public Property ShowColDate() As Boolean
        Get
            Return _showColDate
        End Get
        Set(ByVal value As Boolean)
            _showColDate = value
        End Set
    End Property

    Private _cachekey As String = ""
    Public Property AppendCachekey() As String
        Get
            Return _cachekey
        End Get
        Set(ByVal value As String)
            _cachekey = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            populateVendors(True)
            PopulateSelectedGrid()
        End If
    End Sub

    Public Function GetVendors(Optional ByVal killCache As Boolean = False)
        Dim ds As New DataSet

        ' this gets all vendors and selected vendors
        Dim CacheKey As String = "VendorsList" & Session.SessionID & BizDivId & CompanyID
        If _cachekey <> "" Then
            CacheKey = CacheKey & "-" & _cachekey
        End If

        If killCache Then
            Cache.Remove(CacheKey)
        End If

        If Cache(CacheKey) Is Nothing Then
            ds = ws.WSContact.GetCompanyAccreditations(BizDivId, CompanyID)
            Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        Else
            ds = CType(Page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function

    Public Sub populateVendors(Optional ByVal killCache As Boolean = False)
        Dim ds As New DataSet
        Dim dtAllVendors As New DataTable
        Dim dtSelectedVendors As New DataTable
        Dim dvSelectedVendors As New DataView

        ds = GetVendors(killCache)
        'this dt has list of all vendors
        dtAllVendors = ds.Tables("tblAllVendors")
        'this dt has list of all selected vendors
        dtSelectedVendors = ds.Tables("tblSelectedVendors")
        dvSelectedVendors = dtSelectedVendors.DefaultView

        ' this cache stores the list of unselectd vendors
        Dim CacheKey As String = "UnSelectedVendors" & Session.SessionID & BizDivId & CompanyID

        If killCache Then
            Page.Cache.Remove(CacheKey)
        End If

        'this ds has unselected vendors
        Dim dtUnSelectedVendors As DataTable = CType(Page.Cache(CacheKey), DataTable)

        'if unselected vendors ds has something, then use tht ds
        If IsNothing(dtUnSelectedVendors) Or killCache = False Then
            'else use all vendors ds
            dtUnSelectedVendors = dtAllVendors
        End If

        'if there are a few recs in the list of unselected vendors, then show the vendors box
        If dtUnSelectedVendors.Rows.Count = 0 Then
            pnlAccreds.Visible = False
        Else
            pnlAccreds.Visible = True
        End If

        Dim dvUnSelectedVendors As DataView = dtAllVendors.Copy.DefaultView
        dvUnSelectedVendors.Sort = "VendorName"
        lstBoxVendor.DataSource = dvUnSelectedVendors
        lstBoxVendor.DataBind()
        Page.Cache.Remove(CacheKey)
        Page.Cache.Add(CacheKey, dtUnSelectedVendors, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
    End Sub

    Public Sub PopulateSelectedGrid()
        Dim ds As DataSet = GetVendors()
        Dim dtSelectedVendors As DataTable = ds.Tables("tblSelectedVendors")
        Dim dvSelectedVendors As New DataView
        If Not IsDBNull(dtSelectedVendors) Then
            If dtSelectedVendors.Rows.Count > 0 Then
                dvSelectedVendors = dtSelectedVendors.DefaultView
                dvSelectedVendors.RowFilter = ""
                pnlSelectedAccreds.Visible = True
                '' show hide column depending on the property value
                If Not _showColDate Then
                    tdColDate.Visible = False
                    gvSelectedAccreds.Columns(5).Visible = False
                End If

                If Not _showColMemberno Then
                    tdColMemberNo.Visible = False
                    gvSelectedAccreds.Columns(4).Visible = False
                End If
                gvSelectedAccreds.DataSource = dvSelectedVendors
                gvSelectedAccreds.DataBind()
                If dtSelectedVendors.Rows.Count > 10 Then
                    divAccreds.Attributes.Add("style", "height:208px")
                Else
                    divAccreds.Attributes.Remove("style")
                End If
            Else
                pnlSelectedAccreds.Visible = False
            End If
        End If
    End Sub

    Private Sub lstBoxVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstBoxVendor.SelectedIndexChanged
        lblLevelType.Visible = False
        tdPartnerMember.Visible = False
        tblSubmit.Visible = False
        ' do not show has skill if its already added as selection or Partner is added
        Dim ds As DataSet = GetVendors()
        Dim dtSelectedVendors As DataTable = ds.Tables("tblSelectedVendors")
        ' check if the record already exist, if yes then show message to the user
        Dim vendorID As Integer = lstBoxVendor.SelectedItem.Value
        Dim dvSelectedvendors As DataView = dtSelectedVendors.Copy.DefaultView
        dvSelectedvendors.RowFilter = "HasSkill = 'Yes'  and VendorID = '" & vendorID & "'"
        Dim showMsgCnt As Integer = 0
        If dvSelectedvendors.Count > 0 Then
            HasSkill.Checked = False
            HasSkill.Visible = False
            showMsgCnt = showMsgCnt + 1
        Else
            HasSkill.Checked = False
            HasSkill.Visible = True
        End If

        ' check if its covers all partner
        dvSelectedvendors.RowFilter = "HasSkill <> 'Yes'  and VendorID = '" & vendorID & "'"
        If dvSelectedvendors.Count >= ReturnPartnerCount(ds.Tables("tblAllPartners"), vendorID) Then
            HasPartner.Checked = False
            HasPartner.Visible = False
            showMsgCnt = showMsgCnt + 1
        Else
            HasPartner.Checked = False
            HasPartner.Visible = True
        End If

        If showMsgCnt = 2 Then
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "ValidationMsg", "alert('All vendor\'s accreditation for this vendor are already added.');", True)
        End If
    End Sub


    Public Sub populatePartners(ByVal VendorID As Integer, Optional ByVal killCache As Boolean = False)
        Dim ds As New DataSet
        ds = GetVendors(killCache)
        Dim dtSelectedPartners As New DataTable
        Dim dtAllPartners As New DataTable
        Dim dvSelectedPartners As New DataView

        'this dt has list of all partners
        dtAllPartners = ds.Tables("tblAllPartners")
        'this dt has list of all selected partners
        dtSelectedPartners = ds.Tables("tblSelectedVendors")
        dvSelectedPartners = dtSelectedPartners.DefaultView

        ' list selected partners for the vendor
        Dim strSelpartner As String = ""
        If VendorID <> -1 Then
            dvSelectedPartners.RowFilter = "VendorID = " & VendorID
            For Each dvRow As DataRowView In dvSelectedPartners
                If strSelpartner = "" Then
                    strSelpartner = dvRow("PartnerID")
                Else
                    strSelpartner = strSelpartner & "," & dvRow("PartnerID")
                End If
            Next

            Dim dvShowPartner As DataView
            dvShowPartner = dtAllPartners.Copy.DefaultView
            If strSelpartner <> "" Then
                dvShowPartner.RowFilter = "VendorID = " & VendorID & " and PartnerID not in (" & strSelpartner & ")"
            Else
                dvShowPartner.RowFilter = "VendorID = " & VendorID
            End If

            lstBoxPartner.DataSource = dvShowPartner
            lstBoxPartner.DataBind()
        End If


    End Sub

    Private Sub gvSelectedAccreds_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSelectedAccreds.PreRender
        If Not IsNothing(gvSelectedAccreds) Then
            If Not IsNothing(gvSelectedAccreds.HeaderRow) Then
                gvSelectedAccreds.HeaderRow.Visible = False
            End If
        End If
    End Sub

    Public Sub HasSkill_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles HasSkill.CheckedChanged
        lblLevelType.Visible = False
        tdPartnerMember.Visible = False
        tblSubmit.Visible = True
    End Sub

    Private Sub HasPartner_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles HasPartner.CheckedChanged
        lblLevelType.Visible = True
        tdPartnerMember.Visible = True
        If _showMemberno Then
            tdMember.Visible = True
        Else
            tdMember.Visible = False
        End If
        tblSubmit.Visible = True
        populatePartners(lstBoxVendor.SelectedValue)
    End Sub

    Private Sub lstBoxPartner_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstBoxPartner.SelectedIndexChanged
        tblSubmit.Visible = True
    End Sub

    Private Sub lnkSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSelect.Click
        If HasPartner.Checked = True And lstBoxPartner.SelectedIndex < 0 Then
            lblMessage.Text = ResourceMessageText.GetString("SelectPartner")
            Return
        Else
            lblMessage.Text = ""
            Dim ds As DataSet = GetVendors()
            Dim dtSelectedVendors As DataTable = ds.Tables("tblSelectedVendors")

            Dim dtVenPartUC As UniqueConstraint = New UniqueConstraint(New DataColumn() _
                              {dtSelectedVendors.Columns("VendorID"), _
                               dtSelectedVendors.Columns("PartnerID")})
            Try
                Dim dvCons As Constraint
                For Each dvCons In dtSelectedVendors.Constraints
                    If dvCons.GetType.ToString <> "UniqueConstraint" Then
                        dtSelectedVendors.Constraints.Remove(dvCons)
                    End If
                Next
            Catch ex As Exception

            End Try

            Try
                dtSelectedVendors.Constraints.Add(dtVenPartUC)
            Catch ex As Exception

            End Try


            'this adds partner to the list
            For Each lItem As ListItem In lstBoxPartner.Items
                If lItem.Selected Then
                    Dim dRow As DataRow = dtSelectedVendors.NewRow
                    If HasPartner.Checked = True Then
                        dRow("PartnerID") = lItem.Value
                        dRow("PartnerName") = lItem.Text.ToString.Trim
                        dRow("MemberShipNo") = Trim(txtMembershipNo.Text)
                        dRow("HasSkill") = ""

                        dRow("VendorID") = lstBoxVendor.SelectedItem.Value
                        dRow("VendorName") = lstBoxVendor.SelectedItem.Text.ToString.Trim
                        dRow("Date") = Date.Today
                        dtSelectedVendors.Rows.Add(dRow)
                    End If
                End If
            Next

            'this adds skill to the list
            If HasSkill.Checked = True Then
                Dim dRow As DataRow = dtSelectedVendors.NewRow
                dRow("PartnerID") = 0
                dRow("PartnerName") = ""
                dRow("MemberShipNo") = ""
                dRow("HasSkill") = "Yes"
                dRow("VendorID") = lstBoxVendor.SelectedItem.Value
                dRow("VendorName") = lstBoxVendor.SelectedItem.Text.ToString.Trim
                dRow("Date") = Date.Today
                dtSelectedVendors.Rows.Add(dRow)
            End If

            Dim CacheKey As String = "VendorsList" & Session.SessionID & BizDivId & CompanyID
            Page.Cache.Remove(CacheKey)
            Page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            populateVendors()
            PopulateSelectedGrid()

            'hide the controls
            HasSkill.Visible = False
            HasPartner.Visible = False
            lblLevelType.Visible = False
            tdPartnerMember.Visible = False
            tblSubmit.Visible = False
            HasSkill.Checked = False
            HasPartner.Checked = False
            'Code added by PB: To fix the Bug - Membership Number should be reset to null for each new accreditation being submitted 
            txtMembershipNo.Text = ""
        End If
    End Sub

    Public Sub RemoveCallback(ByVal tag As String, ByVal obj As Object, ByVal reason As CacheItemRemovedReason)
        If (CacheItemRemovedReason.Underused = reason) Then
            Cache.Remove(tag)
            If Not IsNothing(obj) Then
                Cache.Insert(tag, obj, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            End If
        Else
            Cache.Remove(tag)
        End If
    End Sub

    Private Sub lnkRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemove.Click
        lblLevelType.Visible = False
        tdPartnerMember.Visible = False
        tblSubmit.Visible = False
        HasSkill.Checked = False
        HasPartner.Checked = False
        HasSkill.Visible = False
        HasPartner.Visible = False

        Dim ds As DataSet = GetVendors()
        Dim dtSelectedVendors As DataTable = ds.Tables("tblSelectedVendors")
        Dim dvSelectedVendors As New DataView
        dvSelectedVendors = dtSelectedVendors.Copy.DefaultView

        Dim flagCheck As Boolean = False
        'add primary key as combid 
        Dim keys(1) As DataColumn
        keys(0) = dtSelectedVendors.Columns("VendorID")

        'unselected vendors
        Dim CacheKey As String = "UnSelectedVendors" & Session.SessionID & BizDivId & CompanyID
        Dim dtUnSelectedVendors As DataTable = CType(Page.Cache(CacheKey), DataTable)

        'Iteration through grid
        For Each row As GridViewRow In gvSelectedAccreds.Rows
            Dim chkBox As CheckBox = CType(row.FindControl("CheckAccred"), CheckBox)
            If chkBox.Checked Then
                'dtSelectedVendors.PrimaryKey = keys
                flagCheck = True
                If dtSelectedVendors.Rows.Count > 0 Then
                    Dim id As String
                    id = CType(row.FindControl("hdnVendorId"), HtmlInputHidden).Value
                    Dim idArr As String()
                    idArr = id.Split("#")
                    Dim foundrow As DataRow() = dtSelectedVendors.Select("VendorID = '" & idArr(0) & "' and (PartnerID + '' = '" & idArr(1) & "')")
                    If Not (foundrow Is Nothing) Then
                        dtSelectedVendors.Rows.Remove(foundrow(0))
                    End If
                End If

            End If
        Next
        'Add cache for un selected vendors
        Page.Cache.Remove(CacheKey)
        Page.Cache.Add(CacheKey, dtUnSelectedVendors, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)

        Dim CacheKeySelected As String = "VendorsList" & Session.SessionID & BizDivId & CompanyID
        Page.Cache.Remove(CacheKeySelected)
        Page.Cache.Add(CacheKeySelected, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)

        'Show validation message
        If flagCheck Then
            lblMessage.Text = ""
        Else
            lblMessage.Text = ResourceMessageText.GetString("SeleteVendorDel")
        End If

        populateVendors()
        PopulateSelectedGrid()
    End Sub


    Private Function ReturnPartnerCount(ByVal dt As DataTable, ByVal vendorID As Integer) As Integer
        Dim dv As DataView = dt.Copy.DefaultView
        dv.RowFilter = "VendorID = '" & vendorID & "'"
        Return dv.Count
    End Function
End Class