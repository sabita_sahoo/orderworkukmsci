''' <summary>
''' To implement UCMenu
''' </summary>
''' <reference>http://msdn.microsoft.com/msdnmag/issues/02/08/ASPNETCommerceApp/</reference>
''' <remarks></remarks>

Partial Public Class UCMenu
    Inherits System.Web.UI.UserControl




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' preventing pages being cached downstream 
        '        Response.Cache.SetNoStore()

        Try

            Select Case ApplicationSettings.BizDivId
                Case ApplicationSettings.SFUKBizDivId
                    tdWelcome.Visible = True

            End Select

            Dim ClientMenu As Boolean = False
            If IsNothing(Session("SecondaryClassID")) Then
                If Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
                    ClientMenu = False
                ElseIf Session("ContactClassID") = ApplicationSettings.RoleClientID Then
                    ClientMenu = True
                End If
            Else
                ClientMenu = False
            End If


            If ClientMenu = True Then
                divMenuOuter.Attributes.Add("class", "divMenuClient ClientSupplierMenu")
                divTopCurve.Attributes.Add("class", "roundtopOrange")
                divBottomCurve.Attributes.Add("class", "roundbottomOrange")
                imgTopCurve.Src = "~/Images/Curves/OrangeMenu-LTC.gif"
                imgBottomCurve.Src = "~/Images/Curves/OrangeMenu-LBC.gif"
            Else
                divMenuOuter.Attributes.Add("class", "divMenuSupplier ClientSupplierMenu")
                divTopCurve.Attributes.Add("class", "roundtopRed")
                divBottomCurve.Attributes.Add("class", "roundbottomRed")
                imgTopCurve.Src = "~/Images/Curves/Red-LTC.gif"
                imgBottomCurve.Src = "~/Images/Curves/Red-LBC.gif"
            End If

            'Ctype method to cast the object returned from the cache to type DataSet
            Dim ds As DataSet = CType(Cache("LoggedInfo-" & Session("UserName") & Session.SessionID), DataSet)

            'returns the filename of the page onto which the control is added
            Dim filepath As String = Page.Request.Url.PathAndQuery.ToLower
            Dim filepathNoQry As String = Page.Request.FilePath.ToLower
            Dim pagewithqry, pagenoqry As String
            pagewithqry = filepath.Substring(filepathNoQry.LastIndexOf("/") + 1)
            pagenoqry = filepathNoQry.Substring(filepathNoQry.LastIndexOf("/") + 1)


            'Creating a copy of table "Menu"
            Dim dv As DataView = ds.Tables(1).Copy.DefaultView
            'Creating a copy of table "ChildMenu"
            Dim dvSub As DataView = ds.Tables(2).Copy.DefaultView


            'Iterate to the filtered table1 "Menu Table" to check for the page requested 
            Dim flagMain As Boolean = False
            Dim i As Integer
            Dim j As Integer
            For i = 0 To dv.Count - 1
                If dv.Item(i).Item("MenuLink").ToLower = pagenoqry Or dv.Item(i).Item("MenuLink").ToLower = pagewithqry Then
                    flagMain = True
                    Exit For
                End If
            Next

            'if flagMain is false then process Child Menu table for the requesting page is any of childmenu page
            If flagMain = False Then
                For j = 0 To dvSub.Count - 1
                    If dvSub.Item(j).Item("MenuLink").ToLower = pagenoqry Or dvSub.Item(j).Item("MenuLink").ToLower = pagewithqry Then
                        For i = 0 To dv.Count - 1
                            If dv.Item(i).Item("MenuID") = dvSub.Item(j).Item("ParentMenuID") Then
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
            End If

            'Add parent menu id to the session and which is the index
            Try
                Session.Add("ParentMenuID", dv.Item(i).Item("MenuID"))
                RPTMenu.SelectedIndex = i
            Catch ex As Exception
            End Try

            RPTMenu.DataSource = dv
            RPTMenu.DataBind()

        Catch TransEx As Exception
            '      Throw TransEx 'ExceptionUtil.CreateSoapException("Error in UCMenu: " &  TransEx.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Error While trying to render UCMenu.")
        End Try


    End Sub

End Class