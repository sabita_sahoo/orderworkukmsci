
''' <summary>
''' Class to handle Location Listing user control's functionality
''' </summary>
''' <remarks></remarks>
Partial Public Class UCLocationListing
    Inherits System.Web.UI.UserControl

    Shared ws As New WSObjs
    Dim objDs As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            hdnbizid.Value = Request("bizDivId")
            gvContacts.Sort("LocationName", SortDirection.Ascending)
            Dim contactid As Integer = HttpContext.Current.Items("ContactID")
            Dim bizDivId As Integer = HttpContext.Current.Items("bizDivId")
            'poonam modified on 12/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            btnAddLocation.HRef = ApplicationSettings.WebPath & "LocationForm.aspx?CompanyID=" & contactid & "&bizDivId=" & bizDivId & "&classId=" & Request("classId")
            If ((Request("sender") = "buyer") Or (Request("classid") = ApplicationSettings.RoleClientID)) Then
                lnkBackToListing.HRef = "~\CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            Else
                lnkBackToListing.HRef = "~\CompanyProfile.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            End If
        End If


    End Sub

    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' To return dataset for location listing
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                    ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim siteType As String = "site"
        Dim contactID As Integer = HttpContext.Current.Items("ContactID")
        Dim recordCount As Integer
        Dim ds As DataSet

        Select Case ApplicationSettings.SiteType

            Case ApplicationSettings.siteTypes.admin
                siteType = "admin"
            Case ApplicationSettings.siteTypes.site
                siteType = "site"
        End Select

        'ds = GetContactLocation(contactID, sortExpression, startRowIndex, maximumRows, recordCount, False)
        ' Since there is issue with accessing the page variable, fetching data directly without caching.
        ds = ws.WSContact.GetLocationListing(contactID, siteType, sortExpression, startRowIndex, maximumRows, recordCount, txtSearch.Text.Trim)

        HttpContext.Current.Items("rowCount") = recordCount
        Return ds
    End Function

    ''' <summary>
    ''' Method to fetch location dataset to be displayed in the grid view
    ''' </summary>
    ''' <param name="contactID">Contact ID</param>
    ''' <param name="sortExpression">Sort Expression specify the column to sort</param>
    ''' <param name="startRowIndex">Start row index</param>
    ''' <param name="maximumRows">Maximum rows per page</param>
    ''' <param name="recordCount">Number of record counts</param>
    ''' <param name="KillCache">Killcahe as boolean</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Public Function GetContactLocation(ByVal contactID As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, _
    '                                ByVal maximumRows As Integer, ByVal recordCount As Integer, Optional ByVal KillCache As Boolean = False) As DataSet
    '    Dim cacheKey As String = "ContactLocation" & "-" & CStr(contactID) & "-" & ViewState("sessionid")
    '    If KillCache = True Then
    '        Page.Cache.Remove(cacheKey)
    '    End If
    '    Dim ds As DataSet
    '    Dim siteType As String = "site"
    '    Select Case ApplicationSettings.SiteType

    '        Case ApplicationSettings.siteTypes.admin
    '            siteType = "admin"
    '        Case ApplicationSettings.siteTypes.site
    '            siteType = "site"
    '    End Select
    '    If Cache(cacheKey) Is Nothing Then
    '        ds = ws.WSContact.GetLocationListing(contactID, siteType, sortExpression, startRowIndex, maximumRows, recordCount, "")
    '    Else
    '        ds = CType(Page.Cache(cacheKey), DataSet)

    '    End If
    '    Return ds
    'End Function



    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvContacts, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvContacts.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvContacts.DataBind()
    End Sub

    ''' <summary>
    ''' GridView event handler to call MakeGridViewHeaderClickable method
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowDataBound
        MakeGridViewHeaderClickable(gvContacts, e.Row)
    End Sub

    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Private Sub ExportToExcel_click(ByVal sender As Object, ByVal args As EventArgs) Handles btnExportToExcel.Click
        Dim siteType As String = "site"
        Select Case ApplicationSettings.SiteType

            Case ApplicationSettings.siteTypes.admin
                siteType = "admin"
            Case ApplicationSettings.siteTypes.site
                siteType = "site"
        End Select
        DataSetToExcel.ConvertNew(ws.WSContact.GetLocationListing(HttpContext.Current.Items("ContactID"), siteType, "AddressID", 0, 100, 1000, ""), Response)

    End Sub

    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    Private Sub ObjectDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles ObjectDataSource1.Selecting
        If ApplicationSettings.SiteType <> ApplicationSettings.siteTypes.admin Then
            gvContacts.Columns.Item(6).Visible = False
        Else
            gvContacts.Columns.Item(6).Visible = True
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim.Length > 0 Then
            ViewState("FilterString") = txtSearch.Text.Trim
        Else
            ViewState("FilterString") = ""
        End If
        gvContacts.DataBind()

        If gvContacts.Rows.Count = 0 Then
            gvContacts.DataBind()
        End If
    End Sub

    Private Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.Click
        txtSearch.Text = ""
        ViewState("FilterString") = ""
        gvContacts.DataBind()
    End Sub

End Class