


Imports System.Text.RegularExpressions

''' <summary>
''' Class to handle Company Profile User Control Functionality.
''' </summary>
''' <remarks></remarks>
Partial Public Class UCMSCompanyProfile
    Inherits System.Web.UI.UserControl

    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs



    ''' <summary>
    ''' Property to set or get company value for UCCompanyProfile
    ''' </summary>
    ''' <remarks></remarks>
    Private _companyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property

    Private _bizDivID As Integer
    Public Property BizDivID() As Integer
        Get
            Return _bizDivID
        End Get
        Set(ByVal value As Integer)
            _bizDivID = value
        End Set
    End Property


    Private _userID As Integer
    Public Property UserID() As Integer
        Get
            Return _userID
        End Get
        Set(ByVal value As Integer)
            _userID = value
        End Set
    End Property


    Private _classID As Integer
    Public Property ClassID() As Integer
        Get
            Return _classID
        End Get
        Set(ByVal value As Integer)
            _classID = value
        End Set
    End Property


    Private _showEmail As Boolean
    Public Property ShowEmail() As Boolean
        Get
            Return _showEmail
        End Get
        Set(ByVal value As Boolean)
            _showEmail = value

        End Set
    End Property


    Private _showBackToListing As Boolean
    Public Property ShowBackTolisting() As Boolean
        Get
            Return _showBackToListing
        End Get
        Set(ByVal value As Boolean)
            _showBackToListing = value

        End Set
    End Property

    Private _BusinessArea As Integer = 0
    Public Property BusinessArea() As Integer
        Get
            Return _BusinessArea
        End Get
        Set(ByVal value As Integer)
            _BusinessArea = value
        End Set
    End Property


    'Following session variable added to fix the issue: Admin Company profile back to listing does not work
    Private _strBackToListing As String = ""
    Public Property strBackToListing() As String
        Get
            Return _strBackToListing
        End Get
        Set(ByVal value As String)
            _strBackToListing = value
        End Set
    End Property

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'btnSaveTop.Attributes.Add("onClick", "javascript:callSetFocusValidation('UCCompanyProfile1')")
        'btnSaveBottom.Attributes.Add("onClick", "javascript:callSetFocusValidation('UCCompanyProfile1')")
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then
            'Remove validation format check for VAT registration number,Bank Sort Code number,Bank Account number
            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin And ApplicationSettings.Country = ApplicationSettings.CountryDE Then
                If Request("classid") = ApplicationSettings.RoleSupplierID Then
                    rgVatNo.Enabled = False
                    regSortCode.Enabled = False
                    rgAccNo.Enabled = False

                End If
            End If

            Select Case ApplicationSettings.SiteType
                ' Setting evironment for the company profile page when opened in admin site
                Case ApplicationSettings.siteTypes.admin
                    If ApplicationSettings.Country = ApplicationSettings.CountryDE And Request("classid") = ApplicationSettings.RoleSupplierID Then
                        tdBankLabel.Visible = True
                        tdBankBox.Visible = True
                    End If
                    If Request("classid") = ApplicationSettings.RoleSupplierID Then
                        lblUser1.Text = "Supplier"
                        lblUser2.Text = "Supplier"
                        ViewState("viewer") = "sp"
                        'tblAccountManager.Visible = False
                        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                    ElseIf ((Request("classid") = ApplicationSettings.RoleClientID) Or ((Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleClientID)) Then
                        lblUser1.Text = "Buyer"
                        lblUser2.Text = "Buyer"
                        tblAccountManager.Visible = True
                        ViewState("viewer") = ""
                        'pnlContactProfileBeta.Visible = False
                    End If

                    If ((Request("classid") = ApplicationSettings.RoleSupplierID) Or (Session(Request("companyid") & "_ContactClassID") = ApplicationSettings.RoleSupplierID)) Then
                        'OD-804 - .Net - Skills, Accreditation and Location coverage changes to make it Live
                        pnlContactProfileBeta.Visible = True
                        chkRecordVerifed.Visible = True
                    End If

                    setEnvForCompany()
                    'Prepares link for back to listing
                    lnkBackToListing.HRef = getBackToListingLink()

                    Select Case ApplicationSettings.Country
                        Case ApplicationSettings.CountryDE
                            'Disable VAT regular expression validator for DE admin
                            'rgVatNo.Enabled = False
                            'rgAccNo.Enabled = False
                            'regSortCode.Enabled = False
                    End Select
            End Select


            'Dim contactid As Integer = Session("CompanyId")
            Dim cacheKey As String = "Contact" & "-" & CStr(_companyID) & "-" & Session.SessionID
            divValidationMain.Visible = False

            'Populate No of Employees drop down 
            CommonFunctions.PopulateNoOfEmployees(Page, ddlNoOfEmployees)


            'Populate Credit terms
            If Not ddlSuppPaymentsTerms Is Nothing Then
                CommonFunctions.PopulateCreditTerms(Page, ddlSuppPaymentsTerms)
            End If
            If Not ddlBuyerPaymentsTerms Is Nothing Then
                CommonFunctions.PopulateCreditTerms(Page, ddlBuyerPaymentsTerms)
            End If
            If Not ddlPayTerms Is Nothing Then
                CommonFunctions.PopulateCreditTerms(Page, ddlPayTerms)
            End If

            'If pnlSupplierInfo.Visible = True Then

            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                'Populate the admin users for the account manager dropdown
                CommonFunctions.PopulateAdminUsers(Page, ddlOWAccountManager)
                'Populate the Business Area Options - Added by PratikT on 25th Feb, 2009
                CommonFunctions.PopulateBusinessArea(Page, ddlBusinessArea)
            End If


            'Condition is put so that code will get excuted for supplier login only
            If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then

                ''Initialize AOE UC
                UCAOE1.BizDivId = Request("bizDivId")
                UCAdminAccreds.BizDivId = Request("bizDivId")
                UCAdminAccreds.CommonID = Request("CompanyID")
                UCAdminAccreds.Type = "CompanyAccreditations"

                'UCCompAccreds1.BizDivId = Request("bizDivId")
                'UCCompAccreds1.CompanyID = Request("CompanyID")


                'Populate Travel Distance DropDownlist
                'CommonFunctions.PopulateDistance(Page, ddlTravelDistance)

                'Populate service response time dropdown
                'CommonFunctions.PopulateServiceResponseTime(Page, ddlResponseTime)

                'Check if partner's level drop down is visible and so populate Partner's level
                CommonFunctions.PopulatePartnerLevel(Page, ddlpartner, "MS")

                Dim ds As DataSet
                ds = GetContact(Page, ApplicationSettings.BizDivId, True, True, _companyID)
                If BizDivID = ApplicationSettings.SFUKBizDivId Then
                    tblCertQual.Visible = False
                Else
                    tblCertQual.Visible = True
                End If

                Dim dsView1 As DataView
                dsView1 = ds.Tables("ContactSettings").Copy.DefaultView
                dsView1.RowFilter = "AttributeLabel = 'Region'"
                'UCRegions.populateListBox(dsView1, CommonFunctions.GetRegions(Page), "RegionName", "AttributeValue", "RegionID")
            End If

            'End If

            PopulateCompanyProfile()

            'btnSaveTop.Attributes.Add("onClick", "javascript:callSetFocusValidation('UCCompanyProfile1')")
            'btnSaveBottom.Attributes.Add("onClick", "javascript:callSetFocusValidation('UCCompanyProfile1')")
            'populate Comment Reason ddl
            CommonFunctions.PopulateCommentReason(Page, ddlCommentReason)
            ViewState("PopupVisible") = 0
        End If
        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                If chkSendMail.Visible Then
                    If ddlType.SelectedItem.Text = "Delete" Then
                        chkSendMail.Visible = False
                    Else
                        chkSendMail.Visible = True
                    End If
                End If
            End If
        End If
    End Sub


    ''' <summary>
    ''' To populate the company profile UC.
    ''' </summary>
    ''' <param name="killcache"></param>
    ''' <remarks></remarks>
    Protected Sub PopulateCompanyProfile(Optional ByVal killcache As Boolean = False)
        Dim itemcount As Integer = 0
        Dim flag As Boolean = False
        Dim strAppend As String
        Select Case _bizDivID
            Case ApplicationSettings.OWUKBizDivId
                strAppend = "OW"
            Case ApplicationSettings.SFUKBizDivId
                strAppend = "MS"
            Case ApplicationSettings.OWDEBizDivId
                strAppend = "DE"

        End Select

        Dim contactid As Integer = _companyID
        'OD-804 - .Net - Skills, Accreditation and Location coverage changes to make it Live
        Page.Session("ContactIDForSkills") = _companyID


        Dim dsContacts As DataSet
        dsContacts = GetContact(Page, ApplicationSettings.BizDivId, True, True, contactid) ' 4976    contactid

        'Added By Snehal 18 Nov 09 - version control
        If dsContacts.Tables("Contacts").Rows.Count > 0 Then
            'Store the version number in session for Contacts
            CommonFunctions.StoreVerNum(dsContacts.Tables("Contacts"))
        End If

        SetCacheForAttachment(_companyID, "CompanyProfile", dsContacts.Tables("ContactAttachments").Copy.DefaultView)
        SetCacheForAttachment(_companyID, "ProfessionalIndemnity", dsContacts.Tables("ContactAttachments").Copy.DefaultView)
        SetCacheForAttachment(_companyID, "PublicLiability", dsContacts.Tables("ContactAttachments").Copy.DefaultView)
        SetCacheForAttachment(_companyID, "EmployersLiability", dsContacts.Tables("ContactAttachments").Copy.DefaultView)

        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
            If Not IsDBNull(dsContacts.Tables(0).Rows(0).Item("AccountManagerID")) Then
                ddlOWAccountManager.SelectedValue = dsContacts.Tables(0).Rows(0).Item("AccountManagerID")
            End If
        End If
        'Populate supplier  specialise area
        If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then

            'OA-622 - discount changes
            If Not IsPostBack Then
                CommonFunctions.PopulateSupplierDiscountPaymentTerm(Page, ddlSuppDiscountPaymentsTermsOW, "Orderwork")
                CommonFunctions.PopulateSupplierDiscountPaymentTerm(Page, ddlSuppDiscountPaymentsTermsEM, "Empowered")
            End If


            'populate the heard about us dropdown from xml
            Dim dsXML As New DataSet()
            dsXML.ReadXml(MapPath("~/XML/WOFormStandards.xml"))
            Dim dv_HrdAbtUs As DataView = dsXML.Tables("HeardAboutUs").DefaultView
            ddlHrdAbt.DataSource = dv_HrdAbtUs
            ddlHrdAbt.DataBind()
            If (dsContacts.Tables("Contacts").Rows.Count > 0) Then
                If Not IsNothing(dsContacts.Tables("Contacts").Rows(0).Item("HeardAboutUs")) Then
                    If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("HeardAboutUs")) Then
                        For itemcount = 0 To ddlHrdAbt.Items.Count - 1
                            If ddlHrdAbt.Items(itemcount).Text = dsContacts.Tables("Contacts").Rows(0).Item("HeardAboutUs").ToString Then
                                ddlHrdAbt.SelectedIndex = itemcount
                                flag = True
                                Exit For
                            End If

                        Next
                        If Not (flag) Then
                            ddlHrdAbt.SelectedValue = "Other"
                            pnlHrdAbtUs.Style("display") = "block"
                            lblHrdfrom.Text = "Heard from"
                            If (dsContacts.Tables("Contacts").Rows(0).Item("HeardAboutUs").ToString <> "") Then
                                txtHrdAbtUs.Text = dsContacts.Tables("Contacts").Rows(0).Item("HeardAboutUs").ToString
                            Else
                                txtHrdAbtUs.Text = "none specified"
                            End If

                        Else
                            pnlHrdAbtUs.Style("display") = "none"
                            lblHrdfrom.Text = ""
                        End If
                    Else
                        ddlHrdAbt.SelectedValue = "Other"
                        pnlHrdAbtUs.Style("display") = "block"
                        lblHrdfrom.Text = "Heard from"
                        txtHrdAbtUs.Text = "none specified"
                    End If
                Else
                    ddlHrdAbt.SelectedValue = "Other"
                    pnlHrdAbtUs.Style("display") = "block"
                    lblHrdfrom.Text = "Heard from"
                    txtHrdAbtUs.Text = "none specified"
                End If
            Else
                ddlHrdAbt.SelectedValue = "Other"
                pnlHrdAbtUs.Style("display") = "block"
                lblHrdfrom.Text = "Heard from"
                txtHrdAbtUs.Text = "none specified"
            End If


            'If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("BusinessIT")) Then
            '    chkIT.Checked = dsContacts.Tables("Contacts").Rows(0).Item("BusinessIT")
            'Else
            '    chkIT.Checked = False
            'End If

            'If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("FreesatandDigitalAerials")) Then
            '    chkFreesat.Checked = dsContacts.Tables("Contacts").Rows(0).Item("FreesatandDigitalAerials")
            'Else
            '    chkFreesat.Checked = False
            'End If

            'If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("ConsumerElectronicandTV")) Then
            '    chkElex.Checked = dsContacts.Tables("Contacts").Rows(0).Item("ConsumerElectronicandTV")
            'Else
            '    chkElex.Checked = False
            'End If

            'If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("HomePCInstallations")) Then
            '    chkpc.Checked = dsContacts.Tables("Contacts").Rows(0).Item("HomePCInstallations")
            'Else
            '    chkpc.Checked = False
            'End If
        End If



        Dim dvContact As DataView
        dvContact = dsContacts.Tables("Contacts").Copy.DefaultView
        If Not (ddlPayTerms Is Nothing) Then
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("SupplierCreditTerms")) Then
                If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("SupplierCreditTerms")) Then
                    If (dsContacts.Tables("Contacts").Rows(0).Item("SupplierCreditTerms") <> 0) Then
                        Dim litem As ListItem
                        litem = ddlPayTerms.Items.FindByValue(dsContacts.Tables("Contacts").Rows(0).Item("SupplierCreditTerms"))
                        If Not litem Is Nothing Then
                            ddlPayTerms.SelectedValue = dsContacts.Tables("Contacts").Rows(0).Item("SupplierCreditTerms")
                        End If
                    Else
                        Dim lItem As ListItem
                        lItem = ddlPayTerms.Items.FindByValue(ApplicationSettings.PaymentTerms)
                        If Not IsNothing(lItem) Then
                            ddlPayTerms.SelectedValue = ApplicationSettings.PaymentTerms      ' by default, '30 days' selected in dropdown. value is 80
                        End If
                    End If
                Else
                    Dim lItem As ListItem
                    lItem = ddlPayTerms.Items.FindByValue(ApplicationSettings.PaymentTerms)
                    If Not IsNothing(lItem) Then
                        ddlPayTerms.SelectedValue = ApplicationSettings.PaymentTerms      ' by default, '30 days' selected in dropdown. value is 80
                    End If
                End If
            End If
        End If
        'Populate supplier  Credit Terms
        If Not (ddlSuppPaymentsTerms Is Nothing) Then
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("SupplierCreditTerms")) Then
                If dsContacts.Tables("Contacts").Rows(0).Item("SupplierCreditTerms") <> 0 Then
                    Dim litem As ListItem
                    litem = ddlSuppPaymentsTerms.Items.FindByValue(dsContacts.Tables("Contacts").Rows(0).Item("SupplierCreditTerms"))
                    If Not litem Is Nothing Then
                        ddlSuppPaymentsTerms.SelectedValue = dsContacts.Tables("Contacts").Rows(0).Item("SupplierCreditTerms")
                    End If
                Else
                    Dim lItem As ListItem
                    lItem = ddlSuppPaymentsTerms.Items.FindByValue(ApplicationSettings.PaymentTerms)
                    If Not IsNothing(lItem) Then
                        ddlSuppPaymentsTerms.SelectedValue = ApplicationSettings.PaymentTerms  ' by default, '30 days' selected in dropdown value is 80
                    End If
                End If
            Else
                Dim lItem As ListItem
                lItem = ddlSuppPaymentsTerms.Items.FindByValue(ApplicationSettings.PaymentTerms)
                If Not IsNothing(lItem) Then
                    ddlSuppPaymentsTerms.SelectedValue = ApplicationSettings.PaymentTerms  ' by default, '30 days' selected in dropdown value is 80
                End If
            End If
        End If
        'Populate buyer Credit Terms
        If Not (ddlBuyerPaymentsTerms Is Nothing) Then
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("BuyerCreditTerms")) Then
                If (dsContacts.Tables("Contacts").Rows(0).Item("BuyerCreditTerms") <> 0) Then
                    Dim litem As ListItem
                    litem = ddlBuyerPaymentsTerms.Items.FindByValue(dsContacts.Tables("Contacts").Rows(0).Item("BuyerCreditTerms"))
                    If Not litem Is Nothing Then
                        ddlBuyerPaymentsTerms.SelectedValue = dsContacts.Tables("Contacts").Rows(0).Item("BuyerCreditTerms")
                    End If
                Else
                    Dim lItem As ListItem
                    lItem = ddlBuyerPaymentsTerms.Items.FindByValue(ApplicationSettings.CreditTerms)
                    If Not IsNothing(lItem) Then
                        ddlBuyerPaymentsTerms.SelectedValue = ApplicationSettings.CreditTerms      ' by default, '7 days' selected in dropdown. value is 77
                    End If
                End If
            Else
                Dim lItem As ListItem
                lItem = ddlBuyerPaymentsTerms.Items.FindByValue(ApplicationSettings.CreditTerms)
                If Not IsNothing(lItem) Then
                    ddlBuyerPaymentsTerms.SelectedValue = ApplicationSettings.CreditTerms      ' by default, '7 days' selected in dropdown. value is 77
                End If
                '    
            End If
        End If
        If Not (ddlPayTerms Is Nothing) Then

        End If

        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("CompanyName")) Then
            If dsContacts.Tables("Contacts").Rows(0).Item("CompanyName") <> "" Then
                txtCompanyName.Text = dsContacts.Tables("Contacts").Rows(0).Item("CompanyName")
                Session("BuyerCompanyName") = dsContacts.Tables("Contacts").Rows(0).Item("CompanyName")
            End If
        End If
        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("IsTestAccount")) Then
            chkIsTestAccount.Checked = dsContacts.Tables("Contacts").Rows(0).Item("IsTestAccount")
        End If

        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
            Dim strContactName As String
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0)("FullName")) Then
                strContactName = dsContacts.Tables("Contacts").Rows(0)("FullName")
            End If
            ViewState("ContactName") = strContactName

            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0)("UserName")) Then
                ViewState("Email") = dsContacts.Tables("Contacts").Rows(0)("UserName")
            End If
        End If
        'Condition is put so that code will get excuted for supplier login only
        If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
            ' assign AOE combIds
            Dim aoeAttrLabel As String = ""
            Dim combIDs As String = ""
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryUK
                    Select Case BizDivID
                        Case ApplicationSettings.OWUKBizDivId
                            aoeAttrLabel = "OWAOE"
                        Case ApplicationSettings.SFUKBizDivId
                            aoeAttrLabel = "MSAOE"
                    End Select
                Case ApplicationSettings.CountryDE
                    Select Case BizDivID
                        Case ApplicationSettings.OWDEBizDivId
                            aoeAttrLabel = "DEAOE"
                    End Select
            End Select
            dvContact = dsContacts.Tables("ContactSettings").Copy.DefaultView
            dvContact.RowFilter = "AttributeLabel = '" & aoeAttrLabel & "'"
            For Each drv As DataRowView In dvContact
                If combIDs <> "" Then
                    combIDs &= "," & drv("AttributeValue")
                Else
                    combIDs = drv("AttributeValue")
                End If
            Next
            UCAOE1.CombIds = combIDs
            If ddlpartner.Visible = True Then
                If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0)("MSPartnerLevel")) Then
                    ddlpartner.SelectedValue = CStr(dsContacts.Tables("Contacts").Rows(0)("MSPartnerLevel"))
                Else
                    ddlpartner.SelectedValue = ""
                End If
            End If


        End If
        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0)("CompanyRegNo")) Then
            txtCompanyRegNo.Text = dsContacts.Tables("Contacts").Rows(0)("CompanyRegNo")
        End If
        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0)("URL")) Then
            txtCompanyURL.Text = dsContacts.Tables("Contacts").Rows(0)("URL")
        Else
            txtCompanyURL.Text = "http://"
        End If
        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0)("CompanyStrength")) Then
            ddlNoOfEmployees.SelectedValue = CStr(dsContacts.Tables("Contacts").Rows(0)("CompanyStrength"))
        Else
            ddlNoOfEmployees.SelectedValue = ""
        End If
        'Filter and Fill No Of locations

        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0)("NoOfLocations")) Then
            txtNoOfLocations.Text = dsContacts.Tables("Contacts").Rows(0)("NoOfLocations")
        Else
            txtNoOfLocations.Text = ""
        End If
        'If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0)("ISORegNo")) Then
        '    txtISORegNo.Text = dsContacts.Tables("Contacts").Rows(0)("ISORegNo")
        'End If

        If Not IsDBNull(dsContacts.Tables(0).Rows(0).Item("BusinessStartYear")) Then
            txtBusinessStarted.Text = dsContacts.Tables(0).Rows(0).Item("BusinessStartYear")
        End If


        If _bizDivID <> ApplicationSettings.OWDEBizDivId Then

            'Condition is put so that code will get excuted for supplier login only
            If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then

                'change added by PB: this field is hidden for DE site. therefore should not populate this field.
                'If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                '    'PeopleRegNo
                '    If Not IsDBNull(dsContacts.Tables(0).Rows(0).Item("PeopleRegNo")) Then
                '        txtPeopleRegNo.Text = dsContacts.Tables(0).Rows(0).Item("PeopleRegNo")
                '    End If
                'End If

                'CRBChecked
                CRBCheckedYes.Enabled = False
                CRBCheckedNo.Enabled = False
                CRBCheckedEnhanced.Enabled = False
                CSCSYes.Enabled = False
                CSCSNo.Enabled = False
                UKSecurityClearanceYes.Enabled = False
                UKSecurityClearanceNo.Enabled = False
                If Not IsDBNull(dsContacts.Tables(0).Rows(0).Item("ConCRBChecked")) Then
                    If dsContacts.Tables(0).Rows(0).Item("ConCRBChecked") Then
                        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0)("ConCriminalRecordChecks")) Then
                            If dsContacts.Tables("Contacts").Rows(0)("ConCriminalRecordChecks") <> "" Then
                                If dsContacts.Tables("Contacts").Rows(0)("ConCriminalRecordChecks") > 100 Then
                                    CRBCheckedEnhanced.Checked = True
                                End If
                            Else
                                CRBCheckedYes.Checked = True
                            End If
                        Else
                            CRBCheckedYes.Checked = True
                        End If
                    Else
                        CRBCheckedNo.Checked = True
                    End If
                Else
                    CRBCheckedYes.Checked = False
                    CRBCheckedNo.Checked = False
                End If


                'Change added by PB: This field is hidden on the De site. There should not be populated.

                If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                    'UKSecClearance
                    If Not IsDBNull(dsContacts.Tables(0).Rows(0).Item("ConUKSecurityClearance")) Then
                        If dsContacts.Tables(0).Rows(0).Item("ConUKSecurityClearance") Then
                            UKSecurityClearanceYes.Checked = True
                        Else
                            UKSecurityClearanceNo.Checked = True
                        End If
                    Else
                        UKSecurityClearanceYes.Checked = False
                        UKSecurityClearanceNo.Checked = False
                    End If
                    If Not IsDBNull(dsContacts.Tables(0).Rows(0).Item("ConCSCS")) Then
                        If dsContacts.Tables(0).Rows(0).Item("ConCSCS") Then
                            CSCSYes.Checked = True
                        Else
                            CSCSNo.Checked = True
                        End If
                    Else
                        CSCSYes.Checked = False
                        CSCSNo.Checked = False
                    End If

                    'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
                    If Not IsDBNull(dsContacts.Tables(0).Rows(0).Item("ExcelPartner")) Then
                        If dsContacts.Tables(0).Rows(0).Item("ExcelPartner") Then
                            ExcelPartnerYes.Checked = True
                        Else
                            ExcelPartnerNo.Checked = True
                        End If
                    Else
                        ExcelPartnerYes.Checked = False
                        ExcelPartnerNo.Checked = False
                    End If

                    If Not IsDBNull(dsContacts.Tables(0).Rows(0).Item("EPCP")) Then
                        If dsContacts.Tables(0).Rows(0).Item("EPCP") Then
                            EPCPYes.Checked = True
                        Else
                            EPCPNo.Checked = True
                        End If
                    Else
                        EPCPYes.Checked = False
                        EPCPNo.Checked = False
                    End If
                End If
            End If
        End If


        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
            If Not IsDBNull(dsContacts.Tables(0).Rows(0).Item("IndustryID")) Then
                If Not IsDBNull(dsContacts.Tables(0).Rows(0).Item("IndustryID")) Then
                    ddlIndustry.SelectedValue = CStr(dsContacts.Tables(0).Rows(0).Item("IndustryID"))
                Else
                    ddlIndustry.SelectedValue = ""
                End If
            Else
                ddlIndustry.SelectedValue = ""
            End If
            If Not IsDBNull(dsContacts.Tables(0).Rows(0).Item("Status")) Then
                'For approved suppliers only add Approve in the Type drop down list
                If (_classID = ApplicationSettings.RoleSupplierID) Then
                    'Supplier Company
                    If (dsContacts.Tables(0).Rows(0).Item("Status") = ApplicationSettings.StatusApprovedCompany) Then
                        'Status = approved
                        Dim ddItem As New ListItem
                        ddItem.Text = "Approve"
                        ddItem.Value = ApplicationSettings.StatusApprovedCompany
                        If Not (ddlType.Items.Contains(ddItem)) Then
                            ddlType.Items.Add(ddItem)
                        End If
                    Else
                        'For Approved supplier if status is changed then remove Approve from the listing
                        Dim ddItem As New ListItem
                        ddItem.Text = "Approve"
                        ddItem.Value = ApplicationSettings.StatusApprovedCompany

                        If ddlType.Items.Contains(ddItem) Then
                            ddlType.Items.Remove(ddItem)
                        End If
                    End If
                End If
                ddlType.SelectedValue = dsContacts.Tables(0).Rows(0).Item("Status")
                ViewState("UserStatus") = dsContacts.Tables(0).Rows(0).Item("Status")

                If dsContacts.Tables("Contacts").Rows(0).Item("BusinessArea").ToString <> "" Then
                    ddlBusinessArea.SelectedValue = dsContacts.Tables("Contacts").Rows(0).Item("BusinessArea")
                Else
                    ddlBusinessArea.SelectedValue = ""
                End If
                If ((Request("classid") = ApplicationSettings.RoleSupplierID) Or (Session(Request("companyid") & "_ContactClassID") = ApplicationSettings.RoleSupplierID)) Then
                    If ddlType.SelectedValue = ApplicationSettings.StatusNewCompany Or ddlType.SelectedValue = ApplicationSettings.StatusDeletedCompany Then
                        SortCodeIndicator.Style.Add("display", "none")
                        rqSortCode.Enabled = False

                        AccountNumberIndicator.Style.Add("display", "none")
                        rqAccountNumber.Enabled = False

                        txtAccountNameIndicator.Style.Add("display", "none")
                        rqtxtAccountName.Enabled = False
                    Else
                        SortCodeIndicator.Style.Add("display", "inline")
                        rqSortCode.Enabled = True

                        AccountNumberIndicator.Style.Add("display", "inline")
                        rqAccountNumber.Enabled = True

                        txtAccountNameIndicator.Style.Add("display", "inline")
                        rqtxtAccountName.Enabled = True
                    End If
                End If
            Else
                ddlType.SelectedValue = ""
                ddlBusinessArea.SelectedValue = ""
            End If
            If ddlBusinessArea.SelectedValue <> "" Then
                BusinessArea = ddlBusinessArea.SelectedValue
            Else
                BusinessArea = 0
            End If
            Session.Add("BusinessArea", BusinessArea)



        End If


        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("Profile")) Then
            txtProfile.Text = dsContacts.Tables("Contacts").Rows(0).Item("Profile")
        End If


        'Dim dsView As DataView
        'dsView = dsContacts.Tables("ContactAttachments").Copy.DefaultView

        '   Condition is changed. excute following code in if condition for  supplier login.
        If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
            'Sumit
            UCAOE1.PopulateAOE()
            'End If
            'If pnlSupplierInfo.Visible = True Then
            'dvContact = dsContacts.Tables("ContactAttributes").Copy.DefaultView
            'Select Case _bizDivID
            '    Case ApplicationSettings.OWUKBizDivId
            '        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("OWHourlyRate")) Then
            '            txtHourlyRate.Text = dsContacts.Tables("Contacts").Rows(0).Item("OWHourlyRate")
            '        End If
            '        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("OWSourceParts")) Then
            '            If dsContacts.Tables("Contacts").Rows(0).Item("OWSourceParts") Then
            '                SourcePartYes.Checked = True
            '            Else
            '                SourcePartNo.Checked = True
            '            End If
            '        Else
            '            SourcePartNo.Checked = False
            '            SourcePartYes.Checked = False
            '        End If
            '        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("OWServiceResponseTime")) Then
            '            If dsContacts.Tables("Contacts").Rows(0).Item("OWServiceResponseTime") <> 0 Then
            '                ddlResponseTime.SelectedValue = dsContacts.Tables("Contacts").Rows(0).Item("OWServiceResponseTime")
            '            End If

            '        End If
            '    Case ApplicationSettings.SFUKBizDivId
            '        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("MSHourlyRate")) Then
            '            txtHourlyRate.Text = dsContacts.Tables("Contacts").Rows(0).Item("MSHourlyRate")
            '        End If
            '        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("MSSourceParts")) Then
            '            If dsContacts.Tables("Contacts").Rows(0).Item("MSSourceParts") Then
            '                SourcePartYes.Checked = True
            '            Else
            '                SourcePartNo.Checked = True
            '            End If
            '        Else
            '            SourcePartNo.Checked = False
            '            SourcePartYes.Checked = False
            '        End If
            '        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("OWServiceResponseTime")) Then
            '            If (dsContacts.Tables("Contacts").Rows(0).Item("OWServiceResponseTime") <> 0) Then
            '                ddlResponseTime.SelectedValue = dsContacts.Tables("Contacts").Rows(0).Item("MSServiceResponseTime")
            '            End If
            '        End If
            'End Select

            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("NoOfEngineers")) Then
                If (dsContacts.Tables("Contacts").Rows(0).Item("NoOfEngineers") <> 0) Then
                    txtNoOfEngineers.Text = dsContacts.Tables("Contacts").Rows(0).Item("NoOfEngineers")
                End If
            End If
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("StdDayRate")) Then
                If (dsContacts.Tables("Contacts").Rows(0).Item("StdDayRate") <> 0) Then
                    txtStdDayRate.Text = dsContacts.Tables("Contacts").Rows(0).Item("StdDayRate")
                End If
            End If

            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("FulfilInhouse")) Then
                If dsContacts.Tables("Contacts").Rows(0).Item("FulfilInhouse") Then
                    FulfilInhouseYes.Checked = True
                Else
                    FulfilInhouseNo.Checked = True
                End If
            Else
                FulfilInhouseNo.Checked = False
                FulfilInhouseYes.Checked = False
            End If

            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("EmployersLiability")) Then
                If dsContacts.Tables("Contacts").Rows(0).Item("EmployersLiability") Then
                    chkInsurance4.Checked = True
                Else
                    chkInsurance4.Checked = False
                End If
            Else
                chkInsurance4.Checked = False
            End If

            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("IsRecordVerified")) Then
                If dsContacts.Tables("Contacts").Rows(0).Item("IsRecordVerified") Then
                    chkRecordVerifed.Checked = True
                Else
                    chkRecordVerifed.Checked = False
                End If
            Else
                chkRecordVerifed.Checked = False
            End If

            UCFileUpload1.CompanyID = _companyID
            UCFileUpload1.AttachmentForID = _companyID
            UCFileUpload1.BizDivID = _bizDivID
            UCFileUpload1.AttachmentForSource = "EmployersLiability"
            UCFileUpload1.ClearUCFileUploadCache()
            UCFileUpload1.PopulateAttachedFiles()
            UCFileUpload1.PopulateExistingFiles()

            UCFileUpload4.CompanyID = _companyID
            UCFileUpload4.AttachmentForID = _companyID
            UCFileUpload4.BizDivID = _bizDivID
            UCFileUpload4.AttachmentForSource = "Vat"
            UCFileUpload4.ClearUCFileUploadCache()
            UCFileUpload4.PopulateAttachedFiles()
            UCFileUpload4.PopulateExistingFiles()
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("EmpLiabilityExpDate")) Then
                txtEmployeeLiabilityDate.Text = dsContacts.Tables("Contacts").Rows(0).Item("EmpLiabilityExpDate")
            End If
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("EmpLiabilityCoverAmt")) Then
                If (dsContacts.Tables("Contacts").Rows(0).Item("EmpLiabilityCoverAmt")) <> 0 Then
                    txtCoverAmountEmpLiabiity.Text = dsContacts.Tables("Contacts").Rows(0).Item("EmpLiabilityCoverAmt")
                End If
            End If
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("PublicLiability")) Then
                If dsContacts.Tables("Contacts").Rows(0).Item("PublicLiability") Then
                    chkInsurance5.Checked = True
                Else
                    chkInsurance5.Checked = False
                End If
            Else
                chkInsurance5.Checked = False
            End If
            UCFileUpload2.CompanyID = _companyID
            UCFileUpload2.BizDivID = _bizDivID
            UCFileUpload2.AttachmentForID = _companyID
            UCFileUpload2.AttachmentForSource = "PublicLiability"
            UCFileUpload2.ClearUCFileUploadCache()
            UCFileUpload2.PopulateAttachedFiles()
            UCFileUpload2.PopulateExistingFiles()

            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("PublicLiabilityExpDate")) Then
                txtPublicLiabilityDate.Text = dsContacts.Tables("Contacts").Rows(0).Item("PublicLiabilityExpDate")
            End If
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("PublicLiabilityCoverAmt")) Then
                If (dsContacts.Tables("Contacts").Rows(0).Item("PublicLiabilityCoverAmt")) <> 0 Then
                    txtCoverAmountpubLiability.Text = dsContacts.Tables("Contacts").Rows(0).Item("PublicLiabilityCoverAmt")
                End If
            End If
            'Filter and Fill Professional Indemnity

            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("ProfessionalIndemnity")) Then
                If dsContacts.Tables("Contacts").Rows(0).Item("ProfessionalIndemnity") Then
                    chkInsurance6.Checked = True
                Else
                    chkInsurance6.Checked = False
                End If
            Else
                chkInsurance6.Checked = False
            End If

            UCFileUpload3.CompanyID = _companyID
            UCFileUpload3.BizDivID = _bizDivID
            UCFileUpload3.AttachmentForID = _companyID
            UCFileUpload3.AttachmentForSource = "ProfessionalIndemnity"
            UCFileUpload3.ClearUCFileUploadCache()
            UCFileUpload3.PopulateAttachedFiles()
            UCFileUpload3.PopulateExistingFiles()

            'Filter and Fill Professional Indemnity expiry date
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("ProfIndemnityExpDate")) Then
                txtProfessionalIndemnityDate.Text = Trim(dsContacts.Tables("Contacts").Rows(0).Item("ProfIndemnityExpDate"))
            End If
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("ProfIdemnityCoverAmt")) Then
                If (dsContacts.Tables("Contacts").Rows(0).Item("ProfIdemnityCoverAmt")) <> 0 Then
                    txtCoverAmountProfLiabity.Text = Trim(dsContacts.Tables("Contacts").Rows(0).Item("ProfIdemnityCoverAmt"))
                End If
            End If
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("VATRegistered")) Then
                If dsContacts.Tables("Contacts").Rows(0).Item("VATRegistered") Then
                    RegisterYes.Checked = True
                Else
                    RegisterNo.Checked = True
                End If
            Else
                RegisterYes.Checked = False
                RegisterNo.Checked = False
                '  RegisterNo.Checked = False
            End If

            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("VATRegistrationNumber")) Then
                Dim strVAT As String = Trim(dsContacts.Tables("Contacts").Rows(0).Item("VATRegistrationNumber"))
                If strVAT <> "" Then
                    Select Case ApplicationSettings.Country
                        Case ApplicationSettings.CountryUK
                            If strVAT.IndexOf("gb") = -1 And strVAT.IndexOf("GB") = -1 And strVAT.IndexOf("Gb") = -1 And strVAT.IndexOf("gB") = -1 Then
                                strVAT = "GB " & strVAT
                            End If
                        Case ApplicationSettings.CountryDE
                            If strVAT.IndexOf("de") = -1 And strVAT.IndexOf("DE") = -1 And strVAT.IndexOf("De") = -1 And strVAT.IndexOf("dE") = -1 Then
                                strVAT = "DE " & strVAT
                            End If
                    End Select
                    strVAT = StrConv(strVAT, VbStrConv.Uppercase)
                End If
                txtVATRegistrationNumber.Text = strVAT
            End If


            '--- Processing Contact Attributes Memo Table
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("OtherInfo")) Then
                txtOtherInformation.Text = dsContacts.Tables("Contacts").Rows(0).Item("OtherInfo")
            End If

            'Filter and fill Company Accreditions
            'If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("CompanyAccreditations")) Then
            '    txtAccreditations.Text = dsContacts.Tables("Contacts").Rows(0).Item("CompanyAccreditations")
            'End If

            'If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("TravelDistance")) Then
            '    If dsContacts.Tables("Contacts").Rows(0).Item("TravelDistance") <> 0 Then
            '        ddlTravelDistance.SelectedValue = dsContacts.Tables("Contacts").Rows(0).Item("TravelDistance")
            '    Else
            '        ddlTravelDistance.SelectedValue = ""
            '    End If
            'Else

            'End If

            'Populating "Bank Details"

            'only for orderworkde: display kontoname for suppliers
            If ApplicationSettings.Country = ApplicationSettings.CountryDE Then
                If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("KontoName")) Then
                    txtKontoName.Text = Encryption.Decrypt(dsContacts.Tables("Contacts").Rows(0).Item("KontoName"))
                    If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.site Then
                        txtKontoName.Enabled = False
                    End If
                End If
            End If


            'Bank Sort Code
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("NewBankSortCode")) Or dsContacts.Tables("Contacts").Rows(0).Item("NewBankSortCode") <> "" Then
                txtSortCode.Text = Encryption.Decrypt(dsContacts.Tables("Contacts").Rows(0).Item("NewBankSortCode"))
                ViewState("BankSortCode") = Encryption.Decrypt(dsContacts.Tables("Contacts").Rows(0).Item("NewBankSortCode"))

                If txtSortCode.Text <> "" Then
                    txtSortCode.Text = txtSortCode.Text.Substring(0, txtSortCode.Text.Length - 5) + "XX-XX"
                End If

                txtSortCode.Enabled = False

                If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.site Then
                    tblBankDetailsMsg.Visible = True
                End If

                If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                    txtSortCode.Text = ViewState("BankSortCode")
                    txtSortCode.Enabled = True
                    rqSortCode.Enabled = True
                    regSortCode.Enabled = True
                Else
                    txtSortCode.Enabled = False
                    rqSortCode.Enabled = False
                    regSortCode.Enabled = False
                End If


            End If

            'Bank Account No
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("NewBankAccountNo")) Then
                txtAccountNo.Text = Encryption.Decrypt(dsContacts.Tables("Contacts").Rows(0).Item("NewBankAccountNo"))
                ViewState("BankAccountNo") = Encryption.Decrypt(dsContacts.Tables("Contacts").Rows(0).Item("NewBankAccountNo"))

                If txtAccountNo.Text <> "" Then
                    txtAccountNo.Text = txtAccountNo.Text.Substring(0, txtAccountNo.Text.Length - 4) + "XXXX"
                End If

                txtAccountNo.Enabled = False

                If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                    txtAccountNo.Text = ViewState("BankAccountNo")
                    txtAccountNo.Enabled = True
                    rqAccountNumber.Enabled = True
                    rgAccNo.Enabled = True
                Else
                    txtAccountNo.Enabled = False
                    rqAccountNumber.Enabled = False
                    rgAccNo.Enabled = False
                End If

            End If

            'Bank Account Name
            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("NewBankAccountName")) Then
                txtAccountName.Text = Encryption.Decrypt(dsContacts.Tables("Contacts").Rows(0).Item("NewBankAccountName"))
                ViewState("BankAccountName") = Encryption.Decrypt(dsContacts.Tables("Contacts").Rows(0).Item("NewBankAccountName"))

                If txtAccountName.Text <> "" Then
                    txtAccountName.Text = txtAccountName.Text.Substring(0, txtAccountName.Text.Length - (txtAccountName.Text.Length - 2)) + "XXX"
                End If

                txtAccountName.Enabled = False

                If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                    txtAccountName.Text = ViewState("BankAccountName")
                    txtAccountName.Enabled = True
                    rqtxtAccountName.Enabled = True
                Else
                    txtAccountName.Enabled = False
                    rqtxtAccountName.Enabled = False
                End If

            End If

            'OA-622 discount changes
            'Populate discount payment term for suppliers
            If Not (ddlSuppDiscountPaymentsTermsOW Is Nothing) Then
                If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("OwPaymentTermId")) Then
                    If dsContacts.Tables("Contacts").Rows(0).Item("OwPaymentTermId") <> 0 Then
                        Dim litem As ListItem
                        litem = ddlSuppDiscountPaymentsTermsOW.Items.FindByValue(dsContacts.Tables("Contacts").Rows(0).Item("OwPaymentTermId"))
                        If Not litem Is Nothing Then
                            ddlSuppDiscountPaymentsTermsOW.SelectedValue = dsContacts.Tables("Contacts").Rows(0).Item("OwPaymentTermId")
                        End If
                    End If
                End If
            End If
            If Not (ddlSuppDiscountPaymentsTermsEM Is Nothing) Then
                If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("EmpPaymentTermId")) Then
                    If dsContacts.Tables("Contacts").Rows(0).Item("EmpPaymentTermId") <> 0 Then
                        Dim litem As ListItem
                        litem = ddlSuppDiscountPaymentsTermsEM.Items.FindByValue(dsContacts.Tables("Contacts").Rows(0).Item("EmpPaymentTermId"))
                        If Not litem Is Nothing Then
                            ddlSuppDiscountPaymentsTermsEM.SelectedValue = dsContacts.Tables("Contacts").Rows(0).Item("EmpPaymentTermId")
                        End If
                    End If
                End If
            End If


            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("OwPaymentTermDiscount")) Then
                'If dsContacts.Tables("Contacts").Rows(0).Item("PaymentDiscount") <> "" Then
                txtDiscountOW.Text = dsContacts.Tables("Contacts").Rows(0).Item("OwPaymentTermDiscount").ToString()
                ' End If
            End If

            If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("EmpPaymentTermDiscount")) Then
                'If dsContacts.Tables("Contacts").Rows(0).Item("PaymentDiscount") <> "" Then
                txtDiscountEM.Text = dsContacts.Tables("Contacts").Rows(0).Item("EmpPaymentTermDiscount").ToString()
                ' End If
            End If

            If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Or Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID Then
               txtDiscountOW.Enabled = True
                txtDiscountEM.Enabled = True
            Else
               txtDiscountOW.Enabled = False
               txtDiscountEM.Enabled = False
            End If



            'Populating Accept Multiple Work Order for Admin Site
            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                dvContact = dsContacts.Tables("ContactsSetUp").Copy.DefaultView
                If dvContact.Item(0).Item("StatusName") = "Approved" Then 'For approved suppliers only 
                    chkbxAcceptMultipleWO.Visible = True
                    chkbxAcceptMultipleWO.Checked = False
                    If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("AcceptMultipleWO")) Then
                        chkbxAcceptMultipleWO.Checked = dsContacts.Tables("Contacts").Rows(0).Item("AcceptMultipleWO")
                    End If
                End If
            End If
        End If
        'Populating "My Preferences"

        '   Condition is changed. excute following code in if condition for  supplier login.
        'If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
        '    If pnlSupplierPref.Visible = True Then
        '        'WO Min Value
        '        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("WOMinValue")) Then
        '            txtMinWO.Text = dsContacts.Tables("Contacts").Rows(0).Item("WOMinValue")
        '        Else
        '            txtMinWO.Text = "0"
        '        End If
        '    End If
        'End If





        'Populating Blocked companies
        Dim dsBlockedCompanies As DataSet = Nothing
        Dim XMLBlockedCompanies As Xml = Nothing
        If Not IsDBNull(dsContacts.Tables("Contacts").Rows(0).Item("BlockedCompanies")) Then
            If (dsContacts.Tables("Contacts").Rows(0).Item("BlockedCompanies")) <> "" Then
                dsBlockedCompanies = CommonFunctions.GetDSFromXML(dsContacts.Tables("Contacts").Rows(0).Item("BlockedCompanies"))
            End If
        End If


        'Dim strID As String = "txtClient"
        'Dim i As Integer
        'For i = 1 To 8
        '    CType(divAccount.FindControl(strID & i), TextBox).Text = ""
        'Next
        'If Not IsNothing(dsBlockedCompanies) Then
        '    If dsBlockedCompanies.Tables(0).Rows.Count = 1 Then
        '        For i = 0 To dsBlockedCompanies.Tables(0).Rows.Count - 1
        '            CType(divAccount.FindControl(strID & i + 1), TextBox).Text = dsBlockedCompanies.Tables(0).Rows(i)("CompanyName")
        '        Next
        '    Else
        '        For i = 0 To dsBlockedCompanies.Tables(0).Rows.Count - 1
        '            CType(divAccount.FindControl(strID & i + 1), TextBox).Text = dsBlockedCompanies.Tables(0).Rows(i)("CompanyName_Text")
        '        Next
        '    End If

        'End If


        'Initialise General file attachment UC
        UCFileUpload6.BizDivID = _bizDivID
        UCFileUpload6.CompanyID = _companyID
        UCFileUpload6.ExistAttachmentSourceID = _companyID
        UCFileUpload6.ExistAttachmentSource = "CompanyProfile"
        UCFileUpload6.AttachmentForID = _companyID
        UCFileUpload6.AttachmentForSource = "CompanyProfile"
        UCFileUpload6.ClearUCFileUploadCache()
        UCFileUpload6.PopulateAttachedFiles()
        UCFileUpload6.PopulateExistingFiles()




    End Sub


    ''' <summary>
    ''' To save company profile 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Function SaveProfile() As XSDContacts


        Dim strAppend As String
        Select Case _bizDivID
            Case ApplicationSettings.OWUKBizDivId
                strAppend = "OW"
            Case ApplicationSettings.SFUKBizDivId
                strAppend = "MS"
            Case ApplicationSettings.OWDEBizDivId
                strAppend = "DE"
        End Select


        Dim contactid As Integer = _companyID
        'Dim contactid As Integer = 1560
        Dim dsContacts As New XSDContacts()

        'For updating tblContacts
        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
            'Save Account Manager
            Dim nrowAM As XSDContacts.ContactsRow = dsContacts.Contacts.NewRow
            With nrowAM
                .MainContactID = CompanyID
                .ContactID = contactid
                .IsCompany = 1
                If txtProfile.Text.Trim <> "" Then
                    .Profile = txtProfile.Text.Trim
                End If
                .AccountManagerID = ddlOWAccountManager.SelectedValue
                If ddlBusinessArea.SelectedValue <> "" Then
                    .BusinessArea = ddlBusinessArea.SelectedValue
                End If
                .CompanyName = txtCompanyName.Text.Trim
                .IsTestAccount = chkIsTestAccount.Checked
                .Profile = txtProfile.Text.Trim
                'If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
                '    Select Case _bizDivID
                '        Case ApplicationSettings.OWUKBizDivId
                '            .OWHourlyRate = txtHourlyRate.Text.Trim
                '        Case ApplicationSettings.SFUKBizDivId
                '            .MSHourlyRate = txtHourlyRate.Text.Trim
                '    End Select
                '    If ddlTravelDistance.SelectedValue <> "" Then
                '        .TravelDistance = CStr(ddlTravelDistance.SelectedValue)
                '    End If
                'End If


            End With

            dsContacts.Contacts.Rows.Add(nrowAM)

        End If
        Dim nrowCS1 As XSDContacts.ContactsSetupRow = dsContacts.ContactsSetup.NewRow
        With nrowCS1
            .BizDivID = BizDivID
            .ContactID = contactid
            .Status = ddlType.SelectedValue
            .ClassID = Session(Request("companyid") & "_ContactClassID")
        End With
        dsContacts.ContactsSetup.Rows.Add(nrowCS1)


        Dim nrowCA As XSDContacts.ContactsAttributesRow = dsContacts.ContactsAttributes.NewRow

        With nrowCA
            .ContactID = contactid
            'Save credit terms
            If (Not (ddlSuppPaymentsTerms Is Nothing)) Then
                If ddlSuppPaymentsTerms.SelectedValue <> "" Then
                    .SupplierCreditTerms = ddlSuppPaymentsTerms.SelectedValue
                End If
            End If
            If (Not (ddlPayTerms Is Nothing)) Then
                If ddlPayTerms.SelectedValue <> "" Then
                    .SupplierCreditTerms = ddlPayTerms.SelectedValue
                End If
            End If
            If (Not (ddlBuyerPaymentsTerms Is Nothing)) Then
                If ddlBuyerPaymentsTerms.SelectedValue <> "" Then
                    .BuyerCreditTerms = ddlBuyerPaymentsTerms.SelectedValue
                End If
            End If
            If ddlNoOfEmployees.SelectedValue.ToString <> "" Then
                .CompanyStrength = ddlNoOfEmployees.SelectedValue.ToString
            End If
            If txtNoOfLocations.Text.Trim <> "" Then
                .NoOfLocations = txtNoOfLocations.Text.Trim
            End If

            If ddlIndustry.SelectedValue <> "" Then
                .IndustryID = ddlIndustry.SelectedValue
            End If
            If txtBusinessStarted.Text.Trim <> "" Then
                .BusinessStartYear = txtBusinessStarted.Text.Trim
            End If
            If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
                If BizDivID = ApplicationSettings.SFUKBizDivId Then
                    .MSPartnerLevel = ddlpartner.SelectedValue.ToString
                End If
                If RegisterYes.Checked = True Or RegisterNo.Checked = True Then
                    If RegisterYes.Checked = True Then
                        .VATRegistered = RegisterYes.Checked
                    ElseIf RegisterNo.Checked = True Then
                        .VATRegistered = Not (RegisterNo.Checked)
                    End If
                End If
                If txtVATRegistrationNumber.Text.Trim <> "" And RegisterYes.Checked = True Then
                    .VATRegistrationNumber = txtVATRegistrationNumber.Text.Trim()
                Else
                    .VATRegistrationNumber = ""
                End If
                'If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                '    'PeopleRegNo
                '    If txtPeopleRegNo.Text.Trim <> "" Then
                '        .PeopleRegNo = txtPeopleRegNo.Text.Trim
                '    End If
                'End If
                If CRBCheckedYes.Checked Then
                    .CRBChecked = CRBCheckedYes.Checked
                ElseIf CRBCheckedNo.Checked Then
                    .CRBChecked = Not (CRBCheckedNo.Checked)
                End If

                If (txtNoOfEngineers.Text.Trim <> "") Then
                    .NoOfEngineers = txtNoOfEngineers.Text.Trim
                End If
                If (txtStdDayRate.Text.Trim <> "") Then
                    .StdDayRate = txtStdDayRate.Text.Trim
                End If

                If FulfilInhouseYes.Checked Then
                    .FulfilInhouse = True
                ElseIf FulfilInhouseNo.Checked Then
                    .FulfilInhouse = False
                End If


                'Select Case _bizDivID
                '    Case ApplicationSettings.OWUKBizDivId
                '        If (ddlResponseTime.SelectedValue <> "") Then
                '            .OWServiceResponseTime = ddlResponseTime.SelectedValue
                '        End If

                '        If SourcePartYes.Checked Then
                '            .OWSourceParts = True
                '        End If
                '    Case ApplicationSettings.SFUKBizDivId
                '        If ddlResponseTime.SelectedValue <> "" Then
                '            .MSServiceResponseTime = ddlResponseTime.SelectedValue
                '        End If
                '        If SourcePartYes.Checked Then
                '            .MSSourceParts = True
                '        End If
                'End Select
                'Save "Accreditations"
                'If txtAccreditations.Text.Trim <> "" Then
                '    .CompanyAccreditations = txtAccreditations.Text.Trim
                'End If


                'Save "Other Information"
                If txtOtherInformation.Text.Trim <> "" Then
                    .OtherInfo = txtOtherInformation.Text.Trim
                End If
                If UKSecurityClearanceYes.Checked Then
                    .UKSecurityClearance = UKSecurityClearanceYes.Checked
                ElseIf UKSecurityClearanceNo.Checked Then
                    .UKSecurityClearance = Not (UKSecurityClearanceNo.Checked)
                Else
                    .UKSecurityClearance = False
                End If
                If CSCSYes.Checked Then
                    .CSCS = CSCSYes.Checked
                ElseIf CSCSNo.Checked Then
                    .CSCS = Not (CSCSNo.Checked)
                End If
                If chkInsurance4.Checked = True And txtEmployeeLiabilityDate.Text <> "" Then
                    .EmployersLiability = True
                    If txtEmployeeLiabilityDate.Text <> "Enter expiry date" Then
                        .EmpLiabilityExpDate = txtEmployeeLiabilityDate.Text.Trim
                    End If
                    If txtCoverAmountEmpLiabiity.Text <> "Enter cover amount" And txtCoverAmountEmpLiabiity.Text <> "" Then
                        .EmpLiabilityCoverAmt = txtCoverAmountEmpLiabiity.Text.Trim
                    End If

                    dsContacts = UCFileUpload1.ReturnFilledAttachments(dsContacts)
                End If


                If chkInsurance5.Checked = True And txtPublicLiabilityDate.Text <> "" Then
                    .PublicLiability = True
                    If txtPublicLiabilityDate.Text <> "Enter expiry date" Then
                        .PublicLiabilityExpDate = txtPublicLiabilityDate.Text.Trim
                    End If
                    If txtCoverAmountpubLiability.Text <> "Enter cover amount" And txtCoverAmountpubLiability.Text <> "" Then
                        .PublicLiabilityCoverAmt = txtCoverAmountpubLiability.Text
                    End If
                    dsContacts = UCFileUpload2.ReturnFilledAttachments(dsContacts)
                End If
                If chkInsurance6.Checked = True And txtProfessionalIndemnityDate.Text <> "" Then
                    .ProfessionalIndemnity = True
                    If txtProfessionalIndemnityDate.Text <> "Enter expiry date" Then
                        .ProfIndemnityExpDate = txtProfessionalIndemnityDate.Text.Trim
                    End If
                    If txtCoverAmountProfLiabity.Text <> "Enter cover amount" And txtCoverAmountProfLiabity.Text <> "" Then
                        .ProfIdemnityCoverAmt = txtCoverAmountProfLiabity.Text.Trim
                    End If
                    dsContacts = UCFileUpload3.ReturnFilledAttachments(dsContacts)
                End If

                If RegisterYes.Checked = True Then
                    dsContacts = UCFileUpload4.ReturnFilledAttachments(dsContacts)
                End If


                'If txtISORegNo.Text <> "" Then
                '    .ISORegNo = txtISORegNo.Text.Trim
                'End If

            End If
            If txtCompanyURL.Text.Trim <> "" Then
                .URL = txtCompanyURL.Text.Trim
            End If

            If txtCompanyRegNo.Text.Trim <> "" Then
                .CompanyRegNo = txtCompanyRegNo.Text.Trim
            End If
        End With
        dsContacts.ContactsAttributes.Rows.Add(nrowCA)

        'Save AOE for company
        Dim sitePrefix As String = "OW"
        Select Case ApplicationSettings.Country

            Case ApplicationSettings.CountryUK
                Select Case BizDivID
                    Case ApplicationSettings.OWUKBizDivId
                        sitePrefix = "OW"
                    Case ApplicationSettings.SFUKBizDivId
                        sitePrefix = "MS"
                End Select
            Case ApplicationSettings.CountryDE
                Select Case BizDivID
                    Case ApplicationSettings.OWDEBizDivId
                        sitePrefix = "DE"
                End Select
        End Select


        '   Condition is changed. excute following code in if condition for  supplier login.
        If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then

            Dim strAOEIds As String = ""
            If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "" Then
                If (CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value) <> "0" Then
                    strAOEIds = CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value
                End If
            End If


            If strAOEIds <> "" Then
                Dim AOEIds As String() = strAOEIds.Split(",")
                AOEIds = CommonFunctions.RemoveDuplicates(AOEIds)
                For Each aoeId As String In AOEIds
                    If (aoeId <> 0) Then
                        Dim nrowAOE As XSDContacts.ContactsExtAttributesRow = dsContacts.ContactsExtAttributes.NewRow
                        With nrowAOE
                            .ContactID = contactid
                            .AttributeLabel = sitePrefix & "AOE"
                            .AttributeValue = aoeId
                        End With
                        dsContacts.ContactsExtAttributes.Rows.Add(nrowAOE)
                    End If
                Next
            End If



            'For updating tblContactsAccreditations
            'Save accreditations
            'If BizDivID <> ApplicationSettings.SFUKBizDivId Then
            '    Dim ds As DataSet = UCAdminAccreds.GetSelectedAccred()
            '    For Each drowAccreds As DataRow In ds.Tables(0).Rows
            '        If (drowAccreds("TagId") = 0) Then
            '            Dim nrowNewCompAccred As XSDContacts.TagsRow = dsContacts.Tags.NewRow
            '            With nrowNewCompAccred
            '                .TagFor = "Others"
            '                .TagName = drowAccreds("TagName")
            '            End With
            '            dsContacts.Tags.Rows.Add(nrowNewCompAccred)
            '        End If

            '        Dim nrowCompAccred As XSDContacts.TagContactLinkageRow = dsContacts.TagContactLinkage.NewRow
            '        With nrowCompAccred
            '            .ContactID = contactid
            '            .MainContactID = CompanyID
            '            .TagID = drowAccreds("TagId")
            '            .MainTagID = drowAccreds("TagId")
            '            .TagExpiry = drowAccreds("TagExpiry")
            '            .TagInfo = drowAccreds("TagInfo")
            '            .OtherInfo = drowAccreds("OtherInfo")
            '        End With
            '        dsContacts.TagContactLinkage.Rows.Add(nrowCompAccred)
            '    Next
            'End If

            'If BizDivID <> ApplicationSettings.SFUKBizDivId Then
            '    Dim ds As DataSet = UCCompAccreds1.GetVendors()
            '    For Each drowAccreds As DataRow In ds.Tables("tblSelectedVendors").Rows
            '        Dim nrowCompAccred As XSDContacts.tblContactsAccreditationsRow = dsContacts.tblContactsAccreditations.NewRow
            '        With nrowCompAccred
            '            .ContactID = contactid
            '            .BizDivID = BizDivID
            '            .VendorID = drowAccreds("VendorID")
            '            If Not IsDBNull(drowAccreds("PartnerID")) Then
            '                If drowAccreds("PartnerID") <> 0 Then
            '                    .PartnerID = drowAccreds("PartnerID")
            '                End If
            '            End If
            '            If Not IsDBNull(drowAccreds("MemberShipNo")) Then
            '                If drowAccreds("MemberShipNo") <> "" Then
            '                    .MemberShipNo = drowAccreds("MemberShipNo")
            '                End If
            '            End If

            '        End With
            '        dsContacts.tblContactsAccreditations.Rows.Add(nrowCompAccred)
            '    Next
            'End If
            'end of save accreditations
            'Make Log Entry for VAT Regsitered Status Change
            Dim NewVATStatus As String = ""
            If RegisterYes.Checked = True Then
                NewVATStatus = "Yes"
            ElseIf RegisterNo.Checked = True Then
                NewVATStatus = "No"
            End If
            If ViewState("VATRegistered") <> NewVATStatus Then
                Dim nrowALog1 As XSDContacts.tblActionLogRow = dsContacts.tblActionLog.NewRow
                With nrowALog1
                    .LogID = 1
                    .BizDivID = _bizDivID
                    If NewVATStatus = "Yes" Then
                        .ActionID = ApplicationSettings.ActionLogID.VATYes
                    Else
                        .ActionID = ApplicationSettings.ActionLogID.VATNo
                    End If
                    .ActionByCompany = Session("CompanyId")
                    .ActionByContact = Session("UserId")
                    .ActionForCompany = _companyID
                    .ActionForContact = _userID
                    .ActionRef = 0
                    .ActionSrc = ""
                    ._Date = System.DateTime.Now()
                End With
                dsContacts.tblActionLog.Rows.Add(nrowALog1)
            End If
            ViewState("VATRegistered") = NewVATStatus
        End If
        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
            If ViewState("UserStatus") <> ddlType.SelectedValue Then
                Dim nrowALog1 As XSDContacts.tblActionLogRow = dsContacts.tblActionLog.NewRow
                With nrowALog1
                    .BizDivID = _bizDivID

                    If _classID = ApplicationSettings.RoleClientID Then
                        '.ActionID = IIf(ddlType.SelectedValue = ApplicationSettings.StatusSuspendedCompany, ApplicationSettings.ActionSuspend, ApplicationSettings.ActionApprove)
                        If ddlType.SelectedValue = ApplicationSettings.StatusSuspendedCompany Then
                            .ActionID = ApplicationSettings.ActionLogID.Suspend
                        ElseIf ddlType.SelectedValue = ApplicationSettings.StatusDeletedCompany Then
                            .ActionID = ApplicationSettings.ActionLogID.Delete
                        Else
                            .ActionID = ApplicationSettings.ActionLogID.Approve
                        End If
                    ElseIf _classID = ApplicationSettings.RoleSupplierID Then
                        '.ActionID = IIf(ddlType.SelectedValue = ApplicationSettings.StatusSuspendedCompany, ApplicationSettings.ActionSuspend, ApplicationSettings.ActionUnapprove)
                        If ddlType.SelectedValue = ApplicationSettings.StatusSuspendedCompany Then
                            .ActionID = ApplicationSettings.ActionLogID.Suspend
                        ElseIf ddlType.SelectedValue = ApplicationSettings.StatusDeletedCompany Then
                            .ActionID = ApplicationSettings.ActionLogID.Delete
                        Else
                            .ActionID = ApplicationSettings.ActionLogID.Unapprove
                        End If
                    End If
                    .ActionByCompany = Session("CompanyId")
                    .ActionByContact = Session("UserId")
                    .ActionForCompany = _companyID
                    If ddlType.SelectedValue = "Delete" Then
                        .ActionForContact = 0
                    Else
                        .ActionForContact = _userID
                    End If

                    .ActionRef = 0
                    .ActionSrc = ""
                    ._Date = System.DateTime.Now()

                End With
                dsContacts.tblActionLog.Rows.Add(nrowALog1)
            End If
        End If
        'Dim drow As DataRow
        '   Condition is changed. excute following code in if condition for  supplier login.    RoleClientID
        'Dim strID As String = "txtClient"
        'Dim j As Integer
        'Dim xmlBlockedCompanies As New StringBuilder
        'xmlBlockedCompanies.Append("<BlockedCompanies>")




        Dim nrowCS As XSDContacts.ContactsSettingsRow = dsContacts.ContactsSettings.NewRow

        'Dim id As String
        'Dim ids As String()
        'Dim i As Integer
        With nrowCS
            .ContactID = contactid
            If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
                If txtSortCode.Text.Trim <> "" Then
                    .BankSortCode = Encryption.Encrypt(txtSortCode.Text.Trim)
                End If
                If txtAccountNo.Text.Trim <> "" Then
                    .BankAccountNo = Encryption.Encrypt(txtAccountNo.Text.Trim)
                End If
                If txtAccountName.Text.Trim <> "" Then
                    .BankAccountName = Encryption.Encrypt(txtAccountName.Text.Trim)
                End If
                'If txtMinWO.Text.Trim <> "" Then
                '    Dim minValue As String = txtMinWO.Text.Trim
                '    minValue = Replace(minValue, ",", ".")
                '    .WOMinValue = minValue
                'End If

                '.BusinessIT = chkIT.Checked
                '.FreesatandDigitalAerials = chkFreesat.Checked
                '.HomePCInstallations = chkpc.Checked
                '.ConsumerElectronicandTV = chkElex.Checked

                .AcceptMultipleWO = chkbxAcceptMultipleWO.Checked
            End If
            'For j = 1 To 8
            '    If CType(divAccount.FindControl(strID & j), TextBox).Text <> "" Then
            '        xmlBlockedCompanies.Append("<CompanyName>" & CType(divAccount.FindControl(strID & j), TextBox).Text & "</CompanyName>")
            '    End If
            'Next
            'xmlBlockedCompanies.Append("</BlockedCompanies>")
            'If (xmlBlockedCompanies.ToString.Contains("<CompanyName>")) Then
            '    .BlockedCompanies = xmlBlockedCompanies.ToString
            'End If
        End With
        dsContacts.ContactsSettings.Rows.Add(nrowCS)

        '   Condition is changed. excute following code in if condition for  supplier login.    
        If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then

            'For OrderWorkDE: Add Konto Name field in the tblContactsSettings for suppliers only
            'If ApplicationSettings.Country = ApplicationSettings.CountryDE Then
            '    If txtKontoName.Text.Trim <> "" Then
            '        Dim nrowAN As OWContacts.tblContactsSettingsRow = dsContacts.tblContactsSettings.NewRow
            '        With nrowAN
            '            .ContactID = contactid
            '            .SettingLabel = "KontoName"
            '            .SettingValue = Encryption.Encrypt(txtKontoName.Text.Trim)
            '            .Sequence = 0
            '        End With
            '        dsContacts.tblContactsSettings.Rows.Add(nrowAN)
            '    End If
            'End If



            'Save "Regions that you cover"
            'Dim region As DataTable = UCRegions.saveListBox
            'For Each drow In region.Rows
            '    Dim nrowRegion As XSDContacts.ContactsExtAttributesRow = dsContacts.ContactsExtAttributes.NewRow
            '    With nrowRegion
            '        .ContactID = contactid
            '        .AttributeLabel = "Region"
            '        .AttributeValue = drow.Item("Value")
            '    End With
            '    dsContacts.ContactsExtAttributes.Rows.Add(nrowRegion)
            'Next
        End If
        dsContacts = UCFileUpload6.ReturnFilledAttachments(dsContacts)
        Return dsContacts
    End Function

    ''' <summary>
    ''' To fetch the details of the contact to populate company profile.
    ''' </summary>
    ''' <param name="IsMainContact"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetContact(ByVal Page As Page, ByVal paramBizDivId As Integer, Optional ByVal IsMainContact As Boolean = False, Optional ByVal KillCache As Boolean = True, Optional ByVal contactid As Integer = 0) As DataSet
        Dim cacheKey As String = "Contact" & "-" & CStr(contactid) & "-" & Page.Session.SessionID
        If KillCache = True Then
            Page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If Page.Cache(cacheKey) Is Nothing Then
            Select Case ApplicationSettings.SiteType
                ' Setting evironment for the company profile page when opened in admin site
                Case ApplicationSettings.siteTypes.admin
                    ds = CommonFunctions.GetContactsDSAdmin(Page, contactid, IsMainContact, cacheKey, KillCache)
                Case ApplicationSettings.siteTypes.site
                    ds = CommonFunctions.GetContactsDS(Page, Page.Session.SessionID, contactid, IsMainContact, cacheKey, KillCache)
            End Select

        Else
            ds = CType(Page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

    Protected Sub ValidatePage()
        Page.Validate()
        If (Page.IsValid = False) Then
            divValidationMain.Visible = True
        Else
            divValidationMain.Visible = False

            pnlSubmitForm.Visible = False
            pnlConfirm.Visible = True

















        End If
    End Sub


    ''' <summary>
    ''' To handle save profile action triggerd by the Save LinkButton at the top
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub SaveProfileTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click, btnSaveBottom.Click

        '   Condition is changed. excute following code in if condition for  supplier login.    
        If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
            CheckChkBoxOnValueEntered(sender, e)
        End If

        ValidatePage()
    End Sub


    ''' <summary>
    ''' To handle reset profile action triggerd by the Save LinkButton at the top
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ResetProfileTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetTop.Click, btnResetBottom.Click
        ResetProfile()
    End Sub


    ''' <summary>
    ''' Procedure to undo changes to company profile
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetProfile()
        txtComment.Text = ""
        ddlCommentReason.SelectedIndex = 0
        ViewState("PopupVisible") = 0
        '   Condition is changed. excute following code in if condition for  supplier login.    
        If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
            'Clear validation message
            divValidationMain.Visible = False
            UCFileUpload1.ClearUCFileUploadCache()
            UCFileUpload2.ClearUCFileUploadCache()
            UCFileUpload3.ClearUCFileUploadCache()
            UCFileUpload6.ClearUCFileUploadCache()
            UCFileUpload4.ClearUCFileUploadCache()
        End If

        If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
            UCAdminAccreds.populateAccred()
            UCAdminAccreds.PopulateSelectedGrid(SortDirection.Ascending, "TagName")

            'UCCompAccreds1.populateVendors(True)
            'UCCompAccreds1.PopulateSelectedGrid()
        End If

        PopulateCompanyProfile()

    End Sub

    ''' <summary>
    ''' To handle confirm action
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
        Dim ExcelPartner As Integer
        Dim EPCP As Integer
        Dim PaymentDiscountOW As Decimal
        Dim PaymentTermIdOW As Integer
        Dim PaymentDiscountEM As Decimal
        Dim PaymentTermIdEM As Integer
        Dim xmlContent As String = SaveProfile().GetXml
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)
        If ((Request("classid") = ApplicationSettings.RoleSupplierID) Or (Session(Request("companyid") & "_ContactClassID") = ApplicationSettings.RoleSupplierID)) Then
            If ExcelPartnerYes.Checked = True Then
                ExcelPartner = 1
            ElseIf ExcelPartnerNo.Checked = True Then
                ExcelPartner = 0
            End If
            If EPCPYes.Checked = True Then
                EPCP = 1
            ElseIf EPCPNo.Checked = True Then
                EPCP = 0
            End If

            'OA-622 - discount changes
            PaymentDiscountOW = CDec(txtDiscountOW.Text.Trim())
            PaymentTermIdOW = ddlSuppDiscountPaymentsTermsOW.SelectedValue
            PaymentDiscountEM = CDec(txtDiscountEM.Text.Trim())
            PaymentTermIdEM = ddlSuppDiscountPaymentsTermsEM.SelectedValue

        End If


        Dim flagchkRecordVerified As Boolean = False
        If ((Request("classid") = ApplicationSettings.RoleSupplierID) Or (Session(Request("companyid") & "_ContactClassID") = ApplicationSettings.RoleSupplierID)) Then
            flagchkRecordVerified = chkRecordVerifed.Checked
        End If



        Dim Success As Integer = ws.WSContact.UpdateCompanyProfile(xmlContent, _companyID, _bizDivID, CommonFunctions.FetchVerNum(), Session("UserID"), ExcelPartner, EPCP, PaymentDiscountEM, PaymentTermIdEM, PaymentDiscountOW, PaymentTermIdOW, flagchkRecordVerified)

        If Success = 1 Then
            'Poonam added on 12/8/2016 - Task - OA-300 : EA - Make add comment mandatory via pop-up when Deleting or Suspending a company
            If ViewState("PopupVisible") = 1 Then
                Dim Comment As String = txtComment.Text.Trim.Replace(Chr(13), "<BR>")
                Dim attachment As String = "<XSDContacts  />"
                Dim SuccessComment As Boolean = ws.WSContact.AddComments(_companyID, Session("UserId"), Comment, attachment, CInt(ddlCommentReason.SelectedItem.Value))
                ViewState("PopupVisible") = 0
            End If
            Cache.Remove("Contact" & "-" & CStr(_companyID) & "-" & Session.SessionID)

            '   Condition is changed. excute following code in if condition for  supplier login.    
            If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
                UCFileUpload1.ClearUCFileUploadCache()
                UCFileUpload2.ClearUCFileUploadCache()
                UCFileUpload3.ClearUCFileUploadCache()
                UCFileUpload6.ClearUCFileUploadCache()
                UCFileUpload4.ClearUCFileUploadCache()
            End If



            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                If chkSendMail.Checked = True Then
                    If ViewState("UserStatus") <> ddlType.SelectedValue Then

                        Dim CompanyStatus As String = ddlType.SelectedItem.Text

                        If CompanyStatus <> "Delete" Then
                            Emails.SendCompanyStatusMail(ViewState("Email"), ViewState("ContactName"), txtMessage.Text.Trim, CompanyStatus)
                        End If
                    End If
                End If
                'Remove Data from th cache if supplier accepts to select Multiple WOs.
                Cache.Remove("MultipleAcceptServicePartners")
            End If
            Dim IsAccountInfoUpdated As Boolean = False
            Dim ChangedBankSortCode As String = ""
            Dim ChangedBankAccountNo As String = ""
            Dim ChangedBankAccountName As String = ""
            Dim AdminContactName As String = ""
            Dim AdminContactEmail As String = ""

            If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                If Not IsNothing(ViewState("BankSortCode")) Then
                    If (ViewState("BankSortCode") <> txtSortCode.Text.Trim) Then
                        IsAccountInfoUpdated = True
                        ChangedBankSortCode = txtSortCode.Text.Trim
                    End If
                End If

                If Not IsNothing(ViewState("BankAccountNo")) Then
                    If (ViewState("BankAccountNo") <> txtAccountNo.Text.Trim) Then
                        IsAccountInfoUpdated = True
                        ChangedBankAccountNo = txtAccountNo.Text.Trim
                    End If
                End If

                If Not IsNothing(ViewState("BankAccountName")) Then
                    If (ViewState("BankAccountName") <> txtAccountName.Text.Trim) Then
                        IsAccountInfoUpdated = True
                        ChangedBankAccountName = txtAccountName.Text.Trim
                    End If
                End If

                If (IsAccountInfoUpdated = True) Then
                    If Not IsNothing(Session("CompanyAdminName")) Then
                        AdminContactName = Session("CompanyAdminName").ToString
                    Else
                        AdminContactName = ViewState("ContactName")
                    End If
                    If Not IsNothing(Session("CompanyAdminEmail")) Then
                        AdminContactEmail = Session("CompanyAdminEmail").ToString
                    Else
                        AdminContactEmail = ViewState("Email")
                    End If


                    Emails.SendAccountInfoUpdate(AdminContactName, AdminContactEmail, ChangedBankSortCode, ChangedBankAccountNo, ChangedBankAccountName, "", "", "", "", "", "", "", "", "")


                    ' Emails.SendAccountInfoUpdate("Sumit", "sumitk@iniquus.com", ChangedBankSortCode, ChangedBankAccountNo, ChangedBankAccountName, "", "", "", "", "", "", "", "", "")
                End If
            End If

            pnlSubmitForm.Visible = True
            PopulateCompanyProfile()
            pnlConfirm.Visible = False
        ElseIf Success = -10 Then
            pnlSubmitForm.Visible = True
            divValidationMain.Visible = True
            lblError.Text = ResourceMessageText.GetString("AccountsVersionControlMsg").Replace("<Link>", "CompanyProfile.aspx")
            pnlConfirm.Visible = False
        Else
            pnlSubmitForm.Visible = True
            divValidationMain.Visible = True
            lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
            pnlConfirm.Visible = False
        End If

    End Sub


    ''' <summary>
    ''' To handle cancel action on confirm
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Modification made by PB: Exit button should take the user back to the listing.
        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
            Response.Redirect((getBackToListingLink().Replace("~\", "")))
        Else
            pnlSubmitForm.Visible = True
            pnlConfirm.Visible = False
            If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
                'UCCompAccreds1.populateVendors(True)
                'UCCompAccreds1.PopulateSelectedGrid()
                UCAdminAccreds.populateAccred()
                UCAdminAccreds.PopulateSelectedGrid(SortDirection.Ascending, "TagName")
            End If
        End If
    End Sub


    Private Sub chkSendMail_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSendMail.CheckedChanged
        If chkSendMail.Checked = True Then
            showSendMail.Visible = True
        Else
            showSendMail.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' Function will check the checkbox for EmployersLiability, PublicLiability, ProfessionalIndemnity
    ''' Code added by PB
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CheckChkBoxOnValueEntered(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim attachFileEmp As Boolean = False
        Dim attachFilePub As Boolean = False
        Dim attachFileProf As Boolean = False

        Dim tempRepeaterEmp As Repeater
        Dim tempRepeaterPub As Repeater
        Dim tempRepeaterProf As Repeater

        'EmployersLiability
        tempRepeaterEmp = CType(UCFileUpload1.FindControl("rptAttList"), Repeater)
        If tempRepeaterEmp.Items.Count > 0 Then
            attachFileEmp = True
        End If
        If (attachFileEmp = True) Or ((txtEmployeeLiabilityDate.Text.Trim <> "Enter expiry date") And (txtEmployeeLiabilityDate.Text.Trim <> "")) Or (txtCoverAmountEmpLiabiity.Text.Trim <> "") Then
            chkInsurance4.Checked = True
        Else
            chkInsurance4.Checked = False
        End If

        'PublicLiability
        tempRepeaterPub = CType(UCFileUpload2.FindControl("rptAttList"), Repeater)
        If tempRepeaterPub.Items.Count > 0 Then
            attachFilePub = True
        End If
        If (attachFilePub = True) Or ((txtPublicLiabilityDate.Text.Trim <> "Enter expiry date") And (txtPublicLiabilityDate.Text.Trim <> "")) Or (txtCoverAmountpubLiability.Text.Trim <> "") Then
            chkInsurance5.Checked = True
        Else
            chkInsurance5.Checked = False
        End If

        'ProfessionalIndemnity
        tempRepeaterProf = CType(UCFileUpload3.FindControl("rptAttList"), Repeater)
        If tempRepeaterProf.Items.Count > 0 Then
            attachFileProf = True
        End If
        If (attachFileProf = True) Or ((txtProfessionalIndemnityDate.Text.Trim <> "Enter expiry date") And (txtProfessionalIndemnityDate.Text.Trim <> "")) Or (txtCoverAmountProfLiabity.Text.Trim <> "") Then
            chkInsurance6.Checked = True

        Else
            chkInsurance6.Checked = False

        End If











    End Sub

#Region "Admin Code"

    ''' <summary>
    ''' Method to set the session used by the OW Admin to read profiles of companies
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub setEnvForCompany()
        Dim ddItem As New ListItem
        If _classID = ApplicationSettings.RoleClientID Then
            'company type

            ddItem.Text = "Approve"
            ddItem.Value = ApplicationSettings.StatusApprovedCompany
            ddlType.Items.Add(ddItem)

            If Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID Then
                ddItem = Nothing
                ddItem = New ListItem
                ddItem.Text = "Suspend"
                ddItem.Value = ApplicationSettings.StatusSuspendedCompany
                ddlType.Items.Add(ddItem)
            End If
        ElseIf _classID = ApplicationSettings.RoleSupplierID Then
            'company type

            ddItem.Text = "Unapprove"
            ddItem.Value = ApplicationSettings.StatusNewCompany
            ddlType.Items.Add(ddItem)

            If Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID Then
                ddItem = Nothing
                ddItem = New ListItem
                ddItem.Text = "Suspend"
                ddItem.Value = ApplicationSettings.StatusSuspendedCompany
                ddlType.Items.Add(ddItem)
            End If

        End If

        ddItem = Nothing
        ddItem = New ListItem
        ddItem.Text = "Delete"
        ddItem.Value = ApplicationSettings.StatusDeletedCompany
        ddlType.Items.Add(ddItem)
        CommonFunctions.PopulateIndustries(Page, ddlIndustry)

        If Session("EmailMsg") = "" Then
            txtMessage.Text = EmailContents.GetString("AccountStatusMessage")
        End If

    End Sub
    Public Function getBackToListingLink() As String
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender")
                Case "AdminSearchAccounts"
                    link = "~\SearchAccounts.aspx?"
                    link &= "bizDivId=" & Request("BizDivID")
                    link &= "&companyId=" & Request("CompanyID")
                    link &= "&contactId=" & Request("contactId")
                    link &= "&classId=" & Request("ClassID")
                    link &= "&statusId=" & Request("statusId")
                    link &= "&roleGroupId=" & Request("roleGroupId")
                    link &= "&skillsarea=" & Request("skillsarea")
                    link &= "&product=" & Request("product")
                    link &= "&region=" & Request("region")
                    If Not IsNothing(Request("vendorIDs")) Then
                        link &= "&vendorIDs=" & Request("vendorIDs")
                    End If

                    link &= "&txtPostCode=" & Request("txtPostCode")
                    link &= "&txtKeyword=" & Request("txtKeyword")
                    link &= "&refcheck=" & Request("refcheck")
                    link &= "&txtFName=" & Request("txtFName")
                    link &= "&txtLName=" & Request("txtLName")
                    link &= "&txtCompanyName=" & Request("txtCompanyName")
                    'Poonam modified on 31-03-2017 - Task - OA-427 - OA - Unsupported spaces in Admin users
                    link &= "&txtEmail=" & (Request("txtEmail"))
                    link &= "&CRB=" & Request("CRB")
                    link &= "&CSCS=" & Request("CSCS")
                    link &= "&UKSec=" & Request("UKSec")
                    'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
                    link &= "&ExcelPartner=" & Request("ExcelPartner")
                    link &= "&EPCP=" & Request("EPCP")
                    link &= "&vendor=" & Request("vendor")
                    link &= "&Nto=" & Request("Nto")
                    link &= "&TrackSp=" & Request("TrackSp")
                    link &= "&EngineerCRBCheck=" & Request("EngineerCRBCheck")
                    link &= "&EngineerCSCSCheck=" & Request("EngineerCSCSCheck")
                    link &= "&EngineerUKSecurityCheck=" & Request("EngineerUKSecurityCheck")
                    link &= "&EngineerRightToWorkInUK=" & Request("EngineerRightToWorkInUK")
                    link &= "&EngProofOfMeeting=" & Request("EngProofOfMeeting")
                    link &= "&Dts=" & Request("Dts")
                    ' TODO
                    link &= "&mode=" & ""
                    link &= "&sender=" & Request("sender")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")

                Case "SearchWO", "searchwo"
                    link = "~\SearchWOs.aspx?"
                    link &= "PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&SrcWorkorderID=" & Request("WorkorderID")
                    link &= "&SrcCompanyName=" & Request("CompanyName")
                    link &= "&SrcKeyword=" & Request("KeyWord")
                    link &= "&SrcPONumber=" & Request("PONumber")
                    link &= "&SrcReceiptNumber=" & Request("ReceiptNumber")
                    link &= "&SrcPostCode=" & Request("PostCode")
                    link &= "&SrcDateStart=" & Request("DateStart")
                    link &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
                    link &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")
                    link &= "&sender=CompanyProfile"
                Case "CustomerHistory", "customerhistory"
                    link = "~\CustomerHistory.aspx?"
                    link &= "PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&txtWorkorderID=" & Request("txtWorkorderID")
                    link &= "&CompanyName=" & Request("CompanyName")
                    link &= "&Keyword=" & Request("KeyWord")
                    link &= "&PONumber=" & Request("PONumber")
                    link &= "&ReceiptNumber=" & Request("ReceiptNumber")
                    link &= "&PostCode=" & Request("PostCode")
                    link &= "&DateStart=" & Request("DateStart")
                    link &= "&DateSubmittedFrom=" & Request("DateSubmittedFrom")
                    link &= "&DateSubmittedTo=" & Request("DateSubmittedTo")
                    link &= "&sender=SearchWO"

                Case "AdminContactsListing"
                    link = "~\AMContacts.aspx?"
                    link &= "bizDivId=" & Request("BizDivID")
                    link &= "&contactType=" & Request("contactType")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&fromDate=" & Request("fromDate")
                    link &= "&toDate=" & Request("toDate")
                    link &= "&sender=" & Request("sender")
                Case "OrderMatch"
                    link = "~\WOOrderMatch.aspx?"
                    link &= "bizDivId=" & Request("bizDivId")
                    link &= "&bizdiv=" & Request("bizdiv")
                    link &= "&companyId=" & Request("companyId")
                    link &= "&contactId=" & Request("contactId")
                    link &= "&classId=" & Request("classId")
                    If Not IsNothing(Request("vendorIDs")) Then
                        link &= "&vendorIDs=" & Request("vendorIDs")
                    End If

                    link &= "&statusId=" & Request("statusId")
                    link &= "&roleGroupId=" & Request("Request")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&product=" & Request("product")
                    link &= "&region=" & Request("region")
                    link &= "&PostCode=" & Request("PostCode")
                    link &= "&Keyword=" & Request("Keyword")
                    link &= "&Group=" & Request("Group")
                    link &= "&refcheck=" & Request("refcheck")
                    link &= "&MinWOValue=" & Request("MinWOValue")

                    link &= "&companyName=" & Request("companyName")
                    link &= "&noOfEmployees=" & Request("noOfEmployees")
                    link &= "&ShowSupplierWithCW=" & Request("ShowSupplierWithCW")
                    If Not IsNothing(Request("vendorIDs")) Then
                        link &= "&vendorIDs=" & Request("vendorIDs")
                    End If


                    link &= "&mode=" & "edit"
                    link &= "&sender=" & "CompanyProfile"
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&NearestTo=" & Request("NearestTo")
                    link &= "&Distance=" & Request("Distance")
                    link &= "&crbChecked=" & Request("crbChecked")
                    link &= "&ukSecurity=" & Request("ukSecurity")
                    link &= "&CSCS=" & Request("CSCS")
                    'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
                    link &= "&ExcelPartner=" & Request("ExcelPartner")
                    link &= "&EPCP=" & Request("EPCP")
                Case "FavouriteListing"
                    link = "~\FavouriteSupplierList.aspx?Sender=CompanyProfile"
                    link &= Session("BuyersFavListingBacklink")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&classId=" & Request("ClassID")
                Case "AdminSearchSuppliers"
                    link = "~\SearchSuppliers.aspx?Sender=CompanyProfile"
                    link &= Session("BuyersFavListingBacklink")

                    link &= "&product=" & Request("product")
                    link &= "&region=" & Request("region")
                    link &= "&txtPostCode=" & Request("txtPostCode")
                    link &= "&txtKeyword=" & Request("txtKeyword")
                    link &= "&refcheck=" & Request("refcheck")
                    link &= "&txtFName=" & Request("txtFName")
                    link &= "&txtLName=" & Request("txtLName")
                    link &= "&txtCompanyName=" & Server.UrlDecode(Request("txtCompanyName"))
                    'Poonam modified on 31-03-2017 - Task - OA-427 - OA - Unsupported spaces in Admin users
                    link &= "&txtEmail=" & (Request("txtEmail"))
                    ' TODO
                    link &= "&mode=" & ""
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")

                Case "AdminWOHistory", "adminwohistory"
                    link = "~\AdminWOHistory.aspx?Sender=CompanyProfile"
                    link &= "&WOID=" & Request("WOID")
                    link &= "&WorkOrderId=" & Request("WorkOrderId")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&companyid=" & Request("companyid")
                    link &= "&Group=" & Request("Group")
                    If (Request("SupCompId") = "") Then
                        link &= "&SupCompId=0"
                    Else
                        link &= "&SupCompId=" & Request("SupCompId")
                    End If
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    'link += "&SupContactId=0&SupCompID=2675&Group=Invoiced&BizDivID=1&FromDate=&ToDate=&PS=25&PN=0&SC=WorkOrderId&SO=0&CompID=&txtPONumber=&txtPostCode=&txtKeyword=&txtWorkorderID=20130100002&txtCompanyName=&sender=SearchWO&SrcWorkorderID=20130100002&CompanyName=&Keyword=&PONumber=&PostCode="

                Case "AdminWoListing", "adminwolisting"
                    link = "~\AdminWoListing.aspx?Sender=CompanyProfile"
                    link &= "&mode=" & Request("mode")
                    link &= "&Group=" & Request("Group")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&CompID=" & Request("CompID")
                    link &= "&companyid=" & Request("companyid")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&WorkorderID=" & Request("WorkorderID")

                Case "AdminWoDetails", "adminwodetails"
                    link = "~\AdminWoDetails.aspx?Sender=CompanyProfile"
                    link &= "&mode=" & Request("mode")
                    link &= "&Group=" & Request("Group")
                    link &= "&BizDivID=" & Request("bizDivId")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&CompID=" & Request("CompID")
                    link &= "&companyid=" & Request("companyid")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&WorkorderID=" & Request("WorkorderID")

                Case "InsuranceExpiry", "insuranceexpiry"
                    link = "~\InsuranceExpiry.aspx?Sender=CompanyProfile"
                    link &= "&BizDivID=" & Request("bizDivId")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                Case "SearchAllAccounts"
                    link = "~\SearchAllAccounts.aspx?"
                    link &= "SrcCompanyName=" & Request("CompanyName")
                    link &= "&SrcProfile=" & Request("Profile")
                    link &= "&SrcAccreditations=" & Request("txtAccreditations")
                    link &= "&SrcAdditionalInfo=" & Request("OtherInfo")
                    link &= "&SrcKeyword=" & Request("KeyWord")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=CompanyProfile"
                Case "UnapprovedEngineers"
                    link = "~\SpecialistsForm.aspx?"
                    link &= "bizDivId=" & Request("bizDivId")
                    link &= "&ContactId=" & Request("ContactId")
                    link &= "&CompanyID=" & Request("CompanyID")
                    link &= "&ClassId=" & Request("ClassId")
                    link &= "&sender=" & Request("sender")
                Case "SalesInvoiceGeneration"
                    link = "~\SalesInvoiceGeneration.aspx?"
                    link &= "&companyId=" & Request("ClientCompanyID")
                    link &= "&bizDivId=" & Request("bizDivId")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=" & "SalesInvoiceGeneration"
                Case "WorkOrderDetails"
                    link = "~\AdminWoDetails.aspx?Sender=SearchFinance"
                    link &= "&mode=" & Request("mode")
                    link &= "&Group=" & Request("Group")
                    link &= "&BizDivID=" & Request("bizDivId")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&CompID=" & Request("CompID")
                    link &= "&companyid=" & Request("companyid")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&WorkorderID=" & Request("WorkorderID")
                    link &= "&txtPONumber=" & Request("SrcPONumber")
                    link &= "&txtInvoiceNo=" & Request("SrcInvoiceNo")
                    link &= "&txtKeyword=" & Request("SrcKeyword")
                    link &= "&txtWorkorderID=" & Request("SrcWorkOrderId")
                    link &= "&txtCompanyName=" & Request("SrcCompanyName")
                    link &= "&txtInvoiceDate=" & Request("SrcInvoiceDate")
            End Select
        Else
            'Following session variable added to fix the issue: Admin Company profile back to listing does not work
            If Not IsNothing(Session("BackToListinglink")) Then
                link = Session("BackToListinglink")
            Else
                link = "~\AMContacts.aspx" & strBackToListing
            End If
        End If
        Session("BackToListinglink") = link
        Return link
    End Function
#End Region


    Private Sub SetCacheForAttachment(ByVal ID As Integer, ByVal source As String, ByVal dsView As DataView)
        Dim Cachekey As String = ""
        Cachekey = "AttachedFiles-" & _companyID & "-" & source & "-" & Session.SessionID
        If Page.Cache(Cachekey) Is Nothing Then
            dsView.RowFilter = "LinkSource = '" & source & "'"
        Page.Cache.Add(Cachekey, dsView, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        End If

    End Sub
    Public Sub RemoveCallback(ByVal tag As String, ByVal obj As Object, ByVal reason As CacheItemRemovedReason)
        If (CacheItemRemovedReason.Underused = reason) Then
            Cache.Remove(tag)
            If Not IsNothing(obj) Then
                Cache.Insert(tag, obj, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            End If
        Else
            Cache.Remove(tag)
        End If
    End Sub
    'Poonam added on 12/8/2016 - Task - OA-300 : EA - Make add comment mandatory via pop-up when Deleting or Suspending a company
    Public Sub ShowPopup(ByVal sender As Object, ByVal e As System.EventArgs)
        If ((Request("classid") = ApplicationSettings.RoleSupplierID) Or (Session(Request("companyid") & "_ContactClassID") = ApplicationSettings.RoleSupplierID)) Then
            If ddlType.SelectedValue = ApplicationSettings.StatusNewCompany Or ddlType.SelectedValue = ApplicationSettings.StatusDeletedCompany Then
                SortCodeIndicator.Style.Add("display", "none")
                rqSortCode.Enabled = False

                AccountNumberIndicator.Style.Add("display", "none")
                rqAccountNumber.Enabled = False

                txtAccountNameIndicator.Style.Add("display", "none")
                rqtxtAccountName.Enabled = False
                divValidationMain.Visible = False
            Else
                SortCodeIndicator.Style.Add("display", "inline")
                rqSortCode.Enabled = True

                AccountNumberIndicator.Style.Add("display", "inline")
                rqAccountNumber.Enabled = True

                txtAccountNameIndicator.Style.Add("display", "inline")
                rqtxtAccountName.Enabled = True

            End If
        End If
        txtComment.Text = ""
        ddlCommentReason.SelectedIndex = 0
        mdlComment.Show()

    End Sub
    'Poonam added on 19/8/2016 - Task - OA-300 : EA - Make add comment mandatory via pop-up when Deleting or Suspending a company
    Private Sub hdnBtnAction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnBtnAction.Click
        ViewState("PopupVisible") = 1
    End Sub
    'OA-622 - discount changes
    Private Sub ddlSuppDiscountPaymentsTermsOW_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlSuppDiscountPaymentsTermsOW.SelectedIndexChanged


        CommonFunctions.GetStandardDiscount(Page, txtDiscountOW, Convert.ToInt32(ddlSuppDiscountPaymentsTermsOW.SelectedValue), "Orderwork")

    End Sub
    'OA-622 - discount changes
    Private Sub ddlSuppDiscountPaymentsTermsEM_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlSuppDiscountPaymentsTermsEM.SelectedIndexChanged


        CommonFunctions.GetStandardDiscount(Page, txtDiscountEM, Convert.ToInt32(ddlSuppDiscountPaymentsTermsEM.SelectedValue), "Empowered")

    End Sub
End Class


