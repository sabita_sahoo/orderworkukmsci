Partial Public Class UCMSViewCashReceiptUK
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs
    Protected WithEvents lblInvoiceType, lblAddress, lblCity, lblPostCode, lblCompNameInvoiceTo As Label
    Protected WithEvents lblInvoiceTo, lblCompanyName, lblInvoiceNo, lblInvoiceDate As Label
    Protected WithEvents lblDate, lblDescription As Label
    Protected WithEvents lblTotalAmount As Label
    Protected WithEvents lblAccountNo As Label
    Protected WithEvents lblTelNo, lblFaxNo, lblRefNumber, lblRegistartionNo, lblRegistartionNoLine2 As Label
    Protected WithEvents rptList As Repeater



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim ds As DataSet
            If Request.QueryString("ReceiptType") = "UnAllocated" Or Request.QueryString("ReceiptType") = "UA Receipt" Then
                ds = ws.WSFinance.MS_GetUnAllocatedReceiptAdviceDetails(Request("BizDivId"), Request("invoiceNo"))
            Else
                ds = ws.WSFinance.MS_GetReceiptAdviceDetails(Request("BizDivId"), Request("invoiceNo"))
            End If
            If ds.Tables("tblOWAddress").Rows.Count > 0 Then

                lblInvoiceNo.Text = Request("invoiceNo").ToString

                'Company Name
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("CompanyName"))) Then
                    lblCompanyName.Text = ds.Tables("tblOWAddress").Rows(0)("CompanyName")
                End If
                'Address
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Address"))) Then
                    lblAddress.Text = ds.Tables("tblOWAddress").Rows(0)("Address")
                End If
                'OW city

                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("City"))) Then
                    lblCity.Text = ds.Tables("tblOWAddress").Rows(0)("City") & ","
                End If

                'OW post code
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("PostCode"))) Then
                    lblPostCode.Text = ds.Tables("tblOWAddress").Rows(0)("PostCode")
                End If

                'OW Phone
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Phone"))) Then
                    lblTelNo.Text = ds.Tables("tblOWAddress").Rows(0)("Phone")
                End If
                'OW Fax
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Fax"))) Then
                    lblFaxNo.Text = ds.Tables("tblOWAddress").Rows(0)("Fax")
                End If
            End If


            If ds.Tables("tblBillingAddress").Rows.Count > 0 Then
                'Invoice To
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("CompanyName"))) Then
                    lblCompNameInvoiceTo.Text = ds.Tables("tblBillingAddress").Rows(0)("CompanyName")
                End If

                'Address Invoice to
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("Address"))) Then
                    If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("State"))) Then
                        If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("PostCode"))) Then
                            lblInvoiceTo.Text = ds.Tables("tblBillingAddress").Rows(0)("Address") & "<br>" & ds.Tables("tblBillingAddress").Rows(0)("City") & "<br>" & ds.Tables("tblBillingAddress").Rows(0)("State") & " " & ds.Tables("tblBillingAddress").Rows(0)("PostCode")
                        End If
                    End If
                End If

            End If

            If ds.Tables("tblAdviceDetails").Rows.Count > 0 Then

                'Invoice Date
                If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceDate"))) Then
                    lblInvoiceDate.Text = Strings.FormatDateTime(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceDate").ToString(), DateFormat.ShortDate)
                End If

                'Account no
                If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("AccountNo"))) Then
                    lblAccountNo.Text = ds.Tables("tblAdviceDetails").Rows(0)("AccountNo")
                End If

            End If

            If ds.Tables("tblOrderWorkDetails").Rows.Count > 0 Then
                'Registration number
                lblRegistartionNo.Text = "Registered in England"

                If Not (IsDBNull(ds.Tables("tblOrderWorkDetails").Rows(0)("CompanyRegNo"))) Then
                    lblRegistartionNoLine2.Text = "No.: " & ds.Tables("tblOrderWorkDetails").Rows(0)("CompanyRegNo")
                End If
            End If
            If ds.Tables("tblInvoices").Rows.Count > 0 Then
                rptList.DataSource = ds.Tables("tblInvoices").DefaultView
                rptList.DataBind()

                Dim totalAmount As Decimal = 0

                Dim drow As DataRow
                For Each drow In ds.Tables("tblInvoices").Rows
                    totalAmount += drow("Total")
                Next

                lblTotalAmount.Text = FormatCurrency(totalAmount, 2, TriState.True, TriState.True, TriState.False)
            End If

            If ds.Tables("tblUnAllocatedReceipt").Rows.Count > 0 Then
                rptList.DataSource = ds.Tables("tblUnAllocatedReceipt").DefaultView
                rptList.DataBind()
                If lblAccountNo.Text = "" Then
                    lblAccountNo.Text = ds.Tables("tblUnAllocatedReceipt").Rows(0).Item("AccountNo")
                End If
                If lblInvoiceDate.Text = "" Then
                    lblInvoiceDate.Text = ds.Tables("tblUnAllocatedReceipt").Rows(0).Item("InvoiceDate")
                End If

                Dim drow As DataRow
                Dim totalAmount As Double
                For Each drow In ds.Tables("tblUnAllocatedReceipt").Rows
                    totalAmount += drow("Total")
                Next
                If lblTotalAmount.Text = "" Then
                    lblTotalAmount.Text = FormatCurrency(totalAmount, 2, TriState.True, TriState.True, TriState.False)
                End If
            End If

        End If
    End Sub
End Class
