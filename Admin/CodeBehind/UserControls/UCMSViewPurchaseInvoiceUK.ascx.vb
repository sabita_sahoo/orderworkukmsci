Imports System.Web.UI.WebControls.Expressions

Partial Public Class UCMSViewPurchaseInvoiceUK
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs
    Protected WithEvents rptList, rptListOld As Repeater
    Protected WithEvents lblDiscount, lblVatOnDiscount, lblTotalDiscount, lblCity, lblWOID, lblPaymentTermLabel, lblAccountNumberOld, lblSortCodeOld,
                         lblAccountNameOld, lblTotalVatAsPerSiteOld, lblSubTotalOld, lblTotalVATOld, lblTotalAmountOld As Label
    Protected WithEvents tdNewFormat, tdOldFormat As Global.System.Web.UI.HtmlControls.HtmlTableCell

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim ds As DataSet = ws.WSFinance.MS_GetPurchaseAdviceDetails(Request("BizDivId"), Request("invoiceNo"))
            Dim IsNew As Integer = 0

            If ds.Tables("tblAdviceDetails").Rows.Count > 0 Then
                If Convert.ToInt32(ds.Tables("tblAdviceDetails").Rows(0)("IsNew")) = 0 Then
                    IsNew = 0
                Else
                    IsNew = 1
                End If
            End If

            If IsNew = 0 Then
                tdNewFormat.Visible = False
                tdOldFormat.Visible = True
            Else
                tdNewFormat.Visible = True
                tdOldFormat.Visible = False
            End If

            If ds.Tables("tblBillingAddress").Rows.Count > 0 Then
                'Supplier address
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("Address"))) Then
                    lblAddress.Text = ds.Tables("tblBillingAddress").Rows(0)("Address")
                End If
                'Supplier city
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("City"))) Then
                    lblCity.Text = ds.Tables("tblBillingAddress").Rows(0)("City") & ", "
                End If
                'Supplier post code
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("PostCode"))) Then
                    lblPostCode.Text = ds.Tables("tblBillingAddress").Rows(0)("PostCode")
                End If
            End If

            If ds.Tables("tblOWAddress").Rows.Count > 0 Then
                'Invoice To
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("CompanyName"))) Then
                    lblCompNameInvoiceTo.Text = ds.Tables("tblOWAddress").Rows(0)("CompanyName")
                End If

                'Address Invoice to
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Address"))) Then
                    If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("State"))) Then
                        If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("PostCode"))) Then
                            lblInvoiceTo.Text = ds.Tables("tblOWAddress").Rows(0)("Address") & "<br>" & ds.Tables("tblOWAddress").Rows(0)("State") & " " & ds.Tables("tblOWAddress").Rows(0)("PostCode")
                        End If
                    End If
                End If
            End If
            If ds.Tables("tblAdviceDetails").Rows.Count > 0 Then

                'Company Name
                If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("CompanyName"))) Then
                    lblCompanyName.Text = ds.Tables("tblAdviceDetails").Rows(0)("CompanyName")
                End If

                If IsNew = 0 Then
                    'Credit Terms
                    If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("CreditTerms"))) Then
                        lblCreditTerms.Text = ds.Tables("tblAdviceDetails").Rows(0)("CreditTerms")
                        lblPaymentTermLabel.Text = "Credit Terms "
                    End If
                Else
                    'Payment Terms
                    If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("PaymentTermDays"))) Then
                        lblCreditTerms.Text = ds.Tables("tblAdviceDetails").Rows(0)("PaymentTermDays")
                        lblPaymentTermLabel.Text = "Payment Terms "
                    End If
                End If


                'Invoice No.
                If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceNo"))) Then
                    lblInvoiceNo.Text = ds.Tables("tblAdviceDetails").Rows(0)("InvoiceNo")
                End If



                'Invoice Date
                If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceDate"))) Then
                    lblInvoiceDate.Text = Strings.FormatDateTime(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceDate").ToString(), DateFormat.ShortDate)
                End If


                ds.Tables("tblAdviceDetails").DefaultView.Sort = "CloseDate"
                If IsNew = 0 Then
                    rptListOld.DataSource = ds.Tables("tblAdviceDetails").DefaultView
                    rptListOld.DataBind()
                Else
                    rptList.DataSource = ds.Tables("tblAdviceDetails").DefaultView
                    rptList.DataBind()
                End If
                
                Dim totalDiscount As Decimal = 0
                Dim SPFees As Decimal = 0
                Dim VatOnDiscount As Decimal = 0
                Dim totalNetAmount As Decimal = 0
                Dim totalVAT As Decimal = 0
                Dim totalAmount As Decimal = 0

                Dim drow As DataRow

                For Each drow In ds.Tables("tblAdviceDetails").Rows
                    totalDiscount += drow("Discount")
                    SPFees += drow("SPFees")
                    VatOnDiscount += drow("VATOnDisc")
                    totalNetAmount += drow("NetPrice")
                    totalVAT += drow("VAT")
                    totalAmount += drow("Total")
                Next

                ' Discount()
                If IsNew = 0 Then
                    lblDiscount.Text = FormatCurrency(totalDiscount, 2, TriState.True, TriState.True, TriState.False)
                End If

                ' Service partner fee percentage
                If IsNew = 0 Then
                    lblSPFee.Text = FormatCurrency(SPFees, 2, TriState.True, TriState.True, TriState.False)
                End If

                'VAT on  Discount 
                If IsNew = 0 Then
                    lblVatOnDiscount.Text = FormatCurrency(VatOnDiscount, 2, TriState.True, TriState.True, TriState.False)
                End If

                'Total (discount+VAT on discount)
                If IsNew = 0 Then
                    lblTotalDiscount.Text = FormatCurrency(totalDiscount + VatOnDiscount, 2, TriState.True, TriState.True, TriState.False)
                End If

                ' Grand totals
                'Net Price
                If IsNew = 0 Then
                    lblSubTotalOld.Text = FormatCurrency(totalNetAmount, 2, TriState.True, TriState.True, TriState.False)
                    lblTotalVATOld.Text = FormatCurrency(totalVAT, 2, TriState.True, TriState.True, TriState.False)
                    lblTotalAmountOld.Text = FormatCurrency(totalAmount, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblSubTotal.Text = FormatCurrency(totalNetAmount, 2, TriState.True, TriState.True, TriState.False)
                    lblTotalVAT.Text = FormatCurrency(totalVAT, 2, TriState.True, TriState.True, TriState.False)
                    lblTotalAmount.Text = FormatCurrency(totalAmount, 2, TriState.True, TriState.True, TriState.False)
                End If


                
                Dim vatRate As String
                vatRate = ApplicationSettings.VATPercentage(Strings.FormatDateTime(lblInvoiceDate.Text.Trim, DateFormat.ShortDate))

                If IsNew = 0 Then
                    lblTotalVatAsPerSiteOld.Text = vatRate
                    lblTotalVatAsPerSite1.Text = vatRate
                Else
                    lblTotalVatAsPerSite.Text = vatRate
                End If


                If IsNew = 1 Then
                    If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("PaymentDiscount"))) Then
                        lblPaymentDiscount.Text = ds.Tables("tblAdviceDetails").Rows(0)("PaymentDiscount").ToString()
                    End If

                    If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("PaymentTerms"))) Then
                        lblPaymentTerms.Text = ds.Tables("tblAdviceDetails").Rows(0)("PaymentTerms").ToString()
                    End If
                    If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("PaymentTermDiscountApplied"))) Then
                        lblPaymentTermDiscountApplied.Text = FormatCurrency(ds.Tables("tblAdviceDetails").Rows(0)("PaymentTermDiscountApplied").ToString(), 2, TriState.True, TriState.True, TriState.False)
                    End If
                    lblInsurance.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
            If ds.Tables("tblCustomerDetails").Rows.Count > 0 Then
                'Vat Number
                If Not (IsDBNull(ds.Tables("tblCustomerDetails").Rows(0)("VATRegNo"))) Then
                    lblVATNo.Text = ds.Tables("tblCustomerDetails").Rows(0)("VATRegNo")
                End If

                'Account Name
                If Not (IsDBNull(ds.Tables("tblCustomerDetails").Rows(0)("BankAccountName"))) Then
                    If IsNew = 0 Then
                        lblAccountNameOld.Text = Encryption.Decrypt(ds.Tables("tblCustomerDetails").Rows(0)("BankAccountName"))
                    Else
                        lblAccountName.Text = Encryption.Decrypt(ds.Tables("tblCustomerDetails").Rows(0)("BankAccountName"))
                    End If
                End If

                'Sort Code
                If Not (IsDBNull(ds.Tables("tblCustomerDetails").Rows(0)("BankSortCode"))) Then
                    If IsNew = 0 Then
                        lblSortCodeOld.Text = Encryption.Decrypt(ds.Tables("tblCustomerDetails").Rows(0)("BankSortCode"))
                    Else
                        lblSortCode.Text = Encryption.Decrypt(ds.Tables("tblCustomerDetails").Rows(0)("BankSortCode"))
                    End If

                End If

                'Account Number
                If Not (IsDBNull(ds.Tables("tblCustomerDetails").Rows(0)("BankAccountNo"))) Then
                    If IsNew = 0 Then
                        lblAccountNumberOld.Text = Encryption.Decrypt(ds.Tables("tblCustomerDetails").Rows(0)("BankAccountNo"))
                    Else
                        lblAccountNumber.Text = Encryption.Decrypt(ds.Tables("tblCustomerDetails").Rows(0)("BankAccountNo"))
                    End If

                End If

                'Company Reg Number
                If Not (IsDBNull(ds.Tables("tblCustomerDetails").Rows(0)("CompanyRegNo"))) Then
                    lblCompanyRegNo.Text = "Company No : " & ds.Tables("tblCustomerDetails").Rows(0)("CompanyRegNo")
                End If
            End If
        End If
    End Sub

End Class