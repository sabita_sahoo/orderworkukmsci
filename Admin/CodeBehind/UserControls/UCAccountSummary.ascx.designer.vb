'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.42
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On


Partial Public Class UCAccountSummary
    Protected WithEvents tblAccSumm As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lnkAccountProfile As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents lblAccountName As System.Web.UI.WebControls.Label
    Protected WithEvents lblAccountId As System.Web.UI.WebControls.Label
    Protected WithEvents liSubClass As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblSubClass As System.Web.UI.WebControls.Label
    Protected WithEvents liSpecialist As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents litSpecialists As System.Web.UI.WebControls.Literal
    Protected WithEvents liLocation As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents litLocations As System.Web.UI.WebControls.Literal
    Protected WithEvents liFunds As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents liWithdrawalBalance As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents litWithdrawalBalance As System.Web.UI.WebControls.Literal
    Protected WithEvents liWOBalance As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents litWOBalance As System.Web.UI.WebControls.Literal
    Protected WithEvents liLastTransfer As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents litLastTransfer As System.Web.UI.WebControls.Literal
    Protected WithEvents liBalanceDue As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents litBalanceDue As System.Web.UI.WebControls.Literal
    Protected WithEvents lblPosRating As System.Web.UI.WebControls.Label
    Protected WithEvents lblNegRating As System.Web.UI.WebControls.Label
    Protected WithEvents lblNeuRating As System.Web.UI.WebControls.Label
    Protected WithEvents divDispAllRatings As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents litRating As System.Web.UI.WebControls.Literal
    Protected WithEvents liRating As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents litFundsLabel As System.Web.UI.WebControls.Literal
    Protected WithEvents liDateApproved As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblDateApproved As System.Web.UI.WebControls.Label

    Protected WithEvents lblAdminName As System.Web.UI.WebControls.Label
    Protected WithEvents lblAdminEmail As System.Web.UI.WebControls.Label
    Protected WithEvents liActiveInPast90days As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblActiveInPast90days As System.Web.UI.WebControls.Label
    Protected WithEvents liCreated As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblCreated As System.Web.UI.WebControls.Label
    Protected WithEvents liModified As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblModified As System.Web.UI.WebControls.Label
    Protected WithEvents liApproved As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblApproved As System.Web.UI.WebControls.Label

End Class
