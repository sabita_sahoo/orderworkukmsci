
''' <summary>
''' Class to implement the Top Msg label such as who has logged into the system
''' </summary>
''' <remarks></remarks>
Partial Public Class UCTopMSgLabel
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Control to show UC top message after login to the system
        Try
            If Not Session("RoleGroupID") Is Nothing Then

                Dim strAdmin, strRep, strMarketing, strFinance, strFinanceManager As String
                Dim strMgr As String
                Dim struser As String
                Dim strSpec As String
                strSpec = "a Specialist"

                lblUserName.Text = ResourceMessageText.GetString("WelcomeLoginLabel").Replace("<Name>", Session("FirstName") + " " + Session("LastName"))
                strAdmin = ResourceMessageText.GetString("AsAdministrator")
                strMgr = ResourceMessageText.GetString("AsManager")
                struser = ResourceMessageText.GetString("AsUser")
                strRep = ResourceMessageText.GetString("AsRepresentative")
                strFinance = "a Finance"
                strMarketing = "a Marketing"

                If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                    lblUserName.Text = "Welcome " + Session("FirstName") + " " + Session("LastName") + "<br/> You are logged in as "
                    strAdmin = "an Administrator"
                    strMgr = "a Manager"
                    struser = "a User"
                    strRep = "a Representative"
                    strFinance = "a Finance"
                    strMarketing = "a Marketing"
                    strFinanceManager = "a Finance Manager"
                End If


                Select Case Session("RoleGroupID")
                    Case ApplicationSettings.RoleSupplierAdminID
                        lblUserName.Text &= strAdmin
                    Case ApplicationSettings.RoleClientAdminID
                        lblUserName.Text &= strAdmin
                    Case ApplicationSettings.RoleSupplierManagerID
                        lblUserName.Text &= strMgr
                    Case ApplicationSettings.RoleClientManagerID
                        lblUserName.Text &= strMgr
                    Case ApplicationSettings.RoleSupplierUserID
                        lblUserName.Text &= struser
                    Case ApplicationSettings.RoleClientUserID
                        lblUserName.Text &= struser
                    Case ApplicationSettings.RoleSupplierSpecialistID
                        lblUserName.Text &= strSpec
                    Case ApplicationSettings.RoleOWAdminID
                        lblUserName.Text &= strAdmin
                    Case ApplicationSettings.RoleOWRepID
                        lblUserName.Text &= strRep
                    Case ApplicationSettings.RoleOWFinanceID
                        lblUserName.Text &= strFinance
                    Case ApplicationSettings.RoleOWManagerID
                        lblUserName.Text &= strMgr
                    Case ApplicationSettings.RoleOWMarketingID
                        lblUserName.Text &= strMarketing
                    Case ApplicationSettings.RoleFinanceManagerID
                        lblUserName.Text &= strFinanceManager
                End Select

                lblUserName.Text &= ResourceMessageText.GetString("WelcomeLoginLabelEndText")

                Select Case ApplicationSettings.SiteType
                    Case ApplicationSettings.siteTypes.admin
                        lblUserName.Text &= "&nbsp;|&nbsp;<a class='logOutTxt'  href='Logout.aspx'>Logout</a>"

                    Case ApplicationSettings.siteTypes.site
                        lblUserName.Text &= "&nbsp;|&nbsp;<a class='logOutTxt'  href='../Logout.aspx'>Logout</a>"
                End Select





            End If

        Catch ex As Exception
            Response.Write(ex.Message)
            'Response.Write("value of " & Session("RoleGroupID"))
        End Try

    End Sub

End Class