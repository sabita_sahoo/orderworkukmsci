
Option Explicit On

''' <summary>
''' Class to handle the reference user controls fucntionality
''' </summary>
''' <remarks></remarks>
Partial Public Class UCReference
    Inherits System.Web.UI.UserControl

    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs

    Private _companyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            rngWorkDoneDate1.MaximumValue = Date.Today
            rngWorkDoneDate2.MaximumValue = Date.Today
            rngWorkDoneDate3.MaximumValue = Date.Today

            divValidationMain.Visible = False
            'Security.GetPageControls(Me)
            PopulateReference(False)
        End If

    End Sub

    ''' <summary>
    ''' Method to populate references field
    ''' </summary>
    ''' <param name="killcache"></param>
    ''' <remarks></remarks>
    Protected Sub PopulateReference(Optional ByVal killcache As Boolean = False)
        divValidationMain.Visible = False

        Dim ContactId As String = _companyID

        Dim ds As DataSet
        Dim dsRow As DataRow
        Dim i As Integer

        'Fetch the data
        ds = GetContact(False, killcache, contactid)

        i = 1
        For Each dsRow In ds.Tables("ContactReferences").Rows

            If Not IsDBNull(dsRow.Item("Name")) Then
                CType(FindControl("txtReferences" & i & "Name"), TextBox).Text = dsRow.Item("Name")
            End If

            If Not IsDBNull(dsRow.Item("Title")) Then
                CType(FindControl("txtReferences" & i & "JobTitle"), TextBox).Text = dsRow.Item("Title")
            End If

            If Not IsDBNull(dsRow.Item("Company")) Then
                CType(FindControl("txtReferences" & i & "CmpyName"), TextBox).Text = dsRow.Item("Company")
            End If

            If Not IsDBNull(dsRow.Item("Email")) Then
                CType(FindControl("txtReferences" & i & "Email"), TextBox).Text = dsRow.Item("Email")
            End If

            If Not IsDBNull(dsRow.Item("Mobile")) Then
                CType(FindControl("txtReferences" & i & "Mobile"), TextBox).Text = dsRow.Item("Mobile")
            End If

            If Not IsDBNull(dsRow.Item("Phone")) Then
                CType(FindControl("txtReferences" & i & "Phone"), TextBox).Text = dsRow.Item("Phone")
            End If

            If Not IsDBNull(dsRow.Item("WorkDoneDate")) Then
                CType(FindControl("txtReferences" & i & "WorkDone"), TextBox).Text = dsRow.Item("WorkDoneDate")
            End If

            If Not IsDBNull(dsRow.Item("WorkDone")) Then
                CType(FindControl("txtReferences" & i & "WorkDescription"), TextBox).Text = dsRow.Item("WorkDone")
            End If

            i += 1

        Next

    End Sub

    ''' <summary>
    ''' Method to save references data
    ''' </summary>
    ''' <remarks></remarks>
    Protected Function SaveReference() As XSDContacts
        Dim contactid As Integer = _companyID
        Dim dsContacts As New XSDContacts
        Dim i As Integer
        For i = 1 To 3
            Dim nrow As XSDContacts.tblContactsReferencesRow = dsContacts.tblContactsReferences.NewRow
            With nrow
                .ContactId = contactid
                .Name = CType(FindControl("txtReferences" & i & "Name"), TextBox).Text.Trim
                .Title = CType(FindControl("txtReferences" & i & "JobTitle"), TextBox).Text.Trim
                .Company = CType(FindControl("txtReferences" & i & "CmpyName"), TextBox).Text.Trim
                .Email = CType(FindControl("txtReferences" & i & "Email"), TextBox).Text.Trim
                .Phone = CType(FindControl("txtReferences" & i & "Phone"), TextBox).Text.Trim
                .Mobile = CType(FindControl("txtReferences" & i & "Mobile"), TextBox).Text.Trim
                .WorkDoneDate = CType(FindControl("txtReferences" & i & "WorkDone"), TextBox).Text.Trim
                .WorkDone = CType(FindControl("txtReferences" & i & "WorkDescription"), TextBox).Text.Trim
                .Sequence = i
            End With
            dsContacts.tblContactsReferences.Rows.Add(nrow)
        Next
        Return dsContacts
    End Function

    Protected Sub ValidatePage()
        Page.Validate()
        If (Page.IsValid = False) Then
            divValidationMain.Visible = True
        Else
            divValidationMain.Visible = False
            pnlSubmitForm.Visible = False
            pnlConfirm.Visible = True
        End If

    End Sub
    ''' <summary>
    ''' To fetch contacts details which will include the contact references, the dataset is cached.
    ''' </summary>
    ''' <param name="IsMainContact"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetContact(Optional ByVal IsMainContact As Boolean = False, Optional ByVal KillCache As Boolean = False, Optional ByVal contactid As Integer = 0) As DataSet

        Dim cacheKey As String = "Contact" & "-" & CStr(contactid) & "-" & Session.SessionID
        If KillCache = True Then
            Page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If Cache(cacheKey) Is Nothing Then
            ds = CommonFunctions.GetContactsDS(Page, Session.SessionID, contactid, IsMainContact, cacheKey, KillCache)
        Else
            ds = CType(Page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' To handle save action triggerd by the Save LinkButton at the top
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub SaveProfileTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click
        ValidatePage()
    End Sub

    ''' <summary>
    ''' To handle save  action triggerd by the Save LinkButton at the bottom
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub SaveProfileBottom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveBottom.Click
        ValidatePage()
    End Sub

    ''' <summary>
    ''' To handle reset action triggerd by the Save LinkButton at the top
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ResetProfileTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetTop.Click
        ResetProfile()
    End Sub

    ''' <summary>
    ''' To handle reset action triggerd by the Save LinkButton at the bottom
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ResetProfileBottom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetBottom.Click
        ResetProfile()
    End Sub

    ''' <summary>
    ''' Method called by the reset action, hides validation summary and populate form again with earlier saved data
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetProfile()
        'Clear validation message
        divValidationMain.Visible = False
        PopulateReference(False)
    End Sub

    ''' <summary>
    ''' To handle confirm action
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click


        Dim xmlContent As String = SaveReference().GetXml
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)
        Dim success As Boolean
        success = ws.WSContact.UpdateReferences(xmlContent, _companyID)

        If success Then
            Session.Remove("dsContacts")
            Cache.Remove("Contact" & "-" & CStr(_companyID) & "-" & Session.SessionID)

            pnlSubmitForm.Visible = True
            pnlConfirm.Visible = False
            PopulateReference()
        Else
            pnlSubmitForm.Visible = True
            pnlConfirm.Visible = False
            lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
            divValidationMain.Visible = True

        End If


    End Sub

    ''' <summary>
    ''' To handle cancel action on confirm
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlSubmitForm.Visible = True
        pnlConfirm.Visible = False
    End Sub

End Class