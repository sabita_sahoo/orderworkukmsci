Partial Public Class UCMSViewSalesInvoiceUK
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs
    Protected WithEvents lblBankDetails, lblAddress, lblCity, lblPostCode, lblCompNameInvoiceTo As Label
    Protected WithEvents lblListingVAT, lblListingDate, lblListingNetPrice, lblListingTotalPrice, lblStoreIdVal, lblInvoiceTo, lblCompanyName, lblInvoicePo, lblCreditTerms, lblStoreId, lblInvoiceNo, lblInvoiceDate, lblVATOnDiscout, lblTotalAmountLabel As Label
    Protected WithEvents lblDate, lblDescription, lblPrice, lblSubTotal, lblVAT, lblStoreName As Label
    Protected WithEvents lblTotalVAT, lblVATNo, lblAmount, lblTotalAmount, lblAccountName As Label
    Protected WithEvents lblSortCode, lblAccountNo, lblInvoicePoNumber, lbldot As Label
    Protected WithEvents lblListingDesc, lblTelNo, lblTotalVatAsPerSite, lblTotalVatAsPerSite1, lblFaxNo, lblRefNumber, lblRegistartionNo, lblRegistartionNoLine2, lblAccountNumber As Label
    Protected WithEvents rptList As Repeater
    Protected WithEvents pnlListingFee As Panel
    Dim accountNumber As Integer = 0



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim ds As DataSet = ws.WSFinance.MS_GetSalesAdviceDetails(Request("BizDivId"), Request("invoiceNo"))
            Dim totalAmount As Decimal = 0
            Dim totalVAT As Decimal = 0
            Dim totalNetAmount As Decimal = 0
            Dim totalDiscount As Decimal = 0
            Dim totalDiscountPer As Decimal = 0
            Dim totalAmountAfterDesc As Decimal = 0
            Dim VatOnDiscount As Decimal = 0
            Dim drow As DataRow



            If ds.Tables("tblOWAddress").Rows.Count > 0 Then
                'Company Name
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("CompanyName"))) Then
                    lblCompanyName.Text = ds.Tables("tblOWAddress").Rows(0)("CompanyName")
                End If
                'Address
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Address"))) Then
                    lblAddress.Text = ds.Tables("tblOWAddress").Rows(0)("Address")
                End If

                'OW city
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("City"))) Then
                    lblCity.Text = ds.Tables("tblOWAddress").Rows(0)("City") & ","
                End If

                'OW post code
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("PostCode"))) Then
                    lblPostCode.Text = ds.Tables("tblOWAddress").Rows(0)("PostCode")
                End If


                'OW Phone
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Phone"))) Then
                    lblTelNo.Text = ds.Tables("tblOWAddress").Rows(0)("Phone")
                End If
                'OW Fax
                'If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Fax"))) Then
                '    lblFaxNo.Text = ds.Tables("tblOWAddress").Rows(0)("Fax")
                'End If
            End If


            If ds.Tables("tblBillingAddress").Rows.Count > 0 Then
                'Invoice To
                'If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("StoreName"))) Then
                '    lblStoreName.Text = ds.Tables("tblBillingAddress").Rows(0)("StoreName")
                'End If
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("CompanyName"))) Then
                    lblCompNameInvoiceTo.Text = ds.Tables("tblBillingAddress").Rows(0)("CompanyName")
                End If

                'Address Invoice to
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("Address"))) Then
                    If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("City"))) Then
                        If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("PostCode"))) Then
                            lblInvoiceTo.Text = ds.Tables("tblBillingAddress").Rows(0)("Address") & "<br>" & ds.Tables("tblBillingAddress").Rows(0)("City")
                            If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("State"))) Then
                                If ds.Tables("tblBillingAddress").Rows(0)("State") <> "" Then
                                    lblInvoiceTo.Text += "<br>" & ds.Tables("tblBillingAddress").Rows(0)("State") & ", " & ds.Tables("tblBillingAddress").Rows(0)("PostCode")
                                Else
                                    lblInvoiceTo.Text += "<br>" & ds.Tables("tblBillingAddress").Rows(0)("PostCode")
                                End If
                            Else
                                lblInvoiceTo.Text += "<br>" & ds.Tables("tblBillingAddress").Rows(0)("PostCode")
                            End If
                        End If
                    End If
                End If

            End If

            If ds.Tables("tblAdviceDetails").Rows.Count > 0 Then
                'Credit Terms
                If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("CreditTerms"))) Then
                    lblCreditTerms.Text = ds.Tables("tblAdviceDetails").Rows(0)("CreditTerms")
                End If

                'Invoice No.
                If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceNo"))) Then
                    lblInvoiceNo.Text = ds.Tables("tblAdviceDetails").Rows(0)("InvoiceNo")
                    lblRefNumber.Text = ds.Tables("tblAdviceDetails").Rows(0)("InvoiceNo")
                End If

                'Invoice Date
                If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceDate"))) Then
                    lblInvoiceDate.Text = Strings.FormatDateTime(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceDate").ToString(), DateFormat.ShortDate)
                End If

                'Account no
                If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("AccountNo"))) Then
                    lblAccountNo.Text = ds.Tables("tblAdviceDetails").Rows(0)("AccountNo")
                    accountNumber = ds.Tables("tblAdviceDetails").Rows(0)("AccountNo")
                End If

                'Invoice PO
                If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("InvoicePO"))) Then
                    lblInvoicePoNumber.Text = ds.Tables("tblAdviceDetails").Rows(0)("InvoicePO")
                End If

            End If


            If ds.Tables("tblOrderWorkDetails").Rows.Count > 0 Then
                'Vat Number
                If Not (IsDBNull(ds.Tables("tblOrderWorkDetails").Rows(0)("VATRegNo"))) Then
                    lblVATNo.Text = ds.Tables("tblOrderWorkDetails").Rows(0)("VATRegNo")
                End If

                'Account Name
                If Not (IsDBNull(ds.Tables("tblOrderWorkDetails").Rows(0)("BankAccountName"))) Then
                    lblAccountName.Text = Encryption.Decrypt(ds.Tables("tblOrderWorkDetails").Rows(0)("BankAccountName"))
                End If

                'Sort Code
                If Not (IsDBNull(ds.Tables("tblOrderWorkDetails").Rows(0)("BankSortCode"))) Then
                    lblSortCode.Text = Encryption.Decrypt(ds.Tables("tblOrderWorkDetails").Rows(0)("BankSortCode"))
                End If

                'Account Number
                If Not (IsDBNull(ds.Tables("tblOrderWorkDetails").Rows(0)("BankAccountNo"))) Then
                    lblAccountNumber.Text = Encryption.Decrypt(ds.Tables("tblOrderWorkDetails").Rows(0)("BankAccountNo"))
                End If


                If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                    'Registration number
                    lblRegistartionNo.Text = "Registered in England"

                    If Not (IsDBNull(ds.Tables("tblOrderWorkDetails").Rows(0)("CompanyRegNo"))) Then
                        lblRegistartionNoLine2.Text = "No.  " & ds.Tables("tblOrderWorkDetails").Rows(0)("CompanyRegNo")
                    End If
                End If

                If Not (IsDBNull(ds.Tables("tblOrderWorkDetails").Rows(0)("BankAccountNo"))) Then
                    lblAccountNumber.Text = Encryption.Decrypt(ds.Tables("tblOrderWorkDetails").Rows(0)("BankAccountNo"))
                End If
                If (accountNumber = ApplicationSettings.DixonRetailForm Or accountNumber = ApplicationSettings.Khharrods Or accountNumber = ApplicationSettings.khbusiness) Then
                    lblStoreId.Visible = True
                    lblInvoicePo.Visible = True
                    lblInvoicePoNumber.Visible = True
                    lblStoreName.Visible = True
                    lbldot.Visible = True
                Else
                    lblStoreId.Visible = False
                    lblInvoicePo.Visible = False
                    lblInvoicePoNumber.Visible = False
                    lbldot.Visible = False
                    lblStoreName.Visible = False
                End If

            End If
            If ds.Tables("tblWODetails").Rows.Count > 0 Then
                ds.Tables("tblWODetails").DefaultView.Sort = "CloseDate"
                rptList.DataSource = ds.Tables("tblWODetails").DefaultView
                rptList.DataBind()

                For Each drow In ds.Tables("tblWODetails").Rows
                    totalAmount += drow("Total")
                    totalVAT += drow("VAT")
                    totalNetAmount += drow("InvoiceNetAmnt")
                    totalDiscountPer = drow("DiscountPer")
                    totalDiscount += drow("Discount")
                    VatOnDiscount += drow("VATOnDiscount")
                Next

                lblSubTotal.Text = FormatCurrency(totalNetAmount, 2, TriState.True, TriState.True, TriState.False)
                lblTotalVAT.Text = FormatCurrency(totalVAT, 2, TriState.True, TriState.True, TriState.False)
                lblTotalAmount.Text = FormatCurrency(totalAmount, 2, TriState.True, TriState.True, TriState.False)
                If totalDiscount > 0 Then
                    trTotalADiscount.Visible = True
                    'trVatDiscount.Visible = True
                    trDiscount.Visible = True
                    lblDiscount.Text = FormatCurrency(totalDiscount, 2, TriState.True, TriState.True, TriState.False)
                    lblDiscountPer.Text = FormatNumber(totalDiscountPer, 2, TriState.True, TriState.True, TriState.False)
                    lblTotalAmountWithDisc.Text = FormatCurrency((totalAmount - totalDiscount - VatOnDiscount), 2, TriState.True, TriState.True, TriState.False)

                    lblVATOnDiscout.Text = FormatCurrency((totalVAT - VatOnDiscount), 3, TriState.True, TriState.True, TriState.False)
                Else
                    trTotalADiscount.Visible = False
                    'trVatDiscount.Visible = False
                    trDiscount.Visible = False
                End If

            End If
            If ds.Tables("tblBankDetails").Rows.Count > 0 Then
                If Not (IsDBNull(ds.Tables("tblBankDetails").Rows(0)("CompanyName"))) Then
                    lblBankDetails.Text = ds.Tables("tblBankDetails").Rows(0)("CompanyName")
                End If
                If Not (IsDBNull(ds.Tables("tblBankDetails").Rows(0)("Address"))) Then
                    lblBankDetails.Text &= ", " & ds.Tables("tblBankDetails").Rows(0)("Address")
                End If
                If Not (IsDBNull(ds.Tables("tblBankDetails").Rows(0)("City"))) Then
                    lblBankDetails.Text &= ", " & ds.Tables("tblBankDetails").Rows(0)("City")
                End If
                If Not (IsDBNull(ds.Tables("tblBankDetails").Rows(0)("PostCode"))) Then
                    lblBankDetails.Text &= ", " & ds.Tables("tblBankDetails").Rows(0)("PostCode")
                End If
            End If
            If ApplicationSettings.Country = ApplicationSettings.CountryDE Then
                lblBankDetails.Text = "Deutche Bank GmbH"
            End If

            Dim dsRow As DataRow
            Dim strWOIDs As String
            strWOIDs = ""
            If Not IsDBNull(ds.Tables("tblListingFee")) Then
                If ds.Tables("tblListingFee").Rows.Count > 0 Then
                    pnlListingFee.Visible = True
                    If Not (IsDBNull(ds.Tables("tblListingFee").Rows(0)("invoicedate"))) Then
                        lblListingDate.Text = ds.Tables("tblListingFee").Rows(0)("invoicedate")
                    End If
                    If Not IsDBNull(ds.Tables("tblListingFee")) Then
                        If ds.Tables("tblWorkOrderList").Rows.Count > 0 Then
                            For Each dsRow In ds.Tables("tblWorkOrderList").Rows
                                If strWOIDs = "" Then
                                    strWOIDs = dsRow.Item("WorkOrderId")
                                Else
                                    strWOIDs += "," + dsRow.Item("WorkOrderId")
                                End If
                            Next

                            lblListingDesc.Text = "Listing Fee for Workorder Ids - " + strWOIDs
                        End If
                    End If
                    If Not (IsDBNull(ds.Tables("tblListingFee").Rows(0)("WOVal"))) Then
                        lblListingNetPrice.Text = FormatCurrency(ds.Tables("tblListingFee").Rows(0)("WOVal"), 2, TriState.True, TriState.True, TriState.False)
                        totalNetAmount += ds.Tables("tblListingFee").Rows(0)("WOVal")
                    End If
                    'Added by pankaj malav to cater for discount amount in listing fees
                    If Not (IsDBNull(ds.Tables("tblListingFee").Rows(0)("Discount"))) Then
                        totalDiscount += ds.Tables("tblListingFee").Rows(0)("Discount")
                    End If
                    If Not (IsDBNull(ds.Tables("tblListingFee").Rows(0)("VATOnDiscount"))) Then
                        VatOnDiscount += ds.Tables("tblListingFee").Rows(0)("VATOnDiscount")
                    End If

                    If Not (IsDBNull(ds.Tables("tblListingFee").Rows(0)("VAT"))) Then
                        lblListingVAT.Text = FormatCurrency(ds.Tables("tblListingFee").Rows(0)("VAT"), 2, TriState.True, TriState.True, TriState.False)
                        totalVAT += ds.Tables("tblListingFee").Rows(0)("VAT")
                    End If
                    If Not (IsDBNull(ds.Tables("tblListingFee").Rows(0)("Total"))) Then
                        lblListingTotalPrice.Text = FormatCurrency(ds.Tables("tblListingFee").Rows(0)("Total"), 2, TriState.True, TriState.True, TriState.False)
                        totalAmount += ds.Tables("tblListingFee").Rows(0)("Total")
                    End If
                Else
                    pnlListingFee.Visible = False
                End If
            End If

            If Not IsDBNull(ds.Tables("tblFinanceFields")) Then
                If (ds.Tables("tblFinanceFields").Rows.Count > 0) Then
                    If (ds.Tables("tblFinanceFields").Rows(0).Item("Field1Enabled") = "True") Then
                        lblField1.Text = ds.Tables("tblFinanceFields").Rows(0).Item("Field1Label").ToString()
                        lblFieldValue1.Text = ds.Tables("tblFinanceFields").Rows(0).Item("Field1Value").ToString()
                        trField1.Visible = True
                    End If

                    If (ds.Tables("tblFinanceFields").Rows(0).Item("Field2Enabled") = "True") Then
                        lblField2.Text = ds.Tables("tblFinanceFields").Rows(0).Item("Field2Label").ToString()
                        lblFieldValue2.Text = ds.Tables("tblFinanceFields").Rows(0).Item("Field2Value").ToString()
                        trField2.Visible = True
                    End If

                    If (ds.Tables("tblFinanceFields").Rows(0).Item("Field3Enabled") = "True") Then
                        lblField3.Text = ds.Tables("tblFinanceFields").Rows(0).Item("Field3Label").ToString()
                        lblFieldValue3.Text = ds.Tables("tblFinanceFields").Rows(0).Item("Field3Value").ToString()
                        trField3.Visible = True
                    End If

                    If (ds.Tables("tblFinanceFields").Rows(0).Item("Field4Enabled") = "True") Then
                        lblField4.Text = ds.Tables("tblFinanceFields").Rows(0).Item("Field4Label").ToString()
                        lblFieldValue4.Text = ds.Tables("tblFinanceFields").Rows(0).Item("Field4Value").ToString()
                        trField4.Visible = True
                    End If
                End If
            End If

            'outside the last if condition
            lblSubTotal.Text = FormatCurrency(totalNetAmount, 2, TriState.True, TriState.True, TriState.False)
            lblTotalVAT.Text = FormatCurrency(totalVAT, 2, TriState.True, TriState.True, TriState.False)
            lblTotalAmount.Text = FormatCurrency(totalAmount, 2, TriState.True, TriState.True, TriState.False)
            lblVATOnDiscout.Text = FormatCurrency((totalVAT - VatOnDiscount), 2, TriState.True, TriState.True, TriState.False)

            If totalDiscount > 0 Then
                trTotalADiscount.Visible = True
                'trVatDiscount.Visible = True
                trDiscount.Visible = True
                lblDiscount.Text = FormatCurrency(totalDiscount, 2, TriState.True, TriState.True, TriState.False)
                lblTotalAmountWithDisc.Text = FormatCurrency((totalAmount - totalDiscount - VatOnDiscount), 2, TriState.True, TriState.True, TriState.False)
                lblVATOnDiscout.Text = FormatCurrency((totalVAT - VatOnDiscount), 3, TriState.True, TriState.True, TriState.False)

                lblTotalAmountLabel.Text = "Payment after 30 days - Total Amount"
            Else
                trTotalADiscount.Visible = False
                'trVatDiscount.Visible = False
                trDiscount.Visible = False
            End If


            Dim vatRate As String

            vatRate = ApplicationSettings.VATPercentage(Strings.FormatDateTime(lblInvoiceDate.Text.Trim, DateFormat.ShortDate))
            lblTotalVatAsPerSite.Text = vatRate
            lblTotalVatAsPerSite1.Text = vatRate

        End If
    End Sub

    Private Sub rptList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblStoreIdVal As Label = DirectCast(e.Item.FindControl("lblStoreIdVal"), Label)

            If (accountNumber = ApplicationSettings.DixonRetailForm Or accountNumber = ApplicationSettings.Khharrods Or accountNumber = ApplicationSettings.khbusiness) Then
                lblStoreIdVal.Visible = True
            Else
                lblStoreIdVal.Visible = False
            End If

        End If
    End Sub
End Class
