Partial Public Class UCMSSpecialistsForm
    Inherits System.Web.UI.UserControl


    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs

    Private _companyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property

    Private _bizDivID As Integer
    Public Property BizDivID() As Integer
        Get
            Return _bizDivID
        End Get
        Set(ByVal value As Integer)
            _bizDivID = value
        End Set
    End Property


    Private _userID As Integer
    Public Property UserID() As Integer
        Get
            Return _userID
        End Get
        Set(ByVal value As Integer)
            _userID = value
        End Set
    End Property

    Private _classID As Integer
    Public Property ClassID() As Integer
        Get
            Return _classID
        End Get
        Set(ByVal value As Integer)
            _classID = value
        End Set
    End Property

    Private _roleGroupID As Integer
    Public Property RoleGroupID() As Integer
        Get
            Return _roleGroupID
        End Get
        Set(ByVal value As Integer)
            _roleGroupID = value
        End Set
    End Property
    Protected WithEvents CRBCheckedEnhanced As RadioButton
    Protected WithEvents txtDOB As TextBox
    Protected WithEvents UCFileUpload7 As Admin.UCFileUpload_Outer
    Protected WithEvents hdnImageURL As HiddenField


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            'btnCancelBottom.HRef = btnCancelTop.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            divValidationMain.Visible = False
            'hdnAttachmentDisplayPath.Value = ApplicationSettings.AttachmentDisplayPath(1).ToString
            hdnAttachmentDisplayPath.Value = ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/"
            If Not IsNothing(Request("sender")) Then
                If (Request("sender") = "UnapprovedEngineers") Then
                    If Not IsNothing(Request("ClassId")) Then
                        Session("ContactClassID") = Request("ClassId")
                    End If
                End If
            End If

            'Poonam modified on 30/12/2015 - Task - OA-112 : OA - Add Emergency Contact fields in User detail page
            If ((Request("classid") = ApplicationSettings.RoleSupplierID) Or ((Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleSupplierID)) Then
                pnlEmergencyContact.Visible = True
                'Populating relationship type
                CommonFunctions.PopulateRelationType(Page, ddlRelationshipType)
                pnlEditVehicleType.Visible = True
                'Populate Vehicle Type
                CommonFunctions.PopulateVehicleTypeEdit(Page, cblVehicleTypeEdit)

            Else

                pnlEmergencyContact.Visible = False
            End If

            Dim CompanyId As String = _companyID
            UCAccreditations.Type = "EngineerAccreditations"
            'returns the filename of the page onto which the control is added
            Dim filepath As String = CType(CType(sender, UCMSSpecialistsForm).Request, System.Web.HttpRequest).Path
            filepath = filepath.Substring(filepath.LastIndexOf("/") + 1, filepath.Length - (filepath.LastIndexOf("/") + 1))
            ViewState("page") = filepath

            Select Case filepath
                Case "MyProfile.aspx"
                    ViewState("ContactID") = _userID
                    lblTitle.Text = ResourceMessageText.GetString("MyProfile")
                    tdStatusValue.Visible = False
                    tdStatusLabel.Visible = False
                    'ddlUserType.Enabled = False
                    tdEnableLogin.Visible = False

                    'Call Populate method
                    PopulateChangeLogin(False)

                Case "SpecialistsForm.aspx"
                    Select Case ApplicationSettings.SiteType
                        Case ApplicationSettings.siteTypes.admin
                            'poonam modified on 6/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                            btnAddLocation.PostBackUrl = "~/LocationForm.aspx?" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                        Case ApplicationSettings.siteTypes.site
                            btnAddLocation.PostBackUrl = "~/SecurePages/LocationForm.aspx?" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
                    End Select


                    lblTitle.Text = ResourceMessageText.GetString("Users")
                    If Request("ContactId") Is Nothing Or Request("ContactId") = "" Then
                        ddlStatus.Visible = False
                        tdStatusLabel.Visible = False
                        ViewState("ContactID") = 0
                        ViewState("mode") = "add"
                    Else
                        tdStatusLabel.Visible = True
                        ddlStatus.Visible = True
                        ViewState("ContactID") = Request("ContactId")
                        ViewState("mode") = "edit"
                    End If
                    If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                        ChkEmailVerified.Visible = True
                        chkIsDeleted.Visible = True
                    End If

                    'If ChkEnableLogin.Checked = True Then
                    '    pnlEnableDisableLogin.Visible = True
                    'Else
                    '    pnlEnableDisableLogin.Visible = False
                    'End If

                    tblLoginInfo.Visible = False
                    tblEmail.Visible = False

            End Select

            'Populate Communication Preferences DropDownlist
            CommonFunctions.PopulateCommunicationPref(Page, ddlCommunication)

            'Populate Travel Distance DropDownlist
            CommonFunctions.PopulateDistance(Page, ddlTravelDistance)

            'security questions
            CommonFunctions.PopulateSecurityQues(Page, ddlQuestion)

            'Populating user type drop down list
            CommonFunctions.PopulateUserType(Page, ddlUserType, _classID)

            'Populating location drop down list
            CommonFunctions.PopulateLocationsofCompany(Page, ddlLocation, CompanyId)

            'Populating user status of the user
            CommonFunctions.PopulateUserStatus(Page, ddlStatus)

            ''Populating Certification / qualification or Non-MS certification for MS user
            'Dim dvList As DataView
            'dvList = CommonFunctions.GetCertQual(Page, _bizDivID)
            'lblCert.Text = ResourceMessageText.GetString("CertsAndQuals",ApplicationSettings.MessageSource.library)            '   "Certifications/Qualifications  "            '   
            'If _bizDivID = ApplicationSettings.SFUKBizDivId Then 
            '    dvList.RowFilter = "Description = 'Non Microsoft'"
            '    lblCert.Text = ResourceMessageText.GetString("NonMSAcc")            '   "Non Microsoft Accreditations"       '   
            'End If
            'dvList.Sort = "Name"

            'show respective accreds section
            If _bizDivID = ApplicationSettings.SFUKBizDivId Then
                pnlAccreditationsInfo.Visible = True
                pnlAccreditationsInfoOthers.Visible = False
            Else
                pnlAccreditationsInfo.Visible = False
                pnlAccreditationsInfoOthers.Visible = True
            End If

            If (ViewState("mode") = "edit") Then
                PopulateProfile(True)
                If ((Request("classid") = ApplicationSettings.RoleSupplierID) Or ((Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleSupplierID)) Then
                    'OD-804 - .Net - Skills, Accreditation and Location coverage changes to make it Live
                    pnlContactProfileBeta.Visible = True
                    chkRecordVerifed.Visible = True
                End If

            Else
                PopulateAddUserInfo()
                tdPhotoUpload.Style.Add("display", "none")
                pnlEnggRates.Style.Add("display", "none")
            End If


            hdnContactId.Value = ViewState("ContactID")


        End If
        'If IsPostBack Then
        '    If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
        '        If ddlStatus.Text = "1" Then
        '            ChkEnableLogin.Checked = True
        '            chkIsDeleted.Checked = False
        '        ElseIf ddlStatus.Text = "2" Then
        '            ChkEnableLogin.Checked = False
        '            chkIsDeleted.Checked = True
        '        End If
        '    End If
        'End If
        If ApplicationSettings.SendIphoneNotification = "False" Then
            IphoneTdText.Style.Add("display", "none")
            IphoneTdTextBox.Style.Add("display", "none")
        End If

        'OD-804 - .Net - Skills, Accreditation and Location coverage changes to make it Live
        Page.Session("ContactIDForSkills") = ViewState("ContactID")

        UCComments.ContactID = ViewState("ContactID")
        UCComments.CompanyID = _companyID
        UCComments.BizDivID = ApplicationSettings.OWUKBizDivId
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        btnCancelBottom.HRef = "~/Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        btnCancelTop.HRef = "~/Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        If Request("ContactId") Is Nothing Or Request("ContactId") = "" Then
            btnResetTop.HRef = "~/SpecialistsForm.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&sender=" & Request("sender") & "&classId=" & Request("classId")
            btnResetBottom.HRef = "~/SpecialistsForm.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&sender=" & Request("sender") & "&classId=" & Request("classId")
        Else
            btnResetTop.HRef = "~/SpecialistsForm.aspx?CompanyID=" & Request("CompanyID") & "&ContactId=" & Request("ContactId") & "&bizDivId=" & Request("bizDivId") & "&sender=" & Request("sender") & "&classId=" & Request("classId")
            btnResetBottom.HRef = "~/SpecialistsForm.aspx?CompanyID=" & Request("CompanyID") & "&ContactId=" & Request("ContactId") & "&bizDivId=" & Request("bizDivId") & "&sender=" & Request("sender") & "&classId=" & Request("classId")
        End If

        If Not IsNothing(Request("sender")) Then
            If (Request("sender") = "UnapprovedEngineers") Then
                btnCancelBottom.HRef = "~/UnapprovedEngineers.aspx"
                btnCancelTop.HRef = "~/UnapprovedEngineers.aspx"
            End If
        End If

        If CRBCheckedNo.Checked = True Then
            rngDate.Enabled = False
        Else
            rngDate.Enabled = True
        End If


    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        'If Session("UserId") = ViewState("ContactID") Then


        If ChkEnableLogin.Checked = True Then
            pnlEnableDisableLogin.Visible = True
        Else
            pnlEnableDisableLogin.Visible = False
        End If

        'ddlUserType.Enabled = False


        'Else
        'pnlEnableDisableLogin.Visible = False
        'rqddlQuestion.Enabled = False

        'End If
    End Sub


    ''' <summary>
    ''' Procedure to load a new form or populate it with the details of the contact from query string
    ''' </summary>
    ''' <param name="killcache"></param>
    ''' <remarks></remarks>
    Protected Sub PopulateProfile(Optional ByVal killcache As Boolean = False)
        Dim strAppend As String
        Select Case _bizDivID
            Case ApplicationSettings.OWUKBizDivId
                strAppend = "OW"
            Case ApplicationSettings.SFUKBizDivId
                strAppend = "MS"
            Case ApplicationSettings.OWDEBizDivId
                strAppend = "DE"
        End Select

        CRBCheckedEnhanced.Checked = False
        CRBCheckedYes.Checked = False
        CRBCheckedNo.Checked = False

        rdoEU.Checked = False
        rdoUK.Checked = False
        rdoOthers.Checked = False

        Dim contactid As Integer = ViewState("ContactID")

        Dim ds As DataSet
        Dim dsView As DataView

        ds = GetContact(False, killcache, contactid)

        InitializeAttachments()

        If ds.Tables("Contacts").Rows.Count > 0 Then
            lblContactId.Text = "Contact ID&nbsp;&nbsp;&nbsp;" & ds.Tables("Contacts").Rows(0)("AccountNo")
            PanelContactComment.Visible = True
            If chkIsDeleted.Visible = True Then
                If ds.Tables("Contacts").Rows.Count > 0 Then
                    chkIsDeleted.Checked = ds.Tables("Contacts").Rows(0)("IsDeleted")

                End If
            End If

            If ds.Tables("Contacts").Rows.Count > 0 Then
                'Store the version number in session for Contacts
                CommonFunctions.StoreVerNum(ds.Tables("Contacts"))
                txtIphonePin.Text = ds.Tables("Contacts").Rows(0)("iPhonePin")
            End If

            If ClassID = ApplicationSettings.RoleSupplierID Then

                IphoneTdText.Style.Add("display", "block")
                IphoneTdTextBox.Style.Add("display", "block")
                RegExpreIphonePin.Enabled = True
                'CRBChecked
                If Not IsDBNull(ds.Tables("Contacts").Rows(0)("CRBChecked")) Then
                    If ds.Tables("Contacts").Rows(0)("CRBChecked") Then
                        If Not IsDBNull(ds.Tables("Contacts").Rows(0)("CriminalRecordChecks")) Then
                            If ds.Tables("Contacts").Rows(0)("CriminalRecordChecks") <> "" Then
                                If ds.Tables("Contacts").Rows(0)("CriminalRecordChecks") > 100 Then
                                    CRBCheckedEnhanced.Checked = True
                                    tblCRBListbox.Style.Add("display", "inline-table")
                                    DivCRBCheckCalender.Style.Add("display", "inline")
                                    DivCRBCheckFileUpload.Style.Add("display", "inline")
                                End If
                            Else
                                CRBCheckedYes.Checked = True
                                tblCRBListbox.Style.Add("display", "inline-table")
                                DivCRBCheckCalender.Style.Add("display", "inline")
                                DivCRBCheckFileUpload.Style.Add("display", "inline")
                            End If
                        Else
                            CRBCheckedYes.Checked = True
                            tblCRBListbox.Style.Add("display", "inline-table")
                            DivCRBCheckCalender.Style.Add("display", "inline")
                            DivCRBCheckFileUpload.Style.Add("display", "inline")
                        End If
                    Else
                        CRBCheckedNo.Checked = True
                        tblCRBListbox.Style.Add("display", "none")
                        DivCRBCheckCalender.Style.Add("display", "none")
                        DivCRBCheckFileUpload.Style.Add("display", "none")
                    End If
                Else
                    CRBCheckedYes.Checked = False
                    CRBCheckedNo.Checked = False
                    tblCRBListbox.Style.Add("display", "none")
                    DivCRBCheckCalender.Style.Add("display", "none")
                    DivCRBCheckFileUpload.Style.Add("display", "none")
                End If
                If Not IsDBNull(ds.Tables("Contacts").Rows(0)("PassportType")) Then
                    If ds.Tables("Contacts").Rows(0)("PassportType") = "UK" Then
                        rdoUK.Checked = True
                    ElseIf ds.Tables("Contacts").Rows(0)("PassportType") = "EU" Then
                        rdoEU.Checked = True
                    ElseIf ds.Tables("Contacts").Rows(0)("PassportType") = "Others" Then
                        rdoOthers.Checked = True
                    End If
                End If
                'populate vehicle type
                If Not IsDBNull(ds.Tables("ContactSettings").DefaultView.RowFilter = "AttributeLabel='VehicleType'") Then
                    Dim i As Integer = 0
                    For Each item As DataRow In ds.Tables("ContactSettings").Rows
                        If (Convert.ToString(ds.Tables("ContactSettings").Rows(i).Item("AttributeLabel")) = "VehicleType") Then
                            For Each viewLst As ListItem In cblVehicleTypeEdit.Items
                                If viewLst.Value = Convert.ToString(ds.Tables("ContactSettings").Rows(i).Item("AttributeValue")) Then
                                    viewLst.Selected = True
                                End If
                            Next
                        End If
                        i += 1
                    Next
                End If
                If Not IsDBNull(ds.Tables("Contacts").Rows(0)("VehicleRegNo")) Then
                    txtVehicleRegNo.Text = (ds.Tables("Contacts").Rows(0)("VehicleRegNo"))
                End If

                'UKSecClearance
                If Not IsDBNull(ds.Tables("Contacts").Rows(0)("UKSecurityClearance")) Then
                    If ds.Tables("Contacts").Rows(0)("UKSecurityClearance") Then
                        UKSecurityClearanceYes.Checked = True
                        tblSecuCheckListbox.Style.Add("display", "inline")
                        DivUKSecuCalender.Style.Add("display", "inline")
                        DivUKSecuFileUpload.Style.Add("display", "inline")
                    Else
                        UKSecurityClearanceNo.Checked = True
                        tblSecuCheckListbox.Style.Add("display", "none")
                        DivUKSecuCalender.Style.Add("display", "none")
                        DivUKSecuFileUpload.Style.Add("display", "none")
                    End If
                Else
                    UKSecurityClearanceYes.Checked = False
                    UKSecurityClearanceNo.Checked = True
                    tblSecuCheckListbox.Style.Add("display", "none")
                    DivUKSecuCalender.Style.Add("display", "none")
                    DivUKSecuFileUpload.Style.Add("display", "none")
                End If
                'CSCS
                If Not IsDBNull(ds.Tables("Contacts").Rows(0)("CSCS")) Then
                    If ds.Tables("Contacts").Rows(0)("CSCS") Then
                        CSCSYes.Checked = True
                        DivCSCSCalender.Style.Add("display", "inline")
                        DivCSCSFileUpload.Style.Add("display", "inline")
                    Else
                        CSCSNo.Checked = True
                        DivCSCSCalender.Style.Add("display", "none")
                        DivCSCSFileUpload.Style.Add("display", "none")
                    End If
                Else
                    CSCSYes.Checked = False
                    CSCSNo.Checked = False
                    DivCSCSCalender.Style.Add("display", "none")
                    DivCSCSFileUpload.Style.Add("display", "none")
                End If

                If Not IsDBNull(ds.Tables("Contacts").Rows(0)("UKRightToWorkExpDate")) Then
                    txtVisaExpDate.Text = ds.Tables("Contacts").Rows(0)("UKRightToWorkExpDate")
                End If

                If Not IsDBNull(ds.Tables("Contacts").Rows(0)("DOB")) Then
                    txtDOB.Text = (ds.Tables("Contacts").Rows(0)("DOB"))
                    If txtDOB.Text = "01/01/1900" Then
                        txtDOB.Text = ""
                    End If
                End If

                If Not IsDBNull(ds.Tables("Contacts").Rows(0)("DBSExpiryDate")) Then
                    txtDBSExpiryDate.Text = ds.Tables("Contacts").Rows(0)("DBSExpiryDate")
                End If

                If txtDBSExpiryDate.Text = "01/01/1900" Then
                    txtDBSExpiryDate.Text = ""
                End If


                If Not IsDBNull(ds.Tables("Contacts").Rows(0)("DisclosureNumber")) Then
                    txtDisclosureNumber.Text = ds.Tables("Contacts").Rows(0)("DisclosureNumber")
                End If

                If Not IsDBNull(ds.Tables("Contacts").Rows(0)("UKSecurityClearanceIssueDate")) Then
                    txtUKSecuDate.Text = ds.Tables("Contacts").Rows(0)("UKSecurityClearanceIssueDate")
                End If

                If Not IsDBNull(ds.Tables("Contacts").Rows(0)("CSCSExpDate")) Then
                    txtCSCSDate.Text = ds.Tables("Contacts").Rows(0)("CSCSExpDate")
                End If

                'Populating Vetting History
                If (ds.Tables(ds.Tables.Count - 1).Rows.Count > 0) Then
                    tblVettingHistory.Visible = True
                    Dim dvVettingHistory As DataView = ds.Tables(ds.Tables.Count - 1).Copy.DefaultView
                    DLVettingHistory.DataSource = dvVettingHistory
                    DLVettingHistory.DataBind()
                Else
                    tblVettingHistory.Visible = False
                End If

                'Populating Criminal Record Selected listbox
                'If (ds.Tables(ds.Tables.Count - 3).Rows.Count > 0) Then
                '    Dim dvCriminalRecordSel As DataView = ds.Tables(ds.Tables.Count - 3).Copy.DefaultView
                '    hdnCriminalRecordSelected.Value = ""
                '    Dim dtt As DataTable = dvCriminalRecordSel.ToTable
                '    For Each dr As DataRow In dtt.Rows
                '        If (hdnCriminalRecordSelected.Value = "") Then
                '            hdnCriminalRecordSelected.Value = dr("StandardID")
                '        Else
                '            hdnCriminalRecordSelected.Value = hdnCriminalRecordSelected.Value & "," & dr("StandardID")
                '        End If
                '    Next
                '    listCriminalRecordSel.DataSource = dvCriminalRecordSel
                '    listCriminalRecordSel.DataBind()
                'End If

                'Populating SecurityCheck Selected listbox
                If (ds.Tables(ds.Tables.Count - 2).Rows.Count > 0) Then
                    Dim dvSecuCheckSelected As DataView = ds.Tables(ds.Tables.Count - 2).Copy.DefaultView
                    hdnSecuCheckSelected.Value = ""
                    Dim dtt As DataTable = dvSecuCheckSelected.ToTable
                    For Each dr As DataRow In dtt.Rows
                        If (hdnSecuCheckSelected.Value = "") Then
                            hdnSecuCheckSelected.Value = dr("StandardID")
                        Else
                            hdnSecuCheckSelected.Value = hdnSecuCheckSelected.Value & "," & dr("StandardID")
                        End If
                    Next
                    listSecurityCheckSel.DataSource = dvSecuCheckSelected
                    listSecurityCheckSel.DataBind()
                End If

                'Populating Criminal Record listbox 
                Dim dsCriminalRecord As DataSet
                dsCriminalRecord = CommonFunctions.GetStandards(Page, "CRBCheck")
                If dsCriminalRecord.Tables(0).Rows.Count > 0 Then
                    If (hdnCriminalRecordSelected.Value Is Nothing Or hdnCriminalRecordSelected.Value = "") Then
                        hdnCriminalRecordSelected.Value = "0"
                    End If
                    Dim dvCriminalRecord As DataView = dsCriminalRecord.Tables(0).Copy.DefaultView
                    dvCriminalRecord.RowFilter = "StandardID NOT IN (" & hdnCriminalRecordSelected.Value & ")"
                    dvCriminalRecord.Sort = "StandardValue"
                    listCriminalRecord.DataSource = dvCriminalRecord
                    listCriminalRecord.DataBind()
                End If
                'Populating SecurityCheck listbox 
                Dim dsSecurityCheck As DataSet
                dsSecurityCheck = CommonFunctions.GetStandards(Page, "SecurityCheck")
                If dsSecurityCheck.Tables(0).Rows.Count > 0 Then
                    If (hdnSecuCheckSelected.Value Is Nothing Or hdnSecuCheckSelected.Value = "") Then
                        hdnSecuCheckSelected.Value = "0"
                    End If
                    Dim dvSecurityCheck As DataView = dsSecurityCheck.Tables(0).Copy.DefaultView
                    dvSecurityCheck.RowFilter = "StandardID NOT IN (" & hdnSecuCheckSelected.Value & ")"
                    dvSecurityCheck.Sort = "StandardValue"
                    listSecurityCheck.DataSource = dvSecurityCheck
                    listSecurityCheck.DataBind()
                End If

            Else
                IphoneTdText.Style.Add("display", "none")
                IphoneTdTextBox.Style.Add("display", "none")
                RegExpreIphonePin.Enabled = False
                PanelVetting.Visible = False
            End If

            If Not IsDBNull(ds.Tables("Contacts").Rows(0)("FullName")) Then
                lblFullName.Text = ds.Tables("Contacts").Rows(0)("FullName")
                ViewState("FullName") = ds.Tables("Contacts").Rows(0)("FullName")
            End If

            If Not IsDBNull(ds.Tables("Contacts").Rows(0)("CompStatus")) Then
                ViewState("CompStatus") = ds.Tables("Contacts").Rows(0)("CompStatus")
            End If

            If Not IsDBNull(ds.Tables("Contacts").Rows(0)("ImageURL")) Then
                If (ds.Tables("Contacts").Rows(0)("ImageURL") <> "") Then
                    'imgProfile.ImageUrl = OrderWorkLibrary.ApplicationSettings.AttachmentDisplayPath(OrderWorkLibrary.ApplicationSettings.OWUKBizDivId) + "AttachmentsUK/ContactPhoto/" + (ds.Tables("Contacts").Rows(0)("ImageURL"))
                    If (Not Convert.ToString(ds.Tables("Contacts").Rows(0)("ImageURL")).Contains("empty_profile_pic")) Then
                        imgProfile.ImageUrl = OrderWorkLibrary.ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/AttachmentsUK/ContactPhoto/" + (ds.Tables("Contacts").Rows(0)("ImageURL")) + CommonFunctions.AzureStorageAccessKey().ToString
                        ViewState("ImageUrl") = (ds.Tables("Contacts").Rows(0)("ImageURL"))
                    Else
                        imgProfile.ImageUrl = OrderWorkLibrary.ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/AttachmentsUK/ContactPhoto/empty_profile_pic.png" + CommonFunctions.AzureStorageAccessKey().ToString
                        ViewState("ImageUrl") = OrderWorkLibrary.ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/AttachmentsUK/ContactPhoto/empty_profile_pic.png" + CommonFunctions.AzureStorageAccessKey().ToString
                    End If
                Else
                        'imgProfile.ImageUrl = OrderWorkLibrary.ApplicationSettings.AttachmentDisplayPath(OrderWorkLibrary.ApplicationSettings.OWUKBizDivId) + "AttachmentsUK/ContactPhoto/" + "empty_profile_pic.png"
                        imgProfile.ImageUrl = OrderWorkLibrary.ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/AttachmentsUK/ContactPhoto/empty_profile_pic.png" + CommonFunctions.AzureStorageAccessKey().ToString
                    ViewState("ImageUrl") = ""
                End If
            Else
                'imgProfile.ImageUrl = OrderWorkLibrary.ApplicationSettings.AttachmentDisplayPath(OrderWorkLibrary.ApplicationSettings.OWUKBizDivId) + "AttachmentsUK/ContactPhoto/" + "empty_profile_pic.png"
                imgProfile.ImageUrl = OrderWorkLibrary.ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/AttachmentsUK/ContactPhoto/empty_profile_pic.png" + CommonFunctions.AzureStorageAccessKey().ToString
                ViewState("ImageUrl") = ""
            End If


            If Not IsDBNull(ds.Tables("Contacts").Rows(0)("FName")) Then
                txtFName.Text = ds.Tables("Contacts").Rows(0)("FName")
                ViewState("FirstName") = ds.Tables("Contacts").Rows(0)("FName")
            End If
            If Not IsDBNull(ds.Tables("Contacts").Rows(0)("LName")) Then
                txtLName.Text = ds.Tables("Contacts").Rows(0)("LName")
                ViewState("LastName") = ds.Tables("Contacts").Rows(0)("LName")
            End If

            'populating Job title
            If Not IsDBNull(ds.Tables("Contacts").Rows(0)("JobTitle")) Then
                txtJobTitle.Text = ds.Tables("Contacts").Rows(0)("JobTitle")
            End If
            If Not (IsDBNull(ds.Tables("Contacts").Rows(0)("IsDefault"))) Then
                chkIsMain.Checked = ds.Tables("Contacts").Rows(0)("IsDefault")
            Else
                chkIsMain.Checked = False
            End If




            ddlLocation.SelectedValue = ds.Tables("Contacts").Rows(0)("AddressId")

            'Populating other selected location listbox
            If (ds.Tables(ds.Tables.Count - 4).Rows.Count > 0) Then
                Dim dvLocationsSel As DataView = ds.Tables(ds.Tables.Count - 4).Copy.DefaultView
                hdnLocationsSelected.Value = ""
                Dim dtt As DataTable = dvLocationsSel.ToTable
                For Each dr As DataRow In dtt.Rows
                    hdnLocationsSelected.Value = dr("AddressID") & "," & hdnLocationsSelected.Value
                Next
                listLocationsSel.DataSource = dvLocationsSel
                listLocationsSel.DataBind()
            End If





            'Populating other location listbox
            Dim dsOtherLocations As DataSet
            dsOtherLocations = CommonFunctions.GetOtherLocationsofCompany(Page, CompanyID)
            If dsOtherLocations.Tables(0).Rows.Count > 0 Then
                If (hdnLocationsSelected.Value Is Nothing Or hdnLocationsSelected.Value = "") Then
                    hdnLocationsSelected.Value = "0"
                End If
                Dim DefaultAddressID As Integer = CInt(ds.Tables("Contacts").Rows(0)("AddressId"))
                Dim dvLocations As DataView = dsOtherLocations.Tables(0).Copy.DefaultView
                dvLocations.RowFilter = "AddressID <> " & DefaultAddressID & " AND " & "AddressID NOT IN (" & hdnLocationsSelected.Value & ")"
                dvLocations.Sort = "Name"
                listLocations.DataSource = dvLocations
                listLocations.DataBind()
            End If



            Dim roleID As Integer
            ddlUserType.SelectedValue = ds.Tables("Contacts").Rows(0)("RoleGroupID")
            roleID = ds.Tables("Contacts").Rows(0)("RoleGroupID")

            If (ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierAdminID) Or (ddlUserType.SelectedValue = ApplicationSettings.RoleClientAdminID) Then
                'Is User Type is Admin then dont allow Type to be changed
                ddlUserType.Enabled = False

                'chkIsDeleted.Enabled = False
                'donot show the spend limit n Accept limit text boxes
                tdTxtSpendLimit.Visible = False
                trSpendLimit.Visible = False
                tdTxtAcceptWOLimit.Visible = False
                tdAcceptWOLimit.Visible = False

                ' disable - Active/Inactive
                'ddlStatus.Enabled = False
            Else
                'ddlUserType.Enabled = True
                Select Case ApplicationSettings.SiteType
                    Case ApplicationSettings.siteTypes.site
                        'On Edit or Add of User Typr dont provide Admin option to site users.
                        ddlUserType.Items.Remove(ddlUserType.Items.FindByText("Administrator"))
                End Select
                If (ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierDepotID) Or (ddlUserType.SelectedValue = ApplicationSettings.RoleClientDepotID) Then
                    ChkEnableLogin.Checked = False
                    ChkEnableLogin.Enabled = False
                    ChkEmailVerified.Enabled = False
                    ChkEmailVerified.Checked = False
                End If
            End If

            'For Specialist on edit - the can fullfill wo check box should be checked and disabled
            If (ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierSpecialistID) Then
                chkFulfilWOs.Checked = True
                chkFulfilWOs.Enabled = False
            End If

            'if uc access from admin
            'If (Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID) Or (Session("RoleGroupID") = ApplicationSettings.RoleOWRepID) Then
            If (ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierAdminID) Or (ddlUserType.SelectedValue = ApplicationSettings.RoleClientAdminID) Then
                tdTxtSpendLimit.Visible = False
                trSpendLimit.Visible = False
                tdTxtAcceptWOLimit.Visible = False
                tdAcceptWOLimit.Visible = False
            End If
            If (ClassID = ApplicationSettings.RoleClientID) Then
                chkFulfilWOs.Visible = False
                tdTxtAcceptWOLimit.Visible = False
                tdAcceptWOLimit.Visible = False
            End If
            'End If

            'Populating Status of Contact and store it in viewstate to check if status change and send appropriate mail 
            If ddlStatus.Visible Then
                If Not (IsDBNull(ds.Tables("Contacts").Rows(0)("Status"))) Then
                    ddlStatus.SelectedValue = ds.Tables("Contacts").Rows(0)("Status")
                    ViewState("UserStatus") = ds.Tables("Contacts").Rows(0)("Status")
                Else
                    ddlStatus.SelectedItem.Text = ResourceMessageText.GetString("SelectUserType")
                End If
            End If

            If Not (IsDBNull(ds.Tables("Contacts").Rows(0).Item("UserName"))) Then
                txtEmail.Text = ds.Tables("Contacts").Rows(0).Item("UserName")
                ViewState("Email") = ds.Tables("Contacts").Rows(0)("UserName")
            End If

            'Populating User Name
            'Populating User Name
            If Not (IsDBNull(ds.Tables("Contacts").Rows(0)("Password"))) Then
                txtOldPassword.Text = ds.Tables("Contacts").Rows(0).Item("Password")
            End If
            If Not (IsDBNull(ds.Tables("Contacts").Rows(0)("NewsletterStatus"))) Then
                ChkSubscribe.Checked = ds.Tables("Contacts").Rows(0).Item("NewsletterStatus")
            End If

            If Not (IsDBNull(ds.Tables("Contacts").Rows(0)("EnableLogin"))) Then
                If ds.Tables("Contacts").Rows(0).Item("EnableLogin") Then
                    ChkEnableLogin.Checked = True
                    If ChkEnableLogin.Checked Then
                        If contactid <> Session("UserId") Then
                            pnlEnableDisableLogin.Visible = True
                        End If
                        If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("SecurityQues")) Then
                            If CInt(ds.Tables("Contacts").Rows(0).Item("SecurityQues")) = 0 Then
                                ddlQuestion.SelectedValue = ""
                            Else
                                ddlQuestion.SelectedValue = CInt(ds.Tables("Contacts").Rows(0).Item("SecurityQues"))
                            End If
                            ViewState("ContactSecQues") = CInt(ds.Tables("Contacts").Rows(0).Item("SecurityQues"))
                        End If

                        If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("SecurityAns")) Then
                            txtAnswer.Text = ds.Tables("Contacts").Rows(0).Item("SecurityAns")
                            ViewState("ContactSecAns") = ds.Tables("Contacts").Rows(0).Item("SecurityAns")
                        End If

                        If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("Password")) Then
                            ViewState("ContactPassword") = ds.Tables("Contacts").Rows(0).Item("Password")
                        End If

                        ViewState("EnableLogin") = True
                    Else
                        pnlEnableDisableLogin.Visible = False
                        ViewState("EnableLogin") = False
                    End If
                Else
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("Password")) Then
                        ViewState("ContactPassword") = ds.Tables("Contacts").Rows(0).Item("Password")
                    End If
                    pnlEnableDisableLogin.Visible = False
                    ChkEnableLogin.Checked = False
                End If
            End If
            If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("EmailVerified")) Then
                ViewState("EmailVerified") = ds.Tables("Contacts").Rows(0).Item("EmailVerified")
            End If

            'If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("OWApproved")) Then
            '    checkOrderworkApproved.Checked = ds.Tables("Contacts").Rows(0).Item("OWApproved")
            'End If

            If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("CVVerified")) Then
                chkVerifiedCV.Checked = ds.Tables("Contacts").Rows(0).Item("CVVerified")
            End If
            If (ds.Tables("Contacts").Rows(0)("CVRequired") = 0) Then
                DivCvAttachmnet.Style.Add("display", "none")
                chkIsCVRequired.Checked = True
            Else
                chkIsCVRequired.Checked = False
                DivCvAttachmnet.Style.Add("display", "inline")
            End If



            If (ViewState("mode") = "edit") Then
                If ((Request("classid") = ApplicationSettings.RoleSupplierID) Or ((Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleSupplierID)) Then
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("IsRecordVerified")) Then
                        If ds.Tables("Contacts").Rows(0).Item("IsRecordVerified") Then
                            chkRecordVerifed.Checked = True
                        Else
                            chkRecordVerifed.Checked = False
                        End If
                    Else
                        chkRecordVerifed.Checked = False
                    End If
                End If
            End If



            If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("ProofOfMeetingVerified")) Then
                        chkVerifiedPOM.Checked = ds.Tables("Contacts").Rows(0).Item("ProofOfMeetingVerified")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("CRCVerified")) Then
                        chkVerifiedCRC.Checked = ds.Tables("Contacts").Rows(0).Item("CRCVerified")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("UKSecClearanceVerified")) Then
                        chkVerifiedUSC.Checked = ds.Tables("Contacts").Rows(0).Item("UKSecClearanceVerified")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("RightToWorkVerified")) Then
                        chkVerifiedRWUK.Checked = ds.Tables("Contacts").Rows(0).Item("RightToWorkVerified")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("CSCSVerified")) Then
                        chkVerifiedCSCS.Checked = ds.Tables("Contacts").Rows(0).Item("CSCSVerified")
                    End If

                    If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                        ChkEmailVerified.Checked = ViewState("EmailVerified")
                    End If
                    Session("EmailAddr") = txtEmail.Text.Trim

                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("OtherEmail")) Then
                        txtOtherEmail.Text = ds.Tables("Contacts").Rows(0).Item("OtherEmail")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("Phone")) Then
                        txtPhone.Text = ds.Tables("Contacts").Rows(0).Item("Phone")
                        ViewState("Phone") = ds.Tables("Contacts").Rows(0)("Phone")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("Mobile")) Then
                        txtMobile.Text = ds.Tables("Contacts").Rows(0).Item("Mobile")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("StdDayRate")) Then
                        txtDayRate.Text = ds.Tables("Contacts").Rows(0).Item("StdDayRate")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("Profile")) Then
                        txtProfile.Text = ds.Tables("Contacts").Rows(0).Item("Profile")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("Fax")) Then
                        txtFax.Text = ds.Tables("Contacts").Rows(0).Item("Fax")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("SpendLimit")) Then
                        txtSpendLimit.Text = ds.Tables("Contacts").Rows(0).Item("SpendLimit")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("AcceptWOLimit")) Then
                        txtAcceptWOLimit.Text = ds.Tables("Contacts").Rows(0).Item("AcceptWOLimit")
                    End If

                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("CommunicationPref")) Then
                        If ds.Tables("Contacts").Rows(0).Item("CommunicationPref") <> 0 Then
                            ddlCommunication.SelectedValue = ds.Tables("Contacts").Rows(0).Item("CommunicationPref")
                        End If

                    End If

                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("FulFilWOs")) Then
                        chkFulfilWOs.Checked = ds.Tables("Contacts").Rows(0).Item("FulFilWOs")
                    End If
                    If Session("ContactClassID") = System.Configuration.ConfigurationManager.AppSettings("RoleSupplierID") Then
                        If chkFulfilWOs.Checked Then
                            If roleID = ApplicationSettings.RoleSupplierAdminID Or roleID = ApplicationSettings.RoleSupplierSpecialistID Then
                                pnlAreaOfExpertise.Visible = True

                                ''Initialize AOE UC
                                UCAOE1.BizDivId = Request("bizDivId")
                                ' assign AOE combIds
                                Dim aoeAttrLabel As String = ""
                                Dim combIDs As String = ""
                                Select Case ApplicationSettings.Country
                                    Case ApplicationSettings.CountryUK
                                        Select Case BizDivID
                                            Case ApplicationSettings.OWUKBizDivId
                                                aoeAttrLabel = "OWAOE"
                                            Case ApplicationSettings.SFUKBizDivId
                                                aoeAttrLabel = "MSAOE"
                                        End Select
                                    Case ApplicationSettings.CountryDE
                                        Select Case BizDivID
                                            Case ApplicationSettings.OWDEBizDivId
                                                aoeAttrLabel = "DEAOE"
                                            Case ApplicationSettings.SFDEBizDivId
                                                aoeAttrLabel = "MSAOE"
                                        End Select
                                End Select

                                dsView = ds.Tables("ContactSettings").Copy.DefaultView
                                dsView.RowFilter = "AttributeLabel = '" & aoeAttrLabel & "'"

                                For Each drv As DataRowView In dsView
                                    If combIDs <> "" Then
                                        combIDs &= "," & drv("AttributeValue")
                                    Else
                                        combIDs = drv("AttributeValue")
                                    End If
                                Next
                                UCAOE1.CombIds = combIDs

                            Else
                                pnlAreaOfExpertise.Visible = False
                            End If
                        Else
                            pnlAreaOfExpertise.Visible = False
                        End If
                    Else
                        pnlAreaOfExpertise.Visible = False
                    End If
                    'Populating Recieve Work order notificationchkRecvNotif

                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("ReceiveWONotif")) Then
                        chkRecvNotif.Checked = ds.Tables("Contacts").Rows(0).Item("ReceiveWONotif")
                    End If

                    UCAccreditations.BizDivId = ApplicationSettings.OWUKBizDivId
                    UCAccreditations.CommonID = contactid
                    UCAccreditations.Type = "EngineerAccreditations"

                    UCEnggRateInfo.ContactID = contactid

                    If pnlAccreditationsInfo.Visible = True Then
                        txtTranscriptId.Text = ds.Tables("Contacts").Rows(0).Item("TranscriptID")
                        txtAccessCode.Text = ds.Tables("Contacts").Rows(0).Item("AccessCode")
                    End If
                    If Not IsDBNull(ds.Tables("Contacts").Rows(0).Item("TravelDistance")) Then
                        If ds.Tables("Contacts").Rows(0).Item("TravelDistance") <> 0 Then
                            ddlTravelDistance.SelectedValue = ds.Tables("Contacts").Rows(0).Item("TravelDistance")
                        End If
                    End If

                    If _bizDivID = ApplicationSettings.SFUKBizDivId Then
                        Dim dvList As DataView
                        dvList = CommonFunctions.GetCertQual(Page, _bizDivID)
                        If _bizDivID = ApplicationSettings.SFUKBizDivId Then
                            dvList.RowFilter = "Description = 'Non Microsoft'"
                        End If
                        dvList.Sort = "Name"
                        Dim dsView1 As DataView
                        dsView1 = ds.Tables("ContactSkillsExams").Copy.DefaultView
                        dsView1.RowFilter = "Type = 'Skill'"
                        UCNonMicroAccred.populateListBox(dsView1, dvList, "Name", "SkillExamID", "CertQualID")
                    End If
                    'Poonam modified on 30/12/2015 - Task - OA-112 : OA - Add Emergency Contact fields in User detail page
                    If ((Request("classid") = ApplicationSettings.RoleSupplierID) Or ((Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleSupplierID)) Then
                        'supplier only
                        txtEmergencyFname.Text = ds.Tables("Contacts").Rows(0).Item("EmergencyFname")
                        txtEmergencyLname.Text = ds.Tables("Contacts").Rows(0).Item("EmergencyLname")
                        ddlRelationshipType.SelectedValue = ds.Tables("Contacts").Rows(0).Item("RelationshipType")
                        txtTelephoneNumber.Text = ds.Tables("Contacts").Rows(0).Item("EmergencyPhone")
                        txtDoctorName.Text = ds.Tables("Contacts").Rows(0).Item("DoctorName")
                        txtGPAddress.Text = ds.Tables("Contacts").Rows(0).Item("GPAddress")
                        txtGPPostcode.Text = ds.Tables("Contacts").Rows(0).Item("GPPostCode")
                        txtGPTelephoneNumber.Text = ds.Tables("Contacts").Rows(0).Item("GPPhone")

                    End If
                Else

                    PanelContactComment.Visible = False
            If ClassID = ApplicationSettings.RoleSupplierID Then
                PanelVetting.Visible = True
                CRBCheckedNo.Checked = True
                UKSecurityClearanceNo.Checked = True
                CSCSNo.Checked = True

                'Populating Criminal Record listbox
                Dim dsCriminalRecord As DataSet
                dsCriminalRecord = CommonFunctions.GetStandards(Page, "CRBCheck")
                If dsCriminalRecord.Tables(0).Rows.Count > 0 Then
                    Dim dvCriminalRecord As DataView = dsCriminalRecord.Tables(0).Copy.DefaultView
                    dvCriminalRecord.Sort = "StandardValue"
                    listCriminalRecord.DataSource = dvCriminalRecord
                    listCriminalRecord.DataBind()
                End If

                'Populating SecurityCheck listbox
                Dim dsSecurityCheck As DataSet
                dsSecurityCheck = CommonFunctions.GetStandards(Page, "SecurityCheck")
                If dsSecurityCheck.Tables(0).Rows.Count > 0 Then
                    Dim dvSecurityCheck As DataView = dsSecurityCheck.Tables(0).Copy.DefaultView
                    dvSecurityCheck.Sort = "StandardValue"
                    listSecurityCheck.DataSource = dvSecurityCheck
                    listSecurityCheck.DataBind()
                End If
            Else
                PanelVetting.Visible = False
            End If
            'Populating other location listbox
            Dim dsOtherLocations As DataSet
            dsOtherLocations = CommonFunctions.GetOtherLocationsofCompany(Page, CompanyID)
            If dsOtherLocations.Tables(0).Rows.Count > 0 Then
                Dim dvLocations As DataView = dsOtherLocations.Tables(0).Copy.DefaultView
                dvLocations.Sort = "Name"
                listLocations.DataSource = dvLocations
                listLocations.DataBind()
            End If

            'UCAccreds1.BizDivId = _bizDivID
            'UCAccreds1.ContactId = contactid
            'UCAccreds1.CompanyID = _companyID
            UCAccreditations.BizDivId = _bizDivID
            UCAccreditations.CommonID = contactid
            UCAccreditations.Type = "EngineerAccreditations"

            'ddlUserType.Enabled = True
            Select Case ApplicationSettings.SiteType
                Case ApplicationSettings.siteTypes.site
                    'On Edit or Add of User Typr dont provide Admin option to site users.
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByText("Administrator"))
            End Select
            If (ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierDepotID) Or (ddlUserType.SelectedValue = ApplicationSettings.RoleClientDepotID) Then
                ChkEnableLogin.Checked = False
                ChkEnableLogin.Enabled = False
                ChkEmailVerified.Enabled = False
                ChkEmailVerified.Checked = False
            End If
            If Session("ContactClassID") = System.Configuration.ConfigurationManager.AppSettings("RoleSupplierID") Then
                If chkFulfilWOs.Checked Then
                    pnlAreaOfExpertise.Visible = True
                Else
                    pnlAreaOfExpertise.Visible = False
                End If
            Else
                pnlAreaOfExpertise.Visible = False
            End If
            'If (Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID) Or (Session("RoleGroupID") = ApplicationSettings.RoleOWRepID) Then
            If (ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierAdminID) Or (ddlUserType.SelectedValue = ApplicationSettings.RoleClientAdminID) Then
                tdTxtSpendLimit.Visible = False
                trSpendLimit.Visible = False
                tdTxtAcceptWOLimit.Visible = False
                tdAcceptWOLimit.Visible = False
            End If
            If (ClassID = ApplicationSettings.RoleClientID) Then
                chkFulfilWOs.Visible = False
                tdTxtAcceptWOLimit.Visible = False
                tdAcceptWOLimit.Visible = False
            End If
            'End If
        End If
    End Sub

    Public Sub PopulateAddUserInfo()
        Dim contactid As Integer = ViewState("ContactID")

        UCEnggRateInfo.ContactID = contactid
        Dim ds As DataSet
        Dim dsView As DataView

        InitializeAttachments()
        PanelContactComment.Visible = False
        If ClassID = ApplicationSettings.RoleSupplierID Then
            PanelVetting.Visible = True
            CRBCheckedNo.Checked = True
            UKSecurityClearanceNo.Checked = True
            CSCSNo.Checked = True

            'Populating Criminal Record listbox
            Dim dsCriminalRecord As DataSet
            dsCriminalRecord = CommonFunctions.GetStandards(Page, "CRBCheck")
            If dsCriminalRecord.Tables(0).Rows.Count > 0 Then
                Dim dvCriminalRecord As DataView = dsCriminalRecord.Tables(0).Copy.DefaultView
                dvCriminalRecord.Sort = "StandardValue"
                listCriminalRecord.DataSource = dvCriminalRecord
                listCriminalRecord.DataBind()
            End If

            'Populating SecurityCheck listbox
            Dim dsSecurityCheck As DataSet
            dsSecurityCheck = CommonFunctions.GetStandards(Page, "SecurityCheck")
            If dsSecurityCheck.Tables(0).Rows.Count > 0 Then
                Dim dvSecurityCheck As DataView = dsSecurityCheck.Tables(0).Copy.DefaultView
                dvSecurityCheck.Sort = "StandardValue"
                listSecurityCheck.DataSource = dvSecurityCheck
                listSecurityCheck.DataBind()
            End If
        Else
            PanelVetting.Visible = False
        End If
        'Populating other location listbox
        Dim dsOtherLocations As DataSet
        dsOtherLocations = CommonFunctions.GetOtherLocationsofCompany(Page, CompanyID)
        If dsOtherLocations.Tables(0).Rows.Count > 0 Then
            Dim dvLocations As DataView = dsOtherLocations.Tables(0).Copy.DefaultView
            dvLocations.Sort = "Name"
            listLocations.DataSource = dvLocations
            listLocations.DataBind()
        End If

        'UCAccreds1.BizDivId = _bizDivID
        'UCAccreds1.ContactId = contactid
        'UCAccreds1.CompanyID = _companyID
        UCAccreditations.BizDivId = _bizDivID
        UCAccreditations.CommonID = contactid
        UCAccreditations.Type = "EngineerAccreditations"

        'ddlUserType.Enabled = True
        Select Case ApplicationSettings.SiteType
            Case ApplicationSettings.siteTypes.site
                'On Edit or Add of User Typr dont provide Admin option to site users.
                ddlUserType.Items.Remove(ddlUserType.Items.FindByText("Administrator"))
        End Select
        If (ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierDepotID) Or (ddlUserType.SelectedValue = ApplicationSettings.RoleClientDepotID) Then
            ChkEnableLogin.Checked = False
            ChkEnableLogin.Enabled = False
            ChkEmailVerified.Enabled = False
            ChkEmailVerified.Checked = False
        End If
        If Session("ContactClassID") = System.Configuration.ConfigurationManager.AppSettings("RoleSupplierID") Then
            If chkFulfilWOs.Checked Then
                pnlAreaOfExpertise.Visible = True
            Else
                pnlAreaOfExpertise.Visible = False
            End If
        Else
            pnlAreaOfExpertise.Visible = False
        End If
        'If (Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID) Or (Session("RoleGroupID") = ApplicationSettings.RoleOWRepID) Then
        If (ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierAdminID) Or (ddlUserType.SelectedValue = ApplicationSettings.RoleClientAdminID) Then
            tdTxtSpendLimit.Visible = False
            trSpendLimit.Visible = False
            tdTxtAcceptWOLimit.Visible = False
            tdAcceptWOLimit.Visible = False
        End If
        If (ClassID = ApplicationSettings.RoleClientID) Then
            chkFulfilWOs.Visible = False
            tdTxtAcceptWOLimit.Visible = False
            tdAcceptWOLimit.Visible = False
        End If
        'End If
    End Sub


    Public Sub InitializeAttachments()
        UCFileUpload1.CompanyID = _companyID
        UCFileUpload1.AttachmentForID = ViewState("ContactID")
        UCFileUpload1.BizDivID = ApplicationSettings.OWUKBizDivId
        UCFileUpload1.AttachmentForSource = "CV"
        UCFileUpload1.ExistAttachmentSourceID = ViewState("ContactID")
        UCFileUpload1.ExistAttachmentSource = "CV"
        UCFileUpload1.ClearUCFileUploadCache()
        UCFileUpload1.PopulateAttachedFiles()
        UCFileUpload1.PopulateExistingFiles()

        UCFileUpload7.CompanyID = _companyID
        UCFileUpload7.AttachmentForID = ViewState("ContactID")
        UCFileUpload7.BizDivID = ApplicationSettings.OWUKBizDivId
        UCFileUpload7.AttachmentForSource = "DBS"
        UCFileUpload7.ExistAttachmentSourceID = ViewState("ContactID")
        UCFileUpload7.ExistAttachmentSource = "DBS"
        UCFileUpload7.ClearUCFileUploadCache()
        UCFileUpload7.PopulateAttachedFiles()
        UCFileUpload7.PopulateExistingFiles()

        UCFileUpload2.CompanyID = _companyID
        UCFileUpload2.BizDivID = ApplicationSettings.OWUKBizDivId
        UCFileUpload2.AttachmentForID = ViewState("ContactID")
        UCFileUpload2.AttachmentForSource = "ProofOfMeeting"
        UCFileUpload2.ExistAttachmentSourceID = ViewState("ContactID")
        UCFileUpload2.ExistAttachmentSource = "ProofOfMeeting"
        UCFileUpload2.ClearUCFileUploadCache()
        UCFileUpload2.PopulateAttachedFiles()
        UCFileUpload2.PopulateExistingFiles()

        UCFileUpload3.CompanyID = _companyID
        UCFileUpload3.BizDivID = ApplicationSettings.OWUKBizDivId
        UCFileUpload3.AttachmentForID = ViewState("ContactID")
        UCFileUpload3.AttachmentForSource = "PassportVisa"
        UCFileUpload3.ExistAttachmentSourceID = ViewState("ContactID")
        UCFileUpload3.ExistAttachmentSource = "PassportVisa"
        UCFileUpload3.ClearUCFileUploadCache()
        UCFileUpload3.PopulateAttachedFiles()
        UCFileUpload3.PopulateExistingFiles()

        'UCFileUpload4.CompanyID = _companyID
        'UCFileUpload4.BizDivID = ApplicationSettings.OWUKBizDivId
        'UCFileUpload4.AttachmentForID = ViewState("ContactID")
        'UCFileUpload4.AttachmentForSource = "CRBChecked"
        'UCFileUpload4.ExistAttachmentSourceID = ViewState("ContactID")
        'UCFileUpload4.ExistAttachmentSource = "CRBChecked"
        'UCFileUpload4.ClearUCFileUploadCache()
        'UCFileUpload4.PopulateAttachedFiles()
        'UCFileUpload4.PopulateExistingFiles()

        UCFileUpload5.CompanyID = _companyID
        UCFileUpload5.BizDivID = ApplicationSettings.OWUKBizDivId
        UCFileUpload5.AttachmentForID = ViewState("ContactID")
        UCFileUpload5.AttachmentForSource = "UKSecurityClearance"
        UCFileUpload5.ExistAttachmentSourceID = ViewState("ContactID")
        UCFileUpload5.ExistAttachmentSource = "UKSecurityClearance"
        UCFileUpload5.ClearUCFileUploadCache()
        UCFileUpload5.PopulateAttachedFiles()
        UCFileUpload5.PopulateExistingFiles()

        UCFileUpload6.CompanyID = _companyID
        UCFileUpload6.BizDivID = ApplicationSettings.OWUKBizDivId
        UCFileUpload6.AttachmentForID = ViewState("ContactID")
        UCFileUpload6.AttachmentForSource = "CSCS"
        UCFileUpload6.ExistAttachmentSourceID = ViewState("ContactID")
        UCFileUpload6.ExistAttachmentSource = "CSCS"
        UCFileUpload6.ClearUCFileUploadCache()
        UCFileUpload6.PopulateAttachedFiles()
        UCFileUpload6.PopulateExistingFiles()
    End Sub

    ''' <summary>
    ''' To fetch the details of the contact as dataset to populate the My Profile page
    ''' </summary>
    ''' <param name="IsMainContact"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetContact(Optional ByVal IsMainContact As Boolean = False, Optional ByVal KillCache As Boolean = False, Optional ByVal contactid As Integer = 0) As DataSet
        Dim cacheKey As String = "Contact" & "-" & CStr(contactid) & "-" & Session.SessionID
        If KillCache = True Then
            Page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If Cache(cacheKey) Is Nothing Then
            ds = CommonFunctions.GetContactsDS(Page, Session.SessionID, contactid, IsMainContact, cacheKey, KillCache)
        Else
            ds = CType(Page.Cache(cacheKey), DataSet)

        End If
        Return ds
    End Function

    Protected Sub ValidatePage()
        Page.Validate()
        If (Page.IsValid = False) Then
            divValidationMain.Visible = True
        Else
            divValidationMain.Visible = False

            If CheckNewEmailExist() Then
                Exit Sub
            End If

            If Not CheckEmailForAdmin() Then
                Exit Sub
            End If

            If CheckDepotLocation() Then
                Exit Sub
            End If


            'If Account is set to inactive status then perform validation for "One person in copany should be able to  receive the Work Order Notifications. "
            'If Account is set to inactive status then perform validation for "One person in copany should be able to  Fulfill Work Order."
            If Not CheckSettingsOnInactive() Then
                Exit Sub
            End If

            'If Either of checkbox chkRecvNotif or chkFulfilWOs is uncheck then check there are people to recieve work order notfiication and to fulill Work Order
            If Not FulFillSettingsForWO() Then
                Exit Sub
            End If

            '=======================================================================================================


            'Check for server side validation
            Dim Status As Boolean
            'Check if password is correct and both password supplied are same
            Status = PasswordCheckValid()
            'Check id new username supplied by user already exist in the system
            Status = Status And (Not CheckEmailAlreadyExist())

            Session("NewPassword") = Session("Password")

            If Status = True Then
                'If the field is not empty then store encrypted password in session variable
                If txtPassword.Text.Trim <> "" Then
                    Session("NewPassword") = Encryption.EncryptToMD5Hash(Trim(CStr(txtPassword.Text)))
                End If
                If Session("UserId") = ViewState("ContactID") Then
                    Session("NewSecQues") = CInt(ddlQuestion.SelectedValue)
                    Session("NewSecAns") = Trim(txtAnswer.Text)
                Else
                    'Session("NewSecQues") = 8
                    'Session("NewSecAns") = ""
                End If

                pnlSubmitForm.Visible = False
                pnlConfirm.Visible = True
            End If

            '=================================================================================================================

            'pnlSubmitForm.Visible = False
            'pnlConfirm.Visible = True
            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                If ddlUserType.Enabled = True And ddlUserType.SelectedItem.Text = "Administrator" Then
                    lblConfirmMsg.Text = "An account can have only one Administrator. There is already an Administrator for this Account. Making the user whos profile you are editing an Administrator will change the previous Administrator to a Manager. Are you sure you want to do this?"
                Else
                    lblConfirmMsg.Text = ""
                End If
            End If
        End If
    End Sub

    Protected Function SaveProfile() As XSDContacts

        Dim strMode As String
        strMode = ViewState("mode")

        Dim contactid = ViewState("ContactID")
        Dim AddressID As Integer = ddlLocation.SelectedValue

        Dim dsContacts As New XSDContacts
        Dim NewsletterStatus As Integer

        Dim strAppend As String
        Select Case _bizDivID
            Case ApplicationSettings.OWUKBizDivId
                strAppend = "OW"
            Case ApplicationSettings.SFUKBizDivId
                strAppend = "MS"
            Case ApplicationSettings.OWDEBizDivId
                strAppend = "DE"
        End Select

        Dim nrow As XSDContacts.ContactsRow = dsContacts.Contacts.NewRow
        With nrow
            .MainContactID = CompanyID
            .ContactID = contactid
            .IsCompany = 0
            .Profile = txtProfile.Text
            .Src = "OrderWork"
            .IsDeleted = chkIsDeleted.Checked
            .Fname = txtFName.Text.Trim
            .Lname = txtLName.Text.Trim
            .FullName = txtFName.Text.Trim + " " + txtLName.Text.Trim
            .Email = txtEmail.Text.Trim
            .OtherEmail = txtOtherEmail.Text.Trim
            .Phone = txtPhone.Text.Trim
            .Mobile = txtMobile.Text.Trim
            .Fax = txtFax.Text.Trim
            If ddlCommunication.SelectedValue <> "" Then
                .CommunicationPref = ddlCommunication.SelectedValue
            End If
            .JobTitle = txtJobTitle.Text.Trim
            .FulFilWOs = chkFulfilWOs.Checked
            .ReceiveWONotif = chkRecvNotif.Checked
            If txtSpendLimit.Text.Trim <> "" Then
                .SpendLimit = txtSpendLimit.Text.Trim
            End If
            If txtAcceptWOLimit.Text.Trim <> "" Then
                .AcceptWOLimit = txtAcceptWOLimit.Text.Trim
            End If
            If chkFulfilWOs.Checked Then
                If ddlTravelDistance.SelectedValue <> "" Then
                    .TravelDistance = ddlTravelDistance.SelectedValue
                End If
                If txtTranscriptId.Text.Trim <> "" Then
                    .TranscriptID = txtTranscriptId.Text.Trim
                End If
            End If
        End With
        dsContacts.Contacts.Rows.Add(nrow)


        'For updating tblRolesContactLinkage
        Dim nrowRClink As XSDContacts.tblRolesContactLinkageRow = dsContacts.tblRolesContactLinkage.NewRow
        With nrowRClink
            .ContactID = contactid
            .BizDivID = _bizDivID
            .RoleGroupID = ddlUserType.SelectedValue
            If chkIsMain.Checked Then
                .IsDefault = 1
            Else
                .IsDefault = 0
            End If
            .IsActive = 0
            If ddlLocation.SelectedValue <> "" Then
                .AddressID = ddlLocation.SelectedValue
            End If

        End With
        dsContacts.tblRolesContactLinkage.Rows.Add(nrowRClink)

        'For updating
        Dim nrowCS As XSDContacts.ContactsSetupRow = dsContacts.ContactsSetup.NewRow
        With nrowCS
            .BizDivID = _bizDivID
            .ContactID = contactid
            If Not IsNothing(ViewState("ContactID")) Then
                If ViewState("ContactID") <> 0 Then
                    If (ddlStatus.SelectedValue <> "") Then
                        .Status = ddlStatus.SelectedValue
                    Else
                        .Status = 1
                    End If

                Else
                    .Status = 1
                End If
            Else
                .Status = 1
            End If
            .ClassID = _classID
        End With
        dsContacts.ContactsSetup.Rows.Add(nrowCS)

        'For updating tblContactslogin
        'Save NewsLetter Status
        If ChkSubscribe.Checked = True Then
            NewsletterStatus = 1
        End If

        Dim nrowCL As XSDContacts.tblContactsLoginRow = dsContacts.tblContactsLogin.NewRow
        With nrowCL
            .ContactID = contactid
            .UserName = txtEmail.Text.Trim
            'poonam modified on 4/12/2015 - Task - OA-131 :OA/OM - Strengthen automatically and manually generated passwords
            Dim strpass As String = CommonFunctions.GenerateOTP()
            If strMode = "add" Then
                If ChkEnableLogin.Checked = True Then
                    .Password = Encryption.EncryptToMD5Hash(strpass)

                    'If Session("UserId") = ViewState("ContactID") Then
                    .SecurityQues = CInt(ddlQuestion.SelectedValue)
                    .SecurityAns = txtAnswer.Text.Trim
                    'End If


                    ViewState("ContactPassword") = (strpass)

                End If
            Else
                If ViewState("EnableLogin") = False And ChkEnableLogin.Checked = True Then
                    .Password = Encryption.EncryptToMD5Hash(strpass)


                    'If Session("UserId") = ViewState("ContactID") Then
                    .SecurityQues = CInt(ddlQuestion.SelectedValue)
                    .SecurityAns = txtAnswer.Text.Trim

                    'End If



                    ViewState("ContactPassword") = (strpass)

                ElseIf ViewState("EnableLogin") = True And ChkEnableLogin.Checked = True Then
                    .SecurityQues = CInt(ddlQuestion.SelectedValue)
                    .SecurityAns = txtAnswer.Text.Trim
                    .Password = ViewState("ContactPassword")

                Else
                    .SecurityQues = ViewState("ContactSecQues")
                    .SecurityAns = ViewState("ContactSecAns")
                    .Password = ViewState("ContactPassword")
                End If
            End If

            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                .EmailVerified = ChkEmailVerified.Checked
            Else
                .EmailVerified = ViewState("EmailVerified")
            End If
            .NewsletterStatus = NewsletterStatus
            .EnableLogin = ChkEnableLogin.Checked

        End With
        dsContacts.tblContactsLogin.Rows.Add(nrowCL)


        'For updating tblContactsAccreditations
        ''Save accreditations
        'If _bizDivID <> ApplicationSettings.SFUKBizDivId Then
        '    Dim ds As DataSet = UCAccreditations.GetSelectedAccred()
        '    For Each drowAccreds As DataRow In ds.Tables(0).Rows
        '        If (drowAccreds("TagId") = 0) Then
        '            Dim nrowNewCompAccred As XSDContacts.TagsRow = dsContacts.Tags.NewRow
        '            With nrowNewCompAccred
        '                .TagFor = "Others"
        '                .TagName = drowAccreds("TagName")
        '            End With
        '            dsContacts.Tags.Rows.Add(nrowNewCompAccred)
        '        End If

        '        Dim nrowCompAccred As XSDContacts.TagContactLinkageRow = dsContacts.TagContactLinkage.NewRow
        '        With nrowCompAccred
        '            .ContactID = contactid
        '            .MainContactID = CompanyID
        '            .TagID = drowAccreds("TagId")
        '            .MainTagID = drowAccreds("TagId")
        '            .TagExpiry = drowAccreds("TagExpiry")
        '            .TagInfo = drowAccreds("TagInfo")
        '            .OtherInfo = drowAccreds("OtherInfo")
        '        End With
        '        dsContacts.TagContactLinkage.Rows.Add(nrowCompAccred)
        '    Next
        'End If

        'If _bizDivID <> ApplicationSettings.SFUKBizDivId Then
        '    Dim ds As DataSet = UCAccreds1.GetAccreds()
        '    For Each drowAccreds As DataRow In ds.Tables("tblSelectedAccreds").Rows
        '        Dim nrowCompAccred As XSDContacts.tblContactsAccreditationsRow = dsContacts.tblContactsAccreditations.NewRow
        '        With nrowCompAccred
        '            .ContactID = contactid
        '            .BizDivID = _bizDivID
        '            .VendorID = drowAccreds("VendorID")
        '            .CertID = drowAccreds("CertID")
        '            If Not IsDBNull(drowAccreds("CertCode")) Then
        '                If drowAccreds("CertCode") <> "" Then
        '                    .CertCode = drowAccreds("CertCode")
        '                End If
        '            End If

        '            If Not IsDBNull(drowAccreds("AccessCode")) Then
        '                If drowAccreds("AccessCode") <> "" Then
        '                    .AccessCode = drowAccreds("AccessCode")
        '                End If
        '            End If

        '        End With
        '        dsContacts.tblContactsAccreditations.Rows.Add(nrowCompAccred)
        '    Next
        'End If
        'Saving Action log entry
        If strMode = "add" Then
            Dim nrowALog As XSDContacts.tblActionLogRow = dsContacts.tblActionLog.NewRow
            With nrowALog
                .LogID = 1
                .BizDivID = _bizDivID
                .ActionID = ApplicationSettings.ActionLogID.Activate
                .ActionByCompany = _companyID
                .ActionByContact = _userID
                .ActionForCompany = _companyID
                .ActionForContact = ViewState("ContactID")
                .ActionRef = 0
                .ActionSrc = ""
                ._Date = System.DateTime.Now()

            End With
            dsContacts.tblActionLog.Rows.Add(nrowALog)

        ElseIf strMode = "edit" Then
            If ddlStatus.SelectedValue <> ViewState("UserStatus") Then
                Dim nrowALog1 As XSDContacts.tblActionLogRow = dsContacts.tblActionLog.NewRow
                With nrowALog1
                    .LogID = 1
                    .BizDivID = _bizDivID
                    .ActionID = IIf(ddlStatus.SelectedValue = ApplicationSettings.StatusActive, ApplicationSettings.ActionLogID.Activate, ApplicationSettings.ActionLogID.Inactivate)
                    .ActionByCompany = _companyID
                    .ActionByContact = _userID
                    .ActionForCompany = _companyID
                    .ActionForContact = ViewState("ContactID")
                    .ActionRef = 0
                    .ActionSrc = ""
                    ._Date = System.DateTime.Now()

                End With
                dsContacts.tblActionLog.Rows.Add(nrowALog1)
            End If
        End If

        'Save "Certifications/Qualifications"
        'this is to be done only for SF site
        Dim drow As DataRow
        If _bizDivID = ApplicationSettings.SFUKBizDivId Then
            Dim cert As DataTable = UCNonMicroAccred.saveListBox()
            For Each drow In cert.Rows
                dsContacts.tblContactsSkillsExams.Rows.Add(New String() {0, contactid, drow.Item("Value"), "Skill"})
            Next
        End If


        If ClassID = ApplicationSettings.RoleSupplierID Then
            Dim nrowCA As XSDContacts.ContactsAttributesRow = dsContacts.ContactsAttributes.NewRow

            With nrowCA
                .ContactID = contactid

                If CRBCheckedEnhanced.Checked Then
                    CRBCheckedYes.Checked = True
                    .CRBChecked = CRBCheckedYes.Checked
                    .CriminalRecordChecks = 137
                ElseIf CRBCheckedYes.Checked Then
                    .CRBChecked = CRBCheckedYes.Checked
                    '.CriminalRecordChecks = Regex.Replace(hdnCriminalRecordSelected.Value, "^,+|,+$|(?<=,),+", "")
                ElseIf CRBCheckedNo.Checked Then
                    .CRBChecked = Not (CRBCheckedNo.Checked)
                    .CriminalRecordChecks = ""
                    txtDBSExpiryDate.Text = ""
                    txtDisclosureNumber.Text = ""
                End If


                If rdoUK.Checked Then
                    .PassportType = rdoUK.Text.Trim()
                ElseIf rdoEU.Checked Then
                    .PassportType = rdoEU.Text.Trim()
                ElseIf rdoOthers.Checked Then
                    .PassportType = rdoOthers.Text.Trim()
                End If
                If UKSecurityClearanceYes.Checked Then
                    If (hdnSecuCheckSelected.Value Is Nothing Or hdnSecuCheckSelected.Value = "" Or hdnSecuCheckSelected.Value = "0") Then
                        .UKSecurityClearance = False
                        .SecurityChecks = ""
                    Else
                        .UKSecurityClearance = UKSecurityClearanceYes.Checked
                        .SecurityChecks = Regex.Replace(hdnSecuCheckSelected.Value, "^,+|,+$|(?<=,),+", "")
                    End If
                ElseIf UKSecurityClearanceNo.Checked Then
                    .UKSecurityClearance = Not (UKSecurityClearanceNo.Checked)
                    .SecurityChecks = ""
                Else
                    .UKSecurityClearance = False
                    .SecurityChecks = ""
                End If
                If CSCSYes.Checked Then
                    .CSCS = CSCSYes.Checked
                ElseIf CSCSNo.Checked Then
                    .CSCS = Not (CSCSNo.Checked)
                End If
                Dim dayRate As Decimal = 0
                If Decimal.TryParse(txtDayRate.Text, dayRate) Then
                    .StdDayRate = txtDayRate.Text
                End If
                .CVVerified = chkVerifiedCV.Checked
                .ProofOfMeetingVerified = chkVerifiedPOM.Checked
                .CRCVerified = chkVerifiedCRC.Checked
                .UKSecClearanceVerified = chkVerifiedUSC.Checked
                .RightToWorkVerified = chkVerifiedRWUK.Checked
                .CSCSVerified = chkVerifiedCSCS.Checked

                If (CRBCheckedEnhanced.Checked Or CRBCheckedYes.Checked) Then
                    dsContacts = UCFileUpload7.ReturnFilledAttachments(dsContacts)
                End If
                dsContacts = UCFileUpload2.ReturnFilledAttachments(dsContacts)
                dsContacts = UCFileUpload1.ReturnFilledAttachments(dsContacts)




                If txtVisaExpDate.Text <> "Enter expiry date" And txtVisaExpDate.Text <> "" Then
                    .UKRightToWorkExpDate = txtVisaExpDate.Text.Trim
                End If


                .DOB = txtDOB.Text.Trim()

                dsContacts = UCFileUpload3.ReturnFilledAttachments(dsContacts)



                'If txtCRBCheckDate.Text <> "Enter expiry date" And txtCRBCheckDate.Text <> "" Then
                '    .CRBCheckedIssueDate = txtCRBCheckDate.Text.Trim
                'End If


                If txtDisclosureNumber.Text <> "" Then
                    .DisclosureNumber = txtDisclosureNumber.Text.Trim
                End If

                'dsContacts = UCFileUpload4.ReturnFilledAttachments(dsContacts)

                If txtUKSecuDate.Text <> "Date of issue" And txtUKSecuDate.Text <> "" Then
                    .UKSecurityClearanceIssueDate = txtUKSecuDate.Text.Trim
                End If
                dsContacts = UCFileUpload5.ReturnFilledAttachments(dsContacts)

                If txtCSCSDate.Text <> "Enter expiry date" And txtCSCSDate.Text <> "" Then
                    .CSCSExpDate = txtCSCSDate.Text.Trim
                End If
                dsContacts = UCFileUpload6.ReturnFilledAttachments(dsContacts)

            End With
            dsContacts.ContactsAttributes.Rows.Add(nrowCA)


            'Faizan For Skills set save
            Dim strAOEIds As String = CType(UCAOE1.FindControl("hdnCombIDs"), HtmlInputHidden).Value
            If strAOEIds <> "" Then
                Dim AOEIds As String() = strAOEIds.Split(",")
                AOEIds = CommonFunctions.RemoveDuplicates(AOEIds)
                For Each aoeId As String In AOEIds
                    If (aoeId <> 0) Then
                        Dim nrowAOE As XSDContacts.ContactsExtAttributesRow = dsContacts.ContactsExtAttributes.NewRow
                        With nrowAOE
                            .ContactID = contactid
                            .AttributeLabel = "OWAOE"
                            .AttributeValue = aoeId
                        End With
                        dsContacts.ContactsExtAttributes.Rows.Add(nrowAOE)
                    End If
                Next
            End If

        End If



        For Each item As ListItem In cblVehicleTypeEdit.Items
            If item.Selected Then
                Dim nrowVehType As XSDContacts.ContactsExtAttributesRow = dsContacts.ContactsExtAttributes.NewRow
                With nrowVehType
                    .ContactID = contactid
                    .AttributeLabel = "VehicleType"
                    .AttributeValue = item.Value
                End With
                dsContacts.ContactsExtAttributes.Rows.Add(nrowVehType)
            End If
        Next
        Return dsContacts

    End Function


    ''' <summary>
    ''' To populate the security question and answer on the change login form
    ''' </summary>
    ''' <param name="killcache"></param>
    ''' <remarks></remarks>
    Protected Sub PopulateChangeLogin(Optional ByVal killcache As Boolean = False)
        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
            'tblEmail.Visible = False
            'tblSecurity.Visible = False
        End If


        'Hide validation region 
        divValidationMain.Visible = False

        'Clear the fields
        txtNewEmail.Text = ""
        txtEmailConfirm.Text = ""
        'If session variable for security is avaiable then
        If Not IsNothing(Session("SecQues")) Then
            If Not IsDBNull(Session("SecQues")) Then
                If CInt(Session("SecQues")) <> 0 Then
                    ddlQuestion.SelectedValue = CInt(Session("SecQues"))
                End If
            End If
        End If
        If Not IsNothing(Session("SecAns")) Then
            If Not IsDBNull(Session("SecAns")) Then
                txtAnswer.Text = Session("SecAns")
            End If
        End If
    End Sub


    ''' <summary>
    ''' To save changes made in the change login form
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub SaveChangeLogin()
        Page.Validate()
        If (Page.IsValid = False) Then
            divValidationMain.Visible = True
        Else
            'Page is Valid, hide validation region
            divValidationMain.Visible = False

            'Check for server side validation
            Dim Status As Boolean
            'Check if password is correct and both password supplied are same
            Status = PasswordCheckValid()
            'Check id new username supplied by user already exist in the system
            Status = Status And (Not CheckEmailAlreadyExist())

            Session("NewPassword") = Session("Password")

            If Status = True Then
                'If the field is not empty then store encrypted password in session variable
                If txtPassword.Text.Trim <> "" Then
                    Session("NewPassword") = Encryption.EncryptToMD5Hash(Trim(CStr(txtPassword.Text)))
                End If



                Session("NewSecQues") = CInt(ddlQuestion.SelectedValue)
                Session("NewSecAns") = Trim(txtAnswer.Text)

                pnlSubmitForm.Visible = False
                pnlConfirm.Visible = True

            End If
        End If

    End Sub

    ''' <summary>
    ''' Check for password condition
    ''' </summary>
    ''' <returns>boolean status</returns>
    ''' <remarks></remarks>
    Private Function PasswordCheckValid() As Boolean
        Dim status As Boolean
        If txtOldPassword.Text.Trim <> "" Then
            If CStr(Session("Password")) <> Trim(Encryption.EncryptToMD5Hash(txtOldPassword.Text)) Then
                'Check if old password is correct
                divValidationMain.Visible = True
                lblError.Text = ResourceMessageText.GetString("ChangeLoginIncorrectPassword")
                status = False
            Else
                'Check if old password is same as new password, if yes then prompt user
                If txtOldPassword.Text.Trim = txtPassword.Text.Trim Then
                    divValidationMain.Visible = True
                    lblError.Text = "<li>" & ResourceMessageText.GetString("ChangeLoginNoSamePassword") & "</li>"
                    status = False
                Else
                    status = True
                End If

            End If
        Else
            status = True
        End If
        Return status
    End Function

    ''' <summary>
    ''' Check if email already exist in system; if yes then show appropriate message
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckEmailAlreadyExist() As Boolean
        Dim loginexists As Boolean
        If txtNewEmail.Text <> "" Then
            loginexists = ws.WSContact.DoesLoginExist(txtNewEmail.Text.Trim)
            If loginexists = True Then
                'Email already exist, show validation message
                divValidationMain.Visible = True
                lblError.Text = ResourceMessageText.GetString("ChangeLoginEmailExist")
            End If
        End If
        Return loginexists
    End Function

    'Poonam modified on 30/12/2015 - Task - OA-112 : OA - Add Emergency Contact fields in User detail page
    Protected Function SaveEmergencycontacts() As OrderWorkLibrary.XSDEmergencyContact


        Dim dsEmergencyContact As New OrderWorkLibrary.XSDEmergencyContact
        Dim contactid = ViewState("ContactID")
        Dim nrowEC As OrderWorkLibrary.XSDEmergencyContact.EmergencyContactRow = dsEmergencyContact.EmergencyContact.NewRow
        With nrowEC
            .ContactID = contactid
            .Fname = txtEmergencyFname.Text.Trim
            .Lname = txtEmergencyLname.Text.Trim
            If (ddlRelationshipType.SelectedValue <> "") Then
                .RelationshipType = ddlRelationshipType.SelectedValue
            End If
            .Phone = txtTelephoneNumber.Text.Trim
            .DoctorName = txtDoctorName.Text.Trim
            .GPAddress = txtGPAddress.Text.Trim
            .GPPhone = txtGPTelephoneNumber.Text.Trim
            .GPPostCode = txtGPPostcode.Text.Trim
        End With
        dsEmergencyContact.EmergencyContact.Rows.Add(nrowEC)
        Return dsEmergencyContact

    End Function

#Region "EventHandler"

    ''' <summary>
    ''' Method to handle the reset functionality of the Bottom button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Private Sub btnResetBottom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetBottom.Click
    '    PopulateProfile(True)
    '    divValidationMain.Visible = False
    'End Sub

    ''' <summary>
    ''' Method to handle the reset functionality of the Top button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Private Sub btnResettOP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetTop.Click
    '    PopulateProfile(True)
    '    divValidationMain.Visible = False
    'End Sub


    ''' <summary>
    ''' Method to handle the cancel functionality of the Top Exit button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Private Sub btnCancelTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTop.Click
    '    Response.Redirect("Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId"))
    'End Sub

    ''' <summary>
    ''' Method to handle the cancel functionality of the Bottom Exit button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Private Sub btnCancelBottom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelBottom.Click
    '    Response.Redirect("Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId"))
    'End Sub

    ''' <summary>
    ''' Method to handle the save functionality of the Top save  button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click
        ValidatePage()
    End Sub

    ''' <summary>
    ''' Method to handle the save functionality of the Bottom save Exit button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSaveBottom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveBottom.Click
        ValidatePage()
    End Sub

    ''' <summary>
    ''' Method to be called when user checks/unchecks "Can fulfill Work Order alone" check box
    ''' </summary>
    ''' <param name="senser"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub chkFulfilWOs_Click(ByVal senser As System.Object, ByVal e As System.EventArgs) Handles chkFulfilWOs.CheckedChanged
        If chkFulfilWOs.Checked = False Then
            pnlAreaOfExpertise.Visible = False
        Else
            pnlAreaOfExpertise.Visible = True

        End If
    End Sub

    ''' <summary>
    ''' To show/hide security question and answer panel when chkEnableLogin is checked
    ''' </summary>
    ''' <param name="senser"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub chkEnableLogin_Click(ByVal senser As System.Object, ByVal e As System.EventArgs) Handles ChkEnableLogin.CheckedChanged

        'If ApplicationSettings.SiteType <> ApplicationSettings.siteTypes.admin And Session("RoleGroupID") <> ApplicationSettings.RoleSupplierAdminID And Session("RoleGroupID") <> ApplicationSettings.RoleClientAdminID Then
        'If Session("UserId") = ViewState("ContactID") Then
        If ChkEnableLogin.Checked = False Then
            pnlEnableDisableLogin.Visible = False
        Else
            pnlEnableDisableLogin.Visible = True
        End If
        divValidationMain.Visible = False

        'Else
        'pnlEnableDisableLogin.Visible = False
        ''   rqddlQuestion.Enabled = False

        'End If
    End Sub

    ''' <summary>
    ''' Action to perform when user select user type from the drop down list
    ''' </summary>
    ''' <param name="senser"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlUserType_select(ByVal senser As System.Object, ByVal e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged
        If ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierSpecialistID Or ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierAdminID Then
            'If ddlUserType.SelectedItem.Text = "Specialist" Then
            chkFulfilWOs.Checked = True
            chkFulfilWOs.Enabled = False
            pnlAreaOfExpertise.Visible = True
        Else
            chkFulfilWOs.Checked = False
            chkFulfilWOs.Enabled = True
            pnlAreaOfExpertise.Visible = False
            If ddlUserType.SelectedValue = ApplicationSettings.RoleClientDepotID Or ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierDepotID Then
                ChkEnableLogin.Checked = False
                ChkEnableLogin.Enabled = False
                ChkEmailVerified.Enabled = False
                ChkEmailVerified.Checked = False
            Else
                ChkEnableLogin.Enabled = True
                ChkEmailVerified.Enabled = True
            End If
        End If
        divValidationMain.Visible = False
    End Sub

    ''' <summary>
    ''' To handle confirm action
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Dim strMode As String
        If ViewState("ContactID") = 0 Then
            strMode = "add"
        Else
            strMode = "edit"
        End If

        'poonam modified on 9/2/2016 - Task - OA-178 : OA - Re-activated non-admin with email Verified unchecked user does not get Account Activiation workflow 
        If strMode = "edit" And ddlStatus.SelectedValue = ApplicationSettings.StatusActive Then
            If ViewState("UserStatus") = ApplicationSettings.StatusInActive Then
                'status is inactive user need to "ReActivate"   
                ChkEmailVerified.Checked = True
            End If
        End If

        Dim xmlContent As String = SaveProfile().GetXml
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)

        'Poonam modified on 30/12/2015 - Task - OA-112 : OA - Add Emergency Contact fields in User detail page
        Dim xmlEmergencyContact As String = ""
        If ((Request("classid") = ApplicationSettings.RoleSupplierID) Or ((Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleSupplierID)) Then
            xmlEmergencyContact = SaveEmergencycontacts().GetXml
            If Not IsNothing(xmlEmergencyContact) Then
                xmlEmergencyContact = xmlEmergencyContact.Remove(xmlEmergencyContact.IndexOf("xmlns"), xmlEmergencyContact.IndexOf(".xsd") - xmlEmergencyContact.IndexOf("xmlns") + 5)
            Else
                xmlEmergencyContact = ""
            End If
        End If
        'Dim dt As New DataTable
        'dt = CType(UCAccreds1.GetAccreds(), DataSet).Tables("tblSelectedAccreds")
        'Dim dsaccreds As New DataSet
        'dsaccreds.Tables.Add(dt.Copy)
        'Dim xmlaccred As String = dsaccreds.GetXml
        'Dim success As Boolean = ws.WSContact.AddEditSpecialist(xmlContent, ViewState("ContactID"), strMode, _bizDivID)

        '    If new email field is not empty then change the cachekey accordingly and session variable also
        If txtNewEmail.Text.Trim <> "" Then
            Dim ds As DataSet = CType(Cache("LoggedInfo-" & Session("UserName") & Session.SessionID), DataSet)
            Cache.Remove("LoggedInfo-" & Session("UserName") & Session.SessionID)
            Session("UserName") = Trim(txtNewEmail.Text)
            Cache.Add("LoggedInfo-" & Session("UserName") & Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        End If

        Dim success As Integer
        Dim IPhonePin As Integer = 0
        Dim VehicleRegNo As String = ""

        If (txtIphonePin.Text.Trim <> "") Then
            IPhonePin = CInt(txtIphonePin.Text.ToString.Trim)
        End If

        If (txtVehicleRegNo.Text.Trim <> "") Then
            VehicleRegNo = txtVehicleRegNo.Text
        End If
        Dim IsCVRequired As Boolean
        If chkIsCVRequired.Checked = True Then
            IsCVRequired = 0
        Else
            IsCVRequired = 1
        End If

        If txtDBSExpiryDate.Text.Trim() <> "Enter expiry date" And txtDBSExpiryDate.Text.Trim() <> "" Then
            txtDBSExpiryDate.Text = txtDBSExpiryDate.Text.Trim()
        End If

        If ViewState("page") = "MyProfile.aspx" Or strMode = "add" Then
            If strMode = "add" Then
                success = ws.WSContact.AddEditSpecialistAndChangeLoginInfo(xmlContent, ViewState("ContactID"), strMode, _bizDivID, Session("UserName"), Session("Password"), Session("NewPassword"), 0, "", Nothing, Session("UserID"), IPhonePin, hdnLocationsSelected.Value, "", xmlEmergencyContact, VehicleRegNo, IsCVRequired, txtDBSExpiryDate.Text.Trim(), False)
            Else
                success = ws.WSContact.AddEditSpecialistAndChangeLoginInfo(xmlContent, ViewState("ContactID"), strMode, _bizDivID, Session("UserName"), Session("Password"), Session("NewPassword"), Session("NewSecQues"), Session("NewSecAns"), Nothing, Session("UserID"), IPhonePin, hdnLocationsSelected.Value, "", "", VehicleRegNo, IsCVRequired, txtDBSExpiryDate.Text.Trim(), False)
            End If

        ElseIf ViewState("page") = "SpecialistsForm.aspx" And strMode = "edit" Then
            success = ws.WSContact.AddEditSpecialistAndChangeLoginInfo(xmlContent, ViewState("ContactID"), strMode, _bizDivID, "", "", "", 0, "", CommonFunctions.FetchVerNum(), Session("UserID"), IPhonePin, hdnLocationsSelected.Value, "", xmlEmergencyContact, VehicleRegNo, IsCVRequired, txtDBSExpiryDate.Text.Trim(), chkRecordVerifed.Checked)
        End If



            Dim ImageUrl As String = ""
        If ViewState("ImageUrl") <> "" Then
            ImageUrl = ViewState("ImageUrl")
        Else
            'ImageUrl = OrderWorkLibrary.ApplicationSettings.AttachmentDisplayPath(OrderWorkLibrary.ApplicationSettings.BizDivId) + "AttachmentsUK/ContactPhoto/" + "empty_profile_pic.png"
            ImageUrl = OrderWorkLibrary.ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/AttachmentsUK/ContactPhoto/empty_profile_pic.png" + CommonFunctions.AzureStorageAccessKey().ToString
        End If




        If hdnFlag.Value = 1 Then
            Dim dsNew As DataSet = ws.WSContact.UpdateContactImage(CInt(ViewState("ContactID")), "empty_profile_pic_" + ImageUrl)
            hdnFlag.Value = "0"
        Else
            Dim dsNew As DataSet = ws.WSContact.UpdateContactImage(CInt(ViewState("ContactID")), ImageUrl)

        End If





        '  Dim success As Integer = ws.WSContact.AddEditSpecialistAndChangeLoginInfo(xmlContent, ViewState("ContactID"), strMode, _bizDivID, Session("UserName"), Session("Password"), Session("NewPassword"), Session("NewSecQues"), Session("NewSecAns"))


        If success >= 1 Then

            Dim dsProfileCompletion As DataSet
            dsProfileCompletion = GetContact(False, True, ViewState("ContactID"))

            If dsProfileCompletion.Tables("Contacts").Rows.Count > 0 Then
                'Send Email to user and Recruiter if all the profile info is filled
                If Not IsDBNull(dsProfileCompletion.Tables("Contacts").Rows(0)("IsProfileCompleted")) Then
                    If (dsProfileCompletion.Tables("Contacts").Rows(0)("IsProfileCompleted") = 1) Then
                        'Send Email to user and Recruiter if all the profile info is filled
                        Emails.SendUserProfileCompletionMail(dsProfileCompletion.Tables("Contacts").Rows(0)("Email").ToString, dsProfileCompletion.Tables("Contacts").Rows(0)("FName").ToString)
                        Emails.SendRecruitmentProfileCompletionMail(dsProfileCompletion)
                        'Update IsProfileCompleted Info in ContactsAttributes table
                        Dim ds As DataSet = ws.WSContact.UpdateUserProfileCompletionMailInfo(ViewState("ContactID"), 1, Session("UserID"))
                    End If
                End If
            End If

            ''Update engg Rates
            UCEnggRateInfo.UpdateEnginerRates()
            '========================================================================================================================================

            'Dim success1 As Integer
            'success1 = ws.WSSecurity.ChangeLoginInfo(CInt(Session("UserId")), Session("UserName"), Session("Password"), Session("NewPassword"), Session("NewSecQues"), Session("NewSecAns"))
            'If success1 <> -1 Then

            Session("SecQues") = Session("NewSecQues")
            Session("SecAns") = Session("NewSecAns")
            Session("Password") = Session("NewPassword")
            Page.Session.Remove("NewPassword")
            Page.Session.Remove("NewSecQues")
            Page.Session.Remove("NewSecAns")
            pnlSubmitForm.Visible = False
            '  pnlSuccess.Visible = True
            pnlConfirm.Visible = False

            ' lblChangeLoginMsg.Text = ResourceMessageText.GetString("ChangeLoginSuccess")
            'End If

            '========================================================================================================================================


            Cache.Remove("Contact" & "-" & CStr(ViewState("ContactID")) & "-" & Session.SessionID)

            If ViewState("page") <> "MyProfile.aspx" Then

                'to send appropriate mail to the user
                sendMailToUser()
                Dim IsAccountInfoUpdated As Boolean = False
                Dim ChangedFirstName As String = ""
                Dim ChangedLastName As String = ""
                Dim ChangedEmail As String = ""
                Dim ChangedPhone As String = ""
                Dim AdminContactName As String = ""
                Dim AdminContactEmail As String = ""
                If Not IsNothing(ViewState("FirstName")) Then
                    If (ViewState("FirstName") <> txtFName.Text.Trim) Then
                        IsAccountInfoUpdated = True
                        ChangedFirstName = txtFName.Text.Trim
                    End If
                End If
                If Not IsNothing(ViewState("LastName")) Then
                    If (ViewState("LastName") <> txtLName.Text.Trim) Then
                        IsAccountInfoUpdated = True
                        ChangedLastName = txtLName.Text.Trim
                    End If
                End If
                If Not IsNothing(ViewState("Email")) Then
                    If (ViewState("Email") <> txtEmail.Text.Trim) Then
                        IsAccountInfoUpdated = True
                        ChangedEmail = txtEmail.Text.Trim
                    End If
                End If
                If Not IsNothing(ViewState("Phone")) Then
                    If (ViewState("Phone") <> txtPhone.Text.Trim) Then
                        IsAccountInfoUpdated = True
                        ChangedPhone = txtPhone.Text.Trim
                    End If
                End If
                If (IsAccountInfoUpdated = True) Then
                    If Not IsNothing(Session("CompanyAdminName")) Then
                        AdminContactName = Session("CompanyAdminName").ToString
                    Else
                        AdminContactName = ViewState("ContactName")
                    End If
                    If Not IsNothing(Session("CompanyAdminEmail")) Then
                        AdminContactEmail = Session("CompanyAdminEmail").ToString
                    Else
                        AdminContactEmail = ViewState("Email")
                    End If
                    Emails.SendAccountInfoUpdate(AdminContactName, AdminContactEmail, "", "", "", "", "", "", "", "", ChangedPhone, ChangedFirstName, ChangedLastName, ChangedEmail)
                    'Emails.SendAccountInfoUpdate("Sumit", "sumitk@iniquus.com", "", "", "", "", "", "", "", "", ChangedPhone, ChangedFirstName, ChangedLastName, ChangedEmail)
                End If
                'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                Response.Redirect("Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
            Else

                If Session("FirstName").ToString() <> txtFName.Text.Replace("""", "") Then
                    Session("FirstName") = txtFName.Text.Trim.Replace("""", "")
                End If
                If Session("LastName").ToString() <> txtLName.Text.Replace("""", "") Then
                    Session("LastName") = txtLName.Text.Trim.Replace("""", "")
                End If

                pnlSubmitForm.Visible = True
                pnlConfirm.Visible = False
            End If
        ElseIf success = -10 Then
            pnlSubmitForm.Visible = True
            pnlConfirm.Visible = False
            lblError.Text = ResourceMessageText.GetString("AccountsVersionControlMsg").Replace("<Link>", "SpecialistsForm.aspx?ContactId=" & ViewState("ContactID") & "&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId"))
            divValidationMain.Visible = True
        Else
            pnlSubmitForm.Visible = True
            pnlConfirm.Visible = False
            lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
            divValidationMain.Visible = True
            'lblError.Text = "There has been some problem connecting with server; please try again"

        End If

        If success <> -10 Then
            PopulateProfile(True)
        End If

    End Sub

    ''' <summary>
    ''' To handle cancel action on confirm
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'UCAccreds1.PopulateVendors(True)
        'UCAccreds1.PopulateCertificates(0)
        'UCAccreds1.PopulateSelectedGrid()

        UCAccreditations.populateAccred()
        UCAccreditations.PopulateSelectedGrid(SortDirection.Ascending, "FullName")
        UCAccreditations.Type = "EngineerAccreditations"

        pnlSubmitForm.Visible = True
        pnlConfirm.Visible = False
    End Sub

    ''' <summary>
    ''' Checking Depot Location
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckDepotLocation() As Boolean
        Dim IsDepot As Boolean
        IsDepot = False
        If ddlLocation.SelectedValue <> "" Then
            Dim ds As New DataSet
            ds = ws.WSContact.CheckDepotLocation(CType(ddlLocation.SelectedValue, Integer))
            If ds.Tables(0).Rows.Count > 0 Then
                'If selected location is a depot location
                If ds.Tables(0).Rows(0)("IsDepot") = True And ds.Tables(0).Rows(0)("IsBilling") = False Then
                    'If not then check if the user is not depot
                    If ddlUserType.SelectedValue <> ApplicationSettings.RoleClientDepotID And ddlUserType.SelectedValue <> ApplicationSettings.RoleSupplierDepotID Then
                        IsDepot = True
                        divValidationMain.Visible = True
                        lblError.Text = "Only a user of role type Depot can be assigned to this location."
                    End If
                ElseIf ds.Tables(0).Rows(0)("IsDepot") = True And ds.Tables(0).Rows(0)("IsBilling") = True Then
                    If ddlUserType.SelectedValue <> ApplicationSettings.RoleClientDepotID And ddlUserType.SelectedValue <> ApplicationSettings.RoleSupplierDepotID And ddlUserType.SelectedValue <> ApplicationSettings.RoleClientFinanceID And ddlUserType.SelectedValue <> ApplicationSettings.RoleSupplierFinanceID Then
                        IsDepot = True
                        divValidationMain.Visible = True
                        lblError.Text = "Only a user of role type Depot/Finance can be assigned to this location."
                    End If
                Else
                    If ddlUserType.SelectedValue = ApplicationSettings.RoleClientDepotID Or ddlUserType.SelectedValue = ApplicationSettings.RoleSupplierDepotID Then
                        IsDepot = True
                        divValidationMain.Visible = True
                        lblError.Text = "The following user must be associated with a location tagged as a Depot."
                    End If
                End If
            End If
        Else
            IsDepot = True
            divValidationMain.Visible = True
            lblError.Text = "Please select a location tagged as a Depot."
        End If
        Return IsDepot
    End Function

#End Region

#Region "Validators"

    ''' <summary>
    ''' Check if email already exist in system; if yes then show appropriate message
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckNewEmailExist() As Boolean
        Dim loginexists As Boolean
        If txtEmail.Text.Trim <> "" Then
            If ws.WSContact.DoesLoginExist(txtEmail.Text.Trim) Then
                If txtEmail.Text.Trim <> Session("EmailAddr") Then
                    divValidationMain.Visible = True
                    lblError.Text = ResourceMessageText.GetString("EmailExistSorry")
                    loginexists = True
                End If
            End If
        End If
        Return loginexists
    End Function



    ''' <summary>
    ''' Check if administrator has email defined and has login enabled.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckEmailForAdmin() As Boolean
        If ddlUserType.SelectedItem.Text = "Administrator" And txtEmail.Text.Trim = "" And ChkEnableLogin.Checked = False Then
            lblError.Text = "An admnistrator type user must have an email defined and must have login enabled. "
            divValidationMain.Visible = True
            Return False
        Else
            Return True
        End If
    End Function


    ''' <summary>
    ''' When any user's status is set as "INactive" then verify the condition 1. Atleast one person should be able to recive WO notfication 2. Atleast one person should be able to Fulfill WO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckSettingsOnInactive() As Boolean
        Dim flag As Boolean = True
        If ddlStatus.SelectedValue = ApplicationSettings.StatusInActive Then
            Dim ds As DataSet
            If chkRecvNotif.Visible Then
                If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                    ds = ws.WSContact.GetContactSettings(Request("companyid"), ViewState("ContactID"))
                Else
                    ds = ws.WSContact.GetContactSettings(Session("CompanyID"), ViewState("ContactID"))
                End If
                If ds.Tables(0).Rows.Count = 0 Then
                    divValidationMain.Visible = True
                    lblError.Text = ResourceMessageText.GetString("OneActivePersonWONotif")
                    flag = False
                End If
            End If
            If chkFulfilWOs.Visible Then
                If ds.Tables(1).Rows.Count = 0 Then
                    divValidationMain.Visible = True
                    lblError.Text = lblError.Text & "<br>" & ResourceMessageText.GetString("OneActivePersonFulfillWO")
                    flag = False
                End If
            End If
        End If

        Return flag
    End Function

    ''' <summary>
    ''' To verify the condition 1. Atleast one person should be able to recive WO notfication 2. Atleast one person should be able to Fulfill WO.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function FulFillSettingsForWO() As Boolean
        Dim flag As Boolean = True
        If chkRecvNotif.Checked = False Or chkFulfilWOs.Checked = False Then
            Dim ds As DataSet
            Dim dsView As DataView

            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                ds = ws.WSContact.GetContactSettings(Request("companyid"), ViewState("ContactID"))
            Else
                ds = ws.WSContact.GetContactSettings(Session("CompanyID"), ViewState("ContactID"))
            End If
            If chkRecvNotif.Checked = False And chkRecvNotif.Visible = True Then
                If ds.Tables(0).Rows.Count = 0 Then
                    divValidationMain.Visible = True
                    lblError.Text = ResourceMessageText.GetString("OneActivePersonWONotif")
                    flag = False
                End If
            End If
            If ((Request("classid") = ApplicationSettings.RoleSupplierID) Or ((Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleSupplierID)) Then
                If (ViewState("CompStatus") <> ApplicationSettings.StatusNewCompany) Then
                    If chkFulfilWOs.Checked = False And chkFulfilWOs.Visible = True Then
                        If ds.Tables(1).Rows.Count = 0 Then
                            divValidationMain.Visible = True
                            lblError.Text = lblError.Text & "<br>" & ResourceMessageText.GetString("OneActivePersonFulfillWO")
                            flag = False
                        End If
                    End If
                End If
            Else
                If chkFulfilWOs.Checked = False And chkFulfilWOs.Visible = True Then
                    If ds.Tables(1).Rows.Count = 0 Then
                        divValidationMain.Visible = True
                        lblError.Text = lblError.Text & "<br>" & ResourceMessageText.GetString("OneActivePersonFulfillWO")
                        flag = False
                    End If
                End If
            End If

        End If
        Return flag
    End Function

    ''' <summary>
    ''' Server Side Custom validation method to handle if main location exist and to show appropriate message
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Private Sub MainContactExists_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles MainContactExists.ServerValidate
        If chkIsMain.Checked = True And ddlLocation.SelectedValue <> "" Then
            Dim dsLocation As DataSet
            Dim dsView As DataView
            Dim drv As DataRowView
            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                dsLocation = ws.WSContact.GetLocationName(Request("companyid"))
            Else
                dsLocation = ws.WSContact.GetLocationName(Session("CompanyId"))
            End If

            'dsLocation = ws.WSContact.GetLocationName(Session("CompanyId"))
            dsView = dsLocation.Tables(0).Copy.DefaultView
            dsView.RowFilter = "AddressId = " + ddlLocation.SelectedValue
            For Each drv In dsView
                If Not IsDBNull(drv.Item("MainContactID")) Then
                    If drv.Item("MainContactID") = ViewState("ContactID") Then
                        args.IsValid = True
                    Else
                        args.IsValid = False
                    End If
                End If
            Next

        End If
    End Sub

    ''' <summary>
    ''' Server side validation to check if "Recieev WO notification" is checked then email field is required
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Public Sub ReceiveNotifAddr_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles ReceiveNotifAddr.ServerValidate
        If chkRecvNotif.Checked = True And (txtEmail.Text = "" Or ChkEnableLogin.Checked = False) Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    ''' <summary>
    ''' Server side validation method to handle validation for security Q/A when enable Enable Login is checked
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Public Sub rqUserFields_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rqUserFields.ServerValidate
        If ChkEnableLogin.Checked Then
            'If pnlEnableDisableLogin.Visible = True Then
            If ddlQuestion.SelectedIndex <> 0 And txtAnswer.Text.Trim <> "" And txtEmail.Text.Trim <> "" Then
                args.IsValid = True
                lblError.Text = ""
            Else
                If txtEmail.Text.Trim = "" Then
                    lblError.Text = "<ul><li>" & ResourceMessageText.GetString("EnterEmail") & "</li>"
                End If
                If lblError.Text <> "" Then
                    lblError.Text = lblError.Text & "</ul>"
                    args.IsValid = False
                End If
            End If

        End If

    End Sub

    ''' <summary>
    ''' Server side validation method to check if "Subscribe Newsletter" 'is checked then mail should be entered
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Public Sub rqSubscribe_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rqSubscribe.ServerValidate
        If ChkSubscribe.Checked Then
            If txtEmail.Text.Trim = "" Then
                args.IsValid = False
            Else
                args.IsValid = True
            End If
        End If
    End Sub


    ''' <summary>
    ''' Custom validation to check for enter of old password if user enteres new password.
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Public Sub OldPassword_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles custChangePassword.ServerValidate
        If txtPassword.Text.Trim <> "" Then
            If txtOldPassword.Text.Trim <> "" Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        End If
    End Sub

    Public Sub custNewEmail_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles custNewEmail.ServerValidate
        If txtNewEmail.Text.Trim <> "" Then
            If txtEmailConfirm.Text.Trim <> "" Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        End If
    End Sub

#End Region

#Region "SendMail"

    Private Sub sendMailToUser()

        Dim hashDetails As New Hashtable
        hashDetails.Add("AdminFName", Session("FirstName"))
        hashDetails.Add("AdminLName", Session("LastName"))
        hashDetails.Add("CompanyName", Session("CompanyName"))
        hashDetails.Add("UserFName", txtFName.Text.Trim.Replace("""", ""))
        hashDetails.Add("UserLName", txtLName.Text.Trim.Replace("""", ""))
        hashDetails.Add("UserEmail", txtEmail.Text.Trim)
        hashDetails.Add("UserRoleGroup", ddlUserType.SelectedItem.Text)
        hashDetails.Add("NewsLetter", ChkSubscribe.Checked)



        hashDetails.Add("UserSecurityQues", ddlQuestion.SelectedItem.Text)
        hashDetails.Add("UserSecurityAns", txtAnswer.Text.Trim)


        If Not IsNothing(Session("ContactClassID")) Then
            If Session("ContactClassID") = ApplicationSettings.RoleClientID Then
                hashDetails.Add("userType", ResourceMessageText.GetString("Buyer"))
            ElseIf Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
                hashDetails.Add("userType", ResourceMessageText.GetString("Supplier"))
            End If
        End If






        Dim topmessage As String
        If ViewState("mode") = "add" Then
            If ChkEnableLogin.Checked Then
                'Session("AccountStatus") = "Activate"
                topmessage = ResourceMessageText.GetString("MailCongratulateString") & " " & _
                                       hashDetails.Item("AdminFName") & " " & hashDetails.Item("AdminLName") & " " & _
                                            ResourceMessageText.GetString("MailBodyActivate")
                hashDetails.Add("UserPassword", ViewState("ContactPassword"))
                Emails.SendUserStatusMail("Activate", hashDetails, _
                                        ResourceMessageText.GetString("MailSubjectActivate"), topmessage, BizDivID)
            End If
        Else
            'In Edit Mode
            If ddlStatus.SelectedValue = ApplicationSettings.StatusActive Then
                If ViewState("UserStatus") = ApplicationSettings.StatusInActive Then
                    'status is inactive
                    ' Session("AccountStatus") = "ReActivate"   
                    topmessage = ResourceMessageText.GetString("MailCongratulateString") & " " & _
                                    hashDetails.Item("AdminFName") & " " & hashDetails.Item("AdminLName") & " " & _
                                         ResourceMessageText.GetString("MailBodyReActivate")
                    hashDetails.Add("UserPassword", "")
                    Emails.SendUserStatusMail("ReActivate", hashDetails, _
                                        ResourceMessageText.GetString("MailSubjectActivate"), topmessage, BizDivID)
                Else
                    If ChkEnableLogin.Checked <> ViewState("EnableLogin") And ChkEnableLogin.Checked = True Then
                        'Session("AccountStatus") = "EnableLogin"
                        topmessage = ResourceMessageText.GetString("MailCongratulateString") & " " & _
                                            hashDetails.Item("AdminFName") & " " & hashDetails.Item("AdminLName") & " " & _
                                            ResourceMessageText.GetString("MailBodyEnableLogin")
                        'Poonam modified on 19/1/2016 - Task - OA-169 : Reisnstated non-admin user does not get password in new account creation email
                        hashDetails.Add("UserPassword", ViewState("ContactPassword"))
                        Emails.SendUserStatusMail("EnableLogin", hashDetails, _
                                        ResourceMessageText.GetString("MailSubjectEnableLogin"), topmessage, BizDivID)
                    End If
                End If

            ElseIf ddlStatus.SelectedValue = ApplicationSettings.StatusInActive Then

                If ViewState("UserStatus") = ApplicationSettings.StatusActive Then
                    'status was active
                    'Session("AccountStatus") = "InActivate"
                    'Session("AccountStatus") = "EnableLogin"
                    topmessage = "Dear " & txtFName.Text.Trim.Replace("""", "") & " " & txtLName.Text.Trim.Replace("""", "")

                    hashDetails.Add("UserPassword", ViewState("ContactPassword"))
                    Emails.SendUserStatusMail("InActivate", hashDetails, _
                                        ResourceMessageText.GetString("MailSubjectInActivate"), topmessage, BizDivID)
                End If

            End If
        End If



    End Sub

#End Region




    ''' <summary>
    ''' To check/ uncheck the enable login and mark as deleted check boxes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
            If ddlStatus.SelectedItem.Text = "Active" Then
                ChkEnableLogin.Checked = True
                chkIsDeleted.Checked = False
            ElseIf ddlStatus.SelectedItem.Text = "Inactive" Then
                ChkEnableLogin.Checked = False
                chkIsDeleted.Checked = True
            End If
        End If
    End Sub




    Private Sub CRBCheckedNo_CheckedChanged(sender As Object, e As System.EventArgs) Handles CRBCheckedNo.CheckedChanged
        If CRBCheckedNo.Checked = True Then
            regCRBCheckDate.Enabled = False
        Else
            regCRBCheckDate.Enabled = True
        End If
    End Sub

    Private Sub CRBCheckedEnhanced_CheckedChanged(sender As Object, e As System.EventArgs) Handles CRBCheckedEnhanced.CheckedChanged
        If CRBCheckedEnhanced.Checked = True Then
            regCRBCheckDate.Enabled = True
        End If
    End Sub

    Private Sub CRBCheckedYes_CheckedChanged(sender As Object, e As System.EventArgs) Handles CRBCheckedYes.CheckedChanged
        If CRBCheckedYes.Checked = True Then
            regCRBCheckDate.Enabled = True
        End If
    End Sub

    Public Sub rngDate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rngDate.ServerValidate
        If txtDBSExpiryDate.Text.Trim <> "" And txtDBSExpiryDate.Text.Trim <> "Enter expiry date" Then

            If txtDBSExpiryDate.Text.Trim <> "" And txtDBSExpiryDate.Text.Trim <> "Enter expiry date" Then
                Dim DateStart As New Date
                Dim DateEnd As New Date
                DateStart = System.DateTime.Now.Date
                DateEnd = txtDBSExpiryDate.Text.Trim
                If DateStart <= DateEnd Then
                    args.IsValid = True
                Else
                    args.IsValid = False
                    rngDate.Style.Add("display", "")
                    DivCRBCheckCalender.Style.Add("display", "")
                    DivCRBCheckFileUpload.Style.Add("display", "")
                End If
            Else
                args.IsValid = False
                rngDate.Style.Add("display", "")
                DivCRBCheckCalender.Style.Add("display", "")
                DivCRBCheckFileUpload.Style.Add("display", "")
            End If
        End If
    End Sub
End Class