Imports System.IO

Partial Public Class UCFileUpload_Inner
    Inherits System.Web.UI.UserControl

    Private _ID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property


    Private _type As String
    Public Property Type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property


    Private _attachlinkSource As String
    Public Property AttachLinkSource() As String
        Get
            Return _attachlinkSource
        End Get
        Set(ByVal value As String)
            _attachlinkSource = value
        End Set
    End Property



    Shared ws As New WSObjs


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblMsg.Text = ""
        End If

        Dim uploadName As System.Web.UI.WebControls.FileUpload = CType(FindControl("FileUpload1"), System.Web.UI.WebControls.FileUpload)
    End Sub

    'Protected Sub btnUpload_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click

    '    If IsValidFilesExtension() Then
    '        lblMsg.Text = ""
    '        _type = Request("Type")
    '        Dim uploadName As System.Web.UI.WebControls.FileUpload
    '        uploadName = CType(FindControl("FileUpload1"), System.Web.UI.WebControls.FileUpload)

    '        _ID = Request("id")
    '        _attachlinkSource = Request("AttachLinkSource")

    '        If Not (uploadName Is Nothing) Then
    '            If uploadName.HasFile Then
    '                If (_attachlinkSource.ToString.ToLower = "wocomplete" And _type.ToString.ToLower = "workorder") Then
    '                    If uploadName.PostedFile.ContentLength > ApplicationSettings.MaxFileUploadSizeSignOffSheet Then
    '                        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "FileMaxLimit", "alert('" & ResourceMessageText.GetString("FileUploadedMaxLimitSignOffSheet") & "');", True)
    '                        Exit Sub
    '                    End If
    '                Else
    '                    If uploadName.PostedFile.ContentLength > ApplicationSettings.MaxFileUploadSize Then
    '                        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "FileMaxLimit", "alert('" & ResourceMessageText.GetString("FileUploadedMaxLimit") & "');", True)
    '                        Exit Sub
    '                    End If
    '                End If


    '                Dim fileName As String
    '                Dim filestart As String = Request("fileStart")

    '                If _type = "" Then
    '                    _type = "Company"
    '                Else
    '                    _type = _type
    '                End If

    '                'Dim fileWOExtn As String()
    '                Dim ext As String

    '                'fileWOExtn = uploadName.FileName.Split(".")
    '                'fileName = fileWOExtn(0)
    '                'ext = fileWOExtn(1)

    '                ext = System.IO.Path.GetExtension(uploadName.FileName).ToString.Replace(".", "")
    '                fileName = uploadName.FileName.Replace(ext, "").Replace(".", "")

    '                Dim index As Integer = 1
    '                Dim newFileName As String

    '                Dim storePath As String
    '                Dim uploadPath As String = FileUpload1.PostedFile.FileName
    '                Dim strFolder As String = "D:\home\site\wwwroot\Attachments\"
    '                Dim strFileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)

    '                If _type = "Logo" Then
    '                    storePath = ApplicationSettings.AttachmentUploadPath(0)
    '                ElseIf _type = "CompanyLogo" Then
    '                    storePath = ApplicationSettings.AttachmentUploadPath(Request("bizdivid")) + _type
    '                ElseIf _type = "Press Release" Then
    '                    storePath = ApplicationSettings.WebContentUpload(ApplicationSettings.WebContentFolder.PressRelease)
    '                ElseIf _type = "Quotation" Then
    '                    storePath = ApplicationSettings.WebContentUpload(ApplicationSettings.WebContentFolder.Logo)
    '                ElseIf _type = "Articles" Then
    '                    storePath = ApplicationSettings.WebContentUpload(ApplicationSettings.WebContentFolder.Articles)
    '                ElseIf _type = "UserSkillSet" Then
    '                    storePath = ApplicationSettings.AttachmentUploadPath("1") + _type
    '                    newFileName = filestart & "_" & Date.Now.ToString("ddMMyyyy") & "_" & index.ToString & "." & ext

    '                    While System.IO.File.Exists(ApplicationSettings.AttachmentUploadPath("1") + _type + "/" + newFileName)
    '                        index = index + 1
    '                        newFileName = filestart & "_" & Date.Now.ToString("ddMMyyyy") & "_" & index.ToString & "." & ext
    '                    End While

    '                    If FileUpload1.PostedFile.FileName <> "" Then

    '                        If Not Directory.Exists(strFolder) Then
    '                            Directory.CreateDirectory(strFolder)
    '                        End If

    '                        Dim strFilePath As String = strFolder & newFileName

    '                        If File.Exists(strFilePath) Then
    '                            'lblUploadResult.Text = strFileName & " already exists on the server!"
    '                        Else
    '                            FileUpload1.PostedFile.SaveAs(strFilePath)
    '                            'lblUploadResult.Text = strFileName & " has been successfully uploaded."
    '                        End If
    '                    Else
    '                        'lblUploadResult.Text = "Click 'Browse' to select the file to upload."
    '                    End If

    '                    'frmConfirmation.Visible = True
    '                    AzureFileUpload.UploadFileThroughFileDir(uploadPath, newFileName, storePath)

    '                ElseIf _attachlinkSource.ToString.ToLower = "wocomplete" Then
    '                    storePath = ApplicationSettings.AttachmentUploadPath(Request("bizdivid")) + _type
    '                    newFileName = Replace(filestart, "_WOComplete", "") & "_" & Date.Now.ToString("ddMMyyyy") & "_" & _ID & "_" & index.ToString & "." & ext
    '                    While System.IO.File.Exists(ApplicationSettings.AttachmentUploadPath(Request("bizdivid")) + _type + "\" + newFileName)
    '                        newFileName = Replace(filestart, "_WOComplete", "") & "_" & Date.Now.ToString("ddMMyyyy") & "_" & _ID & "_" & index.ToString & "." & ext
    '                        index = index + 1
    '                    End While

    '                Else
    '                    storePath = ApplicationSettings.AttachmentUploadPath(Request("bizdivid")) + _type
    '                    newFileName = filestart & "_" & Date.Now.ToString("ddMMyyyy") & "_" & index.ToString & "." & ext

    '                    While System.IO.File.Exists(ApplicationSettings.AttachmentUploadPath(Request("bizdivid")) + _type + "\" + newFileName)
    '                        index = index + 1
    '                        newFileName = filestart & "_" & Date.Now.ToString("ddMMyyyy") & "_" & index.ToString & "." & ext
    '                    End While

    '                End If

    '                If _type = "Logo" Or _type = "Press Release" Or _type = "Quotation" Or _type = "Articles" Then
    '                    newFileName = fileName & "." & ext

    '                    While System.IO.File.Exists(storePath + "\" + newFileName)
    '                        index = index + 1
    '                        newFileName = fileName & "_" & Date.Now.ToString("ddMMyyyy") & "_" & index.ToString & "." & ext
    '                    End While



    '                ElseIf _type = "CompanyLogo" Then
    '                    newFileName = fileName & "." & ext

    '                    While System.IO.File.Exists(storePath + "\" + newFileName)
    '                        index = index + 1
    '                        newFileName = fileName & "_" & Date.Now.ToString("ddMMyyyy") & "_" & index.ToString & "." & ext
    '                    End While
    '                    storePath = storePath + "\" + newFileName
    '                    FileUpload1.SaveAs(storePath)
    '                ElseIf _type = "WorkOrder" Then
    '                    FileUpload1.SaveAs(storePath)
    '                End If

    '                If _type = "CompanyLogo" Then
    '                    newFileName = fileName & "_" & Date.Now.ToString("ddMMyyyy") & "_" & index.ToString & "." & ext
    '                    storePath = "OWMyUK/AttachmentsUK/CompanyLogo"

    '                    uploadPath = FileUpload1.PostedFile.FileName
    '                    strFolder = "D:\home\site\wwwroot\Attachments\"
    '                    strFileName  = Path.GetFileName(FileUpload1.PostedFile.FileName)

    '                    If FileUpload1.PostedFile.FileName <> "" Then

    '                        If Not Directory.Exists(strFolder) Then
    '                            Directory.CreateDirectory(strFolder)
    '                        End If

    '                        Dim strFilePath As String = strFolder & newFileName

    '                        If File.Exists(strFilePath) Then
    '                            'lblUploadResult.Text = strFileName & " already exists on the server!"
    '                        Else
    '                            FileUpload1.PostedFile.SaveAs(strFilePath)
    '                            'lblUploadResult.Text = strFileName & " has been successfully uploaded."
    '                        End If
    '                    Else
    '                        'lblUploadResult.Text = "Click 'Browse' to select the file to upload."
    '                    End If

    '                    'frmConfirmation.Visible = True
    '                    AzureFileUpload.UploadFileThroughFileDir(uploadPath, newFileName, storePath)
    '                    storePath = storePath + "\" + newFileName
    '                End If

    '                Dim dsView As DataView
    '                dsView = GetAttachedFilesDV(Request("cachekey"))

    '                Dim drow As DataRow = dsView.Table.NewRow

    '                drow("AttachmentID") = 0
    '                drow("Type") = uploadName.PostedFile.ContentType
    '                If _attachlinkSource.ToString.ToLower = "wocomplete" Then
    '                    drow("Name") = newFileName
    '                Else
    '                    ' drow("Name") = uploadName.FileName
    '                    drow("Name") = fileName & "." & ext
    '                End If

    '                drow("FilePath") = storePath 'ApplicationSettings.AttachmentUploadPath(Request("bizdivid")) + _type + "\" + newFileName
    '                drow("FileSize") = uploadName.PostedFile.ContentLength
    '                drow("DateCreated") = Date.Now
    '                drow("LinkID") = _ID
    '                drow("LinkSource") = _attachlinkSource
    '                dsView.Table.Rows.Add(drow)

    '                If _type = "WorkOrder" And _attachlinkSource.ToString.ToLower = "wocomplete" And Request("Control") = "UCFileUpload1" Then
    '                    Page.ClientScript.RegisterStartupScript(Page.GetType, "FileUploaded", "<script language='javascript'>if(!document.all){alert('" & ResourceMessageText.GetString("FileUploadedSuccessfully") & "');}this.parent.document.getElementById('" & Request("Control").Replace("$", "_") & "_btnJavaClickToPopulateAttachFile').click();</script>")
    '                Else
    '                    Dim ct As String = "ctl00_ContentPlaceHolder1_"
    '                    Page.ClientScript.RegisterStartupScript(Page.GetType, "FileUploaded", "<script language='javascript'>if(!document.all){alert('" & ResourceMessageText.GetString("FileUploadedSuccessfully") & "');}this.parent.document.getElementById('" & ct & Request("Control").Replace("$", "_") & "_btnJavaClickToPopulateAttachFile').click();</script>")
    '                End If
    '            End If
    '        End If
    '    Else
    '        Page.ClientScript.RegisterStartupScript(Page.GetType, "InvalidFile", "<script language='javascript'>alert('" & ResourceMessageText.GetString("ValidFilesToUpload") & "');</script>")
    '        Exit Sub
    '    End If

    'End Sub

    Protected Sub btnUpload_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        If IsValidFilesExtension() Then
            lblMsg.Text = ""
            _type = Request("Type")
            Dim uploadName As System.Web.UI.WebControls.FileUpload
            uploadName = CType(FindControl("FileUpload1"), System.Web.UI.WebControls.FileUpload)

            _ID = Request("id")
            _attachlinkSource = Request("AttachLinkSource")

            If Not (uploadName Is Nothing) Then
                If uploadName.HasFile Then
                    If (AttachLinkSource.ToString.ToLower = "wocomplete" And Type.ToString.ToLower = "workorder") Then
                        If uploadName.PostedFile.ContentLength > ApplicationSettings.MaxFileUploadSizeSignOffSheet Then
                            Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "FileMaxLimit", "alert('" & ResourceMessageText.GetString("FileUploadedMaxLimitSignOffSheet") & "');", True)
                            Exit Sub
                        End If
                    Else
                        If uploadName.PostedFile.ContentLength > ApplicationSettings.MaxFileUploadSize Then
                            Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "FileMaxLimit", "alert('" & ResourceMessageText.GetString("FileUploadedMaxLimit") & "');", True)
                            Exit Sub
                        End If
                    End If


                    Dim fileName As String
                    Dim filestart As String = Request("fileStart")

                    If _type = "" Then
                        _type = "Company"
                    Else
                        Type = Type
                    End If

                    'Dim fileWOExtn As String()
                    Dim ext As String

                    'fileWOExtn = uploadName.FileName.Split(".")
                    'fileName = fileWOExtn(0)
                    'ext = fileWOExtn(1)

                    ext = System.IO.Path.GetExtension(uploadName.FileName).ToString.Replace(".", "")
                    fileName = uploadName.FileName.Replace(ext, "").Replace(".", "")

                    'Dim index As Integer = 1
                    Dim index As String = Convert.ToString(Guid.NewGuid()).Substring(0, 3)
                    Dim newFileName As String

                    Dim storePath As String
                    Dim uploadPath As String = FileUpload1.PostedFile.FileName
                    Dim strFileName As String
                    Dim strFilePath As String
                    Dim strFolder As String
                    strFolder = "D:\home\site\wwwroot\Attachments\"
                    strFileName = FileUpload1.PostedFile.FileName
                    strFileName = Path.GetFileName(strFileName)
                    newFileName = fileName & "_" & Date.Now.ToString("ddMMyyyy") & "_" & index.ToString & "." & ext

                    If _type = "Logo" Then
                        storePath = ApplicationSettings.AttachmentUploadPath(0) + _type
                    ElseIf _type = "CompanyLogo" Then
                        storePath = ApplicationSettings.AttachmentUploadPath(Request("bizdivid")) + _type
                    ElseIf _type = "Press Release" Then
                        storePath = ApplicationSettings.WebContentUpload(ApplicationSettings.WebContentFolder.PressRelease)
                    ElseIf _type = "Quotation" Then
                        storePath = ApplicationSettings.WebContentUpload(ApplicationSettings.WebContentFolder.Logo)
                    ElseIf _type = "Articles" Then
                        storePath = ApplicationSettings.WebContentUpload(ApplicationSettings.WebContentFolder.Articles)
                    ElseIf _type = "UserSkillSet" Then
                        storePath = ApplicationSettings.AttachmentUploadPath("1") + _type
                    ElseIf _attachlinkSource.ToString.ToLower = "wocomplete" Then
                        storePath = ApplicationSettings.AttachmentUploadPath(Request("bizdivid")) + _type
                        newFileName = Replace(filestart, "_WOComplete", "") & "_" & Date.Now.ToString("ddMMyyyy") & "_" & _ID & "_" & index.ToString & "." & ext
                    Else
                        storePath = ApplicationSettings.AttachmentUploadPath(Request("bizdivid")) + _type
                    End If

                    If FileUpload1.PostedFile.FileName <> "" Then

                        If Not Directory.Exists(strFolder) Then
                            Directory.CreateDirectory(strFolder)
                        End If

                        strFilePath = strFolder & newFileName

                        If File.Exists(strFilePath) Then
                            'lblUploadResult.Text = strFileName & " already exists on the server!"
                        Else
                            FileUpload1.PostedFile.SaveAs(strFilePath)
                            'lblUploadResult.Text = strFileName & " has been successfully uploaded."
                        End If
                    Else
                        'lblUploadResult.Text = "Click 'Browse' to select the file to upload."
                    End If

                    'frmConfirmation.Visible = True
                    AzureFileUpload.UploadFileThroughFileDir(uploadPath, newFileName, storePath)
                    storePath = storePath + "\" + newFileName

                    'If type = "Logo" Or type = "Press Release" Or type = "Quotation" Or type = "Articles" Then
                    ' newFileName = fileName & "." & ext

                    ' While System.IO.File.Exists(storePath + "\" + newFileName)
                    ' index = index + 1
                    ' newFileName = fileName & "_" & Date.Now.ToString("ddMMyyyy") & "_" & index.ToString & "." & ext
                    ' End While
                    ' storePath = storePath + newFileName
                    ' FileUpload1.SaveAs(storePath)
                    'ElseIf _type = "CompanyLogo" Then
                    ' newFileName = fileName & "." & ext

                    ' While System.IO.File.Exists(storePath + "\" + newFileName)
                    ' index = index + 1
                    ' newFileName = fileName & "_" & Date.Now.ToString("ddMMyyyy") & "_" & index.ToString & "." & ext
                    ' End While
                    ' storePath = storePath + "\" + newFileName
                    ' FileUpload1.SaveAs(storePath)
                    'ElseIf _type = "WorkOrder" Then
                    ' FileUpload1.SaveAs(storePath)
                    'End If
                    'If _type = "CompanyLogo" Then
                    ' newFileName = fileName & "_" & Date.Now.ToString("ddMMyyyy") & "_" & index.ToString & "." & ext
                    ' storePath = "OWMyUK/AttachmentsUK/CompanyLogo"

                    ' If FileUpload1.PostedFile.FileName <> "" Then

                    ' If Not Directory.Exists(strFolder) Then
                    ' Directory.CreateDirectory(strFolder)
                    ' End If

                    ' strFilePath = strFolder & newFileName

                    ' If File.Exists(strFilePath) Then
                    ' 'lblUploadResult.Text = strFileName & " already exists on the server!"
                    ' Else
                    ' FileUpload1.PostedFile.SaveAs(strFilePath)
                    ' 'lblUploadResult.Text = strFileName & " has been successfully uploaded."
                    ' End If
                    ' Else
                    ' 'lblUploadResult.Text = "Click 'Browse' to select the file to upload."
                    ' End If

                    ' 'frmConfirmation.Visible = True
                    ' AzureFileUpload.UploadFileThroughFileDir(uploadPath, newFileName, storePath)
                    ' storePath = storePath + "\" + newFileName
                    'End If

                    Dim dsView As DataView
                    dsView = GetAttachedFilesDV(Request("cachekey"))

                    Dim drow As DataRow = dsView.Table.NewRow

                    drow("AttachmentID") = 0
                    drow("Type") = uploadName.PostedFile.ContentType
                    If _attachlinkSource.ToString.ToLower = "wocomplete" Then
                        drow("Name") = newFileName
                    Else
                        ' drow("Name") = uploadName.FileName
                        drow("Name") = fileName & "." & ext
                    End If

                    drow("FilePath") = storePath 'ApplicationSettings.AttachmentUploadPath(Request("bizdivid")) + _type + "\" + newFileName
                    drow("FileSize") = uploadName.PostedFile.ContentLength
                    drow("DateCreated") = Date.Now
                    drow("LinkID") = _ID
                    drow("LinkSource") = _attachlinkSource
                    dsView.Table.Rows.Add(drow)

                    If Type = "WorkOrder" And AttachLinkSource.ToString.ToLower = "wocomplete" And Request("Control") = "UCFileUpload1" Then
                        Page.ClientScript.RegisterStartupScript(Page.GetType, "FileUploaded", "<script language='javascript'>if(!document.all){alert('" & ResourceMessageText.GetString("FileUploadedSuccessfully") & "');}this.parent.document.getElementById('" & Request("Control").Replace("$", "_") & "_btnJavaClickToPopulateAttachFile').click();</script>")
                    Else
                        Dim ct As String = "ctl00_ContentPlaceHolder1_"
                        Page.ClientScript.RegisterStartupScript(Page.GetType, "FileUploaded", "<script language='javascript'>if(!document.all){alert('" & ResourceMessageText.GetString("FileUploadedSuccessfully") & "');}this.parent.document.getElementById('" & ct & Request("Control").Replace("$", "_") & "_btnJavaClickToPopulateAttachFile').click();</script>")
                    End If
                End If
            End If
        Else
            Page.ClientScript.RegisterStartupScript(Page.GetType, "InvalidFile", "<script language='javascript'>alert('" & ResourceMessageText.GetString("ValidFilesToUpload") & "');</script>")
            Exit Sub
        End If

    End Sub


    ''' <summary>
    ''' Function to get attachment from the database and store in cache
    ''' </summary>
    ''' <param name="killcache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetAttachedFilesDV(ByVal Cachekey As String) As DataView
        'Dim str As String = "AttachedFiles-" & _attachmentForID & "-" & _attachmentForSource & "-" & Session.SessionID
        Dim str As String = Cachekey.Replace("AttachedFiles-", "")
        str = str.Replace("-" & Session.SessionID, "")
        Dim strarr() As String
        strarr = str.Split("-")
        'Dim str As String = Cachekey.Substring(Cachekey.firstIndexOf("-"))
        Dim dsView As DataView
        If Cache(Cachekey) Is Nothing Then
            'ws.WSContact.Timeout = 300000
            dsView = ws.WSContact.GetAttachmentsDetails(strarr(0), strarr(1)).Copy.Tables(0).DefaultView
            Cache.Remove(Cachekey)
            Cache.Add(Cachekey, dsView, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            dsView = CType(Cache(Cachekey), DataView)
        End If
        Return dsView
    End Function



    Private Function IsValidFilesExtension() As Boolean
        Dim flag As Boolean = False
        Dim uploadName As System.Web.UI.WebControls.FileUpload = CType(FindControl("FileUpload1"), System.Web.UI.WebControls.FileUpload)
        If Not (uploadName Is Nothing) Then
            If uploadName.HasFile Then
                Dim fileExtension As String
                Dim allowedExtensions As String() = Nothing
                If Not IsNothing(Request("Type")) Then
                    _type = Request("Type")
                End If
                If Not IsNothing(Request("AttachLinkSource")) Then
                    _attachlinkSource = Request("AttachLinkSource")
                End If

                fileExtension = System.IO.Path.GetExtension(uploadName.FileName).ToLower()
                If _type = "Press Release" Or _type = "Articles" Or _type = "Quotation" Then
                    allowedExtensions = ApplicationSettings.FileTypeImage
                ElseIf _type = "CompanyLogo" Then
                    allowedExtensions = ApplicationSettings.FileTypeLogo
                ElseIf _type.ToString.ToLower = "workorder" And _attachlinkSource.ToString.ToLower = "wocomplete" Then
                    allowedExtensions = ApplicationSettings.ValidFilesExtensionsForComplete
                Else
                    allowedExtensions = ApplicationSettings.ValidFilesExtensions
                End If

                For i As Integer = 0 To allowedExtensions.Length - 1
                    If fileExtension = allowedExtensions(i) Then
                        flag = True
                        Exit For
                    End If
                Next
                If flag = False Then
                    Exit Function
                End If
            End If
        End If


        Return True
    End Function
    Private Function IsValidFileSize() As Boolean
        Dim flag As Boolean = False
        Dim uploadName As System.Web.UI.WebControls.FileUpload = CType(FindControl("FileUpload1"), System.Web.UI.WebControls.FileUpload)
        If Not (uploadName Is Nothing) Then
            If uploadName.HasFile Then
                If (uploadName.PostedFile.ContentLength < 1025) Then
                    flag = True
                End If
            End If
        End If
        Return flag
    End Function
    Private Function proceedUpload()


    End Function
End Class