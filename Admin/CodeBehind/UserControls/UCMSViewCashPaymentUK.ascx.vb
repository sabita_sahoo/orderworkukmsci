Partial Public Class UCMSViewCashPaymentUK
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs
    Protected WithEvents lblInvoiceType, lblInvoiceTo, lblCompanyName, lblAddress, lblCity, lblPostCode, lblCountry, lblCompNameInvoiceTo As Label
    Protected WithEvents lblCompanyRegNo, lblInvoiceDate, lblDescription, lblTotalAmount As Label
    Protected WithEvents rptList As Repeater
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim ds As DataSet = ws.WSFinance.MS_GetPaymentAdviceDetails(Request("BizDivId"), Request("invoiceNo"))

            If ds.Tables("tblBillingAddress").Rows.Count > 0 Then

                'Company Name
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("CompanyName"))) Then
                    lblCompanyName.Text = ds.Tables("tblBillingAddress").Rows(0)("CompanyName")
                End If


                'Supplier address
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("Address"))) Then
                    lblAddress.Text = ds.Tables("tblBillingAddress").Rows(0)("Address")
                End If
                'Supplier city
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("City"))) Then
                    lblCity.Text = ds.Tables("tblBillingAddress").Rows(0)("City") & ","
                End If
                'Supplier post code
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("PostCode"))) Then
                    lblPostCode.Text = ds.Tables("tblBillingAddress").Rows(0)("PostCode")
                End If

            End If


            If ds.Tables("tblOWAddress").Rows.Count > 0 Then


                'Invoice To
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("CompanyName"))) Then
                    lblCompNameInvoiceTo.Text = ds.Tables("tblOWAddress").Rows(0)("CompanyName")
                End If

                'Address Invoice to
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Address"))) Then
                    If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("State"))) Then
                        If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("PostCode"))) Then
                            lblInvoiceTo.Text = ds.Tables("tblOWAddress").Rows(0)("Address") & "<br>" & ds.Tables("tblOWAddress").Rows(0)("State") & " " & ds.Tables("tblOWAddress").Rows(0)("PostCode")
                        End If
                    End If
                End If
            End If

            If ds.Tables("tblCustomerDetails").Rows.Count > 0 Then
                'Company Reg Number
                If Not (IsDBNull(ds.Tables("tblCustomerDetails").Rows(0)("CompanyRegNo"))) Then
                    lblCompanyRegNo.Text = "Company No: " & ds.Tables("tblCustomerDetails").Rows(0)("CompanyRegNo")
                End If
            End If



            If ds.Tables("tblAdviceDetails").Rows.Count > 0 Then
                'Invoice Date
                If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceDate"))) Then
                    lblInvoiceDate.Text = Strings.FormatDateTime(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceDate").ToString(), DateFormat.ShortDate)
                End If


                'Total Amount

                'lblTotalAmount.Text = ""    

                If Not IsDBNull(ds.Tables("tblPaymentDetails")) Then
                    rptList.DataSource = ds.Tables("tblPaymentDetails").DefaultView
                    rptList.DataBind()

                    Dim totalAmount As Decimal = 0
                    Dim drow As DataRow
                    For Each drow In ds.Tables("tblPaymentDetails").Rows
                        totalAmount += drow("Total")
                    Next
                    lblTotalAmount.Text = FormatCurrency(totalAmount, 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If
    End Sub

End Class