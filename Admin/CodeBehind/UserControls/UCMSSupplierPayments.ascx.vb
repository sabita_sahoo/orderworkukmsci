Partial Public Class UCMSSupplierPayments
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs
    Public Property SubAccount() As String
        Get
            If Not IsNothing(ViewState("SubAccount")) Then
                Return ViewState("SubAccount")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("SubAccount") = value
        End Set
    End Property
    Public Property Status() As String
        Get
            If Not IsNothing(ViewState("Status")) Then
                Return ViewState("Status")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("Status") = value
        End Set
    End Property
    Public Property FromDate() As String
        Get
            If Not IsNothing(ViewState("FromDate")) Then
                Return ViewState("FromDate")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("FromDate") = value

        End Set
    End Property
    Public Property ToDate() As String
        Get
            If Not IsNothing(ViewState("ToDate")) Then
                Return ViewState("ToDate")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("ToDate") = value

        End Set
    End Property
    Public Property BizDiv() As String
        Get
            If Not IsNothing(ViewState("BizDiv")) Then
                Return ViewState("BizDiv")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("BizDiv") = value
        End Set
    End Property
    Public Property Viewer() As String
        Get
            If Not IsNothing(ViewState("Viewer")) Then
                Return ViewState("Viewer")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("Viewer") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvSupplierPayments.Sort("VoucherNumber", SortDirection.Descending)
        End If
    End Sub

    Public Sub PopulateGrid(Optional ByVal PageIndex As Integer = 0)
        SetGridSettings()
        If ViewState("Status") = "Made" Then
            gvSupplierPayments.Sort("DateCompleted", SortDirection.Descending)
        Else
            gvSupplierPayments.Sort("Date", SortDirection.Descending)
        End If

        If PageIndex > 0 Then
            gvSupplierPayments.PageIndex = PageIndex
        Else
            gvSupplierPayments.PageIndex = 0
        End If
        gvSupplierPayments.DataBind()
    End Sub

    Public Sub SetGridSettings()
        If Viewer = ApplicationSettings.ViewerAdmin Then
            gvSupplierPayments.Columns.Item(5).Visible = False 'Status  
            If ViewState("Status") = "Made" Then
                gvSupplierPayments.Columns.Item(1).Visible = True ' date completed
                gvSupplierPayments.Columns.Item(2).Visible = True 'supplier name
                gvSupplierPayments.Columns.Item(0).Visible = False 'withdrawal date
            End If
        End If
        'Change added by PB: For DE Site replace Purchase Invoice Number with WOrkOrderId
        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.site Then
            If ApplicationSettings.BizDivId = ApplicationSettings.OWDEBizDivId Then
                gvSupplierPayments.Columns.Item(3).Visible = True 'workorderid                   
                gvSupplierPayments.Columns.Item(4).Visible = False 'Purchase Invoice Number                    
            End If
        End If
    End Sub

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                        ByVal maximumRows As System.Nullable(Of Integer)) As DataSet

        Dim ds As DataSet
        Dim SubAccount As Integer
        Dim BizDivID As Integer
        Dim Status As String
        Dim UpdateVoucherStatus As Integer
        SubAccount = HttpContext.Current.Items("SubAccount")
        Status = HttpContext.Current.Items("Status")
        FromDate = HttpContext.Current.Items("FromDate")
        ToDate = HttpContext.Current.Items("ToDate")
        BizDivID = HttpContext.Current.Items("BizDivID")
        UpdateVoucherStatus = HttpContext.Current.Items("UpdateVoucherStatus")
        ' Since there is issue with accessing the page variable, fetching data directly without caching.
        ds = ws.WSFinance.MSAccounts_GetSupplierPaymentDetails(SubAccount, Status, FromDate, ToDate, BizDivID, UpdateVoucherStatus, sortExpression, startRowIndex, maximumRows, "")
        HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
        Return ds
    End Function

    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    Private Sub ObjectDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles ObjectDataSource1.Selecting
        InitializeGridParameters()
        SetGridSettings()
    End Sub

    Private Sub InitializeGridParameters()
        HttpContext.Current.Items("SubAccount") = SubAccount
        HttpContext.Current.Items("Status") = Status
        HttpContext.Current.Items("FromDate") = FromDate
        HttpContext.Current.Items("ToDate") = ToDate
        HttpContext.Current.Items("BizDivID") = BizDiv
        HttpContext.Current.Items("UpdateVoucherStatus") = 0
    End Sub

    Private Sub gvSupplierPayments_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplierPayments.PreRender
        If gvSupplierPayments.PageIndex > 0 Then
            Me.gvSupplierPayments.Controls(0).Controls(Me.gvSupplierPayments.Controls(0).Controls.Count - 1).Visible = True
        End If
    End Sub

    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvSupplierPayments.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvSupplierPayments, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvSupplierPayments.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvSupplierPayments.DataBind()
    End Sub

    ''' <summary>
    ''' GridView event handler to call MakeGridViewHeaderClickable method
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvSupplierPayments.RowDataBound
        MakeGridViewHeaderClickable(gvSupplierPayments, e.Row)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim Debit As Decimal
            Debit = CType(e.Row.FindControl("lblDebitBalance"), Label).Text
            If Debit < 0 Then
                CType(e.Row.FindControl("lblDebitBalance"), Label).Text = FormatNumber(Debit, 2, TriState.True, TriState.True, TriState.False)
                CType(e.Row.FindControl("lblDebitBalance"), Label).CssClass = "bodytxtValidationMsg"
            Else
                CType(e.Row.FindControl("lblDebitBalance"), Label).Text = FormatNumber(Debit, 2, TriState.True, TriState.True, TriState.False)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Public Function GetLinks(ByVal invoiceNo As String, ByVal companyId As String, ByVal WOID As String) As String
        Dim link As String = ""
        Dim linkParams As String = ""
        Dim imgPath As String = ""

        linkParams &= "invoiceNo=" & invoiceNo
        linkParams &= "&companyId=" & companyId
        linkParams &= "&WOID=" & WOID
        linkParams &= "&bizDivId=" & ViewState("BizDiv")
        link &= "<a target='_blank' href='ViewPurchaseInvoice.aspx?" & linkParams & "'><img src='Images/Icons/View-Purchase-Invoice.gif' alt='View Purchase Invoice' width='18' height='14' hspace='2' vspace='0' border='0' align='absmiddle'></a>"

        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.site Then
            If ApplicationSettings.BizDivId = ApplicationSettings.OWDEBizDivId Then
                link = ""
            End If
        End If

        Return link
    End Function

    Public Function SetDateCompleted(ByVal DateCompleted As Object) As String
        If IsDBNull(DateCompleted) Then
            Return ""
        Else
            Return Strings.FormatDateTime(DateCompleted, DateFormat.ShortDate)
        End If
    End Function

    Public Function GetStatus(ByVal Status As String) As String

        Select Case Status
            Case "Available"
                Status = ResourceMessageText.GetString("Available")
            Case "Paid"
                Status = ResourceMessageText.GetString("Paid")
            Case "Requested"
                Status = ResourceMessageText.GetString("Requested")
            Case "Unpaid"
                Status = ResourceMessageText.GetString("Unpaid")
            Case "In Process"
                Status = ResourceMessageText.GetString("InProcess")
        End Select
        Return Status
    End Function

End Class