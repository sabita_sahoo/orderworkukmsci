Public Partial Class UCDateRange
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Sub rngDate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rngDate.ServerValidate
        If Page.IsValid Then
            If txtFromDate.Text.Trim <> "" And txtToDate.Text.Trim <> "" Then
                Dim validDate As Boolean = True
                Try
                    Dim tmpFromDate As DateTime = DateTime.Parse(txtFromDate.Text.Trim)
                    Dim tmpToDate As DateTime = DateTime.Parse(txtToDate.Text.Trim)
                Catch ex As Exception
                    validDate = False
                End Try
                If validDate = True Then
                    Dim DateStart As New Date
                    Dim DateEnd As New Date
                    DateStart = txtFromDate.Text.Trim
                    DateEnd = txtToDate.Text.Trim
                    If DateStart <= DateEnd Then
                        args.IsValid = True
                    Else
                        args.IsValid = False
                        rngDate.ErrorMessage = "To Date should be greater than From Date"
                    End If
                Else
                    args.IsValid = False
                    rngDate.ErrorMessage = ResourceMessageText.GetString("errInvalidDate")
                End If
            End If
        End If
    End Sub

    Public Sub ValidateDates()
        Page.Validate()
    End Sub

    Public Function DateValidate() As Boolean
        If txtFromDate.Text.Trim <> "" And txtToDate.Text.Trim <> "" Then
            Dim validDate As Boolean = True
            Try
                Dim tmpFromDate As DateTime = DateTime.Parse(txtFromDate.Text.Trim)
                Dim tmpToDate As DateTime = DateTime.Parse(txtToDate.Text.Trim)
            Catch ex As Exception
                validDate = False
                Return False
            End Try
            If validDate = True Then
                Dim DateStart As New Date
                Dim DateEnd As New Date
                DateStart = txtFromDate.Text.Trim
                DateEnd = txtToDate.Text.Trim
                If DateStart <= DateEnd Then
                    'args.IsValid = True
                    Return True
                Else
                    'args.IsValid = False
                    Return False
                End If
            Else
                'args.IsValid = False
                Return False
                rngDate.ErrorMessage = ResourceMessageText.GetString("errInvalidDate")
            End If
        End If
    End Function
End Class