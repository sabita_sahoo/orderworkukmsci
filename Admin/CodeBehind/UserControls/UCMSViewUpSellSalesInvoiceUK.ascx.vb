Public Partial Class UCMSViewUpSellSalesInvoiceUK
    Inherits System.Web.UI.UserControl

    Shared ws As New WSObjs
    Protected WithEvents lblBankDetails, lblAddress, lblCity, lblPostCode, lblCompNameInvoiceTo As Label
    Protected WithEvents lblListingVAT, lblListingDate, lblListingNetPrice, lblListingTotalPrice, lblInvoiceTo, lblCompanyName, lblInvoiceNo, lblInvoiceDate As Label
    Protected WithEvents lblDate, lblDescription, lblPrice, lblSubTotal, lblVAT As Label
    Protected WithEvents lblTotalVAT, lblVATNo, lblAmount, lblTotalAmount, lblAccountName As Label
    Protected WithEvents lblSortCode As Label
    Protected WithEvents lblListingDesc, lblTelNo, lblTotalVatAsPerSite, lblFaxNo, lblRegistartionNo, lblRegistartionNoLine2, lblAccountNumber, lblShowFax As Label
    Protected WithEvents rptList As Repeater
    Protected WithEvents pnlListingFee As Panel

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            imgLogo.Src = ApplicationSettings.WebPath + "Images/Invoice_Logo_New.gif"
            imgPaid.Src = ApplicationSettings.WebPath + "Images/WaterMarkPaid.jpg"
            Dim ds As DataSet = ws.WSFinance.MS_GetUpSellSalesAdviceDetails(Request("BizDivId"), Request("invoiceNo"))
            'Dim totalAmount As Decimal = 0
            'Dim totalVAT As Decimal = 0
            'Dim totalNetAmount As Decimal = 0
            Dim drow As DataRow

            If ds.Tables("tblOWAddress").Rows.Count > 0 Then
                'Company Name
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("CompanyName"))) Then
                    lblCompanyName.Text = ds.Tables("tblOWAddress").Rows(0)("CompanyName")
                End If
                'Address
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Address"))) Then
                    lblAddress.Text = ds.Tables("tblOWAddress").Rows(0)("Address")
                End If

                'OW city
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("City"))) Then
                    lblCity.Text = ds.Tables("tblOWAddress").Rows(0)("City") & ","
                End If

                'OW post code
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("PostCode"))) Then
                    lblPostCode.Text = ds.Tables("tblOWAddress").Rows(0)("PostCode")
                End If


                'OW Phone
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Phone"))) Then
                    lblTelNo.Text = ds.Tables("tblOWAddress").Rows(0)("Phone")
                End If
                'OW Fax
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Fax"))) Then
                    If ds.Tables("tblOWAddress").Rows(0)("Fax") <> "" Then
                        lblFaxNo.Text = ds.Tables("tblOWAddress").Rows(0)("Fax")
                    Else
                        lblShowFax.Visible = False
                    End If
                End If
                End If


                If ds.Tables("tblWorkOrderList").Rows.Count > 0 Then
                    'Invoice To
                    Dim str1 As String = ""
                    Dim str2 As String = ""
                    If Not (IsDBNull(ds.Tables("tblWorkOrderList").Rows(0)("BCFirstName"))) Then
                        str1 = ds.Tables("tblWorkOrderList").Rows(0)("BCFirstName").ToString
                    End If

                    If Not (IsDBNull(ds.Tables("tblWorkOrderList").Rows(0)("BCLastName"))) Then
                        str2 = ds.Tables("tblWorkOrderList").Rows(0)("BCLastName")
                    End If

                    str1 = str1 + " " + str2
                    lblCompNameInvoiceTo.Text = str1.ToString

                    'Address Invoice to
                    If Not (IsDBNull(ds.Tables("tblWorkOrderList").Rows(0)("BCAddress"))) Then
                        If Not (IsDBNull(ds.Tables("tblWorkOrderList").Rows(0)("BCCounty"))) Then
                            If Not (IsDBNull(ds.Tables("tblWorkOrderList").Rows(0)("BCPostCode"))) Then
                                lblInvoiceTo.Text = ds.Tables("tblWorkOrderList").Rows(0)("BCAddress") & "<br>" & IIf((ds.Tables("tblWorkOrderList").Rows(0)("BCCity") <> ""), ds.Tables("tblWorkOrderList").Rows(0)("BCCity") & ",", "") & " " & ds.Tables("tblWorkOrderList").Rows(0)("BCCounty") & IIf(((ds.Tables("tblWorkOrderList").Rows(0)("BCCounty") <> "") Or (ds.Tables("tblWorkOrderList").Rows(0)("BCCity") <> "")), "<br>", "") & ds.Tables("tblWorkOrderList").Rows(0)("BCPostCode")
                            End If
                        End If
                    End If

                End If

                If ds.Tables("tblAdviceDetails").Rows.Count > 0 Then


                    'Invoice No.
                    If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceNo"))) Then
                        lblInvoiceNo.Text = ds.Tables("tblAdviceDetails").Rows(0)("InvoiceNo")
                    End If

                    'Invoice Date
                    If Not (IsDBNull(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceDate"))) Then
                        lblInvoiceDate.Text = Strings.FormatDateTime(ds.Tables("tblAdviceDetails").Rows(0)("InvoiceDate").ToString(), DateFormat.ShortDate)
                    End If


                End If


                If ds.Tables("tblOrderWorkDetails").Rows.Count > 0 Then
                    'Vat Number
                    If Not (IsDBNull(ds.Tables("tblOrderWorkDetails").Rows(0)("VATRegNo"))) Then
                        lblVATNo.Text = ds.Tables("tblOrderWorkDetails").Rows(0)("VATRegNo")
                    End If




                    If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                        'Registration number
                        lblRegistartionNo.Text = "Registered in England"

                        If Not (IsDBNull(ds.Tables("tblOrderWorkDetails").Rows(0)("CompanyRegNo"))) Then
                            lblRegistartionNoLine2.Text = "No.  " & ds.Tables("tblOrderWorkDetails").Rows(0)("CompanyRegNo")
                        End If
                    End If

                End If
                If ds.Tables("tblWODetails").Rows.Count > 0 Then
                    rptList.DataSource = ds.Tables("tblWODetails").DefaultView
                    rptList.DataBind()

                    lblSubTotal.Text = FormatCurrency(CDbl(ds.Tables("tblWODetails").Rows(0).Item("InvoiceNetAmount").ToString), 2, TriState.True, TriState.True, TriState.False)
                    lblTotalVAT.Text = FormatCurrency(CDbl(ds.Tables("tblWODetails").Rows(0).Item("VAT").ToString), 2, TriState.True, TriState.True, TriState.False)
                    lblTotalAmount.Text = FormatCurrency(CDbl(ds.Tables("tblWODetails").Rows(0).Item("TotalAmount").ToString), 2, TriState.True, TriState.True, TriState.False)
                End If


                'lblSubTotal.Text = FormatCurrency(totalNetAmount, 2, TriState.True, TriState.True, TriState.False)
                'lblTotalVAT.Text = FormatCurrency(totalVAT, 2, TriState.True, TriState.True, TriState.False)
                'lblTotalAmount.Text = FormatCurrency(totalAmount, 2, TriState.True, TriState.True, TriState.False)


                Dim vatRate As String
                vatRate = ApplicationSettings.VATPercentage(Strings.FormatDateTime(lblInvoiceDate.Text.Trim, DateFormat.ShortDate))
                lblTotalVatAsPerSite.Text = vatRate

            End If
    End Sub

End Class