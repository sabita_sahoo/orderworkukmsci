Imports System.IO
Imports System.Net

Partial Public Class UCMSWOCloseComplete
    Inherits System.Web.UI.UserControl

    Shared ws As New WSObjs
    Public objEmail As New Emails

    Private _parentName As String
    Public Property ParentName() As String
        Get
            Return _parentName
        End Get
        Set(ByVal value As String)
            _parentName = value
        End Set
    End Property

    Private _classID As Integer
    Public Property ClassID() As Integer
        Get
            Return _classID
        End Get
        Set(ByVal value As Integer)
            _classID = value
        End Set
    End Property

    Private _companyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property

    Private _supCompanyID As Integer
    Public Property SupCompanyID() As Integer
        Get
            Return _supCompanyID
        End Get
        Set(ByVal value As Integer)
            _supCompanyID = value
        End Set
    End Property

    Private _supContactID As Integer
    Public Property SupContactID() As Integer
        Get
            Return _supContactID
        End Get
        Set(ByVal value As Integer)
            _supContactID = value
        End Set
    End Property


    Private _userID As Integer
    Public Property UserID() As Integer
        Get
            Return _userID
        End Get
        Set(ByVal value As Integer)
            _userID = value
        End Set
    End Property

    Private _viewer As String
    Public Property Viewer() As String
        Get
            Return _viewer
        End Get
        Set(ByVal value As String)
            _viewer = value
        End Set
    End Property

    Private _BizDivID As Integer
    Public Property BizDivID() As Integer
        Get
            Return _BizDivID
        End Get
        Set(ByVal value As Integer)
            _BizDivID = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not IsNothing(Request("WOID")) Then
                If Request("WOID") <> "" Then
                    'If ParentName.ToLower = "wocomplete" Then
                    '    pnlAttachments.Visible = True
                    'Else
                    '    pnlAttachments.Visible = False
                    'End If
                    populateData(True)
                End If
            End If
        End If
    End Sub

    Private Sub populateData(Optional ByVal KillCache As Boolean = False)
        Dim str As String = ""
        'fetches the data for the wo
        Dim ds_WO As New DataSet
        Dim tblName As String

        ds_WO = GetActionDetails(KillCache)
        If Not IsNothing(Request("sender")) Then
            If Request("sender") <> "UCWODetails" Then
                'Store version number
                CommonFunctions.StoreVerNum(ds_WO.Tables("tblWorkOrderSummary"))
            End If
        End If

        'populate the wo summary
        If Not IsNothing(ds_WO.Tables("tblWorkOrderSummary")) Then
            If ds_WO.Tables("tblWorkOrderSummary").Rows.Count <> 0 Then

                ViewState("SLA") = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("SLAMet")

                ' ViewState("IsSignOffSheetReqd") = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("IsSignOffSheetReqd").ToString
                'If Not IsNothing(ViewState("IsSignOffSheetReqd")) Then
                If ParentName.ToLower = "wocomplete" Then
                    pnlAttachments.Visible = True
                    CType(UCFileUpload1.FindControl("tdUploadTitle"), HtmlTableCell).InnerText = "Sign Off Sheet Upload:"
                Else
                    pnlAttachments.Visible = False
                End If
                'Else
                '    pnlAttachments.Visible = False
                'End If

                ' work order id
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("RefWOID")) Then
                    'lblWONo.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("WorkOrderId")
                    lblWONo.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("RefWOID")
                End If

                'submitted date
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("DateCreated")) Then
                    lblCreated.Text = Strings.FormatDateTime(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("DateCreated"), DateFormat.ShortDate)
                End If

                'location
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Location")) Then
                    lblLocation.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Location")
                End If

                'contact
                If Viewer = ApplicationSettings.ViewerSupplier Then
                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContact")) Then
                        lblContact.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContact")
                    End If
                ElseIf Viewer = ApplicationSettings.ViewerBuyer Then
                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BuyerContact")) Then
                        lblContact.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BuyerContact")
                    End If
                End If

                'title
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("WOTitle")) Then
                    lblTitle.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("WOTitle")
                End If

                'date start
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart")) Then
                    lblWOStart.Text = Strings.FormatDateTime(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
                End If
                'AptTime if present
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime")) Then
                    ViewState("AptTime") = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime")
                End If

                'proposed price
                If (Viewer = ApplicationSettings.ViewerBuyer) Or (Viewer = ApplicationSettings.ViewerSupplier) Then
                    If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Value")) Then
                        lblPrice.Text = FormatCurrency(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Value"), 2, TriState.True, TriState.True, TriState.False)
                    End If
                ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                    If ParentName.ToLower = "wocomplete" Then
                        If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformPrice")) Then
                            lblPrice.Text = FormatCurrency(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformPrice"), 2, TriState.True, TriState.True, TriState.False)
                        End If
                    ElseIf ParentName.ToLower = "woclose" Then
                        trSLAMet.Visible = True
                        trSLAMetText.Visible = True
                        rqSLA.Enabled = True
                        If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("WholesalePrice")) Then
                            lblPrice.Text = FormatCurrency(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.True, TriState.False)
                        End If
                    End If
                End If


                'status
                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Status")) Then
                    lblStatus.Text = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("Status")
                End If
            End If
        End If

        'populate the message, the wo summary labels, the confirm click button
        Dim Msg1 As String
        If ParentName.ToLower = "wocomplete" Then
            lblAction.Text = ResourceMessageText.GetString("ActionCompleting")
            lblColNameDateCreated.Text = ResourceMessageText.GetString("Accepted")
            lnkConfirm.Text = ResourceMessageText.GetString("CompleteLnk")
            Msg1 = ResourceMessageText.GetString("ConfirmCompleteWO")
            lblMsg1.Text = Msg1.Replace("<WONo>", lblWONo.Text)
            Msg1 = ResourceMessageText.GetString("RateCompleteWO")
            Msg1 = Msg1.Replace("<buyerDetails>", ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BuyerCompany") & "-" & ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("BuyerContact"))
            Msg1 = Msg1.Replace("<WOTitle>", lblTitle.Text)
            lblRatingMsg.Text = Msg1
            lblComments.Text = ResourceMessageText.GetString("CommentCompleteWO")
            tdConfirmComplete.Visible = False
            'populate attachments data
            'If Not IsNothing(ViewState("IsSignOffSheetReqd")) Then
            '    If ViewState("IsSignOffSheetReqd") Then
            pnlAttachments.Visible = True

            UCFileUpload1.BizDivID = BizDivID
            UCFileUpload1.AttachmentForID = Request("WOID")
            UCFileUpload1.CompanyID = CompanyID
            UCFileUpload1.AttachmentForSource = "WOComplete"
            UCFileUpload1.ClearUCFileUploadCache()
            UCFileUpload1.PopulateAttachedFiles()
            'Else
            '    pnlAttachments.Visible = False
            'End If
            '    Else
            'pnlAttachments.Visible = False
            '    End If


            Dim dvQ As New DataView
            If ds_WO.Tables.Count > 1 Then
                dvQ = ds_WO.Tables(1).Copy.DefaultView()
                dvQ.RowFilter = "Type = 'Boolean'"
                If (dvQ.Count > 0) Then
                    trClientQ.Visible = True
                    rpBoolQ.Visible = True
                    rpBoolQ.DataSource = dvQ
                    rpBoolQ.DataBind()
                    ViewState("QA") = "Yes"
                End If
                dvQ = ds_WO.Tables(1).Copy.DefaultView()
                dvQ.RowFilter = "Type = 'TimeBased'"
                If (dvQ.Count > 0) Then
                    trClientQ.Visible = True
                    rpTimeQ.Visible = True
                    rpTimeQ.DataSource = dvQ
                    rpTimeQ.DataBind()
                    ViewState("QA") = "Yes"
                End If
            End If
        Else
            lblAction.Text = ResourceMessageText.GetString("ActionClosing")
            lblColNameDateCreated.Text = ResourceMessageText.GetString("Accepted")
            lnkConfirm.Text = ResourceMessageText.GetString("CloseLnk") + " and Go to Closed Listing"
            Msg1 = ResourceMessageText.GetString("ConfirmCloseWO")
            lblMsg1.Text = Msg1.Replace("<WONo>", lblWONo.Text)
            Msg1 = ResourceMessageText.GetString("RateCloseWO")
            Msg1 = Msg1.Replace("<supplierDetails>", ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompany") & "-" & ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContact"))
            Msg1 = Msg1.Replace("<WOTitle>", lblTitle.Text)
            lblRatingMsg.Text = Msg1
            lblComments.Text = ResourceMessageText.GetString("CommentCloseWO")
            tdConfirmComplete.Visible = True
            Dim dsRating As DataSet
            dsRating = CommonFunctions.GetWORating(Request("WOID"))
            If dsRating.Tables.Count > 0 Then
                If dsRating.Tables(0).Rows.Count > 0 Then
                    If Not IsDBNull(dsRating.Tables(0).Rows(0)(0)) Then
                        Dim Result As Integer = dsRating.Tables(0).Rows(0)(0)
                        Select Case Result
                            Case -1
                                rdoRatingNegative.Checked = True
                            Case 0
                                rdoRatingNeutral.Checked = True
                            Case 1
                                rdoRatingPositive.Checked = True
                        End Select
                    End If
                    txtCompleteComments.Text = dsRating.Tables("tblRating").Rows(0).Item("Comments")
                End If

            End If

        End If


    End Sub

    Public Function GetActionDetails(Optional ByVal KillCache As Boolean = False) As DataSet
        pnlChangedData.Visible = False
        Dim ds As DataSet
        If KillCache = True Then
            Cache.Remove("WorkOrderProcess" & Session.SessionID)
        End If
        If Cache("WorkOrderProcess" & Session.SessionID) Is Nothing Then
            Dim chkValid As Integer
            Select Case BizDivID
                Case ApplicationSettings.OWUKBizDivId
                    chkValid = 0
                Case ApplicationSettings.SFUKBizDivId
                    chkValid = 1
                Case ApplicationSettings.OWDEBizDivId
                    chkValid = 0
            End Select

            If SupCompanyID <> 0 Then
                ds = ws.WSWorkOrder.MS_GetWO(Request("WOID"), ParentName.ToLower, SupCompanyID, BizDivID, chkValid, Viewer)
            Else
                ds = ws.WSWorkOrder.MS_GetWO(Request("WOID"), ParentName.ToLower, CompanyID, BizDivID, chkValid, Viewer)
            End If

            Cache.Add("WorkOrderProcess" & Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(Cache("WorkOrderProcess" & Session.SessionID), DataSet)
        End If
        Return ds
    End Function

    Private Sub lnkConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkConfirm.Click
        Page.Validate()
        If Page.IsValid Then
            Dim str As String
            divValidationMain.Visible = False

            Dim ds As DataSet = GetActionDetails()
            Dim RefWOID As String
            Dim PONumber As String
            Dim StartDate As DateTime

            With ds.Tables("tblWorkOrderSummary").Rows(0)
                RefWOID = .Item("RefWOID")
                PONumber = .Item("PONumber")
                StartDate = Strings.FormatDateTime(.Item("DateStart"), DateFormat.ShortDate)
            End With

            Dim ContactId As Integer
            ContactId = 0

            Dim ds_WOSuccess As DataSet
            Dim strRating As String = ""
            If rdoRatingNeutral.Checked = True Then
                strRating = 0
            ElseIf rdoRatingNegative.Checked = True Then
                strRating = -1
            ElseIf rdoRatingPositive.Checked = True Then
                strRating = 1
            End If

            Dim SLAMet As Integer = -1
            If rdoSLAMetYes.Checked = True Then
                SLAMet = 1
            ElseIf rdoSLAMetNo.Checked = True Then
                SLAMet = 0
            End If

            Dim WOStatusText As String = ""
            Dim dsWorkOrderForAttach As New WorkOrderNew
            dsWorkOrderForAttach = UCFileUpload1.ReturnFilledAttachments(dsWorkOrderForAttach)
            Dim xmlContent As String = dsWorkOrderForAttach.GetXml
            Dim xmlQA As String = ""
            'Remove XML namespace attribute and its value
            xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)

            If (Viewer = ApplicationSettings.ViewerBuyer) Then
                If ParentName.ToLower = "woclose" Then
                    ds_WOSuccess = ws.WSWorkOrder.MS_PerformWOProcess(Request("WOID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompanyID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContactID"), UserID, CompanyID, ParentName.ToLower, BizDivID, Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate), Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate), ds.Tables("tblWorkOrderSummary").Rows(0).Item("WholesalePrice"), txtCompleteComments.Text, ContactId, ClassID, 0, ds.Tables("tblWorkOrderSummary").Rows(0).Item("DataRef"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("LastActionRef"), strRating, txtComments.Text, xmlContent, ApplicationSettings.RoleSupplierID, 0, 0, ViewState("AptTime"), CommonFunctions.FetchVerNum(), Nothing, "", SLAMet, Session("UserId"), 0)
                End If
            ElseIf (Viewer = ApplicationSettings.ViewerSupplier) Then
                If ParentName.ToLower = "wocomplete" Then
                    ds_WOSuccess = ws.WSWorkOrder.MS_PerformWOProcess(Request("WOID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompanyID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContactID"), UserID, CompanyID, ParentName.ToLower, BizDivID, Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate), Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate), ds.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformPrice"), txtCompleteComments.Text, ContactId, ClassID, 0, ds.Tables("tblWorkOrderSummary").Rows(0).Item("DataRef"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("LastActionRef"), strRating, txtComments.Text, xmlContent, ApplicationSettings.RoleClientID, 0, 0, ViewState("AptTime"), CommonFunctions.FetchVerNum(), Nothing, "", SLAMet, Session("UserId"), 0)
                End If
            ElseIf (Viewer = ApplicationSettings.ViewerAdmin) Then
                If ParentName.ToLower = "woclose" Then
                    ds_WOSuccess = ws.WSWorkOrder.MS_PerformWOProcess(Request("WOID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompanyID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContactID"), UserID, CompanyID, ParentName.ToLower, BizDivID, Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate), Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate), ds.Tables("tblWorkOrderSummary").Rows(0).Item("WholesalePrice"), txtCompleteComments.Text, ContactId, ClassID, 0, ds.Tables("tblWorkOrderSummary").Rows(0).Item("DataRef"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("LastActionRef"), strRating, txtComments.Text, xmlContent, ApplicationSettings.RoleSupplierID, 0, 0, ViewState("AptTime"), CommonFunctions.FetchVerNum(), Nothing, "", SLAMet, Session("UserId"), 0)
                ElseIf ParentName.ToLower = "wocomplete" Then
                    If (ViewState("QA") = "Yes") Then
                        Dim dsQA As New OWContactsQASettings()
                        Dim nrowQALabels As OWContactsQASettings.tblContactsQASettingsRow = dsQA.tblContactsQASettings.NewRow
                        With nrowQALabels
                            If (rpBoolQ.Items.Count > 0) Then
                                For Each item As RepeaterItem In rpBoolQ.Items
                                    Select Case item.ItemIndex
                                        Case 0
                                            .Field1Label = CType(rpBoolQ.Items(0).FindControl("lblQ"), Label).Text
                                            .Field1Value = IIf(CType(rpBoolQ.Items(0).FindControl("rdbtnYes"), RadioButton).Checked = True, "Yes", "No")
                                        Case 1
                                            .Field2Label = CType(rpBoolQ.Items(1).FindControl("lblQ"), Label).Text
                                            .Field2Value = IIf(CType(rpBoolQ.Items(1).FindControl("rdbtnYes"), RadioButton).Checked = True, "Yes", "No")
                                        Case 2
                                            .Field3Label = CType(rpBoolQ.Items(2).FindControl("lblQ"), Label).Text
                                            .Field3Value = IIf(CType(rpBoolQ.Items(2).FindControl("rdbtnYes"), RadioButton).Checked = True, "Yes", "No")
                                    End Select
                                Next
                            End If
                            If (rpTimeQ.Items.Count > 0) Then
                                For Each item As RepeaterItem In rpTimeQ.Items
                                    Select Case item.ItemIndex
                                        Case 0
                                            .Field4Label = CType(rpTimeQ.Items(0).FindControl("lblQ"), Label).Text
                                            .Field4Value = CType(rpTimeQ.Items(0).FindControl("txtTime"), TextBox).Text

                                        Case 1
                                            .Field5Label = CType(rpTimeQ.Items(1).FindControl("lblQ"), Label).Text
                                            .Field5Value = CType(rpTimeQ.Items(1).FindControl("txtTime"), TextBox).Text
                                    End Select
                                Next
                            End If
                            .CompanyID = CompanyID
                            .WOID = Request("WOID")
                            .Type = "WorkOrder"
                        End With
                        dsQA.tblContactsQASettings.Rows.Add(nrowQALabels)
                        xmlQA = dsQA.GetXml
                        xmlQA = xmlQA.Remove(xmlQA.IndexOf("xmlns"), xmlQA.IndexOf(".xsd") - xmlQA.IndexOf("xmlns") + 5)
                    End If
                    ds_WOSuccess = ws.WSWorkOrder.MS_PerformWOProcess(Request("WOID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompanyID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContactID"), UserID, CompanyID, ParentName.ToLower, BizDivID, Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate), Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate), ds.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformPrice"), txtCompleteComments.Text, ContactId, ClassID, 0, ds.Tables("tblWorkOrderSummary").Rows(0).Item("DataRef"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("LastActionRef"), strRating, txtComments.Text, xmlContent, ApplicationSettings.RoleClientID, 0, 0, ViewState("AptTime"), CommonFunctions.FetchVerNum(), Nothing, xmlQA, SLAMet, Session("UserId"), 0)
                    'SMS Survey Trial 
                    'not required for Now as we want to stop it for SmartTech
                    'Dim CustomerMobile As String = ds.Tables("tblWorkOrderSummary").Rows(0).Item("CustomerNumber")
                    'Dim workorderCreater As Integer = ds.Tables("tblWorkOrderSummary").Rows(0).Item("WOCreatedBy")
                    'If CustomerMobile <> "" Then
                    '    If workorderCreater = 12017 Or workorderCreater = 12093 Then
                    '        SendSMS.PostSMS(CustomerMobile, "https://tx.vc/s/NB3Z0MB/#PageTrack#")
                    '    End If
                    'End If
                    'OM-125 : SendMail
                    If ds_WOSuccess.Tables(0).Rows.Count <> 0 Then
                        If ds.Tables("tblWorkOrderSummary").Rows(0).Item("CompanyId") = OrderWorkLibrary.ApplicationSettings.OutsourceCompany Then
                            Dim ClientRefNo As String = ""
                            Dim AptDate As String = ""
                            Dim AptTime As String = ""
                            Dim workordernumber As String = ""
                            Dim wotitle As String = ""
                            Dim wolocation As String = ""
                            ClientRefNo = ds.Tables("tblWorkOrderSummary").Rows(0).Item("ReceiptNumber")
                            AptDate = ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart")
                            AptTime = ds.Tables("tblWorkOrderSummary").Rows(0).Item("AptTime")
                            workordernumber = ds.Tables("tblWorkOrderSummary").Rows(0).Item("RefWOID")
                            wotitle = ds.Tables("tblWorkOrderSummary").Rows(0).Item("WOTitle")
                            wolocation = ds.Tables("tblWorkOrderSummary").Rows(0).Item("Location")
                            Emails.SendOutsourceWOCompleteEmail(workordernumber, wotitle, ClientRefNo, AptDate, PONumber, AptTime, wolocation)
                        End If
                    End If
                End If
            End If

                If ds_WOSuccess.Tables.Count = 0 Then
                    divValidationMain.Visible = True
                    lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
                ElseIf ds_WOSuccess.Tables(0).Rows.Count <> 0 Then
                    If ds_WOSuccess.Tables(0).Rows(0).Item("ContactID") = -1 Then
                        divValidationMain.Visible = True
                        If ParentName.ToLower = "wocomplete" Then
                            lblError.Text = ResourceMessageText.GetString("NoCompleteWO")
                        Else
                            lblError.Text = ResourceMessageText.GetString("NoCloseWO")
                        End If
                        'If the ContactID is -10 i.e. version number mismatch
                    ElseIf ds_WOSuccess.Tables(0).Rows(0).Item("ContactID") = -10 Or ds_WOSuccess.Tables(0).Rows(0).Item("ContactID") = -11 Then
                        divValidationMain.Visible = True
                        'Now set the status message according to the status of the WOrkorder
                        Select Case ds_WOSuccess.Tables(0).Rows(0).Item("WOStatus")
                            Case ApplicationSettings.WOStatusID.Completed
                                'lblError.Text = ResourceMessageText.GetString("WOVNoCompleted")
                                WOStatusText = ApplicationSettings.WOGroupCompleted
                            Case ApplicationSettings.WOStatusID.Closed
                                'lblError.Text = ResourceMessageText.GetString("WOVNoClosed")
                                WOStatusText = ApplicationSettings.WOGroupClosed
                            Case ApplicationSettings.WOStatusID.Issue
                                'lblError.Text = ResourceMessageText.GetString("WOVNoIssueRaised")
                                WOStatusText = ApplicationSettings.WOGroupIssue
                            Case ApplicationSettings.WOStatusID.Cancelled
                                'lblError.Text = ResourceMessageText.GetString("WOVNoWOCancelled")
                                WOStatusText = ApplicationSettings.WOGroupCancelled
                                'Case Else
                                '    lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                        End Select
                        Dim BizDivID As Integer
                        If Not IsNothing(Request("BizDivID")) Then
                            BizDivID = Request("BizDivID")
                        Else
                            BizDivID = 1
                        End If
                        lblError.Text = ResourceMessageText.GetString("WOVersionControlMsg").Replace("<Link>", "AdminWODetails.aspx?WOID=" & Request("WOID") & "&WorkOrderID=" & Request("WorkOrderID") & "&SupContactId=" & Request("SupContactId") & "&SupCompId=" & Request("SupCompId") & "&Group=" & WOStatusText & "&CompID=" & Request("CompID") & "&FromDate=&ToDate=&PS=" & Request("PS") & "&PN=0&SC=DateCreated&SO=1&Viewer=" & ViewState("Viewer") & "&sender=UCWOsListing&BizDivID=" & BizDivID)
                        If (ds_WOSuccess.Tables.Count > 1) Then
                            If (ds_WOSuccess.Tables(1).Rows.Count > 0) Then
                                pnlChangedData.Visible = True
                                repChangedData.DataSource = ds_WOSuccess.Tables(1)
                                repChangedData.DataBind()
                            End If
                        End If
                    Else
                        UCFileUpload1.ClearUCFileUploadCache()

                        Dim link, GroupName As String
                        If ParentName.ToLower = "woclose" Then
                            GroupName = "Closed"
                        Else
                            GroupName = "Completed"
                        End If
                        link = getBackToListingLink("Confirm", GroupName)

                        'Added by Hemisha on 28/02/2014 to send the status to SONY
                        If CInt(Request("CompanyID")) = 16363 Then

                            If GroupName = "Completed" Then
                                CommonFunctions.SendStatusUpdateToSONY(RefWOID, PONumber, "EXECU", StartDate)
                            End If

                            Dim strAttachmenURL As System.Net.Mail.Attachment

                            If GroupName = "Closed" Then
                                If Not IsNothing(ds_WOSuccess.Tables("tblAttachments")) Then
                                    If ds_WOSuccess.Tables("tblAttachments").Rows.Count > 0 Then
                                        For Each objDataRow As DataRow In ds_WOSuccess.Tables("tblAttachments").Rows
                                            If objDataRow("LinkSource").ToString.ToLower = "wocomplete" Then
                                                strAttachmenURL = New System.Net.Mail.Attachment(objDataRow("FilePath").ToString)
                                            End If
                                        Next
                                    End If
                                End If

                                If strAttachmenURL.ToString.Length > 0 Then
                                    Emails.SendEmailToSONYonWOClose(PONumber, strAttachmenURL)
                                End If
                            End If
                        End If
                        If GroupName = "Closed" Then
                            CreateAndSendUpsellInvoice(ds_WOSuccess)
                        End If


                    If GroupName = "Completed" Then

                        ds = GetActionDetails()
                        If ds.Tables("tblWorkOrderSummary").Rows.Count <> 0 Then
                            Dim Woid As String = ds.Tables("tblWorkOrderSummary").Rows(0).Item("RefWOID")
                            Dim ClientCompanyName As String = ds.Tables("tblWorkOrderSummary").Rows(0).Item("BuyerCompany")
                            Dim SupplierCompany As String = ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompany")
                            Dim CompletedDate As String = Strings.FormatDateTime(System.DateTime.Now, DateFormat.ShortDate)

                            ds = ws.WSWorkOrder.GetAccountSourceAndPaymentTerms(ds.Tables("tblWorkOrderSummary").Rows(0).Item("WOID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompanyID"))
                            If ds.Tables.Count = 2 Then
                                Dim source As String = ds.Tables("Source").Rows(0).Item("Source")
                                Dim PaymentTerm As String = ds.Tables("PaymentTerm").Rows(0).Item("PaymenetTerm")
                                Dim PaymentTermID As Integer = ds.Tables("PaymentTerm").Rows(0).Item("PaymenetTermID")
                                If source = "Empowered" And PaymentTermID = 5 Then
                                    Emails.Send7dayPortalpaymentalert(Woid, ClientCompanyName, SupplierCompany, CompletedDate)
                                End If
                            End If

                        End If
                    End If

                    Response.Redirect(link)

                End If
            Else
                UCFileUpload1.ClearUCFileUploadCache()

                Dim link, GroupName As String
                If ParentName.ToLower = "woclose" Then
                    GroupName = "Closed"
                Else
                    GroupName = "Completed"
                End If
                link = getBackToListingLink("Confirm", GroupName)

                'Added by Hemisha on 28/02/2014 to send the status to SONY
                If CInt(Request("CompanyID")) = 16363 Then

                    If GroupName = "Completed" Then
                        CommonFunctions.SendStatusUpdateToSONY(RefWOID, PONumber, "EXECU", StartDate)
                    End If

                    Dim strAttachmenURL As System.Net.Mail.Attachment

                    If GroupName = "Closed" Then
                        If Not IsNothing(ds_WOSuccess.Tables("tblAttachments")) Then
                            If ds_WOSuccess.Tables("tblAttachments").Rows.Count > 0 Then
                                For Each objDataRow As DataRow In ds_WOSuccess.Tables("tblAttachments").Rows
                                    If objDataRow("LinkSource").ToString.ToLower = "wocomplete" Then
                                        strAttachmenURL = New System.Net.Mail.Attachment(objDataRow("FilePath").ToString)
                                    End If
                                Next
                            End If
                        End If

                        If strAttachmenURL.ToString.Length > 0 Then
                            Emails.SendEmailToSONYonWOClose(PONumber, strAttachmenURL)
                        End If

                    End If
                End If
                If GroupName = "Closed" Then
                    CreateAndSendUpsellInvoice(ds_WOSuccess)
                End If
                Response.Redirect(link)
                End If
            Else
                divValidationMain.Visible = True
            End If
    End Sub

    Public Sub CreateAndSendUpsellInvoice(ByVal ds_WOSuccess As DataSet)
        'Send upsell Invoice
        If Not IsNothing(ds_WOSuccess.Tables("tblUpsellInfo")) Then
            If ds_WOSuccess.Tables("tblUpsellInfo").Rows.Count > 0 Then
                If (CInt(ds_WOSuccess.Tables("tblUpsellInfo").Rows(0).Item("upsellprice")) > 0 And ds_WOSuccess.Tables("tblUpsellInfo").Rows(0).Item("UpSellSalesInvoice").ToString <> "" And ds_WOSuccess.Tables("tblUpsellInfo").Rows(0).Item("CustomerEmail").ToString <> "") Then
                    'First Send Upsell invoices
                    'Create PDF and send email
                    Dim FileName As String = CommonFunctions.CreatePDFUsingAspxURL(ds_WOSuccess.Tables("tblUpsellInfo").Rows(0).Item("UpSellSalesInvoice").ToString, "UpsellInvoices", "ViewUpSellSalesInvoice.aspx?BizDivId=" & ApplicationSettings.OWUKBizDivId & "&InvoiceNo=" & ds_WOSuccess.Tables("tblUpsellInfo").Rows(0).Item("UpSellSalesInvoice").ToString, HttpContext.Current.Server.MapPath("~/Attachments/"))
                    Dim Attachment As System.Net.Mail.Attachment
                    Attachment = New System.Net.Mail.Attachment(FileName)
                    Emails.SendUpsellInvoices("Orderwork - Receipt details for your installation service and/or materials purchased - " & ds_WOSuccess.Tables("tblUpsellInfo").Rows(0).Item("WorkOrderID").ToString, ds_WOSuccess.Tables("tblUpsellInfo").Rows(0).Item("CustomerEmail").ToString, Attachment, ds_WOSuccess.Tables("tblUpsellInfo").Rows(0).Item("ClientCompanyId"))
                End If
            End If
        End If
    End Sub

    Private Sub btnBackToCompList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackToCompList.Click
        Page.Validate()
        If Page.IsValid Then
            Dim str As String
            divValidationMain.Visible = False

            Dim ds As DataSet = GetActionDetails()

            Dim RefWOID As String
            Dim PONumber As String
            Dim StartDate As DateTime

            With ds.Tables("tblWorkOrderSummary").Rows(0)
                RefWOID = .Item("RefWOID")
                PONumber = .Item("PONumber")
                StartDate = Strings.FormatDateTime(.Item("DateStart"), DateFormat.ShortDate)
            End With

            Dim ContactId As Integer
            ContactId = 0

            Dim ds_WOSuccess As DataSet
            Dim strRating As String = ""
            If rdoRatingNeutral.Checked = True Then
                strRating = 0
            ElseIf rdoRatingNegative.Checked = True Then
                strRating = -1
            ElseIf rdoRatingPositive.Checked = True Then
                strRating = 1
            End If

            Dim SLAMet As Integer = -1
            If rdoSLAMetYes.Checked = True Then
                SLAMet = 1
            ElseIf rdoSLAMetNo.Checked = True Then
                SLAMet = 0
            End If

            Dim WOStatusText As String = ""
            Dim dsWorkOrderForAttach As New WorkOrderNew
            dsWorkOrderForAttach = UCFileUpload1.ReturnFilledAttachments(dsWorkOrderForAttach)
            Dim xmlContent As String = dsWorkOrderForAttach.GetXml
            Dim xmlQA As String = ""
            'Remove XML namespace attribute and its value
            xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)

            If (Viewer = ApplicationSettings.ViewerBuyer) Then
                If ParentName.ToLower = "woclose" Then
                    ds_WOSuccess = ws.WSWorkOrder.MS_PerformWOProcess(Request("WOID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompanyID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContactID"), UserID, CompanyID, ParentName.ToLower, BizDivID, Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate), Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate), ds.Tables("tblWorkOrderSummary").Rows(0).Item("WholesalePrice"), txtCompleteComments.Text, ContactId, ClassID, 0, ds.Tables("tblWorkOrderSummary").Rows(0).Item("DataRef"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("LastActionRef"), strRating, txtComments.Text, xmlContent, ApplicationSettings.RoleSupplierID, 0, 0, ViewState("AptTime"), CommonFunctions.FetchVerNum(), Nothing, "", SLAMet, Session("UserId"), 0)
                End If
            ElseIf (Viewer = ApplicationSettings.ViewerSupplier) Then
                If ParentName.ToLower = "wocomplete" Then
                    ds_WOSuccess = ws.WSWorkOrder.MS_PerformWOProcess(Request("WOID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompanyID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContactID"), UserID, CompanyID, ParentName.ToLower, BizDivID, Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate), Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate), ds.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformPrice"), txtCompleteComments.Text, ContactId, ClassID, 0, ds.Tables("tblWorkOrderSummary").Rows(0).Item("DataRef"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("LastActionRef"), strRating, txtComments.Text, xmlContent, ApplicationSettings.RoleClientID, 0, 0, ViewState("AptTime"), CommonFunctions.FetchVerNum(), Nothing, "", SLAMet, Session("UserId"), 0)
                End If
            ElseIf (Viewer = ApplicationSettings.ViewerAdmin) Then
                If ParentName.ToLower = "woclose" Then
                    ds_WOSuccess = ws.WSWorkOrder.MS_PerformWOProcess(Request("WOID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompanyID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContactID"), UserID, CompanyID, ParentName.ToLower, BizDivID, Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate), Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate), ds.Tables("tblWorkOrderSummary").Rows(0).Item("WholesalePrice"), txtCompleteComments.Text, ContactId, ClassID, 0, ds.Tables("tblWorkOrderSummary").Rows(0).Item("DataRef"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("LastActionRef"), strRating, txtComments.Text, xmlContent, ApplicationSettings.RoleSupplierID, 0, 0, ViewState("AptTime"), CommonFunctions.FetchVerNum(), Nothing, "", SLAMet, Session("UserId"), 0)
                ElseIf ParentName.ToLower = "wocomplete" Then
                    If (ViewState("QA") = "Yes") Then
                        Dim dsQA As New OWContactsQASettings()
                        Dim nrowQALabels As OWContactsQASettings.tblContactsQASettingsRow = dsQA.tblContactsQASettings.NewRow
                        With nrowQALabels
                            If (rpBoolQ.Items.Count > 0) Then
                                For Each item As RepeaterItem In rpBoolQ.Items
                                    Select Case item.ItemIndex
                                        Case 0
                                            .Field1Label = CType(rpBoolQ.Items(0).FindControl("lblQ"), Label).Text
                                            .Field1Value = IIf(CType(rpBoolQ.Items(0).FindControl("rdbtnYes"), RadioButton).Checked = True, "Yes", "No")
                                        Case 1
                                            .Field2Label = CType(rpBoolQ.Items(1).FindControl("lblQ"), Label).Text
                                            .Field2Value = IIf(CType(rpBoolQ.Items(1).FindControl("rdbtnYes"), RadioButton).Checked = True, "Yes", "No")
                                        Case 2
                                            .Field3Label = CType(rpBoolQ.Items(2).FindControl("lblQ"), Label).Text
                                            .Field3Value = IIf(CType(rpBoolQ.Items(2).FindControl("rdbtnYes"), RadioButton).Checked = True, "Yes", "No")
                                    End Select
                                Next
                            End If
                            If (rpTimeQ.Items.Count > 0) Then
                                For Each item As RepeaterItem In rpTimeQ.Items
                                    Select Case item.ItemIndex
                                        Case 0
                                            .Field4Label = CType(rpTimeQ.Items(0).FindControl("lblQ"), Label).Text
                                            .Field4Value = CType(rpTimeQ.Items(0).FindControl("txtTime"), TextBox).Text

                                        Case 1
                                            .Field5Label = CType(rpTimeQ.Items(1).FindControl("lblQ"), Label).Text
                                            .Field5Value = CType(rpTimeQ.Items(1).FindControl("txtTime"), TextBox).Text
                                    End Select
                                Next
                            End If
                            .CompanyID = CompanyID
                            .WOID = Request("WOID")
                            .Type = "WorkOrder"
                        End With
                        dsQA.tblContactsQASettings.Rows.Add(nrowQALabels)
                        xmlQA = dsQA.GetXml
                        xmlQA = xmlQA.Remove(xmlQA.IndexOf("xmlns"), xmlQA.IndexOf(".xsd") - xmlQA.IndexOf("xmlns") + 5)
                    End If
                    ds_WOSuccess = ws.WSWorkOrder.MS_PerformWOProcess(Request("WOID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierCompanyID"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("SupplierContactID"), UserID, CompanyID, ParentName.ToLower, BizDivID, Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate), Strings.FormatDateTime(ds.Tables("tblWorkOrderSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate), ds.Tables("tblWorkOrderSummary").Rows(0).Item("PlatformPrice"), txtCompleteComments.Text, ContactId, ClassID, 0, ds.Tables("tblWorkOrderSummary").Rows(0).Item("DataRef"), ds.Tables("tblWorkOrderSummary").Rows(0).Item("LastActionRef"), strRating, txtComments.Text, xmlContent, ApplicationSettings.RoleClientID, 0, 0, ViewState("AptTime"), CommonFunctions.FetchVerNum(), Nothing, xmlQA, SLAMet, Session("UserId"), 0)
                    'Added By sabita for SMS surevey
                    'not required for Now as we want to stop it for SmartTech
                    'Dim CustomerMobile As String = ds.Tables("tblWorkOrderSummary").Rows(0).Item("CustomerNumber")
                    'Dim workorderCreater As Integer = ds.Tables("tblWorkOrderSummary").Rows(0).Item("WOCreatedBy")
                    'If CustomerMobile <> "" Then
                    '    If workorderCreater = 12017 Or workorderCreater = 12093 Then
                    '       SendSMS.PostSMS(CustomerMobile, "https://tx.vc/s/NB3Z0MB/#PageTrack#")
                    'End If
                    ' End If
                End If
            End If

            If ds_WOSuccess.Tables.Count = 0 Then
                divValidationMain.Visible = True
                lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
            ElseIf ds_WOSuccess.Tables(0).Rows.Count <> 0 Then
                If ds_WOSuccess.Tables(0).Rows(0).Item("ContactID") = -1 Then
                    divValidationMain.Visible = True
                    If ParentName.ToLower = "wocomplete" Then
                        lblError.Text = ResourceMessageText.GetString("NoCompleteWO")
                    Else
                        lblError.Text = ResourceMessageText.GetString("NoCloseWO")
                    End If
                    'If the ContactID is -10 i.e. version number mismatch
                ElseIf ds_WOSuccess.Tables(0).Rows(0).Item("ContactID") = -10 Or ds_WOSuccess.Tables(0).Rows(0).Item("ContactID") = -11 Then
                    divValidationMain.Visible = True
                    'Now set the status message according to the status of the WOrkorder
                    Select Case ds_WOSuccess.Tables(0).Rows(0).Item("WOStatus")
                        Case ApplicationSettings.WOStatusID.Completed
                            'lblError.Text = ResourceMessageText.GetString("WOVNoCompleted")
                            WOStatusText = ApplicationSettings.WOGroupCompleted
                        Case ApplicationSettings.WOStatusID.Closed
                            'lblError.Text = ResourceMessageText.GetString("WOVNoClosed")
                            WOStatusText = ApplicationSettings.WOGroupClosed
                        Case ApplicationSettings.WOStatusID.Issue
                            'lblError.Text = ResourceMessageText.GetString("WOVNoIssueRaised")
                            WOStatusText = ApplicationSettings.WOGroupIssue
                        Case ApplicationSettings.WOStatusID.Cancelled
                            'lblError.Text = ResourceMessageText.GetString("WOVNoWOCancelled")
                            WOStatusText = ApplicationSettings.WOGroupCancelled
                            'Case Else
                            '    lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                    End Select
                    Dim BizDivID As Integer
                    If Not IsNothing(Request("BizDivID")) Then
                        BizDivID = Request("BizDivID")
                    Else
                        BizDivID = 1
                    End If
                    lblError.Text = ResourceMessageText.GetString("WOVersionControlMsg").Replace("<Link>", "AdminWODetails.aspx?WOID=" & Request("WOID") & "&WorkOrderID=" & Request("WorkOrderID") & "&SupContactId=" & Request("SupContactId") & "&SupCompId=" & Request("SupCompId") & "&Group=" & WOStatusText & "&CompID=" & Request("CompID") & "&FromDate=&ToDate=&PS=" & Request("PS") & "&PN=0&SC=DateCreated&SO=1&Viewer=" & ViewState("Viewer") & "&sender=UCWOsListing&BizDivID=" & BizDivID)
                    If (ds_WOSuccess.Tables.Count > 1) Then
                        If (ds_WOSuccess.Tables(1).Rows.Count > 0) Then
                            pnlChangedData.Visible = True
                            repChangedData.DataSource = ds_WOSuccess.Tables(1)
                            repChangedData.DataBind()
                        End If
                    End If
                Else
                    UCFileUpload1.ClearUCFileUploadCache()

                    Dim link As String
                    link = getBackToListingLink("Confirm", "Completed")

                    Dim GroupName As String
                    If ParentName.ToLower = "woclose" Then
                        GroupName = "Closed"
                    Else
                        GroupName = "Completed"
                    End If

                    'Added by Hemisha on 28/02/2014 to send the status to SONY
                    If CInt(Request("CompanyID")) = 16363 Then

                        If GroupName = "Completed" Then
                            CommonFunctions.SendStatusUpdateToSONY(RefWOID, PONumber, "EXECU", StartDate)
                        End If

                        Dim strAttachmenURL As System.Net.Mail.Attachment

                        If GroupName = "Closed" Then
                            If Not IsNothing(ds_WOSuccess.Tables("tblAttachments")) Then
                                If ds_WOSuccess.Tables("tblAttachments").Rows.Count > 0 Then
                                    For Each objDataRow As DataRow In ds_WOSuccess.Tables("tblAttachments").Rows
                                        If objDataRow("LinkSource").ToString.ToLower = "wocomplete" Then
                                            strAttachmenURL = New System.Net.Mail.Attachment(objDataRow("FilePath").ToString)
                                        End If
                                    Next
                                End If
                            End If

                            If strAttachmenURL.ToString.Length > 0 Then
                                Emails.SendEmailToSONYonWOClose(PONumber, strAttachmenURL)
                            End If

                        End If
                    End If
                    If GroupName = "Closed" Then
                        CreateAndSendUpsellInvoice(ds_WOSuccess)
                    End If
                    Response.Redirect(link)

                    ''pnlBackToListing.Visible = True
                    'pnlWOProcess.Visible = False
                    'divValidationMain.Visible = False


                End If
            Else
                UCFileUpload1.ClearUCFileUploadCache()

                Dim link As String
                link = getBackToListingLink("Confirm", "Completed")

                Dim GroupName As String
                If ParentName.ToLower = "woclose" Then
                    GroupName = "Closed"
                Else
                    GroupName = "Completed"
                End If

                'Added by Hemisha on 28/02/2014 to send the status to SONY
                If CInt(Request("CompanyID")) = 16363 Then

                    If GroupName = "Completed" Then
                        CommonFunctions.SendStatusUpdateToSONY(RefWOID, PONumber, "EXECU", StartDate)
                    End If

                    Dim strAttachmenURL As System.Net.Mail.Attachment

                    If GroupName = "Closed" Then
                        If Not IsNothing(ds_WOSuccess.Tables("tblAttachments")) Then
                            If ds_WOSuccess.Tables("tblAttachments").Rows.Count > 0 Then
                                For Each objDataRow As DataRow In ds_WOSuccess.Tables("tblAttachments").Rows
                                    If objDataRow("LinkSource").ToString.ToLower = "wocomplete" Then
                                        strAttachmenURL = New System.Net.Mail.Attachment(objDataRow("FilePath").ToString)
                                    End If
                                Next
                            End If
                        End If

                        If strAttachmenURL.ToString.Length > 0 Then
                            Emails.SendEmailToSONYonWOClose(PONumber, strAttachmenURL)
                        End If

                    End If
                End If
                If GroupName = "Closed" Then
                    CreateAndSendUpsellInvoice(ds_WOSuccess)
                End If
                Response.Redirect(link)

                ''pnlBackToListing.Visible = True
                'pnlWOProcess.Visible = False
                'divValidationMain.Visible = False

            End If
        Else
            divValidationMain.Visible = True
        End If
    End Sub

    Private Sub sendMailer(ByVal ds_WOSuccess As DataSet, ByVal ContactId As Integer)
        'send mailer to respective party

        Dim MailerViewer As String = ""
        Dim ds_WO As New DataSet
        ds_WO = GetActionDetails()
        With ds_WO.Tables("tblWorkOrderSummary").Rows(0)
            objEmail.WorkOrderID = .Item("RefWOID")
            objEmail.WOLoc = .Item("Location")
            objEmail.WOTitle = .Item("WOTitle")
            objEmail.WOCategory = .Item("WOCategory")
            objEmail.WPPrice = .Item("WholesalePrice")
            objEmail.PPPrice = .Item("PlatformPrice")
            objEmail.WOStartDate = Strings.FormatDateTime(.Item("DateStart"), DateFormat.ShortDate)
            objEmail.WOEndDate = Strings.FormatDateTime(.Item("DateEnd"), DateFormat.ShortDate)
            If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                If Not IsDBNull(.Item("WholesaleDayJobRate")) Then
                    objEmail.WorkOrderWholesaleDayRate = .Item("WholesaleDayJobRate")
                    ViewState("WorkOrderWholesaleDayRate") = .Item("WholesaleDayJobRate")
                End If
                If Not IsDBNull(.Item("EstimatedTimeInDays")) Then
                    objEmail.EstimatedTimeInDays = .Item("EstimatedTimeInDays")
                End If

                If Not IsDBNull(.Item("PlatformDayJobRate")) Then
                    objEmail.WorkOrderDayRate = .Item("PlatformDayJobRate")
                End If
                If IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("StagedWO")) Then
                    ViewState("StagedWO") = "No"
                Else
                    ViewState("StagedWO") = Trim(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("StagedWO"))
                End If

                If Not IsDBNull(ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PricingMethod")) Then
                    ViewState("PricingMethod") = ds_WO.Tables("tblWorkOrderSummary").Rows(0).Item("PricingMethod")
                End If
            End If
        End With

        'Dim dv_CCBuyer As New DataView
        'Dim dv_CCSupplier As New DataView
        Dim dvEmail As New DataView
        Dim status As String
        Select Case ParentName.ToLower
            Case "wocomplete"
                status = ApplicationSettings.WOAction.WOComplete
            Case "woclose"
                status = ApplicationSettings.WOAction.WOClose
        End Select

        If Not IsNothing(ViewState("WorkOrderWholesaleDayRate")) Then
            objEmail.WorkOrderWholesaleDayRate = ViewState("WorkOrderWholesaleDayRate") 'For Buyer
        End If
        objEmail.StagedWO = ViewState("StagedWO")

        Select Case ParentName.ToLower
            Case "woclose"
                'Mail will not be sent to the Admin, Supplier and Buyer on Closure of the workorder
                'dvEmail = ds_WOSuccess.Tables("Mailers").Copy.DefaultView

                'If BizDivID = ApplicationSettings.OWDEBizDivId And Viewer <> ApplicationSettings.ViewerAdmin Then
                '    If Viewer = ApplicationSettings.ViewerBuyer Then
                '        MailerViewer = ResourceMessageText.GetString("Buyer")
                '    Else
                '        MailerViewer = ResourceMessageText.GetString("Supplier")
                '    End If
                'End If
                'Emails.SendWorkOrderMail(objEmail, MailerViewer, status, txtCompleteComments.Text, dvEmail)
            Case "wocomplete"
                dvEmail = ds_WOSuccess.Tables("Mailers").Copy.DefaultView

                If BizDivID = ApplicationSettings.OWDEBizDivId And Viewer <> ApplicationSettings.ViewerAdmin Then
                    If Viewer = ApplicationSettings.ViewerBuyer Then
                        MailerViewer = ResourceMessageText.GetString("Buyer")
                    Else
                        MailerViewer = ResourceMessageText.GetString("Supplier")
                    End If
                End If
                Emails.SendWorkOrderMail(objEmail, MailerViewer, status, txtCompleteComments.Text, dvEmail)
        End Select

        'Dim link, GroupName As String
        'If ParentName.ToLower = "woclose" Then
        '    GroupName = "Closed"
        'Else
        '    GroupName = "Completed"
        'End If
        'link = getBackToListingLink("Confirm", GroupName)
        'Response.Redirect(link)

        'pnlBackToListing.Visible = True
        pnlWOProcess.Visible = False
        divValidationMain.Visible = False
    End Sub

    'Private Sub lnkCancel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.ServerClick
    Private Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click
        Dim link As String
        link = getBackToListingLink("Cancel")
        Response.Redirect(link)
    End Sub
    'Added Code by Pankaj Malav on 23 Dec 2008
    'To cater for Search WO Raise Issue Processing on Back to listing and on confirm
    Public Function getBackToListingLink(ByVal BtnIdentify As String, Optional ByVal GroupName As String = "") As String
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            If Request("sender") = "UCWOsListing" Or BtnIdentify = "Confirm" Or Request("sender") = "UCWODetails" Then
                If Request("sender") = "SearchWO" Then
                    link = "SearchWOs.aspx?"
                    link &= "&SrcWorkorderID=" & Request("txtWorkorderID")
                    link &= "&SrcCompanyName=" & Request("CompanyName")
                    link &= "&SrcKeyword=" & Request("KeyWord")
                    link &= "&SrcPONumber=" & Request("PONumber")
                    link &= "&SrcPostCode=" & Request("PostCode")
                    link &= "&SrcDateStart=" & Request("DateStart")
                    link &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
                    link &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    Return (link)
                Else
                    If Viewer = ApplicationSettings.ViewerSupplier Then
                        link = "~\SecurePages\SupplierWOListing.aspx?"
                    ElseIf Viewer = ApplicationSettings.ViewerAdmin Then
                        link = "~\AdminWOListing.aspx?"
                    ElseIf Viewer = ApplicationSettings.ViewerBuyer Then
                        link = "~\SecurePages\BuyerWOListing.aspx?"
                    End If
                End If
            ElseIf Request("sender") = "SearchWO" Then
                link = "SearchWOs.aspx?"
                link &= "&SrcWorkorderID=" & Request("txtWorkorderID")
                link &= "&SrcCompanyName=" & Request("CompanyName")
                link &= "&SrcKeyword=" & Request("KeyWord")
                link &= "&SrcPONumber=" & Request("PONumber")
                link &= "&SrcReceiptNumber=" & Request("ReceiptNumber")
                link &= "&SrcPostCode=" & Request("PostCode")
                link &= "&SrcDateStart=" & Request("DateStart")
                link &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
                link &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")
                link &= "&PS=" & Request("PS")
                link &= "&PN=" & Request("PN")
                link &= "&SC=" & Request("SC")
                link &= "&SO=" & Request("SO")
                Return (link)
            Else
                If Request("Viewer") = "Supplier" Then
                    link = "SupplierWODetails.aspx?"
                ElseIf Request("Viewer") = "Buyer" Then
                    link = "BuyerWODetails.aspx?"
                ElseIf Request("Viewer") = "Admin" Then
                    link = "AdminWODetails.aspx?"
                End If
            End If
            link &= "WOID=" & Request("WOID")
            link &= "&ContactID=" & Request("ContactID")
            link &= "&CompanyID=" & Request("CompanyID")
            link &= "&Viewer=" & Request("Viewer")
            If GroupName <> "" Then
                link &= "&Group=" & GroupName
                link &= "&mode=" & GroupName
            Else
                link &= "&Group=" & Request("Group")
                link &= "&mode=" & Request("Group")
            End If
            link &= "&BizDivID=" & Request("BizDivID")
            link &= "&FromDate=" & Request("FromDate")
            link &= "&ToDate=" & Request("ToDate")
            link &= "&PS=" & Request("PS")
            link &= "&PN=" & Request("PN")
            link &= "&SC=" & Request("SC")
            If Not IsNothing(Request("SearchWorkorderID")) Then
                link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
            End If
            link &= "&SO=" & Request("SO")
            link &= "&sender=" & "UCWOsListing"
            If Not IsNothing(Request("IsNextDay")) Then
                If Request("IsNextDay") = "Yes" Then
                    link &= "&IsNextDay=" & "Yes"
                Else
                    link &= "&IsNextDay=" & "No"
                End If
            End If
        End If
        Return link
    End Function


    Private Sub rqRating_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rqRating.ServerValidate
        If rdoRatingNeutral.Checked = False And rdoRatingNegative.Checked = False And rdoRatingPositive.Checked = False Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    Private Sub rqSLA_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rqSLA.ServerValidate
        If (ViewState("SLA") = 1) Then
            If rdoSLAMetYes.Checked = False And rdoSLAMetNo.Checked = False Then
                args.IsValid = False
            Else
                args.IsValid = True
            End If
        Else
            args.IsValid = True
        End If
    End Sub


    'Protected Sub btnBackToCompList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackToCompList.Click
    '    Dim link As String
    '    link = getBackToListingLink("Confirm", "Completed")
    '    Response.Redirect(link)
    'End Sub


    Protected Sub btnBackToCloseList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackToCloseList.Click
        Dim link As String
        link = getBackToListingLink("Confirm", "Closed")
        Response.Redirect(link)
    End Sub


    Private Sub cstmFileUpload_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cstmFileUpload.ServerValidate

        If ParentName.ToLower = "wocomplete" Then
            Dim dsWorkOrderForAttach As New WorkOrderNew
            Dim flag As Boolean = True
            Dim err As String = ""
            dsWorkOrderForAttach = UCFileUpload1.ReturnFilledAttachments(dsWorkOrderForAttach)
            If Not IsNothing(dsWorkOrderForAttach.Tables("tblAttachmentInfo")) Then
                If dsWorkOrderForAttach.Tables("tblAttachmentInfo").Rows.Count > 0 Then
                    If dsWorkOrderForAttach.Tables("tblAttachmentInfo").Rows.Count > 10 Then
                        cstmFileUpload.ErrorMessage = "You have reached your limit to a maximum of 10 documents"
                        args.IsValid = False
                    End If
                    'Else
                    '    args.IsValid = False
                    '    cstmFileUpload.ErrorMessage = "Warning:  to mark this order as complete you must upload a digital copy of the sign off sheet in either PDF or JPG format."
                    '    validationSummarySubmit.Font.Size = "11"
                End If
                'Else
                '    args.IsValid = False
                '    cstmFileUpload.ErrorMessage = "Warning:  to mark this order as complete you must upload a digital copy of the sign off sheet in either PDF or JPG format."
                '    validationSummarySubmit.Font.Size = "11"
            End If
        End If
    End Sub

End Class