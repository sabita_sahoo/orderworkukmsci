Imports System
Imports System.Data
Imports System.Threading

Partial Public Class UCMSWOForm
    Inherits System.Web.UI.UserControl
    Public Shared ws As New WSObjs
    Private Delegate Sub AfterWOSave(ByVal dsSendMail As DataSet)
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents UCFileUpload1 As UCFileUpload_Outer
    Protected WithEvents chkbxIncludeWeekends, chkbxIncludeHolidays As System.Web.UI.WebControls.CheckBox
    Protected WithEvents UCAccreditations As UCAccreditationForSearch
    Dim WOCustomerPrice As Decimal
    Protected WithEvents txtRelatedWorkorder As System.Web.UI.WebControls.TextBox



#Region "Properties"
    ''' <summary>
    ''' Workorder id. to check if populate the UC or to open a blank form
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOID As Integer = 0
    Public Property WOID() As Integer
        Get
            Return _WOID
        End Get
        Set(ByVal value As Integer)
            _WOID = value
        End Set
    End Property

    ''' <summary>
    ''' Company id of the user who has logged in
    ''' </summary>
    ''' <remarks></remarks>
    Private _CompanyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _CompanyID
        End Get
        Set(ByVal value As Integer)
            _CompanyID = value
        End Set
    End Property

    ''' <summary>
    ''' Contact id of the user who has logged in
    ''' </summary>
    ''' <remarks></remarks>
    Private _ContactID As Integer
    Public Property ContactID() As Integer
        Get
            Return _ContactID
        End Get
        Set(ByVal value As Integer)
            _ContactID = value
        End Set
    End Property
    ''' <summary>
    ''' Company name of the logged in user
    ''' </summary>
    ''' <remarks></remarks>
    Private _Contact As String
    Public Property Contact() As String
        Get
            Return _Contact
        End Get
        Set(ByVal value As String)
            _Contact = value
        End Set
    End Property
    ''' <summary>
    ''' Contact name of the logged in user
    ''' </summary>
    ''' <remarks></remarks>
    Private _ContactName As String
    Public Property ContactName() As String
        Get
            Return _ContactName
        End Get
        Set(ByVal value As String)
            _ContactName = value
        End Set
    End Property

    ''' <summary>
    ''' ClassID of the logged in user - 1 for Client/ 2 for Supplier
    ''' </summary>
    ''' <remarks></remarks>
    Private _ClassID As Integer
    Public Property ClassID() As Integer
        Get
            Return _ClassID
        End Get
        Set(ByVal value As Integer)
            _ClassID = value
        End Set
    End Property

    ''' <summary>
    ''' BizDivID
    ''' </summary>
    ''' <remarks></remarks>
    Private _BizDivID As Integer
    Public Property BizDivID() As Integer
        Get
            Return _BizDivID
        End Get
        Set(ByVal value As Integer)
            _BizDivID = value
        End Set
    End Property

    ''' <summary>
    ''' Property to check the source of the UC
    ''' </summary>
    ''' <remarks></remarks>
    Private _Src As String = ""
    Public Property Src() As String
        Get
            Return _Src
        End Get
        Set(ByVal value As String)
            _Src = value
        End Set
    End Property

    ''' <summary>
    ''' RoleGroup ID - to check whether the logged in user is an Administrator/Manager/Specialist/User
    ''' </summary>
    ''' <remarks></remarks>
    Private _RoleGroupID As Integer
    Public Property RoleGroupID() As Integer
        Get
            Return _RoleGroupID
        End Get
        Set(ByVal value As Integer)
            _RoleGroupID = value
        End Set
    End Property
    Private _AdminCompID As Integer = 0
    Public Property AdminCompID() As Integer
        Get
            Return _AdminCompID
        End Get
        Set(ByVal value As Integer)
            _AdminCompID = value
        End Set
    End Property

    Private _AdminConID As Integer = 0
    Public Property AdminConID() As Integer
        Get
            Return _AdminConID
        End Get
        Set(ByVal value As Integer)
            _AdminConID = value
        End Set
    End Property
    ''' <summary>
    ''' Viewer - of the UC - Admin / buyer
    ''' </summary>
    ''' <remarks></remarks>
    Private _Viewer As String
    Public Property Viewer() As String
        Get
            Return _Viewer
        End Get
        Set(ByVal value As String)
            _Viewer = value
        End Set
    End Property

    ''' <summary>
    ''' Field which decides whether to show or hide the Show Loc to Supplier check box
    ''' </summary>
    ''' <remarks></remarks>
    Private _ShowLocCheckBox As Boolean = True
    Public Property ShowLocCheckBox() As Boolean
        Get
            Return _ShowLocCheckBox
        End Get
        Set(ByVal value As Boolean)
            _ShowLocCheckBox = value
        End Set
    End Property
#End Region

#Region "Declarations"
    Protected WithEvents CWOFileAttach As UCFileUpload_Outer
    Protected WithEvents hdnbtnTrigger As System.Web.UI.WebControls.Button

    Protected WithEvents hdnProductId As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblspanPhone As System.Web.UI.WebControls.Label
    Protected WithEvents lblspanMobile As System.Web.UI.WebControls.Label
    Protected WithEvents ReqMobile As System.Web.UI.WebControls.RequiredFieldValidator
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not IsPostBack Then
        If Not IsPostBack Then
            Session("ClientQAnsId") = Nothing
            Session("ClientQ") = Nothing

            UCAccreditations.Type = "ServiceAccreditations"
            UCAccreditations.Mode = "ServiceAccreditations"
            UCAccreditations.BizDivId = BizDivID


            CWOFileAttach.CompanyID = CompanyID
            CWOFileAttach.ClearUCFileUploadCache()
            CWOFileAttach.BizDivID = BizDivID
            CWOFileAttach.Type = "WorkOrder"
            CWOFileAttach.AttachmentForID = WOID
            CWOFileAttach.CompanyID = CompanyID
            CWOFileAttach.ExistAttachmentSourceID = CompanyID
            CWOFileAttach.ClearUCFileUploadCache()
            CWOFileAttach.PopulateAttachedFiles()
            CWOFileAttach.PopulateExistingFiles()

            UCFileUpload1.BizDivID = BizDivID
            UCFileUpload1.Type = "StatementOfWork"
            UCFileUpload1.AttachmentForID = WOID
            UCFileUpload1.ClearUCFileUploadCache()
            UCFileUpload1.PopulateAttachedFiles()
            UCFileUpload1.PopulateExistingFiles()
            hdnContactName.Value = ContactName
            hdnCurrency.Value = ApplicationSettings.Currency
            'Populating all the details (Standards and Location details)
            PopulateStandards()
            tblInvoiceTitle.Visible = True
            chkShowLoc.Visible = ShowLocCheckBox
            chkShowLoc.Checked = ShowLocCheckBox
            If (WOID = 0) And (Src = "NewWO") Then
                'Check if WOID is zero and src = 'NewWO'. If yes then load a blank form else populate the data
                lblHeading.Text = ResourceMessageText.GetString("NewWOHeadingMS")

                lblNoOfWOText.Visible = True
                txtNoOfWO.Visible = True

                'ddlLocation.SelectedValue = ""
                'PopulateLocation()
                ddlWOSubType.Enabled = False
                If (CompanyID <> 0) And WOID = 0 Then
                    tdViewBtn.Visible = False
                    divWOLoc.Style.Add("display", "block")

                    ViewState("BuyerCompanyId") = CompanyID
                    ViewState("BuyerContactId") = ContactID
                    hdnContactID.Value = CompanyID

                    ViewState("WOID") = 0
                    pnlContact.Visible = False
                    pnlEditCompName.Visible = True
                    PopulateAllDetails()
                    If Not IsNothing(ViewState("CompanyName")) Then
                        txtbxEditCompName.Text = ViewState("CompanyName")
                        txtbxEditCompName.Enabled = False
                        ddlLocation.SelectedValue = ""
                    End If
                    If (WOID = 0) And (Src = "NewWO") Then
                        'Added code by Pankaj Malav on 96 Oct 2008 to retrieve AM Status of selected user in ucsearch contact
                        If Not IsNothing(ViewState("IsAMEnabled")) Then
                            If ViewState("IsAMEnabled") = True Then
                                chkIngoreAutomatch.Enabled = True
                                chkIngoreAutomatch.Visible = True
                                ViewState("IsAMIgnored") = False
                                chkIngoreAutomatch.Checked = False
                            Else
                                chkIngoreAutomatch.Visible = False
                            End If
                        Else
                            ViewState("IsAMEnabled") = False
                            chkIngoreAutomatch.Visible = False
                        End If

                    End If
                Else
                    UCSearchContact1.txtContact.Disabled = False
                    tdViewBtn.Visible = True
                    divWOLoc.Style.Add("display", "none")
                End If
            ElseIf (WOID <> 0) And ((Src = "EditWO")) Then
                'IF WOID <> 0 and Src is either site or Admin the populate the Workorder form from tblWOrkorder
                lblNoOfWOText.Visible = False
                txtNoOfWO.Visible = False
                ' btnFind.Enabled = False
                populateWODetails(WOID)
                tdViewBtn.Visible = False
                divWOLoc.Style.Add("display", "block")
                hdnContactID.Value = CompanyID
                reqtxtProductsPur.Enabled = False
                SpanProductsPurRedStar.Visible = False

                If CompanyID = ApplicationSettings.Khharrods Or CompanyID = ApplicationSettings.DixonRetailForm Then
                    lblspanPhone.Visible = False
                    RequiredFieldValidator8.Enabled = False
                    lblspanMobile.Visible = True
                    ReqMobile.Enabled = True
                Else
                    lblspanPhone.Visible = True
                    RequiredFieldValidator8.Enabled = True
                    lblspanMobile.Visible = False
                    ReqMobile.Enabled = False
                End If

            ElseIf (WOID <> 0) And ((Src = "CreateSimilarWOFromTemplate")) Then
                lblNoOfWOText.Visible = False
                txtNoOfWO.Visible = False
                populateWODetailsFromTemplate(WOID)

                If CompanyID = ApplicationSettings.Khharrods Or CompanyID = ApplicationSettings.DixonRetailForm Then
                    lblspanPhone.Visible = False
                    RequiredFieldValidator8.Enabled = False
                    lblspanMobile.Visible = True
                    ReqMobile.Enabled = True
                Else
                    lblspanPhone.Visible = True
                    RequiredFieldValidator8.Enabled = True
                    lblspanMobile.Visible = False
                    ReqMobile.Enabled = False
                End If
            End If
        End If

    End Sub

#Region "Attachments"
    ''' <summary>
    ''' Function to create the cache for the attached files of the workorder
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="source"></param>
    ''' <param name="dsView"></param>
    ''' <remarks></remarks>
    Private Sub SetCacheForAttachment(ByVal ID As Integer, ByVal source As String, ByVal dsView As DataView)
        Dim Cachekey As String = ""
        Cachekey = "AttachedFiles-" & ID & "-" & source & "-" & Session.SessionID
        If Page.Cache(Cachekey) Is Nothing Then
            dsView.RowFilter = "LinkSource = '" & source & "'"
            Page.Cache.Add(Cachekey, dsView, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        End If
    End Sub

    ''' <summary>
    ''' Function to create the cache for existing files from the compnay profile page
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="source"></param>
    ''' <param name="dsView"></param>
    ''' <remarks></remarks>
    Private Sub SetCacheForExistingAttachment(ByVal ID As Integer, ByVal source As String, ByVal dsView As DataView)
        Dim Cachekey As String = ""
        Cachekey = "ExistingFiles-" & ID & "-" & source & "-" & Session.SessionID
        If Page.Cache(Cachekey) Is Nothing Then
            dsView.RowFilter = "LinkSource = '" & source & "'"
            Page.Cache.Add(Cachekey, dsView, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        End If
    End Sub

    ''' <summary>
    ''' Populating Product Attachments
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub hdnbtnTrigger_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnbtnTrigger.Click
        CWOFileAttach.CompanyID = Session("CompanyID")
        CWOFileAttach.ClearUCFileUploadCache()
        CWOFileAttach.BizDivID = ApplicationSettings.BizDivId
        CWOFileAttach.Type = "WorkOrder"
        CWOFileAttach.AttachmentForID = hdnProductId.Value
        CWOFileAttach.AttachmentForSource = "WOProduct"
        CWOFileAttach.CompanyID = Session("CompanyID")
        CWOFileAttach.ExistAttachmentSourceID = Session("CompanyID")
        CWOFileAttach.ClearUCFileUploadCache()
        CWOFileAttach.PopulateAttachedFiles()
        CWOFileAttach.AttachmentForSource = "WorkOrder"
        CWOFileAttach.AttachmentForID = 0

        UCAccreditations.Type = "ServiceAccreditations"
        UCAccreditations.Mode = "ServiceAccreditations"
        UCAccreditations.BizDivId = BizDivID
        UCAccreditations.CommonID = hdnSelectedService.Value
        UCAccreditations.PopulateSelectedGrid(True)
    End Sub

#End Region

#Region "Server Validations"
    ''' <summary>
    ''' The schedule window end date cannot be earlier to the begin date
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Public Sub custValiDateRange_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rqWODateRange.ServerValidate
        If Page.IsValid Then
            If txtScheduleWInBegin.Text.Trim <> "" And txtScheduleWInEnd.Text.Trim <> "" Then
                Dim DateStart As New Date
                Dim DateEnd As New Date
                DateStart = txtScheduleWInBegin.Text.Trim
                DateEnd = txtScheduleWInEnd.Text.Trim
                If DateStart <= DateEnd Then
                    args.IsValid = True
                Else
                    args.IsValid = False
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Custom validator to check if zero value is entered in the Estimated time field
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Public Sub ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustProposedPriceValidator.ServerValidate
        Try
            Dim num As Int32 = Int32.Parse(CInt(args.Value))
            If rdoNon0ValueWP.Checked = True Then
                If num <> 0 Then
                    args.IsValid = True
                Else
                    args.IsValid = False
                End If
                Exit Sub
            End If
        Catch exc As Exception
        End Try
        args.IsValid = True
    End Sub

#End Region

    ''' <summary>
    ''' Populate the Location
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateLocation(Optional ByVal Flag As Boolean = True)
        'Dim ds_loc As DataSet

        If ddlLocation.SelectedValue = "" Then
            'Added code by Pankaj Malav on 20 Oct 2008
            lbltxtCompLocName.Text = "Company Name"
            'for temp loc
            If Flag = True Then
                ClearFormForTemporaryLocation()
            End If
            If Src = "EditWO" Then
                RqdFName.Enabled = True
                RqdLName.Enabled = True
                tblContactInfo.Style.Add("visibility", "visible")
                txtName.Enabled = True
                txtCompanyAddress.Enabled = True
                txtCity.Enabled = True
                txtCounty.Enabled = True
                txtCompanyPostalCode.Enabled = True
                'ddlCompanyCountry.Enabled = True
                txtCompanyFax.Enabled = True
                txtMobile.Enabled = True
                txtCompanyPhone.Enabled = True
            End If
        End If
    End Sub

    ''' <summary>
    ''' Clearing Form for Temporary Location
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearFormForTemporaryLocation()
        'for temp loc
        tblContactInfo.Style.Add("visibility", "visible")

        RqdFName.Enabled = True
        RqdLName.Enabled = True

        txtName.Text = ""
        txtCompanyAddress.Text = ""
        txtCity.Text = ""
        txtCounty.Text = ""
        txtCompanyPostalCode.Text = ""
        txtCompanyFax.Text = ""
        txtMobile.Text = ""
        txtCompanyPhone.Text = ""

        txtName.Enabled = True
        txtCompanyAddress.Enabled = True
        txtCity.Enabled = True
        txtCounty.Enabled = True
        txtCompanyPostalCode.Enabled = True
        'ddlCompanyCountry.Enabled = True
        txtCompanyFax.Enabled = True
        txtMobile.Enabled = True
        txtCompanyPhone.Enabled = True

    End Sub

    ''' <summary>
    ''' Function to populate the WO 
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <remarks></remarks>
    Private Sub populateWODetails(ByVal WOID As Integer)




        Dim ds As DataSet = ws.WSWorkOrder.MS_WOEditWOGetDetails(WOID)
        pnlContact.Visible = False
        pnlEditCompName.Visible = True

        Dim CompanyId As String = ""
        CompanyId = ds.Tables("tblSummary").Rows(0).Item("CompanyId")

        If CompanyId = ApplicationSettings.OutsourceCompany Or CompanyId = ApplicationSettings.ExponentialECompany Or CompanyId = ApplicationSettings.ITECHSOCompany Or CompanyId = ApplicationSettings.DemoCompany Then
            drpTime.Items.Clear()
            'drpTime.Items.Insert(0, New ListItem("Please Select", ""))
            drpTime.Items.Insert(0, New ListItem("Arrival 0 - 1", "Arrival 0 - 1"))
            drpTime.Items.Insert(1, New ListItem("Arrival 1 - 2", "Arrival 1 - 2"))
            drpTime.Items.Insert(2, New ListItem("Arrival 2 - 3", "Arrival 2 - 3"))
            drpTime.Items.Insert(3, New ListItem("Arrival 3 - 4", "Arrival 3 - 4"))
            drpTime.Items.Insert(4, New ListItem("Arrival 4 - 5", "Arrival 4 - 5"))
            drpTime.Items.Insert(5, New ListItem("Arrival 5 - 6", "Arrival 5 - 6"))
            drpTime.Items.Insert(6, New ListItem("Arrival 6 - 7", "Arrival 6 - 7"))
            drpTime.Items.Insert(7, New ListItem("Arrival 7 - 8", "Arrival 7 - 8"))
            drpTime.Items.Insert(8, New ListItem("Arrival 8 - 9", "Arrival 8 - 9"))
            drpTime.Items.Insert(9, New ListItem("Arrival 9 - 10", "Arrival 9 - 10"))
            drpTime.Items.Insert(10, New ListItem("Arrival 10 - 11", "Arrival 10 - 11"))
            drpTime.Items.Insert(11, New ListItem("Arrival 11 - 12", "Arrival 11 - 12"))
            drpTime.Items.Insert(12, New ListItem("Arrival 12 - 13", "Arrival 12 - 13"))
            drpTime.Items.Insert(13, New ListItem("Arrival 13 - 14", "Arrival 13 - 14"))
            drpTime.Items.Insert(14, New ListItem("Arrival 14 - 15", "Arrival 14 - 15"))
            drpTime.Items.Insert(15, New ListItem("Arrival 15 - 16", "Arrival 15 - 16"))
            drpTime.Items.Insert(16, New ListItem("Arrival 16 - 17", "Arrival 16 - 17"))
            drpTime.Items.Insert(17, New ListItem("Arrival 17 - 18", "Arrival 17 - 18"))
            drpTime.Items.Insert(18, New ListItem("Arrival 18 - 19", "Arrival 18 - 19"))
            drpTime.Items.Insert(19, New ListItem("Arrival 19 - 20", "Arrival 19 - 20"))
            drpTime.Items.Insert(20, New ListItem("Arrival 20 - 21", "Arrival 20 - 21"))
            drpTime.Items.Insert(21, New ListItem("Arrival 21 - 22", "Arrival 21 - 22"))
            drpTime.Items.Insert(22, New ListItem("Arrival 22 - 23", "Arrival 22 - 23"))
            drpTime.Items.Insert(23, New ListItem("Arrival 23 - 24", "Arrival 23 - 24"))
            'OM-153 Added by Sabita New Slot for Amazon
        ElseIf CompanyId = ApplicationSettings.AmazonCompany Then
            drpTime.Items.Clear()
            drpTime.Items.Insert(0, New ListItem("Arrival 8 - 11", "Arrival 8 - 11"))
            'drpTime.Items.Insert(1, New ListItem("Arrival 9 - 12", "Arrival 9 - 12"))
            'drpTime.Items.Insert(2, New ListItem("Arrival 10 - 13", "Arrival 10 - 13"))
            'drpTime.Items.Insert(3, New ListItem("Arrival 11 - 14", "Arrival 11 - 14"))
            drpTime.Items.Insert(4, New ListItem("Arrival 12 - 15", "Arrival 12 - 15"))
            'drpTime.Items.Insert(5, New ListItem("Arrival 13 - 16", "Arrival 13 - 16"))
            'drpTime.Items.Insert(6, New ListItem("Arrival 14 - 17", "Arrival 14 - 17"))
            'drpTime.Items.Insert(7, New ListItem("Arrival 15 - 18", "Arrival 15 - 18"))
            drpTime.Items.Insert(8, New ListItem("Arrival 16 - 19", "Arrival 16 - 19"))
        Else
            drpTime.Items.Clear()
            drpTime.Items.Insert(0, New ListItem("Please Select", "0"))
            drpTime.Items.Insert(1, New ListItem("8am - 1pm", "8am - 1pm"))
            drpTime.Items.Insert(2, New ListItem("1pm - 6pm", "1pm - 6pm"))
            drpTime.Items.Insert(3, New ListItem("All Day", "All Day"))
        End If


        'Store the version number in session

        CommonFunctions.StoreVerNum(ds.Tables("tblSummary"))

        'AutoMatch Settings. Show/Hide IgnoreAutomatch and Store AM Status in ViewState.
        Dim AMStatus As Boolean = ds.Tables("tblSummary").Rows(0).Item("AMStatus")
        Dim AMStatusFromTracking As Integer = ds.Tables("tblSummary").Rows(0).Item("AMStatusFromTracking")
        If (Viewer = "Admin") Then
            If AMStatus = True Then
                ViewState("IsAMEnabled") = True
                If AMStatusFromTracking <> 0 Then
                    chkIngoreAutomatch.Visible = True
                    chkIngoreAutomatch.Enabled = False
                    ViewState("IsAMIgnored") = True
                Else
                    chkIngoreAutomatch.Enabled = True
                    chkIngoreAutomatch.Visible = True
                    ViewState("IsAMIgnored") = False
                    chkIngoreAutomatch.Checked = True
                End If
            Else
                ViewState("IsAMEnabled") = False
                chkIngoreAutomatch.Visible = False
            End If
        End If
        ViewState("BusinessArea") = ds.Tables("tblSummary").Rows(0).Item("WOBusinessArea")
        ViewState("Location") = ds.Tables("tblSummary").Rows(0).Item("Location")

        lblHeading.Text = ResourceMessageText.GetString("EditWOHeadingMS") + ds.Tables("tblSummary").Rows(0).Item("WorkOrderId")
        ViewState("WorkOrderId") = ds.Tables("tblSummary").Rows(0).Item("WorkOrderId")

        Dim dt As DataTable
        dt = ds.Tables("tblWODetails")

        If dt.Rows.Count > 0 Then
            txtTitle.Text = dt.Rows(0).Item("WOTitle")
            If Not IsDBNull(dt.Rows(0).Item("DressCode")) Then
                ddlDressCode.SelectedIndex = ddlDressCode.Items.IndexOf(ddlDressCode.Items.FindByText(CStr(dt.Rows(0).Item("DressCode"))))
            Else
                ddlDressCode.SelectedIndex = 0
            End If
            txtPONumber.Text = dt.Rows(0).Item("PONumber")
            txtRelatedWorkorder.Text = dt.Rows(0).Item("RelatedWorkOrder")
            txtReceiptNumber.Text = dt.Rows(0).Item("ReceiptNumber")
            txtJRSNumber.Text = IIf(Not IsDBNull(dt.Rows(0).Item("JRSNumber")), dt.Rows(0).Item("JRSNumber"), "")
            txtWOLongDesc.Text = Server.HtmlDecode(Server.HtmlDecode(dt.Rows(0).Item("WOLongDesc").Replace("<BR>", Chr(13))))
            'txtWOLongDesc.Text = Server.HtmlDecode(Server.HtmlDecode(dt.Rows(0).Item("WOLongDesc").Replace("<p>", Chr(13))))
            'txtWOLongDesc.Text = Server.HtmlDecode(Server.HtmlDecode(dt.Rows(0).Item("WOLongDesc").Replace("<strong>", "")))
            'txtWOLongDesc.Text = dt.Rows(0).Item("WOLongDesc").Replace("<BR>", Chr(13))

            '''Code Added by Ambar 
            ''' To populate content of new field if present in DB
            If dt.Rows(0).Item("ClientScope").ToString.Trim = "" Then
                txtClientScope.Text = ""
            Else
                txtClientScope.Text = Server.HtmlDecode(Server.HtmlDecode(Server.HtmlDecode(dt.Rows(0).Item("ClientScope").Replace("<BR>", Chr(13)))))
            End If

            If IsDBNull(dt.Rows(0).Item("SpecialInstructions")) Then
                txtSpecialInstructions.Text = ""
            Else
                txtSpecialInstructions.Text = Server.HtmlDecode(Server.HtmlDecode(dt.Rows(0).Item("SpecialInstructions").Replace("<BR>", Chr(13)))) 'Server.HtmlEncode(dt.Rows(0).Item("SpecialInstructions").Replace("<BR>", Chr(13)))
            End If

            'for admin
            ViewState("BuyerCompanyId") = ds.Tables("tblSummary").Rows(0).Item("CompanyId")
            ViewState("BuyerContactId") = ds.Tables("tblSummary").Rows(0).Item("ContactId")
            ViewState("BuyerContactName") = ds.Tables("tblSummary").Rows(0).Item("Contact")
            ViewState("ContactClassID") = ds.Tables("tblSummary").Rows(0).Item("ContactClassID")
            ViewState("BizDivID") = ds.Tables("tblSummary").Rows(0).Item("BizDivID")
            ViewState("WOID") = WOID
            txtbxEditCompName.Text = ds.Tables("tblSummary").Rows(0).Item("BuyerCompany")
            txtbxEditCompName.Enabled = False
            PopulateAllDetails()

            If ds.Tables.Count > 2 Then
                If ds.Tables(2).Rows.Count <> 0 Then
                    PopulateLocation(ds.Tables(2), dt.Rows(0).Item("LocationId"))
                    ViewState("LocId") = dt.Rows(0).Item("LocationId")
                End If
            End If

            drpdwnGoodsCollection.SelectedValue = IIf(Not IsDBNull(dt.Rows(0).Item("GoodsLocation")), dt.Rows(0).Item("GoodsLocation"), 1)
            hdnDepotDetails.Value = IIf(Not IsDBNull(dt.Rows(0).Item("ServiceLocDetails")), dt.Rows(0).Item("ServiceLocDetails"), "")

            'txtProdName.Text = IIf(Not IsDBNull(dt.Rows(0).Item("ProductName")), dt.Rows(0).Item("ProductName"), "")

            ViewState("MainCatId") = dt.Rows(0).Item("MainCatId")
            ViewState("CombId") = dt.Rows(0).Item("WOCategoryId")
            hdnSubCategoryVal.Value = dt.Rows(0).Item("WOCategoryId")
            RepopulateSubCat()

            'Populate Billing Location in pricing section
            drpdwnBillingLocation.SelectedValue = dt.Rows(0).Item("BillingLocID")

            ViewState("WOStatus_Edit") = dt.Rows(0).Item("WOStatus")
            If Not IsDBNull(dt.Rows(0).Item("DataRef")) Then
                ViewState("DataRef") = dt.Rows(0).Item("DataRef")
            Else
                ViewState("DataRef") = 0
            End If

            'txtEstTimeRqrd.Text = dt.Rows(0).Item("EstimatedTimeInDays")
            If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("EstimatedTimeInDays")) Then
                    txtEstTimeRqrd.Text = ds.Tables("tblWODetails").Rows(0).Item("EstimatedTimeInDays")
                Else
                    txtEstTimeRqrd.Text = 1
                End If
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PricingMethod")) Then
                    If ds.Tables("tblWODetails").Rows(0).Item("PricingMethod").ToString.ToLower = "fixed" Then
                        'txtEstTimeRqrd.Enabled = False
                        txtEstTimeRqrd.Text = "1"
                    Else
                        txtEstTimeRqrd.Enabled = True
                    End If
                Else
                    txtEstTimeRqrd.Enabled = True
                End If
            End If


            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                txtInvoiceTitle.Text = IIf(Not IsDBNull(dt.Rows(0).Item("InvoiceTitle")), dt.Rows(0).Item("InvoiceTitle"), "")

                'If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                'populate UpSell related fields
                If Not IsDBNull(dt.Rows(0).Item("UpSellPrice")) Then
                    If Not CInt(dt.Rows(0).Item("UpSellPrice")) = 0 Then
                        txtUpSellPrice.Text = IIf(Not IsDBNull(dt.Rows(0).Item("UpSellPrice")), dt.Rows(0).Item("UpSellPrice"), "")
                        If txtUpSellPrice.Text <> "" And txtUpSellPrice.Text > 0 Then
                            rqtxtCustomerEmail.Enabled = True
                        Else
                            rqtxtCustomerEmail.Enabled = False
                        End If
                        txtUpSellInvoiceTitle.Text = IIf(Not IsDBNull(dt.Rows(0).Item("UpSellInvoiceTitle")), dt.Rows(0).Item("UpSellInvoiceTitle"), "OrderWork Services")
                        txtBCFName.Text = IIf(Not IsDBNull(dt.Rows(0).Item("BCFirstName")), dt.Rows(0).Item("BCFirstName"), "")
                        txtBCLName.Text = IIf(Not IsDBNull(dt.Rows(0).Item("BCLastName")), dt.Rows(0).Item("BCLastName"), "")
                        txtBCAddress.Text = IIf(Not IsDBNull(dt.Rows(0).Item("BCAddress")), dt.Rows(0).Item("BCAddress"), "")
                        txtBCCity.Text = IIf(Not IsDBNull(dt.Rows(0).Item("BCCity")), dt.Rows(0).Item("BCCity"), "")
                        txtBCCounty.Text = IIf(Not IsDBNull(dt.Rows(0).Item("BCCounty")), dt.Rows(0).Item("BCCounty"), "")
                        txtBCPostalCode.Text = IIf(Not IsDBNull(dt.Rows(0).Item("BCPostCode")), dt.Rows(0).Item("BCPostCode"), "")
                        txtBCPhone.Text = IIf(Not IsDBNull(dt.Rows(0).Item("BCPhone")), dt.Rows(0).Item("BCPhone"), "")
                        txtBCFax.Text = IIf(Not IsDBNull(dt.Rows(0).Item("BCFax")), dt.Rows(0).Item("BCFax"), "")

                        'Make UpSell related fields visible
                        tdUpSellPricehdr.Style.Add("display", "inline")
                        tdUpSellInvoicehdr.Style.Add("display", "inline")
                        tdUpSellPrice.Style.Add("display", "inline")
                        tdUpSellInvoice.Style.Add("display", "inline")
                        tblContactInfoUpSell.Style.Add("display", "inline")
                        ChkIncludeUpSell.Checked = True

                    Else
                        rqtxtCustomerEmail.Enabled = False
                    End If
                End If
                'End If

            End If
            'Logic for Saving fields depending upon business areas
            If Not IsNothing(ViewState("BusinessArea")) Then
                If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                    If Request.Browser.Browser = "IE" Then
                        txtProductsPur.Text = IIf(dt.Rows(0).Item("EquipmentDetails") = "", "", dt.Rows(0).Item("EquipmentDetails").Replace("<BR>", Chr(13)))
                        Dim temp1 As String = txtProductsPur.Text + Chr(13) + IIf(Not IsDBNull(dt.Rows(0).Item("AdditionalProducts")), IIf(dt.Rows(0).Item("AdditionalProducts") <> "", dt.Rows(0).Item("AdditionalProducts").Replace("<BR>", Chr(13)), ""), "")
                        txtProductsPur.Text = Server.HtmlDecode(Server.HtmlDecode(temp1))
                        Dim temp As String = IIf(Not IsDBNull(dt.Rows(0).Item("ClientSpecialInstructions")), IIf(dt.Rows(0).Item("ClientSpecialInstructions") <> "", dt.Rows(0).Item("ClientSpecialInstructions").Replace("<BR>", Chr(13)), ""), "")
                        txtClientInstru.Text = Server.HtmlDecode(Server.HtmlDecode(temp.Replace("<BR>", Chr(13))))
                    Else
                        txtProductsPur.Text = IIf(Not IsDBNull(dt.Rows(0).Item("EquipmentDetails")), IIf(dt.Rows(0).Item("EquipmentDetails") <> "", dt.Rows(0).Item("EquipmentDetails").Replace("<BR>", Chr(10)), ""), "")
                        Dim temp1 As String = txtProductsPur.Text + Chr(10) + IIf(Not IsDBNull(dt.Rows(0).Item("AdditionalProducts")), IIf(dt.Rows(0).Item("AdditionalProducts") <> "", dt.Rows(0).Item("AdditionalProducts").Replace("<BR>", Chr(10)), ""), "")
                        txtProductsPur.Text = Server.HtmlDecode(Server.HtmlDecode(temp1))
                        Dim temp As String = IIf(Not IsDBNull(dt.Rows(0).Item("ClientSpecialInstructions")), IIf(dt.Rows(0).Item("ClientSpecialInstructions") <> "", dt.Rows(0).Item("ClientSpecialInstructions").Replace("<BR>", Chr(10)), ""), "")
                        txtClientInstru.Text = Server.HtmlDecode(Server.HtmlDecode(temp.Replace("<BR>", Chr(13))))
                    End If
                    txtSalesAgent.Text = IIf(Not IsDBNull(dt.Rows(0).Item("SalesAgent")), dt.Rows(0).Item("SalesAgent"), "")
                    If Not IsDBNull(dt.Rows(0).Item("AptTime")) Then
                        If dt.Rows(0).Item("AptTime") = ApplicationSettings.EveningInstallation Then
                            'drpTime.Items.Remove(CStr(ApplicationSettings.AllDaySlot))
                            'Dim itemcount As Integer
                            'itemcount = drpTime.Items.Count
                            'Dim liInstallationTime As New ListItem
                            'liInstallationTime.Text = ApplicationSettings.EveningInstallation
                            'liInstallationTime.Value = ApplicationSettings.EveningInstallation
                            'drpTime.Items.Insert(itemcount, liInstallationTime)
                            'itemcount = itemcount + 1
                            'Dim liAllDayTime As New ListItem
                            'liAllDayTime.Text = ApplicationSettings.AllDaySlot
                            'liAllDayTime.Value = ApplicationSettings.AllDaySlot
                            'drpTime.Items.Insert(itemcount, liAllDayTime)
                            hdnDeduction.Value = "1"
                        End If
                        If Not drpTime.Items.FindByText(dt.Rows(0).Item("AptTime")) Is Nothing Then
                            drpTime.SelectedValue = dt.Rows(0).Item("AptTime")
                            hdnInstTime.Value = drpTime.SelectedValue
                        Else
                            Dim itemcount As Integer = 0
                            itemcount = drpTime.Items.Count
                            drpTime.Items.Insert(itemcount, New ListItem(dt.Rows(0).Item("AptTime"), dt.Rows(0).Item("AptTime")))
                            drpTime.SelectedValue = dt.Rows(0).Item("AptTime")
                            hdnInstTime.Value = drpTime.SelectedValue
                        End If

                    End If
                    If Not IsDBNull(dt.Rows(0).Item("FreesatMake")) Then
                        divFreesatMake.Style.Add("display", "block")
                        divFreesatMakeddl.Style.Add("display", "block")
                        Dim dsXML As New DataSet()
                        dsXML.ReadXml(MapPath("~/XML/WOFormStandards.xml"))
                        'get the dataview of table "Country", which is default table name
                        Dim dv_FreesatMake As DataView = dsXML.Tables("FreesatMake").DefaultView
                        drpdwnFreesatMake.DataSource = dv_FreesatMake
                        drpdwnFreesatMake.DataBind()
                        drpdwnFreesatMake.SelectedValue = dt.Rows(0).Item("FreesatMake")
                        hdnFreesat.Value = dt.Rows(0).Item("FreesatMake")
                    End If

                Else
                    If Request.Browser.Browser = "IE" Then
                        txtProductsPur.Text = IIf(Not IsDBNull(dt.Rows(0).Item("EquipmentDetails").ToString), IIf(dt.Rows(0).Item("EquipmentDetails").ToString <> String.Empty, dt.Rows(0).Item("EquipmentDetails").ToString.Replace("<BR>", Chr(13)), String.Empty), String.Empty)
                        Dim temp As String = IIf(Not IsDBNull(dt.Rows(0).Item("ClientSpecialInstructions").ToString), IIf(dt.Rows(0).Item("ClientSpecialInstructions").ToString <> String.Empty, dt.Rows(0).Item("ClientSpecialInstructions").ToString.Replace("<BR>", Chr(13)), String.Empty), String.Empty)
                        txtClientInstru.Text = Server.HtmlDecode(Server.HtmlDecode(temp.Replace("<BR>", Chr(13))))
                    Else
                        txtProductsPur.Text = IIf(Not IsDBNull(dt.Rows(0).Item("EquipmentDetails").ToString), IIf(dt.Rows(0).Item("EquipmentDetails").ToString <> String.Empty, dt.Rows(0).Item("EquipmentDetails").ToString.Replace("<BR>", Chr(10)), String.Empty), String.Empty)
                        Dim temp As String = IIf(Not IsDBNull(dt.Rows(0).Item("ClientSpecialInstructions").ToString), IIf(dt.Rows(0).Item("ClientSpecialInstructions").ToString <> String.Empty, dt.Rows(0).Item("ClientSpecialInstructions").ToString.Replace("<BR>", Chr(10)), String.Empty), String.Empty)
                        txtClientInstru.Text = Server.HtmlDecode(Server.HtmlDecode(temp.Replace("<BR>", Chr(13))))
                    End If
                End If
            End If

        End If


        dt = ds.Tables("tblSummary")

        If dt.Rows.Count > 0 Then

            If Viewer = "Admin" Then
                'Wholesale Price
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice")) Then
                    txtSpendLimitWP.Text = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.False, TriState.False)
                    WOCustomerPrice = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("CustomerPrice"), 2, TriState.True, TriState.False, TriState.False)
                    hdnDBWP.Value = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice"), 3, TriState.True, TriState.False, TriState.False)
                    hdnWPcomp.Value = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.False, TriState.False)
                    'If ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice") = 0 Then
                    '    rdo0ValueWP.Checked = True
                    '    'txtSpendLimitWP.Enabled = False
                    '    rdoNon0ValueWP.Checked = False
                    'End If
                    If dt.Rows(0).Item("ReviewBids") = True Then
                        rdo0ValueWP.Checked = True
                        'txtSpendLimitWP.Enabled = False
                        rdoNon0ValueWP.Checked = False
                    End If
                Else
                    txtSpendLimitWP.Text = FormatNumber(0, 2, TriState.True, TriState.False, TriState.False)
                End If
                'Proposed Price
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PlatformPrice")) Then
                    txtSpendLimitPP.Text = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("PlatformPrice"), 2, TriState.True, TriState.False, TriState.False)
                    If ds.Tables("tblWODetails").Rows(0).Item("PlatformPrice") = 0 Then
                        'rdo0ValuePP.Checked = True
                        ''txtSpendLimitPP.Enabled = False
                        'rdoNon0ValuePP.Checked = False
                        chkReviewBid.Visible = False
                    End If
                    If dt.Rows(0).Item("ReviewBids") = True Then
                        rdo0ValuePP.Checked = True
                        rdoNon0ValuePP.Checked = False
                    End If
                Else
                    txtSpendLimitPP.Text = FormatNumber(0, 2, TriState.True, TriState.False, TriState.False)
                End If
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PPPerRate")) Then
                    txtPPPerRate.Text = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("PPPerRate"), 2, TriState.True, TriState.False, TriState.False)
                Else
                    txtPPPerRate.Text = FormatNumber(0, 2, TriState.True, TriState.False, TriState.False)
                End If
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PerRate")) Then
                    ddPPPerRate.SelectedValue = ds.Tables("tblWODetails").Rows(0).Item("PerRate")
                End If
            End If


            'txtEstimatedTime.Text = dt.Rows(0).Item("EstimatedTime")


            If IsDBNull(dt.Rows(0).Item("DateEnd")) Then
                txtScheduleWInEnd.Text = ""
            Else
                Dim temp As Date = dt.Rows(0).Item("DateEnd")
                txtScheduleWInEnd.Text = Strings.FormatDateTime(temp, DateFormat.ShortDate)
            End If
            If IsDBNull(dt.Rows(0).Item("DateStart")) Then
                txtScheduleWInBegin.Text = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
            Else
                Dim temp As Date = dt.Rows(0).Item("DateStart")
                txtScheduleWInBegin.Text = Strings.FormatDateTime(temp, DateFormat.ShortDate)
            End If
            'Quantity
            If Not IsDBNull(dt.Rows(0).Item("Quantity")) Then
                txtQuantity.Text = dt.Rows(0).Item("Quantity")
            End If
            If dt.Rows(0).Item("ReviewBids") = True Then
                chkReviewBid.Checked = True
            Else
                chkReviewBid.Checked = False
            End If
            If dt.Rows(0).Item("IsSignOffSheetReqd") = True Then
                chkForceSignOff.Checked = True
            Else
                chkForceSignOff.Checked = False
            End If

            If dt.Rows(0).Item("IsWaitingToAutomatch") = True Then
                chkIsWaitingToAutomatch.Checked = True
            Else
                chkIsWaitingToAutomatch.Checked = False
            End If
        End If

        'if WO status = CA or Accepted - WO Description canot be editted.        
        If (ds.Tables("tblWODetails").Rows(0).Item("WOStatus") = ApplicationSettings.WOStatusID.Accepted) Or (ds.Tables("tblWODetails").Rows(0).Item("WOStatus") = ApplicationSettings.WOStatusID.CA) Then
            txtWOLongDesc.Enabled = False
        End If

        If Viewer = "Buyer" Then
            If ds.Tables("tblWODetails").Rows(0).Item("WOStatus") = ApplicationSettings.WOStatusID.Submitted Then
                'if wo is submitted n is being edited, dont allow price fields to be edited
                rdo0ValueWP.Enabled = False
                rdoNon0ValueWP.Enabled = False
                txtSpendLimitWP.Enabled = False
            End If
        ElseIf Viewer = "Admin" Then
            If (ds.Tables("tblWODetails").Rows(0).Item("WOStatusSent") = "Sent") Then
                rdo0ValuePP.Enabled = False
                rdoNon0ValuePP.Enabled = False
                'txtSpendLimitPP.Enabled = False
                chkReviewBid.Enabled = False
            End If
            'Edit Work Order. Pricing section settings. 
            '*******************************************************************************

            If (ds.Tables("tblWODetails").Rows(0).Item("WOStatusSent") = "Submitted") Or (ds.Tables("tblWODetails").Rows(0).Item("WOStatusSent") = "Draft") Then
                'Was the Workorder marked as Staged or Not.
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("StagedWO")) Then
                    If (ds.Tables("tblWODetails").Rows(0).Item("StagedWO") = "Yes") Then
                        rdoStagedYes.Checked = True
                        rdoStagedNo.Checked = False
                        tblUpSell.Style.Add("display", "none")
                        trContent.Style.Add("display", "inline")
                        trHolidays.Style.Add("display", "inline")
                        chkbxIncludeWeekends.Checked = ds.Tables("tblWODetails").Rows(0).Item("IncludeWeekends")
                        chkbxIncludeHolidays.Checked = ds.Tables("tblWODetails").Rows(0).Item("IncludeBankHolidays")
                    Else
                        rdoStagedNo.Checked = True
                        rdoStagedYes.Checked = False
                    End If
                Else
                    rdoStagedNo.Checked = True
                    rdoStagedYes.Checked = False
                End If

                'What was the Estimated time required in days
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("EstimatedTimeInDays")) Then
                    txtEstTimeRqrd.Text = ds.Tables("tblWODetails").Rows(0).Item("EstimatedTimeInDays")
                Else
                    txtEstTimeRqrd.Text = 1
                End If
                'What was the pricing method selected
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PricingMethod")) Then
                    ddlPricingMethod.SelectedValue = ds.Tables("tblWODetails").Rows(0).Item("PricingMethod")
                Else
                    ddlPricingMethod.SelectedValue = "Fixed"
                End If

                'Decide to Show/Hide the WholesaleDayJobRate and PlatformDayJobRate fields
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PricingMethod")) Then
                    If ds.Tables("tblWODetails").Rows(0).Item("PricingMethod") = "DailyRate" Or ds.Tables("tblWODetails").Rows(0).Item("PricingMethod") = "Job" Then
                        trWholesaleDayJobRate.Style.Add("display", "inline")
                        trPlatformDayJobRate.Style.Add("display", "inline")
                        txtWholesaleDayJobRate.Text = ds.Tables("tblWODetails").Rows(0).Item("WholesaleDayJobRate")
                        txtPlatformDayJobRate.Text = ds.Tables("tblWODetails").Rows(0).Item("PlatformDayJobRate")
                        txtEstTimeRqrd.Style.Add("display", "inline")
                        lblEstTimeRqrdDay.Style.Add("display", "inline")
                        lblEstTimeRqrdHdr.Style.Add("display", "inline")
                    ElseIf ds.Tables("tblWODetails").Rows(0).Item("PricingMethod") = "Fixed" Then
                        trWholesaleDayJobRate.Style.Add("display", "none")
                        trPlatformDayJobRate.Style.Add("display", "none")
                    End If
                Else
                    trWholesaleDayJobRate.Style.Add("display", "none")
                    trPlatformDayJobRate.Style.Add("display", "none")
                End If
                'Proposed price and Platform are computed in the JavaScript. Hence they should be made enabled false
                'txtSpendLimitWP.Enabled = False
                'txtSpendLimitPP.Enabled = False
            End If
            '*******************************************************************************
            chkShowLoc.Checked = ds.Tables("tblSummary").Rows(0).Item("ShowLocToSupplier")
        End If

        UCAccreditations.Type = "AllAccreditations"
        UCAccreditations.Mode = "WOAccreditations"
        UCAccreditations.BizDivId = ViewState("BizDivId")
        UCAccreditations.CommonID = WOID
        UCAccreditations.PopulateSelectedGrid(True)

    End Sub

    ''' <summary>
    ''' Populate the details for create work request
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateAllDetails()

        Dim ds As DataSet
        ds = CommonFunctions.GetCreateWorkRequestDetails(ViewState("BuyerContactId"), ViewState("WOID"), ViewState("BuyerCompanyId"))
        If ds.Tables.Count > 5 Then
            ViewState("BuyerContactId") = ds.Tables(5).Rows(0)("ContactID")
            ViewState("BuyerContactName") = ds.Tables(5).Rows(0)("ContactName")
            ViewState("CompanyName") = ds.Tables(5).Rows(0)("Contact")
            hdnContactName.Value = ViewState("BuyerContactName")
            If Src <> "EditWO" Then
                ViewState("BusinessArea") = ds.Tables(5).Rows(0)("BusinessArea")
            End If
            ViewState("IsAMEnabled") = ds.Tables(5).Rows(0)("isAM")
        End If
        PopulatePageSettings()
        'Populate JS for Locations Data
        Dim strarr As New StringBuilder
        Dim i As Integer
        Dim dvLocation As DataView = ds.Tables("tblLocDetails").DefaultView
        dvLocation.RowFilter = "Type = 'Business'"
        strarr.Append("locArr = new Array();" & Chr(13))
        For i = 0 To dvLocation.Count - 1
            strarr.Append("locArr[" & i + 1 & "] = new Array();" & Chr(13))
            strarr.Append("locArr[" & i + 1 & "][0] = '" & (dvLocation.Item(i).Item("AddressID")).ToString.Replace("'", "") & "';")
            strarr.Append("locArr[" & i + 1 & "][1] = '" & (dvLocation.Item(i).Item("Name")).ToString.Replace("'", "") & "';")
            strarr.Append("locArr[" & i + 1 & "][2] = '" & (dvLocation.Item(i).Item("Address")).ToString.Replace("'", "").Replace(Chr(13), "").Replace(Chr(10), "") & "';")
            strarr.Append("locArr[" & i + 1 & "][3] = '" & (dvLocation.Item(i).Item("City")).ToString.Replace("'", "") & "';")
            strarr.Append("locArr[" & i + 1 & "][4] = '" & (dvLocation.Item(i).Item("State")).ToString.Replace("'", "") & "';")
            strarr.Append("locArr[" & i + 1 & "][5] = '" & (dvLocation.Item(i).Item("PostCode")).ToString.Replace("'", "") & "';")
            strarr.Append("locArr[" & i + 1 & "][6] = '" & (dvLocation.Item(i).Item("Phone")).ToString.Replace("'", "") & "';")
            strarr.Append("locArr[" & i + 1 & "][7] = '" & (dvLocation.Item(i).Item("Fax")).ToString.Replace("'", "") & "';")
        Next

        CType(Me.Parent.FindControl("litData"), Literal).Text = strarr.ToString

        'Populate Good Location
        If Not IsNothing(ViewState("BusinessArea")) Then
            If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Or ViewState("BusinessArea") = ApplicationSettings.BusinessAreaIT Then
                Dim dvLocation1 As DataView = ds.Tables("tblLocDetails").DefaultView
                dvLocation1.RowFilter = "IsDepot = 1"
                drpdwnGoodsCollection.DataSource = dvLocation1.ToTable
                drpdwnGoodsCollection.DataBind()
                Dim li As New ListItem
                li = New ListItem
                li.Text = "End User Location"
                li.Value = 1
                drpdwnGoodsCollection.Items.Insert(0, li)
                Dim li1 As New ListItem
                li1 = New ListItem
                li1.Text = "None Specified"
                li1.Value = 0
                drpdwnGoodsCollection.Items.Insert(0, li1)

            End If
        End If

        Dim dv_loc As New DataView(ds.Tables("tblLocDetails"))
        ddlLocation.DataSource = dv_loc
        ddlLocation.DataBind()

        drpdwnBillingLocation.Visible = True
        lblBillingLocation.Visible = True
        dv_loc.RowFilter = "IsBilling = 1"
        drpdwnBillingLocation.DataSource = dv_loc.ToTable
        drpdwnBillingLocation.DataBind()

        Dim liBillLoc As New ListItem
        liBillLoc = New ListItem
        liBillLoc.Text = "Select billing location"
        liBillLoc.Value = ""
        drpdwnBillingLocation.Items.Insert(0, liBillLoc)
        If (dv_loc.ToTable.Rows.Count = 1) Then
            drpdwnBillingLocation.SelectedValue = dv_loc.ToTable.Rows(0)("AddressId")
        End If


        Dim liLoc As New ListItem
        liLoc = New ListItem
        If Not IsNothing(ViewState("BusinessArea")) Then
            If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Or ViewState("BusinessArea") = ApplicationSettings.BusinessAreaIT Then
                liLoc.Text = ResourceMessageText.GetString("TempLocationForITandRetail")
            Else
                liLoc.Text = ResourceMessageText.GetString("TempLocation")
            End If
        Else
            liLoc.Text = ResourceMessageText.GetString("TempLocation")
        End If

        liLoc.Value = ""
        ddlLocation.Items.Insert(ddlLocation.Items.Count, liLoc)
        'populate Attachments 
        'Company

        'Populatin Products in the products dropdown
        If WOID = 0 And (Src = "NewWO") Then
            If Not IsNothing(Request.QueryString("sender")) Then
                If Request.QueryString("sender") = "UCWODetails" Then
                    drpdwnProduct.Visible = False
                Else
                    If ds.Tables("tblProductsList").Rows.Count > 0 Then
                        drpdwnProduct.Visible = True
                        Dim liProducts As New ListItem
                        liProducts = New ListItem
                        liProducts.Text = "Select Service"
                        liProducts.Value = 0
                        trSelectProductList.Visible = True
                        drpdwnProduct.DataSource = ds.Tables("tblProductsList")
                        drpdwnProduct.DataTextField = "ProductName"
                        drpdwnProduct.DataValueField = "ProductID"
                        drpdwnProduct.DataBind()
                        drpdwnProduct.Items.Insert(0, liProducts)
                    Else
                        drpdwnProduct.Visible = False
                    End If
                End If
            Else
                If ds.Tables("tblProductsList").Rows.Count > 0 Then
                    drpdwnProduct.Visible = True
                    Dim liProducts As New ListItem
                    liProducts = New ListItem
                    liProducts.Text = "Select Service"
                    liProducts.Value = 0
                    trSelectProductList.Visible = True
                    drpdwnProduct.DataSource = ds.Tables("tblProductsList")
                    drpdwnProduct.DataTextField = "ProductName"
                    drpdwnProduct.DataValueField = "ProductID"
                    drpdwnProduct.DataBind()
                    drpdwnProduct.Items.Insert(0, liProducts)
                Else
                    drpdwnProduct.Visible = False
                End If
            End If
        Else
            drpdwnProduct.Visible = False
        End If
        SetCacheForExistingAttachment(CompanyID, "CompanyProfile", ds.Tables("tblAttachmentsCompany").Copy.DefaultView)

        SetCacheForAttachment(WOID, "WorkOrder", ds.Tables("tblAttachmentsWO").Copy.DefaultView)

        If ds.Tables("tblContactsSettings").Rows.Count > 0 Then
            If CBool(ds.Tables("tblcontactsSettings").Rows(0)("IsEveningInstallationEnabled")) Then
                hdnWPUplift.Value = ds.Tables("tblcontactsSettings").Rows(0)("WPUplift")
                hdnPPUplift.Value = ds.Tables("tblcontactsSettings").Rows(0)("PPUplift")
                hdnEvengInst.Value = "True"
                hdnEvngInstTime.Value = ds.Tables("tblcontactsSettings").Rows(0)("EvengInstTime")

                If ds.Tables("tblcontactsSettings").Rows(0)("WPUpliftPercent") = "0" Then
                    hdnWPUPliftPercent.Value = "False"
                Else
                    hdnWPUPliftPercent.Value = ds.Tables("tblcontactsSettings").Rows(0)("WPUpliftPercent")
                End If

                If ds.Tables("tblcontactsSettings").Rows(0)("PPUpliftPercent") = "0" Then
                    hdnPPUpliftPercent.Value = "False"
                Else
                    hdnPPUpliftPercent.Value = ds.Tables("tblcontactsSettings").Rows(0)("PPUpliftPercent")
                End If
            Else
                hdnEvengInst.Value = "False"
            End If
            hdnIsSaturdayDisabled.Value = ds.Tables("tblContactsSettings").Rows(0)("IsSaturdayDisabled")
            hdnIsSundayDisabled.Value = ds.Tables("tblContactsSettings").Rows(0)("IsSundayDisabled")
        Else
            hdnEvengInst.Value = "False"
        End If

        calBeginDate.IsWeekendsDisabled = "function x(s, e) { s.set_Weekend(" & IIf(ds.Tables("tblContactsSettings").Rows(0)("IsSaturdayDisabled") = "True", 1, IIf(ds.Tables("tblContactsSettings").Rows(0)("IsSaturdayDisabled") = "False", 0, ds.Tables("tblContactsSettings").Rows(0)("IsSaturdayDisabled"))) _
           & IIf(ds.Tables("tblContactsSettings").Rows(0)("IsSundayDisabled") = "True", 1, IIf(ds.Tables("tblContactsSettings").Rows(0)("IsSundayDisabled") = "False", 0, ds.Tables("tblContactsSettings").Rows(0)("IsSundayDisabled"))) & "); }"
        If ViewState("BusinessArea") <> OrderWorkLibrary.ApplicationSettings.BusinessAreaRetail Then
            calEndDate.IsWeekendsDisabled = "function x(s, e) { s.set_Weekend(" & IIf(ds.Tables("tblContactsSettings").Rows(0)("IsSaturdayDisabled") = "True", 1, IIf(ds.Tables("tblContactsSettings").Rows(0)("IsSaturdayDisabled") = "False", 0, ds.Tables("tblContactsSettings").Rows(0)("IsSaturdayDisabled"))) _
        & IIf(ds.Tables("tblContactsSettings").Rows(0)("IsSundayDisabled") = "True", 1, IIf(ds.Tables("tblContactsSettings").Rows(0)("IsSundayDisabled") = "False", 0, ds.Tables("tblContactsSettings").Rows(0)("IsSundayDisabled"))) & "); }"
        End If
        If ds.Tables("tblContactsSettings").Rows(0)("IsHolidaysDisabled") Then
            calBeginDate.IsDateSelectableFunction = "function x(s, e) { s.set_completedate(holidaydates()); }"
            If ViewState("BusinessArea") <> OrderWorkLibrary.ApplicationSettings.BusinessAreaRetail Then
                calEndDate.IsDateSelectableFunction = "function x(s, e) { s.set_completedate(holidaydates()); }"
            End If
        End If

        Dim itemcount As Integer = 0

        'Populating the appointment time
        If ds.Tables(8).Rows.Count > 0 Then
            If ds.Tables(8).Rows(0).Item("TimeSlot1Enabled") <> 0 Or ds.Tables(8).Rows(0).Item("TimeSlot2Enabled") <> 0 Or ds.Tables(8).Rows(0).Item("TimeSlot3Enabled") <> 0 Then
                drpTime.Items.Clear()


                drpTime.Items.Insert(itemcount, New ListItem("Select Time", ""))
                itemcount = itemcount + 1

                If ds.Tables(8).Rows(0).Item("TimeSlot1Enabled") <> 0 Then
                    drpTime.Items.Insert(itemcount, New ListItem(ds.Tables(8).Rows(0).Item("TimeSlot1"), ds.Tables(8).Rows(0).Item("TimeSlot1")))
                    itemcount = itemcount + 1
                End If
                If ds.Tables(8).Rows(0).Item("TimeSlot2Enabled") <> 0 Then
                    drpTime.Items.Insert(itemcount, New ListItem(ds.Tables(8).Rows(0).Item("TimeSlot2"), ds.Tables(8).Rows(0).Item("TimeSlot2")))
                    itemcount = itemcount + 1
                End If
                If ds.Tables(8).Rows(0).Item("TimeSlot3Enabled") <> 0 Then
                    drpTime.Items.Insert(itemcount, New ListItem(ds.Tables(8).Rows(0).Item("TimeSlot3"), ds.Tables(8).Rows(0).Item("TimeSlot3")))
                    itemcount = itemcount + 1
                End If

                If ds.Tables(8).Rows(0).Item("TimeSlot4Enabled") <> 0 Then
                    drpTime.Items.Insert(itemcount, New ListItem(ds.Tables(8).Rows(0).Item("TimeSlot4"), ds.Tables(8).Rows(0).Item("TimeSlot4")))
                    itemcount = itemcount + 1
                End If

            End If
        End If

        itemcount = drpTime.Items.Count
        If hdnEvengInst.Value = "True" Then
            Dim liInstallationTime As New ListItem
            liInstallationTime.Text = hdnEvngInstTime.Value
            liInstallationTime.Value = hdnEvngInstTime.Value
            drpTime.Items.Insert(itemcount, liInstallationTime)
        End If
    End Sub

    ''' <summary>
    ''' Populating Standards
    ''' Category, Sub Category and Dress Code
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateStandards()
        Dim dsXML As New DataSet()
        dsXML.ReadXml(MapPath("~/XML/WOFormStandards.xml"))
        'get the dataview of table "Country", which is default table name
        Dim dv_WOType As DataView = dsXML.Tables("MainCat").DefaultView
        'Dim dv_FreesatMake As DataView = dsXML.Tables("FreesatMake").DefaultView

        ''Populating Freesat Make model
        'drpdwnFreesatMake.DataSource = dv_FreesatMake
        'drpdwnFreesatMake.DataBind()

        'now define datatext field and datavalue field of dropdownlist
        ddlWOType.DataTextField = "Name"
        ddlWOType.DataValueField = "CombId"
        ddlWOType.DataSource = dv_WOType
        ddlWOType.DataBind()

        Dim li2 As New ListItem
        li2 = New ListItem
        li2.Text = ResourceMessageText.GetString("WOCategoryList")
        li2.Value = ""
        ddlWOType.Items.Insert(0, li2)

        'Populate DressCode Dropdown List
        Dim dv_DressCode As DataView = dsXML.Tables("DressCode").DefaultView
        dv_DressCode.Sort = "Sequence"
        ddlDressCode.DataSource = dv_DressCode
        ddlDressCode.DataTextField = "StandardValue"
        ddlDressCode.DataValueField = "StandardID"
        ddlDressCode.DataBind()

        txtScheduleWInBegin.Text = Strings.FormatDateTime(CType(Date.Now.AddDays(1), Date), DateFormat.ShortDate)
        hdnStartDate.Value = txtScheduleWInBegin.Text

    End Sub

    ''' <summary>
    ''' For Admin: on selection of a contact show the location drop down list and poppulate the details.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        txtQuantity.Text = "1"
        txtScheduleWInBegin.Text = ""
        txtScheduleWInEnd.Text = ""
        txtClientInstru.Text = ""
        txtClientScope.Text = ""
        txtPONumber.Text = ""
        txtRelatedWorkorder.Text = ""
        txtReceiptNumber.Text = ""
        txtWOLongDesc.Text = ""
        txtTitle.Text = ""
        txtJRSNumber.Text = ""
        ddlDressCode.SelectedIndex = 0
        ddlWOType.SelectedIndex = 0
        txtProductsPur.Text = ""
        txtSalesAgent.Text = ""
        If hdnContactID.Value = "" Then
            CompanyID = 0
            divWOLoc.Style.Add("display", "none")
        Else
            CompanyID = hdnContactID.Value

            If CompanyID = ApplicationSettings.Khharrods Or CompanyID = ApplicationSettings.DixonRetailForm Then
                lblspanPhone.Visible = False
                RequiredFieldValidator8.Enabled = False
                lblspanMobile.Visible = True
                ReqMobile.Enabled = True
            Else
                lblspanPhone.Visible = True
                RequiredFieldValidator8.Enabled = True
                lblspanMobile.Visible = False
                ReqMobile.Enabled = False
            End If

            If CompanyID = ApplicationSettings.OutsourceCompany Or CompanyID = ApplicationSettings.ExponentialECompany Or CompanyID = ApplicationSettings.ITECHSOCompany Or CompanyID = ApplicationSettings.DemoCompany Then
                drpTime.Items.Clear()
                'drpTime.Items.Insert(0, New ListItem("Please Select", ""))
                drpTime.Items.Insert(0, New ListItem("Arrival 0 - 1", "Arrival 0 - 1"))
                drpTime.Items.Insert(1, New ListItem("Arrival 1 - 2", "Arrival 1 - 2"))
                drpTime.Items.Insert(2, New ListItem("Arrival 2 - 3", "Arrival 2 - 3"))
                drpTime.Items.Insert(3, New ListItem("Arrival 3 - 4", "Arrival 3 - 4"))
                drpTime.Items.Insert(4, New ListItem("Arrival 4 - 5", "Arrival 4 - 5"))
                drpTime.Items.Insert(5, New ListItem("Arrival 5 - 6", "Arrival 5 - 6"))
                drpTime.Items.Insert(6, New ListItem("Arrival 6 - 7", "Arrival 6 - 7"))
                drpTime.Items.Insert(7, New ListItem("Arrival 7 - 8", "Arrival 7 - 8"))
                drpTime.Items.Insert(8, New ListItem("Arrival 8 - 9", "Arrival 8 - 9"))
                drpTime.Items.Insert(9, New ListItem("Arrival 9 - 10", "Arrival 9 - 10"))
                drpTime.Items.Insert(10, New ListItem("Arrival 10 - 11", "Arrival 10 - 11"))
                drpTime.Items.Insert(11, New ListItem("Arrival 11 - 12", "Arrival 11 - 12"))
                drpTime.Items.Insert(12, New ListItem("Arrival 12 - 13", "Arrival 12 - 13"))
                drpTime.Items.Insert(13, New ListItem("Arrival 13 - 14", "Arrival 13 - 14"))
                drpTime.Items.Insert(14, New ListItem("Arrival 14 - 15", "Arrival 14 - 15"))
                drpTime.Items.Insert(15, New ListItem("Arrival 15 - 16", "Arrival 15 - 16"))
                drpTime.Items.Insert(16, New ListItem("Arrival 16 - 17", "Arrival 16 - 17"))
                drpTime.Items.Insert(17, New ListItem("Arrival 17 - 18", "Arrival 17 - 18"))
                drpTime.Items.Insert(18, New ListItem("Arrival 18 - 19", "Arrival 18 - 19"))
                drpTime.Items.Insert(19, New ListItem("Arrival 19 - 20", "Arrival 19 - 20"))
                drpTime.Items.Insert(20, New ListItem("Arrival 20 - 21", "Arrival 20 - 21"))
                drpTime.Items.Insert(21, New ListItem("Arrival 21 - 22", "Arrival 21 - 22"))
                drpTime.Items.Insert(22, New ListItem("Arrival 22 - 23", "Arrival 22 - 23"))
                drpTime.Items.Insert(23, New ListItem("Arrival 23 - 24", "Arrival 23 - 24"))
                'OM-153 Added by Sabita New Slot for Amazon
            ElseIf CompanyID = ApplicationSettings.AmazonCompany Then
                drpTime.Items.Clear()
                drpTime.Items.Insert(0, New ListItem("Arrival 8 - 11", "Arrival 8 - 11"))
                'drpTime.Items.Insert(1, New ListItem("Arrival 9 - 12", "Arrival 9 - 12"))
                'drpTime.Items.Insert(2, New ListItem("Arrival 10 - 13", "Arrival 10 - 13"))
                'drpTime.Items.Insert(3, New ListItem("Arrival 11 - 14", "Arrival 11 - 14"))
                drpTime.Items.Insert(1, New ListItem("Arrival 12 - 15", "Arrival 12 - 15"))
                'drpTime.Items.Insert(5, New ListItem("Arrival 13 - 16", "Arrival 13 - 16"))
                'drpTime.Items.Insert(6, New ListItem("Arrival 14 - 17", "Arrival 14 - 17"))
                'drpTime.Items.Insert(7, New ListItem("Arrival 15 - 18", "Arrival 15 - 18"))
                drpTime.Items.Insert(2, New ListItem("Arrival 16 - 19", "Arrival 16 - 19"))
            Else
                drpTime.Items.Clear()
                drpTime.Items.Insert(0, New ListItem("Please Select", "0"))
                drpTime.Items.Insert(1, New ListItem("8am - 1pm", "8am - 1pm"))
                drpTime.Items.Insert(2, New ListItem("1pm - 6pm", "1pm - 6pm"))
                drpTime.Items.Insert(3, New ListItem("All Day", "All Day"))

            End If

            ViewState("BuyerCompanyId") = hdnContactID.Value
            divWOLoc.Style.Add("display", "block")
            divValidationMain.Style.Add("display", "none")
            Dim isAM As Boolean

            PopulateAllDetails()
            ddlLocation.SelectedValue = ""
            'Added code by Pankaj Malav on 17 Oct 2008
            lbltxtCompLocName.Text = "Company Name"
            'Get AutoMatch Status for the user.
            isAM = ViewState("IsAMEnabled")
            'Show Option to ignore AutoMatch if it is enabled.

            If isAM Then
                chkIngoreAutomatch.Visible = True
            Else
                chkIngoreAutomatch.Visible = False
            End If

            'Added Code on 4 Nov 2008 by Pankaj Malav to populate the details of the template after the selection of contact as 
            'before this the attachments are getting resetted and therefore not working properly
            If (WOID <> 0) And ((Src = "CreateSimilarWOFromTemplate")) Then
                'IF WOID <> 0 and Src is CreateSimilarWOFromTemplate then populate the Attachments
                'PopulateTemplateAttachments(WOID)
                CWOFileAttach.ExistAttachmentSourceID = CompanyID
                'CWOFileAttach.ClearUCFileUploadCache()
                CWOFileAttach.PopulateExistingFiles()
            Else
                CWOFileAttach.ExistAttachmentSourceID = CompanyID
                CWOFileAttach.ClearUCFileUploadCache()
                CWOFileAttach.PopulateExistingFiles()
            End If
        End If

    End Sub

    Private Sub populateWODetailsFromTemplate(ByVal WOID)

        Dim ds As DataSet = ws.WSWorkOrder.GetTemplateForTemplateID(WOID, BizDivID)

        'lblHeading.Text = ResourceMessageText.GetString("EditWOHeadingMS") + ds.Tables("tblSummary").Rows(0).Item("WorkOrderId")
        'ViewState("WorkOrderId") = ds.Tables("tblSummary").Rows(0).Item("WorkOrderId")

        Dim dt As DataTable
        dt = ds.Tables("tblWODetails")

        If dt.Rows.Count > 0 Then
            txtTitle.Text = IIf(Not IsDBNull(dt.Rows(0).Item("WOTitle")), dt.Rows(0).Item("WOTitle"), "")
            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                txtInvoiceTitle.Text = IIf(Not IsDBNull(dt.Rows(0).Item("InvoiceTitle")), dt.Rows(0).Item("InvoiceTitle"), "")
            End If

            'populateCategory(dt.Rows(0).Item("WOCategoryID"))
            ViewState("MainCatId") = dt.Rows(0).Item("MainCatId")
            ViewState("CombId") = dt.Rows(0).Item("WOCategoryId")
            hdnSubCategoryVal.Value = dt.Rows(0).Item("WOCategoryId")
            RepopulateSubCat()

            If Not IsDBNull(dt.Rows(0).Item("DressCode")) Then
                ddlDressCode.SelectedIndex = ddlDressCode.Items.IndexOf(ddlDressCode.Items.FindByText(CStr(dt.Rows(0).Item("DressCode"))))
            Else
                ddlDressCode.SelectedIndex = 0
            End If

            txtPONumber.Text = IIf(Not IsDBNull(dt.Rows(0).Item("PONumber")), dt.Rows(0).Item("PONumber"), "")
            txtReceiptNumber.Text = IIf(Not IsDBNull(dt.Rows(0).Item("ReceiptNumber")), dt.Rows(0).Item("ReceiptNumber"), "")
            txtJRSNumber.Text = IIf(Not IsDBNull(dt.Rows(0).Item("JRSNumber")), dt.Rows(0).Item("JRSNumber"), "")

            If Not IsDBNull(dt.Rows(0).Item("WOLongDesc")) Then
                txtWOLongDesc.Text = dt.Rows(0).Item("WOLongDesc").Replace("<BR>", Chr(13))
            Else
                txtWOLongDesc.Text = ""
            End If

            If IsDBNull(dt.Rows(0).Item("SpecialInstructions")) Then
                txtSpecialInstructions.Text = ""
            Else
                txtSpecialInstructions.Text = Server.HtmlDecode(Server.HtmlDecode(dt.Rows(0).Item("SpecialInstructions").Replace("<BR>", Chr(13)))) 'dt.Rows(0).Item("SpecialInstructions").Replace("<BR>", Chr(13))
            End If
            'drpdwnGoodsCollection.SelectedValue = IIf(Not IsDBNull(dt.Rows(0).Item("GoodsLocation")), dt.Rows(0).Item("GoodsLocation"), 1)
            'txtProdName.Text = IIf(Not IsDBNull(dt.Rows(0).Item("ProductName")), dt.Rows(0).Item("ProductName"), "")

            '****Check the functionality***
            'ViewState("WOStatus_Edit") = dt.Rows(0).Item("WOStatus")

            If Viewer = "Admin" Then
                'Wholesale Price
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice")) Then
                    txtSpendLimitWP.Text = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.False, TriState.False)
                    WOCustomerPrice = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("CustomerPrice"), 2, TriState.True, TriState.False, TriState.False)
                    hdnDBWP.Value = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice"), 3, TriState.True, TriState.False, TriState.False)
                    hdnWPcomp.Value = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.False, TriState.False)
                    If ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice") = 0 Then
                        rdo0ValueWP.Checked = True
                        'txtSpendLimitWP.Enabled = False
                        rdoNon0ValueWP.Checked = False
                    End If
                Else
                    txtSpendLimitWP.Text = FormatNumber(0, 2, TriState.True, TriState.False, TriState.False)
                End If
                'Proposed Price
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PlatformPrice")) Then
                    txtSpendLimitPP.Text = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("PlatformPrice"), 2, TriState.True, TriState.False, TriState.False)
                    If ds.Tables("tblWODetails").Rows(0).Item("PlatformPrice") = 0 Then
                        'rdo0ValuePP.Checked = True
                        ''txtSpendLimitPP.Enabled = False
                        'rdoNon0ValuePP.Checked = False
                        chkReviewBid.Visible = False
                    End If
                    If dt.Rows(0).Item("ReviewBids") = True Then
                        rdo0ValuePP.Checked = True
                        rdoNon0ValuePP.Checked = False
                    End If
                Else
                    txtSpendLimitPP.Text = FormatNumber(0, 2, TriState.True, TriState.False, TriState.False)
                End If
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PPPerRate")) Then
                    txtPPPerRate.Text = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("PPPerRate"), 2, TriState.True, TriState.False, TriState.False)
                Else
                    txtPPPerRate.Text = FormatNumber(0, 2, TriState.True, TriState.False, TriState.False)
                End If
                If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PerRate")) Then
                    ddPPPerRate.SelectedValue = ds.Tables("tblWODetails").Rows(0).Item("PerRate")
                End If
            End If

            'txtEstimatedTime.Text = dt.Rows(0).Item("EstimatedTime")
            If txtScheduleWInBegin.Text <> "" Then
                txtScheduleWInBegin.Text = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
            Else
                txtScheduleWInBegin.Text = ""
            End If
            If Not IsDBNull(dt.Rows(0).Item("ReviewBids")) Then
                If dt.Rows(0).Item("ReviewBids") = True Then
                    chkReviewBid.Checked = True
                Else
                    chkReviewBid.Checked = False
                End If
            Else
                chkReviewBid.Checked = False
            End If
            'for admin
            ViewState("BizDivID") = dt.Rows(0).Item("BizDivID")
            ViewState("WOID") = WOID
        End If
        If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("ShowLocToSupplier")) Then
            chkShowLoc.Checked = ds.Tables("tblWODetails").Rows(0).Item("ShowLocToSupplier")
        Else
            chkShowLoc.Checked = False
        End If

        PopulateTemplateAttachments(WOID)
    End Sub

    ''' <summary>
    ''' Created a sub as inititalization of attachments requires at the page load and on the selection of Company
    ''' Created By pankaj Malav on 4 Nov 2008
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <remarks></remarks>
    Public Sub PopulateTemplateAttachments(ByVal WOID As Integer)
        'Attachment cache kill
        CWOFileAttach.CompanyID = CompanyID
        CWOFileAttach.ClearUCFileUploadCache()
        'Populate Attachements 
        CWOFileAttach.BizDivID = BizDivID
        CWOFileAttach.Type = "WorkOrder"
        CWOFileAttach.AttachmentForID = WOID
        CWOFileAttach.AttachmentForSource = "TemplateWorkOrder"

        CWOFileAttach.CompanyID = CompanyID
        CWOFileAttach.ExistAttachmentSourceID = CompanyID
        CWOFileAttach.ClearUCFileUploadCache()
        CWOFileAttach.PopulateAttachedFiles()
        'CWOFileAttach.PopulateExistingFiles()
        CWOFileAttach.AttachmentForSource = "WorkOrder"
        CWOFileAttach.AttachmentForID = 0

        'Attachment cache kill
        UCFileUpload1.CompanyID = CompanyID
        UCFileUpload1.ClearUCFileUploadCache()

        UCFileUpload1.BizDivID = BizDivID
        UCFileUpload1.Type = "StatementOfWork"
        UCFileUpload1.AttachmentForID = WOID
        UCFileUpload1.AttachmentForSource = "TemplateStatementOfWork"

        UCFileUpload1.ClearUCFileUploadCache()
        UCFileUpload1.PopulateAttachedFiles()
        UCFileUpload1.PopulateExistingFiles()
        UCFileUpload1.ClearUCFileUploadCache()

    End Sub

    ''' <summary>
    ''' Populate location details
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <remarks></remarks>
    Public Sub PopulateLocation(ByVal dt As DataTable, ByVal LocationID As Integer)
        'Populating Location
        If dt.Rows(0).Item("Type") = "Business" Then
            'Permanent Location
            ddlLocation.SelectedValue = LocationID
            tblContactInfo.Style.Add("visibility", "hidden")
            'Added code by Pankaj Malav
            lbltxtCompLocName.Text = "Location Name"
            RqdFName.Enabled = False
            RqdLName.Enabled = False
            txtName.Text = dt.Rows(0).Item("Name")
            txtName.Enabled = False
            txtCompanyAddress.Enabled = False
            txtCity.Enabled = False
            txtCounty.Enabled = False
            txtCompanyPostalCode.Enabled = False
            txtCompanyFax.Enabled = False
            txtMobile.Enabled = False
            txtCompanyPhone.Enabled = False
        Else
            'Added By pankaj Malav on 20 Oct 2008
            lbltxtCompLocName.Text = "Company Name"
            RqdFName.Enabled = False
            RqdLName.Enabled = False
            tblContactInfo.Style.Add("visibility", "hidden")
            Dim location As String = dt.Rows(0).Item("Name")
            Dim firstItem = ddlLocation.Items(0).Text.Trim
            ddlLocation.SelectedIndex = ddlLocation.Items.IndexOf(ddlLocation.Items.FindByText(location.Trim))
            If ddlLocation.SelectedIndex = 0 And location <> firstItem Then  'customer loc
                ddlLocation.SelectedValue = ""
                RqdFName.Enabled = True
                RqdLName.Enabled = True
                tblContactInfo.Style.Add("visibility", "visible")
            End If

            txtFName.Text = dt.Rows(0).Item("ContactFname")
            txtLName.Text = dt.Rows(0).Item("ContactLname")
            If Not IsDBNull(dt.Rows(0).Item("Name")) Then
                txtName.Text = dt.Rows(0).Item("Name")
            Else
                txtName.Text = ""
            End If
            txtName.Enabled = True
            txtCompanyAddress.Enabled = True
            txtCity.Enabled = True
            txtCounty.Enabled = True
            txtCompanyPostalCode.Enabled = True
            txtCompanyFax.Enabled = True
            txtMobile.Enabled = True
            txtCompanyPhone.Enabled = True
            'Poonam added on 11/1/2017 - Task -  OA-385 :OA - Add Customer email field to New Workorder booking page
            If dt.Rows(0).Item("CustomerEmail") <> "" Then
                txtCustomerEmail.Text = dt.Rows(0).Item("CustomerEmail")
            Else
                txtCustomerEmail.Text = ""
            End If
        End If
        hdnLocationId.Value = LocationID
        txtCompanyAddress.Text = dt.Rows(0).Item("Address")
        txtCity.Text = dt.Rows(0).Item("City")
        txtCounty.Text = dt.Rows(0).Item("State")
        txtCompanyPostalCode.Text = dt.Rows(0).Item("PostCode")
        txtCompanyFax.Text = dt.Rows(0).Item("Fax")
        txtMobile.Text = dt.Rows(0).Item("Mobile")
        txtCompanyPhone.Text = dt.Rows(0).Item("Phone")
        tdCompName.Style.Add("display", "block")
        tdtxtCompName.Style.Add("display", "block")
    End Sub

    ''' <summary>
    ''' Page Settings
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulatePageSettings()
        If Not IsNothing(ViewState("BusinessArea")) Then
            If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                tdGoodsLocation.Visible = True
                tdGoods.Visible = True
                lblLocGoods.Text = "Location of Goods"
                lblProductsPur.Text = "Product Purchased"
                reqtxtProductsPur.ErrorMessage = "Please enter Product Purchased"
                'tdtvMakeModel.Visible = True
                'tdtvMake.Visible = True
                hdnBusinessArea.Value = "Retail"
                RqFldAptTime.Enabled = True
                RqFldAptDate.Enabled = True
                tdSales.Visible = True
                tdSalesAgent.Visible = True
                txtScheduleWInEnd.Visible = False
                btnEndDate.Visible = False
                drpTime.Visible = True
                lblScheduleWinEnd.Visible = False
                rqWODateRange.Enabled = False
                lblScheduleBegin.Text = "Appointment Date"
                tdlblJobNumber.Style.Add("display", "block")
                tdtxtJobNumber.Style.Add("display", "block")
                tdCompName.Style.Add("display", "none")
                tdtxtCompName.Style.Add("display", "none")
                'added by GM
                lblHeading.Text = "Book New Appointment"
                lblDetails.Text = "Appointment Details"
                lblReqTitle.Text = "Title"
                lblReqType.Text = "Type"
                lblReqLocation.Text = "Customer Location"
                tdSelectLoc.Visible = False
                lblTempLoc.Text = ""
                tdTempLoc.Height = "10"
                If txtScheduleWInBegin.Text = "" Then
                    txtScheduleWInBegin.Text = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
                    hdnStartDate.Value = txtScheduleWInBegin.Text
                End If
                'Fizan()
                chkForceSignOff.Checked = True
                chkForceSignOff.Enabled = False

            ElseIf ViewState("BusinessArea") = ApplicationSettings.BusinessAreaIT Then
                tdGoodsLocation.Visible = True
                tdGoods.Visible = True
                lblLocGoods.Text = "Location of Equipment"
                hdnBusinessArea.Value = "IT"
                lblProductsPur.Text = "Equipment Details"
                reqtxtProductsPur.ErrorMessage = "Please enter Equipment Details"
                txtScheduleWInEnd.Visible = True
                btnEndDate.Visible = True
                drpTime.Visible = False

                lblScheduleWinEnd.Visible = True
                tdSales.Visible = False
                tdSalesAgent.Visible = False
                lblScheduleBegin.Text = "Schedule Window Begin"
                tdlblJobNumber.Style.Add("display", "none")
                tdtxtJobNumber.Style.Add("display", "none")
                tdCompName.Style.Add("display", "block")
                tdtxtCompName.Style.Add("display", "block")
                'Added by GM
                lblHeading.Text = "Book Work Order"
                lblDetails.Text = "Job Details"
                lblReqTitle.Text = "Title"
                lblReqType.Text = "Type"
                lblReqLocation.Text = "Customer Location"
                tdSelectLoc.Visible = False
                lblTempLoc.Text = ""
                tdTempLoc.Height = "10"
                If txtScheduleWInBegin.Text = "" Then
                    txtScheduleWInBegin.Text = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
                    hdnStartDate.Value = txtScheduleWInBegin.Text
                End If
                'chkForceSignOff.Enabled = True
                'chkForceSignOff.Checked = False
            Else
                tdGoodsLocation.Visible = False
                tdGoods.Visible = False
                lblProductsPur.Text = "Product Purchased"
                reqtxtProductsPur.ErrorMessage = "Please enter Product Purchased"
                'tdtvMakeModel.Visible = False
                'tdtvMake.Visible = False
                hdnBusinessArea.Value = "None"
                btnEndDate.Visible = True
                drpTime.Visible = False

                lblScheduleWinEnd.Visible = True
                tdSales.Visible = False
                tdSalesAgent.Visible = False
                txtScheduleWInEnd.Visible = True
                btnEndDate.Visible = True
                drpTime.Visible = False
                lblScheduleBegin.Text = "Schedule Window Begin"
                tdlblJobNumber.Style.Add("display", "block")
                tdtxtJobNumber.Style.Add("display", "block")
                tdCompName.Style.Add("display", "block")
                tdtxtCompName.Style.Add("display", "block")
                'Added by GM
                tdTempLoc.Height = "30"
                If txtScheduleWInBegin.Text = "" Then
                    txtScheduleWInBegin.Text = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
                    hdnStartDate.Value = txtScheduleWInBegin.Text
                End If
                chkForceSignOff.Enabled = True
                chkForceSignOff.Checked = False
            End If
        Else
            hdnBusinessArea.Value = "None"
        End If
    End Sub

#Region "Action"

    ''' <summary>
    ''' Function to check if wo value exceeds the Spend limit
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function checkSpendLimit() As Boolean
        Dim Str As String = ""
        ' If Session Variable existis
        If (Not IsNothing(Session("SpendLimit"))) Then
            If (Session("SpendLimit") < CDec(txtSpendLimitWP.Text)) Then
                Str = ResourceMessageText.GetString("SpendLimit")
                Str = Str.Replace("<value>", FormatCurrency(CDec(Session("SpendLimit")), 2, TriState.True, TriState.True, TriState.False))
                lblError.Text = Str
                divValidationMain.Visible = True
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Function to reset the Workorder form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnResetTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetTop.Click, btnResetBtm.Click
        pnlWoForm.Style.Add("display", "block")
        pnlWoSummary.Style.Add("display", "none")
        If WOID <> 0 Then
            'Reset to initial terms
            If (Src = "EditWO") Then
                PopulateAllDetails()
                populateWODetails(WOID)
            End If
        Else
            'Reset to blank form
            txtTitle.Text = ""

            txtSpendLimitWP.Text = 0
            txtSpendLimitPP.Text = 0
            txtPPPerRate.Text = 0
            ddPPPerRate.SelectedValue = "Per Job"

            rdo0ValueWP.Checked = False
            rdoNon0ValueWP.Checked = True
            'txtScheduleWInBegin.Text = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
            txtScheduleWInEnd.Text = ""
            txtQuantity.Text = ""
            txtWOLongDesc.Text = ""
            txtSpecialInstructions.Text = ""
            txtPONumber.Text = ""
            txtRelatedWorkorder.Text = ""

            txtReceiptNumber.Text = ""
            txtJRSNumber.Text = ""
            'txtProdName.Text = ""

            txtClientScope.Text = ""

            ddlLocation.SelectedValue = ""
            ddlWOType.SelectedValue = ""
            ddlWOSubType.Items.Clear()

            tblContactInfo.Style.Add("visibility", "visible")
            RqdFName.Enabled = True
            RqdLName.Enabled = True

            If Not IsNothing(drpdwnProduct) Then
                drpdwnProduct.SelectedValue = 0
                hdnSelectedService.Value = 0
            End If
            drpdwnGoodsCollection.SelectedValue = 0
            ddlDressCode.SelectedValue = 29

            txtName.Text = ""
            txtCompanyAddress.Text = ""
            txtCity.Text = ""
            txtCounty.Text = ""
            txtCompanyPostalCode.Text = ""
            txtCompanyFax.Text = ""
            txtMobile.Text = ""
            txtCompanyPhone.Text = ""
            txtName.Enabled = True
            txtCompanyAddress.Enabled = True
            txtCity.Enabled = True
            txtCounty.Enabled = True
            txtCompanyPostalCode.Enabled = True
            txtCompanyFax.Enabled = True
            txtMobile.Enabled = True
            txtCompanyPhone.Enabled = True
            'Logic for Saving fields depending upon business areas
            If Not IsNothing(ViewState("BusinessArea")) Then
                If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                    txtProductsPur.Text = ""
                    txtClientInstru.Text = ""
                End If
                txtSalesAgent.Text = ""
                drpTime.SelectedIndex = 0
            ElseIf ViewState("BusinessArea") = ApplicationSettings.BusinessAreaIT Then
                txtProductsPur.Text = ""
                txtClientInstru.Text = ""
            End If

            UCAccreditations.Type = "ServiceAccreditations"
            UCAccreditations.Mode = "ServiceAccreditations"
            UCAccreditations.BizDivId = BizDivID
            UCAccreditations.CommonID = hdnSelectedService.Value
            UCAccreditations.PopulateSelectedGrid(True)

        End If
    End Sub

    ''' <summary>
    ''' Repopulating Sub category on each post back
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub RepopulateSubCat()
        If hdnSubCategoryVal.Value <> "" And ViewState("CombId") <> 0 Then
            Dim dsXML As New DataSet()
            Dim MainCatId As Integer
            dsXML.ReadXml(MapPath("~/XML/WOFormStandards.xml"))
            Dim dv_WOType As DataView = dsXML.Tables("MainCat").DefaultView
            Dim dv_WOSubType As DataView = dsXML.Tables("SubCat").DefaultView
            If Not IsNothing(ViewState("CombId")) Then
                dv_WOSubType.RowFilter = "CombId = " & ViewState("CombId")
                If dv_WOSubType.Count <> 0 Then
                    MainCatId = dv_WOSubType.Item(0).Item("MainCatID")
                End If
                dv_WOType.RowFilter = "CombId = " & ViewState("CombId")
                If dv_WOType.Count <> 0 Then
                    MainCatId = dv_WOType.Item(0).Item("MainCatID")
                End If
            Else
                dv_WOType.RowFilter = "CombId = " & ddlWOType.SelectedValue
                MainCatId = dv_WOType.ToTable.Rows(0)("MainCatId")
            End If
            dv_WOType.RowFilter = ""
            ddlWOType.DataTextField = "Name"
            ddlWOType.DataValueField = "CombId"
            ddlWOType.DataSource = dv_WOType
            ddlWOType.DataBind()
            dv_WOType.RowFilter = "MainCatID = " & MainCatId
            If dv_WOType.Count > 0 Then
                ddlWOType.SelectedValue = dv_WOType.Item(0).Item("CombId")
            End If



            dv_WOSubType.RowFilter = "MainCatID = " & MainCatId
            Dim dt As DataTable = dv_WOSubType.ToTable
            ddlWOSubType.DataSource = dt
            ddlWOSubType.DataTextField = "Name"
            ddlWOSubType.DataValueField = "CombId"
            ddlWOSubType.DataBind()
            ddlWOSubType.Enabled = True
            If Not ddlWOSubType.Items.FindByValue(hdnSubCategoryVal.Value) Is Nothing Then
                ddlWOSubType.SelectedValue = hdnSubCategoryVal.Value
            End If
        ElseIf ViewState("CombId") <> 0 Then

            PopulateStandards()
            ddlWOSubType.Enabled = False

        End If
    End Sub

    ''' <summary>
    ''' Function to save data as draft
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveTop.ServerClick, btnSaveBtm.ServerClick
        'New workorder saved as draft
        Dim success As Int16 = 0
        If (Src = "NewWO") Or (Src = "CreateSimilarWOFromTemplate") Then
            ViewState("WOStatus") = ApplicationSettings.WOStatusID.Draft
            ViewState("Comments") = "Draft Created"
            saveWorkOrder()
        ElseIf (Src = "EditWO") Then
            'Workorder editted and saved as draft

            ViewState("Comments") = "Draft Edited"
            ViewState("WOStatus") = ViewState("WOStatus_Edit")
            success = updateWorkOrder(WOID)
        ElseIf (Src = "SampleWO") Then
            'sample workorder saved as draft
            If (Not IsNothing(ViewState("WOID"))) Then
                If (ViewState("WOID") <> 0) Then
                    ViewState("Comments") = "Draft Edited"
                    updateWorkOrder(ViewState("WOID"))
                    ViewState("WOStatus") = ApplicationSettings.WOStatusID.Draft
                End If
            Else
                ViewState("WOStatus") = ApplicationSettings.WOStatusID.Draft
                ViewState("Comments") = "Draft Created"
                saveWorkOrder()
            End If
        End If
        If success <> 1 Then
            CheckRedirect("Draft")
        End If
        Session("ClientQAnsId") = Nothing
        Session("ClientQ") = Nothing

    End Sub

    ''' <summary>
    ''' Exit for the workorder confrimation screes 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect(getBackToListingLink())
    End Sub

    ''' <summary>
    ''' Exit form the workorder from
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancelTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTop.Click, btnCancelBtm.Click
        Response.Redirect(getBackToListingLink())
    End Sub

    ''' <summary>
    ''' Function to get the Back to listing link
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getBackToListingLink() As String
        Dim link As String = ""
        Select Case Request("sender")
            Case "AdminContactsListing"
                link = "~\AMContacts.aspx?"
                link &= "bizDivId=" & Request("BizDivID")
                link &= "&contactType=" & Request("contactType")
                link &= "&PS=" & Request("PS")
                link &= "&PN=" & Request("PN")
                link &= "&SC=" & Request("SC")
                link &= "&SO=" & Request("SO")
                link &= "&fromDate=" & Request("fromDate")
                link &= "&toDate=" & Request("toDate")
                link &= "&sender=" & Request("sender")
            Case "TemplateWOListing"
                link = "~\TemplateWOListing.aspx?"
                link &= "bizDivId=" & Request("BizDivID")
                link &= "&contactType=" & Request("contactType")
                link &= "&PS=" & Request("PS")
                link &= "&PN=" & Request("PN")
                link &= "&SC=" & Request("SC")
                link &= "&SO=" & Request("SO")
                link &= "&sender=WOForm"
            Case "AdminSearchAccounts"
                link = "~\SearchAccounts.aspx?"
                link &= "bizDivId=" & Request("BizDivID")
                link &= "&companyId=" & Request("CompanyID")
                link &= "&contactId=" & Request("contactId")
                link &= "&classId=" & Request("ClassID")
                link &= "&statusId=" & Request("statusId")
                link &= "&roleGroupId=" & Request("roleGroupId")
                If Not IsNothing(Request("vendorIDs")) Then
                    link &= "&vendorIDs=" & Request("vendorIDs")
                End If

                link &= "&product=" & Request("product")
                link &= "&region=" & Request("region")
                link &= "&txtPostCode=" & Request("txtPostCode")
                link &= "&txtKeyword=" & Request("txtKeyword")
                link &= "&refcheck=" & Request("refcheck")
                link &= "&txtFName=" & Request("txtFName")
                link &= "&txtLName=" & Request("txtLName")
                link &= "&txtCompanyName=" & Request("txtCompanyName")
                'Poonam modified on 31-03-2017 - Task - OA-427 - OA - Unsupported spaces in Admin users
                link &= "&txtEmail=" & (Request("txtEmail"))
                link &= "&CRB=" & Request("CRB")
                link &= "&CSCS=" & Request("CSCS")
                link &= "&UKSec=" & Request("UKSec")
                link &= "&vendor=" & Request("vendor")
                link &= "&Nto=" & Request("Nto")
                link &= "&Dts=" & Request("Dts")
                ' TODO
                link &= "&mode=" & ""
                link &= "&sender=" & Request("sender")
                link &= "&PS=" & Request("PS")
                link &= "&PN=" & Request("PN")
                link &= "&SC=" & Request("SC")
                link &= "&SO=" & Request("SO")
            Case Else
                If Not IsNothing(Request("sender")) Then
                    If Request("sender") = "UCWOsListing" Then
                        If Viewer = "Supplier" Then
                            link = "SupplierWOListing.aspx?"
                        ElseIf Viewer = "Buyer" Then
                            link = "BuyerWOListing.aspx?"
                        ElseIf Viewer = "Admin" Then
                            link = "AdminWOListing.aspx?"
                        End If
                    ElseIf Request("sender") = "SearchWO" Then
                        link = "SearchWOs.aspx?"
                    Else
                        If Viewer = "Supplier" Then
                            link = "SupplierWODetails.aspx?"
                        ElseIf Viewer = "Buyer" Then
                            link = "BuyerWODetails.aspx?"
                        ElseIf Viewer = "Admin" Then
                            link = "AdminWODetails.aspx?"
                        End If
                    End If

                    link &= "WOID=" & Request("WOID")
                    link &= "&WorkOrderID=" & Request("WorkOrderID")
                    If Not IsNothing(Request("SearchWorkorderID")) Then
                        link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
                    End If
                    link &= "&ContactId=" & Request("ContactId")
                    link &= "&CompanyId=" & Request("CompanyId")
                    link &= "&SupContactId=" & Request("SupContactId")
                    link &= "&SupCompId=" & Request("SupCompId")
                    link &= "&Group=" & Request("Group")
                    link &= "&mode=" & Request("Group")
                    link &= "&BizDivID=" & Request("BizDivID")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&CompID=" & Request("CompID")
                    link &= "&sender=" & Request("sender")

                    Return link
                Else
                    If Viewer = "Admin" Then
                        Response.Redirect("AdminWOListing.aspx?mode=Submitted")
                    ElseIf Viewer = "Buyer" Then
                        Response.Redirect("BuyerWOListing.aspx?mode=" & Request("Group"))
                    Else
                        Response.Redirect("Welcome.aspx")
                    End If
                End If
        End Select
        Return link
    End Function

    ''' <summary>
    ''' Function to submit workorder
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If (checkReceiptNo()) Then
            ViewState("Comments") = "WorkOrder Created"
            Dim returnStatus As Integer
            'New workorder submitted
            If (Src = "NewWO") Or (Src = "CreateSimilarWOFromTemplate") Then
                ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted
                saveWorkOrder()
            ElseIf (Src = "EditWO") Then
                'Workorder editted & then submitted
                'If workorder is in draft stage and then Submitted - Status = Submitted
                If (ViewState("WOStatus_Edit") = ApplicationSettings.WOStatusID.Draft) Then
                    ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted
                ElseIf (ViewState("WOStatus_Edit") = ApplicationSettings.WOStatusID.Submitted) Then
                    'If workorder in Submitted Stage - then Update the Submitted entry and status = Submitted
                    ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted
                Else
                    'If workorder in CA stage or Sent stage.
                    ViewState("WOStatus") = ViewState("WOStatus_Edit")
                End If



                returnStatus = updateWorkOrder(WOID)

                'Response.Redirect(getBackToListingLink())
            ElseIf (Src = "SampleWO") Then
                'Sample workorder submitted
                If (Not IsNothing(ViewState("WOID"))) Then
                    If (ViewState("WOID") <> 0) Then
                        ViewState("WOStatus_Edit") = ApplicationSettings.WOStatusID.Draft
                        ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted
                        returnStatus = updateWorkOrder(ViewState("WOID"))
                    End If
                Else
                    ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted
                    saveWorkOrder()
                End If
            End If

            CWOFileAttach.ClearUCFileUploadCache()
            CheckRedirect("New")
        End If
        'Response.Redirect("AdminWOListing.aspx?mode=Submitted")
    End Sub

    ''' <summary>
    ''' This function checks for correct version and redirects accordingly
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CheckRedirect(ByVal Type As String)
        Dim link As String = ""
        link &= "&WOID=" & Request("WOID")
        link &= "&WorkOrderID=" & Request("WorkOrderID")
        If Not IsNothing(Request("SearchWorkorderID")) Then
            link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
        End If
        link &= "&ContactId=" & Request("ContactId")
        link &= "&CompanyId=" & Request("CompanyId")
        link &= "&SupContactId=" & Request("SupContactId")
        link &= "&SupCompId=" & Request("SupCompId")
        link &= "&Group=" & Request("Group")
        link &= "&BizDivID=" & Request("BizDivID")
        link &= "&FromDate=" & Request("FromDate")
        link &= "&ToDate=" & Request("ToDate")
        link &= "&PS=" & Request("PS")
        link &= "&PN=" & Request("PN")
        link &= "&SC=" & Request("SC")
        link &= "&SO=" & Request("SO")
        link &= "&CompID=" & Request("CompID")
        link &= "&sender=" & Request("sender")

        If Not IsNothing(ViewState("SucessStatus")) Then
            If ViewState("SucessStatus") <> -10 Then
                CWOFileAttach.ClearUCFileUploadCache()
                If Not IsNothing(Request("group")) Then
                    If Request("group") = ApplicationSettings.WOGroupCA Then
                        Response.Redirect(getBackToListingLink())
                    ElseIf Request("group") = ApplicationSettings.WOGroupSent Then
                        Response.Redirect(getBackToListingLink())
                    Else
                        Session("AdminSelectedCompID") = hdnContactID.Value
                        If (Type = "New") Then
                            Response.Redirect("AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupSubmitted & link)
                        ElseIf (Type = "Draft") Then
                            Response.Redirect("AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupDraft & link)
                        Else
                            Response.Redirect("AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupSubmitted & link)
                        End If
                    End If
                Else
                    Session("AdminSelectedCompID") = hdnContactID.Value
                    If (Type = "New") Then
                        Response.Redirect("AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupSubmitted & link)
                    ElseIf (Type = "Draft") Then
                        Response.Redirect("AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupDraft & link)
                    Else
                        Response.Redirect("AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupSubmitted & link)
                    End If
                End If

            End If
        Else
            CWOFileAttach.ClearUCFileUploadCache()
            Session("AdminSelectedCompID") = hdnContactID.Value
            If (Type = "New") Then
                Response.Redirect("AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupSubmitted & link)
            ElseIf (Type = "Draft") Then
                Response.Redirect("AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupDraft & link)
            Else
                Response.Redirect("AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupSubmitted & link)
            End If
        End If
    End Sub
#End Region

#Region "Save"

    ''' <summary>
    ''' Function to save the workorder details as draft
    ''' </summary>
    ''' <remarks></remarks>
    Private Function saveWorkOrder() As Integer
        ViewState("WOID") = 0
        Dim dsWorkOrder As New WorkOrderNew
        Dim Uplift As Boolean = False
        'Updating workorder tracking table
        Dim nrowWO As WorkOrderNew.tblWorkOrderRow = dsWorkOrder.tblWorkOrder.NewRow
        With nrowWO
            .BizDivID = BizDivID
            .WOID = 0
            .WorkOrderID = "" 'Created using a formula  in the table  
            .DateCreated = Date.Now
            If Not IsNothing(chkIngoreAutomatch) Then
                .IsAMIgnored = chkIngoreAutomatch.Checked
                ViewState("IsAMIgnored") = chkIngoreAutomatch.Checked
            Else
                .IsAMIgnored = ViewState("IsAMIgnored")
            End If



            If hdnSubCategoryVal.Value <> "" Then
                ViewState("WOCategoryID") = hdnSubCategoryVal.Value
            Else
                ViewState("WOCategoryID") = ddlWOSubType.SelectedValue
            End If
            .WOCategoryID = ViewState("WOCategoryID")

            If ddlLocation.SelectedValue = "" Or Not chkUseSameAddress.Checked Then
                .LocationId = 0
            Else
                .LocationId = ddlLocation.SelectedValue
            End If

            .AlternateContactId = ContactID
            .WOTitle = txtTitle.Text
            .WOShortDesc = ""
            .WOLongDesc = txtWOLongDesc.Text.Replace(Chr(13), "<BR>")
            .PONumber = txtPONumber.Text
            .JRSNumber = txtJRSNumber.Text
            .SpecialInstructions = txtSpecialInstructions.Text.Replace(Chr(13), "<BR>")
            .DressCode = ddlDressCode.SelectedValue
            .OfficeTimings = ""
            .WOStatus = ViewState("WOStatus")
            .DataRef = 0
            .LastActionRef = 0
            .SupplierCompanyID = 0
            .SupplierContactID = 0

            'If txtClientScope.Text.Trim <> "" Then
            '    .ClientScope = txtClientScope.Text.Trim.Replace(Chr(13), "<BR>")
            'End If
            'Added code to save billing location to table work order
            If Not IsNothing(drpdwnBillingLocation) Then
                If drpdwnBillingLocation.SelectedValue <> "" Then
                    .BillingLocID = drpdwnBillingLocation.SelectedValue
                    ViewState("BillingLocID") = .BillingLocID
                Else
                    ViewState("BillingLocID") = .BillingLocID
                End If
            End If
            If rdo0ValuePP.Checked = True Then
                .IsRAQ = True
            Else
                .IsRAQ = False
            End If

            If chkForceSignOff.Checked = True Then
                .IsSignOffSheetReqd = True
            Else
                .IsSignOffSheetReqd = False
            End If



            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                If txtInvoiceTitle.Text.Trim <> "" Then
                    .InvoiceTitle = txtInvoiceTitle.Text
                End If
            End If



            If Viewer = "Admin" Then
                'ContactId & CompanyId
                .CompanyId = hdnContactID.Value
                .ContactId = ViewState("BuyerContactId")

                'Buyer Accepted
                If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Draft Then
                    'Work Request - Saved as draft then Buyer Accepted = False, Wholesale Price = WO Value
                    .BuyerAccepted = False
                ElseIf (ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted) Then
                    'Work Request - Submitted with RAQ or Fixed Price
                    .BuyerAccepted = True
                End If


                'Wholesale price & Platform Price
                If hdnWPcomp.Value <> "" Then
                    If CDec(txtSpendLimitWP.Text) = CDec(hdnWPcomp.Value) Then
                        .WholesalePrice = CDec(hdnDBWP.Value)
                    Else
                        .WholesalePrice = CDec(txtSpendLimitWP.Text)
                    End If
                Else
                    .WholesalePrice = CDec(txtSpendLimitWP.Text)
                End If
                '.WholesalePrice = CDec(txtSpendLimitWP.Text)
                .PlatformPrice = CDec(txtSpendLimitPP.Text)

                If Not IsNothing(ViewState("BusinessArea")) Then
                    If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                        If hdnInstTime.Value = ApplicationSettings.EveningInstallation And hdnProductId.Value <> "" Then
                            If txtSpendLimitPP.Text <> "" Then
                                If CDec(hdnPlatformPrice.Value) <> 0 Then
                                    .PlatformPrice = CDec(txtSpendLimitPP.Text)
                                Else
                                    .PlatformPrice = CDec(hdnPPUplift.Value)
                                End If
                            Else
                                .PlatformPrice = CDec(hdnPPUplift.Value)
                            End If
                            'Wholesale Price = WO Value entered.
                            .WholesalePrice = CDec(txtSpendLimitWP.Text)
                        End If
                    End If
                End If
                .SupplierAccepted = False

                'Review Bids is associated with Platform Price only. Pltaform Price can be set by OW rep only.
                .ReviewBids = chkReviewBid.Checked

                'Show loc to Supplier
                .ShowLocToSupplier = chkShowLoc.Checked


                '************************************************************
                'Staged Work Order Code Starts
                If rdoStagedYes.Checked = True Then
                    .StagedWO = "Yes"
                Else
                    .StagedWO = "No"
                End If
                .EstimatedTimeInDays = txtEstTimeRqrd.Text
                .WholesaleDayJobRate = txtWholesaleDayJobRate.Text
                .PlatformDayJobRate = txtPlatformDayJobRate.Text
                .PricingMethod = ddlPricingMethod.SelectedValue
                'Staged Work Order Code Ends
                '************************************************************

            End If


            If Not IsNothing(drpdwnGoodsCollection) Then
                If drpdwnGoodsCollection.SelectedValue <> "" Then
                    .GoodsLocation = drpdwnGoodsCollection.SelectedValue
                End If
            End If

            'Logic for Saving fields depending upon business areas
            If Request.Browser.Browser = "IE" Then
                .EquipmentDetails = txtProductsPur.Text.Replace(Chr(13), "<BR>")
                .ClientSpecialInstructions = txtClientInstru.Text.Replace(Chr(13), "<BR>")
            Else
                .EquipmentDetails = txtProductsPur.Text.Replace(Chr(10), "<BR>")
                .ClientSpecialInstructions = txtClientInstru.Text.Replace(Chr(10), "<BR>")
            End If
            If Not IsNothing(ViewState("BusinessArea")) Then
                If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then

                    .SalesAgent = txtSalesAgent.Text
                    .AptTime = hdnInstTime.Value
                    If hdnFreesat.Value <> "" Then
                        .FreesatMake = hdnFreesat.Value
                    End If
                End If
            End If
        End With
        dsWorkOrder.tblWorkOrder.Rows.Add(nrowWO)


        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then

            Dim nrowUpSell As WorkOrderNew.tblWOUpsellRow = dsWorkOrder.tblWOUpsell.NewRow
            With nrowUpSell
                .WOID = 0
                If ChkIncludeUpSell.Checked = True Then

                    If txtUpSellPrice.Text.Trim <> "" Then
                        .UpSellPrice = txtUpSellPrice.Text

                        If txtUpSellInvoiceTitle.Text.Trim <> "" Then
                            .UpSellInvoiceTitle = txtUpSellInvoiceTitle.Text
                        End If

                        If txtBCFName.Text.Trim <> "" Then
                            .BCFirstName = txtBCFName.Text
                        End If

                        If txtBCLName.Text.Trim <> "" Then
                            .BCLastName = txtBCLName.Text
                        End If

                        If txtBCAddress.Text.Trim <> "" Then
                            .BCAddress = txtBCAddress.Text
                        End If

                        If txtBCCity.Text.Trim <> "" Then
                            .BCCity = txtBCCity.Text
                        End If

                        If txtBCCounty.Text.Trim <> "" Then
                            .BCCounty = txtBCCounty.Text
                        End If

                        If txtBCPostalCode.Text.Trim <> "" Then
                            .BCPostCode = txtBCPostalCode.Text
                        End If

                        If txtBCPhone.Text.Trim <> "" Then
                            .BCPhone = txtBCPhone.Text
                        End If

                        If txtBCFax.Text.Trim <> "" Then
                            .BCFax = txtBCFax.Text
                        End If
                    Else
                        .UpSellPrice = 0.0
                    End If
                End If
            End With
            dsWorkOrder.tblWOUpsell.Rows.Add(nrowUpSell)
        End If


        'Updating the work tracking table
        Dim nrowTracking As WorkOrderNew.tblWOTrackingRow = dsWorkOrder.tblWOTracking.NewRow
        With nrowTracking
            .BizDivID = BizDivID
            .WOTrackingId = 0
            .WOID = 0
            .DateCreated = Date.Now

            If Viewer = "Admin" Then
                .CompanyID = AdminCompID
                .ContactID = AdminConID
                .ContactClassID = ApplicationSettings.RoleOWID

                'WOvalue = Wholesale Price
                .Value = CDec(txtSpendLimitWP.Text)
            End If

            .Status = ViewState("WOStatus")
            If txtScheduleWInBegin.Text <> "" Then
                .DateStart = txtScheduleWInBegin.Text
            Else
                .DateStart = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
            End If
            If txtScheduleWInEnd.Text <> "" Then
                .DateEnd = txtScheduleWInEnd.Text
            Else
                .DateEnd = CType(CDate(txtScheduleWInBegin.Text).AddDays(1), Date).ToString("dd/MM/yyyy")
            End If
            .EstimatedTime = CDate(.DateEnd).Subtract(CDate(.DateStart)).Days.ToString
            ViewState("EstimatedTime") = CDate(.DateEnd).Subtract(CDate(.DateStart)).Days.ToString
            '.EstimatedTime = lblEstimatedTime.Text
            .SpecialistSuppliesParts = False
            .Comments = ViewState("Comments")
            If Not IsNothing(ViewState("BusinessArea")) Then
                If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                    .ActualTime = hdnInstTime.Value
                Else
                    .ActualTime = ""
                End If
            Else
                .ActualTime = ""
            End If
            .DateModified = Date.Now
            .SpecialistID = 0

            'For Tracking
            If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                .EstimatedTimeInDays = txtEstTimeRqrd.Text
                '.DayJobRate = txtPlatformDayJobRate.Text
                .DayJobRate = txtWholesaleDayJobRate.Text
            End If

        End With
        dsWorkOrder.tblWOTracking.Rows.Add(nrowTracking)

        If ddlLocation.SelectedValue = "" Or Not chkUseSameAddress.Checked Then
            'Updating the addresses table
            Dim nrowLoc As WorkOrderNew.tblContactsAddressesRow = dsWorkOrder.tblContactsAddresses.NewRow
            With nrowLoc
                .AddressID = 0

                If Viewer = "Admin" Then
                    .ContactID = hdnContactID.Value
                End If

                .Type = "Temporary"

                .Name = txtName.Text
                .Address = txtCompanyAddress.Text
                .City = txtCity.Text
                .State = txtCounty.Text
                .PostCode = CommonFunctions.FormatPostcode(txtCompanyPostalCode.Text)
                .CountryID = ApplicationSettings.DefaultCountry
                .Sequence = 0
                .IsDefault = False
                .ContactFname = txtFName.Text
                .ContactLname = txtLName.Text
                .ContactPhone = txtCompanyPhone.Text
                .ContactFax = txtCompanyFax.Text
                .Mobile = txtMobile.Text
                .IsDepot = False
                'Poonam added on 11/1/2017 - Task -  OA-385 :OA - Add Customer email field to New Workorder booking page
                .Email = txtCustomerEmail.Text.Trim
            End With
            dsWorkOrder.tblContactsAddresses.Rows.Add(nrowLoc)
        End If


        'CWOFileAttach.AttachmentForSource = "WorkOrder"
        'UCFileUpload1.AttachmentForSource = "StatementOfWork"
        'Attach attachments with th workorder
        If Not IsNothing(ViewState("WOProduct")) Then
            If ViewState("WOProduct") = "True" Then
                CWOFileAttach.Type = "WorkOrder"
                CWOFileAttach.AttachmentForID = hdnProductId.Value
                CWOFileAttach.AttachmentForSource = "WOProduct"
            End If
        End If
        'Changed Code by pankaj malav on 4 nov i.e. remove else if and write it as sepearte if condition.
        If Src = "CreateSimilarWOFromTemplate" Then
            CWOFileAttach.Type = "WorkOrder"
            CWOFileAttach.AttachmentForID = WOID
            CWOFileAttach.AttachmentForSource = "TemplateWorkOrder"
            CWOFileAttach.Type = "StatementOfWork"
            CWOFileAttach.AttachmentForID = WOID
            UCFileUpload1.AttachmentForSource = "TemplateStatementOfWork"
        End If

        dsWorkOrder = CWOFileAttach.ReturnFilledAttachments(dsWorkOrder)

        'For OW Admin Statement of works attachment
        If Viewer = "Admin" Then
            dsWorkOrder = UCFileUpload1.ReturnFilledAttachments(dsWorkOrder)
        End If

        For Each drow As DataRow In dsWorkOrder.Tables("tblattachmentinfo").Rows
            If drow.Item("LinkSource") <> "CompanyProfile" Then
                drow.Item("AttachmentID") = 0
                drow.Item("LinkID") = 0
                'Added by Sabita for OA-76
                'Pre-existing attached file in Company profile are not uploaded to Work orders when selected
            Else
                drow.Item("AttachmentID") = 0
                drow.Item("LinkID") = 0
                drow.Item("LinkSource") = "WorkOrder"
                drow.Item("Type") = "application/vnd"
            End If
        Next
        Dim xmlContent As String = dsWorkOrder.GetXml
        Dim xmlContentMail As String = dsWorkOrder.GetXml
        'Remove XML namespace attribute and its value
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)

        Dim ClientQAnsId As Integer = 0
        If (Not Session("ClientQAnsId") Is Nothing) Then
            ClientQAnsId = Session("ClientQAnsId")
        End If

        Dim PPerRate As Decimal
        If (txtPPPerRate.Text.Trim <> "") Then
            PPerRate = CDec(txtPPPerRate.Text.Trim)
        Else
            PPerRate = CDec(txtSpendLimitPP.Text)
        End If

        Dim ClientScope As String
        ClientScope = ""
        If (txtClientScope.Text.Trim <> "") Then
            ClientScope = txtClientScope.Text.Replace(Chr(13), "<BR>")
        End If

        Dim IsWaitingToAutomatch As Boolean
        If chkIsWaitingToAutomatch.Checked = True Then
            IsWaitingToAutomatch = True
        Else
            IsWaitingToAutomatch = False
        End If

        'Updating the TagOtherLinkage table
        Dim dsTagOtherLinkage As DataSet = UCAccreditations.GetSelectedAccred()
        Dim xmlTagOtherLinkage As String = dsTagOtherLinkage.GetXml

        'Save data to DB
        Dim dsSendMail As DataSet = ws.WSWorkOrder.MS_WOAddUpdateWorkOrderDetails(xmlContent, "add", "", 0, 0, Viewer, hdnProductId.Value, CommonFunctions.FetchVerNum(), Session("UserID"), chkbxIncludeWeekends.Checked, chkbxIncludeHolidays.Checked, ClientQAnsId, PPerRate, ddPPPerRate.SelectedValue, txtReceiptNumber.Text.Trim, ClientScope, xmlTagOtherLinkage, IsWaitingToAutomatch, txtQuantity.Text.Trim, txtRelatedWorkorder.Text.Trim)


        Dim NoOfWO As Integer
        Dim OriginalWOID As Integer
        NoOfWO = 1
        If (txtNoOfWO.Text <> "") Then
            NoOfWO = txtNoOfWO.Text.Trim
        End If
        If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted Then
            If (CInt(ViewState("BuyerCompanyId")) = CInt(ApplicationSettings.ArgosLtdAccount) Or CInt(ViewState("BuyerCompanyId")) = CInt(ApplicationSettings.Telecare24Company) Or CInt(ViewState("BuyerCompanyId")) = CInt(ApplicationSettings.Lifeline24Company)) Then
                If txtCustomerEmail.Text.Trim.Length > 0 Then
                    Emails.SendCustomerJobBookEmail("Booking confirmation", txtCustomerEmail.Text.Trim, dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WorkOrderId"), txtScheduleWInBegin.Text.Trim, hdnInstTime.Value.ToString, txtClientScope.Text.Replace(Chr(13), "<BR>").Replace(Chr(10), "<BR>"), txtFName.Text.Trim & " " & txtLName.Text.Trim, CInt(ViewState("BuyerCompanyId")), txtPONumber.Text.Trim)
                End If
            End If
        End If

        CommonFunctions.IsValidLatLong(CommonFunctions.FormatPostcode(txtCompanyPostalCode.Text.Trim), "workorder", dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WorkOrderId"))

        If (NoOfWO > 1 And NoOfWO < 10) Then
            If dsSendMail.Tables.Count <> 0 Then
                If (dsSendMail.Tables("tblWorkOrder").Rows.Count > 0) Then
                    If Not IsDBNull(dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WOID")) Then
                        WOID = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WOID")
                        dsSendMail = ws.WSWorkOrder.CreateMultipleWO(WOID, NoOfWO)
                        If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted Then

                        End If

                    End If
                End If
            End If
        End If

        'Poonam - Task-  OA-406 : OA - Wo is not showing in Listing when there is no Billing Location
        If ViewState("BillingLocID") = 0 Then
            Emails.SendWOSubmittedWithoutBillingLocation(dsSendMail.Tables("tblWorkOrder").Rows(0)("WorkOrderId"))
        End If

        Dim dSave As [Delegate] = New AfterWOSave(AddressOf AfterSave)
        ThreadUtil.FireAndForget(dSave, New Object() {dsSendMail})

    End Function

    ''' <summary>
    ''' After Save background process
    ''' </summary>
    ''' <param name="dsSendMail"></param>
    ''' <remarks></remarks>
    Public Sub AfterSave(ByVal dsSendMail As DataSet)
        Dim dvEmail As New DataView
        Dim objEmail As New Emails
        Dim fp As IFormatProvider = New System.Globalization.CultureInfo("en-US")
        Try
            If dsSendMail.Tables("EmailStatus").Rows(0).Item("WONotification") = True Then
                If dsSendMail.Tables.Count <> 0 Then
                    If dsSendMail.Tables("tblSendMail").Rows.Count > 0 Then
                        If dsSendMail.Tables("tblSendMail").Rows(0).Item("ContactID") = -1 Then
                            'double entry
                            ''Added By Pankaj Malav
                            'Code for logging information using log4net
                            CommonFunctions.createLog("OWPortal Normal WO - WOSave: Double Entry attempt")
                        ElseIf dsSendMail.Tables("tblSendMail").Rows(0).Item("ContactID") = -3 Then
                            'double entry after CA
                            CommonFunctions.createLog("OWPortal Normal WO - WOSave: double entry after CA")
                            'Return -1
                        Else

                            Dim dt As DataTable
                            Dim dr As DataRow
                            dt = dsSendMail.Tables("tblWorkOrder")
                            Dim AllWos As String
                            AllWos = ""
                            For Each dr In dt.Rows
                                ViewState("WOID") = dr("WOID")
                                ViewState("WOTrackingId") = dr("WOTrackingId")
                                ViewState("LocId") = dr("LocationId")
                                ViewState("Location") = dr("Location")
                                ViewState("WOCategoryID") = dr("WOCategoryId")
                                ViewState("WOCategory") = dr("WOCategory")
                                'Send mail only on submission of work order
                                If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted Then
                                    dvEmail = dsSendMail.Tables("tblSendMail").Copy.DefaultView
                                    objEmail.WorkOrderID = dr("WorkOrderId")
                                    'ViewState("WorkOrderId") = dr("WorkOrderId")
                                    If txtCompanyPostalCode.Text.Length > 5 Then
                                        objEmail.WOLoc = txtCity.Text & "," & txtCompanyPostalCode.Text.Substring(0, 5)
                                    Else
                                        objEmail.WOLoc = txtCity.Text & "," & txtCompanyPostalCode.Text
                                    End If
                                    objEmail.WOTitle = txtTitle.Text
                                    objEmail.WOCategory = ViewState("WOCategory")
                                    objEmail.WOPrice = txtSpendLimitWP.Text
                                    objEmail.WOCustomerPrice = WOCustomerPrice
                                    objEmail.WOStartDate = txtScheduleWInBegin.Text
                                    If txtScheduleWInEnd.Text <> "" Then
                                        objEmail.WOEndDate = txtScheduleWInEnd.Text
                                    Else
                                        objEmail.WOEndDate = DateTime.ParseExact(txtScheduleWInBegin.Text, "dd/MM/yyyy", fp).AddDays(1).ToString("dd/MM/yyyy")
                                    End If
                                    objEmail.WPPrice = txtSpendLimitWP.Text

                                    If rdo0ValueWP.Checked = True Then
                                        objEmail.QuoteRequired = True
                                    End If
                                    'Mail to the client & admin

                                    Emails.SendWorkOrderMail(objEmail, ApplicationSettings.UserType.buyer, ApplicationSettings.WOAction.SubmitWO, "", dvEmail)

                                End If
                                If (AllWos = "") Then
                                    AllWos = dr("WOID")
                                Else
                                    AllWos = AllWos & "," & dr("WOID")
                                End If
                            Next

                            ' check if workorder can participate in AutoMatch
                            ' Check whether automatch is enabled or not
                            If hdnPlatformPrice.Value <> "" Then
                                If CDec(hdnPlatformPrice.Value) > 0 Then
                                    'WOID = ViewState("WOID")
                                    ViewState("PPPrice") = hdnPlatformPrice.Value
                                    If txtScheduleWInBegin.Text <> "" Then
                                        ViewState("DateStart") = txtScheduleWInBegin.Text
                                    Else
                                        ViewState("DateStart") = CType(Date.Now.AddDays(1), Date).ToString("dd/MM/yyyy")
                                    End If

                                    If txtScheduleWInEnd.Text <> "" Then
                                        ViewState("DateEnd") = txtScheduleWInEnd.Text
                                    Else
                                        ViewState("DateEnd") = DateTime.ParseExact(txtScheduleWInBegin.Text, "dd/MM/yyyy", fp).AddDays(1).ToString("dd/MM/yyyy")
                                    End If
                                    CommonFunctions.createLog("OWPortal Normal WO - WOSave Auto Match entering for WorkOrderId : " & AllWos)

                                    If Not IsNothing(ViewState("IsAMIgnored")) Then
                                        If Not ViewState("IsAMIgnored") Then
                                            'Poonam - Task-  OA-406 : OA - Wo is not showing in Listing when there is no Billing Location              
                                            If ViewState("BillingLocID") <> 0 Then
                                                CallAutoMatch(AllWos)
                                            End If
                                        End If
                                    End If
                                    CommonFunctions.createLog("OWPortal Normal WO - WOSave Auto Match exiting for WorkOrderId : " & AllWos)
                                End If
                            End If


                        End If
                    End If
                End If
            Else

                Dim dt As DataTable
                Dim dr As DataRow
                dt = dsSendMail.Tables("tblWorkOrder")
                Dim AllWos As String
                AllWos = ""
                For Each dr In dt.Rows
                    If (AllWos = "") Then
                        AllWos = dr("WOID")
                    Else
                        AllWos = AllWos & "," & dr("WOID")
                    End If
                Next
                ViewState("WOID") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WOID")
                ViewState("WOTrackingId") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WOTrackingId")
                ViewState("LocId") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("LocationId")
                ViewState("Location") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("Location")
                ViewState("WOCategoryID") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WOCategoryId")
                ViewState("WOCategory") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WOCategory")
                ' check if workorder can participate in AutoMatch
                ' Check whether automatch is enabled or not
                If hdnPlatformPrice.Value <> "" Then
                    If CDec(hdnPlatformPrice.Value) > 0 Then
                        WOID = ViewState("WOID")
                        ViewState("PPPrice") = hdnPlatformPrice.Value
                        If txtScheduleWInBegin.Text <> "" Then
                            ViewState("DateStart") = txtScheduleWInBegin.Text
                        Else
                            ViewState("DateStart") = CType(Date.Now.AddDays(1), Date).ToString("dd/MM/yyyy")
                        End If

                        If txtScheduleWInEnd.Text <> "" Then
                            ViewState("DateEnd") = txtScheduleWInEnd.Text
                        Else
                            ViewState("DateEnd") = DateTime.ParseExact(txtScheduleWInBegin.Text, "dd/MM/yyyy", fp).AddDays(1).ToString("dd/MM/yyyy")
                        End If
                        CommonFunctions.createLog("OWPortal Normal WO - WOSave Auto Match entering for WorkOrderId : " & AllWos)
                        If Not IsNothing(ViewState("IsAMIgnored")) Then
                            If Not ViewState("IsAMIgnored") Then
                                CallAutoMatch(AllWos)
                            End If
                        End If
                        CommonFunctions.createLog("OWPortal Normal WO - WOSave Auto Match exiting for WorkOrderId : " & AllWos)
                    End If
                End If
            End If
        Catch ex As Exception
            Dim exceptionstr As String
            exceptionstr = ApplicationSettings.WebPath & "<p> BackGround Process in WOSave. Company Id = " & Session("CompanyId") & " ContactId = " & Session("UserId") & " WorkOrderId = " & ViewState("WOID") & " Previous URL : " & Context.Request.UrlReferrer.AbsoluteUri & "Present Raw URL : " & Context.Request.RawUrl & "</p>"
            CommonFunctions.createLog("OWPortal" & exceptionstr & " WOSave Error: " & ex.ToString)
            SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, exceptionstr & ex.ToString, "Error in WOSave Back ground process: at " & Now())
        End Try
    End Sub

    ''' <summary>
    ''' After Updating WorkOrder Background Process
    ''' </summary>
    ''' <param name="dsSendMail"></param>
    ''' <remarks></remarks>
    Public Sub UpdateWOSave(ByVal dsSendMail As DataSet)
        Try
            Dim dvEmail As New DataView
            Dim objEmail As New Emails
            If dsSendMail.Tables.Count <> 0 Then
                If dsSendMail.Tables("tblSendMail").Rows.Count > 0 Then
                    If dsSendMail.Tables("tblSendMail").Rows(0).Item("ContactID") = -1 Then
                        'double entry

                    ElseIf dsSendMail.Tables("tblSendMail").Rows(0).Item("ContactID") = -3 Then
                        'double entry after CA
                        divValidationWOSummary.Visible = True
                        lblError1.Text = ResourceMessageText.GetString("DoubleEntryOnCA")
                        'Return -1

                    Else
                        'Send mail only on submission of work order
                        If (Src <> "Admin") Then
                            'For new WOrkOrder get the Workorder ID and WorkOrder Tracking ID
                            ViewState("WOID") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WOID")
                            ViewState("WOTrackingId") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WOTrackingId")
                            ViewState("LocId") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("LocationId")
                            ViewState("Location") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("Location")
                            ViewState("WOCategoryID") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WOCategoryId")
                            ViewState("WOCategory") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WOCategory")
                            'For Admin dont send mail.
                            If (ViewState("WOStatus") <> ViewState("WOStatus_Edit")) Then
                                'If Draft workorder is submitted then send mail
                                If (ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted) And (ViewState("WOStatus_Edit") = ApplicationSettings.WOStatusID.Draft) Then
                                    'Send mail only on submission of work order
                                    If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted Then


                                        dvEmail = dsSendMail.Tables("tblSendMail").Copy.DefaultView
                                        objEmail.WorkOrderID = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WorkOrderId")
                                        If txtCompanyPostalCode.Text.Length > 5 Then
                                            objEmail.WOLoc = txtCity.Text & "," & txtCompanyPostalCode.Text.Substring(0, 5)
                                        Else
                                            objEmail.WOLoc = txtCity.Text & "," & txtCompanyPostalCode.Text
                                        End If
                                        objEmail.WOTitle = txtTitle.Text
                                        objEmail.WOCategory = ViewState("WOCategory")
                                        objEmail.WOPrice = txtSpendLimitWP.Text
                                        objEmail.WOCustomerPrice = WOCustomerPrice
                                        objEmail.WOStartDate = txtScheduleWInBegin.Text
                                        objEmail.WOEndDate = txtScheduleWInEnd.Text
                                        objEmail.WPPrice = txtSpendLimitWP.Text


                                        If rdo0ValueWP.Checked = True Then
                                            objEmail.QuoteRequired = True
                                        End If
                                        'Mail to the client & admin
                                        Emails.SendWorkOrderMail(objEmail, ApplicationSettings.UserType.buyer, ApplicationSettings.WOAction.SubmitWO, "", dvEmail)

                                    End If

                                End If
                            ElseIf (ViewState("WOStatus") = ApplicationSettings.WOStatusID.CA) Then
                                If dsSendMail.Tables.Count = 5 Then
                                    'Run loop For sending mail to each supplier
                                    'Find distinct SupplierIDs and accordingly filter table for mail sending and for supplier response
                                    Dim dtDistinct As New DataTable
                                    Dim dvWOSummary As New DataView
                                    Dim dvEmailInfo As New DataView
                                    Dim dvSupplier As New DataView
                                    Dim CompanyID As Integer

                                    ' Store only distinct supplierIS in dtDistinct
                                    dsSendMail.Tables(4).TableName = "tblMail"
                                    dtDistinct = CommonFunctions.SelectDistinct("tblMail", dsSendMail.Tables("tblMail"), "CompanyID")

                                    dvWOSummary = dsSendMail.Tables(2).Copy.DefaultView
                                    dvSupplier = dsSendMail.Tables(3).Copy.DefaultView
                                    dvEmailInfo = dsSendMail.Tables(4).Copy.DefaultView

                                    For Each row As DataRow In dtDistinct.Rows
                                        CompanyID = row("CompanyID")
                                        dvSupplier.RowFilter = "CompanyID = '" & CompanyID & "'"
                                        dvEmailInfo.RowFilter = "CompanyID = '" & CompanyID & "'"
                                        Emails.SendCAMail(dvEmailInfo.ToTable.DefaultView, dvWOSummary, dvSupplier.ToTable.DefaultView, "")
                                    Next
                                End If
                            End If


                            If hdnPlatformPrice.Value <> "" And ViewState("WOStatus") <> ApplicationSettings.WOStatusID.CA Then
                                If CDec(hdnPlatformPrice.Value) > 0 Then
                                    WOID = ViewState("WOID")
                                    ViewState("PPPrice") = hdnPlatformPrice.Value
                                    CallAutoMatch(ViewState("WOID").ToString)
                                End If
                            End If

                            'Return 0
                        End If
                    End If
                Else
                    'no rows returned
                    divValidationWOSummary.Visible = True
                    lblError1.Text = ResourceMessageText.GetString("DBUpdateFail")
                    'Return -1
                End If
            Else
                'no ds sent
                divValidationWOSummary.Visible = True
                lblError1.Text = ResourceMessageText.GetString("DBUpdateFail")
                'Return -1
            End If
        Catch ex As Exception
            Dim exceptionstr As String
            exceptionstr = ApplicationSettings.WebPath & "<p> BackGround Process in WOUpdate. Company Id = " & Session("CompanyId") & " ContactId = " & Session("UserId") & " WorkOrderId = " & ViewState("WOID") & "</p>"
            CommonFunctions.createLog(exceptionstr & " WOUpdate Error: " & ex.ToString)
            SendMail.SendMail("info@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, exceptionstr & ex.ToString, "Error in WOSave Back ground process: at " & Now())
        End Try
    End Sub

    ''' <summary>
    ''' Function to update the workorder details as draft
    ''' </summary>
    ''' <remarks></remarks>
    Private Function updateWorkOrder(ByVal WOID As Integer) As Integer
        Dim dsWorkOrder As New WorkOrderNew

        'Updating workorder table
        Dim nrowWO As WorkOrderNew.tblWorkOrderRow = dsWorkOrder.tblWorkOrder.NewRow
        With nrowWO
            .BizDivID = BizDivID
            .WOID = WOID
            .WorkOrderID = "" 'Created using a formula  in the table  
            .DateCreated = Date.Now
            .IsAMIgnored = ViewState("IsAMIgnored")

            If ViewState("CombId") = 0 Then
                If hdnSubCategoryVal.Value <> "" Then
                    ViewState("WOCategoryID") = hdnSubCategoryVal.Value
                Else
                    ViewState("WOCategoryID") = ddlWOSubType.SelectedValue
                End If
            Else
                If ddlWOSubType.SelectedValue <> "" Then
                    If hdnSubCategoryVal.Value <> "" Then
                        ViewState("WOCategoryID") = hdnSubCategoryVal.Value
                    Else
                        ViewState("WOCategoryID") = ddlWOSubType.SelectedValue
                    End If
                Else
                    ViewState("WOCategoryID") = ddlWOType.SelectedValue
                End If
            End If

            .WOCategoryID = ViewState("WOCategoryID")

            If WOID <> 0 Then
                If Not IsNothing(ViewState("LocId")) Then
                    If (ViewState("LocId") <> 0) Then
                        .LocationId = ViewState("LocId")
                    End If
                Else
                    If ddlLocation.SelectedValue = "" Or Not chkUseSameAddress.Checked Then
                        .LocationId = 0
                    Else
                        .LocationId = ddlLocation.SelectedValue
                    End If
                End If
            Else
                If ddlLocation.SelectedValue = "" Or Not chkUseSameAddress.Checked Then
                    .LocationId = 0
                Else
                    .LocationId = ddlLocation.SelectedValue
                End If
            End If

            .AlternateContactId = ContactID
            .WOTitle = txtTitle.Text
            .WOShortDesc = ""
            .WOLongDesc = txtWOLongDesc.Text.Replace(Chr(13), "<BR>")
            'If txtClientScope.Text.Trim <> "" Then
            '    .ClientScope = txtClientScope.Text.Replace(Chr(13), "<BR>")
            'End If
            .PONumber = txtPONumber.Text
            .JRSNumber = txtJRSNumber.Text
            .SpecialInstructions = txtSpecialInstructions.Text.Replace(Chr(13), "<BR>")
            .DressCode = ddlDressCode.SelectedValue
            .OfficeTimings = ""
            'if workorder is in Draft stage then update the 
            .WOStatus = ViewState("WOStatus")
            .DataRef = ViewState("DataRef")
            .LastActionRef = 0
            .SupplierCompanyID = 0
            .SupplierContactID = 0

            'Added code to save billing location to table work order
            If Not IsNothing(drpdwnBillingLocation) Then
                If drpdwnBillingLocation.SelectedValue <> "" Then
                    .BillingLocID = drpdwnBillingLocation.SelectedValue
                End If
            End If
            If rdo0ValuePP.Checked = True Then
                .IsRAQ = True
            Else
                .IsRAQ = False
            End If

            If chkForceSignOff.Checked = True Then
                .IsSignOffSheetReqd = True
            Else
                .IsSignOffSheetReqd = False
            End If


            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                If txtInvoiceTitle.Text.Trim <> "" Then
                    .InvoiceTitle = txtInvoiceTitle.Text
                End If
            End If


            If (Src = "EditWO") Then
                .CompanyId = ViewState("BuyerCompanyId")
                .ContactId = ViewState("ContactID")
            Else
                'Sample Workorder
                If Viewer = "Buyer" Then
                    .CompanyId = CompanyID
                    .ContactId = ContactID
                ElseIf Viewer = "Admin" Then
                    'ContactId & CompanyId
                    .CompanyId = ViewState("BuyerCompanyId")
                    .ContactId = ViewState("BuyerContactId")
                End If
            End If

            If Viewer = "Admin" Then

                'Buyer Accepted
                If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Draft Then
                    'Work Request - Saved as draft then Buyer Accepted = False, Wholesale Price = WO Value
                    .BuyerAccepted = False
                ElseIf (ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted) Then
                    'Work Request - Submitted with RAQ or Fixed Price
                    .BuyerAccepted = True
                ElseIf (ViewState("WOStatus") = ApplicationSettings.WOStatusID.CA) Then
                    .BuyerAccepted = True
                End If
                If CDec(txtSpendLimitWP.Text) = CDec(hdnWPcomp.Value) Then
                    .WholesalePrice = CDec(hdnDBWP.Value)
                Else
                    .WholesalePrice = CDec(txtSpendLimitWP.Text)
                End If
                '.WholesalePrice = CDec(txtSpendLimitWP.Text)
                .PlatformPrice = CDec(txtSpendLimitPP.Text)
                'Wholesale price & Platform Price
                If Not IsNothing(ViewState("Deduction")) Then
                    If ViewState("Deduction") = "True" And hdnInstTime.Value <> ApplicationSettings.EveningInstallation Then
                        .WholesalePrice = CDec(txtSpendLimitWP.Text) - CDec(hdnWPUplift.Value)
                        .PlatformPrice = CDec(txtSpendLimitPP.Text) - CDec(hdnPPUplift.Value)
                    End If
                End If

                .SupplierAccepted = False

                'Review Bids is associated with Platform Price only. Pltaform Price can be set by OW rep only.
                .ReviewBids = chkReviewBid.Checked

                'Show loc to Supplier
                .ShowLocToSupplier = chkShowLoc.Checked

                '************************************************************
                'Staged Work Order Code Starts
                If rdoStagedYes.Checked = True Then
                    .StagedWO = "Yes"
                Else
                    .StagedWO = "No"
                End If
                .EstimatedTimeInDays = txtEstTimeRqrd.Text
                .WholesaleDayJobRate = txtWholesaleDayJobRate.Text
                .PlatformDayJobRate = txtPlatformDayJobRate.Text
                .PricingMethod = ddlPricingMethod.SelectedValue
                'Staged Work Order Code Ends
                '************************************************************
            End If

            'If drpdwnGoodsCollection.SelectedValue = 0 Then
            If drpdwnGoodsCollection.SelectedValue <> "" Then
                .GoodsLocation = drpdwnGoodsCollection.SelectedValue
            End If
            'End If
            If drpdwnFreesatMake.SelectedValue <> "" Then
                .FreesatMake = drpdwnFreesatMake.SelectedValue
            End If
            'Logic for Saving fields depending upon business areas
            If Not IsNothing(ViewState("BusinessArea")) Then
                If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                    If Request.Browser.Browser = "IE" Then
                        .EquipmentDetails = txtProductsPur.Text.Replace(Chr(13), "<BR>")

                        .ClientSpecialInstructions = txtClientInstru.Text.Replace(Chr(13), "<BR>")
                    Else
                        .EquipmentDetails = txtProductsPur.Text.Replace(Chr(10), "<BR>")

                        .ClientSpecialInstructions = txtClientInstru.Text.Replace(Chr(10), "<BR>")
                    End If
                    .SalesAgent = txtSalesAgent.Text
                    .AptTime = hdnInstTime.Value
                Else
                    If Request.Browser.Browser = "IE" Then
                        .EquipmentDetails = txtProductsPur.Text.Replace(Chr(13), "<BR>")
                        .ClientSpecialInstructions = txtClientInstru.Text.Replace(Chr(13), "<BR>")
                    Else
                        .EquipmentDetails = txtProductsPur.Text.Replace(Chr(10), "<BR>")
                        .ClientSpecialInstructions = txtClientInstru.Text.Replace(Chr(10), "<BR>")
                    End If
                End If
            End If
        End With
        dsWorkOrder.tblWorkOrder.Rows.Add(nrowWO)



        If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then


            Dim nrowUpSell As WorkOrderNew.tblWOUpsellRow = dsWorkOrder.tblWOUpsell.NewRow
            With nrowUpSell
                .WOID = WOID

                'UpSell related fields
                If txtUpSellPrice.Text.Trim <> "" Then
                    .UpSellPrice = txtUpSellPrice.Text
                Else
                    .UpSellPrice = 0.0
                End If

                If txtUpSellInvoiceTitle.Text.Trim <> "" Then
                    .UpSellInvoiceTitle = txtUpSellInvoiceTitle.Text
                End If

                If txtBCFName.Text.Trim <> "" Then
                    .BCFirstName = txtBCFName.Text
                End If

                If txtBCLName.Text.Trim <> "" Then
                    .BCLastName = txtBCLName.Text
                End If

                If txtBCAddress.Text.Trim <> "" Then
                    .BCAddress = txtBCAddress.Text
                End If

                If txtBCCity.Text.Trim <> "" Then
                    .BCCity = txtBCCity.Text
                End If

                If txtBCCounty.Text.Trim <> "" Then
                    .BCCounty = txtBCCounty.Text
                End If

                If txtBCPostalCode.Text.Trim <> "" Then
                    .BCPostCode = txtBCPostalCode.Text
                End If

                If txtBCPhone.Text.Trim <> "" Then
                    .BCPhone = txtBCPhone.Text
                End If

                If txtBCFax.Text.Trim <> "" Then
                    .BCFax = txtBCFax.Text
                End If
                'End If

            End With
            dsWorkOrder.tblWOUpsell.Rows.Add(nrowUpSell)

        End If


        'Updating the work tracking table
        Dim nrowTracking As WorkOrderNew.tblWOTrackingRow = dsWorkOrder.tblWOTracking.NewRow
        With nrowTracking
            .BizDivID = BizDivID
            .WOTrackingId = 0
            .WOID = WOID
            .DateCreated = Date.Now

            If Viewer = "Buyer" Then
                .CompanyID = CompanyID
                .ContactID = ContactID
                .ContactClassID = ApplicationSettings.RoleClientID

                .Value = CDec(txtSpendLimitWP.Text)
            ElseIf Viewer = "Admin" Then
                .CompanyID = AdminCompID
                .ContactID = AdminConID
                .ContactClassID = ApplicationSettings.RoleOWID

                .Value = CDec(txtSpendLimitWP.Text)
            End If

            .Status = ViewState("WOStatus")

            If txtScheduleWInBegin.Text <> "" Then
                .DateStart = txtScheduleWInBegin.Text
            Else
                .DateStart = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
            End If
            If txtScheduleWInEnd.Text <> "" Then
                .DateEnd = txtScheduleWInEnd.Text
            Else
                .DateEnd = CType(CDate(txtScheduleWInBegin.Text).AddDays(1), Date).ToString("dd/MM/yyyy")
            End If
            .EstimatedTime = CDate(.DateEnd).Subtract(CDate(.DateStart)).Days.ToString
            ViewState("EstimatedTime") = CDate(.DateEnd).Subtract(CDate(.DateStart)).Days.ToString

            .SpecialistSuppliesParts = False
            .Comments = ViewState("Comments")
            '.ActualTime = ""
            If Not IsNothing(ViewState("BusinessArea")) Then
                If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                    .ActualTime = hdnInstTime.Value
                Else
                    .ActualTime = ""
                End If
            Else
                .ActualTime = ""
            End If
            .DateModified = Date.Now
            .SpecialistID = 0

            'For Tracking
            If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                .EstimatedTimeInDays = txtEstTimeRqrd.Text
                '.DayJobRate = txtPlatformDayJobRate.Text
                .DayJobRate = txtWholesaleDayJobRate.Text
            End If

        End With
        dsWorkOrder.tblWOTracking.Rows.Add(nrowTracking)

        If ddlLocation.SelectedValue = "" Or Not chkUseSameAddress.Checked Then
            'Updating the addresses table
            Dim nrowLoc As WorkOrderNew.tblContactsAddressesRow = dsWorkOrder.tblContactsAddresses.NewRow
            With nrowLoc
                If WOID <> 0 Then
                    If Not IsNothing(ViewState("LocId")) Then
                        If (ViewState("LocId") <> 0) Then
                            .AddressID = ViewState("LocId")
                        End If
                    Else
                        If ddlLocation.SelectedValue = "" Or Not chkUseSameAddress.Checked Then
                            .AddressID = 0
                        Else
                            .AddressID = ddlLocation.SelectedValue
                        End If
                    End If
                Else
                    If ddlLocation.SelectedValue = "" Or Not chkUseSameAddress.Checked Then
                        .AddressID = 0
                    Else
                        .AddressID = ddlLocation.SelectedValue
                    End If
                End If
                .ContactID = CompanyID
                .Type = "Temporary"
                .Name = txtName.Text
                .Address = txtCompanyAddress.Text
                .City = txtCity.Text
                .State = txtCounty.Text
                .PostCode = CommonFunctions.FormatPostcode(txtCompanyPostalCode.Text)
                .CountryID = ApplicationSettings.DefaultCountry
                .Sequence = 0
                .IsDefault = False
                .ContactFname = txtFName.Text
                .ContactLname = txtLName.Text
                .ContactPhone = txtCompanyPhone.Text
                .ContactFax = txtCompanyFax.Text
                .Mobile = txtMobile.Text
                .IsDepot = False
                'Poonam added on 11/1/2017 - Task -  OA-385 :OA - Add Customer email field to New Workorder booking page
                .Email = txtCustomerEmail.Text.Trim
            End With
            dsWorkOrder.tblContactsAddresses.Rows.Add(nrowLoc)
        End If

        'workorder attachments

        dsWorkOrder = CWOFileAttach.ReturnFilledAttachments(dsWorkOrder)

        'For OW Admin Statement of works attachment
        If Viewer = "Admin" Then
            dsWorkOrder = UCFileUpload1.ReturnFilledAttachments(dsWorkOrder)
        End If

        Dim xmlContent As String = dsWorkOrder.GetXml
        Dim xmlContentMail As String = dsWorkOrder.GetXml
        'Remove XML namespace attribute and its value
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)

        Dim dsSendMail As DataSet
        Dim dvEmail As New DataView
        Dim objEmail As New Emails

        Dim ClientQAnsId As Integer = 0
        If (Not Session("ClientQAnsId") Is Nothing) Then
            ClientQAnsId = Session("ClientQAnsId")
        End If

        Dim PPerRate As Decimal
        If (txtPPPerRate.Text.Trim <> "") Then
            PPerRate = CDec(txtPPPerRate.Text.Trim)
        Else
            PPerRate = CDec(txtSpendLimitPP.Text)
        End If

        Dim ClientScope As String
        ClientScope = ""
        If (txtClientScope.Text.Trim <> "") Then
            ClientScope = txtClientScope.Text.Replace(Chr(13), "<BR>")
        End If

        Dim IsWaitingToAutomatch As Boolean
        If chkIsWaitingToAutomatch.Checked = True Then
            IsWaitingToAutomatch = True
        Else
            IsWaitingToAutomatch = False
        End If

        'Updating the TagOtherLinkage table
        Dim dsTagOtherLinkage As DataSet = UCAccreditations.GetSelectedAccred()
        Dim xmlTagOtherLinkage As String = dsTagOtherLinkage.GetXml

        'Save data to DB
        dsSendMail = ws.WSWorkOrder.MS_WOAddUpdateWorkOrderDetails(xmlContent, "edit", "admin", AdminCompID, AdminConID, Viewer, ViewState("ProductID"), CommonFunctions.FetchVerNum(), Session("UserID"), chkbxIncludeWeekends.Checked, chkbxIncludeHolidays.Checked, ClientQAnsId, PPerRate, ddPPPerRate.SelectedValue, txtReceiptNumber.Text.Trim, ClientScope, xmlTagOtherLinkage, IsWaitingToAutomatch, txtQuantity.Text.Trim, txtRelatedWorkorder.Text.Trim)
        If dsSendMail.Tables.Count > 1 Then
            If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted Then
                If (CInt(ViewState("BuyerCompanyId")) = CInt(ApplicationSettings.ArgosLtdAccount) Or CInt(ViewState("BuyerCompanyId")) = CInt(ApplicationSettings.Telecare24Company) Or CInt(ViewState("BuyerCompanyId")) = CInt(ApplicationSettings.Lifeline24Company)) Then
                    If txtCustomerEmail.Text.Trim.Length > 0 Then
                        Emails.SendCustomerJobBookEmail("Booking confirmation", txtCustomerEmail.Text.Trim, dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WorkOrderId"), txtScheduleWInBegin.Text.Trim, hdnInstTime.Value.ToString, txtClientScope.Text.Replace(Chr(13), "<BR>").Replace(Chr(10), "<BR>"), txtFName.Text.Trim & " " & txtLName.Text.Trim, CInt(ViewState("BuyerCompanyId")), txtPONumber.Text.Trim)
                    End If
                End If
            End If

            CommonFunctions.IsValidLatLong(CommonFunctions.FormatPostcode(txtCompanyPostalCode.Text.Trim), "workorder", dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WorkOrderId"))


            If dsSendMail.Tables("tblWorkOrder").Rows.Count > 0 Then
                If dsSendMail.Tables("tblWorkOrder").Rows(0).Item("WOID") <> 0 Then
                    ViewState("SucessStatus") = dsSendMail.Tables("tblWorkOrder").Rows(0).Item("Status")
                    If dsSendMail.Tables("tblWorkOrder").Rows(0).Item("Status") = -10 Then
                        pnlWoForm.Style("display") = "none"
                        pnlWoSummary.Style("display") = "none"
                        divValidationVersionNo.Visible = True

                        lblErrorVersionNo.Text = ResourceMessageText.GetString("WOVersionControlMsg")
                        lblErrorVersionNo.Text = lblErrorVersionNo.Text.Replace("<Link>", "AdminWODetails.aspx?" & "WOID=" & WOID & "&WorkOrderID=" & ViewState("WOID") & "&FromDate=&ToDate=" & "&ContactID=" & ContactID & "&CompanyID=" & CompanyID & "&SupContactID=0&SupCompID=&PS=25&PN=0&SC=DateCreated&SO=1&BizDivID=1&sender=UCWOsListing&CompID=0")
                    End If
                Else
                    pnlWoForm.Style("display") = "none"
                    pnlWoSummary.Style("display") = "none"
                    divValidationVersionNo.Visible = True
                    lblErrorVersionNo.Text = ResourceMessageText.GetString("DoubleEntryOnCA")
                    Return 1
                End If
            Else
                Dim dSave As [Delegate] = New AfterWOSave(AddressOf UpdateWOSave)
                ThreadUtil.FireAndForget(dSave, New Object() {dsSendMail})
            End If
            If ViewState("SucessStatus") = ApplicationSettings.WOStatusID.CA Then
                Dim dSave As [Delegate] = New AfterWOSave(AddressOf UpdateWOSave)
                ThreadUtil.FireAndForget(dSave, New Object() {dsSendMail})
            End If
        End If

    End Function
    'Private Sub custValidateReceiptNo_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles custValidateReceiptNo.ServerValidate
    '    If (txtReceiptNumber.Text.Trim <> "") Then
    '        Dim dsPONumber As DataSet
    '        dsPONumber = ws.WSContact.CheckUniqueness(txtReceiptNumber.Text.Trim, "ReceiptNumber", CInt(WOID))
    '        If (dsPONumber.Tables(0).Rows.Count > 0) Then
    '            args.IsValid = False
    '        Else
    '            args.IsValid = True
    '        End If
    '    Else
    '        args.IsValid = True
    '    End If

    'End Sub

    Private Function checkReceiptNo() As Boolean
        Dim Str As String = ""
        If (txtReceiptNumber.Text.Trim <> "") Then
            Dim dsPONumber As DataSet
            dsPONumber = ws.WSContact.CheckUniqueness(txtReceiptNumber.Text.Trim, "ReceiptNumber", CInt(WOID))
            If (dsPONumber.Tables(0).Rows.Count > 0) Then
                Str = "This Receipt number has already been used, Please use a different Receipt number."
                'lblError.Text = Str
                'divValidationMain.Style.Add("display", "block")
                divValidationVersionNo.Visible = True
                lblErrorVersionNo.Text = Str
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function

    Private Sub CallAutoMatch(ByVal AllWos As String)
        Dim WOLongDesc As String
        'If Request.Browser.Browser Then
        WOLongDesc = txtWOLongDesc.Text.Replace(Chr(13), "<BR>")
        'Else
        '    WOLongDesc = txtWOLongDesc.Text.Replace(Chr(10), "<BR>")
        'End If

        If (txtClientInstru.Text = "") Then
            'Use of Success Bit can be done in future
            Dim Success As Boolean = OrderWork.Process.OrderMatch(True, CompanyID, ContactID, AllWos,
                        ViewState("DateStart"), ViewState("DateEnd"), CInt(ViewState("EstimatedTime")), ViewState("Location"), ViewState("PPPrice"),
                       ViewState("WorkOrderId"), txtTitle.Text, False, WOLongDesc,
                        0, BizDivID, ViewState("WOCategory"), False,
                        "", "", CType(Date.Now, Date).ToString("dd/MM/yyyy"), ViewState("BuyerCompanyId"))

        End If

    End Sub
#End Region

#Region "PostcodeLookup"

    Public Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        'Initialize the Viewstates on Page Load only
        If Not IsPostBack Then
            ViewState("AttemptCountWOForm") = 0
            ViewState("PostcodeTrial1") = ""
        End If

        'If keyword is not blank, then allow user to lookup
        If txtCompanyPostalCode.Text.Trim <> "" Then

            'Lookup for address only if the postcode provided is different than previous one
            If ViewState("PostcodeTrial1") <> txtCompanyPostalCode.Text Then

                'Increment the Lookup attempt Count
                ViewState("AttemptCountWOForm") = ViewState("AttemptCountWOForm") + 1

                'Allow user to lookup only if attempts made are less than 2. On third attempt, show error message PostcodeLookupTrialsExpiry
                If ViewState("AttemptCountWOForm") < 5 Then
                    tblAddList.Visible = True
                    CommonFunctions.callGetAddressList(txtCompanyPostalCode.Text.Trim, lblErr, lstProperties, txtCompanyPostalCode)
                    ViewState("PostcodeTrial1") = txtCompanyPostalCode.Text
                Else
                    btnFind.Enabled = False
                    tblAddList.Visible = True
                    lstProperties.Visible = False
                    lblErr.Visible = True
                    lblErr.Text = ResourceMessageText.GetString("PostcodeLookupTrialsExpiry")
                End If
            End If
        Else
            tblAddList.Visible = True
            lstProperties.Visible = False
            lblErr.Visible = True
            lblErr.Text = ResourceMessageText.GetString("PostcodeNotAvailable")
        End If
    End Sub


    Private Sub lstProperties_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstProperties.SelectedIndexChanged

        Dim strSelectedItem As String = lstProperties.SelectedValue
        CommonFunctions.callPopulateAddress(lblErr, strSelectedItem, lstProperties, txtCompanyPostalCode, txtName, txtCompanyAddress, txtCity, txtCounty)

        If lblErr.Text = "" Then
            tblAddList.Visible = False
        Else
            tblAddList.Visible = True
        End If
    End Sub
#End Region


#Region "ClientQuestions"

    Public Sub hdnBtnShowHideMdlQuestions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnBtnShowHideMdlQuestions.Click
        divValidationClientQuestions.Visible = False
        lblValidationClientQuestions.Text = ""
        Session("ClientQAnsId") = Nothing
        Session("ClientQ") = Nothing
        Dim ds As New DataSet
        ds = ws.WSWorkOrder.populateWOFormProductDetails(CInt(hdnSelectedService.Value), CInt(hdnContactID.Value), 1, CInt(HttpContext.Current.Session("UserID")), "ServiceQuestion", "")
        If (ds.Tables(0).Rows.Count > 0) Then
            Dim dvContactsQA As New DataView(ds.Tables(0).Copy)
            Dim ClientQCount As Integer = 0
            If (dvContactsQA.Item(0)("Field1Label") <> "") Then
                ClientQCount = ClientQCount + 1
            End If
            If (dvContactsQA.Item(0)("Field2Label") <> "") Then
                ClientQCount = ClientQCount + 1
            End If
            If (dvContactsQA.Item(0)("Field3Label") <> "") Then
                ClientQCount = ClientQCount + 1
            End If
            If (dvContactsQA.Item(0)("Field4Label") <> "") Then
                ClientQCount = ClientQCount + 1
            End If
            If (dvContactsQA.Item(0)("Field5Label") <> "") Then
                ClientQCount = ClientQCount + 1
            End If
            If (dvContactsQA.Item(0)("Field6Label") <> "") Then
                ClientQCount = ClientQCount + 1
            End If
            If (dvContactsQA.Item(0)("Field7Label") <> "") Then
                ClientQCount = ClientQCount + 1
            End If
            If (dvContactsQA.Item(0)("Field8Label") <> "") Then
                ClientQCount = ClientQCount + 1
            End If
            If (dvContactsQA.Item(0)("Field9Label") <> "") Then
                ClientQCount = ClientQCount + 1
            End If
            If (dvContactsQA.Item(0)("Field10Label") <> "") Then
                ClientQCount = ClientQCount + 1
            End If
            If (ClientQCount <> 0) Then
                Session("ClientQ") = dvContactsQA
                PopulateClientQRepeater(dvContactsQA)
                mdlClientQuestions.Show()
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "js", "setPnlConfirmHeight();", True)
            Else
                mdlClientQuestions.Hide()

            End If
        Else
            mdlClientQuestions.Hide()

        End If
    End Sub

    Public Sub hdnClientQAnsSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnClientQAnsSubmit.Click
        If ValidatingClientQuestions() = "" Then
            'Save data in DB
            Dim xmlQA As String = GetQASettings()

            Dim StopBooking As Boolean = False
            Dim ProductId As String = "0"
            If Not IsNothing(rptClientQuestions) Then
                For Each row As RepeaterItem In rptClientQuestions.Items
                    Dim rptClientQuestionAnswers As Repeater = CType(row.FindControl("rptClientQuestionAnswers"), Repeater)
                    If Not IsNothing(rptClientQuestionAnswers) Then
                        For Each rowAns As RepeaterItem In rptClientQuestionAnswers.Items
                            If CType(rowAns.FindControl("radbtnAnswer"), RadioButton).Checked = True Then
                                If (CType(rowAns.FindControl("hdnQAnsStopBooking"), HtmlInputHidden).Value = True) Then
                                    StopBooking = True
                                End If
                                If (CType(rowAns.FindControl("hdnSelectedProduct"), HtmlInputHidden).Value <> "0" And ProductId = "0") Then
                                    ProductId = CType(rowAns.FindControl("hdnSelectedProduct"), HtmlInputHidden).Value
                                End If
                            End If
                            If CType(rowAns.FindControl("txtSingleAnswer"), TextBox).Text.Trim <> "" Then
                                If (CType(rowAns.FindControl("hdnQAnsStopBooking"), HtmlInputHidden).Value = True) Then
                                    StopBooking = True
                                End If
                                If (CType(rowAns.FindControl("hdnSelectedProduct"), HtmlInputHidden).Value <> "0" And ProductId = "0") Then
                                    ProductId = CType(rowAns.FindControl("hdnSelectedProduct"), HtmlInputHidden).Value
                                End If
                            End If
                        Next
                    End If
                Next
            End If

            Dim ds As DataSet
            ds = ws.WSWorkOrder.WOSubmitQuestionAnswers(xmlQA, CInt(Session("UserID")), CInt(ProductId))
            If (ds.Tables.Count > 0) Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    If (ds.Tables(0).Rows(0).Item("Success") = 1) Then
                        Session("ClientQAnsId") = ds.Tables(0).Rows(0).Item("ClientQAnsId")
                        divValidationClientQuestions.Visible = False
                        lblValidationClientQuestions.Text = ""
                        mdlClientQuestions.Hide()
                        UCAccreditations.Type = "ServiceAccreditations"
                        UCAccreditations.Mode = "ServiceAccreditations"
                        UCAccreditations.BizDivId = BizDivID
                        UCAccreditations.CommonID = hdnSelectedService.Value
                        UCAccreditations.PopulateSelectedGrid(True)
                        If (StopBooking = True) Then
                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "ValidationMsg", "getProductDescription(0,'');", True)
                        ElseIf (ProductId <> "0" And ProductId <> "") Then
                            If (ds.Tables(0).Rows(0).Item("IsHidden") = True) Then
                                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "ValidationMsg", "getProductDescription(" & ProductId & ",'" & ds.Tables(0).Rows(0).Item("WOTitle").ToString & "');", True)
                            Else
                                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "ValidationMsg", "getProductDescription(" & ProductId & ",'');", True)
                            End If
                        End If

                    Else
                        divValidationClientQuestions.Visible = True
                        lblValidationClientQuestions.Text = ResourceMessageText.GetString("DBUpdateFail")
                        mdlClientQuestions.Show()
                    End If
                End If
            End If
        Else
            mdlClientQuestions.Show()
        End If
    End Sub
    Public Sub hdnClientQAnsCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnClientQAnsCancel.Click
        divValidationClientQuestions.Visible = False
        lblValidationClientQuestions.Text = ""
        Session("ClientQAnsId") = Nothing
        Session("ClientQ") = Nothing
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "ValidationMsg", "getProductDescription(0,'');", True)
        mdlClientQuestions.Hide()
    End Sub
    Public Function GetQASettings() As String
        'pick this code start
        Dim dsQA As New OWContactsQASettings()

        Dim nrowClientQALabels As OWContactsQASettings.tblContactsQASettingsRow = dsQA.tblContactsQASettings.NewRow
        With nrowClientQALabels
            .CompanyID = hdnContactID.Value

            .Field1Label = ""
            .Field1Value = ""
            .Field1Enabled = False
            .IsField1LabelMandatory = False

            .Field2Label = ""
            .Field2Value = ""
            .Field2Enabled = False
            .IsField2LabelMandatory = False

            .Field3Label = ""
            .Field3Value = ""
            .Field3Enabled = False
            .IsField3LabelMandatory = False

            .Field4Label = ""
            .Field4Value = ""
            .Field4Enabled = False
            .IsField4LabelMandatory = False

            .Field5Label = ""
            .Field5Value = ""
            .Field5Enabled = False
            .IsField5LabelMandatory = False

            .Field6Label = ""
            .Field6Value = ""
            .Field6Enabled = False
            .IsField6LabelMandatory = False

            .Field7Label = ""
            .Field7Value = ""
            .Field7Enabled = False
            .IsField7LabelMandatory = False

            .Field8Label = ""
            .Field8Value = ""
            .Field8Enabled = False
            .IsField8LabelMandatory = False

            .Field9Label = ""
            .Field9Value = ""
            .Field9Enabled = False
            .IsField9LabelMandatory = False

            .Field10Label = ""
            .Field10Value = ""
            .Field10Enabled = False
            .IsField10LabelMandatory = False

            Dim i As Integer = 0
            For Each row As RepeaterItem In rptClientQuestions.Items
                i = i + 1
                Select Case i
                    Case 1
                        .Field1Label = CType(row.FindControl("lblClientQ"), Label).Text.Trim.ToString
                        .Field1Value = GetAns(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                        .Field1Enabled = True
                        .IsField1LabelMandatory = True
                    Case 2
                        .Field2Label = CType(row.FindControl("lblClientQ"), Label).Text.Trim.ToString
                        .Field2Value = GetAns(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                        .Field2Enabled = True
                        .IsField2LabelMandatory = True
                    Case 3
                        .Field3Label = CType(row.FindControl("lblClientQ"), Label).Text.Trim.ToString
                        .Field3Value = GetAns(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                        .Field3Enabled = True
                        .IsField3LabelMandatory = True
                    Case 4
                        .Field4Label = CType(row.FindControl("lblClientQ"), Label).Text.Trim.ToString
                        .Field4Value = GetAns(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                        .Field4Enabled = True
                        .IsField4LabelMandatory = True
                    Case 5
                        .Field5Label = CType(row.FindControl("lblClientQ"), Label).Text.Trim.ToString
                        .Field5Value = GetAns(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                        .Field5Enabled = True
                        .IsField5LabelMandatory = True
                    Case 6
                        .Field6Label = CType(row.FindControl("lblClientQ"), Label).Text.Trim.ToString
                        .Field6Value = GetAns(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                        .Field6Enabled = True
                        .IsField6LabelMandatory = True
                    Case 7
                        .Field7Label = CType(row.FindControl("lblClientQ"), Label).Text.Trim.ToString
                        .Field7Value = GetAns(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                        .Field7Enabled = True
                        .IsField7LabelMandatory = True
                    Case 8
                        .Field8Label = CType(row.FindControl("lblClientQ"), Label).Text.Trim.ToString
                        .Field8Value = GetAns(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                        .Field8Enabled = True
                        .IsField8LabelMandatory = True
                    Case 9
                        .Field9Label = CType(row.FindControl("lblClientQ"), Label).Text.Trim.ToString
                        .Field9Value = GetAns(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                        .Field9Enabled = True
                        .IsField9LabelMandatory = True
                    Case 10
                        .Field10Label = CType(row.FindControl("lblClientQ"), Label).Text.Trim.ToString
                        .Field10Value = GetAns(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                        .Field10Enabled = True
                        .IsField10LabelMandatory = True
                End Select
            Next
            .Type = "WOSubmitQuestion"
            .WOID = 0
        End With
        dsQA.tblContactsQASettings.Rows.Add(nrowClientQALabels)


        Dim xmlQA As String = dsQA.GetXml
        xmlQA = xmlQA.Remove(xmlQA.IndexOf("xmlns"), xmlQA.IndexOf(".xsd") - xmlQA.IndexOf("xmlns") + 5)
        Return xmlQA
    End Function
    Public Function GetAns(ByVal rptClientQuestionAnswers As Repeater)
        Dim QueAns As String = ""
        If Not IsNothing(rptClientQuestionAnswers) Then
            For Each rowAns As RepeaterItem In rptClientQuestionAnswers.Items
                Dim anscount As Integer = CType(rowAns.FindControl("hdnQAnsTotalAnsCount"), System.Web.UI.HtmlControls.HtmlInputHidden).Value
                If CType(rowAns.FindControl("radbtnAnswer"), RadioButton).Checked = True Then
                    QueAns = CType(rowAns.FindControl("radbtnAnswer"), RadioButton).Text.ToString
                End If
                If anscount = 1 Then
                    QueAns = CType(rowAns.FindControl("txtSingleAnswer"), TextBox).Text.ToString
                End If
            Next
        End If
        Return QueAns
    End Function
    Private Function ValidatingClientQuestions() As String
        Dim flagPerQ As Boolean = True
        Dim errmsg As String = ""
        Dim errmsgPerQ As String = ""
        Dim i As Integer = 0

        If Not IsNothing(rptClientQuestions) Then
            For Each row As RepeaterItem In rptClientQuestions.Items
                i = i + 1
                errmsgPerQ = ""
                flagPerQ = True
                Dim rptClientQuestionAnswers As Repeater = CType(row.FindControl("rptClientQuestionAnswers"), Repeater)
                If Not IsNothing(rptClientQuestionAnswers) Then
                    For Each rowAns As RepeaterItem In rptClientQuestionAnswers.Items
                        Dim anscount As Integer = CType(rowAns.FindControl("hdnQAnsTotalAnsCount"), System.Web.UI.HtmlControls.HtmlInputHidden).Value
                        If CType(rowAns.FindControl("radbtnAnswer"), RadioButton).Checked = True Then
                            flagPerQ = False
                            errmsgPerQ = ""
                        ElseIf anscount = 1 Then
                            If CType(rowAns.FindControl("txtSingleAnswer"), TextBox).Text.Trim <> "" Then
                                flagPerQ = False
                                errmsgPerQ = ""
                            Else
                                If errmsg = "" Then
                                    If flagPerQ = True Then
                                        errmsgPerQ = "- Please enter an answer to question " & i

                                        flagPerQ = False
                                    End If
                                Else
                                    If flagPerQ = True Then
                                        errmsgPerQ = errmsgPerQ & "<br>- Please enter an answer to question " & i
                                        flagPerQ = False
                                    End If
                                End If
                            End If
                        Else
                            If errmsg = "" Then
                                If flagPerQ = True Then
                                    errmsgPerQ = "- Please select an answer to question " & i

                                    flagPerQ = False
                                End If
                            Else
                                If flagPerQ = True Then
                                    errmsgPerQ = errmsgPerQ & "<br>- Please select an answer to question " & i
                                    flagPerQ = False
                                End If
                            End If
                        End If
                    Next
                End If
                errmsg = errmsg & errmsgPerQ
            Next
        End If

        If errmsg <> "" Then
            divValidationClientQuestions.Visible = True
            lblValidationClientQuestions.Text = errmsg.ToString
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "js", "setPnlConfirmHeight();", True)
        Else
            divValidationClientQuestions.Visible = False
            lblValidationClientQuestions.Text = ""
        End If
        Return errmsg
    End Function

    Private Sub PopulateClientQRepeater(ByVal dvClientQ As DataView)
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim dCol As New DataColumn
        Dim dColClientQ As New DataColumn

        dCol.DataType = Type.[GetType]("System.String")
        dCol.ColumnName = "QuestionNo"
        dt.Columns.Add(dCol)

        dColClientQ.DataType = Type.[GetType]("System.String")
        dColClientQ.ColumnName = "ClientQ"
        dt.Columns.Add(dColClientQ)

        Dim i As Integer
        Dim j As Integer = 0
        For i = 0 To 9
            If (dvClientQ.Item(0)("Field" & i + 1 & "Label") <> "") Then
                Dim drow As DataRow
                drow = dt.NewRow()
                drow("QuestionNo") = (j + 1).ToString
                drow("ClientQ") = dvClientQ.Item(0)("Field" & i + 1 & "Label")
                j = j + 1
                dt.Rows.Add(drow)
                dt.AcceptChanges()
            End If
        Next i

        ds.Tables.Add(dt.Copy)
        ds.AcceptChanges()

        rptClientQuestions.DataSource = ds
        rptClientQuestions.DataBind()

    End Sub
    Private Sub rptClientQuestions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptClientQuestions.ItemDataBound

        Dim rptClientQuestionAnswers As Repeater = CType(e.Item.FindControl("rptClientQuestionAnswers"), Repeater)
        Dim ddQAnsCount As DropDownList = CType(e.Item.FindControl("ddQAnsCount"), DropDownList)
        Dim hdnQuestionNo As HtmlInputHidden = CType(e.Item.FindControl("hdnQuestionNo"), HtmlInputHidden)
        Dim AnsCount As Integer

        If (Not Session("ClientQ") Is Nothing) Then
            Dim dvClientQ As DataView = CType(Session("ClientQ"), DataView)
            If (CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & CInt(hdnQuestionNo.Value) & "Value"), "TotalAnsCount") <> "") Then
                AnsCount = CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & CInt(hdnQuestionNo.Value) & "Value"), "TotalAnsCount")
            End If
            PopulateClientQAnsRepeater(AnsCount, rptClientQuestionAnswers, dvClientQ, CInt(hdnQuestionNo.Value))
        End If

    End Sub
    Private Sub PopulateClientQAnsRepeater(ByVal totalAns As Integer, ByVal rptClientQuestionAnswers As Repeater, ByVal dvClientQ As DataView, ByVal QuestionNo As Integer)
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim dCol As New DataColumn
        Dim dColQNo As New DataColumn
        Dim dColQTotalAnsCount As New DataColumn
        Dim dColAnswer As New DataColumn
        Dim dColMessage As New DataColumn
        Dim dStopBooking As New DataColumn
        Dim dColProduct As New DataColumn
        dCol.DataType = Type.[GetType]("System.String")
        dCol.ColumnName = "AnsNo"
        dt.Columns.Add(dCol)
        dColAnswer.DataType = Type.[GetType]("System.String")
        dColAnswer.ColumnName = "Answer"
        dt.Columns.Add(dColAnswer)
        dStopBooking.DataType = Type.[GetType]("System.Boolean")
        dStopBooking.ColumnName = "StopBooking"
        dt.Columns.Add(dStopBooking)
        dColMessage.DataType = Type.[GetType]("System.String")
        dColMessage.ColumnName = "Message"
        dt.Columns.Add(dColMessage)
        dColQNo.DataType = Type.[GetType]("System.String")
        dColQNo.ColumnName = "QuestionNo"
        dt.Columns.Add(dColQNo)
        dColQTotalAnsCount.DataType = Type.[GetType]("System.String")
        dColQTotalAnsCount.ColumnName = "TotalAnsCount"
        dt.Columns.Add(dColQTotalAnsCount)
        dColProduct.DataType = Type.[GetType]("System.String")
        dColProduct.ColumnName = "Product"
        dt.Columns.Add(dColProduct)
        Dim i As Integer

        For i = 0 To totalAns - 1
            Dim drow As DataRow
            drow = dt.NewRow()
            drow("AnsNo") = (i + 1).ToString
            If (Not dvClientQ Is Nothing) Then
                If (dvClientQ.Item(0)("Field" & QuestionNo & "Value") <> "") Then
                    If (CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & QuestionNo & "Value"), "Ans" & i + 1) <> "") Then
                        drow("Answer") = CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & QuestionNo & "Value"), "Ans" & i + 1)
                        drow("Message") = CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & QuestionNo & "Value"), "Message" & i + 1)
                        drow("StopBooking") = CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & QuestionNo & "Value"), "IsStopBooking" & i + 1)
                        drow("TotalAnsCount") = CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & QuestionNo & "Value"), "TotalAnsCount")
                        drow("QuestionNo") = QuestionNo.ToString
                        drow("Product") = CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & QuestionNo & "Value"), "Product" & i + 1)
                        If (drow("Product") = "") Then
                            drow("Product") = "0"
                        End If
                    Else
                        drow("Answer") = ""
                        drow("Message") = ""
                        'Task - OA-389 : OA - Various Changes in Question Builder if anscount =1 show free text box
                        drow("TotalAnsCount") = 1
                        drow("StopBooking") = False
                        drow("QuestionNo") = QuestionNo.ToString
                        drow("Product") = "0"
                    End If
                Else
                    drow("Answer") = ""
                    drow("Message") = ""
                    drow("TotalAnsCount") = 0
                    drow("StopBooking") = False
                    drow("QuestionNo") = QuestionNo.ToString
                    drow("Product") = "0"
                End If

            Else
                drow("Answer") = ""
                drow("Message") = ""
                drow("TotalAnsCount") = 0
                drow("StopBooking") = False
                drow("QuestionNo") = QuestionNo.ToString
                drow("Product") = "0"
            End If
            dt.Rows.Add(drow)
            dt.AcceptChanges()
        Next i
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        rptClientQuestionAnswers.DataSource = ds
        rptClientQuestionAnswers.DataBind()

    End Sub

#End Region
End Class