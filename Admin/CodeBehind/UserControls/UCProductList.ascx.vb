Public Partial Class UCProductList
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs
#Region "Declaration"
    '''<summary>
    '''UCSearchContact2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCSearchContact2 As UCSearchContact
    '''<summary>
    '''UCSearchContact1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Public WithEvents pnlMoveTo As Panel
    Public WithEvents hdnButtonMoveTo As Button

    Public WithEvents pnlSwap As Panel
    Public WithEvents hdnButtonSwap As Button

#End Region

#Region "Properties"
    ''' <summary>
    ''' Gets or Sets CompanyID 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CompID() As Integer
        Get
            If Not IsNothing(Request("CompanyID")) Then
                Return Session("ContactCompanyID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            Session("ContactCompanyID") = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets Page Name
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PageName() As String
        Get
            If Not IsNothing(ViewState("PageName")) Then
                Return ViewState("PageName")
            Else
                Return "CompanyProfile"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("PageName") = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or Sets ProductIDs
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SelectedProductIds() As String
        Get
            If Not IsNothing(ViewState("SelectedProductIds")) Then
                Return ViewState("SelectedProductIds")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("SelectedProductIds") = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If PageName <> "CompanyProfile" Then
                tblAddService.Visible = True
                'tblUpdateSeq.Visible = True
                If Not IsNothing(Request.QueryString("sender")) Then
                    If Request.QueryString("sender") <> "productform" Then
                        Session("ContactCompanyID") = 0
                        tblAddService.Visible = False
                        'tblUpdateSeq.Visible = False
                    End If
                Else
                    Session("ContactCompanyID") = 0
                    tblAddService.Visible = False
                    'tblUpdateSeq.Visible = False
                End If
                UCSearchContact1.BizDivID = Session("BizDivId")
                UCSearchContact1.Filter = "AccountsServices"
                UCSearchContact1.populateContact()
                SetGridSettings()
            Else
                PopulateGrid()
            End If
            gvProductList.Sort("Sequence", SortDirection.Ascending)
        End If
    End Sub

    ''' <summary>
    ''' On Click of View
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'ViewState("dsSequence") = Nothing
        Page.Validate()
        If Page.IsValid Then
            If UCSearchContact1.ddlContact.SelectedValue = "" Then
                CompID = 0
                lblError.Visible = False
            Else
                CompID = UCSearchContact1.ddlContact.SelectedValue
                pnlConfirmationSummary.Visible = False
                PopulateGrid()
                lblError.Visible = False
            End If
        End If
    End Sub

    ''' <summary>
    ''' Populating Grid
    ''' </summary>
    ''' <param name="PageIndex"></param>
    ''' <remarks></remarks>
    Public Sub PopulateGrid(Optional ByVal PageIndex As Integer = 0)
        SetGridSettings()
        gvProductList.Sort("Sequence", SortDirection.Ascending)
        gvProductList.DataBind()
        'If Not IsNothing(Session("ContactCompanyID")) And UCSearchContact1.ddlContact.SelectedValue = "" Then
        '    UCSearchContact1.ddlContact.SelectedValue = Session("ContactCompanyID")
        'End If
    End Sub

    ''' <summary>
    ''' Setting Grid Columns
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SetGridSettings()
        If PageName <> "CompanyProfile" Then
            gvProductList.Columns.Item(0).Visible = True 'Multiple Select Check Box            
            pnlSearchContact.Visible = True
            tblBack.Visible = False
            Dim CompanyID As Integer
            CompanyID = 0
            If Not IsNothing(Session("ContactCompanyID")) Then
                If Request("CompanyID") <> 0 Then
                    UCSearchContact1.SelectedContact = Request("CompanyID")
                    CompanyID = Request("CompanyID")
                    tblDuplicate.Visible = True
                    tblAddService.Visible = True
                    'tblUpdateSeq.Visible = True
                    chkbxHideDeleted.Visible = True
                ElseIf Session("ContactCompanyID") <> 0 Then
                    UCSearchContact1.SelectedContact = Session("ContactCompanyID")
                    CompanyID = Session("ContactCompanyID")
                    tblDuplicate.Visible = True
                    tblAddService.Visible = True
                    'tblUpdateSeq.Visible = True
                    chkbxHideDeleted.Visible = True
                Else
                    tblAddService.Visible = False
                    'tblUpdateSeq.Visible = False
                    chkbxHideDeleted.Visible = False
                End If
            Else
                tblAddService.Visible = False
                'tblUpdateSeq.Visible = False
                chkbxHideDeleted.Visible = False
            End If
            If Not IsNothing(Request("CompanyID")) Then
                btnAddLocation.HRef = "~/AdminProduct.aspx?mode=add&pagename=" & PageName & "&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            Else
                btnAddLocation.HRef = "~/AdminProduct.aspx?mode=add&pagename=" & PageName & "&CompanyID=" & CompanyID & "&bizDivId=1"
            End If
        Else
            If Not IsNothing(Request("CompanyID")) Then
                btnAddLocation.HRef = "~/AdminProduct.aspx?mode=add&pagename=" & PageName & "&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            Else
                btnAddLocation.HRef = "~/AdminProduct.aspx?mode=add&pagename=" & PageName & "&CompanyID=" & UCSearchContact1.ddlContact.SelectedValue & "&bizDivId=1"
            End If
            gvProductList.Columns.Item(0).Visible = False 'Multiple Select Check Box
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' To return dataset for location listing
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                    ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim siteType As String = "site"
        Dim contactID As Integer = HttpContext.Current.Items("ContactID")
        Dim recordCount As Integer
        Dim ds As DataSet

        Select Case ApplicationSettings.SiteType
            Case ApplicationSettings.siteTypes.admin
                siteType = "admin"
            Case ApplicationSettings.siteTypes.site
                siteType = "site"
        End Select

        Dim BizDivID As Integer
        BizDivID = Session("BizDivID")
        'If (ViewState("dsSequence") Is Nothing) Then

        ' Since there is issue with accessing the page variable, fetching data directly without caching.
        If Request("CompanyID") <> 0 Then
            Dim CompanyID As Integer
            CompanyID = Request("CompanyID")
            ds = ws.WSWorkOrder.GetProductListing(CompanyID, BizDivID, sortExpression, startRowIndex, 0, recordCount, ViewState("ShowDeleted"))
            HttpContext.Current.Items("rowCount") = recordCount
            If (ds.Tables(0).Rows.Count > 0) Then
                ViewState("SpecificCompany") = ds.Tables(0).Rows(0).Item("SpecificCompany")
            Else
                ViewState("SpecificCompany") = 0
            End If
            'ViewState("dsSequence") = ds
        Else
            If Not IsNothing(Session("ContactCompanyID")) Then
                If Session("ContactCompanyID") <> 0 Then
                    Dim CompanyID As Integer
                    CompanyID = Session("ContactCompanyID")
                    ds = ws.WSWorkOrder.GetProductListing(CompanyID, BizDivID, sortExpression, startRowIndex, 0, recordCount, ViewState("ShowDeleted"))
                    HttpContext.Current.Items("rowCount") = recordCount
                    If (ds.Tables(0).Rows.Count > 0) Then
                        ViewState("SpecificCompany") = ds.Tables(0).Rows(0).Item("SpecificCompany")
                    Else
                        ViewState("SpecificCompany") = 0
                    End If
                Else
                    HttpContext.Current.Items("rowCount") = 0
                End If
            Else
                HttpContext.Current.Items("rowCount") = 0
            End If
        End If
        'Else
        'ds = CType(ViewState("dsSequence"), DataSet)
        'HttpContext.Current.Items("rowCount") = ViewState("rowCount")
        'End If
        Return ds
    End Function

    'Private Sub gvProductList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvProductList.RowCommand
    '    If (e.CommandName = "MoveUp") Then 'Move Up
    '        Dim hdnRowNum As Integer = CInt(e.CommandArgument)
    '        Dim ds As DataSet = CType(ViewState("dsSequence"), DataSet)

    '        ds.Tables(0).DefaultView.RowFilter = "RowNum=" & hdnRowNum
    '        Dim dr1 As DataRow = ds.Tables(0).DefaultView.Item(0).Row

    '        ds.Tables(0).DefaultView.RowFilter = "RowNum=" & hdnRowNum - 1
    '        Dim dr2 As DataRow = ds.Tables(0).DefaultView.Item(0).Row

    '        dr1("RowNum") = hdnRowNum - 1
    '        dr2("RowNum") = hdnRowNum

    '        dr1("Sequence") = hdnRowNum - 1
    '        dr2("Sequence") = hdnRowNum


    '        ds.Tables(0).DefaultView.RowFilter = ""

    '        Dim dv As DataView = ds.Tables(0).DefaultView
    '        dv.Sort = "RowNum"

    '        Dim dsTemp = New DataSet()
    '        Dim dtt As DataTable
    '        dtt = dv.ToTable.Copy
    '        dsTemp.Tables.Add(dtt)
    '        dsTemp.AcceptChanges()

    '        ViewState("dsSequence") = dsTemp
    '        gvProductList.DataBind()
    '    ElseIf (e.CommandName = "MoveDown") Then 'Move Down
    '        Dim hdnRowNum As Integer = CInt(e.CommandArgument)
    '        Dim ds As DataSet = CType(ViewState("dsSequence"), DataSet)

    '        'Dim dv as DataView = 
    '        ds.Tables(0).DefaultView.RowFilter = "RowNum=" & hdnRowNum
    '        Dim dr1 As DataRow = ds.Tables(0).DefaultView.Item(0).Row

    '        ds.Tables(0).DefaultView.RowFilter = "RowNum=" & hdnRowNum + 1
    '        Dim dr2 As DataRow = ds.Tables(0).DefaultView.Item(0).Row

    '        dr1("RowNum") = hdnRowNum + 1
    '        dr2("RowNum") = hdnRowNum

    '        dr1("Sequence") = hdnRowNum + 1
    '        dr2("Sequence") = hdnRowNum

    '        ds.Tables(0).DefaultView.RowFilter = ""



    '        Dim dv As DataView = ds.Tables(0).DefaultView
    '        dv.Sort = "RowNum"

    '        Dim dsTemp = New DataSet()
    '        Dim dtt As DataTable
    '        dtt = dv.ToTable.Copy
    '        dsTemp.Tables.Add(dtt)
    '        dsTemp.AcceptChanges()

    '        ViewState("dsSequence") = dsTemp
    '        gvProductList.DataBind()
    '    End If
    'End Sub

    Private Sub gvProductList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvProductList.RowCommand
        Dim CompanyID As Integer
        CompanyID = Request("CompanyID")
        If (e.CommandName = "MoveUp") Then 'Move Up
            Dim Sequence As Integer = CInt(e.CommandArgument)
            If (Sequence <> 0) Then
                ws.WSWorkOrder.UpdateServiceSequence("MoveUp", Sequence, CompanyID)
                gvProductList.DataBind()
            End If
        ElseIf (e.CommandName = "MoveDown") Then 'Move Down
            Dim Sequence As Integer = CInt(e.CommandArgument)
            If (Sequence <> 0) Then
                ws.WSWorkOrder.UpdateServiceSequence("MoveDown", Sequence, CompanyID)
                gvProductList.DataBind()
            End If
        ElseIf (e.CommandName = "MoveTo") Then 'Move To
            Dim Sequence As Integer = CInt(e.CommandArgument)
            If (Sequence <> 0) Then
                ViewState("SelectedSeqToMove") = Sequence
                'ViewState.Add("SelectedSeqToMove", Sequence)
                lblCurrentSeq.Text = ViewState("SelectedSeqToMove").ToString
                CType(FindControl("mdlMoveTo"), AjaxControlToolkit.ModalPopupExtender).Show()
            End If
        ElseIf (e.CommandName = "Swap") Then 'Swap
            Dim Sequence As Integer = CInt(e.CommandArgument)
            If (Sequence <> 0) Then
                ViewState("SelectedSeqToMove") = Sequence
                'ViewState.Add("SelectedSeqToMove", Sequence)
                lblSwapCurrentSeq.Text = ViewState("SelectedSeqToMove").ToString
                CType(FindControl("mdlSwap"), AjaxControlToolkit.ModalPopupExtender).Show()
            End If
        End If

    End Sub

    'Private Sub gvProductList_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvProductList.Sorting
    '    ViewState("dsSequence") = Nothing
    'End Sub


    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvProductList.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvProductList, e.Row, Me)
        End If
        If (ViewState("SpecificCompany") = 1) Then
            gvProductList.Columns.Item(1).Visible = False 'Date Created
            gvProductList.Columns.Item(9).Visible = False 'Sequence
            gvProductList.Columns.Item(10).Visible = False 'MoveTo
            gvProductList.Columns.Item(11).Visible = False 'Swap
            gvProductList.Columns.Item(12).Visible = False 'MoveDown
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvProductList.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvProductList.DataBind()
    End Sub

    ''' <summary>
    ''' GridView event handler to call MakeGridViewHeaderClickable method
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvProductList.RowDataBound
        MakeGridViewHeaderClickable(gvProductList, e.Row)
    End Sub

    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    ''' <summary>
    ''' Handles show hide of Deleted Products 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub chkbxHideDeleted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkbxHideDeleted.CheckedChanged
        If chkbxHideDeleted.Checked = True Then
            ViewState("ShowDeleted") = "0"
        Else
            ViewState("ShowDeleted") = "1"
        End If
        gvProductList.DataBind()
    End Sub

    ''' <summary>
    ''' Back to company profile which will redirect to company profile only visible when the page is accessed via company profile
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        If (Session(Request("companyid") & "_ContactClassID")) <> ApplicationSettings.RoleClientID Then
            Response.Redirect("CompanyProfile.aspx?contactid=" & HttpContext.Current.Items("ContactID") & "&companyid=" & Request("CompanyID") & "&bizdivid=" & Request("bizDivId") & "&contactType=" & Session(Request("CompanyID") & "_ContactType") & "&classid=" & Session(Request("CompanyID") & "_ContactClassID"))
        Else
            Response.Redirect("CompanyProfileBuyer.aspx?contactid=" & HttpContext.Current.Items("ContactID") & "&companyid=" & Request("CompanyID") & "&bizdivid=" & Request("bizDivId") & "&contactType=" & Session(Request("CompanyID") & "_ContactType") & "&classid=" & Session(Request("CompanyID") & "_ContactClassID"))
        End If
    End Sub

    ''' <summary>
    ''' Method to get Selected ProductIDs
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetSelectedWOIDs()
        Dim selectedIds As String = ""
        For Each row As GridViewRow In gvProductList.Rows
            Dim chkBox As CheckBox = CType(row.FindControl("Check"), CheckBox)
            If Not (chkBox Is Nothing) Then
                If chkBox.Checked Then
                    If selectedIds = "" Then
                        selectedIds = CType(row.FindControl("hdnWOIDs"), HtmlInputHidden).Value
                    Else
                        selectedIds = selectedIds & "," & CType(row.FindControl("hdnWOIDs"), HtmlInputHidden).Value
                    End If
                End If
            End If
        Next
        'Assign Selected to UC Property.
        SelectedProductIds = selectedIds
    End Sub

    ''' <summary>
    ''' Handles click of Duplicate workorders
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnDuplicate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuplicate.Click
        If Not IsNothing(Session("ContactCompanyID")) Then
            GetSelectedWOIDs()
            If SelectedProductIds.Length > 0 Then
                pnlSearchContact.Visible = False
                pnlListing.Visible = False
                pnlConfirmationSummary.Visible = True

                Dim ds As New DataSet
                ds = ws.WSWorkOrder.GetProductListing(Session("ContactCompanyID"), Session("BizDivID"), "ProductID", 0, 1000, 1, 0)
                Dim dvProductFiltered As DataView
                dvProductFiltered = ds.Tables(0).DefaultView
                dvProductFiltered.RowFilter = "ProductID in (" & SelectedProductIds & ")"
                dlValidWO.DataSource = dvProductFiltered.ToTable
                dlValidWO.DataBind()
                UCSearchContact2.BizDivID = Session("BizDivId")
                UCSearchContact2.Filter = "AccountsServices"
                UCSearchContact2.populateContact()

            Else
                pnlListing.Visible = True
                pnlConfirmationSummary.Visible = False
                lblError.Visible = True
            End If
        Else
            pnlListing.Visible = True
            pnlConfirmationSummary.Visible = False
            lblError.Visible = True
        End If
    End Sub

    'Private Sub btnUpdateSeq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateSeq.Click
    '    If Not IsNothing(ViewState("dsSequence")) Then
    '        Dim ds As DataSet = CType(ViewState("dsSequence"), DataSet)
    '        Dim dv As DataView = ds.Tables(0).DefaultView
    '        Dim dsTemp = New DataSet()
    '        Dim dtt As DataTable
    '        dtt = dv.ToTable.Copy
    '        dsTemp.Tables.Add(dtt)
    '        dsTemp.AcceptChanges()

    '        ws.WSWorkOrder.UpdateServiceSequence(dsTemp.GetXml())
    '        ViewState("dsSequence") = Nothing
    '        gvProductList.DataBind()
    '    End If

    'End Sub
    ''' <summary>
    ''' Confirmation button click While copying product
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkConfirm.Click
        If UCSearchContact2.ddlContact.SelectedValue = "" Then
            lblConfError.Visible = True
        Else
            If CompID = UCSearchContact2.ddlContact.SelectedValue Then
                lblConfError.Text = "Please select a different company as the company selected has already same services"
                lblConfError.Visible = True
            Else
                Dim ds As New DataSet
                ds = ws.WSWorkOrder.CopyProduct(SelectedProductIds, UCSearchContact2.ddlContact.SelectedValue)
                If ds.Tables(0).Rows(0)("Success") = "1" Then
                    pnlListing.Visible = True
                    PopulateGrid()
                    pnlConfirmationSummary.Visible = False
                    Session("ContactCompanyID") = UCSearchContact2.ddlContact.SelectedValue
                    UCSearchContact1.ddlContact.SelectedValue = UCSearchContact2.ddlContact.SelectedValue
                    PopulateGrid()
                Else
                    pnlListing.Visible = True
                    pnlConfirmationSummary.Visible = False
                    lblError.Text = "An error has occurred, Please try after some time."
                    lblError.Visible = True
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Returns Page name for creating link to Product Form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCurrentPageName() As String
        Dim sPath As String = System.Web.HttpContext.Current.Request.Url.AbsolutePath
        Dim oInfo As New System.IO.FileInfo(sPath)
        Dim sRet As String = oInfo.Name
        Return sRet
    End Function

    ''' <summary>
    ''' Handles Cancel button click on confirmation summary
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click
        pnlSearchContact.Visible = True
        pnlListing.Visible = True
        pnlConfirmationSummary.Visible = False
    End Sub
    Public Sub MoveTo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButtonMoveTo.Click
        Dim CompanyID As Integer
        CompanyID = Request("CompanyID")
        Dim NewSeq As Integer = CInt(txtNewSeq.Text.Trim)
        Dim Status As DataSet = CommonFunctions.MoveSwapSequence(ViewState("SelectedSeqToMove"), NewSeq, CompanyID, "MoveTo")
        If (Status.Tables(0).Rows(0)("Status") = 1) Then
            txtNewSeq.Text = ""
            gvProductList.DataBind()
        Else
            divModalParent.Style.Add("display", "none")
            progressBar.Style.Add("display", "none")
            divlblMsg.Style.Add("display", "block")
            CType(FindControl("mdlMoveTo"), AjaxControlToolkit.ModalPopupExtender).Show()
        End If

    End Sub

    Public Sub Swap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButtonSwap.Click
        Dim CompanyID As Integer
        CompanyID = Request("CompanyID")
        Dim NewSeq As Integer = CInt(txtSwapNewSeq.Text.Trim)
        Dim Status As DataSet = CommonFunctions.MoveSwapSequence(ViewState("SelectedSeqToMove"), NewSeq, CompanyID, "Swap")
        If (Status.Tables(0).Rows(0)("Status") = 1) Then
            txtSwapNewSeq.Text = ""
            gvProductList.DataBind()
        Else
            divSwapModalParent.Style.Add("display", "none")
            SwapProgressBar.Style.Add("display", "none")
            divSwaplblMsg.Style.Add("display", "block")
            CType(FindControl("mdlSwap"), AjaxControlToolkit.ModalPopupExtender).Show()
        End If

    End Sub

    Public Sub OK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ancOk.ServerClick
        divModalParent.Style.Add("display", "block")
        progressBar.Style.Add("display", "none")
        divlblMsg.Style.Add("display", "none")
        txtNewSeq.Text = ""
    End Sub
    Public Sub SwapOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ancSwapOk.ServerClick
        divSwapModalParent.Style.Add("display", "block")
        SwapProgressBar.Style.Add("display", "none")
        divSwaplblMsg.Style.Add("display", "none")
        txtSwapNewSeq.Text = ""
    End Sub
    Public Function GetLinks(ByVal ProductID As String, ByVal PageName As String, ByVal CompanyID As String, ByVal BizDivID As String, ByVal SpecificCompany As Integer, ByVal ProductName As String) As String
        Dim link As String = ""
        If (ProductName = "") Then
            If (SpecificCompany = 1) Then
                link &= "<a class='footerTxtSelected ' href='AdminProductSpecific.aspx?mode=edit&productid=" & ProductID & "&pagename=" & PageName & "&CompanyID=" & CompanyID & "&BizDivID=" & BizDivID & "'><img src='Images/Icons/Icon-Pencil.gif' border='0'></a>"
            Else
                link &= "<a class='footerTxtSelected ' href='AdminProduct.aspx?mode=edit&productid=" & ProductID & "&pagename=" & PageName & "&CompanyID=" & CompanyID & "&BizDivID=" & BizDivID & "'><img src='Images/Icons/Icon-Pencil.gif' border='0'></a>"
            End If
        Else
            If (SpecificCompany = 1) Then
                link &= "<a class='footerTxtSelected ' href='AdminProductSpecific.aspx?mode=edit&productid=" & ProductID & "&pagename=" & PageName & "&CompanyID=" & CompanyID & "&BizDivID=" & BizDivID & "'>" & ProductName & "</a>"
            Else
                link &= "<a class='footerTxtSelected ' href='AdminProduct.aspx?mode=edit&productid=" & ProductID & "&pagename=" & PageName & "&CompanyID=" & CompanyID & "&BizDivID=" & BizDivID & "'>" & ProductName & "</a>"
            End If
        End If
        Return link

    End Function

End Class