Public Partial Class UCMSViewCreditNoteUK
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim ds As DataSet = ws.WSFinance.MS_GetCreditNoteDetails(Request("BizDivId"), Request("invoiceNo"))
            Dim invoicedateCreated As Date
            If ds.Tables("tblOWAddress").Rows.Count > 0 Then
                'Company Name
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("CompanyName"))) Then
                    lblCompanyName.Text = ds.Tables("tblOWAddress").Rows(0)("CompanyName")
                End If
                'Address
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Address"))) Then
                    lblAddress.Text = ds.Tables("tblOWAddress").Rows(0)("Address")
                End If
                'OW city

                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("City"))) Then
                    lblCity.Text = ds.Tables("tblOWAddress").Rows(0)("City") & ","
                End If

                'OW post code
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("PostCode"))) Then
                    lblPostCode.Text = ds.Tables("tblOWAddress").Rows(0)("PostCode")
                End If

                'OW Phone
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Phone"))) Then
                    lblTelNo.Text = ds.Tables("tblOWAddress").Rows(0)("Phone")
                End If
                'OW Fax
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Fax"))) Then
                    lblFaxNo.Text = ds.Tables("tblOWAddress").Rows(0)("Fax")
                End If
            End If

            If ds.Tables("tblBillingAddress").Rows.Count > 0 Then

                lblCompNameInvoiceTo.Text = ds.Tables("tblBillingAddress").Rows(0)("CompanyName").ToString

                'Address Invoice to
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("Address"))) Then
                    If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("State"))) Then
                        If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("PostCode"))) Then
                            lblInvoiceTo.Text = ds.Tables("tblBillingAddress").Rows(0)("Address") & "<br>" & ds.Tables("tblBillingAddress").Rows(0)("City") & "<br>" & ds.Tables("tblBillingAddress").Rows(0)("State") & " " & ds.Tables("tblBillingAddress").Rows(0)("PostCode")
                        End If
                    End If
                End If

            End If

            If ds.Tables("tblCompRegNo").Rows.Count > 0 Then
                'Registration number
                lblRegistartionNo.Text = "Registered in England"

                If Not (IsDBNull(ds.Tables("tblCompRegNo").Rows(0)("CompanyRegNo"))) Then
                    lblRegistartionNoLine2.Text = "No.: " & ds.Tables("tblCompRegNo").Rows(0)("CompanyRegNo")
                End If
            End If


            If ds.Tables("tblCreditNoteDetails").Rows.Count > 0 Then
                ''DateClosed
                'If Not (IsDBNull(ds.Tables("tblCreditNoteDetails").Rows(0)("DateClosed"))) Then
                '    lblClosedDate.Text = Strings.Format(ds.Tables("tblCreditNoteDetails").Rows(0)("DateClosed"), "dd-MM-yyyy")
                'End If

                ''WOID
                'If Not (IsDBNull(ds.Tables("tblCreditNoteDetails").Rows(0)("WOID"))) Then
                '    lblWOID.Text = ds.Tables("tblCreditNoteDetails").Rows(0)("WOID")
                'End If

                ''WOTitle
                'If Not (IsDBNull(ds.Tables("tblCreditNoteDetails").Rows(0)("WOTitle"))) Then
                '    lblWOTitle.Text = ds.Tables("tblCreditNoteDetails").Rows(0)("WOTitle") & ","
                'End If

                ''PONumber
                'If Not (IsDBNull(ds.Tables("tblCreditNoteDetails").Rows(0)("PONumber"))) Then
                '    lblPONumber.Text = ds.Tables("tblCreditNoteDetails").Rows(0)("PONumber")
                'End If

                ''Refund Amount
                'If Not (IsDBNull(ds.Tables("tblCreditNoteDetails").Rows(0)("RefundAmount"))) Then
                '    lblRefundAmount.Text = FormatCurrency((ds.Tables("tblCreditNoteDetails").Rows(0)("RefundAmount")), 2, TriState.True, TriState.True, TriState.False)
                'End If

                ''VAT
                'If Not (IsDBNull(ds.Tables("tblCreditNoteDetails").Rows(0)("VAT"))) Then
                '    lblVAT.Text = FormatCurrency((ds.Tables("tblCreditNoteDetails").Rows(0)("VAT")), 2, TriState.True, TriState.True, TriState.False)
                'End If
                ds.Tables("tblCreditNoteDetails").DefaultView.Sort = "DateClosed"

                rptList.DataSource = ds.Tables("tblCreditNoteDetails").DefaultView
                rptList.DataBind()

                If Not (IsDBNull(ds.Tables("tblCreditNoteDetails").Rows(0)("SalesInvoiceTotal"))) Then
                    If (CDec(ds.Tables("tblCreditNoteDetails").Rows(0)("Total")) <> CDec(ds.Tables("tblCreditNoteDetails").Rows(0)("SalesInvoiceTotal"))) Then
                        TdQuantityLabel.Visible = False
                    End If
                End If

                'Account No
                If Not (IsDBNull(ds.Tables("tblCreditNoteDetails").Rows(0)("CompanyId"))) Then
                    lblAccountNo.Text = ds.Tables("tblCreditNoteDetails").Rows(0)("CompanyId")
                End If

                'Account No
                If Not (IsDBNull(ds.Tables("tblCreditNoteDetails").Rows(0)("InvoiceDateCreated"))) Then
                    lblInvoiceDate.Text = Strings.Format((ds.Tables("tblCreditNoteDetails").Rows(0)("InvoiceDateCreated")), "dd-MM-yyyy")
                    invoicedateCreated = ds.Tables("tblCreditNoteDetails").Rows(0)("InvoiceDateCreated")
                End If
                'sales invoice Number

                If Not (IsDBNull(ds.Tables("tblCreditNoteDetails").Rows(0)("SalesInvoiceNo"))) Then
                    lblSalesInvoiceNo.Text = ds.Tables("tblCreditNoteDetails").Rows(0)("SalesInvoiceNo")
                End If
            End If

            If ds.Tables("tblTotalAmount").Rows.Count > 0 Then
                'Total
                If Not (IsDBNull(ds.Tables("tblTotalAmount").Rows(0)("Total"))) Then
                    lblTotalAmount.Text = FormatCurrency((ds.Tables("tblTotalAmount").Rows(0)("Total")), 2, TriState.True, TriState.True, TriState.False)
                End If

                'SubTotal
                If Not (IsDBNull(ds.Tables("tblTotalAmount").Rows(0)("SubTotal"))) Then
                    lblSubTotal.Text = FormatCurrency((ds.Tables("tblTotalAmount").Rows(0)("SubTotal")), 2, TriState.True, TriState.True, TriState.False)
                End If

                'Vat
                If Not (IsDBNull(ds.Tables("tblTotalAmount").Rows(0)("TotalVat"))) Then
                    lblTotalVAT.Text = FormatCurrency((ds.Tables("tblTotalAmount").Rows(0)("TotalVat")), 2, TriState.True, TriState.True, TriState.False)
                End If


                If Not (IsDBNull(ds.Tables("tblTotalAmount").Rows(0)("InvoiceNo"))) Then
                    lblInvoiceNo.Text = ds.Tables("tblTotalAmount").Rows(0)("InvoiceNo").ToString
                End If

                Try
                    lblVATPer.Text = ApplicationSettings.VATPercentage((ds.Tables("tblTotalAmount").Rows(0)("TotalVat")), (ds.Tables("tblTotalAmount").Rows(0)("SubTotal")))
                    lblTotalVATPer.Text = lblVATPer.Text
                Catch ex As Exception
                    If Not IsNothing(invoicedateCreated) Then
                        lblVATPer.Text = ApplicationSettings.VATPercentage(invoicedateCreated)
                        lblTotalVATPer.Text = lblVATPer.Text
                    Else
                        lblVATPer.Text = ""
                        lblTotalVATPer.Text = ""
                    End If

                End Try
                
            End If

        End If
    End Sub
End Class