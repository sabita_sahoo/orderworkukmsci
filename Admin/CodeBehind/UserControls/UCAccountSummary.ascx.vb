
''' <summary>
''' Class to handle Account Summary User Control Functionality
''' </summary>
''' <remarks></remarks>
Partial Public Class UCAccountSummary
    Inherits System.Web.UI.UserControl

    Private _showRating As Boolean
    Public Property ShowRating() As Boolean
        Get
            Return _showRating
        End Get
        Set(ByVal value As Boolean)
            _showRating = value
        End Set
    End Property

    Private _BizDivId As Integer
    Public Property BizDivId() As Integer
        Get
            Return _BizDivId
        End Get
        Set(ByVal value As Integer)
            _BizDivId = value
        End Set
    End Property

    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs


    ''' <summary>
    ''' Page Load method to initialise the account summary UC.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Hide the literals 
            litLastTransfer.Visible = False
            liLastTransfer.Visible = False
            Dim ds As DataSet
            Dim strCompanyId As String = ""

            Select Case ApplicationSettings.SiteType
                Case ApplicationSettings.siteTypes.admin
                    ds = ws.WSContact.MS_GetAccountSummaryForCompany(Request("companyid"), Request("bizDivId"), Session("UserID"))
                    strCompanyId = Request("companyid")
                Case Else
                    'Fetch Account Summary for the logged in account
                    ds = ws.WSContact.MS_GetAccountSummaryForCompany(Request("companyid"), ApplicationSettings.BizDivId, Session("UserID"))
                    strCompanyId = Request("companyid")
            End Select


            'If there are record then process if statement
            If ds.Tables(0).Rows.Count <> 0 Then

                Dim compName As String = ResourceMessageText.GetString("AccountName") '& "&nbsp;"
                Dim accNo As String = ResourceMessageText.GetString("AccountNo") '& "&nbsp;"
                Dim iHave As String = ResourceMessageText.GetString("IHave") & "&nbsp;"
                Dim woBalance As String = ResourceMessageText.GetString("ForPlacingWO") & ":&nbsp;"
                Dim woBalance1 As String = ResourceMessageText.GetString("ForPlacingWO") & "&nbsp;"
                Dim withBalance As String = ResourceMessageText.GetString("ForWithdrawal") & ":&nbsp;"
                Dim withBalance1 As String = ResourceMessageText.GetString("ForWithdrawal") & "&nbsp;"
                Dim subClass1 As String = ResourceMessageText.GetString("ReferenceCheckedYes")
                Dim subClass2 As String = ResourceMessageText.GetString("ReferenceCheckedNo")
                Dim location As String = ResourceMessageText.GetString("Location")
                Dim users As String = ResourceMessageText.GetString("Users")
                Dim DateApproved As String = ResourceMessageText.GetString("DateApproved") & "&nbsp;"

                lnkAccountProfile.HRef = "~/CompanyProfile.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")

                Dim dr As DataRow = ds.Tables(0).Rows(0)
                lblAccountName.Text = compName & ": " & dr("CompanyName")
                Session("SubClassID") = dr("SubClassID")
                lblAccountId.Text = accNo & ": " & CStr(strCompanyId).PadLeft(8, "0")

                lblAdminName.Text = "Name: " & dr("FullName")
                Session("CompanyAdminName") = dr("FullName")
                lblAdminEmail.Text = "Email: " & dr("Email")
                Session("CompanyAdminEmail") = dr("Email")

                'Fill information for number of users
                litSpecialists.Text = iHave
                If dr("TotalSpecialists") > 1 Then
                    'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                    litSpecialists.Text &= " <a href='Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId") & "' class='TxtListing'>" + CStr(dr("TotalSpecialists")) + " " + users + ".</a>"
                Else
                    'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                    litSpecialists.Text &= " <a href='Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId") & "' class='TxtListing'>" + CStr(dr("TotalSpecialists")) + " " + users + ".</a>"
                End If

                'Fill information for number of location
                litLocations.Text = iHave
                If Not IsDBNull(dr("TotalLocations")) Then
                    If dr("TotalLocations") > 1 Then
                        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                        litLocations.Text &= " <a href='Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid") & "' class='TxtListing'>" + CStr(dr("TotalLocations")) + " " + location + ".</a>"
                    Else
                        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                        litLocations.Text &= " <a href='Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classid=" & Request("classid") & "' class='TxtListing'>" + CStr(dr("TotalLocations")) + " " + location + ".</a>"
                    End If
                    Session("NoOfLocations") = dr("TotalLocations")
                End If

                'Check for account type 
                Dim GetSummaryFor As String
                If Not IsNothing(Session("ContactClassID")) Then
                    If Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
                        GetSummaryFor = "Supplier"
                    ElseIf Session("ContactClassID") = ApplicationSettings.RoleClientID Then
                        GetSummaryFor = "Client"
                    End If
                Else
                    GetSummaryFor = "Supplier"
                End If

                'fill in the balance due
                'If Not IsDBNull(dr("BalanceDue")) Then
                '    litBalanceDue.Text = ResourceMessageText.GetString("BillingSummary")

                '    If dr("BalanceDue") < 0 Then
                '        litBalanceDue.Text &= " <span style='color:red;'>" & FormatCurrency(dr("BalanceDue"), 2, TriState.True, TriState.True, TriState.False) & "</span>"
                '    Else
                '        litBalanceDue.Text &= " " & FormatCurrency(dr("BalanceDue"), 2, TriState.True, TriState.True, TriState.False)
                '    End If
                'Else
                litBalanceDue.Visible = False
                liBalanceDue.Visible = False
                'End If
                'Fill value for balance amount
                Dim balanceAmt As Decimal
                If IsDBNull(dr("BalanceDue")) Then
                    balanceAmt = 0
                    litWOBalance.Text = woBalance
                Else
                    balanceAmt = dr("BalanceDue")
                    litWOBalance.Text = woBalance
                End If
                If balanceAmt < 0 Then
                    litWOBalance.Text &= "<span style='color:red;'>" & FormatCurrency(balanceAmt, 2, TriState.True, TriState.True, TriState.False) & "</span>"
                Else
                    litWOBalance.Text &= FormatCurrency(balanceAmt, 2, TriState.True, TriState.True, TriState.False)
                End If

                If GetSummaryFor = "Client" Then
                    'Hide controls for client
                    litFundsLabel.Text = ResourceMessageText.GetString("BillingSummary") ' balance due
                    litWithdrawalBalance.Visible = False
                    liWithdrawalBalance.Visible = False
                    liSubClass.Visible = False
                    lblSubClass.Visible = False
                Else
                    litFundsLabel.Text = ResourceMessageText.GetString("BillingSummary") ' balance due
                    'Hide controls for supplier
                    litLastTransfer.Visible = False
                    If IsDBNull(dr("WithdrawalBalance")) Then
                        balanceAmt = 0
                        litWithdrawalBalance.Text = withBalance
                    Else
                        balanceAmt = dr("WithdrawalBalance")
                        litWithdrawalBalance.Text = withBalance
                    End If

                    If balanceAmt < 0 Then
                        litWithdrawalBalance.Text &= "<span style='color:red;'>" & FormatCurrency(balanceAmt, 2, TriState.True, TriState.True, TriState.False) & "&nbsp;</span>"
                    Else
                        litWithdrawalBalance.Text &= FormatCurrency(balanceAmt, 2, TriState.True, TriState.True, TriState.False)
                    End If

                    If Not IsDBNull(dr("SubClassId")) Then
                        If dr("SubClassId") = ApplicationSettings.StatusApprovedCompany Then
                            lblSubClass.Text &= subClass1
                        ElseIf dr("SubClassId") = ApplicationSettings.StatusNewCompany Or Session("SubClassId") = ApplicationSettings.StatusSuspendedCompany Then
                            lblSubClass.Text &= subClass2
                        End If
                    End If
                End If

                '****************************RATING*************************************
                If ShowRating = True Then
                    liRating.Visible = True
                    lblPosRating.Text = dr("PositiveRating")
                    lblNegRating.Text = dr("NegativeRating")
                    lblNeuRating.Text = dr("NeutralRating")
                    litRating.Text = "<a href=javascript:showHideRatings('ctl00_ContentPlaceHolder1_UCAccountSumm1_divDispAllRatings','Company') style='text-decoration: none;' class='TxtListing'>" + "<div id=divMainRating class='backGroundRating formTxtRedBold2' >&nbsp;&nbsp;&nbsp;" & ResourceMessageText.GetString("RatingScore") + CType(dr("TotalRating"), String) + "</div>" + "</a>"
                Else
                    liRating.Visible = False
                End If

                '****************************RATING*************************************

                '****************************Date Approved*************************************
                If dr("DateApproved") <> "" Then
                    liDateApproved.Visible = True
                    lblDateApproved.Text = DateApproved & dr("DateApproved")
                Else
                    liDateApproved.Visible = False
                End If
                '****************************Date Approved*************************************

                '****************************Active In Past 90 days*************************************
                If dr("ActiveInPast90days").ToString <> "" Then
                    liActiveInPast90days.Visible = True
                    lblActiveInPast90days.Text = "Active in the Past 90 days: " & dr("ActiveInPast90days").ToString
                Else
                    liActiveInPast90days.Visible = False
                End If
                '****************************Active In Past 90 days*************************************
            End If
            'Poonam modified on 28/10/2016 - Task - OA-344 : OA - New System details on Account summary
            If ds.Tables(1).Rows.Count <> 0 Then
                Dim dr1 As DataRow = ds.Tables(1).Rows(0)
                '****************************Account History*************************************
                'Created 
                If dr1("CreatedDate").ToString <> "" And dr1("CreatedByName").ToString <> "" Then
                    liCreated.Visible = True
                    If (dr1("CreatedByName").ToString = "Supplier OnBoarding") Then
                        lblCreated.Text = "Registered: " & dr1("CreatedDate").ToString & " by " & dr1("CreatedByName").ToString
                    Else
                        lblCreated.Text = "Created: " & dr1("CreatedDate").ToString & " by " & dr1("CreatedByName").ToString
                    End If
                Else
                    liCreated.Visible = False
                End If

                'Approved
                If dr1("ApprovedDate").ToString <> "" And dr1("ApprovedBy").ToString <> "" Then
                    liApproved.Visible = True
                    liDateApproved.Visible = False
                    lblApproved.Text = "Approved: " & dr1("ApprovedDate").ToString & " by " & dr1("ApprovedBy").ToString
                Else
                    liApproved.Visible = False
                End If

                'Modified
                If dr1("ModifiedDate").ToString <> "" And dr1("ModifiedByName").ToString <> "" Then
                    liModified.Visible = True
                    lblModified.Text = "Last Modified: " & dr1("ModifiedDate").ToString & " by " & dr1("ModifiedByName").ToString
                Else
                    liModified.Visible = False
                End If


                '****************************Account History*************************************
            End If


        End If

    End Sub

End Class