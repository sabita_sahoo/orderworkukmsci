
''' <summary>
''' Method to handle the Sub Menu user controls fucntionality
''' </summary>
''' <remarks></remarks>
Partial Public Class UCSubMenu
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' preventing pages being cached downstream 
        'Response.Cache.SetNoStore()

        Try

            'Ctype method to cast the object returned from the cache to type DataSet
            Dim ds As DataSet = CType(Cache("LoggedInfo-" & Session("UserName") & Session.SessionID), DataSet)

            'returns the filename of the page onto which the control is added
            Dim filepath As String = Page.Request.Url.PathAndQuery.ToLower
            Dim filepathNoQry As String = Page.Request.FilePath.ToLower
            Dim pagewithqry, pagenoqry As String
            pagewithqry = filepath.Substring(filepathNoQry.LastIndexOf("/") + 1)
            pagenoqry = filepathNoQry.Substring(filepathNoQry.LastIndexOf("/") + 1)

            If IsNothing(Session("ParentLink")) Then
                Session("ParentLink") = ""
            End If

            'Creating a copy of table "ChildMenu"
            Dim dv As DataView = ds.Tables(2).Copy.DefaultView
            'Filter table as per the criteria and parentMenuID is already selected so if Myprofile.aspx is Main Menu , then its child menu 
            'are selected to show if their showonMenu property is true

            'Hide WebServices Menu If the Account is not created or disabled for the company
            If ds.Tables("LoginInfo").Rows(0)("EnableWebServices") = True Then
                dv.RowFilter = "ShowOnMenu = 1 and ParentMenuID = " & Session("ParentMenuID")
            Else
                dv.RowFilter = "ShowOnMenu = 1 and ParentMenuID = " & Session("ParentMenuID") & " AND MenuLink <> 'ViewWebServiceLog.aspx' AND MenuLink <> 'WOUploadParams.aspx'"
            End If


            'Fill the data in the dataview for all the childmenu available under the main menu
            Dim i As Integer
            For i = 0 To dv.Count - 1
                If dv.Item(i).Item("MenuLink").ToLower = pagenoqry Or dv.Item(i).Item("MenuLink").ToLower = pagewithqry Or dv.Item(i).Item("MenuLink").ToLower = Session("ParentLink").ToLower Then
                    Session("ParentLink") = ""
                    Exit For
                End If
            Next

         
            'Bind the data
            RPTMenu.SelectedIndex = i
            RPTMenu.DataSource = dv
            RPTMenu.DataBind()        
        Catch Throwex As Exception

        End Try

    End Sub

End Class