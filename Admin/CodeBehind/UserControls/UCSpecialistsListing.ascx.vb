Public Partial Class UCSpecialistsListing
    Inherits System.Web.UI.UserControl
    Protected WithEvents btnChangeAdmin As System.Web.UI.HtmlControls.HtmlAnchor
    Shared ws As New WSObjs
    Dim objDs As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            hdnbizid.Value = Request("bizDivId")
            hdncompanyid.Value = Request("CompanyID")
            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            If ((Request("sender") = "buyer") Or (Request("classid") = ApplicationSettings.RoleClientID)) Then
                lnkBackToListing.HRef = "~\CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            Else
                lnkBackToListing.HRef = "~\CompanyProfile.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            End If

            Select Case ApplicationSettings.SiteType

                Case ApplicationSettings.siteTypes.admin
                    btnAddSpecialist.HRef = "~/SpecialistsForm.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&sender=" & Request("sender") & "&classId=" & Request("classId")
                Case ApplicationSettings.siteTypes.site
                    btnAddSpecialist.HRef = "~/SecurePages/SpecialistsForm.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&sender=" & Request("sender") & "&classId=" & Request("classId")
                    gvContacts.Columns(7).Visible = False
            End Select
            gvContacts.Sort("ContactID", SortDirection.Ascending)

        End If
        'ShowAddUsers()
    End Sub

    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    ''' <summary>
    ''' To return dataset for location listing
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                    ByVal maximumRows As System.Nullable(Of Integer)) As DataSet

        Dim siteType As String = "site"
        
        Dim contactID As Integer = HttpContext.Current.Items("MainContactID") 'Faizan
        Dim BizDivID As Integer = Request("bizDivId")

        Dim recordCount As Integer
        Dim ds As DataSet

        Select Case ApplicationSettings.SiteType

            Case ApplicationSettings.siteTypes.admin              
                siteType = "admin"
            Case ApplicationSettings.siteTypes.site     
                siteType = "site"
        End Select

        'ds = GetContactLocation(contactID, sortExpression, startRowIndex, maximumRows, recordCount, False)
        ' Since there is issue with accessing the page variable, fetching data directly without caching.
        If sortExpression = "LastLoginInfo" Then
            sortExpression = "LastLoginInfo ASC"
        End If
        ds = ws.WSContact.GetSpecialistsListing(contactID, BizDivID, siteType, sortExpression, startRowIndex, maximumRows, recordCount, txtSearch.Text.Trim)

        HttpContext.Current.Items("rowCount") = recordCount

        Dim dvContact As DataView
        dvContact = ds.Tables(2).Copy.DefaultView
        If dvContact.Count > 0 Then
            If dvContact.Item(0).Item("AttributeValue") = "46" Then
                HttpContext.Current.Items("ShowAddUserBtn") = False
                tdAddMultipleUser.Visible = False
            Else
                HttpContext.Current.Items("ShowAddUserBtn") = True
            End If
        Else
            HttpContext.Current.Items("ShowAddUserBtn") = True
        End If

        Return ds
    End Function


    Public Function GetDeletedStatus(ByVal deletedStatus As Boolean) As String

        If deletedStatus Then
            Return "Deleted"
        Else
            Return ""
        End If
    End Function
    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvContacts, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvContacts.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvContacts.DataBind()
    End Sub

    ''' <summary>
    ''' GridView event handler to call MakeGridViewHeaderClickable method
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowDataBound
        MakeGridViewHeaderClickable(gvContacts, e.Row)
    End Sub

    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    ''' <summary>
    ''' Function which is calling when rendering spend limit in gridview
    ''' </summary>
    ''' <param name="SpendLimit"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getSpendLimit(ByVal SpendLimit As String) As Integer

        Try

            If Not IsDBNull(SpendLimit) Then
                If SpendLimit <> "" Then
                    Return CInt(SpendLimit)
                Else
                    Return CInt("")
                End If
            End If
        Catch ex As Exception

        End Try

    End Function

    ''' <summary>
    ''' on Selected event of objectdatasource show/hide add user button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        If tdAddUser.Visible = True Then
            tdAddUser.Visible = HttpContext.Current.Items("ShowAddUserBtn")
        End If
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    ''' <summary>
    ''' this method returns Phone,fax and Mobile numbers if exist
    ''' </summary>
    ''' <param name="Phone"></param>
    ''' <param name="Fax"></param>
    ''' <param name="Mobile"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetInfo(ByVal Phone As Object, ByVal Fax As Object, ByVal Mobile As Object) As String
        Dim str As String = ""
        If Not IsDBNull(Phone) Then
            If Phone <> "" Then
                str &= ResourceMessageText.GetString("Phone") & Phone
            End If
        End If

        If Not IsDBNull(Fax) Then
            If Fax <> "" Then
                str &= ResourceMessageText.GetString("Fax") & Fax
            End If
        End If

        If Not IsDBNull(Mobile) Then
            If Mobile <> "" Then
                str &= ResourceMessageText.GetString("Mobile") & Mobile
            End If
        End If

        Return str

    End Function

    Public Function ReceiveWONotification(ByVal Notification As Object) As String
        If Not IsDBNull(Notification) Then
            Return Notification
        Else
            Return "No"
        End If
    End Function

    Private Sub btnChangeAdmin_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangeAdmin.ServerClick
        Response.Redirect("ChangeAdminAccount.aspx?ContactID=" & HttpContext.Current.Items("MainContactID") & "&BizDivID=" & HttpContext.Current.Items("BizDivID") & "&CompanyID=" & Request("CompanyID"))
    End Sub

    Private Sub hdnbtnNext_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnbtnNext.ServerClick
        Response.Redirect("~/AddMultipleUsers.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId"))
        
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim.Length > 0 Then
            ViewState("FilterString") = txtSearch.Text.Trim
        Else
            ViewState("FilterString") = ""
        End If
        gvContacts.DataBind()

        If gvContacts.Rows.Count = 0 Then
            gvContacts.DataBind()
        End If
    End Sub

    Private Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.Click
        txtSearch.Text = ""
        ViewState("FilterString") = ""
        gvContacts.DataBind()
    End Sub

    Public Function getBackToListingLink() As String
        Dim link As String = ""
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        If ((Request("sender") = "buyer") Or (Request("classid") = ApplicationSettings.RoleClientID)) Then
            link = "~\CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        Else
            link = "~\CompanyProfile.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        End If
        Return link
    End Function


   
End Class




