Imports System.Web
Imports System.Web.UI.Page
'Imports System.Data.SqlClient
Partial Public Class UCLogin

    Inherits System.Web.UI.UserControl

    Private _bizDivId As Integer
    Private _country As String

    ''' <summary>
    ''' property defines the bizdivid for user control
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BizDivId() As Integer
        Get
            Return _bizDivId
        End Get
        Set(ByVal value As Integer)
            _bizDivId = value
        End Set
    End Property
    ''' <summary>
    ''' property defines the country for user control
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 

    Public Property Country() As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property

    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs

    Private Sub InitializeComponent()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim sqlConnection As SqlConnection
        'sqlConnection = New SqlConnection("webdbUK")



        _country = ApplicationSettings.Country
        _bizDivId = ApplicationSettings.BizDivId

        Session.Remove("pagename")
        'direct the client not to store the responses in its history.
        'Response.Cache.SetNoStore()
        'If the page is securelogin then do not display the string "secure login"  on UClogin control
        Dim filepath As String = CType(CType(sender, UCLogin).Request, System.Web.HttpRequest).Path
        'Finally returns only the pagename with its aspx extensions
        filepath = filepath.Substring(filepath.LastIndexOf("/") + 1, filepath.Length - (filepath.LastIndexOf("/") + 1))
        If filepath = "securelogin.aspx" Then
            btnSecure.Visible = False
            Session("pagename") = "secure"
            divValidationMain.Visible = False
        End If

        'Checked for session expiry and if session is expired show the message
        If Not IsNothing(Session("SessionExpired")) Then
            If Session("SessionExpired") = True Then
                lblMsg.Text = ResourceMessageText.GetString("LoginSessionExpired")
                Session("SessionExpired") = False
            End If
        End If

        If Not IsPostBack Then
            Dim cookieName As String = "UserName"
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryUK
                    Select Case ApplicationSettings.SiteType
                        Case ApplicationSettings.siteTypes.admin
                            cookieName = "AdminUserNameUK"
                        Case ApplicationSettings.siteTypes.site
                            Select Case ApplicationSettings.BizDivId
                                Case ApplicationSettings.OWUKBizDivId
                                    cookieName = "UserNameOWUK"
                                Case ApplicationSettings.SFUKBizDivId
                                    cookieName = "UserNameSFUK"
                            End Select
                    End Select
                Case ApplicationSettings.CountryDE
                    Select Case ApplicationSettings.SiteType
                        Case ApplicationSettings.siteTypes.admin
                            cookieName = "AdminUserNameDE"
                        Case ApplicationSettings.siteTypes.site
                            Select Case ApplicationSettings.BizDivId
                                Case ApplicationSettings.OWUKBizDivId
                                    cookieName = "UserNameOWDE"
                                Case ApplicationSettings.SFUKBizDivId
                                    cookieName = "UserNameSFDE"
                            End Select
                    End Select
            End Select
            
            If ((Not (Request.Cookies("UserName")) Is Nothing) _
                     AndAlso (Not (Request.Cookies("Password")) Is Nothing)) Then
                txtLoginUserName.Text = Request.Cookies("UserName").Value
                txtLoginPassword.Attributes("value") = Request.Cookies("Password").Value
            End If
        End If
    End Sub

    ''' <summary>
    ''' Function to call on login button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LoginMe(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkBtnLogin.Click

        If Trim(txtLoginUserName.Text) = "" Or Trim(txtLoginPassword.Text) = "" Then
            'This will check for username and password entered and thus save DB trip if not entered.

            If Session("pagename") = "secure" Then
                divValidationMain.Visible = True
                lblMsg.Text = "<lu><li>" & ResourceMessageText.GetString("LoginMsg") & "</li>"
                Session.Remove("pagename")
            Else
                lblMsg.Text = ResourceMessageText.GetString("LoginMsg")
            End If

        Else

            'Check Authentication
            Dim ds As New DataSet
            Select Case ApplicationSettings.SiteType
                Case ApplicationSettings.siteTypes.admin
                    'Set the BizDivId in the session for Admin
                    If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                        Session("BizDivId") = ApplicationSettings.OWUKBizDivId
                    ElseIf ApplicationSettings.Country = ApplicationSettings.CountryDE Then
                        Session("BizDivId") = ApplicationSettings.OWDEBizDivId
                    End If
                    ds = ws.WSSecurity.AuthenticateAdminUser(Trim(txtLoginUserName.Text), Encryption.EncryptToMD5Hash(Trim(txtLoginPassword.Text)), ApplicationSettings.BusinessId)
                Case ApplicationSettings.siteTypes.site
                    'Fetch Dataset from authentication user SP which will contain LoginInfo, Menu Table, Child Menu table and privilege table
                    ds = ws.WSSecurity.AuthenticateUser(Trim(txtLoginUserName.Text), Encryption.EncryptToMD5Hash(Trim(txtLoginPassword.Text)), ApplicationSettings.BizDivId, ApplicationSettings.BusinessId)
            End Select





            'If user does not exit then login failed
            If ds.Tables(0).Rows.Count = 0 Then
                'Show message as per the locale used in the country
                If Session("pagename") = "secure" Then
                    divValidationMain.Visible = True
                    lblMsg.Text = "<lu><li>" & ResourceMessageText.GetString("LoginFailed") & "</li>"
                    Session.Remove("pagename")
                Else
                    lblMsg.Text = ResourceMessageText.GetString("LoginFailed")
                End If


            Else
                'CommonFunctions.GetAddressUsage()
                'if checkbox "Remember Me" is checked by user then create a cookie to store his username
                If chkRemember.Checked Then
                    Dim cookieName As String = "UserName"
                    Select Case ApplicationSettings.Country
                        Case ApplicationSettings.CountryUK
                            Select Case ApplicationSettings.SiteType
                                Case ApplicationSettings.siteTypes.admin
                                    cookieName = "AdminUserNameUK"
                                Case ApplicationSettings.siteTypes.site
                                    Select Case ApplicationSettings.BizDivId
                                        Case ApplicationSettings.OWUKBizDivId
                                            cookieName = "UserNameOWUK"
                                        Case ApplicationSettings.SFUKBizDivId
                                            cookieName = "UserNameSFUK"
                                    End Select
                            End Select
                        Case ApplicationSettings.CountryDE
                            Select Case ApplicationSettings.SiteType
                                Case ApplicationSettings.siteTypes.admin
                                    cookieName = "AdminUserNameDE"
                                Case ApplicationSettings.siteTypes.site
                                    Select Case ApplicationSettings.BizDivId
                                        Case ApplicationSettings.OWUKBizDivId
                                            cookieName = "UserNameOWDE"
                                        Case ApplicationSettings.SFUKBizDivId
                                            cookieName = "UserNameSFDE"
                                    End Select
                            End Select
                    End Select
                    'If "remember me" checked then create a cookie and store the login UserName & password

                    Response.Cookies("UserName").Expires = DateTime.Now.AddDays(10)
                    Response.Cookies("Password").Expires = DateTime.Now.AddDays(10)
                Else
                    Response.Cookies("UserName").Expires = DateTime.Now.AddDays(-1)
                    Response.Cookies("Password").Expires = DateTime.Now.AddDays(-1)
                End If
                Response.Cookies("UserName").Value = txtLoginUserName.Text.Trim()
                Response.Cookies("Password").Value = txtLoginPassword.Text.Trim()


                'Maintain session for the following with the value as in dataset
                Session("CompanyId") = ds.Tables(0).Rows(0).Item("CompanyId")
                Session("CompanyName") = ds.Tables(0).Rows(0).Item("CompanyName")
                Session("UserId") = ds.Tables(0).Rows(0).Item("UserID")
                Session("UserName") = txtLoginUserName.Text
                Session("FirstName") = ds.Tables(0).Rows(0).Item("FName")
                Session("LastName") = ds.Tables(0).Rows(0).Item("LName")
                Session("SecQues") = ds.Tables(0).Rows(0).Item("SecurityQues")
                Session("SecAns") = ds.Tables(0).Rows(0).Item("SecurityAns")
                Session("Password") = ds.Tables(0).Rows(0).Item("Password")

                If Not IsDBNull(ds.Tables(0).Rows(0).Item("AcceptWOValueLimit")) Then
                    Session("AcceptWOValueLimit") = ds.Tables(0).Rows(0).Item("AcceptWOValueLimit")
                End If

                If Not IsDBNull(ds.Tables(0).Rows(0).Item("SpendLimit")) Then
                    Session("SpendLimit") = ds.Tables(0).Rows(0).Item("SpendLimit")
                End If
                'Update tblContactsLogin table to store recent login information
                ws.WSSecurity.UpdateLoginDate(Session("UserId"))
                Page.Cache.Remove("LoggedInfo-" & Session("UserName") & Session.SessionID)
                'Page.Cache.Add("LoggedInfo-" & Session("UserName") & Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
                SyncLock (GetType(UCLogin))
                    Cache.Insert("LoggedInfo-" & Session("UserName") & Session.SessionID, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
                End SyncLock
                Select Case ApplicationSettings.SiteType
                    Case ApplicationSettings.siteTypes.admin
                        If ds.Tables(0).Rows(0).Item("ClassID") = ApplicationSettings.RoleOWID Then
                            Session("RoleGroupID") = ds.Tables(0).Rows(0).Item("RoleGroupID")
                            Dim cookieName As String = "AdminPref" + Session("UserName")

                            'Change added by PB: to implement Admin DE without the selectadmin logic
                            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                                'If Not Request.Cookies(cookieName) Is Nothing Then
                                'Select Case Request.Cookies(cookieName).Value
                                '    Case "OrderWorkUK"
                                Dim Url As String = Page.Request.RawUrl
                                Dim query As String
                                If Url.Length > Url.LastIndexOf("preURL") + 7 Then
                                    query = Url.Substring(Url.LastIndexOf("preURL") + 7)
                                    If ds.Tables(0).Rows(0).Item("MenuLink") = "" Then
                                        If Not IsNothing(Request.QueryString("preURL")) Then
                                            CommonFunctions.AutoLogin(Page, ApplicationSettings.OWUKBizDivId, Server.UrlDecode(query))
                                        Else
                                            CommonFunctions.AutoLogin(Page, ApplicationSettings.OWUKBizDivId, "AdminWOListing.aspx?mode=Submitted")
                                        End If
                                    Else
                                        If Not IsNothing(Request.QueryString("preURL")) Then
                                            CommonFunctions.AutoLogin(Page, ApplicationSettings.OWUKBizDivId, Server.UrlDecode(query))
                                        Else
                                            'CommonFunctions.AutoLogin(Page, ApplicationSettings.OWUKBizDivId, Page.Request.RawUrl)
                                            CommonFunctions.AutoLogin(Page, ApplicationSettings.OWUKBizDivId, ds.Tables(0).Rows(0).Item("MenuLink"))
                                        End If

                                    End If
                                End If

                                CommonFunctions.AutoLogin(Page, ApplicationSettings.OWUKBizDivId, "AdminWOListing.aspx?mode=Submitted")
                                '    Case "SkillsFinderUK"
                                '        CommonFunctions.AutoLogin(Page, ApplicationSettings.SFUKBizDivId, "AdminWOListing.aspx?mode=Submitted")
                                'End Select
                                'Else
                                '    Response.Redirect("SelectAdminSite.aspx", True)
                                'End If
                            ElseIf ApplicationSettings.Country = ApplicationSettings.CountryDE Then
                                Response.Redirect("AdminWOListing.aspx?mode=Submitted", True)
                            End If
                        Else
                            lblMsg.Text = ResourceMessageText.GetString("LoginRepOnly")
                        End If
                    Case ApplicationSettings.siteTypes.site
                        Session("RoleGroupID") = ds.Tables(0).Rows(0).Item("RoleGroupID")
                        Session("ContactClassID") = ds.Tables(0).Rows(0).Item("ClassID")
                        If ds.Tables(0).Rows.Count > 1 Then
                            Session("SecondaryClassID") = ds.Tables(0).Rows(1).Item("ClassID")
                        End If
                        Session("SubClassID") = ds.Tables(0).Rows(0).Item("Status")
                        If Not IsNothing(Request("calledfrom")) Then
                            If Request("calledfrom").ToLower = "samplewo" Then
                                Response.Redirect("~/SecurePages/WOForm.aspx?Sender=SampleWO&WOID=" & Request("WOID"), True)
                            End If
                        Else
                            If ds.Tables(0).Rows(0).Item("ClassID") = ApplicationSettings.RoleSupplierID Then
                                Response.Redirect("~/SecurePages/Welcome.aspx?User=Supplier", True)
                            ElseIf ds.Tables(0).Rows(0).Item("ClassID") = ApplicationSettings.RoleClientID Then
                                Response.Redirect("~/SecurePages/Welcome.aspx?User=Buyer", True)
                            End If
                        End If
                End Select
            End If
        End If

    End Sub
    Public Sub RemoveCallback(ByVal tag As String, ByVal obj As Object, ByVal reason As CacheItemRemovedReason)
        If (CacheItemRemovedReason.Underused = reason) Then
            Cache.Remove(tag)
            If Not IsNothing(obj) Then
                Cache.Insert(tag, obj, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            End If
        Else
            Cache.Remove(tag)
        End If
    End Sub
End Class