
''' <summary>
''' Code behind class for UC location form.
''' </summary>
''' <remarks></remarks>
Partial Public Class UCLocationForm
    Inherits System.Web.UI.UserControl

    Private dsContacts As OWContacts

    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs


    Private _companyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        Session("ParentLink") = ("Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizdivid") & "classid=" & Request("classid"))
        If Not IsPostBack Then

            divValidationMain.Visible = False

            If CStr(Request("AddressId")) <> "" Then
                ViewState("AddressId") = Request("AddressId")
            Else
                ViewState("AddressId") = 0
            End If


            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                CommonFunctions.PopulateCountry(Page, ddlCompanyCountry, "United Kingdom")
            End If
            tblDepotLocDetails.Visible = False
            trDepotLocDetails.Style.Add("display", "none")
            If Not IsNothing(Request("AddressID")) And Request("AddressID") <> "" Then
                populatedata(True)
            Else
                Dim dsAddress As DataSet
                pnlFinance.Style.Add("display", "none")
                Dim dv As DataView
                Dim dvAltBillingLoc As DataView = Nothing
                Dim contactid As Integer = _companyID
                dsAddress = GetContactLocation(contactid, "AddressID", 0, 1000, 1, False)
                dv = dsAddress.Tables("Locations").Copy.DefaultView
                dvAltBillingLoc = dsAddress.Tables("Locations").Copy.DefaultView
                dvAltBillingLoc.RowFilter = "IsBilling <> 1 AND IsDepot <> 1"
                If Not IsNothing(dvAltBillingLoc) Then
                    ddlAltBillingLoc.DataSource = dvAltBillingLoc
                    ddlAltBillingLoc.DataBind()
                End If
                Dim li As ListItem
                li = New ListItem
                li.Text = "Use Existing"
                li.Value = 0
                ddlAltBillingLoc.Items.Insert(0, li)
            End If
            'If Not IsNothing(Session("BusinessArea")) Then
            '    If Session("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
            '        chkbxDepot.Visible = True
            '    Else
            '        chkbxDepot.Visible = False
            '    End If
            'Else
            '    chkbxDepot.Visible = False
            'End If
        End If
    End Sub

    ''' <summary>
    ''' Method to populate the location form
    ''' </summary>
    ''' <param name="killcache"></param>
    ''' <remarks></remarks>
    Public Sub populatedata(Optional ByVal killcache As Boolean = False)

        divValidationMain.Visible = False
        'Disable the Postcode Lookup
        'btnFind.Enabled = False

        Dim dv As DataView
        Dim dvContactsQA As DataView = Nothing
        Dim dvAltBillingLoc As DataView = Nothing
        Dim contactid As Integer = _companyID

        Dim dsAddress As DataSet
        dsAddress = GetContactLocation(contactid, "AddressID", 0, 1000, 1, False)
        dv = dsAddress.Tables("Locations").Copy.DefaultView
        dvAltBillingLoc = dsAddress.Tables("Locations").Copy.DefaultView
        dvAltBillingLoc.RowFilter = "IsBilling <> 1 AND IsDepot <> 1 AND AddressID <> " + Request.QueryString("AddressId")

        If Not IsNothing(dvAltBillingLoc) Then
            ddlAltBillingLoc.DataSource = dvAltBillingLoc
            ddlAltBillingLoc.DataBind()
        End If
        Dim li As ListItem
        li = New ListItem
        li.Text = "Use Existing"
        li.Value = 0
        ddlAltBillingLoc.Items.Insert(0, li)
        If dsAddress.Tables.Count > 3 Then
            dvContactsQA = dsAddress.Tables(3).Copy.DefaultView
            dvContactsQA.RowFilter = "AddressID = '" + ViewState("AddressId") + "'"
        End If
        'Dim dsAddress As DataSet = CommonFunctions.GetLocationsDS(Page, Session.SessionID, CStr(Request("AddressID")), Session("CompanyId"), CStr(Request("keyword")), CStr(Request("keywordJoinType")), "", 0, "", "", Session("LocationCacheKey"), killcache)
        'dv = dsAddress.Tables("Locations").Copy.DefaultView
        If Not IsNothing(Request("AddressID")) Then

            ' Filtering for particular Address ID
            'dv.RowFilter = "AddressID = " + Request("AddressID")
            dv.RowFilter = "AddressID = '" + ViewState("AddressId") + "'"
            Dim dt As DataTable
            dt = dv.ToTable()
            CommonFunctions.StoreVerNum(dt)
            If Not IsDBNull(dv.Item(0).Item("LocationName")) Then
                If dv.Item(0).Item("LocationName") <> "" Then
                    txtName.Text = dv.Item(0).Item("LocationName")
                End If
            Else
                txtName.Text = ""
            End If
            ViewState("Name") = txtName.Text.Trim
            ddlAltBillingLoc.SelectedValue = dv.Item(0).Item("AltBillingLoc")
            ViewState("AltBillLoc") = dv.Item(0).Item("AltBillingLoc")
            'Populating value for "Is Main Contact"
            If Not IsDBNull(dv.Item(0).Item("Main")) Then
                If dv.Item(0).Item("Main") = "True" Then
                    ChkLocation.Checked = True
                    chkbxBilling.Checked = True
                    chkbxBilling.Enabled = False
                    chkbxDepot.Checked = False
                    chkbxDepot.Enabled = False
                Else
                    ChkLocation.Checked = False
                    chkbxBilling.Checked = False
                    chkbxBilling.Enabled = True
                    chkbxDepot.Enabled = True
                End If
            Else
                ChkLocation.Checked = False
                chkbxBilling.Checked = False
                chkbxBilling.Enabled = True
                chkbxDepot.Enabled = True
            End If

            chkOrderMatch.Enabled = True
            If Not IsDBNull(dv.Item(0).Item("DoNotIncludeInOrdermatch")) Then
                If dv.Item(0).Item("DoNotIncludeInOrdermatch") = "True" Then
                    chkOrderMatch.Checked = True
                Else
                    chkOrderMatch.Checked = False
                End If
            End If
            If dsAddress.Tables("Locations").Rows.Count = 1 Then
                chkOrderMatch.Checked = False
                chkOrderMatch.Enabled = False
            End If

            'Populating value for "Is Deleted Location"
            If Not IsDBNull(dv.Item(0).Item("IsDeleted")) Then
                If dv.Item(0).Item("IsDeleted") = "True" Then
                    ChkIsDeleted.Checked = True
                Else
                    ChkIsDeleted.Checked = False
                End If
            Else
                ChkIsDeleted.Checked = False
            End If

            If ApplicationSettings.SiteType = ApplicationSettings.siteTypes.admin Then
                ChkIsDeleted.Visible = True
                If dv.Item(0).Item("Main") <> "True" Then
                    ChkIsDeleted.Enabled = True
                Else
                    ChkIsDeleted.Enabled = False
                    tblDeleteMsg.Visible = True
                    lblDeleteMsg.Visible = True
                    lblDeleteMsg.Text = "NOTE: This location cannot be marked as deleted as it is the main location. Please, first mark some other location as Main Location to mark this location as Deleted."
                End If
                If dv.Item(0).Item("Main") <> "True" And dv.Item(0).Item("Users") = 0 Then
                    ChkIsDeleted.Enabled = True
                ElseIf dv.Item(0).Item("Main") <> "True" And dv.Item(0).Item("Users") <> 0 Then
                    ChkIsDeleted.Enabled = False
                    tblDeleteMsg.Visible = True
                    lblDeleteMsg.Visible = True
                    lblDeleteMsg.Text = "NOTE: This location cannot be marked as deleted until its users have been reassigned to another location entry."
                End If
            Else
                ChkIsDeleted.Visible = False
                ChkIsDeleted.Enabled = False
            End If

            'Populating Whether the location is a billing centre or not
            If Not IsDBNull(dv.Item(0).Item("IsBilling")) Then
                If dv.Item(0).Item("IsBilling") = "True" Then
                    chkbxBilling.Checked = True
                Else
                    chkbxBilling.Checked = False
                End If
            Else
                chkbxBilling.Checked = False
            End If

            'Populating whether the location is a Depot or not
            If Not IsDBNull(dv.Item(0).Item("IsDepot")) Then
                If dv.Item(0).Item("IsDepot") = "True" Then
                    chkbxDepot.Checked = True
                    tblDepotLocDetails.Visible = True
                    trDepotLocDetails.Style.Add("display", "inline")
                    txtDepotDetails.Text = dv.Item(0).Item("DepotDetails")
                    ChkLocation.Enabled = False
                    ChkLocation.Checked = False
                    'chkbxBilling.Checked = False
                    'chkbxBilling.Enabled = False
                Else
                    chkbxDepot.Checked = False
                End If
            Else
                chkbxDepot.Checked = False
            End If

            ' Populate Fixed Capacity
            If Not IsDBNull(dv.Item(0).Item("FixedCapacity")) Then
                txtFixedCapacity.Text = dv.Item(0).Item("FixedCapacity")
            Else
                txtFixedCapacity.Text = ""
            End If


            'Populating the address field
            If Not IsDBNull(dv.Item(0).Item("Address")) Then
                If dv.Item(0).Item("Address") <> "" Then
                    txtCompanyAddress.Text = dv.Item(0).Item("Address")
                End If
            Else
                txtCompanyAddress.Text = ""
            End If
            ViewState("Address") = txtCompanyAddress.Text.Trim
            'Populating the city field
            If Not IsDBNull(dv.Item(0).Item("City")) Then
                If dv.Item(0).Item("City") <> "" Then
                    txtCity.Text = dv.Item(0).Item("City")
                End If
            Else
                txtCity.Text = ""
            End If
            ViewState("City") = txtCity.Text.Trim
            'Populating the Post Code field
            If Not IsDBNull(dv.Item(0).Item("PostCode")) Then
                If dv.Item(0).Item("PostCode") <> "" Then
                    txtCompanyPostalCode.Text = dv.Item(0).Item("PostCode")
                End If
            Else
                txtCompanyPostalCode.Text = ""
            End If
            ViewState("PostalCode") = txtCompanyPostalCode.Text.Trim
            'Populating the state field
            If Not IsDBNull(dv.Item(0).Item("State")) Then
                If dv.Item(0).Item("State") <> "" Then
                    txtCounty.Text = dv.Item(0).Item("State")
                End If
            Else
                txtCounty.Text = ""
            End If
            ViewState("County") = txtCounty.Text.Trim
            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                'Populating the country
                If Not IsDBNull(dv.Item(0).Item("CountryID")) Then
                    If dv.Item(0).Item("CountryID") <> 0 Then
                        ddlCompanyCountry.SelectedValue = CInt(dv.Item(0).Item("CountryID"))
                    End If
                End If
            End If


            'Populating the Phone field
            If Not IsDBNull(dv.Item(0).Item("Phone")) Then
                If dv.Item(0).Item("Phone") <> "" Then
                    txtCompanyPhone.Text = dv.Item(0).Item("Phone")
                End If
            Else
                txtCompanyPhone.Text = ""
            End If
            ViewState("Phone") = txtCompanyPhone.Text.Trim
            'Populating the Fax Field
            If Not IsDBNull(dv.Item(0).Item("Fax")) Then
                If dv.Item(0).Item("Fax") <> "" Then
                    txtCompanyFax.Text = dv.Item(0).Item("Fax")
                End If
            Else
                txtCompanyFax.Text = ""
            End If
            If chkbxBilling.Checked Then
                pnlFinance.Style.Add("display", "inline")
            Else
                pnlFinance.Style.Add("display", "none")
            End If
            If Not IsNothing(dvContactsQA) Then
                If dvContactsQA.Count > 0 Then
                    txtField1.Text = dvContactsQA.Item(0)("Field1Label")
                    txtField2.Text = dvContactsQA.Item(0)("Field2Label")
                    txtField3.Text = dvContactsQA.Item(0)("Field3Label")
                    txtField4.Text = dvContactsQA.Item(0)("Field4Label")
                    txtFieldValue1.Text = dvContactsQA.Item(0)("Field1Value")
                    txtFieldValue2.Text = dvContactsQA.Item(0)("Field2Value")
                    txtFieldValue3.Text = dvContactsQA.Item(0)("Field3Value")
                    txtFieldValue4.Text = dvContactsQA.Item(0)("Field4Value")
                    chkField1.Checked = dvContactsQA.Item(0)("Field1Enabled")
                    chkField2.Checked = dvContactsQA.Item(0)("Field2Enabled")
                    chkField3.Checked = dvContactsQA.Item(0)("Field3Enabled")
                    chkField4.Checked = dvContactsQA.Item(0)("Field4Enabled")
                    If chkField1.Checked Then
                        reqfld1.Enabled = True
                        reqFldLbl1.Enabled = True
                        spanfld1.Style.Add("display", "inline")
                        spanfldVal1.Style.Add("display", "inline")
                    End If
                    If chkField2.Checked Then
                        reqfld2.Enabled = True
                        reqFldLbl2.Enabled = True
                        spanfld2.Style.Add("display", "inline")
                        spanfldVal2.Style.Add("display", "inline")
                    End If
                    If chkField3.Checked Then
                        reqfld3.Enabled = True
                        reqFldLbl3.Enabled = True
                        spanfld3.Style.Add("display", "inline")
                        spanfldVal3.Style.Add("display", "inline")
                    End If
                    If chkField4.Checked Then
                        reqfld4.Enabled = True
                        reqFldLbl4.Enabled = True
                        spanfld4.Style.Add("display", "inline")
                        spanfldVal4.Style.Add("display", "inline")
                    End If
                End If
            End If

        Else
            txtName.Text = ""
            ChkLocation.Checked = False
            ChkIsDeleted.Checked = False
            chkbxBilling.Visible = False
            txtCity.Text = ""
            txtCompanyAddress.Text = ""
            txtCompanyPostalCode.Text = ""
            txtCounty.Text = ""
            txtCompanyPhone.Text = ""
            txtCompanyFax.Text = ""

            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                ddlCompanyCountry.SelectedValue = CInt(ApplicationSettings.CountryID_UK)
            End If
        End If
    End Sub

    ''' <summary>
    ''' To fetch location for contact
    ''' </summary>
    ''' <param name="contactID"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="recordCount"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetContactLocation(ByVal contactID As Integer, Optional ByVal sortExpression As String = "AddressID", Optional ByVal startRowIndex As Integer = 0, _
                                    Optional ByVal maximumRows As Integer = 1000, Optional ByVal recordCount As Integer = 1000, Optional ByVal KillCache As Boolean = False) As DataSet
        Dim cacheKey As String = "ContactLocation" & "-" & CStr(contactID) & "-" & Session.SessionID
        If KillCache = True Then
            Page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        Dim siteType As String = "site"

        Select Case ApplicationSettings.SiteType

            Case ApplicationSettings.siteTypes.admin
                siteType = "admin"
            Case ApplicationSettings.siteTypes.site
                siteType = "site"
        End Select
        If Cache(cacheKey) Is Nothing Then
            ds = ws.WSContact.GetLocationListing(contactID, siteType, sortExpression, startRowIndex, maximumRows, recordCount, "")
        Else
            ds = CType(Page.Cache(cacheKey), DataSet)

        End If
        Return ds
    End Function

    ''' <summary>
    ''' Method To handle the reset functionality of the top button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnResetTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetTop.Click
        populatedata(True)
    End Sub

    ''' <summary>
    ''' Method to handle the reset functionality of the Bottom button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnResetBottom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetBottom.Click
        populatedata(True)
    End Sub

    ''' <summary>
    ''' Method to handle the cancel functionality of the Top Exit button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancelTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTop.Click
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        Response.Redirect("Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizdivid") & "&classid=" & Request("classid"))
    End Sub

    ''' <summary>
    ''' Method to handle the cancel functionality of the Bottom Exit button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancelBottom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelBottom.Click
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        Response.Redirect("Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizdivid") & "&classid=" & Request("classid"))
    End Sub
    ''' <summary>
    ''' Method to handle the save functionality of the Bottom save Exit button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSaveBottom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveBottom.Click
        ValidatePage()
    End Sub
    Private Sub btnSaveTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click
        ValidatePage()
    End Sub

    Protected Sub ValidatePage()
        setValidations()
        Page.Validate()
        If (Page.IsValid = False) Then
            divValidationMain.Visible = True
        Else
            divValidationMain.Visible = False
            If CheckDepotLocation() Then
                Exit Sub
            End If

            Dim dsAllContact As DataSet = GetContactLocation(_companyID)
            ''********************Check if the location with the same name already exists*******
            Dim updateFlag As Boolean = True
            Dim dv As DataView

            dv = dsAllContact.Tables(0).Copy.DefaultView
            dv.RowFilter = "LocationName Like  '" & Trim(txtName.Text) & "'"
            If dv.Count > 0 Then
                Dim dsRow As DataRowView
                For Each dsRow In dv
                    If dsRow.Item("AddressID") <> Request("AddressID") Then
                        divValidationMain.Visible = True
                        lblError.Text = "<li>" & ResourceMessageText.GetString("MainLocationExist") & "</li>"
                        Exit Sub
                    End If
                Next
                Dim dvRoles As DataView
                If dsAllContact.Tables.Count > 1 Then
                    If dsAllContact.Tables(2).Rows.Count > 0 Then
                        dvRoles = dsAllContact.Tables(2).DefaultView
                        dvRoles.RowFilter = "AddressID = " & Request("AddressID")
                        Dim drowRoles As DataRowView
                        Dim countdvRoles As Integer
                        For Each drowRoles In dvRoles
                            If drowRoles.Item("RoleGroupID") = 10 Or drowRoles.Item("RoleGroupID") = 11 Or drowRoles.Item("RoleGroupID") = ApplicationSettings.RoleSupplierAdminID Or drowRoles.Item("RoleGroupID") = ApplicationSettings.RoleClientAdminID Then
                                countdvRoles = countdvRoles + 1
                            End If
                        Next
                        If countdvRoles > 0 Then
                            ViewState.Add("RoleAccounts", "True")
                        Else
                            ViewState.Add("RoleAccounts", "False")
                        End If
                    End If
                End If
            Else
                ViewState.Add("RoleAccounts", "False")
            End If

            pnlSubmitForm.Visible = False
            pnlConfirm.Visible = True
            ' Code change by pankaj malav to add warning message
            ' Code change by pankaj malav to add warning message
            Dim Confirmmsg As String
            Confirmmsg = ResourceMessageText.GetString("ConfirmationMessageBack")
            'if billing centre is checked then check for account type with which the location is associated
            If chkbxBilling.Checked = True And chkbxDepot.Checked = False Then
                ''check for account type with which the location is associated
                'If ViewState("RoleAccounts") <> "True" Then
                '    Confirmmsg = ResourceMessageText.GetString("AlertBillingLocation").Replace("<locationame>", Trim(txtName.Text))
                '    lblConfirmMsgTxt.Text = Confirmmsg
                '    pnlConfirm.FindControl("divConfirmbtn").Visible = False
                'Else
                If ViewState("RoleAccounts") = "True" Then
                    pnlConfirm.FindControl("divConfirmbtn").Visible = True
                    lblConfirmMsgTxt.Text = Confirmmsg
                End If
            ElseIf chkbxBilling.Checked = True And chkbxDepot.Checked = True Then

            Else
                'If billing is not checked then proceed normally as was done previously
                pnlConfirm.FindControl("divConfirmbtn").Visible = True
                lblConfirmMsgTxt.Text = Confirmmsg
            End If

        End If

    End Sub

    ''' <summary>
    ''' Method to save the changes to the location form, builds an location xsd and directs to confirm action form
    ''' </summary>
    ''' <remarks></remarks>
    Public Function saveLocation() As XSDContacts

        Dim dsContacts As New XSDContacts()
        Dim contactID As String = _companyID
        Dim addressID As Integer = ViewState("AddressId")

        Dim nrowCA As XSDContacts.tblContactsAddressesRow = dsContacts.tblContactsAddresses.NewRow
        With nrowCA
            .ContactID = contactID
            .Type = "Business"
            .Name = txtName.Text.Trim
            .Address = txtCompanyAddress.Text.Trim
            .City = txtCity.Text.Trim
            .State = txtCounty.Text.Trim
            .PostCode = CommonFunctions.FormatPostcode(txtCompanyPostalCode.Text.Trim)



            If ApplicationSettings.Country = ApplicationSettings.CountryUK Then
                .CountryID = ddlCompanyCountry.SelectedValue
            Else
                .CountryID = ApplicationSettings.CountryID_DE
            End If

            If txtFixedCapacity.Text.Trim.Length > 0 Then
                .FixedCapacity = txtFixedCapacity.Text.Trim
            End If

            .Sequence = 0
            .IsDefault = ChkLocation.Checked
            .IsDeleted = ChkIsDeleted.Checked
            ' This code is added by pankaj malav to cater for billing location
            If chkbxBilling.Checked = True Then
                .IsBilling = True
            Else
                .IsBilling = False
            End If
            If chkbxDepot.Checked = True Then
                .IsDepot = True
                If txtDepotDetails.Text <> "" Then
                    .ServiceLocDetails = txtDepotDetails.Text.Trim
                End If
            Else
                .IsDepot = False
                .ServiceLocDetails = ""
            End If
            .Phone = (txtCompanyPhone.Text.Trim)
            .Fax = (txtCompanyFax.Text.Trim)
            If Not IsNothing(ddlAltBillingLoc) Then
                .AltBillingLoc = ddlAltBillingLoc.SelectedValue
            Else
                If Not IsNothing(ViewState("AltBillLoc")) Then
                    .AltBillingLoc = ViewState("AltBillLoc")
                End If
            End If

        End With
        dsContacts.tblContactsAddresses.Rows.Add(nrowCA)

        If chkbxBilling.Checked Then
            If Not (txtField1.Text.Trim = "" And txtField2.Text.Trim = "" And txtField3.Text.Trim = "" And txtField4.Text.Trim = "") Then
                Dim nrowFinanceLabels As XSDContacts.tblContactsQASettingsRow = dsContacts.tblContactsQASettings.NewRow
                With nrowFinanceLabels
                    .CompanyID = CompanyID
                    .Field1Label = txtField1.Text
                    .Field1Value = txtFieldValue1.Text
                    .Field1Enabled = chkField1.Checked

                    .Field2Label = txtField2.Text
                    .Field2Value = txtFieldValue2.Text
                    .Field2Enabled = chkField2.Checked

                    .Field3Label = txtField3.Text
                    .Field3Value = txtFieldValue3.Text
                    .Field3Enabled = chkField3.Checked

                    .Field4Label = txtField4.Text
                    .Field4Value = txtFieldValue4.Text
                    .Field4Enabled = chkField4.Checked
                    .Type = "Finance"
                    .WOID = addressID
                End With
                dsContacts.tblContactsQASettings.Rows.Add(nrowFinanceLabels)
            End If
        End If
        Return dsContacts

    End Function

    ''' <summary>
    ''' Checking Depot Location
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckDepotLocation() As Boolean
        Dim IsDepot As Boolean
        IsDepot = False
        If chkbxDepot.Checked = False Then
            If Not IsNothing(Request("AddressID")) Then
                If Request("AddressID") <> "" Then
                    Dim ds As New DataSet
                    Dim LocationId As Integer
                    LocationId = CType(Request("AddressID"), Integer)
                    ds = ws.WSContact.CheckDepotLocation(LocationId)
                    If ds.Tables(0).Rows.Count > 0 Then
                        'If selected location is a depot location
                        If ds.Tables(0).Rows(0)("IsDepot") = True And ds.Tables(0).Rows(0)("IsBilling") = False Then
                            'If it has user assigned
                            If ds.Tables(1).Rows.Count > 0 Then
                                IsDepot = True
                                divValidationMain.Visible = True
                                lblError.Text = "A user of role type Depot has already been assigned to this location. Please reassign the user."
                            End If
                        End If
                    End If
                End If
            End If
        End If
        Return IsDepot
    End Function

    ''' <summary>
    ''' Method to check if main location exist
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Private Sub MainLocationExists_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles MainLocationExists.ServerValidate
        'commented as per the enhancement where if any new location is added with setting is main location then it is set as main and other as non-deault

        'If ChkLocation.Checked = True Then


        '    Dim dsAddress As DataSet
        '    dsAddress = GetContactLocation(_companyID, "AddressID", 0, 1000, 1, False)
        '    Dim dv As DataView
        '    'Locations
        '    dv = dsAddress.Tables(0).Copy.DefaultView
        '    dv.RowFilter = "Main = true"
        '    If dv.Count > 0 Then
        '        If CInt(Request("AddressID")) <> CInt(dv.Item(0).Item("AddressId")) Then
        '            args.IsValid = False
        '        End If
        '    End If
        'End If
    End Sub

    ''' <summary>
    ''' To handle confirm action
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Dim xmlContent As String = saveLocation().GetXml
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)
        Dim success As Integer
        Dim DoNotIncludeInOrdermatch As Boolean
        If chkOrderMatch.Checked = True Then
            DoNotIncludeInOrdermatch = 1
        Else
            DoNotIncludeInOrdermatch = 0
        End If
        success = ws.WSContact.AddEditLocation(xmlContent, ViewState("AddressId"), CommonFunctions.FetchVerNum(), Session("UserID"), DoNotIncludeInOrdermatch)
        If success = 1 Then
            Cache.Remove("ContactLocation" & "-" & CStr(_companyID) & "-" & Session.SessionID)
            Cache.Remove("LocationNamesList-" & _companyID)
            Dim IsAccountInfoUpdated As Boolean = False
            Dim ChangedName As String = ""
            Dim ChangedPostalCode As String = ""
            Dim ChangedAddress As String = ""
            Dim ChangedCity As String = ""
            Dim ChangedCounty As String = ""
            Dim ChangedPhone As String = ""
            Dim AdminContactName As String = ""
            Dim AdminContactEmail As String = ""
            If Not IsNothing(ViewState("Name")) Then
                If (ViewState("Name") <> txtName.Text.Trim) Then
                    IsAccountInfoUpdated = True
                    ChangedName = txtName.Text.Trim
                End If
            End If
            If Not IsNothing(ViewState("PostalCode")) Then
                If (ViewState("PostalCode") <> txtCompanyPostalCode.Text.Trim) Then
                    IsAccountInfoUpdated = True
                    ChangedPostalCode = txtCompanyPostalCode.Text.Trim
                End If
            End If
            If Not IsNothing(ViewState("Address")) Then
                If (ViewState("Address") <> txtCompanyAddress.Text.Trim) Then
                    IsAccountInfoUpdated = True
                    ChangedAddress = txtCompanyAddress.Text.Trim
                End If
            End If
            If Not IsNothing(ViewState("City")) Then
                If (ViewState("City") <> txtCity.Text.Trim) Then
                    IsAccountInfoUpdated = True
                    ChangedCity = txtCity.Text.Trim
                End If
            End If
            If Not IsNothing(ViewState("County")) Then
                If (ViewState("County") <> txtCounty.Text.Trim) Then
                    IsAccountInfoUpdated = True
                    ChangedCounty = txtCounty.Text.Trim
                End If
            End If
            If Not IsNothing(ViewState("Phone")) Then
                If (ViewState("Phone") <> txtCompanyPhone.Text.Trim) Then
                    IsAccountInfoUpdated = True
                    ChangedPhone = txtCompanyPhone.Text.Trim
                End If
            End If
            If (IsAccountInfoUpdated = True) Then
                If Not IsNothing(Session("CompanyAdminName")) Then
                    AdminContactName = Session("CompanyAdminName").ToString
                Else
                    AdminContactName = ViewState("ContactName")
                End If
                If Not IsNothing(Session("CompanyAdminEmail")) Then
                    AdminContactEmail = Session("CompanyAdminEmail").ToString
                Else
                    AdminContactEmail = ViewState("Email")
                End If
                'Emails.SendAccountInfoUpdate(AdminContactName, AdminContactEmail, "", "", "", ChangedName, ChangedPostalCode, ChangedAddress, ChangedCity, ChangedCounty, ChangedPhone, "", "", "")
                'Emails.SendAccountInfoUpdate("Sumit", "sumitk@iniquus.com", "", "", "", ChangedName, ChangedPostalCode, ChangedAddress, ChangedCity, ChangedCounty, ChangedPhone, "", "", "")
            End If
            CommonFunctions.IsValidLatLong(CommonFunctions.FormatPostcode(txtCompanyPostalCode.Text.Trim), "location", Request("CompanyID") + "-" + ViewState("AddressId"))
            Response.Redirect("Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizdivid") & "&classid=" & Request("classid"))
        ElseIf success = -10 Then
            pnlSubmitForm.Visible = True
            pnlConfirm.Visible = False
            lblError.Text = ResourceMessageText.GetString("AccountsVersionControlMsg").Replace("<Link>", "LocationForm.aspx?AddressId=" & ViewState("AddressId") & "&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId"))
            divValidationMain.Visible = True
        Else
            pnlSubmitForm.Visible = True
            pnlConfirm.Visible = False
            lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
            divValidationMain.Visible = True
        End If

    End Sub

    ''' <summary>
    ''' To handle cancel action on confirm
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlSubmitForm.Visible = True
        pnlConfirm.Visible = False
        If chkbxBilling.Checked Then
            pnlFinance.Style.Add("display", "inline")
        Else
            pnlFinance.Style.Add("display", "none")
        End If
    End Sub

    ''' <summary>
    ''' When clicked on check box Location
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ChkLocation_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkLocation.CheckedChanged
        If ChkLocation.Checked = True Then
            chkbxBilling.Checked = True
            chkbxBilling.Enabled = False
        Else
            chkbxBilling.Enabled = True
        End If
        setValidations()
    End Sub

    ''' <summary>
    ''' When clicked on IsDepot
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub chkbxDepot_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkbxDepot.CheckedChanged
        If chkbxDepot.Checked = True Then
            ChkLocation.Enabled = False
            ChkLocation.Checked = False
            'chkbxBilling.Checked = False
            'chkbxBilling.Enabled = False
            chkbxBilling.Enabled = True
            tblDepotLocDetails.Visible = True
            trDepotLocDetails.Style.Add("display", "inline")
        Else
            ChkLocation.Enabled = True
            'chkbxBilling.Enabled = True
            tblDepotLocDetails.Visible = False
            trDepotLocDetails.Style.Add("display", "none")
            If ViewState("AddressId") = 0 Then
                txtDepotDetails.Text = ""
            End If
        End If
        setValidations()
    End Sub

    Public Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        'Initialize the Viewstates on Page Load only
        If Not IsPostBack Then
            Session("AttemptCountLoc") = 0
            ViewState("PostcodeTrial1") = ""
        End If

        'If keyword is not blank, then allow user to lookup
        If txtCompanyPostalCode.Text.Trim <> "" Then

            'Lookup for address only if the postcode provided is different than previous one
            If ViewState("PostcodeTrial1") <> txtCompanyPostalCode.Text Then
                tblAddList.Visible = True
                CommonFunctions.callGetAddressList(txtCompanyPostalCode.Text.Trim, lblErr, lstProperties, txtCompanyPostalCode)
                ViewState("PostcodeTrial1") = txtCompanyPostalCode.Text
            End If
        Else
            tblAddList.Visible = True
            lstProperties.Visible = False
            lblErr.Visible = True
            lblErr.Text = ResourceMessageText.GetString("PostcodeNotAvailable")
        End If
    End Sub

    Private Sub lstProperties_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstProperties.SelectedIndexChanged
        Dim strSelectedItem As String = lstProperties.SelectedValue
        CommonFunctions.callPopulateAddress(lblErr, strSelectedItem, lstProperties, txtCompanyPostalCode, txtName, txtCompanyAddress, txtCity, txtCounty)

        If lblErr.Text = "" Then
            tblAddList.Visible = False
        Else
            tblAddList.Visible = True
        End If
    End Sub

    Private Sub btnSave_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ValidatePage()
    End Sub
    Private Sub chkField1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkField1.CheckedChanged
        setValidations()
    End Sub
    Private Sub chkField2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkField2.CheckedChanged
        setValidations()
    End Sub
    Private Sub chkField3_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkField3.CheckedChanged
        setValidations()
    End Sub
    Private Sub chkField4_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkField4.CheckedChanged
        setValidations()
    End Sub
    Private Sub setValidations()
        If chkField1.Checked Then
            reqfld1.Enabled = True
            reqFldLbl1.Enabled = True
            spanfld1.Style.Add("display", "inline")
            spanfldVal1.Style.Add("display", "inline")
        Else
            reqfld1.Enabled = False
            reqFldLbl1.Enabled = False
            spanfld1.Style.Add("display", "none")
            spanfldVal1.Style.Add("display", "none")
        End If
        If chkField2.Checked Then
            reqfld2.Enabled = True
            reqFldLbl2.Enabled = True
            spanfld2.Style.Add("display", "inline")
            spanfldVal2.Style.Add("display", "inline")
        Else
            reqfld2.Enabled = False
            reqFldLbl2.Enabled = False
            spanfld2.Style.Add("display", "none")
            spanfldVal2.Style.Add("display", "none")
        End If
        If chkField3.Checked Then
            reqfld3.Enabled = True
            reqFldLbl3.Enabled = True
            spanfld3.Style.Add("display", "inline")
            spanfldVal3.Style.Add("display", "inline")
        Else
            reqfld3.Enabled = False
            reqFldLbl3.Enabled = False
            spanfld3.Style.Add("display", "none")
            spanfldVal3.Style.Add("display", "none")
        End If
        If chkField4.Checked Then
            reqfld4.Enabled = True
            reqFldLbl4.Enabled = True
            spanfld4.Style.Add("display", "inline")
            spanfldVal4.Style.Add("display", "inline")
        Else
            reqfld4.Enabled = False
            reqFldLbl4.Enabled = False
            spanfld4.Style.Add("display", "none")
            spanfldVal4.Style.Add("display", "none")
        End If
        If chkbxBilling.Checked Then
            pnlFinance.Style.Add("display", "inline")
        Else
            pnlFinance.Style.Add("display", "none")
        End If
        divValidationMain.Visible = False
    End Sub
    Private Sub chkbxBilling_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkbxBilling.CheckedChanged
        If chkbxBilling.Checked Then
            pnlFinance.Visible = True
            setValidations()
        Else
            pnlFinance.Visible = False
            setValidations()
        End If

    End Sub

   
End Class