Partial Public Class UCMenuMainAdmin
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            If Not IsNothing(CType(Page.Cache("LoggedInfo-" & Page.Session("UserName") & Page.Session.SessionID), DataSet)) Then

                Dim ds As DataSet = CType(Cache("LoggedInfo-" & Session("UserName") & Session.SessionID), DataSet)
                Dim dv As DataView = ds.Tables(1).Copy.DefaultView
                Dim dvSub As DataView = ds.Tables(2).Copy.DefaultView
                dv.RowFilter = "ShowOnMenu = 1"
                Dim i, j As Integer
                Dim flagMain As Boolean = False
                Dim filepath As String = CType(CType(sender, UCMenuMainAdmin).Request, System.Web.HttpRequest).Path
                filepath = filepath.Substring(filepath.LastIndexOf("/") + 1, filepath.Length - (filepath.LastIndexOf("/") + 1))
                For i = 0 To dv.Count - 1
                    If dv.Item(i).Item("MenuLink").ToLower = filepath.ToLower Then
                        flagMain = True
                        Exit For
                    End If
                Next
                If flagMain = False Then
                    For j = 0 To dvSub.Count - 1
                        If dvSub.Item(j).Item("MenuLink").ToLower = filepath.ToLower Then
                            For i = 0 To dv.Count - 1
                                If dv.Item(i).Item("MenuID") = dvSub.Item(j).Item("ParentMenuID") Then
                                    Exit For
                                End If
                            Next
                            Exit For
                        End If
                    Next
                End If
                Dim k As Integer
                Dim str As String

                For k = 0 To dv.Count - 1
                    dvSub.RowFilter = "ShowOnMenu = 1 and ParentMenuID = " & dv.Item(k).Item("MenuID")
                    If flagMain = True Then
                        If k = i Then
                            str &= "<a id=" & dv.Item(k).Item("MenuID") & " class='MenuTxtSelected' href=" & dv.Item(k).Item("MenuLink") & " onMouseOver=" & Chr(34) & "javascript:try{hideall();show('div" & k + 1 & "', this," & dvSub.Count * 21.1 & ");}catch(error){}" & Chr(34) & " onMouseOut=" & Chr(34) & "javascript:try{hideSlowly();}catch(error){}" & Chr(34) & ">" & dv.Item(k).Item("MenuLabel") & "</a>" & Chr(13)
                        Else
                            str &= "<a id=" & dv.Item(k).Item("MenuID") & " class='MenuTxtBold' href=" & dv.Item(k).Item("MenuLink") & " onMouseOver=" & Chr(34) & "javascript:try{hideall();show('div" & k + 1 & "', this," & dvSub.Count * 21.1 & ");}catch(error){}" & Chr(34) & " onMouseOut=" & Chr(34) & "javascript:try{hideSlowly();}catch(error){}" & Chr(34) & ">" & dv.Item(k).Item("MenuLabel") & "</a>" & Chr(13)
                        End If
                    Else
                        str &= "<a id=" & dv.Item(k).Item("MenuID") & " class='MenuTxtBold' href=" & dv.Item(k).Item("MenuLink") & " onMouseOver=" & Chr(34) & "javascript:try{hideall();show('div" & k + 1 & "', this," & dvSub.Count * 21.1 & ");}catch(error){}" & Chr(34) & " onMouseOut=" & Chr(34) & "javascript:try{hideSlowly();}catch(error){}" & Chr(34) & ">" & dv.Item(k).Item("MenuLabel") & "</a>" & Chr(13)
                    End If
                Next
                ltMainMenu.Text = str

            Else
                Response.Redirect("Login.aspx")

            End If

            
        End If

    End Sub

End Class