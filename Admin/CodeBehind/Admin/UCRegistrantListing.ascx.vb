'Imports CodeLibrary
Partial Public Class UCRegistrantListing
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs
    Protected WithEvents Group1_GridPagerTop As GridPager
    Protected WithEvents Group1_GridPagerBtm As GridPager
    Protected WithEvents gvRegistrants As System.Web.UI.WebControls.GridView
    Delegate Sub DelPopulateObject(ByVal myInt As Integer, ByVal InitialCall As Boolean, ByVal PagerIndex As Integer)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Group1_delPopulate As New DelPopulateObject(AddressOf Group1_PopulateData)
        Group1_GridPagerTop.UpdateIndex = Group1_delPopulate
        Group1_GridPagerTop.PageSize = 10 '- Set this value as per your project requirement
        Group1_GridPagerTop.UCObj = Me
        Group1_GridPagerTop.HasSinglePager = False
        Group1_GridPagerTop.PagerGroup = "Group1"

        Group1_GridPagerBtm.UpdateIndex = Group1_delPopulate
        Group1_GridPagerBtm.PageSize = 10 ' - Set this value as per your project requirement
        Group1_GridPagerBtm.UCObj = Me
        Group1_GridPagerBtm.HasSinglePager = False
        Group1_GridPagerBtm.PagerGroup = "Group1"
        '/* DONOT CHANGE ANYTHING HERE */

        If Not IsPostBack() Then
            'For Paging Enabled
            '/* DONOT CHANGE ANYTHING HERE */
            Group1_GridPagerTop.StartIndex = 1
            Group1_GridPagerBtm.StartIndex = 1
            ViewState("SortExpression") = "FirstName" & " " & "ASC"
            ViewState("SortOrder") = "ASC"
            If Not (IsNothing(Request.QueryString("pn1"))) Then
                ViewState("Group1_PagerIndex") = CInt(Request.QueryString("pn1").Trim)
            Else
                ViewState("Group1_PagerIndex") = 0
            End If
            Group1_PopulateData(1, True, ViewState("Group1_PagerIndex"))
            '/* CREATE EXPORT TO EXCEL LINK*/
            Dim linkParams As String = ""
            linkParams &= "&SortExp=" & "FirstName"
            linkParams &= "&StartIndex=1"
            linkParams &= "&PageSize=" & Group1_GridPagerTop.TotalCount
            CType(Me.Parent.FindControl("btnExport"), HtmlAnchor).HRef = "ExportToExcel.aspx?page=webleads" & linkParams
            '/* DONOT CHANGE ANYTHING HERE */

        End If
    End Sub

#Region "Paging Methods"

    '/* FOR PAGING ENABLED DL - DONOT CHANGE ANYTHING HERE */
    Private Sub Group1_PopulateData(ByVal StartIndex As Integer, ByVal InitialCall As Boolean, ByVal PagerIndex As Integer)
        'Get the data, and send that to the Page:
        Dim ds As DataSet = Group1_GetData(StartIndex)
        Group1_GridPagerTop.TotalCount = ds.Tables(1).Rows(0).Item(0)
        Group1_GridPagerBtm.TotalCount = ds.Tables(1).Rows(0).Item(0)
        If InitialCall = True Then
            Group1_GridPagerTop.InitializePager()
            Group1_GridPagerBtm.InitializePager()
        End If

        Group1_GridPagerTop.SetPager(PagerIndex)
        Group1_GridPagerBtm.SetPager(PagerIndex)

        ViewState("Group1_PagerIndex") = PagerIndex
        ViewState("Group1_StartIndex") = StartIndex
        '/* DONOT CHANGE ANYTHING HERE */
        'Bing datasource to gridview here
        gvRegistrants.DataSource = ds.Tables(0)
        gvRegistrants.DataBind()
    End Sub

    '/* FOR PAGING ENABLED DL - FETCH DATA FUNCTION*/
    Function Group1_GetData(ByVal StartIndex As Integer) As DataSet
        Dim ContactID As Integer
        ContactID = Session("ContactID")
        If IsNothing(Session("ContactID")) Then
            ContactID = Request.QueryString("contactid")
        Else
            ContactID = Session("ContactID")

        End If
        Dim ds As DataSet
        Dim SortExpression As String = ViewState("SortExpression")
        ds = ws.WSContact.GetRegistrantListing(SortExpression, StartIndex, 10)
        'ds = SLLibrary.AuditionDB.GetAuditionListing(ViewState("Kwd"), ViewState("LID"), ViewState("MainCatID"), ViewState("SubCatID"), ViewState("ContactID"), IsAgency, GetInterest, GetLatest, ViewState("SortExpression"), StartIndex, Group1_GridPagerTop.PageSize)
        Return ds
    End Function
    Private Sub gvRegistrants_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvRegistrants.Sorting
        If (ViewState("SortOrder") = "ASC") Then
            ViewState("SortExpression") = e.SortExpression & " DESC"
            ViewState("SortOrder") = "DESC"
        Else
            ViewState("SortOrder") = "ASC"
            ViewState("SortExpression") = e.SortExpression & " ASC"
        End If
        Group1_PopulateData(Group1_GridPagerTop.StartIndex + 1, False, ViewState("PagerIndex"))
    End Sub
#End Region

End Class