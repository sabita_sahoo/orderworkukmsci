Imports System.Xml
Imports System.IO
Partial Public Class UCAdminProduct
    Inherits System.Web.UI.UserControl
    Public Shared ws As New WSObjs
    Protected WithEvents UCAccreditations As UCAccreditationForSearch
#Region "Properties"

    ''' <summary>
    ''' Workorder id. to check if populate the UC or to open a blank form
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOID As Integer = 0
    Public Property WOID() As Integer
        Get
            Return _WOID
        End Get
        Set(ByVal value As Integer)
            _WOID = value
        End Set
    End Property

    ''' <summary>
    ''' Workorder tracking id
    ''' </summary>
    ''' <remarks></remarks>
    Private _WOTrackingId As Integer = 0
    Public Property WOTrackingId() As Integer
        Get
            Return _WOTrackingId
        End Get
        Set(ByVal value As Integer)
            _WOTrackingId = value
        End Set
    End Property

    ''' <summary>
    ''' Field which decides whether to show or hod the review bids check box
    ''' </summary>
    ''' <remarks></remarks>
    Private _hasReviewBids As Boolean = True
    Public Property hasReviewBids() As Boolean
        Get
            Return _hasReviewBids
        End Get
        Set(ByVal value As Boolean)
            _hasReviewBids = value
        End Set
    End Property

    ''' <summary>
    ''' Company id of the user who has logged in
    ''' </summary>
    ''' <remarks></remarks>
    Private _CompanyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _CompanyID
        End Get
        Set(ByVal value As Integer)
            _CompanyID = value
        End Set
    End Property

    ''' <summary>
    ''' Contact id of the user who has logged in
    ''' </summary>
    ''' <remarks></remarks>
    Private _ContactID As Integer
    Public Property ContactID() As Integer
        Get
            Return _ContactID
        End Get
        Set(ByVal value As Integer)
            _ContactID = value
        End Set
    End Property

    ''' <summary>
    ''' Contact name of the logged in user
    ''' </summary>
    ''' <remarks></remarks>
    Private _ContactName As String
    Public Property ContactName() As String
        Get
            Return _ContactName
        End Get
        Set(ByVal value As String)
            _ContactName = value
        End Set
    End Property

    ''' <summary>
    ''' ClassID of the logged in user - 1 for Client/ 2 for Supplier
    ''' </summary>
    ''' <remarks></remarks>
    Private _ClassID As Integer
    Public Property ClassID() As Integer
        Get
            Return _ClassID
        End Get
        Set(ByVal value As Integer)
            _ClassID = value
        End Set
    End Property

    ''' <summary>
    ''' BizDivID
    ''' </summary>
    ''' <remarks></remarks>
    Private _BizDivID As Integer
    Public Property BizDivID() As Integer
        Get
            Return _BizDivID
        End Get
        Set(ByVal value As Integer)
            _BizDivID = value
        End Set
    End Property

    ''' <summary>
    ''' Property to check the source of the UC
    ''' </summary>
    ''' <remarks></remarks>
    Private _Src As String = ""
    Public Property Src() As String
        Get
            Return _Src
        End Get
        Set(ByVal value As String)
            _Src = value
        End Set
    End Property

    ''' <summary>
    ''' RoleGroup ID - to check whether the logged in user is an Administrator/Manager/Specialist/User
    ''' </summary>
    ''' <remarks></remarks>
    Private _RoleGroupID As Integer
    Public Property RoleGroupID() As Integer
        Get
            Return _RoleGroupID
        End Get
        Set(ByVal value As Integer)
            _RoleGroupID = value
        End Set
    End Property

    Private _AdminCompID As Integer = 0
    Public Property AdminCompID() As Integer
        Get
            Return _AdminCompID
        End Get
        Set(ByVal value As Integer)
            _AdminCompID = value
        End Set
    End Property

    Private _AdminConID As Integer = 0
    Public Property AdminConID() As Integer
        Get
            Return _AdminConID
        End Get
        Set(ByVal value As Integer)
            _AdminConID = value
        End Set
    End Property

    ''' <summary>
    ''' Viewer - of the UC - Admin / buyer
    ''' </summary>
    ''' <remarks></remarks>
    Private _Viewer As String
    Public Property Viewer() As String
        Get
            Return _Viewer
        End Get
        Set(ByVal value As String)
            _Viewer = value
        End Set
    End Property

    ''' <summary>
    ''' Field which decides whether to show or hide the Show Loc to Supplier check box
    ''' </summary>
    ''' <remarks></remarks>
    Private _ShowLocCheckBox As Boolean = True
    Public Property ShowLocCheckBox() As Boolean
        Get
            Return _ShowLocCheckBox
        End Get
        Set(ByVal value As Boolean)
            _ShowLocCheckBox = value
        End Set
    End Property

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblMessage.Text = ""

            Dim vat As Decimal
            vat = ApplicationSettings.VATPercentage
            hdnvatvalue.Value = vat / 100
            'Populate Wo details
            UCFileUpload6.ClearUCFileUploadCache()
            UCFileUpload6.CompanyID = Request("companyid")

            ViewState.Add("CompanyID", Request("companyid"))
            ViewState.Add("mode", Request.QueryString("mode"))
            'Populate Standards
            PopulateStandards()

            'Set the uc settings as per the viewer.


            'Initialize Attachments
            UCFileUpload6.BizDivID = BizDivID
            UCFileUpload6.Type = "WOProduct"
            UCFileUpload6.AttachmentForID = 0
            UCFileUpload6.CompanyID = Request("companyid")

            UCFileUpload6.ClearUCFileUploadCache()
            UCFileUpload6.PopulateAttachedFiles()




            If Not IsNothing(Request.QueryString("ProductID")) Then
                ViewState.Add("ProductID", Request.QueryString("ProductID"))
                lnkAddEditTimeBuilderQA.Visible = True
                lnkAddTBQRedirection.Visible = True
                tdCopyServiceTop.Visible = True
                tdCopyServiceBtm.Visible = True
                lnkAddEditTimeBuilderQA.HRef = "~\AddEditTimeBuilderQA.aspx?CompanyID=" & Request("companyid") & "&ProductID=" & Request.QueryString("ProductID")
                lnkAddTBQRedirection.HRef = "~/AddEditTimeBuilderRedirection.aspx?CompanyID=" & Request("CompanyID") & "&ProductID=" & Request.QueryString("ProductID")
            Else
                ViewState.Add("ProductID", 0)
                lnkAddEditTimeBuilderQA.Visible = False
                lnkAddTBQRedirection.Visible = False
                tdCopyServiceTop.Visible = False
                tdCopyServiceBtm.Visible = False
            End If

            Viewer = "Admin"


            If Not IsNothing(ViewState("ProductID")) Then
                'IF WOID <> 0 and Src is either site or Admin the populate the Workorder form from tblWOrkorder
                Dim ds As DataSet = ws.WSWorkOrder.populateProductDetails(ViewState("ProductID"), Request("companyid"), Request("bizdivid"))
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        populateProductDetails(ds)
                    Else
                        PopulateProductLocation(ds)
                    End If
                Else
                    PopulateProductLocation(ds)
                End If
            Else
                Dim ds As DataSet = ws.WSWorkOrder.populateProductDetails(0, Request("companyid"), Request("bizdivid"))
                PopulateProductLocation(ds)
            End If

            UCAccreditations.Type = "ServiceAccreditations"
            UCAccreditations.Mode = "ServiceAccreditations"
            UCAccreditations.BizDivId = BizDivID
            UCAccreditations.CommonID = ViewState("ProductID")

            'chkShowLoc.Visible = ShowLocCheckBox
        End If
    End Sub
    ''' <summary>
    ''' Function to create the cache for the attached files of the workorder
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="source"></param>
    ''' <param name="dsView"></param>
    ''' <remarks></remarks>
    Private Sub SetCacheForAttachment(ByVal ID As Integer, ByVal source As String, ByVal dsView As DataView)
        Dim Cachekey As String = ""
        Cachekey = "AttachedFiles-" & ID & "-" & source & "-" & Session.SessionID
        If Page.Cache(Cachekey) Is Nothing Then
            dsView.RowFilter = "LinkSource = '" & source & "'"
            Page.Cache.Add(Cachekey, dsView, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        End If
    End Sub

#Region "Action"

    ''' <summary>
    ''' Populate the Sub workorder type depending on the top level selected
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlWOType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWOType.SelectedIndexChanged
        Dim ds As DataSet
        If ddlWOType.SelectedValue <> "" Then
            ddlWOSubType.Enabled = True

            ds = CommonFunctions.GetCreateWorkRequestStandards(Me.Page, BizDivID, ContactID, WOID)
            Dim dv_WOType As DataView = ds.Tables("tblWorkOrderTypes").Copy.DefaultView
            dv_WOType.RowFilter = "CombId = " & ddlWOType.SelectedValue

            Dim dv_WOSubType As DataView = ds.Tables("tblWorkOrderSubTypes").Copy.DefaultView
            dv_WOSubType.RowFilter = "MainCatID = " & dv_WOType.Item(0).Item("MainCatID")
            dv_WOSubType.Sort = "Sequence"

            ddlWOSubType.DataTextField = "Name"
            ddlWOSubType.DataValueField = "CombId"
            ddlWOSubType.DataSource = dv_WOSubType
            ddlWOSubType.DataBind()
        Else
            ddlWOSubType.Items.Clear()
            ddlWOSubType.Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Function to reset the Workorder form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnResetTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetTop.Click, btnResetBtm.Click
        divValidationMain.Visible = False
        If ViewState("ProductID") <> 0 Then
            'Reset to initial terms
            Dim ds As DataSet = ws.WSWorkOrder.populateProductDetails(ViewState("ProductID"), Request("companyid"), Request("bizDivId"))
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    populateProductDetails(ds)
                Else
                    PopulateProductLocation(ds)
                End If
            Else
                PopulateProductLocation(ds)
            End If
        Else
            'Reset to blank form
            txtTitle.Text = ""
            txtSpendLimitWP.Text = 0.0
            txtCustPrice.Text = 0.0
            txtSpendLimitPP.Text = 0
            txtPPPerRate.Text = 0
            ddPPPerRate.SelectedValue = "Per Job"
            txtWOLongDesc.Text = ""
            txtSpecialInstructions.Text = ""
            txtInvoiceTitle.Text = ""
            PopulateStandards()
            ddlWOType.SelectedIndex = -1
            ddlWOSubType.SelectedIndex = -1
            drpdwnProductLocation.SelectedValue = 0
            txtStartDateDef.Text = "1"
            txtListingOrder.Text = ""
        End If
    End Sub

    ''' <summary>
    ''' On confirm click check for the Account setting for the user and accordingly show the Confirmation screen.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnConfirmBtm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmTop.Click, btnConfirmBtm.Click
        'If schedule window end date is not added, then schedule window begin date is equal to schedule window end date
        If ValidatingClientQuestions() Then

            setValidationForQA()
            Page.Validate()
            If Page.IsValid = True Then
                'Chaeck Service Sequence
                Dim IsSequenceAva As Boolean
                IsSequenceAva = CheckServiceSequence()
                If (IsSequenceAva = True) Then
                    divValidationMain.Visible = False
                    lblMessage.Text = ""
                    lblError.Text = ""
                    Dim ProductID As Integer
                    'Try
                    If ViewState("mode") = "edit" Then
                        ProductID = ViewState("ProductID")
                        updateWorkOrder(ProductID)
                        divValidationMain.Visible = False
                        lblError.Text = ""
                        lblMessage.Text = "Product Modified"
                        If Not IsNothing(Request.QueryString("pagename")) Then
                            If Request.QueryString("pagename") = "AccountService" Then
                                Response.Redirect("AccountServiceList.aspx?sender=productform")
                            Else
                                'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                                Response.Redirect("AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
                            End If
                        Else
                            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                            Response.Redirect("AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
                        End If
                    Else
                        saveWorkOrder()
                        divValidationMain.Visible = False
                        lblError.Text = ""
                        lblMessage.Text = "Product Saved"
                        If Not IsNothing(Request.QueryString("pagename")) Then
                            If Request.QueryString("pagename") = "AccountService" Then
                                Response.Redirect("AccountServiceList.aspx?sender=productform")
                            Else
                                'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                                Response.Redirect("AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
                            End If
                        Else
                            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                            Response.Redirect("AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
                        End If
                    End If
                    'Catch ex As Exception
                    '    Throw ex
                    'End Try

                Else
                    divValidationMain.Visible = True
                    lblError.Text = "The listing order entered is already occupied by another service.Please enter new listing order."
                End If
            Else
                divValidationMain.Visible = True
                reqfldQ1.Enabled = Qchk1.Checked
                reqfldQ2.Enabled = Qchk2.Checked
                reqfldQ3.Enabled = Qchk3.Checked
                reqfldQ4.Enabled = Qchk4.Checked
                reqfldQ5.Enabled = Qchk5.Checked
                If chkbxIsDeleted.Checked = True Then
                    txtListingOrder.Text = "0"
                    txtListingOrder.Enabled = False
                Else
                    txtListingOrder.Enabled = True
                End If
            End If
        End If
    End Sub
    Private Sub setValidationForQA()
        If Qchk1.Checked Then
            reqfldQ1.Enabled = True
            spanQ1.Style.Add("display", "inline")
            QChkMan1.Disabled = False
        Else
            reqfldQ1.Enabled = False
            spanQ1.Style.Add("display", "none")
            QChkMan1.Disabled = True
        End If
        If Qchk2.Checked Then
            reqfldQ2.Enabled = True
            spanQ2.Style.Add("display", "inline")
            QChkMan2.Disabled = False
        Else
            reqfldQ2.Enabled = False
            spanQ2.Style.Add("display", "none")
            QChkMan2.Disabled = True
        End If
        If Qchk3.Checked Then
            reqfldQ3.Enabled = True
            spanQ3.Style.Add("display", "inline")
            QChkMan3.Disabled = False
        Else
            reqfldQ3.Enabled = False
            spanQ3.Style.Add("display", "none")
            QChkMan3.Disabled = True
        End If
        If Qchk4.Checked Then
            reqfldQ4.Enabled = True
            spanQ4.Style.Add("display", "inline")
            QChkMan4.Disabled = False
        Else
            reqfldQ4.Enabled = False
            spanQ4.Style.Add("display", "none")
            QChkMan4.Disabled = True
        End If
        If Qchk5.Checked Then
            reqfldQ5.Enabled = True
            spanQ5.Style.Add("display", "inline")
            QChkMan5.Disabled = False
        Else
            reqfldQ5.Enabled = False
            spanQ5.Style.Add("display", "none")
            QChkMan5.Disabled = True
        End If
    End Sub

    ''' <summary>
    ''' Handles Cancel button at top
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancelTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTop.Click, btnCancelBtm.Click
        If Not IsNothing(Request.QueryString("pagename")) Then
            If Request.QueryString("pagename") = "AccountService" Then
                Response.Redirect("AccountServiceList.aspx?sender=productform")
            Else
                'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                Response.Redirect("AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
            End If
        Else
            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            Response.Redirect("AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
        End If
    End Sub




#End Region

#Region "Populate"

    ''' <summary>
    ''' Populate the category for the Create Similar or Edit workorders.
    ''' </summary>
    ''' <param name="CombID"></param>
    ''' <remarks></remarks>
    Private Sub populateCategory(ByVal CombID As Integer)
        Dim dsCat As DataSet
        dsCat = CommonFunctions.GetCreateWorkRequestStandards(Page, BizDivID, CompanyID, WOID)

        Dim dvSubCat As New DataView
        Dim dvMainCat As New DataView

        Dim mainID, subID As Integer
        mainID = 0
        subID = 0

        dvMainCat = dsCat.Tables("tblWorkOrderTypes").Copy.DefaultView
        dvSubCat = dsCat.Tables("tblWorkOrderSubTypes").Copy.DefaultView

        dvSubCat.RowFilter = "CombId = " & CombID
        If dvSubCat.Count <> 0 Then
            mainID = dvSubCat.Item(0).Item("MainCatID")
            subID = dvSubCat.Item(0).Item("SubCatID")
        End If

        dvMainCat.RowFilter = "CombId = " & CombID
        If dvMainCat.Count <> 0 Then
            mainID = dvMainCat.Item(0).Item("MainCatID")
        End If

        dvMainCat.RowFilter = ""
        ddlWOType.DataTextField = "Name"
        ddlWOType.DataValueField = "CombId"
        ddlWOType.DataSource = dvMainCat
        ddlWOType.DataBind()
        Dim li As New ListItem
        li = New ListItem
        li.Text = ResourceMessageText.GetString("WOCategoryList")
        li.Value = ""
        ddlWOType.Items.Insert(0, li)
        If mainID <> 0 Then
            dvMainCat.RowFilter = "MainCatID = " & mainID
            ddlWOType.SelectedIndex = ddlWOType.Items.IndexOf(ddlWOType.Items.FindByValue(dvMainCat.Item(0).Item("CombId")))
            ddlWOType.SelectedValue = dvMainCat.Item(0).Item("CombId")
        End If

        dvSubCat.RowFilter = "MainCatID = " & mainID
        If dvSubCat.Count <> 0 Then
            ddlWOSubType.DataTextField = "Name"
            ddlWOSubType.DataValueField = "CombId"
            ddlWOSubType.DataSource = dvSubCat
            ddlWOSubType.DataBind()
            ddlWOSubType.Items.Insert(0, li)
            If subID <> 0 Then
                dvSubCat.RowFilter = "MainCatID = " & mainID & "and SubCatID = " & subID
                ddlWOSubType.SelectedIndex = ddlWOSubType.Items.IndexOf(ddlWOSubType.Items.FindByValue(dvSubCat.Item(0).Item("CombId")))
                ddlWOSubType.SelectedValue = dvSubCat.Item(0).Item("CombId")
            End If
        End If

    End Sub

    ''' <summary>
    ''' Populate the Standards for create work request
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateStandards()
        Dim ds As DataSet
        ds = CommonFunctions.GetCreateWorkRequestStandards(Me.Page, BizDivID, ContactID, WOID, CInt(ViewState("CompanyID")), True)

        'Populate WorkOrder Types Level 1 Drop Down List
        Dim dv_WOType As DataView = ds.Tables("tblWorkOrderTypes").Copy.DefaultView
        dv_WOType.Sort = "Name"

        ddlWOType.DataTextField = "Name"
        ddlWOType.DataValueField = "CombId"
        ddlWOType.DataSource = dv_WOType
        ddlWOType.DataBind()


        'populate next available listing order 
        If (ds.Tables("tblNextAvOrder").Rows.Count > 0) Then
            txtListingOrder.Text = ds.Tables("tblNextAvOrder").Rows(0).Item("NextAvOrder")
        End If


        Dim li As New ListItem
        li = New ListItem
        li.Text = ResourceMessageText.GetString("WOCategoryList")
        li.Value = ""
        ddlWOType.Items.Insert(0, li)


        'populate Attachments 
        'WOID
        SetCacheForAttachment(WOID, "WOProduct", ds.Tables("tblAttachmentsWO").Copy.DefaultView)
    End Sub



    Private Sub populateProductDetails(ByVal ds As DataSet)

        If ds.Tables.Count > 0 Then

            UCAccreditations.PopulateSelectedGrid(True)

            Dim dt As DataTable
            dt = ds.Tables(0)

            If dt.Rows.Count > 0 Then

                txtTitle.Text = IIf(Not IsDBNull(dt.Rows(0).Item("ProductName")), dt.Rows(0).Item("ProductName"), "")
                txtInvoiceTitle.Text = IIf(Not IsDBNull(dt.Rows(0).Item("WOTitle")), dt.Rows(0).Item("WOTitle"), "")

                '****Still need to modify SP for handling WOCategoryID***
                populateCategory(dt.Rows(0).Item("WOCategoryID"))


                If Not IsDBNull(dt.Rows(0).Item("WOLongDesc")) Then
                    txtWOLongDesc.Text = dt.Rows(0).Item("WOLongDesc").Replace("<BR>", Chr(13))
                Else
                    txtWOLongDesc.Text = ""
                End If
                '''Code Added by Ambar
                ''' To populate the new field if it has text
                If dt.Rows(0).Item("ClientScope").ToString.Trim = "" Then
                    txtClientScope.Text = ""
                Else
                    txtClientScope.Text = dt.Rows(0).Item("ClientScope").Replace("<BR>", Chr(13))
                End If
                If (dt.Rows(0).Item("IsSignOffSheetReqd")) = True Then
                    chkForceSignOff.Checked = True
                Else
                    chkForceSignOff.Checked = False
                End If

                If IsDBNull(dt.Rows(0).Item("SpecialInstructions")) Then
                    txtSpecialInstructions.Text = ""
                Else
                    txtSpecialInstructions.Text = dt.Rows(0).Item("SpecialInstructions").Replace("<BR>", Chr(13))
                End If

                '****Check the functionality***
                'ViewState("WOStatus_Edit") = dt.Rows(0).Item("WOStatus")
                If Not IsDBNull(dt.Rows(0).Item("FreesatService")) Then
                    chkBoxFreesatService.Checked = dt.Rows(0).Item("FreesatService")
                Else
                    chkBoxFreesatService.Checked = False
                End If
                If Not IsDBNull(dt.Rows(0).Item("ReviewBids")) Then
                    chkReviewBids.Checked = dt.Rows(0).Item("ReviewBids")
                Else
                    chkReviewBids.Checked = False
                End If
                Dim vatrate As Double
                vatrate = ApplicationSettings.VATPercentage

                'Wholesale Price
                If Not IsDBNull(ds.Tables(0).Rows(0).Item("WholesalePrice")) Then
                    lblWOincludvat.Text = FormatNumber(ds.Tables(0).Rows(0).Item("WholesalePrice"), 3, TriState.True, TriState.False, TriState.False)
                    hdnWPvatvalue.Value = FormatNumber(ds.Tables(0).Rows(0).Item("WholesalePrice"), 3, TriState.True, TriState.False, TriState.False)
                    txtSpendLimitWP.Text = FormatNumber(((Convert.ToDouble(lblWOincludvat.Text)) + (Convert.ToDouble(lblWOincludvat.Text) * (vatrate) / 100)), 2, TriState.True, TriState.False, TriState.False)
                    'If ds.Tables(0).Rows(0).Item("WholesalePrice") = 0 Then

                    '    txtSpendLimitWP.Enabled = False

                    'End If
                Else
                    lblWOincludvat.Text = FormatNumber(0, 3, TriState.True, TriState.False, TriState.False)
                End If
                'Customer Price
                If Not IsDBNull(ds.Tables(0).Rows(0).Item("ExCustomerPrice")) Then
                    lblCPincludvat.Text = FormatNumber(ds.Tables(0).Rows(0).Item("ExCustomerPrice"), 3, TriState.True, TriState.False, TriState.False)
                    hdnCPvatvalue.Value = FormatNumber(ds.Tables(0).Rows(0).Item("ExCustomerPrice"), 3, TriState.True, TriState.False, TriState.False)
                    txtCustPrice.Text = FormatNumber(((Convert.ToDouble(lblCPincludvat.Text)) + (Convert.ToDouble(lblCPincludvat.Text) * (vatrate) / 100)), 2, TriState.True, TriState.False, TriState.False)
                Else
                    lblCPincludvat.Text = FormatNumber(0, 3, TriState.True, TriState.False, TriState.False)
                End If
                'Proposed Price
                If Not IsDBNull(ds.Tables(0).Rows(0).Item("PlatformPrice")) Then
                    txtSpendLimitPP.Text = FormatNumber(ds.Tables(0).Rows(0).Item("PlatformPrice"), 2, TriState.True, TriState.False, TriState.False)
                    'If ds.Tables(0).Rows(0).Item("PlatformPrice") = 0 Then
                    '    txtSpendLimitPP.Enabled = False
                    'End If
                Else
                    txtSpendLimitPP.Text = FormatNumber(0, 2, TriState.True, TriState.False, TriState.False)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(0).Item("PPPerRate")) Then
                    txtPPPerRate.Text = FormatNumber(ds.Tables(0).Rows(0).Item("PPPerRate"), 2, TriState.True, TriState.False, TriState.False)
                Else
                    txtPPPerRate.Text = FormatNumber(0, 2, TriState.True, TriState.False, TriState.False)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(0).Item("PerRate")) Then
                    ddPPPerRate.SelectedValue = ds.Tables(0).Rows(0).Item("PerRate")
                End If
                If Not IsDBNull(dt.Rows(0).Item("ServicePartnerFees")) Then
                    txtSPFee.Text = dt.Rows(0).Item("ServicePartnerFees")
                End If
                If Not IsDBNull(dt.Rows(0).Item("ApplyToPreferredSP")) Then
                    chkPrefSP.Checked = dt.Rows(0).Item("ApplyToPreferredSP")
                End If
                'For Populating Saturday Def - Faizan
                If Not IsDBNull(dt.Rows(0).Item("SaturdayDefferal")) Then
                    txtsatdef.Text = dt.Rows(0).Item("SaturdayDefferal")
                End If

                'Populating StartDateDeferral
                Dim myResult As Double
                myResult = CInt(ds.Tables(0).Rows(0).Item("StartDateDeferral")) Mod 24
                If (myResult <> 0) Then
                    ddStartDefTime.SelectedValue = "Hours"
                    txtStartDateDef.Text = ds.Tables(0).Rows(0).Item("StartDateDeferral")
                Else
                    ddStartDefTime.SelectedValue = "Days"
                    txtStartDateDef.Text = CInt(ds.Tables(0).Rows(0).Item("StartDateDeferral")) / 24
                End If

                'populate Listing order
                txtListingOrder.Text = ds.Tables(0).Rows(0).Item("Sequence")


                'for admin
                ViewState("BizDivID") = dt.Rows(0).Item("BizDivID")
                ViewState("ProductID") = dt.Rows(0).Item("ProductID")

                If ds.Tables(0).Rows(0).Item("IsDeleted") = True Then
                    chkbxIsDeleted.Checked = True
                    txtListingOrder.Enabled = False
                Else
                    chkbxIsDeleted.Checked = False
                    txtListingOrder.Enabled = True
                End If
                If ds.Tables(0).Rows(0).Item("IsHidden") = True Then
                    chkIsHidden.Checked = True
                Else
                    chkIsHidden.Checked = False
                End If
                If ds.Tables(0).Rows(0).Item("IsWaitingToAutomatch") = True Then
                    chkIsWaitingToAutomatch.Checked = True
                Else
                    chkIsWaitingToAutomatch.Checked = False
                End If
                PopulateProductLocation(ds)
                'pick this code start
                Dim dvContactsQA As New DataView(ds.Tables(2).Copy)
                dvContactsQA.RowFilter = "Type = 'ClientQuestion'"
                If dvContactsQA.Count > 0 Then
                    txtQ1.Text = dvContactsQA.Item(0)("Field1Label")
                    txtQ2.Text = dvContactsQA.Item(0)("Field2Label")
                    txtQ3.Text = dvContactsQA.Item(0)("Field3Label")
                    txtQ4.Text = dvContactsQA.Item(0)("Field4Label")
                    txtQ5.Text = dvContactsQA.Item(0)("Field5Label")

                    Qchk1.Checked = dvContactsQA.Item(0)("Field1Enabled")
                    Qchk2.Checked = dvContactsQA.Item(0)("Field2Enabled")
                    Qchk3.Checked = dvContactsQA.Item(0)("Field3Enabled")
                    Qchk4.Checked = dvContactsQA.Item(0)("Field4Enabled")
                    Qchk5.Checked = dvContactsQA.Item(0)("Field5Enabled")

                    QChkMan1.Checked = dvContactsQA.Item(0)("IsField1LabelMandatory")
                    QChkMan2.Checked = dvContactsQA.Item(0)("IsField2LabelMandatory")
                    QChkMan3.Checked = dvContactsQA.Item(0)("IsField3LabelMandatory")
                    QChkMan4.Checked = dvContactsQA.Item(0)("IsField4LabelMandatory")
                    QChkMan5.Checked = dvContactsQA.Item(0)("IsField5LabelMandatory")

                    If Qchk1.Checked Then
                        reqfldQ1.Enabled = True
                        spanQ1.Style.Add("display", "inline")
                        QChkMan1.Disabled = False
                    End If
                    If Qchk2.Checked Then
                        reqfldQ2.Enabled = True
                        spanQ2.Style.Add("display", "inline")
                        QChkMan2.Disabled = False
                    End If
                    If Qchk3.Checked Then
                        reqfldQ3.Enabled = True
                        spanQ3.Style.Add("display", "inline")
                        QChkMan3.Disabled = False
                    End If
                    If Qchk4.Checked Then
                        reqfldQ4.Enabled = True
                        spanQ4.Style.Add("display", "inline")
                        QChkMan4.Disabled = False
                    End If
                    If Qchk5.Checked Then
                        reqfldQ5.Enabled = True
                        spanQ5.Style.Add("display", "inline")
                        QChkMan5.Disabled = False
                    End If
                End If
                dvContactsQA.RowFilter = ""
                dvContactsQA.RowFilter = "Type = 'ServiceQuestion'"
                If dvContactsQA.Count > 0 Then
                    Dim ClientQCount As Integer = 0
                    If (dvContactsQA.Item(0)("Field1Label") <> "") Then
                        ClientQCount = ClientQCount + 1
                    End If
                    If (dvContactsQA.Item(0)("Field2Label") <> "") Then
                        ClientQCount = ClientQCount + 1
                    End If
                    If (dvContactsQA.Item(0)("Field3Label") <> "") Then
                        ClientQCount = ClientQCount + 1
                    End If
                    If (dvContactsQA.Item(0)("Field4Label") <> "") Then
                        ClientQCount = ClientQCount + 1
                    End If
                    If (dvContactsQA.Item(0)("Field5Label") <> "") Then
                        ClientQCount = ClientQCount + 1
                    End If
                    If (dvContactsQA.Item(0)("Field6Label") <> "") Then
                        ClientQCount = ClientQCount + 1
                    End If
                    If (dvContactsQA.Item(0)("Field7Label") <> "") Then
                        ClientQCount = ClientQCount + 1
                    End If
                    If (dvContactsQA.Item(0)("Field8Label") <> "") Then
                        ClientQCount = ClientQCount + 1
                    End If
                    If (dvContactsQA.Item(0)("Field9Label") <> "") Then
                        ClientQCount = ClientQCount + 1
                    End If
                    If (dvContactsQA.Item(0)("Field10Label") <> "") Then
                        ClientQCount = ClientQCount + 1
                    End If
                    Session("ClientQ") = dvContactsQA
                    ViewState.Add("ClientQCount", ClientQCount)
                    hdnQuestionCount.Value = Convert.ToInt32(ViewState("ClientQCount"))
                    PopulateClientQRepeater(ViewState("ClientQCount"), dvContactsQA)
                    'PopulateClientQRepeater(ViewState("ClientQCount"))
                Else
                    ViewState.Add("ClientQCount", 0)
                    hdnQuestionCount.Value = Convert.ToInt32(ViewState("ClientQCount"))
                    Session("ClientQ") = Nothing
                End If
                'pick this code end
                'Attachment cache kill
                'UCFileUpload6.CompanyID = CompanyID
                'UCFileUpload6.ClearUCFileUploadCache()


                'Populate Attachements 
                'Populate Attachements 
                UCFileUpload6.BizDivID = BizDivID
                UCFileUpload6.Type = "WOProduct"
                UCFileUpload6.AttachmentForID = dt.Rows(0).Item("ProductID")
                UCFileUpload6.AttachmentForSource = "WOProduct"

                UCFileUpload6.CompanyID = Request("companyid")
                UCFileUpload6.ExistAttachmentSourceID = Request("companyid")
                UCFileUpload6.ClearUCFileUploadCache()
                UCFileUpload6.PopulateAttachedFiles()
                'CWOFileAttach.PopulateExistingFiles()
                'UCFileUpload6.AttachmentForSource = "WorkOrder"
                'UCFileUpload6.AttachmentForID = 0
            End If

        End If

    End Sub

    ''' <summary>
    ''' Populate Billing Location
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <remarks></remarks>
    Public Sub PopulateProductLocation(ByVal ds As DataSet) 'Populate Billing Location in the drop down list
        Dim li As New ListItem
        li = New ListItem
        li.Text = "Select Service Location"
        li.Value = 0
        If ds.Tables.Count > 0 Then
            'Poonam modified on 4/9/2015 - Task - EA/OA-101 :EA - Add Billing location pick list to the /AdminProduct.aspx?mode=add
            drpdwnProductLocation.Visible = True
            lbltxtBillingLocation.Visible = True
            drpdwnProductLocation.DataSource = ds.Tables(1)
            drpdwnProductLocation.DataTextField = "LocationName"
            drpdwnProductLocation.DataValueField = "AddressID"
            drpdwnProductLocation.DataBind()
            drpdwnProductLocation.Items.Insert(0, li)
            ViewState.Add("BillingLocation", "Visible")
        End If
        Dim dt As DataTable
        If ds.Tables(0).Rows.Count > 0 Then
            dt = ds.Tables(0)
            If dt.Rows(0)("LocationID") <> 0 Then
                drpdwnProductLocation.SelectedValue = dt.Rows(0)("LocationID")
            End If
        End If
    End Sub

#End Region

#Region "Validations"


    ''' <summary>
    ''' Validation function which checks if OW rep provided the PO Number or the Statement Of Works
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Public Function checkForBuyerAcceptedStatus() As Boolean
        Dim attachFile As Boolean = False
        Dim tempRepeater As Repeater

        'Statement OF Works
        tempRepeater = CType(UCFileUpload6.FindControl("rptAttList"), Repeater)
        If tempRepeater.Items.Count > 0 Then
            attachFile = True
        End If

        If (attachFile = True) Then
            'PO Number or Statement of works attachment provided
            Return True
        Else
            Return False
        End If
    End Function

    Private Function CheckServiceSequence() As Boolean
        Dim WOID As Integer
        If ViewState("mode") = "edit" Then
            WOID = ViewState("ProductID")
        Else
            WOID = 0
        End If


        Dim ds As New DataSet

        ds = ws.WSWorkOrder.CheckServiceSequence(CInt(ViewState("CompanyID")), CInt(txtListingOrder.Text), WOID)
        If ds.Tables(0).Rows.Count > 0 Then
            If (ds.Tables(0).Rows(0).Item("Status") = "1") Then
                Return True
            Else
                Return False
            End If
        End If


    End Function


#End Region

#Region "Save"

    ''' <summary>
    ''' Function to save the workorder details as draft
    ''' </summary>
    ''' <remarks></remarks>
    Private Function saveWorkOrder() As DataSet
        ViewState("WOID") = 0
        Dim dsWorkOrder As New WorkOrderTemp

        'Updating workorder tracking table
        Dim nrowWO As WorkOrderTemp.tblWorkOrderTemplatesRow = dsWorkOrder.tblWorkOrderTemplates.NewRow
        With nrowWO
            .BizDivID = BizDivID
            .WOID = 0
            .DateCreated = Date.Now

            If ddlWOSubType.SelectedValue <> "" Then
                .WOCategoryID = ddlWOSubType.SelectedValue
            Else
                If ddlWOType.SelectedValue <> "" Then
                    .WOCategoryID = ddlWOType.SelectedValue
                Else
                    .WOCategoryID = 0
                End If

            End If
            If Not IsNothing(ViewState("BillingLocation")) Then
                If ViewState("BillingLocation") = "Visible" Then
                    If drpdwnProductLocation.SelectedItem.Value <> 0 Then
                        .Location = drpdwnProductLocation.SelectedItem.Value
                    Else
                        .Location = 0
                    End If
                Else
                    .Location = 0
                End If
            Else
                .Location = 0
            End If
            ' Code Added By Ambar 
            ' To save the content of the new field
            If txtClientScope.Text.Trim <> "" Then
                .ClientScope = txtClientScope.Text.Trim.Replace(Chr(13), "<BR>")
            End If
            'Product Name will be saved
            .WOTitle = txtTitle.Text.Replace("""", "")
            .WOShortDesc = ""
            .WOLongDesc = txtWOLongDesc.Text.Replace(Chr(13), "<BR>")
            .SpecialInstructions = txtSpecialInstructions.Text.Replace(Chr(13), "<BR>")
            .WOStatus = ViewState("WOStatus")
            .EstimatedTime = 0
            'Product Title will be saved
            .InvoiceTitle = txtInvoiceTitle.Text

            'ContactId & CompanyId
            .CompanyID = ViewState("CompanyID")
            ' ''.ContactId = 0

            '' ''Buyer Accepted
            ' ''If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Draft Then
            ' ''    'Work Request - Saved as draft then Buyer Accepted = False, Wholesale Price = WO Value
            ' ''    .BuyerAccepted = False
            ' ''ElseIf (ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted) Then
            ' ''    'Work Request - Submitted with RAQ or Fixed Price
            ' ''    .BuyerAccepted = True
            ' ''End If


            'Wholesale price & Platform Price
            .WholesalePrice = CDec(hdnWPvatvalue.Value)
            .CustomerPrice = CDec(hdnCPvatvalue.Value)
            .PlatformPrice = CDec(txtSpendLimitPP.Text)

            If (txtStartDateDef.Text <> "") Then
                If (ddStartDefTime.SelectedValue <> "Days") Then
                    .StartDateDeferral = CInt(txtStartDateDef.Text)
                Else
                    .StartDateDeferral = CInt(txtStartDateDef.Text) * 24
                End If
            Else
                .StartDateDeferral = 24
            End If
            .Sequence = CInt(txtListingOrder.Text)
            'Show loc to Supplier
            '.ShowLocToSupplier = chkShowLoc.Checked
            .FreesatService = chkBoxFreesatService.Checked
            .ServicePartnerFees = txtSPFee.Text
            .ApplyToPreferredSP = chkPrefSP.Checked
            .SaturdayDefferal = txtsatdef.Text.Trim
            .ReviewBids = chkReviewBids.Checked


        End With
        dsWorkOrder.tblWorkOrderTemplates.Rows.Add(nrowWO)

        'Updating the work tracking table
        Dim nrowTracking As WorkOrderTemp.tblWOTrackingRow = dsWorkOrder.tblWOTracking.NewRow
        With nrowTracking
            .EstimatedTime = 0
            .BizDivID = BizDivID
            .WOTrackingId = 0
            .WOID = 0
            .DateCreated = Date.Now

            .CompanyID = 0
            .ContactID = 0
            .ContactClassID = ApplicationSettings.RoleOWID

            'WOvalue = Wholesale Price
            .Value = CDec(hdnWPvatvalue.Value)
            .Status = 0
            .Comments = ViewState("Comments")
            .ActualTime = ""
            .DateModified = Date.Now
            .SpecialistID = 0
        End With
        dsWorkOrder.tblWOTracking.Rows.Add(nrowTracking)

        'Updating the TagOtherLinkage table
        Dim ds As DataSet = UCAccreditations.GetSelectedAccred()
        For Each drowAccreds As DataRow In ds.Tables(0).Rows
            Dim nrowTagOtherLinkage As WorkOrderTemp.TagOtherLinkageRow = dsWorkOrder.TagOtherLinkage.NewRow
            With nrowTagOtherLinkage
                .TagID = drowAccreds("TagId")
            End With
            dsWorkOrder.TagOtherLinkage.Rows.Add(nrowTagOtherLinkage)
        Next

        'Attach attachments with th workorder
        'dsWorkOrder = CWOFileAttach.ReturnFilledAttachments(dsWorkOrder)

        dsWorkOrder = UCFileUpload6.ReturnFilledAttachments(dsWorkOrder)



        Dim SpecialistSuppliesPartsBool As Integer = 0
        'Add code for adding Location and Isdeleted Here
        Dim IsDeleted As Boolean
        If chkbxIsDeleted.Checked = True Then
            IsDeleted = True
        Else
            IsDeleted = False
        End If
        Dim IsHidden As Boolean
        If chkIsHidden.Checked = True Then
            IsHidden = True
        Else
            IsHidden = False
        End If
        Dim IsWaitingToAutomatch As Boolean
        If chkIsWaitingToAutomatch.Checked = True Then
            IsWaitingToAutomatch = True
        Else
            IsWaitingToAutomatch = False
        End If
        Dim PPerRate As Decimal
        If (txtPPPerRate.Text.Trim <> "") Then
            PPerRate = CDec(txtPPPerRate.Text.Trim)
        Else
            PPerRate = CDec(txtSpendLimitPP.Text)
        End If

        Dim Location As Integer
        If Not IsNothing(ViewState("BillingLocation")) Then
            If ViewState("BillingLocation") = "Visible" Then
                Location = drpdwnProductLocation.SelectedItem.Value
            Else
                Location = 0
            End If
        Else
            Location = 0
        End If
        Dim IsSignOffSheetReqd As Boolean
        If chkForceSignOff.Checked = True Then
            IsSignOffSheetReqd = 1
        Else
            IsSignOffSheetReqd = 0
        End If

        Dim xmlContent As String = dsWorkOrder.GetXml
        Dim xmlContentMail As String = dsWorkOrder.GetXml
        'Remove XML namespace attribute and its value
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)
        'This code is added by pankaj malav on 10 oct 2008 such that each work order attachment will be saved as woproduct in case of Product
        xmlContent = xmlContent.Replace("<LinkSource>WorkOrder</LinkSource>", "<LinkSource>WOProduct</LinkSource>")
        Dim xmlQA As String = GetQASettings(0)
        Dim ReturnVal As Integer = ws.WSWorkOrder.AddUpdateAdminProduct(xmlContent, "add", Location, IsDeleted, xmlQA, IsSignOffSheetReqd, IsHidden, PPerRate, ddPPPerRate.SelectedValue, IsWaitingToAutomatch)
        'This code is added by pankaj malav on 10 oct 2008 such that each work order attachment will be saved as woproduct in case of Product
        UCFileUpload6.ClearUCFileUploadCache()
    End Function

    ''' <summary>
    ''' Function to update the workorder details as draft
    ''' </summary>
    ''' <remarks></remarks>
    Private Function updateWorkOrder(ByVal WOID As Integer) As DataSet
        Dim dsWorkOrder As New WorkOrderTemp

        'Updating workorder tracking table
        Dim nrowWO As WorkOrderTemp.tblWorkOrderTemplatesRow = dsWorkOrder.tblWorkOrderTemplates.NewRow
        With nrowWO
            .BizDivID = BizDivID
            .WOID = WOID
            .DateCreated = Date.Now

            If ddlWOSubType.SelectedValue <> "" Then
                .WOCategoryID = ddlWOSubType.SelectedValue
            Else
                If ddlWOType.SelectedValue <> "" Then
                    .WOCategoryID = ddlWOType.SelectedValue
                Else
                    .WOCategoryID = 0
                End If

            End If

            If Not IsNothing(ViewState("BillingLocation")) Then
                If ViewState("BillingLocation") = "Visible" Then
                    If drpdwnProductLocation.SelectedItem.Value <> 0 Then
                        .Location = drpdwnProductLocation.SelectedItem.Value
                    Else
                        .Location = 0
                    End If
                Else
                    .Location = 0
                End If
            Else
                .Location = 0
            End If
            .WOTitle = txtTitle.Text.Replace("""", "")
            .WOShortDesc = ""
            .WOLongDesc = txtWOLongDesc.Text.Replace(Chr(13), "<BR>")
            .ClientScope = txtClientScope.Text.Replace(Chr(13), "<BR>")
            .SpecialInstructions = txtSpecialInstructions.Text.Replace(Chr(13), "<BR>")
            .WOStatus = ViewState("WOStatus")
            'Product Title
            .InvoiceTitle = txtInvoiceTitle.Text
            .CompanyID = ViewState("CompanyID")


            '' ''Buyer Accepted
            ' ''If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Draft Then
            ' ''    'Work Request - Saved as draft then Buyer Accepted = False, Wholesale Price = WO Value
            ' ''    .BuyerAccepted = False
            ' ''ElseIf (ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted) Then
            ' ''    'Work Request - Submitted with RAQ or Fixed Price
            ' ''    .BuyerAccepted = True
            ' ''End If

            'Wholesale price & Platform Price
            .WholesalePrice = CDec(hdnWPvatvalue.Value)
            .CustomerPrice = CDec(hdnCPvatvalue.Value)
            .PlatformPrice = CDec(txtSpendLimitPP.Text)
            If (txtStartDateDef.Text <> "") Then
                If (ddStartDefTime.SelectedValue <> "Days") Then
                    .StartDateDeferral = CInt(txtStartDateDef.Text)
                Else
                    .StartDateDeferral = CInt(txtStartDateDef.Text) * 24
                End If
            Else
                .StartDateDeferral = 24
            End If
            .Sequence = CInt(txtListingOrder.Text)
            'Show loc to Supplier
            '.ShowLocToSupplier = chkShowLoc.Checked

            .FreesatService = chkBoxFreesatService.Checked
            .ServicePartnerFees = txtSPFee.Text
            .ApplyToPreferredSP = chkPrefSP.Checked
            .SaturdayDefferal = txtsatdef.Text.Trim
            .ReviewBids = chkReviewBids.Checked
        End With
        dsWorkOrder.tblWorkOrderTemplates.Rows.Add(nrowWO)

        'Updating the work tracking table
        Dim nrowTracking As WorkOrderTemp.tblWOTrackingRow = dsWorkOrder.tblWOTracking.NewRow
        With nrowTracking
            .EstimatedTime = 0
            .BizDivID = BizDivID
            .WOTrackingId = 0
            .WOID = WOID
            .DateCreated = Date.Now


            .CompanyID = 0
            .ContactID = 0
            .ContactClassID = ApplicationSettings.RoleOWID
            .Value = CDec(hdnWPvatvalue.Value)

            .Status = 0

            .Comments = ViewState("Comments")
            .ActualTime = ""
            .DateModified = Date.Now
            .SpecialistID = 0
        End With
        dsWorkOrder.tblWOTracking.Rows.Add(nrowTracking)

        'WorkOrder(attachments)
        'dsWorkOrder = CWOFileAttach.ReturnFilledAttachments(dsWorkOrder)

        'Updating the TagOtherLinkage table
        Dim ds As DataSet = UCAccreditations.GetSelectedAccred()
        For Each drowAccreds As DataRow In ds.Tables(0).Rows
            Dim nrowTagOtherLinkage As WorkOrderTemp.TagOtherLinkageRow = dsWorkOrder.TagOtherLinkage.NewRow
            With nrowTagOtherLinkage
                .TagID = drowAccreds("TagId")
            End With
            dsWorkOrder.TagOtherLinkage.Rows.Add(nrowTagOtherLinkage)
        Next

        'For OW Admin WOProduct Attachments
        dsWorkOrder = UCFileUpload6.ReturnFilledAttachments(dsWorkOrder)

        Dim SpecialistSuppliesPartsBool As Integer = 0
        Dim IsDeleted As Boolean
        If chkbxIsDeleted.Checked = True Then
            IsDeleted = True
        Else
            IsDeleted = False
        End If
        Dim IsHidden As Boolean
        If chkIsHidden.Checked = True Then
            IsHidden = True
        Else
            IsHidden = False
        End If
        Dim IsWaitingToAutomatch As Boolean
        If chkIsWaitingToAutomatch.Checked = True Then
            IsWaitingToAutomatch = True
        Else
            IsWaitingToAutomatch = False
        End If
        Dim IsSignOffSheetReqd As Boolean
        If chkForceSignOff.Checked = True Then
            IsSignOffSheetReqd = 1
        Else
            IsSignOffSheetReqd = 0
        End If

        Dim PPerRate As Decimal
        If (txtPPPerRate.Text.Trim <> "") Then
            PPerRate = CDec(txtPPPerRate.Text.Trim)
        Else
            PPerRate = CDec(txtSpendLimitPP.Text)
        End If

        Dim Location As Integer
        If Not IsNothing(ViewState("BillingLocation")) Then
            If ViewState("BillingLocation") = "Visible" Then
                Location = drpdwnProductLocation.SelectedItem.Value
            Else
                Location = 0
            End If
        Else
            Location = 0
        End If
        Dim xmlContent As String = dsWorkOrder.GetXml
        Dim xmlContentMail As String = dsWorkOrder.GetXml
        'Remove XML namespace attribute and its value
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)
        xmlContent = xmlContent.Replace("<LinkSource>WorkOrder</LinkSource>", "<LinkSource>WOProduct</LinkSource>")
        Dim xmlQA As String = GetQASettings(WOID)

        Dim ReturnVal As Integer = ws.WSWorkOrder.AddUpdateAdminProduct(xmlContent, "edit", Location, IsDeleted, xmlQA, IsSignOffSheetReqd, IsHidden, PPerRate, ddPPPerRate.SelectedValue, IsWaitingToAutomatch)

        'This code is added by pankaj malav on 10 oct 2008 such that each work order attachment will be saved as woproduct in case of Product
        UCFileUpload6.ClearUCFileUploadCache()

    End Function
    Public Function GetQASettings(ByVal WOID As Integer, Optional ByVal QuestionToRemove As Integer = 0, Optional ByVal dvSequence As DataView = Nothing, Optional ByVal IsSequenceChange As Boolean = False) As String
        'pick this code start
        Dim dsQA As New OWContactsQASettings()
        Dim nrowQALabels As OWContactsQASettings.tblContactsQASettingsRow = dsQA.tblContactsQASettings.NewRow
        With nrowQALabels
            .CompanyID = ViewState("CompanyID")
            .Field1Label = txtQ1.Text
            .Field1Value = ""
            .Field1Enabled = Qchk1.Checked
            .IsField1LabelMandatory = QChkMan1.Checked

            .Field2Label = txtQ2.Text
            .Field2Value = ""
            .Field2Enabled = Qchk2.Checked
            .IsField2LabelMandatory = QChkMan2.Checked

            .Field3Label = txtQ3.Text
            .Field3Value = ""
            .Field3Enabled = Qchk3.Checked
            .IsField3LabelMandatory = QChkMan3.Checked

            .Field4Label = txtQ4.Text
            .Field4Value = ""
            .Field4Enabled = Qchk4.Checked
            .IsField4LabelMandatory = QChkMan4.Checked

            .Field5Label = txtQ5.Text
            .Field5Value = ""
            .Field5Enabled = Qchk5.Checked
            .IsField5LabelMandatory = QChkMan5.Checked

            .Type = "ClientQuestion"
            .WOID = WOID
        End With
        dsQA.tblContactsQASettings.Rows.Add(nrowQALabels)

        If (pnlClientQ.Visible = True) Then
            Dim nrowClientQALabels As OWContactsQASettings.tblContactsQASettingsRow = dsQA.tblContactsQASettings.NewRow
            With nrowClientQALabels
                .CompanyID = ViewState("CompanyID")

                .Field1Label = ""
                .Field1Value = ""
                .Field1Enabled = False
                .IsField1LabelMandatory = False

                .Field2Label = ""
                .Field2Value = ""
                .Field2Enabled = False
                .IsField2LabelMandatory = False

                .Field3Label = ""
                .Field3Value = ""
                .Field3Enabled = False
                .IsField3LabelMandatory = False

                .Field4Label = ""
                .Field4Value = ""
                .Field4Enabled = False
                .IsField4LabelMandatory = False

                .Field5Label = ""
                .Field5Value = ""
                .Field5Enabled = False
                .IsField5LabelMandatory = False

                .Field6Label = ""
                .Field6Value = ""
                .Field6Enabled = False
                .IsField6LabelMandatory = False

                .Field7Label = ""
                .Field7Value = ""
                .Field7Enabled = False
                .IsField7LabelMandatory = False

                .Field8Label = ""
                .Field8Value = ""
                .Field8Enabled = False
                .IsField8LabelMandatory = False

                .Field9Label = ""
                .Field9Value = ""
                .Field9Enabled = False
                .IsField9LabelMandatory = False

                .Field10Label = ""
                .Field10Value = ""
                .Field10Enabled = False
                .IsField10LabelMandatory = False

                Dim seq As Integer = 0
                Dim j As Integer = 0
                If IsSequenceChange = True Then
                    For Each drow As RepeaterItem In rptClientQuestions.Items
                        Dim QNo As Integer = CInt(dvSequence.Item(j).Item("DBQuestionNo"))
                        j = j + 1
                        Select Case QNo
                            Case 1
                                .Field1Label = CType(drow.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                .Field1Value = CreateAnsXml(CType(drow.FindControl("rptClientQuestionAnswers"), Repeater))
                                .Field1Enabled = True
                                .IsField1LabelMandatory = True
                            Case 2
                                .Field2Label = CType(drow.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                .Field2Value = CreateAnsXml(CType(drow.FindControl("rptClientQuestionAnswers"), Repeater))
                                .Field2Enabled = True
                                .IsField2LabelMandatory = True
                            Case 3
                                .Field3Label = CType(drow.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                .Field3Value = CreateAnsXml(CType(drow.FindControl("rptClientQuestionAnswers"), Repeater))
                                .Field3Enabled = True
                                .IsField3LabelMandatory = True
                            Case 4
                                .Field4Label = CType(drow.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                .Field4Value = CreateAnsXml(CType(drow.FindControl("rptClientQuestionAnswers"), Repeater))
                                .Field4Enabled = True
                                .IsField4LabelMandatory = True
                            Case 5
                                .Field5Label = CType(drow.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                .Field5Value = CreateAnsXml(CType(drow.FindControl("rptClientQuestionAnswers"), Repeater))
                                .Field5Enabled = True
                                .IsField5LabelMandatory = True
                            Case 6
                                .Field6Label = CType(drow.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                .Field6Value = CreateAnsXml(CType(drow.FindControl("rptClientQuestionAnswers"), Repeater))
                                .Field6Enabled = True
                                .IsField6LabelMandatory = True
                            Case 7
                                .Field7Label = CType(drow.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                .Field7Value = CreateAnsXml(CType(drow.FindControl("rptClientQuestionAnswers"), Repeater))
                                .Field7Enabled = True
                                .IsField7LabelMandatory = True
                            Case 8
                                .Field8Label = CType(drow.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                .Field8Value = CreateAnsXml(CType(drow.FindControl("rptClientQuestionAnswers"), Repeater))
                                .Field8Enabled = True
                                .IsField8LabelMandatory = True
                            Case 9
                                .Field9Label = CType(drow.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                .Field9Value = CreateAnsXml(CType(drow.FindControl("rptClientQuestionAnswers"), Repeater))
                                .Field9Enabled = True
                                .IsField9LabelMandatory = True
                            Case 10
                                .Field10Label = CType(drow.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                .Field10Value = CreateAnsXml(CType(drow.FindControl("rptClientQuestionAnswers"), Repeater))
                                .Field10Enabled = True
                                .IsField10LabelMandatory = True
                        End Select
                    Next


                Else

                    Dim i As Integer = 0
                    For Each row As RepeaterItem In rptClientQuestions.Items
                        Dim hdnQuestionNo As HtmlInputHidden = CType(row.FindControl("hdnQuestionNo"), HtmlInputHidden)
                        If (hdnQuestionNo.Value <> QuestionToRemove) Then
                            i = i + 1
                            Select Case i
                                Case 1
                                    .Field1Label = CType(row.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                    .Field1Value = CreateAnsXml(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                                    .Field1Enabled = True
                                    .IsField1LabelMandatory = True
                                Case 2
                                    .Field2Label = CType(row.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                    .Field2Value = CreateAnsXml(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                                    .Field2Enabled = True
                                    .IsField2LabelMandatory = True
                                Case 3
                                    .Field3Label = CType(row.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                    .Field3Value = CreateAnsXml(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                                    .Field3Enabled = True
                                    .IsField3LabelMandatory = True
                                Case 4
                                    .Field4Label = CType(row.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                    .Field4Value = CreateAnsXml(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                                    .Field4Enabled = True
                                    .IsField4LabelMandatory = True
                                Case 5
                                    .Field5Label = CType(row.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                    .Field5Value = CreateAnsXml(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                                    .Field5Enabled = True
                                    .IsField5LabelMandatory = True
                                Case 6
                                    .Field6Label = CType(row.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                    .Field6Value = CreateAnsXml(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                                    .Field6Enabled = True
                                    .IsField6LabelMandatory = True
                                Case 7
                                    .Field7Label = CType(row.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                    .Field7Value = CreateAnsXml(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                                    .Field7Enabled = True
                                    .IsField7LabelMandatory = True
                                Case 8
                                    .Field8Label = CType(row.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                    .Field8Value = CreateAnsXml(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                                    .Field8Enabled = True
                                    .IsField8LabelMandatory = True
                                Case 9
                                    .Field9Label = CType(row.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                    .Field9Value = CreateAnsXml(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                                    .Field9Enabled = True
                                    .IsField9LabelMandatory = True
                                Case 10
                                    .Field10Label = CType(row.FindControl("txtClientQ"), TextBox).Text.Trim.ToString
                                    .Field10Value = CreateAnsXml(CType(row.FindControl("rptClientQuestionAnswers"), Repeater))
                                    .Field10Enabled = True
                                    .IsField10LabelMandatory = True
                            End Select
                        End If
                    Next
                End If
                .Type = "ServiceQuestion"
                .WOID = WOID
            End With
            dsQA.tblContactsQASettings.Rows.Add(nrowClientQALabels)
        End If

        Dim xmlQA As String = dsQA.GetXml
        xmlQA = xmlQA.Remove(xmlQA.IndexOf("xmlns"), xmlQA.IndexOf(".xsd") - xmlQA.IndexOf("xmlns") + 5)
        Return xmlQA
        'pick this code end
    End Function
#End Region
#Region "ClientQuestions"
    Private Sub PopulateClientQRepeater(ByVal totalQuestions As Integer, Optional ByVal dvClientQ As DataView = Nothing, Optional ByVal AddQuestion As Integer = 0)
        If (totalQuestions <> 0) Then
            pnlClientQ.Visible = True
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dCol As New DataColumn
            Dim dColDBQNo As New DataColumn
            Dim dColClientQ As New DataColumn
            Dim dColSequence As New DataColumn
            dCol.DataType = Type.[GetType]("System.String")
            dCol.ColumnName = "QuestionNo"
            dt.Columns.Add(dColDBQNo)
            dColDBQNo.DataType = Type.[GetType]("System.String")
            dColDBQNo.ColumnName = "DBQuestionNo"
            dt.Columns.Add(dCol)
            dColClientQ.DataType = Type.[GetType]("System.String")
            dColClientQ.ColumnName = "ClientQ"
            dt.Columns.Add(dColClientQ)
            dColSequence.DataType = System.Type.GetType("System.Int32")
            dColSequence.ColumnName = "Sequence"
            dt.Columns.Add(dColSequence)
            Dim i As Integer
            'If (ViewState("ClientQuestions") Is Nothing) Then
            If (AddQuestion = 1) Then
                Dim drow As DataRow
                If (Not dvClientQ Is Nothing) Then
                    Dim j As Integer = 0
                    For i = 0 To totalQuestions - 2
                        'If (dvClientQ.Item(0)("Field" & i + 1 & "Label") <> "") Then
                        drow = dt.NewRow()
                        drow("QuestionNo") = (j + 1).ToString
                        drow("DBQuestionNo") = (i + 1).ToString
                        drow("ClientQ") = dvClientQ.Item(0)("Field" & i + 1 & "Label")
                        drow("Sequence") = i + 1
                        j = j + 1
                        dt.Rows.Add(drow)
                        dt.AcceptChanges()
                        'End If
                    Next i
                End If
                drow = dt.NewRow()
                drow("QuestionNo") = totalQuestions.ToString
                drow("DBQuestionNo") = totalQuestions.ToString
                drow("ClientQ") = ""
                drow("Sequence") = totalQuestions.ToString
                dt.Rows.Add(drow)
                dt.AcceptChanges()
            Else
                If (Not dvClientQ Is Nothing) Then
                    Dim j As Integer = 0
                    For i = 0 To totalQuestions - 1
                        'If (dvClientQ.Item(0)("Field" & i + 1 & "Label") <> "") Then
                        Dim drow As DataRow
                        drow = dt.NewRow()
                        drow("QuestionNo") = (j + 1).ToString
                        drow("DBQuestionNo") = (i + 1).ToString
                        drow("ClientQ") = dvClientQ.Item(0)("Field" & i + 1 & "Label")
                        drow("Sequence") = i + 1
                        j = j + 1
                        dt.Rows.Add(drow)
                        dt.AcceptChanges()
                        'End If
                    Next i
                Else
                    For i = 0 To totalQuestions - 1
                        Dim drow As DataRow
                        drow = dt.NewRow()
                        drow("QuestionNo") = (i + 1).ToString
                        drow("DBQuestionNo") = (i + 1).ToString
                        drow("ClientQ") = ""
                        drow("Sequence") = i + 1
                        dt.Rows.Add(drow)
                        dt.AcceptChanges()
                    Next i
                End If

            End If
            ds.Tables.Add(dt.Copy)
            ds.AcceptChanges()

            If (ds.Tables(0).Rows.Count > 0) Then
                pnlRemoveallClientQ.Visible = True
                rptClientQuestions.DataSource = ds
                Session("dsClientQWithSequence") = ds
                rptClientQuestions.DataBind()
            Else
                pnlRemoveallClientQ.Visible = False
                rptClientQuestions.DataSource = ds
                Session("dsClientQWithSequence") = ds
                rptClientQuestions.DataBind()
            End If
        Else
            rptClientQuestions.DataSource = Nothing
            Session("dsClientQWithSequence") = Nothing
            rptClientQuestions.DataBind()
            pnlClientQ.Visible = False
            pnlRemoveallClientQ.Visible = False
        End If

    End Sub
    Private Sub PopulateClientQAnsRepeater(ByVal totalAns As Integer, ByVal rptClientQuestionAnswers As Repeater, Optional ByVal dvClientQ As DataView = Nothing, Optional ByVal QuestionNo As Integer = 0)
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim dCol As New DataColumn
        Dim dColAnswer As New DataColumn
        Dim dColMessage As New DataColumn
        Dim dStopBooking As New DataColumn
        Dim dColProduct As New DataColumn
        dCol.DataType = Type.[GetType]("System.String")
        dCol.ColumnName = "AnsNo"
        dt.Columns.Add(dCol)
        dColAnswer.DataType = Type.[GetType]("System.String")
        dColAnswer.ColumnName = "Answer"
        dt.Columns.Add(dColAnswer)
        dStopBooking.DataType = Type.[GetType]("System.Boolean")
        dStopBooking.ColumnName = "StopBooking"
        dt.Columns.Add(dStopBooking)
        dColMessage.DataType = Type.[GetType]("System.String")
        dColMessage.ColumnName = "Message"
        dt.Columns.Add(dColMessage)
        dColProduct.DataType = Type.[GetType]("System.String")
        dColProduct.ColumnName = "Product"
        dt.Columns.Add(dColProduct)
        Dim i As Integer

        For i = 0 To totalAns - 1


            Dim drow As DataRow
            drow = dt.NewRow()
            drow("AnsNo") = (i + 1).ToString
            If (Not dvClientQ Is Nothing) Then
                If (dvClientQ.Item(0)("Field" & QuestionNo & "Value") <> "") Then
                    If (CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & QuestionNo & "Value"), "Ans" & i + 1) <> "") Then
                        drow("Answer") = CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & QuestionNo & "Value"), "Ans" & i + 1)
                        drow("Message") = CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & QuestionNo & "Value"), "Message" & i + 1)
                        drow("StopBooking") = CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & QuestionNo & "Value"), "IsStopBooking" & i + 1)
                        drow("Product") = CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & QuestionNo & "Value"), "Product" & i + 1)
                        If (drow("Product") = "") Then
                            drow("Product") = "0"
                        End If
                    Else
                        If (totalAns = 2) Then
                            Select Case i
                                Case 0
                                    drow("Answer") = "Yes"
                                Case 1
                                    drow("Answer") = "No"
                                Case Else
                                    drow("Answer") = ""
                            End Select
                        Else
                            drow("Answer") = ""
                        End If
                        drow("Message") = ""
                        drow("StopBooking") = False
                        drow("Product") = "0"
                    End If
                Else
                    If (totalAns = 2) Then
                        Select Case i
                            Case 0
                                drow("Answer") = "Yes"
                            Case 1
                                drow("Answer") = "No"
                            Case Else
                                drow("Answer") = ""
                        End Select
                    Else
                        drow("Answer") = ""
                    End If
                    drow("Message") = ""
                    drow("StopBooking") = False
                    drow("Product") = "0"
                End If

            Else
                If (totalAns = 2) Then
                    Select Case i
                        Case 0
                            drow("Answer") = "Yes"
                        Case 1
                            drow("Answer") = "No"
                        Case Else
                            drow("Answer") = ""
                    End Select
                Else
                    drow("Answer") = ""
                End If
                drow("Message") = ""
                drow("StopBooking") = False
                drow("Product") = "0"
            End If
            dt.Rows.Add(drow)
            dt.AcceptChanges()
        Next i
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        rptClientQuestionAnswers.DataSource = ds
        rptClientQuestionAnswers.DataBind()
       

        For Each rowAns As RepeaterItem In rptClientQuestionAnswers.Items
            populateProductListDD(CType(rowAns.FindControl("drpdwnProduct"), DropDownList), CInt(CType(rowAns.FindControl("hdnSelectedProduct"), HtmlInputHidden).Value))
        Next

    End Sub


    Private Sub lnkBtnAddClientQ_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnAddClientQ.Click
        If (ViewState("ClientQCount") = 10) Then
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "ValidationMsg", "alert('Max 10 Client Questions.');", True)
        Else
            ViewState("ClientQCount") = ViewState("ClientQCount") + 1
            hdnQuestionCount.Value = Convert.ToInt32(ViewState("ClientQCount"))
            Dim ds As DataSet
            Dim xmlQA As String = GetQASettings(0)
            ds = CommonFunctions.ConvertXMLToDataSet(xmlQA)
            Dim dvContactsQA As New DataView(ds.Tables(0).Copy)
            dvContactsQA.RowFilter = "Type = 'ServiceQuestion'"

            If dvContactsQA.Count > 0 Then
                Session("ClientQ") = dvContactsQA
            Else
                Session("ClientQ") = Nothing
            End If
            PopulateClientQRepeater(ViewState("ClientQCount"), dvContactsQA, 1)
        End If

    End Sub
    Private Sub lnkBtnRemoveallClientQ_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnRemoveallClientQ.Click
        ViewState("ClientQCount") = 0
        hdnQuestionCount.Value = Convert.ToInt32(ViewState("ClientQCount"))
        Session("ClientQ") = Nothing
        PopulateClientQRepeater(0)
    End Sub

    Private Sub rptClientQuestions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptClientQuestions.ItemDataBound

        Dim rptClientQuestionAnswers As Repeater = CType(e.Item.FindControl("rptClientQuestionAnswers"), Repeater)
        Dim ddQAnsCount As DropDownList = CType(e.Item.FindControl("ddQAnsCount"), DropDownList)
        Dim hdnDBQuestionNo As HtmlInputHidden = CType(e.Item.FindControl("hdnDBQuestionNo"), HtmlInputHidden)

        If (Not Session("ClientQ") Is Nothing) Then
            Dim dvClientQ As DataView = CType(Session("ClientQ"), DataView)
            If (CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & CInt(hdnDBQuestionNo.Value) & "Value"), "TotalAnsCount") <> "") Then
                ddQAnsCount.SelectedValue = CommonFunctions.getXMLElementValue(dvClientQ.Item(0)("Field" & CInt(hdnDBQuestionNo.Value) & "Value"), "TotalAnsCount")
            End If
            PopulateClientQAnsRepeater(ddQAnsCount.SelectedValue, rptClientQuestionAnswers, dvClientQ, CInt(hdnDBQuestionNo.Value))
        Else
            PopulateClientQAnsRepeater(ddQAnsCount.SelectedValue, rptClientQuestionAnswers)
        End If

    End Sub

    Private Sub rptClientQuestions_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptClientQuestions.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
           If (e.CommandName = "RemoveQuestion") Then
                Dim hdnQuestionNo As HtmlInputHidden = CType(e.Item.FindControl("hdnQuestionNo"), HtmlInputHidden)
                ViewState("ClientQCount") = ViewState("ClientQCount") - 1
                hdnQuestionCount.Value = Convert.ToInt32(ViewState("ClientQCount"))
                Dim ds As DataSet
                Dim xmlQA As String = GetQASettings(0, CInt(hdnQuestionNo.Value))
                ds = CommonFunctions.ConvertXMLToDataSet(xmlQA)
                Dim dvContactsQA As New DataView(ds.Tables(0).Copy)
                dvContactsQA.RowFilter = "Type = 'ServiceQuestion'"

                If dvContactsQA.Count > 0 Then
                    Session("ClientQ") = dvContactsQA
                Else
                    Session("ClientQ") = Nothing
                End If
                PopulateClientQRepeater(ViewState("ClientQCount"), dvContactsQA)

                'Remove from database
                'Dim dsSuccess As DataSet = ws.WSWorkOrder.DeleteServiceQuestions(ViewState("ProductID").ToString, CInt(Request("companyid")), hdnQuestionNo.Value.ToString)
                'Dim ds As DataSet = ws.WSWorkOrder.populateProductDetails(ViewState("ProductID"), Request("companyid"), Request("bizdivid"))
                'If ds.Tables.Count > 0 Then
                '    If ds.Tables(0).Rows.Count > 0 Then
                '        populateProductDetails(ds)
                '    Else
                '        PopulateProductLocation(ds)
                '    End If
                'Else
                '    PopulateProductLocation(ds)
                'End If



            ElseIf (e.CommandName = "MoveUp") Then
                Dim Sequence As Integer = CInt(e.CommandArgument)
                If (Sequence <> 0) Then
                    MoveUpDownQuestion(Sequence, "Up")
                End If
            ElseIf (e.CommandName = "MoveDown") Then
                Dim Sequence As Integer = CInt(e.CommandArgument)
                If (Sequence <> 0) Then
                    MoveUpDownQuestion(Sequence, "Down")
                End If
            End If

        End If
    End Sub
    Public Function MoveUpDownQuestion(ByVal Sequence As String, ByVal Mode As String)
        Dim ds As New DataSet
        ds = CType(Session("dsClientQWithSequence"), DataSet)

        For Each drow As DataRow In ds.Tables(0).Rows
            If drow.Item("Sequence") = Sequence Then
                If (Mode = "Up") Then
                    drow.Item("Sequence") = drow.Item("Sequence") - 1
                ElseIf (Mode = "Down") Then
                    drow.Item("Sequence") = drow.Item("Sequence") + 1
                End If
            ElseIf drow.Item("Sequence") = Sequence - 1 Then
                If (Mode = "Up") Then
                    drow.Item("Sequence") = drow.Item("Sequence") + 1
                End If
            ElseIf drow.Item("Sequence") = Sequence + 1 Then
                If (Mode = "Down") Then
                    drow.Item("Sequence") = drow.Item("Sequence") - 1
                End If
            End If

        Next

        ds.Tables(0).AcceptChanges()

        Dim dv As New DataView
        dv = ds.Tables(0).DefaultView
        dv.Sort = "Sequence asc"
        Dim dsnew As DataSet
        Dim xmlQA As String = GetQASettings(0, Nothing, dv, True)
        dsnew = CommonFunctions.ConvertXMLToDataSet(xmlQA)
        Dim dvContactsQA As New DataView(dsnew.Tables(0).Copy)
        dvContactsQA.RowFilter = "Type = 'ServiceQuestion'"

        If dvContactsQA.Count > 0 Then
            Session("ClientQ") = dvContactsQA
        Else
            Session("ClientQ") = Nothing
        End If
        PopulateClientQRepeater(ViewState("ClientQCount"), dvContactsQA)


    End Function

    Private Function ValidatingClientQuestions() As Boolean
        Dim flag As Boolean = True
        Dim errmsg As String = ""
        Dim i As Integer = 0
        Dim j As Integer = 0
        If (pnlClientQ.Visible = True) Then
            If Not IsNothing(rptClientQuestions) Then
                For Each row As RepeaterItem In rptClientQuestions.Items
                    i = i + 1
                    j = 0
                    'Validating First name is not blank
                    If CType(row.FindControl("txtClientQ"), TextBox).Text.Trim.Length = 0 Then
                        If flag = True Then
                            errmsg = "- Please Enter Client Question " & i
                            flag = False
                        Else
                            errmsg = errmsg & "<br>- Please Enter Client Question " & i
                        End If
                    End If
                    Dim rptClientQuestionAnswers As Repeater = CType(row.FindControl("rptClientQuestionAnswers"), Repeater)
                    If Not IsNothing(rptClientQuestionAnswers) Then
                        For Each rowAns As RepeaterItem In rptClientQuestionAnswers.Items
                            Dim anscount As Integer = rptClientQuestionAnswers.Items.Count

                            j = j + 1
                            'Validating First name is not blank
                            If CType(rowAns.FindControl("txtAnswer"), TextBox).Text.Trim.Length = 0 Then
                                If anscount <> 1 Then
                                    If flag = True Then
                                        errmsg = "- Please Enter Client Question " & i & " Answer " & j
                                        flag = False
                                    Else
                                        errmsg = errmsg & "<br>- Please Enter Client Question " & i & " Answer " & j
                                    End If
                                End If

                            End If
                        Next
                    End If
                Next
            End If
        End If

        If Not flag Then
            divValidationMain.Visible = True
            lblError.Text = errmsg.ToString
        End If
        Return flag
    End Function

    Public Function CreateAnsXml(ByVal rptClientQuestionAnswers As Repeater)
        Dim i As Integer = 0
        Dim xmlQueAns As New StringBuilder
        xmlQueAns.Append("<xmlQueAns>")
        If Not IsNothing(rptClientQuestionAnswers) Then
            For Each rowAns As RepeaterItem In rptClientQuestionAnswers.Items
                i = i + 1
                xmlQueAns.Append("<Ans" & i & ">" & CType(rowAns.FindControl("txtAnswer"), TextBox).Text.Trim & "</Ans" & i & ">")
                xmlQueAns.Append("<Message" & i & ">" & CType(rowAns.FindControl("txtAnswerMessage"), TextBox).Text.Trim & "</Message" & i & ">")
                xmlQueAns.Append("<IsStopBooking" & i & ">" & CType(rowAns.FindControl("chkboxStopBooking"), CheckBox).Checked & "</IsStopBooking" & i & ">")
                xmlQueAns.Append("<Product" & i & ">" & CType(rowAns.FindControl("drpdwnProduct"), DropDownList).SelectedValue & "</Product" & i & ">")
            Next
        End If
        xmlQueAns.Append("<TotalAnsCount>" & i & "</TotalAnsCount>")
        xmlQueAns.Append("</xmlQueAns>")
        Return xmlQueAns.ToString
    End Function

    Protected Sub ddQAnsCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddQAnsCount As DropDownList = DirectCast(sender, DropDownList)
        Dim rptClientQuestionAnswers As Repeater

        Dim i As Integer = 0
        For Each row As RepeaterItem In rptClientQuestions.Items
            i = i + 1
            If (ddQAnsCount.ToolTip = i) Then
                rptClientQuestionAnswers = CType(row.FindControl("rptClientQuestionAnswers"), Repeater)
                Exit For
            End If
        Next

        Dim ds As DataSet
        Dim xmlQA As String = GetQASettings(0)
        ds = CommonFunctions.ConvertXMLToDataSet(xmlQA)
        Dim dvContactsQA As New DataView(ds.Tables(0).Copy)
        dvContactsQA.RowFilter = "Type = 'ServiceQuestion'"

        If dvContactsQA.Count > 0 Then
            Session("ClientQ") = dvContactsQA
        Else
            Session("ClientQ") = Nothing
        End If
        PopulateClientQAnsRepeater(ddQAnsCount.SelectedValue, rptClientQuestionAnswers, Session("ClientQ"), CInt(ddQAnsCount.ToolTip))

    End Sub
    Public Sub populateProductListDD(ByVal drpdwnProduct As DropDownList, ByVal SelectedProduct As Integer)
        Dim ds As DataSet
        ds = ws.WSWorkOrder.GetProductListing(Request("companyid"), BizDivID, "Sequence", 0, 0, 0, False)

        If ds.Tables(0).Rows.Count > 0 Then
            drpdwnProduct.Visible = True
            Dim liProducts As New ListItem
            liProducts = New ListItem
            liProducts.Text = "Select Service"
            liProducts.Value = 0
            drpdwnProduct.DataSource = ds.Tables(0)
            drpdwnProduct.DataTextField = "ProductName"
            drpdwnProduct.DataValueField = "ProductID"
            drpdwnProduct.DataBind()
            drpdwnProduct.Items.Insert(0, liProducts)

            If (ViewState("ProductID") <> 0) Then
                Dim liProductsToRemove As New ListItem
                liProductsToRemove = New ListItem
                liProductsToRemove.Value = ViewState("ProductID")
                drpdwnProduct.Items.Remove(drpdwnProduct.Items.FindByValue(ViewState("ProductID")))
            End If
        Else
            drpdwnProduct.Visible = False
        End If

        Dim vListItem As ListItem = drpdwnProduct.Items.FindByValue(SelectedProduct)

        If Not vListItem Is Nothing Then
            drpdwnProduct.SelectedValue = SelectedProduct
        End If

    End Sub
#End Region

    Public Sub CopyService(ByVal sender As Object, ByVal e As EventArgs)
        txtCopyServiceNumber.Text = ""
        mdlCopyService.Show()
    End Sub
    Public Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click
        Dim ds As DataSet = ws.WSWorkOrder.CopyService(ViewState("ProductID"), CInt(txtCopyServiceNumber.Text.Trim), Session("UserId"))
        If Not IsNothing(Request.QueryString("pagename")) Then
            If Request.QueryString("pagename") = "AccountService" Then
                Response.Redirect("AccountServiceList.aspx?sender=productform")
            Else
                'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
                Response.Redirect("AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
            End If
        Else
            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            Response.Redirect("AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId"))
        End If
    End Sub

End Class
