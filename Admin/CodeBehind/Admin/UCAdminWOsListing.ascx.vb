Imports WebLibrary
Imports System.IO
Partial Public Class UCAdminWOsListing
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs
    Public WithEvents pnlConfirm As Panel
    Public WithEvents pnlConfimRemove As Panel
    Public WithEvents hdnButtonRemove As Button


    Public Property Group() As String
        Get
            If Not IsNothing(ViewState("Group")) Then
                Return ViewState("Group")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("Group") = value

        End Set
    End Property
    Public Property BizDiv() As String
        Get
            If Not IsNothing(ViewState("BizDiv")) Then
                Return ViewState("BizDiv")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("BizDiv") = value
        End Set
    End Property
    Public Property CompID() As String
        Get
            If Not IsNothing(ViewState("CompID")) Then
                Return ViewState("CompID")
            Else
                'If Not IsNothing(Request("CompID")) Then
                '    If Request("CompID") = "0" Then
                '        Return 0
                '    Else
                '        Return -1
                '    End If
                'Else
                '    Return -1
                'End If
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("CompID") = value

        End Set
    End Property
    Public Property FromDate() As String
        Get
            If Not IsNothing(ViewState("FromDate")) Then
                Return ViewState("FromDate")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("FromDate") = value

        End Set
    End Property
    Public Property ToDate() As String
        Get
            If Not IsNothing(ViewState("ToDate")) Then
                Return ViewState("ToDate")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("ToDate") = value

        End Set
    End Property
    Public Property WorkorderID() As String
        Get
            If Not IsNothing(ViewState("WorkorderID")) Then
                Return ViewState("WorkorderID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("WorkorderID") = value

        End Set
    End Property
    Public Property CustomerMobile() As String
        Get
            If Not IsNothing(ViewState("CustomerMobile")) Then
                Return ViewState("CustomerMobile")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("CustomerMobile") = value

        End Set
    End Property
    Public Property sortExpression() As String
        Get
            If Not IsNothing(ViewState("sortExpression")) Then
                Return ViewState("sortExpression")
            Else
                Return "WorkOrderId"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("sortExpression") = value
        End Set
    End Property
    Public Property SelectedWOIDs() As String
        Get
            If Not IsNothing(ViewState("SelectedWOIDs")) Then
                Return ViewState("SelectedWOIDs")
            Else
                Return "WorkOrderId"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("SelectedWOIDs") = value
        End Set
    End Property

    Public Property IsNextDay() As String
        Get
            If Not IsNothing(ViewState("IsNextDay")) Then
                Return ViewState("IsNextDay")
            Else
                Return "No"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("IsNextDay") = value
        End Set
    End Property
    Public Property BillLoc() As Integer
        Get
            If Not IsNothing(Session("BillLoc")) Then
                Return Session("BillLoc")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            Session("BillLoc") = value
        End Set
    End Property

    Public Property MainCat() As String
        Get
            If Not IsNothing(ViewState("MainCat")) Then
                Return ViewState("MainCat")
            Else
                Return "No"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("MainCat") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            ' gvWorkOrders.PageSize = ApplicationSettings.GridViewPageSize
            If Not IsNothing(Request("sender")) Then
                If Request("sender") = "UCWOsListing" Or Request("sender") = "CompanyProfile" Then
                    processBackToListing()
                Else
                    gvWorkOrders.Sort("DateCreated", SortDirection.Descending)
                End If
                'Added by PB - to list the submitted listing in the descending order os status for Current sales report
            ElseIf Not IsNothing(Request("calledfrom")) Then
                If (Request("calledfrom") = "CurrentSales") And (ViewState("Group") = "Submitted") Then
                    gvWorkOrders.Sort("Status", SortDirection.Descending)
                Else
                    gvWorkOrders.Sort("DateCreated", SortDirection.Descending)
                End If
            Else
                If Not IsNothing(Request("mode")) Then
                    If Request("mode") = "Completed" Or Request("mode") = "Closed" Or Request("mode") = "Cancelled" Or Request("mode") = "Invoiced" Then
                        gvWorkOrders.Sort("DateModified", SortDirection.Descending)
                        ViewState("sortExpression") = "DateModified"
                    ElseIf Request("mode") = "Accepted" Or Request("mode") = "ActiveElapsed" Then
                        gvWorkOrders.Sort("DateModified", SortDirection.Ascending)
                        ViewState("sortExpression") = "DateModified"
                    ElseIf Request("mode") = "Submitted" Or Request("mode") = "Sent" Or Request("mode") = "CA" Then
                        gvWorkOrders.Sort("DateStart", SortDirection.Ascending)
                        ViewState("sortExpression") = "DateStart"
                    Else
                        gvWorkOrders.Sort("DateCreated", SortDirection.Descending)
                    End If
                End If
            End If
            'poonam modified on 8/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
            'populate note type ddl
            CommonFunctions.PopulateNoteCategory(Page, ddlNoteType)
        End If

    End Sub

    Public Sub PopulateGrid(Optional ByVal PageIndex As Integer = 0)
        SetGridSettings()
        If IsNothing(ViewState("sortExpression")) Then
            If Request("mode") = "Completed" Or Request("mode") = "Closed" Or Request("mode") = "Cancelled" Or Request("mode") = "Invoiced" Then
                gvWorkOrders.Sort("DateModified", SortDirection.Descending)
                ViewState("sortExpression") = "DateModified"
            ElseIf Request("mode") = "Accepted" Or Request("mode") = "ActiveElapsed" Then
                gvWorkOrders.Sort("DateModified", SortDirection.Ascending)
                ViewState("sortExpression") = "DateModified"
            ElseIf Request("mode") = "Submitted" Or Request("mode") = "Sent" Or Request("mode") = "CA" Then
                gvWorkOrders.Sort("DateStart", SortDirection.Ascending)
                ViewState("sortExpression") = "DateStart"
            Else
                gvWorkOrders.Sort("DateCreated", SortDirection.Descending)
            End If
        End If
        If PageIndex > 0 Then
            gvWorkOrders.PageIndex = PageIndex
        Else
            gvWorkOrders.PageIndex = 0
        End If

        If Request("mode") = "Accepted" Then
            gvWorkOrders.Sort("DateStart", SortDirection.Descending)
            ViewState("sortExpression") = "DateStart"
        End If

        gvWorkOrders.DataBind()
    End Sub

    Public Sub SetGridSettings()

        If HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupSubmitted Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupSent Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupAccepted Or HttpContext.Current.Items("Group") = "Activeelapsed" Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupCompleted Then
            gvWorkOrders.Columns.Item(0).Visible = True 'Multiple Select Check Box
        End If

        If HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupDraft Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupSubmitted Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupSent Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupCA Then
            gvWorkOrders.Columns.Item(7).Visible = False 'Buyer Contact
            gvWorkOrders.Columns.Item(8).Visible = False 'Supplier Company
            gvWorkOrders.Columns.Item(9).Visible = False 'Supplier Contact
        End If
        If HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupAccepted Or HttpContext.Current.Items("Group") = "Activeelapsed" Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupIssue Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupCompleted Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupClosed Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupCancelled Or HttpContext.Current.Items("Group") = "Staged" Then
            gvWorkOrders.Columns.Item(7).Visible = False 'Buyer Contact
            gvWorkOrders.Columns.Item(9).Visible = False 'Supplier Contact
        End If
        If HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupSubmitted Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupSent Then
            gvWorkOrders.Columns.Item(2).HeaderText = ResourceMessageText.GetString("Submitted")    '   "Submitted"
            gvWorkOrders.Columns.Item(3).HeaderText = ResourceMessageText.GetString("LastSent")     '   "Last Sent"
            gvWorkOrders.Columns.Item(3).Visible = False 'hide "Last Sent" column
        ElseIf HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupCA Then
            'gvWorkOrders.Columns.Item(2).Visible = False 'Submitted Date
            gvWorkOrders.Columns.Item(2).HeaderText = ResourceMessageText.GetString("Submitted")
            gvWorkOrders.Columns.Item(3).HeaderText = ResourceMessageText.GetString("LastResponse") '    "Last Response"
            'gvWorkOrders.Columns.Item(3).Visible = False 'hide  "Last Response" column
            gvWorkOrders.Columns.Item(5).Visible = False                                            '   hide "Customer Name"

        ElseIf HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupAccepted Or HttpContext.Current.Items("Group") = "Activeelapsed" Or HttpContext.Current.Items("Group") = "Staged" Then
            gvWorkOrders.Columns.Item(2).HeaderText = ResourceMessageText.GetString("Accepted")     '   "Accepted"
            gvWorkOrders.Columns.Item(3).HeaderText = ResourceMessageText.GetString("LastResponse") '    "Last Response"
        ElseIf HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupIssue Then
            gvWorkOrders.Columns.Item(2).HeaderText = ResourceMessageText.GetString("IssueRaised")  '   "Issue Raised"
            gvWorkOrders.Columns.Item(3).HeaderText = ResourceMessageText.GetString("LastResponse") '    "Last Response"
            gvWorkOrders.Columns.Item(5).Visible = False                                            '   hide "Customer Name"
        ElseIf HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupCompleted Then
            gvWorkOrders.Columns.Item(2).HeaderText = ResourceMessageText.GetString("Accepted")     '   "Accepted"
            gvWorkOrders.Columns.Item(3).HeaderText = ResourceMessageText.GetString("Completed")    '   "Closed"
        ElseIf HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupClosed Then
            gvWorkOrders.Columns.Item(2).HeaderText = ResourceMessageText.GetString("Accepted")    '   "Submitted"
            gvWorkOrders.Columns.Item(3).HeaderText = ResourceMessageText.GetString("Closed")       '   "Closed"
        ElseIf HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupCancelled Then
            gvWorkOrders.Columns.Item(2).HeaderText = ResourceMessageText.GetString("Submitted")     '   "Accepted"
            gvWorkOrders.Columns.Item(3).HeaderText = ResourceMessageText.GetString("Cancelled")    '   "Closed"
            gvWorkOrders.Columns.Item(5).Visible = False                                            '   hide "Customer Name"
        End If
        'If HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupDraft Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupSubmitted Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupSent Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupCA Then

        '    gvWorkOrders.Columns.Item(15).Visible = True 'Status

        'Else
        '    gvWorkOrders.Columns.Item(15).Visible = False 'Status
        'End If

        If HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupInvoiced Then
            gvWorkOrders.Columns.Item(17).Visible = True 'InvoiceNo
            gvWorkOrders.Columns.Item(11).Visible = False 'Start Date
            gvWorkOrders.Columns.Item(7).Visible = False 'Buyer Contact
            gvWorkOrders.Columns.Item(9).Visible = False 'Supplier Contact
            'added new condition by pankaj malav on 07/31 to change the header text in case of invoices listing
            gvWorkOrders.Columns.Item(3).HeaderText = ResourceMessageText.GetString("InvoicedDate")
        End If

        If HttpContext.Current.Items("Group") = "Watched" Then
            gvWorkOrders.Columns.Item(0).Visible = True 'Multiple Select Check Box
            gvWorkOrders.Columns.Item(2).Visible = False
            gvWorkOrders.Columns.Item(3).Visible = False
            gvWorkOrders.Columns.Item(9).Visible = False 'Supplier Contact
        End If



        gvWorkOrders.Columns.Item(14).Visible = True 'UpSell
        gvWorkOrders.Columns.Item(15).Visible = True 'Billing Location


        'To Show/Hide the Icons Columns
        Select Case HttpContext.Current.Items("Group")
            Case ApplicationSettings.WOGroupDraft
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                gvWorkOrders.Columns.Item(19).Visible = True 'Edit Icon
                gvWorkOrders.Columns.Item(20).Visible = False 'History Icon
            Case ApplicationSettings.WOGroupSubmitted
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                gvWorkOrders.Columns.Item(19).Visible = True 'Edit Icon
                gvWorkOrders.Columns.Item(21).Visible = True 'OrderMatch Icon
                gvWorkOrders.Columns.Item(25).Visible = True 'Set Wholesale Price Icon
                gvWorkOrders.Columns.Item(26).Visible = True 'Set Platform Price Icon
                'gvWorkOrders.Columns.Item(24).Visible = True 'Buyer Accept WR Icon
                gvWorkOrders.Columns.Item(34).Visible = True 'Cancel Icon
                gvWorkOrders.Columns.Item(20).Visible = False 'History Icon
            Case ApplicationSettings.WOGroupSent
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                gvWorkOrders.Columns.Item(19).Visible = True 'Edit Icon
                gvWorkOrders.Columns.Item(21).Visible = True 'OrderMatch Icon
                gvWorkOrders.Columns.Item(25).Visible = True 'Set Wholesale Price Icon
                gvWorkOrders.Columns.Item(26).Visible = True 'Set Platform Price Icon
                'gvWorkOrders.Columns.Item(24).Visible = True 'Buyer Accept WR Icon
                gvWorkOrders.Columns.Item(34).Visible = True 'Cancel Icon
                gvWorkOrders.Columns.Item(20).Visible = True 'History Icon
            Case ApplicationSettings.WOGroupCA
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                gvWorkOrders.Columns.Item(21).Visible = True 'OrderMatch Icon
                gvWorkOrders.Columns.Item(25).Visible = True 'Set Wholesale Price Icon
                'gvWorkOrders.Columns.Item(24).Visible = True 'Buyer Accept WR Icon
                gvWorkOrders.Columns.Item(34).Visible = True 'Cancel Icon
                'lnkUpdateWO.Visible = False
                'gvWorkOrders.Columns.Item(18).Visible = True
                gvWorkOrders.Columns.Item(20).Visible = True 'History Icon
            Case ApplicationSettings.WOGroupAccepted
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                gvWorkOrders.Columns.Item(22).Visible = True 'Raise Issue Icon
                gvWorkOrders.Columns.Item(25).Visible = True 'Set Wholesale Price Icon
                'If ViewState("StagedWO") = "No" Then
                gvWorkOrders.Columns.Item(24).Visible = True 'Complete Icon
                'End If
                'gvWorkOrders.Columns.Item(24).Visible = True 'Buyer Accept WR Icon
                gvWorkOrders.Columns.Item(34).Visible = True 'Cancel Icon
                gvWorkOrders.Columns.Item(20).Visible = True 'History Icon
                'gvWorkOrders.Columns.Item(36).Visible = True 'Rate Workorder Icon
            Case "Activeelapsed"
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                gvWorkOrders.Columns.Item(22).Visible = True 'Raise Issue Icon
                gvWorkOrders.Columns.Item(25).Visible = True 'Set Wholesale Price Icon
                'If ViewState("StagedWO") = "No" Then
                gvWorkOrders.Columns.Item(24).Visible = True 'Complete Icon
                'End If
                'gvWorkOrders.Columns.Item(24).Visible = True 'Buyer Accept WR Icon
                gvWorkOrders.Columns.Item(34).Visible = True 'Cancel Icon
                gvWorkOrders.Columns.Item(20).Visible = True 'History Icon
                'gvWorkOrders.Columns.Item(36).Visible = True 'Rate Workorder Icon
            Case "Staged"
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                gvWorkOrders.Columns.Item(31).Visible = True 'Generate Stages Icon
                gvWorkOrders.Columns.Item(34).Visible = True 'Cancel Icon
                gvWorkOrders.Columns.Item(20).Visible = True 'History Icon
            Case ApplicationSettings.WOGroupIssue
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                gvWorkOrders.Columns.Item(25).Visible = True 'Set Wholesale Price Icon
                'gvWorkOrders.Columns.Item(24).Visible = True 'Buyer Accept WR Icon
                gvWorkOrders.Columns.Item(34).Visible = True 'Cancel Icon
                gvWorkOrders.Columns.Item(20).Visible = True 'History Icon
            Case ApplicationSettings.WOGroupCompleted
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                gvWorkOrders.Columns.Item(22).Visible = True 'Raise Issue Icon
                gvWorkOrders.Columns.Item(25).Visible = True 'Set Wholesale Price Icon
                gvWorkOrders.Columns.Item(27).Visible = True 'Close Icon
                'gvWorkOrders.Columns.Item(24).Visible = True 'Buyer Accept WR Icon
                gvWorkOrders.Columns.Item(34).Visible = True 'Cancel Icon
                gvWorkOrders.Columns.Item(35).Visible = True 'Remove attachment
                gvWorkOrders.Columns.Item(20).Visible = True 'History Icon
                'gvWorkOrders.Columns.Item(36).Visible = True 'Rate Workorder Icon
            Case ApplicationSettings.WOGroupClosed
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                'gvWorkOrders.Columns.Item(23).Visible = True 'Copy Icon
                'gvWorkOrders.Columns.Item(25).Visible = True 'Cancel Icon
                gvWorkOrders.Columns.Item(35).Visible = True 'Remove attachment
                gvWorkOrders.Columns.Item(20).Visible = True 'History Icon
                'gvWorkOrders.Columns.Item(36).Visible = True 'Rate Workorder Icon
                gvWorkOrders.Columns.Item(38).Visible = True 'Watch/Unwatch Icon
            Case ApplicationSettings.WOGroupCancelled
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                gvWorkOrders.Columns.Item(20).Visible = True 'History Icon
                gvWorkOrders.Columns.Item(38).Visible = False 'Watch/Unwatch Icon
            Case ApplicationSettings.WOGroupInvoiced
                gvWorkOrders.Columns.Item(38).Visible = True 'Watch/Unwatch Icon
            Case "Watched"
                gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon
                gvWorkOrders.Columns.Item(20).Visible = True 'History Icon
                gvWorkOrders.Columns.Item(25).Visible = True 'Set Wholesale Price Icon
                gvWorkOrders.Columns.Item(16).Visible = True 'Status





        End Select
        gvWorkOrders.Columns.Item(18).Visible = True 'Details Icon

    End Sub

    ''' <summary>
    ''' To return dataset for WO listing
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                        ByVal maximumRows As System.Nullable(Of Integer)) As DataSet

        Dim ds As DataSet
        Dim hideTestWOs As Boolean = CType(Me.Parent.FindControl("chkHideTestAcc"), CheckBox).Checked
        Dim chkNextDay As CheckBox = CType(Me.Parent.FindControl("chkNextDay"), CheckBox)
        Dim chkMainCat As CheckBox = CType(Me.Parent.FindControl("chkMainCat"), CheckBox)
        Dim isNextDay As Boolean = False

        If Not IsNothing(chkNextDay) Then
            isNextDay = CType(Me.Parent.FindControl("chkNextDay"), CheckBox).Checked
        End If

        Dim MainCat As Boolean = False
        If Not IsNothing(chkMainCat) Then
            MainCat = CType(Me.Parent.FindControl("chkMainCat"), CheckBox).Checked
        End If

        Dim BL As Integer = 0
        If HttpContext.Current.Items("CompID") = 0 Then
            BL = 0
        Else
            BL = BillLoc
        End If

        Dim Watched As Boolean = False
        If Not IsNothing(Request("mode")) Then
            If Request("mode") = "Watched" Then
                Watched = True
                hideTestWOs = CType(Me.Parent.FindControl("chkWatchHideTestAcc"), CheckBox).Checked
            End If
        End If

        Dim splitCompanyName As String = ""
        If Not IsNothing(Session("dsMultiSelectedCompany")) Then
            Dim dsSelectedCompany As DataSet
            Dim dsTemp As DataSet
            dsSelectedCompany = CType(Session("dsMultiSelectedCompany"), DataSet)

            Dim strCompanyName As String = ""
            Dim SPCompanyName As String = ""
            If Not IsNothing(dsSelectedCompany) Then
                dsTemp = CType(Session("dsMultiSelectedCompany"), DataSet)
                Dim SelectedCompanyName As String = ""
                Dim i As Integer
                i = 0
                For Each drow As DataRow In dsTemp.Tables(0).Rows
                    SelectedCompanyName += drow.Item("CompanyName").ToString + ","
                    i = i + 1
                    strCompanyName = strCompanyName + SelectedCompanyName.Trim
                Next
                splitCompanyName = strCompanyName.Substring(0, strCompanyName.LastIndexOf(","))
            End If
        End If

        If (Watched = True) Then
            ds = ws.WSWorkOrder.GetAdminWOListing(sortExpression, startRowIndex, maximumRows, "", 0, HttpContext.Current.Items("FromDate"), HttpContext.Current.Items("ToDate"), HttpContext.Current.Items("BizDivID"), hideTestWOs, "", isNextDay, BL, False, Watched, HttpContext.Current.Items("WorkorderID"), Session("UserID"), splitCompanyName, HttpContext.Current.Items("CustomerMobile"))
        Else
            ds = ws.WSWorkOrder.GetAdminWOListing(sortExpression, startRowIndex, maximumRows, HttpContext.Current.Items("Group"), HttpContext.Current.Items("CompID"), HttpContext.Current.Items("FromDate"), HttpContext.Current.Items("ToDate"), HttpContext.Current.Items("BizDivID"), hideTestWOs, "", isNextDay, BL, MainCat, Watched, HttpContext.Current.Items("WorkorderID"), Session("UserID"), splitCompanyName, HttpContext.Current.Items("CustomerMobile"))
        End If

        HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")

        HttpContext.Current.Items("tblWOUserNotes") = ds.Tables("tblWOUserNotes")

        'If Not IsDBNull(ds.Tables(0).Rows(0).Item("StagedWO")) Then
        '    ViewState("StagedWO") = ds.Tables(0).Rows(0).Item("StagedWO")
        'Else
        '    ViewState("StagedWO") = "No"
        'End If
        If (ds.Tables.Count > 2) Then
            Session("tblDepotSheet") = ds.Tables(2)
        End If
        'Dim dv As DataView
        'dv = ds.Tables(0).Copy.DefaultView
        'Dim dtUnique As DataTable
        'dtUnique = dv.ToTable(True, "BillingLocID")
        'Dim dt As DataTable
        'dt = dv.ToTable
        'Dim dtnew As New DataTable
        'For dtcount As Integer = 0 To 1
        '    dtnew.Columns.Add(dt.Columns(dtcount).ColumnName)
        'Next

        'Dim billingLocationId As Integer
        'Dim newRow As DataRow = dtnew.NewRow()
        'For k As Integer = 0 To dtUnique.Rows.Count - 1
        '    billingLocationId = 0
        '    newRow = Nothing
        '    For j As Integer = 0 To dt.Rows.Count - 1
        '        If dtUnique.Rows(k)("BillingLocID") = dt.Rows(j)("BillingLocID") Then
        '            'Dim dr As DataRow
        '            dtnew.ImportRow(dt.Rows(j))
        '            Exit For
        '        End If
        '    Next
        'Next
        Dim dvBillLoc As DataView
        dvBillLoc = ds.Tables("tblBillLoc").Copy.DefaultView
        Session("dvBillLoc") = dvBillLoc
        Return ds
    End Function

    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function
    Public Function FillBillingLocation() As DataView
        Dim dvBillLoc As DataView = Nothing
        If Not IsNothing(Session("dvBillLoc")) Then
            dvBillLoc = CType(Session("dvBillLoc"), DataView)
            If Not (dvBillLoc.Count > 0) Then
                dvBillLoc = Nothing
            End If
        End If
        Return dvBillLoc
    End Function

    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    Private Sub ObjectDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles ObjectDataSource1.Selecting
        InitializeGridParameters()
        SetGridSettings()
    End Sub

    Private Sub InitializeGridParameters()
        HttpContext.Current.Items("Group") = Group
        HttpContext.Current.Items("BizDivID") = BizDiv
        HttpContext.Current.Items("CompID") = CompID
        HttpContext.Current.Items("FromDate") = FromDate
        HttpContext.Current.Items("ToDate") = ToDate
        HttpContext.Current.Items("WorkorderID") = WorkorderID
        HttpContext.Current.Items("CustomerMobile") = CustomerMobile
    End Sub



    'Private Sub gvWorkOrders_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvWorkOrders.PreRender  'gvWorkOrders.DataBinding
    '    If gvWorkOrders.PageIndex > 0 Then
    '        Me.gvWorkOrders.Controls(0).Controls(Me.gvWorkOrders.Controls(0).Controls.Count - 1).Visible = True
    '    End If
    'End Sub

    ' code added as on active setting the WP throws error
    Private Sub gvWorkOrders_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvWorkOrders.PreRender
        If gvWorkOrders.PageIndex > 0 Then
            Me.gvWorkOrders.Controls(0).Controls(Me.gvWorkOrders.Controls(0).Controls.Count - 1).Visible = True
        End If
    End Sub

    Private Sub gvWorkOrders_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvWorkOrders.RowCommand

        Dim WorkOrderId As String

        If (e.CommandName = "AddNote") Then
            WorkOrderId = e.CommandArgument.ToString
            ViewState.Add("SelectedWorkOrderId", WorkOrderId)
            CType(FindControl("mdlQuickNotes"), AjaxControlToolkit.ModalPopupExtender).Show()

        End If
        If (e.CommandName = "RemoveAttachment") Then
            ViewState.Add("SelectedWorkOrderId", CInt(e.CommandArgument))
            Dim ds As DataSet
            Session("dsAttachedFiles") = Nothing
            ds = ws.WSWorkOrder.GetAttachments(CInt(e.CommandArgument), "", "GetAttachments", "", "AttachmentID", 1, 25)
            Session("dsAttachedFiles") = ds
            CType(FindControl("dlAttList"), DataList).DataSource = ds
            CType(FindControl("dlAttList"), DataList).DataBind()
            CType(FindControl("mpRemoveAtchmnt"), AjaxControlToolkit.ModalPopupExtender).Show()
        End If
        If (e.CommandName = "AddAttachment") Then
            'ViewState.Add("SelectedWorkOrderId", CInt(e.CommandArgument))
            'CType(FindControl("UCFileUpload1"), UCFileUpload_Outer).AttachmentForID = CInt(e.CommandArgument)
            'CType(FindControl("UCFileUpload1"), UCFileUpload_Outer).CompanyID = 5
            Dim arr As String
            arr = e.CommandArgument.ToString
            Dim arr1 As String() = arr.Split(",")

            For Each row As GridViewRow In gvWorkOrders.Rows
                Dim rowkey As DataKey = gvWorkOrders.DataKeys(row.RowIndex)
                If (rowkey.Value.ToString = arr1(0)) Then
                    CType(row.FindControl("pnlAddAttachment"), AjaxControlToolkit.ModalPopupExtender).Show()
                    ViewState("flUpload") = CType(row.FindControl("flUpload"), FileUpload).ClientID
                End If
            Next
        End If


        If (e.CommandName = "RateWO") Then
            WorkOrderId = e.CommandArgument.ToString
            ViewState.Add("SelectedWorkOrderId", WorkOrderId)
            mdlUpdateRating.Show()
            rdBtnNegative.Checked = False
            rdBtnNeutral.Checked = False
            rdBtnPositive.Checked = False
            txtComment.Text = ""

            Dim ds As DataSet
            ds = CommonFunctions.GetWORating(WorkOrderId)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
                        Dim Result As Integer = ds.Tables(0).Rows(0)(0)
                        Select Case Result
                            Case -1
                                rdBtnNegative.Checked = True
                            Case 0
                                rdBtnNeutral.Checked = True
                            Case 1
                                rdBtnPositive.Checked = True
                        End Select
                    End If
                    txtComment.Text = ds.Tables("tblRating").Rows(0).Item("Comments")
                End If
            End If
        End If

        If (e.CommandName = "Action") Then
            Dim Param() As String
            Param = e.CommandArgument.ToString.Split(",")
            'WorkOrderId = e.CommandArgument.ToString
            WorkOrderId = CType(Param(0), Integer)
            ViewState.Add("SelectedWorkOrderId", WorkOrderId)
            CType(FindControl("mdlAction"), AjaxControlToolkit.ModalPopupExtender).Show()
            'poonam modified on 8/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
            ddlNoteType.SelectedIndex = -1
            rdBtnActionPositive.Checked = False
            rdBtnActionNeutral.Checked = False
            rdBtnActionNegative.Checked = False
            txtCommentAction.Text = ""
            pnlActionRating.Style.Add("display", "none")
            divActionWatch.Style.Add("display", "block")
            chkMyWatch.Checked = False
            chkMyRedFlag.Checked = False


            If Not IsNothing(Request("mode")) Then
                Dim Group As String = Request("mode")
                If (Group = ApplicationSettings.WOGroupAccepted Or Group = "Activeelapsed" Or Group = ApplicationSettings.WOGroupCompleted Or Group = ApplicationSettings.WOGroupClosed) Then
                    pnlActionRating.Style.Add("display", "block")
                End If
                If (Group = ApplicationSettings.WOGroupClosed) Or (Group = ApplicationSettings.WOGroupCancelled) Or (Group = ApplicationSettings.WOGroupInvoiced) Then
                    divActionWatch.Style.Add("display", "none")
                End If
            End If

            Dim Rating As Integer = CType(Param(1), Integer)
            Select Case Rating
                Case -1
                    rdBtnActionNegative.Checked = True
                Case 0
                    rdBtnActionNeutral.Checked = True
                Case 1
                    rdBtnActionPositive.Checked = True
            End Select
            txtCommentAction.Text = CType(Param(2), String)
            chkMyWatch.Checked = Param(3)
            chkMyRedFlag.Checked = Param(4)

            'Dim ds As DataSet
            'ds = CommonFunctions.GetWORating(WorkOrderId, CInt(Session("UserID")), "WOAction")
            'If ds.Tables.Count > 0 Then
            '    If ds.Tables(0).Rows.Count > 0 Then
            '        If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
            '            Dim Result As Integer = ds.Tables(0).Rows(0)(0)
            '            Select Case Result
            '                Case -1
            '                    rdBtnActionNegative.Checked = True
            '                Case 0
            '                    rdBtnActionNeutral.Checked = True
            '                Case 1
            '                    rdBtnActionPositive.Checked = True
            '            End Select
            '        End If
            '        txtCommentAction.Text = ds.Tables("tblRating").Rows(0).Item("Comments")
            '    End If
            '    If ds.Tables(1).Rows.Count > 0 Then
            '        chkMyWatch.Checked = ds.Tables(1).Rows(0).Item("IsWatched")
            '        chkMyRedFlag.Checked = ds.Tables(1).Rows(0).Item("IsFlagged")
            '    End If
            'End If

        End If


        If (e.CommandName = "Watch") Then
            Dim Param As String
            Param = e.CommandArgument.ToString
            Dim lblMultipleWOsMsg As Label = TryCast(Me.Parent.FindControl("lblMultipleWOsMsg"), Label)
            If Param <> "" Then
                ws.WSWorkOrder.AddDeleteWatchedWO(Param, Session("UserID"), "Add")
                lblMultipleWOsMsg.Visible = True
                lblMultipleWOsMsg.Text = "&nbsp;*&nbsp;Added Successfully to watched listing."
                Me.Page.GetType.InvokeMember("Populate", System.Reflection.BindingFlags.InvokeMethod, Nothing, Me.Page, New Object() {})
                PopulateGrid()
            Else
                lblMultipleWOsMsg.Visible = True
                lblMultipleWOsMsg.Text = "&nbsp;*&nbsp;" & ResourceMessageText.GetString("WOMultipleSelectError")
            End If
        End If

        If (e.CommandName = "Unwatch") Then
            Dim Param As String
            Param = e.CommandArgument.ToString
            Dim lblRemoveMsg As Label = TryCast(Me.Parent.FindControl("lblRemoveMsg"), Label)
            If Param <> "" Then
                ws.WSWorkOrder.AddDeleteWatchedWO(Param, Session("UserID"), "Remove")
                lblRemoveMsg.Visible = True
                lblRemoveMsg.Text = "&nbsp;*&nbsp;Removed Successfully from watched listing."
                Me.Page.GetType.InvokeMember("Populate", System.Reflection.BindingFlags.InvokeMethod, Nothing, Me.Page, New Object() {})
                PopulateGrid()
            Else
                lblRemoveMsg.Visible = True
                lblRemoveMsg.Text = "&nbsp;*&nbsp;" & ResourceMessageText.GetString("WOMultipleSelectError")
            End If
        End If

    End Sub


    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvWorkOrders.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvWorkOrders, e.Row, Me)
        End If

    End Sub

    Public Function ShowHidePlatformPriceIcon(ByVal PlatformPrice As Object, ByVal WOID As Object, ByVal mode As Object, ByVal FromDate As Object, ByVal ToDate As Object, ByVal ContactID As Object, ByVal CompanyID As Object, ByVal SupplierContactID As Object, ByVal SupplierCompanyID As Object, ByVal PS As Object, ByVal PN As Object, ByVal SC As Object, ByVal SO As Object, ByVal BizDiv As Object, ByVal CompID As Object, ByVal Status As Object) As String
        Dim sendLink As String = ""
        If Status = ApplicationSettings.WOGroupSubmitted Then
            sendLink = "<a runat=server id='lnkSetPlatformPrice' href= 'WOSetPlatformPrice.aspx?WOID=" & WOID & "&Group=" & mode & "&FromDate=" & FromDate & "&ToDate=" & ToDate & "&ContactID=" & ContactID & "&CompanyID=" & CompanyID & "&SupContactID=" & SupplierContactID & "&SupCompID=" & SupplierCompanyID & "&PS=" & PS & "&PN=" & PN & "&SC=" & SC & "&SO=" & SO & "&BizDivID=" & BizDiv & "&" & "CompID=" & CompID & "&sender=UCWOsListing'><img src='Images/Icons/SetPlatformPrice_Icon.gif' title='Set Portal Price' width='18' height='12' hspace='2' vspace='0' border='0'></a>"
        End If
        Return sendLink
    End Function
    Public Function ShowHideUpdateWOIcon(ByVal PlatformPrice As Object, ByVal WOID As Object, ByVal mode As Object, ByVal FromDate As Object, ByVal ToDate As Object, ByVal ContactID As Object, ByVal CompanyID As Object, ByVal SupplierContactID As Object, ByVal SupplierCompanyID As Object, ByVal PS As Object, ByVal PN As Object, ByVal SC As Object, ByVal SO As Object, ByVal BizDiv As Object, ByVal CompID As Object, ByVal Status As Object, ByVal IsNextDay As String, ByVal UCWOsListing As String) As String
        Dim sendLink As String = ""
        If Status = ApplicationSettings.WOGroupCA Then
            sendLink = "<a runat=server id='lnkUpdateWO' href= 'WOSetWholeSalePrice.aspx?WOID=" & WOID & "&Group=" & mode & "&FromDate=" & FromDate & "&ToDate=" & ToDate & "&ContactID=" & ContactID & "&CompanyID=" & CompanyID & "&SupContactID=" & SupplierContactID & "&SupCompID=" & SupplierCompanyID & "&PS=" & PS & "&PN=" & PN & "&SC=" & SC & "&SO=" & SO & "&BizDivID=" & BizDiv & "&" & "CompID=" & CompID & "&sender=UCWOsListing'><img src='Images/Icons/UpdateWO.gif' title='Update Worko0rder' width='16' height='16' hspace='2' vspace='0' border='0'></a>"
        Else
            sendLink = ""
        End If
        Return sendLink
    End Function
    Public Function ShowHideOrdermatchIcon(ByVal PlatformPrice As Object, ByVal WOID As Object, ByVal mode As Object, ByVal FromDate As Object, ByVal ToDate As Object, ByVal ContactID As Object, ByVal CompanyID As Object, ByVal SupplierContactID As Object, ByVal SupplierCompanyID As Object, ByVal PS As Object, ByVal PN As Object, ByVal SC As Object, ByVal SO As Object, ByVal BizDiv As Object, ByVal CompID As Object, ByVal Status As Object, ByVal BillingLocID As Object) As String
        Dim sendLink As String = ""
        If (BillingLocID.ToString <> "0") Then
            If (Status = ApplicationSettings.WOGroupSubmitted Or Status = ApplicationSettings.WOGroupSent Or Status = "Conditional Accept") And Not IsDBNull(PlatformPrice) Then
                sendLink = "<a runat=server id='lnkOrderMatch' href= 'WOOrdermatch.aspx?WOID=" & WOID & "&Group=" & mode & "&FromDate=" & FromDate & "&ToDate=" & ToDate & "&ContactID=" & ContactID & "&CompanyID=" & CompanyID & "&SupContactID=" & SupplierContactID & "&SupCompID=" & SupplierCompanyID & "&PS=" & PS & "&PN=" & PN & "&SC=" & SC & "&SO=" & SO & "&BizDivID=" & BizDiv & "&" & "CompID=" & CompID & "&sender=UCWOsListing'><img src='Images/Icons/FindSuppliersforWorkOrder.gif' title='OrderMatch' width='15' height='14' hspace='2' vspace='0' border='0'></a>"
            Else
                sendLink = ""
            End If
        Else
            sendLink = "<img src='Images/Icons/FindSuppliersforWorkOrder.gif' title='OrderMatch' width='15' height='14' hspace='2'  vspace='0' border='0' style='cursor:pointer;' onclick='Javascript:NoBillLocOrdermatch();'>"
        End If



        Return sendLink
    End Function

    Public Function ShowHideIcon(ByVal BuyerAccepted As Object, ByVal WOID As Object, ByVal mode As Object, ByVal FromDate As Object, ByVal ToDate As Object, ByVal ContactID As Object, ByVal CompanyID As Object, ByVal SupplierContactID As Object, ByVal SupplierCompanyID As Object, ByVal PS As Object, ByVal PN As Object, ByVal SC As Object, ByVal SO As Object, ByVal BizDiv As Object, ByVal CompID As Object, ByVal WPChangedStatus As Object) As String
        Dim sendLink As String = ""
        If Not IsDBNull(BuyerAccepted) Then
            If Not IsDBNull(WPChangedStatus) Then
                If BuyerAccepted = 0 And (mode = ApplicationSettings.WOGroupSubmitted Or mode = ApplicationSettings.WOGroupCA Or mode = ApplicationSettings.WOGroupSent) Then
                    sendLink = "<a runat=server id='lnkBuyerAcceptWR' href= 'AcceptWorkRequest.aspx?WOID=" & WOID & "&Group=" & mode & "&FromDate=" & FromDate & "&ToDate=" & ToDate & "&ContactID=" & ContactID & "&CompanyID=" & CompanyID & "&SupContactID=" & SupplierContactID & "&SupCompID=" & SupplierCompanyID & "&PS=" & PS & "&PN=" & PN & "&SC=" & SC & "&SO=" & SO & "&BizDivID=" & BizDiv & "&" & "CompID=" & CompID & "&sender=UCWOsListing'><img src='Images/Icons/AcceptedBuyerWorkRequest.gif' title='Buyer Accept Work Request' width='13' height='12' hspace='2' vspace='0' border='0'></a>"
                    gvWorkOrders.Columns.Item(29).Visible = True 'Buyer Accept WR Icon
                Else
                    sendLink = ""
                End If
            End If
        Else
            sendLink = ""
        End If
        Return sendLink
    End Function

    Public Function ShowHideCompleteWOIcon(ByVal StagedWO As Object) As Boolean
        If IsDBNull(StagedWO) Then
            Return True
        ElseIf StagedWO.Trim = "Yes" Then
            Return False
        ElseIf StagedWO.Trim = "No" Then
            Return True
        End If
    End Function
    Public Function GetActionIcon(ByVal NtCnt As Integer, ByVal Rating As Integer) As String
        Dim ActionIcon As String
        ActionIcon = "~/Images/Icons/Action.gif"
        If (NtCnt > 0 And Rating = 2) Then
            ActionIcon = "~/Images/Icons/Action-note.gif"
        ElseIf (NtCnt > 0 And Rating = 0) Then
            ActionIcon = "~/Images/Icons/Action-note-neutral.gif"
        ElseIf (NtCnt > 0 And Rating = 1) Then
            ActionIcon = "~/Images/Icons/Action-note-positive.gif"
        ElseIf (NtCnt > 0 And Rating = -1) Then
            ActionIcon = "~/Images/Icons/Action-note-negative.gif"
        ElseIf (NtCnt = 0 And Rating = 0) Then
            ActionIcon = "~/Images/Icons/Action-neutral.gif"
        ElseIf (NtCnt = 0 And Rating = 1) Then
            ActionIcon = "~/Images/Icons/Action-positive.gif"
        ElseIf (NtCnt = 0 And Rating = -1) Then
            ActionIcon = "~/Images/Icons/Action-negative.gif"
        End If
        Return ActionIcon
    End Function

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvWorkOrders.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvWorkOrders.DataBind()
        'ME.Parent.FindControl("")  
    End Sub

    ''' <summary>
    ''' GridView event handler to call MakeGridViewHeaderClickable method
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvWorkOrders.RowDataBound
        MakeGridViewHeaderClickable(gvWorkOrders, e.Row)
        If e.Row.RowType = DataControlRowType.DataRow Then
            If HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupAccepted Or HttpContext.Current.Items("Group") = "Activeelapsed" Then
                Dim dt As New DataTable
                dt = CType(Session("tblDepotSheet"), DataTable)
                Dim dv As DataView
                dv = dt.DefaultView
                Dim hdnwoid As HtmlInputHidden
                hdnwoid = CType(e.Row.FindControl("hdnWOIDs"), HtmlInputHidden)
                If Not IsNothing(hdnwoid) Then
                    dv.RowFilter = "woid = " & hdnwoid.Value
                End If
                dt = dv.ToTable
                CType(e.Row.FindControl("RptDepotSheet"), Repeater).DataSource = dt.DefaultView
                CType(e.Row.FindControl("RptDepotSheet"), Repeater).DataBind()

            End If
            If HttpContext.Current.Items("Group") <> ApplicationSettings.WOGroupCA And HttpContext.Current.Items("Group") <> ApplicationSettings.WOGroupDraft Then
                Dim PlatformPrice As Decimal
                Dim WholesalePrice As Decimal
                Dim UpSellPrice As Decimal

                PlatformPrice = DataBinder.Eval(e.Row.DataItem, "PlatformPrice")
                WholesalePrice = DataBinder.Eval(e.Row.DataItem, "WholesalePrice")
                UpSellPrice = DataBinder.Eval(e.Row.DataItem, "UpSellPrice")
                If (PlatformPrice > (WholesalePrice + UpSellPrice)) Then
                    CType(e.Row.FindControl("SpanPP"), HtmlGenericControl).Style.Add("color", "Red")
                End If
            End If
            Dim ds As New DataSet
            If Not IsNothing(Session("dsAttachedFiles")) Then
                ds = Session("dsAttachedFiles")
                CType(e.Row.FindControl("dlAttList"), DataList).DataSource = ds
                CType(e.Row.FindControl("dlAttList"), DataList).DataBind()

            End If

            Dim hdnwid As HtmlInputHidden
            hdnwid = CType(e.Row.FindControl("hdnWID"), HtmlInputHidden)
            Dim hdncid As HtmlInputHidden
            hdncid = CType(e.Row.FindControl("hdnCID"), HtmlInputHidden)

            If (DataBinder.Eval(e.Row.DataItem, "TrackSP") = "1" And (HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupAccepted Or HttpContext.Current.Items("Group") = "Activeelapsed" Or HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupCompleted)) Then
                Dim col As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#FFFFBC")
                e.Row.BackColor = col
                'e.Row.Attributes.Add("onMouseOver", "style.background='#FFEBC6';")
                'e.Row.Attributes.Add("onMouseOut", "style.background='#FFFFBC';")
            ElseIf (DataBinder.Eval(e.Row.DataItem, "IsWatch") = "1") Then

                Dim col As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#993366")
                e.Row.BackColor = col
                e.Row.ForeColor = Drawing.Color.White
                For Each tableCell As TableCell In e.Row.Cells
                    tableCell.ForeColor = Drawing.Color.White
                Next
            Else

                Dim OriginalCol As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#EDEDED")
                e.Row.BackColor = OriginalCol
                For Each tableCell As TableCell In e.Row.Cells
                    tableCell.ForeColor = Drawing.Color.Black
                Next
            End If
            'If (DataBinder.Eval(e.Row.DataItem, "IsWatch") = "1") Then
            '    Dim col As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#F78E18")
            '    e.Row.BackColor = col
            '    'e.Row.Attributes.Add("onMouseOver", "style.background='#FFEBC6';")
            '    'e.Row.Attributes.Add("onMouseOut", "style.background='#FFFFBC';")           
            'Else
            '    Dim OriginalCol As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#EDEDED")
            '    e.Row.BackColor = OriginalCol
            'End If
            If ((HttpContext.Current.Items("Group") = "Watched") Or (HttpContext.Current.Items("Group") = ApplicationSettings.WOGroupCancelled)) Then
                Dim OriginalCol As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#EDEDED")
                e.Row.BackColor = OriginalCol
                For Each tableCell As TableCell In e.Row.Cells
                    tableCell.ForeColor = Drawing.Color.Black
                Next
            End If


        End If
    End Sub

    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Public Sub processBackToListing()
        BizDiv = Request("BizDivID")
        FromDate = Request("FromDate")
        ToDate = Request("ToDate")
        gvWorkOrders.PageSize = Request("PS")
        WorkorderID = Request("SearchWorkorderID")
        'Sort Expe
        ViewState!SortExpression = ""
        If Trim(Request("SC")) <> "" Then
            ViewState!SortExpression = Trim(Request("SC"))
        Else
            ViewState!SortExpression = "DateCreated"
        End If
        'Sort direction
        Dim sd As SortDirection
        sd = SortDirection.Descending
        If Trim(Request("SO")) <> "" Then
            If Trim(Request("SO")).ToLower = "0" Then
                sd = SortDirection.Ascending
            Else
                sd = SortDirection.Descending
            End If
        End If
        SetGridSettings()
        gvWorkOrders.Sort(ViewState!SortExpression, sd)
        'gvWorkOrders.DataBind()
        If Request("PN") <> "-1" Then
            gvWorkOrders.PageIndex = IIf(Request("PN") = "", 0, Request("PN"))
        Else
            gvWorkOrders.PageIndex = 0
        End If

    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub

    Public Function GetLinks(ByVal parambizDivId As Object, ByVal companyId As Object, ByVal contactId As Object, ByVal SupplierCompanyName As String) As String
        Dim link As String = ""
        Dim contactType As ApplicationSettings.ContactType = ApplicationSettings.ContactType.approvedSupplier
        Dim classId As String
        Dim statusId As String
        classId = ApplicationSettings.RoleSupplierID
        statusId = ApplicationSettings.StatusApprovedCompany

        Dim bizDivId = parambizDivId

        link &= "<a class='footerTxtSelected ' href='companyprofile.aspx?sender=AdminWoListing&bizDivId=" & bizDivId & "&contactType=" & contactType & "&WorkorderID=" & contactType & "&companyid=" & companyId & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&mode=" & Request("mode") & "&ps=" & gvWorkOrders.PageSize & "&pn=" & gvWorkOrders.PageIndex & "&sc=" & gvWorkOrders.SortExpression & "&so=" & gvWorkOrders.SortDirection & "&CompID=" & CompID & "&FromDate=" & FromDate & "&ToDate=" & ToDate & "'>" & SupplierCompanyName & "</a>"

        Return link

    End Function
    Public Function ShowHideUserNotes(ByVal WOID As Integer) As String
        If Not IsNothing(HttpContext.Current.Items("tblWOUserNotes")) Then
            Dim dt As DataTable = HttpContext.Current.Items("tblWOUserNotes")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "WOID = " & WOID & " and Comments <> ''"
            If dv.Count = 0 Then
                Return "False"
            Else
                Return "True"
            End If
        Else
            Return "False"
        End If

    End Function
    Public Function GetUserNotesDetail(ByVal WOID As Integer) As DataView
        Dim dv As DataView
        If Not IsNothing(HttpContext.Current.Items("tblWOUserNotes")) Then
            Dim dt As DataTable = HttpContext.Current.Items("tblWOUserNotes")
            dv = dt.DefaultView
            dv.RowFilter = "WOID = " & WOID
            Return dv
        Else
            Return dv
        End If
    End Function

    ''' <summary>
    ''' Method to get the Selected WOIDs
    ''' </summary>
    ''' <remarks>PratikT-19-aug, 2008</remarks>
    Public Sub GetSelectedWOIDs()
        Dim selectedIds As String = ""
        For Each row As GridViewRow In gvWorkOrders.Rows
            Dim chkBox As CheckBox = CType(row.FindControl("Check"), CheckBox)
            If Not (chkBox Is Nothing) Then
                If chkBox.Checked Then
                    If selectedIds = "" Then
                        selectedIds = CType(row.FindControl("hdnWOIDs"), HtmlInputHidden).Value
                    Else
                        selectedIds = selectedIds & "," & CType(row.FindControl("hdnWOIDs"), HtmlInputHidden).Value
                    End If
                End If
            End If
        Next
        'Assign Selected to UC Property.
        SelectedWOIDs = selectedIds
    End Sub

    Public Sub hdnBtnRateWO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnBtnRateWO.Click

        Dim Rating As Integer
        If rdBtnNegative.Checked = True Then
            Rating = -1
        ElseIf rdBtnPositive.Checked = True Then
            Rating = 1
        Else
            Rating = 0
        End If
        Dim Success As DataSet = CommonFunctions.SaveRatingComment(ViewState("SelectedWorkOrderId"), Session("CompanyID"), Session("UserID"), txtComment.Text.Trim, Rating)
        txtComment.Text = ""

        PopulateGrid()
    End Sub

    Private Sub hdnBtnAction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnBtnAction.Click
        Dim Note As String = txtNoteAction.Text.Trim.Replace(Chr(13), "<BR>")
        Dim Comments As String = txtCommentAction.Text.Trim.Replace(Chr(13), "<BR>")
        Dim IsFlagged As Boolean = chkMyRedFlag.Checked
        Dim IsWatched As Boolean = chkMyWatch.Checked

        Dim Rating As Integer

        If (pnlActionRating.Style.Item("display") <> "none") Then
            If rdBtnActionNegative.Checked = True Then
                Rating = -1
            ElseIf rdBtnActionPositive.Checked = True Then
                Rating = 1
            ElseIf rdBtnActionNeutral.Checked = True Then
                Rating = 0
            Else
                Rating = 2
            End If
        Else
            Rating = 2
        End If
        'poonam modified on 8/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
        Dim Success As DataSet = CommonFunctions.SaveActionCommentNotes(ViewState("SelectedWorkOrderId"), Session("UserID"), Note, Comments, Session("CompanyID"), Rating, IsFlagged, IsWatched, chkShowClient.Checked, chkShowSupplier.Checked, ddlNoteType.SelectedItem.Value)
        txtNoteAction.Text = ""
        txtCommentAction.Text = ""
        PopulateGrid()
    End Sub
End Class