Imports System.IO
Partial Public Class UCAccountSettings
    Inherits System.Web.UI.UserControl


    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs

    ''' <summary>
    ''' Property to set or get company value for UCCompanyProfile
    ''' </summary>
    ''' <remarks></remarks>

    Private _bizDivID As Integer
    Public Property BizDivId() As Integer
        Get
            Return _bizDivID
        End Get
        Set(ByVal value As Integer)
            _bizDivID = value
        End Set
    End Property

    Private _companyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property

    Private _strBackToListing As String = ""
    Public Property strBackToListing() As String
        Get
            Return _strBackToListing
        End Get
        Set(ByVal value As String)
            _strBackToListing = value
        End Set
    End Property
    ''' <summary>
    ''' To handle Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsNothing(Request("CompanyID")) Then
            If Request("CompanyID") <> "" Then
                CompanyID = Request("CompanyID")
            End If
        End If

        BizDivId = Session("BizDivId")
        'OA-622 discount changes
        If Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Or Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID Then
            hdnRoleId.Value = "True"
        Else
            hdnRoleId.Value = "False"
        End If

        If Not IsNothing(Session("BusinessArea")) Then
            If Session("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                chkEveningInst.Visible = True
                pnlSettings.Visible = True
            Else
                chkEveningInst.Visible = False
                pnlSettings.Visible = False
            End If
        Else
            chkEveningInst.Visible = False
            pnlSettings.Visible = False
        End If

        If Not IsPostBack Then
            PopulateAccountSettings()
        End If
        lnkBackToListing.HRef = getBackToListingLink()
    End Sub

    ''' <summary>
    ''' To populate the setting for Account from DB
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub PopulateAccountSettings()

        Dim dsContacts As DataSet
        dsContacts = GetContact(Page, ApplicationSettings.BizDivId, True, True, CompanyID)
        Dim dvContact As DataView
        'dvContact = dsContacts.Tables("ContactAttributes").Copy.DefaultView
        'Added By Snehal 18 Nov 09 - version control
        If dsContacts.Tables("Contacts").Rows.Count > 0 Then
            'Store the version number in session for Contacts
            CommonFunctions.StoreVerNum(dsContacts.Tables("Contacts"))
        End If
        If dsContacts.Tables(9).Rows.Count > 0 Then
            'Populate Goods Location dropdown
            Dim dvLocation1 As DataView = dsContacts.Tables(9).DefaultView
            drpdwnGoodsCollection.DataSource = dvLocation1.ToTable
            drpdwnGoodsCollection.DataBind()
        End If

        BindTheDefaultBookingGrid()


        'OA-622 - discount changes
        If Not IsNothing(Request("classid")) Then
            If Request("classid") = ApplicationSettings.RoleClientID Then
                If Not IsPostBack Then
                    ddlCompanyType.Items.Insert(0, "Orderwork")
                    ddlCompanyType.Items.Insert(1, "Empowered")
                    ddlCompanyType.DataBind()
                End If
            End If
        End If

        Dim li As New ListItem
        li = New ListItem
        li.Text = "Customer's Home"
        li.Value = 1
        drpdwnGoodsCollection.Items.Insert(0, li)
        Dim li1 As New ListItem
        li1 = New ListItem
        li1.Text = "None Specified"
        li1.Value = 0
        drpdwnGoodsCollection.Items.Insert(0, li1)

        SetCacheForAttachment(_companyID, "CompanyLogo", dsContacts.Tables("ContactAttachments").Copy.DefaultView)
        dvContact = dsContacts.Tables("Contacts").Copy.DefaultView
        If dvContact.Count > 0 Then
            If Not IsDBNull(dvContact.Item(0).Item("EnableGoodsLocation")) Then
                chkEnableGoodsLoc.Checked = CType(dvContact.Item(0).Item("EnableGoodsLocation"), Boolean)
            End If

            drpdwnGoodsCollection.SelectedValue = IIf(Not IsDBNull(dvContact.Item(0).Item("DefaultGoodsLocation")), dvContact.Item(0).Item("DefaultGoodsLocation"), 1)

            If Not IsDBNull(dvContact.Item(0).Item("IsBrigantiaMember")) Then
                chkBrigantia.Checked = CType(dvContact.Item(0).Item("IsBrigantiaMember"), Boolean)
            End If

            drpDefaultPage.SelectedValue = IIf(Not IsDBNull(dvContact.Item(0).Item("DefaultHomePage")), dvContact.Item(0).Item("DefaultHomePage"), "")

            If Not IsDBNull(dvContact.Item(0).Item("IsClientJobListEnabled")) Then
                chkEnableClientJobList.Checked = CType(dvContact.Item(0).Item("IsClientJobListEnabled"), Boolean)
            End If
            If Not IsDBNull(dvContact.Item(0).Item("WONotification")) Then
                chkWoEmailNotice.Checked = CType(dvContact.Item(0).Item("WONotification"), Boolean)
            End If
            If Not IsDBNull(dvContact.Item(0).Item("TrackSP")) Then
                chkTrackSP.Checked = CType(dvContact.Item(0).Item("TrackSP"), Boolean)
            End If
            If Not IsDBNull(dvContact.Item(0).Item("SLA")) Then
                If (dvContact.Item(0).Item("SLA") <> -1) Then
                    chkSLA.Checked = CType(dvContact.Item(0).Item("SLA"), Integer)
                Else
                    chkSLA.Checked = False
                End If
            Else
                chkSLA.Checked = False
            End If

            If Not IsDBNull(dvContact.Item(0).Item("LockSkillSet")) Then
                chkLockSkillSets.Checked = CType(dvContact.Item(0).Item("LockSkillSet"), Boolean)
                If (chkLockSkillSets.Checked = True) Then
                    txtLockSkillSetComments.Style.Add("display", "inline")
                    spanfldLockSkillSetComments.Style.Add("display", "inline")
                    txtFeildSkillSet.Enabled = True

                Else
                    txtLockSkillSetComments.Style.Add("display", "none")
                    spanfldLockSkillSetComments.Style.Add("display", "none")
                    txtFeildSkillSet.Enabled = False
                End If
            Else
                chkLockSkillSets.Checked = False
                txtFeildSkillSet.Enabled = False
                txtLockSkillSetComments.Style.Add("display", "none")
                spanfldLockSkillSetComments.Style.Add("display", "none")
            End If
            If Not IsDBNull(dvContact.Item(0).Item("LockSkillSetComment")) Then
                txtLockSkillSetComments.Text = dvContact.Item(0).Item("LockSkillSetComment")
            End If
            If Not IsDBNull(dvContact.Item(0).Item("RPUplift")) Then
                txtRPUplift.Value = dvContact.Item(0).Item("RPUplift")
            End If
            If Not IsDBNull(dvContact.Item(0).Item("WPUplift")) Then
                txtWPUplift.Value = dvContact.Item(0).Item("WPUplift")
            End If
            If Not IsDBNull(dvContact.Item(0).Item("PPUplift")) Then
                txtPPUplift.Value = dvContact.Item(0).Item("PPUplift")
            End If
            If Not IsDBNull(dvContact.Item(0).Item("IsHolidaysDisabled")) Then
                chkDisableHolidays.Checked = CType(dvContact.Item(0).Item("IsHolidaysDisabled"), Boolean)
            Else
                chkDisableHolidays.Checked = True
            End If
            If Not IsDBNull(dvContact.Item(0).Item("IsEveningInstallationEnabled")) Then
                chkEveningInst.Checked = CType(dvContact.Item(0).Item("IsEveningInstallationEnabled"), Boolean)
            End If
            If Not IsDBNull(dvContact.Item(0).Item("IsProfIdemnityDisabled")) Then
                chkProfIdem.Checked = CType(dvContact.Item(0).Item("IsProfIdemnityDisabled"), Boolean)
            End If
            If Not IsDBNull(dvContact.Item(0).Item("NewPortalAccess")) Then
                chkNewPortalAccess.Checked = CType(dvContact.Item(0).Item("NewPortalAccess"), Boolean)
            End If
            If Not IsDBNull(dvContact.Item(0).Item("HideOldPortalAccess")) Then
                chkOldPortalAccess.Checked = CType(dvContact.Item(0).Item("HideOldPortalAccess"), Boolean)
            End If
            'OA-622 - discount changes
            If Not IsNothing(Request("classid")) Then
                If Request("classid") = ApplicationSettings.RoleSupplierID Then
                    If dvContact.Count > 0 Then
                        pnlSupplierFee.Style.Add("display", "inline")
                        If Not IsDBNull(dvContact.Item(0).Item("servicePartnerFees")) Then
                            txtSPFee.Text = dvContact.Item(0).Item("servicePartnerFees")
                        End If
                    Else
                        pnlSupplierFee.Style.Add("display", "none")
                    End If
                Else
                    pnlSupplierFee.Style.Add("display", "none")
                End If
            End If

            'for client show admin fee
            If Not IsNothing(Request("classid")) Then
                If Request("classid") = ApplicationSettings.RoleClientID Then
                    If dvContact.Count > 0 Then
                        pnlAdminFee.Style.Add("display", "inline")
                        If Not IsDBNull(dvContact.Item(0).Item("servicePartnerFees")) Then
                            txtAdminFee.Text = dvContact.Item(0).Item("servicePartnerFees")
                        End If
                        If Not IsDBNull(dvContact.Item(0).Item("source")) Then
                            If (dvContact.Item(0).Item("source") <> "") Then
                               ddlCompanyType.SelectedValue = ddlCompanyType.Items.FindByText(dvContact.Item(0).Item("source")).ToString
                               End If
                        End If
                        If hdnRoleId.Value = "False" And ddlCompanyType.SelectedValue = "Empowered" Then
                            txtAdminFee.Text = "0"
                            txtAdminFee.Enabled = False
                        End If
                        If hdnRoleId.Value = "False" And ddlCompanyType.SelectedValue = "Orderwork" Then
                            txtAdminFee.Text = "10"
                            txtAdminFee.Enabled = False
                        End If
                    Else
                        pnlAdminFee.Style.Add("display", "none")
                    End If
                Else
                    pnlAdminFee.Style.Add("display", "none")
                End If
            End If



            If Not IsDBNull(dvContact.Item(0).Item("EveningInstTime")) Then
                txtEvngSlotLabel.Value = dvContact.Item(0).Item("EveningInstTime")
            End If
            'Faizan for Companytag line
            If Not IsDBNull(dvContact.Item(0).Item("CompanyTagLine")) Then
                txtcomptag.Text = dvContact.Item(0).Item("CompanyTagLine")
            End If
            If Not IsNothing(Request("classid")) Then
                If Request("classid") = ApplicationSettings.RoleSupplierID Then
                    If dvContact.Count > 0 Then
                        pnlPrefSP.Visible = True
                        If Not IsDBNull(dvContact.Item(0).Item("PrefSPPricing")) Then
                            chkPrefSP.Checked = CType(dvContact.Item(0).Item("PrefSPPricing"), Boolean)
                        Else
                            chkPrefSP.Checked = False
                        End If
                        hdnEvengInst.Value = chkEveningInst.Checked
                    Else
                        pnlPrefSP.Visible = True
                        chkPrefSP.Checked = False
                    End If
                Else
                    pnlPrefSP.Visible = False
                    chkPrefSP.Checked = False
                End If
            Else
                chkPrefSP.Checked = False
            End If
            If dvContact.Count > 0 Then
                If Not IsDBNull(dvContact.Item(0).Item("RPUpliftPercent")) Then
                    If dvContact.Item(0).Item("RPUpliftPercent") Then
                        rdoRPUPPerc.Checked = True
                    Else
                        rdoRPUPFixed.Checked = True
                    End If
                End If
            ElseIf chkEveningInst.Checked = True Then
                rdoRPUPFixed.Checked = True
            End If
            If dvContact.Count > 0 Then
                If Not IsDBNull(dvContact.Item(0).Item("WPUpliftPercent")) Then
                    If dvContact.Item(0).Item("WPUpliftPercent") Then
                        rdoWPUPPerc.Checked = True
                    Else
                        rdoWPUPFixed.Checked = True
                    End If
                End If
            ElseIf chkEveningInst.Checked = True Then
                rdoWPUPFixed.Checked = True
            End If
            If dvContact.Count > 0 Then
                If Not IsDBNull(dvContact.Item(0).Item("PPUpliftPercent")) Then
                    If dvContact.Item(0).Item("PPUpliftPercent") Then
                        rdoPPUPPerc.Checked = True
                    Else
                        rdoPPUPFixed.Checked = True
                    End If
                End If
            ElseIf chkEveningInst.Checked = True Then
                rdoPPUPFixed.Checked = True
            End If

            If Not IsDBNull(dvContact.Item(0).Item("IsSaturdayDisabled")) Then
                chkDisableSaturdays.Checked = CType(dvContact.Item(0).Item("IsSaturdayDisabled"), Boolean)
            Else
                If Not IsNothing(Session("BusinessArea")) Then
                    If Session("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                        chkDisableSaturdays.Checked = False
                    Else
                        chkDisableSaturdays.Checked = True
                    End If
                Else
                    chkDisableSaturdays.Checked = True
                End If
            End If
            If Not IsDBNull(dvContact.Item(0).Item("IsSundayDisabled")) Then
                chkDisableSundays.Checked = CType(dvContact.Item(0).Item("IsSundayDisabled"), Boolean)
            Else
                chkDisableSundays.Checked = True
            End If
            If Not IsDBNull(dvContact.Item(0).Item("CategoryType")) Then
                drpCategoryType.SelectedValue = CStr(dvContact.Item(0).Item("CategoryType"))
            Else
                drpCategoryType.SelectedValue = ""
            End If

            'Populate wo creation flow drop down 
            CommonFunctions.PopulateWoCreationFlow(Page, ddlWoCreatioFlow)
            If Not IsDBNull(dvContact.Item(0).Item("WOCreationFlow")) Then
                ddlWoCreatioFlow.SelectedValue = dvContact.Item(0).Item("WOCreationFlow")
            Else
                ddlWoCreatioFlow.SelectedValue = "0"
            End If


            ' NoChargeClient
            ' NoChargeSP
            ' NoChargeFavSP 11 10 1

            Dim NoChargeClient As Boolean = False
            Dim NoChargeSP As Boolean = False
            Dim NoChargeFavSP As Boolean = False

            If Not IsDBNull(dvContact.Item(0).Item("ChargeClient")) Then
                If dvContact.Item(0).Item("ChargeClient") <> 0 And dvContact.Item(0).Item("ChargeClient") <> 11 And dvContact.Item(0).Item("ChargeClient") <> 10 And dvContact.Item(0).Item("ChargeClient") <> 1 Then
                    NoChargeClient = Left(dvContact.Item(0).Item("ChargeClient"), 1)
                    NoChargeSP = dvContact.Item(0).Item("ChargeClient").ToString.Substring(1, 1)
                    NoChargeFavSP = Right(dvContact.Item(0).Item("ChargeClient"), 1)
                ElseIf dvContact.Item(0).Item("ChargeClient") = 11 Then
                    NoChargeClient = False
                    NoChargeSP = True
                    NoChargeFavSP = True
                ElseIf dvContact.Item(0).Item("ChargeClient") = 10 Then
                    NoChargeClient = False
                    NoChargeSP = True
                    NoChargeFavSP = False
                ElseIf dvContact.Item(0).Item("ChargeClient") = 1 Then
                    NoChargeClient = False
                    NoChargeSP = False
                    NoChargeFavSP = True
                End If

                chkNoChargeClient.Checked = NoChargeClient
                chkNoChargeSP.Checked = NoChargeSP
                chkNoChargeFavSP.Checked = NoChargeFavSP
                chkAllowChangePassword.Checked = dvContact.Item(0).Item("IsPasswordChangeAllowed")

            End If

            If Not IsDBNull(dvContact.Item(0).Item("DiscountPercentage")) Then
                If dvContact.Item(0).Item("DiscountPercentage") <> 0 Then
                    chkDiscount.Checked = True
                    pnldisctxt.Style.Add("display", "inline")
                    txtDiscountPercent.Text = dvContact.Item(0).Item("DiscountPercentage")
                Else
                    txtDiscountPercent.Text = ""
                    chkDiscount.Checked = False
                    pnldisctxt.Style.Add("display", "none")
                End If
            Else
                txtDiscountPercent.Text = ""
                chkDiscount.Checked = False
                pnldisctxt.Style.Add("display", "none")
            End If
        End If
        If chkEveningInst.Checked = True Then
            pnlSettings.Style.Add("display", "inline")
        End If
        ' Disable Holiday/Saturday/Sunday Checked means its working day and hence we need to save by Nothing
        ' For example
        ' if IsSaturdayDisabled is checked then it means working and we need to save False as IsDisabled value

        chkDisableHolidays.Checked = Not (chkDisableHolidays.Checked)
        chkDisableSaturdays.Checked = Not (chkDisableSaturdays.Checked)
        chkDisableSundays.Checked = Not (chkDisableSundays.Checked)
        Dim dvContactsQA As New DataView(dsContacts.Tables("FinanceSettings").Copy)
        dvContactsQA.RowFilter = "Type = 'Finance'"
        If dvContactsQA.Count > 0 Then
            txtField1.Text = dvContactsQA.Item(0)("Field1Label")
            txtField2.Text = dvContactsQA.Item(0)("Field2Label")
            txtField3.Text = dvContactsQA.Item(0)("Field3Label")
            txtField4.Text = dvContactsQA.Item(0)("Field4Label")
            txtFieldValue1.Text = dvContactsQA.Item(0)("Field1Value")
            txtFieldValue2.Text = dvContactsQA.Item(0)("Field2Value")
            txtFieldValue3.Text = dvContactsQA.Item(0)("Field3Value")
            txtFieldValue4.Text = dvContactsQA.Item(0)("Field4Value")
            chkField1.Checked = dvContactsQA.Item(0)("Field1Enabled")
            chkField2.Checked = dvContactsQA.Item(0)("Field2Enabled")
            chkField3.Checked = dvContactsQA.Item(0)("Field3Enabled")
            chkField4.Checked = dvContactsQA.Item(0)("Field4Enabled")
            If chkField1.Checked Then
                reqfld1.Enabled = True
                reqFldLbl1.Enabled = True
                spanfld1.Style.Add("display", "inline")
                spanfldVal1.Style.Add("display", "inline")
            End If
            If chkField2.Checked Then
                reqfld2.Enabled = True
                reqFldLbl2.Enabled = True
                spanfld2.Style.Add("display", "inline")
                spanfldVal2.Style.Add("display", "inline")
            End If
            If chkField3.Checked Then
                reqfld3.Enabled = True
                reqFldLbl3.Enabled = True
                spanfld3.Style.Add("display", "inline")
                spanfldVal3.Style.Add("display", "inline")
            End If
            If chkField4.Checked Then
                reqfld4.Enabled = True
                reqFldLbl4.Enabled = True
                spanfld4.Style.Add("display", "inline")
                spanfldVal4.Style.Add("display", "inline")
            End If
        End If
        If dsContacts.Tables("TimeSlots").Rows.Count <> 0 Then
            txtTimeSlot1.Text = dsContacts.Tables("TimeSlots").Rows(0).Item("TimeSlot1")
            txtTimeSlot2.Text = dsContacts.Tables("TimeSlots").Rows(0).Item("TimeSlot2")
            txtTimeSlot3.Text = dsContacts.Tables("TimeSlots").Rows(0).Item("TimeSlot3")
            txtTimeSlot4.Text = dsContacts.Tables("TimeSlots").Rows(0).Item("TimeSlot4")
            chkTimeSlot1.Checked = dsContacts.Tables("TimeSlots").Rows(0).Item("TimeSlot1Enabled")
            chkTimeSlot2.Checked = dsContacts.Tables("TimeSlots").Rows(0).Item("TimeSlot2Enabled")
            chkTimeSlot3.Checked = dsContacts.Tables("TimeSlots").Rows(0).Item("TimeSlot3Enabled")
            chkTimeSlot4.Checked = dsContacts.Tables("TimeSlots").Rows(0).Item("TimeSlot4Enabled")
            If chkTimeSlot1.Checked Then
                rfvTimeSlot1.Enabled = True
                spnTimeSlot1.Style.Add("display", "inline")
            End If

            If chkTimeSlot2.Checked Then
                rfvTimeSlot2.Enabled = True
                spnTimeSlot2.Style.Add("display", "inline")
            End If

            If chkTimeSlot3.Checked Then
                rfvTimeSlot3.Enabled = True
                spnTimeSlot3.Style.Add("display", "inline")
            End If
            If chkTimeSlot4.Checked Then
                rfvTimeSlot4.Enabled = True
                spnTimeSlot4.Style.Add("display", "inline")
            End If
        End If
        UCFileUpload1.CompanyID = _companyID
        UCFileUpload1.AttachmentForID = _companyID
        UCFileUpload1.BizDivID = _bizDivID
        UCFileUpload1.AttachmentForSource = "CompanyLogo"
        UCFileUpload1.ClearUCFileUploadCache()
        UCFileUpload1.PopulateAttachedFiles()
        UCFileUpload1.PopulateExistingFiles()

    End Sub

    ''' <summary>
    ''' To handle save profile action triggerd by the Save LinkButton at the top
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub SaveTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click, btnSaveBottom.Click
        setValidations()
        Page.Validate()
        If Page.IsValid Then
            Dim RPUplift As Decimal
            Dim WPUplift As Decimal
            Dim PPUplift As Decimal
            Dim IsValid As Boolean = False
            Try
                RPUplift = CDec(txtRPUplift.Value)
                WPUplift = CDec(txtWPUplift.Value)
                PPUplift = CDec(txtPPUplift.Value)
                IsValid = True
            Catch ex As Exception
                IsValid = False
            End Try
            If IsValid Then

                If Not IsNothing(Request("CompanyID")) Then
                    If Request("CompanyID") <> "" Then
                        CompanyID = Request("CompanyID")
                    End If
                End If

                Dim ServicePartnerFees As Decimal = 0.0
                If Not IsNothing(Request("classid")) Then
                    If Request("classid") = ApplicationSettings.RoleClientID Then
                        ServicePartnerFees = CDec(txtAdminFee.Text.Trim())
                    Else
                        ServicePartnerFees = CDec(txtSPFee.Text.Trim())
                    End If

                End If


                Dim dsContacts As New XSDContacts()

                'For updating tblContactsAttributes
                'Save Is Brigantia Member Name
                Dim nrowCA As XSDContacts.ContactsAttributesRow = dsContacts.ContactsAttributes.NewRow
                With nrowCA
                    .ContactID = CompanyID
                    .IsBrigantiaMember = chkBrigantia.Checked
                    .DefaultHomePage = drpDefaultPage.SelectedItem.Value
                End With
                dsContacts.ContactsAttributes.Rows.Add(nrowCA)

                ' adding setting for IsProfIdemnityDisabled
                Dim nrowProf As XSDContacts.ContactsSettingsRow = dsContacts.ContactsSettings.NewRow
                With nrowProf
                    .ContactID = CompanyID
                    .IsProfIdemnityDisabled = chkProfIdem.Checked
                    .IsClientJobListEnabled = chkEnableClientJobList.Checked
                    .TrackSP = chkTrackSP.Checked
                    If (chkSLA.Checked) Then
                        .SLA = 1
                    Else
                        .SLA = 0
                    End If

                    If chkNoChargeClient.Checked = True And chkNoChargeSP.Checked = True And chkNoChargeFavSP.Checked = True Then
                        .ChargeClient = 111
                    ElseIf chkNoChargeClient.Checked = True And chkNoChargeSP.Checked = True And chkNoChargeFavSP.Checked = False Then
                        .ChargeClient = 110
                    ElseIf chkNoChargeClient.Checked = True And chkNoChargeSP.Checked = False And chkNoChargeFavSP.Checked = False Then
                        .ChargeClient = 100
                    ElseIf chkNoChargeClient.Checked = True And chkNoChargeFavSP.Checked = True And chkNoChargeSP.Checked = False Then
                        .ChargeClient = 101
                    ElseIf chkNoChargeSP.Checked = True And chkNoChargeFavSP.Checked = True And chkNoChargeClient.Checked = False Then
                        .ChargeClient = 11 '011
                    ElseIf chkNoChargeSP.Checked = True And chkNoChargeClient.Checked = False And chkNoChargeFavSP.Checked = False Then
                        .ChargeClient = 10 '010
                    ElseIf chkNoChargeFavSP.Checked = True And chkNoChargeClient.Checked = False And chkNoChargeSP.Checked = False Then
                        .ChargeClient = 1 '001
                    Else
                        .ChargeClient = 0 ' 000
                    End If

                    .IsPasswordChangeAllowed = chkAllowChangePassword.Checked

                    .LockSkillSet = chkLockSkillSets.Checked
                    .LockSkillSetComment = txtLockSkillSetComments.Text.Trim
                    .WONotification = chkWoEmailNotice.Checked
                    .IsEveningInstallationEnabled = chkEveningInst.Checked
                    .RPUplift = CDec(txtRPUplift.Value.Trim)
                    .WPUplift = CDec(txtWPUplift.Value.Trim)
                    .PPUplift = txtPPUplift.Value.Trim
                    .CompanyTagLine = txtcomptag.Text.ToString.Trim
                    .IsHolidaysDisabled = Not (chkDisableHolidays.Checked)
                    .IsSaturdayDisabled = Not (chkDisableSaturdays.Checked)
                    .IsSundayDisabled = Not (chkDisableSundays.Checked)
                    .ServicePartnerFees = ServicePartnerFees
                    .EnableGoodsLocation = chkEnableGoodsLoc.Checked
                    If Not IsNothing(drpdwnGoodsCollection) Then
                        If drpdwnGoodsCollection.SelectedValue <> "" Then
                            .DefaultGoodsLocation = drpdwnGoodsCollection.SelectedValue
                        End If
                    End If
                    If Not IsNothing(Request("classid")) Then
                        If Request("classid") = ApplicationSettings.RoleSupplierID Then
                            .PrefSPPricing = chkPrefSP.Checked
                        End If
                    End If
                    If chkDiscount.Checked = True Then
                        .DiscountPercentage = txtDiscountPercent.Text
                    End If

                    .EveningInstTime = txtEvngSlotLabel.Value

                    .RPUpliftPercent = rdoRPUPPerc.Checked

                    .WPUpliftPercent = rdoWPUPPerc.Checked

                    .PPUpliftPercent = rdoPPUPPerc.Checked

                End With

                dsContacts.ContactsSettings.Rows.Add(nrowProf)

                dsContacts = UCFileUpload1.ReturnFilledAttachments(dsContacts)

                Dim dsFinance As New OWContactsQASettings()

                Dim nrowFinanceLabels As OWContactsQASettings.tblContactsQASettingsRow = dsFinance.tblContactsQASettings.NewRow
                With nrowFinanceLabels
                    .CompanyID = CompanyID
                    .Field1Label = txtField1.Text
                    .Field1Value = txtFieldValue1.Text
                    .Field1Enabled = chkField1.Checked

                    .Field2Label = txtField2.Text
                    .Field2Value = txtFieldValue2.Text
                    .Field2Enabled = chkField2.Checked

                    .Field3Label = txtField3.Text
                    .Field3Value = txtFieldValue3.Text
                    .Field3Enabled = chkField3.Checked

                    .Field4Label = txtField4.Text
                    .Field4Value = txtFieldValue4.Text
                    .Field4Enabled = chkField4.Checked
                    .Type = "Finance"
                    .WOID = 0
                End With
                dsFinance.tblContactsQASettings.Rows.Add(nrowFinanceLabels)


                Dim nrowTimeSlots As XSDContacts.tblContactsTimeSlotsRow = dsContacts.tblContactsTimeSlots.NewRow
                With nrowTimeSlots
                    .CompanyID = CompanyID
                    .TimeSlot1Enabled = chkTimeSlot1.Checked
                    .TimeSlot1 = txtTimeSlot1.Text

                    .TimeSlot2Enabled = chkTimeSlot2.Checked
                    .TimeSlot2 = txtTimeSlot2.Text

                    .TimeSlot3Enabled = chkTimeSlot3.Checked
                    .TimeSlot3 = txtTimeSlot3.Text
                    .TimeSlot4Enabled = chkTimeSlot4.Checked
                    .TimeSlot4 = txtTimeSlot4.Text

                End With
                dsContacts.tblContactsTimeSlots.Rows.Add(nrowTimeSlots)

                Dim xmlFinance As String = dsFinance.GetXml
                xmlFinance = xmlFinance.Remove(xmlFinance.IndexOf("xmlns"), xmlFinance.IndexOf(".xsd") - xmlFinance.IndexOf("xmlns") + 5)

                Dim CategoryType As String
                CategoryType = "External"
                If Not drpCategoryType.SelectedValue = "" Then
                    CategoryType = drpCategoryType.SelectedValue
                End If

                Dim Source As String
                Source = "Orderwork"
                If Not ddlCompanyType.SelectedValue = "" Then
                    Source = ddlCompanyType.SelectedValue
                End If


                Dim xmlContent As String = dsContacts.GetXml
                xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)
                Dim Success As Integer = ws.WSContact.UpdateAccountSettingForCompany(xmlContent, _companyID, _bizDivID, CommonFunctions.FetchVerNum(), xmlFinance, Session("UserID"), CategoryType, Source, ddlWoCreatioFlow.SelectedValue, chkNewPortalAccess.Checked, chkOldPortalAccess.Checked)



                If Success = 1 Then
                    pnlSuccess.Visible = True
                    lblMsg.Visible = True
                    lblMsg.Text = ResourceMessageText.GetString("SettingsSaved")
                    Response.Redirect(getBackToListingLink())

                ElseIf Success = -10 Then
                    pnlSuccess.Visible = True
                    lblMsg.Visible = True
                    lblMsg.Text = "<span style='color:#FF0000'><b>" & ResourceMessageText.GetString("AccountsVersionControlMsg").Replace("<Link>", "AccountSettings.aspx?CompanyID=" & CompanyID & "&sender=company") & "</b></span>"
                Else
                    pnlSuccess.Visible = True
                    lblMsg.Visible = True
                    lblMsg.Text = "Settings not saved due to internal error"
                End If
            Else
                pnlSuccess.Visible = True
                lblMsg.Visible = True
                lblMsg.Text = ResourceMessageText.GetString("DecimalError")
            End If
        Else
            reqfld1.Enabled = chkField1.Checked
            reqFldLbl1.Enabled = chkField1.Checked

            reqfld2.Enabled = chkField2.Checked
            reqFldLbl2.Enabled = chkField2.Checked

            reqfld3.Enabled = chkField3.Checked
            reqFldLbl3.Enabled = chkField3.Checked

            reqfld4.Enabled = chkField4.Checked
            reqFldLbl4.Enabled = chkField4.Checked

            rfvTimeSlot1.Enabled = chkTimeSlot1.Checked
            rfvTimeSlot2.Enabled = chkTimeSlot2.Checked
            rfvTimeSlot3.Enabled = chkTimeSlot3.Checked
            rfvTimeSlot4.Enabled = chkTimeSlot3.Checked
        End If
    End Sub

    ''' <summary>
    ''' To fetch the details of the contact to populate company profile.
    ''' </summary>
    ''' <param name="IsMainContact"></param>
    ''' <param name="KillCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetContact(ByVal Page As Page, ByVal paramBizDivId As Integer, Optional ByVal IsMainContact As Boolean = False, Optional ByVal KillCache As Boolean = True, Optional ByVal contactid As Integer = 0) As DataSet
        Dim cacheKey As String = "Contact" & "-" & CStr(contactid) & "-" & Page.Session.SessionID
        If KillCache = True Then
            Page.Cache.Remove(cacheKey)
        End If
        Dim ds As New DataSet
        If Page.Cache(cacheKey) Is Nothing Then
            Select Case ApplicationSettings.SiteType
                ' Setting evironment for the company profile page when opened in admin site
                Case ApplicationSettings.siteTypes.admin
                    ds = CommonFunctions.GetContactsDSAdmin(Page, contactid, IsMainContact, cacheKey, KillCache)
                Case ApplicationSettings.siteTypes.site
                    ds = CommonFunctions.GetContactsDS(Page, Page.Session.SessionID, contactid, IsMainContact, cacheKey, KillCache)
            End Select

        Else
            ds = CType(Page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

    ''' <summary>
    ''' To handle reset profile action triggerd by the Save LinkButton at the top
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ResetTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetTop.Click, btnResetBottom.Click
        ResetProfile()
    End Sub

    ''' <summary>
    ''' Procedure to undo changes to company profile
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetProfile()
        pnlSuccess.Visible = False
        PopulateAccountSettings()
    End Sub

    ''' <summary>
    ''' To create the Back To Company Profile Link
    ''' </summary>
    ''' <returns>link</returns>
    ''' <remarks></remarks>
    Public Function getBackToListingLink() As String
        Dim link As String = ""
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        If ((Request("sender") = "buyer") Or (Request("classid") = ApplicationSettings.RoleClientID)) Then
            link = "~\CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        Else
            link = "~\CompanyProfile.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        End If
        Return link
    End Function
    Private Sub SetCacheForAttachment(ByVal ID As Integer, ByVal source As String, ByVal dsView As DataView)
        Dim Cachekey As String = ""
        Cachekey = "AttachedFiles-" & _companyID & "-" & source & "-" & Session.SessionID
        If Page.Cache(Cachekey) Is Nothing Then
            dsView.RowFilter = "LinkSource = '" & source & "'"
            Page.Cache.Add(Cachekey, dsView, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        End If

    End Sub
    Public Sub RemoveCallback(ByVal tag As String, ByVal obj As Object, ByVal reason As CacheItemRemovedReason)
        If (CacheItemRemovedReason.Underused = reason) Then
            Cache.Remove(tag)
            If Not IsNothing(obj) Then
                Cache.Insert(tag, obj, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            End If
        Else
            Cache.Remove(tag)
        End If
    End Sub

    Private Sub custVal_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles custVal.ServerValidate

        If ((txtField1.Text.Trim = txtField2.Text.Trim And (txtField1.Text.Trim <> "" And txtField2.Text.Trim <> "")) Or (txtField1.Text.Trim = txtField3.Text.Trim And (txtField1.Text.Trim <> "" And txtField3.Text.Trim <> "")) Or (txtField1.Text.Trim = txtField4.Text.Trim And (txtField1.Text.Trim <> "" And txtField4.Text.Trim <> "")) Or (txtField2.Text.Trim = txtField3.Text.Trim And (txtField2.Text.Trim <> "" And txtField3.Text.Trim <> "")) Or (txtField3.Text.Trim = txtField4.Text.Trim And (txtField3.Text.Trim <> "" And txtField4.Text.Trim <> ""))) Then
            args.IsValid = False
            custVal.ErrorMessage = "Flied labels can not be duplicated"
        Else
            args.IsValid = True
        End If
        If (args.IsValid) Then
            If chkDiscount.Checked Then
                If (txtDiscountPercent.Text.Trim = "") Then
                    custVal.ErrorMessage = "Please enter the discount percentage."
                    pnldisctxt.Style.Add("display", "inline")
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            End If
        End If

    End Sub

    Private Sub custValTimeSlots_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles custValTimeSlots.ServerValidate
        Dim isEvngTimeTime1 As Integer = 0

        If ((txtTimeSlot1.Text.Trim = txtTimeSlot2.Text.Trim And (txtTimeSlot1.Text.Trim <> "" And txtTimeSlot2.Text.Trim <> "")) _
        Or (txtTimeSlot1.Text.Trim = txtTimeSlot3.Text.Trim And (txtTimeSlot1.Text.Trim <> "" And txtTimeSlot3.Text.Trim <> "")) _
        Or (txtTimeSlot2.Text.Trim = txtTimeSlot3.Text.Trim And (txtTimeSlot2.Text.Trim <> "" And txtTimeSlot3.Text.Trim <> "")) _
        Or (txtTimeSlot4.Text.Trim = txtTimeSlot1.Text.Trim And (txtTimeSlot4.Text.Trim <> "" And txtTimeSlot1.Text.Trim <> "")) _
        Or (txtTimeSlot4.Text.Trim = txtTimeSlot2.Text.Trim And (txtTimeSlot4.Text.Trim <> "" And txtTimeSlot2.Text.Trim <> "")) _
        Or (txtTimeSlot4.Text.Trim = txtTimeSlot3.Text.Trim And (txtTimeSlot4.Text.Trim <> "" And txtTimeSlot3.Text.Trim <> ""))) Then
            args.IsValid = False
            custVal.ErrorMessage = "Time slots cannot be duplicated"

        Else
            args.IsValid = True
        End If
    End Sub

    Private Sub setValidations()
        If chkTimeSlot1.Checked = True Then
            rfvTimeSlot1.Enabled = True
            spnTimeSlot1.Style.Add("display", "inline")
        Else
            rfvTimeSlot1.Enabled = False
            spnTimeSlot1.Style.Add("display", "none")
        End If

        If chkTimeSlot2.Checked = True Then
            rfvTimeSlot2.Enabled = True
            spnTimeSlot2.Style.Add("display", "inline")
        Else
            rfvTimeSlot2.Enabled = False
            spnTimeSlot2.Style.Add("display", "none")
        End If
        If chkTimeSlot3.Checked = True Then
            rfvTimeSlot3.Enabled = True
            spnTimeSlot3.Style.Add("display", "inline")
        Else
            rfvTimeSlot3.Enabled = False
            spnTimeSlot3.Style.Add("display", "none")
        End If

        If chkTimeSlot4.Checked = True Then
            rfvTimeSlot4.Enabled = True
            spnTimeSlot4.Style.Add("display", "inline")
        Else
            rfvTimeSlot4.Enabled = False
            spnTimeSlot4.Style.Add("display", "none")
        End If




        If chkField1.Checked Then
            reqfld1.Enabled = True
            reqFldLbl1.Enabled = True
            spanfld1.Style.Add("display", "inline")
            spanfldVal1.Style.Add("display", "inline")
        Else
            reqfld1.Enabled = False
            reqFldLbl1.Enabled = False
            spanfld1.Style.Add("display", "none")
            spanfldVal1.Style.Add("display", "none")
        End If
        If chkField2.Checked Then
            reqfld2.Enabled = True
            reqFldLbl2.Enabled = True
            spanfld2.Style.Add("display", "inline")
            spanfldVal2.Style.Add("display", "inline")
        Else
            reqfld2.Enabled = False
            reqFldLbl2.Enabled = False
            spanfld2.Style.Add("display", "none")
            spanfldVal2.Style.Add("display", "none")
        End If
        If chkField3.Checked Then
            reqfld3.Enabled = True
            reqFldLbl3.Enabled = True
            spanfld3.Style.Add("display", "inline")
            spanfldVal3.Style.Add("display", "inline")
        Else
            reqfld3.Enabled = False
            reqFldLbl3.Enabled = False
            spanfld3.Style.Add("display", "none")
            spanfldVal3.Style.Add("display", "none")
        End If
        If chkField4.Checked Then
            reqfld4.Enabled = True
            reqFldLbl4.Enabled = True
            spanfld4.Style.Add("display", "inline")
            spanfldVal4.Style.Add("display", "inline")
        Else
            reqfld4.Enabled = False
            reqFldLbl4.Enabled = False
            spanfld4.Style.Add("display", "none")
            spanfldVal4.Style.Add("display", "none")
        End If
    End Sub

    Private Sub custValEvngTime_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles custValEvngTime.ServerValidate
        If txtTimeSlot1.Text.ToLower.StartsWith("6pm") = True Then
            args.IsValid = False
        ElseIf txtTimeSlot2.Text.ToLower.StartsWith("6pm") = True Then
            args.IsValid = False
        ElseIf txtTimeSlot3.Text.ToLower.StartsWith("6pm") = True Then
            args.IsValid = False
        ElseIf txtTimeSlot4.Text.ToLower.StartsWith("6pm") = True Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    Private Sub grdDefaultSettings_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdDefaultSettings.RowCancelingEdit

    End Sub

    Private Sub grdDefaultSettings_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdDefaultSettings.RowCommand
        GridViewRowCommand(sender, e)
    End Sub

    Private Sub PopulateBookingForms(ByVal DropDownList As DropDownList, ByVal CurrentBookingForm As String)

        Dim strString As String = ""
        Dim objListItem As ListItem
        ' Dim strPath As String = ConfigurationSettings.AppSettings("BookingFormsPath") & "SecurePages\BookingForms"
        ' Dim strPath As String = Server.MapPath("~/SecurePages/BookingForms")
        Dim strPath As String = Server.MapPath(ConfigurationSettings.AppSettings("BookingFormsPath"))

        DropDownList.ClearSelection()
        If Directory.Exists(strPath) Then
            Dim files() As FileInfo = New DirectoryInfo(strPath).GetFiles
            For Each File As FileInfo In files
                objListItem = New ListItem
                If File.Extension = ".aspx" Then
                    objListItem.Text = File.Name.ToString.Substring(0, File.Name.IndexOf(".aspx"))
                    objListItem.Value = File.Name
                    If CurrentBookingForm.ToLower = File.Name.ToString.ToLower Then
                        objListItem.Selected = True
                    End If
                    DropDownList.Items.Add(objListItem)
                End If
            Next
        End If
    End Sub

    Private Sub GridViewRowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim objLinkButton As LinkButton = CType(e.CommandSource, LinkButton)
        Dim objGridViewRow As GridViewRow = CType(objLinkButton.Parent.Parent, GridViewRow)
        Dim drpDropDown As DropDownList = CType(objGridViewRow.FindControl("drpDefaultBookingForm"), DropDownList)
        Dim objLabel As Label = CType(objGridViewRow.FindControl("lblDefaultBookingForm"), Label)
        Dim objCancelButton As LinkButton = CType(objGridViewRow.FindControl("lnkbtnCancel"), LinkButton)

        Select Case e.CommandName

            Case "EditForm"
                PopulateBookingForms(drpDropDown, objLabel.Text)
                objLinkButton.Text = "Save"
                objLinkButton.CommandName = "SaveForm"
                objCancelButton.Visible = True
                drpDropDown.Visible = True
                objLabel.Visible = False

            Case "SaveForm"

                objCancelButton.Visible = False
                drpDropDown.Visible = False
                objLabel.Visible = True
                ' Added by Hemisha to Update DefaultBookingform for the user of the company
                Dim dsUserType As DataSet
                dsUserType = CommonFunctions.UpdateDefaultBookingForm(CompanyID, objLinkButton.CommandArgument, drpDropDown.SelectedItem.Value)
                objLinkButton.Text = "Edit"
                objLinkButton.CommandName = "EditForm"
                BindTheDefaultBookingGrid()

            Case "Cancel"
                BindTheDefaultBookingGrid()

        End Select

    End Sub

    Private Sub BindTheDefaultBookingGrid()
        Dim dsUserType As DataSet
        dsUserType = CommonFunctions.GetUserTypes(CompanyID)
        grdDefaultSettings.DataSource = dsUserType
        grdDefaultSettings.DataBind()
    End Sub

    Private Sub grdDefaultSettings_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdDefaultSettings.RowEditing

    End Sub

    Private Sub grdDefaultSettings_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdDefaultSettings.RowUpdating

    End Sub
End Class