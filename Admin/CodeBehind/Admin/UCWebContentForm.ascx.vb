


Partial Public Class UCWebContentForm
    Inherits System.Web.UI.UserControl

    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs

    Private _contentID As Integer
    Public Property ContentID() As Integer
        Get
            Return _contentID
        End Get
        Set(ByVal value As Integer)
            _contentID = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            divValidationMain.Visible = False
            If CStr(Request("ContentID")) <> "" Then
                ViewState("ContentID") = Request("ContentID")
                btnCancelTop.HRef = getBackToListingLink()
                btnCancelBottom.HRef = getBackToListingLink()
                lnkBackToListing.HRef = getBackToListingLink()
            Else
                ViewState("ContentID") = 0
                btnCancelTop.HRef = "~\SiteContent.aspx"
                btnCancelBottom.HRef = "~\SiteContent.aspx"
                lnkBackToListing.HRef = "~\SiteContent.aspx"
            End If

            '''Following Code is added by Ambar
            ''' It's a requirement by client to remove Drop Down of Site from OWUKAdmin
            ''' CHeck for BizdivID with OWUKBizdivid and set Visible Property to false for the drop down

            If Session("BizDIvID") = ApplicationSettings.OWUKBizDivId Then
                ddlSiteType.Visible = False
                lblSite.Visible = False
                ChkShowOnSite.Visible = False
            Else
                CommonFunctions.PopulateBusiness(Page, ddlSiteType)

            End If


            CommonFunctions.PopulateWebContentType(Page, ddlContentType, Session("BizdivID"))

            CommonFunctions.PopulateAccount(Page, ddlAccountType)
            CommonFunctions.PopulateJobType(Page, ddlJobType)
            UCFileUpload6.BizDivID = 0
            UCFileUpload6.ClearUCFileUploadCache()

            PopulateSiteContent(False)
        End If

        '''Following Code added by Ambar
        ''' to set File Upload proerty Type on drop down change
        Select Case ddlContentType.SelectedValue
            Case ApplicationSettings.WebContents.PressReleases
                UCFileUpload6.Type = "Press Release"

            Case ApplicationSettings.WebContents.Articles
                UCFileUpload6.Type = "Articles"

            Case ApplicationSettings.WebContents.Quotation
                UCFileUpload6.Type = "Quotation"
        End Select
        divValidationMain.Visible = False
    End Sub

    Protected Sub PopulateSiteContent(Optional ByVal killcache As Boolean = False)
        Dim ds As DataSet
        ds = GetContent(False)
        Dim dvView As DataView = ds.Tables(0).Copy.DefaultView
        If ds.Tables(0).Rows.Count > 0 Then
            If Not IsDBNull(dvView.Item(0).Item("TypeID")) Then
                If Not IsNothing(dvView.Item(0).Item("TypeID")) Then
                    ddlContentType.SelectedValue = dvView.Item(0).Item("TypeID")
                    Select Case ddlContentType.SelectedValue
                        Case ApplicationSettings.WebContents.PressReleases
                            pnlTitle.Visible = True
                            pnlCompanyDetails.Visible = True
                            pnlCompanyData.Visible = False
                            pnlCmpHeader.Visible = False
                            pnlCmpText.Visible = False
                            pnlLocation.Visible = False
                            tdUrl.Visible = False
                            tdUrlField.Visible = False
                            'ChkShowOnHome.Visible = False
                            divChkBoxes.Visible = False
                            tblProfile.Visible = True
                            pnlFreeTxtBox.Visible = False
                            pnlTitleBodytext.Visible = False
                            tdtxtContentDate.Visible = True
                            tdDisplayDate.Visible = True

                        Case ApplicationSettings.WebContents.Articles
                            pnlTitle.Visible = True
                            pnlCompanyDetails.Visible = True
                            pnlCompanyData.Visible = False
                            lblAttachmentTitle.Text = "Publication Logo"
                            pnlCmpHeader.Visible = False
                            pnlCmpText.Visible = False
                            pnlLocation.Visible = False
                            tdUrl.Visible = True
                            tdUrlField.Visible = True
                            'ChkShowOnHome.Visible = False
                            divChkBoxes.Visible = False
                            tblProfile.Visible = True
                            pnlFreeTxtBox.Visible = True
                            pnlTitleBodytext.Visible = True
                            tdtxtContentDate.Visible = True
                            tdDisplayDate.Visible = True
                            ddlBodyTxtTypeIndicator.visible = False
                            rfdBodyText.Enabled = False
                            'Case ApplicationSettings.WebContents.Careers
                            '    pnlTitle.Visible = False
                            '    pnlCompanyDetails.Visible = True
                            '    lblAttachmentTitle.Text = "Company Logo"
                            '    pnlCompanyData.Visible = True
                            '    pnlCmpHeader.Visible = False
                            '    pnlCmpText.Visible = False
                            '    pnlLocation.Visible = True
                            '    tdUrl.Visible = False
                            '    tdUrlField.Visible = False
                            '    ChkShowOnHome.Visible = False
                            '    tdAccountType.Visible = False

                        Case ApplicationSettings.WebContents.Quotation
                            divChkBoxes.Visible = True
                            pnlTitle.Visible = False
                            pnlCompanyDetails.Visible = True
                            lblAttachmentTitle.Text = "Company Logo"
                            pnlCompanyData.Visible = True
                            pnlCmpHeader.Visible = True
                            pnlCmpText.Visible = True
                            pnlLocation.Visible = False
                            'ChkShowOnHome.Visible = True
                            tdUrl.Visible = False
                            tdUrlField.Visible = False
                            tdAccountType.Visible = True
                            tblProfile.Visible = True
                            pnlFreeTxtBox.Visible = True
                            pnlTitleBodytext.Visible = True
                            tdtxtContentDate.Visible = False
                            tdDisplayDate.Visible = False
                            ddlBodyTxtTypeIndicator.visible = True
                            rfdBodyText.Enabled = True
                    End Select

                End If
            End If
            '''Following Code is commented as it will populate for Publish Drop Down


            If dvView.Item(0).Item("ShowOnSite") = True Then
                If dvView.Item(0).Item("ShowOnStage") = True And dvView.Item(0).Item("ShowOnLive") = True Then
                    ddlPublish.SelectedValue = ApplicationSettings.Publish.Live
                ElseIf dvView.Item(0).Item("ShowOnStage") = True And dvView.Item(0).Item("ShowOnLive") = False Then
                    ddlPublish.SelectedValue = ApplicationSettings.Publish.Stage
                Else
                    ddlPublish.SelectedValue = ApplicationSettings.Publish.None
                End If
            Else
                ddlPublish.SelectedValue = ApplicationSettings.Publish.None
            End If

            ''' Code Added by Ambar to to check on thise boxes for check boxes which are been saved in Db
            If ds.Tables(2).Rows.Count > 0 Then
                Dim dvViewContentLink As DataView = ds.Tables(2).Copy.DefaultView
                DatalistRetailQuotes.DataSource = dvViewContentLink
                DatalistRetailQuotes.DataBind()
            End If
            'Nitin adding the following code as the ShowOnHome checkbox functionality was not working. 16-08-2007
            'If Not IsDBNull(dvView.Item(0).Item("ShowOnHome")) Then
            '    If Not IsNothing(dvView.Item(0).Item("ShowOnHome")) Then
            '        ChkShowOnHome.Checked = dvView.Item(0).Item("ShowOnHome")
            '    End If
            'End If

            If Not IsDBNull(dvView.Item(0).Item("Title")) Then
                If Not IsNothing(dvView.Item(0).Item("Title")) Then
                    If (ddlContentType.SelectedValue = ApplicationSettings.WebContents.PressReleases) Or (ddlContentType.SelectedValue = ApplicationSettings.WebContents.Articles) Then
                        txtTitle.Text = dvView.Item(0).Item("Title")
                    Else
                        txtJobTitle.Text = dvView.Item(0).Item("Title")
                    End If
                End If
            End If

            If Not IsDBNull(dvView.Item(0).Item("CompanyName")) Then
                If Not IsNothing(dvView.Item(0).Item("CompanyName")) Then
                    txtCompanyName.Text = dvView.Item(0).Item("CompanyName")
                End If
            End If


            If Not IsDBNull(dvView.Item(0).Item("Name")) Then
                If Not IsNothing(dvView.Item(0).Item("Name")) Then
                    txtName.Text = dvView.Item(0).Item("Name")
                End If
            End If
            If Not IsDBNull(dvView.Item(0).Item("ContentDate")) Then
                If Not IsNothing(dvView.Item(0).Item("ContentDate")) Then
                    txtContentDate.Text = Strings.FormatDateTime(dvView.Item(0).Item("ContentDate"), DateFormat.ShortDate)
                End If
            End If


            If Not IsDBNull(dvView.Item(0).Item("SiteUrl")) Then
                If Not IsNothing(dvView.Item(0).Item("SiteUrl")) Then
                    txtURL.Text = dvView.Item(0).Item("SiteUrl")
                End If
            End If


            If Not IsDBNull(dvView.Item(0).Item("BodyContent")) Then
                If Not IsNothing(dvView.Item(0).Item("BodyContent")) Then
                    FTB1.Text = dvView.Item(0).Item("BodyContent")
                End If
            End If
            If Not IsDBNull(dvView.Item(0).Item("LogoUrl")) Then
                If Not IsNothing(dvView.Item(0).Item("LogoUrl")) Then
                    Dim dsContacts As New OWContacts
                    Dim nrow1 As OWContacts.tblAttachmentInfoRow = dsContacts.tblAttachmentInfo.NewRow
                    Dim filepath As String = dvView.Item(0).Item("LogoUrl")
                    Dim index As Integer = filepath.LastIndexOf("\")
                    Dim str As String = filepath.Substring(index + 1)
                    With nrow1
                        .AttachmentID = 0
                        .LinkID = 0
                        .FilePath = dvView.Item(0).Item("LogoUrl")
                        .Name = str
                    End With
                    dsContacts.tblAttachmentInfo.Rows.Add(nrow1)
                    Dim dsView As DataView = dsContacts.tblAttachmentInfo.Copy.DefaultView
                    Dim cachekey As String = "AttachedFiles-" & 0 & "-" & "Logo" & "-" & Session.SessionID
                    Page.Cache.Add(cachekey, dsView, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
                End If
            End If

        Else
            If Session("BizDIvID") <> ApplicationSettings.OWUKBizDivId Then
                ChkShowOnSite.Checked = False
            End If


            'ChkShowOnHome.Checked = False
            txtTitle.Text = ""
            txtJobTitle.Text = ""
            txtCompanyName.Text = ""
            txtName.Text = ""
            txtLocation.Text = ""
            txtJobType.Text = ""
            txtJobType.Text = ""
            txtContentDate.Text = ""
            txtURL.Text = ""
            FTB1.Text = ""

            If Session("BizDIvID") <> ApplicationSettings.OWUKBizDivId Then
                ddlSiteType.SelectedValue = ""
            End If

            ddlContentType.SelectedValue = ""

        End If

        dvView = ds.Tables(1).Copy.DefaultView
    End Sub

    ' To fetch the details for a contentid
    Public Function GetContent(Optional ByVal KillCache As Boolean = False) As DataSet
        Dim cacheKey As String = "Content" & "-" & CStr(ViewState("ContentID")) & "-" & Session.SessionID
        If KillCache = True Then
            Page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If Cache(cacheKey) Is Nothing Then
            ws.WSContact.Timeout = 300000
            ds = ws.WSContact.GetWebContent(ViewState("ContentID"), "All", "admin")
            Page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(Page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function

    Protected Sub ValidatePage()
        Page.Validate()
        If (Page.IsValid = False) Then
            divValidationMain.Visible = True
        Else
            divValidationMain.Visible = False
            pnlSubmitForm.Visible = False
            pnlConfirm.Visible = True
        End If
    End Sub

    Protected Function SaveForm() As OWContacts
        Dim dsContacts As New OWContacts

        If (ddlContentType.SelectedValue = ApplicationSettings.WebContents.Quotation) Or (ddlContentType.SelectedValue = ApplicationSettings.WebContents.PressReleases) Or (ddlContentType.SelectedValue = ApplicationSettings.WebContents.Articles) Then
            dsContacts = UCFileUpload6.ReturnFilledAttachments(dsContacts)
        End If

        Dim nrow As OWContacts.tblWebContentRow = dsContacts.tblWebContent.NewRow
        With nrow
            .ContentID = ViewState("ContentID")
            If ddlContentType.SelectedValue = "" Then
                .TypeID = 0
            Else
                .TypeID = ddlContentType.SelectedValue
            End If

            .CreatedBy = Session("UserId")
            If (ddlContentType.SelectedValue = ApplicationSettings.WebContents.PressReleases) Or (ddlContentType.SelectedValue = ApplicationSettings.WebContents.Articles) Then
                .Title = txtTitle.Text.Trim
            Else
                .Title = txtJobTitle.Text.Trim
            End If
            .BodyContent = FTB1.Text.Trim

            If dsContacts.tblAttachmentInfo.Rows.Count > 0 Then
                .LogoUrl = dsContacts.tblAttachmentInfo.Item(0).Item("FilePath")
                .UKSiteURL = ApplicationSettings.WebContentUpload(ApplicationSettings.WebContentFolder.Logo) & "\" & dsContacts.tblAttachmentInfo.Item(0).Item("Name")
            End If
            .Name = txtName.Text.Trim

            If (ddlContentType.SelectedValue = ApplicationSettings.WebContents.Articles) Then
                If (txtURL.Text.Trim <> "") Then
                    If txtURL.Text.Trim.Contains("http://") Then
                        .SiteUrl = txtURL.Text.Trim
                    Else
                        .SiteUrl = "http://" & txtURL.Text.Trim
                    End If

                Else
                    .SiteUrl = "Articles" & "\" & dsContacts.tblAttachmentInfo.Item(0).Item("Name")
                End If
            ElseIf (ddlContentType.SelectedValue = ApplicationSettings.WebContents.PressReleases) Then
                .SiteUrl = dsContacts.tblAttachmentInfo.Item(0).Item("Name")
            End If

            ''Added By Ambar
            If Session("BizDIvID") = ApplicationSettings.OWUKBizDivId Then
                '' Code for Publish Drop Down
                If ddlPublish.selectedvalue = ApplicationSettings.Publish.None Then
                    .ShowOnSite = False
                    .ShowOnStage = False
                    .ShowOnLive = False
                ElseIf ddlPublish.selectedvalue = ApplicationSettings.Publish.Stage Then
                    .ShowOnSite = True
                    .ShowOnStage = True
                    .ShowOnLive = False
                Else
                    .ShowOnSite = True
                    .ShowOnStage = True
                    .ShowOnLive = True
                End If

            Else
                .ShowOnSite = ChkShowOnSite.Checked
            End If

            .ShowOnHome = 1
            .ContentDate = txtContentDate.Text.Trim
            .DateCreated = Strings.FormatDateTime(Date.Now, DateFormat.ShortDate)
            .DateModified = Strings.FormatDateTime(Date.Now, DateFormat.ShortDate)

            .CompanyName = txtCompanyName.Text.Trim
            .Location = txtLocation.Text.Trim
            .JobType = txtJobType.Text.Trim

            If ddlAccountType.Visible = True Then
                .AccountType = ddlAccountType.SelectedValue
            End If
        End With
        dsContacts.tblWebContent.Rows.Add(nrow)


        ''' Condition to check for BizdivId is of OWUKAdmin Added by Ambar

        If Session("BizDIvID") = ApplicationSettings.OWUKBizDivId Then
            Dim nrow1 As OWContacts.tblWebContentLinkageRow = dsContacts.tblWebContentLinkage.NewRow
            With nrow1
                .BizDivID = ApplicationSettings.OWUKBizDivId
                ViewState("BizDivID") = ApplicationSettings.OWUKBizDivId
                .ContentID = ViewState("ContentID")
            End With
            dsContacts.tblWebContentLinkage.Rows.Add(nrow1)


        Else
            If ddlSiteType.SelectedValue <> "" Then
                If ddlSiteType.SelectedValue = "All" Then
                    Dim item As ListItem
                    For Each item In ddlSiteType.Items
                        If Not (item.Value = "" Or item.Value = "All") Then
                            dsContacts.tblWebContentLinkage.Rows.Add(New String() {item.Value, ViewState("ContentID")})
                        End If
                    Next
                Else
                    Dim nrow1 As OWContacts.tblWebContentLinkageRow = dsContacts.tblWebContentLinkage.NewRow
                    With nrow1
                        .BizDivID = ddlSiteType.SelectedValue
                        ViewState("BizDivID") = ddlSiteType.SelectedValue
                        .ContentID = ViewState("ContentID")
                    End With
                    dsContacts.tblWebContentLinkage.Rows.Add(nrow1)
                End If
            End If
        End If





        Return dsContacts
    End Function

    Private Sub ddlUserType_select(ByVal senser As System.Object, ByVal e As System.EventArgs) Handles ddlContentType.SelectedIndexChanged

        Select Case ddlContentType.SelectedValue
            Case ApplicationSettings.WebContents.PressReleases
                pnlTitle.Visible = True
                pnlCompanyData.Visible = False
                pnlCompanyDetails.Visible = True
                pnlCmpHeader.Visible = False
                pnlCmpText.Visible = False
                pnlLocation.Visible = False
                tdUrl.Visible = False
                tdUrlField.Visible = False
                ''ChkShowOnHome.Visible = False
                divChkBoxes.Visible = False
                UCFileUpload6.Type = "Press Release"
                tblProfile.Visible = True
                pnlFreeTxtBox.Visible = False
                pnlTitleBodytext.Visible = False
                lblAttachmentTitle.Text = "Press Release"
                tdtxtContentDate.Visible = True
                tdDisplayDate.Visible = True
            Case ApplicationSettings.WebContents.Articles
                pnlTitle.Visible = True
                pnlCompanyDetails.Visible = True
                pnlCompanyData.Visible = False
                lblAttachmentTitle.Text = "Articles"
                pnlCmpHeader.Visible = False
                pnlCmpText.Visible = False
                pnlLocation.Visible = False
                tdUrl.Visible = True
                tdUrlField.Visible = True
                'ChkShowOnHome.Visible = False
                UCFileUpload6.Type = "Articles"
                divChkBoxes.Visible = False
                tblProfile.Visible = True
                pnlFreeTxtBox.Visible = True
                pnlTitleBodytext.Visible = True
                tdtxtContentDate.Visible = True
                tdDisplayDate.Visible = True
                rfdBodyText.Enabled = False
                ddlBodyTxtTypeIndicator.visible = False

            Case ApplicationSettings.WebContents.Quotation
                pnlTitle.Visible = False
                pnlCompanyDetails.Visible = True
                lblAttachmentTitle.Text = "Company Logo"
                pnlCompanyData.Visible = True
                pnlCmpHeader.Visible = True
                pnlCmpText.Visible = True
                pnlLocation.Visible = False
                'ChkShowOnHome.Visible = True
                tdUrl.Visible = False
                tdUrlField.Visible = False
                divChkBoxes.visible = True
                UCFileUpload6.Type = "Quotation"
                tblProfile.Visible = True
                pnlFreeTxtBox.Visible = True
                pnlTitleBodytext.Visible = True
                tdtxtContentDate.Visible = False
                tdDisplayDate.Visible = False
                rfdBodyText.Enabled = True
                ddlBodyTxtTypeIndicator.Visible = True

                If (ViewState("ContentID") = 0) Then
                    Dim ds As DataSet
                    ds = GetContent(False)
                    If ds.Tables(2).Rows.Count > 0 Then
                        Dim dvViewContentLink As DataView = ds.Tables(2).Copy.DefaultView
                        DatalistRetailQuotes.DataSource = dvViewContentLink
                        DatalistRetailQuotes.DataBind()
                    End If
                End If

        End Select

    End Sub

    Protected Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Dim strMode As String

        If ViewState("ContentID") = 0 Then
            strMode = "add"
        Else
            strMode = "edit"
        End If

        '''' Code Added by Ambar 
        '''' Implementation of Logic for Check boxes for Quotation
        'Dim strChkBox As String = ""
        'If ddlContentType.SelectedValue = ApplicationSettings.WebContents.Quotation Then
        '    Dim flag As Integer = 0

        '    If ChkInstallWorksHome.Checked = True Then
        '        strChkBox = ApplicationSettings.ContentPageLink.InstallWorksHome
        '        flag = 1
        '    End If
        '    If ChkInstallWorksServices.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.InstallWorksServices
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.InstallWorksServices
        '            flag = 1
        '        End If
        '    End If
        '    If ChkInstallWorksServices.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.InstallWorksServices
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.InstallWorksServices
        '            flag = 1
        '        End If
        '    End If


        '    If ChkRetailHome.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWRetailHome
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWRetailHome
        '            flag = 1
        '        End If
        '    End If
        '    If ChkRetailHomePCInstallation.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWRetailHomePCInstallation
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWRetailHomePCInstallation
        '            flag = 1
        '        End If
        '    End If
        '    If ChkRetailCEAndTVInstalls.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWRetailCEAndTVInstalls
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWRetailCEAndTVInstalls
        '            flag = 1
        '        End If
        '    End If
        '    If ChkRetailFreesatAndDigitalAerials.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWRetailFreesatAndDigitalAerials
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWRetailFreesatAndDigitalAerials
        '            flag = 1
        '        End If
        '    End If
        '    If ChkRetailWhyOrderwork.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWRetailWhyOrderwork
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWRetailWhyOrderwork
        '            flag = 1
        '        End If
        '    End If
        '    If ChkRetailMyOrderWorkRetail.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWRetailMyOrderWork
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWRetailMyOrderWork
        '            flag = 1
        '        End If
        '    End If
        '    If ChkRetailServicePartners.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWRetailServicePartners
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWRetailServicePartners
        '            flag = 1
        '        End If
        '    End If
        '    If ChkRetailRetailers.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWRetailRetailers
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWRetailRetailers
        '            flag = 1
        '        End If
        '    End If




        '    If ChkITHome.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWITHome
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWITHome
        '            flag = 1
        '        End If
        '    End If
        '    If ChkITDeliverables.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWITDeliverables
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWITDeliverables
        '            flag = 1
        '        End If
        '    End If
        '    If ChkITInstallationDeploymentServices.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWITInstallationDeploymentServices
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWITInstallationDeploymentServices
        '            flag = 1
        '        End If
        '    End If
        '    If ChkITIMACITSupportServices.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWITIMACITSupportServices
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWITIMACITSupportServices
        '            flag = 1
        '        End If
        '    End If
        '    If ChkITChannelIT.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWITChannelIT
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWITChannelIT
        '            flag = 1
        '        End If
        '    End If
        '    If ChkITMyOrderWorkOTS.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWITMyOrderWorkOTS
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWITMyOrderWorkOTS
        '            flag = 1
        '        End If
        '    End If
        '    If ChkITServicePartners.Checked = True Then
        '        If flag = 1 Then
        '            strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWITServicePartners
        '        Else
        '            strChkBox = ApplicationSettings.ContentPageLink.OWITServicePartners
        '        End If
        '    End If
        'End If
        ''If ChkShowOnHome.Checked = True Then
        ''    strChkBox = ApplicationSettings.ContentPageLink.OWHome
        ''    flag = 1
        ''End If
        ''If ChkShowOnRetailer.Checked = True Then
        ''    If flag = 1 Then
        ''        strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWRetailer

        ''    Else
        ''        strChkBox = ApplicationSettings.ContentPageLink.OWRetailer
        ''        flag = 1
        ''    End If
        ''End If
        ''If ChkShowOnITFirms.Checked = True Then
        ''    If flag = 1 Then
        ''        strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWITFirms

        ''    Else
        ''        strChkBox = ApplicationSettings.ContentPageLink.OWITFirms
        ''        flag = 1
        ''    End If
        ''End If
        ''If ChkShowOnITStaffing.Checked = True Then
        ''    If flag = 1 Then
        ''        strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWITStaffing

        ''    Else
        ''        strChkBox = ApplicationSettings.ContentPageLink.OWITStaffing
        ''        flag = 1
        ''    End If
        ''End If
        ''If ChkShowOnClients.Checked = True Then
        ''    If flag = 1 Then
        ''        strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWHClients

        ''    Else
        ''        strChkBox = ApplicationSettings.ContentPageLink.OWHClients
        ''        flag = 1
        ''    End If
        ''End If
        ''If ChkShowOnServicePartners.Checked = True Then
        ''    If flag = 1 Then
        ''        strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.OWServicePartner
        ''    Else
        ''        strChkBox = ApplicationSettings.ContentPageLink.OWServicePartner
        ''        flag = 1
        ''    End If
        ''End If
        ''If ChkInstallWorksHome.Checked = True Then
        ''    If flag = 1 Then
        ''        strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.InstallWorksHome
        ''    Else
        ''        strChkBox = ApplicationSettings.ContentPageLink.InstallWorksHome
        ''        flag = 1
        ''    End If
        ''End If
        ''If ChkInstallWorksServices.Checked = True Then
        ''    If flag = 1 Then
        ''        strChkBox = strChkBox & "," & ApplicationSettings.ContentPageLink.InstallWorksServices
        ''    Else
        ''        strChkBox = ApplicationSettings.ContentPageLink.InstallWorksServices
        ''    End If
        ''End If



        Dim selectedIds As String = ""
        For Each item As DataListItem In DatalistRetailQuotes.Items
            Dim chkBox As CheckBox = CType(item.FindControl("Quote"), CheckBox)
            Dim QuoteId As String = CType(item.FindControl("hdnQuoteId"), HtmlControls.HtmlInputHidden).Value
            If Not (chkBox Is Nothing) Then
                If chkBox.Checked Then
                    If selectedIds = "" Then
                        selectedIds = QuoteId  'dl.DataKeys(item.ItemIndex).ToString
                    Else
                        selectedIds = selectedIds & "," & QuoteId  'dl.DataKeys(item.ItemIndex).ToString
                    End If
                End If
            End If
        Next



        Dim xmlContent As String = SaveForm().GetXml
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)

        Dim success As Boolean = ws.WSContact.AddEditWebContent(xmlContent, ViewState("ContentID"), strMode, selectedIds)

        If success Then
            Cache.Remove("Content" & "-" & CStr(ViewState("ContentID")) & "-" & Session.SessionID)
            Cache.Remove("WebContentsForSite-BizDivId-" & ViewState("BizDivID"))
            Cache.Remove(ddlContentType.SelectedItem.Text & "List")
            Cache.Remove(ddlContentType.SelectedItem.Text & "-" & CStr(ViewState("ContentID")))
            If CStr(Request("ContentID")) <> "" Then
                Response.Redirect((getBackToListingLink().Replace("~\", "")))
            Else
                Response.Redirect("SiteContent.aspx")
            End If

        Else
            pnlSubmitForm.Visible = True
            pnlConfirm.Visible = False
            lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
            divValidationMain.Visible = True


        End If

        PopulateSiteContent(True)

    End Sub

    ''' <summary>
    ''' Method to handle the save functionality of the Top save  button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click


        ValidatePage()
    End Sub

    ''' <summary>
    ''' Method to handle the save functionality of the Bottom save Exit button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSaveBottom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveBottom.Click
        Select Case ddlContentType.SelectedValue
            Case ApplicationSettings.WebContents.PressReleases
                rfdAccountType.Enabled = False
                rfdCompanyName.Enabled = False
                rfdName.Enabled = False
                rfdJobTitle.Enabled = False
                rfdLocation.Enabled = False
                rfdJobType.Enabled = False
                rfdBodyText.Enabled = False

            Case ApplicationSettings.WebContents.Articles
                rfdAccountType.Enabled = False
                rfdCompanyName.Enabled = False
                rfdName.Enabled = False
                rfdJobTitle.Enabled = False
                rfdLocation.Enabled = False
                rfdJobType.Enabled = False


            Case ApplicationSettings.WebContents.Quotation
                rfdAccountType.Enabled = False

                rfdLocation.Enabled = False
                rfdJobType.Enabled = False

        End Select
        ValidatePage()
    End Sub
    '''' <summary>
    '''' Method to handle the cancel functionality of the Top Exit button on the UC
    '''' </summary>
    '''' <param name="sender"></param>
    '''' <param name="e"></param>
    '''' <remarks></remarks>
    'Private Sub btnCancelTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTop.Click
    '    If CStr(Request("ContentID")) <> "" Then
    '        Response.Redirect(getBackToListingLink())
    '    Else
    '        Response.Redirect("SiteContent.aspx")
    '    End If
    'End Sub

    '''' <summary>
    '''' Method to handle the cancel functionality of the Bottom Exit button on the UC
    '''' </summary>
    '''' <param name="sender"></param>
    '''' <param name="e"></param>
    '''' <remarks></remarks>
    'Private Sub btnCancelBottom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelBottom.Click
    '    If CStr(Request("ContentID")) <> "" Then
    '        Response.Redirect(getBackToListingLink())
    '    Else
    '        Response.Redirect("SiteContent.aspx")
    '    End If
    'End Sub

    ''' <summary>
    ''' Method to handle the reset functionality of the Bottom button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnResetBottom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetBottom.Click
        PopulateSiteContent(False)
    End Sub

    ''' <summary>
    ''' Method to handle the reset functionality of the Top button on the UC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnResettOP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetTop.Click
        PopulateSiteContent(False)
    End Sub

    ''' <summary>
    ''' To handle cancel action on confirm
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlSubmitForm.Visible = True
        pnlConfirm.Visible = False
    End Sub

    Private Function getBackToListingLink() As String
        Dim appendLink As String = ""


        appendLink = "~\SiteContent.aspx?"
        appendLink &= "contentID=" & ViewState("ContentID")
        appendLink &= "&type=" & Request("type")
        appendLink &= "&OnSite=" & Request("OnSite")
        appendLink &= "&FromDate=" & Request("FromDate")
        appendLink &= "&ToDate=" & Request("ToDate")
        appendLink &= "&PS=" & Request("PS")
        appendLink &= "&PN=" & Request("PN")
        appendLink &= "&SC=" & Request("SC")
        appendLink &= "&SO=" & Request("SO")
        appendLink &= "&sender=" & "SiteContentForm"



        Return appendLink

    End Function


   
End Class