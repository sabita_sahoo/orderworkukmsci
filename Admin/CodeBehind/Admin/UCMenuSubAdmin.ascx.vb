Partial Public Class UCMenuSubAdmin
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetNoStore()
        If Not IsPostBack Then
            Dim ds As DataSet = CType(Cache("LoggedInfo-" & Session("UserName") & Session.SessionID), DataSet)

            Dim filepath As String = CType(CType(sender, UCMenuSubAdmin).Request, System.Web.HttpRequest).Path
            filepath = filepath.Substring(filepath.LastIndexOf("/") + 1, filepath.Length - (filepath.LastIndexOf("/") + 1))

            Dim dv As DataView = ds.Tables(2).Copy.DefaultView

 
            Dim str As New StringBuilder
            str.Append("<script language='JavaScript' type='text/JavaScript'>" & Chr(13))

            str.Append("var scrollTim=1;" & Chr(13))
            str.Append("var current;" & Chr(13))
            str.Append("menuArr = new Array();" & Chr(13))
            Dim dvMain As DataView = ds.Tables(1).Copy.DefaultView
            dvMain.RowFilter = "ShowOnMenu = 1"
            Dim i, j, k As Integer

            For i = 0 To dvMain.Count - 1
                str.Append("menuArr[" & i + 1 & "] = new Array();" & Chr(13))
                str.Append("menuArr[" & i + 1 & "][0] = new Array();" & Chr(13))
                str.Append("menuArr[" & i + 1 & "][0][1] = '" & dvMain.Item(i).Item("MenuID") & "';" & Chr(13))
                str.Append("menuArr[" & i + 1 & "][0][2] = '85';" & Chr(13))
                str.Append("menuArr[" & i + 1 & "][0][3] = '102';" & Chr(13))
                dv.RowFilter = "ShowOnMenu = 1 and ParentMenuID = " & dvMain.Item(i).Item("MenuID")
                For j = 0 To dv.Count - 1
                    str.Append("menuArr[" & i + 1 & "][" & j + 1 & "] = new Array();" & Chr(13))
                    str.Append("menuArr[" & i + 1 & "][" & j + 1 & "][1] = '" & dv.Item(j).Item("MenuLabel") & "';" & Chr(13))
                    str.Append("menuArr[" & i + 1 & "][" & j + 1 & "][2] = '" & dv.Item(j).Item("MenuLink") & "';" & Chr(13))
                    str.Append("menuArr[" & i + 1 & "][" & j + 1 & "][3] = '" & dv.Item(j).Item("ChildMenuID") & "';" & Chr(13))
                Next
            Next


            Dim dvSubMenu As DataView = ds.Tables(4).Copy.DefaultView
            dvSubMenu.RowFilter = "ShowOnMenu = 1"
            i = 0
            j = 0
            k = 0
            str.Append("menuArr2 = new Array();" & Chr(13))
            For i = 0 To dvMain.Count - 1
                str.Append("menuArr2[" & i + 1 & "] = new Array();" & Chr(13))
                str.Append("menuArr2[" & i + 1 & "][0] = new Array();" & Chr(13))
                str.Append("menuArr2[" & i + 1 & "][0][1] = '" & dvMain.Item(i).Item("MenuID") & "';" & Chr(13))
                str.Append("menuArr2[" & i + 1 & "][0][2] = '85';" & Chr(13))
                str.Append("menuArr2[" & i + 1 & "][0][3] = '102';" & Chr(13))
                dv.RowFilter = "ShowOnMenu = 1 and ParentMenuID = " & dvMain.Item(i).Item("MenuID")
                For j = 0 To dvSubMenu.Count - 1
                    For k = 0 To dv.Count - 1
                        If dvSubMenu.Item(j).Item("ParentMenuId") = dv.Item(k).Item("ChildMenuID") Then
                            str.Append("menuArr2[" & i + 1 & "][" & j + 1 & "] = new Array();" & Chr(13))
                            str.Append("menuArr2[" & i + 1 & "][" & j + 1 & "][1] = '" & dvSubMenu.Item(j).Item("MenuLabel") & "';" & Chr(13))
                            str.Append("menuArr2[" & i + 1 & "][" & j + 1 & "][2] = '" & dvSubMenu.Item(j).Item("MenuLink") & "';" & Chr(13))
                            str.Append("menuArr2[" & i + 1 & "][" & j + 1 & "][3] = '" & dvSubMenu.Item(j).Item("ParentMenuId") & "';" & Chr(13))
                        End If
                    Next
                Next
            Next

            str.Append("</script>" & Chr(13))
            ltMenuSub.Text = str.ToString
        End If
    End Sub

End Class