'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.42
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On


Partial Public Class UCAccountSettings
    Protected WithEvents lnkBackToListing As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents chkAutoMatch As System.Web.UI.WebControls.CheckBox
    Protected WithEvents pnlSettings As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents chkRefCheck As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkSecurity As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkCRB As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ddlNearestTo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents drpDefaultPage As DropDownList
    Protected WithEvents drpUserType As DropDownList
    Protected WithEvents grdDefaultSettings As GridView
    Protected WithEvents drpDefaultBookingForm As DropDownList
    Protected WithEvents btnSaveTop As System.Web.UI.WebControls.LinkButton
    Protected WithEvents btnSaveBottom As System.Web.UI.WebControls.LinkButton
    Protected WithEvents btnResetTop As System.Web.UI.WebControls.LinkButton
    Protected WithEvents btnResetBottom As System.Web.UI.WebControls.LinkButton
    Protected WithEvents pnlSuccess As System.Web.UI.WebControls.Panel
    Protected WithEvents lblMsg As System.Web.UI.WebControls.Label
    Protected WithEvents chkProfIdem As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkNewPortalAccess As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkOldPortalAccess As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkBrigantia As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkEveningInst As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkDisableHolidays As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkDisableSundays As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkEnableClientJobList As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkTrackSP As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkEnableGoodsLoc As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkDisableSaturdays As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents txtRPUplift As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents txtPPUplift As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents txtWPUplift As System.Web.UI.HtmlControls.HtmlInputText

    Protected WithEvents chkDiscount As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkField1 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkWoEmailNotice As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkSLA As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkField2 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkField3 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkField4 As System.Web.UI.HtmlControls.HtmlInputCheckBox

    Protected WithEvents chkNoChargeClient As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkNoChargeSP As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkNoChargeFavSP As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkAllowChangePassword As System.Web.UI.HtmlControls.HtmlInputCheckBox

    Protected WithEvents txtDiscountPercent As System.Web.UI.WebControls.TextBox

    Protected WithEvents txtField1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtField2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtField3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtField4 As System.Web.UI.WebControls.TextBox

    Protected WithEvents txtFieldValue1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFieldValue2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFieldValue3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFieldValue4 As System.Web.UI.WebControls.TextBox

    Protected WithEvents FileUpload1 As System.Web.UI.WebControls.FileUpload
    Protected WithEvents UCFileUpload1 As UCFileUpload_Outer
    Protected WithEvents btnUpload As System.Web.UI.WebControls.Button
    Protected WithEvents lblErr As System.Web.UI.WebControls.Label
    Protected WithEvents valsum As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents reqFldLbl1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents reqFldLbl2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents reqFldLbl3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents reqFldLbl4 As System.Web.UI.WebControls.RequiredFieldValidator

    Protected WithEvents reqfld1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents reqfld2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents reqfld3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents reqfld4 As System.Web.UI.WebControls.RequiredFieldValidator
    ' Protected WithEvents divValidationMain As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents custVal As System.Web.UI.WebControls.CustomValidator
    Protected WithEvents pnldisctxt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spanfld1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spanfld2 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spanfld3 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spanfld4 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spanfldVal1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spanfldVal2 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spanfldVal3 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spanfldVal4 As System.Web.UI.HtmlControls.HtmlGenericControl

    Protected WithEvents txtSPFee As System.Web.UI.WebControls.TextBox
    Protected WithEvents pnlPrefSP As System.Web.UI.WebControls.Panel
    Protected WithEvents chkPrefSP As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents custValTimeSlots As System.Web.UI.WebControls.CustomValidator
    Protected WithEvents custValEvngTime As System.Web.UI.WebControls.CustomValidator

    Protected WithEvents spnTimeSlot1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spnTimeSlot2 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spnTimeSlot3 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spnTimeSlot4 As System.Web.UI.HtmlControls.HtmlGenericControl

    Protected WithEvents rfvTimeSlot1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents rfvTimeSlot2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents rfvTimeSlot3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents rfvTimeSlot4 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents chkTimeSlot1 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkTimeSlot2 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkTimeSlot3 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents chkTimeSlot4 As System.Web.UI.HtmlControls.HtmlInputCheckBox

    Protected WithEvents txtTimeSlot1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTimeSlot2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTimeSlot3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtTimeSlot4 As System.Web.UI.WebControls.TextBox

    Protected WithEvents txtEvngSlotLabel As System.Web.UI.HtmlControls.HtmlInputText

    Protected WithEvents rdoRPUPPerc As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdoRPUPFixed As System.Web.UI.WebControls.RadioButton

    Protected WithEvents rdoWPUPPerc As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdoWPUPFixed As System.Web.UI.WebControls.RadioButton

    Protected WithEvents rdoPPUPPerc As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdoPPUPFixed As System.Web.UI.WebControls.RadioButton
    Protected WithEvents hdnEvengInst As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents drpdwnGoodsCollection As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtcomptag As System.Web.UI.WebControls.TextBox
    Protected WithEvents chkLockSkillSets As System.Web.UI.WebControls.CheckBox
    Protected WithEvents txtLockSkillSetComments As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFeildSkillSet As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents spanfldLockSkillSetComments As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents drpCategoryType As DropDownList

    Protected WithEvents pnlSupplierFee As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlAdminFee As System.Web.UI.WebControls.Panel
    Protected WithEvents txtAdminFee As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlCompanyType As DropDownList
    Protected WithEvents hdnRoleId As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddlWoCreatioFlow As DropDownList



End Class
