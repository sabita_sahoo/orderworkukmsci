Imports WebLibrary
Partial Public Class UCSearchContact
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs

    Private _bizDivID As Integer
    Public Property BizDivID() As Integer
        Get
            Return _bizDivID
        End Get
        Set(ByVal value As Integer)
            _bizDivID = value
        End Set
    End Property

    Private _classID As Integer
    Public Property ClassId() As Integer
        Get
            Return _classID
        End Get
        Set(ByVal value As Integer)
            _classID = value
        End Set
    End Property

    Private _filter As String
    Public Property Filter() As String
        Get
            Return _filter
        End Get
        Set(ByVal value As String)
            _filter = value
        End Set
    End Property

    Private _HideTestAccounts As Boolean = "0"
    Public Property HideTestAccounts() As Boolean
        Get
            Return _HideTestAccounts
        End Get
        Set(ByVal value As Boolean)
            _HideTestAccounts = value
        End Set
    End Property

    Private _selectedContact As Integer
    Public Property SelectedContact() As Integer
        Get
            Return _selectedContact
        End Get
        Set(ByVal value As Integer)
            _selectedContact = value
        End Set
    End Property


    Private _IsAMSelected As Boolean
    Public Property IsAMSelected() As Boolean
        Get
            Return _IsAMSelected
        End Get
        Set(ByVal value As Boolean)
            _IsAMSelected = value
        End Set
    End Property

    Private _BusinessArea As Integer
    Public Property BusinessArea() As Integer
        Get
            Return _BusinessArea
        End Get
        Set(ByVal value As Integer)
            _BusinessArea = value
        End Set
    End Property

    Private _ContactType As String
    Public Property ContactType() As String
        Get
            Return _ContactType
        End Get
        Set(ByVal value As String)
            _ContactType = value
        End Set
    End Property

    Private _ContactName As String
    Public Property ContactName() As String
        Get
            Return _ContactName
        End Get
        Set(ByVal value As String)
            _ContactName = value
        End Set
    End Property

    Private _ContactID As Integer
    Public Property ContactID() As Integer
        Get
            Return _ContactID
        End Get
        Set(ByVal value As Integer)
            _ContactID = value
        End Set
    End Property

    Private _DefaultView As Boolean = False

    Public Property DefaultView() As Boolean
        Get
            Return _DefaultView
        End Get
        Set(ByVal value As Boolean)
            _DefaultView = value
        End Set
    End Property

    Private _CompanyAutoSuggest As String = String.Empty
    Public Property CompanyAutoSuggest() As String
        Get
            Return _CompanyAutoSuggest
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                _CompanyAutoSuggest = "False"
            Else
                _CompanyAutoSuggest = value
            End If
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If CompanyAutoSuggest = "True" Then
            divAutoSuggest.Visible = True
            divSearchContact.Visible = False
        Else
            divAutoSuggest.Visible = False
            divSearchContact.Visible = True
        End If

        If Not Page.IsPostBack Then
            If Me.Page.Title <> "AdminWOListing" And Me.Page.Title <> "WOForm" And Me.Page.Title <> "SalesInvoicePaid" And Me.Page.Title <> "OrderWork : Receivables" And Me.Page.Title <> "PurchaseInvoices" And Me.Page.Title <> "SalesInvoiceGeneration" And Me.Page.Title <> "UnPaid-Available" Then
                populateContact()
            End If

        End If
    End Sub

    'Private Sub ddlContact_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContact.SelectedIndexChanged
    '    SelectedContact = ddlContact.SelectedItem.Value
    'End Sub

    Public Sub populateContact(Optional ByVal killCache As Boolean = True)
        If SelectedContact = 0 Then
            Dim ds As DataSet
            ds = getContacts(killCache)
            ds.Tables(0).DefaultView.Sort = "Contact"

            'If ContactType = "BusinessAreaDefined" Then
            '    ds.Tables(0).DefaultView.RowFilter = "BusinessArea = 101"
            'End If

            ddlContact.Items.Clear()
            'If Me.Page.Title = "OrderWork : UnPaid Available" Then
            '    ddlContact.SelectedIndex = Nothing
            '    ddlContact.SelectedValue = Nothing
            'End If
            ddlContact.DataSource = ds.Tables(0).DefaultView
            ddlContact.DataBind()

            If Me.Page.Title = "AdminWOListing" Or Me.Page.Title = "Credit Notes" Or Me.Page.Title = "Debit Notes" Or Me.Page.Title = "OrderWork : List of Engineers" Or Me.Page.Title = "OrderWork : Rating" Then
                ddlContact.Items.Insert(0, New ListItem("Select Contact", ""))
                ddlContact.Items.Insert(1, New ListItem("All Contacts", "ALL"))
            Else
                ddlContact.Items.Insert(0, New ListItem("Select Contact", ""))
            End If

            'If Not IsNothing(Request("CompID")) Then
            '    If Request("CompID") = "0" Then
            '        ddlContact.SelectedIndex = 0
            '    End If
            'End If            

            ddlContact.SelectedIndex = 0
            'ddlContact.SelectedItem.Value = 0
        Else
            ddlContact.SelectedIndex = ddlContact.Items.IndexOf(ddlContact.Items.FindByValue(SelectedContact))

        End If
    End Sub

    Private Function getContacts(Optional ByVal killCache As Boolean = True) As DataSet
        'Dim cacheKey As String = ""
        'cacheKey = "SearchContacts" & "-" & Page.Session.SessionID
        'If killCache = True Then
        '    Page.Cache.Remove(cacheKey)
        'End If
        Dim ds As DataSet
        'If Page.Cache(cacheKey) Is Nothing Then
        ds = ws.WSContact.MS_GetContactsByName(BizDivID, ClassId, Filter, HideTestAccounts, DefaultView, Session("UserID"))
        '    Page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        'Else
        '    ds = CType(Page.Cache(cacheKey), DataSet)
        'End If
        Return ds
    End Function

    Public Sub getContactsDetails(ByVal companyid As Integer, ByRef ContactId As Integer, ByRef ContactName As String, Optional ByRef isAM As Boolean = False, Optional ByRef Contact As String = "")
        Dim ds As DataSet
        ds = getContacts()
        Dim dv As DataView = New DataView(ds.Tables(0))
        dv.RowFilter = "CompanyID = " & companyid

        If dv.Count > 0 Then
            ContactId = dv.Item(0).Item("ContactId")
            ContactName = dv.Item(0).Item("ContactName")
            Contact = dv.Item(0).Item("Contact")
            'If Not IsNothing(isAM) Then
            isAM = dv.Item(0).Item("isAM")
            IsAMSelected = CBool(dv.Item(0).Item("isAM"))
            BusinessArea = CInt(dv.Item(0).Item("BusinessArea"))
            'End If
        End If

    End Sub

End Class