Partial Public Class UCAdminWODetails
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs
    Public WithEvents pnlActionConfirm As Panel
    Public WithEvents lblCustSpecialInstructions As Label
    Public WithEvents pnlThermostatsQuestions As Panel
    Public WithEvents lblThermostatsQuestionsInfo As Label
    Public WithEvents lblProducts As Label
    Public WithEvents lblAddnProducts As Label
    Public WithEvents lblProductsLabel As Label
    Public WithEvents lblStartDateLabel As Label
    Public WithEvents trEndDate As HtmlTableRow
    Public WithEvents lblSalesAgent As Label
    Public WithEvents lblReceiptNumber As Label
    Public WithEvents lblReceiptNumberHeader As Label
    Public WithEvents trJobNumber As HtmlTableRow
    Public WithEvents trFreesatMake As HtmlTableRow
    Public WithEvents trAddnProducts As HtmlTableRow
    Public WithEvents trSalesAgent As HtmlTableRow
    Public WithEvents lblDepotNameLabel As Label
    Public WithEvents imgSetWholesalePrice As System.Web.UI.WebControls.Image
    Public WithEvents ddlNoteType As DropDownList
    Public WithEvents lnkbtnWatch As LinkButton
    Public WithEvents lnkbtnUnwatch As LinkButton

    Private _companyID As Integer


    Dim SupCompID As Integer

    Public Property CompanyID() As Integer
        Get
            If Not IsNothing(Request("CompanyId")) Then
                Return Request("CompanyId")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property

    Private _contactID As Integer
    Public Property ContactID() As Integer
        Get
            Return _contactID
        End Get
        Set(ByVal value As Integer)
            _contactID = value
        End Set
    End Property

    Private _bizdivID As Integer
    Public Property BizdivID() As Integer
        Get
            If Not IsNothing(ViewState("bizdivID")) Then
                Return ViewState("bizdivID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("bizdivID") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetNoStore()
        If Not IsPostBack Then

           

            ViewState("WOID") = Request("WOID")
            ViewState("CompanyID") = CompanyID
            ViewState("Group") = Request("Group")
            If ViewState("Group") = "submitted" Then
                ViewState("Group") = StrConv(ViewState("Group"), VbStrConv.ProperCase)
            ElseIf ViewState("Group") = "Conditional Accept" Then
                ViewState("Group") = "CA"
            End If



            ViewState("FromDate") = Request("FromDate")
            ViewState("ToDate") = Request("ToDate")
            If Not IsNothing(Request("WorkOrderID")) Then
                ViewState("WorkOrderID") = Request("WorkOrderID")
            End If
            If Not IsNothing(Request("WOID")) Then
                lnkbtnQuickNote.CommandArgument = Request("WOID")
            End If
            'Changed by Pankaj Malav on 07 Jan 2009
            'From somewhere BizDivID is coming blank so implementing a check here which checks 
            'Conversion from string "" to type 'Integer' is valid
            If Not IsNothing(Request("BizdivID")) Then
                If Request("BizdivID") <> "" Then
                    BizdivID = Request("BizdivID")
                Else
                    BizdivID = ApplicationSettings.OWUKBizDivId
                End If
            Else
                BizdivID = ApplicationSettings.OWUKBizDivId
            End If
            'End Change
            Cache.Remove("WODetails" & Session.SessionID)
            populateData(True)
            SetLabelNames()
            ShowHideColumns()
            ShowHideLinks()
            ShowHidePanels()
            'poonam modified on 9/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
            'populate note type ddl
            CommonFunctions.PopulateNoteCategory(Page, ddlNoteType)
        End If
    End Sub


    Public Function getWODetails(Optional ByVal killcache As Boolean = False) As DataSet
        If killcache = True Then
            Session.Remove("AdminWODetails" & Session.SessionID)
        End If
        Dim ds As DataSet
        If Session("AdminWODetails" & Session.SessionID) Is Nothing Then
            ds = ws.WSWorkOrder.GetAdminWODetails(ViewState("WOID"), ViewState("CompanyID"), Session("UserID"))
            Session.Add("AdminWODetails" & Session.SessionID, ds)
        Else
            ds = CType(Session("AdminWODetails" & Session.SessionID), DataSet)
        End If
        ViewState("PlatformPrice") = ds.Tables(0).Rows(0).Item("PlatformPrice")
        ViewState("BillingLocID") = ds.Tables(0).Rows(0).Item("BillingLocID")
        If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("StagedWO")) Then
            Dim stagedWO As String = ds.Tables("tblDetails").Rows(0).Item("StagedWO")
            ViewState("StagedWO") = stagedWO.Trim
        Else
            ViewState("StagedWO") = "No"
        End If
        Return ds
    End Function

    Public Sub populateData(Optional ByVal killcache As Boolean = False)
        Dim ds As DataSet = getWODetails(killcache)
        'Set Page View by Business Area
        SetPageViewByBiz(ds)

        CommonFunctions.StoreVerNum(ds.Tables("tblSummary"))
        Dim dvVerNo As DataView
        If ds.Tables.Count > 3 Then
            dvVerNo = ds.Tables("tblLastAction").DefaultView

            CommonFunctions.StoreWOTrackingVerNum(dvVerNo)
        End If

        'Check and store if the wo was saves as staged or not
        If ds.Tables(0).Rows.Count > 0 Then
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("StagedWO")) Then
                If Trim(ds.Tables("tblSummary").Rows(0).Item("StagedWO")) = "Yes" Then
                    ViewState("PricingMethod") = ds.Tables("tblSummary").Rows(0).Item("PricingMethod")
                    ViewState("BusinessArea") = ds.Tables("tblSummary").Rows(0).Item("BusinessArea")
                End If
            End If
        End If

        'The following check is put to handle the simultaneous buyer supplier screen open issue. Temporarily this will not work for the DE site.
        'If ApplicationSettings.BizDivId <> ApplicationSettings.OWDEBizDivId Then
        '    If ViewState("Group") <> ds.Tables("tblSummary").Rows(0).Item("Status") Then
        '        If (ViewState("Group") = ApplicationSettings.WOGroupAccepted Or ViewState("Group") = ApplicationSettings.WOGroupCompleted) Then 'issue can be raised in this two stages.
        '            ViewState("Group") = ds.Tables("tblSummary").Rows(0).Item("Status")
        '        End If
        '    End If
        'End If

        'The following check is put to handle the simultaneous buyer supplier screen open issue. Temporarily this will not work for the DE site.
        If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
            If ViewState("Group") <> ds.Tables("tblSummary").Rows(0).Item("Status") Then
                If (ViewState("Group") = ApplicationSettings.WOGroupAccepted Or ViewState("Group") = "ActiveElapsed" Or ViewState("Group") = ApplicationSettings.WOGroupCompleted) Then 'issue can be raised in this two stages.
                    ViewState("Group") = ds.Tables("tblSummary").Rows(0).Item("Status")
                End If
            End If
        End If

        hdnValue.Value = ""
        hdnSpecialistID.Value = 0
        'Populate WO Summary
        If (ds.Tables("tblSummary").Rows.Count <> 0) Then
            'Buyer Contact ID
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("ContactId")) Then
                ViewState("BuyerContID") = ds.Tables("tblSummary").Rows(0).Item("ContactId")
            End If
            'Buyer Company ID
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("CompanyId")) Then
                ViewState("BuyerCompID") = ds.Tables("tblSummary").Rows(0).Item("CompanyId")
            End If

            'WO No
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("RefWOID")) Then
                lblWONo.Text = ds.Tables("tblSummary").Rows(0).Item("RefWOID")
            End If
            'Created
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateCreated")) Then
                lblCreated.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateCreated"), DateFormat.ShortDate)
            End If
            'Modified
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateModified")) Then
                lblModified.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateModified"), DateFormat.ShortDate)
            End If
            'Location
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Location")) Then
                lblLocation.Text = ds.Tables("tblSummary").Rows(0).Item("Location")
            End If
            'Created By
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("BuyerContact")) Then
                lblBuyerContact1.Text = ds.Tables("tblSummary").Rows(0).Item("BuyerContact")
            End If
            'WO Title
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("WOTitle")) Then
                lblTitle.Text = ds.Tables("tblSummary").Rows(0).Item("WOTitle")
            End If
            'WO Start
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateStart")) Then
                lblWOStart.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
            End If
            'Wholesale Price
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice")) Then
                lblWholesalePrice.Text = FormatCurrency(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.True, TriState.False)
                hdnValue.Value = FormatNumber(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice"), 2)
                ViewState("WholesalePrice") = FormatNumber(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice"), 2)
            End If
            'Location
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("UpSellPrice")) Then
                lblUpsellPrice.Text = ds.Tables("tblSummary").Rows(0).Item("UpSellPrice")
            End If
            'Platform Price
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("PlatformPrice")) Then
                lblPlatformPrice.Text = FormatCurrency(ds.Tables("tblSummary").Rows(0).Item("PlatformPrice"), 2, TriState.True, TriState.True, TriState.False)
                hdnValue.Value = FormatNumber(ds.Tables("tblSummary").Rows(0).Item("PlatformPrice"), 2)
            End If
            'Specialist ID
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("SpecialistID")) Then
                If Not IsNothing(ds.Tables("tblSummary").Rows(0).Item("SpecialistID")) Then
                    hdnSpecialistID.Value = ds.Tables("tblSummary").Rows(0).Item("SpecialistID")
                End If
            End If
            'Status
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Status")) Then
                lblStatus.Text = ds.Tables("tblSummary").Rows(0).Item("Status")
                Session("Group") = ds.Tables("tblSummary").Rows(0).Item("Status")
            End If
            'Billing Location
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("BillingLocName")) Then
                lblbillloc.Text = ds.Tables("tblSummary").Rows(0).Item("BillingLocName")

            End If
            'Category
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("WOCategory")) Then
                lblCategory.Text = ds.Tables("tblSummary").Rows(0).Item("WOCategory")
            End If
            'Start Date
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateStart")) Then
                lblStartDate.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
            End If
            'End Date
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateEnd")) Then
                lblEndDate.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate)
            End If

            'Quantity
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Quantity")) Then
                lblQuantity.Text = Convert.ToString(ds.Tables("tblSummary").Rows(0).Item("Quantity"))
            End If
            'Quantity
            'lblQuantity.Text = "1"
            'If (trEndDate.Visible <> False) Then
            '    If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateStart")) Then
            '        If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateEnd")) Then
            '            Dim dt1 As DateTime = Convert.ToDateTime(lblStartDate.Text)
            '            Dim dt2 As DateTime = Convert.ToDateTime(lblEndDate.Text)
            '            lblQuantity.Text = (DateDiff(DateInterval.Day, dt1, dt2) + 1).ToString
            '        End If
            '    End If

            'End If
            'Estimated Time         

            If ApplicationSettings.Country = ApplicationSettings.CountryDE Then
                'Estimated Time
                If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("EstimatedTime")) Then
                    If ds.Tables("tblSummary").Rows(0).Item("EstimatedTime") <> "" Then
                        lblEstimatedTime.Text = ds.Tables("tblSummary").Rows(0).Item("EstimatedTime")
                    Else
                        lblEstimatedTime.Text = ResourceMessageText.GetString("NoneSpecified")
                    End If
                Else
                    lblEstimatedTime.Text = ResourceMessageText.GetString("NoneSpecified")  '    "None Specified"
                End If
            Else
                If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("EstimatedTimeInDays")) Then
                    If (ds.Tables("tblSummary").Rows(0).Item("EstimatedTimeInDays")).ToString <> "" Then
                        lblEstimatedTime.Text = ds.Tables("tblSummary").Rows(0).Item("EstimatedTimeInDays")
                    Else
                        lblEstimatedTime.Text = ResourceMessageText.GetString("NoneSpecified")
                    End If
                Else
                    lblEstimatedTime.Text = ResourceMessageText.GetString("NoneSpecified")  '    "None Specified"
                End If
            End If

            'Supply Parts
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("SpecialistSuppliesParts")) Then
                If ds.Tables("tblSummary").Rows(0).Item("SpecialistSuppliesParts") = False Then
                    lblSupplyParts.Text = ResourceMessageText.GetString("No")    '   "No"
                Else
                    lblSupplyParts.Text = ResourceMessageText.GetString("Yes")    ' "Yes"
                End If
            End If
            'Show Speciali Instructions
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("SpecialInstruction")) Then
                ViewState("ShowSpecialInstruc") = CBool(ds.Tables("tblSummary").Rows(0).Item("SpecialInstruction"))
            End If
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("BuyerAccepted")) Then
                ViewState("BuyerAccepted") = CBool(ds.Tables("tblSummary").Rows(0).Item("BuyerAccepted"))
            End If

        End If

        If (ds.Tables("tblDetails").Rows.Count <> 0) Then
            'Hide View Notes Icon if there are no Notes.
            If ds.Tables("tblDetails").Rows(0).Item("NtCnt") = 0 Then
                lnkNotes.Visible = False
            End If

            'Hide watch/unwatch icon

            If ds.Tables("tblDetails").Rows(0).Item("IsWatch") = 0 Then
                lnkbtnWatch.Visible = True
                lnkbtnUnwatch.Visible = False
            Else
                lnkbtnWatch.Visible = False
                lnkbtnUnwatch.Visible = True
            End If
           
      

        'ThermostatsQuestions
        If ds.Tables("tblDetails").Rows(0).Item("ThermostatsQuestions") <> "" Then
            pnlThermostatsQuestions.Visible = True
            lblThermostatsQuestionsInfo.Text = ds.Tables("tblDetails").Rows(0).Item("ThermostatsQuestions")
        Else
            pnlThermostatsQuestions.Visible = False
        End If
        'TVSize
        If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("TVSize")) Then
            If ds.Tables("tblDetails").Rows(0).Item("TVSize") <> "" Then
                lblTVSize.Text = ds.Tables("tblDetails").Rows(0).Item("TVSize")
            Else
                lblTVSize.Text = ResourceMessageText.GetString("NoneSpecified")
            End If
        End If
        'PO Number
        If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("PONumber")) Then
            If ds.Tables("tblDetails").Rows(0).Item("PONumber") <> "" Then
                lblPONumber.Text = ds.Tables("tblDetails").Rows(0).Item("PONumber")
            Else
                lblPONumber.Text = ResourceMessageText.GetString("NoneSpecified")
            End If
        Else
            lblPONumber.Text = ResourceMessageText.GetString("NoneSpecified")
        End If
        'JRS Number
        If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("JRSNumber")) Then
            If ds.Tables("tblDetails").Rows(0).Item("JRSNumber") <> "" Then
                lblJRSNumber.Text = ds.Tables("tblDetails").Rows(0).Item("JRSNumber")
            Else
                lblJRSNumber.Text = ResourceMessageText.GetString("NoneSpecified")
            End If
        Else
            lblJRSNumber.Text = ResourceMessageText.GetString("NoneSpecified")
        End If

        'Depot Name (Location Of Goods)
        If ds.Tables("tblDetails").Rows(0).Item("DepotName") <> "" Then
            lblDepotName.Text = ds.Tables("tblDetails").Rows(0).Item("DepotName")
        ElseIf ds.Tables("tblDetails").Rows(0).Item("LocationID") = ds.Tables("tblDetails").Rows(0).Item("GoodsLocation") Then
            lblDepotName.Text = "Customer's Home"
        Else
            lblDepotName.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
        End If

        'Depot User
        If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("DepotUser")) Then
            If ds.Tables("tblDetails").Rows(0).Item("DepotUser") <> "" Then
                lblGoodsUser.Text = ds.Tables("tblDetails").Rows(0).Item("DepotUser")
            Else
                lblGoodsUser.Text = ResourceMessageText.GetString("NoneSpecified")
            End If
        Else
            lblGoodsUser.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
        End If
        'Location Address (previously Business Division)
        If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("BusinessDivision")) Then
            If ds.Tables("tblDetails").Rows(0).Item("BusinessDivision") <> "" Then
                lblBusinessDivision.Text = ds.Tables("tblDetails").Rows(0).Item("BusinessDivision")
            Else
                lblBusinessDivision.Text = ResourceMessageText.GetString("NoneSpecified")
            End If
        Else
            lblBusinessDivision.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
            trDepotAddress.Visible = False
            trDepotUser.Visible = False
        End If

        'Dress Code
        If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("DressCode")) Then
            If ds.Tables("tblDetails").Rows(0).Item("DressCode") <> "" Then
                lblDressCode.Text = ds.Tables("tblDetails").Rows(0).Item("DressCode")
            End If
        Else
            lblDressCode.Text = ResourceMessageText.GetString("NoneSpecified")  '    "None Specified"
        End If


        'Products Purchased
        If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("EquipmentDetails")) Then
            If ds.Tables("tblDetails").Rows(0).Item("EquipmentDetails") <> "" Then
                lblProducts.Text = ds.Tables("tblDetails").Rows(0).Item("EquipmentDetails")
            Else
                lblProducts.Text = ResourceMessageText.GetString("NoneSpecified")  '    "None Specified"
            End If
        Else
            lblProducts.Text = ResourceMessageText.GetString("NoneSpecified")  '    "None Specified"
        End If

        'Additional Products Purchased
        If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")) Then
            If ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts") <> "" Then
                If lblProducts.Text <> ResourceMessageText.GetString("NoneSpecified") Then
                    lblProducts.Text = lblProducts.Text + "<BR>" + ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")
                Else
                    lblProducts.Text = ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")
                End If
            End If
        End If

        'Special Instructions
            'If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("SpecialInstructions")) Then
            '    lblSpecialInstructions.Text = ds.Tables("tblDetails").Rows(0).Item("SpecialInstructions")
            '    End If
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("SpecialInstructions")) Then
                Dim SpecialInstructions As String = ds.Tables("tblDetails").Rows(0).Item("SpecialInstructions").ToString()
                If (SpecialInstructions <> "") Then
                    SpecialInstructions = Replace(SpecialInstructions, "lt;/br&gt;", "", , 1)
                    'divSpecialInstructions.InnerHtml = Server.HtmlDecode(SpecialInstructions.Replace("<br>", ""))
                    divSpecialInstructions.InnerHtml = Server.HtmlDecode(SpecialInstructions)
                    'lblSpecialInstructions.Text = Server.HtmlDecode(SpecialInstructions.Replace("<br>", ""))
                    lblSpecialInstructions.Text = Server.HtmlDecode(SpecialInstructions)
                End If
            End If

        'Client Special Instructions
            'If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("ClientSpecialInstructions")) Then
            '    lblCustSpecialInstructions.Text = ds.Tables("tblDetails").Rows(0).Item("ClientSpecialInstructions")
            '    End If
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("ClientSpecialInstructions")) Then
                Dim ClientSpecialInstructions As String = ds.Tables("tblDetails").Rows(0).Item("ClientSpecialInstructions").ToString()
                If (ClientSpecialInstructions <> "") Then
                    ClientSpecialInstructions = Replace(ClientSpecialInstructions, "lt;/br&gt;", "", , 1)
                    'divCustSpecialInstructions.InnerHtml = Server.HtmlDecode(ClientSpecialInstructions.Replace("<br>", ""))
                    divCustSpecialInstructions.InnerHtml = Server.HtmlDecode(ClientSpecialInstructions)
                    ' lblCustSpecialInstructions.Text = ds.Tables("tblDetails").Rows(0).Item("ClientSpecialInstructions")
                End If
            End If

        'Long Desc
            'If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("WOLongDesc")) Then
            '    lblLongDesc.Text = ds.Tables("tblDetails").Rows(0).Item("WOLongDesc").ToString.Replace(Chr(13), "<BR>")
            'End If
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("WOLongDesc")) Then
                Dim WOLongDesc As String = ds.Tables("tblDetails").Rows(0).Item("WOLongDesc").ToString()
                If (WOLongDesc <> "") Then
                    WOLongDesc = Replace(WOLongDesc, "lt;/br&gt;", "", , 1)
                    'divScopeOfWork.InnerHtml = Server.HtmlDecode(WOLongDesc.Replace("<br>", ""))
                    divScopeOfWork.InnerHtml = Server.HtmlDecode(WOLongDesc)

                    'lblLongDesc.Text = Server.HtmlEncode(ds.Tables("tblDetails").Rows(0).Item("WOLongDesc").ToString.Replace(Chr(13), "<BR>"))
                End If
            End If

        ''ClientScope
        'If ds.Tables("tblDetails").Rows(0).Item("ClientScope").ToString.Trim = "" Then
        '    pnlClientScope.Visible = False
        'Else
        '    pnlClientScope.Visible = True
        '    lblClientScope.Visible = True
        '    lblTagClientScope.Visible = True
        '    lblClientScope.Text = ds.Tables("tblDetails").Rows(0).Item("ClientScope").ToString
        'End If
            ' lblClientScope.Text = ds.Tables("tblDetails").Rows(0).Item("ClientScope").ToString
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("ClientScope")) Then

                Dim ClientScope As String = ds.Tables("tblDetails").Rows(0).Item("ClientScope").ToString()
                If ClientScope <> "" Then
                    ClientScope = Replace(ClientScope, "lt;/br&gt;", "", , 1)
                    'divClientScope.InnerHtml = Server.HtmlDecode(ClientScope.Replace("<br>", ""))
                    divClientScope.InnerHtml = Server.HtmlDecode(ClientScope)
                End If
            End If
            'Installation Time
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("AptTime")) Then
                If ds.Tables("tblDetails").Rows(0).Item("AptTime") <> "" Then
                    lblInstallTime.Text = " - " & ds.Tables("tblDetails").Rows(0).Item("AptTime").ToString
                Else
                    lblInstallTime.Text = ResourceMessageText.GetString("NoneSpecified")
                End If
            Else
                lblInstallTime.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
            End If

            'Freesat Make/Model
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("FreesatMake")) Then
                If ds.Tables("tblDetails").Rows(0).Item("FreesatMake") <> "" Then
                    lblFreesatMake.Text = ds.Tables("tblDetails").Rows(0).Item("FreesatMake").ToString
                Else
                    trFreesatMake.Visible = False
                End If
            Else
                trFreesatMake.Visible = False
            End If

            'Sales Agent
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("SalesAgent")) Then
                If ds.Tables("tblDetails").Rows(0).Item("SalesAgent") <> "" Then
                    lblSalesAgent.Text = ds.Tables("tblDetails").Rows(0).Item("SalesAgent").ToString
                Else
                    lblSalesAgent.Text = ResourceMessageText.GetString("NoneSpecified")
                End If
            Else
                lblSalesAgent.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
            End If

            'Receipt Number
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("ReceiptNumber")) Then
                If ds.Tables("tblDetails").Rows(0).Item("ReceiptNumber") <> "" Then
                    lblReceiptNumber.Text = ds.Tables("tblDetails").Rows(0).Item("ReceiptNumber").ToString
                Else
                    lblReceiptNumber.Text = ResourceMessageText.GetString("NoneSpecified")
                End If
            Else
                lblReceiptNumber.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
            End If

            If CompanyID = OrderWorkLibrary.ApplicationSettings.OutsourceCompany Then
                lblReceiptNumberHeader.Text = "Client Ref. No"
            End If
        End If

        'Buyer Supplier Info
        Dim RowNoBuyer As Integer
        Dim RowNoSupp As Integer
        If (ds.Tables("tblContact").Rows.Count <> 0) Then
            If ds.Tables("tblContact").Rows(0).Item("ContactType") = "Supplier" Then
                RowNoSupp = 0
                RowNoBuyer = 1
            ElseIf ds.Tables("tblContact").Rows(0).Item("ContactType") = "Buyer" Then
                RowNoBuyer = 0
                RowNoSupp = 1
            End If

            'BUYER INFO
            'Name
            If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Name")) Then
                lblClientContact.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Name")
            End If
            'Buyer Company
            If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Company")) Then
                lblClientCompany.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Company")
                lblBuyerCompany.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Company")
            End If
            'Buyer Address
            If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Address")) Then
                lblClientAddress.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Address")
            End If
            'Buyer Phone  
            If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Phone")) And Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Phone")) Then
                lblClientPhone.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Phone")
            End If
            'Buyer Mobile  
            If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Mobile")) And Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Mobile")) Then
                lblClientMobile.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Mobile")
            End If

            'Temporary locatiom
            If Not IsDBNull(ds.Tables("tblWOLoc").Rows(0).Item("Type")) Then
                'If ds.Tables("tblWOLoc").Rows(0).Item("Type") = "Temporary" Then
                'If Temporary loc assigned to WO - 
                If Not IsDBNull(ds.Tables("tblWOLoc").Rows(0).Item("Address")) Then
                    ' lblSpecialInstructions.Text = "WorkOrder Location: " & ds.Tables("tblWOLoc").Rows(0).Item("Address") & "<br>" & lblSpecialInstructions.Text
                    divSpecialInstructions.InnerHtml = "WorkOrder Location: " & ds.Tables("tblWOLoc").Rows(0).Item("Address") & "<br>" & (lblSpecialInstructions.Text)

                End If
                'End If
            End If



            tdBtmRating.Visible = False
            ''this is to show the rating only if webconfig key set to 1 i.e. OW and SF, for DE showrating = 0
            'As per managed services only supplier ratings will be shown
            'If ApplicationSettings.ShowRating = 1 Then
            '    tdBtmRating.Visible = True
            '    'Total Rating
            '    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("TotalRating")) Then
            '        lblRating.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("TotalRating")
            '    End If
            '    'Positive Rating
            '    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("PositiveRating")) Then
            '        lblPosRating.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("PositiveRating")
            '    End If
            '    'Negative Rating
            '    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("NegativeRating")) Then
            '        lblNegRating.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("NegativeRating")
            '    End If
            '    'Neutral Rating
            '    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("NeutralRating")) Then
            '        lblNeuRating.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("NeutralRating")
            '    End If
            'Else
            '    tdBtmRating.Visible = False
            'End If

            'SUPPLIER INFO
            If ds.Tables("tblContact").Rows.Count > 1 Then
                'Email
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Email")) Then
                    lblEmail.Text = "<b>Email Id: </b>" & ds.Tables("tblContact").Rows(RowNoSupp).Item("Email")
                End If
                'Name
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Name")) Then
                    lblContactName.Text = "<b> Contact Name: </b>" & ds.Tables("tblContact").Rows(RowNoSupp).Item("Name")
                    lblSupplierContactName.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("Name")
                End If
                'Supplier Company
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Company")) Then
                    ViewState("SupplierCompany") = ds.Tables("tblContact").Rows(RowNoSupp).Item("Company")
                    lblSupplierCompanyName.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("Company")
                    'lblSupplierCompany.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("Company")
                End If
                'Supplier Address
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Address")) Then
                    lblSupplierAddress.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("Address")
                End If
                'Supplier Phone  
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Phone")) Then
                    lblPhoneNo.Text = "<b>Phone No: </b>" & ds.Tables("tblContact").Rows(RowNoSupp).Item("Phone")
                    lblSupplierPhone.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("Phone")
                End If
                'Supplier Mobile  
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Mobile")) Then
                    lblMobileNo.Text = "<b>Mobile No: </b>" & ds.Tables("tblContact").Rows(RowNoSupp).Item("Mobile")
                    lblSupplierMobile.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("Mobile")
                End If
                'tdTopRating.Visible = False
                'this is to show the rating only if webconfig key set to 1 i.e. OW and SF, for DE showrating = 0
                If ApplicationSettings.ShowRating = 1 Then
                    If Not IsNothing(tdTopRating) Then
                        tdTopRating.Visible = True
                    End If
                    'Total Rating
                    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("RatingScore")) Then
                        lblRatingSupplier.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("RatingScore").ToString().Replace(".00", "") & "%"
                    End If
                    'Positive Rating
                    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("PositiveRating")) Then
                        lblPosRatingSupplier.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("PositiveRating")
                    End If
                    'Negative Rating
                    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("NegativeRating")) Then
                        lblNegRatingSupplier.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("NegativeRating")
                    End If
                    'Neutral Rating  lblSupplierPhone
                    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("NeutralRating")) Then
                        lblNeuRatingSupplier.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("NeutralRating")
                    End If
                Else
                    If Not IsNothing(tdTopRating) Then
                        tdTopRating.Visible = False
                    End If
                End If
            Else
                ViewState("showSupplierInfo") = "False"
            End If
        End If

        If ViewState("Group") = ApplicationSettings.WOGroupCA Then
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("BuyerCompany")) Then
                lblBuyerCompany.Text = ds.Tables("tblSummary").Rows(0).Item("BuyerCompany")
            End If

        End If

        'Last Action
        'If ViewState("Group") = ApplicationSettings.WOGroupAccepted Or ViewState("Group") = ApplicationSettings.WOGroupIssue Then
        If ViewState("Group") = ApplicationSettings.WOGroupIssue Then
            If Not IsNothing(ds.Tables("tblLastAction")) Then
                If ds.Tables("tblLastAction").Rows.Count > 0 Then
                    Dim dvContact As DataView = ds.Tables("tblContact").Copy.DefaultView
                    dvContact.RowFilter = "ContactType = '" + ApplicationSettings.ViewerSupplier + "'"
                    If dvContact.Count <> 0 Then
                        ViewState("SuppCompanyID") = dvContact.Item(0).Item("CompanyID")
                        ViewState("SuppContactID") = dvContact.Item(0).Item("ContactID")
                    End If
                    Dim dv As DataView
                    dv = ds.Tables("tblLastAction").DefaultView
                    Session("BindDataList") = dv
                    BindDataList(dv)
                End If
            End If
        End If

        'CA Details
        If ViewState("Group") = ApplicationSettings.WOGroupCA Then
            If Not IsNothing(ds.Tables("tblCADetails")) Then
                If ds.Tables("tblCADetails").Rows.Count > 0 Then
                    'ViewState("SuppCompanyID") = ds.Tables("tblCADetails").Rows(0).Item("CompanyID")
                    'ViewState("SuppContactID") = ds.Tables("tblCADetails").Rows(0).Item("ContactID")
                    'Added By Pankaj Malav on 16 Dec 2008 as part of added functionality of CA Send Mail
                    'This below code stores the count in view state and later used for showing or hiding the link.
                    ViewState.Add("CASupplierCount", ds.Tables("tblCADetails").Rows.Count)

                    Dim dv As DataView
                    dv = ds.Tables("tblCADetails").DefaultView
                    Session("BindDataList") = dv
                    BindDataList(dv)
                End If
            End If
        End If


        'Attachments
        '******************************************************************************
        If Not Page.IsPostBack Then
            Dim flagPnlAttachment As Boolean = False
            If Not IsNothing(ds.Tables("tblAttachments")) Then
                If ds.Tables("tblAttachments").Rows.Count > 0 Then
                    Dim dv As DataView
                    dv = ds.Tables("tblAttachments").DefaultView
                    dv.RowFilter = "LinkSource in ('WorkOrder','WOComplete','AdminWOIssue')"
                    If dv.Count > 0 Then
                        litAttachments.Text = ResourceMessageText.GetString("WOAttachments")    '   "<strong>WO Attachments:</strong><br>"
                    End If
                    flagPnlAttachment = True
                    Dim drv As DataRowView
                    For Each drv In dv
                        litAttachments.Text &= "<span class='txtGreySmallSelected'><a class='footerTxtSelected' href='" + getlink(drv("FilePath")) + "' target='_blank'>" + drv("Name") + "</a><br></span>"
                    Next

                    '************
                    dv.RowFilter = ""
                    dv.RowFilter = "LinkSource = 'StatementOfWork'"
                    If dv.Count > 0 Then
                        litAttachments.Text &= "<br>"
                        litAttachments.Text &= ResourceMessageText.GetString("SWAttachments")
                        For Each drv In dv
                            litAttachments.Text &= "<span class='txtGreySmallSelected'><a class='footerTxtSelected' href='" + getlink(drv("FilePath")) + "' target='_blank'>" + drv("Name") + "</a><br></span>"
                        Next
                    End If

                    '*************

                End If
            End If
            'Supplier attachments
            'If CStr(ViewState("Group")).ToLower = "completed" Or CStr(ViewState("Group")).ToLower = "closed" Or CStr(ViewState("Group")).ToLower = "cancelled" Then
            If Not IsNothing(ds.Tables("tblSupplierAttachments")) Then
                If ds.Tables("tblSupplierAttachments").Rows.Count > 0 Then
                    Dim dv As DataView
                    dv = ds.Tables("tblSupplierAttachments").DefaultView
                    litAttachments.Text &= ResourceMessageText.GetString("SupplierAttachments")      '   "<br/><strong>Supplier Attachments:</strong><br/>"
                    flagPnlAttachment = True
                    Dim drv As DataRowView
                    For Each drv In dv
                        litAttachments.Text &= "<span class='txtGreySmallSelected'><a class='footerTxtSelected' href='" + getlink(drv("FilePath")) + "' target='_blank'>" + drv("Name") + "</a><br></span>"
                    Next
                End If
            End If
            'End If
            If flagPnlAttachment = False Then
                pnlAttachments.Visible = False
            Else
                pnlAttachments.Visible = True
            End If
            If Not IsNothing(ViewState("Group")) Then
                Dim dvQA As DataView = ds.Tables("tblQA").DefaultView
                If CStr(ViewState("Group")).ToLower = "completed" Or CStr(ViewState("Group")).ToLower = "closed" Or CStr(ViewState("Group")).ToLower = "invoiced" Then
                    dvQA.RowFilter = "Type in ('WorkOrder')"
                    If dvQA.Count > 0 Then
                        pnlQA.Visible = True
                        rpQA.DataSource = dvQA
                        rpQA.DataBind()
                    End If
                    dvQA.RowFilter = ""
                End If
                dvQA.RowFilter = "Type in ('WOSubmitQuestion')"
                If dvQA.Count > 0 Then
                    pnlQAWOSubmit.Visible = True
                    rpQAWOSubmit.DataSource = dvQA
                    rpQAWOSubmit.DataBind()
                End If
                dvQA.RowFilter = ""
            End If

            Dim ds_ServicesSelection As DataSet
            ds_ServicesSelection = ws.WSWorkOrder.GetDixonsServicesSelection(ViewState("WOID"))

            If Not IsNothing(ds_ServicesSelection) Then
                If ds_ServicesSelection.Tables(0).Rows.Count > 0 Then
                    pnlServicesSelection.Visible = True

                    grdServicesSelection.DataSource = ds_ServicesSelection.Tables(0)
                    grdServicesSelection.DataBind()

                End If
            End If

        End If
        '******************************************************************************
    End Sub

    Public Sub BindDataList(ByVal dv As DataView)
        Dim dvSource As DataView = dv
        DLSupplierResponse.DataSource = dvSource
        DLSupplierResponse.DataBind()
    End Sub

    Public Sub ShowHideColumns()
        Dim strGroup As String = ViewState("Group")
        If (strGroup = ApplicationSettings.WOGroupDraft Or strGroup = ApplicationSettings.WOGroupSubmitted Or strGroup = ApplicationSettings.WOGroupSent Or strGroup = ApplicationSettings.WOGroupCA) Then
            tdSupplierCompany.Visible = False
            tdSupplierCompany1.Visible = False
        End If
        tdBuyerContact.Visible = False
        tdBuyerContact1.Visible = False
        tdSupplierContact.Visible = False
        tdSupplierContact1.Visible = False

        If (strGroup = ApplicationSettings.WOGroupDraft) Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Created")       '   Created
            lblHdrModified.Text = ResourceMessageText.GetString("Submitted")  '   "Submitted"
        End If

        'If strGroup = ApplicationSettings.WOGroupDraft Or strGroup = ApplicationSettings.WOGroupSubmitted Or strGroup = ApplicationSettings.WOGroupCA Or strGroup = ApplicationSettings.WOGroupInTray Or strGroup = ApplicationSettings.WOGroupLost Then
        'lblHdrWholesalePrice.Text = ResourceMessageText.GetString("ProposedPrice")  '   "Proposed Price"
        'lblHdrPlatformPrice.Text = ResourceMessageText.GetString("ProposedPrice")  '   "Proposed Price"
        lblHdrWholesalePrice.Text = "Wholesale Price"
        lblHdrPlatformPrice.Text = "Portal Price"
        'End If
        If strGroup = ApplicationSettings.WOGroupSubmitted Or strGroup = ApplicationSettings.WOGroupCA Or strGroup = ApplicationSettings.WOGroupSent Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Submitted")   '   "Submitted"
        End If
        If strGroup = ApplicationSettings.WOGroupInvoiced Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Created")   '   "Created"
            lblHdrModified.Text = ResourceMessageText.GetString("InvoicedDate")  '   "Invoiced Date"
        End If
        If strGroup = ApplicationSettings.WOGroupCancelled Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Submitted")   '   "Submitted"
            lblHdrModified.Text = ResourceMessageText.GetString("Cancelled")  '   "Cancelled"
        End If
        If strGroup = ApplicationSettings.WOGroupSubmitted Or strGroup = ApplicationSettings.WOGroupSent Then
            lblHdrModified.Text = ResourceMessageText.GetString("LastSent") '   "Last Sent"
        End If
        If strGroup = ApplicationSettings.WOGroupAccepted Or strGroup = "ActiveElapsed" Or strGroup = ApplicationSettings.WOGroupCompleted Or strGroup = ApplicationSettings.WOGroupClosed Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Accepted") '   "Accepted"
        End If
        If strGroup = ApplicationSettings.WOGroupAccepted Or strGroup = "ActiveElapsed" Or strGroup = ApplicationSettings.WOGroupIssue Or strGroup = ApplicationSettings.WOGroupCA Then
            lblHdrModified.Text = ResourceMessageText.GetString("LastResponse")
        End If
        If strGroup = ApplicationSettings.WOGroupIssue Then
            lblHdrCreated.Text = ResourceMessageText.GetString("IssueRaised") '   "Issue Raised"
        End If
        If strGroup = ApplicationSettings.WOGroupCompleted Then
            lblHdrModified.Text = ResourceMessageText.GetString("Completed")    '   "Completed"
        End If
        If strGroup = ApplicationSettings.WOGroupClosed Then
            lblHdrModified.Text = ResourceMessageText.GetString("Closed")    '   "Closed"
        End If
        If strGroup = ApplicationSettings.WOGroupInTray Or strGroup = ApplicationSettings.WOGroupLost Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Received") '   "Received"
        End If
        If strGroup = ApplicationSettings.WOGroupLost Then
            lblHdrModified.Text = ResourceMessageText.GetString("Lost")    '   "Lost"
        End If

        If strGroup = ApplicationSettings.WOGroupDraft Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Created")  '   "Created"
            lblHdrModified.Text = ResourceMessageText.GetString("Modified") '   "Modified"
        End If
        If IsNothing(strGroup) Or strGroup = "" Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Created")  '   "Created"
            lblHdrModified.Text = ResourceMessageText.GetString("Modified") '   "Modified"
        End If
    End Sub

    Public Sub ShowHideLinks()
        Dim strGroup As String = ViewState("Group")
        Dim ContactId As Integer
        Dim Companyid As Integer
        Dim SupContactID As Integer

        If Not IsNothing(Request("ContactId")) Then
            If Request("ContactId").Trim <> "" Then
                ContactId = Request("ContactId")
            End If
        End If
        If Not IsNothing(Request("CompanyId")) Then
            If Request("CompanyId").Trim <> "" Then
                Companyid = Request("CompanyId")
            End If
        End If
        If Not IsNothing(Request("SupContactId")) Then
            If Request("SupContactId").Trim <> "" Then
                SupContactID = Request("SupContactId")
            Else
                SupContactID = 0
            End If
        End If

        If Not IsNothing(Request("SupCompId")) Then
            If Request("SupCompId").Trim <> "" Then
                SupCompID = Request("SupCompId")
            Else
                SupCompID = 0
            End If
        End If

        Dim appendLink As String = ""
        Dim ds As DataSet = getWODetails(False)
        appendLink &= "WOID=" & ViewState("WOID")
        appendLink &= "&WorkOrderID=" & ViewState("WorkOrderID")
        appendLink &= "&ContactID=" & ContactId
        appendLink &= "&CompanyID=" & Companyid
        appendLink &= "&SupContactId=" & SupContactID
        appendLink &= "&SupCompID=" & SupCompID

        If (Request("Group") = "ActiveElapsed") Then
            appendLink &= "&Group=" & Request("Group")
        Else
            appendLink &= "&Group=" & ViewState("Group")
        End If
        If Not IsNothing(Request("vendorIDs")) Then
            appendLink &= "&vendorIDs=" & Request("vendorIDs")
        End If

        appendLink &= "&BizDivID=" & Request("BizDivID")
        appendLink &= "&FromDate=" & Request("FromDate")
        appendLink &= "&ToDate=" & Request("ToDate")
        appendLink &= "&PS=" & Request("PS")
        appendLink &= "&PN=" & Request("PN")
        appendLink &= "&SC=" & Request("SC")
        appendLink &= "&SO=" & Request("SO")
        appendLink &= "&CompID=" & Request("CompID")
        If Not IsNothing(ViewState("SuppCompanyID")) And IsNothing(SupCompID) Then
            appendLink &= "&SupCompId=" & ViewState("SuppCompanyID")
        End If
        If Not IsNothing(Request("Over30Days")) Then
            appendLink &= "&Over30Days=" & Request("Over30Days")
        End If
        If Not IsNothing(Request("PONumber")) Then
            appendLink &= "&txtPONumber=" & Request("PONumber")
        End If
        If Not IsNothing(Request("PostCode")) Then
            appendLink &= "&txtPostCode=" & Request("PostCode")
        End If
        If Not IsNothing(Request("txtInvoiceNo")) Then
            appendLink &= "&txtInvoiceNo=" & Request("txtInvoiceNo")
        End If
        If Not IsNothing(Request("Keyword")) Then
            appendLink &= "&txtKeyword=" & Request("Keyword")
        End If
        If Not IsNothing(Request("txtWorkorderID")) Then
            appendLink &= "&txtWorkorderID=" & Request("txtWorkorderID")
        End If
        If Not IsNothing(Request("txtCompanyName")) Then
            appendLink &= "&txtCompanyName=" & Request("txtCompanyName")
        End If
        If Not IsNothing(Request("InvoiceNo")) Then
            appendLink &= "&InvoiceNo=" & Request("InvoiceNo")
        End If
        If Not IsNothing(Request("tab")) Then
            appendLink &= "&tab=" & Request("tab")
        End If

        'Edit Icon
        If strGroup = ApplicationSettings.WOGroupDraft Or strGroup = ApplicationSettings.WOGroupSubmitted Or strGroup = ApplicationSettings.WOGroupSent Then
            lnkEdit.Visible = True
            lnkEdit.HRef = "~/WOForm.aspx?" & appendLink & "&sender=UCWOsListing"
        End If

        'Update WO Icon
        If strGroup = ApplicationSettings.WOGroupCA Then
            lnkUpdateWO.Visible = True
            lnkUpdateWO.HRef = "~/WOSetWholesalePrice.aspx?" & appendLink & "&sender=UCWOsListing&IsUpdate=yes"
            imgSetWholesalePrice.ImageUrl = "~/Images/Icons/UpdateWO.gif"
            imgSetWholesalePrice.AlternateText = "Update Workorder"
            imgSetWholesalePrice.ToolTip = "Update Workorder"
        ElseIf strGroup = ApplicationSettings.WOGroupClosed Or strGroup = ApplicationSettings.WOGroupCancelled Or strGroup = ApplicationSettings.WOGroupInvoiced Or strGroup = ApplicationSettings.WOGroupDraft Then
            lnkUpdateWO.Visible = False
        Else
            'If Request("sender") <> "" Then
            '    If Request("sender") = "SalesInvoiceWOListing" Then
            '        lnkUpdateWO.Visible = False
            '    Else
            '        lnkUpdateWO.Visible = True
            '    End If
            'Else
            '    lnkUpdateWO.Visible = True
            'End If
            lnkUpdateWO.Visible = True ' Faizan
            imgSetWholesalePrice.ImageUrl = "~/Images/Icons/SetWholesalePrice.gif"
            lnkUpdateWO.HRef = "~/WOSetWholesalePrice.aspx?" & appendLink & "&sender=UCWOsListing"
            imgSetWholesalePrice.AlternateText = "Set Wholesale Price"
            imgSetWholesalePrice.ToolTip = "Set Wholesale Price"
        End If



        'Order Match Icon
        If strGroup = ApplicationSettings.WOGroupSubmitted Or strGroup = ApplicationSettings.WOGroupCA Or strGroup = ApplicationSettings.WOGroupSent Then
            If Not IsDBNull(ViewState("PlatformPrice")) Then
                If (ViewState("BillingLocID").ToString <> "0") Then
                    lnkOrderMatch.Visible = True
                    lnkOrderMatch.HRef = "~/WOOrdermatch.aspx?" & appendLink & "&sender=UCWOsListing"
                Else
                    lnkOrderMatch.Visible = True
                    lnkOrderMatch.HRef = "Javascript:NoBillLocOrdermatch();"
                End If

            Else
                lnkSetPlatformPrice.Visible = True
                lnkSetPlatformPrice.HRef = "~/WOSetPlatformPrice.aspx?" & appendLink & "&sender=UCWOsListing"
            End If
        End If

        'Platform Price Icon
        If strGroup = ApplicationSettings.WOGroupSubmitted Then
            lnkSetPlatformPrice.Visible = True
            lnkSetPlatformPrice.HRef = "~/WOSetPlatformPrice.aspx?" & appendLink & "&sender=UCWOsListing"
        End If

        'Notes Link Icon
        If Request("sender") <> "" Then
            lnkNotes.HRef = "~/WONotes.aspx?" & appendLink & "&sender=" & Request("sender")
        Else
            lnkNotes.HRef = "~/WONotes.aspx?" & appendLink & "&sender=UCWODetails"
        End If


        'Cancel WO Icon
        If Not IsNothing(strGroup) Then
            If strGroup <> ApplicationSettings.WOGroupDraft And strGroup <> ApplicationSettings.WOGroupClosed And strGroup <> ApplicationSettings.WOGroupCancelled And strGroup <> "Invoiced" Then
                lnkCancelWO.Visible = True
                lnkCancelWO.HRef = "~/WOCancellation.aspx?" & appendLink & "&sender=UCWOsListing"
            End If
        End If

        'Close Issue
        'If strGroup = ApplicationSettings.WOGroupIssue Then
        '    lnkCloseIssue.Visible = True
        '    lnkCloseIssue.HRef = "~/WOCloseIssue.aspx?" & appendLink & "&sender=UCWODetails"
        'End If

        'Copy Icon
        If strGroup = ApplicationSettings.WOGroupClosed Or strGroup = "Invoiced" Or strGroup = ApplicationSettings.WOGroupSubmitted Or strGroup = ApplicationSettings.WOGroupSent Or strGroup = ApplicationSettings.WOGroupCA Or strGroup = ApplicationSettings.WOGroupAccepted Or strGroup = ApplicationSettings.WOGroupIssue Or strGroup = ApplicationSettings.WOGroupCompleted Or strGroup = ApplicationSettings.WOGroupCancelled Then
            lnkCopy.Visible = True
            lnkCopy.HRef = "~/WOForm.aspx.aspx?" & appendLink & "&sender=UCWODetails"
        End If

        'Delete Icon
        If strGroup = ApplicationSettings.WOGroupDraft Then
            lnkDelete.Visible = True
            lnkDelete.HRef = "~/DeleteWO.aspx?" & appendLink & "&sender=UCWODetails"
        End If

        'Raise Issue Icon
        If strGroup = ApplicationSettings.WOGroupAccepted Or strGroup = "ActiveElapsed" Or strGroup = ApplicationSettings.WOGroupCompleted Then
            lnkRaiseIssue.Visible = True
            lnkRaiseIssue.HRef = "~/WORaiseIssue.aspx?" & appendLink & "&sender=UCWODetails&Action=RaiseIssue"
        End If

        'Complete Icon
        If (strGroup = ApplicationSettings.WOGroupAccepted Or strGroup = "ActiveElapsed") And ViewState("StagedWO") = "No" Then
            lnkComplete.Visible = True
            lnkComplete.HRef = "~/WOComplete.aspx?" & appendLink & "&sender=UCWOsListing"
        End If

        'Close WO Icon
        If strGroup = ApplicationSettings.WOGroupCompleted Then
            lnkClose.Visible = True
            lnkClose.HRef = "~/WOClose.aspx?" & appendLink & "&sender=UCWOsListing"
        End If

        ' Added Accpedted by Hemisha Desai on 25/04/2014
        If strGroup = ApplicationSettings.WOGroupAccepted Then
            lnkCASendMail.Visible = True
            lnkCASendMail.HRef = "~/AcceptedSendMail.aspx?" & appendLink & "&sender=UCWOsListing"
        End If

        'Added By Pankaj Malav on 16 Dec 2008 as part of added functionality of CA Send Mail Functionality
        'CA Send Mail icon
        If strGroup = ApplicationSettings.WOGroupCA Then
            If Not IsNothing(ViewState("CASupplierCount")) Then
                If ViewState("CASupplierCount") > 1 Then
                    lnkCASendMail.Visible = True
                    lnkCASendMail.HRef = "~/CASendMail.aspx?" & appendLink & "&sender=UCWOsListing"
                End If
            End If
        End If

        'RateWO Icon
        If strGroup = ApplicationSettings.WOGroupAccepted Or strGroup = "ActiveElapsed" Or strGroup = ApplicationSettings.WOGroupCompleted Or strGroup = ApplicationSettings.WOGroupClosed Or strGroup = ApplicationSettings.WOGroupIssue Then
            lnkbtnUpdateRating.Visible = True
        End If


        'History Icon
        If (strGroup = ApplicationSettings.WOGroupDraft Or strGroup = ApplicationSettings.WOGroupSubmitted Or strGroup = ApplicationSettings.WOGroupSent Or strGroup = ApplicationSettings.WOGroupCA Or strGroup = ApplicationSettings.WOGroupAccepted Or strGroup = "ActiveElapsed" Or strGroup = ApplicationSettings.WOGroupIssue Or strGroup = ApplicationSettings.WOGroupCompleted Or strGroup = ApplicationSettings.WOGroupClosed Or strGroup = ApplicationSettings.WOGroupCancelled Or strGroup = "Invoiced") Then
            lnkHistory.Visible = True
            lnkHistory.HRef = "~/AdminWOHistory.aspx?" & appendLink & "&sender=UCWOsListing"
            If Request("sender") = "SearchWO" Then
                lnkHistory.HRef = "~/AdminWOHistory.aspx?" & appendLink & "&sender=SearchWO" & "&SrcWorkorderID=" & Request("txtWorkorderID") & "&CompanyName=" & Request("txtCompanyName") & "&Keyword=" & Request("KeyWord") & "&PONumber=" & Request("PONumber") & "&PostCode=" & Request("PostCode")
            End If
        End If
        If ViewState("WholesalePrice") > 0 And ViewState("BuyerAccepted") = 0 And (Request("Group") = ApplicationSettings.WOGroupSubmitted Or Request("Group") = ApplicationSettings.WOGroupCA Or Request("Group") = ApplicationSettings.WOGroupSent) Then

            lnkBuyerAcceptWR.Visible = True
            lnkBuyerAcceptWR.HRef = "~/AcceptWorkRequest.aspx?WOID=" & Request("WOID") & "&Group=" & Request("Group") & "&FromDate=" & Request("FromDate") & "&ToDate=" & Request("ToDate") & "&ContactID=" & Request("ContactId") & "&CompanyID=" & Request("CompanyID") & "&SupContactID=" & Request("SupContactID") & "&SupCompID=" & Request("SupCompID") & "&PS=" & Request("PS") & "&PN=" & Request("PN") & "&SC=" & Request("SC") & "&SO=" & Request("SO") & "&BizDivID=" & Request("BizDivID") & "&" & "CompID=" & Request("CompanyID") & "&sender=UCWOsListing"
        Else
            lnkBuyerAcceptWR.Visible = False
        End If

        'Customer History Icon
        lnkCustomerHistory.Visible = True
        If Not IsNothing(Request("txtWorkorderID")) Then
            appendLink &= "&txtWorkorderID=" & Request("txtWorkorderID")
        End If
        lnkCustomerHistory.HRef = "~/CustomerHistory.aspx?" & appendLink & "&sender=UCWODetails"


        If strGroup = ApplicationSettings.WOGroupCancelled Then
            lnkbtnWatch.Visible = False
            lnkbtnUnwatch.Visible = False
        End If

    End Sub

    Public Sub ShowHidePanels()
        'Sections To Show/Hide 
        'Summary = DGWorkOrders
        'Action Details = pnlSupplierResponse
        'Supplier Info = pnlSupplierContactInfo
        'WO Details (Title,Desc,Spl Ins) = divRatings
        'WO Specs(Type, Dates, Est Time, Supply Parts) = divRatings
        'WO Specs(PO No, Job No, Cust No) = tblWOSpecs
        'Location = = tblWOLocation
        'Dress Code = 
        'Attachments = tblWOAttachments
        'Buyer Info = pnlClientContactInfo
        Dim Group As String = ViewState("Group")


        'Show / Hide Summary
        'Needs to be visible for all statuses

        'Show / Hide Action Details. By default pnlSupplierResponse is Visible = false
        If (Group = ApplicationSettings.WOGroupCA Or Group = ApplicationSettings.WOGroupIssue) Then
            pnlSupplierResponse.Visible = True
        End If

        'Show / Hide Supplier Info

        If (Group = ApplicationSettings.WOGroupDraft Or Group = ApplicationSettings.WOGroupSubmitted Or Group = ApplicationSettings.WOGroupSent Or Group = ApplicationSettings.WOGroupCA) Then
            pnlSupplierContactInfo.Visible = False
        End If
        If Not IsNothing(ViewState("showSupplierInfo")) Then
            If ViewState("showSupplierInfo") = "False" Then
                pnlSupplierContactInfo.Visible = False
            End If
        End If


        'Show / Hide tblWOSpecs (PO No, Job No, Cust No)
        If (Group = ApplicationSettings.WOGroupInTray Or Group = ApplicationSettings.WOGroupLost) Then
            tblWOSpecs.Visible = False
            tblWOAttachments.Visible = False
        End If

        'Show / Hide tblWOLocation

        'Show / Hide tblWOAttachments
        'Handled In Show / Hide tblWOSpecs (PO No, Job No, Cust No) Above


        'Show / Hide Buyer Info
        If Group = ApplicationSettings.WOGroupInTray Or Group = ApplicationSettings.WOGroupLost Then
            pnlClientContactInfo.Visible = False
        End If
    End Sub

    Public Function getlink(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachment")
                Dim str As String = filePath.Substring(index)
                'str = ApplicationSettings.AttachmentDisplayPath(4) + str
                str = ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/" + str + CommonFunctions.AzureStorageAccessKey().ToString
                Return str.Replace("\", "/")
            End If
        End If
        Return ""
    End Function
    'Changed By Pankaj Malav Previously the filter is with WoTracking ID now changed to Company ID
    Public Function getDiscussions(ByVal WOTrackingID As Integer, ByVal CompanyID As Integer) As DataView
        Dim ds As DataSet = getWODetails()
        Dim dv As DataView = ds.Tables("tblComments").DefaultView
        If ViewState("Group") = ApplicationSettings.WOGroupCA Then
            dv.RowFilter = "SupCompany = " & CompanyID
        Else
            dv.RowFilter = "WOTrackingID = " & WOTrackingID
        End If
        Return dv
    End Function
    'Changed By Pankaj Malav Previously the filter is with WoTracking ID now changed to Company ID
    Public Function ShowHideComments(ByVal WOTrackingID As Integer, ByVal CompanyID As Integer) As Boolean
        Dim ds As DataSet = getWODetails()
        Dim dv As DataView = ds.Tables("tblComments").DefaultView
        If ViewState("Group") = ApplicationSettings.WOGroupCA Then
            dv.RowFilter = "SupCompany = " & CompanyID
        Else
            dv.RowFilter = "WOTrackingID = " & WOTrackingID
        End If
        If dv.Count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function SetLabelNames()
        lblBuyerContactLabel.Text = ResourceMessageText.GetString("BuyerInfo")  '    "Buyer Info"
        lblSupplierContactLabel.Text = ResourceMessageText.GetString("SupplierInfo")   '    "Supplier Info"
        Return 0
    End Function

    Private Sub lnkDelete_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDelete.ServerClick
        lnkDelete.Visible = False
        pnlConfirmation.Visible = True
        lblConfirm.Text = ResourceMessageText.GetString("ConfirmDeleteWO")
        ViewState("Action") = "delete"
    End Sub

    Private Sub lnkDiscard_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDiscard.ServerClick
        lnkDiscard.Visible = False
        pnlConfirmation.Visible = True
        lblConfirm.Text = ResourceMessageText.GetString("ConfirmDiscard")
        ViewState("Action") = "discard"
    End Sub

    Public Sub lnkCopy_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopy.ServerClick
        lnkCopy.Visible = False
        pnlConfirmation.Visible = True
        lblConfirm.Text = ResourceMessageText.GetString("ConfirmCopyWO")
        ViewState("Action") = "copy"
    End Sub

    Private Sub lnkSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSubmit.Click
        Dim ds_Success As New DataSet
        Dim TrackCompanyID As Integer = Session("CompanyId")
        Dim TrackContactID As Integer = Session("UserId")
        Dim TrackContactClassID As Integer = Session("RoleGroupID")
        Dim WOStatusText As String = ""
        ds_Success = ws.WSWorkOrder.WO_DiscardDelCopy(ViewState("WOID"), Request("BizDivID"), Request("CompanyID"), Request("ContactID"), ViewState("Action"), TrackCompanyID, TrackContactID, TrackContactClassID, CommonFunctions.FetchVerNum(), Session("UserID"), CommonFunctions.FetchWOTrackingVerNum)

        If ds_Success.Tables.Count = 0 Then
            divValidationMain.Visible = True
            lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
        ElseIf ds_Success.Tables("tblWOID").Rows(0).Item("Status") = -11 Then
            pnlActionConfirm.Visible = False
            divValidationMain.Visible = True

            If Not IsDBNull(ds_Success.Tables("tblWOID").Rows(0).Item("WOStatus")) Then
                Select Case ds_Success.Tables("tblWOID").Rows(0).Item("WOStatus")
                    Case ApplicationSettings.WOStatusID.Sent, ApplicationSettings.WOStatusID.Submitted
                        lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                        WOStatusText = "Sent"
                    Case ApplicationSettings.WOStatusID.Submitted
                        lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                        WOStatusText = "Submitted"
                    Case ApplicationSettings.WOStatusID.Draft
                        lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                        WOStatusText = "Draft"
                    Case ApplicationSettings.WOStatusID.Cancelled
                        lblError.Text = ResourceMessageText.GetString("WOVNoWOCancelled")
                        WOStatusText = "Cancelled"
                    Case ApplicationSettings.WOStatusID.CA
                        lblError.Text = ResourceMessageText.GetString("WOVNoWOCA")
                        WOStatusText = "CA"
                    Case ApplicationSettings.WOStatusID.Accepted
                        lblError.Text = ResourceMessageText.GetString("WOVNoWOAccepted")
                        WOStatusText = "Accepted"
                    Case ApplicationSettings.WOStatusID.Lost
                        lblError.Text = ResourceMessageText.GetString("WOVNoWOLost")
                        WOStatusText = "Lost"
                    Case Else
                        lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                End Select
                lblError.Text = lblError.Text.Replace("<Link>", "AdminWODetails.aspx?WOID=" & Encryption.Encrypt(ViewState("WOID")) & "&WorkOrderID=" & Encryption.Encrypt(ViewState("WorkOrderID")) & "&SupContactId=" & Request("SupContactId") & "&SupCompId=" & Request("SupCompId") & "&Group=" & WOStatusText & "&CompID=" & Request("CompID") & "&FromDate=&ToDate=&PS=" & Request("PS") & "&PN=0&SC=DateCreated&SO=1&Viewer=" & ViewState("Viewer") & "&sender=UCWOsListing")
            Else
                lblError.Text = "The WorkOrder has been deleted"
            End If
            'Case when the version number of the WOTracking has changed
        ElseIf ds_Success.Tables("tblWOID").Rows(0).Item("Status") = -10 Then
            pnlActionConfirm.Visible = False
            divValidationMain.Visible = True
            If Not IsDBNull(ds_Success.Tables("tblWOID").Rows(0).Item("WOStatus")) Then
                Select Case ds_Success.Tables("tblWOID").Rows(0).Item("WOTrackingStatus")
                    Case ApplicationSettings.WOStatusID.Discarded
                        lblError.Text = ResourceMessageText.GetString("WOVNoWODiscarded")
                        WOStatusText = "Discarded"
                    Case ApplicationSettings.WOStatusID.CA
                        lblError.Text = ResourceMessageText.GetString("WOVNoWOCA")
                        WOStatusText = "CA"
                    Case ApplicationSettings.WOStatusID.Lost
                        lblError.Text = ResourceMessageText.GetString("WOVNoWOLost")
                        WOStatusText = "Lost"
                    Case Else
                        lblError.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                End Select
                lblError.Text = lblError.Text.Replace("<Link>", "AdminWODetails.aspx?WOID=" & Encryption.Encrypt(ViewState("WOID")) & "&WorkOrderID=" & Encryption.Encrypt(ViewState("WorkOrderID")) & "&SupContactId=" & Request("SupContactId") & "&SupCompId=" & Request("SupCompId") & "&Group=" & WOStatusText & "&CompID=" & Request("CompID") & "&FromDate=&ToDate=&PS=" & Request("PS") & "&PN=0&SC=DateCreated&SO=1&Viewer=" & ViewState("Viewer") & "&sender=UCWOsListing")
            Else
                pnlActionConfirm.Visible = False
                lblError.Text = "The WorkOrder has been deleted"
            End If

        Else
            divValidationMain.Visible = False
            backToListing("Confirm", ds_Success)
        End If
    End Sub

    Private Sub backToListing(Optional ByVal btnVal As String = "", Optional ByVal ds As DataSet = Nothing)
        Dim appendLink As String = ""
        appendLink &= "WOID=" & ViewState("WOID")
        appendLink &= "&ContactID=" & Request("ContactID")
        appendLink &= "&CompanyID=" & ViewState("CompanyID")
        If btnVal = "Confirm" And ViewState("Action") = "discard" Then
            appendLink &= "&Group=Lost"
            appendLink &= "&sender=" & "UCWOsListing"
            appendLink &= "&mode=Lost"
        Else
            appendLink &= "&Group=" & ViewState("Group")
            appendLink &= "&mode=" & ViewState("Group")
            appendLink &= "&sender=" & "UCWOsDetails"
        End If
        appendLink &= "&BizDivID=" & Request("BizDivID")
        appendLink &= "&FromDate=" & Request("FromDate")
        appendLink &= "&ToDate=" & Request("ToDate")
        appendLink &= "&PS=" & Request("PS")
        appendLink &= "&PN=" & Request("PN")
        appendLink &= "&SC=" & Request("SC")
        appendLink &= "&SO=" & Request("SO")


        'if action is delete and performed by admin - redirect to admin wo listing
        If ViewState("Action") = "delete" Then
            Response.Redirect("~/AdminWOListing.aspx?" & appendLink)
        ElseIf ViewState("Action") = "discard" Then
            Response.Redirect("~/AdminWOListing.aspx?" & appendLink)
        ElseIf ViewState("Action") = "copy" Then
            'Pass the contactid and company id also
            If btnVal = "Confirm" Then
                If Not IsNothing(ds) Then
                    Response.Redirect("~/WOForm.aspx?WOID=" & ds.Tables("tblWOID").Rows(0).Item("WOID") & "&CompanyId=" & Request("CompanyID") & "&ContactId=" & Request("ContactId"))
                Else
                    Response.Redirect("~/WOForm.aspx?WOID=" & ViewState("WOID") & "&CompanyId=" & Request("CompanyID") & "&ContactId=" & Request("ContactId"))
                End If
            Else
                Response.Redirect("~/AdminWODetails.aspx?" & appendLink)
            End If
        End If
    End Sub

    Private Sub lnkCancelAction_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancelAction.ServerClick
        backToListing("Cancel")
    End Sub

    Private Sub DLSupplierResponse_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLSupplierResponse.ItemCreated

        If e.Item.ItemType = ListItemType.Header Then
            e.Item.FindControl("tdDLCompany").Visible = True
            e.Item.FindControl("tdDLContact").Visible = True
            e.Item.FindControl("tdDLPhone").Visible = True
            e.Item.FindControl("tdDLSpecialist").Visible = True
            'e.Item.FindControl("tdActionHdr").Visible = False
            ' CType(e.Item.FindControl("tdRatingHdr"), System.Web.UI.HtmlControls.HtmlTableCell).Attributes.Add("style", "border-right-width:0px;")

            '************************************Staged WO Code*************************************
            If ViewState("Group") = ApplicationSettings.WOGroupCA Or ViewState("Group") = ApplicationSettings.WOGroupIssue Then
                e.Item.FindControl("tdDLEndDate").Visible = False
                If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                    e.Item.FindControl("tdAptTimeHD").Visible = True
                    CType(e.Item.FindControl("lblStartHdg"), Label).Text = "Apt.Date"
                Else
                    e.Item.FindControl("tdAptTimeHD").Visible = False
                    CType(e.Item.FindControl("lblStartHdg"), Label).Text = "Start"
                End If
                e.Item.FindControl("tdDLContact").Visible = False
                e.Item.FindControl("tdDLPhone").Visible = False
                e.Item.FindControl("tdDLSpecialist").Visible = False
                e.Item.FindControl("tdDLSupplyParts").Visible = False
                e.Item.FindControl("tdStatusHdr").Visible = False
                If ViewState("PricingMethod") = "DailyRate" Then
                    e.Item.FindControl("tdDLDayRate").Visible = True
                    e.Item.FindControl("tdDLEstimatedTime").Visible = True
                Else
                    e.Item.FindControl("tdDLDayRate").Visible = False
                    e.Item.FindControl("tdDLEstimatedTime").Visible = False
                End If
            Else
                e.Item.FindControl("tdDLEndDate").Visible = True
                e.Item.FindControl("tdAptTimeHD").Visible = False
                e.Item.FindControl("tdDLContact").Visible = True
                e.Item.FindControl("tdDLPhone").Visible = True
                e.Item.FindControl("tdDLSpecialist").Visible = True
                e.Item.FindControl("tdDLSupplyParts").Visible = True
                e.Item.FindControl("tdStatusHdr").Visible = True
                e.Item.FindControl("tdDLDayRate").Visible = False
                e.Item.FindControl("tdDLEstimatedTime").Visible = False
            End If
            '************************************Staged WO Code*************************************

        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.FindControl("tdDLCompany1").Visible = True
            e.Item.FindControl("tdDLContact1").Visible = True
            e.Item.FindControl("tdDLPhone1").Visible = True
            e.Item.FindControl("tdDLSpecialist1").Visible = True
            'e.Item.FindControl("tdAction").Visible = False
            'CType(e.Item.FindControl("tdRating"), System.Web.UI.HtmlControls.HtmlTableCell).Attributes.Add("style", "border-right-width:0px;")

            '************************************Staged WO Code*************************************
            If ViewState("Group") = ApplicationSettings.WOGroupCA Or ViewState("Group") = ApplicationSettings.WOGroupIssue Then
                e.Item.FindControl("tdDLEndDate1").Visible = False
                e.Item.FindControl("tdAptTime").Visible = True
                e.Item.FindControl("tdDLContact1").Visible = False
                e.Item.FindControl("tdDLPhone1").Visible = False
                e.Item.FindControl("tdDLSpecialist1").Visible = False
                e.Item.FindControl("tdDLSupplyParts1").Visible = False
                e.Item.FindControl("tdStatus").Visible = False
                If ViewState("PricingMethod") = "DailyRate" Then
                    e.Item.FindControl("tdDLDayRate1").Visible = True
                    e.Item.FindControl("tdDLEstimatedTime1").Visible = True
                Else
                    e.Item.FindControl("tdDLDayRate1").Visible = False
                    e.Item.FindControl("tdDLEstimatedTime1").Visible = False
                End If
            Else
                e.Item.FindControl("tdDLEndDate1").Visible = True
                e.Item.FindControl("tdAptTime").Visible = False
                e.Item.FindControl("tdDLContact1").Visible = True
                e.Item.FindControl("tdDLPhone1").Visible = True
                e.Item.FindControl("tdDLSpecialist1").Visible = True
                e.Item.FindControl("tdDLSupplyParts1").Visible = True
                e.Item.FindControl("tdStatus").Visible = True
                e.Item.FindControl("tdDLDayRate1").Visible = False
                e.Item.FindControl("tdDLEstimatedTime1").Visible = False
            End If
            '************************************Staged WO Code*************************************
        End If


        'this is to show the rating only if webconfig key set to 1 i.e. OW and SF, for DE showrating = 0
        If ApplicationSettings.ShowRating = 1 Then
            If e.Item.ItemType = ListItemType.Header Then
                e.Item.FindControl("tdRatingHdr").Visible = True
            End If
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.FindControl("tdRating").Visible = True
            End If
        End If
    End Sub

    Public Function GetActionColLinks(ByVal ContactID As Integer, ByVal CompanyID As Integer)
        Dim SuppContID = ContactID
        Dim SuppCompID = CompanyID
        Dim link As String = ""
        Dim appendLink As String = ""
        Dim strGroup As String = ViewState("Group")
        Dim killcache As Boolean = False
        Dim ds As DataSet = getWODetails(killcache).Copy
        appendLink &= "WOID=" & ViewState("WOID")
        appendLink &= "&ContactId=" & ViewState("BuyerContID")
        appendLink &= "&CompanyId=" & ViewState("BuyerCompID")
        If Not IsNothing(ViewState("SuppContactID")) Then
            appendLink &= "&SupContactId=" & ViewState("SuppContactID")
        Else
            appendLink &= "&SupContactId=" & SuppContID
        End If
        If Not IsNothing(ViewState("SuppCompanyID")) Then
            appendLink &= "&SupCompId=" & ViewState("SuppCompanyID")
        Else
            appendLink &= "&SupCompId=" & SuppCompID
        End If
        appendLink &= "&Group=" & ViewState("Group")
        appendLink &= "&BizDivID=" & Request("BizDivID")
        appendLink &= "&FromDate=" & Request("FromDate")
        appendLink &= "&ToDate=" & Request("ToDate")
        appendLink &= "&PS=" & Request("PS")
        appendLink &= "&PN=" & Request("PN")
        appendLink &= "&SC=" & Request("SC")
        appendLink &= "&SO=" & Request("SO")
        appendLink &= "&sender=" & "UCWODetails"



        'Accept CA Icon and Discuss Issue Icon
        If strGroup = ApplicationSettings.WOGroupCA Then
            If ds.Tables("tblCADetails").Rows.Count > 0 Then
                If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("SupplierAccepted")) Then
                    If ds.Tables("tblSummary").Rows(0).Item("SupplierAccepted") = "False" Then
                        link &= "<a id='lnkAcceptCA' runat='server' href='WOAcceptCA.aspx?Action=AcceptCA&" & appendLink & "'><img src='Images/Icons/ConditionalAccept.gif' title='" & ResourceMessageText.GetString("AcceptCA") & "' width='16' height='12' hspace='2' vspace='0' border='0'></a>"
                    End If
                Else
                    link &= "<a id='lnkAcceptCA' runat='server' href='WOAcceptCA.aspx?Action=AcceptCA&" & appendLink & "'><img src='Images/Icons/ConditionalAccept.gif' title='" & ResourceMessageText.GetString("AcceptCA") & "' width='16' height='12' hspace='2' vspace='0' border='0'></a>"
                End If
            End If
            link &= "<a id='lnkDiscussCA' runat='server' href='WODiscussCA.aspx?Action=DiscussCA&" & appendLink & "'><img src='Images/Icons/Discuss.gif' title='" & ResourceMessageText.GetString("Discuss") & "' width='16' height='12' hspace='2' vspace='0' border='0'></a>"
        End If

        'Change Issue Terms Icon and Accept Issue Icon
        If strGroup = ApplicationSettings.WOGroupIssue Then
            'THE FOLLOWING ICONS WILL BE SHOWN TO THE ADMIN ALL THE TIME IN THIS STATUS
            link &= "<a id='lnkDiscussIssue' runat='server' href='WODiscuss.aspx?Action=DiscussIssue&" & appendLink & "'><img src='Images/Icons/Discuss.gif' title='" & ResourceMessageText.GetString("Discuss") & "' width='16' height='12' hspace='2' vspace='0' border='0'></a>"
            link &= "<a id='lnkAcceptIssue' runat='server' href='WOAcceptIssue.aspx?Action=AcceptIssue&" & appendLink & " '><img src='Images/Icons/Accept-Issue.gif' title='" & ResourceMessageText.GetString("AcceptIssue") & "' width='14' height='13' hspace='2' vspace='0' border='0'></a>"


            'Check here if the issue is raised by this user, then only the icon needs to be shown.
            If Not IsNothing(ds.Tables("tblLastAction")) Then
                If (ds.Tables("tblLastAction").Rows.Count > 0) Then
                    If (ds.Tables("tblLastAction").Rows(0).Item("ContactClassID")) = ApplicationSettings.RoleOWID Then 'Admin has raised the issue
                        link &= "<a id='lnkEditIssue' runat='server' href='WOChangeIssue.aspx?Action=EditIssue&" & appendLink & " '><img src='Images/Icons/EditIssue.gif' title='" & ResourceMessageText.GetString("ChangeTerms") & "' width='17' height='13' hspace='1' vspace='2' border='0'></a>"
                    ElseIf (ds.Tables("tblLastAction").Rows(0).Item("ContactClassID")) = ApplicationSettings.RoleSupplierID Then 'Supplier has raised the issue
                        link &= "<a id='lnkRejectIssue' runat='server' href='WORejectIssue.aspx?Action=RejectIssue&" & appendLink & "&sender=UCWODetails" & " '><img src='Images/Icons/Reject.gif' title='Reject Issue' hspace='2' vspace='0' border='0'></a>"
                    End If
                End If
            End If
        End If
        Return link
    End Function

    Public Function GetSupSkills(ByVal SupCompanyID As Integer, ByVal SpecialistID As Object) As String
        If ApplicationSettings.BizDivId = ApplicationSettings.SFUKBizDivId And ViewState("Group") = ApplicationSettings.WOGroupCA And (ViewState("Viewer") = ApplicationSettings.ViewerBuyer Or ViewState("Viewer") = ApplicationSettings.ViewerAdmin) Then
            Dim ds As DataSet = getWODetails(False)
            Dim dv As DataView = ds.Tables(7).DefaultView

            Dim strfinal As New StringBuilder
            Dim i As Integer
            Dim str, strskills As String
            str = ""
            strskills = ""
            dv.RowFilter = "CompanyID = " & SupCompanyID
            If dv.Count <> 0 Then


                dv.RowFilter = "CompanyID = " & SupCompanyID & " and Type = 'Skill'"
                If dv.Count <> 0 Then
                    strskills = "<b>Skills:</b>"
                    For i = 0 To dv.Count - 1
                        If str <> "" Then
                            str &= ", "
                        End If
                        str &= dv.Item(i).Item("Name")
                    Next
                    strskills &= str
                    str = ""
                End If
                dv.RowFilter = "CompanyID = " & SupCompanyID & " and Type = 'Exam'"
                If dv.Count <> 0 Then
                    strskills &= "<br><br><b>Certifications:</b><br>"
                    For i = 0 To dv.Count - 1
                        If str <> "" Then
                            str &= "<br>"
                        End If
                        str &= dv.Item(i).Item("Name")
                    Next
                    strskills &= str
                    str = ""
                End If

                If strskills <> "" Then
                    strfinal.Append("<span class='discussionTextSupplier'>")
                    If Not IsDBNull(SpecialistID) Then
                        If SpecialistID <> 0 Then
                            strfinal.Append("<p>" & ResourceMessageText.GetString("WOSupplierSpecialistSkills") & "</p>")
                        Else
                            'this filters for microsoft skills
                            dv.RowFilter = "CompanyID = " & SupCompanyID & " and Type = 'Skill' and Description <> 'Non Microsoft'"
                            If dv.Count <> 0 Then
                                strfinal.Append("<p>" & ResourceMessageText.GetString("WOSupplierSkills") & "</p>")
                            Else
                                'show msg tht specialist does not have microsoft skills
                                strfinal.Append("<p>" & ResourceMessageText.GetString("WOSupplierMicroSkills") & "</p>")
                            End If
                        End If
                    Else
                        dv.RowFilter = "CompanyID = " & SupCompanyID & " and Type = 'Skill' and Description <> 'Non Microsoft'"
                        If dv.Count <> 0 Then
                            strfinal.Append("<p>" & ResourceMessageText.GetString("WOSupplierSkills") & "</p>")
                        Else
                            'show msg tht specialist does not have microsoft skills
                            strfinal.Append("<p>" & ResourceMessageText.GetString("WOSupplierMicroSkills") & "</p>")
                        End If
                    End If
                    strfinal.Append("</span><span class='remarksAreaCollapse'>")
                    strfinal.Append(strskills & "</span>")
                End If
            End If
            Return strfinal.ToString
        Else
            Return ""
        End If
    End Function


    Private Sub SortDataList(ByVal SortColumn, ByVal SortOrder)
        Dim dv As DataView = New DataView()
        dv = CType(Session("BindDataList"), DataView)
        dv.Sort = SortColumn & " " & SortOrder
        BindDataList(dv)
    End Sub

    Public Sub lnkID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkID.Click
        If ViewState("IDSortOrder") Is Nothing Then
            ViewState("IDSortOrder") = "ASC"
        Else
            If ViewState("IDSortOrder") = "ASC" Then
                ViewState("IDSortOrder") = "Desc"
            Else
                ViewState("IDSortOrder") = "ASC"
            End If

        End If
        SortDataList("WOTrackingId", ViewState("IDSortOrder"))
    End Sub

    Public Sub lnkActionDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkActionDate.Click

        If ViewState("ActionDateSortOrder") Is Nothing Then
            ViewState("ActionDateSortOrder") = "ASC"
        Else
            If ViewState("ActionDateSortOrder") = "ASC" Then
                ViewState("ActionDateSortOrder") = "Desc"
            Else
                ViewState("ActionDateSortOrder") = "ASC"
            End If

        End If
        SortDataList("DateModified", ViewState("ActionDateSortOrder"))

    End Sub


    Public Sub lnkStart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkStart.Click

        If ViewState("DateStartSortOrder") Is Nothing Then
            ViewState("DateStartSortOrder") = "ASC"
        Else
            If ViewState("DateStartSortOrder") = "ASC" Then
                ViewState("DateStartSortOrder") = "Desc"
            Else
                ViewState("DateStartSortOrder") = "ASC"
            End If

        End If
        SortDataList("DateStart", ViewState("DateStartSortOrder"))
    End Sub


    Public Sub lnkEnd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEnd.Click

        If ViewState("DateEndSortOrder") Is Nothing Then
            ViewState("DateEndSortOrder") = "ASC"
        Else
            If ViewState("DateEndSortOrder") = "ASC" Then
                ViewState("DateEndSortOrder") = "Desc"
            Else
                ViewState("DateEndSortOrder") = "ASC"
            End If

        End If
        SortDataList("WOTrackingId", ViewState("DateEndSortOrder"))
    End Sub


    Public Sub lnkCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCompany.Click

        If ViewState("CompanySortOrder") Is Nothing Then
            ViewState("CompanySortOrder") = "ASC"
        Else
            If ViewState("CompanySortOrder") = "ASC" Then
                ViewState("CompanySortOrder") = "Desc"
            Else
                ViewState("CompanySortOrder") = "ASC"
            End If

        End If
        SortDataList("Company", ViewState("CompanySortOrder"))
    End Sub


    Public Sub lnkContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkContact.Click

        If ViewState("NameSortOrder") Is Nothing Then
            ViewState("NameSortOrder") = "ASC"
        Else
            If ViewState("NameSortOrder") = "ASC" Then
                ViewState("NameSortOrder") = "Desc"
            Else
                ViewState("NameSortOrder") = "ASC"
            End If

        End If
        SortDataList("Name", ViewState("NameSortOrder"))
    End Sub

    Public Sub lnkPhone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPhone.Click

        If ViewState("PhoneSortOrder") Is Nothing Then
            ViewState("PhoneSortOrder") = "ASC"
        Else
            If ViewState("PhoneSortOrder") = "ASC" Then
                ViewState("PhoneSortOrder") = "Desc"
            Else
                ViewState("PhoneSortOrder") = "ASC"
            End If

        End If
        SortDataList("Phone", ViewState("PhoneSortOrder"))
    End Sub

    Public Sub lnkSpecialist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSpecialist.Click

        If ViewState("SpecialistIDSortOrder") Is Nothing Then
            ViewState("SpecialistIDSortOrder") = "ASC"
        Else
            If ViewState("SpecialistIDSortOrder") = "ASC" Then
                ViewState("SpecialistIDSortOrder") = "Desc"
            Else
                ViewState("SpecialistIDSortOrder") = "ASC"
            End If

        End If
        SortDataList("SpecialistID", ViewState("SpecialistIDSortOrder"))
    End Sub


    Public Sub lnkPrice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPrice.Click

        If ViewState("ValueSortOrder") Is Nothing Then
            ViewState("ValueSortOrder") = "ASC"
        Else
            If ViewState("ValueSortOrder") = "ASC" Then
                ViewState("ValueSortOrder") = "Desc"
            Else
                ViewState("ValueSortOrder") = "ASC"
            End If

        End If
        SortDataList("Value", ViewState("ValueSortOrder"))
    End Sub


    Public Sub lnkDayRate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDayRate.Click

        If ViewState("DayJobRateSortOrder") Is Nothing Then
            ViewState("DayJobRateSortOrder") = "ASC"
        Else
            If ViewState("DayJobRateSortOrder") = "ASC" Then
                ViewState("DayJobRateSortOrder") = "Desc"
            Else
                ViewState("DayJobRateSortOrder") = "ASC"
            End If

        End If
        SortDataList("DayJobRate", ViewState("DayJobRateSortOrder"))
    End Sub


    Public Sub lnkTime_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTime.Click
        If ViewState("EstimatedTimeInDaysSortOrder") Is Nothing Then
            ViewState("EstimatedTimeInDaysSortOrder") = "ASC"
        Else
            If ViewState("EstimatedTimeInDaysSortOrder") = "ASC" Then
                ViewState("EstimatedTimeInDaysSortOrder") = "Desc"
            Else
                ViewState("EstimatedTimeInDaysSortOrder") = "ASC"
            End If

        End If
        SortDataList("EstimatedTimeInDays", ViewState("EstimatedTimeInDaysSortOrder"))
    End Sub


    Public Sub lnkSupplyParts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSupplyParts.Click
        If ViewState("SpecialistSuppliesPartsSortOrder") Is Nothing Then
            ViewState("SpecialistSuppliesPartsSortOrder") = "ASC"
        Else
            If ViewState("SpecialistSuppliesPartsSortOrder") = "ASC" Then
                ViewState("SpecialistSuppliesPartsSortOrder") = "Desc"
            Else
                ViewState("SpecialistSuppliesPartsSortOrder") = "ASC"
            End If

        End If
        SortDataList("SpecialistSuppliesParts", ViewState("SpecialistSuppliesPartsSortOrder"))
    End Sub


    Public Sub lnkStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkStatus.Click
        If ViewState("StatusSortOrder") Is Nothing Then
            ViewState("StatusSortOrder") = "ASC"
        Else
            If ViewState("StatusSortOrder") = "ASC" Then
                ViewState("StatusSortOrder") = "Desc"
            Else
                ViewState("StatusSortOrder") = "ASC"
            End If

        End If
        SortDataList("Status", ViewState("StatusSortOrder"))
    End Sub

    Public Sub lnkbtnWatch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnWatch.Click
        ws.WSWorkOrder.AddDeleteWatchedWO(ViewState("WOID"), Session("UserID"), "Add")
        ViewState("IsWatch") = 1
        lnkbtnWatch.Visible = False
        lnkbtnUnwatch.Visible = True
    End Sub

    Public Sub lnkbtnUnwatch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnUnwatch.Click
        ws.WSWorkOrder.AddDeleteWatchedWO(ViewState("WOID"), Session("UserID"), "Remove")
        ViewState("IsWatch") = 0
        lnkbtnWatch.Visible = True
        lnkbtnUnwatch.Visible = False
    End Sub

    Private Sub hdnBtnAction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnBtnAction.Click
        Dim Note As String = txtNoteAction.Text.Trim.Replace(Chr(13), "<BR>")
        Dim Comments As String = txtCommentAction.Text.Trim.Replace(Chr(13), "<BR>")
        Dim IsFlagged As Boolean = chkMyRedFlag.Checked
        Dim IsWatched As Boolean = chkMyWatch.Checked

        Dim Rating As Integer

        If (pnlActionRating.Style.Item("display") <> "none") Then
            If rdBtnActionNegative.Checked = True Then
                Rating = -1
            ElseIf rdBtnActionPositive.Checked = True Then
                Rating = 1
            ElseIf rdBtnActionNeutral.Checked = True Then
                Rating = 0
            Else
                Rating = 2
            End If
        Else
            Rating = 2
        End If
        'poonam modified on 9/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
        Dim Success As DataSet = CommonFunctions.SaveActionCommentNotes(ViewState("WOID"), Session("UserID"), Note, Comments, Session("CompanyID"), Rating, IsFlagged, IsWatched, chkShowClient.Checked, chkShowSupplier.Checked, ddlNoteType.SelectedItem.Value)
        txtNoteAction.Text = ""
        txtCommentAction.Text = ""
    End Sub


    Private Sub SetPageViewByBiz(ByVal ds As DataSet)
        ViewState("BusinessArea") = ds.Tables("tblDetails").Rows(0).Item("BusinessArea")
        If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
            lblStartDateLabel.Text = "Appointment"
            trEndDate.Visible = False
            trSalesAgent.Visible = True
            trJobNumber.Visible = True
            lblInstallTime.Visible = True
            trFreesatMake.Visible = True

            lblDepotNameLabel.Text = "Location of Goods"
            lblProductsLabel.Text = "Product Purchased"
        ElseIf ViewState("BusinessArea") = ApplicationSettings.BusinessAreaIT Then
            lblStartDateLabel.Text = "Start Date"
            trEndDate.Visible = True
            trSalesAgent.Visible = False
            trJobNumber.Visible = False
            lblInstallTime.Visible = False
            trFreesatMake.Visible = False

            lblDepotNameLabel.Text = "Location of Equipment"
            lblProductsLabel.Text = "Equipment Details"
        Else
            lblStartDateLabel.Text = "Start Date"
            trEndDate.Visible = True
            trSalesAgent.Visible = False
            trJobNumber.Visible = True
            lblInstallTime.Visible = False
            trFreesatMake.Visible = False

            lblDepotNameLabel.Text = "Location of Goods"
            lblProductsLabel.Text = "Product Purchased"
        End If
    End Sub

    Public Sub hdnBtnRateWO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnBtnRateWO.Click

        Dim Rating As Integer
        If rdBtnNegative.Checked = True Then
            Rating = -1
        ElseIf rdBtnPositive.Checked = True Then
            Rating = 1
        Else
            Rating = 0
        End If
        Dim Success As DataSet = CommonFunctions.SaveRatingComment(ViewState("WOID"), Session("CompanyID"), Session("UserID"), txtComment.Text.Trim, Rating)
        txtComment.Text = ""
    End Sub


    Private Sub lnkbtnUpdateRating_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnUpdateRating.ServerClick
        mdlUpdateRating.Show()
        rdBtnNegative.Checked = False
        rdBtnNeutral.Checked = False
        rdBtnPositive.Checked = False
        txtComment.Text = ""

        Dim ds As DataSet
        ds = CommonFunctions.GetWORating(CInt(ViewState("WOID")))
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
                    Dim Result As Integer = ds.Tables(0).Rows(0)(0)
                    Select Case Result
                        Case -1
                            rdBtnNegative.Checked = True
                        Case 0
                            rdBtnNeutral.Checked = True
                        Case 1
                            rdBtnPositive.Checked = True
                    End Select
                End If
                txtComment.Text = ds.Tables("tblRating").Rows(0).Item("Comments")
            End If
        End If
    End Sub

    Public Function GetLinks(ByVal parambizDivId As Object, ByVal contactId As Object, ByVal SupplierCompanyName As String) As String
        Dim link As String = ""
        Dim contactType As ApplicationSettings.ContactType = ApplicationSettings.ContactType.approvedSupplier
        Dim classId As String
        Dim statusId As String
        classId = ApplicationSettings.RoleSupplierID
        statusId = ApplicationSettings.StatusApprovedCompany

        Dim bizDivId = parambizDivId
        If SupCompID = 0 Then
            SupCompID = CompanyID
        End If
        If Not IsNothing(Request("sender")) Then
            If Request("sender").ToString.ToLower = "searchfinance" Then
                link &= "<a class='footerTxtSelected ' href='companyprofile.aspx?sender=WorkOrderDetails&bizDivId=" & bizDivId & "&WOID=" & Request("WOID") & "&contactType=" & contactType & "&WorkorderID=" & Request("WorkorderID") & "&companyid=" & SupCompID & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&Group=" & Request("Group") & "&mode=" & Request("mode") & "&ps=25" & "&pn=" & Request("PN") & "&sc=" & "&so=" & "&CompID=" & CompanyID & "&SrcPONumber=" & Request("txtPONumber") & "&SrcInvoiceNo=" & Request("txtInvoiceNo") & "&SrcKeyword=" & Request("txtKeyword") & "&SrcWorkOrderId=" & Request("txtWorkorderID") & "&SrcCompanyName=" & Request("CompanyName") & "&SrcInvoiceDate=" & Request("txtInvoiceDate") & "&FromDate=''" & "&ToDate=''" & "'>" & SupplierCompanyName & "</a>"
            Else
                link &= "<a class='footerTxtSelected ' href='companyprofile.aspx?sender=AdminWoDetails&bizDivId=" & bizDivId & "&WOID=" & Request("WOID") & "&contactType=" & contactType & "&WorkorderID=" & Request("WorkorderID") & "&companyid=" & SupCompID & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&Group=" & Request("Group") & "&mode=" & Request("mode") & "&ps=25" & "&pn=" & Request("PN") & "&sc=" & "&so=" & "&CompID=" & CompanyID & "&FromDate=''" & "&ToDate=''" & "'>" & SupplierCompanyName & "</a>"
            End If
        Else
            link &= "<a class='footerTxtSelected ' href='companyprofile.aspx?sender=AdminWoDetails&bizDivId=" & bizDivId & "&WOID=" & Request("WOID") & "&contactType=" & contactType & "&WorkorderID=" & Request("WorkorderID") & "&companyid=" & SupCompID & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&Group=" & Request("Group") & "&mode=" & Request("mode") & "&ps=25" & "&pn=" & Request("PN") & "&sc=" & "&so=" & "&CompID=" & CompanyID & "&FromDate=''" & "&ToDate=''" & "'>" & SupplierCompanyName & "</a>"
        End If

        'link &= "<a class='footerTxtSelected ' href='companyprofile.aspx?sender=AdminWoDetails&bizDivId=" & bizDivId & "&WOID=" & Request("WOID") & "&contactType=" & contactType & "&WorkorderID=" & Request("WorkorderID") & "&companyid=" & SupCompID & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&Group=" & Request("Group") & "&mode=" & Request("mode") & "&ps=25" & "&pn=" & Request("PN") & "&sc=" & "&so=" & "&CompID=" & CompanyID & "&FromDate=''" & "&ToDate=''" & "'>" & SupplierCompanyName & "</a>"

        Return link

    End Function
    Public Function GetLinksForResponsesupplier(ByVal parambizDivId As Object, ByVal contactId As Object, ByVal SupplierCompanyName As String, ByVal SupCompID As Object) As String
        Dim link As String = ""
        Dim contactType As ApplicationSettings.ContactType = ApplicationSettings.ContactType.approvedSupplier
        Dim classId As String
        Dim statusId As String
        classId = ApplicationSettings.RoleSupplierID
        statusId = ApplicationSettings.StatusApprovedCompany

        Dim bizDivId = parambizDivId

        link &= "<a class='footerTxtSelected ' href='companyprofile.aspx?sender=AdminWoDetails&bizDivId=" & bizDivId & "&WOID=" & Request("WOID") & "&contactType=" & contactType & "&WorkorderID=" & Request("WorkorderID") & "&companyid=" & SupCompID & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&Group=" & Request("Group") & "&mode=" & Request("mode") & "&ps=25" & "&pn=" & Request("PN") & "&sc=" & "&so=" & "&CompID=" & CompanyID & "&FromDate=''" & "&ToDate=''" & "'>" & SupplierCompanyName & "</a>"

        Return link

    End Function


    Private Sub lnkbtnQuickNote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnQuickNote.Click
        Dim WorkOrderId As Long
        WorkOrderId = Long.Parse(lnkbtnQuickNote.CommandArgument)
        CType(FindControl("mdlAction"), AjaxControlToolkit.ModalPopupExtender).Show()
        'poonam modified on 9/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
        ddlNoteType.SelectedIndex = -1
        rdBtnActionPositive.Checked = False
        rdBtnActionNeutral.Checked = False
        rdBtnActionNegative.Checked = False
        txtCommentAction.Text = ""
        pnlActionRating.Style.Add("display", "none")
        divActionWatch.Style.Add("display", "block")
        chkMyWatch.Checked = False
        chkMyRedFlag.Checked = False


        If Not IsNothing(Request("Group")) Then
            Dim Group As String = Request("Group")
            If (Group = ApplicationSettings.WOGroupAccepted Or Group = "Activeelapsed" Or Group = ApplicationSettings.WOGroupCompleted Or Group = ApplicationSettings.WOGroupClosed) Then
                pnlActionRating.Style.Add("display", "block")
            End If
            If (Group = ApplicationSettings.WOGroupClosed) Or (Group = ApplicationSettings.WOGroupCancelled) Or (Group = ApplicationSettings.WOGroupInvoiced) Then
                divActionWatch.Style.Add("display", "none")
            End If
        End If

        Dim ds As DataSet
        ds = CommonFunctions.GetWORating(WorkOrderId, CInt(Session("UserID")), "WOAction")
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
                    Dim Result As Integer = ds.Tables(0).Rows(0)(0)
                    Select Case Result
                        Case -1
                            rdBtnActionNegative.Checked = True
                        Case 0
                            rdBtnActionNeutral.Checked = True
                        Case 1
                            rdBtnActionPositive.Checked = True
                    End Select
                End If
                txtCommentAction.Text = ds.Tables("tblRating").Rows(0).Item("Comments")
            End If
            If ds.Tables(1).Rows.Count > 0 Then
                chkMyWatch.Checked = ds.Tables(1).Rows(0).Item("IsWatched")
                chkMyRedFlag.Checked = ds.Tables(1).Rows(0).Item("IsFlagged")
            End If
        End If
    End Sub

End Class