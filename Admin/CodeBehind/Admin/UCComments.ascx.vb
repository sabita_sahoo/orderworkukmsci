Public Partial Class UCComments
    Inherits System.Web.UI.UserControl

    ''' <summary>
    ''' Shared WSObject to access the web services
    ''' </summary>
    ''' <remarks></remarks>
    Shared ws As New WSObjs

    ''' <summary>
    ''' Property to set or get company value 
    ''' </summary>
    ''' <remarks></remarks>
    Private _companyID As Integer
    Private _contactid As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property
    Public Property ContactID() As Integer
        Get
            Return _contactid
        End Get
        Set(ByVal value As Integer)
            _contactid = value
        End Set
    End Property

    ''' <summary>
    ''' Set/get BizDivID of the contact
    ''' </summary>
    ''' <remarks></remarks>
    Private _bizDivID As Integer
    Public Property BizDivID() As Integer
        Get
            Return _bizDivID
        End Get
        Set(ByVal value As Integer)
            _bizDivID = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            divValidationMain.Visible = False
            CommonFunctions.PopulateCommentReason(Page, ddlCommentReason)
            PopulateComments()
            txtComment.Text = ""
        End If

    End Sub

    ''' <summary>
    ''' Procedure to populate comments page
    ''' </summary>
    ''' <param name="killcache"></param>
    ''' <remarks></remarks>
    Private Sub PopulateComments(Optional ByVal killcache As Boolean = False)



        UCFileUpload8.BizDivID = BizDivID
        UCFileUpload8.AttachmentForID = _contactid
        UCFileUpload8.CompanyID = _companyID
        UCFileUpload8.AttachmentForSource = "ContactComment"
        UCFileUpload8.Type = "ContactComment"
        UCFileUpload8.ShowExistAttach = False
        UCFileUpload8.ClearUCFileUploadCache()


        Dim dsContacts As DataSet
        dsContacts = GetComments(killcache)

        Dim dvContact As DataView
        If dsContacts.Tables(0).Rows.Count > 0 Then
            pnlResponse.Visible = True
            dvContact = dsContacts.Tables(0).Copy.DefaultView
            DLResponse.DataSource = dvContact
            DLResponse.DataBind()
        Else
            pnlResponse.Visible = False
        End If

    End Sub



    ''' <summary>
    ''' Function to show / hide region if comments is avaiable or not
    ''' </summary>
    ''' <param name="ContactID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ShowHideComments(ByVal ContactID As Integer) As Boolean
        Dim showComments As Boolean = True
        Dim ds As DataSet = GetComments()
        Dim dvComments As DataView

        dvComments = ds.Tables(0).Copy.DefaultView
        dvComments.RowFilter = "ContactID = " & ContactID


        If dvComments.Count <> 0 Then
            If Trim(dvComments.Item(0).Item("Comments")) = "" Then
                showComments = False
            End If
        End If
        Return showComments
    End Function


    ''' <summary>
    ''' Procedure to fetch comments data from database and store in cache
    ''' </summary>
    ''' <param name="KillCache"></param>
    ''' <param name="contactid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function GetComments(Optional ByVal KillCache As Boolean = False, Optional ByVal contactid As Integer = 0) As DataSet
        Dim cacheKey As String = "Comments" & "-" & CStr(_contactid) & "-" & Session.SessionID

        If KillCache = True Then
            Page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If Cache(cacheKey) Is Nothing Then
            ds = ws.WSContact.GetCommentsForContact(_contactid, _bizDivID)
        Else
            ds = CType(Page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function


#Region "events"

    ''' <summary>
    ''' Function to handle submit event by user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
       
        If txtComment.Text.Trim <> "" Then
            If ddlCommentReason.SelectedIndex <> 0 Then
                divValidationMain.Visible = False
                lblError.Text = ""
                Dim AttachmentXSD As String = ""
                Dim dsContacts As New XSDContacts
                dsContacts = UCFileUpload8.ReturnFilledAttachments(dsContacts)
                AttachmentXSD = dsContacts.GetXml
                'Remove XML namespace attribute and its value
                AttachmentXSD = AttachmentXSD.Remove(AttachmentXSD.IndexOf("xmlns"), AttachmentXSD.IndexOf(".xsd") - AttachmentXSD.IndexOf("xmlns") + 5)

                Dim success As Boolean = ws.WSContact.AddComments(_contactid, Session("UserId"), txtComment.Text.Trim, AttachmentXSD, CInt(ddlCommentReason.SelectedItem.Value))
                If success Then
                    Cache.Remove("Comments" & "-" & CStr(_contactid) & "-" & Session.SessionID)
                    divValidationMain.Visible = False
                    txtComment.Text = ""
                    PopulateComments()
                    UCFileUpload8.ShowFileUpload()
                Else
                    divValidationMain.Visible = True
                    lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
                End If
                txtComment.Text = ""
                ddlCommentReason.SelectedIndex = 0
            Else
                divValidationMain.Visible = True
                lblError.Text = "Please Select Reason."
            End If

        Else
            divValidationMain.Visible = True
            lblError.Text = "Please Enter Comments."
        End If

    End Sub

#End Region

    Public Function getAttachmentLinkage(ByVal FilePath As String, ByVal Name As String) As String
        Dim returnString As String = ""
        If (FilePath <> "" And Name <> "") Then
            returnString = "<span class='txtGreySmallSelected'><a class='footerTxtSelected' href='" + getlink(FilePath) + "' target='_blank'>" + Name + "</a></span>"
        End If
        Return returnString
    End Function
    Public Function getlink(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachment")
                Dim str As String = filePath.Substring(index)
                'str = ApplicationSettings.AttachmentDisplayPath(4) + str
                str = ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/" + str + CommonFunctions.AzureStorageAccessKey().ToString
                Return str.Replace("\", "/")
            End If
        End If
        Return ""
    End Function

End Class