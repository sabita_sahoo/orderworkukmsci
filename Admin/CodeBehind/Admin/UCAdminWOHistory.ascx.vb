Partial Public Class UCAdminWOHistory
    Inherits System.Web.UI.UserControl
    Public WithEvents lblCustSpecialInstructions As Label

    Shared ws As New WSObjs
    Dim count As Integer = 0
    Dim countForWP As Integer = 0
    Private _bizdivID As Integer
    Dim strCompanyName As String = ""

    Public Property BizdivID() As Integer
        Get
            If Not IsNothing(ViewState("bizdivID")) Then
                Return ViewState("bizdivID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("bizdivID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetNoStore()
        If Not IsPostBack Then
            ViewState("WOID") = Request("WOID")
            ViewState("CompanyID") = 0
            ViewState("SupCompId") = Request("SupCompId")

            If Request("Group") = "Invoiced" Then
                ViewState("Group") = "Closed"
            Else
                ViewState("Group") = Request("Group")
            End If

            If ViewState("Group") = "Conditional Accept" Then
                ViewState("Group") = "CA"
            End If

            ViewState("WorkOrderID") = Request("WorkOrderID")
            If Not IsNothing(Request("BizDivID")) Then
                If Request("BizDivID").ToString <> "" Then
                    BizdivID = CInt(Request("BizDivID"))
                Else
                    BizdivID = CInt(ApplicationSettings.BizDivId)
                End If
            Else
                BizdivID = CInt(ApplicationSettings.BizDivId)
            End If

            Cache.Remove("WOHistory" & Session.SessionID)
            populateData(True)
            SetLabelNames()
            ShowHideColumns()
            ShowHideLinks()
            ShowHidePanels()
        End If
    End Sub

    Public Function getWOHistory(Optional ByVal killcache As Boolean = False)
        If killcache = True Then
            Session.Remove("AdminWOHistory" & Session.SessionID)
        End If
        Dim ds As DataSet
        If Session("AdminWOHistory" & Session.SessionID) Is Nothing Then
            ds = ws.WSWorkOrder.GetAdminWOHistory(ViewState("WOID"), ViewState("CompanyID"))
            Session.Add("AdminWOHistory" & Session.SessionID, ds)
        Else
            ds = CType(Session("AdminWOHistory" & Session.SessionID), DataSet)
        End If
        'ds = ws.WSWorkOrder.GetAdminWOHistory(ViewState("WOID"), ViewState("CompanyID"))
        Return ds
        'If Cache("WOHistory" & Session.SessionID) Is Nothing Then
        '    ds = ws.WSWorkOrder.GetWOHistory(ViewState("WOID"), ViewState("Viewer"), ViewState("CompanyID"))
        '    Cache.Add("WOHistory" & Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        'Else
        '    ds = CType(Cache("WOHistory" & Session.SessionID), DataSet)
        'End If
        'Return ds
    End Function

    Public Sub populateData(Optional ByVal killcache As Boolean = False)
        Dim ds As DataSet = getWOHistory(killcache)
        'Modify the Page view according to the Biz.
        SetPageViewByBiz(ds)

        CommonFunctions.StoreVerNum(ds.Tables("tblSummary"))
        Dim dvVerNo As DataView
        If ds.Tables("tblActionHistory").Rows.Count <> 0 Then
            dvVerNo = ds.Tables("tblActionHistory").Copy.DefaultView
            Session("pTrackingVersionDataView") = dvVerNo
        End If

        'CommonFunctions.StoreVerNum(ds.Tables("tblSummary"))

        hdnValue.Value = ""
        'Populate WO Summary
        If (ds.Tables("tblSummary").Rows.Count <> 0) Then
            'WO No
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("RefWOID")) Then
                lblWONo.Text = ds.Tables("tblSummary").Rows(0).Item("RefWOID")
            End If
            'Created
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateCreated")) Then
                lblCreated.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateCreated"), DateFormat.ShortDate)
            End If
            'Modified
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateModified")) Then
                lblModified.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateModified"), DateFormat.ShortDate)
            End If
            'Location
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Location")) Then
                lblLocation.Text = ds.Tables("tblSummary").Rows(0).Item("Location")
            End If
            'Created By
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("BuyerContact")) Then
                lblBuyerContact1.Text = ds.Tables("tblSummary").Rows(0).Item("BuyerContact")
            End If
            'WO Title
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("WOTitle")) Then
                lblTitle.Text = ds.Tables("tblSummary").Rows(0).Item("WOTitle")
            End If
            'WO Start
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateStart")) Then
                lblWOStart.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
            End If
            'Proposed 
            'If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Value")) Then
            '    lblProposedPrice.Text = FormatCurrency(ds.Tables("tblSummary").Rows(0).Item("Value"), 2, TriState.True, TriState.True, TriState.False)
            'End If
            'Wholesale Price
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice")) Then
                lblWholesalePrice.Text = FormatCurrency(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.True, TriState.False)
                'hdnValue.Value = FormatNumber(ds.Tables("tblSummary").Rows(0).Item("WholesalePrice"), 2)
            End If
            'Platform Price
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("PlatformPrice")) Then
                lblPlatformPrice.Text = FormatCurrency(ds.Tables("tblSummary").Rows(0).Item("PlatformPrice"), 2, TriState.True, TriState.True, TriState.False)
                'hdnValue.Value = FormatNumber(ds.Tables("tblSummary").Rows(0).Item("PlatformPrice"), 2)
            End If

            'UpSell Price
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("UpSellPrice")) Then
                lblUpSellPrice.Text = FormatCurrency(ds.Tables("tblSummary").Rows(0).Item("UpSellPrice"), 2, TriState.True, TriState.True, TriState.False)
                'hdnValue.Value = FormatNumber(ds.Tables("tblSummary").Rows(0).Item("PlatformPrice"), 2)
            End If
            'Status
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Status")) Then
                lblStatus.Text = ds.Tables("tblSummary").Rows(0).Item("Status")
            End If
            'Billing Location
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("BillingLocName")) Then
                lblbillloc.Text = ds.Tables("tblSummary").Rows(0).Item("BillingLocName")

            End If
            'Category
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("WOCategory")) Then
                lblCategory.Text = ds.Tables("tblSummary").Rows(0).Item("WOCategory")
            End If
            'Start Date
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateStart")) Then
                lblStartDate.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
            End If
            'End Date
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateEnd")) Then
                lblEndDate.Text = Strings.FormatDateTime(ds.Tables("tblSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate)
            End If
            'Quantity
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("Quantity")) Then
                lblQuantity.Text = Convert.ToString(ds.Tables("tblSummary").Rows(0).Item("Quantity"))
            End If
            'Quantity
            'lblQuantity.Text = "1"
            'If (trEndDate.Visible <> False) Then
            '    If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateStart")) Then
            '        If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("DateEnd")) Then
            '            Dim dt1 As DateTime = Convert.ToDateTime(lblStartDate.Text)
            '            Dim dt2 As DateTime = Convert.ToDateTime(lblEndDate.Text)
            '            lblQuantity.Text = (DateDiff(DateInterval.Day, dt1, dt2) + 1).ToString
            '        End If
            '    End If

            'End If
            'Estimated Time
            If ApplicationSettings.Country = ApplicationSettings.CountryDE Then
                'Estimated Time
                If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("EstimatedTime")) Then
                    If ds.Tables("tblSummary").Rows(0).Item("EstimatedTime") <> "" Then
                        lblEstimatedTime.Text = ds.Tables("tblSummary").Rows(0).Item("EstimatedTime")
                    Else
                        lblEstimatedTime.Text = ResourceMessageText.GetString("NoneSpecified")
                    End If
                Else
                    lblEstimatedTime.Text = ResourceMessageText.GetString("NoneSpecified")  '    "None Specified"
                End If
            Else
                If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("EstimatedTimeInDays")) Then
                    If (ds.Tables("tblSummary").Rows(0).Item("EstimatedTimeInDays")).ToString <> "" Then
                        lblEstimatedTime.Text = ds.Tables("tblSummary").Rows(0).Item("EstimatedTimeInDays")
                    Else
                        lblEstimatedTime.Text = ResourceMessageText.GetString("NoneSpecified")
                    End If
                Else
                    lblEstimatedTime.Text = ResourceMessageText.GetString("NoneSpecified")  '    "None Specified"
                End If
            End If
            'Supply Parts
            If Not IsDBNull(ds.Tables("tblSummary").Rows(0).Item("SpecialistSuppliesParts")) Then
                If ds.Tables("tblSummary").Rows(0).Item("SpecialistSuppliesParts") = False Then
                    lblSupplyParts.Text = ResourceMessageText.GetString("No")    '   "No"
                Else
                    lblSupplyParts.Text = ResourceMessageText.GetString("Yes")    '   "Yes"
                End If
            End If
        End If

        If (ds.Tables("tblDetails").Rows.Count <> 0) Then
            'Installation Time
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("AptTime")) Then
                If ds.Tables("tblDetails").Rows(0).Item("AptTime") <> "" Then
                    lblInstallTime.Text = " - " & ds.Tables("tblDetails").Rows(0).Item("AptTime")
                Else
                    lblInstallTime.Text = ResourceMessageText.GetString("NoneSpecified")   '   "None Specified"
                End If
            Else
                lblInstallTime.Text = ResourceMessageText.GetString("NoneSpecified")   '   "None Specified"
            End If
            'PO Number
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("PONumber")) Then
                If ds.Tables("tblDetails").Rows(0).Item("PONumber") <> "" Then
                    lblPONumber.Text = ds.Tables("tblDetails").Rows(0).Item("PONumber")
                Else
                    lblPONumber.Text = ResourceMessageText.GetString("NoneSpecified")   '   "None Specified"
                End If
            Else
                lblPONumber.Text = ResourceMessageText.GetString("NoneSpecified")   '   "None Specified"
            End If

            'JRS Number
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("JRSNumber")) Then
                If ds.Tables("tblDetails").Rows(0).Item("JRSNumber") <> "" Then
                    lblJRSNumber.Text = ds.Tables("tblDetails").Rows(0).Item("JRSNumber")
                Else
                    lblJRSNumber.Text = ResourceMessageText.GetString("NoneSpecified")   '   "None Specified"
                End If
            Else
                lblJRSNumber.Text = ResourceMessageText.GetString("NoneSpecified")   '   "None Specified"
            End If
            'Depot Name  - (Location of Goods) (previously Business Division)
            If ds.Tables("tblDetails").Rows(0).Item("DepotName") <> "" Then
                lblDepotName.Text = ds.Tables("tblDetails").Rows(0).Item("DepotName")
            ElseIf ds.Tables("tblDetails").Rows(0).Item("LocationID") = ds.Tables("tblDetails").Rows(0).Item("GoodsLocation") Then
                lblDepotName.Text = "Customer's Home"
            Else
                lblDepotName.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
            End If
            'Depot User
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("DepotUser")) Then
                If ds.Tables("tblDetails").Rows(0).Item("DepotUser") <> "" Then
                    lblGoodsUser.Text = ds.Tables("tblDetails").Rows(0).Item("DepotUser")
                Else
                    lblGoodsUser.Text = ResourceMessageText.GetString("NoneSpecified")
                End If
            Else
                lblGoodsUser.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
            End If
            'Location Address (previously Business Division)
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("BusinessDivision")) Then
                If ds.Tables("tblDetails").Rows(0).Item("BusinessDivision") <> "" Then
                    lblBusinessDivision.Text = ds.Tables("tblDetails").Rows(0).Item("BusinessDivision")
                Else
                    lblBusinessDivision.Text = ResourceMessageText.GetString("NoneSpecified")
                End If
            Else
                lblBusinessDivision.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
                trDepotAddress.Visible = False
                trDepotUser.Visible = False
            End If

            'Equipment Details/Product Purchased - (Previously TV Make/Model) (previously Product name)
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("EquipmentDetails")) Then
                If ds.Tables("tblDetails").Rows(0).Item("EquipmentDetails") <> "" Then
                    lblProducts.Text = ds.Tables("tblDetails").Rows(0).Item("EquipmentDetails")
                Else
                    lblProducts.Text = ResourceMessageText.GetString("NoneSpecified")
                End If
            Else
                lblProducts.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
            End If

            'Additional Products Purchased
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")) Then
                If ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts") <> "" Then
                    If lblProducts.Text <> ResourceMessageText.GetString("NoneSpecified") Then
                        lblProducts.Text = lblProducts.Text + ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")
                    Else
                        lblProducts.Text = ds.Tables("tblDetails").Rows(0).Item("AdditionalProducts")
                    End If
                End If
            End If

            'Dress Code
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("DressCode")) Then
                If ds.Tables("tblDetails").Rows(0).Item("DressCode") <> "" Then
                    lblDressCode.Text = ds.Tables("tblDetails").Rows(0).Item("DressCode")
                Else
                    lblDressCode.Text = ResourceMessageText.GetString("NoneSpecified")   '   "None Specified"
                End If
            Else
                lblDressCode.Text = ResourceMessageText.GetString("NoneSpecified")   '   "None Specified"
            End If
            'Special Instructions
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("SpecialInstructions")) Then
                'lblSpecialInstructions.Text = ds.Tables("tblDetails").Rows(0).Item("SpecialInstructions")
                Dim SpecialInstructions As String = ds.Tables("tblDetails").Rows(0).Item("SpecialInstructions").ToString()
                If (SpecialInstructions <> "") Then
                    SpecialInstructions = Replace(SpecialInstructions, "lt;/br&gt;", "", , 1)
                    'divSpecialInstructions.InnerHtml = Server.HtmlDecode(SpecialInstructions.Replace("<br>", ""))
                    divSpecialInstructions.InnerHtml = Server.HtmlDecode(SpecialInstructions)
                    'lblSpecialInstructions.Text = Server.HtmlDecode(SpecialInstructions.Replace("<br>", ""))
                    lblSpecialInstructions.Text = Server.HtmlDecode(SpecialInstructions)
                End If
            End If

            'Client Special Instructions
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("ClientSpecialInstructions")) Then
                If ds.Tables("tblDetails").Rows(0).Item("ClientSpecialInstructions") <> "" Then
                    'lblCustSpecialInstructions.Text = ds.Tables("tblDetails").Rows(0).Item("ClientSpecialInstructions")
                    Dim ClientSpecialInstructions As String = ds.Tables("tblDetails").Rows(0).Item("ClientSpecialInstructions").ToString()
                    If (ClientSpecialInstructions <> "") Then
                        ClientSpecialInstructions = Replace(ClientSpecialInstructions, "lt;/br&gt;", "", , 1)
                        'divCustSpecialInstructions.InnerHtml = Server.HtmlDecode(ClientSpecialInstructions.Replace("<br>", ""))
                        divCustSpecialInstructions.InnerHtml = Server.HtmlDecode(ClientSpecialInstructions)
                    End If
                Else
                    'lblCustSpecialInstructions.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
                    divCustSpecialInstructions.InnerHtml = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
                End If
            Else
                ' lblCustSpecialInstructions.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
                divCustSpecialInstructions.InnerHtml = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
            End If
            'Sales Agent
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("SalesAgent")) Then
                If ds.Tables("tblDetails").Rows(0).Item("SalesAgent") <> "" Then
                    lblSalesAgent.Text = ds.Tables("tblDetails").Rows(0).Item("SalesAgent")
                Else
                    lblSalesAgent.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
                End If
            Else
                lblSalesAgent.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
            End If
            'Receipt Number
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("ReceiptNumber")) Then
                If ds.Tables("tblDetails").Rows(0).Item("ReceiptNumber") <> "" Then
                    lblReceiptNumber.Text = ds.Tables("tblDetails").Rows(0).Item("ReceiptNumber").ToString
                Else
                    lblReceiptNumber.Text = ResourceMessageText.GetString("NoneSpecified")
                End If
            Else
                lblReceiptNumber.Text = ResourceMessageText.GetString("NoneSpecified") ' "None Specified"
            End If
            'Freesat Make/Model
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("FreesatMake")) Then
                If ds.Tables("tblDetails").Rows(0).Item("FreesatMake") <> "" Then
                    lblFreesatMake.Text = ds.Tables("tblDetails").Rows(0).Item("FreesatMake").ToString
                Else
                    trFreesatMake.Visible = False
                End If
            Else
                trFreesatMake.Visible = False
            End If
            'Long Desc
            If Not IsDBNull(ds.Tables("tblDetails").Rows(0).Item("WOLongDesc")) Then
                Dim WOLongDesc As String = ds.Tables("tblDetails").Rows(0).Item("WOLongDesc").ToString()
                If (WOLongDesc <> "") Then
                    WOLongDesc = Replace(WOLongDesc, "lt;/br&gt;", "", , 1)
                    'divScopeOfWork.InnerHtml = Server.HtmlDecode(WOLongDesc.Replace("<br>", ""))
                    divScopeOfWork.InnerHtml = Server.HtmlDecode(WOLongDesc)
                    'lblLongDesc.Text = ds.Tables("tblDetails").Rows(0).Item("WOLongDesc").ToString.Replace(Chr(13), "<BR>")
                End If
            End If
            'ClientScope
            If ds.Tables("tblDetails").Rows(0).Item("ClientScope").ToString.Trim = "" Then
                pnlClientScope.Visible = False
            Else
                pnlClientScope.Visible = True
                'lblClientScope.Visible = True
                lblTagClientScope.Visible = True

                'lblClientScope.Text = ds.Tables("tblDetails").Rows(0).Item("ClientScope").ToString
                Dim ClientScope As String = ds.Tables("tblDetails").Rows(0).Item("ClientScope").ToString()
                If (ClientScope <> "") Then
                    ClientScope = Replace(ClientScope, "lt;/br&gt;", "", , 1)
                    'divClientScope.InnerHtml = Server.HtmlDecode(ClientScope.Replace("<br>", ""))
                    divClientScope.InnerHtml = Server.HtmlDecode(ClientScope)
                End If
                End If
        End If

        'Buyer Supplier Info
        Dim RowNoBuyer As Integer
        Dim RowNoSupp As Integer
        If (ds.Tables("tblContact").Rows.Count <> 0) Then
            If ds.Tables("tblContact").Rows(0).Item("ContactType") = "Supplier" Then
                RowNoSupp = 0
                RowNoBuyer = 1
                strCompanyName = ds.Tables("tblContact").Rows(RowNoSupp).Item("Company")
            ElseIf ds.Tables("tblContact").Rows(0).Item("ContactType") = "Buyer" Then
                RowNoBuyer = 0
                RowNoSupp = 1
                strCompanyName = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Company")
            End If


            'BUYER INFO
            'Name
            If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Name")) Then
                lblClientContact.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Name")
            End If
            'Buyer Company
            If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Company")) Then
                lblClientCompany.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Company")
                lblBuyerCompany.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Company")
            End If
            'Buyer Address
            If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Address")) Then
                lblClientAddress.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Address")
            End If
            'Buyer Phone  
            If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Phone")) And Not IsDBNull(ds.Tables("tblContact").Rows(RowNoBuyer).Item("Phone")) Then
                lblClientPhone.Text = ds.Tables("tblContact").Rows(RowNoBuyer).Item("Phone")
            End If

            'Temporary locatiom
            If Not IsDBNull(ds.Tables("tblWOLoc").Rows(0).Item("Type")) Then
                If ds.Tables("tblWOLoc").Rows(0).Item("Type") = "Temporary" Then
                    'If Temporary loc assigned to WO - 
                    If Not IsDBNull(ds.Tables("tblWOLoc").Rows(0).Item("Address")) Then
                        divSpecialInstructions.InnerHtml = "WorkOrder Location: " & ds.Tables("tblWOLoc").Rows(0).Item("Address") & "<br>" & lblSpecialInstructions.Text
                    End If
                End If
            End If

            tdBtmRating.Visible = False

            'SUPPLIER INFO
            If ds.Tables("tblContact").Rows.Count > 1 Then
                'Email
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Email")) Then
                    lblEmail.Text = "<b>Email Id: </b>" & ds.Tables("tblContact").Rows(RowNoSupp).Item("Email")
                End If
                'Name
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Name")) Then
                    lblContactName.Text = "<b> Contact Name: </b>" & ds.Tables("tblContact").Rows(RowNoSupp).Item("Name")
                    lblSupplierContactName.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("Name")
                End If
                'Supplier Company
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Company")) Then
                    ViewState("SupplierCompany") = ds.Tables("tblContact").Rows(RowNoSupp).Item("Company")
                    lblSupplierCompanyName.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("Company")
                    'lblSupplierCompany.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("Company")
                End If
                'Supplier Address
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Address")) Then
                    lblSupplierAddress.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("Address")
                End If
                'Supplier Phone  
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Phone")) Then
                    lblPhoneNo.Text = "<b>Phone No: </b>" & ds.Tables("tblContact").Rows(RowNoSupp).Item("Phone")
                    lblSupplierPhone.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("Phone")
                End If
                'Supplier Mobile  
                If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("Mobile")) Then
                    lblMobileNo.Text = "<b>Mobile No: </b>" & ds.Tables("tblContact").Rows(RowNoSupp).Item("Mobile")
                End If

                'tdTopRating.Visible = False
                'this is to show the rating only if webconfig key set to 1 i.e. OW and SF, for DE showrating = 0
                If ApplicationSettings.ShowRating = 1 Then
                    If Not IsNothing(tdTopRating) Then
                        tdTopRating.Visible = True
                    End If

                    'Total Rating
                    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("TotalRating")) Then
                        lblRatingSupplier.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("RatingScore").ToString().Replace(".00", "") & "%"
                    End If
                    'Positive Rating
                    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("PositiveRating")) Then
                        lblPosRatingSupplier.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("PositiveRating")
                    End If
                    'Negative Rating
                    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("NegativeRating")) Then
                        lblNegRatingSupplier.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("NegativeRating")
                    End If
                    'Neutral Rating  lblSupplierPhone
                    If Not IsDBNull(ds.Tables("tblContact").Rows(RowNoSupp).Item("NeutralRating")) Then
                        lblNeuRatingSupplier.Text = ds.Tables("tblContact").Rows(RowNoSupp).Item("NeutralRating")
                    End If
                Else
                    If Not IsNothing(tdTopRating) Then
                        tdTopRating.Visible = False
                    End If
                End If
            Else
                ViewState("showSupplierInfo") = "False"
            End If
        End If

        'Last Action
        '******************************************************************************
        If ViewState("Group") = ApplicationSettings.WOGroupAccepted Or ViewState("Group") = "ActiveElapsed" Or ViewState("Group") = ApplicationSettings.WOGroupCA Or ViewState("Group") = ApplicationSettings.WOGroupIssue Or ViewState("Group") = ApplicationSettings.WOGroupCompleted Or ViewState("Group") = ApplicationSettings.WOGroupClosed Or ViewState("Group") = ApplicationSettings.WOGroupCancelled Or ViewState("Group") = ApplicationSettings.WOGroupSubmitted Or ViewState("Group") = ApplicationSettings.WOGroupSent Then
            If Not IsNothing(ds.Tables("tblActionHistory")) Then
                If ds.Tables("tblActionHistory").Rows.Count > 0 Then
                    Dim dv As DataView
                    dv = ds.Tables("tblActionHistory").DefaultView
                    'Changed by PratikT
                    dv.Sort = "WOTrackingId DESC"
                    DLSupplierResponse.DataSource = dv
                    DLSupplierResponse.DataBind()
                    'Added By Pankaj Malav on 27 Feb 2009 checking whether the status of WorkOrder is Submitted or CA then show undiscard icon else hide it.
                    'If ViewState("Group") = ApplicationSettings.WOGroupCA Or ViewState("Group") = ApplicationSettings.WOGroupSent Then
                    '    CType(DLSupplierResponse.FindControl("tdUndiscard"), System.Web.UI.HtmlControls.HtmlTableCell).Visible = True
                    'Else
                    '    CType(DLSupplierResponse.FindControl("tdUndiscard"), System.Web.UI.HtmlControls.HtmlTableCell).Visible = False
                    'End If
                End If
            End If
            'If ds.Tables("AdditionalHistory").Rows.Count > 0 Then
            '    DLAddtionalHistory.DataSource = ds.Tables("AdditionalHistory")
            '    DLAddtionalHistory.DataBind()
            'End If



        End If


        'Attachments
        '******************************************************************************
        If Not Page.IsPostBack Then
            Dim flagPnlAttachment As Boolean = False
            If Not IsNothing(ds.Tables("tblAttachments")) Then
                If ds.Tables("tblAttachments").Rows.Count > 0 Then
                    Dim dv As DataView
                    dv = ds.Tables("tblAttachments").DefaultView
                    dv.RowFilter = "LinkSource in ('WorkOrder','WOComplete')"
                    If dv.Count > 0 Then
                        litAttachments.Text = ResourceMessageText.GetString("WOAttachments")   '   "<strong>WO Attachments:</strong><br>"
                    End If
                    flagPnlAttachment = True
                    Dim drv As DataRowView
                    For Each drv In dv
                        ' Server.UrlEncode("path")
                        litAttachments.Text &= "<span class='txtGreySmallSelected'><a class='footerTxtSelected' href='" + getlink(drv("FilePath")) + "' target='_blank'>" + drv("Name") + "</a><BR></span>"
                    Next

                    '************
                    dv.RowFilter = ""
                    dv.RowFilter = "LinkSource = 'StatementOfWork'"
                    If dv.Count > 0 Then
                        litAttachments.Text &= "<br>"
                        litAttachments.Text &= ResourceMessageText.GetString("SWAttachments")
                        For Each drv In dv
                            litAttachments.Text &= "<span class='txtGreySmallSelected'><a class='footerTxtSelected' href='" + getlink(drv("FilePath")) + "' target='_blank'>" + drv("Name") + "</a><br></span>"
                        Next
                    End If

                    '*************
                End If
            End If
            'Supplier attachments
            'If CStr(ViewState("Group")).ToLower = "completed" Or CStr(ViewState("Group")).ToLower = "closed" Or CStr(ViewState("Group")).ToLower = "cancelled" Then
            If Not IsNothing(ds.Tables("tblSupplierAttachments")) Then
                If ds.Tables("tblSupplierAttachments").Rows.Count > 0 Then
                    Dim dv As DataView
                    dv = ds.Tables("tblSupplierAttachments").DefaultView
                    litAttachments.Text &= ResourceMessageText.GetString("SupplierAttachments")   '   "<br/><strong>Supplier Attachments:</strong><br/>"
                    flagPnlAttachment = True
                    Dim drv As DataRowView
                    For Each drv In dv
                        litAttachments.Text &= "<span class='txtGreySmallSelected'><a class='footerTxtSelected' href='" + getlink(drv("FilePath")) + "' target='_blank'>" + drv("Name") + "</a><BR></span>"
                    Next
                End If
            End If
            'End If
            If flagPnlAttachment = False Then
                pnlAttachments.Visible = False
            Else
                pnlAttachments.Visible = True
            End If

            If Not IsNothing(ViewState("Group")) Then
                Dim dvQA As DataView = ds.Tables("tblQA").DefaultView
                dvQA.RowFilter = "Type in ('WOSubmitQuestion')"
                If dvQA.Count > 0 Then
                    pnlQAWOSubmit.Visible = True
                    rpQAWOSubmit.DataSource = dvQA
                    rpQAWOSubmit.DataBind()
                End If
                dvQA.RowFilter = ""
            End If

        End If
        '******************************************************************************
    End Sub

    Public Sub ShowHideColumns()
        Dim strGroup As String = ViewState("Group")
        tdBuyerContact.Visible = False
        tdBuyerContact1.Visible = False
        tdSupplierContact.Visible = False
        tdSupplierContact1.Visible = False

        If (strGroup = ApplicationSettings.WOGroupDraft) Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Created") '   "Created"
            lblHdrModified.Text = ResourceMessageText.GetString("Submitted") '   "Submitted"
        End If

        'If strGroup = ApplicationSettings.WOGroupDraft Or strGroup = ApplicationSettings.WOGroupSubmitted Or strGroup = ApplicationSettings.WOGroupCA Or strGroup = ApplicationSettings.WOGroupInTray Or strGroup = ApplicationSettings.WOGroupLost Then
        '    lblHdrProPrice.Text = ResourceMessageText.GetString("ProposedPrice") '   "Proposed Price"
        'Else
        '    lblHdrProPrice.Text = ResourceMessageText.GetString("Price") '   "Price"
        'End If
        lblHdrWholesalePrice.Text = "WP"
        lblHdrPlatformPrice.Text = "PP"
        lblHdrUpSellPrice.Text = "UP"
        If strGroup = ApplicationSettings.WOGroupSubmitted Or strGroup = ApplicationSettings.WOGroupSent Or strGroup = ApplicationSettings.WOGroupCA Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Submitted") '   "Submitted"
        End If
        If strGroup = ApplicationSettings.WOGroupCancelled Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Submitted") '   "Submitted"
            lblHdrModified.Text = ResourceMessageText.GetString("Cancelled") '   "Cancelled"
        End If
        If strGroup = ApplicationSettings.WOGroupSubmitted Or strGroup = ApplicationSettings.WOGroupSubmitted Then
            lblHdrModified.Text = ResourceMessageText.GetString("LastSent") '   "Last Sent"
        End If
        If strGroup = ApplicationSettings.WOGroupAccepted Or strGroup = "ActiveElapsed" Or strGroup = ApplicationSettings.WOGroupCompleted Or strGroup = ApplicationSettings.WOGroupClosed Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Accepted") '   "Accepted"
        End If
        If strGroup = ApplicationSettings.WOGroupAccepted Or strGroup = "ActiveElapsed" Or strGroup = ApplicationSettings.WOGroupIssue Or strGroup = ApplicationSettings.WOGroupCA Then
            lblHdrModified.Text = ResourceMessageText.GetString("LastResponse") '   "Last Response"
        End If
        If strGroup = ApplicationSettings.WOGroupIssue Then
            lblHdrCreated.Text = ResourceMessageText.GetString("IssueRaised") '   "Issue Raised"
        End If
        If strGroup = ApplicationSettings.WOGroupCompleted Then
            lblHdrModified.Text = ResourceMessageText.GetString("Completed") '   "Completed"
        End If
        If strGroup = ApplicationSettings.WOGroupClosed Then
            lblHdrModified.Text = ResourceMessageText.GetString("Closed") '   "Closed"
        End If
        If strGroup = ApplicationSettings.WOGroupInTray Or strGroup = ApplicationSettings.WOGroupLost Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Received") '   "Received"
        End If
        If strGroup = ApplicationSettings.WOGroupLost Then
            lblHdrModified.Text = ResourceMessageText.GetString("Lost") '   "Lost"
        End If
        If strGroup = ApplicationSettings.WOGroupDraft Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Created") '   "Created"
            lblHdrModified.Text = ResourceMessageText.GetString("Modified") '   "Modified"
        End If
        If IsNothing(strGroup) Or strGroup = "" Then
            lblHdrCreated.Text = ResourceMessageText.GetString("Created")  '   "Created"
            lblHdrModified.Text = ResourceMessageText.GetString("Modified") '   "Modified"
        End If
    End Sub

    Public Sub ShowHideLinks()
        Dim strGroup As String = ViewState("Group")
        Dim appendLink As String = ""
        appendLink &= "WOID=" & ViewState("WOID")
        appendLink &= "&WorkOrderID=" & ViewState("WorkOrderID")
        appendLink &= "&ContactID=" & Request("ContactID")
        appendLink &= "&CompanyID=" & Request("CompanyID")
        appendLink &= "&SupContactId=" & Request("SupContactId")
        appendLink &= "&SupCompId=" & Request("SupCompId")
        appendLink &= "&Group=" & ViewState("Group")
        appendLink &= "&PS=" & Request("PS")
        appendLink &= "&PN=" & Request("PN")
        appendLink &= "&SC=" & Request("SC")
        appendLink &= "&SO=" & Request("SO")
        appendLink &= "&CompID=" & Request("CompID")
        If Not IsNothing(Request("BizDivID")) Then
            If Request("BizDivID") <> "" Then
                appendLink &= "&BizDivID=" & Request("BizDivID")
            Else
                appendLink &= "&BizDivID=" & ApplicationSettings.OWUKBizDivId
            End If
        Else
            appendLink &= "&BizDivID=" & ApplicationSettings.BizDivId
        End If
        lnkDetail.HRef = "~/AdminWODetails.aspx?" & "&sender=" & "UCWOsDetails&" & appendLink

        'RateWO Icon
        If strGroup = ApplicationSettings.WOGroupAccepted Or strGroup = "ActiveElapsed" Or strGroup = ApplicationSettings.WOGroupCompleted Or strGroup = ApplicationSettings.WOGroupClosed Or strGroup = ApplicationSettings.WOGroupIssue Then
            lnkbtnUpdateRating.Visible = True
        End If

        'If Request("sender") = "SearchWO" Then
        '    lnkDetail.HRef = "~/SearchWOs.aspx?" & appendLink & "&sender=SearchWO" & "&SrcWorkorderID=" & Request("txtWorkorderID") & "&SrcCompanyName=" & Request("CompanyName") & "&SrcKeyword=" & Request("KeyWord") & "&SrcPONumber=" & Request("PONumber") & "&SrcPostCode=" & Request("PostCode")
        'End If
    End Sub

    Public Sub ShowHidePanels()
        'Sections To Show/Hide 
        'Summary = DGWorkOrders
        'Action Details = pnlSupplierResponse
        'Supplier Info = pnlSupplierContactInfo
        'WO Details (Title,Desc,Spl Ins) = divRatings
        'WO Specs(Type, Dates, Est Time, Supply Parts) = divRatings
        'WO Specs(PO No, Job No, Cust No) = tblWOSpecs
        'Location = = tblWOLocation
        'Dress Code = 
        'Attachments = tblWOAttachments
        'Buyer Info = pnlClientContactInfo
        Dim Group As String = ViewState("Group")

        'Show / Hide Summary
        'Needs to be visible for all statuses

        'Show / Hide Action Details. By default pnlSupplierResponse is Visible = false
        If (Group = ApplicationSettings.WOGroupInTray) Then
            pnlSupplierResponse.Visible = False
        End If

        'Show / Hide Supplier Info
        If (Group = ApplicationSettings.WOGroupDraft Or Group = ApplicationSettings.WOGroupSubmitted Or Group = ApplicationSettings.WOGroupSent Or Group = ApplicationSettings.WOGroupCA) Then
            pnlSupplierContactInfo.Visible = False
        End If
        If Not IsNothing(ViewState("showSupplierInfo")) Then
            If ViewState("showSupplierInfo") = "False" Then
                pnlSupplierContactInfo.Visible = False
            End If
        End If

        'Show / Hide tblWOSpecs (PO No, Job No, Cust No)
        'If ((Group = ApplicationSettings.WOGroupInTray Or Group = ApplicationSettings.WOGroupLost) Or (Group = ApplicationSettings.WOGroupCA And Viewer = ApplicationSettings.ViewerSupplier)) Then
        '    tblWOSpecs.Visible = False
        '    tblWOAttachments.Visible = False
        'End If

        'Show / Hide tblWOLocation

        'Show / Hide tblWOAttachments
        'Handled In Show / Hide tblWOSpecs (PO No, Job No, Cust No) Above


        'Show / Hide Buyer Info
        If (Group = ApplicationSettings.WOGroupInTray Or Group = ApplicationSettings.WOGroupLost) Then
            pnlClientContactInfo.Visible = False
        End If

        'If Viewer = ApplicationSettings.ViewerSupplier And (Group = ApplicationSettings.WOGroupSubmitted Or Group = ApplicationSettings.WOGroupCA) Then
        '    pnlSpecialInstructions.Visible = False
        'End If

    End Sub

    Public Function getlink(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachment")
                Dim str As String = filePath.Substring(index)
                'str = ApplicationSettings.AttachmentDisplayPath(4) + str
                str = ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/" + str + CommonFunctions.AzureStorageAccessKey().ToString
                Return str.Replace("\", "/")
            End If
        End If
        Return ""
    End Function

    Public Function getDiscussions(ByVal WOTrackingId As Integer) As DataView
        Dim ds As DataSet = getWOHistory()
        Dim dv As DataView = ds.Tables("tblComments").DefaultView
        dv.RowFilter = "WOTrackingId = " & WOTrackingId
        Return dv
    End Function

    Public Function ShowHideComments(ByVal WOTrackingId As Integer) As Boolean
        Dim ds As DataSet = getWOHistory()
        Dim dv As DataView = ds.Tables("tblComments").DefaultView
        dv.RowFilter = "WOTrackingId = " & WOTrackingId & " and Comments <> ''"
        If dv.Count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function SetLabelNames()
        lblBuyerContactLabel.Text = ResourceMessageText.GetString("BuyerInfo") '   "Buyer Info"
        lblSupplierContactLabel.Text = ResourceMessageText.GetString("SupplierInfo") '   "Supplier Info"
        Return 0
    End Function

    Public Function GetContactName()
        Return ""
    End Function
    Public Function GetChangedData(ByVal WOTrackingId As Integer, ByVal Status As String)
        Dim ds As DataSet = getWOHistory()
        Dim dv As DataView = ds.Tables("AdditionalHistory").DefaultView
        If (Status = "Issue") Then
            dv.RowFilter = "WOTrackingId = " & WOTrackingId & " AND sourcetable = 'tblWOTracking'"
        Else
            dv.RowFilter = "WOTrackingId = " & WOTrackingId
        End If

        Return dv


    End Function
    Private Sub DLSupplierResponse_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLSupplierResponse.ItemCreated
        If ViewState("Group") = ApplicationSettings.WOGroupCA Or ViewState("Group") = ApplicationSettings.WOGroupSent Then
            If e.Item.ItemType = ListItemType.Header Then
                e.Item.FindControl("tdLastClosed").Visible = True
                e.Item.FindControl("tdNoOfJobs").Visible = True
                e.Item.FindControl("tdDateApproved").Visible = True
            End If
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.FindControl("tdLastClosedValue").Visible = True
                e.Item.FindControl("tdNoOfJobsValue").Visible = True
                e.Item.FindControl("tdDateApprovedValue").Visible = True
            End If
        End If
    End Sub

    'Private Sub DLSupplierResponse_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLSupplierResponse.ItemCreated
    '    If e.Item.ItemType = ListItemType.Header Then
    '        e.Item.FindControl("tdDLCompany").Visible = True
    '    End If
    '    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '        e.Item.FindControl("tdDLCompany1").Visible = True
    '    End If
    '    If e.Item.ItemType = ListItemType.Header Then

    '        e.Item.FindControl("tdDLContact").Visible = False
    '        e.Item.FindControl("tdDLPhone").Visible = False
    '        e.Item.FindControl("tdSupplyParts").Visible = False
    '        e.Item.FindControl("tdDLSpecialist").Visible = True
    '        'e.Item.FindControl("tdActionHdr").Visible = False
    '    End If
    '    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

    '        e.Item.FindControl("tdDLContact1").Visible = False
    '        e.Item.FindControl("tdDLPhone1").Visible = False
    '        e.Item.FindControl("tdSupplyParts1").Visible = False
    '        e.Item.FindControl("tdDLSpecialist1").Visible = True
    '        'e.Item.FindControl("tdAction").Visible = False
    '    End If

    '    'If (ViewState("Viewer") = ApplicationSettings.ViewerBuyer And (ViewState("Group") <> ApplicationSettings.WOGroupDraft And ViewState("Group") <> ApplicationSettings.WOGroupSubmitted Or ViewState("Group") = ApplicationSettings.WOGroupCA)) Then
    '    '    If e.Item.ItemType = ListItemType.Header Then
    '    '        e.Item.FindControl("tdDLSpecialist").Visible = True
    '    '    End If
    '    '    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '    '        e.Item.FindControl("tdDLSpecialist1").Visible = True
    '    '    End If
    '    'End If
    'End Sub

#Region "SortingAdminWOHistory"
    Private Sub DLSupplierResponse_ItemCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles DLSupplierResponse.ItemCommand
        'For Sorting the Datalist
        If e.CommandName.ToString = "Sort" Then
            Dim SortParameter As String
            SortParameter = e.CommandArgument.ToString
            Dim ds As New DataSet
            Dim dv As New DataView
            ds = getWOHistory()
            dv = ds.Tables("tblActionHistory").DefaultView
            If Not IsNothing(ViewState("SortParameter")) Then
                If ViewState("SortParameter") = SortParameter & " ASC" Then
                    dv.Sort = SortParameter & " DESC"
                    ViewState("SortParameter") = SortParameter & " DESC"
                ElseIf ViewState("SortParameter") = SortParameter & " DESC" Then
                    dv.Sort = SortParameter & " ASC"
                    ViewState("SortParameter") = SortParameter & " ASC"
                Else
                    dv.Sort = SortParameter & " ASC"
                    ViewState("SortParameter") = SortParameter & " ASC"
                End If
            Else
                dv.Sort = SortParameter & " ASC"
                ViewState.Add("SortParameter", SortParameter & " ASC")
            End If

            DLSupplierResponse.DataSource = dv
            DLSupplierResponse.DataBind()

            'For Undiscarding the workorder
        ElseIf e.CommandName.ToString = "UnDiscard" Then
            Dim Param() As String
            Dim WOTrackingId As String
            Dim CompanyId As String
            Param = e.CommandArgument.ToString.Split(",")
            WOTrackingId = CType(Param(0), Integer)
            CompanyId = CType(Param(1), Integer)
            Dim ds As New DataSet
            Dim dv As New DataView
            ds = ws.WSWorkOrder.WO_AdminWODiscard(ViewState("WOID"), CompanyId, WOTrackingId)
            Session.Add("AdminWOHistory" & Session.SessionID, ds)
            'If workorder is in active state show validation message that the work order is now active
            If ds.Tables("tblSuccess").Rows(0)("SUCCESS") = 0 Then
                lblUnDiscardMessage.Text = "The work order cannot be un-discarded as it is now active"
            Else
                'Send Mail to Supplier
                Dim dvEmail As DataView
                dvEmail = ds.Tables("tblSupplierEmail").DefaultView
                Emails.SendWOUnDiscardMail(dvEmail, ViewState("WorkOrderID"))
            End If
            dv = ds.Tables("tblActionHistory").DefaultView
            'Bind the datalist again showing latest History
            DLSupplierResponse.DataSource = dv
            DLSupplierResponse.DataBind()

        ElseIf e.CommandName.ToString = "CreateNewWO" Then
            Dim dv As New DataView
            Dim Param() As String
            Dim pContactId As String
            Dim pCompanyId As String

            Dim pNewWorkOrderId As String
            Dim pWOTitle As String
            Dim pWOCategory As String
            Dim pDate As DateTime
            Dim pLocation As String
            Dim pPrice As Decimal
            Dim pStartDate As DateTime
            Dim pEndDate As DateTime
            Dim pComment As String

            Param = e.CommandArgument.ToString.Split(",")
            pContactId = CType(Param(0), Integer)
            pCompanyId = CType(Param(1), Integer)

            Dim TrackCompanyID As Integer = Session("CompanyId")
            Dim TrackContactID As Integer = Session("UserId")
            Dim TrackContactClassID As Integer = Session("RoleGroupID")

            Dim ds_Success As New DataSet
            ds_Success = ws.WSWorkOrder.WO_CreateWorkOrderForCAWO(ViewState("WOID"), Request("BizDivID"), pCompanyId, pContactId, TrackCompanyID, TrackContactID, TrackContactClassID, Session("UserID"))

            If ds_Success.Tables("tblWOID").Rows.Count > 0 Then
                Dim dv_NewWorkOrderId As New DataView(ds_Success.Tables("tblWOID"))
                pNewWorkOrderId = dv_NewWorkOrderId.Item(0).Item("RefWOID").ToString
                pWOTitle = dv_NewWorkOrderId.Item(0).Item("WOTitle").ToString
                pWOCategory = dv_NewWorkOrderId.Item(0).Item("WOCategory").ToString
                pLocation = dv_NewWorkOrderId.Item(0).Item("Location").ToString
                pStartDate = dv_NewWorkOrderId.Item(0).Item("DateStart").ToString
                pEndDate = dv_NewWorkOrderId.Item(0).Item("DateEnd").ToString
                pDate = IIf(Not IsDBNull(dv_NewWorkOrderId.Item(0).Item("DateAccepted")), dv_NewWorkOrderId.Item(0).Item("DateAccepted"), Nothing)
                pPrice = IIf(Not IsDBNull(dv_NewWorkOrderId.Item(0).Item("PlatformPrice")), dv_NewWorkOrderId.Item(0).Item("PlatformPrice"), 0.0)
            End If

            If ds_Success.Tables("tblDiscardMail").Rows.Count > 0 Then
                Dim dv_Email As New DataView(ds_Success.Tables("tblDiscardMail"))
                Emails.SendDiscardCreateNewWOMail(ViewState("WorkOrderID"), dv_Email, pNewWorkOrderId, pWOTitle, pWOCategory, pLocation, pStartDate, pEndDate, pDate, pPrice)
            End If
            'End If

            ''Bind the datalist again showing latest History
            populateData(True)

            'For Discarding the workorder
        ElseIf e.CommandName = "Discard" Then
            Dim dv As New DataView
            Dim Param() As String
            Dim WOTrackingId As String
            Dim pCompanyId As String
            Param = e.CommandArgument.ToString.Split(",")
            WOTrackingId = CType(Param(0), Integer)
            pCompanyId = CType(Param(1), Integer)

            Dim ds_Success As New DataSet
            Dim TrackCompanyID As Integer = Session("CompanyId")
            Dim TrackContactID As Integer = Session("UserId")
            Dim TrackContactClassID As Integer = Session("RoleGroupID")
            Dim WOStatusText As String = ""

            If Not IsNothing(pCompanyId) Then
                Session("pTrackingVersionDataView").RowFilter = "CompanyID=" & pCompanyId
            End If
            OrderWorkLibrary.CommonFunctions.StoreWOTrackingVerNum(Session("pTrackingVersionDataView"))

            ds_Success = ws.WSWorkOrder.WO_DiscardDelCopy(ViewState("WOID"), Request("BizDivID"), pCompanyId, Request("ContactID"), "discardAdmin", TrackCompanyID, TrackContactID, TrackContactClassID, CommonFunctions.FetchVerNum(), Session("UserID"), CommonFunctions.FetchWOTrackingVerNum)

            If ds_Success.Tables.Count = 0 Then
                divValidationMain.Visible = True
                lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
            ElseIf ds_Success.Tables("tblWOID").Rows(0).Item("Status") = -11 Then
                divValidationMain.Visible = True

                Select Case ds_Success.Tables("tblWOID").Rows(0).Item("WOTrackingStatus")
                    Case OrderWorkLibrary.ApplicationSettings.WOStatusID.Sent, OrderWorkLibrary.ApplicationSettings.WOStatusID.Submitted, OrderWorkLibrary.ApplicationSettings.WOStatusID.Draft, OrderWorkLibrary.ApplicationSettings.WOStatusID.InTray
                        'lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOVNoDetailsModified")
                        WOStatusText = "InTray"
                    Case OrderWorkLibrary.ApplicationSettings.WOStatusID.Cancelled
                        'lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOVNoWOCancelled")
                        WOStatusText = "Cancelled"
                    Case OrderWorkLibrary.ApplicationSettings.WOStatusID.CA
                        'lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOVNoWOCA")
                        WOStatusText = "CA"
                    Case OrderWorkLibrary.ApplicationSettings.WOStatusID.Accepted
                        'lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOVNoWOAccepted")
                        WOStatusText = "Accepted"
                    Case OrderWorkLibrary.ApplicationSettings.WOStatusID.Lost
                        'lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOVNoWOLost")
                        WOStatusText = "Lost"
                End Select
                lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOVersionControlMsg").Replace("<Link>", "SupplierWODetails.aspx?WOID=" & OrderWorkLibrary.Encryption.Encrypt(ViewState("WOID")) & "&WorkOrderID=" & OrderWorkLibrary.Encryption.Encrypt(ViewState("WorkOrderID")) & "&SupContactId=" & Request("SupContactId") & "&SupCompId=" & Request("SupCompId") & "&Group=" & WOStatusText & "&CompID=" & OrderWorkLibrary.Encryption.Encrypt(Request("CompID")) & "&FromDate=&ToDate=&PS=" & Request("PS") & "&PN=0&SC=DateCreated&SO=1&Viewer=" & ViewState("Viewer") & "&sender=UCWOsListing")
                'Case when the version number of the WOTracking has changed
            ElseIf ds_Success.Tables("tblWOID").Rows(0).Item("Status") = -10 Then
                divValidationMain.Visible = True

                Select Case ds_Success.Tables("tblWOID").Rows(0).Item("WOTrackingStatus")
                    Case OrderWorkLibrary.ApplicationSettings.WOStatusID.Discarded
                        'lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOVNoWODiscarded")
                        WOStatusText = "Lost"
                    Case OrderWorkLibrary.ApplicationSettings.WOStatusID.CA
                        'lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOVNoWOCA")
                        WOStatusText = "CA"
                    Case OrderWorkLibrary.ApplicationSettings.WOStatusID.Lost
                        'lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOVNoWOLost")
                        WOStatusText = "Lost"
                        'Case Else
                        '    lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOVNoException")
                End Select
                lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("WOVersionControlMsg").Replace("<Link>", "SupplierWODetails.aspx?WOID=" & OrderWorkLibrary.Encryption.Encrypt(ViewState("WOID")) & "&WorkOrderID=" & OrderWorkLibrary.Encryption.Encrypt(ViewState("WorkOrderID")) & "&SupContactId=" & Request("SupContactId") & "&SupCompId=" & Request("SupCompId") & "&Group=" & WOStatusText & "&CompID=" & OrderWorkLibrary.Encryption.Encrypt(Request("CompID")) & "&FromDate=&ToDate=&PS=" & Request("PS") & "&PN=0&SC=DateCreated&SO=1&Viewer=" & ViewState("Viewer") & "&sender=UCWOsListing")
            Else
                'Send mail
                'Added By Pankaj Malav on 08 Jan 2009 so as to cater for the scenario when multiple suppliers are processing at the same time.
                If ds_Success.Tables("tblDiscardMail").Rows.Count > 0 Then
                    Dim dv_Email As New DataView(ds_Success.Tables("tblDiscardMail"))
                    Emails.SendDiscardWOMail(ViewState("WorkOrderID"), dv_Email)
                End If
            End If

            ''Bind the datalist again showing latest History
            populateData(True)

        End If

    End Sub

    Private Sub SetPageViewByBiz(ByVal ds As DataSet)
        ViewState("BusinessArea") = ds.Tables("tblDetails").Rows(0).Item("BusinessArea")
        If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
            lblStartDateLabel.Text = "Appointment"
            trEndDate.Visible = False
            trSalesAgent.Visible = True
            trJobNumber.Visible = True
            lblInstallTime.Visible = True
            trFreesatMake.Visible = True

            lblDepotNameLabel.Text = "Location of Goods"
            lblProductsLabel.Text = "Product Purchased"
        ElseIf ViewState("BusinessArea") = ApplicationSettings.BusinessAreaIT Then
            lblStartDateLabel.Text = "Start Date"
            trEndDate.Visible = True
            trSalesAgent.Visible = False
            trJobNumber.Visible = False
            lblInstallTime.Visible = False
            trFreesatMake.Visible = False

            lblDepotNameLabel.Text = "Location of Equipment"
            lblProductsLabel.Text = "Equipment Details"
        Else
            lblStartDateLabel.Text = "Start Date"
            trEndDate.Visible = True
            trSalesAgent.Visible = False
            trJobNumber.Visible = True
            lblInstallTime.Visible = False
            trFreesatMake.Visible = False

            lblDepotNameLabel.Text = "Location of Goods"
            lblProductsLabel.Text = "Product Purchased"
        End If
    End Sub
#End Region

    Public Sub hdnBtnRateWO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnBtnRateWO.Click

        Dim Rating As Integer
        If rdBtnNegative.Checked = True Then
            Rating = -1
        ElseIf rdBtnPositive.Checked = True Then
            Rating = 1
        Else
            Rating = 0
        End If
        Dim Success As DataSet = CommonFunctions.SaveRatingComment(ViewState("WOID"), Session("CompanyID"), Session("UserID"), txtComment.Text.Trim, Rating)
        txtComment.Text = ""
    End Sub


    Private Sub lnkbtnUpdateRating_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnUpdateRating.ServerClick
        mdlUpdateRating.Show()
        rdBtnNegative.Checked = False
        rdBtnNeutral.Checked = False
        rdBtnPositive.Checked = False
        txtComment.Text = ""

        Dim ds As DataSet
        ds = CommonFunctions.GetWORating(CInt(ViewState("WOID")))
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
                    Dim Result As Integer = ds.Tables(0).Rows(0)(0)
                    Select Case Result
                        Case -1
                            rdBtnNegative.Checked = True
                        Case 0
                            rdBtnNeutral.Checked = True
                        Case 1
                            rdBtnPositive.Checked = True
                    End Select
                End If
                txtComment.Text = ds.Tables("tblRating").Rows(0).Item("Comments")
            End If
        End If
    End Sub

    Public Function GetLinks(ByVal parambizDivId As Object, ByVal contactId As Object, ByVal SupplierCompanyName As String) As String
        Dim link As String = ""
        Dim contactType As ApplicationSettings.ContactType = ApplicationSettings.ContactType.approvedSupplier
        Dim classId As String
        Dim statusId As String
        classId = ApplicationSettings.RoleSupplierID
        statusId = ApplicationSettings.StatusApprovedCompany

        Dim bizDivId = parambizDivId

        link &= "<a class='footerTxtSelected ' href='companyprofile.aspx?sender=AdminWOHistory&bizDivId=" & bizDivId & "&contactType=" & contactType & "&WOID=" & ViewState("WOID") & "&WorkorderID=" & ViewState("WorkOrderID") & "&companyid=" & IIf(Request("SupCompId") = 0, Request("companyid"), Request("SupCompId")) & "&SupCompId=" & Request("SupCompId") & "&userid=" & contactId & "&classid=" & classId & "&group=" & Request("Group") & "&statusId=" & statusId & "&mode=" & Request("mode") & "&ps=25" & "&pn=" & "&sc=" & "&so=" & "&CompID=" & ViewState("CompanyID") & "&FromDate=''" & "&ToDate=''" & "'>" & SupplierCompanyName & "</a>"

        Return link

    End Function

    Public Function GetLinksGrid(ByVal parambizDivId As Object, ByVal companyId As Object, ByVal contactId As Object, ByVal SupplierCompanyName As String, ByVal ContactClassId As String) As String
        Dim link As String = ""
        'Dim classId As String
        Dim statusId As String
        'classId = ApplicationSettings.RoleSupplierID
        statusId = ApplicationSettings.StatusApprovedCompany

        Dim bizDivId = parambizDivId
        Dim companyPageName As String = ""
        If ContactClassId = ApplicationSettings.RoleSupplierID Then
            companyPageName = "CompanyProfile.aspx"
        Else
            companyPageName = "CompanyProfileBuyer.aspx"
        End If

        If Not SupplierCompanyName.ToLower.Contains("orderwork ltd.") Then
            link &= "<a class='footerTxtSelected ' href='" & companyPageName & "?sender=AdminWOHistory&bizDivId=" & bizDivId & "&WOID=" & ViewState("WOID") & "&WorkorderID=" & ViewState("WorkOrderID") & "&companyid=" & companyId & "&SupCompId=" & Request("SupCompId") & "&userid=" & Request("ContactID") & "&classid=" & ContactClassId & "&group=" & Request("Group") & "&statusId=" & statusId & "&mode=" & Request("mode") & "&ps=25" & "&pn=" & "&sc=" & "&so=" & "&CompID=" & companyId & "&FromDate=''" & "&ToDate=''" & "'>" & SupplierCompanyName & "</a>"
        Else
            link = SupplierCompanyName
        End If
        Return link

    End Function

End Class