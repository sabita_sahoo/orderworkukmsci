Partial Public Class UCAutoMatchSettings
    Inherits System.Web.UI.UserControl

    ''' <summary>
    ''' Property to set or get company value for UCCompanyProfile
    ''' </summary>
    ''' <remarks></remarks>



    Private _companyID As Integer
    Public Property CompanyID() As Integer
        Get
            Return _companyID
        End Get
        Set(ByVal value As Integer)
            _companyID = value
        End Set
    End Property

    Private _strBackToListing As String = ""
    Public Property strBackToListing() As String
        Get
            Return _strBackToListing
        End Get
        Set(ByVal value As String)
            _strBackToListing = value
        End Set
    End Property



    ''' <summary>
    ''' To handle Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsNothing(Request("CompanyID")) Then
            If Request("CompanyID") <> "" Then
                CompanyID = Request("CompanyID")
            End If
        End If
        If Not IsPostBack Then
            PopulateAutoMatchSettings()
        End If
        lnkBackToListing.HRef = getBackToListingLink()
    End Sub

    ''' <summary>
    ''' To populate the setting for AutoMatch from DB
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub PopulateAutoMatchSettings()
        Dim ds As New DataSet
        'Populate Distances
        CommonFunctions.PopulateDistance(Page, ddlNearestTo)

        ds = CommonFunctions.PopulateAutoMatchSettings(CompanyID)


        If ds.Tables("tblSettings").Rows.Count <> 0 Then
            chkAutoMatch.Checked = ds.Tables("tblSettings").Rows(0)("AutoMatchStatus")
            chkRefCheck.Checked = ds.Tables("tblSettings").Rows(0)("ReferenceCheck")
            chkSecurity.Checked = ds.Tables("tblSettings").Rows(0)("SecurityCheck")
            chkCRB.Checked = ds.Tables("tblSettings").Rows(0)("CRBCheck")
            chkbxEngCRBCheck.Checked = ds.Tables("tblSettings").Rows(0)("EngineerCRBCheck")
            chkbxEngCSCSCheck.Checked = ds.Tables("tblSettings").Rows(0)("EngineerCSCSCheck")
            chkbxEngRightToWork.Checked = ds.Tables("tblSettings").Rows(0)("EngineerRightToWorkInUK")
            chkbxEngUKSecurityCheck.Checked = ds.Tables("tblSettings").Rows(0)("EngineerUKSecurityCheck")
            If ds.Tables("tblSettings").Rows(0)("NearestDistance") <> 0 Then
                ddlNearestTo.SelectedValue = ds.Tables("tblSettings").Rows(0)("NearestDistance")
            Else
                ddlNearestTo.SelectedIndex = 0
            End If
            'Store the version number in session for Contacts
            CommonFunctions.StoreVerNum(ds.Tables("tblSettings"))
        Else
            chkAutoMatch.Checked = False
            chkRefCheck.Checked = False
            chkSecurity.Checked = False
            chkCRB.Checked = False
            ddlNearestTo.SelectedIndex = 0
            chkbxEngCRBCheck.Checked = False
            chkbxEngCSCSCheck.Checked = False
            chkbxEngRightToWork.Checked = False
            chkbxEngUKSecurityCheck.Checked = False
        End If
        If chkAutoMatch.Checked = True Then
            pnlSettings.Visible = True
            chkRefCheck.Disabled = False
            chkSecurity.Disabled = False
            chkCRB.Disabled = False
            ddlNearestTo.Enabled = True
            chkbxEngCRBCheck.Disabled = False
            chkbxEngCSCSCheck.Disabled = False
            chkbxEngRightToWork.Disabled = False
            chkbxEngUKSecurityCheck.Disabled = False
        Else
            pnlSettings.Visible = True
            chkRefCheck.Disabled = True
            chkSecurity.Disabled = True
            chkCRB.Disabled = True
            ddlNearestTo.Enabled = False
            chkbxEngCRBCheck.Disabled = True
            chkbxEngCSCSCheck.Disabled = True
            chkbxEngRightToWork.Disabled = True
            chkbxEngUKSecurityCheck.Disabled = True
        End If
    End Sub

    ''' <summary>
    ''' To handle save profile action triggerd by the Save LinkButton at the top
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub SaveTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click, btnSaveBottom.Click
        Dim AutoMatchStatus As Boolean = chkAutoMatch.Checked
        Dim ReferenceCheck As Boolean = chkRefCheck.Checked
        Dim SecurityCheck As Boolean = chkSecurity.Checked
        Dim CRBCheck As Boolean = chkCRB.Checked
        Dim NearestDistance As String = ddlNearestTo.SelectedValue
        Dim success As Integer = CommonFunctions.SaveAutoMatchSettings(CompanyID, ReferenceCheck, SecurityCheck, CRBCheck, NearestDistance, AutoMatchStatus, CommonFunctions.FetchVerNum(), Session("UserID"), chkbxEngCRBCheck.Checked, chkbxEngCSCSCheck.Checked, chkbxEngUKSecurityCheck.Checked, chkbxEngRightToWork.Checked)
        'Success = 1 => Successful
        'Success = 2 => Fail - No Fav. Suppliers Available
        'Sucesss = else => Fail - DB Update Fail
        If success = 1 Then
            Response.Redirect(getBackToListingLink())
        ElseIf success = 2 Then
            pnlSuccess.Visible = True
            lblMsg.Text = "<span style='color:#FF0000'><b>Warning: Please ensure that there is at least one supplier assigned to this account.</b></span>"
        ElseIf success = -10 Then
            pnlSuccess.Visible = True
            lblMsg.Text = "<span style='color:#FF0000'><b>" & ResourceMessageText.GetString("AccountsVersionControlMsg").Replace("<Link>", "AutoMatchSettings.aspx?CompanyID=" & CompanyID & "&sender=company" & "&bizDivId=" & Request("bizDivId")) & "</b></span>"
        End If
    End Sub


    ''' <summary>
    ''' To handle reset profile action triggerd by the Save LinkButton at the top
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ResetTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetTop.Click, btnResetBottom.Click
        ResetProfile()
    End Sub


    ''' <summary>
    ''' Procedure to undo changes to company profile
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetProfile()
        pnlSuccess.Visible = False
        PopulateAutoMatchSettings()
    End Sub

    ''' <summary>
    ''' To create the Back To Company Profile Link
    ''' </summary>
    ''' <returns>link</returns>
    ''' <remarks></remarks>
    Public Function getBackToListingLink() As String
        Dim link As String = ""
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        If ((Request("sender") = "buyer") Or (Request("classid") = ApplicationSettings.RoleClientID)) Then
            link = "~\CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        Else
            link = "~\CompanyProfile.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        End If
        Return link
    End Function

End Class