<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" Codebehind="AT800DailyReport.aspx.vb" Inherits="Admin.AT800DailyReport" title="At800 Daily Report"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        //For todays date;
        Date.prototype.today = function () {
            return ((this.getDate() < 10) ? "0" : "") + this.getDate() + "/" + (((this.getMonth() + 1) < 10) ? "0" : "") + (this.getMonth() + 1) + "/" + this.getFullYear()
        };
        function getDate() {
            var newDate = new Date();
            document.write(newDate.today());
        }
    </script>
    <div style="width:1020px;padding:20px;margin:0px auto;">
        <div style="margin:auto;text-align:center;font-size:small; font-family:Arial;">Report Data as at: <asp:DropDownList ID="ddlDate" runat="server" OnSelectedIndexChanged="ddlDate_SelectedIndexChanged" AutoPostBack="true" /></div>
        <br style="clear:both;"/>
        <div style="margin:auto;text-align:center;">LAST UPDATE FOR CLOSED JOBS UP TO: 29th October 3PM</div>
        <br style="clear:both;"/>
        <div style="width:550px;float:left;">
            <table width="550" border="1" cellpadding="3" cellspacing="0">
                <tr style="background-color:#CCCCCC;font-weight:bold;"><td colspan="3" align="center">Progress Summary</td></tr>
                <tr style="background-color:#CCCCCC;font-weight:bold;">
                    <td width="400"></td>
                    <td width="70" align="center">Daily</td>
                    <td width="70" align="center">Cumulative</td>
                </tr>
                <tr>
                    <td>Total Jobs Submitted</td> 
                    <td align="center"><asp:Label ID="lblTotalJobsSubmittedDaily" runat="server" /></td>
                    <td align="center"><asp:Label ID="lblTotalJobsSubmittedCumulative" runat="server" /></td>
                </tr>
                <tr>
                    <td>Jobs "Live"</td> 
                    <td align="center"><asp:Label ID="lblJobsLiveDaily" runat="server" /></td>
                    <td align="center"><asp:Label ID="lblJobsLiveCumulative" runat="server" /></td>
                </tr>
                <tr>
                    <td>Jobs Closed</td> 
                    <td align="center"><asp:Label ID="lblJobsClosedDaily" runat="server" /></td>
                    <td align="center"><asp:Label ID="lblJobsClosedCumulative" runat="server" /></td>
                </tr>
                <tr>
                    <td>Jobs Cancelled</td> 
                    <td align="center"><asp:Label ID="lblJobsCancelledDaily" runat="server" /></td>
                    <td align="center"><asp:Label ID="lblJobsCancelledCumulative" runat="server" /></td>
                </tr>
            </table>
            <br style="clear:both;"/>
            <table width="550" border="1" cellpadding="3" cellspacing="0">
                <tr style="background-color:#CCCCCC;font-weight:bold;">
                    <td width="400">Cancelled Jobs</td>
                    <td width="70" align="center">Daily</td>
                    <td width="70" align="center">Cumulative</td>
                </tr>
                <tr>
                    <td>By viewer</td> 
                    <td align="center"><asp:Label ID="lblCancelledJobsViewerDaily" runat="server" /></td>
                    <td align="center"><asp:Label ID="lblCancelledJobsViewerCumulative" runat="server" /></td>
                </tr>
                <tr>
                    <td>By viewer within 2 hours</td> 
                    <td align="center"><asp:Label ID="lblCancelledJobsViewer2HoursDaily" runat="server" /></td>
                    <td align="center"><asp:Label ID="lblCancelledJobsViewer2HoursCumulative" runat="server" /></td>
                </tr>
                <tr>
                    <td>By installer</td> 
                    <td align="center"><asp:Label ID="lblCancelledJobsInstallerDaily" runat="server" /></td>
                    <td align="center"><asp:Label ID="lblCancelledJobsInstallerCumulative" runat="server" /></td>
                </tr>
            </table>
            <br style="clear:both;"/>
            <table width="550" border="1" cellpadding="3" cellspacing="0">
                <tr style="background-color:#CCCCCC;font-weight:bold;">
                    <td width="400">Completion Reason - Closed Jobs</td>
                    <td width="70" align="center">Daily</td>
                    <td width="70" align="center">Cumulative</td>
                </tr>
                    <asp:Repeater ID="rptCompletionReasonClosedJobs" runat="server" OnItemDataBound="rptCompletionReasonClosedJobs_ItemDataBound">
                        <ItemTemplate>
                            <asp:HiddenField ID="hidKey" runat="server" Value='<%# Eval("key") %>' />
                            <tr>
                                <td><%#Eval("value")%></td>
                                <td align="center"><asp:Label ID="lblCompletionReasonsDaily" runat="server" /></td>
                                <td align="center"><asp:Label ID="lblCompletionReasonsCumulative" runat="server" /></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                <tr>
                    <td>Filter fitted resolved</td>
                    <td align="center">0</td>
                    <td align="center">21</td>
                </tr>
                <tr>
                    <td>Filter fitted no interference</td>
                    <td align="center">0</td>
                    <td align="center">146</td>
                </tr>
                <tr>
                    <td>Not 4G interference</td>
                    <td align="center">0</td>
                    <td align="center">12</td>
                </tr>
                <tr>
                    <td>Platform change reqd</td>
                    <td align="center">0</td>
                    <td align="center">0</td>
                </tr>
                <tr>
                    <td>Bespoke change reqd</td>
                    <td align="center">0</td>
                    <td align="center">0</td>
                </tr>
            </table>
            <br style="clear:both;"/>
            <table width="550" border="1" cellpadding="3" cellspacing="0">
                <tr style="background-color:#CCCCCC;font-weight:bold;">
                    <td width="400">Job Types - Closed Jobs</td>
                    <td width="70" align="center">Daily</td>
                    <td width="70" align="center">Cumulative</td>
                </tr>
                    <asp:Repeater ID="rptJobTypesClosedJobs" runat="server">
                        <ItemTemplate>
                            <asp:HiddenField ID="hidKey" runat="server" Value='<%# Eval("key") %>' />
                            <tr>
                                <td><%#Eval("value")%></td>
                                <td><asp:Label ID="lblJobTypesDaily" runat="server" /></td>
                                <td><asp:Label ID="lblJobTypesCumulative" runat="server" /></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
            </table>
        </div>
        <div style="margin-left:5px;width:440px;float:right;">
            <table width="440" border="1" cellpadding="3" cellspacing="0">
                <tr style="background-color:#CCCCCC;font-weight:bold:">
                    <td width="300">Completion Reason - First visit - Filter fitted - resolved 4G interference</td>
                    <td width="70" align="center">Daily</td>
                    <td width="70" align="center">Cumulative</td>
                </tr>
                    <asp:Repeater ID="rptCompletionReasonFirstVisit" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td><%#Eval("Name")%></td>
                                <td><%#Eval("Daily")%></td>
                                <td><%#Eval("Cumulative")%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
            </table>
        </div>
    </div>
</asp:Content>

