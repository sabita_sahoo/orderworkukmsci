﻿Public Class AccreditationTagsListing
    Inherits System.Web.UI.Page
    ''' <summary>
    ''' Shared wsobject to access web service webmethods
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)

            ViewState!SortExpression = ""
            If Trim(Request("SC")) <> "" Then
                ViewState!SortExpression = Trim(Request("SC"))
            Else
                ViewState!SortExpression = "DateCreated"
            End If
            Dim sd As SortDirection
            sd = SortDirection.Descending
            If Trim(Request("SO")) <> "" Then
                If Trim(Request("SO")).ToLower = "true" Then
                    sd = SortDirection.Descending
                End If
            End If

            gvAccreditationTags.PageSize = hdnPageSize.Value
            gvAccreditationTags.Sort(ViewState!SortExpression, sd)
            PopulateGrid()
            gvAccreditationTags.PageIndex = 0
        End If

        lblMsg.Text = ""
    End Sub

    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvAccreditationTags.DataBind()
        End If

    End Sub


#Region "Grid Functions"

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        Dim rowCount As Integer
        Dim IsActive As Integer
        If (chkIsActive.Checked = True) Then
            IsActive = 1
        Else
            IsActive = 0
        End If
        Dim IsLocked As Integer
        If (chkIsLocked.Checked = True) Then
            IsLocked = 1
        Else
            IsLocked = 0
        End If
        ds = ws.WSContact.GetAccreditationTagsListing(IsActive, IsLocked, sortExpression, startRowIndex, maximumRows, rowCount, txtTagName.Text.Trim, ddlTagFor.SelectedValue, txtDateCreatedFrom.Text.Trim, txtDateCreatedTo.Text.Trim)
        ViewState("rowCount") = rowCount
        Return ds
    End Function

    ''' <summary>
    ''' ObjectDataSource Controls select Count Method which returns the row count for the listing 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvAccreditationTags.RowDataBound
        MakeGridViewHeaderClickable(gvAccreditationTags, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvAccreditationTags.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvAccreditationTags, e.Row, Me)
        End If
    End Sub

    Private Sub gvAccreditationTags_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAccreditationTags.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
                'Dim chkSelectAll As CheckBox = CType(pagerRowTop.FindControl("chkSelectAll"), CheckBox)
                'chkSelectAll.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

                'bottom pager buttons
                'Dim chkSelectAll As CheckBox = CType(pagerRowBottom.FindControl("chkSelectAll"), CheckBox)
                'chkSelectAll.Visible = False
            End If
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)



        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        gvAccreditationTags.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

#End Region

#Region "Actions"

    ''' <summary>
    ''' populates the list of Tags
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnView.Click
        Page.Validate()
        If Page.IsValid Then
            lblMsg.Text = ""
            PopulateGrid()
            gvAccreditationTags.PageIndex = 0

        End If
    End Sub

#End Region

#Region "Status Update"

    Public Function getSelectedTagIds() As String
        Dim TagIds As String = ""
        For Each row As GridViewRow In gvAccreditationTags.Rows
            Dim chkBox As HtmlInputCheckBox = CType(row.FindControl("Check"), HtmlInputCheckBox)
            If chkBox.Checked Then
                If TagIds = "" Then
                    TagIds = CType(row.FindControl("hdnTagId"), HtmlInputHidden).Value
                Else
                    TagIds += "," & CType(row.FindControl("hdnTagId"), HtmlInputHidden).Value
                End If
            End If
        Next
        Return TagIds
    End Function

#End Region



    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
    Public Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click
        Dim dsTagName As DataSet
        dsTagName = ws.WSContact.CheckUniqueness(txtTagNamePopup.Text.Trim, "TagName", ViewState("SelectedTagId"))
        If (dsTagName.Tables(0).Rows.Count > 0) Then
            divMsgPopup.Visible = True
            lblMsgPopup.Text = "Tag Name already exists"
            mdlTagInfo.Show()
            Exit Sub
        Else
            Dim ds As DataSet
            Dim IsActive As Integer
            If (chkIsActivePopup.Checked = True) Then
                IsActive = 1
            Else
                IsActive = 0
            End If
            Dim IsLocked As Integer
            If (chkIsLockedPopup.Checked = True) Then
                IsLocked = 1
            Else
                IsLocked = 0
            End If

            ds = ws.WSContact.AddUpdateAccreditationTags(IsActive, IsLocked, ViewState("SelectedTagId"), ddTagForPopup.SelectedValue, txtTagNamePopup.Text.Trim, Session("UserId"))
            If ds.Tables.Count <> 0 Then
                If ds.Tables(0).Rows(0).Item("Success") <> 0 Then
                    ' For populating the Textbox with the original contents of the mail
                    lblMsg.Text = "Changes Saved/Updated Successfully"
                    divMsgPopup.Visible = False
                    lblMsgPopup.Text = ""
                    ddTagForPopup.SelectedValue = 0
                    txtTagNamePopup.Text = ""
                    mdlTagInfo.Hide()
                    PopulateGrid()
                End If
            End If
        End If
    End Sub

    Public Sub rngDateCreated_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rngDateCreated.ServerValidate
        If Page.IsValid Then
            If txtDateCreatedFrom.Text.Trim <> "" And txtDateCreatedTo.Text.Trim <> "" Then
                Dim validDate As Boolean = True
                Try
                    Dim tmpFromDate As DateTime = DateTime.Parse(txtDateCreatedFrom.Text.Trim)
                    Dim tmpToDate As DateTime = DateTime.Parse(txtDateCreatedTo.Text.Trim)
                Catch ex As Exception
                    validDate = False
                End Try
                If validDate = True Then
                    Dim DateStart As New Date
                    Dim DateEnd As New Date
                    DateStart = txtDateCreatedFrom.Text.Trim
                    DateEnd = txtDateCreatedTo.Text.Trim
                    If DateStart <= DateEnd Then
                        args.IsValid = True
                    Else
                        args.IsValid = False
                    End If
                Else
                    args.IsValid = False
                End If
            End If
        End If
    End Sub

    Public Sub AddNewTag(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState.Add("SelectedTagId", 0)
        divMsgPopup.Visible = False
        lblMsgPopup.Text = ""
        ddTagForPopup.SelectedValue = 0
        txtTagNamePopup.Text = ""
        mdlTagInfo.Show()
    End Sub
    Private Sub gvAccreditationTags_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccreditationTags.RowCommand
        If (e.CommandName = "EditTag") Then
            divMsgPopup.Visible = False
            lblMsgPopup.Text = ""
            ddTagForPopup.SelectedValue = 0
            txtTagNamePopup.Text = ""
            Dim arrList As ArrayList = New ArrayList
            Dim str As String = e.CommandArgument.ToString
            arrList.AddRange(str.Split("#"))
            If (arrList(3) = True) Then
                chkIsActivePopup.Checked = True
            Else
                chkIsActivePopup.Checked = False
            End If
            If (arrList(4) = True) Then
                chkIsLockedPopup.Checked = True
            Else
                chkIsLockedPopup.Checked = False
            End If
            ddTagForPopup.SelectedValue = arrList(2)
            txtTagNamePopup.Text = arrList(1)
            Dim TagId As String
            TagId = arrList(0)
            ViewState.Add("SelectedTagId", TagId)
            mdlTagInfo.Show()

        End If
    End Sub

    'Public Sub gvAccreditationTags_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        Dim dv As DataRowView = CType(e.Row.DataItem, DataRowView)
    '        Dim rblApprovalStatus As RadioButtonList = e.Row.FindControl("rblApprovalStatus")
    '        rblApprovalStatus.DataSource = ws.WSStandards.GetStandards("ApprovalStatus")
    '        rblApprovalStatus.DataBind()
    '        rblApprovalStatus.SelectedValue = dv.Item("ApprovalStatus")
    '    End If
    'End Sub

    'Public Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
    '    For Each gridViewRow As GridViewRow In gvAccreditationTags.Rows
    '        If gridViewRow.RowType = DataControlRowType.DataRow Then
    '            Dim rblApprovalStatus As RadioButtonList = gridViewRow.FindControl("rblApprovalStatus")
    '            Dim hidTagLinkageId As HiddenField = gridViewRow.FindControl("hidTagLinkageId")
    '            ws.WSWorkOrder.UpdateTagLinkage(Integer.Parse(hidTagLinkageId.Value), Integer.Parse(rblApprovalStatus.SelectedValue), DateTime.Now, Integer.Parse(Session("UserID").ToString()))
    '        End If
    '    Next
    'End Sub

    'Public Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim btnDelete As Button = CType(sender, Button)
    '    ws.WSWorkOrder.DeleteTagLinkage(Integer.Parse(btnDelete.CommandArgument))
    '    gvAccreditationTags.DataBind()
    'End Sub

End Class