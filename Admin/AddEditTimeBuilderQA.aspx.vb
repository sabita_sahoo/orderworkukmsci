﻿Public Class AddEditTimeBuilderQA
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            Session("TimeBuilderQ") = Nothing

            If Not IsNothing(Request.QueryString("ProductID")) Then
                ViewState.Add("ProductID", Request.QueryString("ProductID"))
                PopulateData()
            End If
            lnkBackToService.HRef = getBackToServiceLink()
        End If
    End Sub
    Public Sub PopulateData()
        divValidationTBQuestions.Visible = False
        lblValidationTBQuestions.Text = ""
        Dim ds As New DataSet
        ds = ws.WSWorkOrder.populateWOFormProductDetails(CInt(ViewState("ProductID")), CInt(Request("companyid")), 1, CInt(HttpContext.Current.Session("UserID")), "TimeBuilderQuestions", "")
        If ds.Tables.Count > 0 Then
            Session("TimeBuilderQ") = ds
            If ds.Tables(0).Rows.Count > 0 Then
                PopulateTimeBuilderQ()
            End If
        End If
    End Sub
    Public Function getBackToServiceLink() As String
        Dim link As String = ""
        link = "~\AdminProduct.aspx?mode=edit&productid=" & ViewState("ProductID") & "&pagename=CompanyProfile&CompanyID=" & Request("companyid") & "&bizDivId=1"
        Return link
    End Function
    Protected Sub PopulateTimeBuilderQ()
        Dim dsTimeBuilderQ As DataSet
        dsTimeBuilderQ = CType(Session("TimeBuilderQ"), DataSet)

        Dim dvTimeBuilderQ As New DataView
        dvTimeBuilderQ = dsTimeBuilderQ.Tables(0).DefaultView
        dvTimeBuilderQ.RowFilter = "QuestionType = 'Tab1'"
        If (dvTimeBuilderQ.ToTable.Rows.Count > 0) Then
            txtTab1Name.Text = dvTimeBuilderQ.ToTable.Rows(0).Item("TabName")
            rptTimeBuilderTab1Q.Visible = True
            rptTimeBuilderTab1Q.DataSource = dvTimeBuilderQ.ToTable.DefaultView
            rptTimeBuilderTab1Q.DataBind()
        Else
            rptTimeBuilderTab1Q.Visible = False
            rptTimeBuilderTab1Q.DataSource = Nothing
            rptTimeBuilderTab1Q.DataBind()
            txtTab1Name.Text = ""
        End If

        dvTimeBuilderQ.RowFilter = ""
        dvTimeBuilderQ.RowFilter = "QuestionType = 'Tab2'"

        If (dvTimeBuilderQ.ToTable.Rows.Count > 0) Then
            txtTab2Name.Text = dvTimeBuilderQ.ToTable.Rows(0).Item("TabName")
            rptTimeBuilderTab2Q.Visible = True
            rptTimeBuilderTab2Q.DataSource = dvTimeBuilderQ.ToTable.DefaultView
            rptTimeBuilderTab2Q.DataBind()
        Else
            rptTimeBuilderTab2Q.Visible = False
            rptTimeBuilderTab2Q.DataSource = Nothing
            rptTimeBuilderTab2Q.DataBind()
            txtTab2Name.Text = ""
        End If

        dvTimeBuilderQ.RowFilter = ""
        dvTimeBuilderQ.RowFilter = "QuestionType = 'Tab3'"

        If (dvTimeBuilderQ.ToTable.Rows.Count > 0) Then
            txtTab3Name.Text = dvTimeBuilderQ.ToTable.Rows(0).Item("TabName")
            rptTimeBuilderTab3Q.Visible = True
            rptTimeBuilderTab3Q.DataSource = dvTimeBuilderQ.ToTable.DefaultView
            rptTimeBuilderTab3Q.DataBind()
        Else
            rptTimeBuilderTab3Q.Visible = False
            rptTimeBuilderTab3Q.DataSource = Nothing
            rptTimeBuilderTab3Q.DataBind()
            txtTab3Name.Text = ""
        End If

        dvTimeBuilderQ.RowFilter = ""
        dvTimeBuilderQ.RowFilter = "QuestionType = 'Tab4'"

        If (dvTimeBuilderQ.ToTable.Rows.Count > 0) Then
            txtTab4Name.Text = dvTimeBuilderQ.ToTable.Rows(0).Item("TabName")
            rptTimeBuilderTab4Q.Visible = True
            rptTimeBuilderTab4Q.DataSource = dvTimeBuilderQ.ToTable.DefaultView
            rptTimeBuilderTab4Q.DataBind()
        Else
            rptTimeBuilderTab4Q.Visible = False
            rptTimeBuilderTab4Q.DataSource = Nothing
            rptTimeBuilderTab4Q.DataBind()
            txtTab4Name.Text = ""
        End If

        dvTimeBuilderQ.RowFilter = ""
        dvTimeBuilderQ.RowFilter = "QuestionType = 'Tab5'"

        If (dvTimeBuilderQ.ToTable.Rows.Count > 0) Then
            txtTab5Name.Text = dvTimeBuilderQ.ToTable.Rows(0).Item("TabName")
            rptTimeBuilderTab5Q.Visible = True
            rptTimeBuilderTab5Q.DataSource = dvTimeBuilderQ.ToTable.DefaultView
            rptTimeBuilderTab5Q.DataBind()
        Else
            rptTimeBuilderTab5Q.Visible = False
            rptTimeBuilderTab5Q.DataSource = Nothing
            rptTimeBuilderTab5Q.DataBind()
            txtTab5Name.Text = ""
        End If

        dvTimeBuilderQ.RowFilter = ""
        dvTimeBuilderQ.RowFilter = "QuestionType = 'Tab6'"

        If (dvTimeBuilderQ.ToTable.Rows.Count > 0) Then
            txtTab6Name.Text = dvTimeBuilderQ.ToTable.Rows(0).Item("TabName")
            rptTimeBuilderTab6Q.Visible = True
            rptTimeBuilderTab6Q.DataSource = dvTimeBuilderQ.ToTable.DefaultView
            rptTimeBuilderTab6Q.DataBind()
        Else
            rptTimeBuilderTab6Q.Visible = False
            rptTimeBuilderTab6Q.DataSource = Nothing
            rptTimeBuilderTab6Q.DataBind()
            txtTab6Name.Text = ""
        End If


    End Sub

    Private Sub rptTimeBuilderQ_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTimeBuilderTab1Q.ItemDataBound, rptTimeBuilderTab2Q.ItemDataBound, rptTimeBuilderTab3Q.ItemDataBound, rptTimeBuilderTab4Q.ItemDataBound, rptTimeBuilderTab5Q.ItemDataBound, rptTimeBuilderTab6Q.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptTimeBuilderAnswers As Repeater = CType(e.Item.FindControl("rptTimeBuilderQuestionAnswers"), Repeater)
            Dim hdnQuestionId As HtmlInputHidden = CType(e.Item.FindControl("hdnQuestionId"), HtmlInputHidden)
            Dim hdnSequence As HtmlInputHidden = CType(e.Item.FindControl("hdnSequence"), HtmlInputHidden)
            Dim hdnQuestionType As HtmlInputHidden = CType(e.Item.FindControl("hdnQuestionType"), HtmlInputHidden)
            Dim hdnBaseQuestionSequence As HtmlInputHidden = CType(e.Item.FindControl("hdnBaseQuestionSequence"), HtmlInputHidden)
            Dim hdnBaseQuestionSequenceTab As HtmlInputHidden = CType(e.Item.FindControl("hdnBaseQuestionSequenceTab"), HtmlInputHidden)

            Dim ddQAnsCount As DropDownList = CType(e.Item.FindControl("ddQAnsCount"), DropDownList)


            Dim ds As New DataSet
            ds = CType(Session("TimeBuilderQ"), DataSet)
            Dim dv As New DataView
            If ds.Tables.Count > 1 Then
                If ds.Tables(1).Rows.Count > 0 Then
                    dv = ds.Tables(1).DefaultView
                    dv.RowFilter = "QuestionId = " & hdnQuestionId.Value & " AND QuestionType = '" & hdnQuestionType.Value & "'"
                    rptTimeBuilderAnswers.DataSource = dv.ToTable.DefaultView
                    rptTimeBuilderAnswers.DataBind()
                End If
            End If
            ddQAnsCount.SelectedValue = dv.Item(0)("QuestionAnsCount")

            Dim drpdwnQDependent As DropDownList = CType(e.Item.FindControl("drpdwnQDependent"), DropDownList)
            Dim drpdwnQDependentTab As DropDownList = CType(e.Item.FindControl("drpdwnQDependentTab"), DropDownList)
            populateQDependentDD(drpdwnQDependent, drpdwnQDependentTab, CInt(hdnBaseQuestionSequence.Value), hdnBaseQuestionSequenceTab.Value.ToString, CInt(hdnSequence.Value))

        End If
    End Sub
    Public Sub populateQDependentDD(ByVal drpdwnQDependent As DropDownList, ByVal drpdwnQDependentTab As DropDownList, ByVal SelectedQDependent As Integer, ByVal SelectedQDependentTab As String, ByVal Sequence As Integer)
        Dim dsTimeBuilderQ As DataSet
        Dim QuestionType As String

        dsTimeBuilderQ = CType(Session("TimeBuilderQ"), DataSet)

        dsTimeBuilderQ.Tables(0).DefaultView.Sort = "Sequence"

        QuestionType = dsTimeBuilderQ.Tables(0).Rows(0).Item("QuestionType")

        Dim dvTimeBuilderQ As New DataView
        dvTimeBuilderQ = dsTimeBuilderQ.Tables(0).DefaultView
        dvTimeBuilderQ.RowFilter = "QuestionType = '" & QuestionType & "'"
        If (dvTimeBuilderQ.ToTable.Rows.Count > 0) Then
            drpdwnQDependent.Visible = True
            Dim liQDependent As New ListItem
            liQDependent = New ListItem
            liQDependent.Text = "Select"
            liQDependent.Value = 0
            drpdwnQDependent.DataSource = dvTimeBuilderQ.ToTable.DefaultView
            drpdwnQDependent.DataTextField = "Sequence"
            drpdwnQDependent.DataValueField = "Sequence"
            drpdwnQDependent.DataBind()
            drpdwnQDependent.Items.Insert(0, liQDependent)

            'Dim liQDependentToRemove As New ListItem
            'liQDependentToRemove = New ListItem
            'liQDependentToRemove.Value = QuestionId

            'drpdwnQDependent.Items.Remove(drpdwnQDependent.Items.FindByValue(Sequence))

        Else
            drpdwnQDependent.Visible = False
        End If
        dvTimeBuilderQ.RowFilter = ""

        Dim vListItem As ListItem = drpdwnQDependent.Items.FindByValue(SelectedQDependent)

        If Not vListItem Is Nothing Then
            drpdwnQDependent.SelectedValue = SelectedQDependent
        End If

        Dim vListItemTab As ListItem = drpdwnQDependentTab.Items.FindByValue(SelectedQDependentTab)

        If Not vListItemTab Is Nothing Then
            drpdwnQDependentTab.SelectedValue = SelectedQDependentTab
        End If


    End Sub
    Private Sub rptTimeBuilderQ_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTimeBuilderTab1Q.ItemCommand, rptTimeBuilderTab2Q.ItemCommand, rptTimeBuilderTab3Q.ItemCommand, rptTimeBuilderTab4Q.ItemCommand, rptTimeBuilderTab5Q.ItemCommand, rptTimeBuilderTab6Q.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "RemoveQuestion") Then
                Dim hdnQuestionId As HtmlInputHidden = CType(e.Item.FindControl("hdnQuestionId"), HtmlInputHidden)
                Dim hdnQuestionType As HtmlInputHidden = CType(e.Item.FindControl("hdnQuestionType"), HtmlInputHidden)

                UpdateTimeBuilderQA(rptTimeBuilderTab1Q)
                UpdateTimeBuilderQA(rptTimeBuilderTab2Q)
                UpdateTimeBuilderQA(rptTimeBuilderTab3Q)
                UpdateTimeBuilderQA(rptTimeBuilderTab4Q)
                UpdateTimeBuilderQA(rptTimeBuilderTab5Q)
                UpdateTimeBuilderQA(rptTimeBuilderTab6Q)

                RemoveQuestionAndAnswer(hdnQuestionId.Value.ToString, hdnQuestionType.Value.ToString)
            ElseIf (e.CommandName = "MoveUp") Then 'Move Up
                Dim Sequence As Integer = CInt(e.CommandArgument)
                If (Sequence <> 0) Then
                    Dim hdnQuestionType As HtmlInputHidden = CType(e.Item.FindControl("hdnQuestionType"), HtmlInputHidden)
                    MoveUpDownQuestion(Sequence, hdnQuestionType.Value, "Up")
                End If
            ElseIf (e.CommandName = "MoveDown") Then 'Move Down
                Dim Sequence As Integer = CInt(e.CommandArgument)
                If (Sequence <> 0) Then
                    Dim hdnQuestionType As HtmlInputHidden = CType(e.Item.FindControl("hdnQuestionType"), HtmlInputHidden)
                    MoveUpDownQuestion(Sequence, hdnQuestionType.Value, "Down")
                End If
            End If
        End If
    End Sub
    Public Sub MoveUpDownQuestion(ByVal Sequence As String, ByVal QuestionType As String, ByVal Mode As String)
        Dim ds As New DataSet
        ds = CType(Session("TimeBuilderQ"), DataSet)

        For Each drow As DataRow In ds.Tables(0).Rows
            If (drow.Item("QuestionType") = QuestionType) Then
                If drow.Item("Sequence") = Sequence Then
                    If (Mode = "Up") Then
                        drow.Item("RowNum") = drow.Item("RowNum") - 1
                        drow.Item("Sequence") = drow.Item("Sequence") - 1
                    ElseIf (Mode = "Down") Then
                        drow.Item("RowNum") = drow.Item("RowNum") + 1
                        drow.Item("Sequence") = drow.Item("Sequence") + 1
                    End If
                ElseIf drow.Item("Sequence") = Sequence - 1 Then
                    If (Mode = "Up") Then
                        drow.Item("RowNum") = drow.Item("RowNum") + 1
                        drow.Item("Sequence") = drow.Item("Sequence") + 1
                    End If
                ElseIf drow.Item("Sequence") = Sequence + 1 Then
                    If (Mode = "Down") Then
                        drow.Item("RowNum") = drow.Item("RowNum") - 1
                        drow.Item("Sequence") = drow.Item("Sequence") - 1
                    End If
                End If
            End If
        Next

        ds.Tables(0).DefaultView.Sort = "Sequence"

        ds.Tables(0).AcceptChanges()
        Session("TimeBuilderQ") = ds

        PopulateTimeBuilderQ()
        ShowHidePanel(QuestionType)
    End Sub


    Public Sub RemoveQuestionAndAnswer(ByVal QuestionId As String, ByVal QuestionType As String)
        Dim ds As New DataSet
        ds = CType(Session("TimeBuilderQ"), DataSet)

        Dim i As Integer
        i = 0
        For Each drow As DataRow In ds.Tables(0).Rows
            If (drow.Item("QuestionType") = QuestionType) Then
                If drow.Item("QuestionId") <> QuestionId Then
                    i = i + 1
                    drow.Item("RowNum") = i
                    drow.Item("Sequence") = i
                    drow.Item("MaxSequence") = drow.Item("MaxSequence") - 1
                End If
            End If
        Next
        ds.Tables(0).AcceptChanges()
        Session("TimeBuilderQ") = ds

        Dim dtQuestion As DataTable = ds.Tables(0)
        Dim dvQuestion As New DataView
        dvQuestion = dtQuestion.Copy.DefaultView

        'add primary key as QuestionId 
        Dim keysQ(1) As DataColumn
        keysQ(0) = dtQuestion.Columns("QuestionId")

        Dim dtAnswer As DataTable = ds.Tables(1)
        Dim dvAnswer As New DataView
        dvAnswer = dtAnswer.Copy.DefaultView

        'add primary key as QuestionId 
        Dim keysA(1) As DataColumn
        keysA(0) = dtAnswer.Columns("QuestionId")

        Dim foundrowQ As DataRow() = dtQuestion.Select("QuestionId = '" & QuestionId & "'")
        If Not (foundrowQ Is Nothing) Then
            dtQuestion.Rows.Remove(foundrowQ(0))
            ds.Tables(0).AcceptChanges()
            Session("TimeBuilderQ") = ds
        End If

        Dim foundrowAns As DataRow() = dtAnswer.Select("QuestionId = '" & QuestionId & "'")
        If Not (foundrowAns Is Nothing) Then
            i = 0
            For i = 0 To foundrowAns.Length - 1
                dtAnswer.Rows.Remove(foundrowAns(i))
                ds.Tables(1).AcceptChanges()
                Session("TimeBuilderQ") = ds
            Next
        End If


        PopulateTimeBuilderQ()
        ShowHidePanel(QuestionType)
    End Sub

    Protected Sub ddQAnsCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddQAnsCount As DropDownList = DirectCast(sender, DropDownList)
        UpdateTimeBuilderQA(rptTimeBuilderTab1Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab2Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab3Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab4Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab5Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab6Q)


        Dim ds As New DataSet
        ds = CType(Session("TimeBuilderQ"), DataSet)
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim MaxAnswerId As Integer
        Dim totalAns As Integer = CInt(ddQAnsCount.SelectedValue)

        If ds.Tables.Count > 1 Then
            If ds.Tables(1).Rows.Count > 0 Then

                For Each drow As DataRow In ds.Tables(1).Rows
                    If drow.Item("QuestionId") = ddQAnsCount.TabIndex Then
                        i = i + 1
                        drow.Item("QuestionAnsCount") = totalAns
                        drow.Item("RowNum") = i
                    End If
                Next

                ds.AcceptChanges()
                Session("TimeBuilderQ") = ds

                If (i > totalAns) Then
                    Dim rindex As Integer
                    Dim strindexToRemove As String = ""

                    For rindex = 0 To ds.Tables(1).Rows.Count - 1
                        If ds.Tables(1).Rows(rindex)("QuestionId") = ddQAnsCount.TabIndex Then
                            j = j + 1
                            If (totalAns < j) Then
                                If (strindexToRemove = "") Then
                                    strindexToRemove = rindex
                                Else
                                    strindexToRemove = strindexToRemove & ":" & rindex
                                End If
                            End If
                        End If
                    Next

                    If (strindexToRemove <> "") Then
                        j = 0
                        Dim IdsToRemove As String() = strindexToRemove.Split(":")
                        For Each AnsId As String In IdsToRemove
                            ds.Tables(1).Rows.RemoveAt(AnsId - j)
                            ds.Tables(1).AcceptChanges()
                            Session("TimeBuilderQ") = ds
                            j = j + 1
                        Next
                    End If
                Else
                    For i = i + 1 To totalAns
                        For Each drow As DataRow In ds.Tables(1).Rows
                            MaxAnswerId = drow.Item("MaxAnswerId") + 1
                            drow.Item("MaxAnswerId") = MaxAnswerId
                        Next
                        ds.AcceptChanges()
                        Session("TimeBuilderQ") = ds
                        AddAnswers(ddQAnsCount.ToolTip.ToString, CInt(ddQAnsCount.TabIndex), MaxAnswerId, totalAns, i)
                    Next
                End If
            End If
        End If

        PopulateTimeBuilderQ()
        ShowHidePanel(ddQAnsCount.ToolTip.ToString)
    End Sub
    Public Sub ShowHidePanel(ByVal QMode As String)
        divOuterTab1.Style.Add("display", "none")
        divOuterTab2.Style.Add("display", "none")
        divOuterTab3.Style.Add("display", "none")
        divOuterTab4.Style.Add("display", "none")
        divOuterTab5.Style.Add("display", "none")
        divOuterTab6.Style.Add("display", "none")

        divQTab1Tab.Attributes.Add("class", "floatLeft clstabImage1")
        divQTab2Tab.Attributes.Add("class", "floatLeft clstabImage2")
        divQTab3Tab.Attributes.Add("class", "floatLeft clstabImage3")
        divQTab4Tab.Attributes.Add("class", "floatLeft clstabImage4")
        divQTab5Tab.Attributes.Add("class", "floatLeft clstabImage5")
        divQTab6Tab.Attributes.Add("class", "floatLeft clstabImage6")

        If (QMode = "Tab1") Then
            divOuterTab1.Style.Add("display", "block")
            divQTab1Tab.Attributes.Add("class", "floatLeft clstabImage1Selected")
        ElseIf (QMode = "Tab2") Then
            divOuterTab2.Style.Add("display", "block")
            divQTab2Tab.Attributes.Add("class", "floatLeft clstabImage2Selected")
        ElseIf (QMode = "Tab3") Then
            divOuterTab3.Style.Add("display", "block")
            divQTab3Tab.Attributes.Add("class", "floatLeft clstabImage3Selected")
        ElseIf (QMode = "Tab4") Then
            divOuterTab4.Style.Add("display", "block")
            divQTab4Tab.Attributes.Add("class", "floatLeft clstabImage4Selected")
        ElseIf (QMode = "Tab5") Then
            divOuterTab5.Style.Add("display", "block")
            divQTab5Tab.Attributes.Add("class", "floatLeft clstabImage5Selected")
        ElseIf (QMode = "Tab6") Then
            divOuterTab6.Style.Add("display", "block")
            divQTab6Tab.Attributes.Add("class", "floatLeft clstabImage6Selected")
        End If
    End Sub

    Public Sub UpdateTimeBuilderQA(ByVal rptClientQuestions As Repeater)
        Dim rptTimeBuilderAnswers As Repeater
        Dim hdnQuestionId As HtmlInputHidden
        Dim hdnAnswerId As HtmlInputHidden

        Dim ds As New DataSet
        ds = CType(Session("TimeBuilderQ"), DataSet)

        Dim dvQuestions As New DataView
        dvQuestions = ds.Tables(0).DefaultView

        Dim dvAnswers As New DataView
        dvAnswers = ds.Tables(1).DefaultView

        If Not IsNothing(rptClientQuestions) Then
            For Each row As RepeaterItem In rptClientQuestions.Items
                hdnQuestionId = CType(row.FindControl("hdnQuestionId"), HtmlInputHidden)

                dvQuestions.RowFilter = "QuestionId = " & hdnQuestionId.Value

                If dvQuestions.Count > 0 Then

                    dvQuestions.Item(0).Item("Question") = CType(row.FindControl("txtTimeBuilderQ"), TextBox).Text
                    dvQuestions.Item(0).Item("QuestionSummary") = CType(row.FindControl("txtQSummary"), TextBox).Text
                    If (CType(row.FindControl("txtQTime"), TextBox).Text.Trim <> "") Then
                        dvQuestions.Item(0).Item("QuestionTime") = CType(row.FindControl("txtQTime"), TextBox).Text
                    Else
                        dvQuestions.Item(0).Item("QuestionTime") = 0
                    End If
                    dvQuestions.Item(0).Item("QuestionMessage") = CType(row.FindControl("txtQMessage"), TextBox).Text.Replace(Chr(10), "<BR>")
                    dvQuestions.Item(0).Item("IsMandatory") = CType(row.FindControl("chkIsMandatory"), CheckBox).Checked
                    dvQuestions.Item(0).Item("BaseQuestionSequence") = CType(row.FindControl("drpdwnQDependent"), DropDownList).SelectedValue
                    dvQuestions.Item(0).Item("BaseQuestionSequenceTab") = CType(row.FindControl("drpdwnQDependentTab"), DropDownList).SelectedValue
                    If (CType(row.FindControl("txtMinQuantity"), TextBox).Text.Trim <> "") Then
                        dvQuestions.Item(0).Item("MinQuantity") = CType(row.FindControl("txtMinQuantity"), TextBox).Text
                    Else
                        dvQuestions.Item(0).Item("MinQuantity") = 0
                    End If
                    If (CType(row.FindControl("txtMaxQuantity"), TextBox).Text.Trim <> "") Then
                        dvQuestions.Item(0).Item("MaxQuantity") = CType(row.FindControl("txtMaxQuantity"), TextBox).Text
                    Else
                        dvQuestions.Item(0).Item("MaxQuantity") = 0
                    End If
                End If

                rptTimeBuilderAnswers = CType(row.FindControl("rptTimeBuilderQuestionAnswers"), Repeater)

                If Not IsNothing(rptTimeBuilderAnswers) Then
                    For Each rowAns As RepeaterItem In rptTimeBuilderAnswers.Items

                        hdnAnswerId = CType(rowAns.FindControl("hdnAnswerId"), HtmlInputHidden)
                        dvAnswers.RowFilter = "AnswerId = " & hdnAnswerId.Value
                        If dvAnswers.Count > 0 Then
                            dvAnswers.Item(0).Item("Answer") = CType(rowAns.FindControl("txtAnswer"), TextBox).Text
                            dvAnswers.Item(0).Item("AnswerMessage") = CType(rowAns.FindControl("txtAnswerMessage"), TextBox).Text.Replace(Chr(10), "<BR>")
                            dvAnswers.Item(0).Item("AnswerTime") = CType(rowAns.FindControl("txtAnswerTime"), TextBox).Text
                            dvAnswers.Item(0).Item("StopBooking") = CType(rowAns.FindControl("chkStopBooking"), CheckBox).Checked
                        End If
                        dvAnswers.RowFilter = ""
                        ds.Tables(1).AcceptChanges()
                        Session("TimeBuilderQ") = ds
                    Next
                End If
                dvQuestions.RowFilter = ""
                ds.Tables(0).AcceptChanges()
                Session("TimeBuilderQ") = ds
            Next
        End If
    End Sub


    Public Sub AddAnswers(ByVal Type As String, ByVal QuestionId As Integer, ByVal AnswerId As Integer, ByVal QuestionAnsCount As Integer, ByVal RowNum As Integer)

        Dim ds As New DataSet
        ds = CType(Session("TimeBuilderQ"), DataSet)

        Dim dt As New DataTable

        dt = ds.Tables(1)

        Dim drow As DataRow
        drow = dt.NewRow()

        drow("QuestionId") = QuestionId
        drow("Question") = ""
        drow("QuestionType") = Type
        drow("QuestionTime") = 0
        drow("MinQuantity") = 0
        drow("MaxQuantity") = 0
        drow("QuestionMessage") = ""
        drow("AnswerId") = AnswerId
        drow("Answer") = ""
        drow("AnswerTime") = 0
        drow("AnswerMessage") = ""
        drow("StopBooking") = 0
        drow("QuestionAnsCount") = QuestionAnsCount
        drow("MaxAnswerId") = AnswerId
        drow("RowNum") = RowNum

        dt.Rows.Add(drow)
        dt.AcceptChanges()

        ds.AcceptChanges()

        Session("TimeBuilderQ") = ds

    End Sub

    Private Sub lnkAddTimeBuilderTab1Q_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTimeBuilderTab1Q.Click
        AddQuestion("Tab1")
    End Sub
    Private Sub lnkAddTimeBuilderTab2Q_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTimeBuilderTab2Q.Click
        AddQuestion("Tab2")
    End Sub
    Private Sub lnkAddTimeBuilderTab3Q_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTimeBuilderTab3Q.Click
        AddQuestion("Tab3")
    End Sub
    Private Sub lnkAddTimeBuilderTab4Q_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTimeBuilderTab4Q.Click
        AddQuestion("Tab4")
    End Sub
    Private Sub lnkAddTimeBuilderTab5Q_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTimeBuilderTab5Q.Click
        AddQuestion("Tab5")
    End Sub
    Private Sub lnkAddTimeBuilderTab6Q_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTimeBuilderTab6Q.Click
        AddQuestion("Tab6")
    End Sub

    Public Sub AddQuestion(ByVal Type As String)

        UpdateTimeBuilderQA(rptTimeBuilderTab1Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab2Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab3Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab4Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab5Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab6Q)

        Dim ds As New DataSet
        ds = CType(Session("TimeBuilderQ"), DataSet)

        Dim MaxQuestionId As Integer
        Dim MaxAnswerId As Integer
        Dim RowNum As Integer
        RowNum = 1

        For Each drowQ As DataRow In ds.Tables(0).Rows
            MaxQuestionId = drowQ.Item("MaxQuestionId") + 1
            drowQ.Item("MaxQuestionId") = MaxQuestionId
        Next

        ds.AcceptChanges()
        Session("TimeBuilderQ") = ds


        Dim dv As New DataView
        If ds.Tables.Count > 1 Then
            If ds.Tables(0).Rows.Count > 0 Then
                dv = ds.Tables(0).DefaultView
                dv.RowFilter = "QuestionType = '" & Type & "'"
                RowNum = RowNum + dv.ToTable.Rows.Count

                For Each drQ As DataRow In dv.Table.Rows
                    drQ.Item("MaxSequence") = RowNum
                Next

                ds.AcceptChanges()
                Session("TimeBuilderQ") = ds

            End If
        End If

        Dim dt As New DataTable

        dt = ds.Tables(0)

        Dim drow As DataRow
        drow = dt.NewRow()

        drow("QuestionId") = MaxQuestionId
        drow("Question") = ""
        drow("IsMandatory") = 0
        drow("BaseQuestionSequence") = 0
        drow("BaseQuestionSequenceTab") = ""
        drow("QuestionType") = Type
        drow("QuestionTime") = 0
        drow("QuestionMessage") = ""
        drow("QuestionSummary") = ""
        drow("Selected") = "--"
        drow("MaxQuestionId") = MaxQuestionId
        drow("RowNum") = RowNum
        drow("Sequence") = RowNum
        drow("MaxSequence") = RowNum
        drow("MinQuantity") = 0
        drow("MaxQuantity") = 0
        If (Type = "Tab1") Then
            drow("TabName") = txtTab1Name.Text.Trim
        ElseIf (Type = "Tab2") Then
            drow("TabName") = txtTab2Name.Text.Trim
        ElseIf (Type = "Tab3") Then
            drow("TabName") = txtTab3Name.Text.Trim
        ElseIf (Type = "Tab4") Then
            drow("TabName") = txtTab4Name.Text.Trim
        ElseIf (Type = "Tab5") Then
            drow("TabName") = txtTab5Name.Text.Trim
        ElseIf (Type = "Tab6") Then
            drow("TabName") = txtTab6Name.Text.Trim
        End If




        dt.Rows.Add(drow)
        dt.AcceptChanges()

        ds.AcceptChanges()

        Session("TimeBuilderQ") = ds

        ds.AcceptChanges()
        Session("TimeBuilderQ") = ds

        For Each drowA As DataRow In ds.Tables(1).Rows
            MaxAnswerId = drowA.Item("MaxAnswerId") + 1
            drowA.Item("MaxAnswerId") = MaxAnswerId
        Next

        ds.AcceptChanges()
        Session("TimeBuilderQ") = ds

        AddAnswers(Type, MaxQuestionId, MaxAnswerId, 1, 1)

        PopulateTimeBuilderQ()
        ShowHidePanel(Type)

    End Sub

    Protected Sub ResetTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetTop.Click, btnResetBottom.Click
        PopulateData()
    End Sub
    Protected Sub Save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTop.Click, btnSaveBottom.Click
        UpdateTimeBuilderQA(rptTimeBuilderTab1Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab2Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab3Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab4Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab5Q)
        UpdateTimeBuilderQA(rptTimeBuilderTab6Q)

        If ValidatingQuestionsData() = "" Then
            Dim dsTBQ As New XSDTBQ
            Dim rptClientQuestions As Repeater
            Dim rptTimeBuilderAnswers As Repeater

            Dim hdnQuestionId As HtmlInputHidden
            Dim hdnAnswerId As HtmlInputHidden
            Dim hdnQuestionType As HtmlInputHidden

            Dim QuestionId As Integer
            QuestionId = 0

            Dim ds As New DataSet
            ds = CType(Session("TimeBuilderQ"), DataSet)

            Dim dvQuestions As New DataView
            dvQuestions = ds.Tables(0).DefaultView

            Dim dvAnswers As New DataView
            dvAnswers = ds.Tables(1).DefaultView

            Dim i As Integer
            Dim TabNumber As Integer
            TabNumber = 0

            Dim TabName As String
            For i = 0 To 5
                TabName = ""
                If (i = 0) Then
                    rptClientQuestions = rptTimeBuilderTab1Q
                    TabName = txtTab1Name.Text.Trim
                ElseIf i = 1 Then
                    rptClientQuestions = rptTimeBuilderTab2Q
                    TabName = txtTab2Name.Text.Trim
                ElseIf i = 2 Then
                    rptClientQuestions = rptTimeBuilderTab3Q
                    TabName = txtTab3Name.Text.Trim
                ElseIf i = 3 Then
                    rptClientQuestions = rptTimeBuilderTab4Q
                    TabName = txtTab4Name.Text.Trim
                ElseIf i = 4 Then
                    rptClientQuestions = rptTimeBuilderTab5Q
                    TabName = txtTab5Name.Text.Trim
                Else
                    rptClientQuestions = rptTimeBuilderTab6Q
                    TabName = txtTab6Name.Text.Trim
                End If

                If Not IsNothing(rptClientQuestions) Then
                    For Each row As RepeaterItem In rptClientQuestions.Items

                        If (CType(row.FindControl("hdnSequence"), HtmlInputHidden).Value = 1) Then
                            TabNumber = TabNumber + 1
                        End If


                        hdnQuestionId = CType(row.FindControl("hdnQuestionId"), HtmlInputHidden)
                        hdnQuestionType = CType(row.FindControl("hdnQuestionType"), HtmlInputHidden)

                        QuestionId = QuestionId + 1

                        dvQuestions.RowFilter = "QuestionId = " & hdnQuestionId.Value

                        If dvQuestions.Count > 0 Then
                            Dim nrow As XSDTBQ.TimeBuilderQuestionsRow = dsTBQ.TimeBuilderQuestions.NewRow
                            With nrow
                                .QuestionID = QuestionId
                                .CompanyID = CInt(Request("companyid"))
                                .ProductID = CInt(ViewState("ProductID"))
                                .Question = CType(row.FindControl("txtTimeBuilderQ"), TextBox).Text.Trim
                                .QuestionSummary = CType(row.FindControl("txtQSummary"), TextBox).Text.Trim
                                '.QuestionType = hdnQuestionType.Value
                                .QuestionType = "Tab" & TabNumber.ToString
                                If (CType(row.FindControl("txtQTime"), TextBox).Text.Trim <> "") Then
                                    .RequiredTime = CType(row.FindControl("txtQTime"), TextBox).Text.Trim
                                Else
                                    .RequiredTime = 0
                                End If
                                
                                If (CType(row.FindControl("txtMinQuantity"), TextBox).Text.Trim <> "") Then
                                    .MinQuantity = CType(row.FindControl("txtMinQuantity"), TextBox).Text.Trim
                                Else
                                    .MinQuantity = 0
                                End If
                                If (CType(row.FindControl("txtMaxQuantity"), TextBox).Text.Trim <> "") Then
                                    .MaxQuantity = CType(row.FindControl("txtMaxQuantity"), TextBox).Text.Trim
                                Else
                                    .MaxQuantity = 0
                                End If

                                .BaseQuestionSequence = CType(row.FindControl("drpdwnQDependent"), DropDownList).SelectedValue
                                .BaseQuestionSequenceTab = CType(row.FindControl("drpdwnQDependentTab"), DropDownList).SelectedValue
                                .IsMandatory = CType(row.FindControl("chkIsMandatory"), CheckBox).Checked
                                .Message = CType(row.FindControl("txtQMessage"), TextBox).Text.Replace(Chr(10), "<BR>")
                                '.Message = CType(row.FindControl("txtQMessage"), TextBox).Text.Trim
                                .Sequence = CType(row.FindControl("hdnSequence"), HtmlInputHidden).Value
                                .TabName = TabName
                            End With
                            dsTBQ.TimeBuilderQuestions.Rows.Add(nrow)
                        End If

                        rptTimeBuilderAnswers = CType(row.FindControl("rptTimeBuilderQuestionAnswers"), Repeater)

                        If Not IsNothing(rptTimeBuilderAnswers) Then
                            For Each rowAns As RepeaterItem In rptTimeBuilderAnswers.Items
                                hdnAnswerId = CType(rowAns.FindControl("hdnAnswerId"), HtmlInputHidden)
                                dvAnswers.RowFilter = "AnswerId = " & hdnAnswerId.Value
                                If dvAnswers.Count > 0 Then
                                    Dim nrowAnswer As XSDTBQ.TimeBuilderAnswersRow = dsTBQ.TimeBuilderAnswers.NewRow
                                    With nrowAnswer
                                        .AnswerID = hdnAnswerId.Value
                                        .QuestionID = QuestionId
                                        .CompanyId = CInt(Request("companyid"))
                                        .ProductID = CInt(ViewState("ProductID"))
                                        .Answer = CType(rowAns.FindControl("txtAnswer"), TextBox).Text.Trim
                                        If (CType(rowAns.FindControl("txtAnswerTime"), TextBox).Text.Trim <> "") Then
                                            .RequiredTime = CType(rowAns.FindControl("txtAnswerTime"), TextBox).Text.Trim
                                        Else
                                            .RequiredTime = 0
                                        End If
                                        .Message = CType(rowAns.FindControl("txtAnswerMessage"), TextBox).Text.Trim.Replace(Chr(10), "<BR>")
                                        .StopBooking = CType(rowAns.FindControl("chkStopBooking"), CheckBox).Checked
                                    End With
                                    dsTBQ.TimeBuilderAnswers.Rows.Add(nrowAnswer)
                                End If
                                dvAnswers.RowFilter = ""
                            Next
                        End If
                        dvQuestions.RowFilter = ""
                    Next
                End If
            Next
            Dim xmlContent As String = dsTBQ.GetXml
            xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)

            Dim dsSuccess As New DataSet
            dsSuccess = ws.WSContact.AddEditTimeBuilderQA(xmlContent, QuestionId, "TimeBuilderQuestions", CInt(Request("companyid")), CInt(ViewState("ProductID")), Session("UserID"))
            If dsSuccess.Tables.Count > 0 Then
                If dsSuccess.Tables(0).Rows.Count > 0 Then
                    If CStr(dsSuccess.Tables(0).Rows(0)(0)) = "1" Then
                        Response.Redirect("AdminProduct.aspx?mode=edit&productid=" & ViewState("ProductID") & "&pagename=CompanyProfile&CompanyID=" & Request("companyid") & "&bizDivId=1")
                    End If
                End If
            End If
        End If
    End Sub
    Private Function ValidatingQuestionsData() As String

        Dim errmsg As String = ""
        Dim flagPerQ As Boolean = True
        Dim Type As String
        errmsg = ValidatingEachQuestionType(rptTimeBuilderTab1Q, flagPerQ, errmsg)
        If (errmsg <> "") Then
            flagPerQ = False
        End If
        errmsg = ValidatingEachQuestionType(rptTimeBuilderTab2Q, flagPerQ, errmsg)
        If (errmsg <> "") Then
            flagPerQ = False
        End If
        errmsg = ValidatingEachQuestionType(rptTimeBuilderTab3Q, flagPerQ, errmsg)
        If (errmsg <> "") Then
            flagPerQ = False
        End If
        errmsg = ValidatingEachQuestionType(rptTimeBuilderTab4Q, flagPerQ, errmsg)
        If (errmsg <> "") Then
            flagPerQ = False
        End If
        errmsg = ValidatingEachQuestionType(rptTimeBuilderTab5Q, flagPerQ, errmsg)
        If (errmsg <> "") Then
            flagPerQ = False
        End If
        errmsg = ValidatingEachQuestionType(rptTimeBuilderTab6Q, flagPerQ, errmsg)

        If errmsg <> "" Then
            divValidationTBQuestions.Visible = True
            lblValidationTBQuestions.Text = errmsg.ToString
            If (divOuterTab1.Style.Value = "display:block;") Then
                Type = "Tab1"
            ElseIf (divOuterTab2.Style.Value = "display:block;") Then
                Type = "Tab2"
            ElseIf (divOuterTab3.Style.Value = "display:block;") Then
                Type = "Tab3"
            ElseIf (divOuterTab4.Style.Value = "display:block;") Then
                Type = "Tab4"
            ElseIf (divOuterTab5.Style.Value = "display:block;") Then
                Type = "Tab5"
            Else
                Type = "Tab6"
            End If
            divOuterTab1.Style.Add("display", "none")
            divOuterTab2.Style.Add("display", "none")
            divOuterTab3.Style.Add("display", "none")
            divOuterTab4.Style.Add("display", "none")
            divOuterTab5.Style.Add("display", "none")
            divOuterTab6.Style.Add("display", "none")

            divQTab1Tab.Attributes.Add("class", "floatLeft clstabImage1")
            divQTab2Tab.Attributes.Add("class", "floatLeft clstabImage2")
            divQTab3Tab.Attributes.Add("class", "floatLeft clstabImage3")
            divQTab4Tab.Attributes.Add("class", "floatLeft clstabImage4")
            divQTab5Tab.Attributes.Add("class", "floatLeft clstabImage5")
            divQTab6Tab.Attributes.Add("class", "floatLeft clstabImage6")

            PopulateTimeBuilderQ()
            ShowHidePanel(Type)
        Else
            divValidationTBQuestions.Visible = False
            lblValidationTBQuestions.Text = ""
        End If
        Return errmsg
    End Function
    Private Function ValidatingEachQuestionType(ByVal rptTimeBuilder As Repeater, ByVal flagPerQ As Boolean, ByVal errmsg As String) As String

        Dim errmsgPerQ As String = ""
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim hdnQuestionType As HtmlInputHidden

        If Not IsNothing(rptTimeBuilder) Then
            For Each row As RepeaterItem In rptTimeBuilder.Items
                Dim rptTimeBuilderQuestionAnswers As Repeater = CType(row.FindControl("rptTimeBuilderQuestionAnswers"), Repeater)
                hdnQuestionType = CType(row.FindControl("hdnQuestionType"), HtmlInputHidden)
                i = i + 1
                j = 0
                errmsgPerQ = ""
                If CType(row.FindControl("txtTimeBuilderQ"), TextBox).Text.Trim.Length = 0 Then
                    If flagPerQ = True Then
                        errmsgPerQ = "- Please enter question in " & hdnQuestionType.Value & " tab to question " & i
                        flagPerQ = False
                    Else
                        errmsgPerQ = errmsgPerQ & "<br>- Please enter question in " & hdnQuestionType.Value & " tab to question " & i
                    End If
                End If
                If CType(row.FindControl("txtQSummary"), TextBox).Text.Trim.Length = 0 Then
                    If flagPerQ = True Then
                        errmsgPerQ = "- Please enter summary in " & hdnQuestionType.Value & " tab to question " & i
                        flagPerQ = False
                    Else
                        errmsgPerQ = errmsgPerQ & "<br>- Please enter summary in " & hdnQuestionType.Value & " tab to question " & i
                    End If
                End If
                If Not IsNothing(rptTimeBuilderQuestionAnswers) Then
                    For Each rowAns As RepeaterItem In rptTimeBuilderQuestionAnswers.Items
                        j = j + 1
                        If CType(rowAns.FindControl("txtAnswer"), TextBox).Text.Trim.Length = 0 Then
                            If flagPerQ = True Then
                                errmsgPerQ = "- Please enter an answer in " & hdnQuestionType.Value & " tab to question " & i & " Answer " & j
                                flagPerQ = False
                            Else
                                errmsgPerQ = errmsgPerQ & "<br>- Please enter an answer in " & hdnQuestionType.Value & " tab to question " & i & " Answer " & j
                            End If
                        End If
                    Next
                End If
                errmsg = errmsg & errmsgPerQ
            Next
        End If
        Return errmsg
    End Function

    
End Class