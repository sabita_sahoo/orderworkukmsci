<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreatePartPayment.aspx.vb" Inherits="Admin.CreatePartPayment" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Create Part Payment"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
<asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server">
<ContentTemplate>

			<asp:Panel ID="pnlMain" runat="server">			
			  <div id="divValidationMain" Visible=False  class="divValidation" runat=server >
				  <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					</tr>
					<tr valign="middle">
					  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
					  <td class="validationText"><div  id="divValidationMsg"></div>
						  <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
						  <asp:ValidationSummary ID="validationSummarySubmit" runat="server"  CssClass="bodytxtValidationMsg" DisplayMode="BulletList" ></asp:ValidationSummary>
					  </td>
					  <td width="20">&nbsp;</td>
					</tr>
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					</tr>
				  </table>
				  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				</div>							
			<div style="margin-left:20px"><table width="100%" cellpadding="0" cellspacing="0">
					<tr>
					<td width="10px"></td>
					<td class="HeadingRed">Create Cash Receipt</td>
					</tr>
					
				</table>
				
				
<table width="100%" cellspacing="0" cellpadding="0">
			     <tr>
					<td width="11">&nbsp;</td>
					<td width="262">&nbsp;</td>
					<td width="148" align="right" style="padding-left:3px" class="formLabelGrey">Please enter the Amount<SPAN class="bodytxtValidationMsg">*</SPAN>
	                          <asp:RequiredFieldValidator Display="None" ID="rqPayAmt" runat="server" ErrorMessage="Please enter the part payment amount" ForeColor="#EDEDEB"
														                        ControlToValidate="txtAmount" >*</asp:RequiredFieldValidator>
							  </td>
					 <td align="left" width="10">&nbsp;</td>
					 
					 <td width="25" align="left" >&nbsp;
	                               
	                              </td>
					 <td width="448" align="left" valign="middle" Class="formLabelGrey"> Payment Received Date
                     </td>
				</tr>
				
				
			   </table>

				<table width="100%" cellspacing="0" cellpadding="0">
			     <tr>
					<td width="8">&nbsp;</td>
					<td width="150"><uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact></td>
					<td width="262" align="right">&pound; <asp:TextBox ID="txtAmount" style="text-align:right"  CssClass="formFieldGrey width115" runat="server"></asp:TextBox></td>
					 <td align="left" width="80">&nbsp;</td>
					 
					 <td height="24" valign="middle" align="left">
	                                <asp:TextBox id="txtPayRecDate" runat="server" CssClass="formFieldGrey width120" TabIndex="2"></asp:TextBox>		              
		                               <img alt="Click to Select" src="Images/calendar.gif" id="btnBeginDate" style="cursor:pointer; vertical-align:middle;" />
                                      <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnBeginDate TargetControlID="txtPayRecDate" ID="calBeginDate" runat="server">
                                      </cc1:CalendarExtender>
	                              </td>
					 <td align="left"><asp:RegularExpressionValidator ID="rqWOBeginDate"  ControlToValidate="txtPayRecDate" ErrorMessage="Date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB" 
						ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"  runat="server" EnableClientScript="false">*</asp:RegularExpressionValidator>
                     </td>
				</tr>
				
				
			   </table>
			   <table>
			   <tr>
				<td>
					<table>
					<tr>
						 <td style="height:10px"></td>
						</tr>
						<tr>
						 <td><asp:Label ID="lblPerticulars" runat="server" Text="Cash Receipt Line Item Title" CssClass="formLabelGrey">Cash Receipt Line Item Title<SPAN class="bodytxtValidationMsg">*</SPAN> 
	                          <asp:RequiredFieldValidator ID="rqDesc" runat="server" ErrorMessage="Please enter Cash Receipt Line Item Title" ForeColor="#EDEDEB"
														                        ControlToValidate="txtPerticulars" EnableClientScript="false">*</asp:RequiredFieldValidator></asp:Label></td>
						</tr>
						<tr>
						 <td><asp:TextBox id="txtPerticulars" onChange="validateMaxLen(this.id, '200', 'Cash Receipt Line Item Title can have max 200 characters.')" Text="Payment received with thanks" runat="server" CssClass="formFieldGrey width635height60" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"
																				                            Rows="4" TabIndex="8"></asp:TextBox></td>
						</tr>
						<tr>
						 <td>&nbsp;</td>
						</tr>
						<tr>
						 <td><asp:Label ID="lblComments"  runat="server" Text="Please Enter Comments" CssClass="formLabelGrey"></asp:Label></td>
						</tr>
						<tr>
						<tr>
						 <td><asp:TextBox id="txtComments" onChange="validateMaxLen(this.id, '200', 'Comments can have max 200 characters.')" runat="server" CssClass="formFieldGrey width635height60" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"
																				                            Rows="4" TabIndex="8"></asp:TextBox></td>
						</tr>
					</table>
				</td>
				</tr>
				</table>
				
				<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td ></td>
					<td width="10">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
					<tr>
						<td width="570px" align="right">
						    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                <asp:LinkButton ID="btnSubmit" causesValidation=False runat="server" CssClass="txtButtonRed" Text="Submit"></asp:LinkButton>
                            </div>
							</td>
							<td width="10px">&nbsp;</td>
							<td align="left">
								<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                    <asp:LinkButton ID="btnCancel" causesValidation=False runat="server" CssClass="txtButtonRed" Text="Cancel"></asp:LinkButton>
                                </div>
							</td>
					</tr>
					<tr>
					<td ><asp:CustomValidator ID="CustomCompanyValidator" runat="server" ErrorMessage="Please select a Company - Contact" ForeColor="#EDEDEB" EnableClientScript="false">*</asp:CustomValidator></td>
					<td width="10">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				</table></div>
				</asp:Panel>
				<asp:Panel ID="pnlMsg" runat="server">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td width="10">&nbsp;</td>
						<td>
							<asp:Label CssClass="formLabelGrey" runat="server" ID="lblSuccessMsg" Text="Part Payment done successfully."></asp:Label>
						</td>
						<td width="10">&nbsp;</td>
						<td align="left">
								<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                    <asp:LinkButton ID="btnBack" causesValidation=False runat="server" CssClass="txtButtonRed" Text="Back"></asp:LinkButton>
                                </div>
							</td>
					</tr>
				</table>
				</asp:Panel>


</ContentTemplate>
</asp:UpdatePanel>

 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelCreateWorkOrder" runat="server">
        <ProgressTemplate>
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Please Wait...
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="pnlMain" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
				
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
</asp:Content>