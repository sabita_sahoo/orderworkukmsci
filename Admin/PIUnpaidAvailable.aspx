<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PIUnpaidAvailable.aspx.vb"  Inherits="Admin.PIUnpaidAvailable" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="UnPaid-Available"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
 <div id="divContent">       
		<input type="hidden" runat="server" id="hdnEtoE"/>  

        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
    
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />
	 <table border="0" cellspacing="0" cellpadding="0" width="200" id="tblTabInner" runat="server">
            <tr>
            <td height="15px"></td><td height="15px"></td></tr>
				<tr>
					
				<td class="paddingL10"  >
				<asp:LinkButton ID="lnkbtn1" runat="server" OnClick="Tab1Click" CssClass="HighlightTab" Width="180px">UnPaid Available</asp:LinkButton></td>
				<td class="paddingL10" >
				<asp:LinkButton ID="lnkbtn2" runat="server" OnClick="Tab2Click" CssClass="NormalTab" Width="180px">Withheld Payment</asp:LinkButton>
                  </td>
				</tr>
			</table>
			<hr style="background-color:#993366;height:5px;margin:0px;" />
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
					<td width="10px"></td>
					<td class="HeadingRed">Purchase Invoices</td>
					</tr>
					<tr height=""10px><td>&nbsp;</td></tr>
				</table>
                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
				<ContentTemplate>
				<table width="100%" cellspacing="0" cellpadding="0">
			     <tr>
					<td width="10">&nbsp;</td>
					<td width="300"><uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact></td>
					<td width="120" align="left"><asp:CheckBox ID="chkOver30Days" runat="server" Text='Over 30 Days' class="formTxt" AutoPostBack="true" /> </td>
					<td width="120" align="left"><asp:CheckBox ID="chkHideAvailable" runat="server" Text='Hide Available' class="formTxt" /> </td>
					<td width="80" align="left"><asp:CheckBox ID="chkShowAll" runat="server" Text='Show All' class="formTxt" /> </td>
					<td width="130" align="left"><asp:CheckBox ID="CheckHideDemo" runat="server" Text='Hide Demo Accounts' Checked="true" class="formTxt" /> </td>
					 <td align="left" width="80">
                     <input type=hidden id="hdnStatus" name="hdnStatus" runat=server /> 
                     <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                        <asp:LinkButton ID="btnGetPurchaseInvoices" causesValidation="true" runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
                     </div>   
                     </td>
                     <td align="left" width="190">
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                            <asp:linkButton ID="lnkExport" CausesValidation="false" CssClass="txtButtonRed" runat="server" style="cursor:hand" >&nbsp;Export to Excel&nbsp;</asp:linkButton>
                        </div>
		            </td>
				</tr>
				<tr>
					<td width="10">&nbsp;</td>
					<td width="300"><asp:CustomValidator ID="CustomCompanyValidator" Enabled=false  runat="server" ErrorMessage="Please select a Company - Contact" ForeColor="#EDEDEB">*</asp:CustomValidator></td>
					<td>&nbsp;</td>
				</tr>
			   </table>
            
				<div id="divValidationMain" visible=false class="divValidation" runat=server >
				  <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					</tr>
					<tr valign="middle">
					  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
					  <td class="validationText"><div  id="divValidationMsg"></div>
						  <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
						  <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
					  </td>
					  <td width="20">&nbsp;</td>
					</tr>
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					</tr>
				  </table>
				  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				</div>
			   
			   
				<table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                   <td runat="server"  id="tdMakeAvailable" width="120" align="left" class="txtWelcome paddingL10"><div id="divButton" style="width: 120px;  ">
											<div runat="server" class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" runat="server" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>								
											<asp:LinkButton OnClick="MakeAvailable" class="buttonText" style="cursor:hand;" id="btnMakeAvailable"  runat=server><strong>Make Available</strong></asp:LinkButton>
										   <cc1:ConfirmButtonExtender ID="ConfirmBtnAvailable" TargetControlID="btnMakeAvailable" ConfirmText="Do you want to make selected Invoices Available?" runat="server">
										  </cc1:ConfirmButtonExtender> 
											
											<div runat="server" class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" runat="server" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
										</div></td>	

				    <td runat="server"  id="tdHold" width="120"  align="left" class="txtWelcome paddingL10"><div id="divButton" style="width: 120px;  ">
											<div runat="server"  class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" runat="server"  alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>								
											<asp:LinkButton OnClick="WithHoldPayment" class="buttonText" style="cursor:hand;" id="btnHold"  runat=server><strong>Withhold Payment</strong></asp:LinkButton>
										   <cc1:ConfirmButtonExtender ID="ConfirmBtnHold" TargetControlID="btnHold" ConfirmText="Do you want to hold the selected Invoices?" runat="server">
										  </cc1:ConfirmButtonExtender> 
										<div runat="server"  class="bottonBottomGrey"><img runat="server"  src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
										</div></td>									
                  </tr>
                  <tr>
                    <td>
                         <asp:Label ID="lblPermissionError"  Visible="false" CssClass="bodytxtValidationMsg" ForeColor="Red" Font-Bold="true" Font-Size="Small" runat="server"></asp:Label>
                    </td>
                  </tr>
				  <tr> <td height="15">&nbsp;</td><td height="15">&nbsp;</td></tr>
                </table>
				<asp:GridView ID="gvPurchaseInv" visible=false  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#555354" BorderWidth="1px" PageSize=25 BorderColor="White"  
							  DataKeyNames="SupplierName" PagerSettings-Mode=NextPreviousFirstLast PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
							   <EmptyDataTemplate>
							   <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
										<tr>
											<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
											</td>
										</tr>				
							   </table>
								  
							   </EmptyDataTemplate>
							   
							   <PagerTemplate>
									 
								 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
									<tr>
										<td runat="server" id="tdsetType1" width="80">					
										</td>
										
										
										<td id="tdsetType2" align="left" width="121">
											
										</td>
										<td>
											<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
											  <tr>
												<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
													<TR>
														<td align="right" valign="middle">
														  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
														</td>
														<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
														<table width="100%" border="0" cellpadding="0" cellspacing="0">
															<tr>
															<td align="right" width="36">
															<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
															  </div>	
																<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
																</div>																							   
															</td>
															<td width="50" align="center" style="margin-right:3px; ">													
																<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
																</asp:DropDownList>
															</td>
															<td width="30" valign="bottom">
																<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
																</div>
																<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
																</div>
															</td>
															</tr>
														</table>
														</td>
													</TR>
												</TABLE></td>
											  </tr>
											</table>
									  </td>
									</tr>
								</table>  
								   
						</PagerTemplate> 
						
							   <Columns>  

							   <asp:TemplateField HeaderText="<asp:CheckBox id='chkSelectAll' runat='server' onclick=CheckAll(this,'Check') value='checkbox' valign='middle'></asp:CheckBox>" HeaderStyle-CssClass="gridHdr" ItemStyle-Width="15px" ItemStyle-Wrap="true"
								 ItemStyle-HorizontalAlign="left" ItemStyle-CssClass="gridRow">
								 <ItemTemplate > 
										<asp:CheckBox ID="Check" Runat="server" Visible="True"></asp:CheckBox> 
										<INPUT id="hdnID" type="hidden" name="hdnID" runat="server" value='<%# Container.DataItem("AdviceNumber") %>'> 
								 </ItemTemplate>
							   </asp:TemplateField>
							   
							   <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="100px"  HeaderStyle-CssClass="gridHdr gridText"  DataField="AdviceNumber" SortExpression="AdviceNo" HeaderText="Purchase Invoice Number" ItemStyle-Height="40px" />        
							   
							   <asp:BoundField ItemStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr gridText"  DataField="Comments" SortExpression="Comments" HeaderText="Invoice Details" />        
							   
							   <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="70px"  HeaderStyle-CssClass="gridHdr gridText"  DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="Date Issued" DataFormatString="{0:dd/MM/yy}" HtmlEncode=false /> 
							   <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px"  HeaderStyle-CssClass="gridHdr gridText"  DataField="ReleaseDate" SortExpression="ReleaseDate" HeaderText="ReleaseDate" />
							   <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px"  HeaderStyle-CssClass="gridHdr gridText"  DataField="SupplierName" SortExpression="SupplierName" HeaderText="Supplier" />
							   <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px"  HeaderStyle-CssClass="gridHdr gridText"  DataField="BuyerName" SortExpression="BuyerName" HeaderText="Client" />
							   <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="80px"  HeaderStyle-CssClass="gridHdr gridText"  DataField="SupplierCreditTerms" SortExpression="SupplierCreditTerms" HeaderText="Credit Terms" />        
							   <asp:TemplateField SortExpression="Total" HeaderText="Total Invoice Value" HeaderStyle-CssClass="gridHdr gridText"> 
								<ItemStyle CssClass="gridRow" Wrap=true  Width="95px"  HorizontalAlign=Right/>
								<ItemTemplate> 
									<%#FormatCurrency(IIF(Not isDbnull(Container.DataItem("Total")) ,Container.DataItem("Total"),"0"),2,TriState.True, Tristate.true,Tristate.false)%>            
								</ItemTemplate>
							   </asp:TemplateField>
							   <asp:TemplateField SortExpression="Status" HeaderText="Status" HeaderStyle-CssClass="gridHdr gridText"> 
								<ItemStyle CssClass="gridRow" Wrap=true  Width="70px"  HorizontalAlign="left"/>
								<ItemTemplate> 
									<%#Container.DataItem("Status")%>   
									<INPUT id="hdnStatus" type="hidden" name="hdnStatus" runat="server" value='<%# Container.DataItem("Status")%>'>        
								</ItemTemplate>
							   </asp:TemplateField>
							   
							   <asp:TemplateField >               
								   <HeaderStyle CssClass="gridHdr"   Wrap=true /> 
								  <ItemStyle Wrap=true HorizontalAlign=Left Width="60px"   CssClass="gridRow"/>
								   <ItemTemplate>   
										   <%#GetLinks(Container.DataItem("AdviceNumber"), Container.DataItem("WOID"), Container.DataItem("SupplierCompanyID"))%>
								   </ItemTemplate>
							   </asp:TemplateField>   
							   
							   </Columns>
							   
							 
								
								<footerstyle CssClass="gridviewFooterColor"/>
								
								<RowStyle CssClass="gridRowGrey"></RowStyle>
									<AlternatingRowStyle CssClass="gridRowGreyHiglight"></AlternatingRowStyle>
						
			    </asp:GridView>
									
							   
				  <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
						SelectMethod="SelectFromDB"   EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.PIUnpaidAvailable" 
						SortParameterName="sortExpression">    
				</asp:ObjectDataSource>
				
			    </ContentTemplate>
                            <Triggers>
                            <asp:AsyncPostBackTrigger   ControlID="btnGetPurchaseInvoices" EventName="Click"/>  
                             <asp:PostBackTrigger   ControlID="lnkExport" /> 
                                                                
                         </Triggers>
                			    </asp:UpdatePanel>
				 
				 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
						<ProgressTemplate>
							<div class="gridText">
								<img  align=middle src="Images/indicator.gif" />
								<b>Fetching Data... Please Wait</b>
							</div>      
						</ProgressTemplate>
				</asp:UpdateProgress>
				<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePanel1" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
            <!-- InstanceEndEditable --></td>
         </tr>
         </table>
      </div>
</asp:Content>
