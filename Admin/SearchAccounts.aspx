<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master"
    Title="OrderWork :" CodeBehind="SearchAccounts.aspx.vb" Inherits="Admin.SearchAccounts" %>

<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="divContent">
        <div class="roundtopWhite">
            <img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner"
                style="display: none" /></div>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
            <tr>
                <td valign="top">
                    <!-- InstanceBeginEditable name="EdtContent" -->
                    <input name="hdnRegionName" type="hidden" id="hdnRegionName" runat="server">
                    <input name="hdnRegionValue" type="hidden" id="hdnRegionValue" runat="server">
                    <input name="hdnProductIds" type="hidden" id="hdnProductIds" runat="server">
                    <input name="hdnVendorName" type="hidden" id="hdnVendorName" runat="server">
                    <input name="hdnVendorValue" type="hidden" id="hdnVendorValue" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="20">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Search for Accounts </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr valign="top">
                                                            <td width="470">
                                                            </td>
                                                            <td rowspan="3" align="left" valign="top">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="bottom">
                                                                        <td width="293" height="15">
                                                                            <span class="formTxt">Keyword</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="293">
                                                                            <asp:TextBox ID="txtKeyword" CssClass="formField200" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="formTxt">First Name</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtFName" CssClass="formField200" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="formTxt">Last Name</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtLName" CssClass="formField200" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="formTxt">Company Name</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtCompanyName" CssClass="formField200" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="formTxt">Email</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtEmail" CssClass="formField200" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="formTxt">ContactId/AccountId</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtContactIdAccountId" CssClass="formField200" runat="server" />
                                                                        </td>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtContactIdAccountId"
                                                                            runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><span class="formTxt">Category Type</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList CssClass="formField200" ID="drpCategoryType" runat="server">
                                                                                <asp:ListItem Text="Select Category Type" Selected="True" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Internal" Value="Internal"></asp:ListItem>
                                                                                <asp:ListItem Text="External" Value="External"></asp:ListItem>
                                                                            </asp:DropDownList></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <span class="formTxt"><b>Select Company Accreditations</b></span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<%@ Register TagPrefix="uc1" TagName="UCCompAccreds" Src="~/UserControls/UK/UCMSCompAccredsUK.ascx" %>
							                <uc1:UCCompAccreds width="200px" id="UCCompAccreds1" runat="server" AppendCachekey="SearchAccounts" ShowMemberNo="False" ShowColMemberNo="False" ShowColDate="False"> </uc1:UCCompAccreds>  --%>
                                                                                        <%@ register tagprefix="uc1" tagname="UCAccreds" src="~/UserControls/Admin/UCAccreditationForSearch.ascx" %>
                                                                                        <uc1:UCAccreds ID="UCAccreditations" runat="server"></uc1:UCAccreds>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                        <tr class="smallText">
                                                                        <td style="padding-left:5px;">
                                                                        <b>DBS:</b>&nbsp;
                                                                        <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedEnhanced" runat="server" GroupName="CRBChecked" Text="Enhanced" TextAlign="right"></asp:RadioButton>
                                                                        <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedYes" runat="server" GroupName="CRBChecked" Text="Basic" TextAlign="right"></asp:RadioButton>
                                                                        <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedNo" runat="server" GroupName="CRBChecked" Text="No" Checked =true TextAlign="right"></asp:RadioButton>
                                                                           <%-- <asp:CheckBox ID="chkEngCRBChecked" TabIndex="13" runat="server" CssClass="formTxt"
                                                                                TextAlign="right" Text="Engineer DBS Checked"></asp:CheckBox>--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="bottom">
                                                                        <td width="293" height="25">
                                                                            <asp:CheckBox ID="chkRefChecked" TabIndex="7" runat="server" CssClass="formTxt" TextAlign="right"
                                                                                Text="Approved Suppliers Only"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="smallText" style="display:none;">
                                                                        <td>
                                                                            <asp:CheckBox ID="chkCRBChecked" TabIndex="10" runat="server" CssClass="formTxt"
                                                                                TextAlign="right" Text="DBS Checked"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkUKSecurity" TabIndex="11" runat="server" CssClass="formTxt"
                                                                                TextAlign="right" Text="UK Security Clearance"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkCSCS" TabIndex="11" runat="server" CssClass="formTxt" TextAlign="right"
                                                                                Text="CSCS"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkTrackSP" TabIndex="12" runat="server" CssClass="formTxt" TextAlign="right"
                                                                                Text="Track Supplier"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                
                                                                    <%--'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch--%>
                                                                    <tr class="smallText">
                                                                        <td>
                                                                            <asp:CheckBox ID="chkExcelPartner" TabIndex="13" runat="server" CssClass="formTxt"
                                                                                TextAlign="right" Text="Excel Partner"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkEPCP" TabIndex="14" runat="server" CssClass="formTxt" TextAlign="right"
                                                                                Text="EPCP"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkEngUKSecurity" TabIndex="14" runat="server" CssClass="formTxt"
                                                                                TextAlign="right" Text="Engineer UK Security Check"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkEngCSCS" TabIndex="15" runat="server" CssClass="formTxt" TextAlign="right"
                                                                                Text="Engineer CSCS"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkEngRightToWork" TabIndex="16" runat="server" CssClass="formTxt"
                                                                                TextAlign="right" Text="Engineer Right to work in UK"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkEngProofOfMeeting" TabIndex="13" runat="server" CssClass="formTxt"
                                                                                TextAlign="right" Text="Engineer with Proof of meeting" Checked="false"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="trFindNearest" runat="server">
                                                                        <td>
                                                                            <asp:CheckBox ID="chkNearestTo" TabIndex="13" runat="server" CssClass="formTxt" TextAlign="right"
                                                                                Text="Find Nearest" Checked="false" AutoPostBack="True"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="height: 25px;">
                                                                        <td>
                                                                            <div id="divNearestTo" runat="server" visible="false" style="padding-left: 10px;
                                                                                padding-right: 0px">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td height="15">
                                                                                            <span class="formTxt">PostCode</span>&nbsp;&nbsp;<asp:TextBox ID="txtPostCode" onkeyup="makeUppercase(this.id)" MaxLength="8" CssClass="formField width100"
                                                                                                runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span class="formTxt">Distance</span>&nbsp;&nbsp;
                                                                                            <asp:DropDownList ID="ddlNearestTo" TabIndex="10" runat="server" CssClass="formField width100">
                                                                                                <asp:ListItem Text="Upto 10 Miles" Value="10"></asp:ListItem>
                                                                                                 <asp:ListItem Text="Upto 20 Miles" Value="20"></asp:ListItem>
                                                                                                <asp:ListItem Text="Upto 30 Miles" Value="30"></asp:ListItem>
                                                                                                <asp:ListItem Text="Upto 50 Miles" Value="50"></asp:ListItem>
                                                                                                 <asp:ListItem Text="Upto 100 Miles" Value="100"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Button CssClass="formtxt" ID="btnSearch" OnClick="Search_Click" runat="server"
                                                                                Text="Search for Accounts" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td width="470" class="padRight20Left20">
                                                                            <span class="formTxt"><b>Area of Expertise </b></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="trAreaOfExp" runat="server">
                                                                        <td class="padRight20Left20">
                                                                            <%--<span class="formTxt">Choose Areas Of Expertise Site</span>--%>
                                                                            <asp:RadioButtonList ID="rdAOEList" RepeatDirection="Horizontal" AutoPostBack="true"
                                                                                CssClass="formTxt" runat="server">
                                                                                <%-- <asp:ListItem  Value="1" selected="true" ><span class="formTxt">Order Work UK</span></asp:ListItem>--%>
                                                                                <%--   <asp:ListItem   Value="2"><span class="formTxt">Skills Finder UK</span></asp:ListItem>--%>
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" width="440px" class="padRight20Left20">
                                                                            <%@ register tagprefix="uc1" tagname="UCAOE" src="~/UserControls/Admin/UCTableAOE.ascx" %>
                                                                            <uc1:UCAOE ID="UCAOE" runat="server"></uc1:UCAOE>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table style="visibility: hidden" cellspacing="0" cellpadding="0" width="474" border="0">
                                                                    <tr valign="top">
                                                                        <td class="formTxt padRight20Left20" align="left" width="202" height="14">
                                                                            <b>Select Regions</b>
                                                                        </td>
                                                                        <td class="formTxt" align="left" height="30">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td class="formTxt" align="left" width="202" height="14">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="273" class="padRight20Left20">
                                                                            <%@ register tagprefix="uc3" tagname="UCDualListBox" src="~/UserControls/UK/UCDualListBox.ascx" %>
                                                                            <uc3:UCDualListBox ID="UCRegions" runat="server"></uc3:UCDualListBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td align="right">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td colspan="2" class="txtListing ">
                                                                <asp:Label runat="server" ID="lblSearchCriteria" CssClass="txtListing"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblInvalidPostCode" CssClass="bodytxtValidationMsg"
                                                                    Text="The post code been searched is incorrect. Please enter valid post code. Search result will returned has ignored Nearest to. "
                                                                    Visible="false"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <span align="center"><b>
                                            <asp:Label runat="server" ID="lblMsgSent"></asp:Label></b></span>
                                    </td>
                                    <td width="20">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <asp:Panel ID="pnlSearchResult" runat="server" Visible="false">
                                <table width="98%" align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvContacts" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                ForeColor="#333333" BorderWidth="1px" PageSize="25" BorderColor="White" PagerSettings-Mode="NextPreviousFirstLast"
                                                CssClass="gridrow" PagerSettings-Position="TopAndBottom" Width="100%" DataSourceID="ObjectDataSource1"
                                                AllowSorting="true">
                                                <EmptyDataTemplate>
                                                    <table width="100%" border="0" cellpadding="10" cellspacing="0">
                                                        <tr>
                                                            <td align="center" style="text-align: center" valign="middle" height="300" class="txtWelcome">
                                                                Sorry! There are no records in this category.
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EmptyDataTemplate>
                                                <PagerTemplate>
                                                    <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9"
                                                        id="tblMainTop" style="visibility: visible" runat="server">
                                                        <tr>
                                                            <%-- 'Poonam added on 31/03/2015 -4044201 - To disable export to excel button to non admin users--%>
                                                            <td runat="server" id="tdsetType1" width="90" visible="false">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="90">
                                                                    <tr>
                                                                        <td class="txtListing" align="left">
                                                                            <div id="divButton" style="width: 115px;">
                                                                                <div class="bottonTopGrey">
                                                                                    <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                                                                        height="4" class="btnCorner" style="display: none" /></div>
                                                                                <a runat="server" target="_blank" id="btnExport" tabindex="6" class="buttonText"><strong>
                                                                                    Export to Excel</strong></a>
                                                                                <div class="bottonBottomGrey">
                                                                                    <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                                                                        height="4" class="btnCorner" style="display: none" /></div>
                                                                            </div>
                                                                        </td>
                                                                        <td width="5">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="10">
                                                                &nbsp;
                                                            </td>
                                                            <td id="tdsetType2" align="left" width="121">
                                                            </td>
                                                            <td>
                                                                <table width="100%" id="tblTop" runat="server" border="0" cellpadding="0" cellspacing="0"
                                                                    bgcolor="#DAD8D9">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="right" valign="middle">
                                                                                        <div id="tdTotalCount" style="width: 200px; padding-right: 20px; text-align: right"
                                                                                            runat="server" class="formLabelGrey">
                                                                                        </div>
                                                                                    </td>
                                                                                    <td align="right" width="136" valign="middle" id="tdPagerUp" runat="server">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td align="right" width="36">
                                                                                                    <div id="divDoubleBackUp" runat="server" style="float: left; vertical-align: bottom;
                                                                                                        padding-top: 2px;">
                                                                                                        <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif"
                                                                                                            CommandArgument="First" runat="server" />
                                                                                                    </div>
                                                                                                    <div id="divBackUp" runat="server" style="float: right; vertical-align: bottom; padding-top: 2px;">
                                                                                                        <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif"
                                                                                                            CommandArgument="Prev" runat="server" />
                                                                                                    </div>
                                                                                                </td>
                                                                                                <td width="50" align="center" style="margin-right: 3px;">
                                                                                                    <asp:DropDownList Style="margin-left: 4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged"
                                                                                                        CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td width="30" valign="bottom">
                                                                                                    <div id="divNextUp" runat="server" style="float: left; vertical-align: bottom; padding-top: 2px;">
                                                                                                        <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif"
                                                                                                            CommandArgument="Next" runat="server" />
                                                                                                    </div>
                                                                                                    <div id="divDoubleNextUp" runat="server" style="float: right; vertical-align: bottom;
                                                                                                        padding-top: 2px;">
                                                                                                        <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif"
                                                                                                            CommandArgument="Last" runat="server" />
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                                <Columns>
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px" HeaderStyle-CssClass="gridHdr"
                                                        DataField="CompanyName" SortExpression="CompanyName" HeaderText="Company" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="100px" HeaderStyle-CssClass="gridHdr"
                                                        DataField="Name" SortExpression="Name" HeaderText="Administrator Name" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px" HeaderStyle-CssClass="gridHdr"
                                                        DataField="Email" SortExpression="Email" HeaderText="Administrator Email" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px" HeaderStyle-CssClass="gridHdr"
                                                        DataField="Location" SortExpression="Location" HeaderText="Location" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px" HeaderStyle-CssClass="gridHdr"
                                                        DataField="Phone" SortExpression="Phone" HeaderText="Phone" />
                                                    <asp:TemplateField SortExpression="RefCheckedDate" Visible="false" HeaderText="Reference Checked">
                                                        <HeaderStyle CssClass="gridHdr" Width="50px" Wrap="true" />
                                                        <ItemStyle Wrap="true" HorizontalAlign="Left" Width="15px" CssClass="gridRow" />
                                                        <ItemTemplate>
                                                            <%#SetRefCheckedStatus(Container.DataItem("RefCheckedDate"))%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="ValidatedSpecialists" Visible="false" HeaderText="Has Validated Specialists">
                                                        <HeaderStyle Wrap="true" CssClass="gridHdr" Width="50px" />
                                                        <ItemStyle Wrap="true" HorizontalAlign="Left" Width="15px" CssClass="gridRow" />
                                                        <ItemTemplate>
                                                            <%#SetValidatedSpecialistsStatus(Container.DataItem("ValidatedSpecialists"))%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Rating">
                                                        <HeaderStyle CssClass="gridHdr" Width="50px" Wrap="true" />
                                                        <ItemStyle Wrap="true" HorizontalAlign="Right" Width="15px" CssClass="gridRow" />
                                                        <ItemTemplate>
                                                            <%#Container.DataItem("RatingScore").ToString().Replace(".00","") & "%"%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px" HeaderStyle-CssClass="gridHdr"
                                                        DataField="JobsCompleted" SortExpression="JobsCompleted" HeaderText="Jobs Closed" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="50px" HeaderStyle-CssClass="gridHdr"
                                                        DataField="LastJobDate" HeaderText="Date of last closed WO" SortExpression="LastJobDate" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="50px" HeaderStyle-CssClass="gridHdr"
                                                        DataField="Status" SortExpression="Status" HeaderText="Status" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="50px" HeaderStyle-CssClass="gridHdr"
                                                        DataField="Distance" SortExpression="Distance" HeaderText="Distance" />
                                                    <asp:TemplateField ItemStyle-Width="110px">
                                                        <HeaderStyle CssClass="gridHdr" />
                                                        <ItemStyle Wrap="true" HorizontalAlign="Left" CssClass="gridRow" />
                                                        <ItemTemplate>
                                                            <%#GetLinks(Container.DataItem("BizDivID"), Container.DataItem("CompanyID"), Container.DataItem("ContactID"), Container.DataItem("ClassID"), Container.DataItem("StatusID"), Container.DataItem("RoleGroupID"), Container.DataItem("CompanyName"), Container.DataItem("Name"), Container.DataItem("RefCheckedDate"), Container.DataItem("Email"), Container.DataItem("BusinessArea"))%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="gridRow" />
                                                <RowStyle CssClass="gridRow" />
                                                <PagerStyle BorderWidth="0px" BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                                                <HeaderStyle CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top" Height="40px" />
                                                <FooterStyle BackColor="#D7D7D7" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="SelectFromDB" EnablePaging="True" SelectCountMethod="SelectCount"
                                    TypeName="Admin.SearchAccounts" SortParameterName="sortExpression"></asp:ObjectDataSource>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <div class="gridText">
                                <img align="middle" src="Images/indicator.gif" />
                                <b>Fetching Data... Please Wait</b>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1"
                        CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
                    <!-- InstanceEndEditable -->
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
