Public Partial Class CompanyProfile
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs

    Protected WithEvents UCTopMSgLabel1 As UCTopMSgLabel
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents UCCompanyProfile1 As UCMSCompanyProfile
    Protected WithEvents UCAccountSumm1 As UCAccountSummary

    Shared _CompanyID As Integer
    Public Shared Property CompanyID() As Integer
        Get
            Return _CompanyID
        End Get
        Set(ByVal value As Integer)
            _CompanyID = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        'Security.SetPageControlsByAdmin(Me, GetContact(True, True, Session("ContactCompanyID")).Tables(15).Copy.DefaultView)

        'Security.GetPageControls(Page)
        If Not IsPostBack Then


            If Not IsNothing(Request("companyid")) Then
                If Request("companyid") <> "" Then
                    Session("ContactCompanyID") = Request("companyid")

                End If
            End If
            Dim ComID As String = Request("companyid")
            If Not IsNothing(Request("bizdivid")) Then
                If Request("bizdivid") <> "" Then
                    Session(ComID & "_ContactBizDivID") = Request("bizdivid")
                End If
            End If


            If Not IsNothing(Request("rolegroupid")) Then
                If Request("rolegroupid") <> "" Then
                    Session(ComID & "_ContactRoleGroupID") = Request("rolegroupid")
                End If
            End If

            If Not IsNothing(Request("userid")) Then
                If Request("userid") <> "" Then
                    Session(ComID & "_ContactUserID") = Request("userid")
                End If
            End If

            If Not IsNothing(Request("classid")) Then
                If Request("classid") <> "" Then

                    Session(ComID & "_ContactClassID") = Request("classid")
                End If
            End If

            If Not IsNothing(Request("contacttype")) Then
                If Request("contacttype") <> "" Then
                    Session(ComID & "_ContactType") = Request("contacttype")
                End If
            End If

            If Not IsNothing(Request("statusId")) Then
                If Request("statusId") <> "" Then
                    Session(ComID & "_ContactStatusId") = Request("statusId")
                End If
            End If

            If Not IsNothing(Request("mode")) Then
                If Request("mode") <> "" Then
                    Session(ComID & "_ListingMode") = Request("mode")
                End If
            End If

            ' Code to Show/Hide control for OW Admin
            Dim pagename As String
            pagename = CType(CType(Page.Request.Url, Object), System.Uri).AbsolutePath()
            'pagename = pagename.ToLower.Substring(pagename.LastIndexOf("/") + 1, (pagename.Length - 1) - pagename.LastIndexOf("/"))
            pagename = pagename.Substring(pagename.LastIndexOf("/") + 1, (pagename.Length - 1) - pagename.LastIndexOf("/"))

            Dim dvpriv As DataView = UCMSCompanyProfile.GetContact(Me, Request("bizDivId"), True, True, Request("companyid")).Tables(6).Copy.DefaultView

            dvpriv.RowFilter = "PageName Like('" & pagename & "') "
            Security.GetPageControls(Page, dvpriv)
            dvpriv.RowFilter = ""
            'Following session variable added to fix the issue: Admin Company profile back to listing does not work
            If Not IsNothing(Request("sender")) Then
                Session("CompanyProfileQS") = "?sender=" & Request("sender") & "&bizDivId=" & Request("bizDivId") & "&contactType=" & Request("contactType") & "&companyid=" & Request("companyid") & "&userid=" & Request("userid") & "&classid=" & Request("classid") & "&statusId=" & Request("statusId") & "&rolegroupid=" & Request("rolegroupid") & "&mode=" & Request("mode") & "&ps=" & Request("ps") & "&pn=" & Request("pn") & "&sc=" & Request("sc") & "&so=" & Request("so") & "&fromDate=" & Request("fromDate") & "&toDate=" & Request("toDate")
            End If
        End If
        CompanyID = Request("companyid")
        Dim CompID As String = Request("companyid")
        UCCompanyProfile1.CompanyID = Request("companyid")
        UCCompanyProfile1.BizDivID = Session(CompID & "_ContactBizDivID")
        UCCompanyProfile1.UserID = Session(CompID & "_ContactUserID")
        UCCompanyProfile1.ClassID = Session(CompID & "_ContactClassID")
        'poonam added on 5/4/2015 - Task - OA-230 : OA -Supplier's "Client Blacklist" and "Working days" hyperlink missing when accessing profile from WO
        If Not IsNothing(Request("ClassId")) Then
            Session("ContactClassID") = Request("ClassId")
        End If
        If Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
            UCAccountSumm1.ShowRating = True
            lnkViewBlackListClient.Visible = True
            lnkViewWorkingDays.Visible = True
        ElseIf Session("ContactClassID") = ApplicationSettings.RoleClientID Then
            UCAccountSumm1.ShowRating = False
        End If
        UCAccountSumm1.BizDivId = Session(CompID & "_ContactBizDivID")

        'Following session variable added to fix the issue: Admin Company profile back to listing does not work
        UCCompanyProfile1.strBackToListing = Session(CompID & "_CompanyProfileQS")


        'Prepare AutoMatch Link URL
        lnkAutoMatch.HRef = "AutoMatchSettings.aspx?CompanyID=" & Request("companyid") & "&sender=company" & "&bizDivId=" & Session(CompID & "_ContactBizDivID")
        lnkAccSettings.HRef = "AccountSettings.aspx?CompanyID=" & Request("companyid") & "&sender=company" & "&ClassID=" & Session(CompID & "_ContactClassID") & "&bizDivId=" & Session(CompID & "_ContactBizDivID")
        lnkViewAllInvoices.HRef = "ViewAllInvoices.aspx?CompanyID=" & Request("companyid") & "&sender=company" & "&ClassID=" & Session(CompID & "_ContactClassID") & "&bizDivId=" & Session(CompID & "_ContactBizDivID")
        lnkViewAccountHistory.HRef = "AccountHistory.aspx?CompanyID=" & Request("companyid") & "&sender=company" & "&ClassID=" & Session(CompID & "_ContactClassID") & "&bizDivId=" & Session(CompID & "_ContactBizDivID")
        lnkComments.HRef = "Comments.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Session(CompID & "_ContactBizDivID")
        lnkViewFavSuppliers.HRef = "FavouriteSupplierList.aspx?mode=favourite&CompanyID=" & Request("companyid") & "&bizDivId=" & Session(CompID & "_ContactBizDivID")
        lnkAddProduct.HRef = "AdminProductList.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId")
        lnkAddProductNew.HRef = "ServiceListing.aspx?CompanyID=" & Request("CompanyID")
        lnkViewBlackListsupplier.HRef = "FavouriteSupplierList.aspx?mode=blacklist&CompanyID=" & Request("companyid") & "&bizDivId=" & Session(CompID & "_ContactBizDivID")
        lnkReferences.HRef = "References.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Session(CompID & "_ContactBizDivID")
        lnkuser.HRef = "Specialists.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Session(CompID & "_ContactBizDivID")
        'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        lnkloc.HRef = "Locations.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Session(CompID & "_ContactBizDivID") & "&classId=" & Request("classId")
        lnkAddUsers.HRef = "Specialists.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Session(CompID & "_ContactBizDivID")
        lnkAddLocationsSupp.HRef = "Locations.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Session(CompID & "_ContactBizDivID") & "&classId=" & Request("classId")
        lnkViewBlackListClient.HRef = "FavouriteSupplierList.aspx?mode=ClientBlacklist&CompanyID=" & Request("companyid") & "&bizDivId=" & Session(CompID & "_ContactBizDivID")
        lnkViewWorkingDays.HRef = "WorkingDays.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Session(CompID & "_ContactBizDivID")

    End Sub

    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetAccreditationsAutoSuggest(ByVal prefixText As String, ByVal mode As String, ByVal CommonID As String) As DataTable
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest(mode, prefixText, CInt(CommonID))
        Return dt
    End Function
End Class