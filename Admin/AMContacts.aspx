<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AMContacts.aspx.vb" Inherits="Admin.AMContacts" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :Contacts" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


              <div id="divContent">

         

            

        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>

        

             <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">

         

         <tr>

            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->

            <input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />

            <input id="hdnContactType" type="hidden" name="hdnContactType" runat="server">                   

            <input id="hdnPageNo"  value="1" type="hidden" name="hdnPageNo" runat="server">

                  <input id="hdnPageSize" value="25" type="hidden" name="hdnPageSize" runat="server">

                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate> 

                                <table width="100%" cellpadding="0" cellspacing="0">

                                    <tr>

                                    <td width="10px"></td>

                                    <td><asp:Label ID="lblMessage" CssClass="HeadingRed"  runat="server"></asp:Label></td>

                                    </tr>

                                    </table>

                              <table width="100%" cellpadding="0" cellspacing="0" border="0">

                                <tr>

                                 <td width="10px"></td>

                                 <td width="300"><table width="100%" cellpadding="0" cellspacing="0" border="0">

                                     <tr>

                                       <td width="350">

                                           <%@ Register TagPrefix="uc1" TagName="UCDateRange" Src="~/UserControls/UK/UCDateRangeUK.ascx" %>

                                           <uc1:UCDateRange id="UCDateRange1" runat="server"></uc1:UCDateRange> 

                                       </td>

                                      </tr>

                                    </table>

                                 </td>

                                 <td align="left" width="60px">
                                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                        <asp:LinkButton ID="btnView" causesValidation=False runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
                                    </div>
                               </td>

                               <td width="20px"></td>

                               <td align="left">
                                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                        <a runat="server" target="_blank" id="btnExport" class="txtButtonRed"> &nbsp;Export to Excel&nbsp;</a>
                                    </div>
                                </td>

            </tr>

         </table>

            <table >

            <tr>

            <td class="paddingL30 padB21" valign="top" padB21 paddingTop1>

            <asp:Label ID="lblMsg" runat="server" CssClass="bodytxtValidationMsg"></asp:Label>                

            </td>

            </tr>

            </table>                      

                        

                 

                                          

                  

                        

                               

                               

                  <hr style="background-color:#993366;height:5px;margin:0px;" />

                  

                    <asp:Panel ID="pnlListing" runat="server">    

                                <asp:GridView ID="gvContacts"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'

                         BorderColor="White"  dataKeyNames="ContactID" PagerSettings-Mode=NextPreviousFirstLast   PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          

               <EmptyDataTemplate>

               <table  width="100%" border="0" cellpadding="10" cellspacing="0">                            

                                    <tr>

                                          <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.

                                          </td>

                                    </tr>                   

                              </table>

                  

               </EmptyDataTemplate>

               <PagerTemplate>

                   

                  

                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">

                                    <tr>

                                          <td runat="server" id="tdsetType1" width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">

                                            <TR>

                                                <TD class="txtListing" align="left"><strong>

                                                  <asp:CheckBox id="chkSelectAll" runat="server" onclick="CheckAll(this,'Check')" value="checkbox" Text="Select all" valign="middle"></asp:CheckBox>

                                                </strong> </TD>

                                                <TD width="5"></TD>

                                            </TR>

                                          </TABLE>                                  

                                          </td>

                                          

                                          <td runat="server"  id="tdApprove" width="70" align="left" class="txtWelcome paddingL10"><div id="divButton" style="width: 70px;  ">

                                                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                                           

                                                <asp:LinkButton OnClick="ApproveContact" class="buttonText" style="cursor:hand;" id="btnApprove"  runat=server><strong>Approve</strong></asp:LinkButton>

                               <cc2:ConfirmButtonExtender ID="ConfirmBtnApprove" TargetControlID="btnApprove" ConfirmText="Do you want to change the Supplier type?" runat="server">

                                </cc2:ConfirmButtonExtender> 

                                

                                                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>

                                          </div></td>

                                          <td width="10">&nbsp;</td>

                                          <td runat="server"  id="tdSuspend" width="75" align="right" class="txtWelcome paddingL10"><div id="divButton" style="width: 75px;  ">

                                                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                                           

                                                <asp:LinkButton OnClick="SuspendContact" class="buttonText" style="cursor:hand;" id="btnSuspend"  runat=server><strong>Suspend</strong></asp:LinkButton>

                                 <cc2:ConfirmButtonExtender TargetControlID="btnSuspend" ConfirmText="Do you want to change the Supplier type?" ID="ConfirmBtnSuspend" runat="server">

                                </cc2:ConfirmButtonExtender>

                                                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>

                                      </div></td>

                                      <td width="10">&nbsp;</td>

                                      <td runat="server" id="tdNew" width="75" align="right" class="txtWelcome paddingL10"><div id="divButton" style="width: 45px;  ">

                                                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                                           

                                                <asp:LinkButton OnClick="NewContact" class="buttonText" style="cursor:hand;" id="btnNew"  runat=server><strong>New</strong></asp:LinkButton>

                             <cc2:ConfirmButtonExtender  TargetControlID="btnNew" ConfirmText="Do you want to change the Supplier type?" ID="ConfirmBtnNew" runat="server">

                              </cc2:ConfirmButtonExtender>   

                                                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>

                                      </div></td>

                                      <td id="tdsetType2" align="left" width="121">

                                                

                                          </td>

                                          <td>

                                                <table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">

                                                  <tr>

                                                      <td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >

                                                            <TR>

                                                                  <td align="right" valign="middle">

                                                                    <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>

                                                                  </td>

                                                                  <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">

                                                                  <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                                                        <tr>

                                                                        <td align="right" width="36">

                                                                        <div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />

                                                                              </div>      

                                                                              <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                              <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />

                                                                              </div>                                                                                                                                             

                                                                        </td>

                                                                        <td width="50" align="center" style="margin-right:3px; ">                                                                              

                                                                              <asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">

                                                    </asp:DropDownList>

                                                                        </td>

                                                                        <td width="30" valign="bottom">

                                                                              <div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />

                                                                              </div>

                                                                              <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">

                                                                               <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />

                                                                              </div>

                                                                        </td>

                                                                        </tr>

                                                                  </table>

                                                                  </td>

                                                            </TR>

                                                      </TABLE></td>

                                                  </tr>

                                                </table>

                                      </td>

                                    </tr>

                              </table> 

                   

        </PagerTemplate> 

               <Columns>  

                            

               <asp:TemplateField>               

               <HeaderStyle CssClass="gridHdr" />

               <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="gridRow"/>

               <ItemTemplate>             

               <asp:CheckBox ID="Check" runat="server" ></asp:CheckBox>

                <input type=hidden runat=server id="hdnMainContactId"  value='<%#Container.DataItem("CompanyID")%>'/>

               </ItemTemplate>

               </asp:TemplateField>                

 

                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="50px"  HeaderStyle-CssClass="gridHdr"  DataField="Status" SortExpression="Status" HeaderText="Class" />

                <asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="80px" HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="DateCreated" SortExpression="DateCreated" HeaderText="Registered" />                    

                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="65px"   HeaderStyle-CssClass="gridHdr" DataField="CompanyName" SortExpression="CompanyName" HeaderText="Company" />                

                <asp:TemplateField SortExpression="Name" HeaderText="Contact Person">                

                <HeaderStyle CssClass="gridHdr" />

                <ItemStyle  HorizontalAlign=Left Wrap=false  CssClass="gridRow"/>

                <ItemTemplate>             

               <%#Container.DataItem("Name") & "-" & "<a class=footerTxtSelected href=mailto:" & Container.DataItem("Email") & ">" & Container.DataItem("Email") & "</a>"%>

                </ItemTemplate>

               </asp:TemplateField>                

                 <asp:TemplateField HeaderText="Contact Info" SortExpression="Phone" Visible=false>

                <HeaderStyle CssClass="gridHdr" />

                <ItemStyle Wrap=true HorizontalAlign=Left  CssClass="gridRow"/>

                <ItemTemplate>             

                <%#  "Phone: " & Container.DataItem("Phone") & "<br/> Mobile: " & Container.DataItem("Mobile") & "<br/>Fax: " & Container.DataItem("Fax") & "<br/>"

                    %>

                </ItemTemplate>

               </asp:TemplateField>        

               <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="65px"  HeaderStyle-CssClass="gridHdr" Visible=false  DataField="Location" SortExpression="Location" HeaderText="Location" />                

                <asp:TemplateField Visible=false HeaderText="Type" SortExpression="ClassID">

                <HeaderStyle CssClass="gridHdr" />

                <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="gridRow"/>

                <ItemTemplate> 

                <%#GetUserType(Container.DataItem("ClassID"))%>

                </ItemTemplate>

               </asp:TemplateField>     

               <asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="80px"  HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="ActionDate" SortExpression="ActionDate" Visible=false HeaderText="Approved On" />                                   

               <asp:TemplateField ItemStyle-Width="65px"  HeaderText="Rating" >

                 <HeaderStyle CssClass="gridHdr" />

                <ItemStyle Wrap=true HorizontalAlign=Right CssClass="gridRowHighlight"/>

                <ItemTemplate> 

                <%#Container.DataItem("RatingScore").ToString().Replace(".00", "") & "%"%>

                </ItemTemplate>

               </asp:TemplateField>

               <asp:TemplateField HeaderText="Source"   SortExpression="Source" ItemStyle-Width="95">                

                <HeaderStyle CssClass="gridHdr" />

                <ItemStyle Wrap=true HorizontalAlign=Left     CssClass="gridRow"/>

                <ItemTemplate> 

                <%#GetSource(Container.DataItem("Source"))%>

                </ItemTemplate>

               </asp:TemplateField>  

               <asp:TemplateField ItemStyle-Width="100px">                

                <ItemStyle Wrap=true HorizontalAlign=Left   CssClass="gridRow"/>

                <ItemTemplate> 
                <span style='cursor: hand;' onmouseout="javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>','hidden')"
                            onmouseover="javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>','visible')">
                <%#GetLinks(Container.DataItem("BizDivID"), Container.DataItem("CompanyID"), Container.DataItem("ContactID"), Container.DataItem("ClassID"), Container.DataItem("statusId"), Container.DataItem("RoleGroupID"), Container.DataItem("Status"),Container.DataItem("Email"),Container.DataItem("CompanyName"),Container.DataItem("Name"),Container.DataItem("ActionDate"))%>
                 </span>
                  <div class="divUserNotesDetail" id='divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>'
                            onmouseout="javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>','hidden')"
                            onmouseover="javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>','visible')">
                            <asp:Panel runat="server" Style="color: black; width: 230px; max-height: 320px; overflow: scroll;"
                                ID="pnlUserNotesDetail" visible='<%# IIF(Container.DataItem("NtCnt") = 0,False,True) %>'>
                                <div>
                                    <b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
                                </div>
                                <div class="clsRatingInner">
                                      <div id="divNotes" runat="server" visible='<%# IIF(Container.DataItem("NtCnt") = 0,False,True) %>'>
                                        <b style="color: #0C96CF; font-size: 12px;">Notes:</b>
                                        <asp:Repeater ID="DLUserNotesDetail" runat="server" DataSource='<%#GetUserNotesDetail(Container.DataItem("CompanyID"))%>'>
                                            <ItemTemplate>
                                                <div style='visible: <%# iif( Trim(Container.DataItem("Comments")) = "" , "hidden", "visible") %>'>
                                                    <div>
                                                        <%--poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2 --%>
                                                        <%# "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - <b>" & Container.DataItem("OWRepFName") & " " & Container.DataItem("OWRepLName") & "</b><br/><b>" & Container.DataItem("NoteCategory") & "</b></span>"%>
                                                    </div>
                                                    <div style="margin-top: 0px; margin-bottom: 10px;">
                                                        <%# Container.DataItem("Comments") %>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                 
                                </div>
                                <div>
                                    <b class="b4"></b><b class="b3"></b><b class="b2"></b><b class="b1"></b>
                                </div>
                            </asp:Panel>
                        </div>
                </ItemTemplate>

               </asp:TemplateField>    

                </Columns>

                 <AlternatingRowStyle CssClass=gridRow />

                 <RowStyle CssClass=gridRow />            

                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />

                <HeaderStyle  CssClass="gridHdr" HorizontalAlign=Left VerticalAlign=Top  Height="40px" />

                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />

        

              </asp:GridView>

              </asp:Panel>

              <asp:Panel ID="pnlConfirmSendMail" runat="server" Visible="false">

               <div class="tabDivOuter" id="divMainTab">

                <div class="roundtopLightGreyCurve"><img src="Images/Curves/LightGrey-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>

                <div class="padRight20Left20" id="divAccount" runat="server">

              <table width="100%" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                      <td height="56" align="center" valign="top" class="HeadingSmallBlack"><asp:Literal ID="litMessage" runat="server" /></td>

                    </tr>

                  </table>

                  <table width="100%" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                      <td width="475" align="right"><table cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">

                          <tr>

                            <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>

                            <td class="txtBtnImage"><asp:LinkButton id="btnSendMail" runat="server" CausesValidation="false"  class="txtListing" TabIndex="1" > <img src="Images/Icons/Icon-Confirm.gif" width="12" height="12" hspace="3" vspace="3" align="absmiddle" border="0">&nbsp;Send Mail&nbsp;</asp:LinkButton></td>

                            <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>

                          </tr>

                      </table></td>

                      <td width="10">&nbsp;</td>

                      <td align="left"><table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">

                          <tr>

                            <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>

                            <td align="center" class="txtBtnImage"><asp:LinkButton id="btnCancel" CausesValidation="false"  runat="server" class="txtListing" tabindex="2"> <strong><img src="Images/Icons/Icon-Cancel.gif" width="11" height="11" border="0" hspace="3">&nbsp;Cancel&nbsp;</strong></asp:LinkButton>                            

                            <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>

                          </tr>

                      </table></td>

                    </tr>

                  </table>

                  <p>&nbsp;</p>

                  <p class="txtNavigation"><strong>Can edit the text that is sent to the Supplier via email.</strong><b>Please do not remove text "<AccountStatus>" - this will be replaced with the status to which users account has been changed.</b> </p>

                  <asp:TextBox TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" ID="txtMessage" runat="server" CssClass="formFieldGrey700" Width="728" Height="50" style="width:950px; height:50px;"></asp:TextBox>

                  <p>&nbsp;</p>

                  <asp:DataGrid BorderWidth="1px" BorderStyle="Solid" BorderColor="#FFFFFF" CellPadding="5" CellSpacing="0" Width="100%" AutoGenerateColumns="false" ID="DGSuppliers" runat="server" AllowPaging="False" AllowSorting="True" HeaderStyle-ForeColor="#555354">

                    <Columns>                         

                    <asp:TemplateColumn HeaderStyle-CssClass="gridHdr" ItemStyle-Width="15px" ItemStyle-Wrap="true"

                                    ItemStyle-HorizontalAlign="left" ItemStyle-CssClass="gridRow" HeaderText="<input id='chkUpdateAll' checked onClick=CheckAll(this,'Check') type='checkbox' name='chkUpdateAll' />">

                      <ItemTemplate >

                        <asp:CheckBox ID="Check" Checked="true" Runat="server" ></asp:CheckBox>

                        <input type="hidden" Runat="server" id="ContactID" value=<%# databinder.eval(container.dataitem, "ContactLinkId") %>/>

                      </itemtemplate>

                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderStyle-CssClass="gridHdr" ItemStyle-Wrap="true" ItemStyle-CssClass="gridRow" HeaderText="Name">

                      <itemtemplate> <%# container.dataitem("FName") + " " +  container.dataitem("LName") %> 

                                 <input type="hidden" Runat="server" id="Name" value=<%# container.dataitem("FName") + " " +  container.dataitem("LName") %>  />

                                </itemtemplate>

                    </asp:TemplateColumn>

                              <asp:TemplateColumn HeaderStyle-CssClass="gridHdr" ItemStyle-Wrap="true" ItemStyle-CssClass="gridRow" HeaderText="EMail" SortExpression="EMail">

                      <itemtemplate> <%# container.dataitem("EMail") %> 

                                 <input type="hidden" Runat="server" id="Email" value=<%# container.dataitem("EMail")%>  />

                                </itemtemplate>

                    </asp:TemplateColumn>

                    <asp:BoundColumn ItemStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr" DataField="Location" HeaderText="Location" SortExpression="Location"></asp:BoundColumn>

                    </columns>

                  </asp:DataGrid>

                  <p>&nbsp; </p>

                   </div>

              </div>

              </asp:Panel>

              

                         </ContentTemplate>

                         <Triggers>

                            <asp:AsyncPostBackTrigger   ControlID="btnView"  EventName="Click" /> 

                              <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnSendMail" />

                              <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnCancel" />                  

                         </Triggers>

            

            </asp:UpdatePanel>

            

                <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">

                <ProgressTemplate >

                        <div class="gridText">

                               <img  align=middle src="Images/indicator.gif" />

                               <b>Fetching Data... Please Wait</b>

                        </div>   

                        </ProgressTemplate>

                </asp:UpdateProgress>

                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />

             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"

                SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.AMContacts" SortParameterName="sortExpression">                

            </asp:ObjectDataSource>

                                            

                              

            <!-- InstanceEndEditable --></td>

                     

             

         </tr>

         </table>

      </div>
</asp:Content>

 
