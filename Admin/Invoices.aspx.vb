
Partial Public Class Invoices
    Inherits System.Web.UI.Page
    Protected WithEvents UCDateRange1 As UCDateRange
    Protected WithEvents UCSubMenu1 As UCSubMenu
    Protected WithEvents UCMenu1 As UCMenu
    Protected WithEvents lnkBackToListing As Global.System.Web.UI.HtmlControls.HtmlAnchor

    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            lnkBackToListing.HRef = getBackToListingLink()
            If IsNothing(ViewState("selectedTab")) Then
                ViewState("selectedTab") = "Current"
                tdtabCurrent.Attributes.Add("class", "ActiveLinkRed")
                CType(Master.FindControl("ContentPlaceHolder1").FindControl("divCurrent"), System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("style", "cursor:pointer;-moz-border-radius: 10px 10px 0px 0px; border:1px solid #993366 !important; height:26px; border-radius:10px 10px 0px 0px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100%;")
                'change colour to purple
            End If
            If Request("sender") = "Invoices" Then
                ProcessBackToListing()
            Else
                Dim currDate As DateTime = System.DateTime.Today

                UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
                ViewState("fromDate") = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
                UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
                ViewState("toDate") = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
                ViewState!SortExpression = "InvoiceDate"
                gvInvoices.Sort(ViewState!SortExpression, SortDirection.Ascending)
                'prepare export to excel link
                'status
                Dim status As String
                Select Case ViewState("selectedTab").ToString.ToLower
                    Case "current"
                        status = "Unpaid"
                        UCDateRange1.Visible = False
                        tblView.Visible = False
                        ViewState("fromDate") = ""
                        ViewState("toDate") = ""
                    Case "history"
                        status = "Paid"
                        UCDateRange1.Visible = True
                        tblView.Visible = True
                End Select

                btnExport.HRef = "ExportToExcel.aspx?page=Invoices&bizDivId=" & Session("BizDivId") & "&adviceType=Sales&CompanyID=" & Request("CompanyID") & "&fromDate=" & ViewState("fromDate") & "&toDate=" & ViewState("toDate") & "&status=" & status & "&sortExpression=" & gvInvoices.SortExpression


            End If
        End If
    End Sub
    Public Sub SwitchTabs(ByVal sender As Object, ByVal e As System.EventArgs)

        Page.Validate()
        If Page.IsValid Then
            'Inactive previously selected link
            CType(Master.FindControl("ContentPlaceHolder1").FindControl("tdtab" + ViewState("selectedTab")), System.Web.UI.HtmlControls.HtmlTableCell).Attributes.Add("class", "NonActiveLink")
            CType(Master.FindControl("ContentPlaceHolder1").FindControl("div" + ViewState("selectedTab")), System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("style", "cursor:pointer;-moz-border-radius: 10px 10px 0px 0px; border:1px solid #CCCCCC !important; height:26px; border-radius:10px 10px 0px 0px;background-color:#CCCCCC !important;color:#FFFFFF;text-align:center;width:100%;")
            Select Case CType(sender, LinkButton).Text.ToLower
                Case "current"
                    ViewState("selectedTab") = "Current"
                    UCDateRange1.Visible = False
                    tblView.Visible = False
                    ViewState("fromDate") = ""
                    ViewState("toDate") = ""
                Case "history"
                    ViewState("selectedTab") = "History"
                    UCDateRange1.Visible = True
                    tblView.Visible = True
                    Dim currDate As DateTime = System.DateTime.Today
                    UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
                    ViewState("fromDate") = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
                    UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
                    ViewState("toDate") = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
            End Select
            'Active currently selected link
            CType(Master.FindControl("ContentPlaceHolder1").FindControl("tdtab" + ViewState("selectedTab")), System.Web.UI.HtmlControls.HtmlTableCell).Attributes.Add("class", "ActiveLinkRed")
            CType(Master.FindControl("ContentPlaceHolder1").FindControl("div" + ViewState("selectedTab")), System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("style", "cursor:pointer;-moz-border-radius: 10px 10px 0px 0px; border:1px solid #993366 !important; height:26px; border-radius:10px 10px 0px 0px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100%;")
            PopulateGrid()
        End If
    End Sub
    Public Sub PopulateGrid()
        gvInvoices.DataBind()
        Dim status As String = ""
        Dim bizDivId As Integer = Session("BizDivId")

        'status
        Select Case ViewState("selectedTab").ToString.ToLower
            Case "current"
                status = "Unpaid"
            Case "history"
                status = "Paid"
        End Select

        btnExport.HRef = "ExportToExcel.aspx?page=Invoices&bizDivId=" & Session("BizDivId") & "&adviceType=Sales&CompanyID=" & Request("CompanyID") & "&fromDate=" & ViewState("fromDate") & "&toDate=" & ViewState("toDate") & "&status=" & status & "&sortExpression=" & gvInvoices.SortExpression
    End Sub
    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkView.Click
        Page.Validate()
        If Page.IsValid Then
            ViewState("fromDate") = UCDateRange1.txtFromDate.Text
            ViewState("toDate") = UCDateRange1.txtToDate.Text
            Select Case ViewState("selectedTab").ToString.ToLower
                Case "current"
                    UCDateRange1.Visible = False
                    tblView.Visible = False
                    ViewState("fromDate") = ""
                    ViewState("toDate") = ""
                Case "history"
                    UCDateRange1.Visible = True
                    tblView.Visible = True
            End Select
            PopulateGrid()
        End If
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer),
                                      ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        Dim adviceType As String = "Sales"
        Dim status As String = ""
        Dim bizDivId As Integer = Session("BizDivId")

        'status
        Select Case ViewState("selectedTab").ToString.ToLower
            Case "current"
                status = "Unpaid"
            Case "history"
                status = "Paid"
        End Select
        ds = ws.WSFinance.MS_GetSalesReceiptAdviceListing(bizDivId, adviceType, Request("CompanyID"), status, ViewState("fromDate"), ViewState("toDate"), "admin", sortExpression, startRowIndex, maximumRows, "", "")
        If ds.Tables(0).Columns.Contains("AllocatedTo") = True Then
            ViewState("AllocatedToExists") = "True"
        Else
            ViewState("AllocatedToExists") = "False"
        End If
        Session.Add("dsSalesReceiptAdviceListing", ds)

        ViewState("rowCount") = ds.Tables("tblCount").Rows(0)(0)

        Return ds
    End Function

    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Private Sub gvInvoices_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvInvoices.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True
            End If
        End If

    End Sub

    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvInvoices.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvInvoices, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex



    End Sub

    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvInvoices.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvInvoices.DataBind()
    End Sub



    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvInvoices.RowDataBound
        MakeGridViewHeaderClickable(gvInvoices, e.Row)
    End Sub
    Public Function GetLinks(ByVal invoiceNo As String, ByVal companyId As String) As String
        Dim link As String = ""
        Dim linkParams As String = ""

        linkParams &= "invoiceNo=" & invoiceNo
        linkParams &= "&companyId=" & companyId
        linkParams &= "&bizDivId=" & Session("BizDivId")
        linkParams &= "&FromDate=" & ViewState("fromDate")
        linkParams &= "&ToDate=" & ViewState("toDate")
        linkParams &= "&PS=" & gvInvoices.PageSize
        linkParams &= "&PN=" & gvInvoices.PageIndex
        linkParams &= "&SC=" & gvInvoices.SortExpression
        linkParams &= "&SO=" & gvInvoices.SortDirection
        linkParams &= "&tab=" & ViewState("selectedTab")
        linkParams &= "&sender=Invoices"


        Dim ds As DataSet = New DataSet
        Dim dv As DataView = New DataView
        ds = Session("dsSalesReceiptAdviceListing")
        dv = ds.Tables(0).DefaultView
        dv.RowFilter = "InvoiceNo = '" & invoiceNo & "'"

        If Not IsNothing(ViewState("AllocatedToExists")) Then
            If ViewState("AllocatedToExists").ToString = "True" Then

                If Not IsDBNull(dv.Item(0)("AllocatedTo")) Then
                    link &= "<a runat=server target='_blank' id='lnkViewCreditNote' href='ViewCreditNote.aspx?invoiceNo=" & invoiceNo & "&bizDivId=" & Session("BizDivId") & "&companyID=" & companyId & "'><img src='Images/Icons/View-All-WO-SI.gif' alt='View Credit Notes' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
                Else
                    link &= "<a target='_blank' href=' ViewSalesInvoice.aspx?" & linkParams & "'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Invoices' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
                End If

            Else
                link &= "<a target='_blank' href=' ViewSalesInvoice.aspx?" & linkParams & "'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Invoices' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
            End If
        Else
            link &= "<a target='_blank' href=' ViewSalesInvoice.aspx?" & linkParams & "'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Invoices' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        End If


        If ViewState("selectedTab").ToString.ToLower = "history" Then

            If dv.Item(0)("InvoiceType") = "Credit Note" Then
                link &= "<a target='_blank' href='ViewCashReceipt.aspx?ReceiptType=" & dv.Item(0)("AllocToInvType") & "&invoiceNo=" & dv.Item(0)("AllocatedTo") & "&bizDivId=" & Session("BizDivId") & "&companyID=" & companyId & "'><img src='Images/Icons/View-Cash-Receipt.gif' alt='View Receipt' width='15' height='14' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
            Else
                link &= "<a href='ViewCashReceiptsCreditNotes.aspx?" & linkParams & "'><img src='Images/Icons/View-Cash-Receipt.gif' alt='View Cash Receipt' width='15' height='14' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
            End If

        End If
        If ViewState("selectedTab").ToString.ToLower = "current" Then
            link &= "<a runat='server' href='SalesInvoiceWOListing.aspx?" & linkParams & "'><img runat='server' src='Images/Icons/View-All-WO-SI.gif' alt='view Work Orders' width='14' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        End If

        Return link

    End Function
    Public Sub WithDrawFunds(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMsg.Text = ""
        Dim invoiceNumbers As String = CommonFunctions.getSelectedIdsOfGrid(gvInvoices, "Check", "hdnInvoiceNumber")
        If invoiceNumbers = "" Then
            lblMsg.Text = "Please Select atleast one Available purchase invoice."
            Return
        End If
        Dim ds As New DataSet
        ds = ws.WSFinance.MSAccounts_ChangePIStatusToRequested(Session("BizDivId"), invoiceNumbers, Request("CompanyID"), Session("UserId"))
        If ds.Tables(0).Rows(0).Item("Success") = 0 Then
            lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
        End If
        gvInvoices.DataBind()
    End Sub
    Public Sub ProcessBackToListing()



        ViewState("selectedTab") = Request("tab")
        ViewState("fromDate") = Request("FromDate")
        ViewState("toDate") = Request("ToDate")
        UCDateRange1.txtFromDate.Text = Request("FromDate")
        UCDateRange1.txtToDate.Text = Request("ToDate")

        Select Case ViewState("selectedTab").ToString.ToLower
            Case "current"
                UCDateRange1.Visible = False
                tblView.Visible = False
                ViewState("fromDate") = ""
                ViewState("toDate") = ""
            Case "history"
                UCDateRange1.Visible = True
                tblView.Visible = True
        End Select

        If Request("sender") = "Invoices" Then
            gvInvoices.PageIndex = Request("PN")
            gvInvoices.Sort(Request("SC"), Request("SO"))
        Else
            gvInvoices.PageIndex = 0
            gvInvoices.Sort("InvoiceNo", 0)
        End If
        PopulateGrid()
        If ViewState("selectedTab").ToString.ToLower = "history" Then
            'Active currently selected link
            CType(Master.FindControl("ContentPlaceHolder1").FindControl("tdtab" + ViewState("selectedTab")), System.Web.UI.HtmlControls.HtmlTableCell).Attributes.Add("class", "ActiveLinkRed")
            CType(Master.FindControl("ContentPlaceHolder1").FindControl("imgtab" + ViewState("selectedTab") + "Left"), System.Web.UI.HtmlControls.HtmlImage).Attributes.Add("src", "Images/Curves/RedTab-Left.gif")
            CType(Master.FindControl("ContentPlaceHolder1").FindControl("imgtab" + ViewState("selectedTab") + "Right"), System.Web.UI.HtmlControls.HtmlImage).Attributes.Add("src", "Images/Curves/RedTab-Right.gif")
            'Inactive previously selected link
            CType(Master.FindControl("ContentPlaceHolder1").FindControl("tdtabCurrent"), System.Web.UI.HtmlControls.HtmlTableCell).Attributes.Add("class", "NonActiveLink")
            'Change Colour To Grey
            CType(Master.FindControl("ContentPlaceHolder1").FindControl("divHistory"), System.Web.UI.HtmlControls.HtmlGenericControl).Attributes.Add("style", "cursor:pointer;-moz-border-radius: 10px 10px 0px 0px; border:1px solid #CCCCCC !important; height:26px; border-radius:10px 10px 0px 0px;background-color:#CCCCCC !important;color:#FFFFFF;text-align:center;width:100%;")

        End If
    End Sub
    Public Function getBackToListingLink() As String 

        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender")
                Case "AdminContactsListing"
                    link = "~\AMContacts.aspx?"
                    link &= "CompanyID=" & Request("CompanyID")
                    link &= "&contactType=" & Request("contactType")
                    link &= "&userid=" & Request("userid")
                    link &= "&classid=" & Request("classid")
                    link &= "&statusId=" & Request("statusId")
                    link &= "&adviceFor=" & Request("adviceFor")                    
                    link &= "&ps=" & Request("ps")
                    link &= "&pn=" & Request("pn")
                    link &= "&sc=" & Request("sc")
                    link &= "&so=" & Request("so")
                    link &= "&fromDate=" & Request("fromDate")
                    link &= "&toDate=" & Request("toDate")
                    link &= "&sender=" & "AdminContactsListing"
                Case "AdminSearchAccounts"
                    link = "~\SearchAccounts.aspx?"
                    link &= "bizDivId=" & Request("bizDivId")
                    link &= "&companyId=" & Request("companyId")
                    link &= "&contactId=" & Request("contactId")
                    link &= "&classId=" & Request("classId")
                    link &= "&statusId=" & Request("statusId")
                    If Not IsNothing(Request("vendorIDs")) Then
                        link &= "&vendorIDs=" & Request("vendorIDs")
                    End If

                    link &= "&roleGroupId=" & Request("roleGroupId")
                    link &= "&product=" & Request("product")
                    link &= "&region=" & Request("region")
                    link &= "&txtPostCode=" & Request("txtPostCode")
                    link &= "&txtKeyword=" & Request("txtKeyword")
                    link &= "&refcheck=" & Request("refcheck")
                    link &= "&txtFName=" & Request("txtFName")
                    link &= "&txtLName=" & Request("txtLName")
                    link &= "&txtCompanyName=" & Request("txtCompanyName")
                    link &= "&txtEmail=" & Request("txtEmail")
                    link &= "&Email=" & Request("Email")
                    link &= "&Name=" & Request("Name")
                    link &= "&sender=" & "AdminSearchAccounts"
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    'Keep contact name at last
                    link &= "&companyname=" & Request("companyname")
                    link &= "&contactname=" & Request("contactname")
                    link &= "&SkillsArea=" & Request("SkillsArea")
                Case Else
                    Return ""
            End Select
        End If
        Return link
    End Function

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
End Class