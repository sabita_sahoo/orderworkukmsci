﻿Public Class ProfessionalServiceWOReport
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected WithEvents UCMenuMainAdmin As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents lnkExportReportExcel As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lnkExportReportPdf As System.Web.UI.WebControls.LinkButton
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            'Populating Status dropdown
            Dim dsStatus As DataSet
            Dim dvStatus As DataView
            dsStatus = CommonFunctions.GetWoStandards(Page, "Status")
            If dsStatus.Tables(0).Rows.Count > 0 Then
                dvStatus = dsStatus.Tables(0).Copy.DefaultView
                dvStatus.Sort = "StandardValue"
                drpStatus.DataSource = dvStatus
                drpStatus.DataTextField = "StandardValue"
                drpStatus.DataValueField = "StandardValue"
                drpStatus.DataBind()

                Dim liStatus As New ListItem
                'liStatus = New ListItem
                liStatus.Text = "Select Status"
                liStatus.Value = ""
                drpStatus.Items.Insert(0, liStatus)
            End If
            'Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range
            Session("dsSelectedCompany") = Nothing
        End If
    End Sub

    Private Sub lnkBtnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnGenerateReport.Click
        Try
            generateReport()
        Catch Ex As Exception

        End Try
    End Sub
    Private Sub lnkExportReportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportReportExcel.Click
        Dim StrCompanyID As String = ""
        Dim warnings As Microsoft.Reporting.WebForms.Warning()
        Dim streamids As String()
        Dim mimeType As String
        Dim encoding As String
        Dim fileType As String = "EXCEL"
        Dim extension As String = "xls"
        Dim fileName As String = "OWReport"

        Dim objParameter(6) As Microsoft.Reporting.WebForms.ReportParameter

        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/ProfessionalServiceWO"

        objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("StartDate", txtStartDate.Text.ToString.Trim)
        objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("EndDate", txtEndDate.Text.ToString.Trim)
        objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("PONumber", txtPONumber.Text.ToString.Trim)
        objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("SubmittedDateFrom", txtSubmittedDateFrom.Text.ToString.Trim)
        objParameter(4) = New Microsoft.Reporting.WebForms.ReportParameter("SubmittedDateTo", txtSubmittedDateTo.Text.ToString.Trim)
        objParameter(5) = New Microsoft.Reporting.WebForms.ReportParameter("Status", drpStatus.Items(drpStatus.SelectedIndex).Value.ToString.Trim)
        'Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range
        getCompanyName()
        Dim i As Integer
        Dim CompanyName As String = ViewState("SrcCompanyName")
        If CompanyName <> "" Then
            Dim strCompanyName() As String = Split(CompanyName, ",")
            CompanyName = ""
            For i = 0 To strCompanyName.GetLength(0) - 1
                If (strCompanyName.Length > 1) Then
                    If CompanyName <> "" Then
                        CompanyName &= ","
                    End If
                    CompanyName &= "'" & CStr(strCompanyName.GetValue(i)) & "'"
                Else
                    CompanyName = strCompanyName(0).ToString
                End If
            Next
        End If

        objParameter(6) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyName", CompanyName)
        ReportViewerOW.ServerReport.SetParameters(objParameter)


        Dim bytes As Byte() = ReportViewerOW.ServerReport.Render(fileType, Nothing, mimeType, encoding, extension, streamids, warnings)
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=RevenuebyBillingLocation." + extension)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub
    Private Sub lnkExportReportPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportReportPdf.Click
        Dim StrCompanyID As String = ""
        Dim warnings As Microsoft.Reporting.WebForms.Warning()
        Dim streamids As String()
        Dim mimeType As String
        Dim encoding As String
        Dim fileType As String = "PDF"
        Dim extension As String = "pdf"
        Dim fileName As String = "OWReport"


        Dim objParameter(6) As Microsoft.Reporting.WebForms.ReportParameter

        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/ProfessionalServiceWO"

        objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("StartDate", txtStartDate.Text.ToString.Trim)
        objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("EndDate", txtEndDate.Text.ToString.Trim)
        objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("PONumber", txtPONumber.Text.ToString.Trim)
        objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("SubmittedDateFrom", txtSubmittedDateFrom.Text.ToString.Trim)
        objParameter(4) = New Microsoft.Reporting.WebForms.ReportParameter("SubmittedDateTo", txtSubmittedDateTo.Text.ToString.Trim)
        objParameter(5) = New Microsoft.Reporting.WebForms.ReportParameter("Status", drpStatus.Items(drpStatus.SelectedIndex).Value.ToString.Trim)
        'Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range
        getCompanyName()
        Dim i As Integer
        Dim CompanyName As String = ViewState("SrcCompanyName")
        If CompanyName <> "" Then
            Dim strCompanyName() As String = Split(CompanyName, ",")
            CompanyName = ""
            For i = 0 To strCompanyName.GetLength(0) - 1
                If (strCompanyName.Length > 1) Then
                    If CompanyName <> "" Then
                        CompanyName &= ","
                    End If
                    CompanyName &= "'" & CStr(strCompanyName.GetValue(i)) & "'"
                Else
                    CompanyName = strCompanyName(0).ToString
                End If
            Next
        End If

        objParameter(6) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyName", CompanyName)

        ReportViewerOW.ServerReport.SetParameters(objParameter)


        Dim bytes As Byte() = ReportViewerOW.ServerReport.Render(fileType, Nothing, mimeType, encoding, extension, streamids, warnings)
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=RevenuebyBillingLocation" + Date.Now + "." + extension)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub
    Private Sub generateReport()
        If txtSubmittedDateFrom.Text.Trim = "" And txtSubmittedDateTo.Text.Trim <> "" Then
            lblError.Text = "Please enter Submitted Date From"
        Else
            lblError.Text = ""
            Dim StrCompanyID As String = ""
            ReportViewerOW.ShowParameterPrompts = False
            ReportViewerOW.ShowToolBar = True
            Dim reportSErverPath As Uri = New Uri(ApplicationSettings.ReportServerPath)
            ReportViewerOW.ServerReport.ReportServerUrl = reportSErverPath
            ReportViewerOW.ServerReport.ReportServerCredentials = New CustomReportCredentials.CustomReportCredentials(ApplicationSettings.ReportServerUserName, ApplicationSettings.ReportServerPassword, ApplicationSettings.ReportServerDomain)
            Dim objParameter(6) As Microsoft.Reporting.WebForms.ReportParameter

            ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/ProfessionalServiceWO"

            objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("StartDate", txtStartDate.Text.ToString.Trim)
            objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("EndDate", txtEndDate.Text.ToString.Trim)
            objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("PONumber", txtPONumber.Text.ToString.Trim)
            objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("SubmittedDateFrom", txtSubmittedDateFrom.Text.ToString.Trim)
            objParameter(4) = New Microsoft.Reporting.WebForms.ReportParameter("SubmittedDateTo", txtSubmittedDateTo.Text.ToString.Trim)
            objParameter(5) = New Microsoft.Reporting.WebForms.ReportParameter("Status", drpStatus.Items(drpStatus.SelectedIndex).Value.ToString.Trim)
            'Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range
            getCompanyName()
            Dim i As Integer
            Dim CompanyName As String = ViewState("SrcCompanyName")
            If CompanyName <> "" Then
                Dim strCompanyName() As String = Split(CompanyName, ",")
                CompanyName = ""
                For i = 0 To strCompanyName.GetLength(0) - 1
                    If (strCompanyName.Length > 1) Then
                        If CompanyName <> "" Then
                            CompanyName &= ","
                        End If
                        CompanyName &= "'" & CStr(strCompanyName.GetValue(i)) & "'"
                    Else
                        CompanyName = strCompanyName(0).ToString
                    End If
                Next
            End If

            objParameter(6) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyName", CompanyName)

            'StrCompanyID = UCSearchContact1.ddlContact.SelectedValue.ToString.Trim
            'If StrCompanyID <> "" Then
            '    objParameter(6) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyId", UCSearchContact1.ddlContact.SelectedValue.ToString.Trim)
            'Else
            '    objParameter(6) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyId", 0)
            'End If

            ReportViewerOW.ServerReport.SetParameters(objParameter)
            ReportViewerOW.ServerReport.Refresh()
        End If
    End Sub
    Private Sub getCompanyName()
        'Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.
        Dim dsSelectedCompany As DataSet
        Dim ds As DataSet
        dsSelectedCompany = CType(Session("dsSelectedCompany"), DataSet)
        Dim splitCompanyName As String
        Dim strCompanyName As String = ""
        Dim SPCompanyName As String = ""
        If Not IsNothing(dsSelectedCompany) And txtCompanyName.Text.Trim <> "" Then
            ds = CType(Session("dsSelectedCompany"), DataSet)
            Dim SelectedCompanyName As String = ""

            Dim i As Integer
            i = 0
            For Each drow As DataRow In ds.Tables(0).Rows
                SelectedCompanyName += drow.Item("CompanyName").ToString + ","
                i = i + 1
                strCompanyName = strCompanyName + SelectedCompanyName.Trim
            Next
            strCompanyName = strCompanyName + txtCompanyName.Text.Trim + ","
            splitCompanyName = strCompanyName.Substring(0, strCompanyName.LastIndexOf(","))
            ViewState("SrcCompanyName") = splitCompanyName
        ElseIf Not IsNothing(dsSelectedCompany) Then
            ds = CType(Session("dsSelectedCompany"), DataSet)
            Dim SelectedCompanyName As String = ""

            Dim i As Integer
            i = 0
            For Each drow As DataRow In ds.Tables(0).Rows
                SelectedCompanyName += drow.Item("CompanyName").ToString + ","
                i = i + 1
                strCompanyName = strCompanyName + SelectedCompanyName.Trim
            Next
            splitCompanyName = strCompanyName.Substring(0, strCompanyName.LastIndexOf(","))
            ViewState("SrcCompanyName") = splitCompanyName
        ElseIf txtCompanyName.Text.Trim <> "" Then
            strCompanyName = strCompanyName + txtCompanyName.Text.Trim + ","
            splitCompanyName = strCompanyName.Substring(0, strCompanyName.LastIndexOf(","))
            ViewState("SrcCompanyName") = splitCompanyName
        Else
            ViewState("SrcCompanyName") = ""
        End If
        ' hdnCompanyName.Value = ViewState("SrcCompanyName")
    End Sub
    'Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range
    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetCompanyName(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("SearchCompanyName", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function

    'Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range 
    Public Sub hdnbtnSelectedCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnbtnSelectedCompany.Click
        Dim SelectedCompany As String = hdnSelectedCompanyName.Value

        Dim dsSelectedCompany As DataSet
        dsSelectedCompany = CType(Session("dsSelectedCompany"), DataSet)
        Dim ds As New DataSet
        Dim dsExists As DataSet
        Dim dt As New DataTable
        Dim dv As DataView
        Dim drow As DataRow


        If Not IsNothing(dsSelectedCompany) Then
            ds = CType(Session("dsSelectedCompany"), DataSet)
            dv = ds.Tables(0).DefaultView
            dv.RowFilter = "CompanyName IN ('" & SelectedCompany & "')"
            If (dv.Count > 0) Then

                lblSelectedCompany.Text = "Selected item already exists in your selection."
            Else
                lblSelectedCompany.Text = ""
                dt = ds.Tables(0)

                drow = dt.NewRow()
                drow("CompanyID") = (dt.Rows.Count + 1).ToString
                drow("CompanyName") = SelectedCompany
                dt.Rows.Add(drow)
                dt.AcceptChanges()


                ds.AcceptChanges()
            End If


        Else


            Dim dColCompanyID As New DataColumn
            dColCompanyID.DataType = Type.[GetType]("System.String")
            dColCompanyID.ColumnName = "CompanyID"
            dt.Columns.Add(dColCompanyID)
            Dim dColCompanyName As New DataColumn
            dColCompanyName.DataType = Type.[GetType]("System.String")
            dColCompanyName.ColumnName = "CompanyName"
            dt.Columns.Add(dColCompanyName)


            drow = dt.NewRow()
            drow("CompanyID") = "1"
            drow("CompanyName") = SelectedCompany
            dt.Rows.Add(drow)
            dt.AcceptChanges()

            ds.Tables.Add(dt)
            ds.AcceptChanges()

        End If

        dv = ds.Tables(0).DefaultView
        dv.RowFilter = ""
        rptSelectedCompany.DataSource = dv.ToTable.DefaultView
        rptSelectedCompany.DataBind()


        Session("dsSelectedCompany") = ds
        txtCompanyName.Text = ""
        pnlSelectedCompany.Visible = True
    End Sub
    'Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range 
    Private Sub rptSelectedCompany_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSelectedCompany.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "Remove") Then
                RemoveSelectedCompany(e.CommandArgument.ToString)
            End If
        End If
    End Sub
    'Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range 
    Public Sub RemoveSelectedCompany(ByVal CompanyId As String)
        Dim ds As DataSet = CType(Session("dsSelectedCompany"), DataSet)
        Dim dt As DataTable = ds.Tables(0)
        Dim dv As New DataView
        dv = dt.Copy.DefaultView

        'add primary key as TagId 
        Dim keys(1) As DataColumn
        keys(0) = dt.Columns("CompanyID")

        Dim foundrow As DataRow() = dt.Select("CompanyID = '" & CompanyId & "'")
        If Not (foundrow Is Nothing) Then
            dt.Rows.Remove(foundrow(0))
        End If

        Dim i As Integer
        i = 0
        For Each drow As DataRow In ds.Tables(0).Rows
            drow.Item("CompanyID") = (i + 1).ToString
            i = i + 1
        Next

        dv = ds.Tables(0).DefaultView
        rptSelectedCompany.DataSource = dv.ToTable.DefaultView
        rptSelectedCompany.DataBind()
        Session("dsSelectedCompany") = ds
        pnlSelectedCompany.Visible = True
        If (ds.Tables(0).Rows.Count <= 0) Then
            pnlSelectedCompany.Visible = False
        End If




    End Sub
End Class
