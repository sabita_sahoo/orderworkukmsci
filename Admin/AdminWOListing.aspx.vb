
Partial Public Class AdminWOListing
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected WithEvents UCAdminWOsListing1 As UCAdminWOsListing
    Protected WithEvents UCDateRange1 As UCDateRange
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents CValidatorSearchContact As System.Web.UI.WebControls.CustomValidator
    Protected WithEvents lnkBackToListing As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents divButton As System.Web.UI.HtmlControls.HtmlGenericControl
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        UserAction.userAutoLoginAccept(Page)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Poonam modified on 2/8/2016 - Task -OA-311 : EA - Multiple search company in listing does not work in Chrome
        If Not Page.IsPostBack Then
            If IsNothing(Request("sender")) Then
                Session("AdminSelectedCompID") = -1
                Session("dsMultiSelectedCompany") = Nothing
            End If
        End If
        If Not IsNothing(Request("Sender")) Then
            If Request("Sender") = "OrderMatch" Then
                lnkBackToListing.HRef = getBackToListingLink()
                divButton.Visible = True
            Else
                divButton.Visible = False
            End If
        Else
            divButton.Visible = False
        End If

        'Added Code by Pankaj Malav on 11Oct 2008
        'This code is added to fix the issue while login the url is changed to lower case so Submitted is converted to submitted which disables the proper functioning of the page
        'The fix that is found is that we can convert the first character of the mode to upper and leave the rest
        Dim mode As String
        mode = Request.QueryString("mode")
        If mode <> ApplicationSettings.WOGroupCA Then
            mode = StrConv(mode, VbStrConv.ProperCase)
        End If

        If Not IsPostBack Then
            'Initiate Hide Test WO Links
            showHideTestWOLink(mode)
        Else
            ''If ddlBillLoc.Visible Then
            ''    ddlBillLoc.Visible = True
            ''    Dim dvBillLoc As DataView
            ''    dvBillLoc = UCAdminWOsListing1.FillBillingLocation()
            ''    If Not IsNothing(dvBillLoc) Then
            ''        ddlBillLoc.DataSource = dvBillLoc
            ''        ddlBillLoc.DataBind()
            ''    End If

            ''    Dim li As ListItem
            ''    li = New ListItem
            ''    li.Text = "All"
            ''    li.Value = 0
            ''    ddlBillLoc.Items.Insert(0, li)
            ''End If

        End If

        Security.SecurePage(Page, True)
        UCAdminWOsListing1.BizDiv = Session("BizDivId")
        UCSearchContact1.BizDivID = Session("BizDivId")
        UCSearchContact1.HideTestAccounts = chkHideTestAcc.Checked
        UCSearchContact1.DefaultView = chkMainCat.Checked
        UCSearchContact1.ContactType = "BusinessAreaDefined"
        'If Request.QueryString("mode") = ApplicationSettings.WOGroupSubmitted Then
        '    Timer1.Enabled = True
        'Else
        '    Timer1.Enabled = False
        'End If

        UCAdminWOsListing1.Group = mode
        'Hide Validation Message for selection of multiple WOs.
        lblMultipleWOsMsg.Visible = False
        If Not IsPostBack Then
            Dim currDate As DateTime = System.DateTime.Today
            txtCompanyName.Style.Add("display", "none")

            If (UCAdminWOsListing1.Group = ApplicationSettings.WOGroupClosed) Or (UCAdminWOsListing1.Group = ApplicationSettings.WOGroupCancelled) Or (UCAdminWOsListing1.Group = ApplicationSettings.WOGroupInvoiced) Then
                tblwatch.Style.Add("display", "none")
                If IsNothing(Request("FromDate")) And IsNothing(Request("ToDate")) Then
                    UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
                    UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
                Else
                    UCDateRange1.txtFromDate.Text = Request("FromDate")
                    UCDateRange1.txtToDate.Text = Request("ToDate")
                End If
            Else
                tblwatch.Style.Add("display", "inline")
                UCDateRange1.txtFromDate.Text = ""
                UCDateRange1.txtToDate.Text = ""
                'tdDateRange.Visible = False
                'tdDateRange.Style.Add("display", "none")
                'trFilterTypleLabel.Style.Add("display", "none")
                tdDateRange.Visible = False
                trFilterTypleLabel.Visible = False
                UCDateRange1.Visible = False
                'tdViewBtn.Visible = False               
            End If

            If (UCAdminWOsListing1.Group = ApplicationSettings.WOGroupCA) Or (UCAdminWOsListing1.Group = ApplicationSettings.WOGroupSent) Or (UCAdminWOsListing1.Group = ApplicationSettings.WOGroupAccepted) Then
                UCDateRange1.reqFromDate.Enabled = False
                UCDateRange1.reqToDate.Enabled = False
                tdRadioButtonCompany.Visible = True
                If IsNothing(Request("FromDate")) And IsNothing(Request("ToDate")) Then
                    'UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
                    'UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today.AddDays(2), DateFormat.ShortDate)
                    UCDateRange1.txtFromDate.Text = ""
                    UCDateRange1.txtToDate.Text = ""
                Else
                    UCDateRange1.txtFromDate.Text = Request("FromDate")
                    UCDateRange1.txtToDate.Text = Request("ToDate")
                End If
                tdDateRange.Visible = True
                trFilterTypleLabel.Visible = True
                UCDateRange1.Visible = True

                If Not IsNothing(Session("dsMultiSelectedCompany")) Then
                    Dim dsSelectedCompany As DataSet
                    Dim dv As DataView
                    dsSelectedCompany = CType(Session("dsMultiSelectedCompany"), DataSet)
                    dv = dsSelectedCompany.Tables(0).DefaultView

                    rptSelectedCompany.DataSource = dv.ToTable.DefaultView
                    rptSelectedCompany.DataBind()

                    pnlSelectedCompany.Visible = True
                    rdBtnSingleCompany.Checked = False
                    rdBtnMultiCompany.Checked = True
                Else
                    pnlSelectedCompany.Visible = False
                    rdBtnSingleCompany.Checked = True
                    rdBtnMultiCompany.Checked = False
                End If

                If (rdBtnSingleCompany.Checked) Then
                    UCSearchContact1.Visible = True
                    txtCompanyName.Style.Add("display", "none")
                Else
                    UCSearchContact1.Visible = False
                    txtCompanyName.Style.Add("display", "inline")
                End If

            Else
                Session("dsMultiSelectedCompany") = Nothing
                UCDateRange1.reqFromDate.Enabled = True
                UCDateRange1.reqToDate.Enabled = True
                tdRadioButtonCompany.Visible = False
            End If
            UCAdminWOsListing1.FromDate = UCDateRange1.txtFromDate.Text
            UCAdminWOsListing1.ToDate = UCDateRange1.txtToDate.Text

            UCAdminWOsListing1.CustomerMobile = txtCustomerMobile.Text.Trim

            If Not IsNothing(Request("SearchWorkorderID")) Then
                txtWorkorderID.Text = Request("SearchWorkorderID")
            Else
                txtWorkorderID.Text = ""
            End If


            showHeading(mode)
            showHideTestWOLink(mode)
            UCSearchContact1.Filter = mode
            UCSearchContact1.populateContact()
            MultipleWOsHeading(mode)

            Dim selVal As String
            If Not IsNothing(Session("AdminSelectedCompID")) Then
                If Session("AdminSelectedCompID") = 0 Then
                    selVal = "ALL"
                    UCAdminWOsListing1.CompID = 0

                ElseIf Session("AdminSelectedCompID") = -1 Then
                    selVal = ""
                    UCAdminWOsListing1.CompID = -1
                Else
                    selVal = Session("AdminSelectedCompID")
                    ShowHideGenDepot(Session("AdminSelectedCompID"))
                End If
            Else
                Session("AdminSelectedCompID") = -1
                selVal = ""
                UCAdminWOsListing1.CompID = -1
            End If

            If UCSearchContact1.ddlContact.Items.FindByValue(selVal) Is Nothing Then
                UCSearchContact1.ddlContact.SelectedIndex = 0
                UCAdminWOsListing1.CompID = -1
            Else
                UCSearchContact1.ddlContact.SelectedIndex = UCSearchContact1.ddlContact.Items.IndexOf(UCSearchContact1.ddlContact.Items.FindByValue(selVal))
            End If


            If Not IsNothing(Request("CompID")) Then
                If Request("CompID") <> "0" And Request("CompID") <> "" Then
                    UCSearchContact1.ddlContact.SelectedValue = Request("CompID").Trim
                    UCAdminWOsListing1.CompID = Request("CompID")
                End If
            Else
                If Not IsNothing(Session("AdminSelectedCompID")) Then
                    If Session("AdminSelectedCompID") <> 0 Then
                        Dim li As ListItem
                        li = UCSearchContact1.ddlContact.Items.FindByValue(Session("AdminSelectedCompID").ToString)
                        If Not IsNothing(li) Then
                            UCSearchContact1.ddlContact.SelectedValue = Session("AdminSelectedCompID")
                            UCAdminWOsListing1.CompID = Session("AdminSelectedCompID")
                        Else
                            Session("AdminSelectedCompID") = Nothing
                        End If
                    ElseIf Session("AdminSelectedCompID") = 0 Then
                        UCSearchContact1.ddlContact.SelectedIndex = 1
                        UCAdminWOsListing1.CompID = Session("AdminSelectedCompID")
                    Else
                        Session("AdminSelectedCompID") = Nothing
                    End If
                End If
            End If


            'code for showing and hiding for billing location dropdown on pageload
            If Not IsNothing(Session("BillLoc")) And Not IsNothing(Session("AdminSelectedCompID")) Then
                If Session("AdminSelectedCompID") <> 0 And Session("AdminSelectedCompID") <> -1 Then
                    ddlBillLoc.Visible = True
                    UCAdminWOsListing1.PopulateGrid()
                Else
                    ddlBillLoc.Visible = False
                End If

            Else
                ddlBillLoc.Visible = False
            End If
            'code for populating data in dropdown

            Dim dvBillLoc As DataView
            If ddlBillLoc.Visible Then


                dvBillLoc = UCAdminWOsListing1.FillBillingLocation()

                If Not IsNothing(dvBillLoc) Then
                    ddlBillLoc.DataSource = dvBillLoc
                    ddlBillLoc.DataBind()

                    If dvBillLoc.Count > 0 Then
                        Dim li1 As New ListItem
                        li1 = New ListItem
                        li1.Text = "All"
                        li1.Value = 0
                        ddlBillLoc.Items.Insert(0, li1)
                    Else
                        ddlBillLoc.Visible = False
                    End If
                Else
                    ddlBillLoc.Visible = False
                End If

                'for selected value of dropdown billing location
                If Not IsNothing(dvBillLoc) Then
                    If Not IsNothing(Session("BillLoc")) Then
                        dvBillLoc.RowFilter = "BillingLocID = " & Session("BillLoc")
                        If dvBillLoc.ToTable.Rows.Count > 0 Then
                            ddlBillLoc.SelectedValue = Session("BillLoc")
                        Else
                            Session("BillLoc") = 0
                            'UCAdminWOsListing1.PopulateGrid()
                        End If
                    Else
                        Session("BillLoc") = 0
                        'UCAdminWOsListing1.PopulateGrid()
                    End If
                Else
                    Session("BillLoc") = 0
                    'UCAdminWOsListing1.PopulateGrid()
                End If
            Else
                Session("BillLoc") = 0
                'UCAdminWOsListing1.PopulateGrid()
            End If

            'end code for billing location
            If Request.QueryString("mode") = ApplicationSettings.WOGroupAccepted Then
                If Not IsNothing(Request("IsNextDay")) Then
                    If Request("IsNextDay") = "Yes" Then
                        chkNextDay.Checked = True
                        UCAdminWOsListing1.IsNextDay = "Yes"
                    Else
                        chkNextDay.Checked = False
                        UCAdminWOsListing1.IsNextDay = "No"
                    End If
                End If
            End If
        End If
        If (Request.QueryString("mode") <> "") Then
            If Request.QueryString("mode") = ApplicationSettings.WOGroupAccepted Or Request.QueryString("mode").ToLower = "activeelapsed" Then
                ShowHideGenDepot(Session("AdminSelectedCompID"))
            End If
        End If
        btnExport.HRef = prepareExportToExcelLink()

        If (mode = "Watched") Then
            tblAllActionButtons.Visible = False
            tblRemoveWatch.Visible = True
        Else
            tblAllActionButtons.Visible = True
            tblRemoveWatch.Visible = False
        End If
        UCAdminWOsListing1.WorkorderID = txtWorkorderID.Text.Trim

        'OA-480-If the mode is "Draft" then hide the button "Watch"
        If Request.QueryString("mode") = ApplicationSettings.WOGroupDraft Then
            tblwatch.Style.Add("display", "none")
        End If

    End Sub

    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkView.Click
        Page.Validate()
        If Page.IsValid Then

            If (UCAdminWOsListing1.Group = ApplicationSettings.WOGroupCA) Or (UCAdminWOsListing1.Group = ApplicationSettings.WOGroupSent) Or (UCAdminWOsListing1.Group = ApplicationSettings.WOGroupAccepted) Then
                If (rdBtnMultiCompany.Checked) Then
                    UCSearchContact1.ddlContact.SelectedValue = "ALL"
                End If
            End If

            If UCSearchContact1.ddlContact.SelectedValue = "" Then
                UCAdminWOsListing1.CompID = -1
                Session("AdminSelectedCompID") = -1
            ElseIf UCSearchContact1.ddlContact.SelectedValue = "ALL" Then
                UCAdminWOsListing1.CompID = 0
                Session("AdminSelectedCompID") = 0
                ShowHideGenDepot(Session("AdminSelectedCompID"))
            Else
                UCAdminWOsListing1.CompID = UCSearchContact1.ddlContact.SelectedValue
                Session("AdminSelectedCompID") = UCSearchContact1.ddlContact.SelectedValue
                ShowHideGenDepot(Session("AdminSelectedCompID"))
            End If

            UCAdminWOsListing1.WorkorderID = txtWorkorderID.Text.Trim
            UCAdminWOsListing1.CustomerMobile = txtCustomerMobile.Text.Trim

            Populate()
            UCAdminWOsListing1.PopulateGrid()
            'If Not IsNothing(Session("BillLoc")) Then
            ddlBillLoc.Visible = True
            'Else
            '    ddlBillLoc.Visible = False
            'End If
            If UCSearchContact1.ddlContact.SelectedValue = "ALL" Or UCSearchContact1.ddlContact.SelectedValue = "" Then
                'ddlBillLoc.SelectedValue = 0
                ddlBillLoc.Visible = False

            Else

                Dim dvBillLoc As DataView
                dvBillLoc = UCAdminWOsListing1.FillBillingLocation()

                If Not IsNothing(dvBillLoc) Then
                    ddlBillLoc.DataSource = dvBillLoc
                    ddlBillLoc.DataBind()
                    If dvBillLoc.Count > 0 Then
                        Dim li As ListItem
                        li = New ListItem
                        li.Text = "All"
                        li.Value = 0
                        ddlBillLoc.Items.Insert(0, li)
                    Else
                        ddlBillLoc.Visible = False
                    End If
                Else
                    ddlBillLoc.Visible = False
                End If


                If Not IsNothing(dvBillLoc) Then
                    If Not IsNothing(Session("BillLoc")) Then
                        dvBillLoc.RowFilter = "BillingLocID = " & Session("BillLoc")
                        If dvBillLoc.ToTable.Rows.Count > 0 Then
                            ddlBillLoc.SelectedValue = Session("BillLoc")
                        Else
                            Session("BillLoc") = 0
                            'UCAdminWOsListing1.PopulateGrid()
                        End If
                    Else
                        Session("BillLoc") = 0
                        'UCAdminWOsListing1.PopulateGrid()
                    End If
                Else
                    Session("BillLoc") = 0
                    'UCAdminWOsListing1.PopulateGrid()
                End If
            End If


        End If


    End Sub

    ''' <summary>
    ''' Show hide Generate Depot Sheets Logic
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowHideGenDepot(ByVal CompanyId As Integer)
        UCSearchContact1.getContactsDetails(CompanyId, 0, "")
        Dim BusinessArea As Integer
        BusinessArea = UCSearchContact1.BusinessArea
        If Request.QueryString("mode") = ApplicationSettings.WOGroupAccepted Then
            tdGenDepot.Visible = True
            chkNextDay.Visible = True
            tdFastClose.Visible = True
        Else
            tdGenDepot.Visible = False
            chkNextDay.Visible = False
            tdFastClose.Visible = False
        End If
        If (Request.QueryString("mode").ToString.ToLower = "activeelapsed") Then
            tdFastClose.Visible = True
        End If
    End Sub

    Public Sub Populate()
        'Changed By Pankaj Malav added code for changing the query string such that only the first charcter will be in upper case
        If Request.QueryString("mode") <> ApplicationSettings.WOGroupCA Then
            UCAdminWOsListing1.Group = StrConv(Request.QueryString("mode"), VbStrConv.ProperCase)
        Else
            UCAdminWOsListing1.Group = Request.QueryString("mode").Trim
        End If
        UCAdminWOsListing1.FromDate = UCDateRange1.txtFromDate.Text
        UCAdminWOsListing1.ToDate = UCDateRange1.txtToDate.Text
        ''If UCSearchContact1.ddlContact.SelectedValue <> "" And UCSearchContact1.ddlContact.SelectedValue.ToString.ToLower <> "ALL".ToLower Then
        ''    UCAdminWOsListing1.BillLoc = UCSearchContact1.ddlBillLoc.SelectedValue
        ''Else
        ''    UCAdminWOsListing1.BillLoc = 0
        ''End If

        btnExport.HRef = prepareExportToExcelLink()
    End Sub

    Public Function prepareExportToExcelLink() As String
        Dim linkParams As String = ""
        Dim mode As String
        mode = Request.QueryString("mode")
        If mode <> ApplicationSettings.WOGroupCA Then
            mode = StrConv(mode, VbStrConv.ProperCase)
        End If

        linkParams &= "BizDivId=" & UCAdminWOsListing1.BizDiv
        linkParams &= "&Group=" & UCAdminWOsListing1.Group
        If UCSearchContact1.ddlContact.SelectedValue <> "" And UCSearchContact1.ddlContact.SelectedValue.ToString.ToLower <> "all" Then
            linkParams &= "&CompID=" & UCSearchContact1.ddlContact.SelectedValue
        Else
            linkParams &= "&CompID=" & 0
        End If
        linkParams &= "&CustomerMobile=" & UCAdminWOsListing1.CustomerMobile
        linkParams &= "&FromDate=" & UCAdminWOsListing1.FromDate
        linkParams &= "&ToDate=" & UCAdminWOsListing1.ToDate
        If (UCAdminWOsListing1.WorkorderID = "0") Then
            linkParams &= "&WorkorderID="
        Else
            linkParams &= "&WorkorderID=" & UCAdminWOsListing1.WorkorderID
        End If
        linkParams &= "&sortExpression=" & UCAdminWOsListing1.sortExpression
        linkParams &= "&hideTestWOs=" & chkHideTestAcc.Checked
        linkParams &= "&MainCat=" & chkMainCat.Checked
        If (Request.QueryString("mode") = "Watched") Then
            linkParams &= "&Watched=" & True
        Else
            linkParams &= "&Watched=" & False
        End If

        Dim splitCompanyName As String = ""
        If Not IsNothing(Session("dsMultiSelectedCompany")) Then
            Dim dsSelectedCompany As DataSet
            Dim dsTemp As DataSet
            dsSelectedCompany = CType(Session("dsMultiSelectedCompany"), DataSet)

            Dim strCompanyName As String = ""
            Dim SPCompanyName As String = ""
            If Not IsNothing(dsSelectedCompany) Then
                dsTemp = CType(Session("dsMultiSelectedCompany"), DataSet)
                Dim SelectedCompanyName As String = ""
                Dim i As Integer
                i = 0
                For Each drow As DataRow In dsTemp.Tables(0).Rows
                    SelectedCompanyName += drow.Item("CompanyName").ToString + ","
                    i = i + 1
                    strCompanyName = strCompanyName + SelectedCompanyName.Trim
                Next
                splitCompanyName = strCompanyName.Substring(0, strCompanyName.LastIndexOf(","))
            End If
        End If
        linkParams &= "&CompanyName=" & splitCompanyName

        If mode = ApplicationSettings.WOGroupCA Or mode = ApplicationSettings.WOGroupSubmitted Or mode = ApplicationSettings.WOGroupDraft Then
            linkParams &= "&page=" & "AdminWOListingNoSupp"
        Else
            linkParams &= "&page=" & "AdminWOListing"
        End If
        Return "ExportToExcel.aspx?" & linkParams
    End Function

    Private Function showHeading(ByVal strMode As String)
        'Code to add the heading for the page depending on the mode.
        'The hardcoded strings are not replaced with those in the resource files to avoid the neccessary trip to the file, since this is admin page and the text is not going to change.
        Select Case (strMode)
            Case ApplicationSettings.WOGroupDraft
                lblHeading.Text = "Drafts"
            Case ApplicationSettings.WOGroupSubmitted
                lblHeading.Text = "Submitted"
            Case ApplicationSettings.WOGroupSent
                lblHeading.Text = "Sent"
                lblDateFilterType.Text = "Filter by Date Created"
            Case ApplicationSettings.WOGroupCA
                lblHeading.Text = "Conditionally Accepted"
                lblDateFilterType.Text = "Filter by Start Date"
            Case ApplicationSettings.WOGroupAccepted
                lblHeading.Text = "Active"
                lblDateFilterType.Text = "Filter by Start Date"
            Case ApplicationSettings.WOGroupIssue
                lblHeading.Text = "Issue"
                lblDateFilterType.Text = "Filter by Last Response"
            Case ApplicationSettings.WOGroupCompleted
                lblHeading.Text = "Completed"
            Case ApplicationSettings.WOGroupClosed
                lblHeading.Text = "Closed"
                lblDateFilterType.Text = "Filter by Closed Date"
            Case ApplicationSettings.WOGroupCancelled
                lblHeading.Text = "Cancelled"
                lblDateFilterType.Text = "Filter by Cancelled Date"
            Case "Staged"
                lblHeading.Text = "Staged"
            Case "Invoiced"
                lblHeading.Text = "Invoiced"
                lblDateFilterType.Text = "Filter by Invoiced Date"
            Case "Activeelapsed"
                lblHeading.Text = "Active Elapsed"
                lblDateFilterType.Text = "Filter by Start Date"
            Case "Watched"
                lblHeading.Text = "My Watch"
        End Select
        Return Nothing
    End Function

    Private Function showHideTestWOLink(ByVal strMode As String)
        'Now, showing this feature for all WO Stages
        chkHideTestAcc.Visible = True
        chkHideTestAcc.Checked = True
        Return Nothing
    End Function

    ''' <summary>
    ''' Function to show/hide the Show Test WO checkbox
    ''' </summary>
    ''' <remarks>Pratik Trivedi - 21 Nov, 2008</remarks>
    Public Sub ProcessBackToListing()
        UCAdminWOsListing1.FromDate = Request("FromDate")
        UCAdminWOsListing1.ToDate = Request("ToDate")
        UCAdminWOsListing1.WorkorderID = Request("SearchWorkorderID")
        UCAdminWOsListing1.CompID = Request("CompID")
        UCAdminWOsListing1.CustomerMobile = Request("CustomerMobile")
        UCAdminWOsListing1.PopulateGrid()
    End Sub

    Private Function MultipleWOsHeading(ByVal strMode As String)
        'Code to add the heading for the page depending on the mode.
        'The hardcoded strings are not replaced with those in the resource files to avoid the neccessary trip to the file, since this is admin page and the text is not going to change.
        Select Case (strMode)
            Case ApplicationSettings.WOGroupDraft
                HideMultipleWoBtn()
            Case ApplicationSettings.WOGroupSubmitted
                lnkMultipleWOs.Text = "&nbsp;Accept Work Orders&nbsp;"
            Case ApplicationSettings.WOGroupSent
                HideMultipleWoBtn()
            Case ApplicationSettings.WOGroupCA
                HideMultipleWoBtn()
            Case ApplicationSettings.WOGroupAccepted
                lnkMultipleWOs.Text = "&nbsp;Mark WOs as Complete&nbsp;"
            Case ApplicationSettings.WOGroupIssue
                HideMultipleWoBtn()
            Case ApplicationSettings.WOGroupCompleted
                lnkMultipleWOs.Text = "&nbsp;Mark WOs as Closed&nbsp;"
            Case ApplicationSettings.WOGroupClosed
                HideMultipleWoBtn()
            Case ApplicationSettings.WOGroupCancelled
                HideMultipleWoBtn()
            Case "Staged"
                HideMultipleWoBtn()
            Case "Invoiced"
                HideMultipleWoBtn()
            Case "Activeelapsed"
                lnkMultipleWOs.Text = "&nbsp;Mark WOs as Complete&nbsp;"
        End Select
        Return Nothing
    End Function

    Private Function HideMultipleWoBtn()
        lnkMultipleWOs.Visible = False
        'tdMultipleLeft.Visible = False
        'tdMultipleMid.Visible = False
        tdMultipleRight.Visible = False
        Return Nothing
    End Function

    ''' <summary>
    ''' Sub to handle click on click of Multiple selected workorders. Passes WOIDs in string
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Pratik Trivedi-19th Aug, 2008</remarks>
    Public Sub lnkMultipleWOs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMultipleWOs.Click
        UCAdminWOsListing1.GetSelectedWOIDs()
        If UCAdminWOsListing1.SelectedWOIDs <> "" Then
            Dim appendLink As String = ""
            appendLink = "WOID=" & UCAdminWOsListing1.SelectedWOIDs
            appendLink = appendLink & "&mode=" & UCAdminWOsListing1.Group
            appendLink = appendLink & "&FromDate=" & UCAdminWOsListing1.FromDate
            appendLink = appendLink & "&ToDate=" & UCAdminWOsListing1.ToDate
            appendLink = appendLink & "&BizDivId=" & UCAdminWOsListing1.BizDiv
            appendLink &= "&SearchWorkorderID=" & txtWorkorderID.Text
            Response.Redirect("MultipleWOHandling.aspx?" & appendLink)
            lblMultipleWOsMsg.Visible = False
        Else
            lblMultipleWOsMsg.Visible = True
            lblMultipleWOsMsg.Text = "&nbsp;*&nbsp;" & ResourceMessageText.GetString("WOMultipleSelectError")
        End If

    End Sub



    Public Function getBackToListingLink() As String
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender")
                Case "OrderMatch"
                    link = "~\WOOrderMatch.aspx?"
                    'link &= "bizDivId=" & Request("bizDivId")
                    'link &= "&bizdiv=" & Request("bizdiv")
                    link &= "&companyId=" & Request("compId")
                    link &= "&BillLoc=" & Request("BillLoc")
                    If Not IsNothing(Request("vendorIDs")) Then
                        link &= "&vendorIDs=" & Request("vendorIDs")
                    End If

                    'link &= "&contactId=" & Request("contactId")
                    'link &= "&classId=" & Request("classId")
                    'link &= "&statusId=" & Request("statusId")
                    'link &= "&roleGroupId=" & Request("Request")
                    link &= "&WOID=" & Request("WOID")
                    link &= "&product=" & Request("product")
                    link &= "&region=" & Request("region")
                    link &= "&PostCode=" & Request("PostCode")
                    link &= "&Keyword=" & Request("Keyword")
                    link &= "&Group=" & Request("Group")
                    link &= "&refcheck=" & Request("refcheck")
                    link &= "&MinWOValue=" & Request("MinWOValue")

                    link &= "&companyName=" & Request("companyName")
                    link &= "&noOfEmployees=" & Request("noOfEmployees")
                    link &= "&ShowSupplierWithCW=" & Request("ShowSupplierWithCW")
                    If Not IsNothing(Request("vendorIDs")) Then
                        link &= "&vendorIDs=" & Request("vendorIDs")
                    End If


                    'link &= "&mode=" & "edit"
                    link &= "&sender=" & "AdminWOListing"
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&NearestTo=" & Request("NearestTo")
                    link &= "&Distance=" & Request("Distance")
                    link &= "&crbChecked=" & Request("crbChecked")
                    link &= "&ukSecurity=" & Request("ukSecurity")
            End Select
        End If
        Return link
    End Function

    Public Sub Timer1_Tick(ByVal sender As Object, ByVal e As EventArgs)
        Populate()
        UCAdminWOsListing1.PopulateGrid()
    End Sub

    ''' <summary>
    ''' To redirect to Generate Depot Sheet Page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Private Sub lnkbtnGenDepot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnGenDepot.ServerClick
    '    UCAdminWOsListing1.GetSelectedWOIDs()
    '    If UCAdminWOsListing1.SelectedWOIDs <> "" Then
    '        Dim appendLink As String = ""
    '        appendLink = "WOID=" & UCAdminWOsListing1.SelectedWOIDs
    '        appendLink = appendLink & "&mode=" & UCAdminWOsListing1.Group
    '        lnkbtnGenDepot.HRef = "~/GenerateDepot.aspx?" & appendLink
    '        lblGenDepotMsg.Visible = False
    '    Else
    '        lblGenDepotMsg.Visible = True
    '        lnkbtnGenDepot.HRef = ""
    '        lblGenDepotMsg.Text = "&nbsp;*&nbsp;" & ResourceMessageText.GetString("WOMultipleSelectError")
    '    End If
    'End Sub

    Public Sub lnkbtnGenDepot_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnGenDepot.ServerClick
        UCAdminWOsListing1.GetSelectedWOIDs()
        If UCAdminWOsListing1.SelectedWOIDs <> "" Then
            Dim appendLink As String = ""
            appendLink = "WOID=" & UCAdminWOsListing1.SelectedWOIDs
            appendLink = appendLink & "&mode=" & UCAdminWOsListing1.Group
            If chkNextDay.Checked Then
                appendLink = appendLink & "&IsNextDay=Yes"
            Else
                appendLink = appendLink & "&IsNextDay=No"
            End If

            ResponseHelper.Redirect("GenerateDepot.aspx?" & appendLink, "_self", "")
            lblMultipleWOsMsg.Visible = False
        Else
            lblMultipleWOsMsg.Visible = True
            lblMultipleWOsMsg.Text = "&nbsp;*&nbsp;" & "At least one work order must be selected in order to generate depot sheets."
        End If
    End Sub
    Public Sub btnfastClose_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfastClose.ServerClick
        UCAdminWOsListing1.GetSelectedWOIDs()
        If UCAdminWOsListing1.SelectedWOIDs <> "" Then
            Dim appendLink As String = ""
            If Not IsNothing(Session("AdminSelectedCompID")) Then
                If Not IsNothing(Request.QueryString("mode")) Then
                    If Request.QueryString("mode") <> "" Then
                        appendLink = "WOID=" & UCAdminWOsListing1.SelectedWOIDs & "&CompanyID=" & Session("AdminSelectedCompID") & "&mode=" & Request.QueryString("mode")
                    Else
                        appendLink = "WOID=" & UCAdminWOsListing1.SelectedWOIDs
                    End If
                Else
                    appendLink = "WOID=" & UCAdminWOsListing1.SelectedWOIDs
                End If
            Else
                appendLink = "WOID=" & UCAdminWOsListing1.SelectedWOIDs
            End If

            'Poonam : OA - 483- Back button of AdminWOListing.aspx does not return to filtered listing
            appendLink = appendLink & "&FromDate=" & UCAdminWOsListing1.FromDate.Trim()
            appendLink = appendLink & "&ToDate=" & UCAdminWOsListing1.ToDate.Trim()
            appendLink = appendLink & "&BizDivId=" & UCAdminWOsListing1.BizDiv
            appendLink &= "&SearchWorkorderID=" & txtWorkorderID.Text

            ResponseHelper.Redirect("FastCloseWO.aspx?" & appendLink, "_self", "")
            lblMultipleWOsMsg.Visible = False
        Else
            lblMultipleWOsMsg.Visible = True
            lblMultipleWOsMsg.Text = "&nbsp;*&nbsp;" & "No work order has been selected"
        End If
    End Sub

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub

    Public Sub btnWatch_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWatch.ServerClick
        UCAdminWOsListing1.GetSelectedWOIDs()
        If UCAdminWOsListing1.SelectedWOIDs <> "" Then
            ws.WSWorkOrder.AddDeleteWatchedWO(UCAdminWOsListing1.SelectedWOIDs, Session("UserID"), "Add")
            lblMultipleWOsMsg.Visible = True
            lblMultipleWOsMsg.Text = "&nbsp;*&nbsp;Added Successfully to watched listing."
            Populate()
            UCAdminWOsListing1.PopulateGrid()
        Else
            lblMultipleWOsMsg.Visible = True
            lblMultipleWOsMsg.Text = "&nbsp;*&nbsp;" & ResourceMessageText.GetString("WOMultipleSelectError")
        End If
    End Sub

    Public Sub btnRemoveWatch_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveWatch.ServerClick
        UCAdminWOsListing1.GetSelectedWOIDs()
        If UCAdminWOsListing1.SelectedWOIDs <> "" Then
            ws.WSWorkOrder.AddDeleteWatchedWO(UCAdminWOsListing1.SelectedWOIDs, Session("UserID"), "Remove")
            lblRemoveMsg.Visible = True
            lblRemoveMsg.Text = "&nbsp;*&nbsp;Removed Successfully from watched listing."
            Populate()
            UCAdminWOsListing1.PopulateGrid()
        Else
            lblRemoveMsg.Visible = True
            lblRemoveMsg.Text = "&nbsp;*&nbsp;" & ResourceMessageText.GetString("WOMultipleSelectError")
        End If
    End Sub

    Private Sub chkHideTestAcc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkHideTestAcc.CheckedChanged, chkWatchHideTestAcc.CheckedChanged
        Dim mode As String
        mode = Request.QueryString("mode")
        If mode <> ApplicationSettings.WOGroupCA Then
            mode = StrConv(mode, VbStrConv.ProperCase)
        End If
        UCSearchContact1.Filter = mode
        UCSearchContact1.BizDivID = Session("BizDivId")
        UCSearchContact1.HideTestAccounts = chkHideTestAcc.Checked
        UCSearchContact1.DefaultView = chkMainCat.Checked
        UCSearchContact1.populateContact()
        If Not IsNothing(Session("BillLoc")) And Not IsNothing(Session("AdminSelectedCompID")) Then
            If Session("AdminSelectedCompID") <> 0 And Session("AdminSelectedCompID") <> -1 Then
                ddlBillLoc.Visible = True
                UCAdminWOsListing1.PopulateGrid()
            Else
                ddlBillLoc.Visible = False
            End If
        Else
            ddlBillLoc.Visible = False
        End If
        UCAdminWOsListing1.PopulateGrid()
        'Dim dvBillLoc As DataView
        'dvBillLoc = UCAdminWOsListing1.FillBillingLocation()

        'If Not IsNothing(dvBillLoc) Then
        '    ddlBillLoc.DataSource = dvBillLoc
        '    ddlBillLoc.DataBind()
        'End If
        'If dvBillLoc.Count > 0 Then
        '    Dim li As ListItem
        '    li = New ListItem
        '    li.Text = "All"
        '    li.Value = 0
        '    ddlBillLoc.Items.Insert(0, li)
        'Else
        '    ddlBillLoc.Visible = False
        'End If
    End Sub

    Private Sub chkNextDay_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNextDay.CheckedChanged
        If chkNextDay.Checked Then
            UCAdminWOsListing1.IsNextDay = "Yes"
        Else
            UCAdminWOsListing1.IsNextDay = "No"
        End If

        Dim mode As String
        mode = Request.QueryString("mode")
        If mode <> ApplicationSettings.WOGroupCA Then
            mode = StrConv(mode, VbStrConv.ProperCase)
        End If
        UCSearchContact1.Filter = mode
        UCSearchContact1.BizDivID = Session("BizDivId")
        UCSearchContact1.HideTestAccounts = chkHideTestAcc.Checked
        UCSearchContact1.populateContact()
        If Not IsNothing(Session("AdminSelectedCompID")) Then
            If Session("AdminSelectedCompID") <> 0 Then
                Dim li As ListItem
                li = UCSearchContact1.ddlContact.Items.FindByValue(Session("AdminSelectedCompID").ToString)
                If Not IsNothing(li) Then
                    UCSearchContact1.ddlContact.SelectedValue = Session("AdminSelectedCompID")
                    UCAdminWOsListing1.CompID = Session("AdminSelectedCompID")
                Else
                    Session("AdminSelectedCompID") = Nothing
                End If
            ElseIf Session("AdminSelectedCompID") = 0 Then
                UCSearchContact1.ddlContact.SelectedIndex = 1
                UCAdminWOsListing1.CompID = Session("AdminSelectedCompID")
            Else
                Session("AdminSelectedCompID") = Nothing
            End If
        End If

        UCAdminWOsListing1.WorkorderID = txtWorkorderID.Text.Trim

        'UCSearchContact1.ddlContact.SelectedValue = UCSearchContact1.ddlContact.SelectedValue
        UCAdminWOsListing1.PopulateGrid()
    End Sub

    Private Sub chkMainCat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMainCat.CheckedChanged
        Dim mode As String
        mode = Request.QueryString("mode")
        If mode <> ApplicationSettings.WOGroupCA Then
            mode = StrConv(mode, VbStrConv.ProperCase)
        End If
        UCSearchContact1.Filter = mode
        UCSearchContact1.BizDivID = Session("BizDivId")
        UCSearchContact1.HideTestAccounts = chkHideTestAcc.Checked
        UCSearchContact1.DefaultView = chkMainCat.Checked
        UCSearchContact1.populateContact()
        If chkMainCat.Checked Then
            UCAdminWOsListing1.MainCat = "Yes"
        Else
            UCAdminWOsListing1.MainCat = "No"
        End If
        If Not IsNothing(Session("BillLoc")) And Not IsNothing(Session("AdminSelectedCompID")) Then
            If Session("AdminSelectedCompID") <> 0 And Session("AdminSelectedCompID") <> -1 Then
                ddlBillLoc.Visible = True
                UCAdminWOsListing1.PopulateGrid()
            Else
                ddlBillLoc.Visible = False
            End If
        Else
            ddlBillLoc.Visible = False
        End If
        UCAdminWOsListing1.PopulateGrid()
    End Sub

    Private Sub ddlBillLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBillLoc.SelectedIndexChanged
        Dim billloc As Integer
        billloc = ddlBillLoc.SelectedValue
        Session("BillLoc") = ddlBillLoc.SelectedValue

        UCAdminWOsListing1.BillLoc = ddlBillLoc.SelectedValue
    End Sub

    Public Sub hdnbtnSelectedCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnbtnSelectedCompany.Click
        Dim SelectedCompany As String = hdnSelectedCompanyName.Value

        If (SelectedCompany = "All Contacts") Then
            Session("dsMultiSelectedCompany") = Nothing
        Else
            RemoveSelectedCompanyByName("All Contacts")
        End If
        Dim dsMultiSelectedCompany As DataSet
        dsMultiSelectedCompany = CType(Session("dsMultiSelectedCompany"), DataSet)
        Dim ds As New DataSet
        Dim dsExists As DataSet
        Dim dt As New DataTable
        Dim dv As DataView
        Dim drow As DataRow


        If Not IsNothing(dsMultiSelectedCompany) Then
            ds = CType(Session("dsMultiSelectedCompany"), DataSet)
            dv = ds.Tables(0).DefaultView
            dv.RowFilter = "CompanyName IN ('" & SelectedCompany & "')"
            If (dv.Count > 0) Then

                lblSelectedCompany.Text = "Selected item already exists in your selection."
            Else
                lblSelectedCompany.Text = ""
                dt = ds.Tables(0)

                drow = dt.NewRow()
                drow("CompanyID") = (dt.Rows.Count + 1).ToString
                drow("CompanyName") = SelectedCompany
                dt.Rows.Add(drow)
                dt.AcceptChanges()


                ds.AcceptChanges()
            End If


        Else


            Dim dColCompanyID As New DataColumn
            dColCompanyID.DataType = Type.[GetType]("System.String")
            dColCompanyID.ColumnName = "CompanyID"
            dt.Columns.Add(dColCompanyID)
            Dim dColCompanyName As New DataColumn
            dColCompanyName.DataType = Type.[GetType]("System.String")
            dColCompanyName.ColumnName = "CompanyName"
            dt.Columns.Add(dColCompanyName)


            drow = dt.NewRow()
            drow("CompanyID") = "1"
            drow("CompanyName") = SelectedCompany
            dt.Rows.Add(drow)
            dt.AcceptChanges()

            ds.Tables.Add(dt)
            ds.AcceptChanges()

        End If

        dv = ds.Tables(0).DefaultView
        dv.RowFilter = ""
        rptSelectedCompany.DataSource = dv.ToTable.DefaultView
        rptSelectedCompany.DataBind()


        Session("dsMultiSelectedCompany") = ds
        txtCompanyName.Text = ""
        pnlSelectedCompany.Visible = True
    End Sub

    Private Sub rptSelectedCompany_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSelectedCompany.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "Remove") Then
                RemoveSelectedCompany(e.CommandArgument.ToString)
            End If
        End If
    End Sub

    Public Sub RemoveSelectedCompany(ByVal CompanyId As String)
        Dim ds As DataSet = CType(Session("dsMultiSelectedCompany"), DataSet)
        Dim dt As DataTable = ds.Tables(0)
        Dim dv As New DataView
        dv = dt.Copy.DefaultView

        'add primary key as TagId 
        Dim keys(1) As DataColumn
        keys(0) = dt.Columns("CompanyID")

        Dim foundrow As DataRow() = dt.Select("CompanyID = '" & CompanyId & "'")
        If Not (foundrow Is Nothing) Then
            dt.Rows.Remove(foundrow(0))
        End If

        Dim i As Integer
        i = 0
        For Each drow As DataRow In ds.Tables(0).Rows
            drow.Item("CompanyID") = (i + 1).ToString
            i = i + 1
        Next

        dv = ds.Tables(0).DefaultView
        rptSelectedCompany.DataSource = dv.ToTable.DefaultView
        rptSelectedCompany.DataBind()
        Session("dsMultiSelectedCompany") = ds
        pnlSelectedCompany.Visible = True
        If (ds.Tables(0).Rows.Count <= 0) Then
            Session("dsMultiSelectedCompany") = Nothing
            pnlSelectedCompany.Visible = False
        End If




    End Sub

    Public Sub RemoveSelectedCompanyByName(ByVal CompanyName As String)
        If Not IsNothing(Session("dsMultiSelectedCompany")) Then
            Dim ds As DataSet = CType(Session("dsMultiSelectedCompany"), DataSet)
            Dim dt As DataTable = ds.Tables(0)
            Dim dv As New DataView
            dv = dt.Copy.DefaultView

            'add primary key as TagId 
            Dim keys(1) As DataColumn
            keys(0) = dt.Columns("CompanyName")

            Dim foundrow As DataRow() = dt.Select("CompanyName = '" & CompanyName & "'")
            If Not (foundrow Is Nothing) Then
                If (foundrow.Length > 0) Then
                    dt.Rows.Remove(foundrow(0))
                End If
            End If

            Dim i As Integer
            i = 0
            For Each drow As DataRow In ds.Tables(0).Rows
                drow.Item("CompanyID") = (i + 1).ToString
                i = i + 1
            Next

            dv = ds.Tables(0).DefaultView
            rptSelectedCompany.DataSource = dv.ToTable.DefaultView
            rptSelectedCompany.DataBind()
            Session("dsMultiSelectedCompany") = ds
            pnlSelectedCompany.Visible = True
            If (ds.Tables(0).Rows.Count <= 0) Then
                Session("dsMultiSelectedCompany") = Nothing
                pnlSelectedCompany.Visible = False
            End If
        End If
    End Sub
    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetMultiCompanyName(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        Dim MultiCompAutoSuggest = prefixText & "#" & CStr(HttpContext.Current.Session("HideTestAcc")) & "#" & CStr(HttpContext.Current.Session("ISMainCat")) & "#" & CStr(HttpContext.Current.Session("MultiCompMode"))
        dt = ws.WSWorkOrder.GetAutoSuggest("SearchCompanyNameWithAll", MultiCompAutoSuggest, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function
    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetWorkOrderID(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("WorkOrderID", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function
    Public Sub rdbtnCompanySel_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdBtnSingleCompany.CheckedChanged, rdBtnMultiCompany.CheckedChanged
        Session("AdminSelectedCompID") = -1
        Session("dsMultiSelectedCompany") = Nothing
        Session("BillLoc") = 0
        ddlBillLoc.Visible = False
        pnlSelectedCompany.Visible = False
        Session("HideTestAcc") = Nothing
        Session("ISMainCat") = Nothing
        Session("MultiCompMode") = Nothing
        If (rdBtnSingleCompany.Checked) Then
            UCSearchContact1.Visible = True
            txtCompanyName.Style.Add("display", "none")
        Else
            UCSearchContact1.Visible = False
            txtCompanyName.Style.Add("display", "inline")
            Session("HideTestAcc") = chkHideTestAcc.Checked
            Session("ISMainCat") = chkMainCat.Checked
            Session("MultiCompMode") = Request.QueryString("mode")
        End If
    End Sub
End Class