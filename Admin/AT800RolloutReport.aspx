<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master"
    EnableViewState="true" CodeBehind="AT800RolloutReport.aspx.vb" Inherits="Admin.AT800RolloutReport"
    Title="At800 Rollout Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="tsm" runat="server" EnablePageMethods="true" />
    <div style="padding: 20px; margin: 0px auto;">
        <div style="margin: auto; font-size: small; font-family: Arial; font-weight: bold;">
            at800 Rollout Report from 1st July 2013
        </div>
        <br style="clear: both;" />
        <div style="margin: auto; font-weight: bold;">
            Date of Report:
            <asp:Label ID="lblSignOffSheetDate" runat="server" />
        </div>
        <br style="clear: both;" />
        <asp:Button ID="btnRefresh" OnClick="grdView_Filter" runat="server" Text="Refresh" />
        <asp:Button ID="btnExport" OnClick="grdView_Export" runat="server" Text="Export" />
        <br style="clear: both;" />
        <br />
        <style type="text/css">
            .grdView td, .grdView th
            {
                padding: 3px;
                vertical-align:top;
            }
            .grdView tr td:first-child
            {
                vertical-align:middle !important;
            }
            .grdView td span
            {
                display: block;
            }
            .grdView select
            {
                width: 90%;
                vertical-align: bottom;
            }
            .grdView a, .grdView th span
            {
                vertical-align: top;
                line-height: 14px;
                height: 15px;
                display: block;
            }
            .grdViewHeader
            {
                line-height: 35px;
                background-color: #CCCCCC;
            }
            .DatePanel
            {
                width:100px;
            }
            .DatePanel input
            {
                width:70px;
                font-size:10pt !important;
                margin-right:5px;
                padding:2px;
            }
            .DatePanel *
            {
                font-size:8pt !important;
                padding:0px !important;
            }
        </style>
        <script type="text/javascript">
            function update(a, b) {
                a.style.backgroundColor = "#FF9900";
                var rowId = a.id.replace("ctl00_ContentPlaceHolder1_grdView_", "").replace('_' + b, '');
                var hidWOID = document.getElementById("ctl00_ContentPlaceHolder1_grdView_" + rowId + "_hidWOID");
                //                var txtWORefNo = document.getElementById("ctl00_ContentPlaceHolder1_grdView_" + rowId + "_txtWORefNo");
                var txtTCCRefNo = document.getElementById("ctl00_ContentPlaceHolder1_grdView_" + rowId + "_txtTCCRefNo");
                var txtVisitDate = document.getElementById("ctl00_ContentPlaceHolder1_grdView_" + rowId + "_txtVisitDate");
                //                var ddlJobCancelled = document.getElementById("ctl00_ContentPlaceHolder1_grdView_" + rowId + "_ddlJobCancelled");
                var ddlCancellationReason = document.getElementById("ctl00_ContentPlaceHolder1_grdView_" + rowId + "_ddlCancellationReason");
                var ddlOriginalJobType = document.getElementById("ctl00_ContentPlaceHolder1_grdView_" + rowId + "_ddlOriginalJobType");
                var ddlFinalJobType = document.getElementById("ctl00_ContentPlaceHolder1_grdView_" + rowId + "_ddlFinalJobType");
                var ddlJobCompletionReason = document.getElementById("ctl00_ContentPlaceHolder1_grdView_" + rowId + "_ddlJobCompletionReason");
                var txtNotes = document.getElementById("ctl00_ContentPlaceHolder1_grdView_" + rowId + "_txtNotes");
                //                PageMethods.AT800RolloutUpdate(hidSignOffSheetID.value, txtWORefNo.value, txtTCCRefNo.value, ddlVisitDate.value.toString(), ddlJobCancelled.value.toString(), ddlCancellationReason.value, ddlOriginalJobType.value, ddlFinalJobType.value, ddlJobCompletionReason.value, txtNotes.value, a.id, SuccessFunction, FailedFunction);
                //                PageMethods.AT800RolloutUpdate(hidSignOffSheetID.value, txtWORefNo.value, txtTCCRefNo.value, ddlVisitDate.value.toString(), ddlCancellationReason.value, ddlOriginalJobType.value, ddlFinalJobType.value, ddlJobCompletionReason.value, txtNotes.value, a.id, SuccessFunction, FailedFunction);
                PageMethods.AT800RolloutUpdate(hidWOID.value, txtTCCRefNo.value, txtVisitDate.value.toString(), ddlCancellationReason.value, ddlOriginalJobType.value, ddlFinalJobType.value, ddlJobCompletionReason.value, txtNotes.value, a.id, SuccessFunction, FailedFunction);
            }
            function SuccessFunction(ReturnValue) {
                document.getElementById(ReturnValue).style.backgroundColor = "#00FF33";
            }
            function FailedFunction(ReturnValue) {
                document.getElementById(ReturnValue).style.backgroundColor = "#FF0000";
            }
        </script>
        <asp:GridView CssClass="grdView" ID="grdView" runat="server" AllowSorting="true"
            AllowPaging="false" AutoGenerateColumns="false" OnSorting="grdView_Sorting" OnRowDataBound="grdView_DataBound"
            ShowHeaderWhenEmpty="true">
            <HeaderStyle CssClass="grdViewHeader"></HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="Completion Form" SortExpression="FilePath">
                    <HeaderTemplate>
                        <asp:Label ID="lblCompletionFormHeader" runat="server" Text="Completion Form" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="imgFilePath" runat="server" ImageUrl='<%# GetFileIcon(Eval("FilePath")) %>'
                            NavigateUrl='<%# Eval("FilePath") %>' Target="_blank" Style="text-align: center;"
                            ViewStateMode="Disabled" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Region" SortExpression="Region">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblRegionHeader" runat="server" Text="Region" CommandArgument="Region"
                            CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="true" OnDataBound="AddPleaseSelect"
                            DataTextField="Region" DataValueField="Region" OnSelectedIndexChanged="grdView_Filter" style="min-width:50px;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("Region") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Job Submitted" SortExpression="JobSubmitted">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblJobSubmittedHeader" runat="server" Text="Job Submitted" CommandArgument="JobSubmitted"
                            CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlJobSubmitted" runat="server" AutoPostBack="true" OnDataBound="AddPleaseSelect"
                            DataTextField="date" DataValueField="date" OnSelectedIndexChanged="grdView_Filter" style="min-width:50px;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("JobSubmitted") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="WO Ref No" SortExpression="WORefNo">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblWORefNoHeader" runat="server" Text="WO Ref No" CommandArgument="WORefNo"
                            CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:TextBox ID="txtWORefNo" runat="server" OnTextChanged="grdView_Filter" AutoPostBack="true"
                            Style="width: 80px;" />
                        <ajaxToolkit:AutoCompleteExtender runat="server" ID="acWORefNo" TargetControlID="txtWORefNo"
                            ServiceMethod="GetAt800WORefNo" MinimumPrefixLength="2" CompletionInterval="100"
                            EnableCaching="true" CompletionSetCount="100" FirstRowSelected="false">
                        </ajaxToolkit:AutoCompleteExtender>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hidWOID" runat="server" Value='<%#Eval("WOID") %>' />
                        <%#Eval("WORefNo") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TCC Ref No" SortExpression="TCCRefNo">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblTCCRefNoHeader" runat="server" Text="TCC Ref No" CommandArgument="TCCRefNo"
                            CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:TextBox ID="txtTCCRefNo" runat="server" OnTextChanged="grdView_Filter" AutoPostBack="true"
                            Style="width: 80px;" />
                        <ajaxToolkit:AutoCompleteExtender runat="server" ID="acTCCRefNo" TargetControlID="txtTCCRefNo"
                            ServiceMethod="GetAt800TCCRefNo" MinimumPrefixLength="2" CompletionInterval="100"
                            EnableCaching="true" CompletionSetCount="100" FirstRowSelected="false">
                        </ajaxToolkit:AutoCompleteExtender>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtTCCRefNo" runat="server" Text='<%#Bind("TCCRefNo") %>' Style="width: 60px;"
                            OnInit="AddJS" ViewStateMode="Disabled" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Post Code" SortExpression="PostCode">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblPostCodeHeader" runat="server" Text="Post Code" CommandArgument="PostCode"
                            CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:TextBox ID="txtPostCode" onkeyup="makeUppercase(this.id)" MaxLength="8" runat="server" OnTextChanged="grdView_Filter" AutoPostBack="true"
                            Style="width: 80px;" />
                        <ajaxToolkit:AutoCompleteExtender runat="server" ID="acPostCode" TargetControlID="txtPostCode"
                            ServiceMethod="GetAt800PostCodes" MinimumPrefixLength="2" CompletionInterval="100"
                            EnableCaching="true" CompletionSetCount="100" FirstRowSelected="false">
                        </ajaxToolkit:AutoCompleteExtender>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("Postcode") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Visit Date" SortExpression="VisitDate">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblVisitDateHeader" runat="server" Text="Visit Date" CommandArgument="VisitDate"
                            CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlVisitDate" runat="server" AutoPostBack="true" OnDataBound="AddPleaseSelect"
                            DataTextField="date" DataValueField="date" OnSelectedIndexChanged="grdView_Filter" style="min-width:50px;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Panel ID="pnlVisitDate" runat="server" CssClass="DatePanel">
                            <asp:TextBox ID="txtVisitDate" runat="server" Style="float: left;" BackColor="#e6e6e6" ForeColor="Black" Readonly="true" OnInit="AddJS" />
                            <img alt="Click to Select" src="~/Images/calendar.gif" id="btnBeginDate" runat="server"
                                style="cursor: pointer; vertical-align: middle; float: left;" />
                            <ajaxToolkit:CalendarExtender runat="server" TargetControlID="txtVisitDate" Format="dd/MM/yyyy"
                                PopupButtonID="btnBeginDate" ID="calVisitDate" />
                            <%--<asp:DropDownList ID="ddlVisitDate" runat="server" OnDataBound="AddEmpty" DataTextField="VisitDate"
                            DataValueField="VisitDate" Style="width: 90px;" OnInit="AddJS" ViewStateMode="Disabled" />--%>
                        </asp:Panel>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="RC" SortExpression="RC">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblRCHeader" runat="server" Text="RC" CommandArgument="RC" CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlRC" runat="server" AutoPostBack="true" OnDataBound="AddPleaseSelect"
                            DataTextField="RC" DataValueField="RC" OnSelectedIndexChanged="grdView_Filter" style="min-width:50px;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("RC") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Issue (Y/N) See Notes" SortExpression="IsIssue">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblIsIssueHeader" runat="server" Text="Issue (Y/N) See Notes"
                            CommandArgument="IsIssue" CommandName="Sort" Style="width: 70px;" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlIsIssue" runat="server" AutoPostBack="true" OnDataBound="AddPleaseSelect"
                            DataTextField="IsIssue" DataValueField="IsIssue" OnSelectedIndexChanged="grdView_Filter" style="min-width:50px;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("IsIssue") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Job Cancelled" SortExpression="JobCancelled">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblJobCancelledHeader" runat="server" Text="Job Cancelled" CommandArgument="JobCancelled"
                            CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlJobCancelled" runat="server" AutoPostBack="true" OnDataBound="AddPleaseSelect"
                            DataTextField="date" DataValueField="date" OnSelectedIndexChanged="grdView_Filter" style="min-width:50px;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("JobCancelled") %>
                        <%--<asp:DropDownList ID="ddlJobCancelled" runat="server" OnDataBound="AddEmpty" DataTextField="JobCancelled"
                            DataValueField="JobCancelled" Style="width: 90px;" OnInit="AddJS" />--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cancellation Reason" SortExpression="CancellationReason">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblCancellationReasonHeader" runat="server" Text="Cancellation Reason"
                            CommandArgument="CancellationReason" CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlCancellationReason" runat="server" AutoPostBack="true" OnDataBound="AddPleaseSelect"
                            DataTextField="StandardValue" DataValueField="StandardValue" OnSelectedIndexChanged="grdView_Filter" style="min-width:50px;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlCancellationReason" runat="server" OnDataBound="AddEmpty"
                            DataTextField="StandardValue" DataValueField="StandardValue" OnInit="AddJS" ViewStateMode="Disabled" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sign off sheet Received" SortExpression="SignOffSheetReceived">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblSignoffsheetReceivedHeader" runat="server" Text="Sign off sheet Received"
                            CommandArgument="SignOffSheetReceived" CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlSignOffSheetReceived" runat="server" AutoPostBack="true"
                            OnDataBound="AddPleaseSelect" DataTextField="date" DataValueField="date"
                            OnSelectedIndexChanged="grdView_Filter" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("SignoffsheetReceived") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Job Closed" SortExpression="JobClosed">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblJobClosedHeader" runat="server" Text="Job Closed" CommandArgument="JobClosed"
                            CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlJobClosed" runat="server" AutoPostBack="true" OnDataBound="AddPleaseSelect"
                            DataTextField="date" DataValueField="date" OnSelectedIndexChanged="grdView_Filter" style="min-width:50px;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("JobClosed") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Original Job Type" SortExpression="OriginalJobType">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblOriginalJobTypeHeader" runat="server" Text="Original Job Type"
                            CommandArgument="OriginalJobType" CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlOriginalJobType" runat="server" AutoPostBack="true" OnDataBound="AddPleaseSelect"
                            DataTextField="JobType" DataValueField="JobType" OnSelectedIndexChanged="grdView_Filter" style="min-width:50px;"/>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlOriginalJobType" runat="server" OnDataBound="AddEmpty" DataTextField="JobType"
                            DataValueField="JobType" Style="width: 80px;" OnInit="AddJS" ViewStateMode="Disabled" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Final Job Type" SortExpression="FinalJobType">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblFinalJobTypeHeader" runat="server" Text="Final Job Type" CommandArgument="FinalJobType"
                            CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlFinalJobType" runat="server" AutoPostBack="true" OnDataBound="AddPleaseSelect"
                            DataTextField="JobType" DataValueField="JobType" OnSelectedIndexChanged="grdView_Filter" style="min-width:50px;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlFinalJobType" runat="server" OnDataBound="AddEmpty" DataTextField="JobType"
                            DataValueField="JobType" Style="width: 80px;" OnInit="AddJS" ViewStateMode="Disabled" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Job Completion Reason" SortExpression="JobCompletionReason">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblJobCompletionReasonHeader" runat="server" Text="Job Completion Reason"
                            CommandArgument="JobCompletionReason" CommandName="Sort" />
                        <br style="clear: both;" />
                        <asp:DropDownList ID="ddlJobCompletionReason" runat="server" AutoPostBack="true"
                            OnDataBound="AddPleaseSelect" DataTextField="JobCompletionReason" DataValueField="JobCompletionReason"
                            OnSelectedIndexChanged="grdView_Filter" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlJobCompletionReason" runat="server" OnDataBound="AddEmpty"
                            DataTextField="JobCompletionReason" DataValueField="JobCompletionReason" Style="width: 80px;"
                            OnInit="AddJS" ViewStateMode="Disabled" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Notes" SortExpression="Notes">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblNotesHeader" runat="server" Text="Notes" CommandArgument="Notes"
                            CommandName="Sort" />
                        <br style="clear: both;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtNotes" runat="server" Text='<%#Bind("Notes") %>' TextMode="MultiLine"
                            Rows="3" OnInit="AddJS" ViewStateMode="Disabled" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Customer Name" SortExpression="CustomerName">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblCustomerNameHeader" runat="server" Text="Customer Name" CommandArgument="CustomerName"
                            CommandName="Sort" />
                        <br style="clear: both;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("CustomerName") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Customer Address" SortExpression="CustomerAddress">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblCustomerAddressHeader" runat="server" Text="Customer Address"
                            CommandArgument="CustomerAddress" CommandName="Sort" />
                        <br style="clear: both;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("CustomerAddress") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Customer Phone" SortExpression="CustomerPhone">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblCustomerPhoneHeader" runat="server" Text="Customer Phone"
                            CommandArgument="CustomerPhone" CommandName="Sort" />
                        <br style="clear: both;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("CustomerPhone") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Installation Time" SortExpression="InstallationTime">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblInstallationTimeHeader" runat="server" Text="Installation Time"
                            CommandArgument="InstallationTime" CommandName="Sort" />
                        <br style="clear: both;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("InstallationTime") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Engineer Instructions" SortExpression="EngineerInstructions">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblEngineerInstructionsHeader" runat="server" Text="Engineer Instructions"
                            CommandArgument="EngineerInstructions" CommandName="Sort" />
                        <br style="clear: both;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("EngineerInstructions") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="OW Instructions" SortExpression="OWInstructions">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lblOWInstructionsHeader" runat="server" Text="OW Instructions"
                            CommandArgument="OWInstructions" CommandName="Sort" />
                        <br style="clear: both;" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%#Eval("OWInstructions") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:Label CssClass="bold" ID="txtTotalDisplay" Text="" runat="server" Style="clear: both;" />
    </div>
</asp:Content>
