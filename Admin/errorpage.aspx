<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="errorpage.aspx.vb" Inherits="Admin.errorpage" %>

<!--

page directive


-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">  
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/AdminTemplate.dwt" codeOutsideHTMLIsLocked="false" -->
<head runat=server>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>OrderWork :</title>
<!-- InstanceEndEditable --><!-- InstanceParam name="opRgNewsletter" type="boolean" value="false" --><!-- InstanceParam name="opRgClient" type="boolean" value="false" --><!-- InstanceParam name="opRgSupplier" type="boolean" value="false" --><!-- InstanceParam name="opLogo" type="boolean" value="false" --><!-- InstanceParam name="opHomeFlash" type="boolean" value="false" -->
<script language="JavaScript" type="text/JavaScript" src="JS/Scripts.js"></script>
<script language="javascript" src="JS/Scripts.js"  type="text/javascript"></script>
<!-- InstanceParam name="OpMenuRedTopCurves" type="boolean" value="true" --><!-- InstanceParam name="OpMenuRedBtmCurve" type="boolean" value="true" --><!-- InstanceParam name="OpMenuOrangeTopCurve" type="boolean" value="false" --><!-- InstanceParam name="OpMenuBtmOrangeCurve" type="boolean" value="false" --><!-- InstanceParam name="onLoad" type="text" value="" --><!-- InstanceParam name="OpQLRegion" type="boolean" value="false" --><!-- InstanceParam name="OpContentTopCurve" type="boolean" value="true" --><!-- InstanceParam name="OpUCMenu" type="boolean" value="false" --><!-- InstanceParam name="OpContentTopMargin" type="boolean" value="false" --><!-- InstanceParam name="OpAddSpecialist" type="boolean" value="false" --><!-- InstanceParam name="OpAddLocations" type="boolean" value="false" --><!-- InstanceParam name="OpLogoMessage" type="boolean" value="false" --><!-- InstanceParam name="SpacNavAdminHome" type="boolean" value="false" --><!-- InstanceParam name="OpUCSubMenu" type="boolean" value="false" --><!-- InstanceParam name="OpRgLogin" type="boolean" value="false" --><!-- InstanceParam name="OpRgSupplierLogin" type="boolean" value="false" --><!-- InstanceParam name="OpRgClientLogin" type="boolean" value="false" -->
<link href="Styles/OrderWork.css" rel="stylesheet" type="text/css">
<link href="Styles/OrderWork.css" rel="stylesheet" type="text/css">
<link href="Styles/Menu.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<style>
#divMenu {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	line-height: 12px;
	font-weight: normal;
	text-decoration: none;
	width: 100%;
	background-position: right top;
	background-repeat: no-repeat;
	border-top-width: 1px;
	border-top-style: solid; 
	border-top-color: #CECECE;
	background-color: #993366;
}

.divContentTopRed{
	width: 100%;
	background-color: #993366;
	background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
	height: 5px;
	
}
</style>
<!-- InstanceEndEditable --><!-- InstanceParam name="opAccountSumm" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddUser" type="boolean" value="false" --><!-- InstanceParam name="OpRgCLAddLOcation" type="boolean" value="false" --><!-- InstanceParam name="OPWOSummary" type="boolean" value="false" --><!-- InstanceParam name="RgOpBtnNewWO" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddFunds" type="boolean" value="false" --><!-- InstanceParam name="opMenuRegion" type="boolean" value="false" --><!-- InstanceParam name="opContentTopRedCurve" type="boolean" value="false" --><!-- InstanceParam name="opUsersProfile" type="boolean" value="false" --><!-- InstanceParam name="opCompanyProfile" type="boolean" value="false" --><!-- InstanceParam name="opUsersListing" type="boolean" value="false" --><!-- InstanceParam name="opLocations" type="boolean" value="false" --><!-- InstanceParam name="opReferences" type="boolean" value="false" --><!-- InstanceParam name="opPreferences" type="boolean" value="false" --><!-- InstanceParam name="opUsers" type="boolean" value="false" --><!-- InstanceParam name="width" type="text" value="985" --><!-- InstanceParam name="opComments" type="boolean" value="false" --><!-- InstanceParam name="OpRgScriptManager" type="boolean" value="true" --><!-- InstanceParam name="OpFavSuppliers" type="boolean" value="false" --><!-- InstanceParam name="OpFavSuppliersLink" type="boolean" value="false" -->
</head>



<body onLoad=";">
<form id="Form1" method="post" runat="server">

 
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>

<table width="985" height="100%"  border="0" align="center" cellpadding="0" cellspacing="0"> 
  <tr>
    <td valign="top" bgcolor="#f4f5f0">
	
	<div id="divLogo">
      <div class="roundtopSwitch"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
      <table width="100%" height="54" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><!-- InstanceBeginEditable name="EdLogo" --><img src="Images/newOWHeader.png" alt="OrderWork - Trading IT Skills Across the UK" border="0" class="LogoImg" /><!-- InstanceEndEditable --></td>
          <td align="right" valign="bottom" class="LogoTopMsg"></td>
          <td width="19" align="right" valign="bottom" class="footerTxt">&nbsp;</td>
        </tr>
      </table>
      <div class="roundbottomSwitch"><img src="Images/Curves/Main-LBC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
    </div>
    
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
                <asp:Label ID="lblText" runat="server"></asp:Label><input name="hdnError" runat="server" type="hidden" id="hdnError" value=""><asp:Button ID="btnError" runat="server" CssClass="ButtonClass" Text="Show Error"/>
				<br/><asp:Label ID="lblError" runat="server"></asp:Label>
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div></td>
  </tr>
  <tr>
    <td height="51" valign="top" bgcolor="#f4f5f0"><table width="945" height="46"  border="0" align="center" cellpadding="0" cellspacing="0" class="BdrTop">
      <tr valign="top">
        <td width="135" align="left" bgcolor="#f4f5f0" class="footerTxt"><img src="Images/Phone-Icon.gif" alt="Phone - 0203 053 0343" width="15" height="12" align="absmiddle" class="marginR5" /> 0203 053 0343 </td>
        <td align="left" bgcolor="#f4f5f0" class="footerTxt"><a href="mailto:info@orderwork.co.uk" class="footerTxtSelected"><img src="Images/Email-Icon.gif" alt="Contact Us" width="16" height="15" border="0" align="absmiddle" class="marginR5" />&nbsp; info@orderwork.co.uk</a></td>
        <td align="right" bgcolor="#f4f5f0" class="footerTxt"><!-- InstanceBeginEditable name="EdPrivacyPolicy" --><a href="https://www.orderwork.co.uk/privacy-policy/" target="_blank" class="footerTxtSelected">Policies</a><!-- InstanceEndEditable --></td>
      </tr>
    </table>	
    <div class="roundbottomWhite"><img src="Images/Curves/Main-LBC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div></td>
  </tr>
  <tr>
    <td height="35" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="footerTxt"><span >Copyright &copy; <script language="javascript" type="text/javascript">
									var currDate = new Date();
									document.write(currDate.getFullYear());
									</script> OrderWork Ltd.</span></td>
        <td align="right" class="footerTxt"> <!-- Design &amp; Developed by<a href="http://www.iniquus.com" target="_blank" class="footerTxt"> Iniquus</a> --></td>
      </tr>
    </table></td>
  </tr>
</table>
</form>
</body>
<!-- InstanceEnd --></html>
