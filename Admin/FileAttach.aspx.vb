Partial Public Class FileAttach
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request("Type") = "Insurance" Then
            pnlNormalAttach.Visible = False
            pnlInsuranceAttach.Visible = True
            ltStyle.Text = "body { margin:0px 0px 0px 0px;background-color:#F4F5EF;}"
        ElseIf Request("AttachLinkSource") = "WOComplete" Then
            pnlNormalAttach.Visible = True
            pnlInsuranceAttach.Visible = False
            ltStyle.Text = "body { margin:0px 0px 0px 0px;background-color:#F4F5EF;}"
        ElseIf Request("Type") = "WorkOrder" Then
            pnlNormalAttach.Visible = True
            pnlInsuranceAttach.Visible = False
            ltStyle.Text = "body { margin:0px 0px 0px 0px;background-color:#F4F5EF;}"
        ElseIf Request("Type") = "StatementOfWork" Then
            If Not IsNothing(Request("parentname")) Then
                If Request("parentname") <> "" Then
                    pnlNormalAttach.Visible = False
                    pnlStatementOfWork.Visible = True
                    ltStyle.Text = "body { margin:0px 0px 0px 0px;background-color:#F9EBDE;}"
                Else
                    pnlNormalAttach.Visible = False
                    pnlStatementOfWork.Visible = True
                    ltStyle.Text = "body { margin:0px 0px 0px 0px;background-color:#F4F5EF;}"
                End If
            Else
                pnlNormalAttach.Visible = False
                pnlStatementOfWork.Visible = True
                ltStyle.Text = "body { margin:0px 0px 0px 0px;background-color:#F4F5EF;}"
            End If
        Else
            pnlNormalAttach.Visible = True
            pnlInsuranceAttach.Visible = False
            ltStyle.Text = "body { margin:0px 0px 0px 0px;background-color:#F4F5EF;}"
        End If
    End Sub

End Class