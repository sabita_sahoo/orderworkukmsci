﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAllInvoices.aspx.vb" Inherits="Admin.ViewAllInvoices" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : View All Invoices"%>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
    	<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
    	<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
	        <div id="divContent">
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
  		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
     <ContentTemplate>
			<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10px"></td>
			<td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="All Invoices" runat="server"></asp:Label></td>
			</tr>
			</table>		
             <table cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
            <tr>
              <td width="165" class="txtWelcome paddingL10"><div id="divButton" style="width: 165px;  ">
                  <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                  <a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" title="Back to Company Profile" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8" /><strong>Back to Company Profile</strong></a>
                  <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
              </div></td>                  
            </tr>
    </table>
	<asp:Panel runat="server" ID="PnlListing">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
		
		<td width="10px"></td>		
		<td width="300"><table width="100%" cellpadding="0" cellspacing="0" border="0">
			 <tr>
			   <td width="300">
			     <%@ Register TagPrefix="uc1" TagName="UCDateRange" Src="~/UserControls/UK/UCDateRangeUK.ascx" %>
                 <uc1:UCDateRange id="UCDateRange1" runat="server"></uc1:UCDateRange> 
			   </td>
			  </tr>
			</table>
		</td>
		<td align="left" width="100">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                <asp:LinkButton ID="lnkView" causesValidation=False OnClick="lnkView_Click" runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
            </div>		
		</td>	
        <td runat="server" id="tdExport" visible="false">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                            <a runat="server" target="_blank" id="btnExport" class="txtButtonRed"> &nbsp;Export to Excel&nbsp;</a>
           </div>
        </td>		 		
		<td align="left">&nbsp;</td>
		</tr>
		</table>	
		
	
           <table><tr>
             <td class="paddingL10 padB21" valign="top" padB21 paddingTop1>
		<asp:Label ID="lblMsg" runat="server"  CssClass="bodytxtValidationMsg" Text=""></asp:Label>		  	
		</td></tr>
             </table>
	   <div id="div1" class="divredBorder">
              <div class="roundtabRed"><img src="../Images/Curves/TabRed-LC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
            </div>
           	<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td>
              <asp:GridView ID="gvInvoices"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
               PagerSettings-Mode=NextPreviousFirstLast  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               
               <PagerTemplate>
                   
            <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>																				
							<td width="10">&nbsp;</td>												  
						  					
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right" runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
        </PagerTemplate> 
               <Columns>  
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="18%"  HeaderStyle-CssClass="gridHdr"  DataField="InvoiceNumber" SortExpression="InvoiceNumber" HeaderText="Invoice Number" />            
                    <asp:TemplateField ItemStyle-Width="10%" SortExpression="InvoiceDate" HeaderText="Invoice Date" HeaderStyle-CssClass="gridHdr"  >               
                    <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" />
                        <ItemTemplate>             
                            <%#Strings.FormatDateTime(Container.DataItem("InvoiceDate"), DateFormat.ShortDate)%>
                        </ItemTemplate>
                    </asp:TemplateField>                     
                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%"  HeaderStyle-CssClass="gridHdr"  DataField="InvoiceType" SortExpression="InvoiceType" HeaderText="Invoice Type" />            
                    <asp:BoundField ItemStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr"  DataField="CompanyName" SortExpression="CompanyName" HeaderText="Company Name" />            
                  
                    <asp:TemplateField SortExpression="InvoiceValue" HeaderText="Invoice Value" >               
									   <HeaderStyle CssClass="gridHdr"/>
                                    <ItemStyle CssClass="gridRow"/>

									   <ItemTemplate>  
										<%#Strings.FormatNumber(Container.DataItem("InvoiceValue"), 2, TriState.False, TriState.False, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>

                    <asp:BoundField ItemStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr"  DataField="InvoiceStatus" SortExpression="InvoiceStatus" HeaderText="Invoice Status" />            
                    <asp:TemplateField  SortExpression="Discount" HeaderText="Discount" ItemStyle-Width="8%">               
                    <HeaderStyle  CssClass="gridHdr" />
                    <ItemStyle CssClass="gridRow"/>
                        <ItemTemplate>             
                            <%#IIf(Container.DataItem("Discount"), "Y", "N")%>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField ItemStyle-Width="10%">               
                    <HeaderStyle  CssClass="gridHdr" />
                    <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow"/>
                        <ItemTemplate>             
                            <%# GetLinks(Container.DataItem("InvoiceNumber"), Container.DataItem("CompanyID"), Container.DataItem("WorkOrderId"), Container.DataItem("InvoiceType"), Container.DataItem("AllocatedTo"), Container.DataItem("WOID"), Container.DataItem("InvoiceStatus"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                   <%-- <asp:TemplateField ItemStyle-Width="5%">               
                    <HeaderStyle  CssClass="gridHdr" />
                    <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow"/>
                        <ItemTemplate>             
                            <asp:ImageButton ID="ImgBtnPay" Visible='<%#iif(Session("RoleGroupID")<>admin.Applicationsettings.RoleOWRepID , iif(Container.dataitem("InvoiceType")="Sales", iif(Container.dataitem("InvoiceStatus")="Unpaid",true,false), false),false) %>' AlternateText="Mark SI as Paid" CommandArgument='<%#Container.dataitem("InvoiceNumber") & "," & Container.DataItem("CompanyID")%>' CommandName="PaySI" runat="server" ImageUrl="Images/Icons/Icon-Confirm.gif" ></asp:ImageButton>
                            <asp:ImageButton ID="ImgBtnPIAv" Visible='<%#iif(Session("RoleGroupID")<>admin.Applicationsettings.RoleOWRepID , iif(Container.dataitem("InvoiceType")="Purchase", iif(Container.dataitem("InvoiceStatus")="Unpaid" or Container.dataitem("InvoiceStatus")="On Hold",true,false), false),false) %>' AlternateText="Mark PI as Available" CommandArgument='<%#Container.dataitem("InvoiceNumber") %>' CausesValidation="false" OnClick="ImgBtnPIAv_Click" runat="server" Imageurl="Images/Icons/MakeInvoiceAvailable.gif" Height=13 Width=14 ></asp:ImageButton>
                            <asp:ImageButton ID="ImgBtnCNPay" Visible='<%#iif(Session("RoleGroupID")<>admin.Applicationsettings.RoleOWRepID , iif(Container.dataitem("InvoiceType")="Credit Note", iif(Container.dataitem("InvoiceStatus")="UnPaid",true,false), false),false) %>' AlternateText="Mark Credit Note as Paid" CommandArgument='<%#Container.dataitem("InvoiceNumber") & "," & Container.DataItem("CompanyID")%>' CommandName="PayCN" runat="server" Imageurl="Images/Icons/Icon-Confirm.gif" ></asp:ImageButton>
                        </ItemTemplate>
                    </asp:TemplateField>   --%>                            
                </Columns>
               
                 <AlternatingRowStyle CssClass="gridRow" />
                 <RowStyle CssClass="gridRow"/>            
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
        
              </asp:GridView>
				 </td>
            </tr>
            </table>
				
				 <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.ViewAllInvoices" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>

        
          </asp:Panel>		
		  
		  
		  
		  </ContentTemplate>
      
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align="middle" src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
         
				     
         
            
      
			   
		 
           <!-- InstanceEndEditable --> </td></tr>
         </table>
      </div>
</asp:Content>
