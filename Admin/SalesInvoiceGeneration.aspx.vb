
Partial Public Class SalesInvoiceGeneration
    Inherits System.Web.UI.Page

    Protected WithEvents UCDateRange1 As UCDateRange
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents txtInvoiceDate As TextBox
    Protected WithEvents chkListingFee As CheckBox
    Protected WithEvents txtListingFee As TextBox
    Protected WithEvents lblPoundSign As Label
    Protected WithEvents rptWOID As Global.System.Web.UI.WebControls.Repeater

    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            gvWorkOrders.PageSize = ApplicationSettings.GridViewPageSize
            Security.SecurePage(Page)
            If Not Request("sender") Is Nothing Then

                ProcessBackToListing()

            Else
                UCSearchContact1.BizDivID = Session("BizDivId")
                UCSearchContact1.ClassId = 0
                UCSearchContact1.Filter = "GenerateSI"
                UCSearchContact1.populateContact()
                Dim currDate As DateTime = System.DateTime.Today
                ViewState!SortExpression = "CloseDate"
                gvWorkOrders.Sort(ViewState!SortExpression, SortDirection.Ascending)
                ViewState("ContactId") = UCSearchContact1.ddlContact.SelectedValue
                txtInvoiceDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
                btnExport.HRef = "ExportToExcel.aspx?page=SalesInvoiceGeneration&bizDivId=" & Session("BizDivId") & "&ContactId=" & ViewState("ContactId") & "&sortExpression=" & gvWorkOrders.SortExpression
            End If
        End If
    End Sub
    Public Sub ProcessBackToListing()
        UCSearchContact1.BizDivID = Session("BizDivId")
        UCSearchContact1.ClassId = 0
        UCSearchContact1.Filter = "GenerateSI"
        UCSearchContact1.populateContact()
        Dim lItem As ListItem = UCSearchContact1.ddlContact.Items.FindByValue(Request("companyId"))
        If Not lItem Is Nothing Then
            UCSearchContact1.ddlContact.SelectedValue = Request("companyId")
            UCSearchContact1.SelectedContact = Request("companyId")
            ViewState("ContactId") = Request("companyId")
        End If
        txtInvoiceDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)


        gvWorkOrders.PageIndex = Request("PN")
        gvWorkOrders.Sort(Request("SC"), Request("SO"))
        PopulateGrid()
    End Sub

    Public Sub PopulateGrid()
        gvWorkOrders.DataBind()
        btnExport.HRef = "ExportToExcel.aspx?page=SalesInvoiceGeneration&bizDivId=" & Session("BizDivId") & "&ContactId=" & ViewState("ContactId") & "&sortExpression=" & gvWorkOrders.SortExpression
    End Sub
    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then


            ViewState("ContactId") = UCSearchContact1.ddlContact.SelectedValue
            PopulateGrid()
            divValidationMain.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvWorkOrders.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvWorkOrders.DataBind()
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Private Sub ObjectDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles ObjectDataSource1.Selecting
        InitializeGridParameters()
    End Sub
    ''' <summary>
    ''' Initialise paramter before calling the selectDB
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InitializeGridParameters()
        If chkShow100.Checked Then
            gvWorkOrders.PageSize = 100
        ElseIf chkShowAll.Checked Then
            If Not IsNothing(ViewState("rowCount")) And ViewState("rowCount") <> 0 Then
                gvWorkOrders.PageSize = ViewState("rowCount")
            Else
                gvWorkOrders.PageSize = 1000
            End If
        Else
            gvWorkOrders.PageSize = 25
        End If
    End Sub
    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvWorkOrders.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvWorkOrders, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex


    End Sub
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                       ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet

        Dim bizDivId As Integer = Session("BizDivId")
        Dim contactid As String = ""
        If CStr(ViewState("ContactId")) = "" Then
            contactid = "0"
        Else
            contactid = ViewState("ContactId")
        End If
        Dim maxRows As Integer = 0
        If chkShowAll.Checked Then
            maxRows = 0
        Else
            If chkShow100.Checked Then
                maxRows = 100
            Else
                maxRows = maximumRows
            End If
        End If

        Dim cacheKey As String = ""
        cacheKey = "GetClosedWorkOrder-Company" & "-" & "-" & contactid & "-SessionID-" & Page.Session.SessionID
        Dim KillCache As Boolean
        KillCache = hdnKillCache.Value
        If KillCache = True Then
            Page.Cache.Remove(cacheKey)
        End If

        If Page.Cache(cacheKey) Is Nothing Then
            ws.WSContact.Timeout = 300000
            ds = ws.WSWorkOrder.MS_GetClosedWOUnInvoiced(bizDivId, contactid, "", "", sortExpression, startRowIndex, maxRows)
            Page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(Page.Cache(cacheKey), DataSet)
        End If

        ViewState("rowCount") = ds.Tables("tblCount").Rows(0)(0)
        If Not ViewState("rowCount") Is Nothing Then
            If chkShowAll.Checked Then
                gvWorkOrders.AllowPaging = True
            Else
                gvWorkOrders.AllowPaging = True
            End If
        End If
        Return ds
    End Function

    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function
    Public Function GetLinks(ByVal companyId As String, ByVal WOID As String) As String
        Dim link As String = ""
        Dim linkParams As String = ""
        linkParams &= "bizDivId=" & Session("BizDivId")
        linkParams &= "&WOID=" & WOID
        linkParams &= "&companyId=" & companyId
        linkParams &= "&PS=" & gvWorkOrders.PageSize
        linkParams &= "&PN=" & gvWorkOrders.PageIndex
        linkParams &= "&SC=" & gvWorkOrders.SortExpression
        linkParams &= "&SO=" & gvWorkOrders.SortDirection
        linkParams &= "&Group=Invoiced"
        linkParams &= "&sender=SalesInvoiceGeneration"
        link &= "<a href='AdminWODetails.aspx?" & linkParams & "'><img src='Images/Icons/ViewDetails.gif' alt='view Work Order details' width='16' height='16' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        Return link
    End Function
    Public Function GetSupplierLinks(ByVal SupplierCompanyId As String, ByVal SupplierCompanyName As String, ByVal ClientCompanyID As String) As String
        Dim contactType As ApplicationSettings.ContactType = ApplicationSettings.ContactType.approvedSupplier
        Dim classId As String
        Dim statusId As String
        classId = ApplicationSettings.RoleSupplierID
        statusId = ApplicationSettings.StatusApprovedCompany

        Dim link As String = ""
        Dim linkParams As String = ""
        linkParams &= "bizDivId=" & Session("BizDivId")
        linkParams &= "&companyId=" & SupplierCompanyId
        linkParams &= "&ClientCompanyID=" & ClientCompanyID
        linkParams &= "&contactType=" & contactType
        linkParams &= "&classid=" & classId
        linkParams &= "&statusId=" & statusId
        linkParams &= "&PS=" & gvWorkOrders.PageSize
        linkParams &= "&PN=" & gvWorkOrders.PageIndex
        linkParams &= "&SC=" & gvWorkOrders.SortExpression
        linkParams &= "&SO=" & gvWorkOrders.SortDirection
        linkParams &= "&Group=Invoiced"
        linkParams &= "&sender=SalesInvoiceGeneration"

        Session(SupplierCompanyId & "_ContactClassID") = ApplicationSettings.RoleSupplierID

        link &= "<a class='footerTxtSelected ' href='CompanyProfile.aspx?" & linkParams & "'>" & SupplierCompanyName & "</a>"

        Return link
    End Function
    Public Function GetBuyerLinks(ByVal CompanyName As String, ByVal companyId As String) As String
        Dim link As String = ""
        Dim linkParams As String = ""
        linkParams &= "bizDivId=" & Session("BizDivId")
        linkParams &= "&companyId=" & companyId
        linkParams &= "&ClientCompanyID=" & companyId
        linkParams &= "&PS=" & gvWorkOrders.PageSize
        linkParams &= "&PN=" & gvWorkOrders.PageIndex
        linkParams &= "&SC=" & gvWorkOrders.SortExpression
        linkParams &= "&SO=" & gvWorkOrders.SortDirection
        linkParams &= "&Group=Invoiced"
        linkParams &= "&sender=SalesInvoiceGeneration"

        Session(companyId & "_ContactClassID") = ApplicationSettings.RoleClientID

        link &= "<a class='footerTxtSelected ' href='CompanyProfileBuyer.aspx?" & linkParams & "'>" & CompanyName & "</a>"

        Return link
    End Function

    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Private Sub gvWorkOrders_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvWorkOrders.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True
            End If
        End If

    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvWorkOrders.RowDataBound
        MakeGridViewHeaderClickable(gvWorkOrders, e.Row)
    End Sub

    ''' <summary>
    ''' SI Generation Confirmation Screen Code is written here
    ''' Displays Data in confiormation Screen
    ''' Created By Pankaj Malav
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub AssignInvoiceNo(ByVal sender As Object, ByVal e As EventArgs)
        lblMsg.Text = ""
        divValidationMain.Visible = False
        Dim workOrders As String = CommonFunctions.getSelectedIdsOfGrid(gvWorkOrders, "Check", "hdnWOID")
        ViewState("SelectedWOID") = workOrders
        If workOrders = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please Select atleast one closed work order."
            Return
        Else
            'Hide Grid view Panel and show panel for confirmation screen
            pnlShowClosedWorkOrders.Visible = False
            pnlShowInvoice.Visible = True
            pnlSearchTop.Visible = False
        End If
        Dim ListingFee As Decimal
        If chkListingFee.Checked Then
            ListingFee = CDec(txtListingFee.Text.Trim)
        Else
            ListingFee = 0
        End If
        ViewState("ListingFee") = ListingFee
        ViewState("CompanyId") = UCSearchContact1.ddlContact.SelectedValue
        Dim ds As New DataSet
        Dim Mode As String
        Mode = ""
        If (chkIndividualInvoice.Checked = True) Then
            Mode = "Individual"
        ElseIf (chkGroupInvoiceByPO.Checked = True And chkIndividualInvoice.Checked = False) Then
            Mode = "PONumber"
        Else
            Mode = ""
        End If
        ds = ws.WSFinance.GetSIConfirmationScreen(workOrders, Mode, ListingFee)

        If ds.Relations("LinksByHeader") Is Nothing Then
            'Declare the Columns - even though we have multiple fields, we only have two tables, hence TransactionColumns and DetailColumns
            Dim DistincttblColumns() As DataColumn
            Dim FilteredtblColumns() As DataColumn
            lblGroupingItemName.Text = "Billing Location"
            If (chkIndividualInvoice.Checked = True) Then
                DistincttblColumns = New DataColumn() {ds.Tables("tblParent").Columns("RefWOID"), ds.Tables("tblParent").Columns("BusinessArea")}
                FilteredtblColumns = New DataColumn() {ds.Tables("tblChild").Columns("RefWOID"), ds.Tables("tblChild").Columns("BusinessArea")}
            ElseIf (chkGroupInvoiceByPO.Checked = True And chkIndividualInvoice.Checked = False) Then
                lblGroupingItemName.Text = "PONumber"
                DistincttblColumns = New DataColumn() {ds.Tables("tblParent").Columns("PONumber"), ds.Tables("tblParent").Columns("BusinessArea")}
                FilteredtblColumns = New DataColumn() {ds.Tables("tblChild").Columns("PONumber"), ds.Tables("tblChild").Columns("BusinessArea")}
            Else
                DistincttblColumns = New DataColumn() {ds.Tables("tblParent").Columns("BillingLocID"), ds.Tables("tblParent").Columns("BusinessArea")}
                FilteredtblColumns = New DataColumn() {ds.Tables("tblChild").Columns("BillingLocID"), ds.Tables("tblChild").Columns("BusinessArea")}
            End If
            'We could also use all norminals, ordinals or any mixture of them
            ds.Relations.Add("LinksByHeader", DistincttblColumns, FilteredtblColumns, False)
        End If
        'Binding distinct table to Parent repeater
        rptSI.DataSource = ds.Tables("tblParent").Rows
        rptSI.DataBind()

    End Sub

    Private Sub chkListingFee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkListingFee.CheckedChanged
        If chkListingFee.Checked Then
            txtListingFee.Visible = True
            lblPoundSign.Visible = True
        Else
            txtListingFee.Visible = False
            lblPoundSign.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' Added By Pankaj Malav on 20 Nov
    ''' This function enables change of Billing location of work orders
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ChangeBillingLocation(ByVal sender As Object, ByVal e As EventArgs)
        lblMsg.Text = ""
        divValidationMain.Visible = False
        Dim workOrders As String = CommonFunctions.getSelectedIdsOfGrid(gvWorkOrders, "Check", "hdnWOID")
        If workOrders = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please Select atleast one closed work order."
            Return
        Else
            Dim lnk As String
            lnk = ""
            lnk = lnk & "ChangeBillingLocation.aspx?bizdivId=" & Session("BizDivId")
            lnk = lnk & "&woids=" & workOrders
            lnk = lnk & "&companyid=" & UCSearchContact1.ddlContact.SelectedValue
            lnk = lnk & "&PN=" & gvWorkOrders.PageIndex
            lnk = lnk & "&SC=" & gvWorkOrders.SortExpression
            lnk = lnk & "&SO=" & gvWorkOrders.SortDirection
            Response.Redirect(lnk)
        End If
    End Sub

    Public Sub UpdatePONumber(ByVal sender As Object, ByVal e As EventArgs)
        lblMsg.Text = ""
        divValidationMain.Visible = False
        Dim workOrders As String = CommonFunctions.getSelectedIdsOfGrid(gvWorkOrders, "Check", "hdnWOID")
        If workOrders = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please Select atleast one closed work order."
            Return
        Else
            txtPONumber.Text = ""
            ViewState.Add("SelectedWOID", workOrders)
            ViewState.Add("Mode", "BulkUpdate")
            mdlPONumber.Show()
        End If
    End Sub

    ''' <summary>
    ''' Handles Cancel click returns back to original closed wo listing hiding confirmation screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Show Grid view Panel and hide panel for confirmation screen
        pnlShowClosedWorkOrders.Visible = True
        pnlShowInvoice.Visible = False
        pnlSearchTop.Visible = True
    End Sub

    ''' <summary>
    ''' Item bound event which provides datasource for child repeater
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub rptWOID_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptWOID.ItemDataBound
        'Getting WOID for each SI
        Dim rptWOID As Repeater = DirectCast(e.Item.FindControl("rptWOID"), Repeater)
        rptWOID.DataSource = (DirectCast(e.Item.DataItem, DataRow)).GetChildRows("LinksByHeader")
        rptWOID.DataBind()
    End Sub

    ''' <summary>
    ''' Handles submit click on confirmation screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        lblPostIvoiceMsg.Text = ""
        'Check if invoice date is entered
        If txtInvoiceDate.Text = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please enter date for Invoice."
            Return
        Else
            divValidationMain.Visible = False
            lblMsg.Text = ""
        End If
        lblMsg.Text = ""
        divValidationMain.Visible = False

        Dim Mode As String
        Mode = ""
        If (chkIndividualInvoice.Checked = True) Then
            Mode = "Individual"
        ElseIf (chkGroupInvoiceByPO.Checked = True And chkIndividualInvoice.Checked = False) Then
            Mode = "PONumber"
        Else
            Mode = ""
        End If

        Dim ds As New DataSet
        'Call sp to generate Sales invoice
        If Not IsNothing(ViewState("SelectedWOID")) Then
            ds = ws.WSFinance.MSAccounts_GenerateSalesInvoice(Session("BizDivId"), ViewState("SelectedWOID"), ViewState("CompanyId"), txtInvoiceDate.Text, ViewState("ListingFee"), Session("UserId"), Mode)
        Else
            divValidationMain.Visible = True
            lblMsg.Text = "No Work Orders Selected."
            Return
        End If

        'For Sending Emails to Company admin and finance associated with the billing location
        If ds.Tables.Count > 0 Then
            Dim tblcount As Integer
            'TODO for NP :)
            tblcount = ds.Tables.Count
            'This is done because the succes table is coming at last

            If chkbxemailnotification.Checked <> False Then
                'Each table is having data for individual Sales invoice
                'Depending upon no of SI generated, the loop will run for each SI
                For i As Integer = 0 To (ds.Tables.Count - 3)
                    If ds.Tables(i).Rows.Count > 0 Then
                        Dim dvEmail As New DataView
                        dvEmail = ds.Tables(i).DefaultView
                        'Call send email function
                        Emails.SendMailSIGeneration(dvEmail, ApplicationSettings.OWUKBizDivId)
                    End If
                Next i
            ElseIf ds.Tables(tblcount - 2).Rows(0).Item("Success") = 0 Then
                divValidationMain.Visible = True
                lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
            ElseIf ds.Tables(tblcount - 2).Rows(0).Item("Success") = -10 Then
                divValidationMain.Visible = True
                lblMsg.Text = ResourceMessageText.GetString("RecordDuplicate")
            End If

            If ds.Tables(tblcount - 2).Rows(0).Item("Success") = 1 Then
                divSubmitCancel.Visible = False
                divPostInvoiceGen.Visible = True

                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "openpopup", _
                '           "Sys.Application.add_load(function() {OpenNewWindowInvoice('" & ds.Tables(2).Rows(0)("PrintAdviceNo") & "','" & ApplicationSettings.OWUKBizDivId & "','" & ViewState("CompanyId") & "');});", True)

                'lnkPrintInvoice.HRef = "~/InvoicesPrintForm.aspx?BizId=" & ApplicationSettings.OWUKBizDivId & "&CompanyId=" & ViewState("CompanyId") & "&InvoiceNo=" & ds.Tables(2).Rows(0).Item("PrintAdviceNo")
                'Poonam modified on 29-03-2017 Task - OA-430 : OA - Print button throw issue when Sales Invoice has �0 Wholsale Price
                lnkPrintInvoice.HRef = "~/ViewSalesInvoice.aspx?BizDivId=" & ApplicationSettings.OWUKBizDivId & "&CompanyId=" & ViewState("CompanyId") & "&InvoiceNo=" & ds.Tables(2).Rows(0).Item("SalesInvoiceNo")
                'Add value to clipboard
                AddTextToClipboard(ds.Tables(2).Rows(0).Item("MailAdviceNumber").ToString)
            End If
        End If
        ''Show Grid View and Search contact again and hide condirmation screen
        'pnlShowClosedWorkOrders.Visible = True
        'pnlShowInvoice.Visible = False
        'pnlSearchTop.Visible = True
        'gvWorkOrders.DataBind()
    End Sub
    Private Sub AddTextToClipboard(ByVal Value As String)
        If (lblPostIvoiceMsg.Text = "") Then
            lblPostIvoiceMsg.Text = "Sales Invoice Number: " & Value
        Else
            lblPostIvoiceMsg.Text = lblPostIvoiceMsg.Text.ToString & "<BR><BR>Sales Invoice Number: " & Value
        End If
    End Sub
    Private Sub lnkBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBack.ServerClick
        divPostInvoiceGen.Visible = False
        pnlShowClosedWorkOrders.Visible = True
        pnlShowInvoice.Visible = False
        divSubmitCancel.Visible = True
        pnlSearchTop.Visible = True
        gvWorkOrders.DataBind()
    End Sub

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
    Private Sub gvWorkOrders_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvWorkOrders.RowCommand
        If (e.CommandName = "EditPONumber") Then
            txtPONumber.Text = ""
            Dim arrList As ArrayList = New ArrayList
            Dim str As String = e.CommandArgument.ToString
            arrList.AddRange(str.Split("#"))
            txtPONumber.Text = arrList(1)
            Dim WOID As String
            WOID = arrList(0)
            ViewState.Add("SelectedWOID", WOID)
            ViewState.Add("Mode", "")
            mdlPONumber.Show()

        End If
    End Sub
    Public Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click
        Dim ds As DataSet = ws.WSWorkOrder.UpdatePONumber(ViewState("SelectedWOID"), txtPONumber.Text.Trim, Session("UserId"), "", ViewState("Mode"))
        PopulateGrid()
    End Sub
End Class
