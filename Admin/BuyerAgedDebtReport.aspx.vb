
Partial Public Class BuyerAgedDebtReport
    Inherits System.Web.UI.Page
    Protected WithEvents UCMenuMainAdmin1 As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents UCSearchContact1 As UCSearchContact
    'Protected WithEvents UCDateRange1 As UCDateRange
    Protected WithEvents lnkBackToListing As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents tdBackToListing As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lnkExportReportExcel As LinkButton
    Protected WithEvents lnkExportReportPdf As LinkButton



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                UCSearchContact1.BizDivID = ApplicationSettings.OWDEBizDivId
                ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
            Case ApplicationSettings.CountryUK
                UCSearchContact1.BizDivID = ApplicationSettings.OWUKBizDivId
                ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
        End Select

        UCSearchContact1.Filter = "SalesUnPaid"
        'If UCSearchContact1.ddlContact.SelectedIndex > 0 Or txtCompanyNo.Text.Trim <> "" Then
        '    ReportViewerOW.Visible = True
        'Else
        '    ReportViewerOW.Visible = False
        'End If

    End Sub

    Private Sub lnkBtnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnGenerateReport.Click
        generateReport()
    End Sub

    Private Sub lnkExportReportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportReportExcel.Click

        Dim warnings As Microsoft.Reporting.WebForms.Warning()
        Dim streamids As String()
        Dim mimeType As String
        Dim encoding As String
        Dim fileType As String = "EXCEL"
        Dim extension As String = "xls"
        Dim fileName As String = "OWReport"
        Dim languageCode As String = ""
        Dim objParameter(3) As Microsoft.Reporting.WebForms.ReportParameter
        Dim StrCompanyID As String = ""

        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/BuyerAgedDebtReport"
        objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)


        If txtCompanyNo.Text.Trim <> "" Then
            objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyId", txtCompanyNo.Text.Trim)
            StrCompanyID = txtCompanyNo.Text.Trim
        Else
            objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyId", UCSearchContact1.ddlContact.SelectedValue.ToString.Trim)
            StrCompanyID = UCSearchContact1.ddlContact.SelectedValue.ToString.Trim
        End If

        objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "False")

        'Language parameters
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                languageCode = "de-DE"
            Case ApplicationSettings.CountryUK
                languageCode = "en-GB"
        End Select
        objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)

        ReportViewerOW.ServerReport.SetParameters(objParameter)

        Dim bytes As Byte() = ReportViewerOW.ServerReport.Render(fileType, Nothing, mimeType, encoding, extension, streamids, warnings)
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=BuyerAgedDebtReport." + extension)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub

    Private Sub lnkExportReportPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportReportPdf.Click

        Dim warnings As Microsoft.Reporting.WebForms.Warning()
        Dim streamids As String()
        Dim mimeType As String
        Dim encoding As String
        Dim fileType As String = "PDF"
        Dim extension As String = "pdf"
        Dim fileName As String = "OWReport"
        Dim languageCode As String = ""

        Dim objParameter(3) As Microsoft.Reporting.WebForms.ReportParameter
        Dim StrCompanyID As String = ""


        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/BuyerAgedDebtReport"
        objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)

        If txtCompanyNo.Text.Trim <> "" Then
            objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyId", txtCompanyNo.Text.Trim)
            StrCompanyID = txtCompanyNo.Text.Trim
        Else
            objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyId", UCSearchContact1.ddlContact.SelectedValue.ToString.Trim)
            StrCompanyID = UCSearchContact1.ddlContact.SelectedValue.ToString.Trim
        End If
        objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "False")

        'Language parameters
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                languageCode = "de-DE"
            Case ApplicationSettings.CountryUK
                languageCode = "en-GB"
        End Select
        objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)
        ReportViewerOW.ServerReport.SetParameters(objParameter)

        Dim bytes As Byte() = ReportViewerOW.ServerReport.Render(fileType, Nothing, mimeType, encoding, extension, streamids, warnings)
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=BuyerAgedDebtReport" + Date.Now + "." + extension)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub

    Private Sub generateReport()
        Dim languageCode As String = ""

        If UCSearchContact1.ddlContact.SelectedIndex > 0 Or txtCompanyNo.Text.Trim <> "" Then
            'ErrLabel.Text = ""
            'pnlExportReport.Visible = True
            ReportViewerOW.ShowParameterPrompts = False
            ReportViewerOW.ShowToolBar = True
            Dim reportSErverPath As Uri = New Uri(ApplicationSettings.ReportServerPath)
            ReportViewerOW.ServerReport.ReportServerUrl = reportSErverPath
            ReportViewerOW.ServerReport.ReportServerCredentials = New CustomReportCredentials.CustomReportCredentials(ApplicationSettings.ReportServerUserName, ApplicationSettings.ReportServerPassword, ApplicationSettings.ReportServerDomain)
            Dim objParameter(3) As Microsoft.Reporting.WebForms.ReportParameter
            Dim StrCompanyID As String = ""


            ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/BuyerAgedDebtReport"

            objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)

            If txtCompanyNo.Text.Trim <> "" Then
                objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyId", txtCompanyNo.Text.Trim)
                StrCompanyID = txtCompanyNo.Text.Trim
            Else
                objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("CompanyId", UCSearchContact1.ddlContact.SelectedValue.ToString.Trim)
                StrCompanyID = UCSearchContact1.ddlContact.SelectedValue.ToString.Trim
            End If

            objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "False")

            'Language parameters
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryDE
                    languageCode = "de-DE"
                Case ApplicationSettings.CountryUK
                    languageCode = "en-GB"
            End Select
            objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)
            ReportViewerOW.ServerReport.SetParameters(objParameter)
            ReportViewerOW.ServerReport.Refresh()

        Else
            ErrLabel.Visible = True
            ErrLabel.Text = "Please select a Company."
        End If
    End Sub

End Class