
Partial Public Class CreatePartPayment
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected WithEvents UCSearchContact1 As UCSearchContact

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            txtPayRecDate.Text = Strings.FormatDateTime(Date.Today, DateFormat.ShortDate)
            pnlMain.Visible = True
            pnlMsg.Visible = False
            Security.SecurePage(Page)
            UCSearchContact1.BizDivID = Session("BizDivId")
            UCSearchContact1.ClassId = 0
            UCSearchContact1.Filter = ""
            UCSearchContact1.populateContact()
        End If

    End Sub
    Public Sub CustomCompanyValidator_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomCompanyValidator.ServerValidate
        'Check if user has selected a contact from the dropdown 
        If UCSearchContact1.ddlContact.SelectedValue <> "" Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Page.Validate()
        If Page.IsValid Then
            'Dim Valid As Boolean =ValidateAmtTextBoxForNonNum()
            'If Valid = True Then
            Dim a As Object = UCSearchContact1.ddlContact.SelectedItem.Text
            Dim CompanyID As Integer = UCSearchContact1.ddlContact.SelectedValue
            Dim DateCreated As String = txtPayRecDate.Text
            ws.WSFinance.MS_CreatePartPayment(Session("BizDivId"), CompanyID, CompanyID, txtPerticulars.Text, CDec(txtAmount.Text), txtComments.Text, Session("CompanyId"), Session("UserId"), DateCreated, Session("UserId"))
            pnlMain.Visible = False
            pnlMsg.Visible = True
            'End If
        Else
            'lblError.Text = ""
            divValidationMain.Visible = True
        End If
    End Sub
    Public Function ValidateAmtTextBoxForNonNum() As Boolean
        If Not IsNumeric(txtAmount.Text) And txtAmount.Text <> "" Then
            divValidationMain.Visible = True
            lblError.Text = "The part payment you have entered is not a valid amount.  Please enter a correct value."
            Return False
            'ElseIf CType(txtAmount.Text, Integer) <= 0 Then
            '    divValidationMain.Visible = True
            '    lblError.Text = "The part payment you have entered is not a valid amount.  Please enter a correct value."
            '    Return False
        Else
            Return True
        End If
    End Function

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        txtPayRecDate.Text = Strings.FormatDateTime(Date.Today, DateFormat.ShortDate)
        UCSearchContact1.BizDivID = Session("BizDivId")
        UCSearchContact1.ClassId = 0
        UCSearchContact1.Filter = ""
        UCSearchContact1.populateContact()
        txtAmount.Text = ""
        txtComments.Text = ""
        txtPerticulars.Text = "Payment received with thanks"
        divValidationMain.Visible = False
        pnlMain.Visible = True
        pnlMsg.Visible = False

    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("Receivables.aspx")
    End Sub
End Class