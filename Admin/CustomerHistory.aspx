﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerHistory.aspx.vb" Inherits="Admin.CustomerHistory" MasterPageFile="~/MasterPage/OWAdmin.Master" title="Customer History"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
	<script type="text/javascript">
	    function ShowPopup(Flag, LinkId, WorkOrderId) {
	        var strFlag = Flag;
	        var DivId = "PopupDiv" + WorkOrderId;
	        if (Flag == "True") {

	            var div1Pos = $("#" + LinkId).offset();
	            var div1X = div1Pos.left + 80;
	            var div1Y = div1Pos.top;
	            $('#' + DivId).css({ left: div1X, top: div1Y });
	            $("#" + DivId).show();
	        }
	        else {
	            $("#" + DivId).hide();
	        }
	    }         
</script>        
			<asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server">
			<ContentTemplate>
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
       
       <p class="paddingB4 HeadingRed" style="margin-left:15px;"><strong>Customer History</strong> </p>

        <table class="gridBorderB" width="100%"  border="0" cellpadding="10" cellspacing="0">
                      <tr>
					     <td id="tdBackToListing" runat=server>
							<div id="divButton" class="divButton" style="width: 115px;">
								<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
									<a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" alt="Back to Listing" width="4" height="8" hspace="0" vspace="0" border="0" class="marginR8"><strong>Back to Listing</strong></a>
								<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
							</div>
						</td>                          
					 </tr>
         </table>
         <table class="gridBorderB" width="100%"  border="0" cellpadding="10" cellspacing="0">
                    <tr>
					     <td>
                             <div class="formTxt" style="width:100px;float:left;">&nbsp;</div>       
							 <div class="formTxt" style="width:300px;font-weight:bold;font-size:14px;float:left;">Location : <asp:Label ID="lblLocation" runat="server"></asp:Label></div>         
                             <div class="formTxt" style="width:100px;float:left;">&nbsp;</div>         
                             <div class="formTxt" style="width:300px;font-weight:bold;font-size:14px;float:left;">Full Postcode : <asp:Label ID="lblPostcode" runat="server"></asp:Label></div>         
                             <div class="formTxt" style="width:100px;float:left;">&nbsp;</div>         
                             <div class="formTxt" style="font-weight:bold;font-size:14px;float:left;">Customer : <asp:Label ID="lblCustomer" runat="server"></asp:Label></div>         
						</td>                          
					 </tr>
         </table>
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<input id="hdnPageSize" type="hidden" name="hdnPageSize" runat="server"> 
			<input id="hdnPageNo" type="hidden" name="hdnPageNo" runat="server">
			<input id="sortExpr" type="hidden" name="sortExpr" runat="server"> 
			<input id="sortOrder" type="hidden" name="sortOrder" runat="server">
			<input id="totalRecs" type="hidden" name="totalRecs" runat="server"> 
			<input id="pageRecs" type="hidden" name="pageRecs" runat="server">
			<input id="defaultSortImgId" type="hidden" name="defaultSortImgId" runat="server"> 			
			<input type="hidden" id="hdnDateID" name="hdnDateID" runat="server">
			<input type="hidden" id="hdnDate" name="hdnDate" runat="server">
			<input type="hidden" id="hdntxt" name="hdntxt" runat="server">
			<input type="hidden" id="hdnIsDeleted" name="hdnIsDeleted" runat="server">			
			           
             <table width="100%"><tr><td width="10">&nbsp;</td><td>
             <asp:Label ID="lblErr" CssClass="bodytxtValidationMsg" runat="server" Text=""></asp:Label>
             <asp:Panel ID="pnlListing" runat="server" style="display:block;">             
          		
                 <asp:GridView ID="gvCustomerHistory" runat="server" AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
               PagerSettings-Mode="NextPreviousFirstLast" CssClass="gridrow"  PagerSettings-Position="TopAndBottom" Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               <PagerTemplate>                  
                  
                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>							
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
                 </PagerTemplate>
                  <Columns>   
                    <asp:TemplateField HeaderText="WO NO" HeaderStyle-CssClass="gridHdr gridText"
                    SortExpression="RefWOID">
                    <ItemStyle CssClass="gridRowText" Width="60px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <asp:Label ID="lblWOID" runat="server" Text='<%#Container.DataItem("RefWOID")%>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>                                    
                 <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="60px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="DateAccepted" SortExpression="DateAccepted" HeaderText="Accepted" DataFormatString="{0:dd/MM/yy}"
                    HtmlEncode="false" />
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="58px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="DateClosed" SortExpression="DateClosed" HeaderText="Closed"
                    DataFormatString="{0:dd/MM/yy}" HtmlEncode="false" />
               <%-- <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="70px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="Location" SortExpression="Location" HeaderText="Location" />
                <asp:TemplateField HeaderText="Customer" HeaderStyle-CssClass="gridHdr gridText" SortExpression="CustomerName">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="40px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <%#IIf(Container.DataItem("AddressType") = "Temporary", Container.DataItem("CustomerName"), " ")%>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="70px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="BuyerCompany" SortExpression="BuyerCompany" HeaderText="Client" /> 
                <asp:TemplateField SortExpression="SupplierCompany" HeaderText="Supplier" HeaderStyle-CssClass="gridHdr gridText"> 
					 <ItemStyle CssClass="gridRowText" Wrap="true"  Width="80px" />
					 <ItemTemplate> 
                        
                        
						<table width="100%" cellspacing="0" cellpadding="0" >
							<tr>
                                <td width="260">
									<span style='cursor:hand;' onMouseOut=javascript:ShowPopup('False','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>); onMouseOver=javascript:ShowPopup('True','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);>
										<%#GetLinks(1, IIf(IsDBNull(Container.DataItem("SupCompID")), "", Container.DataItem("SupCompID")), IIf(IsDBNull(Container.DataItem("SupplierContactID")), "", Container.DataItem("SupplierContactID")), IIf(IsDBNull(Container.DataItem("SupplierCompany")), "", Container.DataItem("SupplierCompany")), Container.DataItem("Status"), Container.DataItem("WorkOrderId"))%>
									</span> 
								</td>
                                <td id="tdRating" visible="true" valign="top" runat="server" >
									<div style="text-align:right;" id="PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>">
                                        <a style="text-align:right;" id="ShowPopupAnchor" onclick="javascript:ShowPopup('True','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);"><img id="imgTestQuickNote" src="Images/Icons/FindSuppliersforWorkOrder.gif" title="View Supplier Details" alt="View Supplier Details"  width="16" height="15" hspace="2" vspace="0" border="0" /></a></div>
                                        <div id="PopupDiv<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>" style="display: none;position:absolute; margin-left:-50px;">
                                                <asp:Panel runat="server" ID="pnlServicePartner" Width="200px" style="background-color:#FFEBDE; font-family: Tahoma, Arial, Verdana; font-size: 11px; color: #000000; text-decoration: none; line-height: 18px;	border: 1px solid #CECBCE;" >
                                                        <div style="text-align:right;"><a style="color:Black;text-align:right;vertical-align:top;cursor:pointer;" onclick="javascript:ShowPopup('False','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);"><b>Close [X]</b></a></div>
                                                        <asp:Label id="lblContactName" runat="server" Text='<%# Cstr("<b>Contact Name: </b>" + iif(ISDBNULL(Container.DataItem("SupplierContact")),"--",Container.DataItem("SupplierContact"))) %>' ></asp:Label><br />
                                                        <asp:Label id="lblPhoneNo" runat="server" Text='<%#Cstr("<b>Phone No: </b>" + iif(ISDBNULL(Container.DataItem("SupplierPhone")),"--",Container.DataItem("SupplierPhone"))) %>' ></asp:Label><br />
                                                        <asp:Label id="lblMobileNo" runat="server" Text='<%#Cstr("<b>Mobile No: </b>" + iif(ISDBNULL(Container.DataItem("SupplierMobile")),"--",Container.DataItem("SupplierMobile"))) %>' ></asp:Label><br />
                                                        <asp:Label id="lblEmail" runat="server" Text='<%#Cstr("<b>Email Id: </b>" + iif(ISDBNULL(Container.DataItem("SupplierEmail")),"--",Container.DataItem("SupplierEmail"))) %>' ></asp:Label>
                                                </asp:Panel>
                                        </div>	
                                    </div>
                                </td>
							</tr>
						</table>
					 </ItemTemplate>
               </asp:TemplateField>
                    <asp:BoundField ItemStyle-CssClass="gridRowText" HeaderStyle-CssClass="gridHdr gridText"  DataField="WOTitle" SortExpression="WOTitle" HeaderText="WOTitle" />        
                    <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="70px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="DateStart" SortExpression="DateStart" HeaderText="Start Date" DataFormatString="{0:dd/MM/yy}"
                    HtmlEncode="false" />
                <asp:TemplateField SortExpression="WholesalePrice" HeaderText="WP" HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="40px" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#Container.DataItem("WholesalePrice")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="PlatformPrice" HeaderText="PP" HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="40px" HorizontalAlign="Right" />
                    <ItemTemplate>
                         <span id="SpanPP" runat="server"><%#Container.DataItem("PlatformPrice")%></span> 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="UpSellPrice" HeaderText="UP" HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="40px" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#Container.DataItem("UpSellPrice")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="BillingLocation" HeaderText="Billing Location"
                    HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="80px" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#Container.DataItem("BillingLocation")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="Status" HeaderText="Status" HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="80px" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#Container.DataItem("Status")%>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="50"  HeaderStyle-CssClass="gridHdr"  DataField="SalesInvoice" SortExpression="SalesInvoice" HeaderText="Sales Invoice Number" />
               
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="50"  HeaderStyle-CssClass="gridHdr"  DataField="PurchaseInvoice" SortExpression="PurchaseInvoice" HeaderText="Purchase Invoice Number" />
                 <asp:TemplateField >               
                <HeaderStyle  CssClass="gridHdr" />
                <ItemStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <%# getHistoryLinks(Container.DataItem("WOID"), Container.DataItem("StagedWO"), Container.DataItem("WorkOrderID"), Container.DataItem("BuyerCompID"), Container.DataItem("BuyerContactID"), Container.DataItem("SupCompID"), Container.DataItem("Status"), Container.DataItem("BuyerAccept"), Container.DataItem("WholesalePrice"), "PS=" & gvCustomerHistory.PageSize & "&sender=CustomerHistory" & "&" & "PN=" & gvCustomerHistory.PageIndex & "&" & "SC=" & gvCustomerHistory.SortExpression & "&" & "SO=" & gvCustomerHistory.SortDirection )%>
                    </ItemTemplate>
                </asp:TemplateField> 
               <asp:TemplateField ItemStyle-Width="15">               
                <HeaderStyle  CssClass="gridHdr" />
                <ItemStyle Wrap="true" HorizontalAlign="Left" CssClass="gridRowText"/>
                    <ItemTemplate>
                        <div style="float:left;"><a runat="server" id="lnkDetail" 
              href=<%#"~/AdminWODetails.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("WorkOrderID") & "&" & "txtWorkorderID=" & Container.DataItem("WorkOrderID") & "&BizDivID=1&" & "Group=" & iif(Container.DataItem("Status")="Conditional Accept","CA" ,iif(Container.DataItem("Status")="Active","Accepted",Container.DataItem("Status"))) & "&" & "CompanyID=" & Container.DataItem("BuyerCompID") & "&ContactId=" & Container.DataItem("BuyerContactID") & "&" & "SupCompID=" & Container.DataItem("SupCompID") & "&" & "PS=" & gvCustomerHistory.PageSize & "&" & "PN=" & gvCustomerHistory.PageIndex & "&" & "SC=" & gvCustomerHistory.SortExpression & "&" & "SO=" & gvCustomerHistory.SortDirection & "&" & "sender=CustomerHistory"  %> ><img src="Images/Icons/ViewDetails.gif" width="16" height="16" hspace="2" vspace="0" border="0" title="View Details"></a></div>
                    </ItemTemplate>
                </asp:TemplateField>                 
                 <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">                
                <ItemStyle CssClass="gridRowIconsText" Wrap=true  Width="14px"/>
                <ItemTemplate> 
                <span style='cursor:hand;' onMouseOut=javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','visible')>
                    <asp:LinkButton ID="lnkbtnAction" CommandName="Action" CausesValidation="false" CommandArgument='<%#Container.dataitem("WOID") & "," & Container.DataItem("Status") %>' runat="server">
                        <img id="imgAction" runat="server" src=<%#GetActionIcon(Container.DataItem("NtCnt"),Container.DataItem("Rating"))%>  width="24" height="30" hspace="2" vspace="0" border="0" class="cursorHand" />
                    </asp:LinkButton>
                    </span>
                    <div class="divUserNotesDetail" id=divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%> onMouseOut=javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','visible')>
                         <asp:panel runat="server" style="color:black;width: 230px;max-height:320px;overflow:scroll;" id="pnlUserNotesDetail" Visible='<%# IIF(Container.DataItem("NtCnt") = 0,IIF(Container.DataItem("RatingComment") = "",False,True) ,True) %>'>
                         <div><b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b></div>
                         <div class="clsRatingInner">
                         <div id="divRatingComment" runat="server" visible='<%# IIF(Container.DataItem("RatingComment") = "",False,True) %>'>
                            <b style="color:#0C96CF; font-size:12px;">Rating Comment:</b>
                            <div style="margin-top: 0px; margin-bottom: 10px;">
                                <%# Container.DataItem("RatingComment")%>
                            </div>
                         </div>
                         <div id="divNotes" runat="server" visible='<%# IIF(Container.DataItem("NtCnt") = 0,False,True) %>'>
                          <b style="color:#0C96CF; font-size:12px;">Notes:</b>
			                <asp:Repeater ID="DLUserNotesDetail" runat="server" DataSource='<%#GetUserNotesDetail(Container.DataItem("WOID"))%>'>
                             <ItemTemplate>
                                <div style='visible: <%# iif( Trim(Container.DataItem("Comments")) = "" , "hidden", "visible") %>'>
                                <div>
                               <%--poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2 --%>
                                   <%# "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - <b>" & Container.DataItem("OWRepFName") & " " & Container.DataItem("OWRepLName") & "</b><br/><b>" & Container.DataItem("NoteCategory") & "</b></span>"%>
                                </div>
                                <div style="margin-top: 0px; margin-bottom: 10px;">
                                <%# Container.DataItem("Comments") %>
                                </div>
                                    </div>
                             </ItemTemplate>
                            </asp:Repeater>
                           </div>
			             </div>   
			             <div><b class="b4"></b><b class="b3"></b><b class="b2"></b><b class="b1"></b></div>
			             </asp:panel>
		             </div>
				</ItemTemplate>  
               </asp:TemplateField>
                </Columns>
                  <AlternatingRowStyle  CssClass="gridRow" />
                 
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
                 </asp:GridView>
             <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.CustomerHistory" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
            </asp:Panel>
            				      
                      
            
			
          				</td><td width="10">&nbsp;</td></tr></table>			 
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
         </ContentTemplate>
			</asp:UpdatePanel>
			
			 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelCreateWorkOrder" runat="server">
					<ProgressTemplate>
						<div>
							<img  align=middle src="Images/indicator.gif" />
							Please Wait...
						</div>      
					</ProgressTemplate>
			</asp:UpdateProgress>
			<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePanelCreateWorkOrder" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
      
</asp:Content>
