<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="AdminWOListing"  CodeBehind="AdminWOIssueListing.aspx.vb" Inherits="Admin.AdminWOIssueListing" ValidateRequest="false" %>

<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <%@ Register Assembly="WebLibrary" Namespace="WebLibrary" TagPrefix="lib" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
	<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
	
	<%@ Import Namespace="System.Web.Script.Services" %>
    <%@ Import Namespace="System.Web.Services" %>

   <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
			
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>

			    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
            <ContentTemplate>
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
             <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10px"></td>
			<td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Issue" runat="server"></asp:Label></td>
			<td bgcolor="#F4F5F0" ><asp:Label runat="server" ID="lblMultipleWOsMsg" Visible="false" CssClass="formTxtRed" ></asp:Label></td>
			</tr>
			<tr height="10px"><td colspan="3">&nbsp;</td></tr>
			
			<tr>
			<td width="10px"></td>
			<td colspan="2">
			<div id="divButton" style="width: 115px; height:26px;" runat="server" >                      
                      <a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" title="Back to Listing" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8"><strong>Back to Listing</strong></a>
            </div>
			</td>
			</tr>		
		
			</table>
		<table width="100%" cellpadding="0" cellspacing="0" border="0">		
		<tr>
		     <td width="10px" align="left"></td>		        
		      <td align="left" width="80px;" style="padding-right:10px;">
		        <%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
						<uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact>
			  </td>
			  <td align="left" width="80px;">
			   <asp:DropDownList id="ddlBillLoc" Visible="true" runat="server" CssClass="formFieldGrey150"  DataTextField = "BillingLocation" DataValueField = "BillingLocID"  EnableViewState="True" ></asp:DropDownList>
			  </td>
               <td align="left" width="80px;" style="padding-right:10px;">
		       <lib:Input ID="txtWorkorderID" runat="server" DataType="List" Method="GetWorkOrderID" CssClass="formField150"/>
               <cc2:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtWorkorderID" WatermarkText="Work Order ID" WatermarkCssClass="formField150"></cc2:TextBoxWatermarkExtender>        
			  </td>	
			   <td align="left" width="80px;" style="padding-right:10px;vertical-align:top;height:56px;">
			   <div style="font-family:Arial;font-size:11px;">Issues By</div>
			   <asp:DropDownList ID="ddlIssueBy" runat="server" CssClass="formFieldGrey215" Width="80">
				<asp:ListItem>All</asp:ListItem>
				<asp:ListItem>OW Issues</asp:ListItem>
				<asp:ListItem>Supplier Issues</asp:ListItem>
				</asp:DropDownList>
			   </td>	
			    <td id="tdViewBtn" runat="server" align="left" width="60px" style="padding-right:10px;">
                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                    <asp:LinkButton ID="lnkView" causesValidation="true" OnClick="lnkView_Click" runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton></td>
                </div>
		        </td>
			   	<td align="left" id="tdMultipleRight" width="90px"   runat="server" >
                    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                        <asp:LinkButton runat="server" ID="lnkMultipleWOs" OnClick="lnkMultipleWOs_Click" CssClass="txtButtonRed" Text="Accept Issue"></asp:LinkButton>
                    </div>
		        </td>		
                <td>&nbsp;</td>
		      <td>
              <div style="cursor: pointer; -moz-border-radius: 5px; border-radius: 5px; background-color: #993366 !important;
                                            color: #FFFFFF; text-align: center; width: 100px;">
                                            <a runat="server" target="_blank" id="btnExport" class="txtButtonRed">&nbsp;Export to
                                                Excel&nbsp;</a>
                                        </div>
              </td>
		</tr>
		
		<tr>
		 <td colspan="8" style="padding-right:20px;" align="right" >		     
		    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                <a runat="server" id="btnWatch" Class="txtButtonRed">Watch</a>
            </div>
        </td>		
		     <td  style="padding-right:20px;" >
		     <asp:CheckBox runat="server" ID="chkHideTestAcc" Checked="true" Text="Hide Test Accounts" CssClass="formTxt" AutoPostBack="true" />
		     <asp:CheckBox runat="server" ID="chkMainCat" Checked="true" Text="Show Default View" CssClass="formTxt" AutoPostBack="true" />
		     </td>
		</tr>     
	   </table>
			     
				 
			<hr style="background-color:#993366;height:5px;margin:0px;" />
			<%@ Register TagPrefix="uc1" TagName="UCAdminWOsIssueListing" Src="~/UserControls/Admin/UCAdminWOsIssueListing.ascx" %>
                 <uc1:UCAdminWOsIssueListing id="UCAdminWOsIssueListing1" runat="server"></uc1:UCAdminWOsIssueListing> 
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
       </ContentTemplate>
            <%--Poonam modified on 2/8/2016 - Task -OA-311 : EA - Multiple search company in listing does not work in Chrome--%>
            <Triggers>
                <asp:AsyncPostBackTrigger EventName="Click" ControlID="lnkView" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdateProgress AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                <div class="gridText">
                    <img align="middle" src="Images/indicator.gif" />
                    <b>Fetching Data... Please Wait</b>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1"
            CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
      
      </asp:Content>