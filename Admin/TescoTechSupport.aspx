<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TescoTechSupport.aspx.vb" Inherits="Admin.TescoTechSupport" MasterPageFile="~/MasterPage/OWAdmin.Master" title="Tesco Tech Support"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<style type="text/css">

#divMenu {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	line-height: 12px;
	font-weight: normal;
	text-decoration: none;
	border-top-width: 1px;
	border-top-style: solid; 
	border-top-color: #CECECE;
}

.divContentTopRed{
	width: 100%;
	background-color: #993366;
	background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
	height: 5px;
	
}
.dlIssue{
width:100%;
}
.gridRow{
padding-right:0px;
}
.clsBorderTbl{
 border-left-width:0px;
 border-right: solid 1px #ffffff;
 padding:4px 0px 0px 6px;
 margin:0px;
 font-family:Arial;font-size:11px;
 }
 .clsBorderTblDetail{
 border-left-width:0px;
 border-right: solid 1px #ffffff;
 padding:2px 0px 0px 2px;
 margin:0px;
 
 }
 .bgInner
 {
    background-color:#e9ebe8;
 }
 .gridRow{
 padding:0px;
 }
</style>
<script>
function callExport(objLink)
{
    
    var url = "ExportToExcel.aspx?page=TescoTechSupport&Mode=Export&Month=" + document.getElementById("ctl00_ContentPlaceHolder1_ddMonth").value + "&Year=" +  document.getElementById("ctl00_ContentPlaceHolder1_ddYear").value;
    document.getElementById(objLink).href = url

}
</script>




	
	        
			<asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server">
			<ContentTemplate>
	        <div id="divContent" onLoad=";">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
        <table width="100%" cellpadding="0" cellspacing="0">
						<tr>
						<td>&nbsp;</td>
							<td class="HeadingRed" ><strong>Tesco Tech Support Monthly Statistics</strong></td>
						</tr>
					  </table>

					  <table width="800px" cellpadding="0" cellspacing="0" border="0">
					  	<tr >
					  	    <td width="15">&nbsp;</td>
							<td height="24" align="left">&nbsp;</td>
											  <td align="left"><asp:DropDownList CssClass="formFieldGrey121" ID="ddMonth" runat="server">
										<asp:ListItem Text="January" value="1"></asp:ListItem>
												  <asp:ListItem Text="February" value="2"></asp:ListItem>
												  <asp:ListItem Text= "March" value="3"></asp:ListItem>
												  <asp:ListItem Text= "April" value="4"></asp:ListItem>
												  <asp:ListItem Text= "May" value="5"></asp:ListItem>
												  <asp:ListItem Text= "June" value="6"></asp:ListItem>
												  <asp:ListItem Text= "July" value="7"></asp:ListItem>
												  <asp:ListItem Text= "August" value="8"></asp:ListItem>
												  <asp:ListItem Text= "September" value="9"></asp:ListItem>
												  <asp:ListItem Text= "October" value="10"></asp:ListItem>
												  <asp:ListItem Text= "November" value="11"></asp:ListItem>
												  <asp:ListItem Text= "December" value="12"></asp:ListItem>
										</asp:DropDownList>
											  </td>
											  <td height="24" align="left" width="10">&nbsp;</td>
											  <td align="left"><asp:DropDownList CssClass="formFieldGrey121" ID="ddYear" runat="server">
											  <asp:ListItem Text="2006" value="2006"></asp:ListItem>
											  <asp:ListItem Text="2007" value="2007"></asp:ListItem>
											  <asp:ListItem Text="2008" value="2008"></asp:ListItem>
											  <asp:ListItem Text="2009" value="2009"></asp:ListItem>
											  <asp:ListItem Text="2010" value="2010"></asp:ListItem>
											  <asp:ListItem Text="2011" value="2011"></asp:ListItem>
                                              <asp:ListItem Text="2012" value="2012"></asp:ListItem>
                                              <asp:ListItem Text="2013" value="2013"></asp:ListItem>
                                              <asp:ListItem Text="2014" value="2014"></asp:ListItem>
                                              <asp:ListItem Text="2015" value="2015"></asp:ListItem>
											  </asp:DropDownList>
											  </td>
							<td width="20"></td>
							<td align="left">
								<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                    <asp:LinkButton ID="lnkBtnGenerateReport" causesValidation=True runat="server" CssClass="txtButtonRed" Text="Generate Report"></asp:LinkButton>
                                </div>
							 </td>		 
                          <td align="left">
                            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;" id="tblExport" runat="server">
                                <a runat="server" target="_blank" id="btnExport" class="txtButtonRed" onclick='javascript:callExport(this.id)' style="cursor:pointer;" > &nbsp;Export to Excel&nbsp;</a>
                            </div>
		                </td>  
		                <td align="left" >
                            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                <asp:linkButton ID="lnkPrint" Visible="true"  CssClass="txtButtonRed" runat="server" >Print Report</asp:linkButton>
                            </div>
		 </td>    
						</tr>
					</table>
		  <table width="500px" cellpadding="0" cellspacing="0" runat="server" id="tblReportDateTime" style="margin-left:350px; margin-top:10px;">
						<tr>
						<td>&nbsp;</td>
							<td class="HeadingRed" ><strong>Report generated on <asp:Label runat="server" ID="lblReportDateTime" CssClass="HeadingRed"></asp:Label></strong></td>
						</tr>
					  </table>
					
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
						
			           
             <table width="100%"><tr><td width="10">&nbsp;</td><td>
             <asp:Label ID="lblErr" CssClass="bodytxtValidationMsg" runat="server" Text=""></asp:Label>
             <asp:Panel ID="pnlListing" runat="server" style="display:block; background-color:ffffff;">             
             						
        <asp:datalist ID="dlTescoReport" runat="server" ForeColor="#555354"  CssClass="dlIssue" Width="950px" >
                <HeaderTemplate>
	                <table width="950px"  cellspacing="0" cellpadding="0" class="gridHdr gridText" style="color:#000; text-align:left;margin-left:0px;margin-top:0px;">
		                 <tr id="tbldlIssueWOListingHdr" align="left">  
                              <td width="140" class="clsBorderTbl">Store</td>
                              <td width="140" class="clsBorderTbl">Service</td>
                              
                              <td width="80" class="clsBorderTbl">Engineer Availability</td>
                              <td width="80" class="clsBorderTbl">Engineer "On time"</td>
                              <td width="90" class="clsBorderTbl">Average Job Time</td>
                              <td width="100" class="clsBorderTbl">No. of Suppliers used</td>
                              <td width="80" class="clsBorderTbl">Jobs received</td>
                              <td width="80" class="clsBorderTbl">Jobs Invoiced</td>
                              <td width="60" class="clsBorderTbl">Jobs in SLA</td>
                              <td width="60" class="clsBorderTbl">Jobs Active</td>
                              <td width="40" class="clsBorderTbl">Total Jobs</td>
                              
                            </tr>
	                </table>
                </HeaderTemplate>
                <ItemTemplate>
               
                    <table width="950px" cellspacing="0" cellpadding="0">
						<tr id="tblDLRow" align="left" >                 
						      <td width="130" class="clsBorderTbl" style="word-wrap:break-word ;"><b><%#Container.DataItem("StoreName")%></b></td>
						       <td width="136" class="clsBorderTbl" style="word-wrap:break-word ;"><input id="hdnGoodsLocation" type="hidden" name="hdnGoodsLocation" runat="server" value='<%#Container.DataItem("GoodsLocation")%>'/></td>
                              
                              <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;">&nbsp;</td>
                              <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;">&nbsp;</td>
                              <td width="90" class="clsBorderTbl" style="word-wrap:break-word ;">&nbsp;</td>
                              <td width="100" class="clsBorderTbl" style="word-wrap:break-word ;">&nbsp;</td>
                              <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;">&nbsp;</td>
                              <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;">&nbsp;</td>
                              <td width="60" class="clsBorderTbl" style="word-wrap:break-word ;">&nbsp;</td>
                              <td width="60" class="clsBorderTbl" style="word-wrap:break-word ;">&nbsp;</td>
						      <td width="40" class="clsBorderTbl" style="word-wrap:break-word ;"><b><%#Container.DataItem("TotalJobs")%></b></td>
						</tr>
						</table>
						<asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="dlJobsBreakUp" Runat="server" Width="950px" >
						    <ItemTemplate>
						        <table width="950px" cellspacing="0" cellpadding="0">
						            <tr id="tblDLRow" align="left">                 
						                   <td width="137" class="clsBorderTblDetail" style="background-color:#efebe7;word-wrap:break-word ;" >&nbsp;</td>
						                   <td width="130" class="clsBorderTblDetail" style="background-color:#efebe7;word-wrap:break-word ;"><%#Container.DataItem("Service")%></td>
                                          
                                          <td width="80" class="clsBorderTblDetail" style="background-color:#efebe7;word-wrap:break-word ;"><%#Container.DataItem("EngrAvail")%></td>
                                          <td width="80" class="clsBorderTblDetail" style="background-color:#efebe7;word-wrap:break-word ;"><%#Container.DataItem("EngrOnTime")%> %</td>
                                          <td width="90" class="clsBorderTblDetail" style="background-color:#efebe7;word-wrap:break-word ;"><%#Container.DataItem("AvgJobTime")%></td>
                                          <td width="100" class="clsBorderTblDetail" style="background-color:#efebe7;word-wrap:break-word ;"><%#Container.DataItem("SuppliersUsed")%></td>
                                          <td width="80" class="clsBorderTblDetail" style="background-color:#efebe7;word-wrap:break-word ;"><%#Container.DataItem("JobsReceived")%></td>
                                          <td width="80" class="clsBorderTblDetail" style="background-color:#efebe7;word-wrap:break-word ;"><%#Container.DataItem("JobsInvoiced")%></td>
                                          <td width="60" class="clsBorderTblDetail" style="background-color:#efebe7;word-wrap:break-word ;"><%#Container.DataItem("JobsSLA")%></td>
                                          <td width="60" class="clsBorderTblDetail" style="background-color:#efebe7;word-wrap:break-word ;"><%#Container.DataItem("JobsActive")%></td>
						                  <td width="40" class="clsBorderTblDetail" style="background-color:#efebe7;word-wrap:break-word ;">&nbsp;</td>
						            </tr>
						       </table>
						        
						    </ItemTemplate>
						    
						</asp:DataList>
						
						
                </ItemTemplate>
        </asp:datalist>
        
						       
						       <table width="950px" cellspacing="0" cellpadding="0" id="tblTotal" runat="server" style="margin-left:0px; margin-top:-8px;">
						            <tr id="tblDLRow" align="left">                 
						                   <td width="137" class="clsBorderTbl" style="word-wrap:break-word ;" ><b>Total</b></td>
						                   <td width="136" class="clsBorderTbl" style="word-wrap:break-word ;">&nbsp;</td>
                                          
                                          <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalEngAvail" style="font-weight:bold;"></asp:Label></td>
                                          <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalEngrOnTime" style="font-weight:bold;"></asp:Label></td>
                                          <td width="90" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalAvgJobTime" style="font-weight:bold;"></asp:Label></td>
                                          <td width="100" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalSuppliersUsed" style="font-weight:bold;"></asp:Label></td>
                                          <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalJobsReceived" style="font-weight:bold;"></asp:Label></td>
                                          <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalJobsInvoiced" style="font-weight:bold;"></asp:Label></td>
                                          <td width="60" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalJobsSLA" style="font-weight:bold;"></asp:Label></td>
                                          <td width="60" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalJobsActive" style="font-weight:bold;"></asp:Label></td>
						                  <td width="40" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="lblTotalJobs" style="font-weight:bold;"></asp:Label></td>
						            </tr>
						       </table>
             
            </asp:Panel>
            <asp:panel id="pnlNoRecords" runat="server" Visible="false">
                <div style="width:100%; height:500px; vertical-align:middle; text-align:center; margin-top:300px;">No records exist for the selected criteria</div>                      
            </asp:panel>
			
          				</td><td width="10">&nbsp;</td></tr></table>			 
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
         </ContentTemplate>
			</asp:UpdatePanel>
			
			 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelCreateWorkOrder" runat="server">
					<ProgressTemplate>
						<div>
							<img  align=middle src="Images/indicator.gif" />
							Please Wait...
						</div>      
					</ProgressTemplate>
			</asp:UpdateProgress>
			<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePanelCreateWorkOrder" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
       

 </asp:Content>
