﻿
Public Class WorkingDays
    Inherits System.Web.UI.Page

    Public Shared ws As New WSObjs

    Dim HolidayList As Hashtable

    Public list As List(Of DateTime) = New List(Of DateTime)
    Public listCal2 As List(Of DateTime) = New List(Of DateTime)

    Delegate Sub DelPopulateObject(ByVal myInt As Integer, ByVal InitialCall As Boolean, ByVal PagerIndex As Integer)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        HolidayList = Getholiday()


        If Calendar1.SelectedDates.Count = 0 Then
            Calendar1.SelectedDates.Clear()
        End If

        If Calendar2.SelectedDates.Count = 0 Then
            Calendar2.SelectedDates.Clear()
        End If

        If Not IsPostBack Then
            PopulateData()

            Calendar1.Font.Bold = True
            Calendar1.FirstDayOfWeek = WebControls.FirstDayOfWeek.Monday
            Calendar1.VisibleDate = DateTime.Today
            Calendar1.PrevMonthText = MonthName(IIf(Calendar1.VisibleDate.Month = 1, 12, Calendar1.VisibleDate.Month - 1), False)
            Calendar1.TitleFormat = TitleFormat.MonthYear
            Calendar1.ShowGridLines = True
            Calendar1.DayStyle.Height = New Unit(60)
            Calendar1.DayStyle.HorizontalAlign = HorizontalAlign.Center
            Calendar1.DayStyle.VerticalAlign = VerticalAlign.Middle

            'Calendar1.OtherMonthDayStyle.BackColor = System.Drawing.Color.LightGray
            'Calendar1.SelectedDates.Clear()
            'Calendar1.DataBind()


            Calendar2.Font.Bold = True
            Calendar2.FirstDayOfWeek = WebControls.FirstDayOfWeek.Monday
            Calendar2.VisibleDate = DateTime.Today.AddMonths(+1)
            Calendar2.NextMonthText = MonthName(IIf(Calendar2.VisibleDate.Month = 12, 1, Calendar2.VisibleDate.Month + 1), False)
            Calendar2.TitleFormat = TitleFormat.MonthYear
            Calendar2.ShowGridLines = True
            Calendar2.DayStyle.Height = New Unit(60)
            Calendar2.DayStyle.HorizontalAlign = HorizontalAlign.Center
            Calendar2.DayStyle.VerticalAlign = VerticalAlign.Middle
            'Calendar2.OtherMonthDayStyle.BackColor = System.Drawing.Color.LightGray

            'Calendar1.Visible = True

            ' Calendar2.SelectedDates.Clear()
            'Calendar2.DataBind()
        End If
        Dim delPopulate As New DelPopulateObject(AddressOf PopulateHolidays)
        GridPagerTop.UpdateIndex = delPopulate
        GridPagerTop.PageSize = ddlPageSize.SelectedValue
        GridPagerTop.PageObj = Me
        GridPagerTop.HasSinglePager = False
        GridPagerTop.useMaster = True
        GridPagerTop.ContentPlaceHolderId = "ContentPlaceHolder1"
        GridPagerTop.UCObj = Nothing

        GridPagerBtm.UpdateIndex = delPopulate
        GridPagerBtm.PageSize = ddlPageSize.SelectedValue
        GridPagerBtm.PageObj = Me
        GridPagerBtm.HasSinglePager = False
        GridPagerBtm.useMaster = True
        GridPagerBtm.ContentPlaceHolderId = "ContentPlaceHolder1"
        GridPagerBtm.UCObj = Nothing

        ViewState("PagerIndex") = 0

    End Sub

    Public Sub PopulateData()
        Dim ds As DataSet = CommonFunctions.GetSPHoliday(CInt(Request("companyid")), "", "Get")
        ' PopulateHolidays(0, True, 0)
        If ds.Tables("tblSPHolidaySummary").DefaultView.Count > 0 Then
            rptHolidays.DataSource() = ds.Tables("tblSPHolidaySummary")
            rptHolidays.DataBind()
        End If

        If (Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleClientID Then
            lnkBackToListing.HRef = "CompanyProfileBuyer.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId")
        ElseIf Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
            lnkBackToListing.HRef = "CompanyProfile.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId")
        End If

    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetBottom.Click
        PopulateData()
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveBottom.Click
        Dim WorkingDays As String
        WorkingDays = ""
        For i As Integer = 0 To rptHolidays.Items.Count - 1
            If CType(rptHolidays.Items(i).FindControl("chkHoliday"), CheckBox).Checked = True Then
                If (WorkingDays <> "") Then
                    WorkingDays = WorkingDays & "," & CType(rptHolidays.Items(i).FindControl("StandardID"), HtmlControls.HtmlInputHidden).Value
                Else
                    WorkingDays = CType(rptHolidays.Items(i).FindControl("StandardID"), HtmlControls.HtmlInputHidden).Value
                End If
            End If
        Next

        Dim ds As DataSet = CommonFunctions.GetSPHoliday(CInt(Request("companyid")), WorkingDays, "Add")

        If ds.Tables("tblSPHolidaySummary").DefaultView.Count > 0 Then
            rptHolidays.DataSource() = ds.Tables("tblSPHolidaySummary")
            rptHolidays.DataBind()
        End If
        lblMsg.Text = "Saved Successfully."
        PopulateData()
    End Sub

    Public Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles hdnButton.Click
        If Not IsNothing(ViewState("SelectedDateId")) Then
            Dim IsDeleted As Boolean
            'If (chkbxIsActive.Checked) Then
            '    IsDeleted = False
            'Else
            IsDeleted = False
            'End If

            Dim dtDatetimeFrom As Date = Nothing
            Dim dtDatetimeTo As Date = Nothing

            If txtHolidayDate.Text.Trim.Length > 0 Then
                dtDatetimeFrom = txtHolidayDate.Text.Trim
            End If

            If txtHolidayDateTo.Text.Trim.Length > 0 Then
                dtDatetimeTo = txtHolidayDateTo.Text.Trim
            End If

            If txtHolidayDate.Text.Trim.Length > 0 And txtHolidayDateTo.Text.Trim.Length > 0 Then
                If dtDatetimeFrom > dtDatetimeTo Then
                    lblError.Text = "HolidayDateFrom must not be greater than HolidayDateTo"
                    CType(Me.Page.Master.FindControl("ContentPlaceHolder1").FindControl("mdlHoliday"), AjaxControlToolkit.ModalPopupExtender).Show()
                    Exit Sub
                End If
            End If

            ws.WSContact.AddUpdateHolidays(ViewState("SelectedDateId"), txtHolidayDate.Text.Trim, txtHolidayCaption.Text.Trim, IsDeleted, Request("CompanyID"))

            txtHolidayDate.Text = ""
            txtHolidayCaption.Text = ""
            lblError.Text = ""
            CType(Me.Page.Master.FindControl("ContentPlaceHolder1").FindControl("mdlHoliday"), AjaxControlToolkit.ModalPopupExtender).Hide()
            'PopulateHolidays(0, True, 0)
        Else
            lblError.Text = OrderWorkLibrary.ResourceMessageText.GetString("DBUpdateFail")
            CType(Me.Page.Master.FindControl("ContentPlaceHolder1").FindControl("mdlHoliday"), AjaxControlToolkit.ModalPopupExtender).Show()
        End If
    End Sub
    Private Sub PopulateHolidays(ByVal StartIndex As Integer, ByVal InitialCall As Boolean, ByVal PagerIndex As Integer)
        Dim dsHolidays As DataSet

        ViewState("SortExpression") = "HolidayDate"
        ViewState("SortOrder") = "DESC"

        dsHolidays = ws.WSContact.GetHolidays(ViewState("SortExpression") & " " & ViewState("SortOrder"), StartIndex, 10, 0, "", Request("CompanyID"))

        If dsHolidays.Tables(1).Rows(0).Item("TotalRecords") <> 0 Then
            pnlHolidayListing.Visible = True
            'pnlnoRecords.Visible = False
            HttpContext.Current.Items("rowCount") = dsHolidays.Tables(1).Rows(0).Item("TotalRecords")
            grdHolidayList.DataSource = dsHolidays.Tables("HolidaysListing").DefaultView
            grdHolidayList.DataBind()
            GridPagerTop.TotalCount = dsHolidays.Tables(1).Rows(0).Item("TotalRecords")
            GridPagerBtm.TotalCount = dsHolidays.Tables(1).Rows(0).Item("TotalRecords")
            GridPagerTop.PageSize = ddlPageSize.SelectedValue
            GridPagerBtm.PageSize = ddlPageSize.SelectedValue

            If InitialCall = True Then
                GridPagerBtm.InitializePager()
                GridPagerTop.InitializePager()
            End If

            GridPagerTop.SetPager(PagerIndex)
            GridPagerBtm.SetPager(PagerIndex)

            ViewState("PagerIndex") = PagerIndex  'Intialze pager here
            tdTotalRecs.InnerHtml = "Total <strong>" & dsHolidays.Tables(1).Rows(0).Item("TotalRecords") & "</strong> results "
        Else
            pnlHolidayListing.Visible = False
            'pnlnoRecords.Visible = True
        End If
    End Sub

    Private Sub btnAddHoliday_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddHoliday.ServerClick
        ViewState.Add("SelectedDateId", 0)
        Calendar1.Visible = True
        lblHolidatDate.Visible = True
        txtHolidayDateTop.Visible = True
        btnSelectDates.Visible = True
        Calendar1.Enabled = True
        lblSelectDate.Visible = True
        Calendar2.Visible = True
        'ModeList.Visible = True
        btnAddHoliday.Visible = False
        btnClearSelection.Visible = True
        Calendar1.SelectionMode = CalendarSelectionMode.DayWeekMonth
        Calendar2.SelectionMode = CalendarSelectionMode.DayWeekMonth
        'CType(Me.Page.Master.FindControl("ContentPlaceHolder1").FindControl("mdlHoliday"), AjaxControlToolkit.ModalPopupExtender).Show()
    End Sub

    Public Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        ddlPageSizeBtm.SelectedValue = ddlPageSize.SelectedValue
        'PopulateHolidays(0, True, 0)
    End Sub
    Public Sub ddlPageSizeBtm_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        ddlPageSize.SelectedValue = ddlPageSizeBtm.SelectedValue
        ' PopulateHolidays(0, True, 0)
    End Sub

    Private Sub grdHolidayList_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdHolidayList.RowCommand
        Dim objLinkButton As LinkButton = CType(e.CommandSource, LinkButton)
        Dim objGridViewRow As GridViewRow = CType(objLinkButton.Parent.Parent, GridViewRow)
        Dim objCancelButton As LinkButton = CType(objGridViewRow.FindControl("lnkbtnCancel"), LinkButton)
        Dim objHolidayCaption As TextBox = CType(objGridViewRow.FindControl("txtHolidayCaption"), TextBox)
        Dim objLabelCaption As Label = CType(objGridViewRow.FindControl("lblHolidayCaption"), Label)
        Dim objchkIsActive As CheckBox = CType(objGridViewRow.FindControl("chkIsActive"), CheckBox)
        Dim objlblIsActive As Label = CType(objGridViewRow.FindControl("lblIsActive"), Label)
        Dim objtxtHolidayDate As TextBox = CType(objGridViewRow.FindControl("txtHolidayDate1"), TextBox)
        Dim objlblHolidayDate As Label = CType(objGridViewRow.FindControl("lblHolidayDate"), Label)
        Dim objImgCal As HtmlImage = CType(objGridViewRow.FindControl("imgCalHoliday"), HtmlImage)

        Select Case e.CommandName
            Case "EditHoliday"
                objLinkButton.Text = "Save"
                objLinkButton.CommandName = "SaveHoliday"
                objHolidayCaption.Visible = True
                objchkIsActive.Visible = True
                objLabelCaption.Visible = False
                objlblIsActive.Visible = False
                objCancelButton.Visible = True
                objtxtHolidayDate.Visible = True
                objlblHolidayDate.Visible = False
                objImgCal.Visible = True
                ViewState.Add("SelectedDateId", e.CommandArgument)

            Case "SaveHoliday"
                ws.WSContact.AddUpdateHolidays(ViewState("SelectedDateId"), objtxtHolidayDate.Text.Trim, objHolidayCaption.Text.Trim, IIf(objchkIsActive.Checked, 0, 1), Request("CompanyID"))
                objHolidayCaption.Visible = False
                objLinkButton.Text = "Edit"
                objLinkButton.CommandName = "EditHoliday"
                objLabelCaption.Visible = True
                objchkIsActive.Visible = False
                objlblIsActive.Visible = True
                objCancelButton.Visible = False
                objtxtHolidayDate.Visible = False
                objlblHolidayDate.Visible = True
                objImgCal.Visible = False
                PopulateHolidays(0, True, 0)

            Case "InActive"
                ws.WSContact.AddUpdateHolidays(e.CommandArgument, objlblHolidayDate.Text.Trim, objLabelCaption.Text.Trim, 1, Request("CompanyID"))
                PopulateHolidays(0, True, 0)

            Case "Cancel"
                PopulateHolidays(0, True, 0)
        End Select

    End Sub

    Private Sub grdHolidayList_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdHolidayList.RowEditing

    End Sub

    Private Sub grdHolidayList_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdHolidayList.RowUpdating

    End Sub

    Private Sub grdHolidayList_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdHolidayList.RowCancelingEdit

    End Sub

    Public Sub SetPagerButtonStates(ByVal page As Object, ByVal PagerIndex As Integer, ByVal PageSize As Integer)
        Dim pageIndex As Integer = PagerIndex
        Dim pageCount As Integer = GridPagerTop.TotalCount

        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(Me.Page.FindControl("ctl00_ContentPlaceHolder1_GridPagerTop_btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(Me.Page.FindControl("ctl00_ContentPlaceHolder1_GridPagerTop_btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(Me.Page.FindControl("ctl00_ContentPlaceHolder1_GridPagerTop_btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(Me.Page.FindControl("ctl00_ContentPlaceHolder1_GridPagerTop_btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(Me.Page.FindControl("ctl00_ContentPlaceHolder1_GridPagerTop_tdTotalCount"), HtmlGenericControl)

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (PageSize) - PageSize + (ViewState("rowCount") Mod PageSize)
        Else
            calTo = pageIndex * PageSize + PageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * PageSize + 1
        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        tdTotalCount.InnerHtml = "Total <strong>" & ViewState("rowCount") & "</strong> results "

        'CType(gvPagerRow.FindControl("lblPages"), Label).Text = GridView.PageCount.ToString
        Dim ddlPageSelector1 As DropDownList = CType(Me.Page.FindControl("ctl00_ContentPlaceHolder1_GridPagerTop_ddPageNumber"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To GridPagerTop.TotalCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex

        'Dim ddlPageSize As DropDownList = CType(gvPagerRow.FindControl("ddlPageSize"), DropDownList)
        ddlPageSize.SelectedValue = PagerIndex
    End Sub

    Private Function Getholiday() As Hashtable
        Dim holiday As New Hashtable()
        Dim dsHolidays As DataSet
        ViewState("SortExpression") = "HolidayDate"
        ViewState("SortOrder") = "DESC"

        dsHolidays = ws.WSContact.GetHolidays(ViewState("SortExpression") & " " & ViewState("SortOrder"), 0, 0, 0, "", Request("CompanyID"))

        If dsHolidays.Tables(1).Rows(0).Item("TotalRecords") <> 0 Then
            For Each objDataRow As DataRow In dsHolidays.Tables("HolidaysListing").Rows
                holiday(objDataRow("HolidayDate")) = objDataRow("HolidayTxt")
            Next
        End If

        Return holiday
    End Function

    Protected Sub Calendar1_DayRender(sender As Object, e As DayRenderEventArgs) Handles Calendar1.DayRender, Calendar2.DayRender

        If Not e.Day.IsOtherMonth Then

            If Calendar1.SelectionMode = CalendarSelectionMode.DayWeekMonth And Calendar2.SelectionMode = CalendarSelectionMode.DayWeekMonth Then
                e.Cell.Attributes.Add("OnClick", e.SelectUrl)
                e.Cell.Style.Add("cursor", "pointer")
            End If

            Dim newList As List(Of DateTime) = DirectCast(Session("SelectedDates"), List(Of DateTime))

            If Not IsNothing(Session("SelectedDates")) Then
                If Calendar1.SelectedDates.Count = 1 Then
                    If newList.Count = 1 Then
                        If Calendar1.SelectedDate.ToShortDateString.ToString = newList.Item(0).ToShortDateString.ToString And Calendar2.SelectedDates.Count = 0 Then
                            Session("SelectedDates") = ""
                            Calendar1.SelectedDates.Clear()
                        End If
                    End If
                End If

                If Calendar2.SelectedDates.Count = 1 Then
                    If newList.Count = 1 Then
                        If Calendar2.SelectedDate.ToShortDateString.ToString = newList.Item(0).ToShortDateString.ToString And Calendar1.SelectedDates.Count = 0 Then
                            Session("SelectedDates") = ""
                            Calendar2.SelectedDates.Clear()
                        End If
                    End If
                End If
            End If

            If e.Day.IsSelected = True Then
                If list.Contains(e.Day.Date) Then
                    list.Remove(e.Day.Date)
                Else
                    list.Add(e.Day.Date)
                End If
            End If

            Session("SelectedDates") = list

            Dim literal1 As New Literal()
            literal1.Text = "<br/>"
            e.Cell.Controls.Add(literal1)

            If Not IsNothing(HolidayList) Then
                If Not IsNothing(HolidayList(e.Day.[Date].ToShortDateString())) Then
                    Dim objLinkButton As New LinkButton
                    objLinkButton.Text = DirectCast(HolidayList(e.Day.[Date].ToShortDateString()), String)
                    objLinkButton.Font.Size = New FontUnit(FontSize.Smaller)
                    e.Cell.Controls.Add(objLinkButton)
                End If
            End If
        Else
            e.Day.IsSelectable = False
            Calendar1.SelectedDates.Remove(e.Day.Date)
            e.Cell.DisabledCssClass = True
            'e.Cell.Text = ""
            'e.Cell.Visible = False
            'e.Cell.BackColor = Drawing.Color.LightGray
        End If

        'Calendar1.DataBind()
    End Sub

    'Protected Sub Calendar2_DayRender(sender As Object, e As DayRenderEventArgs) Handles Calendar2.DayRender

    '    If Not e.Day.IsOtherMonth Then
    '        e.Cell.Attributes.Add("OnClick", e.SelectUrl)
    '        e.Cell.Style.Add("cursor", "pointer")

    '        Dim newList As List(Of DateTime) = DirectCast(Session("SelectedDatesCal2"), List(Of DateTime))

    '        If Not IsNothing(Session("SelectedDatesCal2")) Then
    '            If Calendar1.SelectedDates.Count = 1 Then
    '                If newList.Count = 1 Then
    '                    If Calendar1.SelectedDate.ToShortDateString.ToString = newList.Item(0).ToShortDateString.ToString And Calendar2.SelectedDates.Count = 0 Then
    '                        Session("SelectedDatesCal2") = ""
    '                        Calendar1.SelectedDates.Clear()
    '                    End If
    '                End If
    '            End If

    '            If Calendar2.SelectedDates.Count = 1 Then
    '                If newList.Count = 1 Then
    '                    If Calendar2.SelectedDate.ToShortDateString.ToString = newList.Item(0).ToShortDateString.ToString Then
    '                        Session.Remove("SelectedDatesCal2")
    '                        Calendar2.SelectedDates.Clear()
    '                    End If
    '                End If
    '            End If
    '        End If

    '        If e.Day.IsSelected = True Then
    '            If listCal2.Contains(e.Day.Date) Then
    '                listCal2.Remove(e.Day.Date)
    '            Else
    '                listCal2.Add(e.Day.Date)
    '            End If
    '        End If

    '        Session("SelectedDatesCal2") = listCal2

    '        Dim literal1 As New Literal()
    '        literal1.Text = "<br/>"
    '        e.Cell.Controls.Add(literal1)

    '        If Not IsNothing(HolidayList) Then
    '            If Not IsNothing(HolidayList(e.Day.[Date].ToShortDateString())) Then
    '                Dim objLinkButton As New LinkButton
    '                objLinkButton.Text = DirectCast(HolidayList(e.Day.[Date].ToShortDateString()), String)
    '                objLinkButton.Font.Size = New FontUnit(FontSize.Smaller)
    '                e.Cell.Controls.Add(objLinkButton)
    '            End If
    '        End If
    '    Else

    '        'e.Day.IsSelectable = False
    '        'e.Cell.Text = ""
    '        'e.Cell.BackColor = Drawing.Color.LightGray
    '    End If

    '    'Calendar1.DataBind()
    'End Sub

    'Protected Sub Calendar2_DayRender(sender As Object, e As DayRenderEventArgs) Handles Calendar2.DayRender
    '    If e.Day.IsSelected = True Then
    '        If list.Contains(e.Day.Date) Then
    '            list.Remove(e.Day.Date)
    '        Else
    '            list.Add(e.Day.Date)
    '        End If
    '    End If

    '    Session("SelectedDates") = list

    '    Dim literal1 As New Literal()
    '    literal1.Text = "<br/>"
    '    e.Cell.Controls.Add(literal1)

    '    'MakeEntireCellClickable(e)
    '    If Not IsNothing(HolidayList) Then
    '        If Not IsNothing(HolidayList(e.Day.[Date].ToShortDateString())) Then
    '            Dim label1 As New Label()
    '            label1.Text = DirectCast(HolidayList(e.Day.[Date].ToShortDateString()), String)
    '            label1.Font.Size = New FontUnit(FontSize.Smaller)
    '            e.Cell.Controls.Add(label1)
    '        End If
    '    End If

    'End Sub

    Private Sub Calendar1_SelectionChanged(sender As Object, e As EventArgs) Handles Calendar1.SelectionChanged

        If Session("SelectedDates") IsNot Nothing Then
            Dim newList As List(Of DateTime) = DirectCast(Session("SelectedDates"), List(Of DateTime))

            For Each dt As DateTime In newList
                If Not IsNothing(dt) Then
                    If Calendar1.SelectedDates.Contains(dt) Then
                        Calendar1.SelectedDates.Remove(dt)
                    Else
                        Calendar1.SelectedDates.Add(dt)
                    End If
                End If
            Next
            'list.Clear()
        End If

    End Sub

    'Public Property SelectedDates() As List(Of DateTime)
    '    Get
    '        If ViewState("Dates") Is Nothing Then
    '            ' Add a hidden dateTime to clear the selection of Date when             
    '            ' there is only one date in the selected Dates            
    '            ViewState("Dates") = New List(Of DateTime)() From { _
    '             DateTime.MaxValue.AddDays(-2) _
    '            }
    '        End If
    '        Return DirectCast(ViewState("Dates"), List(Of DateTime))
    '    End Get
    '    Set(value As List(Of DateTime))
    '        ViewState("Dates") = value
    '    End Set
    'End Property

    Private Sub Calendar2_SelectionChanged(sender As Object, e As EventArgs) Handles Calendar2.SelectionChanged
        If Session("SelectedDates") IsNot Nothing Then
            Dim newList As List(Of DateTime) = DirectCast(Session("SelectedDates"), List(Of DateTime))

            For Each dt As DateTime In newList
                If Calendar2.SelectedDates.Contains(dt) Then
                    Calendar2.SelectedDates.Remove(dt)
                Else
                    Calendar2.SelectedDates.Add(dt)
                End If
            Next
            listCal2.Clear()
        End If
    End Sub

    Private Sub Calendar1_PreRender(sender As Object, e As System.EventArgs) Handles Calendar1.PreRender

        If Calendar1.SelectedDates.Count = 0 Then
            Calendar1.SelectedDates.Clear()
            Session.Remove("SelectedDates")
        End If

        'If Session("SelectedDates") IsNot Nothing Then
        '    Dim newList As List(Of DateTime) = DirectCast(Session("SelectedDates"), List(Of DateTime))

        '    For Each dt As DateTime In newList
        '        If Not IsNothing(dt) Then
        '            If Calendar1.SelectedDates.Contains(dt) Then
        '                Calendar1.SelectedDates.Remove(dt)
        '            Else
        '                Calendar1.SelectedDates.Add(dt)
        '            End If

        '            'If Calendar2.SelectedDates.Count > 0 Then
        '            '    If Calendar2.SelectedDates.Contains(dt) Then
        '            '        Calendar2.SelectedDates.Remove(dt)
        '            '    Else
        '            '        Calendar2.SelectedDates.Add(dt)
        '            '    End If
        '            'End If

        '        End If
        '    Next
        '    list.Clear()
        'End If

    End Sub

    Private Sub Calendar2_PreRender(sender As Object, e As System.EventArgs) Handles Calendar2.PreRender

        If Calendar2.SelectedDates.Count = 0 Then
            Calendar2.SelectedDates.Clear()
            Session.Remove("SelectedDates")
        End If


        'If Session("SelectedDates") IsNot Nothing Then
        '    Dim newList As List(Of DateTime) = DirectCast(Session("SelectedDates"), List(Of DateTime))

        '    For Each dt As DateTime In newList
        '        If Not IsNothing(dt) Then
        '            If Calendar2.SelectedDates.Contains(dt) Then
        '                Calendar2.SelectedDates.Remove(dt)
        '            Else
        '                Calendar2.SelectedDates.Add(dt)
        '            End If

        '            If Calendar1.SelectedDates.Count > 0 Then
        '                If Calendar1.SelectedDates.Contains(dt) Then
        '                    Calendar1.SelectedDates.Remove(dt)
        '                Else
        '                    Calendar1.SelectedDates.Add(dt)
        '                End If
        '            End If

        '        End If
        '    Next
        '    list.Clear()
        'End If

    End Sub

    'Private Sub Calendar2_PreRender(sender As Object, e As System.EventArgs) Handles Calendar2.PreRender
    '    If Session("SelectedDates") IsNot Nothing Then
    '        'Calendar1.SelectedDates.Clear()
    '        For Each dt As DateTime In DirectCast(Session("SelectedDates"), List(Of DateTime))
    '            If Calendar2.SelectedDates.Contains(dt) Then
    '                Calendar2.SelectedDates.Remove(dt)
    '            Else
    '                Calendar2.SelectedDates.Add(dt)
    '            End If

    '            If Calendar1.SelectedDates.Contains(dt) Then
    '                Calendar1.SelectedDates.Remove(dt)
    '            Else
    '                Calendar1.SelectedDates.Add(dt)
    '            End If
    '        Next
    '    End If
    'End Sub

    Protected Sub Calendar1_VisibleMonthChanged(sender As Object, e As MonthChangedEventArgs) Handles Calendar1.VisibleMonthChanged
        SetVisibleDate(Calendar1.VisibleDate)
    End Sub

    Private Sub Calendar2_VisibleMonthChanged(sender As Object, e As System.Web.UI.WebControls.MonthChangedEventArgs) Handles Calendar2.VisibleMonthChanged
        SetVisibleDate(Calendar2.VisibleDate.AddMonths(-1))
    End Sub

    Protected Sub SetVisibleDate(day As DateTime)
        Calendar1.VisibleDate = day
        Calendar2.VisibleDate = day.AddMonths(1)
        Calendar1.PrevMonthText = MonthName(IIf(Calendar1.VisibleDate.Month = 1, 12, Calendar1.VisibleDate.Month - 1), False)
        Calendar2.NextMonthText = MonthName(IIf(Calendar2.VisibleDate.Month = 12, 1, Calendar2.VisibleDate.Month + 1), False)
    End Sub

    Private Sub btnSelectDates_Click(sender As Object, e As System.EventArgs) Handles btnSelectDates.Click
        Dim strHolidayString As New StringBuilder
        Dim strHolidayDates As String

        If Calendar1.SelectedDates.Count = 0 And Calendar2.SelectedDates.Count = 0 Then
            lblErrorMessage.Visible = True
            lblErrorMessage.Text = "Please select dates to book holidays"
            Exit Sub
        End If

        For Each objList As DateTime In DirectCast(Session("SelectedDates"), List(Of DateTime))
            If strHolidayString.ToString.Length > 0 Then
                strHolidayString.Append("," & objList.ToShortDateString.ToString)
            Else
                strHolidayString.Append(objList.ToShortDateString.ToString)
            End If
        Next

        strHolidayDates = strHolidayString.ToString

        ws.WSContact.AddUpdateHolidays(0, strHolidayDates.ToString, txtHolidayDateTop.Text.Trim, 0, Request("CompanyID"))

        HolidayList = Getholiday()

        Calendar1.SelectedDates.Clear()
        Calendar2.SelectedDates.Clear()

        Session.Remove("SelectedDates")

        Calendar1.SelectionMode = CalendarSelectionMode.None
        Calendar2.SelectionMode = CalendarSelectionMode.None

        lblHolidatDate.Visible = False
        txtHolidayDateTop.Visible = False
        btnSelectDates.Visible = False
        lblSelectDate.Visible = False
        txtHolidayCaption.Text = ""
        btnAddHoliday.Visible = True
        btnClearSelection.Visible = False
    End Sub

    'Private Sub Calendar1_Load(sender As Object, e As System.EventArgs) Handles Calendar1.Load
    '    If Session("SelectedDates") IsNot Nothing Then
    '        Dim newList As List(Of DateTime) = DirectCast(Session("SelectedDates"), List(Of DateTime))

    '        For Each dt As DateTime In newList
    '            If Not IsNothing(dt) Then
    '                'If Calendar2.SelectedDates.Count > 0 Then
    '                '    If Calendar2.SelectedDates.Contains(dt) Then
    '                '        Calendar2.SelectedDates.Remove(dt)
    '                '    Else
    '                '        Calendar2.SelectedDates.Add(dt)
    '                '    End If
    '                'End If

    '                If Calendar1.SelectedDates.Contains(dt) Then
    '                    Calendar1.SelectedDates.Remove(dt)
    '                Else
    '                    Calendar1.SelectedDates.Add(dt)
    '                End If
    '            End If
    '        Next
    '        'list.Clear()
    '    End If
    'End Sub

    'Private Sub Calendar2_Load(sender As Object, e As System.EventArgs) Handles Calendar2.Load
    '    If Session("SelectedDatesCal2") IsNot Nothing Then
    '        Dim newList As List(Of DateTime) = DirectCast(Session("SelectedDatesCal2"), List(Of DateTime))

    '        For Each dt As DateTime In newList
    '            If Not IsNothing(dt) Then
    '                If Calendar2.SelectedDates.Contains(dt) Then
    '                    Calendar2.SelectedDates.Remove(dt)
    '                Else
    '                    Calendar2.SelectedDates.Add(dt)
    '                End If

    '                'If Calendar1.SelectedDates.Count > 0 Then
    '                '    If Calendar1.SelectedDates.Contains(dt) Then
    '                '        Calendar1.SelectedDates.Remove(dt)
    '                '    Else
    '                '        Calendar1.SelectedDates.Add(dt)
    '                '    End If
    '                'End If
    '            End If
    '        Next
    '        ' list.Clear()
    '    End If
    'End Sub

    Private Sub btnClearSelection_Click(sender As Object, e As System.EventArgs) Handles btnClearSelection.Click
        Calendar1.SelectedDates.Clear()
        Calendar2.SelectedDates.Clear()
    End Sub
End Class
