<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Generate Depot" CodeBehind="GenerateDepot.aspx.vb" Inherits="Admin.GenerateDepot" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
	<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>




	

			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
               <asp:UpdatePanel ID="UpdatePanelMultipleWO" runat="server">
                <ContentTemplate>
                 <asp:Panel ID="pnlMultipleWO" runat="server">
                  <div style="margin-top:20; margin-left:20px;">     
            <table width="500px" cellpadding="0" cellspacing="0" border="0">
		  <tr>		  			
		   <td align="left" >
                <div id="tblViewBtn" runat="server" style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:120px;">
                    <asp:linkButton ID="lnkPrint" Visible="true" CommandName="PrintDepotSheets" CssClass="txtButtonRed" runat="server" >Print Depot Sheets</asp:linkButton>
                </div>
		 </td>
		 <td width="10px">&nbsp;</td>
		 <td align="left">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:120px;">
                <asp:linkButton ID="lnkEmail" Visible="true" CommandName="EmailDepotSheets" CssClass="txtButtonRed" runat="server" >Email Depot Sheets</asp:linkButton>
            </div>
		</td>
		<td width="10px">&nbsp;</td>
		<td align="left" id="tdGenDepot" runat="server" >
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:120px;">
                <a runat="server" ID="lnkbtnCancel" class="txtButtonRed" onserverclick="lnkbtnCancel_ServerClick">Back to Listing</a>
            </div>	    
		</td>
		 </tr>		
	   </table>
          <cc1:ModalPopupExtender ID="mdlCollectionDate" runat="server" TargetControlID="btnTemp" PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
				      
<asp:Panel runat="server" ID="pnlConfirm" Width="250px" CssClass="pnlConfirm" style="display:none;">
    <div id="divModalParent" style="display:block">
        <span style="font-family:Tahoma; font-size:14px;"><b>Enter Collection Date</b></span><br /><br />
        <asp:TextBox runat="server" ID="txtDate" Height="20" Width="200" CssClass="bodyTextGreyLeftAligned" ></asp:TextBox>                                
        <img alt="Click to Select" src="Images/calendar.gif" id="btnBeginDate" style="cursor:pointer; vertical-align:top;" />		                               
         <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnBeginDate TargetControlID="txtDate" ID="calBeginDate" runat="server" ></cc1:CalendarExtender>
            <div id="divButton" class="divButton" style="width: 85px; float:left;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                    <a class="buttonText cursorHand" onclick='javascript:validate_required("ctl00_ContentPlaceHolder1_txtDate","Please enter a date")' id="ancSubmit">Submit</a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div>
            <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>
    <div id="progressBar" style="display:none">
        <img  align=middle src="Images/indicator.gif" />
                 Processing... Please wait
    </div>							
</asp:Panel>
<asp:Button runat="server" ID="hdnButton" Height="0" Width="0" BorderWidth="0" />
<asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" />
<asp:LinkButton runat="server" ID="lnkPrintDepot"><a id="PrintDepotSheet" target="_blank" runat="server">&nbsp;</a></asp:LinkButton>

             <div style="margin-top:30px;">
             <asp:DataList ID="dlDepotSheet" runat="server">
             <HeaderTemplate>
               <table width="100%" cellpadding="0" cellspacing="0" border="1">        
                <tr>
                    <td width="160" style="padding-left:10px"><b>Client</b></td>
                    <td width="140" style="padding-left:10px"><b>Assigned Depot User</b></td>
                    <td width="190" style="padding-left:10px"><b>Supplier</b></td>
                    <td width="300" style="padding-left:10px"><b>Work Order Number</b></td>
                    <td width="90" style="padding-left:10px"><b>Depot Location</b></td>
                </tr>
             </table>
             </HeaderTemplate>
             <ItemTemplate>               
             <table width="100%" cellpadding="0" cellspacing="0" border="1">        
                <tr>
                    <td width="160" style="padding-left:10px"><%#Container.DataItem("Client")%></td>
                    <td width="140" style="padding-left:10px"><%#Container.DataItem("DepotUser")%></td>
                    <td width="190" style="padding-left:10px"><%#Container.DataItem("Supplier")%></td>
                    <td width="300" style="padding-left:10px"><%#Container.DataItem("WorkOrderId")%></td>
                    <td width="90" style="padding-left:10px"><%#Container.DataItem("DepotLocation")%></td>
                </tr>
              </table>   
             </ItemTemplate>             
             </asp:DataList>    
             </div>                
             </div>
               </asp:Panel>
               <asp:Panel ID="pnlNoRecords" runat="server" >
               <div style="margin-top:50px; text-align:center; ">
                      <div style="margin-left:350px;">
                      <table width="500px" cellpadding="0" cellspacing="0" border="0">
		  <tr>
		   <td align="left" >
                <div id="tblNoRecordsExit" runat="server" style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:120px;">
                    <a runat="server" id="btnNoRecords" class="txtButtonRed" > &nbsp;Back To Listing&nbsp;</a>
                </div>
		 </td></tr></table>
		 </div>
                    <div style="margin-top:50; ">
                        No records exist.
                    </div>
                    </div>
               </asp:Panel>
            </ContentTemplate>
             <Triggers>
  	            
	          
              </Triggers>
            </asp:UpdatePanel>
            
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
<script type="text/javascript">
function validate_required(field,alerttxt)
{
      var text = document.getElementById(field).value;
      if (text==null||text=="")
         {
            alert(alerttxt);
         }
        else {
            var re5digit="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"; //regular expression defining a 5 digit number
            if (document.getElementById(field).value.search(re5digit)==-1) //if match failed
                alert("Date should be in DD/MM/YYYY Format");
            else
            {document.getElementById("divModalParent").style.display = "none";
            document.getElementById("progressBar").style.display = "block";                
                document.getElementById("ctl00_ContentPlaceHolder1_hdnButton").click();}
        }             
}
</script>
      </asp:Content>
