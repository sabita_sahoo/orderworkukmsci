
Partial Public Class CreateDebitNotes
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim invoiceno As String = ""
            Dim pagesender As String = ""
            If Not IsNothing(Request("invoiceNo")) Then
                invoiceno = Request("invoiceNo")
            End If

            If Not IsNothing(Request("sender")) Then
                pagesender = Request("sender")
            End If

            If invoiceno <> "" And pagesender <> "" Then
                If pagesender = "GenCNDN" Then
                    txtPINumber.Text = invoiceno
                    'Added Condition by Pankaj Malav on 20 Nov 2008 so as to raise credit note directly
                ElseIf pagesender = "SearchFinance" Then
                    txtPINumber.Text = invoiceno
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Handles click of submit button on the first screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnSubmit.Click
        'Validating Sales invoice number has been entered
        Page.Validate()
        If Page.IsValid Then
            'Validating Proper Sales invoice
            If validateSI() Then
                hdnIsNew.Value = ViewState("IsNew").ToString()
                hdnAdminFee.Value = ViewState("AdminFee").ToString()
                hdnPaymentDiscount.Value = ViewState("PaymentDiscount").ToString()
                'Hiding Input for Sales invoice
                divValidationMain.Visible = False
                tblSINumber.Visible = False
                'Showing Input for Amount
                tblProceed.Visible = True
                If (ViewState("IsNew") = False) Then
                    lblNewNet.Text = "Amount (exclusive of VAT)"
                    lblNewFinalAmt.Text = "Less 10% Discount (exclusive of VAT)"
                Else
                    lblNewNet.Text = "Amount (exclusive of VAT) less " & ViewState("AdminFee") & "% Admin Fee"
                    lblNewFinalAmt.Text = "Less " & ViewState("PaymentDiscount") & "% early payment fee (exclusive of VAT)"
                End If
                'Populating SI
                lblPINo.Text = ViewState("PI")
                'Populating Amount
                lblPIAmt.Text = ViewState("Amount")
                If (ViewState("IsNew") = False) Then
                    txtbxAmt.Text = ViewState("Amount")
                Else
                    txtbxAmt.Text = Format(ViewState("Amount") - (ViewState("Amount") * ViewState("AdminFee") / 100), "0.00")
                End If

                txtbxAmt.Enabled = False
                If (ViewState("IsNew") = False) Then
                    txtFinalAmt.Text = (ViewState("Amount") * 0.9)
                Else
                    txtFinalAmt.Text = Format(txtbxAmt.Text.Trim() - (txtbxAmt.Text.Trim() * ViewState("PaymentDiscount") / 100), "0.00")
                End If
                txtFinalAmt.Enabled = False
            Else
                divValidationMain.Visible = True
            End If
            Else
                divValidationMain.Visible = True
            End If
    End Sub

    ''' <summary>
    ''' Validating sales invoice
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function validateSI() As Boolean
        Dim ds As New DataSet
        'Checking text entered in sales invoice is valid invoice number if yes then get the amount as well
        ds = ws.WSFinance.ValidateInvoices(txtPINumber.Text.Trim.ToString, "Purchase")
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("IsRefunded") = True Then
                    lblErrMsg.Text = "Purchase Invoice is already refunded"
                    Return False
                ElseIf (ds.Tables(0).Rows(0)("Status") = "Processed") Or (ds.Tables(0).Rows(0)("Status") = "Paid") Then
                    lblErrMsg.Text = "A debit note could not be created as this purchase invoice has been marked as paid"
                    Return False
                Else
                    ViewState("IsNew") = ds.Tables(0).Rows(0)("IsNew")
                    ViewState("AdminFee") = ds.Tables(0).Rows(0)("AdminFee")
                    ViewState("PaymentDiscount") = ds.Tables(0).Rows(0)("PaymentDiscount")
                    ViewState("PI") = ds.Tables(0).Rows(0)("InvoiceNo")
                    ViewState("Amount") = ds.Tables(0).Rows(0)("Amount")
                    ViewState("VatAmt") = ds.Tables(0).Rows(0)("VatAmount")
                    ViewState("TotalAmt") = ds.Tables(0).Rows(0)("TotalAmt")
                    ViewState("VatRate") = ds.Tables(0).Rows(0)("VatRate")
                    ViewState("AmountAfterAdminFee") = ds.Tables(0).Rows(0)("AmountAfterAdminFee")
                    ViewState("AmountAfterDisc") = ds.Tables(0).Rows(0)("AmountAfterDisc")
                    ViewState("PaymentTermDiscountApplied") = ds.Tables(0).Rows(0)("PaymentTermDiscountApplied")

                    If Not IsDBNull(ds.Tables(0).Rows(0)("SPFees")) Then
                        If ds.Tables(0).Rows(0)("SPFees") > 0 Then
                            ViewState("SPFees") = ds.Tables(0).Rows(0)("SPFees")
                        Else
                            ViewState("SPFees") = 0
                        End If
                    Else
                        ViewState("SPFees") = 0
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(0)("VATRegistered")) Then
                        ViewState("VatRegistered") = ds.Tables(0).Rows(0)("VATRegistered")
                    Else
                        ViewState("VatRegistered") = "no"
                    End If
                    Return True
                End If

            Else
                'Show validation message
                lblErrMsg.Text = "Please enter valid Purchase Invoice number"
                Return False
            End If
        Else
            'Show validation message
            lblErrMsg.Text = "Please enter valid Purchase Invoice number"
            Return False
        End If
    End Function

    ''' <summary>
    ''' Handles change of radio button to cater for full or partial amount
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Public Sub rdbtnFull_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbtnFull.CheckedChanged
    '    If rdbtnFull.Checked Then
    '        'If full then show full amount
    '        txtbxAmt.Text = ViewState("Amount")
    '        'Disable text box
    '        txtbxAmt.Enabled = False
    '    Else
    '        'If partial then show 0 amount
    '        txtbxAmt.Text = 0.0
    '        'Enable text box to let the admin enter the values
    '        txtbxAmt.Enabled = True
    '    End If
    'End Sub

    ''' <summary>
    ''' Handles cancel button click on second screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnCancel.Click
        tblSINumber.Visible = True
        tblProceed.Visible = False
    End Sub

    ''' <summary>
    ''' Handles Next button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnNext.Click
        Page.Validate()
        If IsValid() Then
            If CDec(txtbxAmt.Text.Trim) > ViewState("Amount") Then
                lblErrMsg.Text = "Please enter valid amount for Debit Note, It cannot be greater than the Purchase Invoice Amount"
                divValidationMain.Visible = True
            Else
                divValidationMain.Visible = False
                tblConfirmationButtons.Visible = True
                tblProceed.Visible = False
                If (ViewState("IsNew") = False) Then
                    tblConfirmation.Visible = True
                    tblConfirmationNew.Visible = False
                    lblConfPINo.Text = ViewState("PI")
                    lblConfNetAmt.Text = ViewState("Amount")
                    lblConfVatAmt.Text = ViewState("VatAmt")
                    lblConfTotAmt.Text = ViewState("TotalAmt")
                Else
                    tblConfirmation.Visible = False
                    tblConfirmationNew.Visible = True
                    lblConfPINoNew.Text = ViewState("PI")
                    lblConfNetAmtNew.Text = ViewState("Amount")
                    lblAdminFeePI.Text = ViewState("AdminFee")
                    lblPIAmtAdminFee.Text = ViewState("AmountAfterAdminFee")
                    lblPaymentDiscount.Text = ViewState("PaymentDiscount")
                    lblPIPaymentDisc.Text = ViewState("PaymentTermDiscountApplied")
                    lblPIAmtExVat.Text = ViewState("AmountAfterDisc")
                    lblConfVatAmtNew.Text = ViewState("VatAmt")
                    lblConfTotAmtNew.Text = ViewState("TotalAmt")
                End If


                'Apply old calculation logic
                If (ViewState("IsNew") = False) Then
                    Dim amt As Double
                    Dim amtafterdisc As Double
                    Dim vat As Double
                    Dim SPFees As Double = 0
                    Try
                        If (CDec(txtDebitAmt.Text.Trim) <> 0) Then
                            amt = CDec(txtDebitAmt.Text.Trim)
                        Else
                            amt = CDec(txtbxAmt.Text.Trim)
                        End If
                        If Not IsNothing(ViewState("SPFees")) Then
                            SPFees = CDec(ViewState("SPFees"))
                        Else
                            SPFees = 0
                        End If
                        amtafterdisc = amt - (SPFees * amt)
                        If amt > 0 Then
                            If Not IsNothing(ViewState("VatRegistered")) Then
                                If ViewState("VatRegistered").ToString.ToLower = "yes" Then
                                    Dim vatrate As Double
                                    If (Convert.ToDouble(ViewState("VatRate")) <= Convert.ToDouble(ApplicationSettings.VATPercentageOld) + 0.5) And (Convert.ToDouble(ViewState("VatRate")) >= Convert.ToDouble(ApplicationSettings.VATPercentageOld) - 0.5) Then
                                        vatrate = ApplicationSettings.VATPercentageOld
                                    Else
                                        vatrate = ApplicationSettings.VATPercentage
                                    End If
                                    vat = FormatCurrency(amtafterdisc * ((vatrate) / 100))
                                Else
                                    vat = 0.0
                                End If
                            Else
                                vat = 0.0
                            End If
                        Else
                            vat = 0.0
                            amtafterdisc = 0.0
                        End If
                    Catch ex As Exception
                        amt = 0.0
                        vat = 0.0
                        amtafterdisc = 0.0
                    End Try
                    ViewState("DNAmount") = amt
                    lblConfDNNetAmt.Text = FormatCurrency(amt, 2)
                    lblConfDNVat.Text = FormatCurrency(vat, 2)
                    lblConfDNTotAmt.Text = FormatCurrency((amtafterdisc + vat), 2)

                Else 'Apply new calculation logic

                    Dim dnAmt As Double = 0.0
                    Dim dnAmtAdminFee As Double = 0.0
                    Dim dnAmtPaymentDisc As Double = 0.0
                    Dim dnAmtAfterDisc As Double = 0.0
                    Dim dnVat As Double = 0.0
                    Dim dnTotalAmt As Double = 0.0
                    
                    Try
                        If (CDec(txtDebitAmt.Text.Trim) <> 0) Then
                            dnAmt = CDec(txtDebitAmt.Text.Trim)
                        Else
                            dnAmt = CDec(lblPIAmt.Text.Trim)
                        End If
                    Catch ex As Exception
                        dnAmt = 0.0
                     End Try
                    ViewState("DNAmount") = dnAmt
                    lblConfDNNetAmtNew.Text = dnAmt
                    lblDNAdminFee.Text = ViewState("AdminFee")

                    dnAmtAdminFee = CDec(dnAmt - (dnAmt * ViewState("AdminFee") / 100))
                    lblDNAmtAdminFee.Text = (Math.Floor(100 * dnAmtAdminFee) / 100).ToString("N2")

                    lblDNPaymentdiscount.Text = ViewState("PaymentDiscount")

                    dnAmtPaymentDisc = CDec(dnAmtAdminFee * ViewState("PaymentDiscount") / 100)
                    lblDNAmtPaymentdiscount.Text = (Math.Floor(100 * dnAmtPaymentDisc) / 100).ToString("N2")

                    dnAmtAfterDisc = CDec(dnAmtAdminFee - dnAmtPaymentDisc)
                    lblDNAmtExVat.Text = (Math.Floor(100 * dnAmtAfterDisc) / 100).ToString("N2")

                    dnVat = CDec(dnAmtAfterDisc * (ViewState("VatRate") / 100))
                    lblDNVat.Text = (Math.Floor(100 * dnVat) / 100).ToString("N2")

                    dnTotalAmt = CDec(dnAmtAfterDisc + dnVat)
                    lblDNTotalAmt.Text = (Math.Floor(100 * dnTotalAmt) / 100).ToString("N2")
                End If

            End If

        Else
            divValidationMain.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' Handles Confirm button click on the confirmation screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnConf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnConf.Click
        Dim ds As DataSet
        ds = ws.WSFinance.MS_GenerateDebitNotes(ViewState("PI"), ViewState("DNAmount"), Session("UserId"))
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "openpopup", _
                          "Sys.Application.add_load(function() {OpenNewWindowInvoice('" & ViewState("PI") & "','" & ds.Tables(0).Rows(0)("DebitNo") & "','" & ApplicationSettings.OWUKBizDivId & "');location.href='DebitNotes.aspx';});", True)

        'Response.Redirect("DebitNotes.aspx")
    End Sub

    ''' <summary>
    ''' Handles click of cancel button click on confirmation screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnConfCan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnConfCan.Click
        Response.Redirect("DebitNotes.aspx")
    End Sub

    ''' <summary>
    ''' Handles Back button click on confirmation screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnBack.Click
        tblConfirmation.Visible = False
        tblConfirmationButtons.Visible = False
        tblConfirmationNew.Visible = False
        tblProceed.Visible = True
    End Sub

End Class