'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class SpecialistsForm

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''litRating control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litRating As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''divDispAllRatings control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divDispAllRatings As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblPosRating control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPosRating As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNeuRating control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNeuRating As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNegRating control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNegRating As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lnkAddLocationsSupp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkAddLocationsSupp As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''lnkCompProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkCompProfile As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''lnkloc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkloc As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''lnkuser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkuser As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''rgnReferences control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgnReferences As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lnkReferences control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkReferences As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''lnkAddProduct control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkAddProduct As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''lnkAddProductNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkAddProductNew As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''lnkAutoMatch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkAutoMatch As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''lnkViewBlackListClient control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkViewBlackListClient As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''lnkViewWorkingDays control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkViewWorkingDays As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''lnkViewAllInvoices control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkViewAllInvoices As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''lnkViewAccountHistory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkViewAccountHistory As Global.System.Web.UI.HtmlControls.HtmlAnchor
End Class
