
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FavouriteSupplierList.aspx.vb" Inherits="Admin.FavouriteSupplierList" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Favourite Suppliers"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<input type="hidden" id="hdnCompName" runat="Server" value="" />
<input type="hidden" id="hdnContactID" runat="Server" value="" />
<input type="hidden" id="hdnBillingLoc" runat="Server" value="" />
<input type="hidden" id="hdnBillingLocId" runat="Server" value="" />
<input type="hidden" id="LocType" runat="Server" value="" />

<script language="javascript" src="JS/json2.js"  type="text/javascript"></script>
<script language="javascript" src="JS/jquery.js"  type="text/javascript"></script>
<script language="javascript" src="JS/JQuery.jsonSuggestCustom.js"  type="text/javascript"></script>


<style>
#divMenu {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	line-height: 12px;
	font-weight: normal;
	text-decoration: none;
	width: 100%;
	background-position: right top;
	background-repeat: no-repeat;
	border-top-width: 1px;
	border-top-style: solid; 
	border-top-color: #CECECE;
	background-color: #993366;
}

.divContentTopRed{
	width: 100%;
	background-color: #993366;
	background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
	height: 5px;
	
}
.HideButton{
width:0;
height:0;
visibilty:hidden;
}
.bodytxtValidationMsg{
font-size:12px;
}
</style>
<script type="text/javascript">
function dontpost()
{
if (window.event.keyCode == 13) 
{
    event.returnValue=false; 
    event.cancel = true;
}
}
function maxlength() {
    var limitNum = 250;
    var length = (document.getElementById("ctl00_ContentPlaceHolder1_txtComments").value).length
    if (length > 250) {
        document.getElementById("ctl00_ContentPlaceHolder1_txtComments").value = (document.getElementById("ctl00_ContentPlaceHolder1_txtComments").value).substring(0, 250);
    }

}
function CheckIsNumeric(id) {
    if (document.getElementById(id).value != "") {
        if (IsNumeric(document.getElementById(id).value) == false) {
            document.getElementById(id).value = "";
            alert("Please enter positive integer value.");
        }
    }
}
function IsNumeric(sText) {

    var IsNumber
    var isInteger_re = /^[0-9]+[0-9]*$/;
    if (sText.match(isInteger_re)) {
        IsNumber = true;
    }
    else {
        IsNumber = false;

    }

    return IsNumber;
}
</script> 




	
	
	        
			
	        <div id="divContent" onkeydown='javascript:dontpost();'>
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35" style="margin-left:10px">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<input id="hdnPageNo"  value="1" type="hidden" name="hdnPageNo" runat="server">
 			<input id="hdnPageSize" value="25" type="hidden" name="hdnPageSize" runat="server">
 			<input id="hdnMode"  value="FavSP" type="hidden" name="hdnMode" runat="server">

			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
                <tr>
                  <td width="350">
                    <table>
                        <tr>
                           <td style="font-size:11px;" width="360" valign="top"><asp:Label ID="lblType" runat="server" Text="Supplier" ></asp:Label></td>
                        </tr>
                         <tr>
                           <td style="font-size:11px;" style="height:30px;" valign="top">
					        <div id="divAutoSuggest" runat="Server">        
                            <input type="text" id="txtContact" class="formFieldGrey215" style="width:320px;padding-top:3px;" onclick='javascript:getResultsFavSP(this.id)'  onkeyup='javascript:getResultsFavSP(this.id)' onkeydown='javascript:processKey' onblur='javascript:hideSearchResultFav()' runat="server"/><br /><div id="searchResult" class="searchResult" style="font-size:10px;">
                            </div>
                            </div>
                         </td>
                        </tr>
                         <tr>
                           <td style="font-size:11px;" width="360" valign="top">Billing Location</td>
                        </tr>
                        <tr>
                             <td style="font-size:11px;">
					        <div id="divAutoSuggest">        
					        <asp:DropDownList ID="ddlBillLoc" CssClass="formFieldGrey215" runat="server" Width="320"  DataTextField="Location" DataValueField="LocationId"></asp:DropDownList>   
                                <input visible="false" type="text" id="txtBillingLoc" class="formFieldGrey215" style="width:320px;padding-top:3px;" onclick='javascript:getResultsBillingLoc(this.id)' onkeyup='javascript:getResultsBillingLoc(this.id)' onkeydown='javascript:processKey' onblur='javascript:hideSearchResult()' runat="server"/><br />
                            <div id="searchResultLoc" class="searchResult" style="font-size:10px;"></div>                        
                            </div>
                         </td>
                        </tr>
                        <tr id="trFixedCapacity" runat="server" visible="false">
                            <td style="font-size:11px;" width="360" valign="top">
                                <table cellpadding="0" cellpadding="0">
                                     <tr>
                                       <td style="font-size:11px;" width="360" valign="top">Fixed Capacity</td>
                                    </tr>
                                    <tr>
                                       <td style="font-size:11px;"> 
                                            <asp:TextBox ID="txtFixedCapacity" runat="server" Width="320" onblur="javascript:CheckIsNumeric(this.id);" ></asp:TextBox> 
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="font-size:11px;" width="360" valign="top">Fixed Capacity Time Slot</td>
                                    </tr>
                                    <tr>
                                       <td style="font-size:11px;"> 
                                            <asp:DropDownList ID="drptxtFixedCapacityTimeSlot" runat="server" CssClass="formFieldGrey" Width="320" >
											<asp:ListItem Text="Select Time" Value="" Selected="true" ></asp:ListItem>
											<asp:ListItem Text="8am - 1pm" Value="8am - 1pm"></asp:ListItem>
											<asp:ListItem Text="1pm - 6pm" Value="1pm - 6pm"  ></asp:ListItem>											
                                            </asp:DropDownList>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td style="font-size:11px;" width="360" valign="top"><asp:CheckBox id="chkAllowJobAcceptance" runat="server" Text="allow Admin to accept jobs on suppliers behalf"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                       
                        <tr style="padding-top:20px;height:50px;">
                            <td>
                            <table>
                                <tr>
                                    <td id="tdAddSP" runat="server">
                                         <div id="divButton" style="width: 160px;float:left;margin-top:0px;cursor:pointer;">
						                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>								
						                    <a  onclick='javascript:validate()' class="buttonText" style="cursor:hand;;text-align:center" id="btnAdd"><strong>Add New Supplier</strong></a>												
						                    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                                            <asp:Button id="btnAddsupplier" runat="server" Text="" Width="0" Height="0" CssClass="HideButton" CausesValidation="false"/>
				                        </div>
                                        
                                    </td>
                                    <td style="font-size:11px;"></td>
                                    <td>
                                         <div id="divButton" style="width: 160px;float:left;margin-top:0px;cursor:pointer;">
						                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>								
						                    <a class="buttonText" style="cursor:hand;text-align:center" id="btnSearch" runat="server"><strong>Search <asp:Label ID="lblSearchType" runat="server" Text="Supplier"></asp:Label></strong></a>												
						                    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                                            
				                        </div>
                                    
                                    </td>
                                     <td style="font-size:11px;"></td>
                                    <td>
                                         <div id="divButton" style="width: 60px;float:left;margin-top:0px;cursor:pointer;">
						                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>								
						                    <a class="buttonText" style="cursor:hand;text-align:center" id="btnReset" runat="server"><strong>Reset</strong></a>												
						                    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                                            
				                        </div>
                                    
                                    </td>
                                </tr>
                            </table>
                         </td>
                        </tr>
                    </table>
                  </td>
                  <td valign="top" align="left" width="700">
                    <table>
                        <tr>
                           <td style="font-size:11px;" width="360" valign="top">Comments</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Rows="4" Width="250" onKeyUp="javascript:maxlength();" onKeyDown="javascript:maxlength();"></asp:TextBox> 
                            </td>
                        </tr>
                    </table>
                  </td>
                   <td valign="top" align="left">
                    <table>
                        <tr>
                           <td width="170" class="txtWelcome" align="right"><div id="divButton" style="width: 170px; float:right;">
						  <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
						  <a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" title="Back to Listing" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8"><strong>Back to Company Profile</strong></a>
						  <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
					     </div></td>   
                        </tr>                    
                    </table>
                  </td>
                </tr>
            </TABLE>	 
		
			<asp:UpdatePanel ID="UpdatePanel1" runat="server">
              <ContentTemplate>
			  
			  		  <asp:Panel runat="server" ID="PnlConfirm" visible="false">
			        <table width="100%" cellpadding="0" cellspacing="0" style="margin-top:100px; margin-left:100px; ">
			            <tr>
			                <td width="10px"></td>
			                <td><asp:Label ID="lblConfirm" Text="" runat="server"></asp:Label></td>
			            </tr>
			        </table>		
        			hh 
					<table width="300" cellpadding="0" cellspacing="0" border="0" style="margin-top:10px;margin-left:100px; ">
		                <tr>
		                    <td width="10px"></td>
		                    <td align="left">
		                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                    <asp:LinkButton ID="lnkConfirm" causesValidation=False runat="server" CssClass="txtButtonRed" Text="Confirm"></asp:LinkButton>
                                </div>
        			        </td>
			                <td align="left">
		                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                    <asp:LinkButton ID="lnkCancel" causesValidation=False runat="server" CssClass="txtButtonRed" Text="Cancel"></asp:LinkButton>
                                </div>
		                    </td>
		                </tr>
		            </table>	
		  </asp:Panel>
		  <asp:Panel runat="server" ID="pnlContent" Visible="true"><br />
              <asp:Label ID="lblMessage" runat="server" CssClass="bodytxtValidationMsg"></asp:Label>
			  <table width="100%" border="0" cellpadding="10" cellspacing="0">
			  <tr>
			    <td width="550" align="left" class="txtWelcome paddingL10" style="padding-left:0;"   id="tdAdd" runat="server"><asp:Label ID="lblBuyerCompany" runat="server" CssClass="HeadingRed"></asp:Label></td>
				  <td width="125"></td>
				  <td align="right" class="txtWelcome paddingL10" style="padding-right:0px;" id="tdRemove" runat="server"></td>
			  </tr>
			  </table>
			  <hr style="background-color:#993366;height:5px;margin:0px;" />
			  
			  
			
			   <asp:Panel ID="pnlListing" runat="server">    
					  <asp:GridView ID="gvFavSuppliers"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'
                         BorderColor="White"  dataKeyNames="ContactID" PagerSettings-Mode=NextPreviousFirstLast   PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting="true" PagerSettings-Visible="true">          
              				 <EmptyDataTemplate>
						   		<table  width="100%" border="0" cellpadding="10" cellspacing="0">					
									<tr>
										<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
										</td>
									</tr>				
								</table>
                  
               				 </EmptyDataTemplate>
							   <PagerTemplate>
								   
								  
								 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
										<tr>
											
											
										  
										  <td id="tdsetType2" align="left" width="121" runat="server">
											<div id="divButton" style="width: 160px;float:right;margin-top:0;">
						<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>								
						<asp:LinkButton OnClick="RemoveSupplier" CssClass="buttonText" style="cursor:hand;text-align:center" id="btnRemove"  runat="server"><strong>Remove Supplier</strong></asp:LinkButton>
					   <cc2:ConfirmButtonExtender ConfirmText="Do you want to remove the Supplier from list?" ID="ConfirmBtnRemove" TargetControlID="btnRemove" runat="server">
						</cc2:ConfirmButtonExtender> 
						
						<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
					</div>	
										  </td>
											<td>
												<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
												  <tr>
													<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
														<TR>
															<td align="right" valign="middle">
															  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
															</td>
															<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
															<table width="100%" border="0" cellpadding="0" cellspacing="0">
																<tr>
																<td align="right" width="36">
																<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																	<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
																  </div>	
																	<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																	<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
																	</div>																							   
																</td>
																<td width="50" align="center" style="margin-right:3px; ">													
																	<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
																	</asp:DropDownList>
																</td>
																<td width="30" valign="bottom">
																	<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																	 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
																	</div>
																	<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																	 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
																	</div>
																</td>
																</tr>
															</table>
															</td>
														</TR>
													</TABLE></td>
												  </tr>
												</table>
										  </td>
										</tr>
							   </table> 
								   
						</PagerTemplate> 
						   <Columns>  
										
						   <asp:TemplateField   HeaderText=" <input id='chkSelectAll' onClick=CheckAll(this,'Check') type='checkbox' name='chkSelectAll' />" HeaderStyle-CssClass="gridHdr">
						   <ItemStyle Wrap="true" HorizontalAlign="Left" Width="10"  CssClass="gridRow"/>
						   <ItemTemplate>             
						   <asp:CheckBox ID="Check" runat="server"></asp:CheckBox>
							<input type="hidden" runat="server" id="hdnMainContactId"  value='<%#Container.DataItem("CompanyID")%>'/>
							<input type="hidden" runat="server" id="hdnBillLocId"  value='<%#Container.DataItem("BillingLocId")%>'/>
						   </ItemTemplate>
						   </asp:TemplateField>                
														
							<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="20%" HeaderStyle-CssClass="gridHdr" DataField="CompanyName" SortExpression="CompanyName" HeaderText="Supplier" />                
							<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="8%" HeaderStyle-CssClass="gridHdr" DataField="BillingLocation" SortExpression="BillingLocation" HeaderText="Billing Location" />                
                            <%--<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr" DataField="FixedCapacity" SortExpression="FixedCapacity" HeaderText="Fixed Capacity" />--%>                
                            <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="25%" HeaderStyle-CssClass="gridHdr" DataField="FixedCapacityTimeSlot" SortExpression="FixedCapacityTimeSlot" HeaderText="Time Slot (Fixed Capacity)" />
                             <asp:TemplateField HeaderText="Auto Accept" HeaderStyle-CssClass= "gridHdr" SortExpression="AllowJobAcceptance"  >         
                              <HeaderStyle  CssClass="gridHdr"  />
                                <ItemStyle Wrap="true" HorizontalAlign="Left" CssClass="gridRow" Width="5%"/>
                                <ItemTemplate>             
                                    <asp:Label ID="lblAllowJobAcceptance" runat="server" Text='<%# IIF(Container.DataItem("AllowJobAcceptance"), True,False)%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>    
                            <%--<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr" DataField="DateCreated" SortExpression="DateCreated" HeaderText="Date Created" />--%>
                            <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%" HeaderStyle-CssClass="gridHdr" DataField="ActionBy" SortExpression="ActionBy" HeaderText="Action By" />                
                            <asp:BoundField ItemStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr" DataField="Comments" SortExpression="Comments" HeaderText="Comments" />                
						    </Columns>
					<AlternatingRowStyle CssClass="gridRow" />
					<RowStyle CssClass="gridRow" />            
					<PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
					<HeaderStyle  CssClass="gridHdr" HorizontalAlign="Left" VerticalAlign="Top"  Height="40px" />
					<FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
			
				  </asp:GridView>
			  </asp:Panel>           		  
 			</asp:Panel>
            </ContentTemplate>
            </asp:UpdatePanel>
            
                <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            		<div class="gridText">
               			 <img  align=middle src="Images/indicator.gif" />
               			 <b>Fetching Data... Please Wait</b>
            		</div>   
        			</ProgressTemplate>
					
                </asp:UpdateProgress>
                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
				
             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.FavouriteSupplierList" SortParameterName="sortExpression">
            </asp:ObjectDataSource>

			  <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>

</asp:Content>