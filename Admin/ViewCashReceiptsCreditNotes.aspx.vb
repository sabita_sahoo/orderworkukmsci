
Partial Public Class ViewCashReceiptsCreditNotes
    Inherits System.Web.UI.Page

    Protected WithEvents UCDateRange1 As UCDateRange
    Protected WithEvents UCSearchContact1 As UCSearchContact

    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            ViewState!SortExpression = "InvoiceNo"
            gvCRCN.Sort(ViewState!SortExpression, SortDirection.Descending)
            lnkBackToListing.HRef = getBackToListingLink()

        End If
    End Sub
    Public Sub ProcessBackToListing()
        ViewState!SortExpression = Request("SC")
        gvCRCN.Sort(ViewState!SortExpression, Request("SO"))
        PopulateGrid()
        gvCRCN.PageIndex = Request("PN")
    End Sub
    Public Sub PopulateGrid()
        gvCRCN.DataBind()
    End Sub
    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then
            PopulateGrid()
        End If
    End Sub
    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvCRCN.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvCRCN.DataBind()
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                       ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        Dim bizDivId As Integer = Session("BizDivId")
        Dim SINumber As String = Request("InvoiceNo")
        ds = ws.WSFinance.MS_GetCRCNListing(bizDivId, SINumber, sortExpression, startRowIndex, maximumRows)
        ViewState("rowCount") = ds.Tables(1).Rows(0)(0)
        Return ds
    End Function

    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function
    Public Function GetLinks(ByVal invoiceNo As String, ByVal InvoiceType As String, ByVal companyId As String) As String
        Dim link As String = ""
        Dim linkParams As String = ""

        linkParams &= "invoiceNo=" & invoiceNo
        linkParams &= "&companyId=" & companyId
        linkParams &= "&bizDivId=" & Session("BizDivId")
        linkParams &= "&FromDate=" & ViewState("fromDate")
        linkParams &= "&ToDate=" & ViewState("toDate")
        linkParams &= "&PS=" & gvCRCN.PageSize
        linkParams &= "&PN=" & gvCRCN.PageIndex
        linkParams &= "&SC=" & gvCRCN.SortExpression
        linkParams &= "&SO=" & gvCRCN.SortDirection
        linkParams &= "&sender=SalesInvoicePaid"
        If InvoiceType = "Receipt" Or InvoiceType = "UA Receipt" Then
            link &= "<a target='_blank' href='ViewCashReceipt.aspx?ReceiptType=" & InvoiceType & "&" & linkParams & "'><img src='Images/Icons/View-Cash-Receipt.gif' alt='View Receipt' width='15' height='14' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        Else
            link &= "<a target='_blank' href='ViewCreditNote.aspx?" & linkParams & "'><img src='Images/Icons/View-All-WO-SI.gif' alt='View Credit Note' width='15' height='14' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        End If
        Return link
    End Function
    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvCRCN.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvCRCN, e.Row, Me)
        End If
    End Sub
    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex



    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvCRCN.RowDataBound
        MakeGridViewHeaderClickable(gvCRCN, e.Row)
    End Sub
    Public Function getBackToListingLink() As String
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            Select Case Request("sender")                               
                Case "SalesInvoicePaid"
                    link = "~\SalesInvoicePaid.aspx?"
                    link &= "&invoiceNo=" & Request("invoiceNo")
                    link &= "&bizDivId=" & Request("bizDivId")
                    link &= "&companyId=" & Request("companyId")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=SalesInvoicePaid"
                Case "Invoices"
                    link = "~\Invoices.aspx?"
                    link &= "&invoiceNo=" & Request("invoiceNo")
                    link &= "&bizDivId=" & Request("bizDivId")
                    link &= "&companyId=" & Request("companyId")
                    link &= "&FromDate=" & Request("FromDate")
                    link &= "&ToDate=" & Request("ToDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&tab=" & Request("tab")
                    link &= "&sender=Invoices"
                Case "SearchFinance"
                    link = "~\SearchFinance.aspx?"
                    link &= "SrcPONumber=" & Request("txtPONumber")
                    link &= "&SrcInvoiceNo=" & Request("txtInvoiceNo")
                    link &= "&SrcKeyword=" & Request("txtKeyword")
                    link &= "&SrcWorkOrderId=" & Request("txtWorkorderID")
                    link &= "&SrcCompanyName=" & Request("txtCompanyName")
                    link &= "&SrcInvoiceDate=" & Request("txtInvoiceDate")
                    link &= "&PS=" & Request("PS")
                    link &= "&PN=" & Request("PN")
                    link &= "&SC=" & Request("SC")
                    link &= "&SO=" & Request("SO")
                    link &= "&sender=CashReceiptsCreditNotes"
                Case Else
                    Return ""
            End Select
        End If
        Return link
    End Function

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
End Class