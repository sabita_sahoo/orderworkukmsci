<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" EnableViewState="true"
    CodeBehind="AT800RolloutAuthorise.aspx.vb" Inherits="Admin.AT800RolloutAuthorise" Title="At800 Rollout Authorise" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="min-width: 1000px; width: 90%; padding: 20px; margin: 0px auto;text-align:center;">
        <div style="margin: auto; text-align: center; font-size: small; font-family: Arial; font-weight: bold;">
            Make AT800 Rollout Available
        </div>
        <br style="clear: both;" />
        <asp:DropDownList ID="ddlRolloutDate" runat="server" style="margin:0px auto;" />
        <br style="clear:both;"/><br />
        <asp:Button ID="btnAuthorise" runat="server" Text="Authorise" style="margin:0px auto;" />
    </div>
</asp:Content>
