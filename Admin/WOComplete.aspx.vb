Public Partial Class WOComplete
    Inherits System.Web.UI.Page

    '''<summary>
    '''UCWOCloseComplete1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UCWOCloseComplete1 As UCMSWOCloseComplete

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        UCWOCloseComplete1.ParentName = "WOComplete"
        UCWOCloseComplete1.CompanyID = Session("CompanyID")
        UCWOCloseComplete1.UserID = Session("UserID")

        UCWOCloseComplete1.Viewer = ApplicationSettings.ViewerAdmin
        UCWOCloseComplete1.ClassID = ApplicationSettings.RoleOWID
        UCWOCloseComplete1.BizDivID = Session("BizDivId")

        If Not IsNothing(Request("SupCompId")) Then
            If Request("SupCompId") <> 0 Then
                UCWOCloseComplete1.SupCompanyID = Trim(Request("SupCompId"))
            Else
                UCWOCloseComplete1.SupCompanyID = Nothing
            End If
        Else
            UCWOCloseComplete1.SupCompanyID = Nothing
        End If
    End Sub

End Class