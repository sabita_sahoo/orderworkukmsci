﻿Imports System.IO


Public Class ServiceWOReport
    Inherits System.Web.UI.Page

    Public Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            'Populating Status dropdown
            Dim dsStatus As DataSet
            Dim dvStatus As DataView
            dsStatus = CommonFunctions.GetWoStandards(Page, "Status")
            If dsStatus.Tables(0).Rows.Count > 0 Then
                dvStatus = dsStatus.Tables(0).Copy.DefaultView
                dvStatus.Sort = "StandardValue"
                drpStatus.DataSource = dvStatus
                drpStatus.DataTextField = "StandardValue"
                drpStatus.DataValueField = "StandardValue"
                drpStatus.DataBind()

                Dim liStatus As New ListItem
                'liStatus = New ListItem
                liStatus.Text = "Select Status"
                liStatus.Value = ""
                drpStatus.Items.Insert(0, liStatus)
            End If
            'Poonam modified on 9/11/2015 - Task -OA:91 - Professional wo report not giving correct data wrt to Status and Submitted Date range
            Session("dsSelectedCompany") = Nothing
        End If
    End Sub

    Private Sub lnkBtnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnGenerateReport.Click
        generateReport()
    End Sub

    Private Sub generateReport()

        Dim DtFrom, DtTo, DtStart, DtEnd, SelPO, SelStatus, SelCName As String
        DtFrom = txtSubmittedDateFrom.Text.ToString.Trim
        DtTo = txtSubmittedDateTo.Text.ToString.Trim
        DtStart = txtStartDate.Text.ToString.Trim
        DtEnd = txtEndDate.Text.ToString.Trim
        SelPO = txtPONumber.Text.ToString.Trim
        SelStatus = drpStatus.Items(drpStatus.SelectedIndex).Value.ToString()
        SelCName = txtCompanyName.Text.ToString.Trim

        Dim ds As New DataSet
        ds = ws.WSStandards.GetServiceWoReport(DtFrom, DtTo, DtStart, DtEnd, SelPO, SelStatus, SelCName)
        Session("ServiceWoReport") = ds

        If ds.Tables(0).Rows.Count > 0 Then
            rptShowRecords.DataSource = ds.Tables(0)
            rptShowRecords.DataBind()
            pnlShowRecords.Visible = True

            lnkBtnExportToExcel.Visible = True
        Else
            pnlShowRecords.Visible = False
            pnlNoRecords.Visible = True
        End If


    End Sub


    Private Sub lnkBtnExportToExcel_Click(sender As Object, e As EventArgs) Handles lnkBtnExportToExcel.Click
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=RepeaterExport.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        Dim tb As New Table()
        Dim tr1 As New TableRow()
        Dim cell1 As New TableCell()
        cell1.Controls.Add(pnlShowRecords)
        tr1.Cells.Add(cell1)
        Dim cell2 As New TableCell()
        cell2.Text = "&nbsp;"
        Dim tr2 As New TableRow()
        tr2.Cells.Add(cell2)
        tb.Rows.Add(tr1)
        tb.Rows.Add(tr2)
        tb.RenderControl(hw)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub
End Class