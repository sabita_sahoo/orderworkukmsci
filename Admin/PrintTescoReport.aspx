<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintTescoReport.aspx.vb" Inherits="Admin.PrintTescoReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Print Tesco Tech Support</title>
    <link href="Styles/OrderWork.css" rel="stylesheet" type="text/css">
    <style type="text/css">
#divMenu {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	line-height: 12px;
	font-weight: normal;
	text-decoration: none;
	width: 100%;
	background-position: right top;
	background-repeat: no-repeat;
	border-top-width: 1px;
	border-top-style: solid; 
	border-top-color: #CECECE;
	background-color: #993366;
}

.divContentTopRed{
	width: 100%;
	background-color: #993366;
	background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
	height: 5px;
	
}
.dlIssue{
width:100%;
}
.gridRow{
paddin-right:0px;
}
.clsBorderTbl{
 border-left-width:0px;
 border-right: solid 1px #ffffff;
 padding:4px 0px 0px 6px;
 margin:0px;
 font-family:Arial;font-size:11px;
 }
 .clsBorderTblDetail{
 border-left-width:0px;
 border-right: solid 1px #ffffff;
 padding:2px 0px 0px 2px;
 margin:0px;
 }
 .gridRow{
 padding:0px;
 }
</style>
</head>
<body bgcolor="#F4F5F0" onload="javascript:this.print()">
    <form id="form1" runat="server">
    <div style="margin-left:auto; margin-right:auto; background-color:#ffffff; width:950px; height:100%;">
    <asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
			<table height="500" width="700" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FEFFD3">
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" align="center">
						  <tr>
							<td>&nbsp;</td>
							<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#CC3300; font-weight:bold;"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
						  </tr>
						</table>
					</td>
				</tr>
			</table>	
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlPrintContent" Visible="true" BackColor="#FFFFFF">
		
		    <asp:datalist ID="dlTescoReport" runat="server" ForeColor="#555354"   Width="950px" >
                <HeaderTemplate>
	                <table width="950px"  cellspacing="0" cellpadding="0" class="gridHdr gridText" style="color:#000; text-align:left;margin-left:0px;margin-top:0px;">
		                 <tr id="tbldlIssueWOListingHdr" align="left">  
                              <td width="120" class="clsBorderTbl">Store</td>
                              <td width="120" class="clsBorderTbl">Service</td>
                              
                              <td width="80" class="clsBorderTbl">Engineer Availability</td>
                              <td width="80" class="clsBorderTbl">Engineer "On time"</td>
                              <td width="90" class="clsBorderTbl">Average Job Time</td>
                              <td width="100" class="clsBorderTbl">No. of Suppliers used</td>
                              <td width="80" class="clsBorderTbl">Jobs received</td>
                              <td width="80" class="clsBorderTbl">Jobs Invoiced</td>
                              <td width="60" class="clsBorderTbl">Jobs in SLA</td>
                              <td width="60" class="clsBorderTbl">Jobs Active</td>
                              <td width="40" class="clsBorderTbl">Total Jobs</td>
                              
                            </tr>
	                </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="950px" cellspacing="0" cellpadding="0">
						<tr id="tblDLRow" align="left" >                 
						      <td width="115" class="clsBorderTbl"><%#Container.DataItem("StoreName")%></td>
						       <td width="120" class="clsBorderTbl"><input id="hdnGoodsLocation" type="hidden" name="hdnGoodsLocation" runat="server" value='<%#Container.DataItem("GoodsLocation")%>'/></td>
                              
                              <td width="80" class="clsBorderTbl">&nbsp;</td>
                              <td width="80" class="clsBorderTbl">&nbsp;</td>
                              <td width="90" class="clsBorderTbl">&nbsp;</td>
                              <td width="100" class="clsBorderTbl">&nbsp;</td>
                              <td width="80" class="clsBorderTbl">&nbsp;</td>
                              <td width="80" class="clsBorderTbl">&nbsp;</td>
                              <td width="60" class="clsBorderTbl">&nbsp;</td>
                              <td width="60" class="clsBorderTbl">&nbsp;</td>
						      <td width="40" class="clsBorderTbl" ><%#Container.DataItem("TotalJobs")%></td>
						</tr>
						</table>
						<asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="dlJobsBreakUp" Runat="server" Width="950px" >
						    <ItemTemplate>
						        <table width="950px" cellspacing="0" cellpadding="0">
						            <tr id="tblDLRow" align="left">                 
						                   <td width="117" class="clsBorderTbl" style="background-color:#efebe7;" >&nbsp;</td>
						                   <td width="116" class="clsBorderTbl" style="background-color:#efebe7;" ><%#Container.DataItem("Service")%></td>
                                        
                                          <td width="80" class="clsBorderTbl" style="background-color:#efebe7;" ><%#Container.DataItem("EngrAvail")%></td>
                                          <td width="80" class="clsBorderTbl" style="background-color:#efebe7;" ><%#Container.DataItem("EngrOnTime")%></td>
                                          <td width="90" class="clsBorderTbl" style="background-color:#efebe7;" ><%#Container.DataItem("AvgJobTime")%></td>
                                          <td width="100" class="clsBorderTbl" style="background-color:#efebe7;" ><%#Container.DataItem("SuppliersUsed")%></td>
                                          <td width="80" class="clsBorderTbl" style="background-color:#efebe7;" ><%#Container.DataItem("JobsReceived")%></td>
                                          <td width="80" class="clsBorderTbl" style="background-color:#efebe7;" ><%#Container.DataItem("JobsInvoiced")%></td>
                                          <td width="60" class="clsBorderTbl" style="background-color:#efebe7;" ><%#Container.DataItem("JobsSLA")%></td>
                                          <td width="60" class="clsBorderTbl" style="background-color:#efebe7;" ><%#Container.DataItem("JobsActive")%></td>
						                  <td width="40" class="clsBorderTbl" style="background-color:#efebe7;" >&nbsp;</td>
						            </tr>
						       </table>
						    </ItemTemplate>
						</asp:DataList>
						
						
                </ItemTemplate>
        </asp:datalist>
       <table width="950px" cellspacing="0" cellpadding="0" id="tblTotal" runat="server" style="margin-left:0px; margin-top:-8px;">
						            <tr id="tblDLRow" align="left">                 
						                   <td width="137" class="clsBorderTbl" style="word-wrap:break-word ;" ><b>Total</b></td>
						                   <td width="136" class="clsBorderTbl" style="word-wrap:break-word ;">&nbsp;</td>
                                          
                                          <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalEngAvail" style="font-weight:bold;"></asp:Label></td>
                                          <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalEngrOnTime" style="font-weight:bold;"></asp:Label></td>
                                          <td width="90" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalAvgJobTime" style="font-weight:bold;"></asp:Label></td>
                                          <td width="100" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalSuppliersUsed" style="font-weight:bold;"></asp:Label></td>
                                          <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalJobsReceived" style="font-weight:bold;"></asp:Label></td>
                                          <td width="80" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalJobsInvoiced" style="font-weight:bold;"></asp:Label></td>
                                          <td width="60" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalJobsSLA" style="font-weight:bold;"></asp:Label></td>
                                          <td width="60" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="TotalJobsActive" style="font-weight:bold;"></asp:Label></td>
						                  <td width="40" class="clsBorderTbl" style="word-wrap:break-word ;"><asp:Label runat="server" ID="lblTotalJobs" style="font-weight:bold;"></asp:Label></td>
						            </tr>
						       </table>
    </asp:Panel>
    </div>
    </form>
</body>
</html>
