﻿var xmlDoc;
var OldWP = 0.0;
var OldPP = 0.0;
function loadXMLDoc(dname) 
{
if (window.XMLHttpRequest)
  {
  xmlDoc=new window.XMLHttpRequest();
  xmlDoc.open("GET",dname,false);
  xmlDoc.send("");
  return xmlDoc.responseXML;
  }
// IE 5 and IE 6
else if (ActiveXObject("Microsoft.XMLDOM"))
  {
  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
  xmlDoc.async=false;
  xmlDoc.load(dname);
  return xmlDoc;
  }
alert("Error loading document");
return null;
}
// REquired for image roll over on pageload
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

//Script required for the welcome page
function showHideAnswer(objID)
{
 
	if(document.getElementById("A"+objID).className=="faqAns")
	{
		document.getElementById("A"+objID).className="faqAnsShow";
		document.getElementById("I"+objID).src = "../Images/Btn-Minus.gif";
	}
	else
	{
		document.getElementById("A"+objID).className="faqAns";
		document.getElementById("I"+objID).src = "../Images/Btn-Plus.gif";
	}
}

function showPasswordFld(defaultText)
{	
     if(document.getElementById("UCLogin1_txtLoginPassword"))
	if(document.getElementById("UCLogin1_txtLoginPassword").value == "")
	{
		document.getElementById("txtLoginPasswordText").className = "passwordFieldHidden";
		document.getElementById("txtLoginPasswordText").value = defaultText; 
		document.getElementById("UCLogin1_txtLoginPassword").className = "passwordField";		
		document.getElementById("UCLogin1_txtLoginPassword").focus();
	}
}
function hidePasswordFld(defaultText)
{
	if(document.getElementById("UCLogin1_txtLoginPassword"))
	if(document.getElementById("UCLogin1_txtLoginPassword").value == "")
	{
		document.getElementById("txtLoginPasswordText").className = "passwordField";
		document.getElementById("txtLoginPasswordText").value = defaultText;
		document.getElementById("UCLogin1_txtLoginPassword").className = "passwordFieldHidden";
	}
}


function showHideAnswerHIWNew(objID)
{

	if(document.getElementById("A"+objID).className=="faqAnsHIW")
	{		
		document.getElementById("A"+objID).className="faqAnsShowHIW";
		document.getElementById("Q"+objID).className="QTabSelected";
		//document.getElementById("I"+objID).src = "Images/Btn-Minus.gif";
	}
	else
	{
		document.getElementById("A"+objID).className="faqAnsHIW";
		document.getElementById("Q"+objID).className="QTab";
		//document.getElementById("I"+objID).src = "Images/Btn-Plus.gif";
	}
}

function showHideAnswerHIW(objID)
{

	if(document.getElementById("A"+objID).className=="faqAnsHIW")
	{		
		document.getElementById("A"+objID).className="faqAnsShowHIW";
		document.getElementById("Q"+objID).className="QTabSelected";
		//document.getElementById("I"+objID).src = "Images/Btn-Minus.gif";
	}
	else
	{
		document.getElementById("A"+objID).className="faqAnsHIW";
		document.getElementById("Q"+objID).className="QTab";
		//document.getElementById("I"+objID).src = "Images/Btn-Plus.gif";
	}
	for(i=1; i<=4; i++)
	{
	    if(i != objID)
	    {
	        document.getElementById("A" + i).className="faqAnsHIW";
	        //document.getElementById("I" + i).src = "Images/Btn-Plus.gif";
	        //document.getElementById("C" + i).className="ItIssueHighlighted";	
	        document.getElementById("Q"+i).className="QTab";
	    }
	}
}

function showAnswerHIW(objID)
{
	if(document.getElementById("A"+objID).className=="faqAnsHIW")
	{
		
		document.getElementById("A"+objID).className="faqAnsShowHIW";
	//	document.getElementById("I"+objID).src = "Images/Btn-Minus.gif";
	}
	
}

function showWindow()
{
	window.open ('../Impressum.htm', 'newWin', 'scrollbars=no,status=yes,width=455,height=420')
}
function showWindowOuter()
{
	window.open ('Impressum.htm', 'newWin', 'scrollbars=no,status=yes,width=455,height=420')
}

function showDiv(iframe)
{   
	//document.getElementById(iframe).style.visibility = "visible";	
	 document.getElementById(iframe).style.visibility = "visible";   
     document.getElementById(iframe).style.position = "absolute";
	 document.getElementById(iframe).style.height = "225px";
	 document.getElementById(iframe).style.width = "400px";
	 document.getElementById(iframe).style.zIndex = "9999"; 
	// document.getElementById(iframe).style.overflow = "visible";
}
	
function hideDiv(iframe)
{		
	//document.getElementById(iframe).style.visibility = "hidden";
	document.getElementById(iframe).style.visibility = "hidden";
	document.getElementById(iframe).style.position = "absolute";
	document.getElementById(iframe).style.overflow = "hidden";
	document.getElementById(iframe).style.width = "0px";				
	document.getElementById(iframe).style.height = "0px";
}


//Script for ratings
function showHideRatings(idDiv,Type)
{ //	document.getElementById('divMainRating').className = 'backGroundRatingOnclick formTxtRedBold2'
 if(document.getElementById(idDiv))
	 { 
	        //hide Div
			if(document.getElementById(idDiv).style.visibility == "visible")
			 { 
			    if(Type == 'Company')
			    {    
				    document.getElementById('divMainRating').className = 'backGroundRating formTxtRedBold2'
				}   
				else if(Type == 'Contact')
				{
				    document.getElementById('divMainRatingContact').className = 'backGroundRating formTxtRedBold2'
				}
			 
				document.getElementById(idDiv).style.visibility = "hidden";
				document.getElementById(idDiv).style.position = "absolute";
				document.getElementById(idDiv).style.overflow = "hidden";
				document.getElementById(idDiv).style.width = "0px";
				document.getElementById(idDiv).style.height = "0px";
			 }
			 else
			{  
			//show Div
			    if(Type == 'Company')
			    {    
				    document.getElementById('divMainRating').className = 'backGroundRatingOnclick formTxtRedBold2'
				}    
				else if(Type == 'Contact')
				{
				    document.getElementById('divMainRatingContact').className = 'backGroundRatingOnclick formTxtRedBold2'
				}
				document.getElementById(idDiv).style.visibility = "visible";
				document.getElementById(idDiv).style.position = "relative";
				document.getElementById(idDiv).style.height = "100%";
				document.getElementById(idDiv).style.width = "100%";
				document.getElementById(idDiv).style.overflow = "visible"; 
			}

	}
}


function resetSearch()
{
   	document.getElementById("UCSampleSupplierlisting1_ddlAOE").selectedIndex = 0;
	document.getElementById("UCSampleSupplierlisting1_ddlRegion").selectedIndex = 0;
	document.getElementById("rdoNewSearch").checked = false;
}


function resetSearchDetails()
{
   	document.getElementById("UCSampleSupplierDetailsListing1_ddlAOE").selectedIndex = 0;
	document.getElementById("UCSampleSupplierDetailsListing1_ddlRegion").selectedIndex = 0;
	document.getElementById("rdoNewSearch").checked = false;
}


function getTrimmedLabelID(objID)
{
	var strID = new String(objID);
	var strLabelId = new String(strID.substr(0,strID.lastIndexOf("_") + 1));
	return strLabelId;
}


function proposedPrice(obj)
{ 
    var ucName = getTrimmedLabelID(obj);
    
	if(document.getElementById(ucName + "rdoNon0Value").checked == true)
	{
		document.getElementById(ucName + "txtSpendLimit").disabled = false
		if (document.getElementById(ucName + "tdReview") != null)
    	{
			document.getElementById(ucName + "tdReview").style.visibility = 'visible'
			document.getElementById(ucName + "tdChkReview").style.visibility = 'visible'							
		}
	}
	else
	{
		document.getElementById(ucName + "txtSpendLimit").value=0
		document.getElementById(ucName + "txtSpendLimit").disabled = true
		if (document.getElementById(ucName + "tdReview") != null)
		{
			document.getElementById(ucName + "tdReview").style.visibility = 'hidden'
			document.getElementById(ucName + "tdChkReview").style.visibility = 'hidden'		
			document.getElementById(ucName + "chkReviewBid").checked = false			
		}
	}
}


function clearExpiryDate(obj)
{
		if(obj.value == "Enter expiry date")	
			obj.value=""
	
}
function fillExpiryDate(obj)
{ 
		if(obj.value=="")		
		obj.value="Enter expiry date"	
			
}
function clearDateOfIssue(obj)
{
		if(obj.value == "Date of issue")	
			obj.value=""
	
}
function fillDateOfIssue(obj)
{ 
		if(obj.value=="")		
		obj.value="Date of issue"	
			
}

function clearCoverAmount(obj)
{   
		if(obj.value == "Enter cover amount")	
			obj.value=""
	
}
function fillCoverAmount(obj)
{   
		if(obj.value=="")		
		obj.value="Enter cover amount"	
			
}



function CheckRadioSpecialists(CheckedOne)
	{
		var count = 02;
		var check;
		//=$get("rdoValidSpecialist");
		//alert(check)
			for(count = 02; count < document.getElementById("UCWOProcess1_DGSpecialists").rows.length + 1; count ++) 
			{
			    if(count < 10)
				    check = "UCWOProcess1_DGSpecialists_ctl0" + count + "_rdoValidSpecialist";
				else
				    check = "UCWOProcess1_DGSpecialists_ctl" + count + "_rdoValidSpecialist";
				if(document.getElementById(check))
					document.getElementById(check).checked = false;
				if(check == CheckedOne.id)
				 {
					document.getElementById(check).checked = true;
				 }
			}
}



function populateAttachedFiles(objId)
{
    //alert(objId)
    document.getElementById("ctl00_ContentPlaceHolder1_" + objId + "_btnJavaClickToPopulateAttachFile").click();
	//document.getElementById("UCCompanyProfile1_UCFileUpload1_btnJavaClickToPopulateAttachFile").click();
}

function CollapseComments(WOTrackingID, objID)
{ 
	var strTrimValue = getTrimmedLabelID(objID);
	var strImgExpand = "imgExp" + WOTrackingID;	
	var strImgCollapse = "imgCol" + WOTrackingID;	
	if (document.getElementById(strTrimValue).style.visibility == "hidden")	
	{
	    document.getElementById(strImgExpand).style.visibility= "hidden";
	    document.getElementById(strImgExpand).style.position = "absolute";
		document.getElementById(strImgCollapse).style.visibility= "visible";
		document.getElementById(strImgCollapse).style.position = "static";
		
		document.getElementById(strTrimValue).style.visibility = "visible";
		document.getElementById(strTrimValue).style.position = "static";
		document.getElementById(strTrimValue).style.width = "100%";				
	}	
	else if(document.getElementById(strTrimValue).style.visibility == "visible")
	{
	    document.getElementById(strImgExpand).style.visibility= "visible";
		document.getElementById(strImgExpand).style.position = "static";
		document.getElementById(strImgCollapse).style.visibility= "hidden";
		document.getElementById(strImgCollapse).style.position = "absolute";
		
		document.getElementById(strTrimValue).style.visibility = "hidden";
		document.getElementById(strTrimValue).style.position = "absolute";
		document.getElementById(strTrimValue).style.width = "0";		
	}
}
function CollapseIssueDetails(WOTrackingID, objID)
{ 
	var strTrimValue = getTrimmedLabelIDForIssue(objID);
	var strImgExpand = "imgExp" + WOTrackingID;	
	var strImgCollapse = "imgCol" + WOTrackingID;	
	if (document.getElementById(strTrimValue).style.visibility == "hidden")	
	{
	    document.getElementById(strImgExpand).style.visibility= "hidden";
	    document.getElementById(strImgExpand).style.position = "absolute";
		document.getElementById(strImgCollapse).style.visibility= "visible";
		document.getElementById(strImgCollapse).style.position = "static";
		
		document.getElementById(strTrimValue).style.visibility = "visible";
		document.getElementById(strTrimValue).style.position = "static";
		document.getElementById(strTrimValue).style.width = "100%";				
	}	
	else if(document.getElementById(strTrimValue).style.visibility == "visible")
	{
	    document.getElementById(strImgExpand).style.visibility= "visible";
		document.getElementById(strImgExpand).style.position = "static";
		document.getElementById(strImgCollapse).style.visibility= "hidden";
		document.getElementById(strImgCollapse).style.position = "absolute";
		
		document.getElementById(strTrimValue).style.visibility = "hidden";
		document.getElementById(strTrimValue).style.position = "absolute";
		document.getElementById(strTrimValue).style.width = "0";		
	}
}

function changeImageOnMouseAction(imgID, newImagePath)
{
    document.getElementById(imgID).src = newImagePath;
}

//Added Function By Pankaj Malav to show and hide Details of WorkOrderID in SIGeneration
function SIWODetails(objId)
{   
    
    if ( document.getElementById(objId.id + 'details').style.visibility == "hidden" )
    {
        document.getElementById(objId.id + 'details').style.visibility = "visible";
	    document.getElementById(objId.id + 'details').style.position = "static";
	    document.getElementById(objId.id + 'imgExp').style.visibility = "hidden";
	    document.getElementById(objId.id + 'imgExp').style.position = "absolute";
	    document.getElementById(objId.id + 'imgCol').style.visibility = "visible";
	    document.getElementById(objId.id + 'imgCol').style.position = "relative";
	}
	else
	{
	    document.getElementById(objId.id + 'details').style.visibility = "hidden";
	    document.getElementById(objId.id + 'details').style.position = "absolute";
	    document.getElementById(objId.id + 'imgExp').style.visibility = "visible";
	    document.getElementById(objId.id + 'imgExp').style.position = "relative";
	    document.getElementById(objId.id + 'imgCol').style.visibility = "hidden";
	    document.getElementById(objId.id + 'imgCol').style.position = "absolute";   
	    
	}
}

function getTrimmedLabelID(objID)
{
	
	var strID = new String(objID);
	var strLabelId = new String(strID.substr(0,strID.lastIndexOf("_")));
	strLabelId  = strLabelId.concat("_divCommentsDetail"); 
	return strLabelId;
}
function getTrimmedLabelIDForIssue(objID)
{
	
	var strID = new String(objID);
	var strLabelId = new String(strID.substr(0,strID.lastIndexOf("_")));
	strLabelId  = strLabelId.concat("_divIssueDetail"); 
	return strLabelId;
}

function CheckAll(me,checkBoxId)
{

    var index = me.name.indexOf('_');  
    var prefix = me.name.substr(0,index); 

    // Looks for the right checkbox
    for(i=0; i<document.forms[0].length; i++) 
    { 
        var o = document.forms[0][i]; 
        var str =new String();
        str=o.id;       
        if (o.type == 'checkbox' && (me.name != o.name) && (o.name.substring(0, prefix.length) == prefix) ) 
        {       
            if (str.indexOf(checkBoxId)!=-1) 
            {    
                o.checked = me.checked;                 
                //GetSelectedIds(o.id, selectId,strUC);                                        
            }
        } 
    }   
}
function CheckAllReceivables(me,checkBoxId,Type)
{
     document.getElementById("ctl00_ContentPlaceHolder1_lblCalculatedTotal").innerHTML = "0.00";
     document.getElementById("ctl00_ContentPlaceHolder1_lblCreditAndCashTotal").innerHTML = "0.00";
     if (Type == "Invoices") {
        document.getElementById("ctl00_ContentPlaceHolder1_lblInvoicesTotal").innerHTML = "0.00";
     }
     else if (Type == "UnpaidCN") {
        document.getElementById("ctl00_ContentPlaceHolder1_lblUnpaidCNTotal").innerHTML = "0.00";
     }
     else if (Type == "UnallocatedCR") {
        document.getElementById("ctl00_ContentPlaceHolder1_lblUnallocatedCRTotal").innerHTML = "0.00";
     }  
    
    var index = me.name.indexOf('_');  
    var prefix = me.name.substr(0,index); 

    // Looks for the right checkbox
    for(i=0; i<document.forms[0].length; i++) 
    { 
        var o = document.forms[0][i]; 
        var str =new String();
        str=o.id;       
        if (o.type == 'checkbox' && (me.name != o.name) && (o.name.substring(0, prefix.length) == prefix) ) 
        {       
            if (str.indexOf(checkBoxId)!=-1) 
            {    
                o.checked = me.checked;                     
                ShowAmountTotal(Type,o.id);       
                //GetSelectedIds(o.id, selectId,strUC);                                        
            }
        } 
        
    }   
     if(me.checked == false)
    {
        if (Type == "Invoices") {
        document.getElementById("ctl00_ContentPlaceHolder1_lblInvoicesTotal").innerHTML = "0.00";
     }
     else if (Type == "UnpaidCN") {
        document.getElementById("ctl00_ContentPlaceHolder1_lblUnpaidCNTotal").innerHTML = "0.00";
     }
     else if (Type == "UnallocatedCR") {
        document.getElementById("ctl00_ContentPlaceHolder1_lblUnallocatedCRTotal").innerHTML = "0.00";
     }      
    }
    document.getElementById("ctl00_ContentPlaceHolder1_lblCalculatedTotal").innerHTML = parseFloat(parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblInvoicesTotal").innerHTML) - (parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblUnallocatedCRTotal").innerHTML) + parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblUnpaidCNTotal").innerHTML))).toFixed(2);
    document.getElementById("ctl00_ContentPlaceHolder1_lblCreditAndCashTotal").innerHTML = parseFloat(parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblUnallocatedCRTotal").innerHTML) + parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_lblUnpaidCNTotal").innerHTML)).toFixed(2);
}

/*function GetSelectedIds(checkBoxId,selectId,strUC)
{   
    if (!strUC)
        strUC = '';
    var index = checkBoxId.lastIndexOf('_');  
    var prefix = checkBoxId.substring(0,index); 
    var objId = prefix + '_' + selectId;
     
    var UCindex;
    var UCprefix = '';
     
    if (strUC != '')
    {
      UCindex = prefix.lastIndexOf(strUC)
      UCprefix = prefix.substring(0,UCindex) +  strUC + '_'
    }
    var strIDs = new String();
    if (document.getElementById(checkBoxId).checked)
    {
        if (document.getElementById(UCprefix + 'hdnSelectedIds').value == '')
            document.getElementById(UCprefix + 'hdnSelectedIds').value += document.getElementById(objId).value;          
        else  
            document.getElementById(UCprefix  + 'hdnSelectedIds').value += ',' + document.getElementById(objId).value;          
    }        
    else if (!document.getElementById(checkBoxId).checked)      
    {
        var currentIds = document.getElementById(UCprefix  + 'hdnSelectedIds').value;
        var removeId = document.getElementById(objId).value;
        if (currentIds.indexOf(removeId) != -1)
        {  
            var finalIds;
            finalIds = currentIds.substring(0, currentIds.indexOf(removeId)-1)                 
            finalIds += currentIds.substring(currentIds.indexOf(removeId) + removeId.length + 1, currentIds.length);
            document.getElementById(UCprefix + 'hdnSelectedIds').value = finalIds;          
        }                           
    }               
}*/


   function ExpiryDateValidate(source, arguments)
   {	

		var flag = true ;
		

		if(source.controltovalidate == 'UCCompanyProfile1_txtEmployeeLiabilityDate')
		{
			if(document.getElementById('chkInsurance4').checked == true )
			{
				if (isValidDate(document.getElementById(source.controltovalidate).value) || document.getElementById(source.controltovalidate).value == "Enter expiry date" )          
				{	flag = true;  
					isValidDateFlag = true 
				}	
				else  
				{ 
					flag = false; 
					isValidDateFlag = false   
				}
					   					
			}  
		}
		else if(source.controltovalidate == 'UCCompanyProfile1_txtPublicLiabilityDate')
		{
			if( document.getElementById('chkInsurance5').checked == true )
			{ 
				if (isValidDate(document.getElementById(source.controltovalidate).value) || document.getElementById(source.controltovalidate).value == "Enter expiry date" )          
				{	flag = true;  
					isValidDateFlag = true 
				}	
				else  
				{ 
					flag = false; 
					isValidDateFlag = false   
				} 
    		}  
		} 
		else if (source.controltovalidate == 'UCCompanyProfile1_txtProfessionalIndemnityDate')
		{
			if( document.getElementById('chkInsurance6').checked == true )
			{
				if (isValidDate(document.getElementById(source.controltovalidate).value) || document.getElementById(source.controltovalidate).value == "Enter expiry date" )            
				{	flag = true;  
					isValidDateFlag = true 
				}	
				else  
				{ 
					flag = false; 
					isValidDateFlag = false   
				}
    			
    			} 
		}	 
		
			if (flag == true)            
			{
				arguments.IsValid = true;  
			}
			else
			{
				arguments.IsValid = false;  
			}
             
  }  
  function showHideRatingDetails(objId, visible)
{
	document.getElementById(objId).style.visibility = visible;
	//document.getElementById(objId).style.position = "relative";
}


// code to set focus on validation
function callSetFocusValidation(parentObjID)
{
   if (parentObjID != "")
   {        
        parentObjID = parentObjID + "_";               
        if (parentObjID == "UCCompanyProfile1_")
        {   
            if(document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtEmployeeLiabilityDate") != null)
            {
                if (document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtEmployeeLiabilityDate").value == "Enter expiry date")
                  {document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtEmployeeLiabilityDate").value = "";}
                if (document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtProfessionalIndemnityDate").value == "Enter expiry date")
                    {document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtProfessionalIndemnityDate").value = "";}
                if (document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtPublicLiabilityDate").value == "Enter expiry date")
                    {document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtPublicLiabilityDate").value = "";}
            }            
        }
        if (parentObjID == "UCSpecialistsForm1_")
        {           
            if (document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtVisaExpDate").value == "Enter expiry date")
              {document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtVisaExpDate").value = "";}
           
            if (document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtUKSecuDate").value == "Date of issue")
                {document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtUKSecuDate").value = "";}    
            if (document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtCSCSDate").value == "Enter expiry date")
                {document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtCSCSDate").value = "";}  
            if (document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtDBSExpiryDate").value == "Enter expiry date")
                {document.getElementById("ctl00_ContentPlaceHolder1_"+parentObjID+"txtDBSExpiryDate").value = "";}                
        }
   }
   	focusTime = setTimeout("setFocusValidation('" + parentObjID + "')", 5000);	 
}
function setFocusValidation(objID)
{  
	try
	{  
		if(document.getElementById(objID  + "validationSummarySubmit").innerHTML != "" || document.getElementById(objID  + "lblError").innerText != "" || document.getElementById(objID  + "divValidationMsg").innerHTML != "")
		{
			setFocus(objID);
			if (objID == "UCCompanyProfile1_")
            {   
			if (document.getElementById(objID+"txtEmployeeLiabilityDate").value == "")
              {document.getElementById(objID+"txtEmployeeLiabilityDate").value = "Enter expiry date";}
            if (document.getElementById(objID+"txtProfessionalIndemnityDate").value == "")
                {document.getElementById(objID+"txtProfessionalIndemnityDate").value = "Enter expiry date";}
            if (document.getElementById(objID+"txtPublicLiabilityDate").value == "")
                {document.getElementById(objID+"txtPublicLiabilityDate").value = "Enter expiry date";}
            }
             if (parentObjID == "UCSpecialistsForm1_")
            {       
                if (document.getElementById(parentObjID+"txtVisaExpDate").value == "")
                  {document.getElementById(parentObjID+"txtVisaExpDate").value = "Enter expiry date";}
               
                if (document.getElementById(parentObjID+"txtUKSecuDate").value == "")
                    {document.getElementById(parentObjID+"txtUKSecuDate").value = "Date of issue";}    
                if (document.getElementById(parentObjID+"txtCSCSDate").value == "")
                    {document.getElementById(parentObjID+"txtCSCSDate").value = "Enter expiry date";}   
                if (document.getElementById(parentObjID+"txtDBSExpiryDate").value == "")
                {document.getElementById(parentObjID+"txtDBSExpiryDate").value = "Enter expiry date";}              
            }
		}
						
	}
	catch(error){}
	try
	{
	if(document.getElementById("CustomValidatorCtrl"))
		{
		if(document.getElementById("CustomValidatorCtrl").innerHTML != "")
		{
			setFocus(objID);
		}
		}
	}
	catch(error){}
	try
	{
		if(document.getElementById(objID  + "divValidationMsg").innerHTML != "")
			setFocus(objID);
	}
	catch(error){}
}
function setFocus(objId)
{
	try
	{
		if(document.getElementById(objId  + "validationSummarySubmit").innerHTML != "" || document.getElementById(objId  + "lblError").innerText != "" || document.getElementById(objId  + "divValidationMsg").innerHTML != "")
		{		
		      document.getElementById(objId  + "btnFocus").focus()				
		}
		
	}
	catch(error)
	{
		try
		{
			document.getElementById(objId + "btnFocus").focus()
		}
		catch(error){}
	}
	
}


function BeginDateValidate(source, arguments)
  {	     
  
         if (arguments.Value != "" && isValidDate(arguments.Value))          
            arguments.IsValid = true;            
          else          
            arguments.IsValid = false;   
            
            if(source.controltovalidate == 'txtEmployeeLiabilityDate')
			{	
				if (document.getElementById(source.controltovalidate).value == "" || document.getElementById(source.controltovalidate).value == "Enter expiry date" )          
					arguments.IsValid = true;  				
			}
			else if(source.controltovalidate == 'txtPublicLiabilityDate')
			{	
				if (document.getElementById(source.controltovalidate).value == "" || document.getElementById(source.controltovalidate).value == "Enter expiry date" )          
					arguments.IsValid = true;  				
			}  
			else if(source.controltovalidate == 'txtProfessionalIndemnityDate')
			{	
				if (document.getElementById(source.controltovalidate).value == "" || document.getElementById(source.controltovalidate).value == "Enter expiry date" )          
					arguments.IsValid = true;  				
			}             
  }  
  function EndDateValidate(source, arguments)
  {          
         if (arguments.Value != "" && isValidDate(arguments.Value))          
            arguments.IsValid = true;           
          else          
            arguments.IsValid = false;          
  }
  function isValidDate(srcValue)
 {
	return /^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$/.test(srcValue);
 }
  function DateRangeValidate(source, arguments)
  {	
	var temp=ChkRegularExp(document.getElementById("txtScheduleWInBegin"),document.getElementById("txtScheduleWInEnd"))
	if (temp== -1)
		arguments.IsValid = false;
	else
		arguments.IsValid = true;
  }
  function ChkRegularExp(ob1,ob2){		
  
	if(document.all) {
		var vals1 = ob1.Validators;
		var vals2 = ob2.Validators;						
			{
				var date1= new Date();
				var date2= new Date();												
				date1.setFullYear((ob1.value).substring(6,10));				
				date1.setDate((ob1.value).substring(0,2));
				date1.setMonth((ob1.value).substring(3,5));										
				if (ob2.value!="")
				{				
					date2.setFullYear((ob2.value).substring(6,10));
					date2.setDate((ob2.value).substring(0,2));
					date2.setMonth((ob2.value).substring(3,5));					
				}
				else
				{
					
					document.getElementById("btnSubmitTop").click();
					return;
				}
				
				if(date1 <= date2)
				{					
					return 1;									
				}
				else
				{					
					return -1;					
				}		
			}
			}
}
function ClientValidate(source, arguments)
{
           if (arguments.Value != 0)
           arguments.IsValid = true;
          else
            arguments.IsValid = false;
}
function setValFocus(objId)
  {
	setTimeout("document.getElementById('"+objId+"').focus()",200);
  }
  

 /*-----------------Enter Kry ----------------------------------------------------------- */
 
document.onkeydown =
	function (evt)
	{
	   
		var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
		var eventTarget = evt ? evt.target : event.srcElement;
		if (keyCode == 13 && ((eventTarget.id == "UCLogin1_txtLoginUserName")|| (eventTarget.id == "UCLogin1_txtLoginPassword") || (eventTarget.id == "UCActivateAccount1_txtPassword") ) )
		{
		if (document.getElementById("UCLogin1_lnkBtnLogin")=="javascript:__doPostBack('UCLogin1$lnkBtnLogin','')")
		{
		    __doPostBack('UCLogin1$lnkBtnLogin','');					
				return false;
		}
		if (document.getElementById("UCActivateAccount1_lnkbtnSubmit")=="javascript:__doPostBack('UCActivateAccount1$lnkbtnSubmit','')")
		{
		    __doPostBack('UCActivateAccount1$lnkbtnSubmit','');					
				return false;
		}			
		}

        if(keyCode==13&&(eventTarget.id=="ctl00_ContentPlaceHolder1_txtTagName" || eventTarget.id=="ctl00_ContentPlaceHolder1_txtDateCreatedFrom" || eventTarget.id=="ctl00_ContentPlaceHolder1_txtDateCreatedTo"))
        {
            if(document.getElementById("ctl00_ContentPlaceHolder1_btnView") != null)
            {
              document.getElementById("ctl00_ContentPlaceHolder1_btnView").click();  
              return false;
            }
        }
        else if(keyCode==13&&(eventTarget.id=="ctl00_ContentPlaceHolder1_txtCompanyName" || eventTarget.id=="ctl00_ContentPlaceHolder1_txtKeyword" || eventTarget.id=="ctl00_ContentPlaceHolder1_txtFName" || eventTarget.id=="ctl00_ContentPlaceHolder1_txtLName" || eventTarget.id=="ctl00_ContentPlaceHolder1_txtEmail" || eventTarget.id=="ctl00_ContentPlaceHolder1_txtPostCode" || eventTarget.id=="ctl00_ContentPlaceHolder1_txtContactIdAccountId"))
        {
            if(document.getElementById("ctl00_ContentPlaceHolder1_btnSearch") != null)
            {
              document.getElementById("ctl00_ContentPlaceHolder1_btnSearch").click();  
              return false;
            }
        }
					
	}
 /*---------------------------------------------------------------------------------------*/
 /*---------------------------------AJAX Error Handling Code------------------------------------START-------*/
 
 
 setTimeout("InitializeSysObject()",1000);
 function InitializeSysObject()
 {
    if (typeof Sys != 'undefined')
   {
    // alert(Sys);    
    Sys.Application.add_load(AppLoad);
   }
 }  
function AppLoad()
{
  Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
  Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequest);
}

function BeginRequest(sender, args) {
  // Hide AJAX error Message
 //TODO
} 
function EndRequest(sender, args) {
  // Check to see if there's an error on this request.
  if (args.get_error != undefined && args.get_error()!=null)
  {
     var URL = unescape(location.href)          
     
    // Show Ajax Error Message
    //TODO
    
   //Uncomment line below  this is cxommented for testing purpose only
  // alert("We're sorry, an error has occurred. We Request you to kindly try again. If the error re-occurs please try again the next day as our technical team would have fixed the error by then.");
  
    // Let the framework know that the error is handled, 
    //  so it doesn't throw the JavaScript alert.
    
    //Uncomment line below  this is cxommented for testing purpose only
    // args.set_errorHandled(true);       
  sendAJAXErrorDetails(URL,args.get_error().message)
  }
}
function createRequestObject() {
    var ro;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}
var http = createRequestObject();
function sendAJAXErrorDetails(url,message) {
    var errorPageSite="/errorpage.aspx?url="       
    http.open("get", errorPageSite + url +"&message="+message);
    http.onreadystatechange = handleResponse;
    http.send(null);
}
function handleResponse() {
    if(http.readyState == 4){
        var response = http.responseText;                
        //TODO  
       // alert(response) 
    }
}

/*---------------------------------AJAX Error Handling Code--------------------------------------END-----*/

/*------------------------------------------------------------------------------------------------------*/
function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}

function CalculatePrice(ID)
{  
    if(ID == "ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWholesaleDayJobRate" || ID == "ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtEstTimeRqrd")
        {  
            if(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWholesaleDayJobRate") != null)
                {
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").value =roundNumber( document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWholesaleDayJobRate").value * document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtEstTimeRqrd").value,2)
                }
            
        }    
     if(ID == "ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtPlatformDayJobRate" || ID == "ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtEstTimeRqrd")
        {
            if(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtPlatformDayJobRate") != null)
                {
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitPP").value = roundNumber(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtPlatformDayJobRate").value * document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtEstTimeRqrd").value,2)
                }
        }
     if(ID == "ctl00_ContentPlaceHolder1_UCWOProcess1_txtProposedRate" || ID == "ctl00_ContentPlaceHolder1_UCWOProcess1_txtEstimatedTimeInDays")
        {
            document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_txtValue").value =roundNumber(document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_txtProposedRate").value * document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_txtEstimatedTimeInDays").value,2)
        }
      if(ID == "ctl00_ContentPlaceHolder1_txtNoOfStages" && document.getElementById("ctl00_ContentPlaceHolder1_hdnPricingMethod").value == "Fixed")
        { 
            document.getElementById("ctl00_ContentPlaceHolder1_txtWholesaleStagePrice").value = roundNumber(document.getElementById("ctl00_ContentPlaceHolder1_txtWholesalePrice").value / document.getElementById("ctl00_ContentPlaceHolder1_txtNoOfStages").value,2)
            document.getElementById("ctl00_ContentPlaceHolder1_txtPlatformStagePrice").value =roundNumber(document.getElementById("ctl00_ContentPlaceHolder1_txtPlatformPrice").value / document.getElementById("ctl00_ContentPlaceHolder1_txtNoOfStages").value ,2)
        }  
      if(ID == "ctl00_ContentPlaceHolder1_txtEstimatedTimeInDays" && document.getElementById("ctl00_ContentPlaceHolder1_hdnPricingMethod").value == "DailyRate")
        { 
            document.getElementById("ctl00_ContentPlaceHolder1_txtWholesaleStagePrice").value = roundNumber(document.getElementById("txtEstimatedTimeInDays").value * document.getElementById("ctl00_ContentPlaceHolder1_txtWholesaleDayRatePrice").value,2)
            document.getElementById("ctl00_ContentPlaceHolder1_txtPlatformStagePrice").value = roundNumber(document.getElementById("txtEstimatedTimeInDays").value * document.getElementById("ctl00_ContentPlaceHolder1_txtPlatformDayRatePrice").value ,2)
            document.getElementById("ctl00_ContentPlaceHolder1_txtWholesaleStagePrice").disabled = true
            document.getElementById("ctl00_ContentPlaceHolder1_txtPlatformStagePrice").disabled = true
        }  
      if(ID == "ctl00_ContentPlaceHolder1_txtEstimatedTimeInDaysSETWP" || ID == "ctl00_ContentPlaceHolder1_txtWholesaleDayRateSETWP")
        { 
            document.getElementById("ctl00_ContentPlaceHolder1_txtWholesalePrice").value = roundNumber(document.getElementById("ctl00_ContentPlaceHolder1_txtEstimatedTimeInDaysSETWP").value * document.getElementById("ctl00_ContentPlaceHolder1_txtWholesaleDayRateSETWP").value,2)            
        }          
}
/*---------------------------------------------------------------------------------------------*/
/*  ===================================================================================================================  */

function showSiteSubMenu(subMenu)
{
  //  alert("in show")
    document.getElementById(subMenu).style.visibility="visible";   

}
function hideSiteSubMenu(subMenu)
{ 
 //   alert("in hide" + subMenu) 
  
 setTimeout("document.getElementById('"+subMenu+"').style.visibility='hidden'",2000);
  
 // document.getElementById(subMenu).style.visibility="hidden";  
  
}

//Check if OW Admin has provided the PONUmber or Statement of Works

/*  ===================================================================================================================  */
/*Function used in AutoMatch settings for enabling/disabling the AM options*/
function toggleView()
{    
    if (document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkAutoMatch").checked == true)
    {
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkRefCheck").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkSecurity").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkCRB").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_ddlNearestTo").disabled = false;     
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkbxEngCRBCheck").disabled = false;     
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkbxEngCSCSCheck").disabled = false;     
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkbxEngUKSecurityCheck").disabled = false;     
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkbxEngRightToWork").disabled = false;     
    }
    else if (document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkAutoMatch").checked == false)
    {
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkRefCheck").disabled = true;
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkSecurity").disabled = true;
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkCRB").disabled = true;
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_ddlNearestTo").disabled = true;        
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkbxEngCRBCheck").disabled = true;     
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkbxEngCSCSCheck").disabled = true;     
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkbxEngUKSecurityCheck").disabled = true;     
        document.getElementById("ctl00_ContentPlaceHolder1_UCAutoMatchSettings1_chkbxEngRightToWork").disabled = true;                 }
}
/*Function used in Account settings */
function toggleAccountsView()
{    
    if (document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_chkEveningInst").checked == true)
    {
        document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_pnlSettings").style.display = "inline";
        if (document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_hdnEvengInst").value != "True")
        {
            document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_rdoRPUPFixed").checked = true;
            document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_rdoWPUPFixed").checked = true;
            document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_rdoPPUPFixed").checked = true;
        }
    }
    else if (document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_chkEveningInst").checked == false)
    {
        document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_pnlSettings").style.display = "none";
    }
}
/*Function used in Account Settings */
 function toggleLockSkillSetComments()
 {
   if (document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_chkLockSkillSets").checked == true)
    {
        document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_txtLockSkillSetComments").style.display = "inline";
        ValidatorEnable(document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_txtFeildSkillSet"), true); 
       // document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtFeildSkillSet').enabled = true;
       // document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtFeildSkillSet').display = "inline";
        document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_spanfldLockSkillSetComments').style.display = "inline" 
        
    }
    else if (document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_chkLockSkillSets").checked == false)
    {
        document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_txtLockSkillSetComments").style.display = "none";
        ValidatorEnable(document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_txtFeildSkillSet"), false); 
       // document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtFeildSkillSet').enabled = false;
        //document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtFeildSkillSet').display = "none";
        document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_spanfldLockSkillSetComments').style.display = "none"
         
    }
    
 }

//--------------------------------------------------------------------------------------------------------
//Function to call the function to populate the Location details on Selection of a Location from ddlLocation
//This function calls the PopulateLocDetails which further populates the location details.
//Added by Pratik Trivedi on 30th Dec, 2008
//--------------------------------------------------------------------------------------------------------
function onLocationChange(ddlLocName)
{
    if(document.getElementById(ddlLocName).value!="")
        {
            populateLocDetails(document.getElementById(ddlLocName).value,ddlLocName,locArr);
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnLocationId").value=document.getElementById(ddlLocName).value;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_btnFind").disabled=true;
        }
    else
        {
            clearForTempLoc();
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_btnFind").disabled=false;
        }
}

//--------------------------------------------------------------------------------------------------------
//Function to populate the Location details on Selection of a Location from ddlLocation
//This function hides the FirstName and LastName fields, populates location details from array and then disables the textboxes.
//Added by Pratik Trivedi on 30th Dec, 2008
//--------------------------------------------------------------------------------------------------------
function populateLocDetails(curr, ddlLocName, arrayStore)
{  
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tblContactInfo").style.visibility = "hidden";  
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lbltxtCompLocName").innerHTML = "Location Name";    
    
        document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_RqdFName').enabled = false;
        document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_RqdLName').enabled = false;  
        
        
    for(var i= 1 ; i<=arrayStore.length- 1 ; i++)
    {        
        if(arrayStore[i][0]== curr)
        {
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdCompName").style.display = "block";
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdtxtCompName").style.display = "block";
            
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtName").value = arrayStore[i][1];                 
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyAddress").value = arrayStore[i][2];
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCity").value = arrayStore[i][3];
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCounty").value = arrayStore[i][4];
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyPostalCode").value = arrayStore[i][5];
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyPhone").value = arrayStore[i][6];
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyFax").value = arrayStore[i][7];
            
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtName").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyAddress").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCity").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCounty").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyPostalCode").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyPhone").disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyFax").disabled = true;
        }
    }
}

//--------------------------------------------------------------------------------------------------------
//Function to reset the form for Creating a temporary location
//This function shows the FirstName and LastName fields and sets the value of all the textboxes to blank.
//Added by Pratik Trivedi on 30th Dec, 2008
//--------------------------------------------------------------------------------------------------------
function clearForTempLoc() 
{       
        if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
        {document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdCompName").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdtxtCompName").style.display = "none";}            
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lbltxtCompLocName").innerHTML = "Company Name";        
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tblContactInfo").style.visibility = "visible";        
        document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_RqdFName').enabled = true;
        document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_RqdLName').enabled = true;          
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtName").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyAddress").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCity").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCounty").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyPostalCode").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyPhone").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyFax").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtFName").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtLName").value = "";        
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtName").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyAddress").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCity").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCounty").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyPostalCode").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyPhone").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyFax").disabled = false;               
}

//--------------------------------------------------------------------------------------------------------
//Function to call the function to populate the Sub Categories on Selection of a Main Category
//This function calls the populateSubCat which further populates the Sub-Categories.
//Added by Pratik Trivedi on 31st Dec, 2008
//--------------------------------------------------------------------------------------------------------
function onMainCatChange(ddlMainCat)
{  
    var hdnsubcatval = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnSubCategoryVal");
    var hdnsubcattxt = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnSubCategorytxt");
    var subcat = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOSubType");
    hdnsubcatval.value = "";
    hdnsubcattxt.value = ""; 
    if (document.getElementById(ddlMainCat).value != "")
    {
        subcat.disabled = false;
        clearOptions(subcat);
        populateSubCat(document.getElementById(ddlMainCat).value);    
        hdnsubcattxt.value = subcat.options[subcat.selectedIndex].text;        
        hdnsubcatval.value = subcat.value;  
    }
    else
    {
        clearOptions(subcat);
        subcat.disabled = true;
    }
}



function populateSubCat(ddlMainCatValue)
{
    var browserName=navigator.appName;
    var selectedCombID=0;    
    xmlDoc=loadXMLDoc("XML/WOFormStandards.xml");    
    if (browserName=="Netscape")
    {        
        for (i = 1; i <= xmlDoc.childNodes[0].childNodes.length; i++)
        { if (xmlDoc.childNodes[0].childNodes[i] == "[object Element]")
           {
            if (xmlDoc.childNodes[0].childNodes[i].tagName == 'MainCat')            
            {if(xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue==ddlMainCatValue)
                 {selectedCombID=xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue;}
            }
            if (selectedCombID != 0)
            {
                if (xmlDoc.childNodes[0].childNodes[i].tagName == 'SubCat')
                {
                 try
                  {                                   
                    if (xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue==selectedCombID)
                    {
                     addOption(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOSubType"),xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("Name")[0].childNodes[0].nodeValue,xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue);
                    }
                   }
                catch(err)
                 {
               
                 }
                }
            }
           }
        }        
    }
    else if(browserName=="Microsoft Internet Explorer")
    {
        for (i = 1; i <= xmlDoc.childNodes[1].childNodes.length; i++)
        { if (xmlDoc.childNodes[1].childNodes[i])
           {
           if (xmlDoc.childNodes[1].childNodes[i].tagName == 'MainCat')            
            {if(xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue==ddlMainCatValue)
                 {selectedCombID=xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue;}
            }
            if (selectedCombID != 0)
            {
                if (xmlDoc.childNodes[1].childNodes[i].tagName == 'SubCat')
                {
                    try
                    {
                        if (xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("MainCatID")[0].childNodes[0].nodeValue==selectedCombID)
                         {
                            addOption(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOSubType"),xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("Name")[0].childNodes[0].nodeValue,xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("CombID")[0].childNodes[0].nodeValue);
                         }
                     }
                     catch(err)
                     {
               
                    }
                }
            }
           }
        }
    }
       
}
function onSubCatChange(ddlSubCat)
{      
    
    var subcat = document.getElementById(ddlSubCat).value;
    try
   {
    var subcattxt = document.getElementById(ddlSubCat).options[document.getElementById(ddlSubCat).selectedIndex].text;
     }
     catch(err) {    
    } 
    
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = subcat;
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = subcattxt;
    //alert(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnSubCategorytxt").value);
}

function addOption(selectbox,text,value )
{ //Function for adding an option into Select Box
    var optn = document.createElement("OPTION");
    optn.text = text;
    optn.value = value;
    selectbox.options.add(optn); 
}
function removeOption(selectbox,text)
{var i;
  for (i = selectbox.length - 1; i>=0; i--) 
  {if (selectbox.options[i].value==text) 
    {selectbox.remove(i);}
  }
}
function clearOptions(selectbox)
{ //Function for removing all options of a Select Box
    while(selectbox.options.length>0)
        selectbox.remove(0)
}
function ZeroValueWO(status)
{   
    if(status == "true") {    
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimit").value = "0";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimit").disabled = true;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_CustProposedPriceValidator").enabled = false;        
    }
    if(status == "false") {    
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimit").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_CustProposedPriceValidator").enabled = true;
    }
}
function ZeroValueWPWO(status)
{   
    if(status == "true") {    
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").value = "0";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").disabled = true;       
    }
    if(status == "false") {    
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").disabled = false;
        //if((document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlPricingMethod").selectedIndex).value = "DailyRate")
        //{
        //    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").disabled = true;    
        //}
    }
}
function ZeroValuePPWO(status)
{   
    if(status == "true") {
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitPP").value = "0";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitPP").disabled = true;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdchkReviewBid").style.display = "none";
    }
    if(status == "false") {    
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitPP").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdchkReviewBid").style.display = "inline";
        //if((document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlPricingMethod").selectedIndex).value = "DailyRate")
        //{
        //    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitPP").disabled = true;
        //}
    }
}
function ActionStagedYes() {   
   // alert(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlPricingMethod")); 
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trHdr").style.display = "inline";    
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trContent").style.display = "inline";    
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tblUpSell").style.display = "none"; 
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trHolidays").style.display = "inline";
    //document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ChkIncludeUpSell").checked = true;
    
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tblContactInfoUpSell").style.display = "none";
    //Added By Pankaj Malav that if Stage workorder is set to yes every time only the fixed type is to be shown
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlPricingMethod").value = "Fixed"
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtEstTimeRqrd").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblEstTimeRqrdDay").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblEstTimeRqrdHdr").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trWholesaleDayJobRate").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trPlatformDayJobRate").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").disabled = false;
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitPP").disabled = false;
    
    //ChkIncludeUpSell_CheckedChanged();
}
function ActionStagedNo() {
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trHdr").style.display = "none";    
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trContent").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trHolidays").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tblUpSell").style.display = "inline";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ChkIncludeUpSell").checked = false;
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_chkbxIncludeWeekends").checked = false;
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_chkbxIncludeHolidays").checked = false;
    //Added By Pankaj Malav that if Stage workorder is set to no every time only the fixed type is to be shown
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtEstTimeRqrd").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblEstTimeRqrdDay").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblEstTimeRqrdHdr").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trWholesaleDayJobRate").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trPlatformDayJobRate").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").disabled = false;
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitPP").disabled = false;    
    ChkIncludeUpSell_CheckedChanged();
}
function ddlPricingMethodChanged() {    
    if(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlPricingMethod").value == "Fixed") {
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtEstTimeRqrd").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblEstTimeRqrdDay").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblEstTimeRqrdHdr").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trWholesaleDayJobRate").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trPlatformDayJobRate").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").disabled = false;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitPP").disabled = false;

    }
    else {
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtEstTimeRqrd").style.display = "inline";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblEstTimeRqrdDay").style.display = "inline";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblEstTimeRqrdHdr").style.display = "inline";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtEstTimeRqrd").value = "1";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trWholesaleDayJobRate").style.display = "inline";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_trPlatformDayJobRate").style.display = "inline";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").disabled = true;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitPP").disabled = true;
    }
}
function ChkIncludeUpSell_CheckedChanged() {    
    try {
        if(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ChkIncludeUpSell").checked == true) {
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdUpSellPricehdr").style.display = "inline";        
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdUpSellInvoicehdr").style.display = "inline";
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdUpSellPrice").style.display = "inline";        
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdUpSellInvoice").style.display = "inline";
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdChkCCPayeeDetails").style.display = "inline";
        
            //Copy temp loc in Up Sell CC Details
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtBCFName").value = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtFName").value;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtBCLName").value = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtLName").value;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtBCAddress").value = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyAddress").value;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtBCCity").value = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCity").value;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtBCCounty").value = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCounty").value;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtBCPostalCode").value = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyPostalCode").value;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtBCFax").value = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyFax").value;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtBCPhone").value = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyPhone").value;

            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_rqUpSellPrice").enabled = true;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_reUpSellPrice").enabled = true;
                
            if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlLocation").options[document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlLocation").selectedIndex].value == 'Main Office')
            {
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ChkCCPayeeDetails").checked = true;
                ChkCCPayeeDetailsUpdate();
            }
        }
        else {
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdUpSellPricehdr").style.display = "none";
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdUpSellInvoicehdr").style.display = "none";
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdUpSellPrice").style.display = "none";
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdUpSellInvoice").style.display = "none";
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tdChkCCPayeeDetails").style.display = "none";
            
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_rqUpSellPrice").enabled = false;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_reUpSellPrice").enabled = false;

            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tblContactInfoUpSell").style.display = "none";
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ChkCCPayeeDetails").checked = false;
        }
    }
    catch(err) {
        alert("Please select the user for whom you want to create a Work Order.");
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ChkIncludeUpSell").checked = false;        
    }
}
function ChkCCPayeeDetailsUpdate() {
    if(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ChkCCPayeeDetails").checked == true) {        
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tblContactInfoUpSell").style.display = "inline";
    }
    else {        
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_tblContactInfoUpSell").style.display = "none";
    }
}

/************************************************Following Code Added By Ambar To show hide divs for SiteContentForm Page**********************/
function showHideContentForm()
{
    
     
      var selected=document.getElementById("UCWebContent1_ddlContentType");
      for (i=0;i<selected.length;i++)
      {
           if(selected.options[i].selected == true)
           {
               var selectedTxt=selected.options[i].text;
               break;
           }
      }
      
      switch(selectedTxt)
      {
      
        case "Show All Content" : 
              document.getElementById("UCWebContent1_pnlTitle").style.display="block";
              document.getElementById("UCWebContent1_pnlCompanyData").style.display="none";
              document.getElementById("UCWebContent1_pnlCompanyDetails").style.display="none";
              document.getElementById("UCWebContent1_pnlCmpHeader").style.display="none";
              document.getElementById("UCWebContent1_pnlCmpText").style.display="none";
              document.getElementById("UCWebContent1_pnlLocation").style.display="none";
              document.getElementById("divChkBoxes").style.display="none";
              break;
              
        case "Articles" : 
        
              document.getElementById("UCWebContent1_pnlTitle").style.display="block";
              document.getElementById("UCWebContent1_pnlCompanyData").style.display="none";
              document.getElementById("UCWebContent1_pnlCompanyDetails").style.display="block";
              document.getElementById("UCWebContent1_pnlCmpHeader").style.display="none";
              document.getElementById("UCWebContent1_pnlCmpText").style.display="none";
              document.getElementById("UCWebContent1_pnlLocation").style.display="none";
              document.getElementById("divChkBoxes").style.display="none";
                 document.getElementById("UCWebContent1_tdUrlField").style.display="block";  
                 document.getElementById("UCWebContent1_tdUrl").style.display="block";
                 document.getElementById("UCWebContent1_FileUploadType").value="Articles";
                 
                 
              break;
        
        case "Quotation" :
              document.getElementById("UCWebContent1_pnlTitle").style.display="block";
              document.getElementById("UCWebContent1_pnlCompanyData").style.display="block";
              document.getElementById("UCWebContent1_pnlCompanyDetails").style.display="block";
              document.getElementById("UCWebContent1_pnlCmpHeader").style.display="block";
              document.getElementById("UCWebContent1_pnlCmpText").style.display="block";
              document.getElementById("UCWebContent1_pnlLocation").style.display="none";
              document.getElementById("divChkBoxes").style.display="block";
              document.getElementById("UCWebContent1_ddlAccountType").style.display="block";
               document.getElementById("UCWebContent1_FileUploadType").value="Quotation";
              
              break;
        
        case "Press Releases" :
               
              document.getElementById("UCWebContent1_pnlTitle").style.display="block";
              document.getElementById("UCWebContent1_pnlCompanyData").style.display="none";
              document.getElementById("UCWebContent1_pnlCompanyDetails").style.display="inline";
              document.getElementById("UCWebContent1_pnlCmpHeader").style.display="none";
              document.getElementById("UCWebContent1_pnlCmpText").style.display="none";
              document.getElementById("UCWebContent1_pnlLocation").style.display="none";
              document.getElementById("divChkBoxes").style.display="none";
              document.getElementById("UCWebContent1_FileUploadType").value="Press Release";
              
              //document.getElementById("UCWebContent1_tblProfile").style.display="none";
        break;
      }
} 

function SelectAuthor(inp, data) {      
      inp.value = data[0];
      document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnContactID").value = data[1];
      document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnCompName").value = data[0];
}
function ValidateAutoSuggest()
{
    if ((document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_UCSearchContact1_txtContact").value) == (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnCompName").value)) 
    {
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_btnSelect").click();   
     }
    else
    {        
        alert("Please Select Company from Suggestions provided");
    }
}


function getdate()
{   
    var startdate = new Date(getDateObject(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnStartDate").value,"/"));
    return startdate;
}
function getbegindate()
{   
    var startdate = new Date(getDateObject(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInBegin").value,"/"));
    return startdate;
}
function holidaydates()
{
    var HolidayArr = new Array();
    $.ajax({
        type:"POST",
        url:"WOForm.aspx/GetHolidays",
        data: "{}",
        contentType: "application/json; charset=utf-8",    
        dataType:"json",
        success:function(data)    
            {var json = JSON.stringify(data);
            var loop = data.d;
           $.each(loop.rows, function (i, rows) {           
             HolidayArr[i] = rows.HolidayDate;             
            });
        },
       error: function(XMLHttpRequest, textStatus, errorThrown) 
	    {            
		    var jsonError = JSON.parse(XMLHttpRequest.responseText);            
		    alert(JSON.stringify(jsonError));
	    }});
	    return HolidayArr;    
}
function getDateObject(dateString,dateSeperator)
{
//This function return a date object after accepting 
//a date string ans dateseparator as arguments
var curValue=dateString;
var sepChar=dateSeperator;
var curPos=0;
var cDate,cMonth,cYear;
//extract day portion
curPos=dateString.indexOf(sepChar);
cDate=dateString.substring(0,curPos);
//extract month portion 
endPos=dateString.indexOf(sepChar,curPos+1); 
cMonth=dateString.substring(curPos+1,endPos);
//extract year portion 
curPos=endPos;
endPos=curPos+5;
cYear=dateString.substring(curPos+1,endPos);
//Create Date Object
var changeddate = cMonth+"/"+cDate+"/"+cYear;
return changeddate;
}

function getProductDescription(myval,HiddenWoTitle)
{  
    if (myval != "0")
    {
        //inp.value = data[0];	
	    //document.getElementById("hdnValue").value = document.getElementById("last").value
          if(HiddenWoTitle != "")
          {
              var i;
              var ISItemExists;
              ISItemExists = 0
              for (i = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnProduct").length - 1; i>=0; i--) 
              {             
              if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnProduct").options[i].value == myval) 
                {ISItemExists = 1}
              }          
              if(ISItemExists == 0)
              {
                addOption(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnProduct"),HiddenWoTitle,myval);
              }            
          }
	     document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnProduct").value = parseInt(myval);
         document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnSelectedService").value = parseInt(myval);
	    $.ajax({
        type:"POST",
        url:"WOForm.aspx/GetProductDetails",
        data: "{'productid':'"+myval+"','companyid':'"+document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnContactID").value+"'}",
        contentType: "application/json; charset=utf-8",    
        dataType:"json",        
        success:function(data)    
         {  var json = JSON.stringify(data);
            var loop = data.d;
            $.each(loop.rows, function(i,rows){    
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnProductId").value = parseInt(rows.WOID);                     
            //document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnbtnTrigger");
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnbtnTrigger").click(); 
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtTitle").value = rows.WOTitle;
        if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnBusinessArea").value == "IT")
        {
            if (rows.IsSignOffSheetReqd == true )
            {   document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_chkForceSignOff").checked = true;
                }
            else
            {document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_chkForceSignOff").checked = false;
            }
         }
          if (rows.IsWaitingToAutomatch == true )
            {   document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_chkIsWaitingToAutomatch").checked = true;
                }
            else
            {document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_chkIsWaitingToAutomatch").checked = false;
            }
          //  alert(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_chkForceSignOff").checked);
           if (rows.ReviewBids == true )
            {   document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_chkReviewBid").checked = true;
                }
            else
            {document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_chkReviewBid").checked = false;
            }
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWOLongDesc").value = rows.WOLongDesc.replace(/<BR>/g,"");
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtClientScope").value = rows.ClientScope.replace(/<BR>/g,"");
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtPONumber").value = rows.PONumber;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtJRSNumber").value = rows.JRSNumber;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpecialInstructions").value = rows.SpecialInstructions.replace(/<BR>/g,"");        
            if (parseInt(rows.LocationID) > 0)
                {document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnBillingLocation").value = parseInt(rows.LocationID);}
            var WP = rows.WholesalePrice;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnDBWP").value = WP;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnWPcomp").value = WP.toFixed(2);
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").value = WP.toFixed(2);
            
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOType").value = parseInt(rows.CombId);
            
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOSubType").disabled = true;
            clearOptions(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOSubType"));
            
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = "";
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = ""; 
            populateSubCat(parseInt(rows.CombId));
            
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOSubType").value = parseInt(rows.WOCategoryID);       
            onSubCatChange("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOSubType") ;
                                              
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInBegin").value = rows.AdminDeferredDate;  
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnStartDate").value = rows.AdminDeferredDate;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnPlatformPrice").value = rows.PlatformPrice;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitPP").value = rows.PlatformPrice;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtPPPerRate").value = rows.PPPerRate;
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddPPPerRate").value = rows.PerRate;

          if (rows.FreesatService == true && document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
                {                
                if(rows.DefaultGoodsLocation != 1)
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection").value = rows.DefaultGoodsLocation
                else
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection").selectedIndex = 1
                
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_divFreesatMake").style.display = "block";          
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "block";
                populateFreesatMake();}
            else
                {                
                if(rows.DefaultGoodsLocation != 1)
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection").value = rows.DefaultGoodsLocation
                else
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection").selectedIndex = 0
                
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_divFreesatMake").style.display = "none";
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_divFreesatMakeddl").style.display = "none";}      
                
              
                 if (rows.ServiceQuestion == 1)                           
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnBtnShowHideMdlQuestions").click();
                   
//            if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail")
//                {
//                if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnEvengInst").value == "True" && document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpTime").options.length == 4)
//                    {
//                    removeOption(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpTime"),"All Day");
//                    addOption(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpTime"),"6pm - 8pm","6pm - 8pm");
//                    addOption(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpTime"),"All Day","All Day");
//                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInBegin").value = "" ;
//                    }
//                }
           });
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) 
	    {            
		    var jsonError = JSON.parse(XMLHttpRequest.responseText);            
		    alert(JSON.stringify(jsonError));
	    }});	       
	}
	else
	{
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnProduct").selectedIndex = 0
	    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnProductId").value = 0;
	    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtTitle").value = "";
	    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWOLongDesc").value = "";
	    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtPONumber").value = "";
	    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtJRSNumber").value = "";
	    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpecialInstructions").value = "";
	    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").value = 0;
	    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitPP").value = 0;
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtPPPerRate").value = 0;
         document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddPPPerRate").value = "Per Job";
	    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOType").value = "";
	    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOSubType").value = "";
	    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnSubCategoryVal").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnSubCategorytxt").value = ""; 
        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnStartDate").value = getTodayDate();
        if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnEvengInst").value == "True")
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpTime").remove(2);
	}
}

function setPnlConfirmHeight()
{

         var ht = document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_pnlConfirm').offsetHeight;
       
        if ( ht >400 && ht < 600) {
            document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_pnlConfirm').style.height = '400px';
            document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_pnlConfirm').style.overflow = 'auto'
              }
        else if (ht > 600) {
            document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_pnlConfirm').style.height = '600px';
            document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_pnlConfirm').style.overflow = "auto"
        }
}

function PopulateDepotDetails(myval)
{
    var ScopeOfWork = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWOLongDesc").value
    ScopeOfWork = ScopeOfWork.replace("Collection Point Details:" ,"")   
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWOLongDesc").value = ScopeOfWork;
     if(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnDepotDetails").value != "" && document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnDepotDetails").value != "0")
        {         
            ScopeOfWork = ScopeOfWork.replace(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnDepotDetails").value,"");            
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWOLongDesc").value = ScopeOfWork;
            ScopeOfWork = ScopeOfWork.replace(/[\r\n]+/, "");         
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWOLongDesc").value = ScopeOfWork;
        }
    
    if (myval != "0" && myval != "1")
    {
              
        $.ajax({
        type:"POST",
        url:"WOForm.aspx/DepotLocDetails",
        data: "{'AddressID':'"+myval+"'}",
        contentType: "application/json; charset=utf-8",    
        dataType:"json",        
        success:function(data)    
           {var json = JSON.stringify(data);
            var loop = data.d;
           $.each(loop.rows, function (i, rows) {  
            var ScopeOfWork = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWOLongDesc").value
            if (rows.ServiceLocDetails != '')
            {
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWOLongDesc").value = "Collection Point Details:" + "\n" + rows.ServiceLocDetails.replace(/<BR>/g,"") + "\n" + "\n" + ScopeOfWork;
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnDepotDetails").value = rows.ServiceLocDetails.replace(/<BR>/g,"");
            }
           });
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) 
	    {            
		    var jsonError = JSON.parse(XMLHttpRequest.responseText);            
		    alert(JSON.stringify(jsonError));
	    }});	       
	}
	else
	{
	   document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnDepotDetails").value = ""; 
	}
}

function populateWOFOrm()
{
    //alert("Hi I've reached");
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_pnlWoForm").style.display = "block";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_pnlWoSummary").style.display = "none";
    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_divValidationMain").style.display = "none";  
}
function contactValidation(type)
{
    
    if ((document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnContactID").value)=="")
    {
        
        alert ('Please select a contact first');
    } 
    else
    {
        populateConfirmationSummary(type);
    }
}

function populateConfirmationSummary(type)
        {var flag;
        var errorstring;
        flag = true;
        errorstring = "";
        if (Page_ClientValidate() == false)
            {if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlLocation").value != "")
                {document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_RqdFName').enabled = false;
                document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_RqdLName').enabled = false;}
            else
                {document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_RqdFName').enabled = true;
                document.getElementById('ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_RqdLName').enabled = true;}   
            document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_divValidationMain").style.display = "block";}
        else
            {if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object]")
                {if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection").options[document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection").selectedIndex].text == "None Specified")
                    {if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                        {flag = false;
                         errorstring = "Please enter the location where the specified goods will be located";}}}
                if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtProductsPur") == "[object]" || document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtProductsPur") == "[object HTMLInputElement]") 
                {if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtProductsPur").value.length > 2000 )
                    {flag = false;
                        if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                        {errorstring = errorstring + "<br>Maximum 2000 characters are allowed in Products Purchased";}
                        else
                        {errorstring = errorstring + "<br>Maximum 2000 characters are allowed in Equipment Details";}}}
                if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtAddProducts") == "[object]" || document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtAddProducts") == "[object HTMLInputElement]") 
                {if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtAddProducts").value.length > 2000 )
                    {flag = false;
                    errorstring = errorstring + "<br>Maximum 2000 characters are allowed in Additional Products Purchased";}}
                    
                    //added by PratikT on 1st July, 2009 for validating Freesat Make/Model
                    if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake") == "[object]")                    
                    {if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_divFreesatMake").style.display == "block" )
  
                        {if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake").options[document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake").selectedIndex].text == "None Specified")
                            { if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnBusinessArea").value == "Retail") 
                                {flag = false;
                                 errorstring = "Please select the Freesat Make/Model";}}}}
                    
                if (flag == false)
                {document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_divValidationMain").style.display = "block";
                 document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblError").innerHTML = errorstring;}
            else
            {               
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_divValidationMain").style.display = "none"; 
                //document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_pnlWoForm").style.display = "none";
                //document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_pnlWoSummary").style.display = "block";
                if(type == "Confirm")
                {
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_btnSubmit").click();
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_btnSubmit").disabled=true;                    
                }
               else if(type == "Draft")
                {
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_btnSaveBtm").click();
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_btnSaveTop").disabled=true;
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_btnSaveBtm").disabled=true;                    
                }

                var category = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOType").options[document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOType").selectedIndex].text;
                category = category + " - " + document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOSubType").options[document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlWOSubType").selectedIndex].text;            
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblCategory").innerHTML = category;                  
                if(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtClientScope").value !="")
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblClientScope").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtClientScope").value);//.replace(/\\n/g,"<br>")
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblLongDesc").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWOLongDesc").value);//.replace(/\\n/g,"<br>");
                if(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpecialInstructions").value !="")
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblSpecialInstructions").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpecialInstructions").value);//.replace(/\\n/g,"<br>");
                if(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtClientInstru").value !="")
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblClientInstru").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtClientInstru").value);//.replace(/\\n/g,"<br>");
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblDateCreated").innerHTML = getTodayDate();
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblLocName").innerHTML = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCity").value + ", " + document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtCompanyPostalCode").value;
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblContact").innerHTML = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnContactName").value;
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblWOTitle").innerHTML = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtTitle").value;               
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblPrice").innerHTML = "&" + document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnCurrency").value + " " + document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpendLimitWP").value;                       
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblStartDate").innerHTML = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInBegin").value;
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblDateStart").innerHTML = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInBegin").value;            
                if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnBusinessArea").value != "Retail")
                {if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInEnd").value == "")
                        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblEndDate").innerHTML = "None Specified";            
                    else
                        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblEndDate").innerHTML = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInEnd").value;
                    if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnBusinessArea").value == "IT")
                        {document.getElementById("divConfProductsPur").style.display = "block";
                        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblProdPur").innerHTML = "Equipment Details: ";
                        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblConfProdPur").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtProductsPur").value);
                        document.getElementById("divConfRetail").style.display = "none";                        
                        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblConfGoods").innerHTML = "Equipment Location";}
                    else
                        {document.getElementById("divConfRetail").style.display = "none";                        
                        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblConfProdPur").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtProductsPur").value);}}
                else
                    {document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtlblEndDate").innerHTML = "Installation Time";
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblEndDate").innerHTML = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpTime").options[document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpTime").selectedIndex].text;
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnInstTime").value = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpTime").options[document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpTime").selectedIndex].text;                    
                    document.getElementById("divConfProductsPur").style.display = "block";
                    document.getElementById("divConfRetail").style.display = "block";
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblConfProdPur").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtProductsPur").value);
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblConfSalesAgent").innerHTML = convertTextToHTML(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSalesAgent").value);}
               
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblSupplyParts").innerHTML = "No";
                if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtPONumber").value == "")
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblPONumber").innerHTML = "None Specified";
                else
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblPONumber").innerHTML = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtPONumber").value;
                if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtJRSNumber").value == "")
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblJRSNumber").innerHTML = "None Specified";
                else
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblJRSNumber").innerHTML = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtJRSNumber").value;          
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblDressCode").innerHTML = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlDressCode").options[document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ddlDressCode").selectedIndex].text;            
                if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection") == "[object]") 
                {                   
                    var GoodsLocation = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection").options[document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnGoodsCollection").selectedIndex].text;
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblBusinessDivision").innerHTML = GoodsLocation;
                }
                else
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblBusinessDivision").innerHTML = "None Specified";
                if ((document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake") == "[object HTMLSelectElement]" || document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake") == "[object]") && document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake").value != "") 
                {                    
                    var FreesatMake = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake").options[document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake").selectedIndex].text;
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_lblFreesatMake").innerHTML = FreesatMake;
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnFreesat").value = document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake").value;}
                else
                    document.getElementById("divConfirmFreesat").style.display = "none";
                
            }}
}
function convertTextToHTML(strMultiLineText)
{
var strSingleLineText = strMultiLineText.replace(
// Replace out the new line character.
new RegExp( "\\n", "g" ), 
 

// Put in ... so we can see a visual representation of where
// the new line characters were replaced out.
" <br> " 
);
return strSingleLineText;
}
function getTodayDate()
    {var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    return (day + "/" + month + "/" + year);}
function populateFreesatMake()
    {
    xmlDoc=loadXMLDoc("XML/WOFormStandards.xml"); 
    //xmlDoc.onreadystatechange = 
    readXML();
    //xmlDoc.load("XML/WOFormStandards.xml");
}
function readXML()
{var browserName=navigator.appName;
    if (browserName=="Netscape")
    {
    for (i = 1; i <= xmlDoc.childNodes[0].childNodes.length; i++)
     { if (xmlDoc.childNodes[0].childNodes[i] == "[object Element]")
        {
            if (xmlDoc.childNodes[0].childNodes[i].tagName == 'FreesatMake')
            {
                addOption(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake"),xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("StandardValue")[0].childNodes[0].nodeValue,xmlDoc.childNodes[0].childNodes[i].getElementsByTagName("StandardID")[0].childNodes[0].nodeValue);
            }            
        }       
      }
    }
    else if(browserName=="Microsoft Internet Explorer")
    {
        for (i = 1; i <= xmlDoc.childNodes[1].childNodes.length; i++)
        { if (xmlDoc.childNodes[1].childNodes[i])
           {
           if (xmlDoc.childNodes[1].childNodes[i].tagName == 'FreesatMake')            
            {
            try{
               addOption(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_drpdwnFreesatMake"),xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("StandardValue")[0].childNodes[0].nodeValue,xmlDoc.childNodes[1].childNodes[i].getElementsByTagName("StandardID")[0].childNodes[0].nodeValue);
            }
            catch(err){}
            }
           }
        }
    }
}
function AddHoliday(mode)
{
    if (mode == 'AddEdit')
    
        {
        document.getElementById("ctl00_ContentPlaceHolder1_pnlListing").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_pnlConfirm").style.display = "block";
        document.getElementById("ctl00_ContentPlaceHolder1_hdnAddEditDateID").value = 0;
        document.getElementById("ctl00_ContentPlaceHolder1_txtDate").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_txtHolidayTxt").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_chkbxIsDeleted").checked = false;
        }
    else
    {
//       document.getElementById("pnlListing").style.display = "block";
//        document.getElementById("pnlConfirm").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_hdnAddEditDateID").value = 0;
         document.getElementById("ctl00_ContentPlaceHolder1_hdnButtonCancel").click();
        }
}
function validate_required(field,alerttxt)
{
      var text = document.getElementById(field).value;
      if (text==null||text=="")
         {
            alert(alerttxt);
         }
        else {
            var re5digit="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"; //regular expression defining a 5 digit number
            if (document.getElementById(field).value.search(re5digit)==-1) //if match failed
                alert("Date should be in DD/MM/YYYY Format");
            else
            {document.getElementById("divModalParent").style.display = "none";
            document.getElementById("divProgressDate").style.display = "block";
            document.getElementById("ctl00_ContentPlaceHolder1_hdnButton").click();}
        }             
}
function validate_required_Holiday(field,alerttxt)
{
      var text = document.getElementById(field).value;
      if (text==null||text=="")
         {
            alert(alerttxt);
         }
        else 
        {
            var re5digit="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"; //regular expression defining a 5 digit number
            if (document.getElementById(field).value.search(re5digit)==-1) //if match failed uu
                alert("Date should be in DD/MM/YYYY Format");
            else
            {
               document.getElementById("ctl00_ContentPlaceHolder1_hdnButton").click();
            }
        }             
}
function validateCN_required(field,alerttxt)
{
      var text = document.getElementById(field).value;
      if (text==null||text=="")
         {alert(alerttxt);}
        else {
            var re5digit="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"; //regular expression defining a 5 digit number
            if (document.getElementById(field).value.search(re5digit)==-1) //if match failed
                alert("Date should be in DD/MM/YYYY Format");
            else
            {document.getElementById("divModalPopUp").style.display = "none";
            document.getElementById("divShowProgress").style.display = "block";
            document.getElementById("ctl00_ContentPlaceHolder1_hdnButtonCN").click();}
        }             
}
function validate_requiredForIssueNote(field,alerttxt)
{//alert(field);
      var text = document.getElementById(field).value;
      
      if (text != "Enter a new note here")
      {
         while(text.indexOf(" ") != -1 )
        {
            text = text.replace(" ","");
        }
      }
      if (text==null||text==""||text=="Enter a new note here")
         {
            alert(alerttxt);
         }
        else {
                             
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_hdnButton").click();
        }             
}
function ondrpTimeChange(objID)
         
{
    
    var WP = objID.replace("drpTime","txtSpendLimitWP");
    
    var PP = objID.replace("drpTime","txtSpendLimitPP");
    
    var WPUplift = objID.replace("drpTime","hdnWPUplift");
    
    var PPUplift = objID.replace("drpTime","hdnPPUplift");
    
    var InstTime = objID.replace("drpTime","hdnInstTime"); 
    
    var EvengInstTimeID = objID.replace("drpTime","hdnEvngInstTime"); 
    
    
    var WPUpliftPercID = objID.replace("drpTime","hdnWPUPliftPercent");
    var WPUpliftPerc = "False";
    
    
    var PPUpliftPerc = "False";
    var PPUpliftPercID = objID.replace("drpTime","hdnPPUpliftPercent");
    
    
    try{
        var EvengInstTime = document.getElementById(EvengInstTimeID).value;
        WPUpliftPerc = document.getElementById(WPUpliftPercID).value;
        PPUpliftPerc = document.getElementById(PPUpliftPercID).value;
    if ((document.getElementById(objID).value == "6pm - 8pm" || document.getElementById(objID).value == EvengInstTime) && (document.getElementById(InstTime).value != "6pm - 8pm" || document.getElementById(InstTime).value != EvengInstTime))
       {
            OldWP = parseFloat(document.getElementById(WP).value);
            OldPP = parseFloat(document.getElementById(PP).value);
            if (WPUpliftPerc == "True")
                document.getElementById(WP).value = (parseFloat(document.getElementById(WP).value) + parseFloat((document.getElementById(WPUplift).value / 100) * parseFloat(document.getElementById(WP).value))).toFixed(2);
            else
                document.getElementById(WP).value = parseFloat(document.getElementById(WP).value) + parseFloat(document.getElementById(WPUplift).value);
            
            if (PPUpliftPerc == "True")
                document.getElementById(PP).value = (parseFloat(document.getElementById(PP).value) + parseFloat((document.getElementById(PPUplift).value / 100) * parseFloat(document.getElementById(PP).value))).toFixed(2);
            else
                document.getElementById(PP).value = parseFloat(document.getElementById(PP).value) + parseFloat(document.getElementById(PPUplift).value);
       }
    if ((document.getElementById(objID).value != "6pm - 8pm" || document.getElementById(objID).value != EvengInstTime) && (document.getElementById(InstTime).value == "6pm - 8pm" || document.getElementById(InstTime).value == EvengInstTime))
       {
             if (OldWP == 0.00)
                OldWP = document.getElementById(WP).value;
             
             if (OldPP == 0.00)
                OldPP = document.getElementById(PP).value
                
            if (WPUpliftPerc == "True")
                document.getElementById(WP).value = (parseFloat(document.getElementById(WP).value) - parseFloat((document.getElementById(WPUplift).value / 100) * OldWP)).toFixed(2);
            else
                document.getElementById(WP).value = parseFloat(document.getElementById(WP).value) - parseFloat(document.getElementById(WPUplift).value);
                
            if (PPUpliftPerc == "True")
                document.getElementById(PP).value = (parseFloat(document.getElementById(PP).value) - parseFloat((document.getElementById(PPUplift).value / 100) * OldPP)).toFixed(2);
            else
                document.getElementById(PP).value = parseFloat(document.getElementById(PP).value) - parseFloat(document.getElementById(PPUplift).value);
       }
            document.getElementById(InstTime).value = document.getElementById(objID).value;
    }
    catch(err){}
}
function noUsers()
{
if (Page_ClientValidate() == false)
       document.getElementById("ctl00_ContentPlaceHolder1_divValidationMain").style.display = "block";
    else 
       document.getElementById("ctl00_ContentPlaceHolder1_lnkbtnEnter").click();
}
function disableLogin(objid)
{   
    var targetid = objid.replace("drpdwnUserType","chkbxEnableLogin");    
    if (document.getElementById(objid).options[document.getElementById(objid).selectedIndex].text == "Depot")
    {document.getElementById(targetid).checked = false;
        document.getElementById(targetid).disabled = true;}
    else
    {document.getElementById(targetid).disabled = false;}
}
function ChangeAptTimeNPrice(objID)
{
     EvengInstTime = document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_hdnEvngInstTime").value
     if(document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_hdnAptTM").value!="6pm - 8pm" || document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_hdnAptTM").value!= EvengInstTime)
     {
         if (document.getElementById(objID).value == "6pm - 8pm" || document.getElementById(objID).value == EvengInstTime)
         {
             var originalPrice=0.0;
             originalPrice=parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_hdnPrice").value);     
             var uplift=0.0;
             uplift=parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_hdnPPUp").value);
             var upliftPerc = "False";
             upliftPerc = document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_hdnPPUpliftPercent").value;
             var newprice = 0.0;
             
             if (upliftPerc == "True")
                newprice = (originalPrice + ((uplift / 100) * originalPrice)).toFixed(2);
             else
                newprice = (originalPrice + uplift).toFixed(2);
                
             document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_txtValue").value=newprice;
         }
         else
         {
            //document.getElementById("UCWOProcess1_txtValue").value=parseFloat(document.getElementById("UCWOProcess1_hdnPrice").value).toFixed(2);  
         }
     }
     else if(document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_hdnAptTM").value=="6pm - 8pm" || document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_hdnAptTM").value == EvengInstTime) 
     {
        
        if (document.getElementById(objID).value != "6pm - 8pm" || document.getElementById(objID).value != EvengInstTime)
         {
             var originalPrice=0.0;
             originalPrice=parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_hdnPrice").value);     
             var uplift=0.0;
             uplift=parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_hdnPPUp").value);
             
             var upliftPerc = "False";
             upliftPerc = document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_hdnPPUpliftPercent").value;
             
             var newprice = 0.0;
             
             if (upliftPerc == "True")
                newprice = (originalPrice - ((uplift / 100) * originalPrice)).toFixed(2);
             else
                newprice = (originalPrice - uplift).toFixed(2);
                
             document.getElementById("ctl00_ContentPlaceHolder1_UCWOProcess1_txtValue").value=newprice;
         }
         else
         {
            //document.getElementById("UCWOProcess1_txtValue").value=parseFloat(document.getElementById("UCWOProcess1_hdnPrice").value).toFixed(2);  
         }
     
     }
}
function SetHrdAboutUs(id)
{
    if(document.getElementById(id).value=="Other")
    {   
         document.getElementById('UCCompanyProfile1_pnlHrdAbtUs').style.display="block";
         
         document.getElementById('tdHrd').innerHTML="How did they heard about us?";
    }
    else
    {
     document.getElementById('UCCompanyProfile1_pnlHrdAbtUs').style.display="none";
     document.getElementById('tdHrd').innerHTML="";
    }
}
function ShowDepotSheetDetails(WOTrackingID, count)
{
	var strModifiedValue ="_divDepotSheet"+WOTrackingID;		
	if (document.getElementById("ctl00_ContentPlaceHolder1_lblHeading").innerHTML == "Active")
	{
	if(count>0)
	{
	    if (document.getElementById(strModifiedValue ).style.visibility == "hidden")
	    { 	document.getElementById(strModifiedValue ).style.visibility = "visible";
    		
	    }	
	    else if(document.getElementById(strModifiedValue).style.visibility == "visible")
	    { document.getElementById(strModifiedValue).style.visibility = "hidden";
	    }
	}
	} 		
}


var selectedRow
var selectedContactId
function SelectAutoSuggestRow(inp, data) {      
      inp.value = data[0];
      selectedRow = inp.value;
      selectedContactId = data[1];
}
function ValidateAutoSuggestGen(txtbxvalue, btn)
{   
     
     document.getElementById("ctl00_ContentPlaceHolder1_hdnContactID").value = selectedContactId
    if (selectedRow == (document.getElementById(txtbxvalue).value)) 
    {
       document.getElementById(btn).click();   
       
     }
    else
   {        
        alert("Please Select Company from Suggestions provided");
        document.getElementById(txtbxvalue).value = "";
        return false; 
    }
}

 function ConfirmMigration()
     {
            if (confirm("Converting Client to Supplier is irreversible process."))
            {
                    document.getElementById("ctl00_ContentPlaceHolder1_btnConfirmClick").click();
                    
            }
            else
            {
                   return false;    
             }      
      }

var selectedRowName
var selectedRowId1
function SelectAutoSuggestRowName(inp, data) {      
      inp.value = data[0];
      selectedRowName = inp.value;
      selectedRowId1 = data[1];
}

var selectedRowId
function SelectAutoSuggestRowId(inp, data) {      
      inp.value = data[0];
      selectedRowId = inp.value;
}
function ValidateAutoSuggestForAttachments( )
{   
    if((document.getElementById("ctl00_ContentPlaceHolder1_txtCompanyName").value == "") && (document.getElementById("ctl00_ContentPlaceHolder1_txtworkorderid").value == ""))   
     {
         alert("Please select workorderID or Company Name to search the attachments");
     }
    else 
    {
        if (selectedRowId1)
        {
           document.getElementById("ctl00_ContentPlaceHolder1_hdnContactID").value = selectedRowId1
        }
        else 
        { 
          document.getElementById("ctl00_ContentPlaceHolder1_hdnContactID").value = "" 
        }
        if (selectedRowName == (document.getElementById("ctl00_ContentPlaceHolder1_txtCompanyName").value)) 
        {
           document.getElementById("ctl00_ContentPlaceHolder1_btnSubmit").click();   
         }
        else if(selectedRowId == (document.getElementById("txtworkorderid").value))  
        {        
           document.getElementById("ctl00_ContentPlaceHolder1_btnSubmit").click();   
        }
        else 
        {        
           alert("Please Select Company Name and/or WorkorderId from Suggestions provided");
           document.getElementById("ctl00_ContentPlaceHolder1_txtCompanyName").value = "";
           document.getElementById("ctl00_ContentPlaceHolder1_txtworkorderid").value = "";
           return false; 
        }
    }
}


 function ConfirmRemoveAttachments()
     {
            if (confirm("Do you want to remove the attachments?"))
            {
                    document.getElementById("ctl00_ContentPlaceHolder1_btnRemoveClick").click();
                    
            }
            else
            {
                   return false;    
             }      
      }
      
 function fireclick()
     {
        document.getElementById("UCAdminWOsListing1_btnCancelClick").click();
     }
function SaveAttachment()
{
document.getElementById("UCAdminWOsListing1_hdnButtonAttach").click();
}

function RemoveAttachment()
{
document.getElementById("UCAdminWOsListing1_hdnButtonRemove").click();
}
function xShowModalDialog(pageURL, title,w,h)
{
var left = (screen.width/2)-(w/2);
var top = (screen.height/2)-(h/2);
var targetWin = window.open (pageURL, title, 'toolbar=0,location=0, directories=0, status=0, menubar=0, scrollbars=0, resizable=0, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);

}

function clearText(field)
{
    document.getElementById(field).value = "";
}

function validateMaxLen(textID,maxlen,alertMsg)
{
    var textToVal = document.getElementById(textID).value;
    if (textToVal.length > maxlen)
    {
       alert(alertMsg);
       textToVal = textToVal.substring(0,maxlen - 1);
       document.getElementById(textID).value = textToVal;
    }
}
 
function validate()
{   
    if (document.getElementById("ctl00_ContentPlaceHolder1_LocType").value=="txt")
    {
        if(document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value=='' && document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").value=='')
            {
            alert('Please select the supplier company and billing location')
            return;
            }
        else if(document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value=='')
            {
            alert('Please select the supplier company')
            return;
            }
        else if(document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").value=='')
            {
            alert('Please select the billing location')
            return;
            }
        else
           ValidateAutoSuggestSelection();
                      
    }
    else if (document.getElementById("ctl00_ContentPlaceHolder1_LocType").value=="ddl")
    {
            if(document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value=='')
            {
            alert('Please select the supplier company')
            return;
            }
           else
           ValidateAutoSuggestSelection();
    }
}

function NoBillLocOrdermatch()
{
 alert('Ordermatch will not be done for workorder having no billing location');
}

///*****************************************************Function For List Box Selection Starts Here *******************************************

function addToList(SourceListID, TargetListID, HiddenFieldID,Type)
{

    arr=new Array();	
    var opts = document.getElementById(SourceListID).options
    found = true
    while (found)
	{
	    found=false
	    for (j=0;j<opts.length;j++)
	    {
	        if (opts[j].selected==true)
        	{
	            moveListItem(j,opts[j],SourceListID, TargetListID)
	            found=true;
	        }
	    }
	}
    if(Type == 'Add')
    {    
      addToHiddenFromList(HiddenFieldID,TargetListID)		
    }
    else
    {    
       addToHiddenFromList(HiddenFieldID,SourceListID)		
    }        
}
	
function moveListItem(I,optsI,SourceListID, TargetListID)
{
    SourceList=document.getElementById(SourceListID);
    TargetList=document.getElementById(TargetListID);
    opt = new Option(optsI.text,optsI.value);
    TargetList.options[TargetList.options.length]=opt;
    SourceList.options[I]=null;
}
function addToHiddenFromList(HiddenFieldID, SourceListID)			
{
	SourceList=document.getElementById(SourceListID);
	HiddenField=document.getElementById(HiddenFieldID);
	HiddenField.value=""
	var opts = SourceList.options
	for (j=0;j<opts.length;j++)
	{
	    HiddenField.value = HiddenField.value + opts[j].value + ","		
	}
	if (opts.length!=0)
	{
	    HiddenField.value=HiddenField.value.substring(0,HiddenField.value.length-1)
	}
}

function DisableButton(anchorId,btnId)			
{

    if(document.getElementById(anchorId).disabled != true)
    { 
        disableAllCancelBtns();
       
      document.getElementById(btnId).click();
    }
}
function disableAllCancelBtns()
{    
    if(document.getElementById('ctl00_ContentPlaceHolder1_AnchorRematch') != null)
    {   
        document.getElementById('ctl00_ContentPlaceHolder1_AnchorRematch').disabled = true;
        document.getElementById('ctl00_ContentPlaceHolder1_AnchorCopy').disabled = true;
    }
    document.getElementById('ctl00_ContentPlaceHolder1_AnchorSubmit').disabled = true;    
}

function CheckOWSpecialInstruction(Id)
 {
     if(document.getElementById(Id).value != "")
    {
        createCustomAlert("Are you sure you want to notify Orderwork with these instructions?",Id)	    
        removeHTML(Id); 
        validateMaxLen(Id, '700', 'Special instructions to Orderwork can have max 700 characters.');
    }
 } 

// constants to define the title of the alert and button text.
var ALERT_TITLE = "OrderWork Confirmation";
var ALERT_BUTTON_YES_TEXT = "Yes";
var ALERT_BUTTON_NO_TEXT = "NO";

// over-ride the alert method only if this a newer browser.
function createCustomAlert(txt,Id) {
	// shortcut reference to the document object
	d = document;

	// if the modalContainer object already exists in the DOM, bail out.
	if(d.getElementById("modalContainer")) return;

	// create the modalContainer div as a child of the BODY element
	mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
	mObj.id = "modalContainer";
	 // make sure its as tall as it needs to be to overlay all the content on the page
	mObj.style.height = document.documentElement.scrollHeight + "px";

	// create the DIV that will be the alert 
	alertObj = mObj.appendChild(d.createElement("div"));
	alertObj.id = "alertBox";
	// MSIE doesnt treat position:fixed correctly, so this compensates for positioning the alert
	if(d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
	// center the alert box
	alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth)/2 + "px";

	// create an H1 element as the title bar
	h1 = alertObj.appendChild(d.createElement("h1"));
	h1.appendChild(d.createTextNode(ALERT_TITLE));

	// create a paragraph element to contain the txt argument
	msg = alertObj.appendChild(d.createElement("p"));
	if (msg.innerHTML == "")
	msg.innerHTML = txt;
	else
	msg.innerHTML = "<br>" + txt;
	
    // create an anchor element to use as the confirmation button.
	btn = alertObj.appendChild(d.createElement("a"));
	btn.id = "yesBtn";
	btn.appendChild(d.createTextNode(ALERT_BUTTON_YES_TEXT));
	btn.href = "#";
	// set up the onclick event to remove the alert when the anchor is clicked
	btn.onclick = function() { YesAlert();return false; }	

	// create an anchor element to use as the confirmation button.
	btn = alertObj.appendChild(d.createElement("a"));
	btn.id = "noBtn";
	btn.appendChild(d.createTextNode(ALERT_BUTTON_NO_TEXT));
	btn.href = "#";
	// set up the onclick event to remove the alert when the anchor is clicked
	btn.onclick = function() { NoAlert(Id);return false; }	
}
// Yes action and then removes the custom alert from the DOM
function YesAlert() {
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
}

// removes the custom alert from the DOM
function NoAlert(Id) {
     document.getElementById(Id).value = "";
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
}




function ShowVettingListBox(TableID,Action)
{    

    if(Action == "Yes")
    {
     if(TableID == "ctl00_ContentPlaceHolder1_UCSpecialistsForm1_tblCRBListbox")
     {
      document.getElementById(TableID).style.display = "inline-table";
      document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivCRBCheckCalender").style.display = "inline";
      document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivCRBCheckFileUpload").style.display = "inline";
     }  
     if(TableID == "ctl00_ContentPlaceHolder1_UCSpecialistsForm1_tblSecuCheckListbox")
     {
      document.getElementById(TableID).style.display = "inline";
      document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivUKSecuCalender").style.display = "inline";
      document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivUKSecuFileUpload").style.display = "inline";
     }  
     if(TableID == "")
     {      
      document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivCSCSCalender").style.display = "inline";
      document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivCSCSFileUpload").style.display = "inline";
     }  
     
    }
    if(Action == "No")
    {
      
      if(TableID == "ctl00_ContentPlaceHolder1_UCSpecialistsForm1_tblCRBListbox")
      {
       document.getElementById(TableID).style.display = "none";
       document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivCRBCheckCalender").style.display = "none";
       document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivCRBCheckFileUpload").style.display = "none";
      }  
      if(TableID == "ctl00_ContentPlaceHolder1_UCSpecialistsForm1_tblSecuCheckListbox")
      {
       document.getElementById(TableID).style.display = "none";
       document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivUKSecuCalender").style.display = "none";
       document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivUKSecuFileUpload").style.display = "none";
      }  
      if(TableID == "")
      {      
       document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivCSCSCalender").style.display = "none";
       document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivCSCSFileUpload").style.display = "none";
      }  
    }   
}


///*****************************************************Function For List Box Selection Ends Here *******************************************

///*****************************************************Function For Tabular AOE selection Starts Here *******************************************

function CheckAOEMainCat(AOEDivId, hdnCombIDs) {
    var objId = AOEDivId.replace("AOE", "");
    var hdnPageName = hdnCombIDs.replace("hdnCombIDs", "hdnPageName");
    var strIDs = document.getElementById(hdnCombIDs).value;
    var SelSubCatCombId = document.getElementById("hdnSelSubCatCombId" + objId).value;
    var SubCatOfMainCat = document.getElementById("hdnSubCatOfMainCat" + objId).value;
    SelSubCatCombId = SelSubCatCombId.replace("," + objId, "");
    if (document.getElementById(AOEDivId).className == "clsNotSelectedSkillLeftDiv") {
        if (strIDs != "")
        { strIDs = strIDs + "," }
        strIDs = strIDs + objId

        var items=SubCatOfMainCat.split(","); 
        for (var i = 0; i < items.length; i++) 
        {
          var item = items[i];          
          if (strIDs != "")
          {
            if (strIDs.indexOf("," + item)  == -1)
            {
               strIDs = strIDs + "," + item
               if (SelSubCatCombId != "")
              { SelSubCatCombId = SelSubCatCombId + "," }
                SelSubCatCombId = SelSubCatCombId + item
            }
          }
          else
          {
            strIDs = strIDs + item
          }
          document.getElementById("AOE" + item).className = "clsSelectedSkillDiv";
        }

        document.getElementById(AOEDivId).className = "clsSelectedSkillLeftDiv";
    }
    else {

//        if(document.getElementById(hdnPageName).value != "NoMainCatSel")
//        {
            // if(SelSubCatCombId == "0")
           // {
                var str = "," + objId;
                strIDs = strIDs.replace(str, "")

                var items=SubCatOfMainCat.split(","); 
                for (var i = 0; i < items.length; i++) 
                {
                  var item = items[i];
              
                    str = "," + item;
                    strIDs = strIDs.replace(str, "")   
                
                      var strSelSubCatCombId = "," + item;
                     SelSubCatCombId = SelSubCatCombId.replace(strSelSubCatCombId, "");      

                  document.getElementById("AOE" + item).className = "clsNotSelectedSkillDiv";
                }
           
                document.getElementById(AOEDivId).className = "clsNotSelectedSkillLeftDiv";
            //}
//        }
//        else
//        {
//             var str = "," + objId;
//             strIDs = strIDs.replace(str, "")
//           
//             document.getElementById(AOEDivId).className = "clsNotSelectedSkillLeftDiv";
//        }      
       
    }
    document.getElementById(hdnCombIDs).value = strIDs;       
    document.getElementById("hdnSelSubCatCombId" + objId).value = SelSubCatCombId; 
}

function CheckAOESubCat(AOEDivId, hdnCombIDs) {
    var objId = AOEDivId.replace("AOE", "");
    var hdnPageName = hdnCombIDs.replace("hdnCombIDs", "hdnPageName");
    var MainCatCombId = document.getElementById("hdnMainCatCombId" + objId).value;
    var SelSubCatCombId = document.getElementById("hdnSelSubCatCombId" + MainCatCombId).value;
    SelSubCatCombId = SelSubCatCombId.replace("," + MainCatCombId, "")
    var strIDs = document.getElementById(hdnCombIDs).value;
   if (document.getElementById(AOEDivId).className == "clsNotSelectedSkillDiv") {
        if (strIDs != "")
        { 
         strIDs = strIDs.replace("," + MainCatCombId, "")
         strIDs = strIDs + "," 
        }
        strIDs = strIDs + objId
        if(document.getElementById(hdnPageName).value != "NoMainCatSel")
        {
            strIDs = strIDs + "," + MainCatCombId
        }

        if (SelSubCatCombId != "")
        { SelSubCatCombId = SelSubCatCombId + "," }
        SelSubCatCombId = SelSubCatCombId + objId

        document.getElementById(AOEDivId).className = "clsSelectedSkillDiv";
        if(document.getElementById(hdnPageName).value != "NoMainCatSel")
        {
          document.getElementById("AOE" + MainCatCombId).className = "clsSelectedSkillLeftDiv";
        }
        
    }
    else {
        var str = "," + objId;
        strIDs = strIDs.replace(str, "")
        
        var strSelSubCatCombId = "," + objId;
        SelSubCatCombId = SelSubCatCombId.replace(strSelSubCatCombId, "");

        if(SelSubCatCombId == "0")
        {
            document.getElementById("AOE" + MainCatCombId).className = "clsNotSelectedSkillLeftDiv";
            var str = "," + MainCatCombId;
            strIDs = strIDs.replace(str, "")
        }

       

        document.getElementById(AOEDivId).className = "clsNotSelectedSkillDiv";
        
    }
    document.getElementById(hdnCombIDs).value = strIDs;  
    document.getElementById("hdnSelSubCatCombId" + MainCatCombId).value = SelSubCatCombId; 
    //alert(document.getElementById(hdnCombIDs).value);
}

function CloseWindowForAttchments(status)
{
    if(status==9)
    window.opener.location.href="AdminWOListing.aspx?mode=Completed";
    else if(status==10)
    window.opener.location.href="AdminWOListing.aspx?mode=Closed";
    self.close();
}
function handleSPOutstandingEtoE()
{
     window.open(document.getElementById('ctl00_ContentPlaceHolder1_hdnEtoE').value);
    if(document.getElementById('ctl00_ContentPlaceHolder1_hdnStatus').value == "Requested")
    {
        document.getElementById('ctl00_ContentPlaceHolder1_tblExport').style.visibility = "hidden";
        document.getElementById('ctl00_ContentPlaceHolder1_divConfirm').style.visibility = "visible";
    }
}
function handleSPOutstandingBOS()
{
    window.open(document.getElementById('ctl00_ContentPlaceHolder1_hdnBOS').value);
}
function handleReceivablesEtoE()
{
    window.open(document.getElementById('ctl00_ContentPlaceHolder1_hdnEtoE').value);
}
function cancelUpdate()
{
    document.getElementById('ctl00_ContentPlaceHolder1_tblExport').style.visibility = "visible";
    document.getElementById('ctl00_ContentPlaceHolder1_divConfirm').style.visibility = "hidden";
}

function FormatttedAmount(AmtID)
{
    var Amt = document.getElementById(AmtID).value;
    //Remove the commas before rounding
    var Nocommas = ReplaceCharacterInString(Amt,",","");
    Amt = RoundAmounts(Nocommas);
    
    if(isNaN(Amt))
    {
        Amt = 0;
    }
    //Check for negative
    if (parseFloat(Amt) < 0)
    {
       Amt = 0.0000;
    }
    //Add commas to the string
    Amt = numberFormat(Amt,2);
    //alert(Amt)
    document.getElementById(AmtID).value = Amt;
    
}

function numberFormat(nStr,digitCount)
{ 
    nStr += ''; 
   //Get the index (existence) of a decimal point in the number entered
    var SearchDecimalpoint = nStr.indexOf(".");

    //Split the number by "."
    x = nStr.split('.');  
    //Get the value of first index from the array - the value before the decimal point
    x1 = x[0]; 
    x2=''; 
    //Check if the decimal point exists or not
    if (SearchDecimalpoint != -1)

    {

        //Yes the decimal point exists, so we can go ahead with our logic

        //Gives you the the value after decimal point 

        x2 = x.length > 1 ? '.' + x[1] : '';
       
        //Run a loop on the length of the numbers after decimal point

        for(var i = 0; i <= x2.length-1; i++)
        {
            //Check if the length of numbers after decimal point
            //Does not exceed the actual digitCount
            if((x2.length-1) < digitCount)
            {

                //Yes the length is less than the digit count

                //We can go ahead and append zeroes based on the digit count specified
                x2 = x2 + 0;
               //This loop will append zeroes till the length of the numbers is exhausted
            }
           //alert("Manipulated value of x2: " + x2)
        }
    } 
    else
    {
        //This means that the entered number does not contain any decimal point.
      //go ahead and append a decimal point with zeroes
      if (x1 == 0 || x1 == '')
      {
         x2 = '';
      }
      else
      {
        if (digitCount == 2)
        x2 = '.' + '00';
        if (digitCount == 4) 
        x2 = '.' + '0000';  
         if (digitCount == 7) 
        x2 = '.' + '0000000';   
      }
    }

    //Checks the availability of a three numbers before the decimal point before 
    var rgx = /(\d+)(\d{3})/;  
    while (rgx.test(x1))    //While the expression passes and returns true
    x1 = x1.replace(rgx, '$1' + ',' + '$2');  //Go ahead and append comma after the third number
    return (x1 + x2);

}
function ReplaceCharacterInString(InputString,ReplaceChar,ReplaceToChar)
{
    
    while(InputString.indexOf(ReplaceChar) != -1 )
    {
        InputString = InputString.replace(ReplaceChar,ReplaceToChar);
    }
    return InputString
}

function RoundAmounts(Amount)
{
          
               	//Round the number to 2 decimal places
               	
  		 		var RoundedAmount = roundNumber(Amount,2);
  		 		return RoundedAmount;
  		 		
}

function EnableDisableFields(chkboxid,obj1id,obj2id,fld1,fld2,FldMandatory)
{
       if (document.getElementById(chkboxid).checked == true)
      {     
           document.getElementById(obj1id).enabled = true;
           if(obj2id!='')  
           document.getElementById(obj2id).enabled = true;
           document.getElementById(fld1).style.display="inline";
           if(fld2!='')  
           document.getElementById(fld2).style.display="inline";  
           if(FldMandatory!='')
           document.getElementById(FldMandatory).disabled = false; 
          
      }
      else
      {
            document.getElementById(obj1id).enabled = false;
            if(obj2id!='')
            document.getElementById(obj2id).enabled = false;
            document.getElementById(fld1).style.display="none";  
            if(fld2!='')  
           document.getElementById(fld2).style.display="none"; 
           if(FldMandatory!='')
           {
           document.getElementById(FldMandatory).checked = false; 
           document.getElementById(FldMandatory).disabled = true; 
           }
      }
       
}
function ValidateDiscount(txtBoxID)
{

                if (parseFloat(ReplaceCharacterInString(document.getElementById(txtBoxID).value,",","")) > 100)
                {
                    alert("Discount percentage cannot be greater than 100");
                    document.getElementById(txtBoxID).value = numberFormat(10.00,2);
                }
                
                if (parseFloat(ReplaceCharacterInString(document.getElementById(txtBoxID).value,",","")) <= 0)
                {
                    alert("Discount percentage cannot be less than or equal to 0");
                    document.getElementById(txtBoxID).value = numberFormat(10.00,2);
                }
                
                
}
function ValidateFees()
{
/*for product details*/
{
                if (parseFloat(ReplaceCharacterInString(document.getElementById('UCAdminProduct1_txtSPFee').value,",","")) > 100)
                {
                    alert("Service partner fees percentage cannot be greater than 100");
                    document.getElementById('UCAdminProduct1_txtSPFee').value = 0;
                }
                
                if (parseFloat(ReplaceCharacterInString(document.getElementById('UCAdminProduct1_txtSPFee').value,",","")) < 0)
                {
                    alert("Service partner fees percentage cannot be less than or equal to 0");
                    document.getElementById('UCAdminProduct1_txtSPFee').value = 0;
                }
}
}

///*****************************************************Function For Tabular AOE selection Ends Here *******************************************

//date1 & date2in dd/mm/yyyy format
function myDatediff(date1, date2, interval) {
    var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
    var date1Parts = date1.split("/");
    var date2Parts = date2.split("/");
    date1 = new Date(date1Parts[2], date1Parts[1] - 1, date1Parts[0]);
    date2 = new Date(date2Parts[2], date2Parts[1] - 1, date2Parts[0]);
    var timediff = date2 - date1;
    if (isNaN(timediff)) return 0;
    switch (interval) {
        case "years": return date2.getFullYear() - date1.getFullYear();
        case "months": return (
            (date2.getFullYear() * 12 + date2.getMonth())
            -
            (date1.getFullYear() * 12 + date1.getMonth())
        );
        case "weeks": return Math.floor(timediff / week);
        case "days": return Math.floor(timediff / day);
        case "hours": return Math.floor(timediff / hour);
        case "minutes": return Math.floor(timediff / minute);
        case "seconds": return Math.floor(timediff / second);
        default: return undefined;
    }
}
//----------------------------------------Uppercase--------------------------------------------------
function makeUppercase(ID) {
    var x = ID;
    var text = document.getElementById(ID).value.toUpperCase(); 
   document.getElementById(ID).value = text;
   }