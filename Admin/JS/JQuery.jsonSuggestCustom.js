﻿    
var keycode;
var idArr = new Array();
function getCompanyName(myval)
{   if (/^13$|^9$|^27$|^38$|^40$/.test(keycode))
        return false;
    else
    $.ajax({
        type:"POST",
        url:"WOForm.aspx/GetCompanyName",
        data: "{'prefixText':'"+myval+"'}",
        contentType: "application/json; charset=utf-8",
        dataType:"json",        
        success:function(data)
        {
            idArr = new Array();
            var json = JSON.stringify(data);                
            $('#searchResult').html("");
            var sresult = "<table id='tbl'>";
            var loop = data.d;
            $.each(loop.rows, function (i, rows) {
            if (myval.length != 0)
            {var startIndex = parseInt(rows.Company.search(new RegExp(myval, "i" )));
            var endIndex = parseInt(rows.Company.search(new RegExp(myval, "i" ))) + parseInt(myval.length);
            var existingWord = rows.Company.substring(startIndex, endIndex);            
            sresult += "<tr id=" + rows.ContactID + " onclick='javascript:callback(this.id)' onmouseover='javascript:highlight(this.id)'  ><td>"
                        + rows.Company.replace(new RegExp(myval, "gi" ), '<span class="txthighlight">'+existingWord+'</span>') + "</td></tr>";}
            else
                sresult += "<tr id=" + rows.ContactID + " onclick='javascript:callback(this.id)' onmouseover='javascript:highlight(this.id)'  ><td>"
                        + rows.Company + "</td></tr>";
            idArr[i] = rows.ContactID;
            });
            sresult += "</table>";                
            results = sresult;
            $('#searchResult').append(sresult);
            $('#searchResult').slideDown("slow");
        },
              error: function(XMLHttpRequest, textStatus, errorThrown) 
        {            
	        var jsonError = JSON.parse(XMLHttpRequest.responseText);            
	        alert(JSON.stringify(jsonError));
        }});    
}

function GetSPForFavourite(myval)
{
   if (/^13$|^9$|^27$|^38$|^40$/.test(keycode))
        return false;
    else
    $.ajax({
        type:"POST",
        url:"FavouriteSupplierList.aspx/GetSPForFavouriteAutoSuggest",
        data: "{'prefixText':'"+myval+"', 'mode':'" + document.getElementById("ctl00_ContentPlaceHolder1_hdnMode").value + "'}",
        contentType: "application/json; charset=utf-8",
        dataType:"json",        
        success:function(data)
        {   
            idArr = new Array();
            var json = JSON.stringify(data);                
            $('#searchResult').html("");
            var sresult = "<table id='tbl'>";
            var loop = data.d;
            if (loop.rows != null)
             {
                
                $.each(loop.rows, function (i, rows)
             {
            if (myval.length != 0)
            {
            var startIndex = parseInt(rows.Company.search(new RegExp(myval, "i" )));
            var endIndex = parseInt(rows.Company.search(new RegExp(myval, "i" ))) + parseInt(myval.length);
            var existingWord = rows.Company.substring(startIndex, endIndex);             
            sresult += "<tr id=" + rows.ContactID + " onclick='javascript:callbackForFavSP(this.id)' onmouseover='javascript:highlight(this.id)'  ><td>"
                        + rows.Company.replace(new RegExp(myval, "gi" ), '<span class="txthighlight">'+existingWord+'</span>') + "</td></tr>";}
            else
                sresult += "<tr id=" + rows.ContactID + " onclick='javascript:callbackForFavSP(this.id)' onmouseover='javascript:highlight(this.id)'  ><td>"
                        + rows.Company + "</td></tr>";
            idArr[i] = rows.ContactID;
            });}
            else
                sresult += "<tr><td></td></tr>";
            sresult += "</table>";                
            results = sresult;
            $('#searchResult').append(sresult);
            $('#searchResult').slideDown("slow");
        },
              error: function(XMLHttpRequest, textStatus, errorThrown) 
        {            
	        var jsonError = JSON.parse(XMLHttpRequest.responseText);            
	        alert(JSON.stringify(jsonError));
        }});    
}
function getBillingLocation(myval)
{   if (/^13$|^9$|^27$|^38$|^40$/.test(keycode))
        return false;
    else
    $.ajax({
        type:"POST",
        url:"FavouriteSupplierList.aspx/GetBillingLocationAutoSuggest",
        data: "{'prefixText':'" + myval + "', 'mode':'" + document.getElementById("ctl00_ContentPlaceHolder1_hdnMode").value + "', 'contactId':'" + document.getElementById("ctl00_ContentPlaceHolder1_hdnContactID").value + "'}",
        contentType: "application/json; charset=utf-8",
        dataType:"json",        
        success:function(data)
        {  
            idArr = new Array();
            var json = JSON.stringify(data);                                                    
            $('#searchResultLoc').html("");
            var sresult = "<table id='tbl'>";
            var loop = data.d;
            if (loop.rows != null) 
                {

                    $.each(loop.rows, function (i, rows)
                {
            if (myval.length != 0)
            {var startIndex = parseInt(rows.Location.search(new RegExp(myval, "i" )));
            var endIndex = parseInt(rows.Location.search(new RegExp(myval, "i" ))) + parseInt(myval.length);
            var existingWord = rows.Location.substring(startIndex, endIndex);            
            sresult += "<tr id=" + rows.LocationID + " onclick='javascript:callbackBillingLocation(this.id)' onmouseover='javascript:highlight(this.id)'  ><td>"
                        + rows.Location.replace(new RegExp(myval, "gi" ), '<span class="txthighlight">'+existingWord+'</span>') + "</td></tr>";}
            else
                sresult += "<tr id=" + rows.LocationID + " onclick='javascript:callbackBillingLocation(this.id)' onmouseover='javascript:highlight(this.id)'  ><td>"
                        + rows.Location + "</td></tr>";
            idArr[i] = rows.LocationID;
            });}
            else
                sresult += "<tr><td></td></tr>";
            sresult += "</table>";                
            results = sresult;
            $('#searchResultLoc').append(sresult);
            $('#searchResultLoc').slideDown("slow");
        },
              error: function(XMLHttpRequest, textStatus, errorThrown) 
        {            
	        var jsonError = JSON.parse(XMLHttpRequest.responseText);            
	        alert(JSON.stringify(jsonError));
        }});    
}

function highlight(id)
{normalize();document.getElementById(id).className = "highlight";}
function normalize()
{var i = 0;
if (idArr.length != null)
for(i=0; i<idArr.length; i++)
{if (document.getElementById(idArr[i]) != null)
document.getElementById(idArr[i]).className = "normalize";}
}
function processKey(e) {

// handling up/down/escape requires results to be visible
// handling enter/tab requires that AND a result to be selected
keycode = (window.event)?event.keyCode:e.keyCode
if (/^13$|^9$|^27$|^38$|^40$/.test(keycode)) {
    keycode = e.keyCode;	    
    switch(keycode) {
    case 38:prevRes();	break;	// up
    case 40:nextRes(); break;	// down
    case 27: Hide(); break;	//	escape
    case 9:	case 13:selCurRes();break;	// tab/enter
}}}
function getCurSel() {  

    var selected = 0;
    if ((document.getElementById(idArr[0]) != null) && (idArr.length != null))
	{for(i=0; i<idArr.length; i++)
	    if (document.getElementById(idArr[i]).className == "highlight")
            return idArr[i];            
    return 0;}
    else
        return null;}

function selCurRes() {

	var curRes = getCurSel();
	callback(curRes);}

function nextRes() {
var curRes = getCurSel();	
var flag = false;
normalize();
for(i=0; i<idArr.length - 1; i++)
    if (idArr[i] == curRes)
    {   flag = true;
        document.getElementById(idArr[i+1]).className = "highlight";}
if (flag == false)
    document.getElementById(idArr[0]).className = "highlight";}
function prevRes() {
var curRes = getCurSel();
var flag = false;
normalize();
for(i=1; i<idArr.length; i++)
    if (idArr[i] == curRes)
    {   flag = true;
        document.getElementById(idArr[i-1]).className = "highlight";}
if (flag == false)
document.getElementById(idArr[idArr.length-1]).className = "highlight";
}

function getResults(id)
{var value = document.getElementById(id).value;                                                                                          
getCompanyName(value);
}
function getResultsFavSP(id)
{
var value = document.getElementById(id).value;                                                                                          
GetSPForFavourite(value);
}
function getResultsBillingLoc(id)
{
var value = document.getElementById(id).value;  
getBillingLocation(value);
}

function callback(item)
{if (item != null){document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnContactID").value=item; 
document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_UCSearchContact1_txtContact").value = replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(document.getElementById(item).innerHTML,"<TD>",""),"</TD>",""),'<SPAN class=txthighlight>',''),'</SPAN>',""),'<SPAN class="txthighlight">',""),"&nbsp;"," ")
var str2= document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_UCSearchContact1_txtContact").value;
document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_UCSearchContact1_txtContact").value=str2.replace("&amp;", "&");
$('#searchResult').html("");
document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnCompName").value=document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_UCSearchContact1_txtContact").value;
document.getElementById("searchResult").style.display = "none";}}
function replaceAll(txt, replace, with_this) 
{return txt.replace(new RegExp(replace, 'gi'),with_this);}
function hideSearch()
{$('#searchResult').slideUp("slow");
if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnCompName").value != document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_UCSearchContact1_txtContact").value)
 {alert("Please select a Company from the suggestions provided");
 document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnCompName").value = "";
 document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnContactID").value = "";
 document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_UCSearchContact1_txtContact").value = "";
 }}
 
function callbackForFavSP(item)
{if (item != null){document.getElementById("ctl00_ContentPlaceHolder1_hdnContactID").value=item; 
document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value = replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(document.getElementById(item).innerHTML,"<TD>",""),"</TD>",""),'<SPAN class=txthighlight>',''),'</SPAN>',""),'<SPAN class="txthighlight">',""),"&nbsp;"," ")
var str=document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value;
document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value = str.replace("&amp;", "&");
$('#searchResult').html("");
document.getElementById("ctl00_ContentPlaceHolder1_hdnCompName").value=document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value;
document.getElementById("searchResult").style.display = "none";}}
function hideSearchResultFav()
{setTimeout('hideSearchFav()',350);}
function replaceAll(txt, replace, with_this) 
{return txt.replace(new RegExp(replace, 'gi'),with_this);}
function hideSearchFav() {
    $('#searchResult').slideUp("slow");
   // alert(document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value);
   // alert(document.getElementById("ctl00_ContentPlaceHolder1_hdnCompName").value);
    if (document.getElementById("ctl00_ContentPlaceHolder1_hdnMode").value == "ClientBlacklist") {
        if (document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value != "") {
            if (document.getElementById("ctl00_ContentPlaceHolder1_hdnCompName").value == document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value) {
                document.getElementById("ctl00_ContentPlaceHolder1_hdnBillingLocId").value = "";
                document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").value = "";
                document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").disabled = false
            }
            else {
                document.getElementById("ctl00_ContentPlaceHolder1_hdnBillingLocId").value = "";
                document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").value = "";
                document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").disabled = true
            }
        }
        else {
            document.getElementById("ctl00_ContentPlaceHolder1_hdnBillingLocId").value = "";
            document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").value = "";
            document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").disabled = true
        }
       
    }
}
 
function callbackBillingLocation(item)
{if (item != null){document.getElementById("ctl00_ContentPlaceHolder1_hdnBillingLocId").value=item; 
document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").value = replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(document.getElementById(item).innerHTML,"<TD>",""),"</TD>",""),'<SPAN class=txthighlight>',''),'</SPAN>',""),'<SPAN class="txthighlight">',""),"&nbsp;"," ")
var str1=document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").value;
document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").value=str1.replace("&amp;", "&");
$('#searchResultLoc').html("");
document.getElementById("ctl00_ContentPlaceHolder1_hdnBillingLoc").value=document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").value;
document.getElementById("searchResultLoc").style.display = "none";}}
function hideSearchResult()
{setTimeout('hideSearch()',350);}
function replaceAll(txt, replace, with_this) 
{return txt.replace(new RegExp(replace, 'gi'),with_this);}
function hideSearch() {
    $('#searchResultLoc').slideUp("slow");   
}


function ValidateAutoSuggestSelection() {
    var validateSuccess;
    validateBillingSuccess = 1;
    var validateFavSuccess;
    validateFavSuccess = 1;

    if (document.getElementById("ctl00_ContentPlaceHolder1_hdnBillingLoc") != null) {
        if (document.getElementById("ctl00_ContentPlaceHolder1_LocType").value != "ddl") {
            if (document.getElementById("ctl00_ContentPlaceHolder1_hdnBillingLoc").value != document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").value) {
                validateBillingSuccess = 0;
                document.getElementById("ctl00_ContentPlaceHolder1_hdnBillingLoc").value = "";
                document.getElementById("ctl00_ContentPlaceHolder1_txtBillingLoc").value = "";
            }
        }        
        
    }

    if (document.getElementById("ctl00_ContentPlaceHolder1_hdnCompName").value != document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value) {        
        validateFavSuccess = 0;
        document.getElementById("ctl00_ContentPlaceHolder1_hdnCompName").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_hdnContactID").value = "";
        document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value = "";       
    }    

    if (validateBillingSuccess == 0 && validateFavSuccess == 0) {
        alert("Please select a Billing Location and Company Name from the suggestions provided");
    }
    if (validateBillingSuccess == 0 && validateFavSuccess == 1) {
        alert("Please select a Billing Location from the suggestions provided");
    }
    if (validateBillingSuccess == 1 && validateFavSuccess == 0) {
        alert("Please select a Company Name from the suggestions provided");
    }
    if (validateBillingSuccess == 1 && validateFavSuccess == 1) {
        document.getElementById("ctl00_ContentPlaceHolder1_btnAddsupplier").click();
    }
}


//****************Accreditations starts here******************//
function getResultsAccred(id) {
    var txtExpertise = GetClientId("txtExpertise");
    var divMoreInfoAutoSuggest = txtExpertise.replace("txtExpertise", "divMoreInfoAutoSuggest");
    var hdnType = divMoreInfoAutoSuggest.replace("divMoreInfoAutoSuggest", "hdnType");
    var mode = document.getElementById(hdnType).value;
    var hdnCommonID = divMoreInfoAutoSuggest.replace("divMoreInfoAutoSuggest", "hdnCommonID");
    var CommonID = document.getElementById(hdnCommonID).value;
    
    document.getElementById(divMoreInfoAutoSuggest).style.display = "none";
    var value = document.getElementById(id).value;
    GetAccred(value, mode, CommonID);
}

function GetAccred(myval, mode, CommonID) {
       //alert(mode);
    if (/^13$|^9$|^27$|^38$|^40$/.test(keycode))
        return false;
    else
        $.ajax({
            type: "POST",
            url: "CompanyProfile.aspx/GetAccreditationsAutoSuggest",
            data: "{'prefixText':'" + myval + "', 'mode':'" + mode + "', 'CommonID':'" + CommonID + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                idArr = new Array();
                var json = JSON.stringify(data);
                $('#searchResultAccredations').html("");
                var sresult = "<table id='tbl1'>";
                var loop = data.d;
                if (loop.rows != null) {

                    $.each(loop.rows, function (i, rows) {
                        if (myval.length != 0) {
                            var startIndex = parseInt(rows.TagName.search(new RegExp(myval, "i")));
                            var endIndex = parseInt(rows.TagName.search(new RegExp(myval, "i"))) + parseInt(myval.length);
                            var existingWord = rows.TagName.substring(startIndex, endIndex);
                            sresult += "<tr id=Accred" + rows.TagId + " title = " + rows.Type + " onclick='javascript:callbackForAccred(this.id,this.title)' onmouseover='javascript:highlightKeyword(this.id)'  ><td>"
                        + rows.TagName.replace(new RegExp(myval, "gi"), '<span class="txthighlight">' + existingWord + '</span>') + "</td></tr>";
                        }
                        else
                            sresult += "<tr id=Accred" + rows.TagId + " title = " + rows.Type + " onclick='javascript:callbackForAccred(this.id,this.title)' onmouseover='javascript:highlightKeyword(this.id)'  ><td>"
                        + rows.TagName + "</td></tr>";
                        idArr[i] = "Accred" + rows.TagId;
                    });
                }
                else {
                    var TagId = GetClientId("hdnTagID");
                    var hdnType = TagId.replace("hdnTagID", "hdnType");
                    var mode = document.getElementById(hdnType).value;
                    if (mode != "AllAccreditations" && mode != "ServiceAccreditations") {
                        sresult += "<tr title = '' onclick='javascript:AddAccr(this.title)'  style='background-color:#F3F3F3;font-weight:bold;cursor:pointer;'><td style='padding:5px;width:'>Add   " + myval + " as new?</td></tr>";
                    }
                    else {
                        sresult += "<tr><td></td></tr>";
                    }
                }
                sresult += "</table>";
                results = sresult;
                $('#searchResultAccredations').append(sresult);
                $('#searchResultAccredations').slideDown("slow");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var jsonError = JSON.parse(XMLHttpRequest.responseText);
                alert(JSON.stringify(jsonError));
            }
        });
}


function callbackForAccred(item, SelectedType) {
        if (item != null) {
        var TagId = GetClientId("hdnTagID");
        var hdnSelectedType = TagId.replace("hdnTagID", "hdnSelectedType");
        var txtExpertise = TagId.replace("hdnTagID", "txtExpertise");
        var hdnType = txtExpertise.replace("txtExpertise", "hdnType");
        var mode = document.getElementById(hdnType).value;
        var AccrTagId = item.replace("Accred", "");
        document.getElementById(TagId).value = AccrTagId;
        document.getElementById(hdnSelectedType).value = SelectedType;
        document.getElementById(txtExpertise).value = replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(replaceAll(document.getElementById(item).innerHTML, "<TD>", ""), "</TD>", ""), '<SPAN class=txthighlight>', ''), '</SPAN>', ""), '<SPAN class="txthighlight">', ""), "&nbsp;", " ")
        var str = document.getElementById(txtExpertise).value;
        document.getElementById(txtExpertise).value = str.replace("&amp;", "&");
        $('#searchResultAccredations').html("");
        //document.getElementById("ctl00_ContentPlaceHolder1_hdnCompName").value = document.getElementById("ctl00_ContentPlaceHolder1_txtContact").value;
        document.getElementById("searchResultAccredations").style.display = "none";
        //alert(mode);
        if (mode != "AllAccreditations" && mode != "ServiceAccreditations")
            AddAccr('FromAutoSuggest');
        else
            AddAccr('');
    }
}
function hideSearchResultAccred()
{ setTimeout('hideSearchAccred()', 350); }

function hideSearchAccred() {
    $('#searchResultAccredations').slideUp("slow");
}

function AddAccr(type) {
    var TagId = GetClientId("hdnTagID");
    var hdnType = TagId.replace("hdnTagID", "hdnType");
    var mode = document.getElementById(hdnType).value;
    if (mode != "AllAccreditations" && mode != "ServiceAccreditations") {
        callAddAccrButton(type);
    }
    else {
        if (document.getElementById(TagId).value != "") {
            callAddAccrButton(type);
        }
        else {
            alert("Please select from autosuggest")
        }
    }
}
function callAddAccrButton(type) {
    var txtExpertise = GetClientId("txtExpertise");
    var hdnAccrSubmit = txtExpertise.replace("txtExpertise", "hdnAccrSubmit");
    var divMoreInfoAutoSuggest = txtExpertise.replace("txtExpertise", "divMoreInfoAutoSuggest");
    if (type == 'FromAutoSuggest') {
        txtExpertise = document.getElementById(txtExpertise).value;
        var hdnSelectedType = divMoreInfoAutoSuggest.replace("divMoreInfoAutoSuggest", "hdnSelectedType");
        if (document.getElementById(hdnSelectedType).value != "Others") {
            if (txtExpertise != "") {
                document.getElementById(divMoreInfoAutoSuggest).style.display = "block";
            }
            else {
                document.getElementById(divMoreInfoAutoSuggest).style.display = "none";
            }
        }
        else {
            if (txtExpertise != "") {
                if (document.getElementById(divMoreInfoAutoSuggest).style.display == "none")
                    AddAccrMoreInfo();
            }

        }   
    }
    else {
        txtExpertise = document.getElementById(txtExpertise).value;
        if (txtExpertise != "") {
            if (document.getElementById(divMoreInfoAutoSuggest).style.display == "none")
            AddAccrMoreInfo();
        }      
    }
}

function AddAccrMoreInfo() {
    var hdnAccrSubmit = GetClientId("hdnAccrSubmit");
    var divMoreInfoAutoSuggest = hdnAccrSubmit.replace("hdnAccrSubmit", "divMoreInfoAutoSuggest");
    document.getElementById(divMoreInfoAutoSuggest).style.display = "none";
    document.getElementById(hdnAccrSubmit).click();
}


function highlightKeyword(id)
{ normalizeKeyword(); document.getElementById(id).className = "highlightKeyword"; }
function normalizeKeyword() {
    var i = 0;
    if (idArr.length != null)
        for (i = 0; i < idArr.length; i++) {
            if (document.getElementById(idArr[i]) != null)
                document.getElementById(idArr[i]).className = "normalizeKeyword";
        }
}


function GetClientId(strid) {
    var count = document.forms[0].length;
    var i = 0;
    var eleName;
    for (i = 0; i < count; i++) {
        eleName = document.forms[0].elements[i].id;
        pos = eleName.indexOf(strid);
        if (pos >= 0) break;
    }
    return eleName;
}

function HideDivMoreInfo() {
    var txtExpertise = GetClientId("txtExpertise");
    var divMoreInfoAutoSuggest = txtExpertise.replace("txtExpertise", "divMoreInfoAutoSuggest");
    document.getElementById(divMoreInfoAutoSuggest).style.display = "none";
}

//****************Accreditations ends here******************//