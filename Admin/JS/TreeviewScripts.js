function clicktree(nodeindex)
{
    alert("this");
	var tree = window.event.srcElement;
	var node = tree.getTreeNode(nodeindex)	
	tree.selectedNodeIndex = nodeindex;	
	var parent = node.getParent();
	var siblingChecked = false;
	if(parent != null) {
		var children = parent.getChildren();
		if ( children[0] != undefined) {
			for (var i = 0; i < children.length; i++) {
				if(children[i].getAttribute('checked') == true) {
					if(children[i].getAttribute('id') != node.getAttribute('id'))
						siblingChecked = true;
				}
			}
		}
	}	
	setChildNodesState(node, node.getAttribute('checked'))
	if (siblingChecked == false && parent != null && node.getAttribute('checked') != false)
		setParentNode(node, node.getAttribute('checked'))
	var childnodes = node.getChildren();
		if(node.getAttribute('checked') == true)
		{
			if(document.getElementById('UCCategoriesList1_hdnCombIDList').value.indexOf(node.getAttribute('id')) == -1)
			{
				if(document.getElementById('UCCategoriesList1_hdnCombIDList').value != "")
				{
					document.getElementById('UCCategoriesList1_hdnCombIDList').value = document.getElementById('UCCategoriesList1_hdnCombIDList').value + ","
				}
				document.getElementById('UCCategoriesList1_hdnCombIDList').value = document.getElementById('UCCategoriesList1_hdnCombIDList').value + node.getAttribute('id');
			}
			
		}
		else {
			str = document.getElementById('UCCategoriesList1_hdnCombIDList').value
			str = str.replace(node.getAttribute('id') + "," , "")
			str = str.replace(node.getAttribute('id') , "")				
			document.getElementById('UCCategoriesList1_hdnCombIDList').value = str
		}
}


function setChildNodesState(node, state)
{
	var children = node.getChildren();
	var str;
	if ( children[0] != undefined ) {
		for (var i = 0; i < children.length; i++) {
			children[i].setAttribute('checked',state);
			if(state == false) {
				str = document.getElementById('UCCategoriesList1_hdnCombIDList').value
				str = str.replace(children[i].getAttribute('id') + "," , "")			
				document.getElementById('UCCategoriesList1_hdnCombIDList').value = str
			} else 			
			{
			if(document.getElementById('UCCategoriesList1_hdnCombIDList').value.indexOf(children[i].getAttribute('id')) == -1)
			{
				if(document.getElementById('UCCategoriesList1_hdnCombIDList').value != ""){				
					document.getElementById('UCCategoriesList1_hdnCombIDList').value = document.getElementById('UCCategoriesList1_hdnCombIDList').value + ","; }
					
				document.getElementById('UCCategoriesList1_hdnCombIDList').value = document.getElementById('UCCategoriesList1_hdnCombIDList').value + children[i].getAttribute('id');
			}
			}
			setChildNodesState(children[i], state);
		}
	}
}

function setParentNode(node, state)
{
	var parent = node.getParent();
	var siblingChecked = false;
	if(state == false) {
		str = document.getElementById('UCCategoriesList1_hdnCombIDList').value
		str = str.replace(parent.getAttribute('id') + "," , "")			
		document.getElementById('UCCategoriesList1_hdnCombIDList').value = str
	} else 
	{
	if(document.getElementById('UCCategoriesList1_hdnCombIDList').value.indexOf(parent.getAttribute('id')) == -1)
	{
		if(document.getElementById('UCCategoriesList1_hdnCombIDList').value != ""){
			document.getElementById('UCCategoriesList1_hdnCombIDList').value = document.getElementById('UCCategoriesList1_hdnCombIDList').value + ",";}
		document.getElementById('UCCategoriesList1_hdnCombIDList').value = document.getElementById('UCCategoriesList1_hdnCombIDList').value + parent.getAttribute('id');
	}

	}
	if(parent.getParent() != null) {
		var children = parent.getParent().getChildren();
		if (children[0] != undefined ) {
			for (var i = 0; i < children.length; i++) {
				if(children[i].getAttribute('checked') == true) {
					if(children[i].getAttribute('id') != node.getAttribute('id'))
						siblingChecked = true;
				}
			}
		}
		if (siblingChecked == false) {
			setParentNode(parent, state)
		}
	}
		parent.setAttribute('checked',state);
}


function populateTree()
{
      var tree = document.getElementById("UCCategoriesList1_tvCategories")
      children = tree.getChildren()
      if ( children[0] != undefined ) 
            {
                  for (var i = 0; i < children.length; i++) 
                  {
                        children[i].setAttribute('checked',false);
                        //Get Sub
                        subChildren = children[i].getChildren()
                        if ( subChildren[0] != undefined ) 
                        {
                              for (var j = 0; j < subChildren.length; j++) 
                              {
                                    subChildren[j].setAttribute('checked',false);
                                    //Get Low
                                    lowChildren = subChildren[j].getChildren()
                                    if ( lowChildren[0] != undefined ) 
                                    {
                                          for (var k = 0; k < lowChildren.length; k++) 
                                          { 
                                          lowChildren[k].setAttribute('checked',false);
                                          }
                                    }                 
                              }
                        }                 
                  }
            }
      if(document.getElementById("UCCategoriesList1_hdnCombIDList").value != "")
      {
            var str =  document.getElementById("UCCategoriesList1_hdnCombIDList").value            
            //Get Main 
            if ( children[0] != undefined ) 
            {
                  for (var i = 0; i < children.length; i++) 
                  {
                        if(str.indexOf(children[i].getAttribute("id")) > -1)
                        {
                              children[i].setAttribute('checked',true);                              
                        } 
                        //Get Sub
                        subChildren = children[i].getChildren()
                        if ( subChildren[0] != undefined ) 
                        {
                              for (var j = 0; j < subChildren.length; j++) 
                              {
                                    if(str.indexOf(subChildren[j].getAttribute("id")) > -1)
                                    {
                                          subChildren[j].setAttribute('checked',true);
                                    } 
                                    //Get Low
                                    lowChildren = subChildren[j].getChildren()
                                    if ( lowChildren[0] != undefined ) 
                                    {
                                          for (var k = 0; k < lowChildren.length; k++) 
                                          { 
                                                if(str.indexOf(lowChildren[k].getAttribute("id")) > -1)
                                                {     
                                                      lowChildren[k].setAttribute('checked',true);
                                                      setParentNode(lowChildren[k], true)
                                                }
                                          }
                                    }                 
                              }
                        }                 
                  }
            }
      }      
}