﻿// JScript File


var selectedTab = "";

function LinkActivate() 
{
	var pagename = document.URL;
	pagename = pagename.toLowerCase();
	
	if(pagename.indexOf("privacypolicy") != -1)	
		selectedTab = "Privacy"
	
}


function showHide(newTab, height)
{   //alert(" newTab =  " + newTab)
    
    
	var pagename = document.URL;
	pagename = pagename.toLowerCase();
		
	
	if(document.getElementById("div"+newTab)) 
	{   
		if(selectedTab != newTab) 
		{   //alert("selectedTab1 = " + selectedTab)
			resetTabs();	
			//hide previously selected tab
			if(document.getElementById("div"+selectedTab)) 
			{
				document.getElementById("div"+selectedTab).style.visibility = "hidden";
				document.getElementById("div"+selectedTab).style.position = "absolute";
				document.getElementById("div"+selectedTab).style.overflow = "hidden";
				document.getElementById("div"+selectedTab).style.width = "0px";
				document.getElementById("div"+selectedTab).style.height = "0px";				
				document.getElementById("tdtab"+selectedTab).style.cursor = "hand";
				document.getElementById("tdtab"+selectedTab).style.cursor = "pointer";				
			}
			//show selected tab
			document.getElementById("div"+newTab).style.visibility = "visible";
			document.getElementById("div"+newTab).style.position = "relative";
			document.getElementById("div"+newTab).style.height = height;
			//document.getElementById("div"+newTab).style.width = "100%";
			if(!document.all)//for Firefox 
			{ //alert("For FireFox")
				if (pagename.indexOf("spprivacypolicy") != -1)
				    //The specific width is assigened to the page coz it dont have QL region.
					document.getElementById("div"+newTab).style.width = "905px";
				else if (pagename.indexOf("forgotpassword") != -1)
					document.getElementById("div"+newTab).style.width = "318px";
				else
					document.getElementById("div"+newTab).style.width = "680px";
			}
            else
               document.getElementById("div"+newTab).style.width = "100%";
            
            document.getElementById("div"+newTab).style.overflow = "visible";
			document.getElementById("tdtab"+newTab).onmouseout = dummy;
			document.getElementById("tdtab"+newTab).onmouseover =  dummy;
			selectTab(newTab);
			document.getElementById("tdtab"+newTab).style.cursor = "default";
			selectedTab = newTab;
		}		
		//To set the initial position of cursor on first boxes
		 
	
	}
}

function resetTabs() 
{
	var objs = document.getElementsByTagName("td");
	var ObjId;
	var lnkid;
	for (i = 0; i < objs.length; i++) 
	{
		if(objs[i].id.indexOf("tab") != -1)	
		{
			lnkid = objs[i].id;
			lnkid = lnkid.replace("tdtab","");			
			deselectTab(lnkid);
			
		}
	}
}


function selectTab(obj)
{	
	var imgsrc = document.getElementById("imgtab"+obj+"Left").src
	var imgPath = imgsrc.substring(0,imgsrc.indexOf("Images/") + 7)
		
	if (document.getElementById("tdtab"+obj).className.indexOf("Red") != -1)
		{colorName = "Red"}
	
		
	if(obj != null) 
	{
		if(selectedTab != obj)
		{ 
			document.getElementById("tdtab"+obj).className="ActiveLink" + colorName;
			document.getElementById("imgtab"+obj+"Left").src= imgPath + "Curves/" + colorName + "Tab-Left.gif";
			document.getElementById("imgtab"+obj+"Right").src= imgPath + "Curves/" + colorName + "Tab-Right.gif";
		}			
	}			
}


function deselectTab(obj)
{   	
	var imgsrc = document.getElementById("imgtab"+obj+"Left").src
	var imgPath = imgsrc.substring(0,imgsrc.indexOf("Images/") + 7)
	
	
	if (document.getElementById("tdtab"+obj).className.indexOf("Red") != -1)
	{
		colorName = "Red"
		curve="S"
	}
	else if (document.getElementById("tdtab"+obj).className.indexOf("Orange") != -1)
	{
		colorName = "Orange"
		curve = "C"
	}
	if(obj != null) 
	{
		document.getElementById("tdtab"+obj).className="NonActiveLink" + colorName;
		document.getElementById("imgtab"+obj+"Left").src= imgPath + "Curves/GreyTab-Left-" + curve + ".gif";
		document.getElementById("imgtab"+obj+"Right").src=imgPath + "Curves/GreyTab-Right-" + curve + ".gif";
	}
}

function dummy(){}



