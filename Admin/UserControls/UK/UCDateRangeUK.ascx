<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCDateRange.ascx.vb" Inherits="Admin.UCDateRange" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<script language="javascript" src="JS/Scripts.js"  type="text/javascript"></script>


<asp:UpdatePanel ID="UpdatePanel2" runat="server"  >
 <ContentTemplate>
<table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">  
  <tr align="left" valign="top">
  <td width="6"></td>
    <td class="formTxt" valign="top" height="16">From Date (dd/mm/yyyy) <span class="bodytxtValidationMsg"></span>
       
    </td>
    <td width="10">&nbsp;</td>
    <td class="formTxt" height="16">To Date (dd/mm/yyyy)  <span class="bodytxtValidationMsg"></span>
    </td>
  </tr>
  <tr valign="top">
  <td width="6"></td>
    <td height="24" align="left" style="cursor:pointer; vertical-align:top;" ><asp:TextBox ID="txtFromDate" TabIndex="1" runat="server" CssClass="formField105"></asp:TextBox>
	            <img alt="Click to Select" src="Images/calendar.gif" id="btnFromDate" style="cursor:pointer; vertical-align:top;" />		    	
              <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnFromDate TargetControlID="txtFromDate" ID="calFromDate" runat="server">
              </cc1:CalendarExtender>
			  
	</td>
	<td width="10">&nbsp;</td>
    <td align="left"><asp:TextBox ID="txtToDate" TabIndex="3" runat="server" CssClass="formField105"></asp:TextBox>            
            <img alt="Click to Select" src="Images/calendar.gif" id="btnToDate" style="cursor:pointer; vertical-align:top;" />
              <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnToDate TargetControlID="txtToDate" ID="calToDate" runat="server">
              </cc1:CalendarExtender>
			  
	</td>
  </tr>
  <tr valign="top">
  <td width="6"></td>
  <td class="formTxt" colspan="3">
      <asp:ValidationSummary ID="valSumm" runat="server"></asp:ValidationSummary>
      <asp:CustomValidator ID="rngDate" runat="server" ErrorMessage="To Date should be greater than From Date" ForeColor="#EDEDEB" ControlToValidate="txtToDate">*</asp:CustomValidator>
      <asp:RegularExpressionValidator ID="regExpFromDate" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtFromDate" ErrorMessage="From Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
      <asp:RegularExpressionValidator ID="regExpToDate" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtToDate" ErrorMessage="To Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
      <asp:RequiredFieldValidator  ForeColor="#EDEDEB" ID="reqFromDate" ControlToValidate="txtFromDate" runat="server" ErrorMessage="Please Enter From Date"></asp:RequiredFieldValidator>
      <asp:RequiredFieldValidator  ForeColor="#EDEDEB" ID="reqToDate" ControlToValidate="txtToDate" runat="server" ErrorMessage="Please Enter To Date"></asp:RequiredFieldValidator>
  </td>    
  </tr>  
</table>

</ContentTemplate>   
</asp:UpdatePanel> 