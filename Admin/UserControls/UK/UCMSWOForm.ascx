<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSWOForm.ascx.vb"
    Inherits="Admin.UCMSWOForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>


<style type="text/css">
     .ajax__calendar_disabled .ajax__calendar_day {background-color:#cccccc;border-color:#ffffff;color:#a6a6a6; width:18px;}
}
 </style>
<%--Poonam - OA-508 OA - Misplaced error message in WO Creation--%>
<asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <script type="text/javascript" >
            $(document).ready(function () {
                removeHTML("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtWOLongDesc");                   

            });
        </script>
          <script type="text/javascript" >
            $(document).ready(function () {
               
                removeHTML("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtClientScope");
                     

            });
        </script>
          <script type="text/javascript" >
            $(document).ready(function () {
               
                removeHTML("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtSpecialInstructions");
                         

            });
        </script>
          <script type="text/javascript" >
            $(document).ready(function () {
               
                removeHTML("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtClientInstru");
                       

            });
        </script>
          <script type="text/javascript" >
            $(document).ready(function () {
               
                removeHTML("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtProductsPur");               

            });
        </script>
    
    
  
        <div id="divValidationVersionNo" class="divValidation" runat="server" visible="false">
            <div class="roundtopVal">
                <img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner"
                    style="display: none" /></div>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="15" align="center">
                        &nbsp;
                    </td>
                    <td width="565" height="15">
                        &nbsp;
                    </td>
                    <td height="15">
                        &nbsp;
                    </td>
                </tr>
                <tr valign="middle">
                    <td width="63" align="center" valign="top">
                        <img src="Images/Icons/Validation-Alert.gif">
                    </td>
                    <td width="565" class="validationText">
                        <asp:Label ID="lblErrorVersionNo" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                    </td>
                    <td width="20">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td height="15" align="center">
                        &nbsp;
                    </td>
                    <td width="565" height="15">
                        <input type="button" id="Button2" name="btnFocus" class="hdnClass" />
                    </td>
                    <td height="15">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <div class="roundbottomVal">
                <img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner"
                    style="display: none" /></div>
        </div>
        <div id="pnlWoSummary" runat="server" style="display: none">
            <div class="mainLocationForm" style="border-bottom: solid 1px #E1E2DD; padding-bottom: 20px;
                width: 100%;">
                <div id="divValidationWOSummary" class="divValidation" runat="server" visible="false">
                    <div class="roundtopVal">
                        <img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner"
                            style="display: none" /></div>
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td height="15" align="center">
                                &nbsp;
                            </td>
                            <td width="565" height="15">
                                &nbsp;
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                        </tr>
                        <tr valign="middle">
                            <td width="63" align="center" valign="top">
                                <img src="Images/Icons/Validation-Alert.gif">
                            </td>
                            <td width="565" class="validationText">
                                <asp:Label ID="lblError1" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                            </td>
                            <td width="20">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="15" align="center">
                                &nbsp;
                            </td>
                            <td width="565" height="15">
                                <input type="button" id="Button1" name="btnFocus" class="hdnClass" />
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <div class="roundbottomVal">
                        <img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner"
                            style="display: none" /></div>
                </div>
                <table width="100%" cellpadding="5" cellspacing="0">
                    <tr>
                        <td class="BorderW HdrText">
                            Date Created
                        </td>
                        <td class="BorderW HdrText">
                            Location Name
                        </td>
                        <td class="BorderW HdrText">
                            Contact
                        </td>
                        <td class="BorderW HdrText">
                            WO Title
                        </td>
                        <td class="BorderW HdrText">
                            Date Start
                        </td>
                        <td class="BorderW HdrText BorderWhiteR">
                            Proposed Price
                        </td>
                    </tr>
                    <tr>
                        <td class="BorderW RowText">
                            <asp:Label ID="lblDateCreated" runat="server"></asp:Label>
                        </td>
                        <td class="BorderW RowText">
                            <asp:Label ID="lblLocName" runat="server"></asp:Label>
                        </td>
                        <td class="BorderW RowText">
                            <asp:Label ID="lblContact" runat="server"></asp:Label>
                        </td>
                        <td class="BorderW RowText">
                            <asp:Label ID="lblWOTitle" runat="server"></asp:Label>
                        </td>
                        <td class="BorderW RowText">
                            <asp:Label ID="lblDateStart" runat="server"></asp:Label>
                        </td>
                        <td class="BorderW RowText BorderWhiteR">
                            <asp:Label ID="lblPrice" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <div style="float: left; width: 100%;">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="txtWelcome">
                        <tr>
                            <td width="30" align="right" valign="top">
                                &nbsp;
                            </td>
                            <td align="left" valign="top" class="bodyTextGreyLeftAligned paddingT10B24MarginT8">
                                <strong>
                                    <asp:Label ID="lblCategory" runat="server"></asp:Label>:</strong>
                            </td>
                            <td width="20">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="txtWelcome">
                        <tr>
                            <td width="30" align="right" valign="top">
                                &nbsp;
                            </td>
                            <td id="tdDGDetailedDesc" align="left" valign="top">
                                <span id="divLongDesc" runat="Server"><strong>Scope of Work: </strong>
                                    <asp:Label ID="lblLongDesc" runat="server"></asp:Label></span><br>
                                <br>
                                <span id="divClientScope" runat="Server"><strong>Client Scope: </strong>
                                    <asp:Label ID="lblClientScope" runat="server"></asp:Label></span>
                                <br>
                                <br>
                                <asp:Panel ID="pnlSpecialInstructions" runat="server">
                                    <strong>Special instructions to the Engineers:</strong>
                                    <asp:Label ID="lblSpecialInstructions" runat="server"></asp:Label>
                                </asp:Panel>
                                <br>
                                <br>
                                <strong>Special instructions to Orderwork:</strong>
                                <asp:Label ID="lblClientInstru" runat="server"></asp:Label>
                                <div id="divConfRetail">
                                    <br>
                                    <span><strong>Sales Agent: </strong>
                                        <asp:Label ID="lblConfSalesAgent" runat="server"></asp:Label></span>
                                </div>
                                <div id="divConfProductsPur">
                                    <br>
                                    <span><strong>
                                        <asp:Label ID="lblProdPur" runat="server" Text="Product Purchased: "></asp:Label></strong><asp:Label
                                            ID="lblConfProdPur" runat="server"></asp:Label></span>
                                </div>
                            </td>
                            <td width="317" align="left" valign="top" class="paddingL23">
                                <div id="divAdditionalInfo">
                                    <div id="divAdditionalInfoTopBand">
                                    </div>
                                    <div id="divAdditionalInfoMiddleBand">
                                        <div id="divRow" class="bodyTextGreyLeftAligned">
                                            <div id="leftText" style="width: 110px;">
                                                Start Date</div>
                                            <div id="divRightText" style="width: 130px;">
                                                :&nbsp;<asp:Label ID="lblStartDate" runat="server"></asp:Label></div>
                                        </div>
                                        <div id="divRow" class="bodyTextGreyLeftAligned" style="padding-top: 5px;">
                                            <div id="leftText" style="width: 110px;">
                                                <asp:Label ID="txtlblEndDate" runat="server" Text="End Date"></asp:Label>&nbsp;</div>
                                            <div id="divRightText" style="width: 130px;">
                                                :&nbsp;<asp:Label ID="lblEndDate" runat="server"></asp:Label></div>
                                        </div>
                                        <div id="divRow" class="bodyTextGreyLeftAligned" style="padding-top: 5px;">
                                            <div id="leftText" style="width: 110px;">
                                                Supply Parts</div>
                                            <div id="divRightText" style="width: 130px;">
                                                :&nbsp;<asp:Label ID="lblSupplyParts" runat="server"></asp:Label></div>
                                        </div>
                                        <div id="divRow" class="bodyTextGreyLeftAligned" style="padding-top: 5px;">
                                            <div id="leftText" style="width: 110px;">
                                                PO Number</div>
                                            <div id="divRightText" style="width: 130px;">
                                                :&nbsp;<asp:Label ID="lblPONumber" runat="server"></asp:Label></div>
                                        </div>
                                        <div id="divRow" class="bodyTextGreyLeftAligned" style="padding-top: 5px;">
                                            <div id="leftText" style="width: 110px;">
                                                Job Number</div>
                                            <div id="divRightText" style="width: 130px;">
                                                :&nbsp;<asp:Label ID="lblJRSNumber" runat="server"></asp:Label></div>
                                        </div>
                                        <div id="divRow" class="bodyTextGreyLeftAligned" style="padding-top: 5px;">
                                            <div id="leftText" style="width: 110px;">
                                                Dress Code</div>
                                            <div id="divRightText" style="width: 130px;">
                                                :&nbsp;<asp:Label ID="lblDressCode" runat="server"></asp:Label></div>
                                        </div>
                                        <div id="divRow" class="bodyTextGreyLeftAligned" style="padding-top: 5px;">
                                            <div id="leftText" style="width: 110px;">
                                                <asp:Label ID="lblConfGoods" runat="server" Text="Location of Goods"></asp:Label></div>
                                            <div id="divRightText" style="width: 130px;">
                                                :&nbsp;<asp:Label ID="lblBusinessDivision" runat="server"></asp:Label></div>
                                        </div>
                                        <div id="divConfirmFreesat">
                                            <div id="divRow" class="bodyTextGreyLeftAligned" style="padding-top: 5px;">
                                                <div id="leftText" style="width: 110px;">
                                                    Freesat Make/Model</div>
                                                <div id="divRightText" style="width: 130px;">
                                                    :&nbsp;<asp:Label ID="lblFreesatMake" runat="server"></asp:Label></div>
                                            </div>
                                        </div>
                                        <div id="div1">
                                        </div>
                                    </div>
                                    <div id="div23">
                                    </div>
                                    <div id="divAdditionalInfoBottomBand">
                                        &nbsp;</div>
                                    <div id="div24">
                                    </div>
                                </div>
                            </td>
                            <td width="30">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="30">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td width="30">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblMessage" runat="server" CssClass="bodytxtRedSmall"></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="5px">
                                &nbsp;
                            </td>
                            <td height="5px">
                                &nbsp;
                            </td>
                            <td height="5px">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="right">
                                <table cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" border="0">
                                    <tr>
                                        <td width="5">
                                            <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                        </td>
                                        <td width="80" class="txtBtnImage">
                                            <asp:LinkButton ID="btnSubmit" runat="server" CssClass="txtListing" TabIndex="61"
                                                CausesValidation="false"> <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Submit &nbsp;</asp:LinkButton>
                                            <input type="button" id="btnSaveBtm" name="btnSaveBtm" class="hdnClass" runat="server" />
                                            <input type="button" id="btnSaveTop" name="btnSaveTop" class="hdnClass" runat="server" />
                                        </td>
                                        <td width="5">
                                            <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right" width="100px">
                                <table cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" border="0">
                                    <tr>
                                        <td width="5">
                                            <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                        </td>
                                        <td width="80" class="txtBtnImage">
                                            <a id="btnEdit" cssclass="txtListing" onclick='javascript:populateWOFOrm()'>
                                                <img src="Images/Icons/Edit.gif" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Edit&nbsp;</a>
                                        </td>
                                        <td width="5">
                                            <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right" width="100px">
                                <table cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" border="0">
                                    <tr>
                                        <td width="5">
                                            <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                        </td>
                                        <td width="80" class="txtBtnImage">
                                            <asp:LinkButton ID="btnExit" runat="server" CausesValidation="false" CssClass="txtListing"> <img src="Images/Icons/Icon-Cancel.gif"  hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
                                        </td>
                                        <td width="5">
                                            <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div id="div25">
                    </div>
                </div>
                <div id="div26">
                </div>
            </div>
        </div>
        <cc1:ModalPopupExtender ID="mdlClientQuestions" runat="server" TargetControlID="btnTemp"
            PopupControlID="pnlConfirm" OkControlID="" CancelControlID="" BackgroundCssClass="modalBackground"
            Drag="False" DropShadow="False">
        </cc1:ModalPopupExtender>
        <asp:Panel runat="server" ID="pnlConfirm" CssClass="pnlConfirm" Style="position: fixed;
            z-index: 11; left: 360.5px; margin-top: 303.5px; top: 278.5px; width: 533px;">
            <div id="divModalParent">
                <div id="divValidationClientQuestions" class="divValidation" runat="server" visible="false">
                    <div class="roundtopVal">
                        <blockquote>
                            <p>
                                <img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner"
                                    style="display: none" /></p>
                        </blockquote>
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="middle">
                            <td width="63" align="center" valign="top">
                                <img src="Images/Icons/Validation-Alert.gif" width="31" height="31">
                            </td>
                            <td class="validationText">
                                <div id="div2">
                                </div>
                                <asp:Label ID="lblValidationClientQuestions" CssClass="bodytxtValidationMsg" Text=""
                                    runat="server"></asp:Label>
                            </td>
                            <td width="20">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <div class="roundbottomVal">
                        <blockquote>
                            <p>
                                <img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner"
                                    style="display: none" /></p>
                        </blockquote>
                    </div>
                </div>
                <span style="font-family: Tahoma; font-size: 14px;"><b>Service Questions</b></span><br />
                <br />
                <asp:Repeater ID="rptClientQuestions" runat="server">
                    <ItemTemplate>
                        <table style="padding: 5px; margin-top: 10px; background-color: White; border: 1px solid #CECBCE;
                            width: 520px;">
                            <tr>
                                <td>
                                    Q<%#Container.dataitem("QuestionNo")%><input type="hidden" id="hdnQuestionNo" runat="server"
                                        value='<%#Container.dataitem("QuestionNo")%>' />&nbsp;<asp:Label ID="lblClientQ"
                                            runat="server" Text='<%#Container.dataitem("ClientQ")%>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id='DivAns<%#Container.dataitem("QuestionNo")%>'>
                                        <asp:Repeater ID="rptClientQuestionAnswers" runat="server">
                                            <ItemTemplate>
                                                <input type="hidden" id="hdnQuestionNo" runat="server" value='<%#Container.dataitem("QuestionNo")%>' />
                                                <input type="hidden" id="hdnQuestionAnsNo" runat="server" value='<%#Container.dataitem("AnsNo")%>' />
                                                <input type="hidden" id="hdnQuestionAnsMessage" runat="server" value='<%#Container.dataitem("Message")%>' />
                                                <input type="hidden" id="hdnQAnsStopBooking" runat="server" value='<%#Container.dataitem("StopBooking")%>' />
                                                <input type="hidden" id="hdnQAnsTotalAnsCount" runat="server" value='<%#Container.dataitem("TotalAnsCount")%>' />
                                                <input type="hidden" id="hdnSelectedProduct" runat="server" value='<%#Container.dataitem("Product")%>' />
                                                <br />
                                                <asp:RadioButton ID="radbtnAnswer" Visible='<%# IIf(Container.DataItem("TotalAnsCount") > 1, "True", "False")%>'
                                                    Text='<%#Container.dataitem("Answer")%>' Style="cursor: pointer;" onclick="Javascript:SetRadioButton(this.id,this)"
                                                    runat="server" />
                                                &nbsp;<asp:TextBox ID="txtSingleAnswer" runat="server" Height="50px" Width="450px"
                                                    Visible='<%# IIf(Container.DataItem("TotalAnsCount") = 1, "True", "False")%>'
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <br />
                                                <asp:Label ID="lblClientQAnsMsg" runat="server" name="ClientQAnsMsg" Style="color: Red;"></asp:Label>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
                <br />
                <div id="divButton" class="divButton" style="width: 85px; float: left; cursor: pointer;">
                    <div class="bottonTopGrey">
                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" href='javascript:validate()' id="ancSubmit"><strong>
                        Submit</strong></a>
                    <div class="bottonBottomGrey">
                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                </div>
                <div id="divButton" class="divButton" style="width: 85px; float: left; margin-left: 20px;
                    cursor: pointer;">
                    <div class="bottonTopGrey">
                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancCancel" href='javascript:CancelClientQ()'>
                        <strong>Cancel</strong></a>
                    <div class="bottonBottomGrey">
                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                </div>
                <br />
                <br />
                <br />
            </div>
        </asp:Panel>
        <asp:Button runat="server" ID="hdnClientQAnsSubmit" OnClick="hdnClientQAnsSubmit_Click"
            CausesValidation="false" Height="0" Width="0" BorderWidth="0" Style="visibility: hidden;" />
        <asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" Style="visibility: hidden;" />
        <asp:Button runat="server" ID="hdnBtnShowHideMdlQuestions" Height="0" Width="0" BorderWidth="0"
            Style="visibility: hidden;" OnClick="hdnBtnShowHideMdlQuestions_Click" CausesValidation="false" />
        <asp:Button runat="server" ID="hdnClientQAnsCancel" OnClick="hdnClientQAnsCancel_Click"
            CausesValidation="false" Height="0" Width="0" BorderWidth="0" Style="visibility: hidden;" />
        <input type="hidden" id="hdnSelectedService" runat="server" value="0" />
        <script type="text/javascript">
            function validate() {
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnClientQAnsSubmit").click();
            }
            function CancelClientQ() {
                document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnClientQAnsCancel").click();
            }
            function validateMaxLenNew(textID, maxlen) {
                var textToVal = document.getElementById(textID).value;
                if (textToVal.length > maxlen) {
                    textToVal = textToVal.substring(0, maxlen - 1);
                    document.getElementById(textID).value = textToVal;
                }
            }
            function SetRadioButton(nameregex, current) {
                var hdnQuestionNo = nameregex.replace("radbtnAnswer", "hdnQuestionNo");
                var hdnQuestionAnsMessage = nameregex.replace("radbtnAnswer", "hdnQuestionAnsMessage");
                var hdnQuestionAnsNo = nameregex.replace("radbtnAnswer", "hdnQuestionAnsNo");
                var hdnQAnsTotalAnsCount = nameregex.replace("radbtnAnswer", "hdnQAnsTotalAnsCount");

                var lblClientQAnsMsg
                if (document.getElementById(hdnQAnsTotalAnsCount).value > 9)
                    lblClientQAnsMsg = hdnQuestionAnsNo.replace("_ctl0" + (document.getElementById(hdnQuestionAnsNo).value - 1) + "_hdnQuestionAnsNo", "_ctl" + (document.getElementById(hdnQAnsTotalAnsCount).value) + "_lblClientQAnsMsg");
                else
                    lblClientQAnsMsg = hdnQuestionAnsNo.replace("_ctl0" + (document.getElementById(hdnQuestionAnsNo).value - 1) + "_hdnQuestionAnsNo", "_ctl0" + (document.getElementById(hdnQAnsTotalAnsCount).value) + "_lblClientQAnsMsg");

                document.getElementById(lblClientQAnsMsg).innerHTML = ""
                var radios = document.getElementById("DivAns" + document.getElementById(hdnQuestionNo).value).getElementsByTagName('input');
                for (var i = 0; i < radios.length; i++) {
                    if (radios[i] != current) {
                        radios[i].checked = false;
                    }
                }
                document.getElementById(lblClientQAnsMsg).innerHTML = document.getElementById(hdnQuestionAnsMessage).value
                current.checked = true;
            }

            function txtUpSellPrice_Changed() {

                if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_ChkIncludeUpSell").checked = true) {
                    if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtUpSellPrice").value > 0) {
                        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_rqtxtCustomerEmail").enabled = true;
                    }
                    else {
                        document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_rqtxtCustomerEmail").enabled = false;
                    }
                }
            }
            function CalculateQuantity() {
                // alert(myDatediff(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInBegin").value, document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInEnd").value, "days"));
                if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInEnd") != null) {
                    //document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtQuantity").value = myDatediff(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInBegin").value, document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInEnd").value, "days") + 1;
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtQuantity").value = GetDaysInfo(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInBegin").value, document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtScheduleWInEnd").value);
                }
                else
                    document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_txtQuantity").value = 1
            }
            //date1 & date2in dd/mm/yyyy format
            function myDatediff(date1, date2, interval) {
                var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
                var date1Parts = date1.split("/");
                var date2Parts = date2.split("/");
                date1 = new Date(date1Parts[2], date1Parts[1] - 1, date1Parts[0]);
                date2 = new Date(date2Parts[2], date2Parts[1] - 1, date2Parts[0]);
                var timediff = date2 - date1;
                if (isNaN(timediff)) return 0;
                switch (interval) {
                    case "years": return date2.getFullYear() - date1.getFullYear();
                    case "months": return (
            (date2.getFullYear() * 12 + date2.getMonth())
            -
            (date1.getFullYear() * 12 + date1.getMonth())
        );
                    case "weeks": return Math.floor(timediff / week);
                    case "days": return Math.floor(timediff / day);
                    case "hours": return Math.floor(timediff / hour);
                    case "minutes": return Math.floor(timediff / minute);
                    case "seconds": return Math.floor(timediff / second);
                    default: return undefined;
                }
            }

            function GetDaysInfo(d0, d1) {
                //alert(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnIsSaturdayDisabled").value);
                //alert(document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnIsSundayDisabled").value);
                var date1Parts = d0.split("/");
                var date2Parts = d1.split("/");
                d0 = new Date(date1Parts[2], date1Parts[1] - 1, date1Parts[0]);
                d1 = new Date(date2Parts[2], date2Parts[1] - 1, date2Parts[0]);
                var ndays = 1 + Math.round((d1.getTime() - d0.getTime()) / (24 * 3600 * 1000));

                if (isNaN(ndays)) return 1;

                var nsaturdays = Math.floor((d0.getDay() + ndays) / 7);
                var snsundays = (2 * nsaturdays + (d0.getDay() == 0) - (d1.getDay() == 6)) - nsaturdays;

                if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnIsSaturdayDisabled").value == "True") {
                    ndays = ndays - nsaturdays;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_UCCreateWorkOrderUK1_hdnIsSundayDisabled").value == "True") {
                    ndays = ndays - snsundays;
                }

                return ndays;
                //        return 2 * nsaturdays + (d0.getDay() == 0) - (d1.getDay() == 6);
            }
        </script>
    </ContentTemplate>
</asp:UpdatePanel>
<div id="pnlWoForm" runat="server" style="display: block;">
    <input type="hidden" id="hdnSubCategoryVal" runat="Server" value="" />
    <input type="hidden" id="hdnSubCategorytxt" runat="Server" value="" />
    <input type="hidden" id="hdnLocationId" runat="Server" value="" />
    <input type="hidden" id="hdnProductId" runat="Server" value="" />
    <input type="hidden" id="hdnContactName" runat="Server" value="" />
    <input type="hidden" id="hdnContactID" runat="Server" value="" />
    <input type="hidden" id="hdnCompName" runat="Server" value="" />
    <input type="hidden" id="hdnCurrency" runat="Server" value="" />
    <input type="hidden" id="hdnBusinessArea" runat="Server" value="" />
    <input type="hidden" id="hdnFreesat" runat="Server" value="" />
    <input type="hidden" id="hdnEvengInst" runat="Server" value="" />
    <input type="hidden" id="hdnInstTime" runat="Server" value="" />
    <input type="hidden" id="hdnWPUplift" runat="Server" value="0" />
    <input type="hidden" id="hdnPPUplift" runat="Server" value="0" />
    <input type="hidden" id="hdnDeduction" runat="Server" value="0" />
    <input type="hidden" id="hdnStartDate" runat="server" value="" />
    <input type="button" id="btnFocus" name="btnFocus" class="hdnClass" runat="server" />
    <input type="hidden" id="hdnWPUPliftPercent" runat="Server" value="0" />
    <input type="hidden" id="hdnPPUpliftPercent" runat="Server" value="0" />
    <input type="hidden" id="hdnEvngInstTime" runat="Server" value="0" />
    <input type="hidden" id="hdnDepotDetails" runat="Server" value="0" />
    <input type="hidden" id="hdnDBWP" runat="Server" />
    <input type="hidden" id="hdnWPcomp" runat="Server" />
    <input type="hidden" id="hdnIsSundayDisabled" runat="Server" value="True" />
    <input type="hidden" id="hdnIsSaturdayDisabled" runat="Server" value="True" />
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="padding-right: 0px;
        padding-left: 0px; padding-bottom: 10px; margin: 0px; padding-top: 0px">
        <tr>
            <td colspan="7">
                <div style="margin-left: 15px; margin-bottom: 10px;">
                    <asp:Panel ID="pnlContact" runat="server" Visible="true">
                        <table width="400" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="25" width="280">
                                    <%@ register tagprefix="uc1" tagname="UCSearchContact" src="~/UserControls/Admin/UCSearchContact.ascx" %>
                                    <uc1:UCSearchContact ID="UCSearchContact1" runat="server" CompanyAutoSuggest="True">
                                    </uc1:UCSearchContact>
                                </td>
                                <td id="tdViewBtn" runat="server" align="right" width="70px">
                                    <div style="margin-left: 10px; cursor: pointer; -moz-border-radius: 5px; border-radius: 5px;
                                        background-color: #993366 !important; color: #FFFFFF; text-align: center;">
                                        <a id="lnkView" runat="server" class="txtButtonRed" onclick="javascript:ValidateAutoSuggest();"
                                            style="cursor: pointer">Select</a>
                                    </div>
                                </td>
                                <td>
                                    <asp:Button ID="btnSelect" Width="0" Height="0" BorderWidth="0" runat="server" Text="Button"
                                        OnClick="btnSelect_Click" CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnlEditCompName" runat="server" Width="280" Visible="false">
                        <div style="float: left">
                            <asp:TextBox ID="txtbxEditCompName" runat="server" Text="" CssClass="formFieldGrey"
                                Width="280"></asp:TextBox></div>
                    </asp:Panel>
                </div>
            </td>
        </tr>
        <tr runat="server" id="trSelectProductList" visible="false">
            <td runat="server" id="tdSelectProduct" width="340">
                <div style="margin-left: 15px; margin-bottom: 10px;">
                    <asp:DropDownList ID="drpdwnProduct" runat="server" onkeydown='javascript:getProductDescription(this.value,"");CalculateQuantity();'
                        onkeyup='javascript:getProductDescription(this.value,"");CalculateQuantity();'
                        onchange='javascript:getProductDescription(this.value,"");CalculateQuantity();'
                        CssClass="formFieldGrey" Width="335">
                    </asp:DropDownList>
                </div>
            </td>
            <td width="90" align="right" valign="top">
                <table cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" border="0">
                    <tr>
                        <td width="5">
                            <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                        </td>
                        <td width="80" class="txtBtnImage">
                            <a id="btnConfirmTop" class="txtListing" onclick="contactValidation('Confirm');"
                                style="cursor: pointer;">
                                <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle"
                                    border="0">&nbsp;Confirm&nbsp;</a>
                        </td>
                        <td width="5">
                            <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                        </td>
                    </tr>
                </table>
            </td>
            <td width="10">
            </td>
            <td align="right" runat="server" id="tdSaveTop" width="140px">
                <table cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" border="0">
                    <tr>
                        <td width="5">
                            <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                        </td>
                        <td width="120" class="txtBtnImage">
                            <a id="AncBtnaveTop" class="txtListing" onclick="contactValidation('Draft');" style="cursor: pointer;">
                                <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0"
                                    align="absmiddle" border="0">&nbsp;Save As Draft&nbsp;</a>
                        </td>
                        <td width="5">
                            <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                        </td>
                    </tr>
                </table>
            </td>
            <td width="10">
            </td>
            <td align="right" width="90" runat="server" id="tdCancelTop" valign="top">
                <table cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" border="0">
                    <tr>
                        <td width="5">
                            <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                        </td>
                        <td width="80" class="txtBtnImage">
                            <asp:LinkButton ID="btnCancelTop" runat="server" CausesValidation="false" CssClass="txtListing"> <img src="Images/Icons/Cancel.gif" width="11" height="11" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Cancel&nbsp;</asp:LinkButton>
                        </td>
                        <td width="5">
                            <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                        </td>
                    </tr>
                </table>
            </td>
            <td width="10">
                <div>
                    <asp:HiddenField ID="hdnPlatformPrice" Value="0" runat="server" />
                    <input type="hidden" id="hdnAMinTrack" runat="server" /></div>
            </td>
            <td valign="top" align="right" width="90">
                <table cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" border="0">
                    <tr>
                        <td width="5">
                            <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                        </td>
                        <td width="80" class="txtBtnImage">
                            <asp:LinkButton ID="btnResetTop" runat="server" CausesValidation="false" CssClass="txtListing"><img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Reset&nbsp;</asp:LinkButton>
                        </td>
                        <td width="5">
                            <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                        </td>
                    </tr>
                </table>
            </td>
            <td width="170">
            </td>
        </tr>
        <tr>
            <td class="formLabelGrey" colspan="6">
                <div style="margin-left: 15px; margin-bottom: 10px;">
                    <asp:Label ID="lblBillingLocation" runat="server" Text="Billing Location " Visible="false"></asp:Label>
                    <asp:DropDownList ID="drpdwnBillingLocation" runat="server" Visible="False" CssClass="formFieldGrey"
                        Width="150" DataTextField="Name" DataValueField="AddressID">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="reqBillLoc" runat="server" ErrorMessage="Please select Billing Location"
                        ForeColor="#EDEDEB" ControlToValidate="drpdwnBillingLocation">*</asp:RequiredFieldValidator>
                </div>
            </td>
        </tr>
    </table>
    <div id="divMainTab" class="tabDivOuter width730">
        <div class="roundtopLightGreyCurve">
            <img src="Images/Curves/LightGrey-LTC.gif" alt="" width="5" height="5" class="corner"
                style="display: none"></div>
        <div id="divProfile" style="height: 100%; width: 700px; padding-left: 10px;">
            <div id="divValidationMain" class="divValidation" runat="server" style="display: none;">
                <div class="roundtopVal">
                    <img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner"
                        style="display: none" /></div>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="15" align="center">
                            &nbsp;
                        </td>
                        <td width="565" height="15">
                            &nbsp;
                        </td>
                        <td height="15">
                            &nbsp;
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td width="63" align="center" valign="top">
                            <img src="Images/Icons/Validation-Alert.gif">
                        </td>
                        <td width="565" class="validationText">
                            <div id="divValidationMsg">
                            </div>
                            <asp:Label ID="lblError" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                            <span class="validationText">
                                <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg"
                                    DisplayMode="BulletList" EnableClientScript="true"></asp:ValidationSummary>
                            </span>
                        </td>
                        <td width="20">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td height="15" align="center">
                            &nbsp;
                        </td>
                        <td width="565" height="15">
                            &nbsp;
                        </td>
                        <td height="15">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div class="roundbottomVal">
                    <img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner"
                        style="display: none" /></div>
            </div>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="35" class="HeadingRed">
                        <strong>
                            <asp:Label ID="lblHeading" runat="server"></asp:Label>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!--WO Details -->
                        <div id="divWODetails" class="divWorkOrder" runat="server">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" />&nbsp;<strong><asp:Label
                                    ID="lblDetails" runat="server" Text="Work Request Details"></asp:Label></strong></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="15">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="352" height="15" class="formLabelGrey" valign="top">
                                                    <asp:Label Text="Work Request Title" runat="server" ID="lblReqTitle"></asp:Label><span
                                                        class="bodytxtValidationMsg">*</span>
                                                    <asp:RequiredFieldValidator ID="rfTitle" runat="server" ErrorMessage="Please enter Work Request title."
                                                        ForeColor="#EDEDEB" ControlToValidate="txtTitle">*</asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtTitle"
                                                        ErrorMessage="Please Enter WOTitle Less than 200 Characters" SetFocusOnError="True"
                                                        ValidationExpression="^[\s\S]{0,200}$" ForeColor="#EDEDEB">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td width="168" height="15" class="formLabelGrey" valign="top">
                                                    <asp:Label ID="lblScheduleBegin" runat="server" Text="Schedule Window Begin"></asp:Label><span
                                                        class="bodytxtValidationMsg">*</span>
                                                    <asp:RequiredFieldValidator ID="RqFldAptDate" Enabled="false" runat="server" ErrorMessage="Please enter Appointment date"
                                                        ForeColor="#EDEDEB" ControlToValidate="txtScheduleWInBegin">*</asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="rqWOBeginDate" ControlToValidate="txtScheduleWInBegin"
                                                        ErrorMessage="Work Request Schedule Window Begin Date should be in DD/MM/YYYY Format"
                                                        ForeColor="#EDEDEB" ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                        runat="server">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td width="160" height="15" class="formLabelGrey" valign="top">
                                                    <asp:Label ID="lblScheduleWinEnd" runat="server" Text="Schedule Window End"></asp:Label>
                                                    <asp:RegularExpressionValidator ID="rqWOEnd" ControlToValidate="txtScheduleWInEnd"
                                                        ErrorMessage="Work Request Schedule Window End Date should be in DD/MM/YYYY Format"
                                                        ForeColor="#EDEDEB" ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                        runat="server">*</asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="rqWODateRange" runat="server" ErrorMessage="Work Request Schedule Window Begin Date should be earlier than Work Request Schedule Window End Date"
                                                        OnServerValidate="custValiDateRange_ServerValidate" ForeColor="#EDEDEB" ControlToValidate="txtScheduleWInEnd">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="352" height="24" valign="top">
                                                    <asp:TextBox ID="txtTitle" runat="server" CssClass="formFieldGrey width330" TabIndex="1"></asp:TextBox>
                                                </td>
                                                <td width="168" height="24" valign="top"> 
                                                    <asp:TextBox ID="txtScheduleWInBegin" runat="server" CssClass="formFieldGrey width120"
                                                        TabIndex="2" onChange="javascript:CalculateQuantity()"></asp:TextBox>
                                                    <img alt="Click to Select" src="~/Images/calendar.gif" id="btnBeginDate" runat="server"
                                                        style="cursor: pointer; vertical-align: middle;" />
                                                    <cc1:OWCalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnBeginDate"
                                                        IsDateSelectableFunction="function x(s, e) { s.set_completedate(holidaydates()); }"
                                                        TargetControlID="txtScheduleWInBegin" ID="calBeginDate" runat="server" OnClientShowing="function x(s, e) { s.set_MinimumAllowableDate(getbegindate()); }">
                                                    </cc1:OWCalendarExtender>
                                                </td>
                                                <td width="160" height="24" valign="top">
                                                    <asp:TextBox ID="txtScheduleWInEnd" runat="server" CssClass="formFieldGrey width120"
                                                        TabIndex="3" onChange="javascript:CalculateQuantity()"></asp:TextBox>
                                                    <img alt="Click to Select" src="~/Images/calendar.gif" id="btnEndDate" runat="server"
                                                        style="cursor: pointer; vertical-align: middle;" />
                                                    <cc1:OWCalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnEndDate"
                                                        TargetControlID="txtScheduleWInEnd" ID="calEndDate" runat="server" OnClientShowing="function x(s, e) { s.set_MinimumAllowableDate(getbegindate()); }">
                                                    </cc1:OWCalendarExtender>
                                                    <asp:DropDownList Visible="false" ID="drpTime" runat="server" CssClass="formFieldGrey"
                                                        Width="100" TabIndex="4" onkeydown='javascript:ondrpTimeChange(this.id);' onkeyup='javascript:ondrpTimeChange(this.id);'
                                                        onchange='javascript:ondrpTimeChange(this.id);'>                                                       
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RqFldAptTime" Enabled="false" runat="server" ErrorMessage="Please select Appointment time"
                                                        ForeColor="#EDEDEB" ControlToValidate="drpTime">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="341" height="15" class="formLabelGrey" valign="top">
                                                    <asp:Label Text="Work Request Type" runat="server" ID="lblReqType"></asp:Label><span
                                                        class="bodytxtValidationMsg">*</span>
                                                    <asp:RequiredFieldValidator ID="rqWOType" runat="server" ErrorMessage="Please enter Work Request type"
                                                        ForeColor="#EDEDEB" ControlToValidate="ddlWOType">*</asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rqWOSubType" runat="server" ErrorMessage="Please enter Sub Category for Work Request type"
                                                        ForeColor="#EDEDEB" ControlToValidate="ddlWOSubType">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td width="163" height="15" class="formLabelGrey" valign="top">
                                                    <asp:Label ID="lblNoOfWOText" runat="server" Visible="false" Text="No. Of WorkOrders"></asp:Label>
                                                    <asp:RegularExpressionValidator ID="regExNoOfWO" runat="server" ForeColor="#EDEDEB"
                                                        ControlToValidate="txtNoOfWO" ErrorMessage="Please enter No. Of WorkOrders between 1 to 9"
                                                        ValidationExpression="^[123456789]$">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td width="148" height="15" class="formLabelGrey" valign="top" rowspan="2">
                                                    <asp:CheckBox runat="server" ID="chkUseSameAddress" Text="Use Same Address for Each Duplicate Work Order"
                                                        Checked="false" ToolTip="By ticking this any changes made to the address on any of the duplicate work orders will directly affect any other work order that uses the same address" />
                                                    <asp:CheckBox runat="server" ID="chkIngoreAutomatch" Text="&nbsp;Ignore AutoMatch"
                                                        CssClass="formLabelGrey" TabIndex="7" Visible="false" Style="clear: both; display: block;">
                                                    </asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td height="28">
                                                    <asp:Panel ID="pnlWOType" runat="server">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="165" height="28" valign="top">
                                                                    <asp:DropDownList ID="ddlWOType" runat="server" onkeydown='javascript:onMainCatChange(this.id);'
                                                                        onkeyup='javascript:onMainCatChange(this.id);' onchange='javascript:onMainCatChange(this.id);'
                                                                        CssClass="formFieldGrey" Width="160" TabIndex="5" AutoPostBack="false">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td width="163" height="28" valign="top">
                                                                    <asp:DropDownList ID="ddlWOSubType" runat="server" CssClass="formFieldGrey" Width="160"
                                                                        onchange='javascript:onSubCatChange(this.id)' TabIndex="6">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                                <td width="163" height="15" valign="top">
                                                    <asp:TextBox ID="txtNoOfWO" runat="server" CssClass="formFieldGrey" TabIndex="8"
                                                        Visible="false" Text="1"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="tblInvoiceTitle" visible="false" runat="server" width="100%" border="0"
                                            cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="352" height="15" class="formLabelGrey" valign="top">
                                                    &nbsp;Sales Invoice Title<span class="bodytxtValidationMsg">*</span>
                                                    <asp:RequiredFieldValidator ID="ReqValInvoiceTitle" runat="server" ErrorMessage="Please enter Sales Invoice Title"
                                                        ForeColor="#EDEDEB" ControlToValidate="txtInvoiceTitle">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td height="15" class="formLabelGrey" valign="top">
                                                    &nbsp;Quantity<span class="bodytxtValidationMsg">*</span>
                                                    <asp:RequiredFieldValidator ID="ReqValQuantity" runat="server" ErrorMessage="Please enter Quantity"
                                                        ForeColor="#EDEDEB" ControlToValidate="txtQuantity">*</asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegExpQuantity" ControlToValidate="txtQuantity"
                                                        ErrorMessage="Quantity should be in decimal Format" ForeColor="#EDEDEB" ValidationExpression="^[1-9]\d*(\.\d+)?$"
                                                        runat="server">*</asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="24" valign="top">
                                                    <asp:TextBox ID="txtInvoiceTitle" MaxLength="600" runat="server" CssClass="formFieldGrey width330"
                                                        TabIndex="8"></asp:TextBox>
                                                </td>
                                                <td height="24" valign="top">
                                                    <asp:TextBox ID="txtQuantity" runat="server" CssClass="formFieldGrey" TabIndex="8"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="15" class="formLabelGrey" valign="top">
                                                    &nbsp;Related WorkOrder<span class="bodytxtValidationMsg"></span>
                                                   
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtRelatedWorkorder"
                                                        ErrorMessage="Please Enter Valid Related Work Order Number" ForeColor="#EDEDEB" ValidationExpression="^\d{11,11}$"
                                                        runat="server"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                              <tr>
                                                <td height="24" valign="top">
                                                    <asp:TextBox ID="txtRelatedWorkorder" runat="server" CssClass="formFieldGrey"
                                                        TabIndex="9"></asp:TextBox>
                                                </td>
                                               
                                            </tr>
                                        </table>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td height="20" class="formLabelGrey">
                                                    Scope of Work<span class="bodytxtValidationMsg">*</span>
                                                    <asp:RequiredFieldValidator ID="rqWOLongDesc" runat="server" ErrorMessage="Please enter Scope of Work"
                                                        ForeColor="#EDEDEB" ControlToValidate="txtWOLongDesc">*</asp:RequiredFieldValidator>
                                                </td>txtSpecialInstructions
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox MaxLength="8000" onChange="validateMaxLenNew(this.id, '8000')" ID="txtWOLongDesc"
                                                        runat="server" CssClass="formFieldGrey width635height142" TextMode="MultiLine" 
                                                        onblur="javascript:removeHTML(this.id);" Rows="4" TabIndex="9"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" class="formLabelGrey">
                                                    Client Scope
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtClientScope" runat="server" CssClass="formFieldGrey width635height142"
                                                        TextMode="MultiLine" MaxLength="8000" onChange="validateMaxLenNew(this.id, '8000')" 
                                                        onblur="javascript:removeHTML(this.id);" Rows="4" TabIndex="10"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="22" valign="bottom" class="formLabelGrey">
                                                    Special instructions to the Engineers
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtSpecialInstructions" runat="server" CssClass="formFieldGrey width635height60"
                                                        TextMode="MultiLine" MaxLength="4000" onChange="validateMaxLenNew(this.id, '4000')" 
                                                        onblur="javascript:removeHTML(this.id);" Rows="4" TabIndex="11"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="22" valign="bottom" class="formLabelGrey" runat="server" id="tdClientSpInst">
                                                    Special instructions to Orderwork
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="tdClientSpInsttxt" runat="server">
                                                    <asp:TextBox ID="txtClientInstru" runat="server" CssClass="formFieldGrey width635height60"
                                                        TextMode="MultiLine" onblur="javascript:CheckOWSpecialInstruction(this.id);"
                                                        Rows="4"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--WO LOC -->
                        <div id="divWOLoc" class="divWorkOrder" runat="server">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" />&nbsp;<strong><asp:Label
                                    Text="Work Request Location" runat="server" ID="lblReqLocation"></asp:Label></strong></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="15">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td height="22" valign="bottom" class="formLabelGrey" style="padding-bottom: 3px"
                                                    runat="server" id="tdSelectLoc">
                                                    Select an existing location for the Work Request
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formLabelGrey">
                                                    <asp:DropDownList ID="ddlLocation" onkeyup='javascript:onLocationChange(this.id);'
                                                        onkeydown='javascript:onLocationChange(this.id);' onchange='javascript:onLocationChange(this.id);'
                                                        DataValueField="AddressID" DataTextField="Name" runat="server" CssClass="formFieldGrey"
                                                        Width="160" TabIndex="12" AutoPostBack="false">
                                                    </asp:DropDownList>
                                                    <a href="CLLocationForm.aspx" style="visibility: hidden">
                                                        <img style="visibility: hidden" src="Images/Icons/Icon-Plus-Sign.gif" title="Add Location"
                                                            width="12" height="12" hspace="0" vspace="0" border="0" align="middle"></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="30" valign="bottom" class="formLabelGrey" style="padding-bottom: 4px"
                                                    runat="server" id="tdTempLoc">
                                                    <asp:Label Text="Or create a Temporary Location:" runat="server" ID="lblTempLoc"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="170" class="formLabelGrey" runat="server" id="tdCompName">
                                                                        <asp:Label ID="lbltxtCompLocName" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                    <td rowspan="2">
                                                                        <table border="0" cellspacing="0" cellpadding="0" runat="server" id="tblContactInfo">
                                                                            <tr>
                                                                                <td width="170" class="formLabelGrey">
                                                                                    First Name<span class="bodytxtValidationMsg"> *</span>
                                                                                    <asp:RequiredFieldValidator ID="RqdFName" runat="server" ForeColor="#EDEDEB" ErrorMessage="Please enter Contact First Name"
                                                                                        ControlToValidate="txtFName">*</asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td width="170" class="formLabelGrey">
                                                                                    Last Name<span class="bodytxtValidationMsg"> *</span>
                                                                                    <asp:RequiredFieldValidator ID="RqdLName" runat="server" ForeColor="#EDEDEB" ErrorMessage="Please enter Contact Last Name"
                                                                                        ControlToValidate="txtLName">*</asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td class="formLabelGrey">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr valign="top">
                                                                                <td width="170">
                                                                                    <asp:TextBox ID="txtFName" TabIndex="13" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                                </td>
                                                                                <td width="170">
                                                                                    <asp:TextBox ID="txtLName" TabIndex="14" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                                </td>
                                                                                <td class="formLabelGrey">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="170" runat="server" id="tdtxtCompName">
                                                                        <asp:TextBox ID="txtName" TabIndex="15" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td height="15" class="formLabelGrey">
                                                                        Postal Code<span class="bodytxtValidationMsg"> *</span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ForeColor="#EDEDEB"
                                                                        ErrorMessage="Please enter Postal Code" ControlToValidate="txtCompanyPostalCode">*</asp:RequiredFieldValidator>
                                                                         <asp:RegularExpressionValidator ID="rgtxtCompanyPostalCode" Display="None" runat="server"
                                                                          ControlToValidate="txtCompanyPostalCode" ErrorMessage="Please enter valid postcode"
                                                                          ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                    <td height="15" class="formLabelGrey">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td height="24" colspan="2">
                                                                        <asp:TextBox ID="txtCompanyPostalCode" TabIndex="16" onkeyup="makeUppercase(this.id)" runat="server" MaxLength="8" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                        <span style="margin-left: 11px;">
                                                                            <asp:Button ID="btnFind" CausesValidation="false" TabIndex="17" CssClass="formField"
                                                                                runat="server" Text="Get Address" Width="75" /></span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblAddList" runat="Server"
                                                                visible="false">
                                                                <tr>
                                                                    <td valign="Top">
                                                                        <asp:ListBox runat="server" ID="lstProperties"  CssClass="formFieldGrey" AutoPostBack="true"
                                                                            Style="width: 324px; height: 250px; margin: 5px 0px;" Visible="true"></asp:ListBox>
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td valign="Top" style="padding-bottom: 10px;">
                                                                        <asp:Label runat="Server" ID="lblErr" CssClass="bodytxtValidationMsg"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td height="15" class="formLabelGrey">
                                                                        Address<span class="bodytxtValidationMsg"> *</span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ForeColor="#EDEDEB"
                                                                            ErrorMessage="Please enter Address" ControlToValidate="txtCompanyAddress">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td height="15">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td height="15" class="formLabelGrey">
                                                                        City<span class="bodytxtValidationMsg"> *</span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ForeColor="#EDEDEB"
                                                                            ErrorMessage="Please enter City" ControlToValidate="txtCity">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td height="15" class="formLabelGrey">
                                                                        County/State
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td height="24" colspan="2">
                                                                        <asp:TextBox ID="txtCompanyAddress" TabIndex="18" runat="server" CssClass="formFieldGrey"
                                                                            Width="320"></asp:TextBox>
                                                                    </td>
                                                                    <td height="24">
                                                                        <asp:TextBox ID="txtCity" TabIndex="19" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                    </td>
                                                                    <td height="24">
                                                                        <asp:TextBox ID="txtCounty" TabIndex="20" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="15" class="formLabelGrey">
                                                                        Phone<%--<span id="spanPhone" runat="server" class="bodytxtValidationMsg"> *</span>--%><asp:Label
                                                                            ID="lblspanPhone" runat="server" Text="*"  class="bodytxtValidationMsg"></asp:Label>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ForeColor="#EDEDEB"
                                                                            ErrorMessage="Please enter Phone" ControlToValidate="txtCompanyPhone">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td height="15" class="formLabelGrey">
                                                                        Fax
                                                                    </td>
                                                                    <td height="15" class="formLabelGrey">
                                                                        Mobile<%--<span id="spanMobile" runat="server" class="bodytxtValidationMsg"> *</span>--%><asp:Label
                                                                            ID="lblspanMobile" runat="server" Text="*" class="bodytxtValidationMsg"></asp:Label>
                                                                        <asp:RequiredFieldValidator ID="ReqMobile" runat="server" ForeColor="#EDEDEB"
                                                                            ErrorMessage="Please enter Mobile" ControlToValidate="txtMobile">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <%--Poonam added on 11/1/2017 - Task -  OA-385 :OA - Add Customer email field to New Workorder booking page--%>
                                                                    <td width="170" class="formLabelGrey">
                                                                        Customer Email
                                                                        <asp:RegularExpressionValidator ID="rgtxtCustomerEmail" Display="None" runat="server"
                                                                            ControlToValidate="txtCustomerEmail" ErrorMessage="Please enter Customer Email in correct format"
                                                                            ValidationExpression="^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,}$"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator ID="rqtxtCustomerEmail" runat="server" ForeColor="#EDEDEB"
                                                                            ErrorMessage="Please specify the email of the customer for the upsell Receipt."
                                                                            ControlToValidate="txtCustomerEmail" Enabled="false">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td height="24">
                                                                        <asp:TextBox ID="txtCompanyPhone" TabIndex="21" runat="server" CssClass="formFieldGrey width150"
                                                                            MaxLength="49"></asp:TextBox>
                                                                    </td>
                                                                    <td height="24">
                                                                        <asp:TextBox ID="txtCompanyFax" TabIndex="22" runat="server" CssClass="formFieldGrey width150"
                                                                            MaxLength="49"></asp:TextBox>
                                                                    </td>
                                                                    <td height="24">
                                                                        <asp:TextBox ID="txtMobile" TabIndex="23" runat="server" CssClass="formFieldGrey width150"
                                                                            MaxLength="49"></asp:TextBox>
                                                                    </td>
                                                                    <td width="170">
                                                                        <asp:TextBox ID="txtCustomerEmail" TabIndex="24" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--WO Pricing -->
                        <div id="divWOPrice" class="divWorkOrder" runat="server">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" />&nbsp;<strong>Pricing</strong></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="10">
                                <tr>
                                    <td>
                                        <asp:Panel runat="server" ID="pnlStageWO" Visible="true">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr valign="top" id="trRDOStagedWO" runat="server">
                                                    <td width="145" class="formLabelGrey">
                                                        <strong>
                                                            <asp:Label ID="lblStageWO" runat="server" Text="Staged Work Order"></asp:Label></strong>
                                                    </td>
                                                    <td width="50" class="formLabelGrey">
                                                        <asp:RadioButton TabIndex="24" runat="server" ID="rdoStagedYes" Text="Yes" CssClass="formLabelGrey"
                                                            GroupName="rdoStatedWO" AutoPostBack="false" onclick="javascript:ActionStagedYes();">
                                                        </asp:RadioButton>
                                                    </td>
                                                    <td class="formLabelGrey">
                                                        <asp:RadioButton TabIndex="25" runat="server" ID="rdoStagedNo" Text="No" CssClass="formLabelGrey"
                                                            GroupName="rdoStatedWO" Checked="true" AutoPostBack="false" onclick="javascript:ActionStagedNo();">
                                                        </asp:RadioButton>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="350" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="145">
                                                        &nbsp;
                                                    </td>
                                                    <td width="50">
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="trHdr" style="display: none;" valign="top" runat="server">
                                                    <td width="145" id="tdPricingMethodLabel" visible="false" runat="server" class="formLabelGrey">
                                                        <asp:Label ID="lblPricingMethod" runat="server" Text="Pricing Method"></asp:Label>
                                                    </td>
                                                    <td width="145" class="formLabelGrey" style="padding-left: 20px;">
                                                        <asp:Label ID="lblEstTimeRqrdHdr" Text="Estimated Time Required" runat="server"></asp:Label>&nbsp;
                                                        <asp:RequiredFieldValidator ID="rfEstimatedTimeInDays" runat="server" ErrorMessage="Please enter Estimated Time Required."
                                                            ForeColor="#EDEDEB" ControlToValidate="txtTitle">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td width="50" class="formLabelGrey">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="trContent" style="display: none;" runat="server" valign="top">
                                                    <td width="145" id="tdPricingMethodDD" runat="server" class="formLabelGrey">
                                                        <asp:DropDownList ID="ddlPricingMethod" runat="server" CssClass="formFieldGrey width150"
                                                            TabIndex="26" AutoPostBack="false" onchange="javascript:ddlPricingMethodChanged();">
                                                            <asp:ListItem Text="Fixed" Value="Fixed"></asp:ListItem>
                                                            <asp:ListItem Text="Daily Rate" Value="DailyRate"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td width="145" class="formLabelGrey" style="padding-left: 20px;">
                                                        <asp:TextBox ID="txtEstTimeRqrd" Text="1" runat="server" onChange="javascript:CalculatePrice(this.id)"
                                                            CssClass="formFieldGrey width140" Style="text-align: right;" TabIndex="27"></asp:TextBox>&nbsp;
                                                    </td>
                                                    <td width="50" class="formLabelGrey">
                                                        <asp:Label ID="lblEstTimeRqrdDay" runat="server" Text="Day(s)"></asp:Label>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="145">
                                                        &nbsp;
                                                    </td>
                                                    <td width="50">
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="trHolidays" style="display: none;" runat="server" valign="top">
                                                    <td width="145" id="td1" runat="server" class="formLabelGrey">
                                                        <asp:CheckBox TabIndex="28" runat="server" ID="chkbxIncludeWeekends" Text="Include Weekends"
                                                            CssClass="formLabelGrey" Checked="false"></asp:CheckBox>&nbsp;
                                                    </td>
                                                    <td width="145" class="formLabelGrey" style="padding-left: 20px;" colspan="2">
                                                        <asp:CheckBox TabIndex="29" runat="server" ID="chkbxIncludeHolidays" Text="Include Bank Holidays"
                                                            CssClass="formLabelGrey" Checked="false"></asp:CheckBox>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="145">
                                                        &nbsp;
                                                    </td>
                                                    <td width="50">
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="pnlWholesalePrice" Visible="True">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr valign="top">
                                                    <td width="165" class="formLabelGrey">
                                                        &nbsp;Wholesale Price (excl. VAT)<span class="bodytxtValidationMsg">*</span>
                                                        <asp:RequiredFieldValidator ID="rqWOValue_WP" runat="server" ErrorMessage="Please enter Wholesale Price"
                                                            ForeColor="#EDEDEB" ControlToValidate="txtSpendLimitWP">*</asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ForeColor="#EDEDEB"
                                                            ControlToValidate="txtSpendLimitWP" ErrorMessage="Please enter a positive number for Wholesale Price"
                                                            ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator>
                                                        <asp:CustomValidator ID="CustProposedPriceValidator" runat="server" ControlToValidate="txtSpendLimitWP"
                                                            OnServerValidate="ServerValidate" ErrorMessage="Please enter a positive number for Wholesale Price">*</asp:CustomValidator>
                                                    </td>
                                                    <td width="163" class="formLabelGrey">
                                                        &nbsp;
                                                    </td>
                                                    <td width="153" class="formLabelGrey">
                                                        &nbsp;
                                                    </td>
                                                    <td width="148" class="formLabelGrey">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td width="165" class="formLabelGrey">
                                                        <asp:RadioButton TabIndex="30" runat="server" ID="rdo0ValueWP" Text="Require Quotation for work"
                                                            CssClass="formLabelGrey" GroupName="rdoValueWP" AutoPostBack="false" onclick='javascript:ZeroValueWPWO("true")'>
                                                        </asp:RadioButton>
                                                    </td>
                                                    <td width="163" class="formLabelGrey">
                                                        <asp:RadioButton Checked="true" TabIndex="31" runat="server" ID="rdoNon0ValueWP"
                                                            Text="Propose a price(excl. VAT)" CssClass="formLabelGrey" GroupName="rdoValueWP"
                                                            AutoPostBack="false" onclick='javascript:ZeroValueWPWO("false")'></asp:RadioButton>
                                                    </td>
                                                    <td width="153">
                                                        <span class="formLabelGrey">&pound;</span>
                                                        <asp:TextBox ID="txtSpendLimitWP" Text="0" runat="server" CssClass="formFieldGrey width140"
                                                            Style="text-align: right" TabIndex="32"></asp:TextBox>
                                                    </td>
                                                    <td width="148" class="formLabelGrey">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="trWholesaleDayJobRate" style="display: none;" runat="Server">
                                                    <td width="153">
                                                        &nbsp;
                                                    </td>
                                                    <td width="165" class="formLabelGrey">
                                                        &nbsp;Wholesale Day/Job Rate
                                                    </td>
                                                    <td style="padding: 3px 0px 0px 0px;" width="163">
                                                        <span class="formLabelGrey">&pound;</span>
                                                        <asp:TextBox ID="txtWholesaleDayJobRate" Text="0.0" runat="server" onChange="javascript:CalculatePrice(this.id)"
                                                            CssClass="formFieldGrey width140" Style="text-align: right" TabIndex="33"></asp:TextBox>
                                                    </td>
                                                    <td width="148">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="165">
                                                        &nbsp;
                                                    </td>
                                                    <td width="163">
                                                        &nbsp;
                                                    </td>
                                                    <td width="153">
                                                        &nbsp;
                                                    </td>
                                                    <td width="148">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="pnlPlatformPrice">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr valign="top">
                                                    <td width="165" class="formLabelGrey">
                                                        &nbsp;Portal Price (excl. VAT)
                                                    </td>
                                                    <td width="163" class="formLabelGrey">
                                                        &nbsp;
                                                    </td>
                                                    <td width="153" class="formLabelGrey">
                                                        &nbsp;
                                                    </td>
                                                    <td width="148" class="formLabelGrey" id="td3" runat="server">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td width="165" class="formLabelGrey">
                                                        <asp:RadioButton TabIndex="34" runat="server" ID="rdo0ValuePP" Text="Require Quotation for work"
                                                            CssClass="formLabelGrey" GroupName="rdoValuePP" AutoPostBack="false" onclick='javascript:ZeroValuePPWO("true");'>
                                                        </asp:RadioButton>
                                                    </td>
                                                    <td width="163" class="formLabelGrey">
                                                        <asp:RadioButton Checked="true" TabIndex="35" runat="server" ID="rdoNon0ValuePP"
                                                            Text="Propose a price(excl. VAT)" CssClass="formLabelGrey" GroupName="rdoValuePP"
                                                            AutoPostBack="false" onclick='javascript:ZeroValuePPWO("false");'></asp:RadioButton>
                                                    </td>
                                                    <td width="153">
                                                        <span class="formLabelGrey">&pound;</span>
                                                        <asp:TextBox ID="txtSpendLimitPP" Text="0" runat="server" CssClass="formFieldGrey width140"
                                                            Style="text-align: right" TabIndex="36"></asp:TextBox>
                                                        <asp:CustomValidator ID="cvPlatformPrice" ErrorMessage="Please enter valid Portal Price"
                                                            runat="server">&nbsp;</asp:CustomValidator>
                                                    </td>
                                                    <td width="148" height="24" align="left" class="formLabelGrey" runat="server" id="tdchkReviewBid"
                                                        style="display: inline;">
                                                        <asp:CheckBox TabIndex="37" runat="server" ID="chkReviewBid" Text="Allow Review Bids"
                                                            CssClass="formLabelGrey"></asp:CheckBox>
                                                    </td>
                                                </tr>
                                                <tr id="trPlatformDayJobRate" style="display: none;" runat="Server">
                                                    <td width="153">
                                                        &nbsp;
                                                    </td>
                                                    <td width="165" class="formLabelGrey">
                                                        &nbsp;Platform Day/Job Rate
                                                    </td>
                                                    <td width="163">
                                                        <span class="formLabelGrey">&pound;</span>
                                                        <asp:TextBox ID="txtPlatformDayJobRate" Text="0.0" runat="server" onChange="javascript:CalculatePrice(this.id)"
                                                            CssClass="formFieldGrey width140" Style="text-align: right" TabIndex="38"></asp:TextBox>
                                                    </td>
                                                    <td width="148">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="165">
                                                        &nbsp;
                                                    </td>
                                                    <td width="163">
                                                        &nbsp;
                                                    </td>
                                                    <td width="153">
                                                        &nbsp;
                                                    </td>
                                                    <td width="148">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" class="formLabelGrey" style="padding-right: 90px; text-align: right;">
                                                        &nbsp;PP Per Rate&nbsp;&nbsp;<span class="formLabelGrey">&pound;</span><asp:TextBox
                                                            ID="txtPPPerRate" Text="0.0" runat="server" CssClass="formFieldGrey width140"
                                                            Style="text-align: right"></asp:TextBox>
                                                        <asp:CustomValidator ID="cvPPPerRate" ErrorMessage="Please enter valid PP Per Rate"
                                                            runat="server">&nbsp;</asp:CustomValidator>
                                                        &nbsp;<asp:DropDownList ID="ddPPPerRate" runat="server" CssClass="formFieldGrey"
                                                            Width="70">
                                                            <asp:ListItem Text="Per Job" Value="Per Job"></asp:ListItem>
                                                            <asp:ListItem Text="Per Hour" Value="Per Hour"></asp:ListItem>
                                                            <asp:ListItem Text="Per Day" Value="Per Day"></asp:ListItem>
                                                            <asp:ListItem Text="Per Week" Value="Per Week"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="165">
                                                        &nbsp;
                                                    </td>
                                                    <td width="163">
                                                        &nbsp;
                                                    </td>
                                                    <td width="153">
                                                        &nbsp;
                                                    </td>
                                                    <td width="148">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <div style="float: left; margin-left: 5px;">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr valign="top">
                                                    <td width="170" class="formLabelGrey" valign="bottom" runat="server" id="tdlblJobNumber">
                                                        Job Number
                                                    </td>
                                                    <td width="170" class="formLabelGrey" valign="bottom">
                                                        &nbsp;PO Number
                                                    </td>
                                                    <td width="150" class="formLabelGrey" valign="bottom">
                                                        &nbsp;Receipt Number
                                                       <%-- <asp:CustomValidator ID="custValidateReceiptNo" ErrorMessage="This Receipt number has already been used, Please use a different Receipt number."
                                                            runat="server" ControlToValidate="txtReceiptNumber">&nbsp;</asp:CustomValidator>--%>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td class="formLabelGrey" valign="top" runat="server" id="tdtxtJobNumber">
                                                        <asp:TextBox ID="txtJRSNumber" MaxLength="15" runat="server" CssClass="formFieldGrey width150"
                                                            TabIndex="39"></asp:TextBox>
                                                    </td>
                                                    <td class="formLabelGrey" valign="top">
                                                        <asp:TextBox ID="txtPONumber" runat="server" CssClass="formFieldGrey width150" TabIndex="40"></asp:TextBox>
                                                    </td>
                                                    <td class="formLabelGrey" valign="top">
                                                        <asp:TextBox ID="txtReceiptNumber" runat="server" CssClass="formFieldGrey width150"
                                                            TabIndex="40"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td colspan="3">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="240" class="formLabelGrey" align="left" valign="middle">
                                                                    <asp:Panel runat="server" ID="pnlStatementOfWork">
                                                                        <%@ Register TagPrefix="uc4" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>
                                                                        <uc4:UCFileUpload ID="UCFileUpload1" runat="server" Control="UCCreateWorkOrderUK1_UCFileUpload1"
                                                                            Type="StatementOfWork" AttachmentForSource="StatementOfWork" ShowNewAttach="True"
                                                                            ShowUpload="True" MaxFiles="1"></uc4:UCFileUpload>
                                                                    </asp:Panel>
                                                                </td>
                                                                <td width="30">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="divClearBoth">
                                        </div>
                                        <!-- UpSell Fields Starts-->
                                        <div style="float: left; width: 100%;">
                                            <table id="tblUpSellFields" width="100%" align="left" border="0" cellspacing="0"
                                                cellpadding="0" style="display: inline;" runat="server">
                                                <tr>
                                                    <td>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" id="tblUpSell" style="display: inline;" runat="Server" border="0"
                                                                        cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td height="15" width="171" class="formLabelGrey">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td id="tdUpSellPricehdr" width="180" runat="server" height="15" class="formLabelGrey"
                                                                                style="display: none;">
                                                                                &nbsp;&nbsp;&nbsp;UpSell Price(excl.VAT)<span class="bodytxtValidationMsg">*</span>
                                                                            </td>
                                                                            <td id="tdUpSellInvoicehdr" runat="server" height="15" class="formLabelGrey" style="display: none;">
                                                                                UpSell Invoice Title
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td height="24" id="tdUpSell" runat="server" width="171">
                                                                                <asp:CheckBox ID="ChkIncludeUpSell" TabIndex="41" runat="server" Text='Include UpSell Price'
                                                                                    CssClass="formLabelGrey" AutoPostBack="false" onclick="javascript:ChkIncludeUpSell_CheckedChanged();">
                                                                                </asp:CheckBox>
                                                                            </td>
                                                                            <td height="24" id="tdUpSellPrice" width="180" runat="server" class="formLabelGrey"
                                                                                style="display: none;">
                                                                                &pound;
                                                                                <asp:TextBox ID="txtUpSellPrice" TabIndex="42" runat="server" Style="text-align: right"
                                                                                    CssClass="formFieldGrey width120" Text="0" OnChange="javascript:txtUpSellPrice_Changed();"></asp:TextBox>
                                                                            </td>
                                                                            <td height="24" id="tdUpSellInvoice" runat="server" style="display: none;">
                                                                                <asp:TextBox ID="txtUpSellInvoiceTitle" TabIndex="43" runat="server" CssClass="formFieldGrey width150"
                                                                                    Text="OrderWork Services"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="24" colspan="3" class="formLabelGrey">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td id="tdChkCCPayeeDetails" height="15" width="171" class="formLabelGrey" style="display: none;">
                                                                                <asp:CheckBox ID="ChkCCPayeeDetails" TabIndex="44" runat="server" Text='CC Payee Details'
                                                                                    CssClass="formLabelGrey" AutoPostBack="false" onclick="javascript:ChkCCPayeeDetailsUpdate();">
                                                                                </asp:CheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="24" colspan="3" class="formLabelGrey">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <asp:RequiredFieldValidator ID="rqUpSellPrice" runat="server" ErrorMessage="Please enter UpSell Price"
                                                                                    ForeColor="#EDEDEB" ControlToValidate="txtUpSellPrice" Enabled="false">*</asp:RequiredFieldValidator>
                                                                                <asp:RegularExpressionValidator ID="reUpSellPrice" runat="server" ForeColor="#EDEDEB"
                                                                                    ControlToValidate="txtUpSellPrice" ErrorMessage="Please enter a positive number for UpSell Price"
                                                                                    ValidationExpression="^(\d)?(\d|,)*\.?\d*$" Enabled="false">*</asp:RegularExpressionValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblContactInfoUpSell"
                                                                        runat="server" style="display: none;">
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                <table id="Table1" border="0" cellspacing="0" cellpadding="0" runat="server">
                                                                                    <tr>
                                                                                        <td width="171" class="formLabelGrey">
                                                                                            CC First Name
                                                                                        </td>
                                                                                        <td width="160" id="tdCCLastName" class="formLabelGrey" runat="server">
                                                                                            CC Last Name
                                                                                        </td>
                                                                                        <td class="formLabelGrey">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr valign="top">
                                                                                        <td width="171">
                                                                                            <asp:TextBox ID="txtBCFName" TabIndex="45" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                                        </td>
                                                                                        <td width="160">
                                                                                            <asp:TextBox ID="txtBCLName" TabIndex="46" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="formLabelGrey">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="15" class="formLabelGrey">
                                                                                CC Address
                                                                            </td>
                                                                            <td height="15">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td height="15" class="formLabelGrey">
                                                                                CC City
                                                                            </td>
                                                                            <td height="15" class="formLabelGrey">
                                                                                CC County/State
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td height="24" colspan="2">
                                                                                <asp:TextBox ID="txtBCAddress" TabIndex="47" runat="server" CssClass="formFieldGrey width330"></asp:TextBox>
                                                                            </td>
                                                                            <td height="24">
                                                                                <asp:TextBox ID="txtBCCity" TabIndex="48" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                            </td>
                                                                            <td height="24">
                                                                                <asp:TextBox ID="txtBCCounty" TabIndex="49" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="15" class="formLabelGrey">
                                                                                CC Postal Code
                                                                            </td>
                                                                            <td height="15" class="formLabelGrey">
                                                                                CC Phone
                                                                            </td>
                                                                            <td height="15" class="formLabelGrey">
                                                                                CC Fax
                                                                            </td>
                                                                            <td height="15" class="formLabelGrey">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td height="24">
                                                                                <asp:TextBox ID="txtBCPostalCode" TabIndex="50" runat="server" CssClass="formFieldGrey width150"></asp:TextBox>
                                                                            </td>
                                                                            <td height="24">
                                                                                <asp:TextBox ID="txtBCPhone" TabIndex="51" runat="server" CssClass="formFieldGrey width150"
                                                                                    MaxLength="49"></asp:TextBox>
                                                                            </td>
                                                                            <td height="24" colspan="2">
                                                                                <asp:TextBox ID="txtBCFax" TabIndex="52" runat="server" CssClass="formFieldGrey width150"
                                                                                    MaxLength="49"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!-- UpSell Fields Ends-->
                                    </td>
                                </tr>
                            </table>
                            <div class="WorkOrderBottomCurve">
                                <img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner"
                                    style="display: none" /></div>
                        </div>
                        <!--WO Additional Info -->
                        <div id="divWOAddInfo" class="divWorkOrder" runat="server">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" />&nbsp;<strong>Additional
                                    Information</strong></div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="15">
                                <tr>
                                    <td>
                                        <table width="640" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="170" height="15" class="formLabelGrey">
                                                    Dress Code
                                                </td>
                                                <td width="270" height="15" class="formLabelGrey">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td width="170" height="24">
                                                    <asp:DropDownList ID="ddlDressCode" DataTextField="StandardValue" DataValueField="StandardID"
                                                        runat="server" CssClass="formFieldGrey width150" TabIndex="53">
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="270" height="15" class="formLabelGrey">
                                                    <asp:CheckBox ID="chkShowLoc" runat="server" Text="Show Company's Main Office Location To Suppliers" />
                                                </td>
                                            </tr>
                                        </table>
                                        <table runat="server" id="tblAdditionalFields" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td height="16" class="formLabelGrey" valign="bottom" runat="server" id="tdGoodsLocation"
                                                    visible="false">
                                                    <asp:Label ID="lblLocGoods" runat="server" Text="Location of Goods"></asp:Label><span
                                                        class="bodytxtValidationMsg"> *</span>
                                                </td>
                                                <td width="170" class="formLabelGrey" valign="bottom" runat="server" id="tdSalesAgent"
                                                    visible="false" align="left">
                                                    Sales Agent
                                                </td>
                                                <td width="100" height="20" class="formLabelGrey" style="vertical-align: text-bottom;"
                                                    valign="bottom" runat="server" id="tdFreesatMake">
                                                    <span style="display: none; width: 110px;" id="divFreesatMake" runat="server">Freesat
                                                        Make/Model<span class="bodytxtValidationMsg"> *</span></span>
                                                </td>
                                                <td width="45">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="170" class="formLabelGrey" valign="top" runat="server" id="tdGoods" visible="false">
                                                    <asp:DropDownList ID="drpdwnGoodsCollection" runat="server" TabIndex="54" DataTextField="Name"
                                                        DataValueField="AddressId" CssClass="formFieldGrey" Width="150" AutoPostBack="False"
                                                        onchange='javascript:PopulateDepotDetails(this.value)'>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvGoodsLocation" runat="server" ErrorMessage="Please enter the location where the specified goods will be located"
                                                        ForeColor="#EDEDEB" ControlToValidate="drpdwnGoodsCollection">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td width="170" class="formLabelGrey" valign="top" runat="server" id="tdSales" visible="false"
                                                    align="left">
                                                    <asp:TextBox runat="server" ID="txtSalesAgent" TabIndex="55" CssClass="formFieldGrey"
                                                        Width="150"></asp:TextBox>
                                                </td>
                                                <td width="170" class="formLabelGrey" valign="top" id="tdFreesatMakeddl" runat="server"
                                                    align="left">
                                                    <div style="display: none;" id="divFreesatMakeddl" runat="server">
                                                        <asp:DropDownList ID="drpdwnFreesatMake" runat="server" TabIndex="56" DataTextField="StandardValue"
                                                            DataValueField="StandardID" CssClass="formFieldGrey" Width="150" AutoPostBack="False">
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                                <td width="45">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="540" class="formLabelGrey" valign="bottom" runat="server" id="tdProductsPur"
                                                    colspan="4" height="20">
                                                    <asp:Label ID="lblProductsPur" runat="server" Text="Product Purchased"></asp:Label>
                                                    <span id="SpanProductsPurRedStar" runat="server" class="bodytxtValidationMsg">*</span>
                                                    <asp:RequiredFieldValidator ID="reqtxtProductsPur" runat="server" ErrorMessage="Please enter Product Purchased"
                                                        ForeColor="#EDEDEB" ControlToValidate="txtProductsPur">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="540" class="formLabelGrey" valign="top" runat="server" id="tdProductsPurTxt"
                                                    colspan="4">
                                                    <asp:TextBox runat="server" ID="txtProductsPur" TabIndex="57" MaxLength="2000" CssClass="formFieldGrey width635height60"
                                                        TextMode="MultiLine" onChange="validateMaxLenNew(this.id, '2000')" onblur="javascript:removeHTML(this.id);"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanelAccred" runat="server">
                            <ContentTemplate>
                                <div class="WorkOrderTopCurve">
                                    <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" />&nbsp;<strong>Accreditations</strong></div>
                                <table width="100%" cellspacing="9" cellpadding="0" border="0">
                                    <tr>
                                        <td style="width: 680px;">
                                            <%@ register tagprefix="uc1" tagname="UCAccreds" src="~/UserControls/Admin/UCAccreditationForSearch.ascx" %>
                                            <uc1:UCAccreds ID="UCAccreditations" runat="server"></uc1:UCAccreds>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanelAttachments" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="hdnbtnTrigger" CssClass="txtListing" runat="server" CausesValidation="false"
                                    Height="0" Width="0" BorderStyle="None" Text=""></asp:Button>
                                <div id="divWOAttachInfo" class="divWorkOrder" runat="server">
                                    <div class="WorkOrderTopCurve">
                                        <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" />&nbsp;<strong>Attachments</strong></div>
                                    <%@ register tagprefix="uc2" tagname="UCFileUpload" src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>
                                    <uc2:UCFileUpload ID="CWOFileAttach" runat="server" Control="UCCreateWorkOrderUK1_CWOFileAttach"
                                        ExistAttachmentSource="CompanyProfile" Type="WorkOrder" AttachmentForSource="WorkOrder"
                                        UploadCount="1" ShowExistAttach="True" ShowNewAttach="True" ShowUpload="True"
                                        MaxFiles="20"></uc2:UCFileUpload>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanelAttachments"
                            runat="server">
                            <ProgressTemplate>
                                <div>
                                    <img align="middle" src="Images/indicator.gif" />
                                    Please wait, Processing...
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="divWOAttachInfo"
                            CssClass="updateProgress" TargetControlID="UpdateProgress2" runat="server" />
                        <div style="padding: 10px;">
                            <asp:CheckBox ID="chkForceSignOff" runat="server" Text="Force Sign off sheet" /></div>
                        <div style="padding: 10px; margin-bottom: 20px;">
                            <asp:CheckBox ID="chkIsWaitingToAutomatch" runat="server" Text="Waiting to go through automatch" /></div>
                        <div class="WorkOrderButtonTop">
                        </div>
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="padding-right: 0px;
                            padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 16px">
                            <tr>
                                <td width="350" align="right" valign="top">
                                    &nbsp;
                                </td>
                                <td width="90" align="right" valign="top">
                                    <table cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" border="0">
                                        <tr>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                            </td>
                                            <td width="80" class="txtBtnImage">
                                                <a id="btnConfirmBtm" class="txtListing" tabindex="58" onclick="contactValidation('Confirm');"
                                                    style="cursor: pointer;">
                                                    <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle"
                                                        border="0">&nbsp;Confirm&nbsp;</a>
                                            </td>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10">
                                </td>
                                <td align="right" runat="server" id="td2" width="140px">
                                    <table cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" border="0">
                                        <tr>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                            </td>
                                            <td width="120" class="txtBtnImage">
                                                <a id="ancBtnSaveBtm" class="txtListing" onclick="contactValidation('Draft');" style="cursor: pointer;">
                                                    <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0"
                                                        align="absmiddle" border="0">&nbsp;Save As Draft&nbsp;</a>
                                            </td>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10">
                                </td>
                                <td valign="top" align="right" width="90">
                                    <table cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" border="0">
                                        <tr>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                            </td>
                                            <td width="80" class="txtBtnImage">
                                                <asp:LinkButton ID="btnCancelBtm" runat="server" CausesValidation="false" TabIndex="59"
                                                    CssClass="txtListing"> <img src="Images/Icons/Cancel.gif" width="11" height="11" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Cancel&nbsp;</asp:LinkButton>
                                            </td>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10">
                                </td>
                                <td valign="top" align="right" width="90">
                                    <table cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" border="0">
                                        <tr>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                            </td>
                                            <td width="80" class="txtBtnImage">
                                                <asp:LinkButton ID="btnResetBtm" runat="server" CausesValidation="false" TabIndex="60"
                                                    CssClass="txtListing"><img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Reset&nbsp;</asp:LinkButton>
                                            </td>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="divBtmBand">
    </div>
</div>
<asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelCreateWorkOrder"
    runat="server">
    <ProgressTemplate>
        <div>
            <img align="middle" src="Images/indicator.gif" />
            Please wait, Processing...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender2" ControlToOverlayID="pnlWoForm"
    CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
