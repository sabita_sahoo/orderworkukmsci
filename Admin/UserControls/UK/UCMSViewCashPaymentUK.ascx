<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSViewCashPaymentUK.ascx.vb" Inherits="Admin.UCMSViewCashPaymentUK" %>
<style type="text/css">
<!--
.invoiceLabel {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 32px;
	color: #B7B7B7;
	font-weight: bold;
	line-height:38px;
}
.InvoiceText {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.bdr1 {
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 4px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.bdr2 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #C6C6C6;
	border-right-color: #C6C6C6;
	border-bottom-color: #C6C6C6;
	border-left-color: #C6C6C6;
}
.bdr3 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.style3 {font-size: 11px}
.style5 {font-weight: bold}
.style6 {font-weight: bold}
td
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 16px;
	color: #000000;
}
body {
	background-color: #CECECE;
	height:100%;
}
-->
</style>
<table height="95%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="45" height="100">&nbsp;</td>
    <td width="650" height="140" valign="top"><table width="100%" cellpadding="0" cellspacing="0" style=" border-width:medium; border-bottom-width:5px;border-bottom-color:#000000;">
        <tr>
          <td width="311">&nbsp;</td>
          <td width="131" align="left" valign="bottom" >&nbsp;</td>
        </tr>
        <tr>
          <td width="311" valign="middle">&nbsp;</td>
          <td align="right" valign="bottom" ><table width="99%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right" valign="bottom"  style="height: 22px;"><span class="style1 style5">
                  <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                </span></td>
              </tr>
              <tr>
                <td align="right" valign="bottom"  ><asp:Label ID="lblAddress" runat="server"></asp:Label></td>
              </tr>
                   <tr>
				<td align="right" valign="bottom" style="height: 15px"  ><asp:Label ID="lblCity" runat="server"></asp:Label><asp:Label ID="lblPostCode" runat="server"></asp:Label>
				</td>
              </tr>
              <tr>
                <td align="right" valign="bottom"  > &nbsp;
                    <span class="AddressTxt">
                    <asp:Label ID="lblCompanyRegNo" runat="server"></asp:Label>
                    </span></td>
              </tr>
              <tr>
                <td valign="bottom" ></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td width="311" class="bdr1" >&nbsp;</td>
          <td align="left" valign="bottom" class="bdr1" >&nbsp;</td>
        </tr>
    </table></td>
    <td width="45" height="100">&nbsp;</td>
  </tr>
  <tr>
    <td width="45" height="180" align="center">&nbsp;</td>
    <td width="650" height="220" align="center" valign="top"><p class="invoiceLabel">Receipt</p>
        <table border="0" cellpadding="5" cellspacing="0" width="100%">
          <tr>
            <td align="left" valign="top" style="border-bottom: 0px; border-right-width: 0px"><strong>Invoice To:<br>
              </strong>
                <asp:Label ID="lblCompNameInvoiceTo" runat="server"></asp:Label>
                <br />
                <asp:Label ID="lblInvoiceTo" runat="server"></asp:Label>
&nbsp;</td>
            <td align="right" valign="top">&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0">
          <tr >
            <td width="259" align="Left"  ></td>
            <td width="185">&nbsp;</td>
            <td align="right" width="200"  ><strong> Date</strong></td>
          </tr>
          <tr >
            <td width="259">&nbsp;</td>
            <td width="185">&nbsp;</td>
            <td align="right" width="200" ><asp:Label ID="lblInvoiceDate" runat="server"></asp:Label></td>
          </tr>
    </table>        <p>&nbsp;</p></td>
    <td width="45" height="180" align="center">&nbsp;</td>
  </tr>
  <tr >
    <td width="45" height="40%" align="right">&nbsp;</td>
    <td width="650" height="45%" align="right" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td  align="left"  style=" border-bottom:0px; border-right-width:0px; " ><strong>Description</strong><strong></strong></td>
        <td width="98" align="right"  style=" border-bottom:0px;"><strong>Amount</strong></td>
      </tr>
      <asp:Repeater runat="server"  ID="rptList" Visible="true">
        <itemtemplate>
          <tr>
            <td height="30" align="left"  style="border-right-width:0px;" ><%#Container.DataItem("Description")%>&nbsp;&nbsp;</td>
            <td width="98" height="30"   align="right"  >&nbsp;<%#FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%></td>
          </tr>
        </itemtemplate>
      </asp:Repeater>
    </table>
      <table border="0" cellspacing="0">
          <tr>
            <td width="104" height="50" align="right"   style="border-right-width:0px; "><strong>Total Amount </strong></td>
            <td width="98" height="40"  align="right" ><span class="style6">
              <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
            </span> </td>
        </tr>
      </table>
    </td>
    <td width="45" height="20" align="right">&nbsp;</td>
  </tr>
</table>
