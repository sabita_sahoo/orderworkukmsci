<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCDualListBox.ascx.vb" Inherits="Admin.UCDualListBox" %>

  <asp:Panel ID="PnlDualListBox" runat="server"  >
 <asp:UpdatePanel ID="UpdatePnlDualListBox" runat="server"  UpdateMode="Conditional" RenderMode=Inline>
  <ContentTemplate>


<TABLE cellSpacing="0" cellPadding="0" border="0">
		<TR vAlign="top">
			<TD width=205 valign="Top"  align="left" id="tdOptions" runat=server>
				<asp:ListBox  class="formFieldWhite width202height110" id="lstDeSelected" runat="server" Rows="10" SelectionMode="multiple"></asp:ListBox>
			</TD>
			
			<TD width=90 id="tdButtons" runat=server align="left" height="24"  class="paddingT4" >
				
				<TABLE cellSpacing="0" cellPadding="0" width="90" border="0">
					<TR>
						<TD vAlign="top" align="center" height="22">
							<TABLE cellSpacing="0" cellPadding="0" width="70" bgColor="#a0a0a0" border="0">
								<TR>
									<TD height="18" width="5"><IMG height="18" src="Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
									<TD><asp:linkbutton  CausesValidation="false" class="txtButtonNoArrow" id="lnkbtnAddAll" runat="server">Add All</asp:linkbutton></TD>
									<TD width="5"><IMG height="18" src="Images/Curves/Grey-BtnRight.gif" width="5"></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					
					
					
					<TR>
						<TD vAlign="top" align="center" height="36">
							<TABLE cellSpacing="0" cellPadding="0" width="70" bgColor="#a0a0a0" border="0">
					        <TR>
					        <TD height="18" width="5"><IMG height="18" src="Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
					        <TD><asp:linkbutton   CausesValidation="false" class="txtButtonNoArrow" id="lnkbtnAdd" runat="server" >Add</asp:linkbutton> </TD>
					        <TD width="5"><IMG height="18" src="Images/Curves/Grey-BtnRight.gif" width="5"></TD>
					        </TR>
					        </TABLE>
							
						</TD>
					</TR>
					
					
						
					<TR>
					<TD vAlign="top" align="center" height="22">
					
					<TABLE cellSpacing="0" cellPadding="0" width="70" bgColor="#a0a0a0" border="0">
								<TR>
									<TD height="18" width="5"><IMG height="18" src="Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
									<TD><asp:linkbutton CausesValidation="false" class="txtButtonNoArrow" id="lnkbtnClearAll" runat="server">Clear All</asp:linkbutton>  </TD>
									<TD width="5"><IMG height="18" src="Images/Curves/Grey-BtnRight.gif" width="5"></TD>
								</TR>
							</TABLE>
					
					
				
					
					</TD>
					</TR>
				
				
					<TR>
					<TD vAlign="top" align="center"><TABLE cellSpacing="0" cellPadding="0" width="70" bgColor="#a0a0a0" border="0">
					<TR>
					<TD height="18" width="5"><IMG height="18" src="Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
					<TD><asp:linkbutton CausesValidation="false"   class="txtButtonNoArrow" id="lnkbtnClear" runat="server" >Clear</asp:linkbutton></TD>
					<TD width="5"><IMG height="18" src="Images/Curves/Grey-BtnRight.gif" width="5"></TD>
					</TR>
					</TABLE>
					</TD>
					</TR>
					</TABLE>
				
			</TD>
			
			<TD width=205 vAlign="top" align="right" id="tdSelected" runat=server><asp:ListBox  class="formFieldGrey width202height110" id="lstSelected" runat="server" Rows="7"
			SelectionMode="multiple"></asp:ListBox></TD>
		</TR>
</TABLE>


</ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID="lnkbtnAddAll" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="lnkbtnClearAll" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="lnkbtnAdd" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="lnkbtnClear" EventName="Click" />
</Triggers>
</asp:UpdatePanel>
</asp:Panel> 

