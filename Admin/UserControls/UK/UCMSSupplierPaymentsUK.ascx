<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSSupplierPayments.ascx.vb" Inherits="Admin.UCMSSupplierPayments" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
             <ContentTemplate>
			 
            <table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td><asp:GridView ID="gvSupplierPayments"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>' BorderColor="White"  
               PagerSettings-Mode=NextPreviousFirstLast  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
					<tr>
						<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
						</td>
					</tr>				
				</table>
                  
               </EmptyDataTemplate>
               
               <PagerTemplate>
                   
                  
                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>
							<td runat="server" id="tdsetType1" width="90"></td>
							
							
							<td width="10">&nbsp;</td>
						
						  
						  <td id="tdsetType2" align="left" width="121">
								
							</td>
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
        </PagerTemplate> 
               <Columns>  
                 <asp:TemplateField SortExpression="Date" HeaderText="Withdrawal Request Date" HeaderStyle-CssClass="gridHdr gridText" ItemStyle-Height="20px"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="200px"/>
                <ItemTemplate>             
                    <%#Strings.FormatDateTime(Container.DataItem("Date"), DateFormat.ShortDate)%>
				</ItemTemplate>
                </asp:TemplateField> 
				
				 <asp:TemplateField visible=false SortExpression="DateCompleted" HeaderText="Date Completed" HeaderStyle-CssClass="gridHdr gridText" ItemStyle-Height="20px"> 
                <ItemStyle CssClass="gridRow" Wrap=true Width="200px"/>
                <ItemTemplate>             
                    <%# SetDateCompleted(Container.DataItem("DateCompleted")) %>
				</ItemTemplate>
                </asp:TemplateField>   

				<asp:BoundField  visible=false ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"  HeaderStyle-CssClass="gridHdr gridText"  DataField="SupplierName" SortExpression="SupplierName" HeaderText="Supplier" ItemStyle-Height="40px" />        				
				
			    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"  HeaderStyle-CssClass="gridHdr gridText"  DataField="AdviceNumber" SortExpression="AdviceNo" HeaderText="Purchase Invoice Number" ItemStyle-Height="40px" />        
											
				<asp:TemplateField SortExpression="Total" HeaderText="Invoice Amount" HeaderStyle-CssClass="gridHdr gridText"> 
                <ItemStyle CssClass="gridRow" Wrap=true  Width="150px"  HorizontalAlign=Right/>
                <ItemTemplate>             
                     <asp:Label runat="server" Text='<%#FormatCurrency(IIF(Not isDbnull(Container.DataItem("Total")) ,Container.DataItem("Total"),"0"),2,TriState.True, Tristate.true,Tristate.false)%>' id="lblDebitBalance"></asp:Label> 
				</ItemTemplate>
                </asp:TemplateField>  
                <asp:BoundField ItemStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr gridText"  DataField="Status" SortExpression="Status" HeaderText="Status" />  
				<asp:TemplateField >               
				   <HeaderStyle CssClass="gridHdr"   Wrap=true /> 
				  <ItemStyle Wrap=true HorizontalAlign=Left Width="50px"   CssClass="gridRow"/>
				   <ItemTemplate>   
						   <%#GetLinks(Container.DataItem("AdviceNumber"), Container.DataItem("SupplierName"),  Container.DataItem("WOID"))%>
				   </ItemTemplate>
			   	</asp:TemplateField> 							                        
				</Columns>
               
                 <AlternatingRowStyle    CssClass=gridRow />
                 <RowStyle CssClass=gridRow />            
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Middle  Height="25px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
        
              </asp:GridView>
				 </td>
            </tr>
            </table>
				
				 <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="UCMSSupplierPayments" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
          </asp:Panel>
          </ContentTemplate>
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />

