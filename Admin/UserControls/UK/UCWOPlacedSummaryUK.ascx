<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCWOPlacedSummary.ascx.vb" Inherits="Admin.UCWOPlacedSummary" %>


<div id="divPlacedWO" class="divPlacedWO">
  <div class="divPlacedWOTopCurves"><img src="../Images/Curves/MPWO-Bdr-TL.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
  <table width="100%" height="217" border="0" cellpadding="0" cellspacing="0" class="summaryBox">
    <tr>
      <td valign="top" ><p class="txtListing"><a href="~/SecurePages/BuyerWOListing.aspx?mode=Draft" id="lnkPlacedWO" runat="server" class='txtListing'><strong>Buy Work:</strong></a></p>
        <ul class='liNavigationBlack'>
          <asp:Repeater id="DLSummary" runat="server">
            <ItemTemplate>
              <li > <a href='<%#GetWOLink(Container.DataItem("StatusID"))%>' class='txtListing'> I have <%#Container.DataItem("WOCount")%> <%#Container.DataItem("Status")%> </a> </li>
            </ItemTemplate>
          </asp:Repeater>
        </ul>
        <table id="tblNoWO" runat="server" Visible="False" width="100%" border="0" height="80"
					cellpadding="0" cellspacing="0">
          <tr>
            <td class="txtListing" valign="middle"><a href="../SecurePages/WOForm.aspx" class='txtListing'>Click here to create a new <br>&nbsp;Work Request.</a> </td>
          </tr>
        </table>
	  </td>
	 </tr>
   <tr>
      <td height="50" valign="top"><span class="txtListing"> <img src='../Images/Grey-Bullet.gif' border="0" align="absMiddle"> &nbsp;&nbsp;Total value of Placed work <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;orders is <b>
    <asp:Label id="lblTotal" runat="server"></asp:Label>
  </b> (excl. VAT) </span> </td>
    </tr>
    
  </table>
  <div class="divPlacedWOBotCurves"><img src="../Images/Curves/MPWO-Bdr-BL.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
</div>

