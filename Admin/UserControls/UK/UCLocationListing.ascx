<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCLocationListing.ascx.vb" Inherits="Admin.UCLocationListing" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<table cellspacing="0" cellpadding="0" width="100%" border="0" style="padding: 0px 0px 10px 0px;
            margin: 0px;">
            <tr>
                <td width="165" class="txtWelcome paddingL10">
                    <div id="divButton" style="width: 165px;">
                        <div class="bottonTopGrey">
                            <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                height="4" class="btnCorner" style="display: none" /></div>                                
                                <a class="buttonText" runat="server" id="lnkBackToListing">
                                        <img src="Images/Arrows/Arrow-Back.gif" title="Back to Company Profile" width="4"
                                            height="8" hspace="0" vspace="0" border="0" class="paddingL10R8" /><strong>Back to Company
                                    Profile</strong></a>
                        <div class="bottonBottomGrey">
                            <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                height="4" class="btnCorner" style="display: none" /></div>
                    </div>
                </td>
            </tr>
        </table>
						
                						
<div id="divListingOuter">

 
  <div class="roundtopListngOuter"><img src="Images/Curves/Grid-TLC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
  <input type="hidden" id="hdnbizid" runat="server" />
  <table width="100%" height="44" cellpadding="0" cellspacing="0" class="txtListing">
    <tr>
    <td width="240" align="right" nowrap><TABLE border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="120">
          <TR>
            <TD width="5"><asp:Label ID="lblSearch" runat="server" Text="Search : "/></TD>
            <TD class="txtListing"><asp:TextBox ID="txtSearch" Width="250px" runat="server"></asp:TextBox></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
            <TD class="txtListing"><asp:Button BorderStyle="None" class="txtListing" style="cursor:pointer;" align="absMiddle" Font-Bold="true" ID="btnSearch" BorderWidth="0" runat="server" Text="GO"/></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
            <TD class="txtListing"><asp:Button BorderStyle="None" class="txtListing" style="cursor:pointer;" align="absMiddle" Font-Bold="true" ID="btnClear" BorderWidth="0" runat="server" Text="Clear"/></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
          </TR>
      </TABLE></td>
      <td width="193" height="30" align="right" nowrap>&nbsp;</td>
      <td width="125" align="right" nowrap>&nbsp;</td>
      <td width="247" align="right" nowrap>&nbsp;</td>
      <td width="240" align="right" nowrap><TABLE border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="120">
          <TR>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
            <TD class="txtListing"><a id="btnExportToExcel" target="_blank" tabindex="10" class="txtListing" href='<%="ExportToExcel.aspx?page=Location&CompanyID=" & Request("companyID")  & "&bizDivId=" & Request("bizDivId") & "&FilterString=" & ViewState("FilterString")%>'><strong><img src="Images/Icons/Icon-Excel.gif" width="15" height="15" hspace="5" vspace="0" align="absMiddle"  border="0">&nbsp;Export to Excel</strong> </a></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
          </TR>
      </TABLE></td>
      <td width="13">&nbsp;</td>
      <td width="125" align="left" nowrap><TABLE border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="120">
          <TR>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
            <TD class="txtListing"><a id="btnAddLocation" runat="server" class="txtListing" tabindex="11"> <strong><img src="Images/Icons/Icon-SpecialistSmall.gif" width="12" height="16" hspace="5" vspace="0" align="absMiddle"  border="0">&nbsp;Add Location</strong> </a></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
          </TR>
      </TABLE></td>
    </tr>
  </table>


  <asp:GridView ID="gvContacts" runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#555354"
        border="0" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'  DataKeyNames="ContactID"   PagerSettings-Mode=NextPreviousFirstLast
        CssClass="bdrBtmListing" DataSourceID="ObjectDataSource1" PagerSettings-Position=TopAndBottom Width="100%" AllowSorting="true">
     
         <PagerTemplate>
         	<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="10" cellspacing="0" bgcolor="#DAD8D9">
				<tr>
				    <td width="176">&nbsp;</td>
				    <td align="right" >
				        <TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
				            <TR>
				                <td align="right" valign="middle">
					                <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
					            </td>
				                <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
					                <table width="100px" border="0" cellpadding="0" cellspacing="0" align="right">
			                            <tr>
					                    <td align="right" width="36">
					                        <div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
					                            <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
					                        </div>	
					                        <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
					                            <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
					                        </div>																							   
					                    </td>
					                    <td width="50" align="center" style="margin-right:3px; ">													
					                        <asp:DropDownList style="margin-left:4px;"  OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField width40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
					                    <td width="30" >
					                        <div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
						                        <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
						                    </div>
						                    <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
						                        <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
						                    </div>
					                    </td>
					                    </tr>
				                    </table>
				                </td>
				            </TR>
			            </TABLE>
			        </td>
			    </tr>
			</table>
            
        </PagerTemplate>
        
         <headerstyle BackColor="#993366" Font-Bold="true" ForeColor="#CE3538" ></headerstyle>
        <Columns >
            
  
            <asp:TemplateField SortExpression="LocationName" HeaderText="Location Name" HeaderStyle-CssClass="headerListing">               
               <ItemStyle Wrap=true  Width="10%"  CssClass="contentListing" />
               <ItemTemplate>
               <%--'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User--%>
                   <%#"<a class=footerTxtSelected href=LocationForm.aspx?AddressId=" & Container.DataItem("AddressID") & "&companyId=" & Container.DataItem("ContactID") & "&bizDivId=" & hdnbizid.Value & "&classId=" & Request("classId") & ">" & Container.DataItem("LocationName") & "</a>"%>
               </ItemTemplate>
            </asp:TemplateField> 
                     
            
            <asp:TemplateField SortExpression="Address" HeaderText="Address" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true  Width="35%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <%#  Container.DataItem("Address") & IIF( Container.DataItem("City") <> "" ,", " &Container.DataItem("City"),"") & IIF( Container.DataItem("State") <> "" ,", " &Container.DataItem("State"),"") & IIF( Container.DataItem("PostCode") <> "" ,", " &Container.DataItem("PostCode"),"") & IIF( Container.DataItem("Country") <> "" ,", " &Container.DataItem("Country") & ".","") %>
                </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField SortExpression="Phone" HeaderText="Contact Numbers" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true  Width="15%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <%#  IIF( IsDBNull(Container.DataItem("Phone")) <> true, "Phone: " & Container.DataItem("Phone"),"" ) & IIF( IsDBNull(Container.DataItem("Fax")) <> true, "<br/>Fax: " & Container.DataItem("Fax"),"" ) %>
                </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField SortExpression="Main" HeaderText="Main" HeaderStyle-CssClass="headerListing"  > 
                <ItemStyle Wrap=true Width="5%" CssClass="contentListing" />
                <ItemTemplate>             
                    <%#IIF(Container.DataItem("Main"),"Yes","No")  %>
                </ItemTemplate>
            </asp:TemplateField>             
            <asp:BoundField   ItemStyle-CssClass="contentListing" HeaderStyle-CssClass="headerListing"  ItemStyle-Width="20%"  Visible=true  DataField="Contact" SortExpression="Contact" HeaderText="Contact" />                
            <asp:BoundField  ItemStyle-CssClass="contentListing" HeaderStyle-CssClass="headerListing" ItemStyle-Width="5%"   Visible=true  DataField="Users" SortExpression="Users" HeaderText="Users" />                
			<asp:TemplateField SortExpression="IsBilling" HeaderText="Billing Location" HeaderStyle-CssClass="headerListing"> 
                        <ItemStyle Wrap=true  Width="20%" CssClass="contentListing"  />
                        <ItemTemplate>             
                            <%#IIf(Container.DataItem("IsBilling") <> "True", "No", "Yes")%>
                        </ItemTemplate>
                   </asp:TemplateField>

            <asp:TemplateField SortExpression="FixedCapacity" HeaderText="Fixed Capacity" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true  Width="35%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <%# IIf(IsDBNull(Container.DataItem("FixedCapacity")), "NA",Container.DataItem("FixedCapacity"))%>
                </ItemTemplate>
            </asp:TemplateField>
			
			<asp:TemplateField SortExpression="IsDeleted" HeaderText="Delete Status" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true  Width="35%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <%#  IIF(Container.DataItem("IsDeleted"),"Deleted","") %>
                </ItemTemplate>
            </asp:TemplateField> 
			
            <asp:TemplateField HeaderText="Edit" HeaderStyle-CssClass="headerListingBdrTop">               
               <ItemStyle Wrap=true HorizontalAlign=Left Width="5%" CssClass="contentListingBdrTop" />
               <ItemTemplate >        
                <%--'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User--%>     
                   <%#"<a class=footerTxtSelected href=LocationForm.aspx?AddressId=" & Container.DataItem("AddressID") & "&companyId=" & Container.DataItem("ContactID") & "&bizDivId=" & hdnbizid.Value & "&classId=" & Request("classId") & "><img src='Images/Icons/Icon-Pencil.gif' border='0'>" & "</a>"%>
               </ItemTemplate>
            </asp:TemplateField> 
        </Columns>
        

         
         
        
</asp:GridView>


<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SelectFromDB"   EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.UCLocationListing" 
        SortParameterName="sortExpression">    
              
</asp:ObjectDataSource>
  
<div class="roundBtmListngOuter"><img src="Images/Curves/Grid-BLC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
</div>


 </ContentTemplate>
 </asp:UpdatePanel>
 
 
 
 
 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
        <ProgressTemplate>
            <div>
                <img  align=middle src="Images/indicator.gif" />
                 Fetching Data... Please wait
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="gvContacts" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />


    


    
    
           
