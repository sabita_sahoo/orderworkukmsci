<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCFile_Outer.ascx.vb" Inherits="Admin.UCFileUpload_Outer" %>
<asp:UpdatePanel ID="UpdatePnlCompanyProfile" runat="server"  UpdateMode="Conditional"> <contenttemplate>
 
<!--<div class="divAttachment" runat=server style="width:100%; "  bgcolor="#F4F5EF" >
	<div class="AttachtopVal "><img src="Images/Curves/Attach-TL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
	-->
<asp:Button id="btnJavaClickToPopulateAttachFile" onClick="btnJavaClickToPopulateAttachFile_Click" runat="server" causesValidation=False  CssClass="passwordFieldHidden" ></asp:Button>
<input type=button id="btnFocusAttach" runat=server class="passwordFieldHidden"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="paddingT5">
    <tr id="trTitle" runat="server">
        <td width="12px">&nbsp;</td>
        <td height="14" class="formLabelGrey" width="185px" id="tdExistTitle" runat="server">Select and attach existing file: </td>
        
        <td height="14" class="formLabelGrey" width="250px" id="tdUploadTitle"  runat="server">Select file to upload:</td>
        
         <td height="14" class="formLabelGrey" width="250px" id="tdAttachTitle"  runat="server">Attached files:</td>
    </tr>
    
    <tr>
        <td width="12px">&nbsp;</td>
        <td id="tdExist" runat="server" valign="top">
        
        
                    <asp:Repeater runat="server" ID="rptExistingAttList" Visible="true">
                      <itemtemplate>
                        <input type="checkbox" id="chkExistAtt"  runat="server"/>
                        <input type=hidden runat=server id="hdnExistAttachID"  value='<%#Container.DataItem("AttachmentID")%>'/>
                        <input type=hidden runat=server id="hdnExistFilePath"  value='<%#Container.DataItem("FilePath")%>'/>
                        
                        <span class='txtGreySmallSelected'>
                              <a class='footerTxtSelected' href='<%# getlink(Container.DataItem("FilePath")) %>' target='_blank'>
                                                            <%# Container.DataItem("Name")%></a>
                                                    </a>
                        </span><br>
                      </itemtemplate>
                    </asp:Repeater>
                              
                    <TABLE cellSpacing="0"  cellPadding="0" width="70"  border="0">
                        <tr>
                            <td>&nbsp;</td>
                        <tr bgcolor="#a0a0a0">
			                <td height="18">
                                <img height="18" src="Images/Curves/Grey-BtnLeft.gif" width="5">
                                </img>
                            </td>
                            <td>
                                <asp:LinkButton ID="btnAttach" runat="server" causesvalidation="false" class="txtButtonNoArrow">Attach</asp:LinkButton>
                            </td>
                            <td width="5">
                                <img height="18" src="Images/Curves/Grey-BtnRight.gif" width="5">
                                </img>
                            </td>
		            </TABLE>
		         
		   		    
                                    
              
        </td>
        
        <td id="tdUpload"  runat="server" valign="top">
        
            <iframe src="FileAttach.aspx?MODE=IFRAME" id="frameUpload" style="width:100%;height:60px;border-width:0;" runat="server" frameborder="0">
			            Your browser does not support iframes
                </iframe>

        </td>
        
        <td id="tdAttach"  runat="server" valign="top">
                
               
	
                        <asp:panel id="pnlList" runat="server">
                            <ul>
                            <asp:Repeater runat="server" ID="rptAttList" Visible="true" OnItemCommand="Button_ItemCommand">
                              <itemtemplate>
                                <li>
                                <input type=hidden runat=server id="attachID"  value='<%#Container.DataItem("AttachmentID")%>'/>
                                <input type=hidden runat=server id="filePath"  value='<%#Container.DataItem("FilePath")%>'/>
                                    <span class='txtGreySmallSelected'>
                                          <a class='footerTxtSelected' href='<%# getlink(Container.DataItem("FilePath")) %>' target='_blank'>
                                                                    <%# Container.DataItem("Name")%></a>
                                                            </a>
                                    </span>
                                    
                                      <asp:linkbutton id="btnRemove" causesvalidation="false"  CommandName="Remove"   width="50px" runat="server" class="txtListing">&nbsp;...remove&nbsp;</asp:LinkButton>  
                                       <asp:panel id="pnlImage" runat="server" visble="false" class="paddingT10">
                                            <asp:image id="imgLogo" runat="server" ImageUrl=""  visble="false"/>
                                        </asp:panel>
                                    
                                
                                 <br>
                              </itemtemplate>
                            </asp:Repeater>
                            </ul>
                        </asp:panel>
                         
                          
                        
                        
                    				
	                                     
            
        </td>
        
        
    </tr>

</table>
<!--
	<div class="AttachbottomVal "><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
</div>
-->

</contenttemplate>
  
  
   <triggers>
    <asp:AsyncPostBackTrigger ControlID="btnAttach" EventName="Click" /> 


    

    </triggers> </asp:UpdatePanel>

