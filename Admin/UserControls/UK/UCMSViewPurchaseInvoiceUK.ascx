<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSViewPurchaseInvoiceUK.ascx.vb" Inherits="Admin.UCMSViewPurchaseInvoiceUK" %>
<style type="text/css">
<!--
.invoiceLabel {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 32px;
	color: #B7B7B7;
	font-weight: bold;
	line-height:38px;

}
.InvoiceText {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.bdr1 {
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 4px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.bdr2 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #C6C6C6;
	border-right-color: #C6C6C6;
	border-bottom-color: #C6C6C6;
	border-left-color: #C6C6C6;
}
.bdr3 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.style3 {font-size: 11px}
.style5 {font-weight: bold}
.style6 {font-weight: bold}
td
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 16px;
	color: #000000;
}
body {
	background-color: #CECECE;
	height:100%;
}.VatMessage {
	text-align: center;
}
-->
</style>
<table height="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="45" height="140">&nbsp;</td>
    <td width="650" height="140"><table width="100%" cellpadding="0" cellspacing="0" style=" border-width:medium; border-bottom-width:5px;border-bottom-color:#000000;">
        <tr>
          <td width="311">&nbsp;</td>
          <td width="131" align="left" valign="bottom" >&nbsp;</td>
        </tr>
        <tr>
          <td width="311" valign="middle"></td>
          <td align="right" valign="bottom" ><table width="99%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right" valign="bottom"  style="height: 22px;"><span class="style1 style5">
                <span class="AddressTxt">
                <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                </span> </span></td>
              </tr>
              <tr>
                <td align="right" valign="bottom"  ><span class="AddressTxt">
                  <asp:Label ID="lblAddress" runat="server"></asp:Label>
                </span></td>
              </tr>
              <tr>
                <td align="right" valign="bottom"><span class="AddressTxt">
                    <asp:Label ID="lblCity" runat="server"></asp:Label></span>
                <span class="AddressTxt">
                </span></td>
              </tr>
              <tr>
                <td align="right" valign="bottom"  > 
                  <asp:Label ID="lblPostCode" runat="server"></asp:Label></td>
              </tr>
              <tr>
                <td align="right" valign="bottom" > &nbsp;
                    <span class="AddressTxt">
                    <asp:Label ID="lblCompanyRegNo" runat="server"></asp:Label>
                </span></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td width="311" class="bdr1" >&nbsp;</td>
          <td align="left" valign="bottom" class="bdr1" >&nbsp;</td>
        </tr>
    </table></td>
    <td width="45" height="140">&nbsp;</td>
  </tr>
  <tr>
    <td width="45" height="180" align="center">&nbsp;</td>
    <td width="650" height="180" align="center" valign="top"><p class="invoiceLabel">SELF BILL INVOICE</p>
        <table border="0" cellpadding="5" cellspacing="0" width="100%">
          <tr>
            <td align="left" valign="top"  style="border-bottom: 0px; border-right-width: 0px"><strong>Invoice To:<br>
              </strong>
                <asp:Label ID="lblCompNameInvoiceTo" runat="server"></asp:Label>
                <br />
                <asp:Label ID="lblInvoiceTo" runat="server"></asp:Label>
            &nbsp;</td>
            <td align="right" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="95" align="left"><strong>Invoice No.&nbsp;</strong></td>
                  <td width="8" align="left">:</td>
                  <td align="left"><asp:Label ID="lblInvoiceNo" runat="server"></asp:Label></td>
                </tr>
                <tr>
                  <td width="95" align="left"><strong><asp:Label ID="lblPaymentTermLabel" runat="server"></asp:Label></strong></td>
                  <td align="left">:</td>
                  <td align="left"><asp:Label ID="lblCreditTerms" runat="server"></asp:Label></td>
                </tr>
                <tr>
                  <td width="95" align="left"><strong>Invoice Date&nbsp;</strong></td>
                  <td align="left">:</td>
                  <td align="left"><asp:Label ID="lblInvoiceDate" runat="server"></asp:Label></td>
                </tr>
                <tr>
                  <td width="95" align="left"><strong>VAT No.</strong></td>
                  <td align="left">:</td>
                  <td align="left"><strong>
                    <asp:Label ID="lblVATNo" runat="server" ></asp:Label>
                  </strong></td>
                </tr>
            </table></td>
          </tr>
        </table>
    <p>&nbsp;</p></td>
    <td width="45" height="180" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td width="45" align="right" valign="top">&nbsp;</td>
      
      <%--old format--%>
      
       <td width="650" align="right" valign="top" id="tdOldFormat" runat="server">
      <table width="650" border="0" cellspacing="0" cellpadding="0">
     
      <tr>
        <td width="80" height="30" align="left" ><strong>Date</strong></td>
        <td width="124" height="30"  align="left"  style="border-right-width: 0px;"><strong>Work Order ID</strong><strong></strong></td>
        <td width="206" height="30" align="left"  style="border-right-width: 0px"><strong>Description</strong></td>
        <td width="60" height="30" align="left"  style="border-right-width: 0px"><strong>Quantity</strong></td>
        <td width="82" height="30" align="right"  ><strong>Net Price</strong></td>
        <td width="90" height="30" align="right" style="width:border-right-width: 0px"><strong>VAT (
              <asp:Label ID="lblTotalVatAsPerSiteOld" runat="server">&nbsp;</asp:Label>
        %)</strong></td>
        <td width="72" height="30" align="right" ><strong>Amount</strong></td>
      </tr>

      <asp:Repeater ID="rptListOld" runat="server" Visible="true">
        <ItemTemplate>
          <tr>
            <td align="left" valign="top"  style="border-right-width: 0px;border-top-width: 0px;"><%#Strings.FormatDateTime(Container.DataItem("CloseDate"), DateFormat.ShortDate)%> &nbsp;</td>
            <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"><%# Container.DataItem("WorkOrderId")%> </td>
            <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"><%# Container.DataItem("Description")%> </td>
            <td align="left"  style="border-right-width: 0px;border-top-width: 0px;text-align:center;"><%# Container.DataItem("Quantity")%> </td>
            <td align="right"  style="border-right-width: 0px;border-top-width: 0px;"><%# FormatCurrency(Container.DataItem("WOVal"), 2, TriState.True, TriState.True, TriState.False)%></td>
            <td align="right"  style="border-right-width: 0px;border-top-width: 0px;"><%# FormatCurrency(Container.DataItem("VATOnWP"), 2, TriState.True, TriState.True, TriState.False)%></td>
            <td align="right"  style="border-top-width: 0px;"><%# FormatCurrency(Container.DataItem("WoVal"), 2, TriState.True, TriState.True, TriState.False)%></td>
          </tr>
       </ItemTemplate>
      </asp:Repeater>
      <tr>
        <td align="left" valign="top"  style="border-right-width: 0px;border-top-width: 0px;">&nbsp;</td>
        <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
        <td align="left"  style="border-right-width: 0px;border-top-width: 0px;">Less <asp:Label ID="lblSPFee" runat="server"></asp:Label>% discount</td>
        <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
        <td align="right"  style="border-right-width: 0px;border-top-width: 0px;"><asp:Label ID="lblDiscount" runat="server"></asp:Label></td>
        <td align="right"  style="border-right-width: 0px;border-top-width: 0px;"><asp:Label ID="lblVatOnDiscount" runat="server"></asp:Label></td>
        <td align="right"  style="border-top-width: 0px;"><asp:Label ID="lblTotalDiscount" runat="server"></asp:Label></td>


      </tr>
    </table>
      <br>
      <table border="0" cellspacing="0">
          <tr>
            <td width="190" align="right"  style=" border-bottom:0px; border-right-width:0px; "><strong>Sub-Total</strong></td>
            <td width="70" align="right"  style=" border-bottom:0px; "><asp:Label ID="lblSubTotalOld" runat="server"></asp:Label>
            </td>
          </tr>
          <tr>
            <td width="190" height="20" align="right"   style=" border-bottom:0px; border-right-width:0px; "><strong>Total VAT (
                  <asp:Label ID="lblTotalVatAsPerSite1" runat="server">&nbsp;</asp:Label>
            %) </strong></td>
            <td width="70" height="20" align="right"  style=" border-bottom:0px; "><asp:Label ID="lblTotalVATOld" runat="server"></asp:Label>
            </td>
          </tr>
          <tr>
            <td width="190" height="50" align="right"   style="border-right-width:0px; "><strong>Total Amount </strong></td>
            <td width="70" height="40"  align="right" ><span class="style6">
              <asp:Label ID="lblTotalAmountOld" runat="server"></asp:Label>
            </span> </td>
          </tr>
      </table>
      <p class="VatMessage"><strong>The VAT shown is your output tax due to Customs & Excise</strong>        </p>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
               <tr>
                   <td height="40" colspan="3"  align="left" valign="middle" class="bdr2" ><strong >Supplier Bank Details</strong></td>
               </tr>
               <tr>
                   <td width="110" align="left"><strong>Account Name </strong></td>
                   <td width="8" align="left" ><strong>:</strong></td>
                   <td align="left"  ><asp:Label ID="lblAccountNameOld" runat="server" Width="320px"></asp:Label></td>
               </tr>
               <tr>
                   <td align="left"><strong>Sort-Code &nbsp;</strong></td>
                   <td align="left" ><strong>:</strong></td>
                   <td align="left"  ><asp:Label ID="lblSortCodeOld" runat="server" Width="320px"></asp:Label></td>
               </tr>
               <tr>
                   <td align="left"><strong>Account Number</strong></td>
                   <td align="left" ><strong>:</strong></td>
                   <td align="left"  ><asp:Label ID="lblAccountNumberOld" runat="server" Width="320px"></asp:Label></td>
               </tr>
               <tr>
                   <td height="30" align="left">&nbsp;</td>
                   <td height="30" align="left" >&nbsp;</td>
                   <td height="30" align="left" >&nbsp;</td>
               </tr>
           </table>
       </td>


      
      <%--New format--%>

    <td width="650" align="right" valign="top" id="tdNewFormat" runat="server" >
        <table width="650" border="0" cellspacing="0" cellpadding="0">
       <tr>
        <td width="50" height="30" align="left" ><strong>Date</strong></td>
        <td width="112" height="30"  align="left"  style="border-right-width: 0px;"><strong>Work Order ID</strong><strong></strong></td>
        <td width="242" height="30" align="left"  style="border-right-width: 0px"><strong>Description</strong></td>
        <td width="30" height="30" align="left"  style="border-right-width: 0px"><strong>Quantity</strong></td>
       <td width="47" height="30" align="right" ><strong>Amount</strong></td>
      </tr>

      <asp:Repeater ID="rptList" runat="server" Visible="true">
        <ItemTemplate>
          <tr>
            <td align="left" valign="top"  style="border-right-width: 0px;border-top-width: 0px;"><%#Strings.FormatDateTime(Container.DataItem("CloseDate"), DateFormat.ShortDate)%> &nbsp;</td>
            <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"><%# Container.DataItem("WorkOrderId")%> </td>
            <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"><strong><%# Container.DataItem("Description")%>&nbsp;(<%# Container.DataItem("Source")%>)</strong> </td>
            <td align="left"  style="border-right-width: 0px;border-top-width: 0px;text-align:center;"><strong><%# Container.DataItem("Quantity")%> </strong></td>
            <td align="right"  style="border-top-width: 0px;"><strong><%# FormatCurrency(Container.DataItem("WoValWithAdminFee"), 2, TriState.True, TriState.True, TriState.False)%></strong></td>

          </tr>
       </ItemTemplate>
      </asp:Repeater>
            <tr>  <td width="45" align="right" valign="top">&nbsp;</td></tr>
      <tr>
        <td align="left" valign="top"  style="border-right-width: 0px;border-top-width: 0px;">&nbsp;</td>
        <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
        <td align="left"  style="border-right-width: 0px;border-top-width: 0px;">Option Fast Payment Discount (<asp:Label ID="lblPaymentTerms" runat="server" Text="0"></asp:Label> days selected - <asp:Label ID="lblPaymentDiscount" runat="server" Text="0"></asp:Label>%) </td>
        <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
        <td align="right"  style="border-right-width: 0px;border-top-width: 0px;">-&nbsp;<asp:Label ID="lblPaymentTermDiscountApplied" runat="server"></asp:Label></td>
       </tr>
            
            <tr>
                <td align="left" valign="top"  style="border-right-width: 0px;border-top-width: 0px;">&nbsp;</td>
                <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
                <td align="left"  style="border-right-width: 0px;border-top-width: 0px;">Insurance Option (not selected) </td>
                <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
                <td align="right"  style="border-right-width: 0px;border-top-width: 0px;">-&nbsp;<asp:Label ID="lblInsurance" runat="server" ></asp:Label></td>
            </tr>
            <tr>  <td width="45" align="right" valign="top">&nbsp;</td></tr>
          <tr>
                <td align="left" valign="top"  style="border-right-width: 0px;border-top-width: 0px;">&nbsp;</td>
                <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
                <td align="left"  style="border-right-width: 0px;border-top-width: 0px;">Total Value exc. VAT</td>
                <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
                <td align="right"  style="border-right-width: 0px;border-top-width: 0px;"><asp:Label ID="lblSubTotal" runat="server" Text="81"></asp:Label></td>
            </tr>

            <tr>
                <td align="left" valign="top"  style="border-right-width: 0px;border-top-width: 0px;">&nbsp;</td>
                <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
                <td align="left"  style="border-right-width: 0px;border-top-width: 0px;">VAT (<asp:Label ID="lblTotalVatAsPerSite" runat="server">&nbsp;</asp:Label>%)</td>
               <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
                <td align="right"  style="border-right-width: 0px;border-top-width: 0px;"><asp:Label ID="lblTotalVAT" runat="server" Text="81"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" valign="top"  style="border-right-width: 0px;border-top-width: 0px;">&nbsp;</td>
                <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
                <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"><strong>Total Value inc.VAT</strong></td>
                <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"></td>
                <td align="right"  style="border-right-width: 0px;border-top-width: 0px;"><strong><asp:Label ID="lblTotalAmount" runat="server" Text="81"></asp:Label></strong></td>
            </tr>
    </table>
      <br>
        <p class="VatMessage"><strong>The VAT shown is your output tax due to Customs & Excise</strong>        </p>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="40" colspan="3"  align="left" valign="middle" class="bdr2" ><strong >Supplier Bank Details</strong></td>
          </tr>
          <tr>
            <td width="110" align="left"><strong>Account Name </strong></td>
            <td width="8" align="left" ><strong>:</strong></td>
            <td align="left"  ><asp:Label ID="lblAccountName" runat="server" Width="320px"></asp:Label></td>
          </tr>
          <tr>
            <td align="left"><strong>Sort-Code &nbsp;</strong></td>
            <td align="left" ><strong>:</strong></td>
            <td align="left"  ><asp:Label ID="lblSortCode" runat="server" Width="320px"></asp:Label></td>
          </tr>
          <tr>
            <td align="left"><strong>Account Number</strong></td>
            <td align="left" ><strong>:</strong></td>
            <td align="left"  ><asp:Label ID="lblAccountNumber" runat="server" Width="320px"></asp:Label></td>
          </tr>
          <tr>
            <td height="30" align="left">&nbsp;</td>
            <td height="30" align="left" >&nbsp;</td>
            <td height="30" align="left" >&nbsp;</td>
          </tr>
    </table>
    </td>
  

    <td width="45" align="right" valign="top">&nbsp;</td>
  </tr>
</table>
