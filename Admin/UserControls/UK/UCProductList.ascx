<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCProductList.ascx.vb" Inherits="Admin.UCProductList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<style type="text/css">
.pnlConfirm1 {	
	background-color:#FFEBDE;
	font-family: Tahoma, Arial, Verdana;
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	line-height: 18px;
	border: 1px solid #CECBCE;
	padding: 10px;
	position:absolute !important;
	margin-top:350;
}
</style>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">

<ContentTemplate>
<asp:Panel ID="pnlSearchContact" runat="server" Visible = "false">
<table>
 <tr>    
    <td><%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
			<uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact>                
    </td>
    <td width="10px">&nbsp;</td>
       <td id="tdViewBtn" runat="server" align="left" width="60px">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                <asp:LinkButton ID="lnkView" causesValidation="true" OnClick="lnkView_Click" runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
            </div>
        </td>
 </tr> 
 </table>
 <div style="color:Red; margin-left:10px; font-weight:bold;"><asp:Label ID="lblError" runat="server" Text="Please select at least one service to copy" Visible="false"></asp:Label></div>
</asp:Panel><asp:Panel ID="pnlConfirmationSummary" runat ="server" Visible ="false">
      <div style="color:Red; margin-left:10px; font-weight:bold;">
      <asp:Label ID="lblConfError" runat="server" Text="Please select at least one a client account for the selected service(s) to be copied across to" Visible="false"></asp:Label>
      </div>
      <div id="divSelectContact" runat="server" style="margin-top:10px;">
        <strong><asp:Label ID="lblSelect" runat="server" Text="Select Contact to which the Selected products needs to be copied: "></asp:Label></strong><br />
        <%@ Register TagPrefix="uc2" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
						<uc2:UCSearchContact id="UCSearchContact2" runat="server"></uc2:UCSearchContact>        
       
    </div>  
      <div id="divValidWOIDs" runat="server" style="margin-top:10px;">
        <strong><asp:Label ID="lblProducts" runat="server" Text="List of Selected Products are: "></asp:Label></strong><br />
        <asp:DataList ID="dlValidWO" runat="server" CssClass="formLabelGrey" Font-Size="10px">
                  <ItemTemplate>  
                    <div style="width:100%;">
                        <div style=" float:left;">Service Name: <%#Container.DataItem("ProductName")%> </div> 
                    </div>
                   </ItemTemplate>     
       </asp:DataList>       
    </div>  
    <table runat="server" id="tblbtn">
        <tr>
            <td><TABLE border="0" cellPadding="0" cellSpacing="0"  bgColor="#f0f0f0" width="120">
                  <TR>
                    <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                    <TD class="txtBtnImage"><asp:LinkButton ID="lnkConfirm" runat="server" tabindex="10" class="txtListing"><strong><img src="Images/Icons/Submit.gif" hspace="5" vspace="0" align="absMiddle" border="0">&nbsp;Confirm</strong></asp:LinkButton></TD>
                    <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                  </TR>
              </TABLE>
            </td>            
            <td width="20">&nbsp;</td>
            <td><TABLE border="0" cellPadding="0" cellSpacing="0" bgColor="#f0f0f0" width="120">
                  <TR>
                    <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                    <TD class="txtBtnImage"><asp:LinkButton ID="lnkCancel" runat="server" tabindex="10" class="txtListing"><strong><img src="Images/Icons/Cancel.gif" hspace="5" vspace="0" align="absMiddle" border="0">&nbsp;Cancel</strong></asp:LinkButton></TD>
                    <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                  </TR>
              </TABLE></td>
        </tr>
    </table>
   </asp:Panel>
   <asp:Panel ID="pnlListing" runat ="server" Visible="true">
<div id="divListingOuter" style="margin-top:20px;">
  <div class="roundtopListngOuter"><img src="Images/Curves/Grid-TLC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
  
  <table width="100%" height="44" cellpadding="0" cellspacing="0" class="txtListing">    
    <tr>
      <td width="10">&nbsp;</td>
      <td width="283" height="30" align="left" nowrap><TABLE border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="180" runat="server" id="tblBack" visible ="true">
          <TR>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
            <TD class="txtListing">
                <asp:LinkButton ID="btnBack" runat="server" tabindex="10" class="txtListing"><strong><img src="Images/Icons/back.gif" hspace="5" vspace="0" align="absMiddle" border="0">&nbsp;Back to Company Profile</strong></asp:LinkButton></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
          </TR>
      </TABLE>            
      <TABLE border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="180" runat="server" id="tblDuplicate" visible ="False">
          <TR>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
            <TD class="txtListing">
                <asp:LinkButton ID="btnDuplicate" runat="server" tabindex="10" class="txtListing"><strong><img src="Images/Icons/Copy.gif" hspace="5" vspace="0" align="absMiddle" border="0">&nbsp;Duplicate Services</strong></asp:LinkButton></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
          </TR>
      </TABLE>
      </td>
      <td width="125" align="left" nowrap><asp:CheckBox ID="chkbxHideDeleted" runat="server" Visible="True" Checked="True" Text="Hide Deleted" AutoPostBack="true" /></td>
      <td width="147" align="right" nowrap>&nbsp;</td>
      <td width="240" align="right" nowrap>
     <%-- <TABLE id="tblUpdateSeq" runat="server" border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="140">
          <TR>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
            <TD class="txtListing"><asp:LinkButton ID="btnUpdateSeq" runat="server" tabindex="10" class="txtListing"><strong><img src="Images/Icons/update-seq-icon.gif" width="12" height="16" hspace="5" vspace="0" align="absMiddle"  border="0">&nbsp;Update Sequence</strong></asp:LinkButton></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
          </TR>
      </TABLE>--%>
      </td>            
      <td width="13">&nbsp;</td>
      <td width="125" align="left" nowrap><TABLE id="tblAddService" runat="server" border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="120">
          <TR>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
            <TD class="txtListing"><a id="btnAddLocation" class="txtListing" runat="server" tabindex="11" > <strong><img src="Images/Icons/Icon-SpecialistSmall.gif" width="12" height="16" hspace="5" vspace="0" align="absMiddle"  border="0">&nbsp;Add Service</strong> </a></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
          </TR>
      </TABLE></td>
    </tr>
  </table>


  <asp:GridView ID="gvProductList" runat=server AllowPaging="false"  AutoGenerateColumns="False" ForeColor="#555354"
        border="0" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'  DataKeyNames="ProductID" PagerSettings-Mode=NextPreviousFirstLast
        CssClass="bdrBtmListing"   PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>
     
        <PagerTemplate>
         	<table width="100%" id="tblTop" runat="server" visible="false"  border="0" cellpadding="10" cellspacing="0" bgcolor="#DAD8D9">
				<tr>
				    <td width="176">&nbsp;</td>
				    <td align="right" >
				        <TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
				            <TR>
				                <td align="right" valign="middle">
					                <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
					            </td>
				                <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
					                <table width="100px" border="0" cellpadding="0" cellspacing="0" align="right">
			                            <tr>
					                    <td align="right" width="36">
					                        <div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
					                            <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
					                        </div>	
					                        <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
					                            <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
					                        </div>																							   
					                    </td>
					                    <td width="50" align="center" style="margin-right:3px; ">													
					                        <asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField width40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
					                    <td width="30" >
					                        <div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
						                        <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
						                    </div>
						                    <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
						                        <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
						                    </div>
					                    </td>
					                    </tr>
				                    </table>
				                </td>
				            </TR>
			            </TABLE>
			        </td>
			    </tr>
			</table>
            
        </PagerTemplate>
        <EmptyDataTemplate>
            <div style="text-align:center;color:#CE3538; height:200px; vertical-align:middle; padding-top:50px;" class="contentListing">Sorry, No services have been defined</div>
        </EmptyDataTemplate>
         <headerstyle BackColor="#993366" Font-Bold="true" ForeColor="#CE3538" ></headerstyle>
        <Columns >        
         <asp:TemplateField Visible="True" HeaderText="<input id='chkAll' onClick=CheckAll(this,'Check') type='checkbox' name='chkAll' />" HeaderStyle-CssClass="headerListing" >			   
			   <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="contentListing"/>
			   <ItemTemplate>
				<asp:CheckBox ID="Check" runat="server" ></asp:CheckBox>
				<input type=hidden runat=server id="hdnWOIDs"   value='<%#Container.DataItem("ProductID")%>'/>
			   </ItemTemplate>
			</asp:TemplateField>
			 
            <asp:TemplateField HeaderText="Date Created" HeaderStyle-CssClass="headerListing" SortExpression="DateCreated">               
               <ItemStyle Wrap=true  Width="10%"  CssClass="contentListing" />
               <ItemTemplate>             
                   <%#Container.DataItem("DateCreated")%>
               </ItemTemplate>
            </asp:TemplateField> 
  
            <asp:TemplateField HeaderText="Service" HeaderStyle-CssClass="headerListing" SortExpression="ProductName">               
               <ItemStyle Wrap=true  Width="22%"  CssClass="contentListing" />
               <ItemTemplate>    
                   <%#GetLinks(Container.DataItem("ProductID"), PageName, (IIf(Not IsNothing(Request("CompanyID")), IIf(Request("CompanyID") <> "", Request("CompanyID"), Container.DataItem("CompanyID")), Container.DataItem("CompanyID"))), (IIf(Not IsNothing(Request("BizDivID")), IIf(Request("BizDivID") <> "", Request("BizDivID"), Container.DataItem("BizDivID")), Container.DataItem("BizDivID"))), Container.DataItem("SpecificCompany"), Container.DataItem("ProductName"))%>                                               
               </ItemTemplate>
            </asp:TemplateField> 
                     
            
            <asp:TemplateField SortExpression="Location" HeaderText="Billing Location" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true  Width="10%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <%#  Container.DataItem("Location") %>
                </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField SortExpression="StartDateDeferral" HeaderText="Start Date Deferral" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true  Width="7%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <%#Container.DataItem("StartDateDeferral")%>
                </ItemTemplate>
            </asp:TemplateField> 
            
            <asp:TemplateField SortExpression="WholeSalePrice" HeaderText="WholeSale price" HeaderStyle-CssClass="headerListing" ItemStyle-HorizontalAlign="right"> 
                <ItemStyle Wrap=true  Width="7%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <%#Container.DataItem("WholesalePrice")%>
                </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField SortExpression="CustomerPrice" HeaderText="Customer price" HeaderStyle-CssClass="headerListing" ItemStyle-HorizontalAlign="right"> 
                <ItemStyle Wrap=true  Width="10%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <%#Container.DataItem("ExCustomerPrice")%>
                </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField SortExpression="PlatformPrice" HeaderText="Portal Price" HeaderStyle-CssClass="headerListing" ItemStyle-HorizontalAlign="right"> 
                <ItemStyle Wrap=true Width="9%" CssClass="contentListing" />
                <ItemTemplate>             
                    <%#Container.DataItem("PlatformPrice")%>
                </ItemTemplate>
            </asp:TemplateField>             
            		
			<asp:TemplateField SortExpression="IsDeleted" HeaderText="Delete Status" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true Width="10%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <%#IIf(Container.DataItem("IsDeleted") <> 0, "Yes", "No")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="Sequence" HeaderText="Sequence" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true Width="5%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <%#Container.DataItem("Sequence")%>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true Width="2%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <asp:LinkButton ID="lnkbtnMoveTo" CommandName="MoveTo" Visible='<%#IIF((Container.DataItem("Sequence")) = 0, False, True)%>'  CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server"><img id="imgMoveTo" runat="server" src="~/Images/Icons/MoveTo.gif" title="Move To" width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" /></asp:LinkButton>                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true Width="2%"  CssClass="contentListing" />
                <ItemTemplate> 
                    <asp:LinkButton ID="lnkbtnSwap" CommandName="Swap" Visible='<%#IIF((Container.DataItem("Sequence")) = 0, False, True)%>'  CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server"><img id="imgSwap" runat="server" src="~/Images/Icons/Swap.gif" title="Swap" width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" /></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true Width="7%"  CssClass="contentListing" />
                <ItemTemplate>    
                 <asp:LinkButton ID="LnkBtnMoveUp" Visible='<%#IIF(Container.DataItem("Sequence") = Container.DataItem("MinSequence") , False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>' CommandName="MoveUp" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server"><img id="imgMoveUp" runat="server" src="~/Images/Arrows/arrow-up.jpg" title="Move Up" border="0" class="cursorHand" /></asp:LinkButton>
                 <asp:LinkButton ID="LnkBtnMoveDown" Visible='<%#IIF(Container.DataItem("Sequence") = Container.DataItem("MaxSequence"), False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>'  CommandName="MoveDown" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server"><img id="imgMoveDown" runat="server" src="~/Images/Arrows/arrow-down.jpg" title="Move Down" border="0" class="cursorHand" /></asp:LinkButton>
               <%--  <asp:imagebutton imageurl="~/images/arrows/arrow-up.jpg" commandname="update" commandargument="<%#container.dataitem("rownum")%>" runat="server" id="imgbtnmoveup" alternatetext="move up" tooltip="move up" causesvalidation="false" visible='<%#iif((container.dataitem("rownum")) = "1", false, true)%>' ></asp:imagebutton>
                    <asp:imagebutton imageurl="~/images/arrows/arrow-down.jpg" commandname="edit" commandargument="<%#container.dataitem("rownum")%>" runat="server" id="imgbtnmovedown" alternatetext="move down" tooltip="move down" causesvalidation="false"></asp:imagebutton>						    
                 --%> 
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Edit" HeaderStyle-CssClass="headerListingBdrTop">               
               <ItemStyle Wrap=true HorizontalAlign=Left Width="5%" CssClass="contentListingBdrTop" />
               <ItemTemplate >             
                   <%#GetLinks(Container.DataItem("ProductID"), PageName, (IIf(Not IsNothing(Request("CompanyID")), IIf(Request("CompanyID") <> "", Request("CompanyID"), Container.DataItem("CompanyID")), Container.DataItem("CompanyID"))), (IIf(Not IsNothing(Request("BizDivID")), IIf(Request("BizDivID") <> "", Request("BizDivID"), Container.DataItem("BizDivID")), Container.DataItem("BizDivID"))), Container.DataItem("SpecificCompany"), "")%>                   
               </ItemTemplate>
            </asp:TemplateField>  
        </Columns>
</asp:GridView>


<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SelectFromDB"   EnablePaging="true" SelectCountMethod="SelectCount" TypeName="Admin.UCProductList" 
        SortParameterName="sortExpression">    
              
</asp:ObjectDataSource>
 
<div style="vertical-align:top; line-height:0px; font-size:0px; background-repeat:no-repeat; padding:0px; margin:0px;"><img src="Images/Curves/Grid-BLC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none; background-repeat:no-repeat;"></div>
</div>
</asp:Panel>  


 </ContentTemplate>
 </asp:UpdatePanel>
 
 
 
 
 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
        <ProgressTemplate>
            <div>
                <img  align=middle src="Images/indicator.gif" />
                 Fetching Data... Please wait
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePanel1" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />

<cc1:ModalPopupExtender ID="mdlMoveTo" runat="server" TargetControlID="btnTemp" PopupControlID="pnlMoveTo" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
			      
<asp:Panel runat="server" ID="pnlMoveTo" Width="200px" CssClass="pnlConfirm1" style="display:none;">
    <div id="divModalParent" runat="server">
        <span style="font-family:Tahoma; font-size:14px;color:#993366;"><b>Move To New Sequence</b></span><br /><br />
        
         <span style="display:none;"><b>Current Sequence - </b><asp:Label ID="lblCurrentSeq" runat="server"></asp:Label><br /><br /></span>
        <asp:TextBox runat="server" ID="txtNewSeq" Height="15" Width="150" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>        
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtNewSeq" WatermarkText="Enter a new sequence here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        
        <br /><br />
            <div id="divButton" class="divButton" style="width: 85px; float:left;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                    <a class="buttonText cursorHand" onclick='javascript:validate_required("ctl00_ContentPlaceHolder1_UCProductListing1_txtNewSeq","Enter a new sequence here","MoveTo")' id="ancSubmit">Submit</a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div>
            <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>		
     <div id="divlblMsg" runat="server"  style="display:none" >
        <span style="font-family:Tahoma; font-size:14px;color:#993366;">New Sequence does Not Exists.</span>
        <br /><br />
        <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancOk"><strong>OK</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>			
    <div id="progressBar" style="display:none" runat="server">
        <img  align=middle src="Images/indicator.gif" />
                 Processing... Please wait
    </div>											
</asp:Panel>

<cc1:ModalPopupExtender ID="mdlSwap" runat="server" TargetControlID="btnSwapTemp" PopupControlID="pnlSwap" OkControlID="" CancelControlID="ancSwapCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
			      
<asp:Panel runat="server" ID="pnlSwap" Width="200px" CssClass="pnlConfirm1" style="display:none;">
    <div id="divSwapModalParent" runat="server">
        <span style="font-family:Tahoma; font-size:14px;color:#993366;"><b>Swap With New Sequence</b></span><br /><br />
        
        <span style="display:none;"><b>Current Sequence - </b><asp:Label ID="lblSwapCurrentSeq" runat="server"></asp:Label><br /><br /></span>
        <asp:TextBox runat="server" ID="txtSwapNewSeq" Height="15" Width="150" CssClass="bodyTextGreyLeftAligned"></asp:TextBox>        
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSwapNewSeq" WatermarkText="Enter a new sequence here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        
        <br /><br />
            <div id="divButton" class="divButton" style="width: 85px; float:left;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                    <a class="buttonText cursorHand" onclick='javascript:validate_required("ctl00_ContentPlaceHolder1_UCProductListing1_txtSwapNewSeq","Enter a new sequence here","Swap")' id="a1">Submit</a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div>
            <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancSwapCancel"><strong>Cancel</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>	
    <div id="divSwaplblMsg" runat="server"  style="display:none" >
        <span style="font-family:Tahoma; font-size:14px;color:#993366;">New Sequence does Not Exists.</span>
        <br /><br />
        <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancSwapOk"><strong>OK</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>			
    <div id="SwapProgressBar" style="display:none" runat="server">
        <img  align=middle src="Images/indicator.gif" />
                 Processing... Please wait
    </div>		
    
   
</asp:Panel>

<asp:Button runat="server" ID="hdnButtonMoveTo" OnClick="MoveTo_Click" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
<asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

<asp:Button runat="server" ID="hdnButtonSwap" OnClick="Swap_Click" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
<asp:Button runat="server" ID="btnSwapTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
 <script type="text/javascript">
function validate_required(field,alerttxt,Mode)
{
    var text = document.getElementById(field).value;
       
      if (text==null||text==""||text=="Enter a new sequence here")
            {alert(alerttxt);
           }
        else 
        {                      
            if(Mode=="MoveTo")
            {
                document.getElementById("ctl00_ContentPlaceHolder1_UCProductListing1_divModalParent").style.display = "none";
                document.getElementById("ctl00_ContentPlaceHolder1_UCProductListing1_progressBar").style.display = "block";      
                document.getElementById("ctl00_ContentPlaceHolder1_UCProductListing1_hdnButtonMoveTo").click();
            }
            if(Mode=="Swap")
            {
                document.getElementById("ctl00_ContentPlaceHolder1_UCProductListing1_divSwapModalParent").style.display = "none";
                document.getElementById("ctl00_ContentPlaceHolder1_UCProductListing1_SwapProgressBar").style.display = "block";      
                document.getElementById("ctl00_ContentPlaceHolder1_UCProductListing1_hdnButtonSwap").click();
            }
        }
}
</script>

