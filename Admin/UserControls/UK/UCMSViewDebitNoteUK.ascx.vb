Public Partial Class UCMSViewDebitNoteUK
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs
    Protected WithEvents lblNewVat As Global.System.Web.UI.WebControls.Label


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim IsNew As Integer = 0
            Dim ds As DataSet = ws.WSFinance.MS_GetDebitNoteDetails(Request("BizDivId"), Request("PaymentNo"))

            If ds.Tables("tblOWAddress").Rows.Count > 0 Then
                'Company Name
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("CompanyName"))) Then
                    lblCompanyName.Text = ds.Tables("tblOWAddress").Rows(0)("CompanyName")
                End If
                'Address
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Address"))) Then
                    lblAddress.Text = ds.Tables("tblOWAddress").Rows(0)("Address")
                End If
                'OW city

                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("City"))) Then
                    lblCity.Text = ds.Tables("tblOWAddress").Rows(0)("City") & ","
                End If

                'OW post code
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("PostCode"))) Then
                    lblPostCode.Text = ds.Tables("tblOWAddress").Rows(0)("PostCode")
                End If

                'OW Phone
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Phone"))) Then
                    lblTelNo.Text = ds.Tables("tblOWAddress").Rows(0)("Phone")
                End If
                'OW Fax
                If Not (IsDBNull(ds.Tables("tblOWAddress").Rows(0)("Fax"))) Then
                    lblFaxNo.Text = ds.Tables("tblOWAddress").Rows(0)("Fax")
                End If
            End If

            If ds.Tables("tblBillingAddress").Rows.Count > 0 Then

                lblCompNameInvoiceTo.Text = ds.Tables("tblBillingAddress").Rows(0)("CompanyName").ToString

                'Address Invoice to
                If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("Address"))) Then
                    If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("State"))) Then
                        If Not (IsDBNull(ds.Tables("tblBillingAddress").Rows(0)("PostCode"))) Then
                            lblInvoiceTo.Text = ds.Tables("tblBillingAddress").Rows(0)("Address") & "<br>" & ds.Tables("tblBillingAddress").Rows(0)("State") & " " & ds.Tables("tblBillingAddress").Rows(0)("PostCode")
                        End If
                    End If
                End If

            End If

            If ds.Tables("tblCompRegNo").Rows.Count > 0 Then
                'Registration number
                lblRegistartionNo.Text = "Registered in England"

                If Not (IsDBNull(ds.Tables("tblCompRegNo").Rows(0)("CompanyRegNo"))) Then
                    lblRegistartionNoLine2.Text = "No.: " & ds.Tables("tblCompRegNo").Rows(0)("CompanyRegNo")
                End If

                If Not (IsDBNull(ds.Tables("tblCompRegNo").Rows(0)("VATRegNo"))) Then
                    lblVatNo.Text = ds.Tables("tblCompRegNo").Rows(0)("VATRegNo")
                End If
            End If

            If ds.Tables("tblDebitNoteDetails").Rows.Count > 0 Then
                IsNew = Convert.ToInt32(ds.Tables("tblDebitNoteDetails").Rows(0)("IsNew"))
            End If

           If ds.Tables("tblDebitNoteDetails").Rows.Count > 0 Then

                If IsNew = 0 Then  'calculate with old format

                  DivNewFormat.Visible = False
                    DivOldFormat.Visible = True
                    DivOldFormat1.Visible = True
                    DivOldFormat2.Visible = True

                    ds.Tables("tblDebitNoteDetails").DefaultView.Sort = "DateClosed"
                    rptList.DataSource = ds.Tables("tblDebitNoteDetails").DefaultView
                    rptList.DataBind()


                    If ds.Tables("tblTotalAmount").Rows.Count > 0 Then
                        'Total
                        If Not (IsDBNull(ds.Tables("tblTotalAmount").Rows(0)("Total"))) Then
                            lblTotalAmount.Text = FormatCurrency((ds.Tables("tblTotalAmount").Rows(0)("Total")), 2, TriState.True, TriState.True, TriState.False)
                        End If

                        'SubTotal
                        Dim totalAmt As Decimal = 0
                        If Not (IsDBNull(ds.Tables("tblTotalAmount").Rows(0)("SubTotal"))) Then
                            lblSubTotal.Text = FormatCurrency((ds.Tables("tblTotalAmount").Rows(0)("SubTotal")), 2, TriState.True, TriState.True, TriState.False)
                            totalAmt = ds.Tables("tblTotalAmount").Rows(0)("SubTotal")
                        End If

                        'Vat
                        Dim totalVATAmt As Decimal = 0
                        If Not (IsDBNull(ds.Tables("tblTotalAmount").Rows(0)("TotalVat"))) Then
                            lblTotalVAT.Text = FormatCurrency((ds.Tables("tblTotalAmount").Rows(0)("TotalVat")), 2, TriState.True, TriState.True, TriState.False)
                            totalVATAmt = ds.Tables("tblTotalAmount").Rows(0)("TotalVat")
                        End If


                       
                        'If Not (IsDBNull(ds.Tables("tblTotalAmount").Rows(0)("SPFees"))) Then
                        '    .Text = ds.Tables("tblTotalAmount").Rows(0)("SPFees").ToString
                        'End If
                        If Not (IsDBNull(ds.Tables("tblTotalAmount").Rows(0)("VATRate"))) Then
                            lblVATPer.Text = ds.Tables("tblTotalAmount").Rows(0)("VATRate")
                            lblTotalVATPer.Text = lblVATPer.Text
                        Else
                            lblVATPer.Text = ApplicationSettings.VATPercentage((ds.Tables("tblTotalAmount").Rows(0)("TotalVat")), (ds.Tables("tblTotalAmount").Rows(0)("SubTotal")))
                            lblTotalVATPer.Text = lblVATPer.Text
                        End If
                    End If
                Else 'calculate with new format

                    DivNewFormat.Visible = True
                    DivOldFormat.Visible = False
                    DivOldFormat1.Visible = False
                    DivOldFormat2.Visible = False


                    ds.Tables("tblDebitNoteDetails").DefaultView.Sort = "DateClosed"
                    rptListNew.DataSource = ds.Tables("tblDebitNoteDetails").DefaultView
                    rptListNew.DataBind()

                End If

                If Not (IsDBNull(ds.Tables("tblTotalAmount").Rows(0)("InvoiceNo"))) Then
                    lblInvoiceNo.Text = ds.Tables("tblTotalAmount").Rows(0)("InvoiceNo").ToString
                End If
                'Account No
                    If Not (IsDBNull(ds.Tables("tblDebitNoteDetails").Rows(0)("CompanyId"))) Then
                        lblAccountNo.Text = ds.Tables("tblDebitNoteDetails").Rows(0)("CompanyId")
                    End If

                    'Account No
                    If Not (IsDBNull(ds.Tables("tblDebitNoteDetails").Rows(0)("InvoiceDateCreated"))) Then
                        lblInvoiceDate.Text = Strings.Format((ds.Tables("tblDebitNoteDetails").Rows(0)("InvoiceDateCreated")), "dd-MM-yyyy")
                    End If
                    'sales invoice Number

                    If Not (IsDBNull(ds.Tables("tblDebitNoteDetails").Rows(0)("PurchaseInvoiceNo"))) Then
                        lblPurchaseInvoiceNo.Text = ds.Tables("tblDebitNoteDetails").Rows(0)("PurchaseInvoiceNo")
                    End If

            End If
        End If

    End Sub
End Class