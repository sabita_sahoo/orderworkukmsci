
<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCAOE.ascx.vb" Inherits="Admin.UCAOE" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<script language=javascript type="text/javascript" src="~/JS/Scripts.js"></script>

<input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />


<asp:UpdatePanel ID="UpdatePanel2" runat="server"  >
 <ContentTemplate>
	<table><tr>
    <td  ><asp:Label ID=lblMessage  CssClass="bodytxtValidationMsg" runat=server></asp:Label></td>
  </tr></table>
<asp:Panel ID="pnlAOEListBoxes" runat=server >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="formTxt" width="240px">Main-Category </td>
        <td class="formTxt"  width="285">Sub-Category(use CTRL key to make multiple selections) </td> 
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <asp:ListBox height=150px  ID="LBMainCategory"  AutoPostBack=true  class="formFieldWhite width202height110" DataTextField="Name" DataValueField="MainCatID" runat="server"></asp:ListBox>
        </td>
        <td>
          <asp:ListBox height=150px ID="LBSubCategory"  class="formFieldWhite width202height110" SelectionMode="multiple" DataTextField="Name" DataValueField="CombID" runat="server"></asp:ListBox>
       </td>
        <td align="left" valign="bottom" ><TABLE cellSpacing="0" cellPadding="0" width="70" bgColor="#a0a0a0" border="0">
								<TR>
									<TD height="18"><IMG runat=server height="18" src="~/Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
									<TD><asp:linkbutton   class="txtButtonNoArrow" id="lnkbtnSelect" CausesValidation=false runat="server">Submit</asp:linkbutton></TD>
									<TD width="5"><IMG runat=server height="18" src="~/Images/Curves/Grey-BtnRight.gif" width="5"></TD>
								</TR>
							</TABLE></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td valign=top height=20px class="formTxt"><em>Note: Please press Ctrl+ for multiple selection for Sub-Category </em></td>
    
  </tr>
</table>
</asp:Panel>
<asp:Panel ID="pnlSelectedAOE" runat=server Visible=false>
<table width="585"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="25" class="formTxt">List of selected Areas of Expertise </td>
  </tr>
  <tr>
    <td><table width="100%" class="tblBorder"  bgcolor="#A0A0A0" border="0" cellspacing="0" cellpadding="2">
      <tr >
        <td width="25" align="center" class="formtxt"><input id='chkAll' onClick=CheckAll(this,'CheckAOE') type='checkbox' name='chkAll' /></td>
        <td width="250" class="txtButtonNoArrow ">Main Category </td>
        <td class="txtButtonNoArrow">Sub Category</td>
      </tr>
    </table>
      <div id="divAOE" runat=server class="treeControl">
      <asp:GridView ID="gvSelectedAOE" CellPadding=2 CellSpacing=0 runat=server  AutoGenerateColumns="False" BorderWidth="1px"  BorderColor="#EDEDED" Width="583" >
               <Columns>  
                   <asp:TemplateField ItemStyle-CssClass="gridRowAOE" ItemStyle-Width="15px" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="center"   >
						<ItemTemplate >
							 <asp:CheckBox ID="CheckAOE" Runat="server" ></asp:CheckBox> <input type="hidden" Runat="server" id="hdnCombId" value=<%# databinder.eval(container.dataitem, "CombID") %>/>
						</ItemTemplate>
						<ItemStyle Wrap=true HorizontalAlign=center  Width="15px"  />
					</asp:TemplateField>
	                <asp:BoundField ItemStyle-CssClass="gridTextNew" ItemStyle-HorizontalAlign=left  DataField="MainCatName"   ItemStyle-Width="250px"   />            
    	            <asp:BoundField ItemStyle-CssClass="gridTextNew" ItemStyle-HorizontalAlign=left      DataField="SubCatName"  />                        
                </Columns>
                 <AlternatingRowStyle   BackColor="#E7E7E7" />
                 <RowStyle CssClass=gridRow />                            
               </asp:GridView></div></td>
  </tr>
  
   <tr>
    <td height="40" valign="middle" class="formTxt"> <TABLE cellSpacing="0" cellPadding="0" width="100" bgColor="#a0a0a0" border="0">
								<TR>
									<TD height="18"><IMG height="18" runat=server src="~/Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
									<TD><asp:linkbutton   class="txtButtonNoArrow" CausesValidation=false id="lnkbtnRemove" runat="server">Delete Selected</asp:linkbutton></TD>
									<TD width="5"><IMG height="18" runat=server src="~/Images/Curves/Grey-BtnRight.gif" width="5"></TD>
								</TR>
							</TABLE> </td>
  </tr>
</table>


</asp:Panel>

</ContentTemplate>   
</asp:UpdatePanel> 