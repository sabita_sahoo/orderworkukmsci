<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSubMenuDE.ascx.vb" Inherits="Admin.UCSubMenu" %>

<div id="divSubMenu">
	<div class="roundtopLightGrey"><img src="../Images/Curves/Submenu-LC.gif" alt="" width="5" height="5" class="corner" style="display: none" />
	</div>
	<asp:Datalist id="RPTMenu" runat="server" RepeatDirection="Horizontal">
		<ItemTemplate>
			<a href='<%#Container.DataItem("MenuLink")%>' class='SubMenuTxtBold'><%#Container.DataItem("MenuLabel")%></a>
		</ItemTemplate>
		<SelectedItemTemplate>
			<span class="SubMenuTxtBoldSelected"><%#Container.DataItem("MenuLabel")%></span>
		</SelectedItemTemplate>
		<SeparatorTemplate>
			<img src="../Images/Grey-Bullet.gif" width="4" height="4" class=marginL15 align=absmiddle>
		</SeparatorTemplate>
	</asp:Datalist>
</div>
