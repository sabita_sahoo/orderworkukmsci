<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSWOProcess.ascx.vb"
    Inherits="Admin.UCMSWOProcess" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<!--
dont go
 -->
<script language="javascript" src="JS/Scripts.js" type="text/javascript"></script>
<input type="hidden" id="hdnPPUp" runat="Server" value="0" />
<input type="hidden" id="hdnPrice" runat="Server" value="0" />
<input type="hidden" id="hdnAptTM" runat="Server" value="0" />
<input type="hidden" id="hdnPPUpliftPercent" runat="Server" value="0" />
<input type="hidden" id="hdnEvngInstTime" runat="Server" value="0" />
<asp:UpdatePanel ID="UpdatePanelProcessWO" runat="server">
    <contenttemplate>
<div id="divValidationMain" visible="false" class="divValidation" runat=server >
  <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" title="" width="5" height="5" class="corner" style="display: none" /></div>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td height="15" align="center">&nbsp;</td>
	  <td height="15">&nbsp;</td>
	  <td height="15">&nbsp;</td>
	</tr>
	<tr valign="middle">
	  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
	  <td class="validationText"><div  id="divValidationMsg"></div>
		  <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
           <asp:Panel runat="server" ID="pnlChangedData" Visible="false" style="width:350px;background-color:#D7D7D7;margin: 8px;padding : 8px;margin-bottom:0px;" >
          <asp:Repeater  ID="repChangedData" Runat="server" >
           <ItemTemplate> 

			<span style="line-height:16px;" runat="server" id="span1"><b><%# Eval("FullName")%> - <%#Eval("DateModified")%></b><br /></span>
			<span id="Span2" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldWOStatus") <> DataBinder.Eval(Container.DataItem, "NewWOStatus"), True, False)%>' >WO Status Changed:  <%# DataBinder.Eval(Container.DataItem, "NewWOStatus")%><br /></span>
            <span id="Span3" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldValue") <> DataBinder.Eval(Container.DataItem, "NewValue"), True, False)%>' >Price Changed:  <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br /></span>
			<span id="Span4" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldAptTime") & "" <> DataBinder.Eval(Container.DataItem, "NewAptTime"), True, False)%>' >Appointment Time Changed:  <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br /></span> 
			<span id="Span5" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldDateStart") <> DataBinder.Eval(Container.DataItem, "NewDateStart"), True, False)%>' ><asp:label id="lblStartdate" runat="server" Text='<%#IIf(DataBinder.Eval(Container.DataItem, "BusinessArea") <> 101,"Start Date Changed:","Appointment Date Changed:")%>'></asp:label> <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br /></span>
			<span id="Span6" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldDateEnd") <> DataBinder.Eval(Container.DataItem, "NewDateEnd"), True, False)%>' >End Date Changed:  <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br /></span>
			<span id="Span7" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldPlatformPrice") <> DataBinder.Eval(Container.DataItem, "NewPlatformPrice"), True, False)%>' >Platform Price Changed:  <%# DataBinder.Eval(Container.DataItem, "NewPlatformPrice")%><br /></span>
            <span id="Span8" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldWholesalePrice") <> DataBinder.Eval(Container.DataItem, "NewWholesalePrice"), True, False)%>' >Wholesale Price Changed:  <%# DataBinder.Eval(Container.DataItem, "NewWholesalePrice")%><br /></span>
            <span id="Span9" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldPONumber") <> DataBinder.Eval(Container.DataItem, "NewPONumber"), True, False)%>' >PO Number Changed:  <%# DataBinder.Eval(Container.DataItem, "NewPONumber")%><br /></span>
            <span id="Span10" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldJobNumber") <> DataBinder.Eval(Container.DataItem, "NewJobNumber"), True, False)%>' >Job Number Changed:  <%# DataBinder.Eval(Container.DataItem, "NewJobNumber")%><br /></span>
            <span id="Span11" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldGoodsLocation") <> DataBinder.Eval(Container.DataItem, "NewGoodsLocation"), True, False)%>' >Goods Location Changed:  <%# DataBinder.Eval(Container.DataItem, "NewGoodsLocation")%><br /></span>
                        
            </ItemTemplate>
          </asp:Repeater>
          </asp:Panel>
		  <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
	  </td>
	  <td width="20">&nbsp;</td>
	</tr>
	<tr>
	  <td height="15" align="center">&nbsp;</td>
	  <td height="15">&nbsp;</td>
	  <td height="15">&nbsp;</td>
	</tr>
  </table>
  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" title="" width="5" height="5" class="corner" style="display: none" /></div>
</div>

<asp:panel ID="pnlWOProcess" runat="server">
<table id="DGWorkOrders" width="100%" border="0" cellpadding="5" cellspacing="0">
  <tr valign="top" >
    <td  class="gridHdr gridBorderRB">WO No</td>
    <td  class="gridHdr gridBorderRB"><asp:label ID="lblColNameDateCreated" runat="server"></asp:label></td>
    <td  class="gridHdr gridBorderRB">Location</td>
	<td  class="gridHdr gridBorderRB">Contact</td>
	<td  class="gridHdr gridBorderRB">WO Title</td>
	<td  class="gridHdr gridBorderRB">WO Start</td>
	<td  class="gridHdr gridBorderRB"><asp:label ID="lblColNamePrice" runat="server"></asp:label></td>
	<td id="tdColNameProposedDayRate" runat=server  class="gridHdr gridBorderRB"><asp:label ID="lblColNameProposedDayRate" runat="server"></asp:label></td>
	<td id="tdColNameEstimatedTimeInDays" runat=server  class="gridHdr gridBorderRB"><asp:label ID="lblColNameEstimatedTimeInDays" runat="server"></asp:label></td>
	<td  class="gridHdrHighlight gridBorderB">Status</td>
  </tr>
 <tr>
    <td width="55px"  class="gridRow gridBorderRB" align="left" ><asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label></td>
	<td width="75px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblCreated"></asp:Label></td>
	<td width="65px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblLocation"></asp:Label></td>
	<td width="80px"  class="gridRow gridBorderRB" align="left" ><asp:Label runat="server" CssClass="formTxt" ID="lblContact"></asp:Label></td>
	<td  class="gridRow gridBorderRB" align="left" ><asp:Label  runat="server" CssClass="formTxt" ID="lblTitle"></asp:Label></td>
	<td width="75px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOStart"></asp:Label></td>
	<td width="90px" align="right" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblProposedPrice"></asp:Label>&nbsp;</td>
	<td width="90px" id="tdlblProposedDayRate" runat="server" align="right" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblProposedDayRate"></asp:Label>&nbsp;</td>
	<td width="90px" id="tdlblEstimatedTimeInDays" runat="server" align="right" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblEstimatedTimeInDays"></asp:Label>&nbsp;</td>
	<td width="57px" class="gridRowHighlight gridBorderB"><asp:Label runat="server" CssClass="gridRowHighlight" ID="lblStatus"></asp:Label></td>
  </tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
	  <td height="10" align="left" valign="middle">&nbsp;</td>
	  <td class="smallText"></td>
</tr>
<tr>
	 <td height="25" align="left" valign="middle"><asp:Label runat="server" ID="lblMsg1" CssClass="txtWelcome" Text=""></asp:Label></td>
	 <td width="20">&nbsp;</td>
</tr>
</table>

<!-- this is the data grid which can be modified-->
<asp:Panel runat="server" ID="pnlActionModifyGrid">
<table width="940" cellspacing="0" cellpadding="0">
	 <tr id="tblDGYourResponseHdr" align="left">
		  <td width="13" valign="top" style="padding:0px; border:0px; "><img src="Images/Curves/Bdr-Red-TL.gif" width="5" height="5" hspace="0" vspace="0" border="0"></td>
		  <td width="153"><asp:Label ID="lblstrtHdrIssue" Text="Start" runat="server"></asp:Label> 
			<asp:RegularExpressionValidator id="RegularExpressionValidator3" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtDateStart" ErrorMessage="Start Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$" >*</asp:RegularExpressionValidator>
		  </td>
		  <td width="153" id="tdEndDHdr" runat="server">End<asp:RegularExpressionValidator id="regExEndDate" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtDateEnd" ErrorMessage="End Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$" >*</asp:RegularExpressionValidator>							  
		  <asp:CustomValidator id="rqWODateRange" runat="server" ErrorMessage="End Date should be greater than Start Date" ForeColor="#EDEDEB" OnServerValidate="custValiDateRange_ServerValidate" ControlToValidate="txtDateEnd">*</asp:CustomValidator>	
		  </td>
		  <td width="153" id="tdAptTimeHdr" runat="server" visible="false">Apt. Time</td>
		 <td width="152" style="padding-left:3px; text-align:right;"><asp:label id="lblPrice" runat="server" Text="Price"></asp:label><asp:RequiredFieldValidator id="rqWOValue" runat="server" ErrorMessage="Please enter Price" ForeColor="#EDEDEB"	ControlToValidate="txtValue">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator id="RegularExpressionValidator1" CssClass="bodytxtValidationMsg" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtValue"  ErrorMessage="The price should not be less that 0" ValidationExpression="[0-9,.]*" >*</asp:RegularExpressionValidator>
		 </td>
		 <td id="tdHdrPropDayRate" runat=server style="padding-left:3px; text-align:right;">Proposed Day Rate </td>
		 <td id="tdHdrEstTime" runat=server style="padding-left:3px; text-align:right;">Estimated Time (Days) </td>
		  <td style=" border-right-width:0px; ">&nbsp;</td>
		  <td width="16" align="right" valign="top" style=" border-right-width:0px; padding:0px;"><img src="Images/Curves/Bdr-Red-TR.gif" width="5" height="5" hspace="0" vspace="0" border="0"></td>
	  </tr>
	<tr  align="left">
		  <td class="tblDGRowInside" valign="top" style="padding:0px; border-right:0 ">&nbsp;</td>
		  <td class="tblDGRowInside"><asp:TextBox style=" width:65px;" runat="server" ID="txtDateStart" CssClass="txtWelcome"></asp:TextBox>
			 
			 <img alt="Click to Select" src="Images/calendar.gif" id="btnStartDate" style="cursor:pointer; vertical-align:top;" />
              <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnStartDate TargetControlID="txtDateStart" ID="calStartDate" runat="server">
              </cc1:CalendarExtender> 
		</td>
		  <td class="tblDGRowInside" id="tdEndDTxt" runat="server"><asp:TextBox style=" width:65px;" runat="server" ID="txtDateEnd" CssClass="txtWelcome"></asp:TextBox>			  
			  <img alt="Click to Select" src="Images/calendar.gif" id="btnEndDate" style="cursor:pointer; vertical-align:top;" />
              <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnEndDate TargetControlID="txtDateEnd" ID="calEndDate" runat="server">
              </cc1:CalendarExtender>
		  </td>
		  <td class="tblDGRowInside" id="tdAptTime" runat="server" visible="false" >
		  <asp:DropDownList ID="ddlAptTime" runat="server" onchange='javascript:ChangeAptTimeNPrice(this.id);'>
		 <%-- <asp:ListItem Text="8am - 1pm"></asp:ListItem> 
		  <asp:ListItem Text="1pm - 6pm"></asp:ListItem> 
		  <asp:ListItem Text="All Day" Value="All Day"  ></asp:ListItem>--%>
		  </asp:DropDownList>   
		  </td>
		  <td class="tblDGRowInside" style="padding-left:2px; padding-right:5px; text-align:right;"><span class="formLabelGrey">&nbsp; <%# "&" & admin.Applicationsettings.Currency %> &nbsp;</span> &pound; <asp:TextBox style=" width:80px;text-align:right;" runat="server" ID="txtValue" CssClass="txtWelcome" Enabled="false"></asp:TextBox></td>
		  <td id="tdPropDayRate" runat=server class="tblDGRowInside" style="padding-left:2px; padding-right:5px; text-align:right;"><span class="formLabelGrey">&nbsp; <%# "&" & admin.Applicationsettings.Currency %> &nbsp;</span> &pound;  <asp:TextBox style=" width:80px;text-align:right;" runat="server" ID="txtProposedRate" CssClass="txtWelcome" onChange="javascript:CalculatePrice(this.id)"></asp:TextBox></td>
		  <td id="tdEstTime" runat=server class="tblDGRowInside" style="padding-left:2px; padding-right:5px; text-align:right;"><span class="formLabelGrey">&nbsp; <%# "&" & admin.Applicationsettings.Currency %> &nbsp;</span><asp:TextBox style=" width:80px;text-align:right;" runat="server" ID="txtEstimatedTimeInDays" CssClass="txtWelcome" onChange="javascript:CalculatePrice(this.id)"></asp:TextBox></td>
		  <td class="tblDGRowInside" style=" border-right-width:0px; ">&nbsp;</td>
		  <td class="tblDGRowInside" align="right" valign="top" style=" border-right-width:0px; padding:0px;">&nbsp;</td>
	  </tr>
</table>
</asp:Panel>
<!-- end of the data grid which can be modified-->

<!-- this is the data grid which cannot be modified-->
<asp:Panel runat="server" ID="pnlActionNonModifyGrid">
	<table  width="940" cellspacing="0" cellpadding="0" >
		  <tr>
			<td width="5">&nbsp;</td>
			<td height="28" colspan="7" align="left" valign="middle" class="SubMenuTxtBoldSelected"><asp:Label ID="lblDLHeader" runat="server"></asp:Label></td>
			<td width="5">&nbsp;</td>
		  </tr>
		 <tr id="tblDGYourResponseHdr" align="left">
			  <td width="5" valign="top" style="padding:0px; border:0px; "><img src="Images/Curves/Bdr-Red-TL.gif" width="5" height="5" hspace="0" vspace="0" border="0"></td>
			  <td width="100">Submitted</td>
			  <td width="100"><asp:Label ID="lblStart" runat="server" Text="Start"></asp:Label></td>
			  <td width="100" id="tdEndDateHdr" runat=server><asp:Label ID="lblEnd" runat="server" Text="End"></asp:Label></td>
			  <td width="200"><asp:Label ID="lblNonModifyPrice" runat="server"></asp:Label></td>
			  <td runat="server" id="tdDLDayRate" visible=false>Proposed Day Rate</td>
			  <td runat="server" id="tdDLEstimatedTime" style="border-right-width:0px;"  width="200" visible=false>Estimated Time (Days)</td>
			  <td width="200" runat="server" id="tdLblSpecialistName">Specialist</td>
			  <td style="border-right-width:0px;" id="tdStatusHdr" runat=server><span class="formTxtOrange">Status</span></td>
			  
			  <td id="tdCurveHdr" runat=server width="5" align="right" valign="top" style=" border-right-width:0px; padding:0px;"><img src="Images/Curves/Bdr-Red-TR.gif" width="5" height="5" hspace="0" vspace="0" border="0"></td>
		</tr>
		<tr align="left">
			  <td class="tblDGRowInside" width="5" valign="top" style="padding:0px;  border-right:0 ; ">&nbsp;</td>
			  <td class="tblDGRowInside" width="100"><asp:Label runat="server" ID="lblSubmitted"></asp:Label></td>
			  <td class="tblDGRowInside" width="100"><asp:label CssClass=<%# iif(lblStartD.Text <> lblWOStart.Text,"bodytxtValidationMsg","gridRow")%> id="lblStartD" runat="server"></asp:label></td>
			  <td class="tblDGRowInside" width="100" id="tdEndDateValue" runat=server><asp:label id="lblEndD" runat="server"></asp:label><asp:label id="lblAptTime" runat="server"></asp:label></td>
			  <td class="tblDGRowInside" style="padding-left:2px; padding-right:5px; text-align:right;"> <asp:label id="lblNewPrice" runat="server"></asp:label> </td>
			  <td class="tblDGRowInside" style="padding-left:2px; padding-right:5px; text-align:right;" runat="server" id="tdDLDayRate1" width="200" visible=false> <asp:Label runat="server" ID="lblDayRate"></asp:Label></td>
			  <td class="tblDGRowInside" style="border-right-width:0px;" runat="server" id="tdDLEstimatedTime1" width="200" visible=false><asp:Label runat="server" ID="lblTimeInDays"></asp:Label></td>
			  <td class="tblDGRowInside" width="200" runat="server" id="tdSpecialistName"><asp:label id="lblSpecialistName" runat="server"></asp:label></td>
			  <td class="tblDGRowInside"  style=" border-right-width:0px; " id="tdStatusValue" runat=server><asp:Label CssClass="bodytxtValidationMsg" runat="server" ID="lblCurrentStatus"></asp:Label></td>
			  
			  <td id="tdCurveData" runat=server class="tblDGRowInside" width="5" align="right" valign="top" style=" border-right-width:0px; padding:0px;">&nbsp;</td>
			</tr>
	</table>
</asp:Panel>
<!-- end of the data grid which cannot be modified-->

<!-- remarks area-->
<asp:Panel runat="server" ID="pnlRemarks" Visible="false">
<table width="940"  border="0" cellpadding="0" cellspacing="0" bgcolor="#EDEDED" class="marginB5">
		 <tr>
			<td width="10" height="10" style="padding:0px; margin:0px"></td>
			<td height="10" style="padding:0px; margin:0px"></td>
			<td width="10" height="10" style="padding:0px; margin:0px"></td>
		</tr>
		<tr>
			<td width="10">&nbsp;</td>
			<td>
				
					<div class="topCurveRightRemarks marginT2"><img src="Images/Curves/Bdr-Box2-TL.gif"></div>
					<table id="tblDiscussion" width="920"  border="0" cellspacing="0" cellpadding="0" bgcolor="#FAFCF1">
					
				  <tr>
						<td width="10px">&nbsp;</td>
						<td height="32px" align="left" class="labelText" style="padding:0px 10px 0px 10px"><strong>Remarks :</strong></td>
					  </tr>
					  <tr>
						<td width="10px">&nbsp;</td>
						<td>
							<asp:DataList  RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLDiscussion" Runat="server" Width="920">
						    <ItemTemplate>
							  <table width="680px"  border="0" cellspacing="0" cellpadding="0">
								<tr>
								  <td width="200" valign="top" style="padding:0px 0px 0px 10px"><%#IIf(Viewer = admin.Applicationsettings.ViewerAdmin, IIf(DataBinder.Eval(Container.DataItem, "ContactClassID") = admin.Applicationsettings.RoleOWID, "<span class='discussionTextClient'>" & Container.DataItem("CommentsDate") & " - " & (Container.DataItem("CommentsBy")) & " </span>", "<span class='discussionTextSupplier'>" & Container.DataItem("CommentsDate") & " - " & (Container.DataItem("CommentsBy")) & " </span>"), IIf(Viewer = admin.Applicationsettings.ViewerSupplier, IIf(DataBinder.Eval(Container.DataItem, "ContactClassID") = admin.Applicationsettings.RoleSupplierID, "<span class='discussionTextSupplier'>" & Container.DataItem("CommentsDate") & " - " & (Container.DataItem("CommentsBy")) & " </span>", "<span class='discussionTextClient'>" & Container.DataItem("CommentsDate") & " - " & (Container.DataItem("CommentsBy")) & " </span>"), ""))%></td>
								  <td width="20" align="center" valign="top">:</td>
								  <td valign="top" class="remarksArea" style="padding:0px 10px 0px 0px"> <span class="labelText"><%# Container.DataItem("Comments") %> </span></td>
								</tr>
							  </table>
							</ItemTemplate>
					      </asp:DataList>
						</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					</table>
					<div  class="botCurveRightRemarks"><img src="Images/Curves/Bdr-Box2-BL.gif"></div>
				
				</td>
			<td width="10">&nbsp;</td>
	   </tr>
		<tr>
		  <td height="10" style="padding:0px; margin:0px"></td>
		  <td height="10" style="padding:0px; margin:0px"></td>
		  <td height="10" style="padding:0px; margin:0px"></td>
		  </tr>
	  
</table>
</asp:Panel>
<!-- end of remarks area-->
				
<asp:Panel ID="pnlSpecialist" CssClass="txtWelcome" runat="server">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	  <td height="20" align="left" valign="middle">&nbsp;</td>
	  <td>&nbsp;</td>
	</tr>
	<tr>
		 <td height="10" align="left" valign="middle"><asp:Label ID="lblSpecialist" CssClass="formLabelGrey" runat="server"></asp:Label>
		 <asp:DataGrid BorderWidth="1px" BorderStyle="Solid" BorderColor="#FFFFFF" CellPadding="5" CellSpacing="0" Width="925" AutoGenerateColumns="false" ID="DGSpecialists" runat="server" HeaderStyle-ForeColor="#555354" AllowPaging="False" AllowSorting="True">
		  <Columns>
		  <asp:TemplateColumn HeaderStyle-CssClass="gridHdr" ItemStyle-Width="5px" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="left" ItemStyle-CssClass="gridRow">
			<ItemTemplate >
			  <input type="radio" name="rdoSpecialist" id="rdoValidSpecialist" checked=<%# iif(databinder.eval(container.dataitem, "ContactID") = ViewState("SpecialistID"), true, false) %> onClick="javascript:CheckRadioSpecialists(this)" runat="server"/>
			  <input type="hidden" Runat="server" id="SpecialistID" value=<%# databinder.eval(container.dataitem, "ContactID") %>/>
			</ItemTemplate>
		  </asp:TemplateColumn>
		  <asp:BoundColumn HeaderStyle-CssClass="gridHdr" ItemStyle-CssClass="gridRow" ItemStyle-Width="250px" HeaderText="Contact"  DataField="Name"></asp:BoundColumn>
		  </Columns>
		</asp:DataGrid>
		 </td>
		 <td class="smallText" width="20"></td>
	</tr>
	<tr>
	  <td height="10" align="left" valign="middle">&nbsp;</td>
	  <td class="smallText"></td>
	  </tr>
</table>
</asp:Panel>

 


<table width="100%"  border="0" cellspacing="0" cellpadding="0">
 <%--<tr>
 
    <td align="left" valign="top" class="bodyTextGreyLeftAligned" style="padding-left:20px;"><asp:Panel ID="pnlShowAttachments" runat="server">
    <asp:Literal ID="litAttachments" runat="server" />                      
    </asp:Panel>
    </td>
    <td>&nbsp;</td>
</tr>--%>
<tr id="trChangeService" runat="server" visible="false">
    <td width="20">&nbsp;</td>
    <td class="formLabelGrey" width="120">Change Service</td>
    <td class="formLabelGrey" ><asp:DropDownList id="drpdwnProduct" runat="server" CssClass="formFieldGrey" Width="300"  Height="22"></asp:DropDownList>   </td>
    <td width="20">&nbsp;</td>
</tr>
<tr id="trChangeSpecialist" runat="server" visible="false">
    <td width="20">&nbsp;</td>
    <td class="formLabelGrey"><br style="clear:both;" />Change Specialist</td>
    <td class="formLabelGrey" ><br style="clear:both;" /><asp:DropDownList id="drpdwnSpecialist" runat="server" CssClass="formFieldGrey" Width="300"  Height="22"></asp:DropDownList>   </td>
    <td width="20">&nbsp;</td>
</tr>
<tr>
	<td colspan="3" class="formLabelGrey" >&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
    <td width="20">&nbsp;</td>
    <td colspan="2" class="formLabelGrey" ><asp:Label runat="server" ID="lblcommentsHdg" Text="Please add any comments you have below. Thank You."></asp:Label> <asp:RequiredFieldValidator id="rqWOComments" runat="server" ErrorMessage="Please enter Comment." ForeColor="#EDEDEB"
			ControlToValidate="txtWOComment">*</asp:RequiredFieldValidator></td>
	<td width="20">&nbsp;</td>
</tr>
<tr>
	<td colspan="3"><asp:TextBox id="txtWOComment"  runat="server" CssClass="formFieldGrey width934" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" Rows="4"></asp:TextBox></td>
	<td width="20">&nbsp;</td>
</tr>
</table>

 	<asp:Panel runat="server" ID="pnlAttachments" Visible="false">
	<br />
	 <div id="Div1" class="divAttachment" runat=server  >
	<div class="AttachtopVal"><img src="Images/Curves/Attach-TL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
	
	
		<%@ Register TagPrefix="uc1" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>
		<uc1:UCFileUpload id="UCFileUpload1" runat="server" Control="UCWOProcess1_UCFileUpload1" 
		Type="AdminWOIssue" AttachmentForSource="AdminWOIssue"   ShowNewAttach="True" ShowUpload="True" MaxFiles="12" ></uc1:UCFileUpload>                             
	   
	   <div class="AttachbottomVal"><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
    </div>
	</asp:Panel>

<table  border="0" align="right" cellpadding="0" cellspacing="0">
	<tr>
	 	<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	    <td width="20">&nbsp;</td>
	</tr>
	<tr>
		<td align="right" valign="top">
			  <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0" runat="server" id="tblConfirm">
				<TR>
				  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
				  <TD class="txtBtnImage">
				  <asp:LinkButton ID="lnkConfirm" CausesValidation="false" runat="server" CssClass="txtListing"></asp:LinkButton></TD>
				  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
				</TR>
		  </TABLE>
		</td>
        	<td align="right" valign="top">
			  <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0" runat="server" id="tblConfirmAndBack" visible="false">
				<TR>
				  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
				  <TD class="txtBtnImage">
				  <asp:LinkButton ID="lnkConfirmAndBack" CausesValidation="false" runat="server" CssClass="txtListing"></asp:LinkButton></TD>
				  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
				</TR>
		  </TABLE>
		</td>
		<td align="right">
		  <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
			<TR>
			  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			  <TD class="txtBtnImage">
			   <a id="lnkCancel" runat="server" CausesValidation="false" class="txtListing" href="#"><img src='Images/Icons/Cancel.gif' title='Cancel' width='11' height='11' align='absmiddle' hspace='1' vspace='2' border='0'> Cancel</a></TD>
			  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
			</TR>
		  </TABLE>
		</td>
        <td width="20" align="right">&nbsp;</td>
   </tr>
</table>
</asp:panel>
</contenttemplate>
    <triggers>
  <asp:AsyncPostBackTrigger EventName="Click" ControlID="lnkConfirm" />
  </triggers>
</asp:UpdatePanel>
<asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelProcessWO"
    runat="server">
    <progresstemplate>
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Please wait while we process your data ...
            </div>      
        </progresstemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="pnlWOProcess"
    CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
