<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCFileUploadInner_Insurance.ascx.vb" Inherits="Admin.UCFileUpload_Inner" %>
<style>
.formFieldGrey {
    border: 1px solid #999999;
    color: #5F5F5F;
    font-family: Geneva,Arial,Helvetica,sans-serif;
    font-size: 11px;
    line-height: 11px;
    padding-left: 3px;
    text-align: left;
    text-decoration: none;
    vertical-align: middle;
}
.txtButtonNoArrow {
    background-color: #A0A0A0;
    background-position: center center;
    background-repeat: no-repeat;
    color: #FFFFFF;
    font-family: Geneva,Arial,Helvetica,sans-serif;
    font-size: 11px;
    font-weight: normal;
    text-decoration: none;
}
</style>

 <table width="100%" id="tblFileAttachments" border="0" cellspacing="0" cellpadding="0" runat="server"  >
      <tr >
        <td >
	        <asp:FileUpload ID="FileUpload1" CssClass="formFieldGrey" width="160px" runat="server" />
	  
        </td>
    
      </tr>
      
   
      <tr align="left" valign="bottom">
        <td height="18" >
            <asp:label id="lblMsg" runat="server" class="formTxt"/>
        </td>
      </tr>
      
      
         <tr align="left" valign="bottom">
         <td>
        
                   	    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;line-height:20px;">
                            <asp:linkbutton CausesValidation="false" class="txtButtonNoArrow" id="btnUpload" runat="server" style="background-color:#993366 !important;">Upload</asp:linkbutton>
                        </div>
        </td>
      </tr>

</table>


