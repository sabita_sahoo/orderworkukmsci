<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCLocationForm.ascx.vb" Inherits="Admin.UCLocationForm" %>


<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:RequiredFieldValidator id="RqdName" runat="server" ForeColor="#EDEDEB" ErrorMessage="Please enter Location Name" ControlToValidate="txtName">*</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator id="RequiredFieldValidator6" runat="server" ForeColor="#EDEDEB" ErrorMessage="Please enter Postal Code" ControlToValidate="txtCompanyPostalCode">*</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" ForeColor="#EDEDEB" ErrorMessage="Please enter Address"
																ControlToValidate="txtCompanyAddress">*</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" ForeColor="#EDEDEB" ErrorMessage="Please enter City"
																ControlToValidate="txtCity">*</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator id="RequiredFieldValidator7" runat="server" ForeColor="#EDEDEB" ErrorMessage="Please enter Country"
																ControlToValidate="ddlCompanyCountry">*</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator id="RequiredFieldValidator8" runat="server" ForeColor="#EDEDEB" ErrorMessage="Please enter Phone"
																ControlToValidate="txtCompanyPhone">*</asp:RequiredFieldValidator>

<asp:UpdatePanel ID="UpdatePnlLocationForm" runat="server">
  <ContentTemplate>

<asp:Panel ID="pnlSubmitForm" runat="server" visible="true">

<div id="divValidationMain" class="divValidation" runat=server >
	<div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="15" align="center">&nbsp;</td>
				<td height="15">&nbsp;</td>
				<td height="15">&nbsp;</td>
			</tr>
			<tr valign="middle">
				<td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				<td class="validationText"><div  id="divValidationMsg"></div>
				<asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
				<asp:ValidationSummary id="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg bg" DisplayMode="BulletList"></asp:ValidationSummary>
				</td>
				<td width="20">&nbsp;</td>
			</tr>
			<tr>
				<td height="15" align="center">&nbsp;</td>
				<td height="15">&nbsp;</td>
				<td height="15">&nbsp;</td>
			</tr>
		</table>
	<div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
</div>	


<div id="divMainTab">
              <DIV class="paddedBox" id="divAccount" runat="server">
              
   			<TABLE width="100%" border="0" cellPadding="0" cellSpacing="0">
              <TR>
                <TD align="left" class="HeadingRed"><STRONG>Location</STRONG></TD>
                <TD align="left" class="padTop17">
                    <TABLE id="tblSaveTop" runat=server cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                      <TR>
                        <TD align="right" vAlign="top">
                            <TABLE cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
                                <TR>
                                  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                  <TD width="110" class="txtBtnImage"><asp:Linkbutton runat="server" id="btnSaveTop" CausesValidation="false" CssClass="txtListing" TabIndex="10"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:Linkbutton></TD>
                                  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                </TR>
                            </TABLE>
                        </TD>
                        <TD width="10"></TD>
                        <TD width="125" align="right" vAlign="top" style="padding:0px 0px 0px 0px;">
                            <TABLE cellSpacing="0" cellPadding="0" width="125" bgColor="#F0F0F0" border="0">
                                <TR>
                                  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                  <TD width="115" class="txtBtnImage"><asp:LinkButton id="btnResetTop" CausesValidation="false" runat="server" TabIndex="11"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton></TD>
                                  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                </TR>   
                            </TABLE>
                        </TD>
                        <TD width="10"></TD>
                        
                        <TD width="60" align="right">
                            <TABLE width="60" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
                                <TR>
                                    <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                    <TD class="txtBtnImage">
                                        <asp:LinkButton id="btnCancelTop" runat="server" TabIndex="12"  CausesValidation="false" class="txtListing"> <img src="Images/Icons/Icon-Cancel.gif" align="absmiddle" width="11" height="10" hspace="5" vspace="0" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
	    			 	            </TD>
                                    <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                </TR>
                            </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                </TD>
                
              </TR>
            </TABLE>  
       		  

			
		
         
				
                <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
                  <tr valign="top">
                    <td class="formTxt" valign="top" align="left" width="250" height="14">Name<span class="bodytxtValidationMsg"> *</span>
                        </td>
                    <td class="formTxt" align="left" height="14">
                    <asp:CustomValidator ErrorMessage="A main location already exists. There can be only one main location at a time - so first unassign the current main location and then assign this location to be the main one." ID="MainLocationExists" CssClass="bodytxtValidationMsg" runat="server">&nbsp;</asp:CustomValidator></td>
                  </tr>
                  
                  <tr valign="top">
                    <td align="left" height="24"><asp:TextBox id="txtName" tabIndex="13" runat="server" CssClass="formField width240"></asp:TextBox></td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
                <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr valign="top">
                      <td colspan="2" class="formTxt" align="left" width="162" height="14">Postal Code<span class="bodytxtValidationMsg"> *</span>
                            </td>                    
                      <td class="formTxt" align="left" height="14"></td>
                    </tr>                  
                    <tr valign="top">
                      <td align="left" height="24"><asp:TextBox id="txtCompanyPostalCode" onkeyup="makeUppercase(this.id)" tabIndex="15" MaxLength="8" runat="server" CssClass="formField width150"></asp:TextBox>
                      <asp:RegularExpressionValidator ID="rgtxtCompanyPostalCode" Display="None" runat="server"
ControlToValidate="txtCompanyPostalCode" ErrorMessage="Please enter valid postcode"
ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$"></asp:RegularExpressionValidator>
                      <asp:Button id="btnFind" CausesValidation="false" tabIndex="16" runat="server" CssClass="formField" Text="Get Address" Width="75" style="margin-left:10px;" /></td>
                    </tr>
                </table>
                <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server" id="tblAddList" visible="false">
                    <tr valign="top">
                      <td class="formTxt" align="left" width="162" height="14">
                        <asp:ListBox runat="server" CssClass="formField" ID="lstProperties"  AutoPostBack="true" style="width:475px; height:250px; margin:5px 0px;" Visible="false"></asp:ListBox>
                      </td>                      
                    </tr>
                    <tr class="formTxt" align="left" width="162" height="14">
                        <td valign="Top" style="padding-bottom:10px; color:#FF0000; font-size:11px; width:354px;"><asp:Label runat="Server" ID="lblErr"></asp:Label></td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                  <tr>
                    <td class="formTxt" valign="top">Address<span class="bodytxtValidationMsg"> *</span>
                        </td>
                    <td class="formTxt" valign="top">City<span class="bodytxtValidationMsg"> *</span>
                        </td>
					<td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td valign="top" width="324" height="24"><asp:TextBox id="txtCompanyAddress" tabIndex="17" runat="server" CssClass="formField width312"></asp:TextBox></td>
                    <td valign="top" height="24" width="170"><asp:TextBox id="txtCity" tabIndex="18" runat="server" CssClass="formField width150"></asp:TextBox></td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
                <div id="divLocLeft" style="width:500px; float:left; clear:right">
                <table height="18" cellspacing="0" cellpadding="0" width="496px" border="0">
                  <tr valign="top">
                    <td class="formTxt" align="left" width="162" height="14">County/State</td>                    
                    <td class="formTxt" align="left" height="14" width="170">Country<span class="bodytxtValidationMsg"> *</span>
                        </td>
				    <td>&nbsp;</td>
                  </tr>
                  <tr valign="top">
                    <td align="left" height="24"><asp:TextBox id="txtCounty" tabIndex="20" runat="server" CssClass="formField width150"></asp:TextBox></td>                    
                    <td align="left" height="24"><asp:DropDownList id="ddlCompanyCountry" tabIndex="21" runat="server" CssClass="formField width150"></asp:DropDownList></td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="496px" border="0">
                  <tr valign="top">
                    <td class="formTxt" align="left" width="162" height="14">Phone<span class="bodytxtValidationMsg"> *</span>
                        </td>
                    <td class="formTxt" align="left" width="162" height="14">Fax</td>
                    <td class="formTxt" align="left" height="14">&nbsp;</td>
                  </tr>
                  <tr valign="top">
                    <td align="left" height="24"><asp:TextBox id="txtCompanyPhone" tabIndex="23" runat="server" CssClass="formField width150"></asp:TextBox></td>
                    <td align="left" height="24"><asp:TextBox id="txtCompanyFax" tabIndex="24" runat="server" CssClass="formField width150"></asp:TextBox></td>
                    <td align="left" height="24"><asp:CheckBox id="ChkIsDeleted" runat="server" Visible="false" Text='Mark Location as Deleted' TabIndex="25"  CssClass="formTxt"></asp:CheckBox></td>
                  </tr>
                </table>
				</div>
				
				<div>
				<table width="100%" border="0" cellPadding="0" cellSpacing="0">
				<tr>
				<TD align="left" class="HeadingRed"><STRONG>Location Preferences</STRONG></TD>
				</tr>

                <tr valign="top">
                    <td class="formTxt" align="left" width="100" height="14">Fixed Capacity</td>
                  </tr>
                  <tr valign="top">
                    <td align="left" height="24"><asp:TextBox id="txtFixedCapacity" runat="server"  CssClass="formField width150"></asp:TextBox></td>
                  </tr>
       			<tr>
				<td height="24" align="left" valign="middle"><asp:CheckBox id="ChkLocation" runat="server" Text='Is Main Location' TabIndex="14"  CssClass="formTxt" AutoPostBack="true"></asp:CheckBox>
                    </td>
				</tr>
                	<tr>
				<td height="24" align="left" valign="middle"><asp:CheckBox id="chkOrderMatch" runat="server" Text='Do Not Include in Ordermatch' TabIndex="14"  CssClass="formTxt" ></asp:CheckBox>
                    </td>
				</tr>
				<tr>
				<td height="24" align="left" valign="middle"><asp:CheckBox id="chkbxDepot" runat="server" Text='Depot' TabIndex="19"  CssClass="formTxt" AutoPostBack="true"></asp:CheckBox></td>
				</tr>
				
				
				<tr runat="server" id="trDepotLocDetails" >
				<td height="24" align="left" valign="middle">
				   
				<table height="18" cellspacing="0" cellpadding="0" width="200px" border="0" runat="server" id="tblDepotLocDetails">
                  <tr valign="top">
                    <td class="formTxt" align="left" width="162" height="14">Service Building collection details</td>                    
                  </tr>
                  <tr valign="top">
                    <td align="left" height="24"><asp:TextBox runat="server" ID="txtDepotDetails" CssClass="formField" Width = "200px" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" Height="59px" onchange='javascript:validateMaxLen(this.id,500,"Service Building collection details cannot exceed 500 characters")'></asp:TextBox></td>                    
                  </tr>
                 
                </table>
				    
				
				</td>
				</tr>
				 <tr>
				<td height="24" align="left" valign="middle"><asp:CheckBox id="chkbxBilling" AutoPostBack="true" runat="server" Text='Billing Centre' TabIndex="22"  CssClass="formTxt"></asp:CheckBox></td>
				</tr>
				</table>
				<asp:Panel ID="pnlFinance" runat="server">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
                 <tr>
				 <td class="formTxt"></td>
				 <td height="24" width="180" class="formTxt">Alternative billing Address</td>
				 <td><asp:DropDownList runat="server" ID="ddlAltBillingLoc" CssClass="formField width150" DataTextField="LocationName" DataValueField="AddressId"></asp:DropDownList></td>
				 <td></td>
				 </tr>
	             <tr style="height:24px;">
	             <td width="10" class="gridTextNew"></td>
	             <td class="gridTextNew"><strong>Field Label</strong></td>
	             <td class="gridTextNew"><strong>Field Answer</strong></td>
			     <td></td>
	             </tr>
	             <tr id="tr1" style="height:24px;">
	             <td width="10">
	             
	             <asp:CheckBox ID="chkField1" runat="server" AutoPostBack="true"/>  
	             </td>
			     <td><asp:TextBox ID="txtField1"  MaxLength="50" TabIndex="34" runat="server" CssClass="formField width150"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanfld1" runat="server">&nbsp;*</span><asp:RequiredFieldValidator  ID="reqFldLbl1" runat="server" ControlToValidate="txtField1"  Display="None" Enabled="false" ErrorMessage="Field label can not be blank">*</asp:RequiredFieldValidator></td>
			     <td><asp:TextBox ID="txtFieldValue1"  MaxLength="50" TabIndex="35" runat="server" CssClass="formField width150"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanfldVal1" runat="server">&nbsp;*</span>
				 <asp:RequiredFieldValidator ID="reqfld1"  runat="server" ControlToValidate="txtFieldValue1" Display="None" Enabled="false" ErrorMessage="Field answer can not be blank"></asp:RequiredFieldValidator> </td>
			     <td></td>
	             </tr>
	             <tr id="tr2" style="height:24px;">
	             <td width="10">
	             <asp:CheckBox id="chkField2" runat ="server" AutoPostBack="true"/>
	             
	             </td>
	             <td><asp:TextBox ID="txtField2"  MaxLength="50" TabIndex="37" runat="server" CssClass="formField width150"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanfld2" runat="server">&nbsp;*</span></td>
	             <td><asp:TextBox ID="txtFieldValue2"  MaxLength="50" TabIndex="38" runat="server" CssClass="formField width150"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanfldVal2" runat="server">&nbsp;*</span>
					     <asp:RequiredFieldValidator ID="reqfld2"  runat="server" ControlToValidate="txtFieldValue2" Display="None" Enabled="false" ErrorMessage="Field answer can not be blank"></asp:RequiredFieldValidator> </td>
	             <td><asp:RequiredFieldValidator  ID="reqFldLbl2" runat="server" ControlToValidate="txtField2" Display="None" Enabled="false" ErrorMessage="Field label can not be blank"></asp:RequiredFieldValidator></td>
	             </tr>
	             <tr id="tr3" style="height:24px;">
	             <td width="10">
	             <asp:CheckBox ID="chkField3" runat ="server" AutoPostBack="true"/>
	             
	             <td><asp:TextBox ID="txtField3"  MaxLength="50" TabIndex="40" runat="server" CssClass="formField width150"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanfld3" runat="server">&nbsp;*</span></td>
	             <td><asp:TextBox ID="txtFieldValue3"  MaxLength="50" TabIndex="41" runat="server" CssClass="formField width150"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanfldVal3" runat="server">&nbsp;*</span>
				  <asp:RequiredFieldValidator ID="reqfld3"  runat="server" ControlToValidate="txtFieldValue3" Display="None" Enabled="false" ErrorMessage="Field answer can not be blank"></asp:RequiredFieldValidator> </td>
	             <td><asp:RequiredFieldValidator  ID="reqFldLbl3" runat="server" ControlToValidate="txtField3" Display="None" Enabled="false" ErrorMessage="Field label can not be blank"></asp:RequiredFieldValidator></td>
	             </tr>
	             <tr id="tr4" style="height:24px;">
	             <td width="10">
	             <asp:CheckBox ID="chkField4" runat ="server" AutoPostBack="true"/>
	             </td>
	             <td><asp:TextBox ID="txtField4"  MaxLength="50" TabIndex="43" runat="server" CssClass="formField width150"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanfld4" runat="server">&nbsp;*</span></td>
	             <td><asp:TextBox ID="txtFieldValue4"  MaxLength="50" TabIndex="44" runat="server" CssClass="formField width150"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanfldVal4" runat="server">&nbsp;*</span>
				 <asp:RequiredFieldValidator ID="reqfld4"  runat="server" ControlToValidate="txtFieldValue4" Display="None" Enabled="false" ErrorMessage="Field answer can not be blank"></asp:RequiredFieldValidator>
				 </td>
	             <td><asp:RequiredFieldValidator  ID="reqFldLbl4" runat="server" ControlToValidate="txtField4" Display="None" Enabled="false" ErrorMessage="Field label can not be blank"></asp:RequiredFieldValidator> </td>
	             </tr>
	             </table>
				 </asp:Panel> 
				</div>
				
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
					  <td height="20" align="right">&nbsp;</td>
					  <td height="20" align="right">&nbsp;</td>
					  <td height="20" align="right">&nbsp;</td>
					   <TD width="10">&nbsp;</TD>
					  </tr>
					<tr>
					<td align="right"><TABLE cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
					  <TR>
						<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
						<TD width="110" class="txtBtnImage">
						<asp:LinkButton id="btnSaveBottom" runat="server"  CausesValidation="false" class="txtListing" tabindex="26">
						<strong><img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" >&nbsp;Save Changes&nbsp;</strong></asp:LinkButton>
						</a>
						
						</TD>
						<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
					  </TR>
					</TABLE>
					</td>
					<TD width="10">&nbsp;</TD>
					<td width="125" align="right"><TABLE cellSpacing="0"  width="125" cellPadding="0" bgColor="#F0F0F0" border="0">
					  <TR>
						<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
						<TD class="txtBtnImage">
						 <asp:LinkButton id="btnResetBottom" runat="server" TabIndex="27"  CausesValidation="false" class="txtListing">
							  <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton>
						</TD>
						 <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
					  </TR>
					</TABLE></td>
					<TD width="10"></TD>
					<td width="60" align="right"><TABLE width="60" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
					  <TR>
						<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
						<TD class="txtBtnImage">
							<asp:LinkButton id="btnCancelBottom" runat="server" TabIndex="28"  CausesValidation="false" class="txtListing">
								<img src="Images/Icons/Icon-Cancel.gif" align="absmiddle" width="11" height="10" hspace="5" vspace="0" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
								   
						</TD>
						<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
					  </TR>
					</TABLE></td>
					<TD width="20">&nbsp;</TD>
					</tr>
					<tr>
					  <td height="20" align="right">&nbsp;</td>
					  <td height="20" align="right">&nbsp;</td>
					  <td height="20" align="right">&nbsp;</td>
					   <TD width="10">&nbsp;</TD>
					  </tr>
					</table>
					<table id="tblDeleteMsg" visible=false runat=server cellpadding="0" cellspacing="0">
				<tr><td>&nbsp;</td></tr>
					<tr>
					 <td><asp:Label ID="lblDeleteMsg" CssClass="formTxt" Text="" runat="server"></asp:Label></td>
					</tr>
				</table>
				  </DIV>
			</div>
			
			
			
			
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
					<tr><td>&nbsp;</td></tr>            
                  <tr valign="top">
                    <td class="formTxt" align="left" height="14">&nbsp;</td>
                  </tr>
				   <tr valign="top">
                    <td class="formTxt" align="left" height="14">&nbsp;</td>
                  </tr>
            </table>
                
    </div>
                
  </asp:panel>


   <!-- Confirm Action panel --> 
			<asp:panel ID="pnlConfirm" runat="server" visible="false">
			
			  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="padTop50">
			    <tr>
			        <td colspan="4" align="center"><asp:Label ID="lblConfirmMsgTxt" runat="server"></asp:Label></td>
			    </tr>
                <tr>
                  <td width="350" align="right">
                    <div id="divConfirmbtn" runat="server">
                      <table cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                          <tr>
                            <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                            <td class="txtBtnImage"><asp:LinkButton id="btnConfirm" runat="server" CausesValidation="false" class="txtListing" TabIndex="29" > <img src="Images/Icons/Icon-Confirm.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Confirm </asp:LinkButton></td>
                            <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                          </tr>
                      </table>
                    </div>
                  </td>
                  <td width="10">&nbsp;</td>
                  <td align="left"><table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0" align="left">
                      <tr>
                 <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td align="center" class="txtBtnImage">
                            <asp:LinkButton id="btnCancel" runat="server" CausesValidation="false" class="txtListing" TabIndex="30" > <img src="Images/Icons/Icon-Cancel.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
                          </td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                  <td width="300">&nbsp;</td>
                </tr>
              </table>
           
            </asp:panel>
</ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID="btnFind" EventName="Click" />

<asp:AsyncPostBackTrigger ControlID="btnResetTop" EventName="Click" />

<asp:AsyncPostBackTrigger ControlID="btnResetBottom" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnConfirm" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />


</Triggers>
</asp:UpdatePanel>


 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePnlLocationForm" runat="server">
        <ProgressTemplate>
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Please Wait..processing data
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="pnlSubmitForm" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />



        
       
