<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSSalesInvoiceWOListingUK.ascx.vb" Inherits="Admin.UCMSSalesInvoiceWOListingUK" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
    <table  width="100%"  border="0" cellpadding="10" cellspacing="0" >
                      <tr>
					     <td >
							<TABLE cellSpacing="0" cellPadding="0" width="115" border="0">
								  <TR>
									<TD >
										<div id="divButton" class="divButton" style="width: 115px;">
											<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
												<a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" alt="Back to Listing" width="4" height="8" hspace="0" vspace="0" border="0" class="marginR8"><strong>Back to Listing</strong></a>
											<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
										</div>
									</TD>
								  </TR>
                        	</TABLE>

						</td>
                        <td width="176px">&nbsp;</td>
			 			<td align="left" >
                            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
							    <a runat="server" target="_blank" id="btnExport" class="txtButtonRed">&nbsp;Export to Excel&nbsp;</a>
                            </div>
						</td>
					</tr>
         </table>
<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10px"></td>			
			<td class="HeadingRed"><asp:Label ID="lblHeading" runat=server></asp:Label></td>
			</tr>
			</table>			
		    <table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td width="10px"></td>
              </tr>
            </table>
		    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline> <ContentTemplate>
<table><tr>
             <td class="paddingL30 padB21" valign="top" padB21 paddingTop1>
		<asp:Label ID="lblMsg" runat="server"  CssClass="bodytxtValidationMsg"></asp:Label>		  	
		</td></tr>
             </table>
	   <div id="div1" class="divredBorder">
              <div class="roundtabRed"><img src="../Images/Curves/TabRed-LC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
            </div>
           	<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td>
              <asp:GridView ID="gvWorkOrders"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>' BorderColor="White"  
               PagerSettings-Mode=NextPreviousFirstLast  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               
               <PagerTemplate>
                   
            <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>																				
							<td width="10">&nbsp;</td>												  
						  					
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right" runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
												  </div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
        </PagerTemplate> 
               <Columns>  
                           
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="RefWOID" SortExpression="RefWOID" HeaderText="WorkOrderID" />                
                <asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="180px" HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="CloseDate" SortExpression="CloseDate" HeaderText="Closed Date" />                    
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="WOTitle" SortExpression="WOTitle" HeaderText="Description" /> 
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="SupplierName" SortExpression="SupplierName" HeaderText="Supplier" /> 
                
                
                
                <asp:TemplateField SortExpression="WholeSalePrice" HeaderText="Whole Sale Amount" >               
               <HeaderStyle CssClass="gridHdr" Width="200px"  Wrap=true />
               <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
               <ItemTemplate>   
                <%#FormatCurrency(Container.DataItem("WholeSalePrice"), 2, TriState.True, TriState.True, TriState.False)%>                       
               </ItemTemplate>
               </asp:TemplateField>
                                
                <asp:TemplateField >               
               <HeaderStyle CssClass="gridHdr"   Wrap=true /> 
              <ItemStyle Wrap=true HorizontalAlign=Left   CssClass="gridRow"/>
               <ItemTemplate>   
                      <div id="divWODetails" runat="server" Visible='<%#IIF(Container.DataItem("WOID") = 0 , False,True)%>'>
                        <%#GetLinks(Container.DataItem("WOID"), Container.DataItem("CompanyId"))%>
                      </div>                         
               </ItemTemplate>
               </asp:TemplateField>  
				</Columns>
               
                 <AlternatingRowStyle    CssClass=gridRow />
                 <RowStyle CssClass=gridRow />            
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
        
              </asp:GridView>
				 </td>
            </tr>
            </table>
				
				 <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.UCMSSalesInvoiceWOListingUK" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
          </asp:Panel>
          </ContentTemplate>
      
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
              </asp:UpdateProgress>
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
         
		
				     