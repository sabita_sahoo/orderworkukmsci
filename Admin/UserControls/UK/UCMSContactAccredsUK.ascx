<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSContactAccreds.ascx.vb" Inherits="Admin.UCMSContactAccreds" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
  <%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>
<script language=javascript type="text/javascript" src="~/JS/Scripts.js"></script>
<style>
.gridTextNew {
color:#000000;
font-family:Geneva,Arial,Helvetica,sans-serif;
font-size:11px;
padding:0 0 0 3px;
text-decoration:none;
}
</style>
<input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />

<asp:UpdatePanel ID="UpdatePanel2" runat="server"  >
 <ContentTemplate>
<table><tr>
    <td  ><asp:Label ID=lblMessage  CssClass="bodytxtValidationMsg" runat=server></asp:Label></td>
  </tr></table>
<asp:Panel ID="pnlAccredListBoxes" runat=server >
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="formTxt" width="240px">Select a Vendor </td>
        <td class="formTxt" id="tdCerts" visible="false" runat="server" width="220">List of certifications</td>
        <td id="tdCertID" class="formTxt" runat="server" visible="false">Certification ID </td>
      </tr>
      <tr>
        <td>
          <asp:ListBox height=150  ID="lstBoxVendor"  AutoPostBack=true  class="formFieldWhite width202height110" DataTextField="VendorName" DataValueField="VendorID" runat="server"></asp:ListBox>
        </td>
        <td>
          <asp:ListBox height=150 ID="lstBoxCerts" Visible="false" AutoPostBack=true class="formFieldWhite width202height110" SelectionMode="multiple" DataTextField="CertName" DataValueField="CertID" runat="server"></asp:ListBox>
       </td>
        <td align="left" valign="top" id="tdCertAccessSubmit" visible="false" runat="server" >
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td valign="top"><asp:TextBox ID="txtCertificateCode" runat="server" CssClass="formField width150"></asp:TextBox></td>
			  </tr>
			  <tr>
				<td valign="top" class="formTxt">Access Code </td>
			  </tr>
			  <tr>
				<td valign="top"><asp:TextBox ID="txtAccessCode" runat="server" CssClass="formField width150"></asp:TextBox></td>
			  </tr>
			  <tr>
			  	<td height="10">&nbsp;</td>
			  </tr>
			  
			  <tr>
				<td align="right">
					<TABLE cellSpacing="0" cellPadding="0" width="50" bgColor="#a0a0a0" border="0">
						<TR>
							<TD height="18"><IMG runat=server height="18" src="~/Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
							<TD><asp:linkbutton   class="txtButtonNoArrow" id="lnkSelect" CausesValidation=false runat="server">&nbsp;Submit</asp:linkbutton></TD>
							<TD width="5"><IMG runat=server height="18" src="~/Images/Curves/Grey-BtnRight.gif" width="5"></TD>
						</TR>
				  </TABLE></td>
			  </tr>
			</table>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td valign=top height=20px class="formTxt"><em>Note: Please press Ctrl+ for multiple selection for Certificates </em></td>
    
  </tr>
   <tr id="trFileUpload" visible="false" runat="server">
			  	<td height="10">
			  	    <asp:UpdatePanel runat="server" id="updatepanel7">
			  	    <ContentTemplate>
			  	    <uc2:UCFileUpload id="UCFileUpload7" runat="server" Control="UCSpecialistsForm1_UCAccreds1_UCFileUpload7" 
		                Type="UserSkillSet" 
		                AttachmentForSource="UserSkillSet" ShowExistAttach = "False"
		                ShowNewAttach="True" ShowUpload="True" 
		                MaxFiles="1"></uc2:UCFileUpload> 
		                </ContentTemplate>
		            </asp:UpdatePanel>
			  	</td>
			  </tr>
</table>
</asp:Panel>
<asp:Panel ID="pnlSelectedAccreds" runat=server Visible=false>

<table width="650"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="25" class="formTxt">List of Submitted Vendor Accreditations </td>
  </tr>
  <tr>
    <td><table width="100%" class="tblBorder" bgcolor="#A0A0A0" BorderWidth="1px"  BorderColor="#EDEDED" cellspacing="0" cellpadding="2">
      <tr >
        <td width="25" align="center" class="formtxt"><input id='chkAll' onClick=CheckAll(this,'CheckAccreds') type='checkbox' name='chkAll' /></td>
        <td class="txtButtonNoArrow ">Vendor</td>
        <td width="120" class="txtButtonNoArrow">Certification</td>
        <td width="80" class="txtButtonNoArrow">Certification ID </td>
        <td width="80" class="txtButtonNoArrow">Access Code </td>
        <td width="80" class="txtButtonNoArrow">Attachments</td>        
      </tr>
    </table>
      <div id="divAccreds" runat=server class="treeControl">
		<asp:GridView ID="gvSelectedAccreds" CellPadding="2" CellSpacing="0" runat=server AutoGenerateColumns="False" BorderWidth="1px"  BorderColor="#EDEDED" Width="650">
	   <Columns>  
		   <asp:TemplateField ItemStyle-Width="25px" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="gridRowAOE" >
				<ItemTemplate >
					 <asp:CheckBox ID="CheckAccreds" Runat="server" ></asp:CheckBox> <input type="hidden" Runat="server" id="hdnCertId" value=<%# databinder.eval(container.dataitem, "CertID") %>/>
				</ItemTemplate>
			   <ItemStyle   />
			</asp:TemplateField>
			<asp:BoundField   ItemStyle-CssClass="gridTextNew" ItemStyle-HorizontalAlign=left  DataField="VendorName"   />            
			<asp:BoundField   ItemStyle-CssClass="gridTextNew" ItemStyle-HorizontalAlign=left  DataField="CertName"   ItemStyle-Width="120px"   />            
			<asp:BoundField   ItemStyle-CssClass="gridTextNew" ItemStyle-HorizontalAlign=left  DataField="CertCode"   ItemStyle-Width="80px"   />            
			<asp:BoundField   ItemStyle-CssClass="gridTextNew" ItemStyle-HorizontalAlign=left  DataField="AccessCode" ItemStyle-Width="80px"  />            
			<asp:TemplateField ItemStyle-Width="80px" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="gridRowAOE" >
			    <ItemTemplate>
			    <asp:Literal ID="litAttachments" Text='<%#getAttachmentLinkage(Eval("FilePath"),Eval("FileName"))%>' runat="server" />
			    </ItemTemplate>
			</asp:TemplateField>
		</Columns>
		 <AlternatingRowStyle   BackColor="#E7E7E7" />
		 <RowStyle CssClass="gridRow" />                            
	   </asp:GridView>
		</div></td>
  </tr>
  
   <tr>
    <td height="40" valign="middle" class="formTxt"> 
	 <TABLE cellSpacing="0" cellPadding="0" width="100" bgColor="#a0a0a0" border="0">
		<TR>
			<TD height="18"><IMG height="18" runat=server src="~/Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
			<TD><asp:linkbutton   class="txtButtonNoArrow" CausesValidation=false id="lnkRemove" runat="server">Delete Selected</asp:linkbutton></TD>
			<TD width="5"><IMG height="18" runat=server src="~/Images/Curves/Grey-BtnRight.gif" width="5"></TD>
		</TR>
	</TABLE></td>
  </tr>
</table>

</asp:Panel>

</ContentTemplate>   
</asp:UpdatePanel> 