<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSWOCloseComplete.ascx.vb" Inherits="Admin.UCMSWOCloseComplete" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<style>
.moveLeft{
margin-left:-4px;
}
</style>
<!--

dont go

-->


<div id="divValidationMain" visible="false" class="divValidation" runat=server >
  <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td height="15" align="center">&nbsp;</td>
	  <td height="15">&nbsp;</td>
	  <td height="15">&nbsp;</td>
	</tr>
	<tr valign="middle">
	  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
	  <td class="validationText"><div  id="divValidationMsg"></div>
		  <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
          <asp:Panel runat="server" ID="pnlChangedData" Visible="false" style="width:350px;background-color:#D7D7D7;margin: 8px;padding : 8px;margin-bottom:0px;" >
                <asp:Repeater  ID="repChangedData" Runat="server" >
           <ItemTemplate> 

			<span style="line-height:16px;" runat="server" id="span1"><b><%# Eval("FullName")%> - <%#Eval("DateModified")%></b><br /></span>
			<span id="Span2" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldWOStatus") <> DataBinder.Eval(Container.DataItem, "NewWOStatus"), True, False)%>' >WO Status Changed:  <%# DataBinder.Eval(Container.DataItem, "NewWOStatus")%><br /></span>
            <span id="Span3" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldValue") <> DataBinder.Eval(Container.DataItem, "NewValue"), True, False)%>' >Price Changed:  <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br /></span>
			<span id="Span4" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldAptTime") & "" <> DataBinder.Eval(Container.DataItem, "NewAptTime"), True, False)%>' >Appointment Time Changed:  <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br /></span> 
			<span id="Span5" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldDateStart") <> DataBinder.Eval(Container.DataItem, "NewDateStart"), True, False)%>' ><asp:label id="lblStartdate" runat="server" Text='<%#IIf(DataBinder.Eval(Container.DataItem, "BusinessArea") <> 101,"Start Date Changed:","Appointment Date Changed:")%>'></asp:label> <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br /></span>
			<span id="Span6" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldDateEnd") <> DataBinder.Eval(Container.DataItem, "NewDateEnd"), True, False)%>' >End Date Changed:  <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br /></span>
			<span id="Span7" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldPlatformPrice") <> DataBinder.Eval(Container.DataItem, "NewPlatformPrice"), True, False)%>' >Platform Price Changed:  <%# DataBinder.Eval(Container.DataItem, "NewPlatformPrice")%><br /></span>
            <span id="Span8" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldWholesalePrice") <> DataBinder.Eval(Container.DataItem, "NewWholesalePrice"), True, False)%>' >Wholesale Price Changed:  <%# DataBinder.Eval(Container.DataItem, "NewWholesalePrice")%><br /></span>
            <span id="Span9" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldPONumber") <> DataBinder.Eval(Container.DataItem, "NewPONumber"), True, False)%>' >PO Number Changed:  <%# DataBinder.Eval(Container.DataItem, "NewPONumber")%><br /></span>
            <span id="Span10" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldJobNumber") <> DataBinder.Eval(Container.DataItem, "NewJobNumber"), True, False)%>' >Job Number Changed:  <%# DataBinder.Eval(Container.DataItem, "NewJobNumber")%><br /></span>
            <span id="Span11" runat="server" visible='<%#IIf(DataBinder.Eval(Container.DataItem, "OldGoodsLocation") <> DataBinder.Eval(Container.DataItem, "NewGoodsLocation"), True, False)%>' >Goods Location Changed:  <%# DataBinder.Eval(Container.DataItem, "NewGoodsLocation")%><br /></span>
                        
            </ItemTemplate>
          </asp:Repeater>
          </asp:Panel>
		  <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
	  </td>
	  <td width="20">&nbsp;</td>
	</tr>
	<tr>
	  <td height="15" align="center">&nbsp;</td>
	  <td height="15">&nbsp;</td>
	  <td height="15">&nbsp;</td>
	</tr>
  </table>
  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
</div>

<asp:panel ID="pnlWOProcess" runat="server">
<table id="DGWorkOrders" width="100%" border="0" cellpadding="5" cellspacing="0">
  <tr valign="top" >
    <td  class="gridHdr gridBorderRB">WO No</td>
    <td  class="gridHdr gridBorderRB"><asp:label ID="lblColNameDateCreated" runat="server"></asp:label></td>
    <td  class="gridHdr gridBorderRB">Location</td>
	<td  class="gridHdr gridBorderRB">Contact</td>
	<td  class="gridHdr gridBorderRB">WO Title</td>
	<td  class="gridHdr gridBorderRB">WO Start</td>
	<td  class="gridHdr gridBorderRB">Price</td>
	<td  class="gridHdrHighlight gridBorderB">Status</td>
  </tr>
 <tr>
    <td width="100px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label></td>
	<td width="75px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblCreated"></asp:Label></td>
	<td width="65px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblLocation"></asp:Label></td>
	<td width="80px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblContact"></asp:Label></td>
	<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblTitle"></asp:Label></td>
	<td width="75px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOStart"></asp:Label></td>
	<td width="90px" align="right" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="gridRow" ID="lblPrice"></asp:Label>&nbsp;</td>
	<td width="57px" class="gridRowHighlight gridBorderB"><asp:Label runat="server" CssClass="gridRowHighlight" ID="lblStatus"></asp:Label></td>
  </tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
	  <td height="10" align="left" valign="middle">&nbsp;</td>
	  <td class="smallText"></td>
</tr>
<tr>
	 <td height="25" align="left" valign="middle"><asp:Label runat="server" ID="lblMsg1" CssClass="txtWelcome" Text=""></asp:Label></td>
	 <td width="20">&nbsp;</td>
</tr>
</table>

<div class="tabDivOuter" id="divMainTab">
 <div class="roundtopLightGreyCurve"><img src="Images/Curves/LightGrey-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
 <DIV class="paddedBox" id="divAccount" runat="server">
   <!-- comments for wo -->
   <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
	 <TR>
	   <TD height="50" align="left" vAlign="top">
		   <table width="100%"  border="0" cellpadding="0" cellspacing="0">
			 <tr>
			   <td height="15" valign="bottom" class="formLabelGrey" >Please add any comments you have regarding <asp:Label ID="lblAction" runat="server"></asp:Label> this Work Order below:
			     <asp:RegularExpressionValidator id="RegExCompleteComments" runat="server" ValidationExpression="(.|[\r\n]){1,500}"  ErrorMessage="Please enter comments less than 500 characters" ForeColor="#EDEDEB"
		          ControlToValidate="txtCompleteComments">*</asp:RegularExpressionValidator>
			   </td>
			 </tr>
			 <tr>
			   <td><asp:TextBox id="txtCompleteComments"  runat="server" CssClass="formFieldGrey width470height58" style="width:100%;"  TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" Rows="4"></asp:TextBox></td>
			 </tr>
			 <tr>
			   <td>&nbsp;</td>
			 </tr>
		   </table>
	   </TD>
	 </TR>
  </TABLE>
  <!-- comments for wo end-->
  
	<table visible="true" width="100%"  border="0" cellspacing="0" cellpadding="0">
	 <tr>
	   <td style="height:20px;line-height:18px;" valign="bottom" class="formLabelGrey" ><asp:Label id="lblRatingMsg" runat="server" Visible="true" CssClass="formLabelGrey lineheight18"></asp:Label></td>
	 </tr>
	 <tr>
	   <td height="20" valign="bottom" class="formLabelGrey" >Select a Rating<asp:CustomValidator id="rqRating" runat="server" ForeColor="#EDEDEB"
												ErrorMessage="Please select a rating">*</asp:CustomValidator></td>
	 </tr>
	 <tr>
	   <td style="height:30px;" valign="top"><asp:RadioButton id="rdoRatingPositive" style="vertical-align:baseline; " runat="server" CssClass="formLabelGrey" Text="Positive" TextAlign="right" GroupName="rdoRating"></asp:RadioButton>
			&nbsp;&nbsp;
			<asp:RadioButton id="rdoRatingNeutral" runat="server" CssClass="formLabelGrey" Text="Neutral" TextAlign="right" GroupName="rdoRating"></asp:RadioButton>
			&nbsp;&nbsp;
			<asp:RadioButton id="rdoRatingNegative" runat="server" CssClass="formLabelGrey" Text="Negative"	TextAlign="right" GroupName="rdoRating"></asp:RadioButton>
	   </td>
	 </tr>
     <tr id="trSLAMetText" runat="server" visible="false">
	   <td height="20" valign="bottom" class="formLabelGrey" ><b>Service Level Agreement Met</b><asp:CustomValidator id="rqSLA" runat="server" ForeColor="#EDEDEB" Enabled="false"
												ErrorMessage="Please select Service Level Agreement Met">*</asp:CustomValidator></td>
	 </tr>
      <tr id="trSLAMet" runat="server" visible="false">
	   <td style="height:30px;" valign="top">
       <asp:RadioButton id="rdoSLAMetYes" style="vertical-align:baseline; " runat="server" CssClass="formLabelGrey" Text="Yes" TextAlign="right" GroupName="rdoSLA"></asp:RadioButton>
	   &nbsp;&nbsp;
	   <asp:RadioButton id="rdoSLAMetNo" runat="server" CssClass="formLabelGrey" Text="No" TextAlign="right" GroupName="rdoSLA"></asp:RadioButton>
       </td>
	 </tr>
      <tr>
	   <td height="20" valign="bottom" class="formLabelGrey" >&nbsp;</td>
	 </tr>
	 <tr id="trClientQ" runat="server" visible="false" class="formLabelGrey">
	 <td>
	 <b style="font-size:11px;">To complete the workorder please answer the following questions</b><br />
	 <asp:Repeater ID="rpBoolQ" runat="server" Visible="false">
	 <ItemTemplate>
	 <table cellpadding="0" cellspacing="0"><tr><td style="width:300px;">
	 <asp:Label ID="lblQ" runat="server" Visible='<%#Container.DataItem("IsEnabled")%>' Text='<%#Container.DataItem("Question")%>' CssClass="formLabelGrey lineheight18"></asp:Label></td>
	 <td><asp:RadioButton ID="rdbtnYes" Visible='<%#Container.DataItem("IsEnabled")%>'  runat="server" Text="Yes" GroupName="ClientQuestion" CssClass="formLabelGrey lineheight18"/>&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rdbtnNo" runat="server" Visible='<%#Container.DataItem("IsEnabled")%>' Text="No" GroupName="ClientQuestion" Checked="true" CssClass="formLabelGrey lineheight18"/> <span id="spanreqfldTime" runat="server" style="color:#FF0000;font-size:11px;" visible='<%#Container.DataItem("IsMandatory")%>'> *</span></td>
	 </tr></table> 
	 </ItemTemplate>
	 </asp:Repeater>  
	 <asp:Repeater ID="rpTimeQ" runat="server" Visible="false">
	 <ItemTemplate>
	    <table cellpadding="0" cellspacing="0"><tr>
	   <td style="width:300px;height:20px;" ><asp:Label ID="lblQ" runat="server" Visible='<%#Container.DataItem("IsEnabled")%>' Text='<%#Container.DataItem("Question")%>' CssClass="formLabelGrey lineheight18"></asp:Label></td>
	   <td style="padding-left:6px;"><asp:TextBox id="txtTime" runat="server" ToolTip="hh:mm" CssClass="ddlTime"></asp:TextBox>  
<asp:RegularExpressionValidator ID="regexTime" runat="server" Display="None" ControlToValidate="txtTime" ValidationExpression="^(([0-1][0-9]|2[0-3]):([0-5][0-9]))$" ErrorMessage="Enter time in 24 hour format" Enabled="true"></asp:RegularExpressionValidator>     
<cc1:textboxwatermarkextender id="TBWE2" runat="server"
    TargetControlID="txtTime"
    WatermarkText="hh:mm"
    WatermarkCssClass="watermarked" />
	   <span id="spanreqfldTime" runat="server" style="color:#FF0000;font-size:11px;" visible='<%#Container.DataItem("IsMandatory")%>'> *</span>
	   <asp:RequiredFieldValidator ID="reqFldTimeHH" runat="server" ControlToValidate="txtTime" Enabled='<%#Container.DataItem("IsMandatory")%>' Display="None" ErrorMessage="Please enter time"></asp:RequiredFieldValidator> 
	    </td>
	   </tr></table>
	 </ItemTemplate>
	 </asp:Repeater>
	 </td>
	 </tr>
	 <tr style="display:none">
	   <td height="20" valign="bottom" class="formLabelGrey" ><asp:Label id="lblComments" runat="server" Visible="true" CssClass="formLabelGrey"></asp:Label>
	    <asp:RegularExpressionValidator id="RegExComments" runat="server" ValidationExpression="(.|[\r\n]){1,5}"  ErrorMessage="Please enter comments less than 500 characters" ForeColor="#EDEDEB"
		ControlToValidate="txtComments">*</asp:RegularExpressionValidator>
	   </td>
	 </tr>
	 <tr style="display:none">
	   <td><asp:TextBox id="txtComments"  runat="server" CssClass="formFieldGrey width470height58" style="width:100%;" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" Rows="4"></asp:TextBox></td>
	 </tr>
	</table> 

	<asp:Panel runat="server" ID="pnlAttachments" Visible="false">
	<br />
	<asp:CustomValidator id="cstmFileUpload" Enabled="True" runat="server" ForeColor="#EDEDEB" ErrorMessage="Please upload sign off sheet before completion">*</asp:CustomValidator>
	 <div id="Div1" class="divAttachment" runat=server  >
	<div class="AttachtopVal"><img src="Images/Curves/Attach-TL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
	
	
		<%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>
		<uc2:UCFileUpload id="UCFileUpload1" runat="server" Control="UCWOCloseComplete1_UCFileUpload1" 
		Type="WorkOrder" AttachmentForSource="WOComplete"  UploadCount="1" 
	   ShowExistAttach="False" ShowNewAttach="True" ShowUpload="True" MaxFiles="20"></uc2:UCFileUpload>                             
	   
	   <div class="AttachbottomVal"><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
    </div>
	</asp:Panel>
</DIV>
</div>
				 
<table  align="right" border="0" cellspacing="0" cellpadding="0">
	<tr>
		 <td height="15">&nbsp;</td>
		 <td>&nbsp;</td>
         <td width="20">&nbsp;</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			  <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
				<TR>
				  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
				  <TD class="txtBtnImage">
				  <asp:LinkButton ID="lnkConfirm" CausesValidation="false" runat="server" CssClass="txtListing"></asp:LinkButton></TD>
				  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
				</TR>
	      </TABLE>
		</td>
		<td width="350" align="right" id="tdConfirmComplete" runat="server"><table class="marginL20" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td class="txtBtnImage"><asp:LinkButton id="btnBackToCompList" runat="server" CausesValidation="false" class="txtListing" TabIndex="65" > <img src="Images/Icons/Icon-Back.gif" width="14" height="16" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Close and Back to Completed WO listing&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
	    <td align="left">
				 <TABLE class="marginL20" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
					<TR>
					  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
					  <TD class="txtBtnImage">
					  <%-- <a id="lnkCancel" runat="server" class="txtListing">--%>
					   <img src='Images/Icons/Cancel.gif' alt='Cancel' width='11' height='11' align='absmiddle' hspace='1' vspace='2' border='0'>
					   <asp:LinkButton ID="lnkCancel" CausesValidation="false" runat="server" Text="Cancel" CssClass="txtListing" ></asp:LinkButton>
					    <%--Cancel</a>--%></TD>
					  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
					</TR>
	      </TABLE>
		</td>
        <td width="20" align="left">&nbsp;</td>
  </tr>
 </table>
</asp:Panel>	

  <!-- Confirm Action panel --> 
			<asp:panel ID="pnlBackToListing" runat="server" visible="false" >
			
			  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="padTop50">
                <tr>
                  
                  <td width="10">&nbsp;</td>
                  <td align="left" ><table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0" >
                      <tr>
                 <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td align="center" class="txtBtnImage">
                            <asp:LinkButton id="btnBackToCloseList" runat="server" CausesValidation="false" class="txtListing" TabIndex="66" > <img src="Images/Icons/Icon-Back.gif" width="14" height="16" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Back to Closed WO listing&nbsp;</asp:LinkButton>
                          </td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
            </asp:panel>								 
 