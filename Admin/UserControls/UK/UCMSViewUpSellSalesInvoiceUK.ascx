<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSViewUpSellSalesInvoiceUK.ascx.vb" Inherits="Admin.UCMSViewUpSellSalesInvoiceUK" %>
<style type="text/css">
<!--
.invoiceLabel {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 36px;
	color: #B7B7B7;
	font-weight: bold;
}
.InvoiceText {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.bdr1 {
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 4px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.bdr2 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #C6C6C6;
	border-right-color: #C6C6C6;
	border-bottom-color: #C6C6C6;
	border-left-color: #C6C6C6;
}
.bdr3 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.style3 {font-size: 11px}
.style5 {font-weight: bold}
.style6 {font-weight: bold}
td
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 16px;
	color: #000000;
}
body {
	background-color: #CECECE;
}
-->
</style>
<table height="95%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" >
  <tr>
    <td width="45" height="140">&nbsp;</td>
    <td width="650" height="140"><table width="100%" cellpadding="0" cellspacing="0" style=" border-width:medium; border-bottom-width:5px;border-bottom-color:#000000;">
        <tr>
          <td width="311">&nbsp;</td>
         
          <td width="131" align="left" valign="bottom" >&nbsp;</td>
        </tr>
        <tr>
          <td width="311" valign="middle"><img runat="server" id="imgLogo" alt="OrderWork - IT Services Reinvented" width="181" height="64" border="0" class="LogoImg" /></td>
          
          <td align="right" valign="bottom" ><table width="99%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right" valign="bottom"  style="height: 22px;"><span class="style1 style5">
                  <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                </span></td>
              </tr>
              <tr>
                <td align="right" valign="bottom"  ><asp:Label ID="lblAddress" runat="server"></asp:Label></td>
              </tr>
             <tr>
				<td align="right" valign="bottom" style="height: 15px"  ><asp:Label ID="lblCity" runat="server"></asp:Label><asp:Label ID="lblPostCode" runat="server"></asp:Label>
				</td>
              </tr>
              <tr>
                <td align="right" valign="bottom"  > Tel:&nbsp;
                    <asp:Label ID="lblTelNo" runat="server"></asp:Label></td>
              </tr>
              <tr>
                <td align="right" valign="bottom" > <asp:Label ID="lblShowFax" runat="server" Text =" Fax:"></asp:Label>&nbsp;
                    <asp:Label ID="lblFaxNo" runat="server"></asp:Label></td>
              </tr>
              <tr>
                <td valign="bottom" ></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td width="311" class="bdr1" >&nbsp;</td>
          <td align="left" valign="bottom" class="bdr1" >&nbsp;</td>
        </tr>
    </table></td>
    <td width="45" height="140">&nbsp;</td>
  </tr>
  <tr>
    <td width="45" height="100" align="center">&nbsp;</td>
    <td width="650" height="100" align="center"><p class="invoiceLabel"><br />Invoice</p>
        <table border="0" cellpadding="5" cellspacing="0" width="100%" style="">
          <tr>
            <td align="left" valign="top"  style="border-bottom: 0px; border-right-width: 0px"><strong>Invoice To:<br>
              </strong>
                <asp:Label ID="lblCompNameInvoiceTo" runat="server"></asp:Label>
                <br />
                <asp:Label ID="lblInvoiceTo" runat="server"></asp:Label>
&nbsp;</td><td valign="bottom"><img alt="PAID" title="PAID" id="imgPaid" runat="server"/></td>
            <td align="right" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="95" align="left"><strong>Invoice No.&nbsp;</strong></td>
                  <td align="left">:</td>
                  <td align="left"><strong>
                    <asp:Label ID="lblInvoiceNo" runat="server"></asp:Label>
                  </strong> </td>
                </tr>
                <tr>
                  <td width="95" align="left"><strong>Invoice Date&nbsp;</strong></td>
                  <td align="left">:</td>
                  <td align="left"><strong><asp:Label ID="lblInvoiceDate" runat="server"></asp:Label></strong></td>
                </tr>
                <tr>
                  <td width="95" align="left"><strong>VAT No.</strong></td>
                  <td align="left">:</td>
                  <td align="left"><strong>
                    <asp:Label ID="lblVATNo" runat="server" ></asp:Label>
                  </strong></td>
                </tr>
            </table></td>
          </tr>
        </table>
    <p>&nbsp;</p></td>
    <td width="45" height="100" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td width="45" align="right" valign="top">&nbsp;</td>
    <td width="650" align="right" valign="top"><table width="650" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td height="30" align="left"  style="border-right-width: 0px;"><strong>Description</strong><strong></strong></td>
		<td height="30" width="250" align="right" ><strong>Our Ref</strong></td>
		<td height="30" width="250" align="right" ><strong>Amount</strong></td>
      </tr>
      <asp:Repeater ID="rptList" runat="server" Visible="true">
        <ItemTemplate>
          <tr>
            <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"><%#Container.DataItem("Description")%> </td>
	        <td height="30" align="right" style="border-top-width: 0px;"><%#Container.DataItem("WorkOrderId")%></td>
		    <td align="right"  style="border-top-width: 0px;"><%#FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%></td>
          </tr>
        </ItemTemplate>
      </asp:Repeater>
    </table>
	<asp:Panel Visible="false" runat="server" ID="pnlListingFee">
	<table width="650" border="0" cellpadding="0" cellspacing="0">
	  <tr>
		<td width="70" valign="top" align="left" ><asp:Label runat="server" ID="lblListingDate"></asp:Label></td>
		<td align="left"  style="border-right-width: 0px;"><asp:Label runat="server" ID="lblListingDesc"></asp:Label></td>
		<td width="50"  align="center"  style="border-right-width: 0px">&nbsp;</td>
		<td width="76"  align="right"  ><asp:Label runat="server" ID="lblListingNetPrice"></asp:Label></td>
		<td width="108" align="right" style="width:border-right-width: 0px"><asp:Label runat="server" ID="lblListingVAT"></asp:Label></td>
		<td width="60" align="right" ><asp:Label runat="server" ID="lblListingTotalPrice"></asp:Label></td>
	  </tr>
	</table>
	</asp:Panel>
	<br>
	<table border="0" cellspacing="0">
        <tr>
          <td width="140" align="right"  style=" border-bottom:0px; border-right-width:0px; "><strong>Sub-Total</strong></td>
          <td width="80" align="right"  style=" border-bottom:0px; "><asp:Label ID="lblSubTotal" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td width="140" height="20" align="right"   style=" border-bottom:0px; border-right-width:0px; "><strong>Total VAT (
                <asp:Label ID="lblTotalVatAsPerSite" runat="server">&nbsp;</asp:Label>
          %) </strong></td>
          <td width="80" height="20" align="right"  style=" border-bottom:0px; "><asp:Label ID="lblTotalVAT" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td width="140" height="50" align="right"   style="border-right-width:0px; "><strong>Total Amount </strong></td>
          <td width="80" height="40"  align="right" ><span class="style6">
            <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
          </span>          </td>
        </tr>
    </table>
    </td>
    <td width="45" align="right" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td width="45" height="60" align="center">&nbsp;</td>
    <td width="650" height="60" align="center" class="bdr3 style3"><strong>Registered Office: </strong>Montpelier Chambers, 61-63 High Street South, Dunstable, LU6 3SF<br>
        <strong>
        <asp:Label ID="lblRegistartionNo" runat="server"></asp:Label>
      :</strong>&nbsp;
      <asp:Label ID="lblRegistartionNoLine2" runat="server" Text=""></asp:Label>
    </td>
    <td width="45" height="60" align="center">&nbsp;</td>
  </tr>
</table>
