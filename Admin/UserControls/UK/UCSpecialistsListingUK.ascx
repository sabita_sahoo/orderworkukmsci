<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSpecialistsListing.ascx.vb" Inherits="Admin.UCSpecialistsListing" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<input type="hidden" id="hdnbizid" runat="server" />
<input type="hidden" id="hdncompanyid" runat="server" />
<asp:UpdateProgress  ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                                        <ProgressTemplate>
                                            <div>
                                                <img  align=middle src="Images/indicator.gif" />
                                                Please wait, Processing...
                                            </div>      
                                        </ProgressTemplate>
                                </asp:UpdateProgress>
                                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress2" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
<asp:UpdatePanel ID="UpdatePanel1" runat="server">

<ContentTemplate>

<table cellspacing="0" cellpadding="0" width="100%" border="0" style="padding: 0px 0px 10px 0px;
            margin: 0px;">
            <tr>
                <td width="165" class="txtWelcome paddingL10">
                    <div id="divButton" style="width: 165px;">
                        <div class="bottonTopGrey">
                            <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                height="4" class="btnCorner" style="display: none" /></div>                                
                                <a class="buttonText" runat="server" id="lnkBackToListing">
                                        <img src="Images/Arrows/Arrow-Back.gif" title="Back to Company Profile" width="4"
                                            height="8" hspace="0" vspace="0" border="0" class="paddingL10R8" /><strong>Back to Company
                                    Profile</strong></a>
                        <div class="bottonBottomGrey">
                            <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                height="4" class="btnCorner" style="display: none" /></div>
                    </div>
                </td>
            </tr>
        </table>

 <div id="divValidationMain" class="divValidation" runat="server" style="display:none;" >
	                <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
			                <tr>
				                <td height="15" align="center">&nbsp;</td>
				                <td height="15">&nbsp;</td>
				                <td height="15">&nbsp;</td>
			                </tr>
			                <tr valign="middle">
				                <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				                <td class="validationText"><div  id="divValidationMsg"></div>
				                <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
				                <asp:ValidationSummary id="validationSummarySubmit"  runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="true"></asp:ValidationSummary>
				                </td>
				                <td width="20">&nbsp;</td>
			                </tr>
			                <tr>
				                <td height="15" align="center">&nbsp;</td>
				                <td height="15">&nbsp;</td>
				                <td height="15">&nbsp;</td>
			                </tr>
		                </table>
	                <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                </div>                	
            
<div id="divListingOuter">
  <div class="roundtopListngOuter"><img src="Images/Curves/Grid-TLC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
  <table width="100%" height="44" cellpadding="0" cellspacing="0" class="txtListing">
  <tr>
    <td width="240" align="right" nowrap><TABLE border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="120">
          <TR>
            <TD width="5"><asp:Label ID="lblSearch" runat="server" Text="Search : "/></TD>
            <TD class="txtListing"><asp:TextBox ID="txtSearch" Width="250px" runat="server"></asp:TextBox></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
            <TD class="txtListing"><asp:Button BorderStyle="None" class="txtListing" style="cursor:pointer;" align="absMiddle" Font-Bold="true" ID="btnSearch" BorderWidth="0" runat="server" Text="GO"/></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
            <TD class="txtListing"><asp:Button BorderStyle="None" class="txtListing" style="cursor:pointer;" align="absMiddle" Font-Bold="true" ID="btnClear" BorderWidth="0" runat="server" Text="Clear"/></TD>
            <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
          </TR>
      </TABLE></td>
	  <td width="380" height="30" align="right" nowrap>&nbsp;</td>
	  <td width="269" align="center" nowrap>
	  <div style="float:left; width:120px; margin-right:19px;">
	  <TABLE runat=server id="tblChangeAdmin" border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="120" visible="false">
		<TR>
		  <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
		  <TD class="txtListing"><a id="btnChangeAdmin" tabindex="10" class="txtListing" href="ChangeAdminAccount.aspx" runat="server"> <strong><img src="Images/Icons/ChangeAdmin.gif" width="15" height="15" hspace="5" vspace="0" align="absMiddle"  border="0">&nbsp;Change Admin</strong> </a></TD>
		  <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
		</TR>
	  </TABLE>
	  </div>
	  <div style="float:left; width:120px;">
	  <TABLE runat=server id="tblExportToExcel" border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="120">
		<TR>
		  <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
		  <TD class="txtListing"><a id="btnExportToExcel" target="_blank" tabindex="10" class="txtListing" href='<%="ExportToExcel.aspx?page=specialist&CompanyID=" & Request("companyID")  & "&bizDivId=" & Request("bizDivId") & "&FilterString=" & ViewState("FilterString")%>'> <strong><img src="Images/Icons/Icon-Excel.gif" width="15" height="15" hspace="5" vspace="0" align="absMiddle"  border="0">&nbsp;Export to Excel</strong> </a></TD>
		  <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
		</TR>
	  </TABLE>
	  </div>
	  </td>
	  <td id="tdAddUser" runat="server" width="124" align="center" nowrap><TABLE  border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="90">
		<TR>
		  <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
		  <TD class="txtListing"><a id="btnAddSpecialist" runat=server class="txtListing" tabindex="11" >
												<strong><img src="Images/Icons/Icon-SpecialistSmall.gif" width="12" height="16" hspace="5" vspace="0" align="absMiddle"  border="0">&nbsp;Add User</strong>
												  </a></TD>
		  <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
		</TR>
	</TABLE></td>
	
	<td width="160" align="center" nowrap  id="tdAddMultipleUser" runat="server">
	<div style="float:left; width:160px; height:24px;">
	<TABLE  border="0" cellPadding="0" cellSpacing="0" bgColor="#e6e6e6" width="150">
		<TR>
		  <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-LC.gif" width="5"></TD>
		  <TD class="txtListing"><a id="hdnbtnNext" runat=server class="txtListing" tabindex="11" style="cursor:pointer;">
			<strong><img src="Images/Icons/Icon-SpecialistSmall.gif" width="12" height="16" hspace="5" vspace="0" align="absMiddle"  border="0">&nbsp;Add Multiple Users</strong>
			  </a>			  
			  </TD>
		  <TD width="5"><IMG height="24" src="Images/Curves/Grid-Btn-RC.gif" width="5"></TD>
		</TR>
	</TABLE></div>	
            </td>
  </tr>
  <tr>
    <td>
      
    </td>
  </tr>
</table>

  <asp:GridView ID="gvContacts" runat="server" AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#555354"
        Border="0" PageSize='<%#admin.Applicationsettings.GridViewPageSize%>'  DataKeyNames="ContactID"   PagerSettings-Mode="NextPreviousFirstLast"
        CssClass="bdrBtmListing" DataSourceID="ObjectDataSource1" PagerSettings-Position="TopAndBottom" Width="100%"   AllowSorting="true">
     
         <PagerTemplate>
         	<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="10" cellspacing="0" bgcolor="#DAD8D9">
				<tr>
				    <td width="176">&nbsp;</td>
				    <td align="right">
				        <TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
				            <TR>
				                <td align="right" valign="middle">
					                <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
					            </td>
				                <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
					                <table width="100px" border="0" cellpadding="0" cellspacing="0" align="right">
			                            <tr>
					                    <td align="right" width="36">
					                        <div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
					                            <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
					                        </div>	
					                        <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
					                            <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
					                        </div>																							   
					                    </td>
					                    <td width="50" align="center" style="margin-right:3px; ">													
					                        <asp:DropDownList style="margin-left:4px;"  OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField width40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
					                    <td width="30" >
					                        <div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
						                        <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
						                    </div>
						                    <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
						                        <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
						                    </div>
					                    </td>
					                    </tr>
				                    </table>
				                </td>
				            </TR>
			            </TABLE>
			        </td>
			    </tr>
			</table>
            
        </PagerTemplate>
    <headerstyle BackColor="#993366" Font-Bold="true" ForeColor="#CE3538" ></headerstyle> 
        <Columns>
            
  
            <asp:TemplateField SortExpression="Name" HeaderText="Name" HeaderStyle-CssClass="headerListing">                
               <ItemStyle Wrap=true  Width="10%"  CssClass="contentListing" />
               <ItemTemplate>             
                   <%#Container.DataItem("Name")%>
               </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField SortExpression="Phone" HeaderText="Contact Numbers" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true  Width="15%"  CssClass="contentListing" />
                <ItemTemplate>             
                    <%#  GetInfo( Container.DataItem("Phone"),Container.DataItem("Fax"),Container.DataItem("Mobile")  ) %>
                </ItemTemplate>
            </asp:TemplateField> 
            
             <asp:TemplateField SortExpression="Email" HeaderText="Email" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true  Width="15%"  CssClass="contentListing" />
                <ItemTemplate>             
                     <%#"<a class=footerTxtSelected href='mailto:"   & Container.DataItem("Email") & "'>" &  Container.DataItem("Email") & "</a>"%>
                </ItemTemplate>
            </asp:TemplateField> 
			
            
            <asp:BoundField ItemStyle-CssClass="contentListing" HeaderStyle-CssClass="headerListing" ItemStyle-Width="17%"   Visible=true  DataField="JobTitle" SortExpression="JobTitle" HeaderText="Job Title" />  
            <asp:TemplateField HeaderText="Spend Limit" SortExpression="SpendLimit" HeaderStyle-CssClass="headerListing">               
               <ItemStyle Wrap=true HorizontalAlign=Right Width="10%" CssClass="contentListing" />
               <ItemTemplate >             
                   <%#(Container.DataItem("SpendLimit"))%>
               </ItemTemplate>
            </asp:TemplateField> 
            
             <asp:TemplateField SortExpression="ReceiveWONotification" HeaderText="WO Notification" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true  Width="12%"  CssClass="contentListing" />
                <ItemTemplate>             
                     <%#ReceiveWONotification(Container.DataItem("ReceiveWONotification"))%>
                </ItemTemplate>
            </asp:TemplateField>   
            
            <asp:BoundField ItemStyle-CssClass="contentListing" HeaderStyle-CssClass="headerListing" ItemStyle-Width="5%"   Visible=true  DataField="Type" SortExpression="Type" HeaderText="Type" />  
			
            <%--Poonam modified on 31/08/2016 - Task - OA-315 : OA - Add Last login date in User listings for Suppliers and Clients --%>
            <asp:TemplateField SortExpression="LastLoginInfo" HeaderText="Last Login" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true  Width="14%"  CssClass="contentListing" />
                <ItemTemplate>              
                   <%#(Container.DataItem("LastLoginInfo"))%>
               </ItemTemplate>
            </asp:TemplateField>

			 <asp:TemplateField SortExpression="DeletedStatus" HeaderText="Delete Status" HeaderStyle-CssClass="headerListing"> 
                <ItemStyle Wrap=true  Width="12%"  CssClass="contentListing" />
                <ItemTemplate>              
                   <%#GetDeletedStatus(Container.DataItem("DeletedStatus"))%>
               </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField HeaderText="Performance rating" SortExpression="PercentageRating" HeaderStyle-CssClass="headerListing">               
               <ItemStyle Wrap=true HorizontalAlign=Right Width="7%" CssClass="contentListing" />
               <ItemTemplate >             
                 <%#Container.DataItem("PercentageRating").ToString().Replace(".00","") & "%"%> 
               </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField HeaderText="Total Ratings" SortExpression="TotalRating" HeaderStyle-CssClass="headerListing">               
               <ItemStyle Wrap=true HorizontalAlign=Right Width="7%" CssClass="contentListing" />
               <ItemTemplate >             
                 <%#Container.DataItem("TotalRating").ToString().Replace(".00","")%> 
               </ItemTemplate>
            </asp:TemplateField> 

			
            <asp:BoundField ItemStyle-CssClass="contentListing" HeaderStyle-CssClass="headerListing" ItemStyle-Width="5%"   Visible=true  DataField="Status" SortExpression="Status" HeaderText="Status" />  
            <asp:TemplateField HeaderText="Edit" HeaderStyle-CssClass="headerListingBdrTop">               
               <ItemStyle Wrap=true HorizontalAlign=Left Width="5%" CssClass="contentListingBdrTop" />
               <ItemTemplate >    
               <%--'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User--%>         
                   <%# "<a class=footerTxtSelected href=SpecialistsForm.aspx?ContactId=" & Container.DataItem("ContactID") & "&CompanyID=" & hdncompanyid.Value & "&bizDivId=" & hdnbizid.Value & "&classId=" & Request("classId") & "><img src='Images/Icons/Icon-Pencil.gif' border='0'>" & "</a>"%>
               </ItemTemplate>
            </asp:TemplateField> 
        </Columns>
         <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" />
         
</asp:GridView>


<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SelectFromDB"   EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.UCSpecialistsListing" 
        SortParameterName="sortExpression">    
              
</asp:ObjectDataSource>
  
<div class="roundBtmListngOuter"><img src="Images/Curves/Grid-BLC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
</div>


 </ContentTemplate>
 </asp:UpdatePanel>
   