<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCAccountSummary.ascx.vb"
    Inherits="Admin.UCAccountSummary" %>
<script language="javascript" src="JS/Scripts.js" type="text/javascript"></script>
<div id="divAccountSummary">
    <div class="divAccountTopCurves">
        <img src="Images/Curves/MAS-Bdr-TL.gif" alt="" width="5" height="5" class="corner"
            style="display: none" />
    </div>
    <table runat="server" id="tblAccSumm" width="200" border="0" cellpadding="0" cellspacing="0"
        class="summaryBox">
        <tr valign="top">
            <td align="left">
                <p class="TxtListing">
                    <strong><a class="TxtListing" runat="server" id="lnkAccountProfile">My Company Summary:</a></strong></p>
                <ul class="liNavigationBlack">
                    <li>
                        <asp:Label ID="lblAccountName" runat="server"></asp:Label></li>
                    <li>
                        <asp:Label ID="lblAccountId" runat="server"></asp:Label></li>
                    <li>
                        <asp:Label ID="lblAdminName" runat="server"></asp:Label></li>
                    <li>
                        <asp:Label ID="lblAdminEmail" runat="server"></asp:Label></li>
                    <li runat="server" id="liSubClass">
                        <asp:Label ID="lblSubClass" runat="server"></asp:Label></li>
                    <li runat="server" id="liDateApproved">
                        <asp:Label ID="lblDateApproved" runat="server"></asp:Label></li>
                    <li runat="server" id="liActiveInPast90days">
                        <asp:Label ID="lblActiveInPast90days" runat="server"></asp:Label></li>
                    <li runat="server" id="liSpecialist">
                        <asp:Literal ID="litSpecialists" runat="server"></asp:Literal></li>
                    <li runat="server" id="liLocation">
                        <asp:Literal ID="litLocations" runat="server"></asp:Literal></li></ul>
                <div id="liRating" runat="server">
                    <asp:Literal ID="litRating" runat="server"></asp:Literal></div>
                <div id="divDispAllRatings" style="visibility: hidden; overflow: hidden; position: absolute;
                    height: 0px;" runat="server">
                    <table class="whiteBackground" style="margin: 0px;" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="20" align="center">
                                <span class="liNavigationBlack">
                                    <img src="Images/Icons/Icon-Positive.gif" width="18" height="18" align="absmiddle" /></span>
                            </td>
                            <td align="left">
                                <span class="liNavigationBlack">Positive
                                    <asp:Label ID="lblPosRating" Text="" runat="server"></asp:Label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <span sclass="liNavigationBlack">
                                    <img src="Images/Icons/Icon-Neutral.gif" width="13" height="5" align="absmiddle" /></span>
                            </td>
                            <td align="left">
                                <span class="liNavigationBlack">Neutral
                                    <asp:Label ID="lblNeuRating" Text="" runat="server"></asp:Label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <span class="liNavigationBlack">
                                    <img src="Images/Icons/Icon-Negative.gif" width="17" height="18" align="absmiddle" /></span>
                            </td>
                            <td align="left">
                                <span class="liNavigationBlack">Negative
                                    <asp:Label ID="lblNegRating" Text="" runat="server"></asp:Label>
                                </span>
                            </td>
                        </tr>
                    </table>
                    <div class="bottomCurveWhiteRight width152">
                        <img src="Images/Curves/Bdr-White-BL.gif" alt="" width="5" height="5" class="corner"
                            style="display: none" /></div>
                </div>
            </td>
        </tr>
    </table>
    <table height="64" id="tblFunds" runat="server" class="summaryBox">
        <tr>
            <td valign="top" class="paddingL13">
                <ul class="liNavigationBlack">
                    <li runat="server" id="liBalanceDue">
                        <asp:Literal ID="litBalanceDue" runat="server"></asp:Literal></li>
                    <li runat="server" id="liFunds">
                        <asp:Literal runat="server" ID="litFundsLabel"></asp:Literal>
                        <ul class="liNavigation paddingT5">
                            <li runat="server" id="liWithdrawalBalance">
                                <asp:Literal ID="litWithdrawalBalance" runat="server"></asp:Literal></li>
                            <li runat="server" id="liWOBalance">
                                <asp:Literal ID="litWOBalance" runat="server"></asp:Literal></li>
                        </ul>
                    </li>
                    <li runat="server" id="liLastTransfer">
                        <asp:Literal ID="litLastTransfer" runat="server"></asp:Literal></li>
                </ul>
            </td>
        </tr>
    </table>
   <%--'Poonam modified on 28/10/2016 - Task - OA-344 : OA - New System details on Account summary--%>
    <table runat="server" id="tblAccountHistory" width="200" border="0" cellpadding="0"
        cellspacing="0" class="summaryBox">
        <tr valign="top">
            <td align="left">
                <ul class="liNavigationBlack">
                    <li id="liCreated" runat="server">
                        <asp:Label ID="lblCreated" runat="server"></asp:Label></li>

                    <li id="liApproved" runat="server">
                        <asp:Label ID="lblApproved" runat="server"></asp:Label></li>

                    <li id="liModified" runat="server">
                        <asp:Label ID="lblModified" runat="server"></asp:Label></li>
                </ul>
            </td>
        </tr>
    </table>
    <div class="divAccountBotCurves">
        <img src="Images/Curves/MAS-Bdr-BL.gif" alt="" width="5" height="5" class="corner"
            style="display: none" />
    </div>
</div>
