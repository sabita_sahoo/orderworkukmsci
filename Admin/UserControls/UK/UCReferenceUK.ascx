<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCReference.ascx.vb" Inherits="Admin.UCReference" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Panel ID="pnlUserForm" runat="server"  >
 <asp:UpdatePanel ID="UpdatePnlUserForm" runat="server"  UpdateMode="Conditional" RenderMode=Inline>
  <ContentTemplate>

<asp:Panel ID="pnlSubmitForm" runat="server" visible="true">

<div id="divValidationMain" class="divValidation" runat=server >
	<div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="15" align="center">&nbsp;</td>
				<td height="15">&nbsp;</td>
				<td height="15">&nbsp;</td>
			</tr>
			<tr valign="middle">
				<td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				<td class="validationText"><div  id="divValidationMsg"></div>
				<asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
				<asp:ValidationSummary id="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
				</td>
				<td width="20">&nbsp;</td>
			</tr>
			<tr>
				<td height="15" align="center">&nbsp;</td>
				<td height="15">&nbsp;</td>
				<td height="15">&nbsp;</td>
			</tr>
		</table>
	<div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
</div>	


<div id="divMainTab">
       		  			<DIV class="paddedBox" id="divReferences">
						
						
					<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
                      <TR>
                        <TD align="left" class="HeadingRed">References</TD>
                        <TD align="left" class="padTop17"><TABLE id="tblTopBtn" cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; " runat="server">
                          <TR>
                            <TD vAlign="bottom" align="right"><TABLE cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
                                <TR>
                                  <TD width="5"><IMG height="24" src="../Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                  <TD width="110" class="txtBtnImage"><asp:Linkbutton id="btnSaveTop" CausesValidation="false" runat="server" class="txtListing" TabIndex="10" OnClientClick="javascript:callSetFocusValidation('UCReferenceUK1')"> <img src="Images\Icons\Icon-Save.gif" width="14" height="13" hspace="5" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:Linkbutton></TD>
                                  <TD width="5"><IMG height="24" src="../Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                </TR>
                            </TABLE></TD>
                            <TD width="10"></TD>
                            <TD width="125" align="right" vAlign="top"><TABLE width="125" border="0" cellPadding="0" cellSpacing="0" bgColor="#F0F0F0">
                                <TR>
                                  <TD width="5"><IMG height="24" src="../Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                  <TD class="txtBtnImage"><asp:LinkButton id="btnResetTop" runat="server" TabIndex="11" CausesValidation="false" class="txtListing"> <img src="Images\Icons\Icon-Reset.gif" width="14" height="16" hspace="5" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton></TD>
                                  <TD width="5"><IMG height="24" src="../Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                </TR>
                            </TABLE></TD>
                          </TR>
                        </TABLE></TD>
                      </TR>
                    </TABLE>
						
						
						
					<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">

														<TR>
															<TD class="formTxt" vAlign="top" height="50">Clients want Suppliers with good 
																references! Please add a reference now. OrderWork Ltd. may contact your references but will not share your reference details with any of its buyers or other third parties.</TD>
														</TR>
						  </TABLE>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD class="formTxtOrange" vAlign="top" height="18">Reference 1</TD>
														</TR>
													</TABLE>
													<TABLE height="18" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR vAlign="top">
															<TD class="formTxt" vAlign="top" align="left" width="162" height="14">Name</TD>
															<TD class="formTxt" align="left" width="162" height="14">Job Title
															</TD>
															<TD class="formTxt" align="left" height="14">Company Name
															</TD>
														</TR>
														<TR vAlign="top">
															<TD align="left" height="24">
																<asp:TextBox id="txtReferences1Name" tabIndex="12" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD align="left" height="24">
																<asp:TextBox id="txtReferences1JobTitle" tabIndex="13" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD align="left" height="24">
																<asp:TextBox id="txtReferences1CmpyName" tabIndex="14" runat="server" CssClass="formField width150"></asp:TextBox></TD>
														</TR>
													</TABLE>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR vAlign="top">
															<TD class="formTxt" align="left" width="162" height="14">Email Address
																<asp:RegularExpressionValidator id="RegularExpressionValidator2" runat="server" ErrorMessage="Please enter your Reference 1 Email Address in correct format"
																	ForeColor="#EDEDEB" ControlToValidate="txtReferences1Email" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
														  </TD>
															<TD class="formTxt" width="162" height="14">Mobile</TD>
															<TD class="formTxt" width="162" height="14">Phone</TD>
															<TD class="formTxt" height="14" style="padding:0px 0px 0px 1px; margin:0px;">Work Done Date
															
															<asp:RegularExpressionValidator ID="rqWorkDoneDate1"  ControlToValidate="txtReferences1WorkDone" ErrorMessage="Work Done Date for Reference 1 should be in DD/MM/YYYY Format" ForeColor="#EDEDEB" 
							                                    	ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"  runat="server">*</asp:RegularExpressionValidator>
                                                                <asp:RangeValidator ID="rngWorkDoneDate1" ControlToValidate="txtReferences1WorkDone" MinimumValue="31/12/1985" MaximumValue="31/12/2206" Type="Date" ForeColor="#EDEDEB"
				                                                    ErrorMessage="Work Done Date for Reference 1 should not be later than the current date." runat="server">*</asp:RangeValidator>

															</TD>
														</TR>
														<TR vAlign="top">
															<TD height="24">
																<asp:TextBox id="txtReferences1Email" tabIndex="15" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD height="24">
																<asp:TextBox id="txtReferences1Mobile" tabIndex="16" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD align="left" height="24">
																<asp:TextBox id="txtReferences1Phone" tabIndex="17" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD height="24">
																<asp:TextBox id="txtReferences1WorkDone" tabIndex="18" runat="server" CssClass="formField width105"></asp:TextBox>																
																 <img alt="Click to Select" src="Images/calendar.gif" id="btnWorkDoneDate3" style="cursor:pointer; vertical-align:top;" />
                                                                <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnWorkDoneDate3 TargetControlID="txtReferences1WorkDone" ID="calWorkDoneDate1" runat="server">
                                                                </cc1:CalendarExtender>
																</TD>
														</TR>
													</TABLE>
													<TABLE height="18" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR vAlign="top">
															<TD class="formTxt" align="left" height="14" style="padding:0px 0px 0px 1px; margin:0px;">Work Done Description</TD>
														</TR>
														<TR vAlign="top">
															<TD align="left" height="63">
																<asp:TextBox id="txtReferences1WorkDescription" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" tabIndex="19" runat="server" CssClass="formFieldGrey width686height60"></asp:TextBox></TD>
														</TR>
													</TABLE>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD class="formTxtOrange" vAlign="middle" height="10">&nbsp;</TD>
														</TR>
														<TR>
															<TD class="formTxtOrange" vAlign="top" height="18">Reference 2
															</TD>
														</TR>
													</TABLE>
													<TABLE height="18" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR vAlign="top">
															<TD class="formTxt" vAlign="top" align="left" width="162" height="14">Name</TD>
															<TD class="formTxt" align="left" width="162" height="14">Job Title
															</TD>
															<TD class="formTxt" align="left" height="14">Company Name
															</TD>
														</TR>
														<TR vAlign="top">
															<TD align="left" height="24">
																<asp:TextBox id="txtReferences2Name" tabIndex="20" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD align="left" height="24">

																<asp:TextBox id="txtReferences2JobTitle" tabIndex="21" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD align="left" height="24">
																<asp:TextBox id="txtReferences2CmpyName" tabIndex="22" runat="server" CssClass="formField width150"></asp:TextBox></TD>
														</TR>
													</TABLE>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR vAlign="top">
															<TD class="formTxt" align="left" width="162" height="14">Email Address
																<asp:RegularExpressionValidator id="RegularExpressionValidator3" runat="server" ErrorMessage="Please enter your Reference 2 Email Address in correct format"
																	ForeColor="#EDEDEB" ControlToValidate="txtReferences2Email" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></TD>
															<TD class="formTxt" width="162" height="14">Mobile</TD>
															<TD class="formTxt" width="162" height="14">Phone</TD>
															<TD class="formTxt" height="14" style="padding:0px 0px 0px 1px; margin:0px;">Work Done Date
															<asp:RegularExpressionValidator ID="rqWorkDoneDate2"  ControlToValidate="txtReferences2WorkDone" ErrorMessage="Work Done Date for Reference 2 should be in DD/MM/YYYY Format" ForeColor="#EDEDEB" 
							                                    	ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"  runat="server">*</asp:RegularExpressionValidator>
                                                                <asp:RangeValidator ID="rngWorkDoneDate2" ControlToValidate="txtReferences2WorkDone" MinimumValue="31/12/1985" MaximumValue="31/12/2206" Type="Date" ForeColor="#EDEDEB"
				                                                    ErrorMessage="Work Done Date for Reference 2 should not be later than the current date." runat="server">*</asp:RangeValidator>
															</TD>
														</TR>
														<TR vAlign="top">
															<TD height="24">
																<asp:TextBox id="txtReferences2Email" tabIndex="23" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD height="24">
																<asp:TextBox id="txtReferences2Mobile" tabIndex="24" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD align="left" height="24">
																<asp:TextBox id="txtReferences2Phone" tabIndex="25" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD height="24">
																<asp:TextBox id="txtReferences2WorkDone" tabIndex="26" runat="server" CssClass="formField width105"></asp:TextBox>																
																<img alt="Click to Select" src="Images/calendar.gif" id="btnWorkDoneDate2" style="cursor:pointer; vertical-align:top;" />
                                                                <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnWorkDoneDate2 TargetControlID="txtReferences2WorkDone" ID="calWorkDoneDate2" runat="server">
                                                                </cc1:CalendarExtender>
																</TD>
														</TR>
													</TABLE>
													<TABLE height="18" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR vAlign="top">
															<TD class="formTxt" align="left" height="14" style="padding:0px 0px 0px 1px; margin:0px;">Work Done Description</TD>
														</TR>
														<TR vAlign="top">
															<TD align="left" height="63">
																<asp:TextBox id="txtReferences2WorkDescription" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" tabIndex="27" runat="server" CssClass="formFieldGrey width686height60"></asp:TextBox></TD>
														</TR>
													</TABLE>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD class="formTxtOrange" vAlign="middle" height="10">&nbsp;</TD>
														</TR>
														<TR>
															<TD class="formTxtOrange" vAlign="top" height="18">Reference 3</TD>
														</TR>
													</TABLE>
													<TABLE height="18" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR vAlign="top">
															<TD class="formTxt" vAlign="top" align="left" width="162" height="14">Name</TD>
															<TD class="formTxt" align="left" width="162" height="14">Job Title
															</TD>
															<TD class="formTxt" align="left" height="14">Company Name
															</TD>
														</TR>
														<TR vAlign="top">
															<TD align="left" height="24">
																<asp:TextBox id="txtReferences3Name" tabIndex="28" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD align="left" height="24">
																<asp:TextBox id="txtReferences3JobTitle" tabIndex="29" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD align="left" height="24">
																<asp:TextBox id="txtReferences3CmpyName" tabIndex="30" runat="server" CssClass="formField width150"></asp:TextBox></TD>
														</TR>
													</TABLE>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR vAlign="top">
															<TD class="formTxt" align="left" width="162" height="14">Email Address
																<asp:RegularExpressionValidator id="RegularExpressionValidator4" runat="server" ErrorMessage="Please enter your Reference 3  Email Address in correct format"
																	ForeColor="#EDEDEB" ControlToValidate="txtReferences3Email" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></TD>
															<TD class="formTxt" width="162" height="14">Mobile</TD>
															<TD class="formTxt" width="162" height="14">Phone</TD>
															<TD class="formTxt" height="14" style="padding:0px 0px 0px 1px; margin:0px;">Work Done Date
															<asp:RegularExpressionValidator ID="rqWorkDoneDate3"  ControlToValidate="txtReferences3WorkDone" ErrorMessage="Work Done Date for Reference 3 should be in DD/MM/YYYY Format" ForeColor="#EDEDEB" 
							                                    	ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"  runat="server">*</asp:RegularExpressionValidator>
                                                                <asp:RangeValidator ID="rngWorkDoneDate3" ControlToValidate="txtReferences3WorkDone" MinimumValue="31/12/1985" MaximumValue="31/12/2206" Type="Date" ForeColor="#EDEDEB"
				                                                    ErrorMessage="Work Done Date for Reference 3 should not be later than the current date." runat="server">*</asp:RangeValidator>
															</TD>
														</TR>
														<TR vAlign="top">
															<TD height="24">
																<asp:TextBox id="txtReferences3Email" tabIndex="31" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD height="24">
																<asp:TextBox id="txtReferences3Mobile" tabIndex="32" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD align="left" height="24">
																<asp:TextBox id="txtReferences3Phone" tabIndex="33" runat="server" CssClass="formField width150"></asp:TextBox></TD>
															<TD height="24">
																<asp:TextBox id="txtReferences3WorkDone" tabIndex="34" runat="server" CssClass="formField width105"></asp:TextBox>
																<img alt="Click to Select" src="Images/calendar.gif" id="btnWorkDoneDate1" style="cursor:pointer; vertical-align:top;" />
                                                                <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnWorkDoneDate1 TargetControlID="txtReferences3WorkDone" ID="calWorkDoneDate3" runat="server">
                                                                </cc1:CalendarExtender>
																</TD>
														</TR>
													</TABLE>
													<TABLE height="18" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR vAlign="top">
															<TD class="formTxt" align="left" height="14" style="padding:0px 0px 0px 1px; margin:0px;">Work Done Description</TD>
														</TR>
														<TR vAlign="top">
															<TD align="left" height="63">
																<asp:TextBox id="txtReferences3WorkDescription"  TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" tabIndex="35" runat="server" CssClass="formFieldGrey width686height60"></asp:TextBox></TD>
														</TR>
													</TABLE>
													
													
													
						<!-- panel to hide for upgradation -->
						    <table id="tblBottomBtn" width="100%" border="0" cellspacing="0" cellpadding="0" runat="server">
							      <tr>
								    <td>&nbsp;</td>
								    <td width="130">&nbsp;</td>
								    <td width="10">&nbsp;</td>
								    <td width="125">&nbsp;</td>
							      </tr>
							      <tr valign="top">
								    <td>&nbsp;</td>
								    <td align="right"><TABLE cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
								      <TR>
									    <TD width="5"><IMG height="24" src="../Images/Curves/IconGrey-LC.gif" width="5"></TD>
									    <TD width="110" class="txtBtnImage"><asp:Linkbutton id="btnSaveBottom" CausesValidation="false" runat="server" TabIndex="36" class="txtListing" OnClientClick="javascript:callSetFocusValidation('UCReferenceUK1')"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:Linkbutton></TD>
									    <TD width="5"><IMG height="24" src="../Images/Curves/IconGrey-RC.gif" width="5"></TD>
								      </TR>
								    </TABLE></td>
								    <td>&nbsp;</td>
								    <td align="right"><TABLE cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
								      <TR>
									    <TD width="5"><IMG height="24" src="../Images/Curves/IconGrey-LC.gif" width="5"></TD>
									    <TD class="txtBtnImage"><asp:LinkButton id="btnResetBottom" runat="server" TabIndex="37" CausesValidation="false" class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton></TD>
									    <TD width="5"><IMG height="24" src="../Images/Curves/IconGrey-RC.gif" width="5"></TD>
								      </TR>
								    </TABLE></td>
							      </tr>
							</table>
				  </DIV>
				</div>
				
				</asp:Panel>	
				
				<!-- panel to show for upgradation -->
			<asp:panel ID="pnlUpgradation" runat="server" visible="false">
			
			
			    <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
							  <TR>
								<TD vAlign="top" align="left" height="20">&nbsp;</TD>
								<TD vAlign="top" align="left" height="20">&nbsp;</TD>
								<TD vAlign="top" align="left" height="20">&nbsp;</TD>
								<TD vAlign="top" align="left" width="20">&nbsp;</TD>
							  </TR>
							  <TR>
								<TD vAlign="top" align="right">
								  <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
								    <asp:linkbutton class="txtButtonRedBack" ID="lnkComplete" tabIndex="26" runat="server" causesvalidation="False">&nbsp;Back&nbsp;</asp:linkbutton>
							      </div>
                                </TD>
								<TD width="10"></TD>
								<TD vAlign="top" align="right" width="120">
								  <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:120px;">
                                    <asp:linkbutton class="txtButtonRed" ID="lnkNextSubmit" tabIndex="27" runat="server" causesvalidation="False">&nbsp;Complete Upgrade&nbsp;</asp:linkbutton>
                                  </div>
                                </TD>
								<TD vAlign="top" align="left" width="20">&nbsp;</TD>
							  </TR>
					</TABLE>				
							
		
			</asp:panel>	
				
				   <!-- Confirm Action panel --> 
			<asp:panel ID="pnlConfirm" runat="server" visible="false">
			  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="padTop50">
                <tr>
                  <td width="350" align="right"><table cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td class="txtBtnImage"><asp:LinkButton id="btnConfirm" runat="server" CausesValidation="false" class="txtListing" TabIndex="38" > <img src="Images/Icons/Icon-Confirm.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Confirm </asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                  <td width="10">&nbsp;</td>
                  <td align="left" ><table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0" >
                      <tr>
                 <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td align="center" class="txtBtnImage">
                            <asp:LinkButton id="btnCancel" runat="server" CausesValidation="false" class="txtListing" TabIndex="39" > <img src="Images/Icons/Icon-Cancel.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
                          </td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
            </asp:panel>
            
            
 </ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID="btnSaveTop" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnResetTop" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnSaveBottom" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnResetBottom" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnConfirm" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />

</Triggers>
</asp:UpdatePanel>
</asp:Panel> 

 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatepnlUserForm" runat="server">
        <ProgressTemplate>
            <div>
                <img  align=middle src="Images/indicator.gif" />
                  Please Wait, saving data...
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="pnlUserForm" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />



        
       
