<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSViewSalesInvoiceUK.ascx.vb" Inherits="Admin.UCMSViewSalesInvoiceUK" %>
  
<style type="text/css">
<!--
.invoiceLabel {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 32px;
	color: #B7B7B7;
	font-weight: bold;
	line-height:38px;
}
.InvoiceText {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.bdr1 {
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 4px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.bdr2 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #C6C6C6;
	border-right-color: #C6C6C6;
	border-bottom-color: #C6C6C6;
	border-left-color: #C6C6C6;
}
.bdr3 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.style3 {font-size: 11px}
.style5 {font-weight: bold}
.style6 {font-weight: bold}
td
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 16px;
	color: #000000;
}
body {
	background-color: #CECECE;
}
    .style7
    {
        width: 148px;
    }
-->
</style>
<table height="95%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="45" height="140">&nbsp;</td>
    <td width="650" height="140"><table width="100%" cellpadding="0" cellspacing="0" style=" border-width:medium; border-bottom-width:5px;border-bottom-color:#000000;">
        <tr>
          <td width="311">&nbsp;</td>
          <td width="131" align="left" valign="bottom" >&nbsp;</td>
        </tr>
        <tr>
          <td width="311" valign="middle"><img src="../../Images/Invoice_Logo_New.gif" runat="server" id="imgLogo" alt="OrderWork - IT Services Reinvented" width="181" height="64" border="0" class="LogoImg" /></td>
          <td align="right" valign="bottom" ><table width="99%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right" valign="bottom"  style="height: 22px;"><span class="style1 style5">
                  <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                </span></td>
              </tr>
              <tr>
                <td align="right" valign="bottom"  ><asp:Label ID="lblAddress" runat="server"></asp:Label></td>
              </tr>
              <tr>
				<td align="right" valign="bottom" style="height: 15px"  ><asp:Label ID="lblCity" runat="server"></asp:Label><asp:Label ID="lblPostCode" runat="server"></asp:Label>
				</td>
              </tr>
              
              <tr>
                <td align="right" valign="bottom"  > Tel:&nbsp;
                    <asp:Label ID="lblTelNo" runat="server"></asp:Label></td>
              </tr>
              <tr>
                <td valign="bottom" ></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td width="311" class="bdr1" >&nbsp;</td>
          <td align="left" valign="bottom" class="bdr1" >&nbsp;</td>
        </tr>
    </table></td>
    <td width="45" height="140">&nbsp;</td>
  </tr>
  <tr>
    <td width="45" height="180" align="center">&nbsp;</td>
    <td width="650" height="180" align="center"><p class="invoiceLabel">Invoice</p>
        <table border="0" cellpadding="5" cellspacing="0" width="100%">
          <tr>
            <td align="left" valign="top"  style="border-bottom: 0px; border-right-width: 0px"><strong>Invoice To:<br>
              </strong>
                <asp:Label ID="lblStoreName" Visible =false Text="Natasha Vora" runat="server"></asp:Label>
                <br />
                <asp:Label ID="lblCompNameInvoiceTo" runat="server"></asp:Label>
                <br />
                <asp:Label ID="lblInvoiceTo" runat="server"></asp:Label>
&nbsp;</td>
            <td align="right" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="95" align="left"><strong>Account No.&nbsp;</strong></td>
                  <td width="8" align="left">:</td>
                  <td width="85" align="left"><asp:Label ID="lblAccountNo" runat="server"></asp:Label></td>
                </tr>
                <tr>
                  <td width="95" align="left"><strong>Credit Terms&nbsp;</strong></td>
                  <td align="left">:</td>
                  <td align="left"><asp:Label ID="lblCreditTerms" runat="server"></asp:Label></td>
                </tr>
                <tr>
                  <td width="95" align="left"><strong>Invoice No.&nbsp;</strong></td>
                  <td align="left">:</td>
                  <td align="left"><strong>
                    <asp:Label ID="lblInvoiceNo" runat="server"></asp:Label>
                  </strong> </td>
                </tr>
                <tr>
                  <td width="95" align="left"><strong>Invoice Date&nbsp;</strong></td>
                  <td align="left">:</td>
                  <td align="left"><asp:Label ID="lblInvoiceDate" runat="server"></asp:Label></td>
                </tr>
                <tr>
                  <td width="95" align="left"><strong>VAT No.</strong></td>
                  <td align="left">:</td>
                  <td align="left"><strong>
                    <asp:Label ID="lblVATNo" runat="server" ></asp:Label>
                  </strong></td>
                </tr>
                 <tr>
                  <td width="95" align="left"><strong><asp:Label ID="lblInvoicePo" Text="Invoice PO" runat="server" ></asp:Label></strong></td>
                   <td align="left"><asp:Label ID="lbldot" Text=":" runat="server" ></asp:Label></td>
                    <td align="left"><strong>
                    <asp:Label ID="lblInvoicePoNumber" runat="server" ></asp:Label>
                  </strong></td>
                </tr>
            </table></td>
          </tr>
        </table>
    <p>&nbsp;</p></td>
    <td width="45" height="180" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td width="45" align="right" valign="top">&nbsp;</td>
    <td width="650" align="right" valign="top"><table width="650" border="0" cellpadding="0" cellspacing="0">
      <tr>        
        <td width="150" height="30;"  align="left"  style="border-right-width: 0px;" class="style7"><strong>Description</strong><strong></strong></td>
        <td style="border-right-width: 0px;" width="50" height="30;" align="left" ><strong>Quantity</strong></td>
        <td width="50" height="30;" align="left"   style="border-right-width: 0px"><strong>Your PO</strong></td>
                <td width="50" height="30;" align="left"  style="border-right-width: 0px"><strong>
         <asp:Label ID="lblStoreId" runat="server" Text="Store Id"></asp:Label>
             </strong></td>
        <td width="50" height="30;" align="left"   ><strong>Net Price</strong></td>
        <td width="60" height="30;" align="right" background="Drag to a file to choose it." style="width:border-right-width: 0px"><strong>VAT(
              <asp:Label ID="lblTotalVatAsPerSite1" runat="server"></asp:Label>%)</strong></td>
        <td width="50" height="30" align="right" ><strong>Amount</strong></td>
      </tr>
      <asp:Repeater ID="rptList" runat="server" Visible="true">
        <ItemTemplate>
          <tr>
            <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"><%#Container.DataItem("Description")%> </td>
            <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"><%#Container.DataItem("Quantity")%> </td>
            <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"><%#Container.DataItem("PONumber")%> </td>
                       <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"> <asp:Label ID="lblStoreIdVal" runat="server" Text='<%# Container.DataItem("StoreId")%>'></asp:Label></td>
            <td align="left"  style="border-right-width: 0px;border-top-width: 0px;"><%# FormatCurrency(Container.DataItem("InvoiceNetAmnt"), 2, TriState.True, TriState.True, TriState.False)%></td>
            <td align="right"  style="border-right-width: 0px;border-top-width: 0px;"><%# FormatCurrency(Container.DataItem("VAT"), 2, TriState.True, TriState.True, TriState.False)%></td>
            <td align="right"  style="border-top-width: 0px;"><%# FormatCurrency(Container.DataItem("Total"), 2, TriState.True, TriState.True, TriState.False)%></td>
          </tr>
        </ItemTemplate>
      </asp:Repeater>
    </table>
	<asp:Panel Visible="false" runat="server" ID="pnlListingFee">
	<table width="650" border="0" cellpadding="0" cellspacing="0">
	  <tr>
		<td width="70" valign="top" align="left" ><asp:Label runat="server" ID="lblListingDate"></asp:Label></td>
		<td width="194" align="left"  style="border-right-width: 0px;"><asp:Label runat="server" ID="lblListingDesc"></asp:Label></td>
		<td width="97" align="center"  style="border-right-width: 0px">&nbsp;</td>
		<td width="77" align="right"  ><asp:Label runat="server" ID="lblListingNetPrice"></asp:Label></td>
		<td width="114" align="right" style="width:border-right-width: 0px"><asp:Label runat="server" ID="lblListingVAT"></asp:Label></td>
		<td width="98" align="right" ><asp:Label runat="server" ID="lblListingTotalPrice"></asp:Label></td>
	  </tr>
	</table>
	</asp:Panel>
	<br>
	<table border="0" cellspacing="0" width="400">
        <tr>
          <td width="330" align="right"  style=" border-bottom:0px; border-right-width:0px; "><strong>Sub-Total</strong></td>
          <td   align="right"  style=" border-bottom:0px; "><asp:Label ID="lblSubTotal" runat="server"></asp:Label>
          </td>
        </tr>
        <tr id="Tr1" runat="server" visible="false">        
          <td height="20" align="right"   style=" border-bottom:0px; border-right-width:0px; "><strong>Total VAT (%) </strong></td>
          <td  height="20" align="right"  style=" border-bottom:0px; "><asp:Label ID="lblTotalVAT" runat="server"></asp:Label>
          </td>
        </tr>
        <tr  runat="server" id="trDiscount">
        <td align="right"  style=" border-bottom:0px; border-right-width:0px; "><strong>Payment within 30 days Discount (<asp:Label ID="lblDiscountPer" runat="server">&nbsp;</asp:Label>%) </strong></td>
          <td  align="right"  style=" border-bottom:0px; "><asp:Label ID="lblDiscount" runat="server"></asp:Label></td>
        </tr>
        
        <tr runat="server" id="trVatDiscount">
          <td  height="20" align="right"   style=" border-bottom:0px; border-right-width:0px; "><strong>Total VAT (<asp:Label ID="lblTotalVatAsPerSite" runat="server">&nbsp;</asp:Label>%) </strong></td>
          <td  height="20" align="right"  style="border-bottom:0px; "><asp:Label ID="lblVATOnDiscout" runat="server"></asp:Label></td>
        </tr>
         <tr>
          <td  height="20" align="right"   style="border-right-width:0px; padding-top:10px;"><strong><asp:Label runat="server" ID="lblTotalAmountLabel" Text="Total Amount"></asp:Label> </strong></td>
          <td    align="right" style="padding-top:10px" ><span class="style6"><asp:Label ID="lblTotalAmount" runat="server"></asp:Label></span></td>
        </tr>
           <tr runat="server" id="trTotalADiscount">
          <td height="20" align="right" style="border-right-width:0px;"><strong>Payment within 30 days - Total Amount </strong></td>
          <td align="right" ><span class="style6"><asp:Label ID="lblTotalAmountWithDisc" runat="server"></asp:Label></span></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
    </table>
	</td></tr>
	<tr>
	<td></td>
	<td><table width="100%" border="0" cellpadding="0" cellspacing="0">
	 <tr>
	 <td width="50%">
    <table  border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td height="40" colspan="3"  align="left" valign="middle" class="bdr2" ><strong >OrderWork Bank Details</strong></td>
      </tr> 
      <tr valign="top">
        <td width="110" align="left"><strong>Account Name </strong></td>
        <td width="8" align="left" ><strong>:</strong></td>
        <td align="left" style="border-right:2px solid #c6c6c6;"><asp:Label ID="lblAccountName" runat="server" ></asp:Label></td>
      </tr>
      <tr valign="top">
        <td align="left"><strong>Sort-Code &nbsp;</strong></td>
        <td align="left" ><strong>:</strong></td>
        <td align="left" style="border-right:2px solid #c6c6c6;"><asp:Label ID="lblSortCode" runat="server" ></asp:Label></td>
      </tr>
      <tr valign="top">
        <td align="left"><strong>Account Number</strong></td>
        <td align="left" ><strong>:</strong></td>
        <td align="left" style="border-right:2px solid #c6c6c6;" ><asp:Label ID="lblAccountNumber" runat="server"></asp:Label></td>
      </tr>
      <tr valign="top" runat="server" visible="false">
        <td align="left"><strong>Bank</strong></td>
        <td align="left"><strong>:</strong></td>
        <td align="left" style="border-right:2px solid #c6c6c6;"><asp:Label ID="lblBankDetails" runat="server" ></asp:Label></td>
      </tr>
      <tr valign="top">
        <td align="left"><strong>Reference Number&nbsp; </strong></td>
        <td align="left"><strong>:</strong></td>
        <td align="left" style="border-right:2px solid #c6c6c6;"><asp:Label ID="lblRefNumber" runat="server"></asp:Label></td>
      </tr>
      <tr valign="top">
        <td height="30" align="left">&nbsp;</td>
        <td height="30" align="left" >&nbsp;</td>
        <td height="30" align="left">&nbsp;</td>
      </tr>
    </table></td>
    <td  valign="top" width="50%">
	 <table cellspacing="0" cellpadding="0" border="0" width="100%">
	    <tr valign="top">
		  <td height="40"  width="120px"  class="gridTextNew bdr2">&nbsp;&nbsp;</td>
		  <td width="5" class="gridTextNew bdr2">&nbsp;</td>
		  <td  class="gridTextNew bdr2">&nbsp;</td>
		</tr>
	   <tr runat="server" id="trField1" valign="top" visible="false">
	    <td style="padding-left:20px;"><b><asp:Label runat="server" id="lblField1"></asp:Label></b></td>
	    <td width="5" >:</td>
	    <td><asp:Label  runat="server" id="lblFieldValue1"></asp:Label></td>
	   </tr>
	   <tr runat="server" id="trField2" valign="top" visible="false">
	    <td style="padding-left:20px;"><b><asp:Label runat="server" id="lblField2"></asp:Label></b></td>
	    <td width="5">:</td>
	    <td><asp:Label runat="server" id="lblFieldValue2"></asp:Label></td>
	   </tr>
	   <tr runat="server" id="trField3" valign="top" visible="false">
	    <td style="padding-left:20px;"><b><asp:Label runat="server" id="lblField3"></asp:Label></b></td>
	    <td width="5">:</td>
	    <td><asp:Label runat="server" id="lblFieldValue3"></asp:Label></td>
	   </tr>
	   <tr runat="server" id="trField4" valign="top" visible="false">
	    <td style="padding-left:20px;"><b><asp:Label runat="server" id="lblField4"></asp:Label></b></td>
	    <td width="5">:</td>
	    <td><asp:Label runat="server" id="lblFieldValue4"></asp:Label></td>
	   </tr>
      </table>
	</td>
	 </tr>
	</table>
	</td>
  </tr>
  <tr>
    <td width="45" height="60" align="center">&nbsp;</td>
    <td width="650" height="60" align="center" class="bdr3 style3"><strong>Registered Office: </strong> Montpelier Chambers, 61-63 High Street South, Dunstable, Bedfordshire LU6 3SF<br>
        <strong>
        <asp:Label ID="lblRegistartionNo" runat="server"></asp:Label>
      :</strong>&nbsp;
      <asp:Label ID="lblRegistartionNoLine2" runat="server" Text=""></asp:Label>
    </td>
    <td width="45" height="60" align="center">&nbsp;</td>
  </tr>
</table>
