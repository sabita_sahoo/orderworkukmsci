<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSSpecialistsForm.ascx.vb"
    Inherits="Admin.UCMSSpecialistsForm" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>
<asp:UpdatePanel ID="UpdatePnlUserForm" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <script type="text/javascript">
            function RemoveImage() {
                //var AttachmentDisplayPath;
                //AttachmentDisplayPath = document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_hdnAttachmentDisplayPath").value + "AttachmentsUK/ContactPhoto/";
                //AttachmentDisplayPath = document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_hdnFlag").value = 1;
                //document.getElementById('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_imgProfile').setAttribute('src', AttachmentDisplayPath.toString() + 'empty_profile_pic.png');
                document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_hdnFlag").value = 0;
                document.getElementById('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_imgProfile').setAttribute('src', 'https://livedb.file.core.windows.net/attachments/OWMYUK/AttachmentsUK/ContactPhoto/empty_profile_pic.png?sv=2017-11-09&ss=bfqt&srt=sco&sp=rwdlacup&se=2019-12-31T00:45:17Z&st=2018-11-19T16:45:17Z&spr=https,http&sig=SyaW5ONn8E40Yftij25taeh57VOKDRy%2B7YV1wGZXCVY%3D');
                document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_hdnFlag").value = 1;
            }
            function EditImage() {
                //alert(document.getElementById("ctl00_ContentHolder_UCSpecialistsForm1_hdnContactId").value);
                OpenPopUp("395px", "260px", "PhotoUpload.aspx?PhotoUploadContact=" + document.getElementById("ctl00_ContentPlaceHolder1_UCSpecialistsForm1_hdnContactId").value);
            }
            function OpenPopUp(winWidth, winHeight, strUrl) {
                winObj = window.open(strUrl, 'winPop', 'toolbars=0, resizable=0, scrollbars=no, width=' + winWidth + ', height=' + winHeight)
                winObj.focus();
            }
            function ShowHideCvData(id, DivId) {
                //alert(DivId);
                if (document.getElementById(id).checked) {
                    document.getElementById(DivId).style.display = "none";
                }
                else {
                    document.getElementById(DivId).style.display = "inline";
                }
                return;
            }

            

        </script>
        <asp:Panel ID="pnlUserForm" runat="server">
            <asp:Panel ID="pnlSubmitForm" runat="server" Visible="true">
                <input type="button" id="btnFocus" name="btnFocus" class="hdnClass" runat="server" />
                <div id="divValidationMain" class="divValidation" runat="server" visible="false">
                    <div class="roundtopVal">
                        <img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner"
                            style="display: none" /></div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="15" align="center">
                                &nbsp;
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                        </tr>
                        <tr valign="middle">
                            <td width="63" align="center" valign="top">
                                <img src="Images/Icons/Validation-Alert.gif" width="31" height="31">
                            </td>
                            <td class="validationText">
                                <div id="divValidationMsg">
                                </div>
                                <asp:Label ID="lblError" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                                <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg"
                                    DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
                            </td>
                            <td width="20">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="15" align="center">
                                &nbsp;
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <div class="roundbottomVal">
                        <img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner"
                            style="display: none" /></div>
                </div>
                <div id="divMainTab">
                    <div class="paddedBox" id="divAccount" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td align="left" class="HeadingRed">
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td align="left" class="HeadingRed">
                                                <asp:Label ID="lblTitle" runat="server" />
                                            </td>
                                            <%--'OD-804 - .Net - Skills, Accreditation and Location coverage changes to make it Live--%>
                                              <%--User Profile Beta--%>
                                                   <td align="left" class="padTop17">
                                                        <asp:Panel ID="pnlContactProfileBeta" runat="server" Visible="false">
                                                              <table  runat="server" cellspacing="0" cellpadding="0" width="100%"
                                                                  border="0" style="padding: 0px 0px 16px 0px; margin: 0px;">
                                                                  <tr>
                                                                      <td valign="top">
                                                                          <table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                                                                              <tr>
                                                                                  <td width="5">
                                                                                      <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                                                  </td>
                                                                                  <td width="100" class="txtBtnImage">
                                                                                   <a href ="ContactProfileBeta.aspx" id="lnkUserProfileBeta" target="_blank">User Profile Beta</a>
                                                                                  </td>
                                                                                  <td width="5">
                                                                                      <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                                                  </td>
                                                                              </tr>
                                                                          </table>
                                                                      </td>  
                                                                         <td valign="middle">
                                                         <table cellspacing="0" cellpadding="0" width="50" bgcolor="#F0F0F0" border="0">
                                                             <tr>
                                                                 <asp:CheckBox ID="chkRecordVerifed" runat="server" Text="Record Verified" TextAlign="Right" Visible="false" /> 
                                                             </tr>
                                                         </table>
                                                     </td>
                                                                     
                                                                  </tr>
                                                              </table>
                                                            </asp:Panel>
                                                 </td>






                                            <%----            --%>
                                            <td align="left" class="padTop17">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0" style="padding: 0px 0px 16px 0px;
                                                    margin: 0px;">
                                                    <tr>
                                                        <td valign="bottom" align="right">
                                                            <table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                                                                <tr>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                                    </td>
                                                                    <td width="110" class="txtBtnImage">
                                                                        <asp:LinkButton ID="btnSaveTop" AccessKey="S" runat="server" OnClientClick="callSetFocusValidation('UCSpecialistsForm1')"
                                                                            CausesValidation="false" TabIndex="47" CssClass="txtListing"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;<u>S</u>ave Changes&nbsp;</asp:LinkButton>
                                                                    </td>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="10">
                                                        </td>
                                                        <td width="125" align="right" valign="top">
                                                            <table width="125" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                                                <tr>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                                    </td>
                                                                    <td class="txtBtnImage">
                                                                        <a id="btnResetTop" runat="server" accesskey="U" tabindex="48" class="txtListing">
                                                                            <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0"
                                                                                align="absmiddle" border="0">&nbsp;<u>U</u>ndo Changes&nbsp;</a>
                                                                    </td>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="10">
                                                        </td>
                                                        <td valign="top" align="right" width="60" runat="server" id="tdExit">
                                                            <table width="60" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                                                <tr>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                                    </td>
                                                                    <td class="txtBtnImage">
                                                                        <a id="btnCancelTop" runat="server" tabindex="49" class="txtListing">
                                                                            <img src="Images/Icons/Icon-Cancel.gif" width="11" height="10" hspace="5" vspace="0"
                                                                                align="absmiddle" border="0">&nbsp;Exit&nbsp;</a>
                                                                    </td>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div style="margin-bottom: 10px;">
                            <strong>
                                <asp:Label ID="lblContactId" runat="server"></asp:Label></strong></div>
                        <!--  Personal Info -->
                        <div class="divWorkOrder">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Personal
                                    Info</strong></div>
                            <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="formTxt" valign="bottom" align="left" width="170" height="24">
                                                    First Name <span class="bodytxtValidationMsg">*</span>
                                                    <asp:RequiredFieldValidator ID="rqFName" runat="server" ErrorMessage="Please enter First Name"
                                                        ForeColor="#EDEDEB" ControlToValidate="txtFName">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td width="168" height="24" align="left" valign="bottom" class="formTxt">
                                                    Last Name <span class="bodytxtValidationMsg">*</span>
                                                    <asp:RequiredFieldValidator ID="rqLName" runat="server" ErrorMessage="Please enter Last Name"
                                                        ForeColor="#EDEDEB" ControlToValidate="txtLName">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td width="165" height="24" align="left" valign="bottom" class="formTxt">
                                                    Job Title
                                                </td>
                                                <td width="160" valign="middle" class="formTxt" id="tdStatusLabel" runat="server">
                                                    Status
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="170">
                                                    <asp:TextBox ID="txtFName" TabIndex="12" runat="server" CssClass="formField width160"></asp:TextBox>
                                                </td>
                                                <td width="168">
                                                    <asp:TextBox ID="txtLName" TabIndex="13" runat="server" CssClass="formField width160"></asp:TextBox>
                                                </td>
                                                <td width="170">
                                                    <asp:TextBox ID="txtJobTitle" TabIndex="14" runat="server" CssClass="formField width160"></asp:TextBox>
                                                </td>
                                                <td width="160" valign="middle" class="formTxt" id="tdStatusValue" runat="server">
                                                    <asp:DropDownList ID="ddlStatus" AutoPostBack="true" TabIndex="15" runat="server"
                                                        CssClass="formField width160">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td class="formTxt" valign="bottom" style="float: right; width: 130px; margin: 0 0 0 9px;"
                                                    height="24" colspan="3" id="tdPhotoUpload" runat="server">
                                                    <asp:HiddenField runat="server" ID="hdnContactId" />
                                                    <input id="hdnAttachmentDisplayPath" runat="server" type="hidden" />
                                                    <input id="hdnFlag" runat="server" type="hidden" value="0" />
                                                 <%--   <a id="ancProfileImage" runat="server" style="cursor: pointer; float: right;" onclick="JavaScript:EditImage();">
                                                        <img style="border-width: 0px; vertical-align: top;" alt="Edit" title="Edit" src="Images/Icons/EditIssue.gif" />
                                                    </a>&nbsp;&nbsp; <a id="a1" runat="server" style="cursor: pointer; float: right;"
                                                        onclick="JavaScript:RemoveImage();">
                                                     <img style="border-width: 0px; vertical-align: top;" alt="Remove" title="Remove"
                                                            src="Images/Icons/Cancel.gif" />
                                                    </a>
                                                     --%>
                                                    

                                                     <a id="a1" runat="server" style="cursor: pointer" onclick="JavaScript:RemoveImage();">
                                                        <img style="border-width: 0px; vertical-align: top;padding-left: 85px;" alt="Remove" title="Remove" src="Images/Icons/Cancel.gif" />
                                                     </a>
                                                     <a id="ancProfileImage" runat="server" style="cursor: pointer; float: right;" onclick="JavaScript:EditImage();">
                                                        <img style="border-width: 0px; vertical-align: top;" alt="Edit" title="Edit" src="Images/Icons/EditIssue.gif" />
                                                     </a>
                                                     
                                                 
                                                    <asp:Image ID="imgProfile" ImageUrl="../OWLibrary/Images/profilepic.png" runat="server"
                                                        Style="border-radius: 50%; width: 110px; height: 110px" />
                                                    <div style="text-align: center;">
                                                        <asp:Label ID="lblFullName" runat="server" Style="font-weight: bold;"></asp:Label></div>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="175" height="20" align="left" valign="bottom" class="formTxt">
                                                    Email Address
                                                    <asp:RegularExpressionValidator ID="regularEmail" runat="server" ErrorMessage="Please enter Email Address in correct format"
                                                        ForeColor="#EDEDEB" ControlToValidate="txtEmail" ValidationExpression="^.+@[^\.].*\.[a-z]{2,}$">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td width="175" height="20" align="left" valign="bottom" class="formTxt">
                                                    Other Email Address
                                                    <asp:RegularExpressionValidator ID="regularOtherEmail" runat="server" ErrorMessage="Please enter Other Email Address in correct format"
                                                        ForeColor="#EDEDEB" ControlToValidate="txtOtherEmail" ValidationExpression="^.+@[^\.].*\.[a-z]{2,}$">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td width="175" height="20" align="left" valign="bottom" class="formTxt">
                                                    Phone
                                                </td>
                                                <td width="175" height="20" align="left" valign="bottom" class="formTxt">
                                                    Mobile
                                                </td>
                                                <td height="20" align="left" valign="bottom" class="formTxt">
                                                    Day Rate
                                                </td>
                                                <tr>
                                                </tr>
                                            </tr>
                                            <tr>
                                                <td width="175">
                                                    <asp:TextBox ID="txtEmail" TabIndex="16" runat="server" CssClass="formField width160"></asp:TextBox>
                                                </td>
                                                <td width="175">
                                                    <asp:TextBox ID="txtOtherEmail" TabIndex="17" runat="server" CssClass="formField width160"></asp:TextBox>
                                                </td>
                                                <td width="175">
                                                    <span class="formTxt">
                                                        <asp:TextBox ID="txtPhone" TabIndex="18" runat="server" CssClass="formField width160"></asp:TextBox>
                                                    </span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMobile" TabIndex="19" runat="server" CssClass="formField width160"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDayRate" TabIndex="20" runat="server" CssClass="formField width160"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="smallText">
                                                <td width="175" height="8" align="left" valign="top" class="smallText">
                                                    &nbsp;
                                                </td>
                                                <td width="175" height="8" align="left" valign="top" class="smallText">
                                                    &nbsp;
                                                </td>
                                                <td width="175" height="8" align="left" valign="top" class="smallText">
                                                    &nbsp;
                                                </td>
                                                <td height="8" align="left" valign="top" class="smallText">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="175" height="14" align="left" valign="top" class="formTxt" style="display: none;">
                                                    Fax
                                                </td>
                                                <td width="175" height="14" align="left" valign="top" class="formTxt">
                                                    Communication Preference
                                                </td>
                                                <td width="175" height="17" align="left" valign="top" class="formTxt">
                                                    Main Location<span class="bodytxtValidationMsg">*</span>
                                                    <asp:RequiredFieldValidator ID="rqddlLocation" runat="server" InitialValue="0" ErrorMessage="Please select main location."
                                                        ForeColor="#EDEDEB" ControlToValidate="ddlLocation">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="175" style="display: none;">
                                                    <asp:TextBox ID="txtFax" TabIndex="20" runat="server" CssClass="formField width160"></asp:TextBox>
                                                </td>
                                                <td width="175">
                                                    <asp:DropDownList ID="ddlCommunication" TabIndex="21" runat="server" CssClass="formField width160">
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="175">
                                                    <asp:DropDownList ID="ddlLocation" TabIndex="22" runat="server" CssClass="formField width120">
                                                    </asp:DropDownList>
                                                    <asp:ImageButton title="Add New Location" AlternateText="Add New Location" TabIndex="23"
                                                        PostBackUrl="~/SecurePages/LocationForm.aspx" ImageAlign="Middle" hspace="5"
                                                        name="btnAddLocation" ID="btnAddLocation" CausesValidation="false" ImageUrl="~/Images/icons/LocationIcon.gif"
                                                        runat="server"></asp:ImageButton>
                                                </td>
                                                <td>
                                                    <span class="formTxt">
                                                        <asp:CheckBox ID="chkIsMain" TabIndex="24" runat="server" CssClass="formTxt" Text="Is Main Contact for Location"
                                                            TextAlign="right"></asp:CheckBox>
                                                    </span>
                                                    <asp:CustomValidator ErrorMessage="A Main Contact already exists for this Location."
                                                        ID="MainContactExists" CssClass="bodytxtValidationMsg" runat="server">&nbsp;</asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="50%" border="0" cellspacing="0" cellpadding="0">
                                            <asp:Panel ID="pnlEditVehicleType" runat="server" Visible="false">
                                                <tr>
                                                    <td width="175" height="14" align="left" valign="top" class="formTxt">
                                                        Vehicle Type
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td width="175" height="14" align="left" valign="top" class="formTxt">
                                                        My Vehicle Reg No
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBoxList ID="cblVehicleTypeEdit" runat="server" CssClass="formField" Style="width: 450px;"
                                                            RepeatDirection="Horizontal">
                                                        </asp:CheckBoxList>
                                                    </td>
                                                    <td>
                                                        &nbsp;&nbsp;&nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtVehicleRegNo" runat="server" CssClass="formField width160"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </asp:Panel>
                                        </table>
                                        <table cellpadding="1" cellspacing="1">
                                            <tr>
                                                <td>
                                                    <input type="hidden" runat="server" id="hdnLocationsSelected" name="hdnLocationsSelected">
                                                    <strong>Add Location</strong><br />
                                                    <asp:ListBox ID="listLocations" title="Location" Style="margin-top: 5px" SelectionMode="Multiple"
                                                        runat="server" Width="235" Height="190px" ToolTip="Location" DataValueField="AddressID"
                                                        DataTextField="Name" DataMember="GroupLevel" CssClass="formField"></asp:ListBox>
                                                    <br>
                                                    <span style="color: #999999; font-size: 11px; line-height: 11px;">Note: Hold down CTRL
                                                        to select more than one<br>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Use Command key for
                                                        Mac </span>
                                                </td>
                                                <td>
                                                    <input type="button" title="Add" width="20" height="20" onclick="addToList('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listLocations','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listLocationsSel','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_hdnLocationsSelected','Add')"
                                                        value='>>' style="width: 35px; height: 20px; padding-bottom: 0px; margin-bottom: 0px;
                                                        vertical-align: middle; text-align: center;" title="Add an item from Left"><br />
                                                    <br />
                                                    <br />
                                                    <input type="button" title="Remove" width="20" height="20" onclick="addToList('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listLocationsSel','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listLocations','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_hdnLocationsSelected','Remove')"
                                                        value='<<' style="width: 35px; height: 20px; padding-bottom: 0px; margin-bottom: 0px"
                                                        title="Remove an item from Right">
                                                </td>
                                                <td>
                                                    <strong>Selected Locations</strong>
                                                    <br />
                                                    <asp:ListBox ID="listLocationsSel" title="Selected Locations" Style="margin-top: 5px"
                                                        runat="server" Width="235px" Height="190px" DataTextField="Name" DataValueField="AddressID"
                                                        SelectionMode="Multiple" CssClass="formField"></asp:ListBox>
                                                    <br>
                                                    <span style="color: #999999; font-size: 11px; line-height: 11px;">Note: Hold down CTRL
                                                        to select more than one<br>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Use Command key for
                                                        Mac </span>
                                                </td>
                                                <td width="20">
                                                </td>
                                                <td style="vertical-align: top">
                                                    <strong>Profile</strong>
                                                    <br />
                                                    <asp:TextBox ID="txtProfile" runat="server" Width="235px" Height="190px" TextMode="MultiLine"
                                                        CssClass="formField" Style="vertical-align: top; margin-top: 5px;" />
                                                </td>
                                            </tr>
                                        </table>
                                        <%--Poonam modified on 30/12/2015 - Task - OA-112 : OA - Add Emergency Contact fields in User detail page--%>
                                        <asp:Panel ID="pnlEmergencyContact" runat="server" Visible="false">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="175" height="20" align="left" valign="bottom" class="formTxt">
                                                        Emergency Contact First Name
                                                    </td>
                                                    <td width="175" height="20" align="left" valign="bottom" class="formTxt">
                                                        Emergency Contact Last Name
                                                    </td>
                                                    <td width="175" height="20" align="left" valign="bottom" class="formTxt">
                                                        Relationship Type
                                                    </td>
                                                    <td width="175" height="20" align="left" valign="bottom" class="formTxt">
                                                        Telephone Number
                                                    </td>
                                                    <td height="20" align="left" valign="bottom" class="formTxt">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="175">
                                                        <asp:TextBox ID="txtEmergencyFname" TabIndex="25" runat="server" CssClass="formField width160"></asp:TextBox>
                                                    </td>
                                                    <td width="175">
                                                        <asp:TextBox ID="txtEmergencyLname" TabIndex="26" runat="server" CssClass="formField width160"></asp:TextBox>
                                                    </td>
                                                    <td width="175">
                                                        <asp:DropDownList ID="ddlRelationshipType" TabIndex="27" runat="server" CssClass="formField width160">
                                                        </asp:DropDownList>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTelephoneNumber" TabIndex="28" runat="server" CssClass="formField width160"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr class="smallText">
                                                    <td width="175" height="8" align="left" valign="top" class="smallText">
                                                        &nbsp;
                                                    </td>
                                                    <td width="175" height="8" align="left" valign="top" class="smallText">
                                                        &nbsp;
                                                    </td>
                                                    <td width="175" height="8" align="left" valign="top" class="smallText">
                                                        &nbsp;
                                                    </td>
                                                    <td height="8" align="left" valign="top" class="smallText">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="175" height="14" align="left" valign="top" class="formTxt">
                                                        Name of Doctor
                                                    </td>
                                                    <td width="175" height="14" align="left" valign="top" class="formTxt">
                                                        G.P. Address
                                                    </td>
                                                    <td width="175" height="14" align="left" valign="top" class="formTxt">
                                                        G.P. Postcode
                                                    </td>
                                                    <td width="175" height="17" align="left" valign="top" class="formTxt">
                                                        G.P. Telephone Number
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="175">
                                                        <asp:TextBox ID="txtDoctorName" TabIndex="29" runat="server" CssClass="formField width160"></asp:TextBox>
                                                    </td>
                                                    <td width="175">
                                                        <asp:TextBox ID="txtGPAddress" TabIndex="30" runat="server" CssClass="formField width160"></asp:TextBox>
                                                    </td>
                                                    <td width="175">
                                                        <asp:TextBox ID="txtGPPostcode" TabIndex="31" runat="server" CssClass="formField width160"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtGPTelephoneNumber" TabIndex="32" runat="server" CssClass="formField width160"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <div class="WorkOrderBottomCurve">
                                <img src="Images/Curves/BtmLeft.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                        </div>
                        <!--  My settings -->
                        <div class="divWorkOrder">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>My settings</strong></div>
                            <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="175" height="24" align="left" valign="bottom" class="formTxt">
                                                    User Type <span class="bodytxtValidationMsg">*</span>
                                                    <asp:RequiredFieldValidator InitialValue="0" ID="rqUserType" runat="server" ErrorMessage="Please select User Type"
                                                        ForeColor="#EDEDEB" ControlToValidate="ddlUserType">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td id="tdEnableLoginTop" runat="server" width="175">
                                                    &nbsp;
                                                </td>
                                                <td width="175">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="175">
                                                    <span class="formTxt">
                                                        <asp:DropDownList ID="ddlUserType" AutoPostBack="true" TabIndex="33" runat="server"
                                                            CssClass="formField width160">
                                                        </asp:DropDownList>
                                                    </span>
                                                </td>
                                                <td id="tdEnableLogin" runat="server" width="175" align="left" valign="middle" class="formTxt">
                                                    <asp:CheckBox ID="ChkEnableLogin" TabIndex="34" runat="server" CssClass="formTxt"
                                                        Text="Enable Login" AutoPostBack="true" TextAlign="right" Checked="false"></asp:CheckBox>
                                                    <asp:CustomValidator ID="rqUserFields" CssClass="bodytxtValidationMsg" runat="server"></asp:CustomValidator>
                                                </td>
                                                <td width="175" align="left" valign="middle" class="formTxt">
                                                    <asp:CheckBox ID="ChkSubscribe" TabIndex="35" runat="server" CssClass="formTxt" Text="Subscribe to Newsletter"
                                                        TextAlign="right" Checked="false"></asp:CheckBox>
                                                    </asp:CheckBox><asp:CustomValidator ID="rqSubscribe" CssClass="bodytxtValidationMsg"
                                                        runat="server" ErrorMessage="Please enter Email Address to receive Newsletter."
                                                        OnServerValidate="rqSubscribe_ServerValidate" ClientValidationFunction="validateLoginData">*</asp:CustomValidator>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="ChkEmailVerified" TabIndex="36" runat="server" CssClass="formTxt"
                                                        Text="Email Verified" TextAlign="right" Checked="false" Visible="False"></asp:CheckBox>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr class="smallText">
                                                <td width="175" height="8" align="left" valign="top" class="smallText">
                                                    &nbsp;
                                                </td>
                                                <td width="175" height="8" align="left" valign="top" class="smallText">
                                                    &nbsp;
                                                </td>
                                                <td width="175" height="8" align="left" valign="top" class="smallText">
                                                    &nbsp;
                                                </td>
                                                <td height="8" align="left" valign="top" class="smallText">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="650" border="0" cellspacing="0" cellpadding="0">
                                            <tr id="trHideForAdmin" runat="server">
                                                <td width="175" align="left" valign="top" class="formTxt" id="trSpendLimit" runat="server">
                                                    Spend Limit
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ForeColor="#EDEDEB"
                                                        ControlToValidate="txtSpendLimit" ErrorMessage="Please enter a positive number for Spend Limit"
                                                        ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td width="175" height="17" align="left" valign="top" class="formTxt" id="tdAcceptWOLimit"
                                                    runat="server">
                                                    Accept WO Value Limit
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ForeColor="#EDEDEB"
                                                        ControlToValidate="txtAcceptWOLimit" ErrorMessage="Please enter a positive number for Accept Work Order Limit"
                                                        ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="175" valign="top" class="formTxt" id="tdTxtSpendLimit" runat="server">
                                                    <asp:TextBox ID="txtSpendLimit" TabIndex="37" runat="server" CssClass="formField width160"></asp:TextBox>
                                                </td>
                                                <td width="175" valign="top" class="formTxt" id="tdTxtAcceptWOLimit" runat="server">
                                                    <asp:TextBox ID="txtAcceptWOLimit" TabIndex="38" runat="server" CssClass="formField width160"></asp:TextBox>
                                                </td>
                                                <td width="140" id="tdChkFulfilWOs" runat="server" valign="top">
                                                    <asp:CheckBox ID="chkFulfilWOs" TabIndex="39" runat="server" AutoPostBack="true"
                                                        CssClass="formTxt" Text="Can fulfil Work Orders?" TextAlign="right" Checked="false">
                                                    </asp:CheckBox>
                                                </td>
                                                <td style="padding-left: 35px">
                                                    <asp:CheckBox ID="chkIsDeleted" TabIndex="40" runat="server" CssClass="formTxt" Text="Mark user as Deleted"
                                                        TextAlign="right" Checked="false" Visible="false"></asp:CheckBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="650" style="margin-top: 8px;" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="175" align="left" valign="top" class="formTxt" id="IphoneTdText" runat="server">
                                                    Set iPhone PIN (4 digit number)
                                                    <asp:RegularExpressionValidator ControlToValidate="txtIphonePin" ErrorMessage="IPhone Pin must contain 4 numeric characters."
                                                        ForeColor="#EDEDEB" ID="RegExpreIphonePin" runat="server" ValidationExpression="^\d{4}$">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="175" valign="top" class="formTxt" id="IphoneTdTextBox" runat="server">
                                                    <asp:TextBox ID="txtIphonePin" MaxLength="4" TabIndex="32" runat="server" CssClass="formField width150"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkRecvNotif" TabIndex="30" runat="server" CssClass="formTxt" Text="CC: me into other users WO notifications"
                                                        TextAlign="right"></asp:CheckBox>
                                                    <asp:CustomValidator ID="ReceiveNotifAddr" ErrorMessage="The user does not have access to the site, thus cannot have Receive WO notification checked."
                                                        CssClass="bodytxtValidationMsg" runat="server">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="pnlEnableDisableLogin" runat="server" Visible="true">
                                            <!--  Login Info -->
                                            <div class="divWorkOrder" style="margin: 20px 0px 0px 0px;">
                                                <div class="WorkOrderTopCurve">
                                                    <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Login
                                                        Info</strong></div>
                                                <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                                    <tr>
                                                        <td>
                                                            <table height="18" cellspacing="0" cellpadding="0" border="0" id="tblLoginInfo" runat="server">
                                                                <tr valign="top">
                                                                    <td class="formTxt" valign="bottom" align="left" height="24" colspan="2">
                                                                        Old Password
                                                                        <asp:RegularExpressionValidator ControlToValidate="txtOldPassword" ErrorMessage="Password must contain 8 or more alphanumeric characters."
                                                                            ForeColor="#EDEDEB" ID="rgOldPassword" runat="server" ValidationExpression="[0-9a-zA-Z]{8,}">*</asp:RegularExpressionValidator>
                                                                        <asp:CustomValidator ID="custChangePassword" runat="server" ErrorMessage="Please enter your old Password"
                                                                            ForeColor="#EDEDEB">*</asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td align="left" height="24" colspan="2">
                                                                        <asp:TextBox ID="txtOldPassword" TabIndex="31" runat="server" CssClass="formField width150"
                                                                            TextMode="Password"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td class="formTxt" align="left" width="160" height="14">
                                                                        New Password
                                                                        <asp:CustomValidator ID="custPassword" runat="server" ErrorMessage="Please enter your Password"
                                                                            ForeColor="#EDEDEB" ControlToValidate="txtPassword">*</asp:CustomValidator>
                                                                        <asp:RegularExpressionValidator ControlToValidate="txtPassword" ErrorMessage="Password must contain 8 or more alphanumeric characters."
                                                                            ForeColor="#EDEDEB" ID="rgPassword" runat="server" ValidationExpression="[0-9a-zA-Z]{8,}">*</asp:RegularExpressionValidator>
                                                                    </td>
                                                                    <td class="formTxt" align="left" width="310" height="14">
                                                                        Confirm Password
                                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Password does not match, please reconfirm"
                                                                            ControlToValidate="txtPassword" ControlToCompare="txtConfirmPassword">*</asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td align="left" height="24">
                                                                        <asp:TextBox ID="txtPassword" TabIndex="32" runat="server" CssClass="formField width150"
                                                                            TextMode="Password"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" height="24">
                                                                        <asp:TextBox ID="txtConfirmPassword" TabIndex="33" runat="server" CssClass="formField width150"
                                                                            TextMode="Password"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table id="tblEmail" height="18" cellspacing="0" cellpadding="0" width="100%" border="0"
                                                                runat="server">
                                                                <tr valign="top">
                                                                    <td class="formTxt" align="left" height="14">
                                                                        New Email Address
                                                                        <asp:RegularExpressionValidator ID="regularNewEmail" runat="server" ErrorMessage="Please enter New Email Address in correct format"
                                                                            ForeColor="#EDEDEB" ControlToValidate="txtNewEmail" ValidationExpression="^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,}$">*</asp:RegularExpressionValidator>
                                                                    </td>
                                                                    <td height="14" align="left" class="formTxt">
                                                                        Confirm Email Address
                                                                        <asp:RegularExpressionValidator ID="rgConfirmEmail" runat="server" ControlToValidate="txtEmailConfirm"
                                                                            ErrorMessage="Please enter Confirm Email Address in correct format" ValidationExpression="^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,}$">*</asp:RegularExpressionValidator>
                                                                        <asp:CustomValidator ID="custNewEmail" ClientValidationFunction="isNewEmailEntered"
                                                                            runat="server" OnServerValidate="custNewEmail_ServerValidate" ErrorMessage="Please enter confirm email address"
                                                                            ForeColor="#EDEDEB">*</asp:CustomValidator>
                                                                        <asp:CompareValidator ID="conEmails" runat="server" ErrorMessage="Email IDs are not same"
                                                                            ForeColor="#EDEDEB" ControlToCompare="txtNewEmail" ControlToValidate="txtEmailConfirm">*</asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td width="160" align="left" height="24">
                                                                        <asp:TextBox ID="txtNewEmail" TabIndex="34" runat="server" CssClass="formField width150"></asp:TextBox>
                                                                    </td>
                                                                    <td height="24" align="left">
                                                                        <asp:TextBox ID="txtEmailConfirm" TabIndex="35" runat="server" CssClass="formField width150"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="formTxt" width="311">
                                                                        Security Question <span class="bodytxtValidationMsg">*</span>
                                                                    </td>
                                                                    <td width="9">
                                                                    </td>
                                                                    <td class="formTxt">
                                                                        Security Answer <span class="bodytxtValidationMsg">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="311" height="20" valign="top">
                                                                        <asp:DropDownList ID="ddlQuestion" TabIndex="36" runat="server" CssClass="formField width311">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="rqddlQuestion" runat="server" ErrorMessage="Please select Security Question"
                                                                            ForeColor="#EDEDEB" ControlToValidate="ddlQuestion">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td width="12">
                                                                    </td>
                                                                    <td height="20" valign="top">
                                                                        <asp:TextBox ID="txtAnswer" TabIndex="37" runat="server" CssClass="formField width160"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rqtxtAnswer" runat="server" ErrorMessage="Please enter Security Answer."
                                                                            ForeColor="#EDEDEB" ControlToValidate="txtAnswer">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <div class="WorkOrderBottomCurve">
                                <img src="Images/Curves/BtmLeft.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                        </div>
                        <asp:Panel ID="pnlEnggRates" runat="server">
                            <div class="divWorkOrder">
                                <div class="WorkOrderTopCurve">
                                    <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Engineer�s
                                        rates</strong></div>
                                <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                    <tr>
                                        <td>
                                            <%@ register tagprefix="UCEnggRate1" tagname="UCEnggRate" src="~/UserControls/UK/UCEngineerRate.ascx" %>
                                            <UCEnggRate1:UCEnggRate ID="UCEnggRateInfo" runat="server"></UCEnggRate1:UCEnggRate>
                                        </td>
                                    </tr>
                                </table>
                                <div class="WorkOrderBottomCurve">
                                    <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                                        style="display: none" /></div>
                            </div>
                        </asp:Panel>
                        <!-- Supplier Region -->
                        <asp:Panel ID="pnlAreaOfExpertise" runat="server">
                            <!-- Area of Expertise	-->
                            <div class="divWorkOrder">
                                <div class="WorkOrderTopCurve">
                                    <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Area
                                        of Expertise</strong></div>
                                <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="100%" border="0" id="tblAreaOfExpertise"
                                                runat="server">
                                                <tr valign="top">
                                                    <td align="left">
                                                        <div id="divAOE" class="divAttachment" runat="server">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="padRight20Left20 formTxt">
                                                                        <%@ register tagprefix="uc1" tagname="UCAOE" src="~/UserControls/Admin/UCTableAOE.ascx" %>
                                                                        <uc1:UCAOE ID="UCAOE1" runat="server"></uc1:UCAOE>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <div class="WorkOrderBottomCurve">
                                    <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                                        style="display: none" /></div>
                            </div>
                            <!--  My SkillSet -->
                            <div class="divWorkOrder">
                                <div class="WorkOrderTopCurve">
                                    <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>My SkillSet</strong></div>
                                <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                <tr class="paddingT10B4">
                                                    <td valign="middle" align="left" height="24" class="formTxt">
                                                        <b>Please enter the following information if this user should function as a specialist
                                                        </b>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                <tr valign="top">
                                                    <td class="formTxt" align="left" width="175" height="14">
                                                        Distance to Travel
                                                    </td>
                                                    <td class="formTxt" align="left" width="152" height="14">
                                                        &nbsp;
                                                    </td>
                                                    <td class="formTxt" align="left" height="14">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td align="left" height="24">
                                                        <asp:DropDownList ID="ddlTravelDistance" TabIndex="39" runat="server" CssClass="formField width160">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" height="24">
                                                        &nbsp;
                                                    </td>
                                                    <td align="left" height="24">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--this is for OW and DE-->
                                            <asp:Panel ID="pnlAccreditationsInfoOthers" runat="server" Visible="False">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0" id="tblCertQual" runat="server">
                                                    <tr>
                                                        <td valign="bottom" align="left">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td align="left" width="202" height="24">
                                                            <asp:Label ID="lblAccre" runat="server" Text="Technical Expertise / Accreditations & keywords"
                                                                CssClass="HeadingSmallBlack"> </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td align="left">
                                                            <div class="divAttachment" runat="server">
                                                                <%--<table width="90%" cellpadding="0" cellspacing="0" border="0" >
																	<tr>
																		<td  class="formTxt"> <%@ Register TagPrefix="uc1" TagName="UCAccreds" Src="~/UserControls/UK/UCMSContactAccredsUK.ascx" %>
																						 <uc1:UCAccreds id="UCAccreds1" runat="server"></uc1:UCAccreds>  
																					
																	</td>
																	</tr>
																	</table>  --%>
                                                                <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formTxt">
                                                                            <%@ register tagprefix="uc1" tagname="UCAccreds" src="~/UserControls/Admin/UCAdminAccreditations.ascx" %>
                                                                            <uc1:UCAccreds ID="UCAccreditations" runat="server"></uc1:UCAccreds>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <!--this is for SF-->
                                            <asp:Panel ID="pnlAccreditationsInfo" runat="server" Visible="False">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr class="subHeading">
                                                        <td valign="middle" align="left" height="30" class="formTxt">
                                                            <b>Accreditations Info </b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="left" height="18">
                                                            <a style="cursor: hand;" class="footerTxtSelected" onmouseout="javascript:hideDiv('ifrmMSAccr')"
                                                                onmouseover="javascript:showDiv('ifrmMSAccr')">Information for Validating Microsoft
                                                                Accreditations</a>
                                                            <iframe frameborder="0" scrolling="no" style="overflow: hidden; visibility: hidden;
                                                                position: absolute; width: 0px; height: 0px;" id="ifrmMSAccr" src="../MSAccrHelpText.htm">
                                                            </iframe>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr valign="top">
                                                        <td width="175" height="14" align="left" valign="top" class="formTxt">
                                                            Transcript ID
                                                        </td>
                                                        <td width="175" height="14" align="left" valign="top" class="formTxt">
                                                            Access Code
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td width="175">
                                                            <asp:TextBox ID="txtTranscriptId" TabIndex="40" MaxLength="6" runat="server" CssClass="formField width160"></asp:TextBox>
                                                        </td>
                                                        <td width="175">
                                                            <asp:TextBox ID="txtAccessCode" TabIndex="41" runat="server" CssClass="formField width160"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <span class="formTxt">
                                                    <asp:RegularExpressionValidator ControlToValidate="txtTranscriptId" ErrorMessage="Transcript ID must be 6 digit numeric value"
                                                        ID="regTranscriptID" runat="server" ValidationExpression="[0-9]{6,6}" ForeColor="#EDEDEB">*</asp:RegularExpressionValidator>
                                                </span>
                                                <table cellspacing="0" cellpadding="0" width="474" border="0">
                                                    <tr valign="top">
                                                        <td class="formTxt" align="left" width="202" height="14">
                                                            Non Microsoft Accreditations
                                                        </td>
                                                        <td class="formTxt" align="left" height="14">
                                                            &nbsp;
                                                        </td>
                                                        <td class="formTxt" align="left" width="202" height="14">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellspacing="0" cellpadding="0" width="474" border="0">
                                                    <tr valign="top">
                                                        <td>
                                                            <%@ register tagprefix="uc1" tagname="UCDualListBox" src="~/UserControls/UK/UCDualListBox.ascx" %>
                                                            <uc1:UCDualListBox ID="UCNonMicroAccred" runat="server"></uc1:UCDualListBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                                <div class="WorkOrderBottomCurve">
                                    <img src="Images/Curves/BtmLeft.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="PanelVetting" runat="server">
                            <div class="divWorkOrder">
                                <div class="WorkOrderTopCurve">
                                    <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Personal
                                        Vetting</strong></div>
                                <div id="divVetting" class="divAttachment">
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr valign="top">
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td class="formTxt" align="left">
                                                <p>
                                                    To help us to better find you work can you provide us with a copy of the following
                                                    documentation in either of the following formats i.e. DOC, JPEG or PDF</p>
                                                <p>
                                                    <b>Please note that when filling in a section all parts will need to be completed.<br />
                                                        e.g. when filling in a Disclosure and Barring Service check, you will need to select
                                                        the correct disclosure, enter the issue date and enter the disclosure number.</b></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td class="smallText borderWhite1R">
                                                            <span class="formTxt">
                                                                <asp:CustomValidator ID="AttachmentValidatorInsurance" runat="server" ErrorMessage="Only PDF, JPEG and JPG files can be uploaded.">*</asp:CustomValidator>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- VALIDATION DONE -->
                                                <!--  	style="visibility:hidden; overflow:hidden; position:absolute"	 -->
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 10px;">
                                                    <tr>
                                                        <td align="left" width="420">
                                                            <b>CV</b>
                                                            <asp:CheckBox ID="chkIsCVRequired" TabIndex="28" runat="server" CssClass="formTxt"
                                                                Text="CV Not Required" TextAlign="right" Checked="false" onclick="javascript:return ShowHideCvData(this.id, 'ctl00_ContentPlaceHolder1_UCSpecialistsForm1_DivCvAttachmnet');">
                                                            </asp:CheckBox>
                                                            &nbsp;&nbsp;<asp:CheckBox ID="chkVerifiedCV" TabIndex="42" runat="server" CssClass="formTxt"
                                                                Text="Verified" TextAlign="right"></asp:CheckBox>
                                                        </td>
                                                        <td align="left" width="250">
                                                            <b>Proof of meeting</b><asp:CheckBox ID="chkVerifiedPOM" TabIndex="43" runat="server"
                                                                CssClass="formTxt" Text="Verified" TextAlign="right"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="Top">
                                                            <div id="DivCvAttachmnet" runat="server">
                                                                <uc2:UCFileUpload ID="UCFileUpload1" runat="server" Control="UCSpecialistsForm1_UCFileUpload1"
                                                                    Type="CV" AttachmentForSource="CV" ExistAttachmentSource="CV" ShowNewAttach="True"
                                                                    ShowUpload="True" MaxFiles="1"></uc2:UCFileUpload>
                                                            </div>
                                                        </td>
                                                        <td width="300" align="left" valign="Top">
                                                            <uc2:UCFileUpload ID="UCFileUpload2" runat="server" Control="UCSpecialistsForm1_UCFileUpload2"
                                                                Type="ProofOfMeeting" AttachmentForSource="ProofOfMeeting" ExistAttachmentSource="ProofOfMeeting"
                                                                ShowNewAttach="True" ShowUpload="True" MaxFiles="1"></uc2:UCFileUpload>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0" style="padding-left: 10px;
                                                    margin-top: 20px;">
                                                    <tr valign="bottom" class="formTxt">
                                                        <td width="350" height="24" class="formTxt">
                                                            <b>Disclosure and Barring Service Checks</b><asp:CheckBox ID="chkVerifiedCRC" TabIndex="44"
                                                                runat="server" CssClass="formTxt" Text="Verified" TextAlign="right"></asp:CheckBox>
                                                        </td>
                                                        <td height="24" width="250" class="formTxt">
                                                            <b>UK Security Clearance</b><asp:CheckBox ID="chkVerifiedUSC" TabIndex="45" runat="server"
                                                                CssClass="formTxt" Text="Verified" TextAlign="right"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr align="left">
                                                        <td height="20" valign="top">
                                                            <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedEnhanced" TabIndex="56"
                                                                runat="server" GroupName="CRBChecked" onclick="Javascript:ShowVettingListBox('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_tblCRBListbox','Yes');"
                                                                Text="Enhanced" TextAlign="right"></asp:RadioButton>
                                                            <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedYes" TabIndex="56" runat="server"
                                                                GroupName="CRBChecked" onclick="Javascript:ShowVettingListBox('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_tblCRBListbox','Yes');"
                                                                Text="Basic" TextAlign="right"></asp:RadioButton>
                                                            <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedNo" TabIndex="57" runat="server"
                                                                GroupName="CRBChecked" onclick="Javascript:ShowVettingListBox('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_tblCRBListbox','No');"
                                                                Text="No" TextAlign="right"></asp:RadioButton>
                                                        </td>
                                                        <td valign="top" height="20">
                                                            <asp:RadioButton class="radformFieldPadT0" ID="UKSecurityClearanceYes" TabIndex="58"
                                                                runat="server" GroupName="UKSecurityClearance" onclick="Javascript:ShowVettingListBox('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_tblSecuCheckListbox','Yes');"
                                                                Text="Yes" TextAlign="right"></asp:RadioButton>
                                                            <asp:RadioButton class="radformFieldPadT0" ID="UKSecurityClearanceNo" TabIndex="59"
                                                                runat="server" GroupName="UKSecurityClearance" onclick="Javascript:ShowVettingListBox('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_tblSecuCheckListbox','No');"
                                                                Text="No" TextAlign="right"></asp:RadioButton>
                                                        </td>
                                                    </tr>
                                                    <tr align="left">
                                                        <td height="20" valign="top">
                                                            <table cellpadding="1" cellspacing="1" id="tblCRBListbox" runat="server" width="320"
                                                                style="display: none;">
                                                                <tr valign="Center" class="formTxt">
                                                                    <td width="150" height="24" class="formTxt">
                                                                        Upload Document :
                                                                        <br />
                                                                        <uc2:UCFileUpload ID="UCFileUpload7" runat="server" Control="UCSpecialistsForm1_UCFileUpload7"
                                                                            Type="DBS" AttachmentForSource="DBS" ExistAttachmentSource="DBS" ShowNewAttach="True"
                                                                            ShowUpload="True" MaxFiles="1"></uc2:UCFileUpload>
                                                                    </td>
                                                                </tr>
                                                                <tr style="display: none;">
                                                                    <td>
                                                                        <input type="hidden" runat="server" id="hdnCriminalRecordSelected" name="hdnCriminalRecordSelected">
                                                                        <asp:ListBox ID="listCriminalRecord" title="Criminal Record Checks" Style="margin-top: 5px"
                                                                            SelectionMode="Multiple" runat="server" Width="160" Height="50px" ToolTip="Criminal Record"
                                                                            DataValueField="StandardID" DataTextField="StandardValue" DataMember="GroupLevel"
                                                                            CssClass="formField"></asp:ListBox>
                                                                    </td>
                                                                    <td>
                                                                        <input type="button" title="Add" width="20" height="20" onclick="addToList('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listCriminalRecord','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listCriminalRecordSel','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_hdnCriminalRecordSelected','Add')"
                                                                            value='+' style="width: 20px; height: 20px; padding-bottom: 0px; margin-bottom: 0px;
                                                                            vertical-align: middle; text-align: center;" title="Add an item from Left"><br />
                                                                        <input type="button" title="Remove" width="20" height="20" onclick="addToList('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listCriminalRecordSel','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listCriminalRecord','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_hdnCriminalRecordSelected','Remove')"
                                                                            value='-' style="width: 20px; height: 20px; padding-bottom: 0px; margin-bottom: 0px"
                                                                            title="Remove an item from Right">
                                                                    </td>
                                                                    <td>
                                                                        <asp:ListBox ID="listCriminalRecordSel" title="Selected Criminal Records" Style="margin-top: 5px"
                                                                            runat="server" Width="160" Height="50px" DataTextField="StandardValue" DataValueField="StandardID"
                                                                            SelectionMode="Multiple" CssClass="formField"></asp:ListBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="display: none;">
                                                                    <td colspan="3">
                                                                        <div style="color: #999999; font-size: 11px; line-height: 11px;">
                                                                            Note: To add a disclosure, please add the relevent item(s) to the right hand side.</div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td height="20" valign="top">
                                                            <table cellpadding="1" cellspacing="1" id="tblSecuCheckListbox" runat="server" width="230"
                                                                style="display: none;">
                                                                <tr>
                                                                    <td>
                                                                        <input type="hidden" runat="server" id="hdnSecuCheckSelected" name="hdnSecuCheckSelected">
                                                                        <asp:ListBox ID="listSecurityCheck" title="Security Checks" Style="margin-top: 5px"
                                                                            SelectionMode="Multiple" runat="server" Width="100" Height="50px" ToolTip="UK Security Clearance"
                                                                            DataValueField="StandardID" DataTextField="StandardValue" DataMember="GroupLevel"
                                                                            CssClass="formField"></asp:ListBox>
                                                                    </td>
                                                                    <td>
                                                                        <input type="button" title="Add" width="20" height="20" onclick="addToList('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listSecurityCheck','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listSecurityCheckSel','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_hdnSecuCheckSelected','Add')"
                                                                            value='+' style="width: 20px; height: 20px; padding-bottom: 0px; margin-bottom: 0px;
                                                                            vertical-align: middle; text-align: center;" title="Add an item from Left"><br />
                                                                        <input type="button" title="Remove" width="20" height="20" onclick="addToList('ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listSecurityCheckSel','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_listSecurityCheck','ctl00_ContentPlaceHolder1_UCSpecialistsForm1_hdnSecuCheckSelected','Remove')"
                                                                            value='-' style="width: 20px; height: 20px; padding-bottom: 0px; margin-bottom: 0px"
                                                                            title="Remove an item from Right">
                                                                    </td>
                                                                    <td>
                                                                        <asp:ListBox ID="listSecurityCheckSel" title="Selected Security Checks" Style="margin-top: 5px"
                                                                            runat="server" Width="100" Height="50px" DataTextField="StandardValue" DataValueField="StandardID"
                                                                            SelectionMode="Multiple" CssClass="formField"></asp:ListBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <span style="color: #999999; font-size: 11px; line-height: 11px;">Note: To add a clearance,
                                                                            please move the item(s) to the right hand side.</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 10px;">
                                                    <tr>
                                                        <td width="350" height="25" align="left" valign="Top" class="formTxt">
                                                            <div id="DivCRBCheckCalender" runat="server">
                                                                Certificate Expiry Date
                                                                <br />
                                                                <asp:TextBox ID="txtDBSExpiryDate" runat="server" TabIndex="38" CssClass="formField width177"
                                                                    Text="Enter expiry date" onfocus="clearExpiryDate(this);" onblur="fillExpiryDate(this)"></asp:TextBox>
                                                                <img alt="Click to Select" src="Images/calendar.gif" id="btnCRBCheckDate" style="cursor: pointer;
                                                                    vertical-align: middle;" />
                                                                <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnCRBCheckDate"
                                                                    TargetControlID="txtDBSExpiryDate" ID="calCRBCheckDate" runat="server">
                                                                </cc1:CalendarExtender>
                                                                <asp:RegularExpressionValidator ID="regCRBCheckDate" ControlToValidate="txtDBSExpiryDate"
                                                                    ErrorMessage="DBS Certificate Expiry Date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB"
                                                                    ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                                    Enabled="True" runat="server">*</asp:RegularExpressionValidator>
                                                             <%--   <asp:CustomValidator Display="none" ID="rngDate" runat="server" ErrorMessage="DBS Expiry Date should be greater than Today's Date"
                                                                    ForeColor="#EDEDEB" ControlToValidate="txtDBSExpiryDate">*</asp:CustomValidator>--%>
                                                                     <asp:CustomValidator ID="rngDate" ErrorMessage="DBS Expiry Date should be greater than Today's Date"
                                                                        CssClass="bodytxtValidationMsg" Display= "None" runat="server"></asp:CustomValidator>
                                                            </div>
                                                        </td>
                                                        <td align="left" width="250">
                                                            <div id="DivUKSecuCalender" runat="server" style="display: none;">
                                                                Date of issue
                                                                <br />
                                                                <asp:TextBox ID="txtUKSecuDate" runat="server" TabIndex="40" CssClass="formField width177"
                                                                    Text="Date of issue" onfocus="clearDateOfIssue(this)" onblur="fillDateOfIssue(this)"></asp:TextBox>
                                                                <img alt="Click to Select" src="Images/calendar.gif" id="btnUKSecuDate" style="cursor: pointer;
                                                                    vertical-align: middle;" />
                                                                <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnUKSecuDate"
                                                                    TargetControlID="txtUKSecuDate" ID="calUKSecuDate" runat="server">
                                                                </cc1:CalendarExtender>
                                                                <asp:RegularExpressionValidator ID="regUKSecuDate" ControlToValidate="txtUKSecuDate"
                                                                    ErrorMessage="UK Security Cearance date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB"
                                                                    ValidationExpression="(^(((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3}))$)"
                                                                    runat="server">*</asp:RegularExpressionValidator>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" runat="server" border="0" cellspacing="0" cellpadding="0" style="padding-top: 10px;">
                                                    <tr>
                                                        <td width="350" height="25" align="left" valign="Top" class="formTxt" style="padding-left: 10px;">
                                                            <div id="DivCRBCheckFileUpload" runat="server">
                                                                Certificate Number
                                                                <br />
                                                                <asp:TextBox ID="txtDisclosureNumber" runat="server" TabIndex="40" MaxLength="100"
                                                                    CssClass="formField width177"></asp:TextBox>
                                                            </div>
                                                        </td>
                                                        <td width="250" align="left" valign="Top">
                                                            <div id="DivUKSecuFileUpload" runat="server" style="display: none">
                                                                <uc2:UCFileUpload ID="UCFileUpload5" runat="server" Control="UCSpecialistsForm1_UCFileUpload5"
                                                                    Type="UKSecurityClearance" AttachmentForSource="UKSecurityClearance" ExistAttachmentSource="UKSecurityClearance"
                                                                    ShowNewAttach="True" ShowUpload="True" MaxFiles="1"></uc2:UCFileUpload>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 10px;
                                                    margin-top: 20px;">
                                                    <tr>
                                                        <td align="left" width="350">
                                                            <b>Right to work in UK (Passport/Visa/Residents Permit)</b><asp:CheckBox ID="chkVerifiedRWUK"
                                                                TabIndex="24" runat="server" CssClass="formTxt" Text="Verified" TextAlign="right">
                                                            </asp:CheckBox><br />
                                                            <br />
                                                            <span class="formTxt">Passport Type</span>
                                                            <asp:RadioButton class="radformFieldPadT0" ID="rdoUK" TabIndex="56" runat="server"
                                                                Checked="true" GroupName="rdoPassportType" Text="UK" TextAlign="right"></asp:RadioButton>
                                                            <asp:RadioButton class="radformFieldPadT0" ID="rdoEU" TabIndex="56" runat="server"
                                                                GroupName="rdoPassportType" Text="EU" TextAlign="right"></asp:RadioButton>
                                                            <asp:RadioButton class="radformFieldPadT0" ID="rdoOthers" TabIndex="56" runat="server"
                                                                GroupName="rdoPassportType" Text="Others" TextAlign="right"></asp:RadioButton>
                                                            <br />
                                                            <br />
                                                            <span class="formTxt">Expiry Date</span>
                                                            <br />
                                                            <asp:TextBox ID="txtVisaExpDate" runat="server" TabIndex="42" CssClass="formField"
                                                                Text="Enter expiry date" onfocus="clearExpiryDate(this)" onblur="fillExpiryDate(this)"></asp:TextBox>
                                                            <img alt="Click to Select" src="Images/calendar.gif" id="btnVisaExpDate" style="cursor: pointer;
                                                                vertical-align: middle;" />
                                                            <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnVisaExpDate"
                                                                TargetControlID="txtVisaExpDate" ID="calVisaExpDate" runat="server">
                                                            </cc1:CalendarExtender>
                                                            <asp:RegularExpressionValidator ID="regVisaExpDate" ControlToValidate="txtVisaExpDate"
                                                                ErrorMessage="Visa Expiry Date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB"
                                                                ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                                runat="server">*</asp:RegularExpressionValidator>
                                                        </td>
                                                        <td align="left" width="250">
                                                            <b>CSCS</b><asp:CheckBox ID="chkVerifiedCSCS" TabIndex="24" runat="server" CssClass="formTxt"
                                                                Text="Verified" TextAlign="right"></asp:CheckBox><br />
                                                            <asp:RadioButton class="radformFieldPadT0" ID="CSCSYes" TabIndex="59" runat="server"
                                                                GroupName="CSCS" onclick="Javascript:ShowVettingListBox('','Yes');" Text="Yes"
                                                                TextAlign="right"></asp:RadioButton>
                                                            <asp:RadioButton class="radformFieldPadT0" ID="CSCSNo" TabIndex="60" runat="server"
                                                                GroupName="CSCS" onclick="Javascript:ShowVettingListBox('','No');" Text="No"
                                                                TextAlign="right"></asp:RadioButton>
                                                            <br />
                                                            <div id="DivCSCSCalender" runat="server" style="display: none;">
                                                                <span class="formTxt">Expiry Date</span><br />
                                                                <asp:TextBox ID="txtCSCSDate" runat="server" TabIndex="42" CssClass="formField width177"
                                                                    Text="Enter expiry date" onfocus="clearExpiryDate(this)" onblur="fillExpiryDate(this)"></asp:TextBox>
                                                                <img alt="Click to Select" src="Images/calendar.gif" id="btnCSCSDate" style="cursor: pointer;
                                                                    vertical-align: middle;" />
                                                                <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnCSCSDate"
                                                                    TargetControlID="txtCSCSDate" ID="calCSCSDate" runat="server">
                                                                </cc1:CalendarExtender>
                                                                <asp:RegularExpressionValidator ID="regCSCSDate" ControlToValidate="txtCSCSDate"
                                                                    ErrorMessage="CSCS Date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB" ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                                    runat="server">*</asp:RegularExpressionValidator>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr valign="bottom" class="formTxt">
                                                        <td height="24" class="formTxt">
                                                            Date of Birth
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20" valign="top" style="float: left;">
                                                            <asp:TextBox ID="txtDOB" runat="server" TabIndex="43" CssClass="formField"></asp:TextBox>
                                                            <img alt="Click to Select" src="Images/calendar.gif" id="btntxtDOB" style="cursor: pointer;
                                                                vertical-align: Top;" />
                                                            <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btntxtDOB"
                                                                TargetControlID="txtDOB" ID="CalendartxtDOB" runat="server">
                                                            </cc1:CalendarExtender>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="Vetting"
                                                                ControlToValidate="txtDOB" ErrorMessage="Please Enter Valid Date of Birth" ValidationExpression="^[^<>]+$"
                                                                ForeColor="#EDEDEB">*</asp:RegularExpressionValidator>
                                                            <asp:RegularExpressionValidator ID="regtxtDOB" ControlToValidate="txtDOB" ErrorMessage="Date of Birth should be in DD/MM/YYYY Format"
                                                                ForeColor="#EDEDEB" ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                                runat="server">*</asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table id="Table1" width="100%" runat="server" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" valign="Top" width="350">
                                                            <uc2:UCFileUpload ID="UCFileUpload3" runat="server" Control="UCSpecialistsForm1_UCFileUpload3"
                                                                Type="PassportVisa" AttachmentForSource="PassportVisa" ExistAttachmentSource="PassportVisa"
                                                                ShowNewAttach="True" ShowUpload="True" MaxFiles="3"></uc2:UCFileUpload>
                                                        </td>
                                                        <td align="left" valign="Top" width="250">
                                                            <div id="DivCSCSFileUpload" runat="server" style="display: none;">
                                                                <uc2:UCFileUpload ID="UCFileUpload6" runat="server" Control="UCSpecialistsForm1_UCFileUpload6"
                                                                    Type="CSCS" AttachmentForSource="CSCS" ExistAttachmentSource="CSCS" ShowNewAttach="True"
                                                                    ShowUpload="True" MaxFiles="1"></uc2:UCFileUpload>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%--<div class="formTxt" style="margin-top:10px;margin-bottom:10px;padding-left:10px;">
								    <asp:CheckBox id="checkOrderworkApproved" tabIndex="24" runat="server" CssClass="formTxt" Text="Orderwork Approved" TextAlign="right" ></asp:CheckBox>								
								 </div>--%>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblVettingHistory"
                                                    style="display: none" runat="server" visible="false">
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>Vetting History</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLVettingHistory"
                                                                runat="server" Width="100%">
                                                                <HeaderTemplate>
                                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                                        <tr id="tblDGYourResponseHdr" align="left">
                                                                            <td width="5" valign="top" style="padding: 0px; border: 0px;">
                                                                                <img src="Images/Curves/Bdr-Red-TL.gif" width="5" height="5" hspace="0" vspace="0"
                                                                                    border="0">
                                                                            </td>
                                                                            <td width="150px">
                                                                                Changed By
                                                                            </td>
                                                                            <td>
                                                                                Change
                                                                            </td>
                                                                            <td width="100" style="border-right: 0px">
                                                                                Change Date
                                                                            </td>
                                                                            <td width="5" align="right" valign="top" style="padding: 0px; border: 0px;">
                                                                                <img src="Images/Curves/Bdr-Red-TR.gif" width="5" height="5" hspace="0" vspace="0"
                                                                                    border="0">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                                        <tr id="tblDGRowInside" align="left">
                                                                            <td width="5" valign="top" style="padding: 0px; border: 0px;">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td width="150">
                                                                                <%#Container.DataItem("ChangedByName")%>
                                                                            </td>
                                                                            <td>
                                                                                <%#Container.DataItem("Change")%>
                                                                            </td>
                                                                            <td width="100" style="border-right: 0px">
                                                                                <%#Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "ChangeDate"), DateFormat.ShortDate)%>
                                                                            </td>
                                                                            <td width="5" align="right" valign="top" style="border-right-width: 0px; padding: 0px;">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="WorkOrderBottomCurve">
                                    <img src="Images/Curves/BtmLeft.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="PanelContactComment" runat="server">
                            <div class="divWorkOrder">
                                <div class="WorkOrderTopCurve">
                                    <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Comments</strong></div>
                                <div id="divAttachment" class="divAttachment">
                                    <%@ register tagprefix="ucc" tagname="UCComments" src="~/UserControls/Admin/UCComments.ascx" %>
                                    <ucc:UCComments ID="UCComments" runat="server"></ucc:UCComments>
                                </div>
                                <div class="WorkOrderBottomCurve">
                                    <img src="Images/Curves/BtmLeft.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                            </div>
                        </asp:Panel>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td align="left" class="HeadingRed">
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td align="left" class="HeadingRed">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="padTop17">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0" style="padding: 0px 0px 16px 0px;
                                                    margin: 0px;">
                                                    <tr>
                                                        <td valign="bottom" align="right">
                                                            <table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                                                                <tr>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                                    </td>
                                                                    <td width="110" class="txtBtnImage">
                                                                        <asp:LinkButton ID="btnSaveBottom" runat="server" CausesValidation="false" OnClientClick="callSetFocusValidation('UCSpecialistsForm1')"
                                                                            TabIndex="42" CssClass="txtListing"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" >&nbsp;Save Changes&nbsp;</asp:LinkButton>
                                                                    </td>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="10">
                                                        </td>
                                                        <td width="125" align="right" valign="top">
                                                            <table width="125" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                                                <tr>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                                    </td>
                                                                    <td class="txtBtnImage">
                                                                        <a id="btnResetBottom" runat="server" tabindex="43" causesvalidation="false" class="txtListing">
                                                                            <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0"
                                                                                align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</a>
                                                                    </td>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="10">
                                                        </td>
                                                        <td valign="top" align="right" width="60" runat="server" id="tdExitBottom">
                                                            <table width="60" cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                                                <tr>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                                    </td>
                                                                    <td class="txtBtnImage">
                                                                        <a id="btnCancelBottom" runat="server" tabindex="44" class="txtListing">
                                                                            <img src="Images/Icons/Icon-Cancel.gif" width="11" height="10" hspace="5" vspace="0"
                                                                                align="absmiddle" border="0">&nbsp;Exit&nbsp;</a>
                                                                    </td>
                                                                    <td width="5">
                                                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
            <!-- Confirm Action panel -->
            <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
                <table width="100%" class="padTop50" cellpadding="10" cellspacing="10">
                    <tr>
                        <td>
                            <asp:Label ID="lblConfirmMsg" CssClass="bodytxtValidationMsg" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="right">
                            <table cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                <tr>
                                    <td width="5">
                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" />
                                    </td>
                                    <td class="txtBtnImage">
                                        <asp:LinkButton ID="btnConfirm" runat="server" CausesValidation="false" class="txtListing"
                                            TabIndex="45"> <img src="Images/Icons/Icon-Confirm.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Confirm </asp:LinkButton>
                                    </td>
                                    <td width="5">
                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="10">
                            &nbsp;
                        </td>
                        <td align="left">
                            <table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
                                <tr>
                                    <td width="5">
                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" />
                                    </td>
                                    <td align="center" class="txtBtnImage">
                                        <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" class="txtListing"
                                            TabIndex="46"> <img src="Images/Icons/Icon-Cancel.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
                                    </td>
                                    <td width="5">
                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </asp:Panel>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSaveTop" EventName="Click" />
        <%--<asp:AsyncPostBackTrigger ControlID="btnResetTop" EventName="Click" />--%>
        <asp:AsyncPostBackTrigger ControlID="btnSaveBottom" EventName="Click" />
        <%--<asp:AsyncPostBackTrigger ControlID="btnResetBottom" EventName="Click" />--%>
        <asp:AsyncPostBackTrigger ControlID="btnConfirm" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
<asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePnlUserForm"
    runat="server">
    <ProgressTemplate>
        <div>
            <img align="middle" src="Images/indicator.gif" />
            Please Wait..processing data
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePnlUserForm"
    CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
