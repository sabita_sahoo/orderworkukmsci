<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSAcceptWorkRequest.ascx.vb" Inherits="Admin.UCMSAcceptWorkRequest" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!--
dont go
-->
<script language="javascript" src="JS/Scripts.js" type="text/javascript"></script>

<asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server">
<ContentTemplate>

<div id="divValidationMain" class="divValidation" runat="server" visible="false" >
<div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
  <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	  <td height="15" align="center">&nbsp;</td>
	  <td width="565" height="15">&nbsp;</td>
	  <td height="15">&nbsp;</td>
	</tr>
	<tr valign="middle">
	  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
	  <td width="565" class="validationText"><div  id="divValidationMsg"></div>
	  <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
	  <span class="validationText">
	  <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
  	  </span>
	  </td>
	  <td width="20">&nbsp;</td>
	</tr>
	<tr>
	  <td height="15" align="center">&nbsp;</td>
	  <td width="565" height="15">&nbsp;</td>
	  <td height="15">&nbsp;</td>
	</tr>
  </table>
  
  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
</div>
<!-- confirmation panel used for delete/discard/copy wo as draft-->
<asp:Panel ID="pnlConfirmation" runat="server" Visible="false">
	</asp:Panel>				  
<!-- confirmation panel end-->	
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="35" class="HeadingRed"><strong> &nbsp;Accept Work Request</strong></td>
  </tr>
  <tr>
    <td><asp:Label ID="lblMsg" runat="server" CssClass="formLabelGrey"></asp:Label></td>
  </tr>
</table>	
<table id="DGWorkOrders" width="100%" border="0" cellpadding="5" cellspacing="0">
  <tr valign="top" >
    <td  class="gridHdr gridBorderRB">WO No</td>
    <td  class="gridHdr gridBorderRB">Submitted</td>
    <td  class="gridHdr gridBorderRB">Location</td>
	<td  class="gridHdr gridBorderRB" >Client Company</td>
	<td  class="gridHdr gridBorderRB" >Client Contact</td>
	<td  class="gridHdr gridBorderRB">WO Title</td>
	<td  class="gridHdr gridBorderRB">WO Start</td>
	<td  class="gridHdr gridBorderRB">Proposed Price</td>
	<td  class="gridHdrHighlight gridBorderRB">Status</td>
  </tr>
 <tr>
    <td width="65px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label></td>
	<td width="70px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblCreated"></asp:Label></td>
	<td width="65px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblLocation"></asp:Label></td>
	<td width="90px" class="gridRow gridBorderRB" runat="server" id="tdBuyerCompany1"><asp:Label runat="server" CssClass="formTxt" ID="lblBuyerCompany"></asp:Label></td>
	<td width="90px" class="gridRow gridBorderRB" runat="server" id="tdBuyerContact1"><asp:Label runat="server" CssClass="formTxt" ID="lblBuyerContact"></asp:Label></td>
	<td width="150px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblTitle"></asp:Label></td>
	<td width="60px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOStart"></asp:Label></td>
	<td width="90px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblProposedPrice"></asp:Label></td>
	<td width="57px" class="gridRowHighlight gridBorderRB"><asp:Label runat="server" CssClass="gridRowHighlight" ID="lblStatus"></asp:Label></td>
  </tr>
</table>
 			  
<div class="divRatings" id="divWODetail">
  <div class="divAccountTopCurves"><img src="Images/Curves/MAS-Bdr-TL.gif" title="" width="5" height="5" class="corner" style="display: none" /></div>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="txtWelcome">
	<tr class="smallText">
	  <td height="8" align="left" valign="top" class="smallText">&nbsp;</td>
	  <td height="8" align="left" valign="top"  class="smallText">&nbsp;</td>
	  <td height="8" align="left" valign="top" class="smallText" >&nbsp;</td>
	  <td height="8" class="smallText">&nbsp;</td>
	</tr>
	<tr>
	  <td width="15" align="left" valign="top" >&nbsp;</td>
	  <td id="tdDGDetailedDesc" align="left" valign="top">
		<strong>Scope of Work:</strong><br>
		<asp:Label ID="lblLongDesc" runat="server"></asp:Label>
		<br>
		<br>                            
		  <asp:Panel ID="pnlSpecialInstructions" runat="server"> <strong>Special Instructions:</strong><br>
			
			  <asp:Label ID="lblSpecialInstructions" runat="server"></asp:Label>
		  </asp:Panel><br>
		  <asp:Panel ID="pnlProducts" runat="server"> <strong><asp:label runat="Server" ID="lblProductsLabel"></asp:label>:</strong><br>
		      <asp:Label ID="lblProductName" runat="server"></asp:Label>
		      <asp:TextBox ID="txtProductName" runat="server" Width="395" CssClass="formFieldGrey width635height60" Visible="false"></asp:TextBox>
		  </asp:Panel><br>
		  	  
	  </td>
	  <td width="217" align="left" valign="top" class="paddingL23"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td align="left" valign="top" class="txtOrange">WO Type </td>
				  <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
				  <td class="txtOrange" align="left" valign="top"><asp:Label ID="lblCategory" runat="server"></asp:Label></td>
				</tr>
				<tr>
				  <td align="left" valign="top" class="bodyTextGreyLeftAligned">&nbsp;</td>
				  <td align="center" valign="top" class="bodyTextGreyLeftAligned">&nbsp;</td>
				  <td class="bodyTextGreyLeftAligned" align="left" valign="top">&nbsp;</td>
				</tr>
				<tr>
				  <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned"><asp:Label runat="server" ID="lblStartDateLabel"></asp:Label></td>
				  <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
				  <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblStartDate" runat="server"></asp:Label><asp:Label ID="lblInstallTime" runat="server"></asp:Label></td>
				</tr>
				<tr runat="server" id="trEndDate">
				  <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">End Date </td>
				  <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
				  <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblEndDate" runat="server"></asp:Label></td>
				</tr>
				<tr>
				  <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">Estimated Time </td>
				  <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
				  <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblEstimatedTime" runat="server"></asp:Label></td>
				</tr>
				<tr>
				  <td class="bodyTextGreyLeftAligned" width="85" align="left" valign="top">Supply Parts </td>
				  <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">:</td>
				  <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblSupplyParts" runat="server"></asp:Label></td>
				</tr>
				<tr>
				  <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">Dress Code</td>
				  <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
				  <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblDressCode" runat="server"></asp:Label></td>
				</tr>
				<tr runat="Server" id="trSalesAgent">
				  <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">Sales Agent</td>
				  <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
				  <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblSalesAgent" runat="server"></asp:Label></td>
				</tr>
			</table></td>
		  </tr>
		  <tr>
			<td class="smallText" height="16" align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
		   <td><table width="100%" id="tblWOSpecs" runat="server" cellpadding="0" cellspacing="0">
			<tr>
				  <td class="bodyTextGreyLeftAligned" width="85" align="left" valign="top">PO Number</td>
				  <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">:</td>
				  <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblPONumber" runat="server"></asp:Label>
				  <asp:TextBox ID="txtPONumber" runat="server" CssClass="formFieldGrey width150" Visible="false"></asp:TextBox>
				  </td>
			 </tr>
			  <tr>
			<td class="smallText" height="5" align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
			<td class="smallText" height="5" align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
			<td class="smallText" height="5" align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
			<td class="smallText" height="5" align="left" valign="top">&nbsp;</td>
		  </tr>
		  	<tr runat="server" id="trJobNumber">
				  <td class="bodyTextGreyLeftAligned" width="85" align="left" valign="top">Job Number </td>
				  <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">:</td>
				  <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblJRSNumber" runat="server"></asp:Label>
   				  <asp:TextBox ID="txtJRSNumber" runat="server" CssClass="formFieldGrey width150" Visible="false"></asp:TextBox>
				  </td>
			 </tr>
			  <tr>
			<td class="smallText" height="5" align="left" valign="top">&nbsp;</td>
		  </tr>
			 <tr runat="Server" id="trLocOfGoods">
				  <td class="bodyTextGreyLeftAligned" width="85" align="left" valign="top" ><asp:label runat="server" ID="lblLocOfGoods"></asp:label></td>
				  <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top" >:</td>
				  <td class="bodyTextGreyLeftAligned" align="left" valign="top" ><asp:Label ID="lblBizDiv" runat="server"></asp:Label>
					<asp:dropdownlist ID="drpdwnGoodsCollection" runat="server" CssClass="formFieldGrey width150" Visible="false"></asp:dropdownlist>
				  </td>
			 </tr>
			  <tr>
			<td class="smallText" height="5" align="left" valign="top">&nbsp;</td>
		  </tr>			 
			</table></td>
		  </tr>
		  <tr>
			<td class="smallText" height="5" align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
			<td align="left" valign="top" ><table width="100%" id="tblWOAttachments" runat="server" cellpadding="0" cellspacing="0">
			 <tr>
			<td class="smallText" height="16" align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
			<td valign="top"  align="center" class="bodyTextGreyLeftAligned"><asp:Panel ID="pnlAttachments" runat="server">
				<asp:Literal ID="litAttachments" runat="server" />
				<asp:Literal ID="litSWAttachments" runat="server" />
				 <asp:Panel runat="server" ID="pnlSWBuyerAcceptForm" >
                                                   <%@ Register TagPrefix="uc4" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %> 
                                                  <uc4:UCFileUpload id="UCFileUpload1" runat="server" Control="UCAcceptWR1_UCFileUpload1" 
                                                    Type="StatementOfWork"  
                                                    AttachmentForSource="StatementOfWork"
                                                    ShowNewAttach="True" ShowUpload="True" 
                                                    MaxFiles="1" ></uc4:UCFileUpload>               
                                               </asp:Panel>                        
				</asp:Panel>
			</td>
		  </tr>
			</table>

			</td>
		  </tr>
	  </table></td>
	  <td width="15">&nbsp;</td>
	</tr>
	<tr class="smallText">
	  <td height="8" align="left" valign="top" class="smallText">&nbsp;</td>
	  <td height="8" align="left" valign="top"  class="smallText">&nbsp;</td>
	  <td height="8" align="left" valign="top" class="smallText" >&nbsp;</td>
	  <td height="8" class="smallText">&nbsp;</td>
	</tr>
  </table>
  <div class="divAccountBotCurves"><img src="Images/Curves/MAS-Bdr-BL.gif" title="" width="5" height="5" class="corner" style="display: none" /></div>
</div>
</asp:Panel>
<asp:Panel ID="pnlClientContactInfo" runat="server">
<div class="divRatings" id="divRatings">
  <div class="divAccountTopCurves"><img src="Images/Curves/MAS-Bdr-TL.gif" title="" width="5" height="5" class="corner" style="display: none" /></div>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0">
	<tr class="smallText">
	  <td width="15" height="8" class="smallText">&nbsp;</td>
	  <td height="8" class="smallText">&nbsp;</td>
	  <td width="15" height="8" class="smallText">&nbsp;</td>
	</tr>
	<tr>
	  <td height="20">&nbsp;</td>
	  <td height="20" valign="top"></td>
	  <td height="20">&nbsp;</td>
	</tr>
	<tr>
	  <td>&nbsp;</td>
	  <td><span class="txtOrange"><strong><asp:Label ID="lblBuyerContactLabel" runat="server"></asp:Label></strong></span>                                <br>
		<span class="bodyTextGreyLeftAligned"><strong>
		<asp:Label ID="lblClientContact" runat="server"></asp:Label>
-
<asp:Label ID="lblClientCompany" runat="server"></asp:Label>
, </strong>
			<asp:Label ID="lblClientAddress" runat="server"></asp:Label>
&nbsp;&nbsp; Tel:
<asp:Label ID="lblClientPhone" runat="server"></asp:Label>
<strong> </strong>
<strong>
</strong>
<div id="divTempContactIfo" style="visibility:hidden;position:absolute" runat="server"> 
	<span class="txtOrange"><strong>Work Order Location Info:</strong></span><span class="txtOrange">							<br></span>
	<strong><asp:Label ID="lblTempClientContact" runat="server"></asp:Label>
	: </strong><asp:Label ID="lblTempClientAddress" runat="server"></asp:Label> 
	&nbsp;&nbsp; Tel: <asp:Label ID="lblTempClientPhone" runat="server"></asp:Label>
	</div></span>
</td>
	  <td>&nbsp;</td>
	</tr>
	
	
	<tr class="smallText">
	  <td height="8" class="smallText">&nbsp;</td>
	  <td height="8" class="smallText">&nbsp;</td>
	  <td height="8" class="smallText">&nbsp;</td>
	</tr>
  </table>
  <div class="divAccountBotCurves"><img src="Images/Curves/MAS-Bdr-BL.gif" title="" width="5" height="5" class="corner" style="display: none" /></div>
</div>
</asp:Panel>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="30">&nbsp;</td>
	<td>&nbsp;</td>
	<td width="30">&nbsp;</td>
  </tr>
  <tr>
	<td>&nbsp;</td>
	<td align="right">
						
		<TABLE cellSpacing="0" cellPadding="0"  border="0" >
		  <TR>
			<TD align="right" vAlign="top">&nbsp;</TD>
			<TD align="right" vAlign="top"><TABLE cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
			  <TR>
				<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
				<TD class="txtBtnImage"><asp:Linkbutton id="lnkSubmit" runat="server" CssClass="txtListing" TabIndex="8"> <img src="Images/Icons/Submit.gif" width="14" height="13"  align="absmiddle" border="0">&nbsp;Confirm </asp:Linkbutton></TD>
				<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
			  </TR>
			</TABLE></TD>
			<TD width="10"></TD>
			<TD vAlign="top" align="left"><TABLE cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
				<TR>
				  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
				  <TD class="txtBtnImage"><asp:Linkbutton id="lnkCancel" runat="server" CssClass="txtListing" TabIndex="8"> <img src="Images/Icons/Cancel.gif" width="11" height="11"  align="absmiddle" border="0">&nbsp;Cancel&nbsp;</asp:Linkbutton></TD>
				  <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
				</TR>
			</TABLE></TD>       						  
		  </TR>
	  </TABLE>
	</td>
	<td>&nbsp;</td>
  </tr>
  <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
  </tr>
</table>

</ContentTemplate>
  <Triggers>  	
	<asp:AsyncPostBackTrigger EventName="Click" ControlID="lnkSubmit" />	
  </Triggers>
</asp:UpdatePanel>

 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelCreateWorkOrder" runat="server">
        <ProgressTemplate>
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Please Wait...
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="UpdatePanelCreateWorkOrder" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />

