<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSCompAccreds.ascx.vb" Inherits="Admin.UCMSCompAccreds" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<input type="hidden" runat="server" id="hdnSelectedIds" name="hdnSelectedIds" />
<asp:UpdatePanel ID="UpdatePanel2" runat="server"  >
 <ContentTemplate>
<table>
<tr>
    <td><asp:Label ID=lblMessage  CssClass="bodytxtValidationMsg" runat=server></asp:Label></td>
</tr>
</table>
<asp:Panel ID="pnlAccreds" runat="server">
<table width="650"  border="0" cellpadding="0" cellspacing="0">
	<tr>
	  <td width="220" height="24" valign="middle" class="formTxt">Select a Vendor </td>
	  <td width="130">&nbsp;</td>
	  <td width="240" valign="bottom" class="formTxt"><asp:Label ID="lblLeveltype" runat="server" Text="Level Type Selection" Visible="false"></asp:Label></td>
	  <td width="75" class="formTxt">
	  </td>
	</tr>
	<tr>
	  <td width="220">
	  	<asp:ListBox height=150  ID="lstBoxVendor"  AutoPostBack=true  class="formFieldWhite width202height110" DataTextField="VendorName" DataValueField="VendorID" runat="server"></asp:ListBox>
	  </td>
	  <td width="135" valign="top"  class="formTxt"> 
		<asp:RadioButton class="radformFieldPadT0" AutoPostBack="true" ID="HasSkill" Visible="false" runat="server" GroupName="rdSkillPartner"
																Text="Has Skills" TextAlign="right"></asp:RadioButton><br>
		<asp:RadioButton class="radformFieldPadT0" ID="HasPartner" AutoPostBack="true" Visible="false" runat="server" GroupName="rdSkillPartner"
														Text="Is Partner" TextAlign="right"></asp:RadioButton>
	  </td>
	  <td width="240" valign="top" >
	  	<TABLE cellSpacing="0" cellPadding="0" width="230" border="0" id="tdPartnerMember" visible="false" runat="server">
			<TR>
				<TD><asp:ListBox Rows="4" height=115 ID="lstBoxPartner" class="formFieldWhite width202 height115" DataTextField="PartnerName" DataValueField="PartnerID" runat="server"  SelectionMode="multiple"></asp:ListBox></TD>
			</TR>
			<TR >
				<TD valign="bottom" class="formTxt" id="tdMember" runat="server">Membership No<br>
			  <asp:TextBox ID="txtMembershipNo" runat="server" CssClass="formField width202"></asp:TextBox></TD>
			</TR>
		</TABLE>
	  </td>
	  <td width="75" align="left" valign="bottom" >
	  	<TABLE runat="server" id="tblSubmit" visible="false" cellSpacing="0" cellPadding="0" width="70" bgColor="#a0a0a0" border="0">
		  <TR>
			<TD height="18"><IMG runat=server height="18" src="~/Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
			<TD><asp:linkbutton   class="txtButtonNoArrow" id="lnkSelect" CausesValidation=false runat="server">Submit</asp:linkbutton></TD>
			<TD width="5"><IMG runat=server height="18" src="~/Images/Curves/Grey-BtnRight.gif" width="5"></TD>
		  </TR>
	   </TABLE>
	  </td>
	</tr>
</table>
</asp:Panel>
<br>
<asp:Panel ID="pnlSelectedAccreds" runat="server">
<table width="650"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="25" class="formTxt">List of Submitted Vendor Accreditations</td>
  </tr>
  <tr>
    <td><table width="100%" class="tblBorder" bgcolor="#A0A0A0"  border="0" cellspacing="0" cellpadding="2">
      <tr >
        <td width="25" align="center" class="formtxt"><input id='chkAll' onClick=CheckAll(this,'CheckAccred') type='checkbox' name='chkAll' /></td>
        <td class="txtButtonNoArrow ">Vendor</td>
        <td width="60" class="txtButtonNoArrow">Have Skill </td>
        <td width="150" class="txtButtonNoArrow">Partner level </td>
        <td width="145" class="txtButtonNoArrow"  id="tdColMemberNo" runat=server>Membership No </td>
        <td width="70" class="txtButtonNoArrow"  id="tdColDate" runat=server>Date Added</td>
      </tr>
    </table>
      <div id="divAccreds" runat=server class="treeControl">
		 <asp:GridView ID="gvSelectedAccreds" CellPadding=2 CellSpacing=0 runat=server  AutoGenerateColumns="False" BorderWidth="1px"  BorderColor="#EDEDED" Width="648">
			<Columns>
			<asp:TemplateField ItemStyle-Width="15px" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="center">
			<ItemTemplate>
			  <asp:CheckBox ID="CheckAccred" Runat="server" ></asp:CheckBox>
			  <input type="hidden" Runat="server" id="hdnVendorId" value=<%# databinder.eval(container.dataitem, "VendorID") & "#" & databinder.eval(container.dataitem, "PartnerID") & "" %>/>
			</ItemTemplate>
			<ItemStyle  />
			</asp:TemplateField> 
			
			<asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left  DataField="VendorName"  /> 
			<asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left  DataField="HasSkill"   ItemStyle-Width="55px"   /> 		
			<asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left  DataField="PartnerName"   ItemStyle-Width="145px"   /> 		
			<asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left  DataField="MemberShipNo"   ItemStyle-Width="140px"   /> 		
			<asp:TemplateField  ItemStyle-Width="70px" ItemStyle-CssClass="gridRowAOE"> 
			<ItemStyle  Wrap=true />
			<ItemTemplate>             
				<%#Strings.FormatDateTime(Container.DataItem("Date"), DateFormat.ShortDate)%>
			</ItemTemplate>
			</asp:TemplateField>  
			</Columns>
			<AlternatingRowStyle   BackColor="#E7E7E7" /> <RowStyle CssClass=gridRow /> 
		 </asp:GridView>
		
		</div></td>
  </tr>
  
   <tr>
    <td height="40" valign="middle" class="formTxt"> 
	<TABLE cellSpacing="0" cellPadding="0" width="100" bgColor="#a0a0a0" border="0">
          <TR>
            <TD height="18"><IMG height="18" runat=server src="~/Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
            <TD><asp:linkbutton   class="txtButtonNoArrow" CausesValidation=false id="lnkRemove" runat="server">Delete Selected</asp:linkbutton></TD>
            <TD width="5"><IMG height="18" runat=server src="~/Images/Curves/Grey-BtnRight.gif" width="5"></TD>
          </TR>
        </TABLE> </td>
  </tr>
</table>
</asp:Panel>

</ContentTemplate>   
</asp:UpdatePanel> 