'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class UCMSViewDebitNoteUK

    '''<summary>
    '''imgLogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgLogo As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lblCompanyName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCompanyName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAddress As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCity As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPostCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPostCode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTelNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTelNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFaxNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFaxNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCompNameInvoiceTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCompNameInvoiceTo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInvoiceTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInvoiceTo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAccountNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAccountNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInvoiceNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInvoiceNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInvoiceDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInvoiceDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPurchaseInvoiceNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPurchaseInvoiceNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblVatNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblVatNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''DivOldFormat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DivOldFormat As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''lblVATPer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblVATPer As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rptList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptList As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''DivOldFormat2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DivOldFormat2 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''DivOldFormat1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DivOldFormat1 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''lblSubTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSubTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotalVATPer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalVATPer As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotalVAT control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalVAT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotalAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalAmount As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''DivNewFormat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DivNewFormat As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''rptListNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptListNew As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''lblRegistartionNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegistartionNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRegistartionNoLine2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegistartionNoLine2 As Global.System.Web.UI.WebControls.Label
End Class
