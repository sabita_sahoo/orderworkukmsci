<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCChangeCredentials.ascx.vb" Inherits="Admin.UCChangeCredentials" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<asp:Panel ID="PnlChangeCredentials" runat="server"  >
 <asp:UpdatePanel ID="UpdatePnlChangeCredentials" runat="server"  UpdateMode="Conditional" RenderMode=Inline>
  <ContentTemplate>
 
<asp:Panel ID="pnlSubmitForm" runat="server" visible="true">
<div id="divValidationMain" class="divValidation" runat=server >
	<div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="15" align="center">&nbsp;</td>
				<td height="15">&nbsp;</td>
				<td height="15">&nbsp;</td>
			</tr>
			<tr valign="middle">
				<td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				<td class="validationText"><div  id="divValidationMsg"></div>
				<asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
				<asp:ValidationSummary id="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
				</td>
				<td width="20">&nbsp;</td>
			</tr>
			<tr>
				<td height="15" align="center">&nbsp;</td>
				<td height="15">&nbsp;</td>
				<td height="15">&nbsp;</td>
			</tr>
		</table>
	<div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
</div>	


<div class="tabDivOuter" >

	<div class="tabDivOuter" id="divMainTab">
	 <div class="roundtopLightGreyCurve"><img src="Images/Curves/LightGrey-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
       		  
											
											
			<DIV class="paddedBox" id="divSubmit">
											
							
			<TABLE width="100%" border="0" cellPadding="0" cellSpacing="0">
              <TR>
                <TD align="left" class="HeadingRed"><STRONG>Account Info</STRONG></TD>
                <TD align="left" class="padTop17"><TABLE id="tblSaveTop" class="clshideundo" runat=server cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                  <TR>
                    <TD align="right"   vAlign="top"><TABLE cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
                        <TR>
                          <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                          <TD width="110" class="txtBtnImage"><asp:Linkbutton id="btnSaveTop" CausesValidation="false"  CssClass="txtListing" runat="server" TabIndex="10"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:Linkbutton></TD>
                          <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                        </TR>
                    </TABLE></TD>
                    <TD width="10"></TD>
                    <TD width="125" align="right" vAlign="top" style="padding:0px 0px 0px 0px;"><TABLE cellSpacing="0" class="clshideundo" cellPadding="0" width="125" bgColor="#F0F0F0" border="0">
                        <TR>
                          <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                          <TD width="115" class="txtBtnImage"><asp:LinkButton id="btnResetTop" CausesValidation="false" runat="server" TabIndex="11"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton></TD>
                          <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                        </TR>
                    </TABLE></TD>
                  </TR>
                </TABLE></TD>
              </TR>
            </TABLE>
			 
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD vAlign="top" align="left" height="18" class="formTxt"><STRONG>Login Info</STRONG></TD>
														</TR>
													</TABLE>
													<TABLE height="18" cellSpacing="0" cellPadding="0"  border="0">
														<TR vAlign="top">
															<TD class="formTxt" align="left" height="14" colspan="2">Old Password
																<asp:RegularExpressionValidator ControlToValidate="txtOldPassword"  ErrorMessage="Password must contain 8 or more alphanumeric characters." ForeColor="#EDEDEB" ID="rgOldPassword" runat="server" ValidationExpression="[0-9a-zA-Z]{8,}" >*</asp:RegularExpressionValidator>
																	<asp:CustomValidator ID="custChangePassword"  runat="server" ErrorMessage="Please enter your old Password"  ForeColor="#EDEDEB" >*</asp:CustomValidator>
																</TD>
														</TR>
														<TR vAlign="top">
															<TD align="left" height="24" colspan="2">
																<asp:TextBox id="txtOldPassword" tabIndex="12" runat="server" CssClass="formField width150" TextMode="Password"></asp:TextBox></TD>
														</TR>
														<TR vAlign="top">
															<TD class="formTxt" align="left" width="160" height="14">New Password
																<asp:CustomValidator ID="custPassword"  runat="server" ErrorMessage="Please enter your Password"  ForeColor="#EDEDEB" ControlToValidate="txtPassword">*</asp:CustomValidator>
																<asp:RegularExpressionValidator ControlToValidate="txtPassword" ErrorMessage="Password must contain 8 or more alphanumeric characters." ForeColor="#EDEDEB" ID="rgPassword" runat="server" ValidationExpression="[0-9a-zA-Z]{8,}" >*</asp:RegularExpressionValidator></TD>
															<TD class="formTxt" align="left" width="310" height="14">Confirm Password
																<asp:CompareValidator id="CompareValidator1" runat="server" ErrorMessage="Password does not match, please reconfirm"
																	ControlToValidate="txtPassword" ControlToCompare="txtConfirmPassword">*</asp:CompareValidator></TD>
														</TR>
														<TR vAlign="top">
															<TD align="left" height="24" >
																<asp:TextBox id="txtPassword" tabIndex="13" runat="server" CssClass="formField width150" TextMode="Password"></asp:TextBox></TD>
															<TD align="left" height="24">
																<asp:TextBox id="txtConfirmPassword" tabIndex="14" runat="server" CssClass="formField width150" TextMode="Password"></asp:TextBox></TD>
														</TR>
													</TABLE>
													<TABLE id="tblEmail" height="18" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server" >
														<TR vAlign="top">
															<TD class="formTxt" align="left" height="14">New Email Address
															<asp:RegularExpressionValidator id="regularNewEmail" runat="server" ErrorMessage="Please enter Email Address in correct format"
																	ForeColor="#EDEDEB" ControlToValidate="txtNewEmail" ValidationExpression="^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,}$">*</asp:RegularExpressionValidator></TD>
															
															<TD height="14" align="left" class="formTxt">Confirm Email Address  
															  <asp:RegularExpressionValidator ID="rgConfirmEmail" runat="server" ControlToValidate="txtEmailConfirm" ErrorMessage="Please enter Confirm Email Address in correct format" ValidationExpression="^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,}$">*</asp:RegularExpressionValidator>
																<asp:CustomValidator ID="custNewEmail" ClientValidationFunction="isNewEmailEntered" runat="server" OnServerValidate="custNewEmail_ServerValidate" ErrorMessage="Please enter confirm email address" ForeColor="#EDEDEB" >*</asp:CustomValidator>
													   	  <asp:CompareValidator ID="conEmails" runat="server" ErrorMessage="Email IDs are not same" ForeColor="#EDEDEB" ControlToCompare="txtNewEmail" ControlToValidate="txtEmailConfirm">*</asp:CompareValidator></TD>
															
														</TR>
														<TR vAlign="top">
															<TD width="160" align="left" height="24">
																<asp:TextBox id="txtNewEmail" tabIndex="15" runat="server"  CssClass="formField width150"></asp:TextBox></TD>
															<TD height="24" align="left"><asp:TextBox id="txtEmailConfirm" tabIndex="16" runat="server" CssClass="formField width150"></asp:TextBox>
													        </TD>	
														</TR>
													</TABLE>
													<TABLE id="tblSecurity" height="18" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server" >
														<TR vAlign="top">
															<TD class="formTxt" align="left" height="14">Security Question
																<SPAN class="bodytxtValidationMsg">*</SPAN>
																<asp:RequiredFieldValidator id="rqQuestion" runat="server" ErrorMessage="Please select a Security Question"
																	ForeColor="#EDEDEB" ControlToValidate="ddlQuestion">*</asp:RequiredFieldValidator></TD>
														</TR>
														<TR vAlign="top">
															<TD align="left" height="24">
																<asp:DropDownList id="ddlQuestion" tabIndex="17" runat="server" CssClass="formField width310"></asp:DropDownList></TD>
														</TR>
														<TR vAlign="top">
															<TD class="formTxt" align="left" height="14">Security Answer<SPAN class="bodytxtValidationMsg"> *</SPAN>
																<asp:RequiredFieldValidator id="rqAnswer" runat="server" ErrorMessage="Please enter a Security Answer" ForeColor="#EDEDEB"
																	ControlToValidate="txtAnswer">*</asp:RequiredFieldValidator>
																<SPAN class="bodytxtValidationMsg">*</SPAN>
																</TD>
														</TR>
														<TR vAlign="top">
															<TD align="left" height="24">
																<asp:TextBox id="txtAnswer"  tabIndex="18" runat="server" CssClass="formField width310"></asp:TextBox></TD>
														</TR>
													</TABLE>
													
			  								  </DIV>
											  </div>
												<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
              <TR>
                <TD align="left" class="HeadingRed">&nbsp;</TD>
                <TD align="left" class="padTop17"><TABLE id="tblSaveBottom" runat=server cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                  <TR>
                    <TD align="right" class="clsalignleft" vAlign="top"><TABLE cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
                        <TR>
                          <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                          <TD width="110" class="txtBtnImage"><asp:Linkbutton id="btnSaveBottom" CausesValidation="false" CssClass="txtListing" runat="server" TabIndex="19"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:Linkbutton></TD>
                          <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                        </TR>
                    </TABLE></TD>
                    <TD width="10"></TD>
                    <TD width="125" align="right" vAlign="top" style="padding:0px 18px 0px 0px;">
                    <TABLE cellSpacing="0" class="clshideundo" cellPadding="0" width="125" bgColor="#F0F0F0" border="0">
                        <TR>
                          <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                          <TD width="115" class="txtBtnImage"><asp:LinkButton id="btnResetBottom"  CausesValidation="false" runat="server" TabIndex="20"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton></TD>
                          <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                        </TR>
                    </TABLE></TD>
                  </TR>
                </TABLE></TD>
              </TR>
            </TABLE>
		</asp:Panel>
												
			<!-- Confirm Action panel --> 
			<asp:panel ID="pnlConfirm" runat="server" visible="false">
			
			  <table width="100%" class="clshieghtwidth" border="0" cellpadding="0" cellspacing="0" class="padTop50">
                <tr>
                  <td width="300" align="right"><table cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td class="txtBtnImage"><asp:LinkButton id="btnConfirm" runat="server" CausesValidation="false" class="txtListing" TabIndex="21" > <img src="Images/Icons/Icon-Confirm.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Confirm </asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                  <td width="10">&nbsp;</td>
                  <td align="left"><table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
                      <tr>
                 <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td align="center" class="txtBtnImage">
                            <asp:LinkButton id="btnCancel" runat="server" CausesValidation="false" class="txtListing" TabIndex="22" > <img src="Images/Icons/Icon-Cancel.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
                          </td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
              		
			
			</asp:panel>
												
												
			<asp:panel ID="pnlSuccess" runat="server" >
			    <table width="100%" class="clshieghtwidth" height="40" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center"><asp:Label ID="lblChangeLoginMsg" CssClass="bodytxtRed" runat="server"></asp:Label></td>
                    </tr>
                </table>
			</asp:panel>
				  
 </div>
 
 </ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID="btnSaveTop" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnResetTop" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnSaveBottom" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnResetBottom" EventName="Click" />

</Triggers>
</asp:UpdatePanel>
</asp:Panel> 

 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePnlChangeCredentials" runat="server">
        <ProgressTemplate>
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Please Wait..saving data
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="PnlChangeCredentials" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />


    


   