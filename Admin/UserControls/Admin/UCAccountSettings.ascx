<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCAccountSettings.ascx.vb"
    Inherits="Admin.UCAccountSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>
<style>
    #UCAccountSettings1_valsum
    {
        background-color: #FFFDB9;
        width: 940px;
        padding: 10px;
        margin-left: 10px;
        padding-bottom: 0px;
        padding-top: 20px;
    }
</style>
<script type="text/javascript">

    function maxlength() {
        var limitNum = 150;
        var length = (document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_txtcomptag").value).length
        if (length > 150) {
            document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_txtcomptag").value = (document.getElementById("ctl00_ContentPlaceHolder1_UCAccountSettings1_txtcomptag").value).substring(0, 150);
        }

    }

    function SetCompanyType(id) {
       // alert(document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_hdnRoleId').value);
        if (document.getElementById(id).value == "Empowered" && document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_hdnRoleId').value == "True") {
            document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtAdminFee').disabled = false;
            document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtAdminFee').value = "0";
        }
        else if (document.getElementById(id).value == "Empowered" &&
            document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_hdnRoleId').value == "False") {
            document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtAdminFee').disabled = true;
            document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtAdminFee').value = "0";
        }
        else if (document.getElementById(id).value == "Orderwork" && document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_hdnRoleId').value == "True") {
            document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtAdminFee').disabled = false;
            document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtAdminFee').value = "10";
        }
        else {
            document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtAdminFee').disabled = true;
            document.getElementById('ctl00_ContentPlaceHolder1_UCAccountSettings1_txtAdminFee').value = "10";
        }
    }

</script>
<input type="hidden" id="hdnEvengInst" runat="Server" value="False" />
<input type="hidden" id="hdnRoleId" runat="Server" value="True" />
<asp:ValidationSummary ID="valsum" runat="server" DisplayMode="BulletList" ValidationGroup="VGAcc"
    HeaderText="<div style='float:left;width:60px;'><img  vspace='0' src='Images/Icons/Validation-Alert.gif' align='absmiddle'></div>">
</asp:ValidationSummary>
<asp:Panel ID="PnlCompanyProfile" runat="server">
    <asp:Panel ID="pnlSubmitForm" runat="server" Visible="true">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="padding: 0px 0px 16px 0px;
            margin: 0px;">
            <tr>
                <td width="165" class="txtWelcome paddingL10">
                    <div id="divButton" style="width: 165px;">
                        <div class="bottonTopGrey">
                            <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                height="4" class="btnCorner" style="display: none" /></div>
                        <a class="buttonText" runat="server" id="lnkBackToListing">
                            <img src="Images/Arrows/Arrow-Back.gif" title="Back to Company Profile" width="4"
                                height="8" hspace="0" vspace="0" border="0" class="paddingL10R8" /><strong>Back to Company
                                    Profile</strong></a>
                        <div class="bottonBottomGrey">
                            <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                height="4" class="btnCorner" style="display: none" /></div>
                    </div>
                </td>
            </tr>
        </table>
        <div style="margin: 0px 5px 0px 5px;" id="divMainTab">
            <div class="paddedBox" id="divProfile">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" class="HeadingRed">
                            <strong>Account Settings</strong>
                        </td>
                        <td align="left" class="padTop17">
                            <table id="tblSaveTop" runat="server" cellspacing="0" cellpadding="0" width="100%"
                                border="0" style="padding: 0px 0px 16px 0px; margin: 0px;">
                                <tr>
                                    <td align="right" valign="top">
                                        <table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                                            <tr>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" />
                                                </td>
                                                <td width="110" class="txtBtnImage">
                                                    <asp:LinkButton ID="btnSaveTop" ValidationGroup="VGAcc" CssClass="txtListing" runat="server"
                                                        TabIndex="67"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton>
                                                </td>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="10">
                                    </td>
                                    <td width="125" align="right" valign="top" style="padding: 0px 0px 0px 0px;">
                                        <table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                                            <tr>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" />
                                                </td>
                                                <td width="115" class="txtBtnImage">
                                                    <asp:LinkButton ID="btnResetTop" CausesValidation="false" runat="server" TabIndex="68"
                                                        class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton>
                                                </td>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="divWorkOrder">
                    <div class="WorkOrderTopCurve">
                        <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Settings</strong></div>
                    <table width="100%" cellspacing="0" cellpadding="9" border="0">
                        <tr>
                            <td>
                                <br />
                                <input runat="server" type="checkbox" id="chkNewPortalAccess" />&nbsp;<span class="txtNavigation">New Portal Acess</span>&nbsp;&nbsp;  <input runat="server" type="checkbox" id="chkOldPortalAccess" />&nbsp;<span class="txtNavigation">Hide Old Portal Acess</span><br />
                                <input runat="server" type="checkbox" id="chkProfIdem" />&nbsp;<span class="txtNavigation">Disable
                                    Professional Indemnity</span><br />
                                <input runat="server" type="checkbox" id="chkBrigantia" />&nbsp;<span class="txtNavigation">Are
                                    you a Brigantia Member</span><br />
                                <input runat="server" type="checkbox" id="chkDisableHolidays" />&nbsp;<span class="txtNavigation">Enable
                                    Holidays</span><br />
                                <input runat="server" type="checkbox" id="chkDisableSaturdays" />&nbsp;<span class="txtNavigation">Enable
                                    Saturday</span><br />
                                <input runat="server" type="checkbox" id="chkDisableSundays" />&nbsp;<span class="txtNavigation">Enable
                                    Sunday</span><br />
                                <input runat="server" type="checkbox" id="chkEnableClientJobList" />&nbsp;<span class="txtNavigation">Enable
                                    Client List of Jobs</span><br />
                                <input runat="server" type="checkbox" id="chkTrackSP" />&nbsp;<span class="txtNavigation">Track
                                    Supplier</span><br />
                                <input runat="server" type="checkbox" id="chkWoEmailNotice" />&nbsp;<span class="txtNavigation">Enable
                                    WO Submit Confirmation Email</span><br />
                                <input runat="server" type="checkbox" id="chkSLA" />&nbsp;<span class="txtNavigation">Enable
                                    Service Level Agreement</span><br />
                                <input runat="server" type="checkbox" id="chkAllowChangePassword" />&nbsp;<span class="txtNavigation">Allow
                                    OrderWork User to set Password</span><br />
                                <input runat="server" type="checkbox" id="chkNoChargeClient" />&nbsp;<span class="txtNavigation">Do
                                    not Charge Client</span><br />
                                <input runat="server" type="checkbox" id="chkNoChargeSP" />&nbsp;<span class="txtNavigation">Do
                                    not Pay All Suppliers</span><br />
                                <input runat="server" type="checkbox" id="chkNoChargeFavSP" />&nbsp;<span class="txtNavigation">Do
                                    not Pay All Favourite Suppliers</span><br />
                                <input runat="server" type="checkbox" id="chkEnableGoodsLoc" />&nbsp;<span class="txtNavigation">Enable
                                    Goods Location</span> <span style="margin-left: 40px;" class="txtNavigation">Default
                                        Goods Location:</span><asp:DropDownList ID="drpdwnGoodsCollection" runat="server"
                                            TabIndex="29" DataTextField="Name" DataValueField="AddressId" CssClass="formFieldGrey"
                                            Width="150">
                                        </asp:DropDownList>
                                <br />
                                <span style="margin-left: 185px;" class="txtNavigation">Category Type:</span>
                                <asp:DropDownList CssClass="formField200" ID="drpCategoryType" runat="server">
                                    <asp:ListItem Text="Select Category Type" Selected="True" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Internal" Value="Internal"></asp:ListItem>
                                    <asp:ListItem Text="External" Value="External"></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <div style="margin-top: 4px;">
                                </div>
                                <asp:CheckBox ID="chkLockSkillSets" runat="server" Text="Lock Skill Sets" CssClass="txtNavigation"
                                    OnClick="javascript:toggleLockSkillSetComments()" />
                                <asp:TextBox runat="server" MaxLength="150" TextMode="MultiLine" ID="txtLockSkillSetComments"
                                    Style="margin-left: 83px; vertical-align: text-top;" onChange="validateMaxLen(this.id, '150', 'Lock skill set comments can have max 150 characters.')"></asp:TextBox><span
                                        style="width: 10px; font-size: 12px; color: Red; vertical-align: text-top;" id="spanfldLockSkillSetComments"
                                        runat="server">&nbsp;*</span>
                                <asp:RequiredFieldValidator ValidationGroup="VGAcc" ID="txtFeildSkillSet" runat="server"
                                    ControlToValidate="txtLockSkillSetComments" Display="None" Enabled="false" ErrorMessage="Lock skill set comments can not be blank">*</asp:RequiredFieldValidator>
                                <asp:CheckBox CssClass="txtNavigation" ID="chkEveningInst" runat="server" Text="&nbsp;Enable Evening Installation"
                                    AutoPostBack="false" OnClick="javascript:toggleAccountsView()" Visible="false">
                                </asp:CheckBox><br />
                                <div style="clear: both">
                                </div>
                                <div style="margin-top: 10px;">
                                    <span class="txtNavigation" style="vertical-align: top">Company tag line: </span>
                                    <asp:TextBox runat="server" Style="margin-left: 83px;" TextMode="MultiLine" MaxLength="200"
                                        onblur="javascript:removeHTML(this.id);" ID="txtcomptag" onKeyUp="javascript:maxlength();"
                                        onKeyDown="javascript:maxlength();"></asp:TextBox></div>
                                <br />
                                <span  class="txtNavigation">WO Creation Flow:</span>
                                <asp:DropDownList CssClass="formField200" ID="ddlWoCreatioFlow" runat="server"></asp:DropDownList>
                                                          <br />
                                <div id="pnlSettings" runat="server" style="display: none; float: left;">
                                    <div style="margin-left: 300px; margin-bottom: 10px;">
                                        <span id="Span2" style="width: 40px;" class="gridTextNew"><strong>Type</strong></span><span
                                            id="Span3" style="width: 40px; margin-left: 50px;" class="gridTextNew"><strong>Value</strong></span></div>
                                    <div id="divEveningTimeLbl" runat="server" style="float: left; clear: right;">
                                        <span id="Span1" style="width: 70px; margin-left: 20px;" class="gridTextNew"><strong>
                                            Label:</strong></span><input runat="server" maxlength="20" type="text" id="txtEvngSlotLabel"
                                                value="6pm - 8pm" style="width: 100px;" />
                                    </div>
                                    <div style="float: left;">
                                        <table cellpadding="0" cellspacing="0" style="margin-left: 40px;">
                                            <tr>
                                                <td id="spnRP" style="width: 70px;" class="txtNavigation">
                                                    RP Uplift(�)
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdoRPUPPerc" runat="server" Text="%" GroupName="RPUPlift" /><asp:RadioButton
                                                        ID="rdoRPUPFixed" runat="server" Text="Fixed" GroupName="RPUPlift" Style="margin-left: 10px;
                                                        margin-right: 10px;" /><input runat="server" type="text" id="txtRPUplift" value="0.00"
                                                            style="text-align: right; width: 50px;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="spnWP" style="width: 70px;" class="txtNavigation">
                                                    WP Uplift(�)
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdoWPUPPerc" runat="server" Text="%" GroupName="WPUPlift" /><asp:RadioButton
                                                        ID="rdoWPUPFixed" runat="server" Text="Fixed" GroupName="WPUPlift" Style="margin-left: 10px;
                                                        margin-right: 10px;" /><input runat="server" type="text" id="txtWPUplift" value="0.00"
                                                            style="text-align: right; width: 50px;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="spnPP" style="width: 70px;" class="txtNavigation">
                                                    PP Uplift(�)
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdoPPUPPerc" runat="server" Text="%" GroupName="PPUPlift" /><asp:RadioButton
                                                        ID="rdoPPUPFixed" runat="server" Text="Fixed" GroupName="PPUPlift" Style="margin-left: 10px;
                                                        margin-right: 10px;" /><input runat="server" type="text" id="txtPPUplift" value="0.00"
                                                            style="text-align: right; width: 50px;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="WorkOrderBottomCurve">
                        <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                            style="display: none" /></div>
                </div>
                <asp:UpdatePanel ID="updProfileSettings" runat="server">
                    <ContentTemplate>
                        <div class="divWorkOrder">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>MyOrderWorkUK
                                    Profile Setting</strong></div>
                            <table width="100%" cellspacing="2" cellpadding="2" border="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDefaultPage" runat="server" Text="Default Home Page: "></asp:Label>
                                        <asp:DropDownList CssClass="formFieldGrey" ID="drpDefaultPage" runat="server">
                                            <asp:ListItem Text="--SELECT--" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Dashboard" Value="BuyerWOlisting.aspx"></asp:ListItem>
                                            <asp:ListItem Text="RetailDixons" Value="RetailDixonWOForm.aspx"></asp:ListItem>
                                            <asp:ListItem Text="B2CRetailWithScope" Value="B2CRetailWithScope.aspx"></asp:ListItem>
                                            <asp:ListItem Text="Welcome" Value="Welcome.aspx"></asp:ListItem>
                                            <asp:ListItem Text="Book Work Order" Value="WOForm.aspx"></asp:ListItem>
                                            <asp:ListItem Text="Stone Group WOForm" Value="StoneGroupWOForm.aspx"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="grdDefaultSettings" ForeColor="Maroon" Font-Size="11px" BorderWidth="1px"
                                            BorderColor="Black" runat="server" AutoGenerateColumns="false" Width="350px">
                                            <Columns>
                                                <asp:BoundField HeaderText="User Type" ItemStyle-Width="100px" DataField="RoleGroupName" />
                                                <asp:TemplateField HeaderText="Booking Form" ItemStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDefaultBookingForm" runat="server" Text='<%#Eval("DefaultBookingForm") %>'></asp:Label>
                                                        <asp:DropDownList CssClass="formFieldGrey" ID="drpDefaultBookingForm" Visible="false"
                                                            runat="server">
                                                            <asp:ListItem Text="--SELECT--" Value=""></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandName="EditForm"
                                                            CommandArgument='<%#Eval("RoleGroupId") %>'></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtnCancel" runat="server" Text="Cancel" Visible="false" CommandName="Cancel"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                   </asp:UpdatePanel>
                <div class="divWorkOrder">
                    <div class="WorkOrderTopCurve">
                        <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Upload
                            Company Logo</strong></div>
                    <br />
                    <uc2:UCFileUpload ID="UCFileUpload1" runat="server" Control="UCAccountSettings1_UCFileUpload1"
                        Type="CompanyLogo" AttachmentForSource="CompanyLogo" ShowNewAttach="True" ShowUpload="True"
                        MaxFiles="1"></uc2:UCFileUpload>
                </div>
                <div class="divWorkOrder">
                    <div class="WorkOrderTopCurve">
                        <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Finance
                            Settings</strong></div>
                    <table width="100%" cellspacing="0" cellpadding="9" border="0">
                        <tr>
                            <td>
                                <br />
                                <input runat="server" type="checkbox" id="chkDiscount" onclick='javascript:ShowHideField(this.id,"ctl00_ContentPlaceHolder1_UCAccountSettings1_pnldisctxt");' />&nbsp;<span
                                    class="txtNavigation">Enable Discount </span>&nbsp;
                                <div runat="server" id="pnldisctxt" style="display: none;">
                                    <asp:TextBox ID="txtDiscountPercent" MaxLength="18" runat="server" CssClass="formFieldRight width60"
                                        onblur='FormatttedAmount(this.id); ValidateDiscount("UCAccountSettings1_txtDiscountPercent");'></asp:TextBox>&nbsp;%
                                    <br />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" cellpadding="9" border="0">
                        <tr>
                            <td width="180px" class="gridTextNew">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Field Label</strong>
                            </td>
                            <td class="gridTextNew">
                                <strong>Field Answer</strong>
                            </td>
                        </tr>
                        <tr>
                            <td width="190px">
                                <input runat="server" type="checkbox" id="chkField1" style="vertical-align: middle;"
                                    onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAccountSettings1_reqFldLbl1","ctl00_ContentPlaceHolder1_UCAccountSettings1_reqfld1","ctl00_ContentPlaceHolder1_UCAccountSettings1_spanfld1","ctl00_ContentPlaceHolder1_UCAccountSettings1_spanfldVal1","")'
                                    tabindex="33" />&nbsp;<asp:TextBox ID="txtField1" ValidationGroup="VGAcc" MaxLength="50"
                                        TabIndex="34" runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                            font-size: 12px; color: Red; display: none;" id="spanfld1" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ValidationGroup="VGAcc" ID="reqFldLbl1" runat="server"
                                    ControlToValidate="txtField1" Display="None" Enabled="false" ErrorMessage="Field label can not be blank">*</asp:RequiredFieldValidator>
                                <input runat="server" type="checkbox" id="chkField2" style="vertical-align: middle;"
                                    onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAccountSettings1_reqFldLbl2","ctl00_ContentPlaceHolder1_UCAccountSettings1_reqfld2","ctl00_ContentPlaceHolder1_UCAccountSettings1_spanfld2","ctl00_ContentPlaceHolder1_UCAccountSettings1_spanfldVal2","")'
                                    tabindex="36" />&nbsp;<asp:TextBox ID="txtField2" ValidationGroup="VGAcc" MaxLength="50"
                                        TabIndex="37" runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                            font-size: 12px; color: Red; display: none;" id="spanfld2" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ValidationGroup="VGAcc" ID="reqFldLbl2" runat="server"
                                    ControlToValidate="txtField2" Display="None" Enabled="false" ErrorMessage="Field label can not be blank"></asp:RequiredFieldValidator>
                                <input runat="server" type="checkbox" id="chkField3" style="vertical-align: middle;"
                                    onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAccountSettings1_reqFldLbl3","ctl00_ContentPlaceHolder1_UCAccountSettings1_reqfld3","ctl00_ContentPlaceHolder1_UCAccountSettings1_spanfld3","ctl00_ContentPlaceHolder1_UCAccountSettings1_spanfldVal3","")'
                                    tabindex="39" />&nbsp;<asp:TextBox ID="txtField3" ValidationGroup="VGAcc" MaxLength="50"
                                        TabIndex="40" runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                            font-size: 12px; color: Red; display: none;" id="spanfld3" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ValidationGroup="VGAcc" ID="reqFldLbl3" runat="server"
                                    ControlToValidate="txtField3" Display="None" Enabled="false" ErrorMessage="Field label can not be blank"></asp:RequiredFieldValidator>
                                <input runat="server" type="checkbox" id="chkField4" style="vertical-align: middle;"
                                    onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAccountSettings1_reqFldLbl4","ctl00_ContentPlaceHolder1_UCAccountSettings1_reqfld4","ctl00_ContentPlaceHolder1_UCAccountSettings1_spanfld4","ctl00_ContentPlaceHolder1_UCAccountSettings1_spanfldVal4","")'
                                    tabindex="42" />&nbsp;<asp:TextBox ID="txtField4" ValidationGroup="VGAcc" MaxLength="50"
                                        TabIndex="43" runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                            font-size: 12px; color: Red; display: none;" id="spanfld4" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ValidationGroup="VGAcc" ID="reqFldLbl4" runat="server"
                                    ControlToValidate="txtField4" Display="None" Enabled="false" ErrorMessage="Field label can not be blank"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFieldValue1" ValidationGroup="VGAcc" MaxLength="50" TabIndex="35"
                                    runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                        font-size: 12px; color: Red; display: none;" id="spanfldVal1" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ID="reqfld1" ValidationGroup="VGAcc" runat="server" ControlToValidate="txtFieldValue1"
                                    Display="None" Enabled="false" ErrorMessage="Field answer can not be blank"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtFieldValue2" ValidationGroup="VGAcc" MaxLength="50" TabIndex="38"
                                    runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                        font-size: 12px; color: Red; display: none;" id="spanfldVal2" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ID="reqfld2" ValidationGroup="VGAcc" runat="server" ControlToValidate="txtFieldValue2"
                                    Display="None" Enabled="false" ErrorMessage="Field answer can not be blank"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtFieldValue3" ValidationGroup="VGAcc" MaxLength="50" TabIndex="41"
                                    runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                        font-size: 12px; color: Red; display: none;" id="spanfldVal3" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ID="reqfld3" ValidationGroup="VGAcc" runat="server" ControlToValidate="txtFieldValue3"
                                    Display="None" Enabled="false" ErrorMessage="Field answer can not be blank"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtFieldValue4" ValidationGroup="VGAcc" MaxLength="50" TabIndex="44"
                                    runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                        font-size: 12px; color: Red; display: none;" id="spanfldVal4" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ID="reqfld4" ValidationGroup="VGAcc" runat="server" ControlToValidate="txtFieldValue4"
                                    Display="None" Enabled="false" ErrorMessage="Field answer can not be blank"></asp:RequiredFieldValidator>
                                <asp:CustomValidator runat="server" ID="custVal" ValidationGroup="VGAcc" Display="None"
                                    ErrorMessage="Flied labels can not be duplicated"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                     <%--'OA-622 - discount changes--%>
                    <asp:Panel runat="server" ID="pnlSupplierFee" style="display:none">
                        <table width="100%" cellspacing="0" cellpadding="9" border="0">
                            <tr>
                                <td class="gridTextNew" style="width: 200px;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Supplier fee (%)<span style="width: 10px;
                                        font-size: 12px; color: Red;">&nbsp;*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSPFee" runat="server" MaxLength="5" CssClass="formFieldRight width60"
                                        onblur='FormatttedAmount(this.id); ValidateDiscount("UCAccountSettings1_txtSPFee");'
                                        Text="10.00" TabIndex="45"></asp:TextBox>&nbsp;%<br />
                                    <asp:RequiredFieldValidator ID="reqSPFees" runat="server" ControlToValidate="txtSPFee"
                                        ErrorMessage="Please enter the supplier fee" Display="None" ValidationGroup="VGAcc"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                     <%--'OA-622 - discount changes--%>
                    <asp:Panel runat="server" ID="pnlAdminFee" style="display:none">
                        <table width="100%" cellspacing="0" cellpadding="9" border="0">
                            <tr>
                                <td class="gridTextNew" style="width: 200px;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Admin fee (%)<span style="width: 10px;
                                        font-size: 12px; color: Red;">&nbsp;*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAdminFee" runat="server" MaxLength="5" CssClass="formFieldRight width60"
                                        onblur='FormatttedAmount(this.id); ValidateDiscount("UCAccountSettings1_txtAdminFee");'
                                        Text="10.00" TabIndex="45"></asp:TextBox>&nbsp;%<br />
                                    <asp:RequiredFieldValidator ID="reqtxtAdminFee" runat="server" ControlToValidate="txtAdminFee"
                                        ErrorMessage="Please enter the Admin fee" Display="None" ValidationGroup="VGAcc"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="gridTextNew" style="width: 200px;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Company<span style="width: 10px;
                                        font-size: 12px; color: Red;">&nbsp;*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCompanyType" TabIndex="33" runat="server" CssClass="formField150"  onchange='javascript:SetCompanyType(this.id);'>
                                    </asp:DropDownList>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlPrefSP" Visible="False">
                        <span style="padding-left: 10px;">
                            <input runat="server" type="checkbox" id="chkPrefSP" tabindex="46" />&nbsp;<span
                                class="txtNavigation">Preferred Supplier Pricing</span></span></asp:Panel>
                    <div class="WorkOrderBottomCurve">
                        <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                            style="display: none" /></div>
                </div>
                <div class="divWorkOrder">
                    <div class="WorkOrderTopCurve">
                        <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Appointment
                            Time Settings</strong></div>
                    <table width="100%" cellspacing="0" cellpadding="9" border="0">
                        <tr>
                            <td width="190px">
                                <input runat="server" type="checkbox" id="chkTimeSlot1" style="vertical-align: middle;"
                                    onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAccountSettings1_rfvTimeSlot1","","ctl00_ContentPlaceHolder1_UCAccountSettings1_spnTimeSlot1","","")'
                                    tabindex="47" />&nbsp;<asp:TextBox ID="txtTimeSlot1" ValidationGroup="VGAcc" MaxLength="50"
                                        TabIndex="48" runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                            font-size: 12px; color: Red; display: none;" id="spnTimeSlot1" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ValidationGroup="VGAcc" ID="rfvTimeSlot1" runat="server"
                                    ControlToValidate="txtTimeSlot1" Display="None" Enabled="false" ErrorMessage="Time Slot 1 cannot be blank">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="rgvTimSlot1" runat="server" ControlToValidate="txtTimeSlot1" Display="None"
                                        ValidationExpression="^[a-zA-Z0-9 :.-]{1,20}$" ValidationGroup="VGAcc" ErrorMessage="Please enter the time slot 1 in proper format"></asp:RegularExpressionValidator>
                                <input runat="server" type="checkbox" id="chkTimeSlot2" style="vertical-align: middle;"
                                    onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAccountSettings1_rfvTimeSlot2","","ctl00_ContentPlaceHolder1_UCAccountSettings1_spnTimeSlot2","","")'
                                    tabindex="48" />&nbsp;<asp:TextBox ID="txtTimeSlot2" ValidationGroup="VGAcc" MaxLength="50"
                                        TabIndex="49" runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                            font-size: 12px; color: Red; display: none;" id="spnTimeSlot2" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ValidationGroup="VGAcc" ID="rfvTimeSlot2" runat="server"
                                    ControlToValidate="txtTimeSlot2" Display="None" Enabled="false" ErrorMessage="Time Slot 2 cannot be blank"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="rgvTimeSlot2" runat="server" ControlToValidate="txtTimeSlot2" Display="None"
                                        ValidationExpression="^[a-zA-Z0-9 :.-]{1,20}$" ValidationGroup="VGAcc" ErrorMessage="Please enter the time slot 2 in proper format"></asp:RegularExpressionValidator>
                                <input runat="server" type="checkbox" id="chkTimeSlot3" style="vertical-align: middle;"
                                    onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAccountSettings1_rfvTimeSlot3","","ctl00_ContentPlaceHolder1_UCAccountSettings1_spnTimeSlot3","","")'
                                    tabindex="50" />&nbsp;<asp:TextBox ID="txtTimeSlot3" ValidationGroup="VGAcc" MaxLength="50"
                                        TabIndex="51" runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                            font-size: 12px; color: Red; display: none;" id="spnTimeSlot3" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ValidationGroup="VGAcc" ID="rfvTimeSlot3" runat="server"
                                    ControlToValidate="txtTimeSlot3" Display="None" Enabled="false" ErrorMessage="Time Slot 3 cannot be blank"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="rgvTimeSlot3" runat="server" ControlToValidate="txtTimeSlot3" Display="None"
                                        ValidationExpression="^[a-zA-Z0-9 :.-]{1,20}$" ValidationGroup="VGAcc" ErrorMessage="Please enter the time slot 3 in proper format"></asp:RegularExpressionValidator>
                                <input runat="server" type="checkbox" id="chkTimeSlot4" style="vertical-align: middle;"
                                    onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAccountSettings1_rfvTimeSlot4","","ctl00_ContentPlaceHolder1_UCAccountSettings1_spnTimeSlot4","","")'
                                    tabindex="50" />&nbsp;<asp:TextBox ID="txtTimeSlot4" ValidationGroup="VGAcc" MaxLength="50"
                                        TabIndex="51" runat="server" CssClass="formField width150"></asp:TextBox><span style="width: 10px;
                                            font-size: 12px; color: Red; display: none;" id="spnTimeSlot4" runat="server">&nbsp;*</span><br />
                                <asp:RequiredFieldValidator ValidationGroup="VGAcc" ID="rfvTimeSlot4" runat="server"
                                    ControlToValidate="txtTimeSlot4" Display="None" Enabled="false" ErrorMessage="Time Slot 4 cannot be blank"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="rgvTimeSlot4" runat="server" ControlToValidate="txtTimeSlot3" Display="None"
                                        ValidationExpression="^[a-zA-Z0-9 :.-]{1,20}$" ValidationGroup="VGAcc" ErrorMessage="Please enter the time slot 4 in proper format"></asp:RegularExpressionValidator>
                                <asp:CustomValidator runat="server" ID="custValTimeSlots" ValidationGroup="VGAcc"
                                    Display="None" ErrorMessage="Time slots cannot be duplicated"></asp:CustomValidator>
                                <asp:CustomValidator runat="server" ID="custValEvngTime" ValidationGroup="VGAcc"
                                    Display="None" ErrorMessage="Time slot cannot have evening installation time"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                    <div class="WorkOrderBottomCurve">
                        <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                            style="display: none" /></div>
                </div>
                <asp:Panel ID="pnlSuccess" runat="server" Visible="false">
                    <table width="500" border="0" cellspacing="0" cellpadding="10" height="100">
                        <tr>
                            <td align="center">
                                <asp:Label runat="server" ID="lblMsg"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td align="left" class="HeadingRed">
                            &nbsp;
                        </td>
                        <td align="left" class="padTop17">
                            <table id="tblSaveBottom" runat="server" cellspacing="0" cellpadding="0" width="100%"
                                border="0" style="padding: 0px 0px 16px 0px; margin: 0px;">
                                <tr>
                                    <td align="right" valign="top">
                                        <table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                                            <tr>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" />
                                                </td>
                                                <td width="110" class="txtBtnImage">
                                                    <asp:LinkButton ID="btnSaveBottom" ValidationGroup="VGAcc" CssClass="txtListing"
                                                        runat="server" TabIndex="63"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton>
                                                </td>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="10">
                                    </td>
                                    <td width="125" align="right" valign="top" style="padding: 0px 0px 0px 0px;">
                                        <table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                                            <tr>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" />
                                                </td>
                                                <td width="115" class="txtBtnImage">
                                                    <asp:LinkButton ID="btnResetBottom" CausesValidation="false" runat="server" TabIndex="64"
                                                        class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton>
                                                </td>
                                                <td width="5">
                                                    <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</asp:Panel>
