 <%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCRegistrantListing.ascx.vb" Inherits="Admin.UCRegistrantListing" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
            <div id="divListingContainer" style="background-color:#DAD8D9">
								
									  <div id="divHideListing" runat="server" class="divProfileListing">
									  <div id="divProfilePaging" style="border:solid 1px #ffffff; text-align:right;">
 									    <%@ Register TagPrefix="uc1" TagName="GridPager" Src="../../UserControls/Admin/GridPager.ascx" %>
										<uc1:GridPager id="Group1_GridPagerTop" runat="server" ></uc1:GridPager></div>
										<div id="divListing" >
                                            <asp:GridView ID="gvRegistrants" runat="server" AllowPaging=false AllowSorting=true AutoGenerateColumns=false BorderWidth="1px" BorderColor="White" ForeColor="#555354">
                                                <Columns>
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridHdr gridText" DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridHdr gridText" DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="170px" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridHdr gridText" DataField="Email" HeaderText="Email Address" SortExpression="Email" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="210px" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridHdr gridText" DataField="CName" HeaderText="Company Name" SortExpression="CompanyName" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridHdr gridText" DataField="Position" HeaderText="Position" SortExpression="Position" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridHdr gridText" DataField="MarketingActivity" HeaderText="Marketing Activity" SortExpression="MarketingActivity" />
                                                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridHdr gridText" DataField="DateRegistered" HeaderText="Date Registered" DataFormatString="{0:dd/MM/yy}" SortExpression="DateRegistered" />
                                                </Columns>                                            
                                            </asp:GridView>
													<div id="divClearBoth"></div>
												</div>
												<div id="div2" style="border:solid 1px #ffffff; text-align:right;"><uc1:GridPager id="Group1_GridPagerBtm" runat="server"></uc1:GridPager>
												</div>
												
									  </div>										
								</div>

    </ContentTemplate>

</asp:UpdatePanel>
<style>
 #divOuterGrid
 {
    width:300px;
 }
</style>