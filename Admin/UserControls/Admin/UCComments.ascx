<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCComments.ascx.vb" Inherits="Admin.UCComments" %>

<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
  <%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>

 <div id="divValidationMain" class="divValidation" runat=server >
      <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="15" align="center">&nbsp;</td>
          <td height="15">&nbsp;</td>
          <td height="15">&nbsp;</td>
        </tr>
        <tr valign="middle">
          <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
          <td class="validationText"><div  id="divValidationMsg"></div>
              <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
              <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
          </td>
          <td width="20">&nbsp;</td>
        </tr>
        <tr>
          <td height="15" align="center">&nbsp;</td>
          <td height="15">&nbsp;</td>
          <td height="15">&nbsp;</td>
        </tr>
      </table>
      <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
    </div>

<div class="tabDivOuter" style="margin:0px 0px 0px 0px" id="divMainTab">
	<div class="roundtopLightGreyCurve"><img src="../Images/Curves/LightGrey-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
       		<DIV class="paddedBox" id="divReferences">
						
	
   					
    <asp:Panel ID="pnlEnterComment" runat="server" class="paddingt17">
    
        <table id="tblProfile" runat="server" height="18" cellspacing="0" cellpadding="0" width="100%" border="0" > 
          <tr valign="top">
            <td class="formTxt" align="left" height="14">Enter Comments: </td>
          </tr>
          <tr valign="top">
            <td align="left" height="66"><asp:TextBox ID="txtComment" TabIndex="30" runat="server" CssClass="formFieldGrey" width="100%" height="60px" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox>
           
            </td>
          </tr>
            <tr valign="top">
            <td class="formTxt" align="left" height="14">Reason:</td>
          </tr>
          <tr valign="top">
            <td align="left" > <asp:DropDownList ID="ddlCommentReason" AutoPostBack="false" width= "20%" TabIndex="14" runat="server" CssClass="formField90"></asp:DropDownList>
              
                          </td>
          </tr>
          <tr valign="top">
            <td align="right" valign="bottom">
                    <table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></td>
                          <td width="50" class="txtBtnImage"><asp:LinkButton ID="btnSubmit" CausesValidation="false"   CssClass="txtListing" runat="server" TabIndex="51"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Submit&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></td>
                        </tr>
                    </table>
            </td>
          </tr>
        </table>
        
        <uc2:UCFileUpload id="UCFileUpload8" runat="server" Control="UCSpecialistsForm1_UCComments_UCFileUpload8" 
		Type="ContactComment" 
		AttachmentForSource="ContactComment" ShowExistAttach = "False" 
		ShowNewAttach="True" ShowUpload="True" 
		MaxFiles="1"></uc2:UCFileUpload> 
   
    </asp:Panel>	
    <div style="padding-top:20px;">
		<p><b class="formTxt" >Attachments can only be submitted with a relevant comment</b></p>
	</div>					
    <asp:Panel ID="pnlResponse" runat="server">		
					 <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                      
                        <td align="left" valign="top">
				            <asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLResponse" Runat="server" Width="100%" >
				            <HeaderTemplate>
					            <table width="100%" cellspacing="0" cellpadding="0">
						             <tr id="tblDGYourResponseHdr" align="left">
                                          <td width="5" valign="top" style="padding:0px; border:0px; "><img src="Images/Curves/Bdr-Red-TL.gif" width="5" height="5" hspace="0" vspace="0" border="0"></td>
                                          <td width="150px">Comment By</td>
                                          <td >Summary</td>
                                          <td width="100" >Attachments</td>
                                          <td width="100" >Comment Date</td>
                                          <td width="200" style="border-right: 0px">Reason</td>
                                          <td width="5" align="right" valign="top" style="padding:0px; border:0px;"><img src="Images/Curves/Bdr-Red-TR.gif" width="5" height="5" hspace="0" vspace="0" border="0"></td>
                                     </tr>
					            </table>
				            </HeaderTemplate>
				   
                    				           
				            <ItemTemplate>
					            <table width="100%" cellspacing="0" cellpadding="0">
						            <tr id="tblDGRowInside" align="left">
                                          <td width="5" valign="top" style="padding:0px; border:0px; ">&nbsp;</td>
                                          <td width="150"><%# Container.DataItem("CommentByName") %></td>
                                          <td ><%# strings.Left(DataBinder.Eval(Container.DataItem, "Comments"),70) %>...</td>
                                          <td width="100">
                                          <asp:Literal ID="litAttachments" Text='<%#getAttachmentLinkage(Container.DataItem("FilePath"),Container.DataItem("Name"))%>' runat="server" />
                                          </td>
                                          <td width="100" ><%#Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateCreated"), DateFormat.ShortDate)%></td>
 						                 <td width="200" style="border-right: 0px"><%# Container.DataItem("Reason") %></td>
                                          <td width="5" align="right" valign="top" style=" border-right-width:0px; padding:0px;">&nbsp;</td>
                                      </tr>
					            </table>
					             <div id="divCommentsDetail" runat="server" style="VISIBILITY:hidden;  WIDTH: 920px; POSITION: absolute; ">
					             <table id="tblCommentDetails" runat="server" width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="#EDEDED" >
                                         <tr>
    						                <td width="10">&nbsp;</td>
    						                <td>
							                    <!--<div class="topCurveRightRemarks marginT2"><img src="../Images/Curves/Bdr-Box2-TL.gif"></div>-->
									                <table id="tblDiscussion" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#EDEDED">
            									    
									                     <tr>
										                   <td width="10px">&nbsp;</td>
										                   <td>
											               <table  border="0" cellspacing="0" cellpadding="0">
							                                         <tr runat=server visible='<%# iif( Trim(Container.DataItem("Comments")) = "" , "False", "True") %>'>
            							                                  <td valign="top" class="remarksAreaCollapse"><%# Container.DataItem("Comments") %>&nbsp;</td>
							                                         </tr>
										                   </table>
											                
											               								
            												
										                </td> <td width="10px">&nbsp;</td>
									                  </tr>
            									      
									                </table>
									                <!--<div  class="botCurveRightRemarks"><img src="../Images/Curves/Bdr-Box2-BL.gif"></div>-->
            								    
								                </td>
    						                <td width="10">&nbsp;</td>
  					                   </tr>
  						                <tr>
  						                  <td height="10" style="padding:0px; margin:0px"></td>
  						                  <td height="10" style="padding:0px; margin:0px"></td>
  						                  <td height="10" style="padding:0px; margin:0px"></td>
						                  </tr>
            					      
					            </table>
					            </div>
					             <table width="100%" id="tblCommentsCollapse"  border="0" cellspacing="0" cellpadding="0" bgcolor="#DFDFDF" runat="server" visible= <%#ShowHideComments(Container.DataItem("CommentID"))%>>
                              	              <tr>
                                                <td class="smallText" height="8" style="padding:0px; margin:0px"></td>
                                	            <td height="8" align="center" class="smallText" style="padding:0px; margin:0px" id="tdCollapse" runat="server">
                                	                <a href="" border="0" onClick='<%# "javascript:CollapseComments(" & Container.DataItem("CommentID") & ", this.id)" %>' runat="server" id="collapseDetails">                                	                    
                                	                    <img src="Images/Arrows/Grey-Arrow.gif" width="50" title='Expand' height="8" border="0" name='<%# "imgExp" & Container.DataItem("CommentID") %>'  id='<%# "imgExp" & Container.DataItem("CommentID") %>' onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow-Roll.gif')" onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow.gif');" style="position:static;" />
                    	                                <img src="Images/Arrows/Orange-Arrow-Col.gif" width="50" title='Collapse' height="8" border="0" name='<%# "imgCol" & Container.DataItem("CommentID") %>'  id='<%# "imgCol" & Container.DataItem("CommentID") %>' onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col-Roll.gif')" onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col.gif');" style="visibility:hidden; position:absolute;" />
                                	                </a>
                                	            </td>
	                                            <td class="smallText" height="8" style="padding:0px; margin:0px"></td>
                                              </tr>
                                             </table>
				            </ItemTemplate>
			            </asp:DataList>
				       
					
		 
			</td>
			
			 </tr>
			</table>
    </asp:Panel>
							

  </DIV>
</div>

		
				
	
	
			
			
	
  


