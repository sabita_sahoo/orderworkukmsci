<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCWebContentForm.ascx.vb" Inherits="Admin.UCWebContentForm" %>



<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc3" TagName="UCDualListBox" Src="~/UserControls/UK/UCDualListBox.ascx" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>


   <%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>


<script language="javascript" src="JS/Scripts.js"  type="text/javascript"></script>






  <asp:Panel ID="pnlSubmitForm" runat="server" Visible="true">

     <asp:panel id="pnlBackToListing" runat="server" >
  		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
                <TR>
                  <td width="115" class="txtWelcome paddingL10"><div id="divButton" style="width: 115px;  ">
                      <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                      <a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" alt="Back to Listing" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8"><strong>Back to Listing</strong></a>
                      <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                  </div></td>                  
                </TR>
       </TABLE>
     </asp:panel>

  
    <div id="divValidationMain" class="divValidation" runat=server >
      <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="15" align="center">&nbsp;</td>
          <td height="15">&nbsp;</td>
          <td height="15">&nbsp;</td>
        </tr>
        <tr valign="middle">
          <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
          <td class="validationText"><div  id="divValidationMsg"></div>
              <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
              <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" ></asp:ValidationSummary>
          </td>
          <td width="20">&nbsp;</td>
        </tr>
        <tr>
          <td height="15" align="center">&nbsp;</td>
          <td height="15">&nbsp;</td>
          <td height="15">&nbsp;</td>
        </tr>
      </table>
      <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
    </div>
    
    
    <div class="tabDivOuter" style="margin:0px 0px 0px 0px" id="divMainTab">
      <div class="roundtopLightGreyCurve">
        <img src="Images/Curves/LightGrey-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" />
      </div>
      <div class="paddedBox"  id="divProfile">
      
      
      <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
                      <TR>
                        <TD align="left" class="HeadingRed"><TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
                      <TR>
                        <TD align="left" class="HeadingRed">
                            Site Content
                        </TD>
                        <TD align="left" class="padTop17"><TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
                          <TR>
                            <TD vAlign="bottom" align="right"><TABLE cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD width="110" class="txtBtnImage"><asp:LinkButton id="btnSaveTop" CausesValidation="false"  AccessKey="S"   runat="server"  TabIndex="40" CssClass="txtListing"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;<u>S</u>ave Changes&nbsp;</asp:LinkButton></TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                            </TABLE></TD>
                            <TD width="10"></TD>
                            <TD width="125" align="right" vAlign="top"><TABLE  width="125" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD class="txtBtnImage"><asp:LinkButton id="btnResetTop" runat="server" AccessKey="U" TabIndex="41" CausesValidation="false"   class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;<u>U</u>ndo Changes&nbsp;</asp:LinkButton></TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                            </TABLE></TD>
							<TD width="10"></TD>
                        <TD vAlign="top" align="right" width="60" runat="server" id="tdExit"><TABLE width="60" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0" >
                             <TR>
                              <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                              <TD class="txtBtnImage" >
							  <a id="btnCancelTop" TabIndex="42" class="txtListing" runat="server">
							     <img src="Images/Icons/Icon-Cancel.gif" width="11" height="10" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</a>
							  </TD>
                              <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                            </TR>
                        </TABLE></TD>
                          </TR>
                        </TABLE></TD>
						
                      </TR>
                    </TABLE></TD>
                      </TR>
					   
        </TABLE>
                    
 
        
        
         <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="175" height="17" align="left" valign="top" class="formTxt">Content Type
                    <span class="bodytxtValidationMsg" id="ddlContentTypeIndicator" runat="server"> *</span>
                
                 <asp:RequiredFieldValidator ID="rqddlContentType" runat="server" ErrorMessage="Please select content type."
																	ForeColor="#EDEDEB" ControlToValidate="ddlContentType">*</asp:RequiredFieldValidator>
                       
                </td>
                <td width="175" height="17" align="left" valign="top" class="formTxt">Publish</asp:label>
                </td>
                <td width="175" height="17" align="left" valign="top" class="formTxt"><asp:label ID="lblSite" runat="server" Text="Site" Visible="true"></asp:label>
                </td>
                <td width="100">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td width="175">
                    <span class="formTxt">
                            <asp:DropDownList id="ddlContentType" AutoPostBack="true"   tabIndex="56" runat="server"  CssClass="formField width160"></asp:DropDownList>
                    </span>
                </td>
                <td width="175">
                    <span class="formTxt">
                            <asp:DropDownList id="ddlPublish" AutoPostBack="false"   tabIndex="57" runat="server"  CssClass="formField width160">
                                <asp:ListItem Value="0">None</asp:ListItem>
                                <asp:ListItem Value="1">Publish On Stage</asp:ListItem>
                                <asp:ListItem Value="2">Publish On Live</asp:ListItem>
                            </asp:DropDownList>
                    </span>
                </td>
                <td width="175">
                    <span class="formTxt">
                            <asp:DropDownList id="ddlSiteType" AutoPostBack="true"  tabIndex="58" runat="server"  CssClass="formField width160"></asp:DropDownList>
                    </span>
                </td>
                
                                 
                <TD width="100" align="left" valign="middle" class="formTxt">
                    <asp:CheckBox id="ChkShowOnSite" tabIndex="59" runat="server" CssClass="formTxt" Text="Publish to Site"
				    TextAlign="right" Checked="false"></asp:CheckBox>
			        
			        
			    </td>
			    
                
                
              </tr>
              <tr class="smallText">
                <TD width="175" height="8" align="left" valign="top" class="smallText">&nbsp;</TD>
                <TD width="175" height="8" align="left" valign="top" class="smallText">&nbsp;</TD>
                <TD width="100" height="8" align="left" valign="top" class="smallText">&nbsp;</TD>
                <TD height="8" align="left" valign="top" class="smallText">&nbsp;</TD>
              </tr>
        </table>
        
       <asp:panel id="pnlTitle" runat="server" Visible="true" >
            <table height="38" cellspacing="0" cellpadding="0" border="0">
              <tr valign="top" >
                <td class="formTxt" align="left" width="600" height="14">Title
                <span class="bodytxtValidationMsg" id="ddlformTxtTypeIndicator" runat="server"> *</span>
                </td>
                <td  height="14" align="left" class="formTxt">&nbsp;</td>
              </tr>
              <tr valign="top">                
                <td align="left" ><asp:TextBox ID="txtTitle" TabIndex="65" runat="server" CssClass="formField" width="550px"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="rfdTitle" runat="server" ErrorMessage="Please enter Title."
                                  ForeColor="#EDEDEB" ControlToValidate="txtTitle">*</asp:RequiredFieldValidator>



                </td>
                 
				 <td  height="14" align="left" class="formTxt">&nbsp;</td>
              </tr>
            </table>
        </asp:panel>
        
          
   
        
 
					
   
		<asp:panel id="pnlCompanyDetails" runat="server" Visible="false">
		
    		  <asp:Panel ID="pnlCompanyData" runat="server" visible="True" >	 
                                    <table height="38" cellspacing="0" cellpadding="0" border="0">
                                      <tr valign="top" >
                                            <asp:panel id="pnlCmpHeader" runat="server" style="display:none;">
                                                   <td class="formTxt" align="left" width="175" height="14" >Company Name
                                                    </td>
                                                   <td align="left"  class="formTxt" width="175">Name
                                                    <span class="bodytxtValidationMsg" id="ddlNameTypeIndicator" runat="server"> *</span>
                                                   </td>
                                            </asp:panel>
                                                     <td align="left"  class="formTxt" width="175" >Job Title 
                                                      
                                                     </td>
                                                <td id="Td1"  width="175" height="14" align="left" class="formTxt" runat="server" visible="false">Account Type
                                                    <span class="bodytxtValidationMsg" id="ddlAccountTypeIndicator" runat="server"> *</span>
                                                  </td>
                                      </tr>
                                                    <tr valign="top">
                                           <asp:panel id="pnlCmpText" runat="server" style="display:none;">
                                                     <td align="left" height="24" ><asp:TextBox ID="txtCompanyName" TabIndex="12" runat="server" CssClass="formField" width="160px"></asp:TextBox>

                                                     <asp:RequiredFieldValidator ID="rfdCompanyName" Enabled="false" runat="server" ErrorMessage="Please enter Company Name."
                                                            ForeColor="#EDEDEB" ControlToValidate="txtCompanyName">*</asp:RequiredFieldValidator>
                                             </td>
                                                      <td align="left" >
                                                      <asp:TextBox ID="txtName" TabIndex="12" runat="server" CssClass="formField" width="160px"></asp:TextBox>
                                                      <asp:RequiredFieldValidator ID="rfdName" runat="server" ErrorMessage="Please enter Name."
                                                             ForeColor="#EDEDEB" ControlToValidate="txtName">*</asp:RequiredFieldValidator>
                                                      </td> 
                                                </asp:panel>
                                                      <td align="left" ><asp:TextBox ID="txtJobTitle" TabIndex="12" runat="server" CssClass="formField" width="160px"> </asp:TextBox>
                                             
                                                       <asp:RequiredFieldValidator ID="rfdJobTitle" Enabled="false" runat="server" ErrorMessage="Please enter Job Title."
                                                          ForeColor="#EDEDEB" ControlToValidate="txtJobTitle">*</asp:RequiredFieldValidator>

                                                      </td>
													  <td  id="tdAccountType" align="left" runat="server">
               
                                                  <asp:DropDownList id="ddlAccountType" AutoPostBack="false" Visible="False"  tabIndex="56" runat="server"  CssClass="formField width160"></asp:DropDownList>  
                                                  <asp:RequiredFieldValidator ID="rfdAccountType" runat="server" ErrorMessage="Please select Account Type."
                                                                ForeColor="#EDEDEB" ControlToValidate="ddlAccountType" Enabled="false">*</asp:RequiredFieldValidator>


                </td>
                                      
                                          </tr>
                                    </table>
            
              </asp:panel>
                                                                
                <div id="divChkBoxes"  class="formTxt" runat="server" visible="false" style="width:690px; margin-top:10px;"  >
                    <table cellSpacing="0" cellPadding="0" width="100%" border="0">
                     <%-- <tr>
                        <td align="left" class="HeadingRed" style="padding-top:20px;padding-bottom:0px;">Install Works</td>                        
                      </tr>
                      <tr>
                          <td>
                           <asp:CheckBox id="ChkInstallWorksHome" tabIndex="58" runat="server" CssClass="formTxt" Text="Install Works Home"
				           TextAlign="right" style="float:left;" Checked="false" visible="true"></asp:CheckBox>
				           <asp:CheckBox id="ChkInstallWorksServices" tabIndex="58" runat="server" CssClass="formTxt" Text="Install Works Services"
				           TextAlign="right" style="float:left; margin-left:10px" Checked="false" visible="true"></asp:CheckBox>
                          </td>
                      </tr>
                      <tr>
                        <td align="left" class="HeadingRed" style="padding-top:20px;padding-bottom:0px;">Retail</td>                        
                      </tr>--%>
                       <tr>
                          <td>
                          <input id="hdnQuoteId" type="hidden" name="hdnQuoteId" runat="server" value="0" /> 
                            <asp:DataList ID="DatalistRetailQuotes" runat="server"  >
                             <ItemTemplate>                             
                              <asp:CheckBox runat="server" id="Quote" Checked='<%#IIf(Container.DataItem("ShowOnSite") = "1","True","False") %>' CssClass="formTxt" 
				              TextAlign="right" style="float:left;" Text='<%#container.dataitem("StandardValue")%>' />
				               <INPUT id="hdnQuoteId" type="hidden" name="hdnQuoteId" runat="server" value='<%#(Container.DataItem("standardid"))%>' />	        
				              <br />
                            </ItemTemplate>    
                           </asp:DataList>
                          
                         <%--  <asp:CheckBox id="ChkRetailHome" runat="server" CssClass="formTxt" Text="Home"
				           TextAlign="right" style="float:left;" Checked="false" visible="true"></asp:CheckBox>	
				           <asp:CheckBox id="ChkRetailHomePCInstallation"  runat="server" CssClass="formTxt" Text="Home PC Installation"
				           TextAlign="right" style="float:left; margin-left:10px" Checked="false" visible="true"></asp:CheckBox>
				           <asp:CheckBox id="ChkRetailCEAndTVInstalls"  runat="server" CssClass="formTxt" Text="CE and TV Installs"
				           TextAlign="right" style="float:left; margin-left:10px" Checked="false" visible="true"></asp:CheckBox>
				           <asp:CheckBox id="ChkRetailFreesatAndDigitalAerials"  runat="server" CssClass="formTxt" Text="Freesat and Digital Aerials"
				           TextAlign="right" style="float:left; margin-left:10px" Checked="false" visible="true"></asp:CheckBox>
				           <asp:CheckBox id="ChkRetailWhyOrderwork"  runat="server" CssClass="formTxt" Text="Why Orderwork"
				           TextAlign="right" style="float:left; margin-left:10px" Checked="false" visible="true"></asp:CheckBox>
				           <asp:CheckBox id="ChkRetailMyOrderWorkRetail"  runat="server" CssClass="formTxt" Text="My OrderWork Retail"
				           TextAlign="right" style="float:left;margin-left:10px" Checked="false" visible="true"></asp:CheckBox>	
				            <asp:CheckBox id="ChkRetailServicePartners"  runat="server" CssClass="formTxt" Text="Suppliers"
				           TextAlign="right" style="float:left;" Checked="false" visible="true"></asp:CheckBox>
				           <asp:CheckBox id="ChkRetailRetailers"  runat="server" CssClass="formTxt" Text="Retailers"
				           TextAlign="right" style="float:left; margin-left:10px" Checked="false" visible="true"></asp:CheckBox>--%>
                          </td>
                      </tr>
                      <%-- <tr>
                        <td align="left" class="HeadingRed" style="padding-top:20px;padding-bottom:0px;">IT</td>                        
                      </tr>
                       <tr>
                          <td>
                           <asp:CheckBox id="ChkITHome"  runat="server" CssClass="formTxt" Text="Home"
				           TextAlign="right" style="float:left;" Checked="false" visible="true"></asp:CheckBox>	
				           <asp:CheckBox id="ChkITDeliverables"  runat="server" CssClass="formTxt" Text="OrderWork Deliverables"
				           TextAlign="right" style="float:left; margin-left:10px" Checked="false" visible="true"></asp:CheckBox>
				           <asp:CheckBox id="ChkITInstallationDeploymentServices"  runat="server" CssClass="formTxt" Text="Installation & Deployment Services"
				           TextAlign="right" style="float:left; margin-left:10px" Checked="false" visible="true"></asp:CheckBox>
				           <asp:CheckBox id="ChkITIMACITSupportServices"  runat="server" CssClass="formTxt" Text="IMAC & IT Support Services"
				           TextAlign="right" style="float:left; margin-left:10px" Checked="false" visible="true"></asp:CheckBox>
				           <asp:CheckBox id="ChkITChannelIT"  runat="server" CssClass="formTxt" Text="IT Channel"
				           TextAlign="right" style="float:left; margin-left:10px" Checked="false" visible="true"></asp:CheckBox>
				           <asp:CheckBox id="ChkITMyOrderWorkOTS"  runat="server" CssClass="formTxt" Text="MyOrderWork OTS"
				           TextAlign="right" style="float:left;" Checked="false" visible="true"></asp:CheckBox>	
				            <asp:CheckBox id="ChkITServicePartners"  runat="server" CssClass="formTxt" Text="Suppliers"
				           TextAlign="right" style="float:left;" Checked="false" visible="true"></asp:CheckBox>				           
                          </td>
                      </tr>--%>
                    </table>
                </div>
               
               <table id="tblLogo" runat="server" height="18" cellspacing="0" cellpadding="0" width="550px" border="0" >
                                      <tr valign="top">
                                        <td class="formTxt" align="left" height="14">&nbsp;</td>
                                      </tr>
                                      <tr valign="top">
                                        <td class="formTxt" align="left" height="14"><asp:Label ID="lblAttachmentTitle" runat="server" CssClass="formTxt"></asp:Label></td>
                                      </tr>
                                      <tr valign="top">
                                        <td align="left" height="66">
                                        
                                         <div id="Div1" class="divAttachment" runat="server"  >
	                                        <div class="AttachtopVal"><img src="Images/Curves/Attach-TL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                        	
                        	                    <input type="hidden" runat="server" id="FileUploadType" />
                                                   <uc2:UCFileUpload id="UCFileUpload6" runat="server" Control="UCWebContent1_UCFileUpload6" 
                                                   
                                                    AttachmentForSource="Logo"
                                                    ShowNewAttach="True" ShowUpload="True" 
                                                    MaxFiles="1" ></uc2:UCFileUpload>
                                         
                                           <div class="AttachbottomVal"><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                                          </div>
                                         
                                        </td>
                                      </tr>
                                    </table>
        
        </asp:panel>
        
        
        <asp:panel id="pnlLocation" runat="server" Visible="false" style="display:none;">
            <table height="38" cellspacing="0" cellpadding="0" width="100%" border="0">
              <tr valign="top">
                <td width="175" align="left" class="formTxt"> Location
                  <span class="bodytxtValidationMsg" id="ddlLocationTypeIndicator" runat="server"> *</span>
                </td>
                <td width="175" height="14" align="left" class="formTxt">Job Type
                   <span class="bodytxtValidationMsg" id="ddlJobTypeIndicator" runat="server"> *</span>
                </td>
				
                <td align="left" class="formTxt">&nbsp;</td>
              </tr>
              <tr valign="top">
                <td width="162" align="left"><asp:TextBox ID="txtLocation" TabIndex="16" runat="server" CssClass="formField" width="160px"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="rfdLocation" runat="server" ErrorMessage="Please enter Location."
                                                              ForeColor="#EDEDEB" ControlToValidate="txtLocation">*</asp:RequiredFieldValidator>


                </td>
                <td width="162" height="24" align="left">
            <!--<asp:TextBox ID="txtJobType" TabIndex="17" runat="server" CssClass="formField" width="160px"></asp:TextBox> -->
                  
                  
                <asp:DropDownList id="ddlJobType" AutoPostBack="true"  tabIndex="56" runat="server"  CssClass="formField width160"></asp:DropDownList> 
                  <asp:RequiredFieldValidator ID="rfdJobType" runat="server" ErrorMessage="Please select Job Type."
                                                                ForeColor="#EDEDEB" ControlToValidate="ddlJobType">*</asp:RequiredFieldValidator>


                </td>
				
                <td align="left">&nbsp;</td>
              </tr>
            </table>
        </asp:panel>
        
        
        <table height="38" cellspacing="0" cellpadding="0" border="0">
          <tr valign="top" >
            <td class="formTxt" align="left" width="185" height="14" id="tdDisplayDate" runat="server">Display Date
             <span class="bodytxtValidationMsg" id="ddlDDTypeIndicator" runat="server"> *</span>
                <asp:RegularExpressionValidator ID="rqContentDate"  ControlToValidate="txtContentDate" 
                    ErrorMessage="Date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB" 
								    ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"  runat="server">*</asp:RegularExpressionValidator>
								
            
            </td>
																		
 
            <td align="left"  width="400px" style="margin-left:5px" class="formTxt" id="tdUrl" runat="server"> URL </td>
            
          </tr>
          <tr valign="top">
            <td align="left" height="24" id="tdtxtContentDate" runat="server"><asp:TextBox ID="txtContentDate" TabIndex="68" runat="server" CssClass="formField" width="160px"></asp:TextBox>
                	    <asp:RequiredFieldValidator ID="rfdDisplayDate" runat="server" ErrorMessage="Please enter display Date."
                                                                ForeColor="#EDEDEB" ControlToValidate="txtContentDate">*</asp:RequiredFieldValidator>
            <img alt="Click to Select" src="Images/calendar.gif" id="btnCalConDate" style="cursor:pointer; vertical-align:middle; margin-left:-5px" />                	
              <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnCalConDate TargetControlID="txtContentDate" ID="calConDate" runat="server">
              </cc1:CalendarExtender>
            
            </td>

            <td align="left" id="tdUrlField" runat="server" ><asp:TextBox ID="txtURL" TabIndex="70" runat="server" CssClass="formField width310" style="margin-left:5px">http://</asp:TextBox>
            </td>
            
          </tr>
        </table>
        
        <table id="tblProfile" runat="server" height="18" cellspacing="0" cellpadding="0" width="100%" border="0" >
          <tr valign="top">
           
            <td class="formTxt" align="left" height="14" ID="pnlTitleBodytext" runat="server">Body Text
               <span class="bodytxtValidationMsg" id="ddlBodyTxtTypeIndicator" runat="server"> *</span>
            
            </td>
            
          </tr>
          <tr valign="top">
            <td align="left" height="66">
            <asp:Panel ID="pnlFreeTxtBox" runat="server">
                      <FTB:FreeTextBox id="FTB1" runat="server" width="550px"></FTB:FreeTextBox>
                      
            <asp:TextBox ID="txtBody" visible="false" TabIndex="75" runat="server" CssClass="formFieldGrey width686height60" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox>

              <asp:RequiredFieldValidator ID="rfdBodyText" runat="server" ErrorMessage="Please enter Body Text" ForeColor="#EDEDEB" ControlToValidate="FTB1">*</asp:RequiredFieldValidator>
            </asp:Panel>
            </td>

          </tr>
        </table>
        
       
   
        <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
                      <TR>
                        <TD align="left" class="HeadingRed"><TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
                      <TR>
                        <TD align="left" class="HeadingRed">&nbsp;</TD>
                        <TD align="left" class="padTop17"><TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
                          <TR>
                            <TD vAlign="bottom" align="right"><TABLE cellSpacing="0" cellPadding="0" width="120" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD width="110" class="txtBtnImage"><asp:LinkButton id="btnSaveBottom" runat="server" CausesValidation="false" TabIndex="79" CssClass="txtListing"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:LinkButton></TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                            </TABLE></TD>
                            <TD width="10"></TD>
                            <TD width="125" align="right" vAlign="top"><TABLE  width="125" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
                              <TR>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                <TD class="txtBtnImage"><asp:LinkButton id="btnResetBottom" runat="server" TabIndex="80" CausesValidation="false" class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton></TD>
                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                              </TR>
                            </TABLE></TD>
							<TD width="10"></TD>
                        <TD vAlign="top" align="right" width="60" runat="server" id="tdExitBottom"><TABLE width="60" cellSpacing="0" cellPadding="0" bgColor="#F0F0F0" border="0">
                             <TR>
                              <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                              <TD class="txtBtnImage">
							  <a  id="btnCancelBottom" TabIndex="81" class="txtListing" runat="server">
							  <img src="Images/Icons/Icon-Cancel.gif" width="11" height="10" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</a></TD>
                              <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                            </TR>
                        </TABLE></TD>
                          </TR>
                        </TABLE></TD>
						
                      </TR>
                    </TABLE></TD>
                      </TR>
					   
        </TABLE>
			
     </div>
      </div>

          
        </asp:Panel>




            <!-- Confirm Action panel -->
          <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="padTop50">
              <tr>
                <td width="350" align="right"><table cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                    <tr>
                      <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                      <td class="txtBtnImage"><asp:LinkButton ID="btnConfirm" runat="server"  class="txtListing" TabIndex="53" > <img src="Images/Icons/Icon-Confirm.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Confirm </asp:LinkButton></td>
                      <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                    </tr>
                </table></td>
                <td width="10">&nbsp;</td>
                <td align="left" ><table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0" >
                    <tr>
                      <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                      <td align="center" class="txtBtnImage"><asp:LinkButton ID="btnCancel" runat="server" class="txtListing" TabIndex="54" > <img src="Images/Icons/Icon-Cancel.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
                      </td>
                      <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                    </tr>
                </table></td>
              </tr>
            </table>
          </asp:Panel>
  
        
  

  
  

          

