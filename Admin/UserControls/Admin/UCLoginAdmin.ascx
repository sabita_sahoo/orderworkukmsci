
<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCLogin.ascx.vb" Inherits="Admin.UCLogin"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>



<body onLoad="MM_preloadImages('Images/Btn-Login-Go-Roll.gif')">

  <asp:Panel ID="PnlLogin" runat="server"  defaultbutton="lnkBtnLogin">
 <asp:UpdatePanel ID="UpdatePnlLogin" runat="server"  UpdateMode="Conditional" RenderMode=Inline>
  <ContentTemplate>
 


<div align="center">
	<p class="txtHeading"><strong>
	<div id="divLogin" >
	<div class="roundtopNavRight"><img src="Images/Curves/Login-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
	
	<table width="100%" height="70" cellpadding="7" >
		<tr>
			<td valign="top" class="padRight20Left20"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td height="18" valign="top"><span class="txtTopNavigation"><strong>Login as OW Rep </strong></span><span class="txtGreySmall"></span></td>
		</tr>
		<tr>
		<td height="12" valign="top" class="txtGreySmall">Username</td>
		</tr>
		<tr>
		<td height="23" valign="top">
		
		
		<asp:TextBox ID="txtLoginUserName" TabIndex="1" CssClass="formField150" runat="server" ></asp:TextBox>
					 <cc1:TextBoxWatermarkExtender TargetControlID="txtLoginUserName" ID="TextBoxWatermarkExtender1" WatermarkText="Enter email address"  runat="server"/>
		
		</td>
		</tr>
		<tr>
		<td height="18" class="txtGreySmall">Password</td>
		</tr>
		<tr>
		<td height="18">
		
		<%--Poonam modified on 16/2/2016 - Task - OA-188 : OA - New password not recognised (happens when Password is not hidden (bullets))--%>
				<asp:TextBox TabIndex="2"   id="txtLoginPassword"  CssClass="formField150"  runat="server" TextMode="Password" ></asp:TextBox>	
		</tr>
		<tr>
				<td height="25" valign="bottom">
					<asp:CheckBox ID="chkRemember" runat="server" CssClass="txtGreySmall"  TabIndex="104" Text="Remember me" Checked="true"></asp:CheckBox>
				</td>
		</tr>
		<tr>
		<td height="38" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		
		<tr>
		<td>            
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
		        <asp:LinkButton cssclass="txtButtonNav" id="lnkBtnLogin" causesvalidation="false" runat="server" TabIndex="104">&nbsp;&nbsp;Login</asp:LinkButton>
            </div>
        </td>
		<td align="right"><!--a href="#" class="footerTxtSelected" tabindex="109">Forgot Password?</a--></td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td align="right">&nbsp;</td>
		</tr>
		</table>
		<asp:Label id="lblMsg" runat="server" CssClass="bodytxtValidationMsg"></asp:Label>
		</td>
		</tr>
		</table></td>
		</tr>
		</table>
		<p class="txtTopNavigation">&nbsp;</p></td>
		</tr>
	</table>
	</div>
	</p>
</div>



</ContentTemplate>
<Triggers>
<asp:PostBackTrigger  ControlID="lnkBtnLogin" />
</Triggers>
</asp:UpdatePanel>
</asp:Panel> 

</body>







