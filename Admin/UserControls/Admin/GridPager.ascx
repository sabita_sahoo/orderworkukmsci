<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GridPager.ascx.vb" Inherits="Admin.GridPager" %>
<div id="divOuterGrid" >
    <TABLE  border="0" cellPadding="5" cellSpacing="0" id="tblPager" runat="server">
	    <TR>
		    <td width="0" align="right" valign="middle" class="tdWidth"  >
		      <div id="tdTotalCount"  runat="server" class="divMessage"></div>
		    </td>
		    <td width="140" valign="middle" id="tdPager"  runat="server" class="tdOuterPager">
		    <table width="133" border="0" cellpadding="0" cellspacing="0" class="tblInnerPager">
			    <tr>
			        <td width="40" class="tdNextButtons" >
			              <div id="divDoubleBack" >
				             <asp:ImageButton CausesValidation=false ID="btnFirst" OnClick="GetFirstPage" ImageUrl="~/Images/Icons/first.gif" runat="server" />
			              </div>	
				            <div id="divBack" >
				             <asp:ImageButton CausesValidation=false ID="btnPrevious"  OnClick="GetPreviousPage" ImageUrl="~/Images/Icons/back.gif" runat="server" />
				            </div>																							   
			        </td>
			        <td width="53" align="center">													
                      <asp:DropDownList OnSelectedIndexChanged="ChangePage" CssClass="formField"  align="center" ID="ddPageNumber" runat="server" AutoPostBack="true"> </asp:DropDownList>
</td>
			        <td width="40" class="tdNextButtons" align="left" >			            
				            <div id="divNext" >
				                <asp:ImageButton CausesValidation=false ID="btnNext"  OnClick="GetNextPage" ImageUrl="~/Images/Icons/next.gif" runat="server" />
				            </div>
				            <div id="divDoubleNext" >
				                <asp:ImageButton CausesValidation=false ID="btnLast"  OnClick="GetLastPage" ImageUrl="~/Images/Icons/last.gif" runat="server" />
				            </div>				        
			        </td>
			    </tr>
		    </table>
		    </td>
	    </TR>
    </TABLE>
</div>