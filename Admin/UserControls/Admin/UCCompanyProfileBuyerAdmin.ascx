<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSCompanyProfile.ascx.vb" Inherits="Admin.UCMSCompanyProfile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc3" TagName="UCDualListBox" Src="~/UserControls/UK/UCDualListBox.ascx" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
   
   
      <%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>


<script language="javascript" src="JS/Scripts.js"  type="text/javascript"></script>


<asp:Panel ID="PnlCompanyProfile" runat="server"  >
 <asp:UpdatePanel ID="UpdatePnlCompanyProfile" runat="server"  UpdateMode="Conditional" RenderMode=Inline>
  <ContentTemplate>
 
 
 <asp:Panel ID="pnlSubmitForm" runat="server" visible="true">
 
		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
                <TR>
                  <td width="115" class="txtWelcome paddingL10"><div id="divButton" style="width: 115px;  ">
                      <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                      <a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" title="Back to Listing" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8"><strong>Back to Listing</strong></a>
                      <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                  </div></td>                  
                </TR>
        </TABLE>


<div id="divValidationMain" class="divValidation" runat=server >
	<div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="15" align="center">&nbsp;</td>
				<td height="15">&nbsp;</td>
				<td height="15">&nbsp;</td>
			</tr>
			<tr valign="middle">
				<td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				<td class="validationText"><div  id="divValidationMsg"></div>
				<asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
				<asp:ValidationSummary id="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
				</td>
				<td width="20">&nbsp;</td>
			</tr>
			<tr>
				<td height="15" align="center">&nbsp;</td>
				<td height="15">&nbsp;</td>
				<td height="15">&nbsp;</td>
			</tr>
		</table>
	<div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
</div>	






	 <div class="tabDivOuter" style="margin:0px 5px 0px 5px" id="divMainTab">
      <div class="roundtopLightGreyCurve"><img src="Images/Curves/LightGrey-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
      <div class="paddedBox"  id="divProfile">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td align="left" class="HeadingRed"><strong>Company Profile</strong></td>
            <td align="left" class="padTop17"><table id="tblSaveTop" runat=server cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                <tr>
                  <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></td>
                        <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveTop" OnClientClick="javascript:callSetFocusValidation('UCCompanyProfile1')" CausesValidation="false"  CssClass="txtListing" runat="server" TabIndex="34"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></td>
                      </tr>
                  </table></td>
                  <td width="10"></td>
                  <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></td>
                        <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetTop" CausesValidation="false" runat="server" TabIndex="35"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></td>
                      </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
		
        </table>
		<table id="tblAccountManager" runat="server" cellpadding="0" cellspacing="0" width="100%" border="0">
		  <tr>
		  <td width="6px">&nbsp;</td>
		  <td height="50px" width="155"><asp:DropDownList ID="ddlOWAccountManager" runat="server" CssClass="formField150"></asp:DropDownList></td>
		  <td width="6px">&nbsp;</td>
		  <td class="formTxt" valign="middle">Account Manager</td>
		  </tr>
		</table>
        
             
			
			
		<!--  Company Info -->	
			          <div class="divWorkOrder" >
							<div class="WorkOrderTopCurve"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/><strong>Company Information</strong></div>	
							<table  width="100%" cellspacing="0" cellpadding="9" border="0">
								<tr>
									<td>
										
										<TABLE height="18" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR vAlign="top">
												<TD class="formTxt" colspan="3" valign="top">
													<div id="showSendMail" runat="server">
														<p class="txtNavigation"><strong>An email is sent to the <SPAN><asp:Label id="lblUser1" runat="server"></asp:Label></SPAN> indicating him/her of the change in Account Status.</strong></p>
														<p class="txtNavigation">Can edit the text that is sent to the <SPAN><asp:Label id="lblUser2" runat="server"></asp:Label></SPAN> via email.<b>Please do not remove text "<AccountStatus>" - this will be replaced with the status to which users account has been changed.</b> 
														<asp:TextBox TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" ID="txtMessage" TabIndex="11" runat="server" CssClass="formFieldGrey150" Width="471" Height="50" style="width:684px; height:50px;"></asp:TextBox></p>
													</div>
												</TD>
											</TR>
										</TABLE>
										
										<TABLE height="18" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR >
													<TD class="formTxt" width="162" align="left" valign="bottom" height="14">Company Name<SPAN class="bodytxtValidationMsg" id="CompanyNameIndicator" runat="server"> *</SPAN>
													  <asp:RequiredFieldValidator id="rqCompanyName" runat="server" ErrorMessage="Please enter Company Name" ForeColor="#EDEDEB"
															ControlToValidate="txtCompanyName">*</asp:RequiredFieldValidator>
													</TD>
													<TD class="formTxt" width="162" align="left" valign="bottom" height="14">Business Area</TD>
													<TD align="left" class="formTxt" id="tdPartnerTitle" runat="server"  visible="False">
																Partner  <SPAN class="bodytxtValidationMsg" id="PartnerLevelIndicator" runat="server"> *</SPAN>
																	<asp:RequiredFieldValidator id="rqPartnerLevel" runat="server" ErrorMessage="Please select Partner Level" ForeColor="#EDEDEB"
																		ControlToValidate="ddlPartner">*</asp:RequiredFieldValidator>
																			
													</TD>
													<TD class="formTxt" align="left" valign="bottom"  runat="server" >Type</TD>
													
												</TR>
												<TR vAlign="top">
													<TD align="left" height="24"><asp:TextBox id="txtCompanyName" TabIndex="12" runat="server" CssClass="formField150"></asp:TextBox></TD>
													<TD align="left" height="24"><asp:dropdownlist id="ddlBusinessArea" TabIndex="12" runat="server" CssClass="formField150"></asp:dropdownlist></TD>
													<TD width="170" align="left" id="tdPartnerValue" runat="server" visible="False">
														<asp:DropDownList id="ddlPartner" tabIndex="13" runat="server" CssClass="formField150" visible="false"></asp:DropDownList>
													</TD>
													
													<TD align="left" height="24">
														<asp:DropDownList id="ddlType" AutoPostBack="true" tabIndex="14" runat="server" CssClass="formField90" OnSelectedIndexChanged="ShowPopup"></asp:DropDownList>
														 <%-- 'Poonam added on 12/8/2016 - Task - OA-300 : EA - Make add comment mandatory via pop-up when Deleting or Suspending a company--%>
                                                    <asp:LinkButton ID="lnkDummy" runat="server" Style="display: none" />
                                                    <asp:Button runat="server" ID="hdnBtnAction" CausesValidation="false" Height="0" Width="0" BorderWidth="0" Style="visibility: hidden;" />
                                                    <cc1:ModalPopupExtender ID="mdlComment" runat="server" TargetControlID="lnkDummy" PopupControlID="pnlComment" OkControlID="" CancelControlID="ancCancelAction" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False"></cc1:ModalPopupExtender>
                                                    <asp:Panel runat="server" ID="pnlComment" Width="340px" CssClass="pnlConfirmWOListingForAction" Style="display: none;">
                                                        <div id="div3">
                                                            <div class="divTopActionStyle"><b>Comment</b> </div>
                                                            <div class="divTopActionStyle">
                                                                Enter Comments&nbsp;<span class="bodytxtValidationMsg">*</span><br />
                                                                <asp:TextBox runat="server" ID="txtComment" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned" TextMode="MultiLine"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtComment" WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>
                                                                <br />
                                                                Reason&nbsp;<span class="bodytxtValidationMsg">*</span>
                                                                <br />
                                                                <asp:DropDownList ID="ddlCommentReason" AutoPostBack="false" TabIndex="14" runat="server" CssClass="formField90" Width="227px"></asp:DropDownList>
                                                                <br />
                                                            </div>
                                                            <div style="padding-left: 20px; padding-top: 10px; float: left; width: 305px;">

                                                                <div class="divButtonAction" style="width: 85px; float: left; cursor: pointer;">
                                                                    <div class="bottonTopGrey">
                                                                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" />
                                                                    </div>
                                                                    <a class="buttonText cursorHand" onclick='javascript:validate_required_comment("ctl00_ContentPlaceHolder1_UCCompanyProfile1_txtComment","ctl00_ContentPlaceHolder1_UCCompanyProfile1_ddlCommentReason","Please Enter Comment","Please Select Reason")' id="ancSubmitAction">Submit</a>
                                                                    <div class="bottonBottomGrey">
                                                                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" />
                                                                    </div>
                                                                </div>

                                                                <div class="divButtonAction" style="width: 85px; float: left; margin-left: 20px; cursor: pointer;">
                                                                    <div class="bottonTopGrey">
                                                                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" />
                                                                    </div>
                                                                    <a class="buttonText cursorHand" runat="server" id="ancCancelAction" onclick='javascript:refreshPage()'>Cancel</a>
                                                                    <div class="bottonBottomGrey">
                                                                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" />
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                    
                                                        
                                                        <asp:CheckBox CssClass="formTxt" ID="chkSendMail" Text="Send Mail" runat="server" AutoPostBack="true" Checked="true"></asp:CheckBox>
                                                        <asp:CheckBox CssClass="formTxt" ID="chkIsTestAccount" Text="Is Test Account" runat="server"></asp:CheckBox>&nbsp;
													</TD>												
												</TR>
										</TABLE>
										
										<TABLE height="18" cellSpacing="0" cellPadding="0" width="100%" border="0">
										  <TR vAlign="top">
											<TD class="formTxt" align="left" width="162" height="14">Company Registration No.</TD>
											<TD class="formTxt" align="left" width="324" height="14">URL </TD>
											<TD class="formTxt" align="left"  height="14">Year Business Started </TD>
										  </TR>
										    
										  <TR vAlign="top">
										   <TD align="left" height="24">
													<asp:TextBox id="txtCompanyRegNo" tabIndex="15" runat="server" CssClass="formField150"></asp:TextBox></TD>
											<TD align="left" height="24"><asp:TextBox id="txtCompanyURL" tabIndex="16" runat="server" CssClass="formField310">http://</asp:TextBox></TD>
										 	<TD class="formTxt" align="left" height="14"><asp:TextBox ID="txtBusinessStarted" TabIndex="17" runat="server" CssClass="formField width150"></asp:TextBox> </TD>
										  </TR>
										</TABLE>
										<TABLE height="18" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR vAlign="top">
												<TD class="formTxt" align="left" width="160" height="14">No. of Employees</TD>
												<TD class="formTxt" align="left" width="162" height="14">No. of Locations</TD>
												<TD height="14" align="left" class="formTxt"><span  runat="server" id="spIndustry">Industry</span>&nbsp;</TD>
												
											</TR>
											<TR vAlign="top">
												<TD width="160" height="24" align="left">
												<asp:DropDownList id="ddlNoOfEmployees" tabIndex="18" runat="server" CssClass="formField150"></asp:DropDownList></TD>
												<TD width="162" height="24" align="left"><asp:TextBox id="txtNoOfLocations" tabIndex="19" runat="server" CssClass="formField150"></asp:TextBox></TD>
												<TD height="24" align="left"><asp:DropDownList id="ddlIndustry" tabIndex="20" runat="server" CssClass="formField150"></asp:DropDownList></TD>
											  
											</TR>
											<TR vAlign="top">
											<TD width="160" align="left" class="formTxt">Credit Terms</TD>
											<TD width="162" align="left" class="formTxt">Payment Terms</TD>
											<td>&nbsp;</td>
											</tr>
											<tr><TD width="140" align="left"><asp:DropDownList id="ddlBuyerPaymentsTerms" tabIndex="34" runat="server" CssClass="formField150"></asp:DropDownList></TD>
											<td width="162" align="left"><asp:DropDownList id="ddlPayTerms" tabIndex="34" runat="server" CssClass="formField150"></asp:DropDownList></td>
											<td height="24">&nbsp;</td>
											</TR>
										 </TABLE>
										  
										
										  <table id="tblProfile" runat="server" height="40" cellspacing="0" cellpadding="0" width="100%" border="0">
											  <tr valign="bottom">
												<td class="formTxt" align="left" height="18">Profile</td>
											  </tr>
											  <tr valign="top">
												<td align="left" ><asp:TextBox ID="txtProfile" TabIndex="21" runat="server" CssClass="formField width686height60" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox></td>
											  </tr>
										 </table>
										
									</td>
								</tr>
							
							</table>								 
							<div class="WorkOrderBottomCurve"><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
					    </div>
					
		 
		  
		  
	<%--	  <!-- Company preferences	-->
		  <div class="divWorkOrder" >
				<div class="WorkOrderTopCurve"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/><strong>Company preferences</strong></div>	
					<table width="100%" cellspacing="0" cellpadding="9" border="0">
						<tr>
							<td>
								 <div id="divAccount" runat="server">									  
									 								  
									  <table cellspacing="0" cellpadding="0" width="100%" border="0">
										<tr>
										  <td height="14" colspan="5" valign="top" class="formTxt">&nbsp;</td>
										</tr>
										<tr>
										  <td height="14" colspan="5" valign="top" class="formTxt">List Companies with whom you do not wish to work with</td>
										</tr>
										<tr valign="bottom">
										  <td width="180" height="24"><asp:TextBox TabIndex="22" runat="server" ID="txtClient1" CssClass="formField width150"></asp:TextBox></td>
										  <td width="180" height="24"><asp:TextBox TabIndex="23" runat="server" ID="txtClient2" CssClass="formField width150"></asp:TextBox></td>
										  <td width="180" height="24"><asp:TextBox TabIndex="24" runat="server" ID="txtClient3" CssClass="formField width150"></asp:TextBox></td>
										  <td height="24"><asp:TextBox TabIndex="25" runat="server" ID="txtClient4" CssClass="formField width150"></asp:TextBox></td>
										</tr>
										<tr valign="bottom">
										  <td width="162" height="24"><asp:TextBox TabIndex="26" runat="server" ID="txtClient5" CssClass="formField width150"></asp:TextBox></td>
										  <td height="24"><asp:TextBox TabIndex="27" runat="server" ID="txtClient6" CssClass="formField width150"></asp:TextBox></td>
										  <td width="162" height="24"><asp:TextBox TabIndex="28" runat="server" ID="txtClient7" CssClass="formField width150"></asp:TextBox></td>
										  <td height="24"><asp:TextBox TabIndex="29" runat="server" ID="txtClient8" CssClass="formField width150"></asp:TextBox></td>
										</tr>
										
									  </table>
									  
									 </div>
							</td>
						</tr>
					</table>					
		 		<div class="WorkOrderBottomCurve"><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		  </div>--%>
          
         
		 
          
         <!--	Attachments	-->
		  <div class="divWorkOrder" >
				<div class="WorkOrderTopCurve"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/><strong>Attachments</strong></div>	
					<table width="100%" cellspacing="0" cellpadding="9" border="0">
						<tr>
							<td>
								<asp:Panel ID="pnlAttachment" runat="server" >
								  <table cellspacing="0" cellpadding="0" width="100%" border="0">									
																	
									<tr>
									  <td valign="top" align="left" height="18" class="formTxt">											  
										 <div id="Div2" class="divAttachment" runat=server  >
											<div class="AttachtopVal"><img src="Images/Curves/Attach-TL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
							
										   
										   <uc2:UCFileUpload id="UCFileUpload6" runat="server" Control="UCCompanyProfile1_UCFileUpload6" 
												ExistAttachmentSource="CompanyProfile" Type="Company" 
												AttachmentForSource="CompanyProfile"  UploadCount="1" 
												ShowExistAttach="False" ShowNewAttach="True" ShowUpload="True" 
												MaxFiles="12"></uc2:UCFileUpload>
										 <div class="AttachbottomVal"><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
									   </div>  												   
									  </td>
									</tr>
								  </table>							   
								</asp:Panel>
							</td>
						</tr>
					</table>
				
		  		<div class="WorkOrderBottomCurve"><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		  </div>  
      
         
          <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
              <td align="left" class="HeadingRed">&nbsp;</td>
              <td align="left" class="padTop17"><table id="tblSaveBottom" runat=server cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                  <tr>
                    <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></td>
                          <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveBottom" CausesValidation="false" OnClientClick="callSetFocusValidation('UCCompanyProfile1')"  CssClass="txtListing" runat="server" TabIndex="30"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></td>
                        </tr>
                    </table></td>
                    <td width="10"></td>
                    <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></td>
                          <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetBottom"  CausesValidation="false" runat="server" TabIndex="31"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></td>
                        </tr>
                    </table></td>
                  </tr>
              </table></td>
            </tr>
          </table>
       
      </div>
    </div>
  </asp:Panel>





        
        
        <!-- Confirm Action panel --> 
			<asp:panel ID="pnlConfirm" runat="server" visible="false">
			
			  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="padTop50">
                <tr>
                  <td width="350" align="right"><table cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td class="txtBtnImage"><asp:LinkButton id="btnConfirm" runat="server" CausesValidation="false" class="txtListing" TabIndex="32" > <img src="Images/Icons/Icon-Confirm.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Confirm&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                  <td width="10">&nbsp;</td>
                  <td align="left" ><table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0" >
                      <tr>
                 <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td align="center" class="txtBtnImage">
                            <asp:LinkButton id="btnCancel" runat="server" CausesValidation="false" class="txtListing" TabIndex="33" > <img src="Images/Icons/Icon-Cancel.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
                          </td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
            </asp:panel>


  		
  	

</ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID="btnSaveTop" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnResetTop" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnSaveBottom" EventName="Click" />
<asp:AsyncPostBackTrigger ControlID="btnResetBottom" EventName="Click" />
  <asp:AsyncPostBackTrigger ControlID="hdnBtnAction" EventName="Click" />

</Triggers>
</asp:UpdatePanel>
</asp:Panel> 
 <%--'Poonam added on 19/8/2016 - Task - OA-300 : EA - Make add comment mandatory via pop-up when Deleting or Suspending a company--%>
<script type="text/javascript">
    function validate_required_comment(field, ddlfield, alerttxt, ddlalerttext) {
        var text = document.getElementById(field).value;
        var ddl = document.getElementById(ddlfield);
        var ddltext = ddl.options[ddl.selectedIndex].text;
        if (text == null || text == "" || text == "Enter a new comment here") {
            alert(alerttxt);
        }
        else if (ddltext == "Select Comment Reason") {
            alert('Select Comment Reason');
        }
        else {
            document.getElementById("ctl00_ContentPlaceHolder1_UCCompanyProfile1_hdnBtnAction").click();
        }
    }
    function refreshPage() {
        window.location.reload();
    }
</script>
        
       

 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePnlCompanyProfile" runat="server">
        <ProgressTemplate>
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="PnlCompanyProfile" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />



        

        
       




        
       


