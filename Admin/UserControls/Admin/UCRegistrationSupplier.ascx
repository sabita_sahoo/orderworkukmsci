﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCRegistrationSupplier.ascx.vb" Inherits="Admin.UCRegistrationSupplier" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode=Conditional RenderMode=Inline >
 <ContentTemplate>

 <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnNextSubmit">
 <div id="divValidationMain" class="PopUpTailRegisterDiv" runat=server visible="false"  style="width:830px;">			                
  <img src="Images/Note_Image.jpg" align="left">
  <div style="margin-left:80px;">
    <asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
    <asp:ValidationSummary ValidationGroup="VG"  id="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" ></asp:ValidationSummary>									
    </div>
</div>

	<div class="LoginInfo"><b>Login-Info</b>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         First Name <span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         <asp:RequiredFieldValidator id="rqFName" runat="server" Display="None" ValidationGroup="VG" ErrorMessage="Please enter First Name" ForeColor="#EDEDEB" ControlToValidate="txtFName"></asp:RequiredFieldValidator>
	                                         </span>
	                                         <asp:TextBox ID="txtFName" CssClass="TxtLogin" runat="server" MaxLength="100"></asp:TextBox>
	                                         </div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         Last Name <span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         <asp:RequiredFieldValidator id="rqLName" ValidationGroup="VG" Display="None" runat="server" ErrorMessage="Please enter Last Name" ForeColor="#EDEDEB" ControlToValidate="txtLName"></asp:RequiredFieldValidator>
	                                         </span>
	                                         <asp:TextBox ID="txtLName" CssClass="TxtLogin" runat="server" MaxLength="100"></asp:TextBox>
	                                         </div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         Email address <span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         <asp:RequiredFieldValidator id="rqEmail" Display="None" ValidationGroup="VG" runat="server" ErrorMessage="Please enter Email Address" ForeColor="#EDEDEB" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
	                                         <asp:RegularExpressionValidator ID="rgEmail" Display="None" runat="server" ValidationGroup="VG" ControlToValidate="txtEmail" ErrorMessage="Please enter Email Address in correct format" ValidationExpression="^.+@[^\.].*\.[a-z]{2,}$"></asp:RegularExpressionValidator>
	                                         </span>
	                                         <asp:TextBox ID="txtEmail" CssClass="TxtLogin" runat="server" MaxLength="150"></asp:TextBox>
	                                         </div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                          Email address(Confirm) <span style="color:#FF0000 ">*</span>
	                                          <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                          <asp:RequiredFieldValidator ValidationGroup="VG" Display="None" id="rqConfirmEmail" runat="server" ErrorMessage="Please enter Confirm Email Address"   ControlToValidate="txtEmailConfirm"></asp:RequiredFieldValidator>
	                                          <asp:RegularExpressionValidator ID="rgConfirmEmail" runat="server" ValidationGroup="VG" Display="None" ControlToValidate="txtEmailConfirm" ErrorMessage="Please enter Confirm Email Address in correct format" ValidationExpression="^.+@[^\.].*\.[a-z]{2,}$"></asp:RegularExpressionValidator>
	                                          <asp:CompareValidator ID="conEmails" runat="server" ErrorMessage="Email IDs are not same"   ValidationGroup="VG" Display="None" ControlToCompare="txtEmail" ControlToValidate="txtEmailConfirm"></asp:CompareValidator>
	                                         </span>
	                                         <asp:TextBox ID="txtEmailConfirm" CssClass="TxtLogin" runat="server" MaxLength="150"></asp:TextBox>
	                                         </div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         Password <span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                        <%--'poonam modified on 4/12/2015 - Task - OA-131 :OA/OM - Strengthen automatically and manually generated passwords--%>
                                             <asp:RegularExpressionValidator ControlToValidate="txtPassword" ValidationGroup="VG"  ErrorMessage="Password must have at least 8 characters, at least one upper case letter, at least one lower case letter and at least one digit." ForeColor="#EDEDEB" ID="rgPassword" runat="server" ValidationExpression="^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?!.*[\W]).*$" >*</asp:RegularExpressionValidator>
                                             <asp:RequiredFieldValidator id="rqPassword" ValidationGroup="VG" Display="None" runat="server" ErrorMessage="Please enter Password"   ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
	                                         <asp:CompareValidator ID="CompareValidator1" runat="server" ValidationGroup="VG" Display="None" ErrorMessage="Password and Confirm Password are not same" ForeColor="#EDEDEB" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword"></asp:CompareValidator>
	                                         </span>
	                                         <asp:TextBox ID="txtPassword" CssClass="TxtLogin" TextMode="Password" runat="server" MaxLength="50"></asp:TextBox>
	                                         </div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         Password(Confirm)<span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         <asp:RequiredFieldValidator id="rqconPassword" ValidationGroup="VG" Display="None" runat="server" ErrorMessage="Please enter Confirm Password"   ControlToValidate="txtConfirmPassword"></asp:RequiredFieldValidator>
	                                         </span>
	                                         <asp:TextBox ID="txtConfirmPassword" TextMode="Password" CssClass="TxtLogin" runat="server" MaxLength="50"></asp:TextBox>
	                                         </div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         Security Question<span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         <asp:RequiredFieldValidator id="rqSecQuest" runat="server" ValidationGroup="VG" Display="None" ErrorMessage="Please select Security Question"   ControlToValidate="ddlQuestion">*</asp:RequiredFieldValidator>
	                                         </span>
	                                         <asp:DropDownList  ID="ddlQuestion" Width="354px" Height="21px" CssClass="TxtLogin" runat="server"></asp:DropDownList>
	                                         </div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         Security Answer<span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         <asp:RequiredFieldValidator id="rqSecAns" runat="server" ValidationGroup="VG" Display="None" ErrorMessage="Please enter Security Answer"   ControlToValidate="txtAnswer">*</asp:RequiredFieldValidator>
	                                         </span>
	                                         <asp:TextBox ID="txtAnswer" CssClass="TxtLogin" runat="server" MaxLength="100"></asp:TextBox>
	                                         </div>
	                                          <div class="Row">
                                             <%@ Register TagPrefix="uc1" TagName="TableOfSkillControl" Src="UCTableOfSkills.ascx" %>
<%--<uc1:TableOfSkillControl id="TableOfSkillControl01" runat="server"></uc1:TableOfSkillControl>--%>
                    <uc1:TableOfSkillControl ID="tbl01" runat="server" /></div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                     </div>
    <div class="displaynone">&nbsp;&nbsp;</div>
	<div class="LoginInfo"><b>Company Profile</b>
	                                     
	                                        <asp:UpdatePanel runat="Server" ID="updatePostcode" UpdateMode="conditional" RenderMode="inline">
                                            <ContentTemplate>
	                                        <div class="Row">
	                                            <span class="TxtName">
	                                                Company Name <span style="color:#FF0000 ">*</span>
	                                                <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                                <asp:RequiredFieldValidator id="rqCompanyName" ValidationGroup="VG" Display="None" runat="server" ErrorMessage="Please enter Company Name"   ControlToValidate="txtCompanyName"></asp:RequiredFieldValidator>
	                                            </span>
	                                            <asp:TextBox ID="txtCompanyName" CssClass="TxtLogin" runat="server" MaxLength="150"></asp:TextBox>
	                                         </div>
	                                         
                                    	     <div class="displaynone">&nbsp;&nbsp;</div>
                                                
                                                    <div class="Row">
	                                                <span class="TxtName">
	                                                    Postcode <span style="color:#FF0000 ">*</span>
	                                                    <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                                    <asp:RequiredFieldValidator id="rqPostCode" runat="server" ErrorMessage="Please enter Post Code" ForeColor="#EDEDEB" ControlToValidate="txtCompanyPostalCode" ValidationGroup="VG" Display="None"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="rgtxtCompanyPostalCode" Display="None" runat="server"
ControlToValidate="txtCompanyPostalCode" ErrorMessage="Please enter valid postcode" ValidationGroup="VG"
ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$"></asp:RegularExpressionValidator>
	                                                </span>
	                                                <asp:TextBox ID="txtCompanyPostalCode" onkeyup="makeUppercase(this.id)" style="width:75px;" CssClass="TxtLogin" runat="server" MaxLength="8" ></asp:TextBox>
	                                                <div class="clsmarginfloat" style="margin-left:5px;cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;line-height:20px !important;">
                                                        <asp:LinkButton runat="server" ID="btnFind" CausesValidation="false" Text="Get Address"  ToolTip="Get the addresses for the entered Postcode" CssClass="txtButtonRed" />	                                                           
                                                    </div>                                    
	                                             </div>
    	                                         
	                                             <div class="displaynone">&nbsp;&nbsp;</div>	                                         
    	                                         
    	                                         
	                                             <div class="Row" id="divAddList" runat="server" visible="false">
	                                                <span class="TxtName">	                                                
	                                                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>	                                                
	                                                </span>
	                                                <table>
	                                                    <tr>
	                                                        <td>
	                                                            <asp:ListBox runat="server" ID="lstProperties" CssClass="TxtName" AutoPostBack="true" style="width:354px; margin-top:5px; height:250px;"></asp:ListBox>
	                                                            <asp:Label runat="Server" ID="lblErr" style="color:#FF0000; font-size:11px; width:354px;text-align:left;"></asp:Label>
	                                                        </td>
	                                                    </tr>
	                                                </table>
	                                             </div>
	                                             <div class="displaynone">&nbsp;&nbsp;</div>
	                                         
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         Address <span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         <asp:RequiredFieldValidator id="rqAddress" runat="server" ValidationGroup="VG" Display="None" ErrorMessage="Please enter the Address"   ControlToValidate="txtCompanyAddress">*</asp:RequiredFieldValidator>
	                                         </span>
	                                         <asp:TextBox ID="txtCompanyAddress" CssClass="TxtLogin" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" MaxLength="250" Height="50px" runat="server"></asp:TextBox>
	                                         </div>
                                    	     <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         City <span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         <asp:RequiredFieldValidator id="rqCity" runat="server" ErrorMessage="Please enter City" ForeColor="#EDEDEB" ControlToValidate="txtCity" ValidationGroup="VG" Display="None"></asp:RequiredFieldValidator>
	                                         </span>
	                                         <asp:TextBox ID="txtCity" CssClass="TxtLogin" runat="server" MaxLength="50" ></asp:TextBox>
	                                         </div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         Country  
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>	                                         
	                                         </span>
	                                         <asp:TextBox ID="txtCounty" CssClass="TxtLogin" runat="server" MaxLength="50" ></asp:TextBox>
	                                         </div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                         </ContentTemplate>
                                           <Triggers>
                                           <asp:AsyncPostBackTrigger ControlID="btnFind" EventName="Click" />
                                           </Triggers>
                                           </asp:UpdatePanel>
	                                     <div class="Row">
	                                         <span class="TxtName">
	                                         Phone <span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         <asp:RequiredFieldValidator id="rqCompanyPhone" runat="server" ErrorMessage="Please enter Phone Number" ForeColor="#EDEDEB" ControlToValidate="txtCompanyPhone" ValidationGroup="VG" Display="None"></asp:RequiredFieldValidator>
	                                         </span>
	                                         <asp:TextBox ID="txtCompanyPhone" CssClass="TxtLogin" runat="server" MaxLength="50" ></asp:TextBox>
	                                         </div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         Fax
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         </span>
	                                         <asp:TextBox ID="txtCompanyFax" CssClass="TxtLogin" runat="server" MaxLength="50"  ></asp:TextBox>
	                                         </div>
	                                         <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         Website
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         </span>
	                                         <asp:TextBox ID="txtWebsiteURL" CssClass="TxtLogin" runat="server" MaxLength="100" ></asp:TextBox>
	                                         </div>
                                    	     <div class="displaynone">&nbsp;&nbsp;</div>
                                    	      <div class="Row">
	                                         <span class="TxtName">
	                                         Business Area<span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         <asp:RequiredFieldValidator id="reqValBusinessArea" runat="server" ValidationGroup="VG" Display="None" ErrorMessage="Please select Business Area"   ControlToValidate="ddlBusinessArea">*</asp:RequiredFieldValidator>
	                                         </span>
	                                         <asp:DropDownList  ID="ddlBusinessArea" Width="354px" Height="21px" CssClass="TxtLogin" runat="server"></asp:DropDownList>
	                                         </div>
	                                         <div class="Row">
	                                         <span class="TxtName">
	                                         Profile <span style="color:#FF0000 ">*</span>
	                                         <span class="displaynone">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	                                         <asp:RequiredFieldValidator id="rqProfile" runat="server" ErrorMessage="Please enter Company Profile" ForeColor="#EDEDEB" ControlToValidate="txtProfile" ValidationGroup="VG" Display="None"></asp:RequiredFieldValidator>
	                                         </span>
	                                         <asp:TextBox ID="txtProfile" CssClass="TxtLogin" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" Height="50px" runat="server"></asp:TextBox>
	                                         </div> 
	                                      <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row">
	                                         <div class="TxtName" style="width:146px;">
	                                         </div>
	                                       
	                                         <div style="margin-top:5px;"><asp:CheckBox runat="server" ID="ChkSubscribe" style="margin-left:146px; font-size:10px;" />Subscribe to Newsletter</div>
                                              
	                                         <div style="margin-top:5px;"><asp:CheckBox runat="server" ID="ChkIsTestAccount" style="margin-left:146px; font-size:10px;" />Is Test Account</div>
	                                         </div>
	                                             
                                    	     <div class="displaynone">&nbsp;&nbsp;</div>
	                                         <div class="Row" style="width:550px; padding-right:124px;">
	                                         <TABLE cellSpacing="0" cellPadding="0" style="float:right;" width="100" bgColor="#F0F0F0" border="0">
                                              <TR>
                                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
                                                <TD width="65" class="txtBtnImage"><img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;<asp:LinkButton runat="server" ID="btnNextSubmit" Text="Register" OnClick="RegisterMe"  CausesValidation="false" ValidationGroup="VG" CssClass="txtListing" /></TD>
                                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                              </TR>
                                            </TABLE>
	                                         
	                                         </div>
                                    	     <div class="displaynone">&nbsp;&nbsp;</div>
 </div>
 </asp:Panel> 
  <asp:Panel ID="pnlRegistrationComplete" Visible="false" runat="server">
	<div style="margin-left:20px; margin-top:20px;">
	<asp:Literal id="ltrRegistrationCompleteMessage" runat="server"></asp:Literal>									        																			
	</div>											
</asp:Panel>		
 </ContentTemplate>
   <Triggers>
   <asp:AsyncPostBackTrigger ControlID="btnNextSubmit" EventName="Click" />
   </Triggers>
   </asp:UpdatePanel> 

     <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
        <ProgressTemplate>
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
        </asp:UpdateProgress>
<cc1:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="pnlMain" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />