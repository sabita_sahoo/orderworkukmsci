<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCAdminWODetails.ascx.vb"
    Inherits="Admin.UCAdminWODetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function ShowPopup(Flag, LinkId, WorkOrderId) {
        var strFlag = Flag;
        var DivId = "PopupDiv" + WorkOrderId;
        if (Flag == "True") {

            var div1Pos = $("#" + LinkId).offset();
            var div1X = div1Pos.left + 80;
            var div1Y = div1Pos.top;
            $('#' + DivId).css({ left: div1X, top: div1Y });
            $("#" + DivId).show();
        }
        else {
            $("#" + DivId).hide();
        }
    }
    function validate_ActionComment(field, alerttxt) {
        var NoteTypeValue = document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWODetails1_ddlNoteType").value;
        if (NoteTypeValue == "") {
            alert('Please Select Note Type');
            return 0;
        }
        if (document.getElementById("pnlActionRating").style.display == "block") {
            var RatingSelected = 0;

            if (document.getElementById("rdBtnActionPositive").checked) {
                RatingSelected = 1;
            }
            else if (document.getElementById("rdBtnActionNeutral").checked) {
                RatingSelected = 1;
            }
            else if (document.getElementById("rdBtnActionNegative").checked) {
                RatingSelected = 1;
            }
            if (RatingSelected == 1) {
                var text = document.getElementById(field).value;
                if (text == null || text == "" || text == "Enter a new comment here") {
                    //alert(alerttxt);
                    document.getElementById("hdnBtnAction").click();
                }
                else {
                    if (text.length > 500) {
                        alert('Please enter comments less than 500 characters');
                    }
                    else {
                        document.getElementById("hdnBtnAction").click();
                    }
                }
            }
            else {
                document.getElementById("hdnBtnAction").click();
            }

        }
        else {
            document.getElementById("hdnBtnAction").click();
        }
    }

    function validate_RateWO(field, alerttxt) {

        var text = document.getElementById(field).value;
        if (text == null || text == "" || text == "Enter a new comment here") {
            alert(alerttxt);
        }
        else {
            if (text.length > 500) {
                alert('Please enter comments less than 500 characters');
            }
            else {
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWODetails1_hdnBtnRateWO").click();
            }
        }
    }

</script>
<script language="javascript" src="/JS/Scripts.js" type="text/javascript"></script>
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<asp:UpdatePanel runat="server" ID="updatepanel1" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" />
        <asp:Panel runat="server" ID="pnlUpdateWO" Width="300px" CssClass="pnlConfirmWOListing"
            Style="display: none;">
            <div id="div1">
                <b>Rate workorder</b>
                <br />
                <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnPositive" Checked="false" runat="server"
                    Text="Positive" />
                <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNeutral" Checked="false" runat="server"
                    Text="Neutral" />
                <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNegative" Checked="false" runat="server"
                    Text="Negative" /><br />
                <br />
                <b>Add comment here</b><br />
                <asp:TextBox runat="server" ID="txtComment" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned"
                    TextMode="MultiLine" onblur="javascript:removeHTML(this.id);"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtComment"
                    WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned">
                </cc1:TextBoxWatermarkExtender>
                <div id="divButton" class="divButton" style="width: 85px; float: left;">
                    <div class="bottonTopGrey">
                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" href='javascript:validate_RateWO("ctl00_ContentPlaceHolder1_UCAdminWODetails1_txtComment","Enter a new comment here")'
                        id="a1"><strong>Submit</strong></a>
                    <div class="bottonBottomGrey">
                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                </div>
                <div id="divButton" class="divButton" style="width: 85px; float: left; margin-left: 20px;">
                    <div class="bottonTopGrey">
                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancCancel1"><strong>Cancel</strong></a>
                    <div class="bottonBottomGrey">
                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                </div>
            </div>
        </asp:Panel>
        <asp:Button runat="server" ID="hdnBtnRateWO" Height="0" Width="0" BorderWidth="0"
            Style="visibility: hidden;" />
        <asp:Button runat="server" ID="btnTempRateWO" Height="0" Width="0" BorderWidth="0"
            Style="visibility: hidden;" />
        <div id="divValidationMain" visible="false" class="divValidation" runat="server">
            <div class="roundtopVal">
                <img src="Images/Curves/Validation-TLC.gif" title="" width="5" height="5" class="corner"
                    style="display: none" /></div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="15" align="center">
                        &nbsp;
                    </td>
                    <td height="15">
                        &nbsp;
                    </td>
                    <td height="15">
                        &nbsp;
                    </td>
                </tr>
                <tr valign="middle">
                    <td width="63" align="center" valign="top">
                        <img src="Images/Icons/Validation-Alert.gif" width="31" height="31">
                    </td>
                    <td class="validationText">
                        <div id="divValidationMsg">
                        </div>
                        <asp:Label ID="lblError" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                        <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg"
                            DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
                    </td>
                    <td width="20">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td height="15" align="center">
                        &nbsp;
                    </td>
                    <td height="15">
                        &nbsp;
                    </td>
                    <td height="15">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <div class="roundbottomVal">
                <img src="Images/Curves/Validation-BLC.gif" title="" width="5" height="5" class="corner"
                    style="display: none" /></div>
        </div>
        <!-- confirmation panel used for delete/discard/copy wo as draft-->
        <asp:Panel ID="pnlConfirmation" runat="server" Visible="false">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="30">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td width="30">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td align="center">
                        <p>
                            &nbsp;</p>
                        <asp:Label runat="server" ID="lblConfirm" CssClass="bodytxtRedSmall"></asp:Label>
                        <p>
                            &nbsp;</p>
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td align="right" valign="top">
                                    &nbsp;
                                </td>
                                <td align="right" valign="top">
                                    <table cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                        <tr>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                            </td>
                                            <td class="txtBtnImage">
                                                <asp:LinkButton ID="lnkSubmit" runat="server" CssClass="txtListing" TabIndex="8"> <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="3" align="absmiddle" border="0">&nbsp;Confirm &nbsp;</asp:LinkButton>
                                            </td>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10">
                                </td>
                                <td valign="top" align="left">
                                    <table cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                        <tr>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                            </td>
                                            <td class="txtBtnImage">
                                                <a id="lnkCancelAction" runat="server" causesvalidation="false" class="txtListing"
                                                    tabindex="8">
                                                    <img src="Images/Icons/Cancel.gif" width="11" height="11" hspace="5" vspace="3" align="absmiddle"
                                                        border="0">&nbsp;Cancel&nbsp;</a>
                                            </td>
                                            <td width="5">
                                                <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <!-- confirmation panel end-->
        <table id="DGWorkOrders" width="100%" border="0" cellpadding="5" cellspacing="0">
            <tr valign="top">
                <td width="80px" class="gridHdr gridBorderRB">
                    WO No
                </td>
                <td width="70px" class="gridHdr gridBorderRB">
                    <asp:Label runat="server" ID="lblHdrCreated"></asp:Label>
                </td>
                <td width="70px" class="gridHdr gridBorderRB" runat="server" id="tdDateModified">
                    <asp:Label runat="server" ID="lblHdrModified"></asp:Label>
                </td>
                <td width="65px" class="gridHdr gridBorderRB">
                    Location
                </td>
                <td width="75px" class="gridHdr gridBorderRB" runat="server" id="tdBuyerCompany">
                    Client Company
                </td>
                <td width="90px" class="gridHdr gridBorderRB" runat="server" id="tdBuyerContact">
                    <asp:Label runat="server" ID="lblBuyerContact"></asp:Label>
                </td>
                <td width="75px" class="gridHdr gridBorderRB" runat="server" id="tdSupplierCompany">
                    Supplier Company
                </td>
                <td width="90px" class="gridHdr gridBorderRB" runat="server" id="tdSupplierContact">
                    Supplier Contact
                </td>
                <td class="gridHdr gridBorderRB">
                    WO Title
                </td>
                <td width="60px" class="gridHdr gridBorderRB">
                    WO Start
                </td>
                <td width="80px" class="gridHdr gridBorderRB">
                    <asp:Label runat="server" ID="lblHdrWholesalePrice"></asp:Label>
                </td>
                <td width="75px" class="gridHdr gridBorderRB">
                    <asp:Label runat="server" ID="lblHdrPlatformPrice"></asp:Label>
                </td>
                <td width="60px" class="gridHdr gridBorderRB">
                    Upsell Price
                </td>
                <td width="60px" class="gridHdr gridBorderRB">
                    Billing Location
                </td>
                <td width="57px" class="gridHdrHighlight gridBorderRB">
                    Status
                </td>
                <td width="80px" class="gridHdr gridBorderB">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="80px" class="gridRow gridBorderRB">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label>
                </td>
                <td width="70px" class="gridRow gridBorderRB">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblCreated"></asp:Label>
                </td>
                <td width="70px" class="gridRow gridBorderRB" runat="server" id="tdDateModified1">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblModified"></asp:Label>
                </td>
                <td width="65px" class="gridRow gridBorderRB">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblLocation"></asp:Label>
                </td>
                <td width="75px" class="gridRow gridBorderRB" runat="server" id="tdBuyerCompany1">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblBuyerCompany"></asp:Label>
                </td>
                <td width="90px" class="gridRow gridBorderRB" runat="server" id="tdBuyerContact1">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblBuyerContact1"></asp:Label>
                </td>
                <td width="75px" class="gridRow gridBorderRB" runat="server" id="tdSupplierCompany1">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblSupplierCompany"></asp:Label>
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="260">
                                <span style='cursor: hand;' onmouseout="javascript:ShowPopup('False','PAnchor<%= ViewState("WorkOrderID")%>',<%= ViewState("WorkOrderID")%>);"
                                    onmouseover="javascript:ShowPopup('True','PAnchor<%= ViewState("WorkOrderID")%>',<%= ViewState("WorkOrderID")%>);">
                                    <%=GetLinks(1, IIf(IsDBNull(ViewState("SuppCompanyID")), "", ViewState("SuppCompanyID")), IIf(IsDBNull(ViewState("SupplierCompany")), "", ViewState("SupplierCompany")))%>
                                </span>
                            </td>
                            <td id="tdRating" visible="true" valign="top" runat="server">
                                <div style="text-align: right;" id="PAnchor<%= ViewState("WorkOrderID")%>">
                                    <a style="text-align: right;" id="ShowPopupAnchor" onclick="javascript:ShowPopup('True','PAnchor<%= ViewState("WorkOrderID")%>',<%= ViewState("WorkOrderID")%>);">
                                        <img id="imgTestQuickNote" src="Images/Icons/FindSuppliersforWorkOrder.gif" title="View Supplier Details"
                                            alt="View Supplier Details" width="16" height="15" hspace="2" vspace="0" border="0" /></a></div>
                                <div id="PopupDiv<%= ViewState("WorkOrderID")%>" style="display: none; position: absolute;
                                    margin-left: -50px;">
                                    <asp:Panel runat="server" ID="pnlServicePartner" Width="200px" Style="background-color: #FFEBDE;
                                        font-family: Tahoma, Arial, Verdana; font-size: 11px; color: #000000; text-decoration: none;
                                        line-height: 18px; border: 1px solid #CECBCE;">
                                        <div style="text-align: right;">
                                            <a style="color: Black; text-align: right; vertical-align: top; cursor: pointer;"
                                                onclick="javascript:ShowPopup('False','PAnchor<%= ViewState("WorkOrderID")%>',<%= ViewState("WorkOrderID")%>);">
                                                <b>Close [X]</b></a></div>
                                        <asp:Label ID="lblContactName" runat="server"></asp:Label><br />
                                        <asp:Label ID="lblPhoneNo" runat="server"></asp:Label><br />
                                        <asp:Label ID="lblMobileNo" runat="server"></asp:Label><br />
                                        <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                    </asp:Panel>
                                </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="90px" class="gridRow gridBorderRB" runat="server" id="tdSupplierContact1">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblSupplierContact"></asp:Label>
                </td>
                <td class="gridRow gridBorderRB">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblTitle"></asp:Label>
                </td>
                <td width="60px" class="gridRow gridBorderRB">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblWOStart"></asp:Label>
                </td>
                <td width="80px" class="gridRow gridBorderRB">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblWholesalePrice"></asp:Label>
                </td>
                <td width="75px" class="gridRow gridBorderRB">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblPlatformPrice"></asp:Label>
                </td>
                <td width="75px" class="gridRow gridBorderRB">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblUpsellPrice"></asp:Label>
                </td>
                <td width="60px" class="gridRow gridBorderRB">
                    <asp:Label runat="server" CssClass="formTxt" ID="lblbillloc"></asp:Label>
                </td>
                <td width="57px" class="gridRowHighlight gridBorderRB">
                    <asp:Label runat="server" CssClass="gridRowHighlight" ID="lblStatus"></asp:Label>
                </td>
                <td width="120px" class="gridRow gridBorderB">
                    <a runat="server" id="lnkEdit" visible="false">
                        <img src="Images/Icons/Edit.gif" title="Edit" width="12" height="11" hspace="2" vspace="0"
                            border="0"></a> <a runat="server" id="lnkUpdateWO" visible="false">
                                <asp:Image ImageUrl="Images/Icons/UpdateWO.gif" ID="imgSetWholesalePrice" runat="server" />
                                <%--<img src="Images/Icons/UpdateWO.gif" id="imgSetWholesalePrice" runat="server"  hspace="2" vspace="0" border="0" />--%>
                            </a><a runat="server" id="lnkOrderMatch" visible="false">
                                <img src="Images/Icons/FindSuppliersforWorkOrder.gif" title="OrderMatch" width="15"
                                    height="14" hspace="2" vspace="0" border="0"></a> <a runat="server" id="lnkSetPlatformPrice"
                                        visible="false">
                                        <img src='Images/Icons/SetPlatformPrice_Icon.gif' title='Set Portal Price' width='18'
                                            height='12' hspace='2' vspace='0' border='0'></a> <a runat="server" id='lnkBuyerAcceptWR'
                                                visible="false">
                                                <img src='Images/Icons/AcceptedBuyerWorkRequest.gif' title='Buyer Accept Work Request'
                                                    width='13' height='12' hspace='2' vspace='0' border='0'></a>
                    <a runat="server" id="lnkCopy" visible="false">
                        <img src="Images/Icons/Copy.gif" title="Copy" width="15" height="13" hspace="2" vspace="0"
                            border="0"></a> <a runat="server" id="lnkCancel" visible="false">
                                <img src="Images/Icons/Cancel.gif" title="Cancel" width="11" height="11" hspace="2"
                                    vspace="0" border="0"></a> <a runat="server" id="lnkRefund" visible="false">
                                        <img src="Images/Icons/Refund.gif" title="Refund" width="13" height="16" hspace="2"
                                            vspace="0" border="0"></a> <a runat="server" id="lnkAccept" visible="false">
                                                <img src="Images/Icons/Accept.gif" title="Accept" width="13" height="12" hspace="2"
                                                    vspace="0" border="0"></a> <a runat="server" id="lnkCA" visible="false">
                                                        <img src="Images/Icons/ConditionalAccept.gif" title="Conditional Accept" width="16"
                                                            height="12" hspace="2" vspace="0" border="0"></a>
                    <a runat="server" id="lnkChangeCATerms" visible="false">
                        <img src="Images/Icons/EditCA.gif" title="Change CA Terms" width="14" height="13"
                            hspace="2" vspace="0" border="0"></a> <a runat="server" id="lnkAcceptCA" visible="false">
                                <img src="Images/Icons/ConditionalAccept.gif" title="Accept CA" width="16" height="12"
                                    hspace="2" vspace="0" border="0"></a> <a runat="server" id="lnkChangeIssueTerms"
                                        visible="false">
                                        <img src="Images/Icons/EditCA.gif" title="Change Issue Terms" width="14" height="13"
                                            hspace="2" vspace="0" border="0"></a> <a runat="server" id="lnkCASendMail" visible="false">
                                                <img src="Images/Icons/Sendmail.gif" title="CA Send Mail" hspace="2" vspace="0" border="0"></a>
                    <a runat="server" id="lnkRaiseIssue" visible="false">
                        <img src="Images/Icons/RaiseIssue.gif" title="Raise Issue" width="14" height="13"
                            hspace="2" vspace="0" border="0"></a> <a runat="server" id="lnkAcceptIssue" visible="false">
                                <img src="Images/Icons/Accept-Issue.gif" title="Accept Issue" width="14" height="13"
                                    hspace="2" vspace="0" border="0"></a> <a runat="server" id="lnkComplete" visible="false">
                                        <img src="Images/Icons/Complete.gif" title="Complete" width="12" height="12" hspace="2"
                                            vspace="0" border="0"></a> <a runat="server" id="lnkClose" visible="false">
                                                <img src="Images/Icons/Close.gif" title="Close" width="13" height="14" hspace="2"
                                                    vspace="0" border="0"></a> <a runat="server" id="lnkCloseIssue" visible="false">
                                                        <img src="Images/Icons/CloseIssue.gif" title="Close Issue" width="15" height="14"
                                                            hspace="2" vspace="0" border="0"></a> <a runat="server" id="lnkDiscard" visible="false">
                                                                <img src="Images/Icons/Discard.gif" title="Discard" width="13" height="15" hspace="2"
                                                                    vspace="0" border="0"></a> <a runat="server" id="lnkHistory" visible="false">
                                                                        <img src="Images/Icons/ViewHistory.gif" title="History" width="13" height="15" hspace="2"
                                                                            vspace="0" border="0"></a>
                    <a runat="server" id="lnkCustomerHistory" visible="false">
                        <img src="Images/Icons/CustomerHistory.gif" title="Customer History" hspace="2" vspace="0"
                            border="0"></a> <a runat="server" id="lnkDelete" visible="false">
                                <img src="Images/Icons/Delete.gif" title="Delete" width="13" height="14" hspace="2"
                                    vspace="0" border="0"></a>
                    <%--<img id="imgQuickNote" runat="server" src="~/Images/Icons/QuickNote.gif" title="Add a Quick Note"
                        width="14" height="14" hspace="2" vspace="0" border="0" class="cursorHand" />--%>
                    <asp:LinkButton ID="lnkbtnQuickNote" CommandName="AddNote" CausesValidation="false"
                        runat="server">
                        <img id="imgQuickNote" runat="server" src="~/Images/Icons/QuickNote.gif" title="Add a Quick Note"
                            width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" />
                    </asp:LinkButton>
                    <cc1:ModalPopupExtender ID="mdlAction" runat="server" TargetControlID="btnActionTemp"
                        PopupControlID="pnlActionConfirm" OkControlID="" CancelControlID="ancCancelAction"
                        BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
                    </cc1:ModalPopupExtender>
                    <asp:Panel runat="server" ID="pnlActionConfirm" Width="340px" CssClass="pnlConfirmWOListingForAction"
                        Style="display: none;">
                        <div id="div2">
                            <div class="divTopActionStyle">
                                <b>OrderWork Action Form</b>
                            </div>
                            <asp:Panel ClientIDMode="Static" CssClass="divTopActionStyle" ID="pnlActionRating"
                                runat="server" Style="display: none;">
                                <b>Rate workorder</b>
                                <br />
                                <asp:RadioButton GroupName="SelectedActionRatings" ID="rdBtnActionPositive" Checked="false"
                                    runat="server" Text="Positive" ClientIDMode="Static" />
                                <asp:RadioButton GroupName="SelectedActionRatings" ID="rdBtnActionNeutral" Checked="false"
                                    runat="server" Text="Neutral" ClientIDMode="Static" />
                                <asp:RadioButton GroupName="SelectedActionRatings" ID="rdBtnActionNegative" Checked="false"
                                    runat="server" Text="Negative" ClientIDMode="Static" /><br />
                                Add comment for rating<br />
                                <asp:TextBox runat="server" ID="txtCommentAction" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned"
                                    TextMode="MultiLine" onblur="javascript:removeHTML(this.id);"></asp:TextBox>
                                <%-- <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtComment" WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        --%>
                            </asp:Panel>
                            <div class="divTopActionStyle">
                                <b>Post Quick Note</b><br />
                                <asp:TextBox runat="server" ID="txtNoteAction" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned"
                                    TextMode="MultiLine" onblur="javascript:removeHTML(this.id);"></asp:TextBox>
                                <%--poonam modified on 9/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types--%>
                                <br />
                                <b>Note Type</b><br />
                                <asp:DropDownList Width="280px" Height="25px" CssClass="bodyTextGreyLeftAligned"
                                    ID="ddlNoteType" runat="server" AutoPostBack="false">
                                </asp:DropDownList>
                                <br />
                                <span style="color: #e51312; font-size: 11px;">*Notes entered here are NOT visible to
                                    the Suppliers/Clients.</span><br />
                                <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtNote" WatermarkText="Enter a new note here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        --%>
                            </div>
                            <div class="divTopActionStyle" id="div3" runat="server">
                                <asp:CheckBox ID="chkShowClient" runat="server" Style="font-weight: bold;" Text="Show To Client">
                                </asp:CheckBox>
                                <asp:CheckBox ID="chkShowSupplier" runat="server" Style="font-weight: bold;" Text="Show To Supplier">
                                </asp:CheckBox>
                            </div>
                            <div class="divTopActionStyle" id="divActionWatch" runat="server" style="display: none;">
                                <asp:CheckBox ID="chkMyWatch" runat="server" Style="font-weight: bold;" Text="Add To MyWatch">
                                </asp:CheckBox>
                            </div>
                            <div class="divTopActionStyle" style="font-weight: bold;">
                                <asp:CheckBox ID="chkMyRedFlag" runat="server" Text="Add To MyRedFlag"></asp:CheckBox>
                            </div>
                            <div style="padding-left: 20px; padding-top: 10px; float: left; width: 305px;">
                                <div class="divButtonAction" style="width: 85px; float: left; cursor: pointer;">
                                    <div class="bottonTopGrey">
                                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                            height="4" class="btnCorner" /></div>
                                    <a class="buttonText cursorHand" onclick='javascript:validate_ActionComment("ctl00_ContentPlaceHolder1_UCAdminWOsListing1_txtCommentAction","Please enter rating comment")'
                                        id="ancSubmitAction">Submit</a>
                                    <div class="bottonBottomGrey">
                                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                            height="4" class="btnCorner" /></div>
                                </div>
                                <div class="divButtonAction" style="width: 85px; float: left; margin-left: 20px;
                                    cursor: pointer;">
                                    <div class="bottonTopGrey">
                                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                            height="4" class="btnCorner" style="display: none" /></div>
                                    <a class="buttonText cursorHand" runat="server" id="ancCancelAction">Cancel</a>
                                    <div class="bottonBottomGrey">
                                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                            height="4" class="btnCorner" style="display: none" /></div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Button ClientIDMode="Static" runat="server" ID="hdnBtnAction" Height="0" Width="0"
                        BorderWidth="0" Style="visibility: hidden;" />
                    <asp:Button runat="server" ID="btnActionTemp" Height="0" Width="0" BorderWidth="0"
                        Style="visibility: hidden;" />
                    <a runat="server" id="lnkNotes" visible="true">
                        <img src="Images/Icons/Note.gif" title="View Notes" width="12" height="13" hspace="2"
                            vspace="0" border="0"></a> <a runat="server" id="lnkCancelWO" visible="false">
                                <img src="Images/Icons/Cancel.gif" title="Cancel WO" width="11" height="11" hspace="2"
                                    vspace="0" border="0"></a>
                    <cc1:ModalPopupExtender ID="mdlUpdateRating" runat="server" TargetControlID="btnTempRateWO"
                        PopupControlID="pnlUpdateWO" OkControlID="" CancelControlID="ancCancel1" BackgroundCssClass="modalBackground"
                        Drag="False" DropShadow="False">
                    </cc1:ModalPopupExtender>
                    <a id="lnkbtnUpdateRating" runat="server" visible="false">
                        <img id="imgUpdateRating" runat="server" src="~/Images/Icons/Update_rating.gif" title="Rate WO"
                            width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" /></a>
                  
                    <asp:LinkButton ID="lnkbtnWatch" OnClick="lnkbtnWatch_Click"  CausesValidation="false"
                        runat="server">
                        <img id="imgwatch" runat="server" src="~/Images/Icons/open.png" width="24" height="20"
                            hspace="2" vspace="0" border="0" class="cursorHand" />
                    </asp:LinkButton>
                    <asp:LinkButton ID="lnkbtnUnwatch" OnClick="lnkbtnUnwatch_Click"  CausesValidation="false"
                        runat="server">
                        <img id="imgUnwatch" runat="server" src="~/Images/Icons/Closed.png" width="24" height="15"
                            hspace="2" vspace="0" border="0" class="cursorHand" />
                    </asp:LinkButton>
                </td>
            </tr>
        </table>
      
        <input type="hidden" id="hdnValue" runat="server" name="hdnValue">
        <input type="hidden" id="hdnSpecialistID" runat="server" name="hdnSpecialistID">
        <asp:Panel ID="pnlSupplierResponse" runat="server" Visible="false">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="28" align="left" valign="middle" class="SubMenuTxtBoldSelected">
                        <asp:Label CssClass="gridText" ID="lblDLHeader" runat="server"></asp:Label>
                    </td>
                    <td width="20">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top">
                        <asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLSupplierResponse"
                            runat="server" Width="100%">
                            <HeaderTemplate>
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr id="tblDGYourResponseHdr" align="left">
                                        <td width="5" valign="top" style="padding: 0px; border: 0px;">
                                            <img src="~/Images/Curves/Bdr-Red-TL.gif" width="5" height="5" hspace="0" vspace="0"
                                                border="0">
                                        </td>
                                        <td width="48" style="border-left-width: 0px;">
                                            <asp:LinkButton ID="lnkID" runat="server" OnClick="lnkID_Click" Style="color: #FFFFFF;">ID</asp:LinkButton>
                                        </td>
                                        <td width="78">
                                            <asp:LinkButton ID="lnkActionDate" runat="server" OnClick="lnkActionDate_Click" Style="color: #FFFFFF;">Action Date</asp:LinkButton>
                                        </td>
                                        <td width="72">
                                            <asp:LinkButton ID="lnkStart" runat="server" OnClick="lnkStart_Click" Style="color: #FFFFFF;">
                                                <asp:Label ID="lblStartHdg" runat="server" Text="Start"></asp:Label>
                                            </asp:LinkButton>
                                        </td>
                                        <td id="tdAptTimeHD" runat="server" width="80">
                                            <asp:Label ID="lblAptTMHD" runat="server" Text="Apt.Time"></asp:Label>
                                        </td>
                                        <td id="tdDLEndDate" runat="server" width="70">
                                            <asp:LinkButton ID="lnkEnd" runat="server" OnClick="lnkEnd_Click" Style="color: #FFFFFF;">End</asp:LinkButton>
                                        </td>
                                        <td runat="server" id="tdDLCompany" visible="false">
                                            <asp:LinkButton ID="lnkCompany" runat="server" OnClick="lnkCompany_Click" Style="color: #FFFFFF;">Company</asp:LinkButton>
                                        </td>
                                        <td width="70" runat="server" id="tdDLContact" visible="false">
                                            <asp:LinkButton ID="lnkContact" runat="server" OnClick="lnkContact_Click" Style="color: #FFFFFF;">Contact</asp:LinkButton>
                                        </td>
                                        <td width="80" runat="server" id="tdDLPhone" visible="false">
                                            <asp:LinkButton ID="lnkPhone" runat="server" OnClick="lnkPhone_Click" Style="color: #FFFFFF;">Phone</asp:LinkButton>
                                        </td>
                                        <td width="70" runat="server" id="tdDLSpecialist" visible="false">
                                            <asp:LinkButton ID="lnkSpecialist" runat="server" OnClick="lnkSpecialist_Click" Style="color: #FFFFFF;">Specialist</asp:LinkButton>
                                        </td>
                                        <td width="80">
                                            <asp:LinkButton ID="lnkPrice" runat="server" OnClick="lnkPrice_Click" Style="color: #FFFFFF;">Price</asp:LinkButton>
                                        </td>
                                        <td id="tdDLDayRate" runat="server" width="60">
                                            <asp:LinkButton ID="lnkDayRate" runat="server" OnClick="lnkDayRate_Click" Style="color: #FFFFFF;">Day Rate</asp:LinkButton>
                                        </td>
                                        <td id="tdDLEstimatedTime" runat="server" width="90">
                                            <asp:LinkButton ID="lnkTime" runat="server" OnClick="lnkTime_Click" Style="color: #FFFFFF;">Time (Days)</asp:LinkButton>
                                        </td>
                                        <td id="tdDLSupplyParts" runat="server" width="100">
                                            <asp:LinkButton ID="lnkSupplyParts" runat="server" OnClick="lnkSupplyParts_Click"
                                                Style="color: #FFFFFF;">Supply Parts</asp:LinkButton>
                                        </td>
                                        <td width="100" id="tdStatusHdr" runat="server">
                                            <span class="formTxtOrange">
                                                <asp:LinkButton CssClass="formTxtOrange" ID="lnkStatus" runat="server" OnClick="lnkStatus_Click">Status</asp:LinkButton></span>
                                        </td>
                                        <td width="50" id="tdRatingHdr" visible="false" runat="server">
                                            Rating
                                        </td>
                                        <td id="tdActionHdr" runat="server" width="80" style="border-right-width: 0px;">
                                            Action
                                        </td>
                                        <td width="5" valign="top" align="right" style="padding: 0px; border: 0px;">
                                            <img align="right" src="~/Images/Curves/Bdr-Red-TR.gif" width="5" height="5" hspace="0"
                                                vspace="0" border="0">
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr id="tblDLRow" align="left">
                                        <td width="5" valign="top" style="padding: 0px; border: 0px;">
                                            &nbsp;
                                        </td>
                                        <td width="30" style="border-left-width: 0px;">
                                            <%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%>
                                        </td>
                                        <td width="78">
                                            <%#Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateModified"), DateFormat.ShortDate)%>
                                        </td>
                                        <td width="72">
                                            <asp:Label ID="lblStartDate" CssClass='<%# iif(DataBinder.Eval(Container.DataItem, "DateStart") <> lblStartDate.Text,"bodytxtValidationMsg","gridRow")%>'
                                                runat="server" Text='<%# Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateStart"),DateFormat.ShortDate) %>'></asp:Label>
                                        </td>
                                        <td width="80" runat="server" id="tdAptTime" visible='<%# iif(DataBinder.Eval(Container.DataItem, "BusinessArea") <> 101,"false","true")%>'>
                                            <asp:Label runat="server" ID="lblAptTime" Text='<%# DataBinder.Eval(Container.DataItem, "AptTime") %>'
                                                CssClass='<%# iif(DataBinder.Eval(Container.DataItem, "AptTime") <>DataBinder.Eval(Container.DataItem, "AptTimeOld"),"bodytxtValidationMsg","gridRow")%>'></asp:Label>
                                        </td>
                                        <td id="tdDLEndDate1" runat="server" width="70">
                                            <asp:Label ID="lblEndDate" CssClass='<%# iif(DataBinder.Eval(Container.DataItem, "DateEnd") <> lblEndDate.Text,"bodytxtValidationMsg","gridRow")%>'
                                                runat="server" Text='<%# Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateEnd"),DateFormat.ShortDate) %>'></asp:Label>
                                        </td>
                                        <td id="tdDLCompany1" runat="server" visible="false" style="border-left-width: 0px;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td width="160" id="td1" visible="true" runat="server" style="border: 0;">
                                                        <div style='visibility: hidden; position: absolute; margin-left: 100px;' id='divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%>'>
                                                            <table width="300" height="100" bgcolor="#D6D7D6" border="0" cellspacing="0" cellpadding="0"
                                                                style="padding-left: 10px;">
                                                                <tr>
                                                                    <td style="background-color: #D6D7D6; border: 0; font-size: 12;" align='left'>
                                                                        <strong>Admin:</strong>
                                                                        <%# iif(Container.DataItem("Name") = " ","--",Container.DataItem("Name"))%>
                                                                    </td>
                                                                    <td style="background-color: #D6D7D6; border: 0; font-size: 12; padding-right: 5px;
                                                                        width: 50px;" align='left'>
                                                                        <a style="color: Red; text-align: right; vertical-align: top; cursor: pointer;" onclick="javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%>','hidden')">
                                                                            <b>Close [X]</b></a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="background-color: #D6D7D6; border: 0; font-size: 12;" align='left'>
                                                                        <strong>Phone No:</strong>
                                                                        <%# iif(Container.DataItem("Phone") = " ","--",Container.DataItem("Phone"))%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="background-color: #D6D7D6; border: 0; font-size: 12;" align='left'>
                                                                        <strong>Mobile No:</strong>
                                                                        <%# iif(Container.DataItem("Mobile") = " ","--",Container.DataItem("Mobile"))%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="background-color: #D6D7D6; border: 0; font-size: 12;" align='left'>
                                                                        <strong>Email Id:</strong>
                                                                        <%# iif(Container.DataItem("Email") = " ","--",Container.DataItem("Email"))%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <span style="border-left-width: 0px; vertical-align: bottom; cursor: hand; font-family: Geneva, Arial, Helvetica, sans-serif;
                                                            font-size: 11px; color: #1D85BA" onmouseout="javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%>','hidden')"
                                                            onmouseover="javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%>','visible')">
                                                            <%--<%# DataBinder.Eval(Container.DataItem, "Company")%>--%>
                                                            <%#GetLinksForResponsesupplier(1, IIf(IsDBNull(Container.DataItem("CompanyID")), "", Container.DataItem("CompanyID")), IIf(IsDBNull(Container.DataItem("Company")), "", Container.DataItem("Company")), IIf(IsDBNull(Container.DataItem("CompanyID")), "", Container.DataItem("CompanyID")))%>
                                                        </span><a style="text-align: right; cursor: pointer;" id="ShowPopupSupplier" onclick="javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%>','visible')">
                                                            <img id="imgTestQuickNote" src="Images/Icons/FindSuppliersforWorkOrder.gif" title="View Details"
                                                                alt="View Details" width="16" height="15" hspace="2" vspace="0" border="0" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="70" id="tdDLContact1" runat="server" visible="false" style="border-left-width: 0px;">
                                            <%# DataBinder.Eval(Container.DataItem, "Name")%>
                                        </td>
                                        <td width="80" id="tdDLPhone1" runat="server" visible="false" style="border-left-width: 0px;">
                                            <%# DataBinder.Eval(Container.DataItem, "Phone")%>
                                        </td>
                                        <td width="70" id="tdDLSpecialist1" runat="server" visible="false" style="border-left-width: 0px;">
                                            <asp:Label CssClass='<%# iif(DataBinder.Eval(Container.DataItem, "SpecialistID") <> hdnSpecialistID.Value,"bodytxtValidationMsg","gridRow")%>'
                                                runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Specialist")%>'></asp:Label>
                                        </td>
                                        <td width="80" style="padding-left: 2px; padding-right: 5px; text-align: right;">
                                            <asp:Label ID="lblPrice" CssClass='<%# iif(FormatNumber(Container.DataItem("Value"),2, TriState.True, TriState.True, TriState.False) <> hdnValue.Value,"bodytxtValidationMsg","gridRow")%>'
                                                runat="server" Text='<%#FormatCurrency(IIF(Not isDbnull(Container.DataItem("Value")) ,Container.DataItem("Value"),"0"),2,TriState.True, Tristate.true,Tristate.false)%>'></asp:Label>
                                        </td>
                                        <td runat="server" id="tdDLDayRate1" width="60">
                                            <%# DataBinder.Eval(Container.DataItem, "DayJobRate")%>
                                        </td>
                                        <td runat="server" id="tdDLEstimatedTime1" width="90">
                                            <%# DataBinder.Eval(Container.DataItem, "EstimatedTimeInDays")%>
                                        </td>
                                        <td id="tdDLSupplyParts1" runat="server" width="100">
                                            <asp:Label ID="lblSupplier" CssClass='<%# iif(iif(DataBinder.Eval(Container.DataItem, "SpecialistSuppliesParts") = True,"Yes","No") <> lblSupplyParts.Text,"bodytxtValidationMsg","gridRow")%>'
                                                runat="server" Text='<%# iif(DataBinder.Eval(Container.DataItem, "SpecialistSuppliesParts") = True,"Yes","No")%>'></asp:Label>
                                        </td>
                                        <td width="100" id="tdStatus" runat="server">
                                            <span class="bodytxtValidationMsg">
                                                <%# DataBinder.Eval(Container.DataItem, "Status")%></span>
                                        </td>
                                        <td width="50" id="tdRating" visible="false" runat="server">
                                            <div style='background-color: #FFFFFF; visibility: hidden; position: absolute; margin-left: 20px;
                                                z-index: 100' id='divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>'>
                                                <table width='153px' height='65px' bgcolor='#FFFFFF' border='0' cellspacing='0' cellpadding='0'>
                                                    <tr>
                                                        <td width='20' align='center'>
                                                            <img src='Images/Icons/Icon-Positive.gif' width='18' height='18' align='absmiddle' />
                                                        </td>
                                                        <td class='bgWhite' align='left'>
                                                            Positive
                                                            <%# iif(ISDBNULL(Container.DataItem("PositiveRating")),0,Container.DataItem("PositiveRating"))%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width='20' align='center'>
                                                            <img src='Images/Icons/Icon-Neutral.gif' width='13' height='5' align='absmiddle' />
                                                        </td>
                                                        <td class='bgWhite' align='left'>
                                                            Neutral
                                                            <%# iif(ISDBNULL(Container.DataItem("NeutralRating")),0,Container.DataItem("NeutralRating"))%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width='20' align='center'>
                                                            <img src='Images/Icons/Icon-Negative.gif' width='17' height='18' align='absmiddle' />
                                                        </td>
                                                        <td class='bgWhite' align='left'>
                                                            Negative
                                                            <%# iif(ISDBNULL(Container.DataItem("NegativeRating")),0,Container.DataItem("NegativeRating"))%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <span style='cursor: hand;' onmouseout="javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>','hidden')"
                                                onmouseover="javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>','visible')">
                                                <%#Container.DataItem("RatingScore").ToString().Replace(".00", "") & "%"%>
                                            </span>
                                        </td>
                                        <td id="tdAction" runat="server" width="80" style="border-right-width: 0px;" align="center">
                                            <%#GetActionColLinks(Container.DataItem("ContactID"), Container.DataItem("CompanyID"))%>
                                        </td>
                                        <td width="5" valign="top" style="padding: 0px; border: 0px;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <div id="divCommentsDetail" runat="server" style="visibility: hidden; width: 945px;
                                    position: absolute;">
                                    <table id="tblCommentDetails" runat="server" width="100%" border="0" cellpadding="0"
                                        cellspacing="0" bgcolor="#EDEDED">
                                        <tr>
                                            <td width="10" height="10" style="padding: 0px; margin: 0px">
                                            </td>
                                            <td height="10" style="padding: 0px; margin: 0px">
                                            </td>
                                            <td width="10" height="10" style="padding: 0px; margin: 0px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <table id="tblDiscussion" width="100%" border="0" cellspacing="0" cellpadding="0"
                                                    bgcolor="#EDEDED">
                                                    <tr>
                                                        <td width="10px">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="10px">
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <div id="divSupplierSkills" class="marginB20" runat="server">
                                                                <asp:Literal ID="ltSupSkills" runat="server" Text='<%#GetSupSkills(Container.DataItem("CompanyID"),Container.DataItem("SpecialistID"))%>'></asp:Literal>
                                                            </div>
                                                            <asp:DataList DataSource='<%#GetDiscussions(Container.DataItem("WOTrackingID"),Container.DataItem("CompanyID"))%>'
                                                                RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLDiscussion" runat="server"
                                                                Width="100%">
                                                                <ItemTemplate>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td width="200" valign="baseline">
                                                                                <%#IIf(DataBinder.Eval(Container.DataItem, "Status") <> admin.Applicationsettings.WOStatusID.CAMailSent, (IIf((DataBinder.Eval(Container.DataItem, "ContactClassID") = admin.Applicationsettings.RoleOWID), "<span class='discussionTextClient'>" & Container.DataItem("CommentsDate") & " - " & Container.DataItem("CommentsBy") & "  </span>", "<span class='discussionTextSupplier'>" & Container.DataItem("CommentsDate") & " - " & Container.DataItem("CommentsBy") & "  </span>")), "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - " & ("Admin's CA Mail") & "  </span>")%>
                                                                            </td>
                                                                            <td width="20" align="center" valign="top">
                                                                                :
                                                                            </td>
                                                                            <td valign="baseline" class="remarksAreaCollapse">
                                                                                <%# Container.DataItem("Comments") %>
                                                                            </td>
                                                                            <td width="10px">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="10">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="padding: 0px; margin: 0px">
                                            </td>
                                            <td height="10" style="padding: 0px; margin: 0px">
                                            </td>
                                            <td height="10" style="padding: 0px; margin: 0px">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <table width="100%" id="tblCommentsCollapse" border="0" cellspacing="0" cellpadding="0"
                                    bgcolor="#DFDFDF" runat="server" visible='<%#ShowHideComments(Container.DataItem("WOTrackingID"), Container.DataItem("CompanyID"))%>'>
                                    <tr>
                                        <td class="smallText" height="8" style="padding: 0px; margin: 0px">
                                        </td>
                                        <td height="8" align="center" class="smallText" style="padding: 0px; margin: 0px"
                                            id="tdCollapse" runat="server">
                                            <a href="" border="0" onclick='<%# "javascript:CollapseComments(" & Container.DataItem("WOTrackingID") & ", this.id)" %>'
                                                runat="server" id="collapseDetails">
                                                <img src="Images/Arrows/Grey-Arrow.gif" width="50" title='Expand' height="8" border="0"
                                                    name='<%# "imgExp" & Container.DataItem("WOTrackingID") %>' id='<%# "imgExp" & Container.DataItem("WOTrackingID") %>'
                                                    onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow-Roll.gif')"
                                                    onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow.gif');"
                                                    style="position: static;" />
                                                <img src="Images/Arrows/Orange-Arrow-Col.gif" width="50" title='Collapse' height="8"
                                                    border="0" name='<%# "imgCol" & Container.DataItem("WOTrackingID") %>' id='<%# "imgCol" & Container.DataItem("WOTrackingID") %>'
                                                    onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col-Roll.gif')"
                                                    onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col.gif');"
                                                    style="visibility: hidden; position: absolute;" />
                                            </a>
                                        </td>
                                        <td class="smallText" height="8" style="padding: 0px; margin: 0px">
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlSupplierContactInfo" runat="server">
            <div class="divRatings">
                <div class="divAccountTopCurves">
                    <img src="~/Images/Curves/MAS-Bdr-TL.gif" title="" width="5" height="5" class="corner"
                        style="display: none" /></div>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr class="smallText">
                        <td width="15" height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td width="15" height="8" class="smallText">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="15" height="8">
                            &nbsp;
                        </td>
                        <td height="8" id="tdSupplierInfo" valign="top" runat="server">
                            <span class="bodytxtValidationMsg"><strong>
                                <asp:Label ID="lblSupplierContactLabel" runat="server"></asp:Label></strong></span>
                        </td>
                        <td height="8">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="15">
                            &nbsp;
                        </td>
                        <td>
                            <span class="bodyTextGreyLeftAligned"><strong>
                                <asp:Label ID="lblSupplierContactName" runat="server"></asp:Label>
                                <asp:Label ID="lblSupplierCompanyName" runat="server"></asp:Label>
                            </strong>
                                <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label>&nbsp;&nbsp; Tel:
                                <asp:Label ID="lblSupplierPhone" runat="server"></asp:Label>&nbsp;&nbsp; Mob:
                                <asp:Label ID="lblSupplierMobile" runat="server"></asp:Label>
                            </span>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="15">
                            &nbsp;
                        </td>
                        <td class="bodyTextGreyLeftAligned" id="tdTopRating" runat="server">
                            <strong>Performance Rating :
                                <asp:Label ID="lblRatingSupplier" runat="server"></asp:Label></strong> &nbsp;<img
                                    src="Images/Icons/Icon-Positive.gif" width="18" height="18" align="absmiddle">
                            Positive :
                            <asp:Label ID="lblPosRatingSupplier" runat="server"></asp:Label>
                            <img src="Images/Icons/Icon-Neutral.gif" width="13" height="5" align="absmiddle">
                            Neutral :
                            <asp:Label ID="lblNeuRatingSupplier" runat="server"></asp:Label>
                            <img src="Images/Icons/Icon-Negative.gif" width="17" height="18" align="absmiddle">
                            Negative :
                            <asp:Label ID="lblNegRatingSupplier" runat="server"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="smallText">
                        <td width="15" height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div class="divAccountBotCurves">
                    <img src="~/Images/Curves/MAS-Bdr-BL.gif" title="" width="5" height="5" class="corner"
                        style="display: none" /></div>
            </div>
        </asp:Panel>
        <div class="divRatings" id="divWODetail">
            <div class="divAccountTopCurves">
                <img src="~/Images/Curves/MAS-Bdr-TL.gif" title="" width="5" height="5" class="corner"
                    style="display: none" /></div>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="txtWelcome">
                <tr class="smallText">
                    <td height="8" align="left" valign="top" class="smallText">
                        &nbsp;
                    </td>
                    <td height="8" align="left" valign="top" class="smallText">
                        &nbsp;
                    </td>
                    <td height="8" align="left" valign="top" class="smallText">
                        &nbsp;
                    </td>
                    <td height="8" class="smallText">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td width="15" align="left" valign="top">
                        &nbsp;
                    </td>
                    <td id="tdDGDetailedDesc" align="left" valign="top">
                        <strong>Scope of Work:</strong><br><br>
                        <div runat="server" id="divScopeOfWork">
                        <%--<asp:Label ID="lblLongDesc" runat="server"></asp:Label>--%>
                        </div>
                        <br />
                        <br />
                        <asp:Panel ID="pnlClientScope" runat="server">
                            <asp:Label ID="lblTagClientScope" runat="server"><strong>Client Scope of Work:</strong></asp:Label><br><br>
                            <div runat="server" id="divClientScope">                     
                            <%--<asp:Label ID="lblClientScope" runat="server"></asp:Label>--%>
                            </div>
                        </asp:Panel>
                        <br />
                        <br />
                        <asp:Panel ID="pnlSpecialInstructions" runat="server">
                            <strong>Special instructions to the Engineers:</strong><br><br>
                            <div runat="server" id="divSpecialInstructions"> 
                            <asp:Label ID="lblSpecialInstructions" runat="server" Visible="False"></asp:Label>
                            </div>
                            <br />
                            <br />
                            <strong>Special instructions to Orderwork:</strong><br><br>
                            <div runat="server" id="divCustSpecialInstructions"> 

                            <%--<asp:Label ID="lblCustSpecialInstructions" runat="server"></asp:Label>--%>
                            </div>
                            <br />
                        </asp:Panel>
                    </td>
                    <td width="250" align="left" valign="top" class="paddingL23">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" valign="top" class="txtOrange">
                                                WO Type
                                            </td>
                                            <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">
                                                :
                                            </td>
                                            <td class="txtOrange" align="left" valign="top">
                                                <asp:Label ID="lblCategory" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="bodyTextGreyLeftAligned">
                                                &nbsp;
                                            </td>
                                            <td align="center" valign="top" class="bodyTextGreyLeftAligned">
                                                &nbsp;
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">
                                                <asp:Label ID="lblStartDateLabel" runat="server"></asp:Label>
                                            </td>
                                            <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">
                                                :
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                <asp:Label ID="lblStartDate" runat="server"></asp:Label><asp:Label ID="lblInstallTime"
                                                    runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trEndDate" runat="Server">
                                            <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">
                                                End Date
                                            </td>
                                            <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">
                                                :
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">
                                                Quantity
                                            </td>
                                            <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">
                                                :
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">
                                                Estimated Time
                                            </td>
                                            <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">
                                                :
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                <asp:Label ID="lblEstimatedTime" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bodyTextGreyLeftAligned" width="105" align="left" valign="top">
                                                Supply Parts
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">
                                                :
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                <asp:Label ID="lblSupplyParts" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">
                                                Dress Code
                                            </td>
                                            <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">
                                                :
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                <asp:Label ID="lblDressCode" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" id="tblWOSpecs" runat="server" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="bodyTextGreyLeftAligned" width="105" align="left" valign="top">
                                                PO Number
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">
                                                :
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                <asp:Label ID="lblPONumber" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trJobNumber" runat="server">
                                            <td class="bodyTextGreyLeftAligned" width="105" align="left" valign="top">
                                                Job Number
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">
                                                :
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                <asp:Label ID="lblJRSNumber" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trFreesatMake" runat="server">
                                            <td class="bodyTextGreyLeftAligned" width="105" align="left" valign="top">
                                                Freesat Make/Model
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">
                                                :
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                <asp:Label ID="lblFreesatMake" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trSalesAgent" runat="Server">
                                            <td class="bodyTextGreyLeftAligned" width="105" align="left" valign="top">
                                                Sales Agent
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">
                                                :
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                <asp:Label ID="lblSalesAgent" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trReceiptNumber" runat="Server">
                                            <td class="bodyTextGreyLeftAligned" width="105" align="left" valign="top">
                                               <asp:Label ID="lblReceiptNumberHeader" runat="server" Text="Receipt Number"></asp:Label>
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">
                                                :
                                            </td>
                                            <td class="bodyTextGreyLeftAligned" align="left" valign="top">
                                                <asp:Label ID="lblReceiptNumber" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="smallText" height="16" align="left" valign="top">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <table width="100%" id="tblWOAttachments" runat="server" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="smallText" height="16" align="left" valign="top">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" class="bodyTextGreyLeftAligned">
                                                <asp:Panel ID="pnlAttachments" runat="server">
                                                    <asp:Literal ID="litAttachments" runat="server" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="15">
                        &nbsp;
                    </td>
                </tr>
                <tr class="smallText">
                    <td height="8" align="left" valign="top" class="smallText">
                        &nbsp;
                    </td>
                    <td height="8" align="left" valign="top" class="smallText">
                        &nbsp;
                    </td>
                    <td height="8" align="left" valign="top" class="smallText">
                        &nbsp;
                    </td>
                    <td height="8" class="smallText">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <div class="divAccountBotCurves">
                <img src="~/Images/Curves/MAS-Bdr-BL.gif" title="" width="5" height="5" class="corner"
                    style="display: none" /></div>
        </div>
        <asp:Panel ID="pnlGoodsCollection" runat="server">
            <div class="divRatings" id="divRatings">
                <div class="divAccountTopCurves">
                    <img src="~/Images/Curves/MAS-Bdr-TL.gif" title="" width="5" height="5" class="corner"
                        style="display: none" /></div>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr class="smallText">
                        <td width="15" height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td width="15" height="8" class="smallText">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <span class="txtOrange"><strong>
                                <asp:Label ID="lblGoodsCollection" runat="server" Text="Goods Collection"></asp:Label></strong></span>
                            <br>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bodyTextGreyLeftAligned">
                                <tr>
                                    <td width="180" valign="top" class="bodyTextGreyLeftAligned">
                                        <b>
                                            <asp:Label runat="server" ID="lblDepotNameLabel"></asp:Label>:</b>
                                    </td>
                                    <td class="bodyTextGreyLeftAligned">
                                        <asp:Label ID="lblDepotName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr runat="server" valign="top" id="trDepotAddress">
                                    <td width="180" class="bodyTextGreyLeftAligned">
                                        <b>Location Address:</b>
                                    </td>
                                    <td class="bodyTextGreyLeftAligned">
                                        <asp:Label ID="lblBusinessDivision" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr runat="Server" valign="top" id="trDepotUser">
                                    <td width="180" class="bodyTextGreyLeftAligned">
                                        <b>Depot User:</b>
                                    </td>
                                    <td class="bodyTextGreyLeftAligned">
                                        <asp:Label ID="lblGoodsUser" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr runat="Server" valign="top" id="trTVSize">
                                    <td width="180" class="bodyTextGreyLeftAligned">
                                        <b>TV Size:</b>
                                    </td>
                                    <td class="bodyTextGreyLeftAligned">
                                        <asp:Label ID="lblTVSize" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr runat="Server" valign="top" id="trProducts">
                                    <td width="180" class="bodyTextGreyLeftAligned">
                                        <b>
                                            <asp:Label ID="lblProductsLabel" runat="Server"></asp:Label>:</b>
                                    </td>
                                    <td class="bodyTextGreyLeftAligned">
                                        <asp:Label ID="lblProducts" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="smallText">
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div class="divAccountBotCurves">
                    <img src="~/Images/Curves/MAS-Bdr-BL.gif" title="" width="5" height="5" class="corner"
                        style="display: none" /></div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlServicesSelection" runat="server" Visible="false">
            <div class="divRatings" style="padding: 10px;">
                <span class="txtOrange"><strong>Services Selection</strong></span>
                <asp:GridView ID="grdServicesSelection" ForeColor="Maroon" Font-Size="11px" BorderWidth="1px"
                    BorderColor="Black" runat="server" AutoGenerateColumns="false" Width="1000px">
                    <Columns>
                        <asp:BoundField HeaderText="WOTitle" ItemStyle-Width="350px" DataField="WOTitle" />
                        <asp:BoundField HeaderText="SKU" ItemStyle-Width="350px" DataField="SKU" />
                        <asp:BoundField HeaderText="Quantity" ItemStyle-Width="80px" DataField="Quantity" />
                        <asp:TemplateField HeaderText="Customer Unit Price (Ex VAT)" ItemStyle-Width="80px">
                            <ItemTemplate>
                                <asp:Label ID="lblCustomerPrice" runat="server" Text='<%# string.Format("{0:C}", Eval("CustomerPrice"))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Customer Total Price (Ex VAT)" ItemStyle-Width="80px">
                            <ItemTemplate>
                                <asp:Label ID="lblCustomerTotalPrice" runat="server" Text='<%# string.Format("{0:C}", Eval("CustomerPrice") * Eval("Quantity"))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Wholesale Unit Price (Ex VAT)" ItemStyle-Width="80px">
                            <ItemTemplate>
                                <asp:Label ID="lblWholesalePrice" runat="server" Text='<%# string.Format("{0:C}", Eval("WholesalePrice"))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Wholesale Total Price (Ex VAT)" ItemStyle-Width="80px">
                            <ItemTemplate>
                                <asp:Label ID="lblWholesaleTotalPrice" runat="server" Text='<%# string.Format("{0:C}", Eval("WholesalePrice") * Eval("Quantity"))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Platform Unit Price (Ex VAT)" ItemStyle-Width="80px">
                            <ItemTemplate>
                                <asp:Label ID="lblPlatformPrice" runat="server" Text='<%# string.Format("{0:C}", Eval("PlatformPrice"))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Platform Total Price (Ex VAT)" ItemStyle-Width="80px">
                            <ItemTemplate>
                                <asp:Label ID="lblPlatformTotalPrice" runat="server" Text='<%# string.Format("{0:C}", Eval("PlatformPrice") * Eval("Quantity"))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlQAWOSubmit" runat="server" Visible="false">
            <div class="divRatings" style="padding: 10px;">
                <span class="txtOrange"><strong>Client Work Order Submit Responses</strong></span>
                <asp:Repeater ID="rpQAWOSubmit" runat="server">
                    <HeaderTemplate>
                        <table>
                            <tr class="bodyTextGreyLeftAligned">
                                <td style="width: 350px;" class="bodyTextGreyLeftAligned">
                                    <b>Question</b>
                                </td>
                                <td style="width: 20px;">
                                    &nbsp;
                                </td>
                                <td class="bodyTextGreyLeftAligned">
                                    <b>Answer</b>
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table class="bodyTextGreyLeftAligned">
                            <tr>
                                <td style="width: 350px;" class="bodyTextGreyLeftAligned">
                                    <%#Eval("Question")%>
                                </td>
                                <td style="width: 20px;" class="bodyTextGreyLeftAligned">
                                    :
                                </td>
                                <td class="bodyTextGreyLeftAligned">
                                    <%#Eval("Answer")%>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlQA" runat="server" Visible="false">
            <div class="divRatings" style="padding: 10px;">
                <span class="txtOrange"><strong>Supplier Responses</strong></span>
                <asp:Repeater ID="rpQA" runat="server">
                    <HeaderTemplate>
                        <table>
                            <tr class="bodyTextGreyLeftAligned">
                                <td style="width: 350px;" class="bodyTextGreyLeftAligned">
                                    <b>Question</b>
                                </td>
                                <td style="width: 20px;">
                                    &nbsp;
                                </td>
                                <td class="bodyTextGreyLeftAligned">
                                    <b>Answer</b>
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table class="bodyTextGreyLeftAligned">
                            <tr>
                                <td style="width: 350px;" class="bodyTextGreyLeftAligned">
                                    <%#Eval("Question")%>
                                </td>
                                <td style="width: 20px;" class="bodyTextGreyLeftAligned">
                                    :
                                </td>
                                <td class="bodyTextGreyLeftAligned">
                                    <%#Eval("Answer")%>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlThermostatsQuestions" runat="server" Visible="false">
            <div class="divRatings" id="div4">
                <div class="divAccountTopCurves">
                    <img src="~/Images/Curves/MAS-Bdr-TL.gif" title="" width="5" height="5" class="corner"
                        style="display: none" /></div>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr class="smallText">
                        <td width="15" height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td width="15" height="8" class="smallText">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <span class="txtOrange"><strong>
                                <asp:Label ID="Label1" runat="server" Text="Questionnaire"></asp:Label></strong></span>
                            <br>
                            <asp:Label ID="lblThermostatsQuestionsInfo" runat="server"></asp:Label></strong></span>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="smallText">
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div class="divAccountBotCurves">
                    <img src="~/Images/Curves/MAS-Bdr-BL.gif" title="" width="5" height="5" class="corner"
                        style="display: none" /></div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlClientContactInfo" runat="server">
            <div class="divRatings" id="divRatings">
                <div class="divAccountTopCurves">
                    <img src="~/Images/Curves/MAS-Bdr-TL.gif" title="" width="5" height="5" class="corner"
                        style="display: none" /></div>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr class="smallText">
                        <td width="15" height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td width="15" height="8" class="smallText">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <span class="txtOrange"><strong>
                                <asp:Label ID="lblBuyerContactLabel" runat="server"></asp:Label></strong></span>
                            <br>
                            <span class="bodyTextGreyLeftAligned"><strong>
                                <asp:Label ID="lblClientContact" runat="server"></asp:Label>
                                -
                                <asp:Label ID="lblClientCompany" runat="server"></asp:Label>
                                , </strong>
                                <asp:Label ID="lblClientAddress" runat="server"></asp:Label>
                                &nbsp;&nbsp; Tel:
                                <asp:Label ID="lblClientPhone" runat="server"></asp:Label>
                                &nbsp;&nbsp; Mobile:
                                <asp:Label ID="lblClientMobile" runat="server"></asp:Label>
                                <strong></strong><strong></strong>
                                <div id="divTempContactIfo" style="visibility: hidden; position: absolute" runat="server">
                                    <span class="txtOrange"><strong>Work Order Location Info:</strong></span><span class="txtOrange">
                                        <br>
                                    </span><strong>
                                        <asp:Label ID="lblTempClientContact" runat="server"></asp:Label>
                                        : </strong>
                                    <asp:Label ID="lblTempClientAddress" runat="server"></asp:Label>
                                    &nbsp;&nbsp; Tel:
                                    <asp:Label ID="lblTempClientPhone" runat="server"></asp:Label>
                                </div>
                            </span>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="15">
                            &nbsp;
                        </td>
                        <td class="bodyTextGreyLeftAligned" id="tdBtmRating" runat="server">
                            <strong>Rating Feedback :
                                <asp:Label ID="lblRating" runat="server"></asp:Label>
                            </strong>&nbsp;<img src="Images/Icons/Icon-Positive.gif" width="18" height="18" align="absmiddle">
                            Positive :
                            <asp:Label ID="lblPosRating" runat="server"></asp:Label>
                            <img src="Images/Icons/Icon-Neutral.gif" width="13" height="5" align="absmiddle">
                            Neutral :
                            <asp:Label ID="lblNeuRating" runat="server"></asp:Label>
                            <img src="Images/Icons/Icon-Negative.gif" width="17" height="18" align="absmiddle">
                            Negative :
                            <asp:Label ID="lblNegRating" runat="server"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="smallText">
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                        <td height="8" class="smallText">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div class="divAccountBotCurves">
                    <img src="~/Images/Curves/MAS-Bdr-BL.gif" title="" width="5" height="5" class="corner"
                        style="display: none" /></div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
