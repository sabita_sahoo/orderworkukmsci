<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCAdminWOsListing.ascx.vb"
    Inherits="Admin.UCAdminWOsListing" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>
<script type="text/javascript">
    function ShowPopup(Flag, LinkId, WorkOrderId) {
        var strFlag = Flag;
        var DivId = "PopupDiv" + WorkOrderId;
        if (Flag == "True") {

            var div1Pos = $("#" + LinkId).offset();
            var div1X = div1Pos.left + 80;
            var div1Y = div1Pos.top;
            $('#' + DivId).css({ left: div1X, top: div1Y });
            $("#" + DivId).show();
        }
        else {
            $("#" + DivId).hide();
        }
    }

    function validate_RateWO(field, alerttxt) {

        var text = document.getElementById(field).value;
        if (text == null || text == "" || text == "Enter a new comment here") {
            alert(alerttxt);
        }
        else {
            if (text.length > 500) {
                alert('Please enter comments less than 500 characters');
            }
            else {
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsListing1_hdnBtnRateWO").click();
            }
        }
    }

    function validate_ActionComment(field, alerttxt) {
        var NoteTypeValue = document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsListing1_ddlNoteType").value;
        if (NoteTypeValue == "") {
            alert('Please Select Note Type');
            return 0;
        }
        if (document.getElementById("pnlActionRating").style.display == "block") {

            var RatingSelected = 0;



            if (document.getElementById("rdBtnActionPositive").checked) {
                RatingSelected = 1;
            }
            else if (document.getElementById("rdBtnActionNeutral").checked) {
                RatingSelected = 1;
            }
            else if (document.getElementById("rdBtnActionNegative").checked) {
                RatingSelected = 1;
            }
            if (RatingSelected == 1) {
                var text = document.getElementById(field).value;
                if (text == null || text == "" || text == "Enter a new comment here") {
                    //alert(alerttxt);
                    document.getElementById("hdnBtnAction").click();
                }
                else {
                    if (text.length > 500) {
                        alert('Please enter comments less than 500 characters');
                    }
                    else {
                        document.getElementById("hdnBtnAction").click();
                    }
                }
            }
            else {
                document.getElementById("hdnBtnAction").click();
            }

        }
        else {
            document.getElementById("hdnBtnAction").click();
        }
    }

</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:GridView ID="gvWorkOrders" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            BorderWidth="1px" PageSize="25" BorderColor="White" DataKeyNames="WOID" PagerSettings-Mode="NextPreviousFirstLast"
            PagerSettings-Position="TopAndBottom" Width="100%" DataSourceID="ObjectDataSource1"
            AllowSorting="true">
            <EmptyDataTemplate>
                <table width="100%" border="0" cellpadding="10" cellspacing="0">
                    <tr>
                        <td align="center" style="text-align: center" valign="middle" height="300" class="txtWelcome">
                            Sorry! There are no records in this category.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <PagerTemplate>
                <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9"
                    id="tblMainTop" style="visibility: visible" runat="server">
                    <tr>
                        <td>
                            <table width="100%" id="tblTop" runat="server" border="0" cellpadding="0" cellspacing="0"
                                bgcolor="#DAD8D9">
                                <tr>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="right" valign="middle">
                                                    <div id="tdTotalCount" style="width: 200px; padding-right: 20px; text-align: right"
                                                        runat="server" class="formLabelGrey">
                                                    </div>
                                                </td>
                                                <td align="right" width="136" valign="middle" id="tdPagerUp" runat="server">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="right" width="36">
                                                                <div id="divDoubleBackUp" title="First" runat="server" style="float: left; vertical-align: bottom;
                                                                    padding-top: 2px;">
                                                                    <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif"
                                                                        CommandArgument="First" runat="server" />
                                                                </div>
                                                                <div id="divBackUp" title="Previous" runat="server" style="float: right; vertical-align: bottom;
                                                                    padding-top: 2px;">
                                                                    <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif"
                                                                        CommandArgument="Prev" runat="server" />
                                                                </div>
                                                            </td>
                                                            <td width="50" align="center" style="margin-right: 3px;">
                                                                <asp:DropDownList Style="margin-left: 4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged"
                                                                    CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td width="30" valign="bottom">
                                                                <div id="divNextUp" title="Next" runat="server" style="float: left; vertical-align: bottom;
                                                                    padding-top: 2px;">
                                                                    <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif"
                                                                        CommandArgument="Next" runat="server" />
                                                                </div>
                                                                <div id="divDoubleNextUp" title="Last" runat="server" style="float: right; vertical-align: bottom;
                                                                    padding-top: 2px;">
                                                                    <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif"
                                                                        CommandArgument="Last" runat="server" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </PagerTemplate>
            <Columns>
                <asp:TemplateField Visible="True" HeaderText="<input id='chkAll' onClick=CheckAll(this,'Check') type='checkbox' name='chkAll' />">
                    <HeaderStyle CssClass="gridHdr" />
                    <ItemStyle Wrap="true" HorizontalAlign="Left" Width="15px" CssClass="gridRowText" />
                    <ItemTemplate>
                        <asp:CheckBox ID="Check" runat="server"></asp:CheckBox>
                        <input type="hidden" runat="server" id="hdnWOIDs" value='<%#Container.DataItem("WOID")%>' />
                        <input type="hidden" runat="server" id="hdnWID" value='<%#Container.DataItem("WOID")%>' />
                        <input type="hidden" runat="server" id="hdnCID" value='<%#Container.DataItem("CompanyID")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="WO NO" HeaderStyle-CssClass="gridHdr gridText" SortExpression="RefWOID">
                    <ItemStyle CssClass="gridRowText" Width="110px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <%--<img src="Images/Icons/Flag.gif" title="Action" width="16" height="17" hspace="2" vspace="0" border="0" style="float:left;">--%>
                        <asp:Image ID="imgIsFlagged" runat="server" ImageUrl="~/Images/Icons/Flag.gif" hspace="2"
                            vspace="0" Width="16" Height="17" Style="float: left;" Visible='<%#IIf(Container.DataItem("IsFlagged") = "1" ,True,false)%>' />
                        <asp:Image ID="imgDepoShtMailed" runat="server" ImageUrl="~/Images/Icons/senticon.gif"
                            ImageAlign="absMiddle" Visible='<%#IIf(Container.DataItem("DepotSheetMailed") > 0 AND Request("mode") = admin.Applicationsettings.WOGroupAccepted ,True,False)%>'
                            onmouseover='<%# "javascript:ShowDepotSheetDetails(" & Container.DataItem("RefWOID") & ", " & Container.DataItem("DepotSheetMailed") & ")" %>'
                            onmouseout='<%# "javascript:ShowDepotSheetDetails(" & Container.DataItem("RefWOID") & ", " & Container.DataItem("DepotSheetMailed") & ")" %>' />&nbsp;<asp:Label
                                ID="lblWOID" runat="server" Text='<%#Container.DataItem("RefWOID")%>' onmouseover='<%# "javascript:ShowDepotSheetDetails(" & Container.DataItem("RefWOID") & ", " & Container.DataItem("DepotSheetMailed") & ")" %>'
                                onmouseout='<%# "javascript:ShowDepotSheetDetails(" & Container.DataItem("RefWOID") & ", " & Container.DataItem("DepotSheetMailed") & ")" %>'></asp:Label>
                        <div id='_divDepotSheet<%# DataBinder.Eval(Container.DataItem,"RefWOID") %>' style='visibility: hidden;
                            position: absolute; margin-left: -90px; margin-top: 20px; background-color: #FFFFE1;
                            border: solid 1px #000000; padding: 10px; font-size: 11px;'>
                            <asp:Repeater ID="RptDepotSheet" runat="server">
                                <ItemTemplate>
                                    <p>
                                        <b>Depot Sheet Mailed</b><br />
                                        by&nbsp;<asp:Label runat="server" ID="lblMailedby" Text='<%#Container.DataItem("Name")%>'> </asp:Label><br />
                                        on&nbsp;<asp:Label runat="server" ID="lblMailedOn" Text='<%#Container.DataItem("DateMailed")%>'> </asp:Label></p>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <span class="gridRowHighlight">
                            <%#IIf(Request("mode") = "CA", IIf(Container.DataItem("CACount") > 0, "(" & Container.DataItem("CACount") & ")", ""), "")%></span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="60px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="DateCreated" SortExpression="DateCreated" HeaderText="Created" DataFormatString="{0:dd/MM/yy}"
                    HtmlEncode="false" />
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="58px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="DateModified" SortExpression="DateModified" HeaderText="Modified"
                    DataFormatString="{0:dd/MM/yy}" HtmlEncode="false" />
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="70px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="Location" SortExpression="Location" HeaderText="Location" />
                <asp:TemplateField HeaderText="Customer" HeaderStyle-CssClass="gridHdr gridText"
                    SortExpression="CustomerName">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="40px" HorizontalAlign="Left" />
                    <ItemTemplate>
                        <%#IIf(Container.DataItem("AddressType") = "Temporary", Container.DataItem("CustomerName"), " ")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="70px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="BuyerCompany" SortExpression="BuyerCompany" HeaderText="Client" />
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="70px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="BuyerContact" SortExpression="BuyerContact" HeaderText="Client Contact" />
                <asp:TemplateField SortExpression="SupplierCompany" HeaderText="Supplier" HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="80px" />
                    <ItemTemplate>
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="260">
                                    <span style='cursor: hand;' onmouseout="javascript:ShowPopup('False','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);"
                                        onmouseover="javascript:ShowPopup('True','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);">
                                        <%#GetLinks(iif(ISDBNULL(Container.DataItem("BizDivID")),"",Container.DataItem("BizDivID")), iif(ISDBNULL(Container.DataItem("SupplierCompanyID")),"",Container.DataItem("SupplierCompanyID")), iif(ISDBNULL(Container.DataItem("SupplierContactID")),"",Container.DataItem("SupplierContactID")),iif(ISDBNULL(Container.DataItem("SupplierCompany")),"",Container.DataItem("SupplierCompany")))%>
                                    </span>
                                </td>
                                <td id="tdRating" visible="true" valign="top" runat="server">
                                    <div style="text-align: right;" id="PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>">
                                        <a style="text-align: right;" id="ShowPopupAnchor" onclick="javascript:ShowPopup('True','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);">
                                            <img id="imgTestQuickNote" src="Images/Icons/FindSuppliersforWorkOrder.gif" title="View Supplier Details"
                                                alt="View Supplier Details" width="16" height="15" hspace="2" vspace="0" border="0" /></a></div>
                                    <div id="PopupDiv<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>" style="display: none;
                                        position: absolute; margin-left: -50px;">
                                        <asp:Panel runat="server" ID="pnlServicePartner" Width="200px" Style="background-color: #FFEBDE;
                                            font-family: Tahoma, Arial, Verdana; font-size: 11px; color: #000000; text-decoration: none;
                                            line-height: 18px; border: 1px solid #CECBCE;">
                                            <div style="text-align: right;">
                                                <a style="color: Black; text-align: right; vertical-align: top; cursor: pointer;"
                                                    onclick="javascript:ShowPopup('False','PAnchor<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>',<%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>);">
                                                    <b>Close [X]</b></a></div>
                                            <asp:Label ID="lblContactName" runat="server" Text='<%# Cstr("<b>Contact Name: </b>" + iif(ISDBNULL(Container.DataItem("SupplierContact")),"--",Container.DataItem("SupplierContact"))) %>'></asp:Label><br />
                                            <asp:Label ID="lblPhoneNo" runat="server" Text='<%#Cstr("<b>Phone No: </b>" + iif(ISDBNULL(Container.DataItem("SupplierPhone")),"--",Container.DataItem("SupplierPhone"))) %>'></asp:Label><br />
                                            <asp:Label ID="lblMobileNo" runat="server" Text='<%#Cstr("<b>Mobile No: </b>" + iif(ISDBNULL(Container.DataItem("SupplierMobile")),"--",Container.DataItem("SupplierMobile"))) %>'></asp:Label><br />
                                            <asp:Label ID="lblEmail" runat="server" Text='<%#Cstr("<b>Email Id: </b>" + iif(ISDBNULL(Container.DataItem("SupplierEmail")),"--",Container.DataItem("SupplierEmail"))) %>'></asp:Label>
                                        </asp:Panel>
                                    </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="80px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="SupplierContact" SortExpression="SupplierContact" HeaderText="Supplier Contact" />
                <asp:BoundField ItemStyle-CssClass="gridRowText" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="WOTitle" SortExpression="WOTitle" HeaderText="WOTitle" />
                <asp:BoundField ItemStyle-CssClass="gridRowText" ItemStyle-Width="70px" HeaderStyle-CssClass="gridHdr gridText"
                    DataField="DateStart" SortExpression="DateStart" HeaderText="Start Date" DataFormatString="{0:dd/MM/yy}"
                    HtmlEncode="false" />
                <asp:TemplateField SortExpression="WholesalePrice" HeaderText="WP" HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="40px" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#Container.DataItem("WholesalePrice")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="PlatformPrice" HeaderText="PP" HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="40px" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <span id="SpanPP" runat="server">
                            <%#Container.DataItem("PlatformPrice")%></span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="UpSellPrice" HeaderText="UP" HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="40px" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#Container.DataItem("UpSellPrice")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="BillingLocation" HeaderText="Billing Location"
                    HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="80px" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#Container.DataItem("BillingLocation")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField SortExpression="Status" Visible="false" HeaderText="Status" HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText" Wrap="true" Width="80px" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#Container.DataItem("Status")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--  <asp:TemplateField visible="False" HeaderText="Status" HeaderStyle-CssClass="gridHdr gridText"> 
                <ItemStyle CssClass="gridRowHighlight gridText" Wrap=true  Width="60px" />
			   <ItemTemplate> 
                    <%#IIF(Request("mode") = "CA", IIF (Container.DataItem("CACount") > 0,Container.DataItem("Status")  &  "(" & Container.DataItem("CACount") & ")",Container.DataItem("Status")),Container.DataItem("Status"))%>            
				</ItemTemplate>
               </asp:TemplateField>--%>
                <asp:TemplateField Visible="False" SortExpression="InvoiceNo" HeaderText="InvoiceNo"
                    HeaderStyle-CssClass="gridHdr gridText">
                    <ItemStyle CssClass="gridRowText gridText" Wrap="true" Width="60px" />
                    <ItemTemplate>
                        <%#iif(ISDBNULL(Container.DataItem("InvoiceNo")),"--",Container.DataItem("InvoiceNo"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" id="lnkDetail" href='<%#"~/AdminWODetails.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("RefWOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & IIF(Request("mode")<>"Watched", Request("mode"), Container.DataItem("status")) & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & iif(BizDiv <> "", BizDiv, admin.Applicationsettings.OWUKBizDivId)  & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID & "&" & "IsNextDay=" & IsNextDay & "&" & "IsWatched=" & IIF(Request("mode")<>"Watched", 0, 1) %>'>
                            <img src="Images/Icons/ViewDetails.gif" alt="View Details" title="View Details" width="16"
                                height="16" hspace="2" vspace="0" border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" id="lnkEdit" href='<%#"~/WOForm.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID %>'>
                            <img src="Images/Icons/Edit.gif" title="Edit" width="12" height="11" hspace="2" vspace="0"
                                border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" id="lnkViewHistory" href='<%#"~/AdminWOHistory.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("RefWOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & IIF(Request("mode")<>"Watched", Request("mode"), Container.DataItem("status")) & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & iif(BizDiv <> "", BizDiv, admin.Applicationsettings.OWUKBizDivId)  & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID & "&" & "IsNextDay=" & IsNextDay & "&" & "IsWatched=" & IIF(Request("mode")<>"Watched", 0, 1)%>'>
                            <img src="Images/Icons/ViewHistory.gif" alt="View Details" title="View History" hspace="2"
                                vspace="0" border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <%#ShowHideOrdermatchIcon(Container.DataItem("PlatformPrice"), Container.DataItem("WOID"), Request("mode"), FromDate, ToDate, Container.DataItem("ContactID"), Container.DataItem("CompanyID"), Container.DataItem("SupplierContactID"), Container.DataItem("SupplierCompanyID"), gvWorkOrders.PageSize, gvWorkOrders.PageIndex, gvWorkOrders.SortExpression, gvWorkOrders.SortDirection, BizDiv, CompID, Container.DataItem("Status"), Container.DataItem("BillingLocID"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" id="lnkRaiseIssue" href='<%#"~/WORaiseIssue.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID & "&" & "IsNextDay=" & IsNextDay & "&" & "Action=RaiseIssue"%>'>
                            <img src="Images/Icons/RaiseIssue.gif" title="Raise Issue" width="14" height="13"
                                hspace="2" vspace="0" border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" id="lnkAcceptIssue" href='<%#"~/WOForm.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID%>'>
                            <img src="Images/Icons/Accept-Issue.gif" title="Accept Issue" width="14" height="13"
                                hspace="2" vspace="0" border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" id="lnkComplete" visible='<%#ShowHideCompleteWOIcon(Container.DataItem("StagedWO"))%>'
                            href='<%#"~/WOComplete.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID & "&" & "IsNextDay=" & IsNextDay%>'>
                            <img src="Images/Icons/Complete.gif" title="Complete" width="12" height="12" hspace="2"
                                vspace="0" border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="false" />
                    <ItemTemplate>
                        <a runat="server" id="lnkChangeWholesalePrice" visible='<%#IIF(Request("mode") = "CA",False,True)%>'
                            href='<%#"~/WOSetWholesalePrice.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID & "&" & "IsNextDay=" & IsNextDay%>'>
                            <img src="Images/Icons/SetWholesalePrice.gif" title="Set Wholesale Price" width="18"
                                height="12" hspace="2" vspace="3" border="0"></a> <a runat="server" visible='<%#IIF(Request("mode") = "CA",True,False)%>'
                                    id="lnkUpdateWO" href='<%#"~/WOSetWholesalePrice.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID & "&" & "IsNextDay=" & IsNextDay & "&IsUpdate=yes"%>'>
                                    <img src="Images/Icons/UpdateWO.gif" title="Update Workorder" width="16" height="16"
                                        hspace="1" vspace="0" border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <%#ShowHidePlatformPriceIcon(Container.DataItem("PlatformPrice"),Container.DataItem("WOID"),Request("mode"),FromDate,ToDate,Container.DataItem("ContactID"),Container.DataItem("CompanyID"),Container.DataItem("SupplierContactID"),Container.DataItem("SupplierCompanyID"),gvWorkOrders.PageSize,gvWorkOrders.PageIndex,gvWorkOrders.SortExpression,gvWorkOrders.SortDirection,BizDiv,CompID,Container.DataItem("Status"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" id="lnkClose" href='<%#"~/WOClose.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID%>'>
                            <img src="Images/Icons/Close.gif" title="Close" width="13" height="14" hspace="2"
                                vspace="0" border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" id="lnkCloseIssue" href='<%#"~/WOForm.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID%>'>
                            <img src="Images/Icons/CloseIssue.gif" title="Close Issue" width="15" height="14"
                                hspace="2" vspace="0" border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" id="lnkCopy" href='<%#"~/WOForm.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID%>'>
                            <img src="Images/Icons/Copy.gif" title="Copy" width="15" height="13" hspace="2" vspace="0"
                                border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <%#ShowHideIcon(Container.DataItem("BuyerAccepted"),Container.DataItem("WOID"),Request("mode"),FromDate,ToDate,Container.DataItem("ContactID"),Container.DataItem("CompanyID"),Container.DataItem("SupplierContactID"),Container.DataItem("SupplierCompanyID"),gvWorkOrders.PageSize,gvWorkOrders.PageIndex,gvWorkOrders.SortExpression,gvWorkOrders.SortDirection,BizDiv,CompID,Container.DataItem("WPChangedStatus"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" id="lnkSplitWO" href='<%#"~/SplitWO.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "WorkOrderID=" & Container.DataItem("RefWOID") & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID%>'>
                            <img src="Images/Icons/Split-WO.gif" title="Generate Stages" width="13" height="13"
                                hspace="2" vspace="0" border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnQuickNote" CommandName="AddNote" CausesValidation="false"
                            CommandArgument='<%#Container.dataitem("WOID") %>' runat="server">
                            <img id="imgQuickNote" runat="server" src='<%# IIF(Container.DataItem("NtCnt") = 0,"~/Images/Icons/QuickNote.gif","~/Images/Icons/Note.gif") %>'
                                title='<%# IIF(Container.DataItem("NtCnt") = 0,"Add a Quick Note","") %>' width="13"
                                height="13" hspace="2" vspace="0" border="0" class="cursorHand" />
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" visible='<%# IIF(Container.DataItem("NtCnt") = 0,False,True) %>'
                            id="lnkNotes" href='<%#"~/WONotes.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID%>'>
                            <img src="Images/Icons/Note.gif" width="10" height="11" hspace="2" vspace="0" border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a runat="server" id="lnkCancel" href='<%#"~/WOCancellation.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("RefWOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=" & gvWorkOrders.PageSize & "&" & "PN=" & gvWorkOrders.PageIndex & "&" & "SC=" & gvWorkOrders.SortExpression & "&" & "SO=" & gvWorkOrders.SortDirection & "&" & "BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID & "&" & "IsNextDay=" & IsNextDay%>'>
                            <img src="Images/Icons/Cancel.gif" title="Cancel" width="11" height="11" hspace="2"
                                vspace="0" border="0"></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <a id="aRemove" style="cursor: pointer;" onclick="javascript:xShowModalDialog('RemoveAttachments.aspx?WOID=<%#DataBinder.Eval(Container.DataItem,"WOID") %>&WOStatus=<%#DataBinder.Eval(Container.DataItem,"WOStatus") %>','Sign',320,140);">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Icons/icon1-attachement_remove.gif"
                                AlternateText="Remove Attachment" ToolTip="Remove Attachment" hspace="2" vspace="0"
                                border="0" Visible='<%# IIF(Container.DataItem("AttachmentCount") = 0,False,True) %>' /></a>
                        <a id="aUpload" style="cursor: pointer;" onclick="javascript:xShowModalDialog('FileUpload.aspx?WOID=<%#DataBinder.Eval(Container.DataItem,"WOID") %>&CompanyID=<%#DataBinder.Eval(Container.DataItem,"CompanyID") %>&BizDivID=<%#DataBinder.Eval(Container.DataItem,"BizDivID") %>&WOStatus=<%#DataBinder.Eval(Container.DataItem,"WOStatus") %>','Sign',800,170);">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Icons/icon-attachement.gif"
                                AlternateText="Upload Attachment" ToolTip="Upload Attachment" hspace="2" vspace="0"
                                border="0" Visible='<%# IIF(Container.DataItem("AttachmentCount") = 0,True,False) %>' /></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnUpdateRating" CausesValidation="false" CommandName="RateWO"
                            CommandArgument='<%#Container.dataitem("WOID") %>' runat="server">
                            <img id="imgUpdateRating" runat="server" src="~/Images/Icons/Update_rating.gif" title="Rate WO"
                                width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" /></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField  HeaderStyle-CssClass="gridHdr">                
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true"  Width="14px"/>
                    <ItemTemplate> 
                        <a runat="server" id="lnkAction" href="#"><img src="Images/Icons/Action-icon.gif" title="Action" width="14" height="15" hspace="2" vspace="0" border="0"></a>
				    </ItemTemplate>  
               </asp:TemplateField>--%>
                <asp:TemplateField Visible="true" HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <span style='cursor: hand;' onmouseout="javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','hidden')"
                            onmouseover="javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','visible')">
                            <asp:LinkButton ID="lnkbtnAction" CommandName="Action" CausesValidation="false" CommandArgument='<%#Container.DataItem("WOID") & "," & Container.DataItem("Rating") & "," & Container.DataItem("RatingComment") & "," & Container.DataItem("IsWatch") & "," & Container.DataItem("IsFlagged")%>'
                                runat="server">
                                <img id="imgAction" runat="server" src='<%#GetActionIcon(Container.DataItem("NtCnt"),Container.DataItem("Rating"))%>'
                                    width="24" height="30" hspace="2" vspace="0" border="0" class="cursorHand" />
                            </asp:LinkButton>
                        </span>
                        <div class="divUserNotesDetail" id='divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>'
                            onmouseout="javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','hidden')"
                            onmouseover="javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','visible')">
                            <asp:Panel runat="server" Style="color: black; width: 230px; max-height: 320px; overflow: scroll;"
                                ID="pnlUserNotesDetail" Visible='<%# IIF(Container.DataItem("NtCnt") = 0,IIF(Container.DataItem("RatingComment") = "",False,True) ,True) %>'>
                                <div>
                                    <b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
                                </div>
                                <div class="clsRatingInner">
                                    <div id="divRatingComment" runat="server" visible='<%# IIF(Container.DataItem("RatingComment") = "",False,True) %>'>
                                        <b style="color: #0C96CF; font-size: 12px;">Rating Comment:</b>
                                        <div style="margin-top: 0px; margin-bottom: 10px;">
                                            <%# Container.DataItem("RatingComment")%>
                                        </div>
                                    </div>
                                    <div id="divNotes" runat="server" visible='<%# IIF(Container.DataItem("NtCnt") = 0,False,True) %>'>
                                        <b style="color: #0C96CF; font-size: 12px;">Notes:</b>
                                        <asp:Repeater ID="DLUserNotesDetail" runat="server" DataSource='<%#GetUserNotesDetail(Container.DataItem("WOID"))%>'>
                                            <ItemTemplate>
                                                <div style='visible: <%# iif( Trim(Container.DataItem("Comments")) = "" , "hidden", "visible") %>'>
                                                    <div>
                                                        <%--poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2 --%>
                                                        <%# "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - <b>" & Container.DataItem("OWRepFName") & " " & Container.DataItem("OWRepLName") & "</b><br/><b>" & Container.DataItem("NoteCategory") & "</b></span>"%>
                                                    </div>
                                                    <div style="margin-top: 0px; margin-bottom: 10px;">
                                                        <%# Container.DataItem("Comments") %>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div>
                                    <b class="b4"></b><b class="b3"></b><b class="b2"></b><b class="b1"></b>
                                </div>
                            </asp:Panel>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnWatch" CommandName="Watch" CausesValidation="false" runat="server"
                            CommandArgument='<%#Container.DataItem("WOID") %>' Visible='<%# IIF(Container.DataItem("IsWatch") = 0,True,False) %>'>
                            <img id="imgwatch" runat="server" src="~/Images/Icons/open.png" width="24" height="20"
                                hspace="2" vspace="0" border="0" class="cursorHand" />
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnUnwatch" CommandName="Unwatch" CausesValidation="false"
                            CommandArgument='<%#Container.DataItem("WOID") %>' runat="server" Visible='<%# IIF(Container.DataItem("IsWatch") = 1,True,False) %>'>
                            <img id="imgUnwatch" runat="server" src="~/Images/Icons/Closed.png" width="24" height="15"
                                hspace="2" vspace="0" border="0" class="cursorHand" />
                         </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="gridviewFooterColor" />
            <AlternatingRowStyle CssClass="gridRowGreyHiglight"></AlternatingRowStyle>
        </asp:GridView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="SelectFromDB" EnablePaging="True" SelectCountMethod="SelectCount"
            TypeName="Admin.UCAdminWOsListing" SortParameterName="sortExpression"></asp:ObjectDataSource>
        <cc1:ModalPopupExtender ID="mdlUpdateRating" runat="server" TargetControlID="btnTempRateWO"
            PopupControlID="pnlUpdateWO" OkControlID="" CancelControlID="ancCancel1" BackgroundCssClass="modalBackground"
            Drag="False" DropShadow="False">
        </cc1:ModalPopupExtender>
        <asp:Panel runat="server" ID="pnlUpdateWO" Width="300px" CssClass="pnlConfirmWOListing"
            Style="display: none;">
            <div id="divModalParent">
                <b>Rate workorder</b>
                <br />
                <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnPositive" Checked="false" runat="server"
                    Text="Positive" />
                <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNeutral" Checked="false" runat="server"
                    Text="Neutral" />
                <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNegative" Checked="false" runat="server"
                    Text="Negative" /><br />
                <br />
                <b>Add comment here</b><br />
                <asp:TextBox runat="server" ID="txtComment" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned"
                    TextMode="MultiLine" onblur="javascript:removeHTML(this.id);"></asp:TextBox>
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtComment"
                    WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned">
                </cc1:TextBoxWatermarkExtender>
                <div id="divButton" class="divButton" style="width: 85px; float: left;">
                    <div class="bottonTopGrey">
                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" href='javascript:validate_RateWO("ctl00_ContentPlaceHolder1_UCAdminWOsListing1_txtComment","Enter a new comment here")'
                        id="a1"><strong>Submit</strong></a>
                    <div class="bottonBottomGrey">
                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                </div>
                <div id="divButton" class="divButton" style="width: 85px; float: left; margin-left: 20px;">
                    <div class="bottonTopGrey">
                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancCancel1"><strong>Cancel</strong></a>
                    <div class="bottonBottomGrey">
                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                            height="4" class="btnCorner" style="display: none" /></div>
                </div>
            </div>
        </asp:Panel>
        <asp:Button runat="server" ID="hdnBtnRateWO" OnClick="hdnBtnRateWO_Click" Height="0"
            Width="0" BorderWidth="0" Style="visibility: hidden;" />
        <asp:Button runat="server" ID="btnTempRateWO" Height="0" Width="0" BorderWidth="0"
            Style="visibility: hidden;" />
        <cc1:ModalPopupExtender ID="mdlAction" runat="server" TargetControlID="btnActionTemp"
            PopupControlID="pnlActionConfirm" OkControlID="" CancelControlID="ancCancelAction"
            BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
        </cc1:ModalPopupExtender>
        <asp:Panel runat="server" ID="pnlActionConfirm" Width="340px" CssClass="pnlConfirmWOListingForAction"
            Style="display: none;">
            <div id="div1">
                <div class="divTopActionStyle">
                    <b>OrderWork Action Form</b>
                </div>
                <asp:Panel ClientIDMode="Static" CssClass="divTopActionStyle" ID="pnlActionRating"
                    runat="server" Style="display: none;">
                    <b>Rate workorder</b>
                    <br />
                    <asp:RadioButton GroupName="SelectedActionRatings" ID="rdBtnActionPositive" Checked="false"
                        ClientIDMode="Static" runat="server" Text="Positive" />
                    <asp:RadioButton GroupName="SelectedActionRatings" ID="rdBtnActionNeutral" Checked="false"
                        ClientIDMode="Static" runat="server" Text="Neutral" />
                    <asp:RadioButton GroupName="SelectedActionRatings" ID="rdBtnActionNegative" Checked="false"
                        ClientIDMode="Static" runat="server" Text="Negative" /><br />
                    Add comment for rating<br />
                    <asp:TextBox runat="server" ID="txtCommentAction" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned"
                        TextMode="MultiLine" onblur="javascript:removeHTML(this.id);"></asp:TextBox>
                    <%-- <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtComment" WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        --%>
                </asp:Panel>
                <div class="divTopActionStyle">
                    <b>Post Quick Note</b><br />
                    <asp:TextBox runat="server" ID="txtNoteAction" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned"
                        TextMode="MultiLine" onblur="javascript:removeHTML(this.id);"></asp:TextBox>
                    <%--poonam modified on 8/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types--%>
                    <br />
                    <b>Note Type</b><br />
                    <asp:DropDownList Width="280px" Height="25px" CssClass="bodyTextGreyLeftAligned"
                        ID="ddlNoteType" runat="server" AutoPostBack="false">
                    </asp:DropDownList>
                    <br />
                    <span style="color: #e51312; font-size: 11px;">*Notes entered here are NOT visible to
                        the Suppliers/Clients.</span><br />
                    <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtNote" WatermarkText="Enter a new note here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        --%>
                </div>
                <div class="divTopActionStyle" id="div2" runat="server">
                    <asp:CheckBox ID="chkShowClient" runat="server" Style="font-weight: bold;" Text="Show To Client">
                    </asp:CheckBox>
                    <asp:CheckBox ID="chkShowSupplier" runat="server" Style="font-weight: bold;" Text="Show To Supplier">
                    </asp:CheckBox>
                </div>
                <div class="divTopActionStyle" id="divActionWatch" runat="server" style="display: none;">
                    <asp:CheckBox ID="chkMyWatch" runat="server" Style="font-weight: bold;" Text="Add To MyWatch">
                    </asp:CheckBox>
                </div>
                <div class="divTopActionStyle" style="font-weight: bold;">
                    <asp:CheckBox ID="chkMyRedFlag" runat="server" Text="Add To MyRedFlag"></asp:CheckBox>
                </div>
                <div style="padding-left: 20px; padding-top: 10px; float: left; width: 305px;">
                    <div class="divButtonAction" style="width: 85px; float: left; cursor: pointer;">
                        <div class="bottonTopGrey">
                            <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                height="4" class="btnCorner" /></div>
                        <a class="buttonText cursorHand" onclick='javascript:validate_ActionComment("ctl00_ContentPlaceHolder1_UCAdminWOsListing1_txtCommentAction","Please enter rating comment")'
                            id="ancSubmitAction">Submit</a>
                        <div class="bottonBottomGrey">
                            <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                height="4" class="btnCorner" /></div>
                    </div>
                    <div class="divButtonAction" style="width: 85px; float: left; margin-left: 20px;
                        cursor: pointer;">
                        <div class="bottonTopGrey">
                            <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                height="4" class="btnCorner" style="display: none" /></div>
                        <a class="buttonText cursorHand" runat="server" id="ancCancelAction">Cancel</a>
                        <div class="bottonBottomGrey">
                            <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                height="4" class="btnCorner" style="display: none" /></div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Button runat="server" ID="hdnBtnAction" ClientIDMode="Static" Height="0" Width="0"
            BorderWidth="0" Style="visibility: hidden;" />
        <asp:Button runat="server" ID="btnActionTemp" Height="0" Width="0" BorderWidth="0"
            Style="visibility: hidden;" />
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
    <ProgressTemplate>
        <div class="gridText">
            <img align="middle" src="Images/indicator.gif" />
            <b>Fetching Data... Please Wait</b>
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="gvWorkOrders"
    CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
<asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" Style="visibility: hidden;" />
