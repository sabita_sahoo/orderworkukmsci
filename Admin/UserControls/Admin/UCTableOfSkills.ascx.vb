﻿Public Class UCTableOfSkills
    Inherits System.Web.UI.UserControl

    Public Property BizDivId() As Integer
        Get
            If Not IsNothing(ViewState("AOEBizDivId")) Then
                Return ViewState("AOEBizDivId")
            Else
                Return 1
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("AOEBizDivId") = value
        End Set
    End Property
    Public Property CombIds() As String
        Get
            If Not IsNothing(ViewState("AOECombIds")) Then
                Return ViewState("AOECombIds")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("AOECombIds") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            PopulateAOE()
        End If
    End Sub
    Public Sub PopulateAOE()
        If (CombIds = "" Or CombIds = "0") Then
            hdnCombIDs.Value = "0"
        Else
            If (CombIds.Substring(0, 2) = "0,") Then
                hdnCombIDs.Value = CombIds
            Else
                hdnCombIDs.Value = "0," & CombIds
            End If
        End If

        Dim dsAOE As DataSet
        'SyncLock Me
        '    If IsNothing(Application("LinkToDB")) Then
        '        Try
        '            Application("LinkToDB") = New OrderWorkUK.DBHelperClass(OrderWorkUK.DBHelperClass.DatabaseType.SQLServer, ConfigurationManager.AppSettings("connection.webdbUK"))
        '        Catch ex As Exception

        '        Finally

        '        End Try
        '    End If
        'End SyncLock

        'OrderWorkLibrary.CommonFunctions.setCultureSettings(OrderWorkLibrary.ApplicationSettings.CountryUK, OrderWorkLibrary.ApplicationSettings.SiteType)
        'Dim conn As SqlConnection = OrderWorkLibrary.DBUtil.getDBConn

        'Dim ProcedureName As String = "spContacts_GetAOE"
        'Dim cmd As New SqlCommand(ProcedureName, conn)
        'Dim ParamList As New List(Of Common.DbParameter)

        ''setting the command type and also parameter of the Stored procedure
        'ParamList.Add(cmd.Parameters.AddWithValue("@combIds", CombIds))
        'ParamList.Add(cmd.Parameters.AddWithValue("@BizDivId", BizDivId))
        'dsAOE = DataTableCommand({"AOE"}, "spContacts_GetAOE", ParamList, CommandType.StoredProcedure)

        dsAOE = Admin.CommonFunctions.GetContactsAOE(CombIds, BizDivId)

        'Dim dbClass As OrderWorkUK.DBHelperClass = DirectCast(Application("LinkToDB"), OrderWorkUK.DBHelperClass)
        'Dim ParamList As New List(Of Common.DbParameter)
        'ParamList.Add(dbClass.NewParameter("@combIds", CombIds, DbType.String, 50))
        'ParamList.Add(dbClass.NewParameter("@BizDivId", BizDivId, DbType.Int32))
        'dsAOE = dbClass.DataTableCommand({"AOE"}, "spContacts_GetAOE", ParamList, CommandType.StoredProcedure)

        Session.Add("dsAOE" & Session.SessionID, dsAOE)
        Dim dtAOEMain As New DataTable
        Dim dvAOEMain As New DataView
        dtAOEMain = dsAOE.Tables(0)
        dvAOEMain = dsAOE.Tables(0).DefaultView

        dvAOEMain.RowFilter = "CombID NOT IN (148,156,153,244,258)"
        rptAOEMainIT.DataSource = dvAOEMain
        rptAOEMainIT.DataBind()

        dvAOEMain.RowFilter = ""

        dvAOEMain.RowFilter = "CombID IN (148,156,153,244,258)"
        rptAOEMainRetail.DataSource = dvAOEMain
        rptAOEMainRetail.DataBind()

    End Sub


    Private Sub rptAOEMainIT_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptAOEMainIT.ItemDataBound
        Dim rptAOESubIT As Repeater = CType(e.Item.FindControl("rptAOESubIT"), Repeater)
        Dim lblMainCatNameIT As Label = CType(e.Item.FindControl("lblMainCatNameIT"), Label)
        Dim ds As New DataSet
        ds = CType(Session("dsAOE" & Session.SessionID), DataSet)

        Dim dv As New DataView

        If ds.Tables(1).Rows.Count > 0 Then
            dv = ds.Tables(1).DefaultView
            dv.RowFilter = "MainCatName = '" & lblMainCatNameIT.Text & "'"

            rptAOESubIT.DataSource = dv.ToTable.DefaultView
            rptAOESubIT.DataBind()
        End If
    End Sub


    Private Sub rptAOEMainRetail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptAOEMainRetail.ItemDataBound
        Dim rptAOESubRetail As Repeater = CType(e.Item.FindControl("rptAOESubRetail"), Repeater)
        Dim lblMainCatNameRetail As Label = CType(e.Item.FindControl("lblMainCatNameRetail"), Label)
        Dim ds As New DataSet
        ds = CType(Session("dsAOE" & Session.SessionID), DataSet)

        Dim dv As New DataView

        If ds.Tables(1).Rows.Count > 0 Then
            dv = ds.Tables(1).DefaultView
            dv.RowFilter = "MainCatName = '" & lblMainCatNameRetail.Text & "'"

            rptAOESubRetail.DataSource = dv.ToTable.DefaultView
            rptAOESubRetail.DataBind()
        End If
    End Sub

End Class