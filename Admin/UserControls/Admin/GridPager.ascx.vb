Public Partial Class GridPager
    Inherits System.Web.UI.UserControl

#Region "Declaration"

    ''' <summary>
    ''' Page Size of Grid
    ''' </summary>
    ''' <remarks></remarks>
    Private _pageSize As Integer
    Public Property PageSize() As Integer
        Get
            Return _pageSize
        End Get
        Set(ByVal value As Integer)
            _pageSize = value
        End Set
    End Property

    ''' <summary>
    ''' Total Number of Records
    ''' </summary>
    ''' <remarks></remarks>
    Private _totalCount As Integer
    Public Property TotalCount() As Integer
        Get
            Return _totalCount
        End Get
        Set(ByVal value As Integer)
            _totalCount = value
        End Set
    End Property

    ''' <summary>
    ''' Page Number - Grid View displaying data of which Page.
    ''' </summary>
    ''' <remarks></remarks>
    Private _startIndex As Integer
    Public Property StartIndex() As Integer
        Get
            Return _startIndex
        End Get
        Set(ByVal value As Integer)
            _startIndex = value
        End Set
    End Property

    ''' <summary>
    ''' Listing page has one or two pagers
    ''' </summary>
    ''' <remarks></remarks>
    Private _hasSinglePager As Boolean
    Public Property HasSinglePager() As Boolean
        Get
            Return _hasSinglePager
        End Get
        Set(ByVal value As Boolean)
            _hasSinglePager = value
        End Set
    End Property

    ''' <summary>
    ''' Delegate Function
    ''' </summary>
    ''' <remarks></remarks>
    Private _delUpdateIndex As System.Delegate
    Public WriteOnly Property UpdateIndex() As System.Delegate
        Set(ByVal Value As System.Delegate)
            _delUpdateIndex = Value
        End Set
    End Property

    ''' <summary>
    ''' Page Object 
    ''' </summary>
    ''' <remarks></remarks>
    Private _pageObj As Page = Nothing
    Public Property PageObj() As Page
        Get
            Return _pageObj
        End Get
        Set(ByVal value As Page)
            _pageObj = value
        End Set
    End Property

    ''' <summary>
    ''' User Control object
    ''' </summary>
    ''' <remarks></remarks>
    Private _ucObj As UserControl = Nothing
    Public Property UCObj() As UserControl
        Get
            Return _ucObj
        End Get
        Set(ByVal value As UserControl)
            _ucObj = value
        End Set
    End Property

    ''' <summary>
    ''' Pager Group
    ''' </summary>
    ''' <remarks></remarks>
    Private _PagerGroup As String = ""
    Public Property PagerGroup() As String
        Get
            Return _PagerGroup
        End Get
        Set(ByVal value As String)
            _PagerGroup = value
            If PagerGroup <> "" Then
                ViewState("PagerGroup") = PagerGroup & "_"
            Else
                ViewState("PagerGroup") = ""
            End If
        End Set
    End Property

    ''' <summary>
    ''' If set to True hides the record message
    ''' </summary>
    ''' <remarks></remarks>
    Private _hideRecordMessage As Boolean = False
    Public Property hideRecordMessage() As Boolean
        Get
            Return _hideRecordMessage
        End Get
        Set(ByVal value As Boolean)
            _hideRecordMessage = value
        End Set
    End Property
    ''' <summary>
    ''' HideFirstLastDropDown
    ''' </summary>
    ''' <remarks></remarks>
    Private _hideFirstLastDropdown As Boolean = False
    Public Property hideFirstLastDropdown() As Boolean
        Get
            Return _hideFirstLastDropdown
        End Get
        Set(ByVal value As Boolean)
            _hideFirstLastDropdown = value
        End Set
    End Property


    ''' <summary>
    ''' To check if the page uses master page
    ''' </summary>
    ''' <remarks></remarks>
    Private _useMaster As Boolean = False
    Public Property useMaster() As Boolean
        Get
            Return _useMaster
        End Get
        Set(ByVal value As Boolean)
            _useMaster = value
        End Set
    End Property

    ''' <summary>
    ''' Pass the content place holder id where the grid pager is added
    ''' </summary>
    ''' <remarks></remarks>
    Private _ContentPlaceHolderId As String = ""
    Public Property ContentPlaceHolderId() As String
        Get
            Return _ContentPlaceHolderId
        End Get
        Set(ByVal value As String)
            _ContentPlaceHolderId = value
        End Set
    End Property

    Private _ImagePath As String = ""
    Public Property ImagePath() As String
        Get
            Return _ImagePath
        End Get
        Set(ByVal value As String)
            _ImagePath = value
        End Set
    End Property
    Public Property CompID() As String
        Get
            If Not IsNothing(ViewState("CompID")) Then
                Return ViewState("CompID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("CompID") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If _hideFirstLastDropdown = True Then
            ddPageNumber.Visible = False
            btnLast.Visible = False
            btnFirst.Visible = False
        End If

        If (ImagePath <> "") Then
            SetImagePath()
        End If
    End Sub

#Region "Pager Settings"

    Private Sub SetImagePath()
        If HasSinglePager = True Then
            If Not (IsNothing(PageObj)) Then
                'IF master page is used
                If useMaster Then
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), Image).ImageUrl = ImagePath & "first.gif"
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), Image).ImageUrl = ImagePath & "back.gif"
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), Image).ImageUrl = ImagePath & "next.gif"
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), Image).ImageUrl = ImagePath & "last.gif"
                Else
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), Image).ImageUrl = ImagePath & "first.gif"
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), Image).ImageUrl = ImagePath & "back.gif"
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), Image).ImageUrl = ImagePath & "next.gif"
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), Image).ImageUrl = ImagePath & "last.gif"
                End If
            Else
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), Image).ImageUrl = ImagePath & "first.gif"
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), Image).ImageUrl = ImagePath & "back.gif"
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), Image).ImageUrl = ImagePath & "next.gif"
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), Image).ImageUrl = ImagePath & "last.gif"
            End If
        Else
            If Not (IsNothing(PageObj)) Then
                ' If Grid Pager is inside a Page
                'IF master page is used
                If useMaster Then
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), Image).ImageUrl = ImagePath & "first.gif"
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), Image).ImageUrl = ImagePath & "back.gif"
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), Image).ImageUrl = ImagePath & "next.gif"
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), Image).ImageUrl = ImagePath & "last.gif"

                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnFirst"), Image).ImageUrl = ImagePath & "first.gif"
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnPrevious"), Image).ImageUrl = ImagePath & "back.gif"
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnNext"), Image).ImageUrl = ImagePath & "next.gif"
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnLast"), Image).ImageUrl = ImagePath & "last.gif"
                Else
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), Image).ImageUrl = ImagePath & "first.gif"
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), Image).ImageUrl = ImagePath & "back.gif"
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), Image).ImageUrl = ImagePath & "next.gif"
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), Image).ImageUrl = ImagePath & "last.gif"

                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnFirst"), Image).ImageUrl = ImagePath & "first.gif"
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnPrevious"), Image).ImageUrl = ImagePath & "back.gif"
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnNext"), Image).ImageUrl = ImagePath & "next.gif"
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnLast"), Image).ImageUrl = ImagePath & "last.gif"
                End If

            ElseIf Not (IsNothing(UCObj)) Then
                'If Grid Pager is Inside an UC
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), Image).ImageUrl = ImagePath & "first.gif"
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), Image).ImageUrl = ImagePath & "back.gif"
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), Image).ImageUrl = ImagePath & "next.gif"
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), Image).ImageUrl = ImagePath & "last.gif"

                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnFirst"), Image).ImageUrl = ImagePath & "first.gif"
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnPrevious"), Image).ImageUrl = ImagePath & "back.gif"
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnNext"), Image).ImageUrl = ImagePath & "next.gif"
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnLast"), Image).ImageUrl = ImagePath & "last.gif"
            End If
        End If
    End Sub

    ''' <summary>
    ''' Initialize the Pager. I.e. If the total number of records exceed the pagesize is enabled.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub InitializePager()
        Dim i As Integer
        Dim pages As Integer
        If PageSize <> 0 Then
            i = TotalCount Mod PageSize
            If TotalCount > PageSize Then
                pages = Math.Floor(TotalCount / PageSize)
                If i > 0 Then
                    pages = pages + 1
                End If
                Dim intSelectedIndex As Integer = ddPageNumber.SelectedIndex
                ddPageNumber.Items.Clear()
                Dim j As Integer = pages - 1
                For i = 0 To (pages - 1)
                    Dim item As New ListItem
                    item.Value = j
                    item.Text = j + 1
                    j = j - 1
                    ddPageNumber.Items.Insert(0, item)
                Next
                Try
                    ddPageNumber.SelectedIndex = intSelectedIndex
                Catch ex As Exception
                    ddPageNumber.SelectedIndex = 0
                End Try

                If pages > 1 Then
                    tdPager.Visible = True
                Else
                    tdPager.Visible = False
                End If
            Else
                tdPager.Visible = False
            End If
        Else
            tdPager.Visible = False
        End If
        ViewState("PageCount") = pages
        PopulateRecordsLabel(ddPageNumber.SelectedIndex)
    End Sub

    Public Sub SetPager(ByVal PagerIndex As Integer)
        If ddPageNumber.Items.Count > 0 Then
            ddPageNumber.SelectedIndex = PagerIndex
        End If
        PopulateRecordsLabel(ddPageNumber.SelectedIndex)
    End Sub

    ''' <summary>
    ''' Show the number of records message.
    ''' </summary>
    ''' <param name="PageIndex"></param>
    ''' <remarks></remarks>
    Public Sub PopulateRecordsLabel(ByVal PageIndex As Integer)
        tblPager.Visible = True

        Dim str As String
        str = "Showing "

        If (TotalCount = 0) Then
            'No records
            str = " " ' Show no message
            tblPager.Visible = False ' Hide the table
        ElseIf (TotalCount <= PageSize) Then
            'Total Count less than page size
            'Total Count same as Page size
            If (hideRecordMessage) Then
                'if hide record message is set to true
                str = " " ' Show no message
                tblPager.Visible = False ' hide table
            Else
                If TotalCount > 1 Then
                    str &= CStr(TotalCount) & " Record(s)" ' show message
                Else
                    str &= CStr(TotalCount) & " Record" ' show message
                End If
            End If
        Else
            ' Records greater than page size.
            If (PageIndex * PageSize) + PageSize > TotalCount Then
                str &= CStr((PageIndex * PageSize) + 1) & " to " & CStr(TotalCount) & " of " & CStr(TotalCount)
            Else
                str &= CStr((PageIndex * PageSize) + 1) & " to " & CStr((PageIndex * PageSize) + PageSize) & " of " & CStr(TotalCount)
            End If
        End If

        If HasSinglePager = True Then
            If Not (IsNothing(PageObj)) Then
                'IF master page is used
                If useMaster Then
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("tdTotalCount"), HtmlContainerControl).InnerText = str
                Else
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("tdTotalCount"), HtmlContainerControl).InnerText = str
                End If
            Else
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("tdTotalCount"), HtmlContainerControl).InnerText = str
            End If
        Else
            If Not (IsNothing(PageObj)) Then
                ' If Grid Pager is inside a Page
                'IF master page is used
                If useMaster Then
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("tdTotalCount"), HtmlContainerControl).InnerText = str
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("tdTotalCount"), HtmlContainerControl).InnerText = str
                Else
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("tdTotalCount"), HtmlContainerControl).InnerText = str
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("tdTotalCount"), HtmlContainerControl).InnerText = str
                End If

            ElseIf Not (IsNothing(UCObj)) Then
                'If Grid Pager is Inside an UC
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("tdTotalCount"), HtmlContainerControl).InnerText = str
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("tdTotalCount"), HtmlContainerControl).InnerText = str
            End If
        End If

        'Show/Hide Paging Controls
        'Top Pager
        If Not (IsNothing(PageObj)) Then
            If HasSinglePager = True Then
                ' If Grid Pager is inside a Page
                'IF master page is used
                If useMaster Then
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                Else
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                End If
            Else
                'IF master page is used
                If useMaster Then
                    ' If Grid Pager is inside a Page
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))

                    'Bottom Pager
                    ' If Grid Pager is inside a Page
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnFirst"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnPrevious"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnNext"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnLast"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                Else
                    ' If Grid Pager is inside a Page
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))

                    'Bottom Pager
                    ' If Grid Pager is inside a Page
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnFirst"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnPrevious"), ImageButton).Visible = (PageIndex <> 0)
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnNext"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnLast"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                End If
            End If
        ElseIf Not (IsNothing(UCObj)) Then
            If HasSinglePager = True Then
                ' If Grid Pager is inside a Page
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), ImageButton).Visible = (PageIndex <> 0)
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), ImageButton).Visible = (PageIndex <> 0)
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
            Else
                'If Grid Pager is Inside an UC
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), ImageButton).Visible = (PageIndex <> 0)
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnPrevious"), ImageButton).Visible = (PageIndex <> 0)
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnNext"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))

                'Bottom Pager
                'If Grid Pager is Inside an UC
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnFirst"), ImageButton).Visible = (PageIndex <> 0)
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnPrevious"), ImageButton).Visible = (PageIndex <> 0)
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnNext"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnLast"), ImageButton).Visible = (PageIndex < (ViewState("PageCount") - 1))
            End If
        End If

        If _hideFirstLastDropdown = True Then
            ddPageNumber.Visible = False
            If Not (IsNothing(PageObj)) Then
                'IF master page is used
                If useMaster Then
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), ImageButton).Visible = False
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnLast"), ImageButton).Visible = False

                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnFirst"), ImageButton).Visible = False
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), ImageButton).Visible = False
                Else
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnLast"), ImageButton).Visible = False
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnLast"), ImageButton).Visible = False

                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("btnFirst"), ImageButton).Visible = False
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("btnFirst"), ImageButton).Visible = False
                End If
            End If
        End If

    End Sub

#End Region

#Region "Events"

    ''' <summary>
    ''' Pager selected index change
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ChangePage(ByVal sender As Object, ByVal e As EventArgs)
        Dim idx As Integer = CType(sender, DropDownList).SelectedIndex
        SetDropdDownValue(idx)
        SetCurrentIndex()
    End Sub

    ''' <summary>
    ''' Show first set of records
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub GetFirstPage(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim idx As Integer = 0
        SetDropdDownValue(idx)
        SetCurrentIndex()
    End Sub

    ''' <summary>
    ''' Previuos Set of Records
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub GetPreviousPage(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrevious.Click
        If ddPageNumber.SelectedIndex <> 0 Then
            Dim idx As Integer = ddPageNumber.SelectedIndex - 1
            SetDropdDownValue(idx)
            SetCurrentIndex()
        End If
    End Sub

    ''' <summary>
    ''' Next set of Records
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub GetNextPage(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If ddPageNumber.SelectedIndex < ddPageNumber.Items.Count - 1 Then
            Dim idx As Integer = ddPageNumber.SelectedIndex + 1
            SetDropdDownValue(idx)
            SetCurrentIndex()
        End If
    End Sub

    ''' <summary>
    ''' Last set of Records
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub GetLastPage(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim idx As Integer = ddPageNumber.Items.Count - 1
        SetDropdDownValue(idx)
        SetCurrentIndex()
    End Sub

    Private Sub SetDropdDownValue(ByVal idx As Integer)
        If HasSinglePager = True Then
            If Not (IsNothing(PageObj)) Then
                'IF master page is used
                If useMaster Then
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("ddPageNumber"), DropDownList).SelectedIndex = idx
                Else
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("ddPageNumber"), DropDownList).SelectedIndex = idx
                End If
            Else
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("ddPageNumber"), DropDownList).SelectedIndex = idx
            End If
        Else
            If Not (IsNothing(PageObj)) Then
                ' If Grid Pager is inside a Page
                'IF master page is used
                If useMaster Then
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("ddPageNumber"), DropDownList).SelectedIndex = idx
                    CType(PageObj.Master.FindControl(ContentPlaceHolderId).FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("ddPageNumber"), DropDownList).SelectedIndex = idx
                Else
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("ddPageNumber"), DropDownList).SelectedIndex = idx
                    CType(PageObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("ddPageNumber"), DropDownList).SelectedIndex = idx
                End If
            ElseIf Not (IsNothing(UCObj)) Then
                'If Grid Pager is Inside an UC
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerTop").FindControl("ddPageNumber"), DropDownList).SelectedIndex = idx
                CType(UCObj.FindControl(ViewState("PagerGroup") & "GridPagerBtm").FindControl("ddPageNumber"), DropDownList).SelectedIndex = idx
            End If
        End If
    End Sub

#End Region
    ''' <summary>
    ''' Delegate event.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetCurrentIndex()
        'Populate the Records Label - Showing x to x of x records. Also Return Start Index.
        _startIndex = (ddPageNumber.SelectedIndex * PageSize)

        'call method to re-populate parent page data, 
        '	given current index:
        'Public Sub Populate(ByVal CompID As Integer, ByVal StartIndex As Integer, ByVal InitialCall As Boolean, ByVal PagerIndex As Integer, Optional ByVal killcache As Boolean = False)
        If Request("mode") Is Nothing Then
            Dim aObj(2) As Object
            aObj(0) = _startIndex
            aObj(1) = False
            aObj(2) = ddPageNumber.SelectedIndex
            _delUpdateIndex.DynamicInvoke(aObj)
        Else
            Dim aObj(6) As Object
            aObj(0) = CInt(CompID)
            aObj(1) = _startIndex
            aObj(2) = False
            aObj(3) = ddPageNumber.SelectedIndex
            aObj(4) = False
            aObj(5) = Session("IssueBy")
            aObj(6) = Session("IssueWorkorderID")
            _delUpdateIndex.DynamicInvoke(aObj)
        End If
    

        PopulateRecordsLabel(ddPageNumber.SelectedIndex)
    End Sub
End Class