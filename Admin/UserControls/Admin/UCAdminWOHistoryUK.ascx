<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCAdminWOHistory.ascx.vb" Inherits="Admin.UCAdminWOHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>

<script language="javascript" src="JS/Scripts.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>

<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<asp:UpdatePanel runat="server" ID="updatepanel1">
<ContentTemplate>
<script type="text/javascript">
    function validate_RateWO(field, alerttxt) {

        var text = document.getElementById(field).value;
        if (text == null || text == "" || text == "Enter a new comment here") {
            alert(alerttxt);
        }
        else {
            if (text.length > 500) {
                alert('Please enter comments less than 500 characters');
            }
            else {
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOHistory1_hdnBtnRateWO").click();
            }
        }
    }

   function ShowPopup(Flag, LinkId, WorkOrderId) {
        var strFlag = Flag;
        var DivId = "PopupDiv" + WorkOrderId;
        if (Flag == "True") {

            var div1Pos = $("#" + LinkId).offset();
            var div1X = div1Pos.left + 80;
            var div1Y = div1Pos.top;
            $('#' + DivId).css({ left: div1X, top: div1Y });
            $("#" + DivId).show();            
        }
        else {
            $("#" + DivId).hide();
        }
    }

</script>
 <asp:Panel runat="server" ID="pnlUpdateWO"  Width="300px" CssClass="pnlConfirmWOListing" style="display:none;">
  <div id="div2">
  <b>Rate workorder</b> <br />  
                 <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnPositive" Checked="false" runat="server" Text="Positive" />
                 <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNeutral"  Checked="false" runat="server"  Text="Neutral"/>
                 <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNegative" Checked="false" runat="server" Text="Negative" /><br /><br />
                 <b>Add comment here</b><br />
                    <asp:TextBox runat="server" ID="txtComment" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox>        
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtComment" WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        
                 <div id="divButton" class="divButton" style="width: 85px; float:left;">
                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                        <a class="buttonText cursorHand" href='javascript:validate_RateWO("ctl00_ContentPlaceHolder1_UCAdminWOHistory1_txtComment","Enter a new comment here")' id="a1"><strong>Submit</strong></a>
                        <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    </div>
                    <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                        <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                        <a class="buttonText cursorHand" runat="server" id="ancCancel1"><strong>Cancel</strong></a>
                        <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    </div> 
                    </div>
  </asp:Panel>
 <asp:Button runat="server" ID="hdnBtnRateWO"  Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
 <asp:Button runat="server" ID="btnTempRateWO" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

 <div id="divValidationMain" visible="false" class="divValidation" runat="server">
            <div class="roundtopVal">
                <img src="Images/Curves/Validation-TLC.gif" title="" width="5" height="5" class="corner"
                    style="display: none" /></div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="15" align="center">
                        &nbsp;
                    </td>
                    <td height="15">
                        &nbsp;
                    </td>
                    <td height="15">
                        &nbsp;
                    </td>
                </tr>
                <tr valign="middle">
                    <td width="63" align="center" valign="top">
                        <img src="Images/Icons/Validation-Alert.gif" width="31" height="31">
                    </td>
                    <td class="validationText">
                        <div id="divValidationMsg">
                        </div>
                        <asp:Label ID="lblError" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                        <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg"
                            DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
                    </td>
                    <td width="20">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td height="15" align="center">
                        &nbsp;
                    </td>
                    <td height="15">
                        &nbsp;
                    </td>
                    <td height="15">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <div class="roundbottomVal">
                <img src="Images/Curves/Validation-BLC.gif" title="" width="5" height="5" class="corner"
                    style="display: none" /></div>
        </div>

<table id="DGWorkOrders" width="100%" border="0" cellpadding="5" cellspacing="0">
  <tr valign="top" >
    <td width="80px"  class="gridHdr gridBorderRB">WO No</td>
    <td width="70px"  class="gridHdr gridBorderRB"><asp:Label runat="server"  ID="lblHdrCreated"></asp:Label></td>
    <td width="70px"  class="gridHdr gridBorderRB" runat="server" id="tdDateModified"><asp:Label runat="server" ID="lblHdrModified"></asp:Label></td>
    <td width="65px"  class="gridHdr gridBorderRB">Location</td>
	<td width="75px"  class="gridHdr gridBorderRB" runat="server" id="tdBuyerCompany">Client Company</td>
	<td width="90px"  class="gridHdr gridBorderRB" runat="server" id="tdBuyerContact"><asp:Label runat="server" ID="lblBuyerContact"></asp:Label></td>
	<td width="75px"  class="gridHdr gridBorderRB" runat="server" id="tdSupplierCompany">Supplier Company</td>
	<td width="90px"  class="gridHdr gridBorderRB" runat="server" id="tdSupplierContact">Supplier Contact</td>
	<td  class="gridHdr gridBorderRB">WO Title</td>
	<td width="60px" class="gridHdr gridBorderRB">WO Start</td>
	<td width="50px" class="gridHdr gridBorderRB"><asp:Label runat="server" ID="lblHdrWholesalePrice"></asp:Label></td>
	<td width="50px"  class="gridHdr gridBorderRB"><asp:Label runat="server" ID="lblHdrPlatformPrice"></asp:Label></td>
	<td width="50px"  class="gridHdr gridBorderRB"><asp:Label runat="server" ID="lblHdrUpSellPrice"></asp:Label></td>
    <td width="60px" class="gridHdr gridBorderRB">Billing Location</td>
	<td width="57px" class="gridHdrHighlight gridBorderRB">Status</td>
	<td width="25px" class="gridHdr gridBorderB">&nbsp;</td>
  </tr>
 <tr>
    <td width="80px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWONo"></asp:Label></td>
	<td width="70px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblCreated"></asp:Label></td>
	<td width="70px" class="gridRow gridBorderRB" runat="server" id="tdDateModified1"><asp:Label runat="server" CssClass="formTxt" ID="lblModified"></asp:Label></td>
	<td width="65px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblLocation"></asp:Label></td>
	<td width="75px" class="gridRow gridBorderRB" runat="server" id="tdBuyerCompany1"><asp:Label runat="server" CssClass="formTxt" ID="lblBuyerCompany"></asp:Label></td>
	<td width="90px" class="gridRow gridBorderRB" runat="server" id="tdBuyerContact1"><asp:Label runat="server" CssClass="formTxt" ID="lblBuyerContact1"></asp:Label></td>
	<td width="75px" class="gridRow gridBorderRB" runat="server" id="tdSupplierCompany1"><asp:Label runat="server" CssClass="formTxt" ID="lblSupplierCompany"></asp:Label>
    <table width="100%" cellspacing="0" cellpadding="0" >
							<tr>
                                <td width="260">
									<span style='cursor:hand;' onMouseOut=javascript:ShowPopup('False','PAnchor<%= ViewState("WorkOrderID")%>',<%= ViewState("WorkOrderID")%>); onMouseOver=javascript:ShowPopup('True','PAnchor<%= ViewState("WorkOrderID")%>',<%= ViewState("WorkOrderID")%>);>
										<%=GetLinks(1, IIf(IsDBNull(ViewState("SupCompId")), "", ViewState("SupCompId")), IIf(IsDBNull(ViewState("SupplierCompany")), "", ViewState("SupplierCompany")))%>
									</span> 
								</td>

                                <%If Not ViewState("SupplierCompany") = "" Then%>
                                <td id="tdRating" visible="true" valign="top" runat="server" >
									<div style="text-align:right;" id="PAnchor<%= ViewState("WorkOrderID")%>">
                                        <a style="text-align:right;" id="ShowPopupAnchor" onclick="javascript:ShowPopup('True','PAnchor<%= ViewState("WorkOrderID")%>',<%= ViewState("WorkOrderID")%>);"><img id="imgTestQuickNote" src="Images/Icons/FindSuppliersforWorkOrder.gif" title="View Supplier Details" alt="View Supplier Details"  width="16" height="15" hspace="2" vspace="0" border="0" /></a></div>
                                        <div id="PopupDiv<%= ViewState("WorkOrderID")%>" style="display: none;position:absolute; margin-left:-50px;">
                                                <asp:Panel runat="server" ID="pnlServicePartner" Width="200px" style="background-color:#FFEBDE; font-family: Tahoma, Arial, Verdana; font-size: 11px; color: #000000; text-decoration: none; line-height: 18px;	border: 1px solid #CECBCE;" >
                                                        <div style="text-align:right;"><a style="color:Black;text-align:right;vertical-align:top;cursor:pointer;" onclick="javascript:ShowPopup('False','PAnchor<%= ViewState("WorkOrderID")%>',<%= ViewState("WorkOrderID")%>);"><b>Close [X]</b></a></div>
                                                        <asp:Label id="lblContactName" runat="server"></asp:Label><br />
                                                        <asp:Label id="lblPhoneNo" runat="server"  ></asp:Label><br />
                                                        <asp:Label id="lblMobileNo" runat="server"  ></asp:Label><br />
                                                        <asp:Label id="lblEmail" runat="server"  ></asp:Label>
                                                </asp:Panel>
                                        </div>	
                                    </div>
                                </td>
                                <%End If %>
							</tr>
						</table>
    </td>
	<td width="90px" class="gridRow gridBorderRB" runat="server" id="tdSupplierContact1"><asp:Label runat="server" CssClass="formTxt" ID="lblSupplierContact"></asp:Label></td>
	<td  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblTitle"></asp:Label></td>
	<td width="60px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWOStart"></asp:Label></td>
	<td width="50px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblWholesalePrice"></asp:Label></td>
	<td width="40px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblPlatformPrice"></asp:Label></td>
	<td width="40px"  class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblUpSellPrice"></asp:Label></td>
    <td width="60px" class="gridRow gridBorderRB"><asp:Label runat="server" CssClass="formTxt" ID="lblbillloc"></asp:Label></td>
	<td width="57px" class="gridRowHighlight gridBorderRB"><asp:Label runat="server" CssClass="gridRowHighlight" ID="lblStatus"></asp:Label></td>
	<td width="45px" class="gridRow gridBorderB">			
	  <a runat=server id="lnkDetail" visible=true><img src="Images/Icons/ViewDetails.gif" alt="View Details" width="16" height="16" hspace="2" vspace="0" border="0"></a>
       <cc1:ModalPopupExtender ID="mdlUpdateRating" runat="server" TargetControlID="btnTempRateWO" PopupControlID="pnlUpdateWO" OkControlID="" CancelControlID="ancCancel1" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False"></cc1:ModalPopupExtender>
      <a ID="lnkbtnUpdateRating" runat="server" visible="false"><img id="imgUpdateRating" runat="server" src="~/Images/Icons/Update_rating.gif" title="Rate WO" width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" /></a>
	</td>
  </tr>
</table>


 <input type="hidden" id="hdnValue" runat="server" name="hdnValue">
<asp:Panel ID="pnlSupplierResponse" runat="server">
			 <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
					    <td width="855px">&nbsp;</td>
                        <td align="left" valign="middle" class="SubMenuTxtBoldSelected"><asp:Label ID="lblDLHeaderWP" Visible="true" runat="server" Text="*WP = Wholesale Price"></asp:Label></td>
                        </tr>
						<tr>
						 <td width="855px">&nbsp;</td>
                        <td align="left" valign="middle" class="SubMenuTxtBoldSelected"><asp:Label ID="lblDLHeaderPP" Visible="true" runat="server" Text="*PP = Portal Price"></asp:Label></td>
                        </tr>
						<tr>
						 <td width="855px">&nbsp;</td>
                        <td align="left" valign="middle" class="SubMenuTxtBoldSelected"><asp:Label ID="lblDLHeaderUP" Visible="true" runat="server" Text="*UP = UpSell Price"></asp:Label></td>
                        </tr>
                        <tr>
                            <td width="855px" class="SubMenuTxtBoldSelected"><asp:Label runat="server" ID="lblUnDiscardMessage" Text="" ></asp:Label></td>
                            <td>&nbsp; </td>
                        </tr>
                    </table>
					 <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                      
                        <td align="left" valign="top">
				<asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLSupplierResponse" Runat="server" Width="100%" >
				<HeaderTemplate>
					<table width="100%" cellspacing="0" cellpadding="0">
						 <tr id="tblDGYourResponseHdr" align="left">
                              <td width="5" valign="top" style="padding:0px; border:0px; "><img src="../Images/Curves/Bdr-Red-TL.gif" width="5" height="5" hspace="0" vspace="0" border="0"></td>
                              <td width="45" style=" border-left-width:0px; " ><asp:LinkButton ID="IdSort" Runat="server" CommandName="Sort" CommandArgument="WOTrackingId" CssClass="lnkbtnHistoryHeader">ID</asp:LinkButton></td>
                              <td width="75"><asp:LinkButton ID="ADateSort" Runat="server" CommandArgument="DateModified" CommandName="Sort" CssClass="lnkbtnHistoryHeader">Action Date</asp:LinkButton></td>                              								  
                              <td runat="server" id="tdDLCompany" width="125" ><asp:LinkButton ID="CompanySort" Runat="server" CommandArgument="Company" CommandName="Sort" CssClass="lnkbtnHistoryHeader">Company</asp:LinkButton></td>
                              <td width="75"><asp:LinkButton ID="CompStatusSort" Runat="server" CommandArgument="CompStatus" CommandName="Sort" CssClass="lnkbtnHistoryHeader">Company Status</asp:LinkButton></td>                                       
                              <td width="40" runat="server" id="tdRating" ><asp:LinkButton ID="RatingSort" Runat="server" CommandArgument="Rating" CommandName="Sort" CssClass="lnkbtnHistoryHeader">Rating</asp:LinkButton></td>
                              <td width="40" >WP*</td>
                              <td width="40" ><asp:LinkButton ID="PPSort" Runat="server" CommandArgument="PlatformPrice" CssClass="lnkbtnHistoryHeader">PP*</asp:LinkButton></td>							  
                              <td width="50" runat="server" id="tdSupDistance"><asp:LinkButton ID="DistSort" Runat="server" CommandArgument="SupplierDistance" CommandName="Sort" CssClass="lnkbtnHistoryHeader">Distance</asp:LinkButton></td>
							  <td width="100"><asp:LinkButton ID="StatusSort" Runat="server" CommandArgument="Status" CommandName="Sort" CssClass="lnkbtnHistoryHeader"><span class="formTxtOrange">Status</span></asp:LinkButton></td>
							  <td width="135" runat="server" id="tdLastClosed" visible = "false"><asp:LinkButton ID="DateOfLastClosedWOSort" Runat="server" CommandArgument="LastJobDate" CommandName="Sort" CssClass="lnkbtnHistoryHeader">Date of Last Closed WO</asp:LinkButton></td>
                              <td width="135" runat="server" id="tdNoOfJobs" visible = "false"><asp:LinkButton ID="NumberOfJobsClosedSort" Runat="server" CommandArgument="JobsCompleted" CommandName="Sort" CssClass="lnkbtnHistoryHeader">Number of Jobs Closed</asp:LinkButton></td>
                              <td width="85" runat="server" id="tdDateApproved" visible = "false"><asp:LinkButton ID="DateApprovedSort" Runat="server" CommandArgument="DateApproved" CommandName="Sort" CssClass="lnkbtnHistoryHeader">Date Approved</asp:LinkButton></td>
                              <td>Items Changed</td>
                              
                            </tr>
					</table>
				</HeaderTemplate>
				<ItemTemplate>
					<table width="100%" cellspacing="0" cellpadding="0" class="tblDGRowInside1">
						<tr id="tblDLRow" align="left">
                              <td width="5" valign="top" style="padding:0px; border:0px; ">&nbsp;</td>
                              <td width="40" style=" border-left-width:0px; " ><%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%></td>
                              <td width="75"><%#Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateModified"), DateFormat.GeneralDate)%></td> 		
                             				      														  
                              <td width="125" id="tdDLCompany1" valign="middle" runat="server">
                              <table width="100%" cellspacing="0" cellpadding="0" border="0" >
							<tr>
								
                                <td width="260">
									<span style='cursor:hand;' onMouseOut=javascript:ShowPopup('False','PAnchor<%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%>',<%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%>); onMouseOver=javascript:ShowPopup('True','PAnchor<%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%>',<%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%>);>
										<%#GetLinksGrid(1, DataBinder.Eval(Container.DataItem, "CompanyId"), IIf(IsDBNull(ViewState("SupCompId")), "", ViewState("SupCompId")), DataBinder.Eval(Container.DataItem, "Company"), DataBinder.Eval(Container.DataItem, "ContactClassId"))%>
									</span> 
								</td>                                  
                                         <td id="tdRating" visible="true" valign="middle" runat="server" >
									<div style="text-align:right;" id="PAnchor<%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%>">
                                        <a style="text-align:right;" id="ShowPopupAnchor" onclick="javascript:ShowPopup('True','PAnchor<%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%>',<%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%>);"><img id="imgTestQuickNote" src="Images/Icons/FindSuppliersforWorkOrder.gif" title="View Supplier Details" alt="View Supplier Details"  width="16" height="15" hspace="2" vspace="0" border="0" /></a></div>
                                        <div id="PopupDiv<%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%>" style="display: none;position:absolute; margin-left:-50px;">
                                                <asp:Panel runat="server" ID="pnlServicePartner" Width="200px" style="background-color:#FFEBDE; font-family: Tahoma, Arial, Verdana; font-size: 11px; color: #000000; text-decoration: none; line-height: 18px;	border: 1px solid #CECBCE;" >
                                                        <div style="text-align:right;"><a style="color:Black;text-align:right;vertical-align:top;cursor:pointer;" onclick="javascript:ShowPopup('False','PAnchor<%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%>',<%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%>);"><b>Close [X]</b></a></div>
                                                        <asp:Label id="lblContactName" runat="server" Text='<%# Cstr("<b>Contact Name: </b>" + iif(ISDBNULL(Container.DataItem("Name")),"--",Container.DataItem("Name"))) %>' ></asp:Label><br />
                                                        <asp:Label id="lblPhoneNo" runat="server" Text='<%#Cstr("<b>Phone No: </b>" + iif(ISDBNULL(Container.DataItem("Phone")),"--",Container.DataItem("Phone"))) %>' ></asp:Label><br />
                                                        <asp:Label id="lblMobileNo" runat="server" Text='<%#Cstr("<b>Mobile No: </b>" + iif(ISDBNULL(Container.DataItem("Mobile")),"--",Container.DataItem("Mobile"))) %>' ></asp:Label><br />
                                                        <asp:Label id="lblEmail" runat="server" Text='<%#Cstr("<b>Email Id: </b>" + iif(ISDBNULL(Container.DataItem("Email")),"--",Container.DataItem("Email"))) %>' ></asp:Label>
                                                </asp:Panel>
                                        </div>	
                                    </div>
                                </td>
							</tr>
						    </table>
                              </td>
                               <td width="75"><%# DataBinder.Eval(Container.DataItem, "CompStatus")%></td> 
                             <td width="40" runat="server" id="td1" ><%#DataBinder.Eval(Container.DataItem, "RatingScore").ToString().Replace(".00", "") & "%"%></td>
                              <td width="40" style="padding-left:2px; padding-right:5px; text-align:right;"><%# DataBinder.Eval(Container.DataItem, "WholesalePrice")%></td>
							  <td width="40" style="padding-left:2px; padding-right:5px; text-align:right;"><%# DataBinder.Eval(Container.DataItem, "PlatformPrice")%></td>
                              <td width="50" runat="server" id="tdSupplyParts1"><%#IIf(DataBinder.Eval(Container.DataItem, "SupplierDistance") > 0, Container.DataItem("SupplierDistance"), "NA")%></td>
							  <td width="100"><span class="bodytxtValidationMsg" style="margin-right:10px;" ><%# DataBinder.Eval(Container.DataItem, "Status")%></span><asp:LinkButton ID="lnkbtnUndiscard" Runat="server" visible='<%# iif(DataBinder.Eval(Container.DataItem, "Undiscard")= "Yes", "True", "False")%>' CommandName="UnDiscard" CommandArgument='<%#Container.DataItem("WOTrackingId") & "," & Container.DataItem("CompanyId")%>'  CssClass="lnkbtnHistoryHeader"><img src="Images/Icons/undiscard.gif" alt="UnDiscard" width="16" height="16" hspace="2" vspace="0" border="0"></asp:LinkButton>							  
                              <td width="135" runat="server" id="tdLastClosedValue" visible = "false"><%# IIf(DataBinder.Eval(Container.DataItem, "LastJobDate").ToString() = "01/01/1900 00:00:00", "NA", Container.DataItem("LastJobDate").ToString().Replace("00:00:00", ""))%></td> 
                              <td width="135" runat="server" id="tdNoOfJobsValue" visible = "false"><%# DataBinder.Eval(Container.DataItem, "JobsCompleted")%></td> 
                              <td width="85" runat="server" id="tdDateApprovedValue" visible = "false"><%# IIf(DataBinder.Eval(Container.DataItem, "DateApproved").ToString() = "01/01/1900 00:00:00", "NA", Container.DataItem("DateApproved").ToString().Replace("00:00:00", ""))%></td> 
                              <span class="bodytxtValidationMsg" style="margin-right:10px;" ><asp:LinkButton ID="lnkbtnDiscard" Runat="server" visible='<%# IIF(DataBinder.Eval(Container.DataItem, "Status")= "Conditional Accept", "True", "False")%>' CommandName="Discard" CommandArgument='<%#Container.DataItem("WOTrackingId") & "," & Container.DataItem("CompanyId")%>'  CssClass="lnkbtnHistoryHeader"><img src="Images/Icons/delete.gif" alt="Discard" title="Discard" width="14" height="14" hspace="2" vspace="0" border="0"></asp:LinkButton> <asp:LinkButton ID="lnkbtnCreateNewWO" Runat="server" visible='<%# IIF(DataBinder.Eval(Container.DataItem, "Status")= "Conditional Accept", "True", "False")%>' CommandName="CreateNewWO" CommandArgument='<%#Container.DataItem("ContactId") & "," & Container.DataItem("CompanyId")%>'  CssClass="lnkbtnHistoryHeader"><img src="Images/Icons/Create-New-WO.gif" alt="Create New WO" title="Create New WO" width="14" height="14" hspace="2" vspace="0" border="0"></asp:LinkButton></td>
							  <td>
							  <asp:Panel runat="server" ID="pnlChangedData" visible='<%#IIf((Eval("Status") <> "New" AND Eval("Status") <> "RatingUpdated" AND Eval("Status") <> "UnDiscarded" AND Eval("Status") <> "Submitted" AND Eval("Status") <> "Sent" AND Eval("Status") <> "Lost"  AND Eval("Status") <> "Closed" AND Eval("Status") <> "Completed"  AND Eval("Status") <> "Issue Rejected"),True, False)%>' >
							  <asp:Repeater  ID="rptChangedData" Runat="server" DataSource='<%#GetChangedData(Eval("WOTrackingId"),Eval("Status"))%>'  >
							    <ItemTemplate><br />
							    <p>
							    <%--<div id="divUpdate" runat="server" visible='<%#IIf(Eval("DMLType")="U" AND (Eval("Status")<> 53) AND (Eval("Status")<> 125) AND (Eval("Status")<> 51) AND (Eval("Status")<> 126) AND (Eval("Status")<> 4) AND (Eval("Status")<>7) AND (Eval("Status")<>6) AND (Eval("Status")<>11), True, False) %>'>							    
							    <span style="line-height:16px;" runat="server" id="span5"  visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7,False,IIF(DataBinder.Eval(Container.DataItem, "IsDataChanged") <> 0 ,True, False))%>' ><b><%# Eval("FullName")%> - <%#Eval("DateModified")%></b></span><br />
                                <span id="Span1" style="line-height:14px;" runat="server"  visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldValue") <> DataBinder.Eval(Container.DataItem, "NewValue"),True, False), IIF(Eval("OldValue")="" AND Eval("NewValue")="",False, True))%>' ><asp:Label runat="server" id="lblprice" Text=''></asp:Label>  Portal Price Changed from <%#DataBinder.Eval(Container.DataItem, "OldValue")%> to <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br /></span>
                                <span id="Span2" style="line-height:14px;" runat="server"   visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldAptTime") & "" <> DataBinder.Eval(Container.DataItem, "NewAptTime"), True, False), IIF(Eval("OldAptTime")="" AND Eval("NewAptTime")="",False, True))%>' >Appointment Time Changed from <%#DataBinder.Eval(Container.DataItem, "OldAptTime")%> to <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br /></span> 
                                <span id="Span3" style="line-height:14px;" runat="server"  visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldDateStart") <> DataBinder.Eval(Container.DataItem, "NewDateStart"), True, False), IIF(Eval("OldDateStart")="" AND Eval("NewDateStart")="",False, True))%>' >Start Date Changed from <%#DataBinder.Eval(Container.DataItem, "OldDateStart")%> to <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br /></span>
                                <span id="Span4" style="line-height:14px;" runat="server"  visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldDateEnd") <> DataBinder.Eval(Container.DataItem, "NewDateEnd"), True, False), IIF(Eval("OldDateEnd")="" AND Eval("NewDateEnd")="",False, True))%>' >End Date Changed from <%#DataBinder.Eval(Container.DataItem, "OldDateEnd")%> to <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br /></span>
                                </div>--%>
                                <div id="divInsert" runat="server" visible='<%#IIf((Eval("Status")=7), True, False) %>'>
							    <span style="line-height:16px;" runat="server" id="span6"  ><b><%#Eval("FullName")%> - <%#Eval("DateModified")%></b></span><br />
                                <%--<span id="Span7" style="line-height:14px;" runat="server" ><asp:Label runat="server" id="Label1" Text=''></asp:Label>  Portal Price is <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br /></span>
                                <span id="Span8" style="line-height:14px;" runat="server" visible='<%#IIf(Eval("BusinessArea")=101, True, False) %>'>Appointment Time <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br /></span> 
                                <span id="Span9" style="line-height:14px;" runat="server"><%#IIf(Eval("NewAptTime") = "", "Start Date", "Appointment Date")%> <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br /></span>
                                <span id="Span10" style="line-height:14px;" runat="server" visible='<%#IIf(Eval("NewAptTime")="",True,False)%>'>End Date <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br /></span>--%>
                                <span id="Span10" style="line-height:14px;" runat="server"  visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldValue") <> DataBinder.Eval(Container.DataItem, "NewValue"),True, False), IIF(Eval("OldValue")="" AND Eval("NewValue")="",False, True))%>' ><asp:Label runat="server" id="Label1" Text=''></asp:Label>  Portal Price Changed from <%#DataBinder.Eval(Container.DataItem, "OldValue")%> to <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br /></span>
                                <span id="Span7" style="line-height:14px;" runat="server"   visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldAptTime") & "" <> DataBinder.Eval(Container.DataItem, "NewAptTime"), True, False), IIF(Eval("OldAptTime")="" AND Eval("NewAptTime")="",False, True))%>' >Appointment Time Changed from <%#DataBinder.Eval(Container.DataItem, "OldAptTime")%> to <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br /></span> 
                                <span id="Span8" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldDateStart") <> DataBinder.Eval(Container.DataItem, "NewDateStart"),IIF(Eval("OldDateStart")="",False, True), False)%>' ><%#IIf(Eval("NewAptTime") = "", "Start Date", "Appointment Date")%> Changed from <%#DataBinder.Eval(Container.DataItem, "OldDateStart")%> to <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br /></span>
                                <%--<span id="Span9" style="line-height:14px;" runat="server"  visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldDateEnd") <> DataBinder.Eval(Container.DataItem, "NewDateEnd"), True, False), IIF(Eval("OldDateEnd")="" AND Eval("NewDateEnd")="",False, IIf(Eval("NewAptTime")="",True,False)))%>' >End Date Changed from <%#DataBinder.Eval(Container.DataItem, "OldDateEnd")%> to <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br /></span>--%>
                                </div>
                                 <div id="divInsertWP" runat="server" visible='<%#IIf((DataBinder.Eval(Container.DataItem, "OldWholesalePrice") <> DataBinder.Eval(Container.DataItem, "NewWholesalePrice")) AND Eval("Status")=53, True, False) %>'>
							    <span style="line-height:16px;" runat="server" id="span11"  ><b><%#Eval("FullName")%> - <%#Eval("DateModified")%></b></span><br />
                               <%-- <span id="Span12" style="line-height:14px;" runat="server" ><asp:Label runat="server" id="Label2" Text=''></asp:Label>  Wholesale Price is changed to <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br /></span>
                                <span id="Span13" style="line-height:14px;" runat="server">Appointment Time <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br /></span> 
                                <span id="Span14" style="line-height:14px;" runat="server"><%#IIf(Eval("NewAptTime") = "", "Start Date", "Appointment Date")%> <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br /></span>
                                <span id="Span15" style="line-height:14px;" runat="server" visible='<%#IIf(Eval("NewAptTime")="",True,False)%>'>End Date <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br /></span>--%>
                                <span id="Span21" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldWholesalePrice") <> DataBinder.Eval(Container.DataItem, "NewWholesalePrice"),True, False)%>' >Wholesale Price Changed from <%#DataBinder.Eval(Container.DataItem, "OldWholesalePrice")%> to <%#DataBinder.Eval(Container.DataItem, "NewWholesalePrice")%><br /></span>
                                </div>
                                 <div id="divInsertPP" runat="server" visible='<%#IIf(Eval("DMLType")="I" AND Eval("Status")=52, True, False) %>'>
							    <span style="line-height:16px;" runat="server" id="span16"  ><b><%#Eval("FullName")%> - <%#Eval("DateModified")%></b></span><br />
                                <span id="Span17" style="line-height:14px;" runat="server" ><asp:Label runat="server" id="Label3" Text=''></asp:Label>  Platform Price is changed to <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br /></span>                              
                                </div>
                                <%-- <div id="divUpdateWO" runat="server" visible='<%#IIf(Eval("DMLType")="U" AND (Eval("Status")=125), True, False) %>'>
							    <span style="line-height:16px;" runat="server" id="span18"  ><b><%#Eval("FullName")%> - <%#Eval("DateModified")%></b></span><br />                                
                                <span id="Span20" style="line-height:14px;" runat="server" visible='<%#IIf(Eval("BusinessArea")=101, True, False) %>'>Appointment Time <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br /></span> 
                                <span id="Span21" style="line-height:14px;" runat="server"><%#IIf(Eval("NewAptTime") = "", "Start Date", "Appointment Date")%> <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br /></span>
                                <span id="Span22" style="line-height:14px;" runat="server" visible='<%#IIf(Eval("NewAptTime")="",True,False)%>'>End Date <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br /></span>
                                <span id="Span23" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldPlatformPrice") <> DataBinder.Eval(Container.DataItem, "NewPlatformPrice"),True, False)%>' >Portal Price Changed from <%#DataBinder.Eval(Container.DataItem, "OldPlatformPrice")%> to <%#DataBinder.Eval(Container.DataItem, "NewPlatformPrice")%><br /></span>
                                <span id="Span24" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldWholesalePrice") <> DataBinder.Eval(Container.DataItem, "NewWholesalePrice"),True, False)%>' >Wholesale Price Changed from <%#DataBinder.Eval(Container.DataItem, "OldWholesalePrice")%> to <%#DataBinder.Eval(Container.DataItem, "NewWholesalePrice")%><br /></span>
                                <span id="Span25" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldPONumber") <> DataBinder.Eval(Container.DataItem, "NewPONumber"),True, False)%>' >PO Number Changed from <%#DataBinder.Eval(Container.DataItem, "OldPONumber")%> to <%#DataBinder.Eval(Container.DataItem, "NewPONumber")%><br /></span>
                                <span id="Span26" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldJobNumber") <> DataBinder.Eval(Container.DataItem, "NewJobNumber"),True, False)%>' >Job Number Changed from <%#DataBinder.Eval(Container.DataItem, "OldJobNumber")%> to <%#DataBinder.Eval(Container.DataItem, "NewJobNumber")%><br /></span>
                                <span id="Span27" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldGoodsLocation") <> DataBinder.Eval(Container.DataItem, "NewGoodsLocation"),True, False)%>' >Goods Location Changed from <%#DataBinder.Eval(Container.DataItem, "OldGoodsLocation")%> to <%#DataBinder.Eval(Container.DataItem, "NewGoodsLocation")%><br /></span>                                

                                </div>--%>

                                <%--<div id="divInsertAccept" runat="server" visible='<%#IIf(Eval("DMLType")="I" AND Eval("Status")=6, True, False) %>'>
                                 <span style="line-height:16px;" runat="server" id="span20"  ><b><%#Eval("FullName")%> - <%#Eval("DateModified")%></b></span><br />
                                 <span id="Span21" style="line-height:14px;" runat="server"   visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldAptTime") & "" <> DataBinder.Eval(Container.DataItem, "NewAptTime"), True, False), IIF(Eval("OldAptTime")="" AND Eval("NewAptTime")="",False, True))%>' >Appointment Time Changed from <%#DataBinder.Eval(Container.DataItem, "OldAptTime")%> to <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br /></span> 
                                <span id="Span22" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldDateStart") <> DataBinder.Eval(Container.DataItem, "NewDateStart"),IIF(Eval("OldDateStart")="",False, True), False)%>' ><%#IIf(Eval("NewAptTime") = "", "Start Date", "Appointment Date")%> Changed from <%#DataBinder.Eval(Container.DataItem, "OldDateStart")%> to <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br /></span>
                                <span id="Span30" style="line-height:14px;" runat="server"  visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldDateEnd") <> DataBinder.Eval(Container.DataItem, "NewDateEnd"), True, False), IIF(Eval("OldDateEnd")="" AND Eval("NewDateEnd")="",False, IIf(Eval("NewAptTime")="",True,False)))%>' >End Date Changed from <%#DataBinder.Eval(Container.DataItem, "OldDateEnd")%> to <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br /></span>
                                </div>--%>
                                <div id="divUpdateWO" runat="server" visible='<%#IIf(Eval("DMLType")="U" AND (Eval("Status")=125), True, False) %>'>
							    <span style="line-height:16px;" runat="server" id="span20"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "IsDataChanged") <> 0 ,True, False)%>' ><b><%# Eval("FullName")%> - <%#Eval("DateModified")%></b></span><br />
                                <span id="Span30" style="line-height:14px;" runat="server"  visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldValue") <> DataBinder.Eval(Container.DataItem, "NewValue"),True, False), IIF(Eval("OldValue")="" AND Eval("NewValue")="",False, True))%>' >Price Changed from <%#DataBinder.Eval(Container.DataItem, "OldValue")%> to <%#DataBinder.Eval(Container.DataItem, "NewValue")%><br /></span>
                                <span id="Span31" style="line-height:14px;" runat="server"   visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldAptTime") & "" <> DataBinder.Eval(Container.DataItem, "NewAptTime"), True, False), IIF(Eval("OldAptTime")="" AND Eval("NewAptTime")="",False, True))%>' >Appointment Time Changed from <%#DataBinder.Eval(Container.DataItem, "OldAptTime")%> to <%#DataBinder.Eval(Container.DataItem, "NewAptTime")%><br /></span> 
                                <span id="Span18" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldDateStart") <> DataBinder.Eval(Container.DataItem, "NewDateStart"),IIF(Eval("OldDateStart")="",False, True), False)%>' ><%#IIf(Eval("BusinessArea") <> 101, "Start Date", "Appointment Date")%> Changed from <%#DataBinder.Eval(Container.DataItem, "OldDateStart")%> to <%#DataBinder.Eval(Container.DataItem, "NewDateStart")%><br /></span>
                                <%--    <span id="Span33" style="line-height:14px;" runat="server"  visible='<%#IIf(Eval("Status")<>5 AND Eval("Status")<>7, IIF(DataBinder.Eval(Container.DataItem, "OldDateEnd") <> DataBinder.Eval(Container.DataItem, "NewDateEnd"), True, False), IIF(Eval("OldDateEnd")="" AND Eval("NewDateEnd")="",False, IIf(Eval("NewAptTime")="",True,False)))%>' >End Date Changed from <%#DataBinder.Eval(Container.DataItem, "OldDateEnd")%> to <%#DataBinder.Eval(Container.DataItem, "NewDateEnd")%><br /></span>--%>
                                <span id="Span23" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldPlatformPrice") <> DataBinder.Eval(Container.DataItem, "NewPlatformPrice"),True, False)%>' >Portal Price Changed from <%#DataBinder.Eval(Container.DataItem, "OldPlatformPrice")%> to <%#DataBinder.Eval(Container.DataItem, "NewPlatformPrice")%><br /></span>
                                <span id="Span24" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldWholesalePrice") <> DataBinder.Eval(Container.DataItem, "NewWholesalePrice"),True, False)%>' >Wholesale Price Changed from <%#DataBinder.Eval(Container.DataItem, "OldWholesalePrice")%> to <%#DataBinder.Eval(Container.DataItem, "NewWholesalePrice")%><br /></span>
                                <span id="Span25" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldPONumber") <> DataBinder.Eval(Container.DataItem, "NewPONumber"),True, False)%>' >PO Number Changed from <%#DataBinder.Eval(Container.DataItem, "OldPONumber")%> to <%#DataBinder.Eval(Container.DataItem, "NewPONumber")%><br /></span>
                                <span id="Span26" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldJobNumber") <> DataBinder.Eval(Container.DataItem, "NewJobNumber"),True, False)%>' >Job Number Changed from <%#DataBinder.Eval(Container.DataItem, "OldJobNumber")%> to <%#DataBinder.Eval(Container.DataItem, "NewJobNumber")%><br /></span>
                                <span id="Span27" style="line-height:14px;" runat="server"  visible='<%#IIF(DataBinder.Eval(Container.DataItem, "OldGoodsLocation") <> DataBinder.Eval(Container.DataItem, "NewGoodsLocation"),True, False)%>' >Goods Location Changed from <%#DataBinder.Eval(Container.DataItem, "OldGoodsLocation")%> to <%#DataBinder.Eval(Container.DataItem, "NewGoodsLocation")%><br /></span>                                

                                </div>
                                  <div id="divAcceptIPhone" runat="server" visible='<%#IIf(Eval("DMLType")="I" AND Eval("Status")=51, True, False) %>'>
							         <span style="line-height:16px;" runat="server" id="span19"  ><%#Eval("Comments")%></span>                                
                                </div>
                                
                                   <div id="divRejectIphone" runat="server" visible='<%#IIf(Eval("DMLType")="I" AND Eval("Status")=4, True, False) %>'>
							         <span style="line-height:16px;" runat="server" id="span28"  ><%#Eval("Comments")%></span>                                
                                </div>  
                                </p>
                                </ItemTemplate> 
							  </asp:Repeater>
							  </asp:Panel>  
							  </td>
                              
                            </tr>
					</table>
					<div id="divCommentsDetail" runat="server" style="VISIBILITY:hidden;  WIDTH: 920px; POSITION: absolute; ">
					 <table id="tblCommentDetails" runat="server" width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="#EDEDED" >
                             <tr>
    						    <td width="10" height="10" style="padding:0px; margin:0px"></td>
    						    <td height="10" style="padding:0px; margin:0px"></td>
    						    <td width="10" height="10" style="padding:0px; margin:0px"></td>
  						    </tr>
  						    <tr>
    						    <td width="10">&nbsp;</td>
    						    <td>
							        
									    <table id="tblDiscussion" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#EDEDED">
									    
									        <tr>
										       <td width="10px">&nbsp;</td>
										       
											   <td width="10px">&nbsp;</td>
									        </tr>
									        <tr>
										       <td width="10px">&nbsp;</td>
										       <td>
											
											    <asp:Repeater  ID="DLDiscussion" Runat="server" DataSource='<%#GetDiscussions(Container.DataItem("WOTrackingId"))%>'  >
						                          <ItemTemplate> 
							                          <table width="100%" runat=server visible='<%# iif( Trim(Container.DataItem("Comments")) = "" , "False", "True") %>'  border="0" cellspacing="0" cellpadding="0">
							                             <tr>
							                             
														   <%--<td width="150" valign="baseline"><%#IIf( Container.DataItem("ContactClassID") &"" <> admin.Applicationsettings.RoleOWID,(IIf( Container.DataItem("ContactClassID") &""  = admin.Applicationsettings.RoleClientID, "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - " &  "Buyer's Remarks" & "  </span>",  "<span class='discussionTextSupplier'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - " &  "Supplier's Remarks" & "  </span>")),  "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - Admin's Remarks  </span>")%></td>--%>
														   <td width="180" valign="baseline"><%#IIf(Container.DataItem("ContactClassID") & "" <> admin.Applicationsettings.RoleOWID, (IIf(Container.DataItem("ContactClassID") & "" = admin.Applicationsettings.RoleClientID, "<span class='discussionTextClient'>" & Container.DataItem("CommentsDate") & " - " & Container.DataItem("CommentsBy") & "  </span>", "<span class='discussionTextSupplier'>" & Container.DataItem("CommentsDate") & " - " & Container.DataItem("CommentsBy") & "  </span>")), "<span class='discussionTextClient'>" & Container.DataItem("CommentsDate") & " - " & Container.DataItem("CommentsBy") & " </span>")%></td>
												           <td width="20" align="center" valign="top">:</td>
												           <td valign="baseline" class="remarksAreaCollapse"><%# Container.DataItem("Comments") %>&nbsp;</td>
														 
							                             </tr>
									                </table>
						                          </ItemTemplate>
					                            </asp:Repeater>										
												
										    </td> <td width="10px">&nbsp;</td>
									      </tr>
									      
									    </table>
									    
								    
								    </td>
    						    <td width="10">&nbsp;</td>
  					       </tr>
  						    <tr>
  						      <td height="10" style="padding:0px; margin:0px"></td>
  						      <td height="10" style="padding:0px; margin:0px"></td>
  						      <td height="10" style="padding:0px; margin:0px"></td>
						      </tr>
					      
					</table>
					</div>
					 <table width="100%" id="tblCommentsCollapse"  border="0" cellspacing="0" cellpadding="0" bgcolor="#DFDFDF" runat="server" visible= <%#ShowHideComments(Container.DataItem("WOTrackingID"))%>>
					  <tr>
						<td class="smallText" height="8" style="padding:0px; margin:0px"></td>
						<td height="8" align="center" class="smallText" style="padding:0px; margin:0px" id="tdCollapse" runat="server">
						    <a href="" border="0" onClick='<%# "javascript:CollapseComments(" & Container.DataItem("WOTrackingID") & ", this.id)" %>' runat="server" id="collapseDetails">
						        <img src="Images/Arrows/Grey-Arrow.gif" width="50" title='Expand' height="8" border="0" name='<%# "imgExp" & Container.DataItem("WOTrackingID") %>'  id='<%# "imgExp" & Container.DataItem("WOTrackingID") %>' onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow-Roll.gif')" onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow.gif');" style="position:static;" />
                    	        <img src="Images/Arrows/Orange-Arrow-Col.gif" width="50" title='Collapse' height="8" border="0" name='<%# "imgCol" & Container.DataItem("WOTrackingID") %>'  id='<%# "imgCol" & Container.DataItem("WOTrackingID") %>' onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col-Roll.gif')" onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col.gif');" style="visibility:hidden; position:absolute;" />
						    </a>
						</td>
			             
                    	 
                    	        
                    	    
						<td class="smallText" height="8" style="padding:0px; margin:0px"></td>
					  </tr>
					 </table>
				</ItemTemplate>
			</asp:DataList>

				
			</td>
			
			 </tr>
			</table>
			  </asp:Panel>
			
			   <asp:Panel ID="pnlSupplierContactInfo" runat="server">
				<div class="divRatings">
                  <div class="divAccountTopCurves"><img src="../Images/Curves/MAS-Bdr-TL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                  <table width="100%"  border="0" cellpadding="0" cellspacing="0">
                    <tr class="smallText">
                      <td width="15" height="8" class="smallText">&nbsp;</td>
                      <td height="8" class="smallText">&nbsp;</td>
                      <td width="15" height="8" class="smallText">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="15" height="8">&nbsp;</td>
                      <td height="8" valign="top"><span class="bodytxtValidationMsg"><strong><asp:Label ID="lblSupplierContactLabel" runat="server"></asp:Label></strong></span></td>
                      <td height="8">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="15">&nbsp;</td>
                      <td><span class="bodyTextGreyLeftAligned"><strong>
                        </strong><strong>
                        <asp:Label ID="lblSupplierContactName" runat="server"></asp:Label>
                        - 
                        <asp:Label ID="lblSupplierCompanyName" runat="server"></asp:Label>
                        </strong>,
                        <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label>
&nbsp;&nbsp; Tel:
<asp:Label ID="lblSupplierPhone" runat="server"></asp:Label>
</span></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="15">&nbsp;</td>
                      <td class="bodyTextGreyLeftAligned" id="tdTopRating" runat="server"><strong>Performance Rating :
                            <asp:Label id="lblRatingSupplier" runat="server"></asp:Label>
                        </strong> &nbsp;<img src="Images/Icons/Icon-Positive.gif" width="18" height="18" align="absmiddle"> Positive :
                        <asp:Label id="lblPosRatingSupplier" runat="server"></asp:Label>
                        <img src="Images/Icons/Icon-Neutral.gif" width="13" height="5" align="absmiddle"> Neutral :
                        <asp:Label id="lblNeuRatingSupplier" runat="server"></asp:Label>
                        <img src="Images/Icons/Icon-Negative.gif" width="17" height="18" align="absmiddle"> Negative :
                        <asp:Label id="lblNegRatingSupplier" runat="server"></asp:Label>
                      </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="smallText">
                      <td width="15" height="8" class="smallText">&nbsp;</td>
                      <td height="8" class="smallText">&nbsp;</td>
                      <td height="8" class="smallText">&nbsp;</td>
                    </tr>
                  </table>
                  <div class="divAccountBotCurves"><img src="../Images/Curves/MAS-Bdr-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  </div>
				</asp:Panel>
			   <div class="divRatings" id="divWODetail">
                      <div class="divAccountTopCurves"><img src="../Images/Curves/MAS-Bdr-TL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                      <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="txtWelcome">
                        <tr class="smallText">
                          <td height="8" align="left" valign="top" class="smallText">&nbsp;</td>
                          <td height="8" align="left" valign="top"  class="smallText">&nbsp;</td>
                          <td height="8" align="left" valign="top" class="smallText" >&nbsp;</td>
                          <td height="8" class="smallText">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="15" align="left" valign="top" >&nbsp;</td>
                          <td id="tdDGDetailedDesc" align="left" valign="top">
                            <strong>Scope of Work:</strong><br><br>
                              <div runat="server" id="divScopeOfWork">
                            <%--<asp:Label ID="lblLongDesc" runat="server"></asp:Label>--%>
                              </div>
                            <br /><br />   
                            <asp:Panel ID="pnlClientScope" runat="server">
                                 <asp:Label ID="lblTagClientScope" runat="server"><strong>Client Scope:</strong></asp:Label><br><br>
                                <div runat="server" id="divClientScope">   
                                 <%--<asp:Label ID="lblClientScope" runat="server"></asp:Label>  --%> 
                                 </div>
                                </asp:Panel>                            
                            <br /><br />                            
                            <asp:Panel ID="pnlSpecialInstructions" runat="server"> 
                                  <strong>Special instructions to the Engineers:</strong><br><br>     
                                <div runat="server" id="divSpecialInstructions">                               
                                  <asp:Label ID="lblSpecialInstructions" runat="server"></asp:Label>
                                </div>
                                  <br /><br />
                                  <strong>Special instructions to Orderwork:</strong><br><br>
                                <div runat="server" id="divCustSpecialInstructions">                            
                                  <%--<asp:Label ID="lblCustSpecialInstructions" runat="server"></asp:Label>--%>
                                </div>
                                  <br />
                            </asp:Panel>
                          </td>
                          <td width="217" align="left" valign="top" class="paddingL23"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td align="left" valign="top" class="txtOrange">WO Type </td>
                                    <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
                                    <td class="txtOrange" align="left" valign="top"><asp:Label ID="lblCategory" runat="server"></asp:Label></td>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="top" class="bodyTextGreyLeftAligned">&nbsp;</td>
                                    <td align="center" valign="top" class="bodyTextGreyLeftAligned">&nbsp;</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned"><asp:Label ID="lblStartDateLabel" runat="server"></asp:Label></td>
                                    <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblStartDate" runat="server"></asp:Label><asp:Label ID="lblInstallTime" runat="server"></asp:Label></td>
                                  </tr>
                                  <tr runat="server" id="trEndDate">
                                    <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">End Date </td>
                                    <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblEndDate" runat="server"></asp:Label></td>
                                  </tr>
                                  <tr>
                                    <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">Quantity </td>
                                    <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblQuantity" runat="server"></asp:Label></td>
                                  </tr>
                                  <tr>
                                    <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">Estimated Time </td>
                                    <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblEstimatedTime" runat="server"></asp:Label></td>
                                  </tr>
                                  <tr>
                                    <td class="bodyTextGreyLeftAligned" width="85" align="left" valign="top">Supply Parts </td>
                                    <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">:</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblSupplyParts" runat="server"></asp:Label></td>
                                  </tr>
                                  <tr>
                                    <td width="85" align="left" valign="top" class="bodyTextGreyLeftAligned">Dress Code</td>
                                    <td width="10" align="center" valign="top" class="bodyTextGreyLeftAligned">:</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblDressCode" runat="server"></asp:Label></td>
                                  </tr>
                              </table></td>
                            </tr>
                            <tr>
                              <td><table width="100%" id="tblWOSpecs" runat="server" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td class="bodyTextGreyLeftAligned" width="85" align="left" valign="top">PO Number</td>
                                    <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">:</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblPONumber" runat="server"></asp:Label>
                                    </td>
                                  </tr>
                                   <tr runat="server" id="trJobNumber">
                                    <td class="bodyTextGreyLeftAligned" width="85" align="left" valign="top">Job Number </td>
                                    <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">:</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblJRSNumber" runat="server"></asp:Label></td>
                                  </tr>
                                   <tr runat="server" id="trSalesAgent">
                                    <td class="bodyTextGreyLeftAligned" width="85" align="left" valign="top">Sales Agent </td>
                                    <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">:</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblSalesAgent" runat="server"></asp:Label></td>
                                  </tr>
                                  <tr runat="server" id="trReceiptNumber">
                                    <td class="bodyTextGreyLeftAligned" width="85" align="left" valign="top">Receipt Number </td>
                                    <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">:</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblReceiptNumber" runat="server"></asp:Label></td>
                                  </tr>			
                                   <tr runat="server" id="trFreesatMake">
                                    <td class="bodyTextGreyLeftAligned" width="85" align="left" valign="top">Freesat Make/Model </td>
                                    <td class="bodyTextGreyLeftAligned" width="10" align="center" valign="top">:</td>
                                    <td class="bodyTextGreyLeftAligned" align="left" valign="top"><asp:Label ID="lblFreesatMake" runat="server"></asp:Label></td>
                                  </tr>							                                    
                              </table></td>
                            </tr>
                            <tr>
                              <td class="smallText" height="16" align="left" valign="top">&nbsp;</td>
                            </tr>
                            <tr>
                              <td align="left" valign="top" ><table width="100%" id="tblWOAttachments" runat="server" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td class="smallText" height="16" align="left" valign="top">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="top" class="bodyTextGreyLeftAligned"><asp:Panel ID="pnlAttachments" runat="server">
                                        <asp:Literal ID="litAttachments" runat="server" />                          
            </asp:Panel> 
          </td>
                                  </tr>
                              </table></td>
                            </tr>
                          </table></td>
                          <td width="15">&nbsp;</td>
                        </tr>
                        <tr class="smallText">
                          <td height="8" align="left" valign="top" class="smallText">&nbsp;</td>
                          <td height="8" align="left" valign="top"  class="smallText">&nbsp;</td>
                          <td height="8" align="left" valign="top" class="smallText" >&nbsp;</td>
                          <td height="8" class="smallText">&nbsp;</td>
                        </tr>
                      </table>
                      <div class="divAccountBotCurves"><img src="../Images/Curves/MAS-Bdr-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                    </div>          
 <asp:Panel ID="pnlGoodsCollection" runat="server">
                <div class="divRatings" id="div1">
                  <div class="divAccountTopCurves"><img src="../Images/Curves/MAS-Bdr-TL.gif" title="" width="5" height="5" class="corner" style="display: none" /></div>
                  <table width="100%"  border="0" cellpadding="0" cellspacing="0">
                    <tr class="smallText">
                      <td width="15" height="8" class="smallText">&nbsp;</td>
                      <td height="8" class="smallText">&nbsp;</td>
                      <td width="15" height="8" class="smallText">&nbsp;</td>
                    </tr>                           
                    <tr>
                      <td>&nbsp;</td>
                      <td><span class="txtOrange"><strong><asp:Label ID="lblGoodsCollection" runat="server" Text="Goods Collection"></asp:Label></strong></span>                                <br>
                       
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bodyTextGreyLeftAligned">
                            <tr>
                                <td width="180" valign="top" class="bodyTextGreyLeftAligned"><b><asp:label runat="server" ID="lblDepotNameLabel"></asp:label>:</b></td>
                                <td class="bodyTextGreyLeftAligned"><asp:Label ID="lblDepotName" runat="server"></asp:Label></td>
                            </tr>
                            <tr runat="server" valign="top" id="trDepotAddress">
                                <td width="180" class="bodyTextGreyLeftAligned"><b>Location Address:</b></td>
                                <td class="bodyTextGreyLeftAligned"><asp:Label ID="lblBusinessDivision" runat="server"></asp:Label></td>
                            </tr>                                    
                            <tr runat="Server" valign="top" id="trDepotUser">
                                <td width="180" class="bodyTextGreyLeftAligned"><b>Depot User:</b></td>
                                <td class="bodyTextGreyLeftAligned"><asp:Label ID="lblGoodsUser" runat="server"></asp:Label></td>
                            </tr>
                            <tr runat="Server" valign="top" id="trProducts">
                                <td width="180" class="bodyTextGreyLeftAligned"><b><asp:label ID="lblProductsLabel" runat="Server"></asp:label>:</b></td>
                                <td class="bodyTextGreyLeftAligned"><asp:Label ID="lblProducts" runat="server"></asp:Label></td>
                            </tr>
                                                         
                        </table>
                       
                      </td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr class="smallText">
                              <td height="8" class="smallText">&nbsp;</td>
                              <td height="8" class="smallText">&nbsp;</td>
                              <td height="8" class="smallText">&nbsp;</td>
                            </tr>
                          </table>
                          <div class="divAccountBotCurves"><img src="../Images/Curves/MAS-Bdr-BL.gif" title="" width="5" height="5" class="corner" style="display: none" /></div>
					    </div>
    
        </asp:Panel>                   
                    
<asp:Panel ID="pnlQAWOSubmit" runat="server" Visible="false">
<div  class="divRatings" style="padding:10px;" >
<span class="txtOrange"><strong>Client Work Order Submit Responses</strong></span> 
<asp:Repeater ID="rpQAWOSubmit" runat="server">
<HeaderTemplate>
<table>
<tr class="bodyTextGreyLeftAligned">
<td style="width:350px;" class="bodyTextGreyLeftAligned"><b>Question</b></td>
<td style="width:20px;">&nbsp;</td>
<td class="bodyTextGreyLeftAligned"><b>Answer</b></td>
</tr>
</table>
</HeaderTemplate>
<ItemTemplate>
<table class="bodyTextGreyLeftAligned">
<tr>
<td style="width:350px;" class="bodyTextGreyLeftAligned"><%#Eval("Question")%></td>
<td style="width:20px;" class="bodyTextGreyLeftAligned">:</td>
<td class="bodyTextGreyLeftAligned"><%#Eval("Answer")%></td>
</tr>
</table>
</ItemTemplate>
</asp:Repeater> 
</div>
</asp:Panel>                    
                    
 <asp:Panel ID="pnlClientContactInfo" runat="server">
						<div class="divRatings" id="divRatings">
                          <div class="divAccountTopCurves"><img src="../Images/Curves/MAS-Bdr-TL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
                          <table width="100%"  border="0" cellpadding="0" cellspacing="0">
                            <tr class="smallText">
                              <td width="15" height="8" class="smallText">&nbsp;</td>
                              <td height="8" class="smallText">&nbsp;</td>
                              <td width="15" height="8" class="smallText">&nbsp;</td>
                            </tr>
                            <tr>
                              <td height="20">&nbsp;</td>
                              <td height="20" valign="top"></td>
                              <td height="20">&nbsp;</td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td><span class="txtOrange"><strong><asp:Label ID="lblBuyerContactLabel" runat="server"></asp:Label></strong></span>
                                <span class="bodyTextGreyLeftAligned"><strong> 
                                <br><asp:Label ID="lblClientContact" runat="server"></asp:Label>
        -
        <asp:Label ID="lblClientCompany" runat="server"></asp:Label>
        , </strong>
                                    <asp:Label ID="lblClientAddress" runat="server"></asp:Label>
&nbsp;&nbsp; Tel:
        <asp:Label ID="lblClientPhone" runat="server"></asp:Label>
        <strong><br>        
         </strong>
        <div id="divTempContactIfo" style="visibility:hidden;position:absolute" runat="server"> 
							<span class="txtOrange"><strong>Work Order Location Info</strong>:</span><BR>	
							<strong><asp:Label ID="lblTempClientContact" runat="server"></asp:Label>
							: </strong><asp:Label ID="lblTempClientAddress" runat="server"></asp:Label> 
							&nbsp;&nbsp; Tel: <asp:Label ID="lblTempClientPhone" runat="server"></asp:Label>
							</div></span></td>
                              <td>&nbsp;</td>
                            </tr>
							
							<tr>
                                <td width="15">&nbsp;</td>
                                <td class="bodyTextGreyLeftAligned" id="tdBtmRating" runat="server"><strong>Rating Feedback :
                                      <asp:Label id="lblRating" runat="server"></asp:Label>
                                  </strong> &nbsp;<img src="Images/Icons/Icon-Positive.gif" width="18" height="18" align="absmiddle"> Positive :
                                  <asp:Label id="lblPosRating" runat="server"></asp:Label>
                                  <img src="Images/Icons/Icon-Neutral.gif" width="13" height="5" align="absmiddle"> Neutral :
                                  <asp:Label id="lblNeuRating" runat="server"></asp:Label>
                                  <img src="Images/Icons/Icon-Negative.gif" width="17" height="18" align="absmiddle"> Negative :
                                  <asp:Label id="lblNegRating" runat="server"></asp:Label>		
                                </td>
                                <td>&nbsp;</td>
                              </tr>
                            <tr class="smallText">
                              <td height="8" class="smallText">&nbsp;</td>
                              <td height="8" class="smallText">&nbsp;</td>
                              <td height="8" class="smallText">&nbsp;</td>
                            </tr>
                          </table>
                          <div class="divAccountBotCurves"><img src="../Images/Curves/MAS-Bdr-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
					    </div>
						</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>