'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class GridPager

    '''<summary>
    '''tblPager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblPager As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''tdTotalCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdTotalCount As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''tdPager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdPager As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''btnFirst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnFirst As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''btnPrevious control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPrevious As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''ddPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddPageNumber As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnNext As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''btnLast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnLast As Global.System.Web.UI.WebControls.ImageButton
End Class
