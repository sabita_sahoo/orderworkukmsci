﻿Public Class UCAdminAccreditations
    Inherits System.Web.UI.UserControl

    Shared ws As New WSObjs

    Public Property BizDivId() As Integer
        Get
            If Not IsNothing(ViewState("AccredBizDivId")) Then
                Return ViewState("AccredBizDivId")
            Else
                Return ApplicationSettings.BizDivId
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("AccredBizDivId") = value
        End Set
    End Property
    Public Property CommonID() As Integer
        Get
            If Not IsNothing(ViewState("CommonID")) Then
                Return ViewState("CommonID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("CommonID") = value
        End Set
    End Property
    Public Property Type() As String
        Get
            If Not IsNothing(ViewState("Type")) Then
                Return ViewState("Type")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("Type") = value
        End Set
    End Property
    Public Property Mode() As String
        Get
            If Not IsNothing(ViewState("Mode")) Then
                Return ViewState("Mode")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("Mode") = value
        End Set
    End Property
    Public Property SortExpression As String
        Get
            If Not ViewState("SortExpression") Is Nothing Then
                Return ViewState("SortExpression").ToString()
            Else
                Return "FullName"
            End If
        End Get
        Private Set(ByVal value As String)
            ViewState("SortExpression") = value
        End Set
    End Property

    Public Property SortDirection As SortDirection
        Get
            If Not ViewState("SortDirection") Is Nothing Then
                Return ViewState("SortDirection")
            Else
                Return SortDirection.Ascending
            End If
        End Get
        Private Set(ByVal value As SortDirection)
            ViewState("SortDirection") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            hdnType.Value = Type
            hdnCommonID.Value = CommonID
            populateAccred()
            PopulateSelectedGrid(WebControls.SortDirection.Ascending, "FullName")
        End If
    End Sub
    Public Sub populateAccred()
        Dim ds As New DataSet
        ds = GetSelectedAccred()
    End Sub
    Public Function GetSelectedAccred()
        Dim ds As New DataSet
        ds = ws.WSContact.GetAccreditations(CommonID, Mode)
        Return ds
    End Function

    Public Sub PopulateSelectedGrid(ByVal selectedSortDirection As SortDirection, ByVal selectedSortExpression As String)
        Dim ds As DataSet = GetSelectedAccred()
        Dim dtSelectedAccred As DataTable = ds.Tables(0)
        Dim dvSelectedAccred As New DataView
        If Not IsDBNull(dtSelectedAccred) Then
            If dtSelectedAccred.Rows.Count > 0 Then
                dvSelectedAccred = dtSelectedAccred.DefaultView
                dvSelectedAccred.Sort = selectedSortExpression + ConvertSortDirectionToSql(selectedSortDirection)
                If (Type = "AllAccreditations") Then
                    pnlSelectedCompanyAccreds.Visible = False
                    pnlSelectedEnggAccreds.Visible = False

                    dvSelectedAccred.RowFilter = "Type IN ('Company')"
                    If (dvSelectedAccred.Count > 0) Then
                        pnlSelectedCompany.Visible = True

                        rptSelectedCompany.DataSource = dvSelectedAccred
                        rptSelectedCompany.DataBind()
                    Else
                        pnlSelectedCompany.Visible = False
                    End If

                    dvSelectedAccred.RowFilter = "Type IN ('Engineer')"
                    If (dvSelectedAccred.Count > 0) Then
                        pnlSelectedEngineer.Visible = True

                        rptSelectedEngineer.DataSource = dvSelectedAccred
                        rptSelectedEngineer.DataBind()
                    Else
                        pnlSelectedEngineer.Visible = False
                    End If

                    dvSelectedAccred.RowFilter = ""

                    dvSelectedAccred.RowFilter = "Type IN ('Others')"
                    If (dvSelectedAccred.Count > 0) Then
                        pnlSelectedOthers.Visible = True

                        rptSelectedOthers.DataSource = dvSelectedAccred
                        rptSelectedOthers.DataBind()
                    Else
                        pnlSelectedOthers.Visible = False
                    End If

                    dvSelectedAccred.RowFilter = ""

                Else
                    pnlSelectedCompany.Visible = False
                    pnlSelectedEngineer.Visible = False
                    pnlSelectedOthers.Visible = False

                    dvSelectedAccred.RowFilter = "Type NOT IN ('Engineer')"
                    If (dvSelectedAccred.Count > 0) Then
                        pnlSelectedCompanyAccreds.Visible = True

                        gvSelectedCompanyAccreds.DataSource = dvSelectedAccred
                        gvSelectedCompanyAccreds.DataBind()
                    Else
                        pnlSelectedCompanyAccreds.Visible = False
                    End If
                    dvSelectedAccred.RowFilter = ""
                    dvSelectedAccred.RowFilter = "Type IN ('Engineer')"
                    If (dvSelectedAccred.Count > 0) Then
                        pnlSelectedEnggAccreds.Visible = True

                        gvSelectedEngineerAccreds.DataSource = dvSelectedAccred
                        gvSelectedEngineerAccreds.DataBind()
                    Else
                        pnlSelectedEnggAccreds.Visible = False
                    End If
                    dvSelectedAccred.RowFilter = ""
                End If
            Else
                pnlSelectedCompanyAccreds.Visible = False
                pnlSelectedEnggAccreds.Visible = False
                pnlSelectedCompany.Visible = False
                pnlSelectedEngineer.Visible = False
                pnlSelectedOthers.Visible = False
            End If
        End If
    End Sub

    Public Sub hdnAccrSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnAccrSubmit.Click
        divMoreInfoAutoSuggest.Style.Add("display", "none")
        If txtExpiryDate.Text <> "" Then
            'prepare regular expression
            Dim str As String = "(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
            'match input value against regular expression 
            Dim myMatch As Match = System.Text.RegularExpressions.Regex.Match(txtExpiryDate.Text.Trim, str)
            If Not myMatch.Success Then
                'if format is not correct
                lblMoreInfo.Text = "Expiry date should be in dd/mm/yyyy"
                divMoreInfoAutoSuggest.Style.Add("display", "block")
                Exit Sub
            End If
        End If

        Dim ds As New DataSet
        Dim TagId As Integer
        Dim Type As String

        If (hdnTagID.Value = "") Then
            TagId = 0
            Type = "Others"
        Else
            TagId = hdnTagID.Value
            If (hdnSelectedType.Value = "Company") Then
                Type = "Company"
            ElseIf (hdnSelectedType.Value = "Engineer") Then
                Type = "Engineer"
            Else
                Type = "Others"
            End If
        End If

        ds = ws.WSContact.AddRemoveAccreditations(CommonID, TagId, Type, txtExpertise.Value.Trim, txtTagInfo.Text.Trim, txtOtherInfo.Text.Trim, txtExpiryDate.Text.Trim, "Add", "", Session("UserID"))
        If (ds.Tables(0).Rows.Count > 0) Then
            If (ds.Tables(0).Rows(0)("Success") = 1) Then
                PopulateSelectedGrid(SortDirection.Ascending, "FullName")
                ResetData()
            End If
        End If

    End Sub

    Public Sub ResetData()
        txtExpertise.Value = ""
        txtTagInfo.Text = ""
        txtExpiryDate.Text = ""
        txtOtherInfo.Text = ""
        hdnTagID.Value = ""
        hdnSelectedType.Value = ""
        lblMoreInfo.Text = ""
        lblMessage.Text = ""
    End Sub


    Public Sub lnkCompanyRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCompanyRemove.Click
        'Iteration through grid
        Dim id As String
        id = ""
        For Each row As GridViewRow In gvSelectedCompanyAccreds.Rows
            Dim chkBox As CheckBox = CType(row.FindControl("CheckAccred"), CheckBox)
            If chkBox.Checked Then
                If (id = "") Then
                    id = CType(row.FindControl("hdnTagId"), HtmlInputHidden).Value.ToString
                Else
                    id = id & "," & CType(row.FindControl("hdnTagId"), HtmlInputHidden).Value.ToString
                End If
            End If
        Next
        RemoveAccred(id)
    End Sub
    Public Sub lnkEngineerRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEngineerRemove.Click
        'Iteration through grid
        Dim id As String
        id = ""
        For Each row As GridViewRow In gvSelectedEngineerAccreds.Rows
            Dim chkBox As CheckBox = CType(row.FindControl("CheckAccred"), CheckBox)
            If chkBox.Checked Then
                If (id = "") Then
                    id = CType(row.FindControl("hdnTagId"), HtmlInputHidden).Value.ToString
                Else
                    id = id & "," & CType(row.FindControl("hdnTagId"), HtmlInputHidden).Value.ToString
                End If
            End If
        Next
        RemoveAccred(id)
    End Sub

    Private Sub rptSelectedCompany_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSelectedCompany.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "Remove") Then
                RemoveAccred(e.CommandArgument.ToString)
            End If
        End If
    End Sub
    Private Sub rptSelectedEngineer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSelectedEngineer.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "Remove") Then
                RemoveAccred(e.CommandArgument.ToString)
            End If
        End If
    End Sub
    Private Sub rptSelectedOther_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSelectedOthers.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "Remove") Then
                RemoveAccred(e.CommandArgument.ToString)
            End If
        End If
    End Sub

    Public Sub RemoveAccred(ByVal TagId As String)
        Dim ds As DataSet
        ds = ws.WSContact.AddRemoveAccreditations(CommonID, 0, "", "", "", "", "", "Remove", TagId.ToString, Session("UserID"))

        If (ds.Tables(0).Rows.Count > 0) Then
            If (ds.Tables(0).Rows(0)("Success") = 1) Then
                PopulateSelectedGrid(SortDirection.Ascending, "FullName")
                ResetData()
            End If
        End If
    End Sub

    Public Sub gvSelectedEngineerAccreds_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dv As DataRowView = CType(e.Row.DataItem, DataRowView)
            Dim rblApprovalStatus As RadioButtonList = e.Row.FindControl("rblApprovalStatus")
            rblApprovalStatus.DataSource = ws.WSStandards.GetStandards("ApprovalStatus")
            rblApprovalStatus.DataBind()
            rblApprovalStatus.SelectedValue = dv.Item("ApprovalStatus")
        End If
    End Sub

    Public Sub gvSelectedCompanyAccreds_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dv As DataRowView = CType(e.Row.DataItem, DataRowView)
            Dim rblApprovalStatus As RadioButtonList = e.Row.FindControl("rblApprovalStatus")
            rblApprovalStatus.DataSource = ws.WSStandards.GetStandards("ApprovalStatus")
            rblApprovalStatus.DataBind()
            rblApprovalStatus.SelectedValue = dv.Item("ApprovalStatus")
        End If
    End Sub

    Public Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        For Each gridViewRow As GridViewRow In gvSelectedEngineerAccreds.Rows
            If gridViewRow.RowType = DataControlRowType.DataRow Then
                Dim rblApprovalStatus As RadioButtonList = gridViewRow.FindControl("rblApprovalStatus")
                Dim hidTagContactLinkageId As HiddenField = gridViewRow.FindControl("hidTagContactLinkageId")
                ws.WSWorkOrder.UpdateTagContactLinkage(Integer.Parse(hidTagContactLinkageId.Value), Integer.Parse(rblApprovalStatus.SelectedValue), DateTime.Now, Integer.Parse(Session("UserID").ToString()))
            End If
        Next
    End Sub

    Public Sub btnCompanyUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCompanyUpdate.Click
        For Each gridViewRow As GridViewRow In gvSelectedCompanyAccreds.Rows
            If gridViewRow.RowType = DataControlRowType.DataRow Then
                Dim rblApprovalStatus As RadioButtonList = gridViewRow.FindControl("rblApprovalStatus")
                Dim hidTagContactLinkageId As HiddenField = gridViewRow.FindControl("hidTagContactLinkageId")
                ws.WSWorkOrder.UpdateTagContactLinkage(Integer.Parse(hidTagContactLinkageId.Value), Integer.Parse(rblApprovalStatus.SelectedValue), DateTime.Now, Integer.Parse(Session("UserID").ToString()))
            End If
        Next
    End Sub

    Protected Sub grdView_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs)
        If SortExpression = e.SortExpression Then
            If SortDirection = WebControls.SortDirection.Ascending Then
                SortDirection = WebControls.SortDirection.Descending
            Else
                SortDirection = WebControls.SortDirection.Ascending
            End If
        Else
            SortDirection = WebControls.SortDirection.Ascending
        End If
        SortExpression = e.SortExpression
        PopulateSelectedGrid(SortDirection, SortExpression)
    End Sub

    Protected Function ConvertSortDirectionToSql(ByVal SortDirection As SortDirection) As String
        Dim newSortDirection As String = String.Empty
        Select Case SortDirection
            Case SortDirection.Ascending
                newSortDirection = " ASC"
                Exit Select
            Case SortDirection.Descending
                newSortDirection = " DESC"
                Exit Select
        End Select
        Return newSortDirection
    End Function

End Class