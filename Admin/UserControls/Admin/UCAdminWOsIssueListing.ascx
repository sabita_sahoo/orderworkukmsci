<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCAdminWOsIssueListing.ascx.vb" Inherits="Admin.UCAdminWOsIssueListing" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register TagPrefix="uc1" TagName="GridPager" Src="../../UserControls/Admin/GridPager.ascx"%>
<style>
    
    
.dlIssue{
width:100%;
}
.gridRow{
paddin-right:0px;
}
.clsBorderTbl{
 border-left-width:0px;
 border-right: solid 1px #ffffff;
 padding:4px 0px 0px 6px;
 margin:0px;
 font-family:Arial;font-size:11px;
 }
 .clsBorderTblDetail{
 border-left-width:0px;
 border-right: solid 1px #ffffff;
 padding:2px 0px 0px 2px;
 margin:0px;
 }
 .gridRow{
 padding:0px;
 }
 #divOuterGrid
 {
    float:right;width:280px;
 }
</style>


<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<asp:Panel runat="server" ID= "pnlIssueListing">
<div style="background-color:#dad8d9;width:100%;margin:2px;" >
<uc1:GridPager ID="GridPagerTop" runat="server" /></div>


<asp:datalist ID="dlIssueWOListing" runat="server" ForeColor="#555354"  CssClass="dlIssue" >
<HeaderTemplate>
	<table width="100%"  cellspacing="0" cellpadding="0" class="gridHdr gridText" style="color:#000; text-align:left;margin-left:0px;margin-top:0px;">
		 <tr id="tbldlIssueWOListingHdr" align="left">  
		      <td width="16" class="clsBorderTbl" style="padding-right:4px;"><input id='chkAll' onClick=CheckAll(this,'Check') type='checkbox' name='chkAll' /></td>                       
              <td width="90" class="clsBorderTbl"><asp:LinkButton id="lnkWOID" runat="server" CommandArgument="RefWOIDSortIcon"  CommandName="RefWOID" style=" color:#000;font-weight:bold;" >WO No</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/RedArrow-Up.gif" id="RefWOIDSortIcon" runat="server" Visible="false"  /></td>
              <td width="70" class="clsBorderTbl"><asp:LinkButton id="lnkIssueRaised" runat="server"  CommandName="DateCreated" style=" color:#000;font-weight:bold;" >Issue Raised</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/RedArrow-Up.gif" id="DateCreatedSortIcon" runat="server" Visible="false" /></td>
              <td width="100" class="clsBorderTbl"><asp:LinkButton id="lnkLastResp" runat="server"  CommandName="DateModified" style=" color:#000;font-weight:bold;" >Last Response</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/RedArrow-Up.gif" id="DateModifiedSortIcon" runat="server" Visible="false" /></td>
              <td width="100" class="clsBorderTbl"><asp:LinkButton id="lnkLocation" runat="server"  CommandName="Location" style=" color:#000;font-weight:bold;" >Location</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/RedArrow-Up.gif" runat="server" id="LocationSortIcon" Visible="false" /></td>
              <td width="80" class="clsBorderTbl"><asp:LinkButton id="lnkBuyer" runat="server"  CommandName="BuyerCompany" style=" color:#000;font-weight:bold;" >Client</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/RedArrow-Up.gif" runat="server" id="BuyerSortIcon" Visible="false" /></td>
              <td width="120" class="clsBorderTbl"><asp:LinkButton id="lnkSupplier" runat="server"  CommandName="SupplierContact" style=" color:#000;font-weight:bold;" >Supplier</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/RedArrow-Up.gif" runat="server"  id="SupplierContactSortIcon" Visible="false"  /></td>
              <td width="70" class="clsBorderTbl"><asp:LinkButton id="lnkWOTitle" runat="server"  CommandName="WOTitle" style=" color:#000;font-weight:bold;" >WOTitle</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/RedArrow-Up.gif" runat="server" id="WOTitleSortIcon" Visible="false"  /></td>
              <td width="80" class="clsBorderTbl"><asp:LinkButton id="lnkStartDate" runat="server"  CommandName="DateStart" style=" color:#000;font-weight:bold;" >Start Date</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/RedArrow-Up.gif" runat="server" id="DateStartSortIcon" Visible="false"  /></td>
			  <td width="40" class="clsBorderTbl"><asp:LinkButton id="lnkWP" runat="server"  CommandName="WholesalePrice" style=" color:#000;font-weight:bold;" >WP</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/RedArrow-Up.gif" runat="server"  id="WholesalePriceSortIcon" Visible="false"  /></td>
			  <td width="40" class="clsBorderTbl"><asp:LinkButton id="lnkPP" runat="server"  CommandName="PlatformPrice" style=" color:#000;font-weight:bold;" >PP</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/RedArrow-Up.gif" runat="server"  id="PlatformPriceSortIcon" Visible="false"  /></td>
              <td width="35" class="clsBorderTbl"><asp:LinkButton id="lnkUP" runat="server"  CommandName="UpSellPrice" style=" color:#000;font-weight:bold;" >UP</asp:LinkButton>&nbsp;<asp:Image ImageUrl="~/Images/RedArrow-Up.gif" runat="server" id="UpSellPriceSortIcon" Visible="false"  /></td>                              
              <td  valign="top" align="right" style="padding:0px; border:0px;border-right: solid 1px #ffffff;"></td>
            </tr>
	</table>
</HeaderTemplate>   
<ItemTemplate>
<div style="border-bottom: solid 0px #ffffff; " >
<div class="gridRowText"  style=" margin:0px; padding:0px;" >
    <table width="100%"  cellspacing="0" cellpadding="0" class="gridHdrText gridRowText" style="color:#000; text-align:left;margin-left:0px;margin-top:0px;">
    <tr class="gridRowText" align="left" style="font-family:Arial;font-size:11px;">
        <td width="16" class="clsBorderTbl" style="padding-right:4px;"><asp:CheckBox ID="Check" runat="server" /><input type="hidden" id="hdnWOIDs" runat="server" value='<%#Container.DataItem("WOID")%>' /></td>
        <td width="90" class="clsBorderTbl"><asp:Image ID="imgIsFlagged" runat="server" ImageUrl="~/Images/Icons/Flag.gif" hspace="2" vspace="0" Width="16" Height="17" style="float:left;" Visible='<%#IIf(Container.DataItem("IsFlagged") = "1" ,True,false)%>' /><asp:Label ID="lblWOID" runat="server" Text='<%#Container.DataItem("RefWOID")%>'></asp:Label></td>    
        <td width="70" class="clsBorderTbl"><asp:Label ID="lblIssueDate" runat="server" Text='<%#Container.dataitem("DateCreated")%>' ></asp:Label></td>
        <td width="100" class="clsBorderTbl">
        <div style='visibility:hidden;position:absolute;margin-left:70px;background-color:#FFFFE1;width:250px;padding:10px; border:solid 1px #000;' id='divIssueBy<%# DataBinder.Eval(Container.DataItem, "CompanyID")%><%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>' >
        <b>Response By:</b>&nbsp;<%#Container.DataItem("LastResponseName")%>
        </div>
        <span style='cursor:hand;' onMouseOut=javascript:showHideRatingDetails('divIssueBy<%# DataBinder.Eval(Container.DataItem, "CompanyID")%><%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divIssueBy<%# DataBinder.Eval(Container.DataItem, "CompanyID")%><%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>','visible')>
        <asp:Label ID="lblLastAction" runat="server" Text='<%#Container.DataItem("DateModified")%>'></asp:Label>
        </span>
        </td>
        <td width="100" class="clsBorderTbl"><asp:Label ID="lblLocation" runat="server" Text='<%#Container.DataItem("Location")%>' ></asp:Label></td>
        <td width="80" class="clsBorderTbl"><asp:Label ID="lblBuyer" runat="server" Text='<%#Container.dataitem("BuyerCompany")& " - " &Container.dataitem("BillingLocation")%>' ></asp:Label></td>
        <td width="120" class="clsBorderTbl">
               
                <table width="100%" cellspacing="0" cellpadding="0" >
							<tr>
								<td width="160" id="tdRating" visible="true" runat="server" >
									<div style='visibility:hidden;position:absolute;margin-left:90px;' id=divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%><%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%> >
											<table width="300" height="100" bgcolor="#D6D7D6" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;" >
												<tr>
												  <td class='bgWhite' align='left'><strong>Contact Name:</strong> <%# iif(ISDBNULL(Container.DataItem("SupplierContact")),"--",Container.DataItem("SupplierContact"))%>  </td>
                                                  <td class='bgWhite' align='left' style="padding-right:5px;width:55px;" align='left'>
                                                    <a style="color:Red;text-align: right; vertical-align: top; cursor: pointer;"
                                                        onclick=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%><%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>','hidden')>
                                                        <b>Close [X]</b></a>
                                                </td>
												</tr>
												<tr>
												  <td class='bgWhite' align='left'><strong>Phone No:</strong> <%# iif(ISDBNULL(Container.DataItem("SupplierPhone")),"--",Container.DataItem("SupplierPhone"))%>  </td>
												</tr>
												<tr>
												  <td class='bgWhite' align='left'><strong>Mobile No:</strong> <%# iif(ISDBNULL(Container.DataItem("SupplierMobile")),"--",Container.DataItem("SupplierMobile"))%>  </td>
												</tr>
												<tr>
												  <td class='bgWhite' align='left'><strong>Email Id:</strong> <%# iif(ISDBNULL(Container.DataItem("SupplierEmail")),"--",Container.DataItem("SupplierEmail"))%>  </td>
												</tr>
											</table>
											<a ></a>
									</div>
									<span style='cursor:hand;' onMouseOut=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%><%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%><%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>','visible')>
										<%#GetLinks(iif(ISDBNULL(Container.DataItem("BizDivID")),"",Container.DataItem("BizDivID")), iif(ISDBNULL(Container.DataItem("SupplierCompanyID")),"",Container.DataItem("SupplierCompanyID")), iif(ISDBNULL(Container.DataItem("SupplierContactID")),"",Container.DataItem("SupplierContactID")),iif(ISDBNULL(Container.DataItem("SupplierCompany")),"",Container.DataItem("SupplierCompany")))%>
									</span> 
                                      <a style="text-align: right;cursor: pointer;" id="ShowPopupSupplier" onclick=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%><%# DataBinder.Eval(Container.DataItem, "WorkOrderId")%>','visible') >
                                    <img id="imgTestQuickNote" src="Images/Icons/FindSuppliersforWorkOrder.gif" title="View Details"
                                        alt="View Details" width="16" height="15" hspace="2" vspace="0"
                                        border="0" /></a>
								</td>
							</tr>
						</table>
            
           </td>
           <td width="70" class="clsBorderTbl"><asp:Label ID="lblWOTitle" runat="server" Text='<%#Container.dataitem("WOTitle")%>' ></asp:Label></td>
           <td width="80" class="clsBorderTbl"><asp:Label ID="lblStartDate" runat="server" Text='<%#Container.dataitem("DateStart")%>' ></asp:Label></td>
           <td width="40" class="clsBorderTbl"><asp:Label ID="lblWP" runat="server" Text='<%#Container.dataitem("WholesalePrice")%>' ></asp:Label></td>
           <td width="40" class="clsBorderTbl"><span id="SpanPP" runat="server"><asp:Label ID="lblPP" runat="server" Text='<%#Container.dataitem("PlatformPrice")%>' ></asp:Label></span></td>
           <td width="35" class="clsBorderTbl"><asp:Label ID="lblUP" runat="server" Text='<%#Container.dataitem("UpSellPrice")%>' ></asp:Label></td>
           <td  class="clsBorderTbl"><a runat=server id="lnkDetail" href= '<%#"~/AdminWODetails.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("WorkOrderID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&PS=25&PN=0&SC=DateCreated&SO=DateCreated&BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID%>' ><img src="Images/Icons/ViewDetails.gif" title="View Details" width="16" height="16" hspace="2" vspace="0" border="0"></a>
           <a runat=server id="lnkViewHistory" href= '<%#"~/AdminWOHistory.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("WorkOrderID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&PS=25&PN=0&SC=DateCreated&SO=DateCreated&BizDivID=" & iif(BizDiv <> "", BizDiv, admin.Applicationsettings.OWUKBizDivId)  & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID %>'><img src="Images/Icons/ViewHistory.gif" alt="View Details" title="View History" hspace="2" vspace="0" border="0"></a>
           <a runat="server" id="lnkChangeWholesalePrice" href= '<%#"~/WOSetWholesalePrice.aspx?WOID=" & Container.DataItem("WOID") & "&" & "Group=" & Request("mode") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&PS=25&PN=0&SC=DateCreated&SO=DateCreated&BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID%>'><img src="Images/Icons/SetWholesalePrice.gif" title="Set Wholesale Price" width="18" height="12" hspace="2" vspace="0" border="0"></a>
           <a runat="server" id="lnkCancel" href= '<%#"~/WOCancellation.aspx?WOID=" & Container.DataItem("WOID") & "&" & "WorkOrderID=" & Container.DataItem("WorkOrderID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&PS=25&PN=0&SC=DateCreated&SO=DateCreated&BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID%>'><img src="Images/Icons/Cancel.gif" title="Cancel" width="11" height="11" hspace="2" vspace="0" border="0"></a>
           
           <%--  <asp:LinkButton ID="lnkbtnQuickNote" CommandName="AddNote" CausesValidation="false"  CommandArgument='<%#Container.dataitem("WOID") %>' runat="server">
                    <img id="imgQuickNote" runat="server" src='<%# IIF(Container.DataItem("NtCnt") = 0,"~/Images/Icons/QuickNote.gif","~/Images/Icons/Note.gif") %>' title='<%# IIF(Container.DataItem("NtCnt") = 0,"Add a Quick Note","") %>' width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" />
                </asp:LinkButton>
           
            <a runat="server" visible='<%# IIF(Container.DataItem("NtCnt") = 0,False,True) %>' id="lnkNotes" href= '<%#"~/WONotes.aspx?WOID=" & Container.DataItem("WOID") & "&" & "SearchWorkorderID=" & WorkorderID & "&" & "Group=" & Request("mode") & "&" & "FromDate=" & FromDate & "&" & "ToDate=" & ToDate & "&" & "ContactID=" & Container.DataItem("ContactID") & "&" & "CompanyID=" & Container.DataItem("CompanyID") & "&" & "SupContactID=" & Container.DataItem("SupplierContactID") & "&" & "SupCompID=" & Container.DataItem("SupplierCompanyID") & "&" & "PS=25&PN=0&SC=DateCreated&SO=DateCreated&BizDivID=" & BizDiv & "&" & "sender=UCWOsListing" & "&" & "CompID=" & CompID%>'><img src="Images/Icons/Note.gif" width="10" height="11" hspace="2" vspace="0" border="0"></a>
            
           <asp:LinkButton ID="lnkbtnUpdateRating" CausesValidation="false"   CommandName="RateWO" CommandArgument='<%#Container.dataitem("WOID") %>' runat="server"><img id="imgUpdateRating" runat="server" src="~/Images/Icons/Update_rating.gif" title="Rate WO" width="13" height="13" hspace="2" vspace="0" border="0" class="cursorHand" /></asp:LinkButton>
           --%>


          <span style='cursor:hand;' onMouseOut=javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%>','visible')>
          <asp:LinkButton ID="lnkbtnAction" CommandName="Action" CausesValidation="false" CommandArgument='<%#Container.DataItem("WOID") & "," & Container.DataItem("Rating") & "," & Container.DataItem("RatingComment") & "," & Container.DataItem("IsWatch") & "," & Container.DataItem("IsFlagged")%>' runat="server">
            <img id="imgAction" runat="server" src=<%#GetActionIcon(Container.DataItem("NtCnt"),Container.DataItem("Rating"))%>  width="24" height="30" hspace="2" vspace="0" border="0" class="cursorHand" />
          </asp:LinkButton>
          </span> 
             <div class="divUserNotesDetailForIssueList" id=divUserNotesDetail<%# DataBinder.Eval(Container.DataItem, "RefWOID")%> >
                 <asp:panel runat="server" style="width:230px;" id="pnlUserNotesDetail" Visible='<%# IIF(Container.DataItem("NtCnt") = 0,IIF(Container.DataItem("RatingComment") = "",False,True) ,True) %>' >
                 <div><b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b></div>
                 <div class="clsRatingInner">
                 <div id="divRatingComment" runat="server" visible='<%# IIF(Container.DataItem("RatingComment") = "",False,True) %>'>
                            <b style="color:#0C96CF; font-size:12px;">Rating Comment:</b>
                            <div style="margin-top: 0px; margin-bottom: 10px;">
                                <%# Container.DataItem("RatingComment")%>
                            </div>
                </div>
                <div id="divNotes" runat="server" visible='<%# IIF(Container.DataItem("NtCnt") = 0,False,True) %>'>
                  <b style="color:#0C96CF; font-size:12px;">Notes:</b>
			        <asp:Repeater ID="DLUserNotesDetail" runat="server" DataSource='<%#GetUserNotesDetail(Container.DataItem("WOID"))%>'>
                     <ItemTemplate>
                        <div style='visible: <%# iif( Trim(Container.DataItem("Comments")) = "" , "hidden", "visible") %>'>
                        <div>
                           <%# "<span class='discussionTextClient'>" & Strings.FormatDateTime(Container.DataItem("DateCreated"), DateFormat.ShortDate) & " - <b>" & Container.DataItem("OWRepFName") & " " & Container.DataItem("OWRepLName") & "</b></span>"%>
                        </div>
                        <div style="margin-top: 0px; margin-bottom: 10px;">
                        <%# Container.DataItem("Comments") %>
                        </div>
                            </div>
                     </ItemTemplate>
                    </asp:Repeater>
                  </div>
			     </div>   
			     <div><b class="b4"></b><b class="b3"></b><b class="b2"></b><b class="b1"></b></div>
			     </asp:panel>
		   </div>
            <asp:TemplateField HeaderStyle-CssClass="gridHdr">
                    <ItemStyle CssClass="gridRowIconsText" Wrap="true" Width="14px" />
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnWatch" CommandName="Watch" CausesValidation="false" runat="server"
                            CommandArgument='<%#Container.DataItem("WOID") %>' Visible='<%# IIF(Container.DataItem("IsWatch") = 0,True,False) %>'>
                            <img id="imgwatch" runat="server" src="~/Images/Icons/open.png" width="24" height="20"
                                hspace="2" vspace="0" border="0" class="cursorHand" />
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnUnwatch" CommandName="Unwatch" CausesValidation="false"
                            CommandArgument='<%#Container.DataItem("WOID") %>' runat="server" Visible='<%# IIF(Container.DataItem("IsWatch") = 1,True,False) %>'>
                            <img id="imgUnwatch" runat="server" src="~/Images/Icons/Closed.png" width="24" height="15"
                                hspace="2" vspace="0" border="0" class="cursorHand" />
                         </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
           </td>
          
    </tr>
    </table>
    </div>
  
        <div id="divIssueDetail" runat="server" style="VISIBILITY:hidden; WIDTH: 945px; POSITION: absolute; ">
            <asp:DataList RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLSupplierResponse" Runat="server" Width="100%" >
				<HeaderTemplate>
					<table width="100%"  cellspacing="0" cellpadding="0" >
						 <tr id="tblDGYourResponseHdr" align="left">                              
                              <td width="36"><asp:LinkButton id="lnkID" runat="server" style=" color:#FFFFFF;" >ID</asp:LinkButton> </td>
                              <td width="78"><asp:LinkButton id="lnkActionDate" runat="server" style=" color:#FFFFFF;" >Action Date</asp:LinkButton> </td>
                              <td width="70"><asp:LinkButton id="lnkStart" runat="server" style=" color:#FFFFFF;" ><asp:label ID="lblStartHdg" runat ="server" Text="Start"></asp:label> </asp:LinkButton></td>
                              <td width="70" id="tdAptTimeHD" runat ="server"><asp:label ID="lblAptTMHD" runat ="server" Text="Apt.Time"></asp:label></td>
                              <td width="70" id="tdDLEndDate" runat="server" ><asp:LinkButton id="lnkEnd" runat="server" style=" color:#FFFFFF;" >End</asp:LinkButton></td>
                              <td width="100" runat="server"  id="tdDLCompany" visible="false"><asp:LinkButton id="lnkCompany" runat="server" style=" color:#FFFFFF;" >Company</asp:LinkButton></td>
                              <td width="70" runat="server" id="tdDLContact" visible="false"><asp:LinkButton id="lnkContact" runat="server" style=" color:#FFFFFF;" >Contact</asp:LinkButton></td>
                              <td width="70" runat="server" id="tdDLPhone" visible="false"><asp:LinkButton id="lnkPhone" runat="server" style=" color:#FFFFFF;" >Phone</asp:LinkButton></td>
                              <td width="70" runat="server" id="tdDLSpecialist" visible="false"><asp:LinkButton id="lnkSpecialist" runat="server" style=" color:#FFFFFF;" >Specialist</asp:LinkButton></td>
                              <td width="70"><asp:LinkButton id="lnkPrice" runat="server" style=" color:#FFFFFF;" >Price</asp:LinkButton></td>
							  <td width="60" id="tdDLDayRate" runat="server" ><asp:LinkButton id="lnkDayRate" runat="server" style=" color:#FFFFFF;" >Day Rate</asp:LinkButton></td>
							  <td width="90" id="tdDLEstimatedTime" runat="server" ><asp:LinkButton id="lnkTime" runat="server" style=" color:#FFFFFF;" >Time (Days)</asp:LinkButton></td>
                              <td width="100" id="tdDLSupplyParts" runat=server ><asp:LinkButton id="lnkSupplyParts" runat="server" style=" color:#FFFFFF;" >Supply Parts</asp:LinkButton></td>
                              <td width="100" id="tdStatusHdr" runat="server"><span class="formTxtOrange"><asp:LinkButton CssClass="formTxtOrange" id="lnkStatus" runat="server" >Status</asp:LinkButton></span></td>
							  <td width="50" id="tdRatingHdr" VISIBLE=false runat="server" >Rating</td>
                              <td width="80" id="tdActionHdr" runat="server" >Action</td>                              
                            </tr>
					</table>
				</HeaderTemplate>
				<ItemTemplate>
					<table width="100%" cellspacing="0" cellpadding="0">
						<tr id="tblDLRow" align="left">                             
                              <td width="36"><%# DataBinder.Eval(Container.DataItem, "WOTrackingId")%></td>
                              <td Width="78"><%#Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateModified"), DateFormat.ShortDate)%></td>
                              <td width="70"><asp:label id="lblStartDate" runat="server" Text='<%# Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateStart"),DateFormat.ShortDate) %>' CssClass='<%# iif(Container.DataItem("DateStart") <> DataBinder.Eval(Container.DataItem, "DateStartOld"),"bodytxtValidationMsg","gridRow")%>'></asp:label></td>
                              <td width="70" runat="server" id="tdAptTime"><asp:Label runat="server" ID="lblAptTime" Text='<%#iif(Container.DataItem("AptTime")="", "NA", Container.DataItem("AptTime"))%>' CssClass='<%# iif(DataBinder.Eval(Container.DataItem, "AptTime") <>DataBinder.Eval(Container.DataItem, "AptTimeOld"),"bodytxtValidationMsg","gridRow")%>'></asp:Label></td>
                              <td width="70" id="tdDLEndDate1" runat="server" ><asp:label id="lblEndDate" runat="server" Text='<%# Strings.FormatDateTime(DataBinder.Eval(Container.DataItem, "DateEnd"),DateFormat.ShortDate) %>' CssClass='<%# iif(Container.DataItem("DateEnd") <> DataBinder.Eval(Container.DataItem, "DateEndOld"),"bodytxtValidationMsg","gridRow")%>'></asp:label></td>
                              <td id="tdDLCompany1" width="100"  runat="server"  visible="false">
                              <table width="100%" cellspacing="0" cellpadding="0" border="0" >
							    <tr>
								<td width="160" id="td1" visible="true" runat="server" style="border:0;" >
									<div style='visibility:hidden;position:absolute;margin-left:100px;' id=divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%> >
											<table width="250" height="100" bgcolor="#D6D7D6" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;" >
												<tr>
												  <td style="background-color:#D6D7D6; border:0; font-size:12;"  align='left'><strong>Admin:</strong> <%# iif(Container.DataItem("Name") = " ","--",Container.DataItem("Name"))%>  </td>
												</tr>
												<tr>
												  <td style="background-color:#D6D7D6;border:0;font-size:12;" align='left'><strong>Phone No:</strong> <%# iif(Container.DataItem("Phone") = " ","--",Container.DataItem("Phone"))%>  </td>
												</tr>
												<tr>
												  <td style="background-color:#D6D7D6;border:0;font-size:12;" align='left'><strong>Mobile No:</strong> <%# iif(Container.DataItem("Mobile") = " ","--",Container.DataItem("Mobile"))%>  </td>
												</tr>
												<tr>
												  <td style="background-color:#D6D7D6;border:0;font-size:12;" align='left'><strong>Email Id:</strong> <%# iif(Container.DataItem("Email") = " ","--",Container.DataItem("Email"))%>  </td>
												</tr>
											</table>
									</div>
									<span style=" border-left-width:0px; vertical-align:bottom; cursor:hand; font-family: Geneva, Arial, Helvetica, sans-serif; font-size:11px; color:#1D85BA " onMouseOut=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "ContactID")%>','visible')>
							              <%# DataBinder.Eval(Container.DataItem, "Company")%>
									</span> 
								</td>
							</tr>
						    </table>
                             </td>
                              <td width="70" id="tdDLContact1" runat="server"  visible="false" ><%# DataBinder.Eval(Container.DataItem, "Name")%></td>
                              <td width="70" id="tdDLPhone1" runat="server"  visible="false" ><%# DataBinder.Eval(Container.DataItem, "Phone")%></td>
                              <td width="70" id="tdDLSpecialist1" runat="server"  visible="false"><asp:label ID="Label1" CssClass="gridRow" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Specialist")%>'></asp:Label></td>
                              <td width="70" align="right"><asp:label id="lblPrice" CssClass='<%# iif(Container.DataItem("Value") > DataBinder.Eval(Container.DataItem, "ValueOld"),"bodytxtValidationMsg","gridRow")%>' runat="server" Text='<%#FormatCurrency(IIF(Not isDbnull(Container.DataItem("Value")) ,Container.DataItem("Value"),"0"),2,TriState.True, Tristate.true,Tristate.false)%>' ></asp:label></td>
							  <td width="60" runat="server" id="tdDLDayRate1" ><%# DataBinder.Eval(Container.DataItem, "DayJobRate")%></td>
							  <td width="90" runat="server" id="tdDLEstimatedTime1" ><%# DataBinder.Eval(Container.DataItem, "EstimatedTimeInDays")%></td>
                              <td width="100" id="tdDLSupplyParts1" runat=server ><asp:label id="lblSupplier" CssClass="gridRow" runat="server" Text='<%# iif(DataBinder.Eval(Container.DataItem, "SpecialistSuppliesParts") = True,"Yes","No")%>'></asp:label></td>
                              <td width="100" id="tdStatus" runat="server"><span class="bodytxtValidationMsg" ><%# DataBinder.Eval(Container.DataItem, "Status")%></span></td>
							  <td width="50" id="tdRating" visible="false" runat="server" >
							 	<div style='background-color:#FFFFFF; visibility:hidden; position:absolute; margin-left:20px; z-index:100' id=divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%> >
								  <table width='153px' height='65px' bgcolor='#FFFFFF'  border='0' cellspacing='0' cellpadding='0'>
								    <tr>
									  <td width='20' align='center'><img src='Images/Icons/Icon-Positive.gif' width='18' height='18'  align='absmiddle' /></td>
									  <td class='bgWhite' align='left'>Positive  <%# iif(ISDBNULL(Container.DataItem("PositiveRating")),0,Container.DataItem("PositiveRating"))%>  </td>
									</tr>
									<tr>
									  <td width='20' align='center'><img src='Images/Icons/Icon-Neutral.gif' width='13' height='5' align='absmiddle' /></td>
									  <td class='bgWhite' align='left'>Neutral  <%# iif(ISDBNULL(Container.DataItem("NeutralRating")),0,Container.DataItem("NeutralRating"))%>  </td>
									</tr>
									<tr>
									  <td width='20' align='center'><img src='Images/Icons/Icon-Negative.gif' width='17' height='18'  align='absmiddle' /></td>
									  <td class='bgWhite' align='left'>Negative  <%# iif(ISDBNULL(Container.DataItem("NegativeRating")),0,Container.DataItem("NegativeRating"))%>  </td>
									</tr>
								   </table>
								</div>
								<span style='cursor:hand;' onMouseOut=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>','hidden') onMouseOver=javascript:showHideRatingDetails('divRatingDetail<%# DataBinder.Eval(Container.DataItem, "CompanyID")%>','visible')> <%# Container.DataItem("TotalRating")%> </span>	
							  </td>
                              <td width="80" id="tdAction" runat="server"   align="center">
                               <%#GetActionColLinks(Container.DataItem("ContactID"), Container.DataItem("CompanyID"), Container.DataItem("WOID"), Container.DataItem("ContactClassID"))%>
                              </td>
                             
                            </tr>
					</table>
					<div id="divCommentsDetail" runat="server" visible='<%#ShowHideComments(Container.DataItem("WOTrackingID"), Container.DataItem("CompanyID"))%>'  style="border-top:solid 1px #fff;padding-bottom:5px;">
					<table id="tblCommentDetails" runat="server" width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="#EDEDED" >
                             <tr>
    						    <td width="10" height="10" style="padding:0px; margin:0px"></td>
    						    <td height="10" style="padding:0px; margin:0px"></td>
    						    <td width="10" height="10" style="padding:0px; margin:0px"></td>
  						    </tr>
  						    <tr>
    						    <td width="10">&nbsp;</td>
    						    <td>
							        <table id="tblDiscussion" width="100%"  border="0" cellspacing="0" cellpadding="0" bgcolor="#EDEDED">
									   <tr><td width="10px">&nbsp;</td></tr>
									      <tr><td width="10px">&nbsp;</td><td>
											
											<asp:DataList DataSource='<%#GetDiscussions(Container.DataItem("WOTrackingID"),Container.DataItem("CompanyID"))%>'  RepeatLayout="Table" CellPadding="0" CellSpacing="0" ID="DLDiscussion" Runat="server" Width="100%">
											    <ItemTemplate>
												<table width="100%"  border="0" cellspacing="0" cellpadding="0">
												    <tr>
												      <td width="200" valign="baseline"><%#IIf(DataBinder.Eval(Container.DataItem, "Status") <> admin.Applicationsettings.WOStatusID.CAMailSent, (IIf((DataBinder.Eval(Container.DataItem, "ContactClassID") = admin.Applicationsettings.RoleOWID), "<span class='discussionTextClient'>" & Container.DataItem("CommentsDate") & " - " & (Container.DataItem("CommentsBy")) & "  </span>", "<span class='discussionTextSupplier'>" & Container.DataItem("CommentsDate") & " - " & (Container.DataItem("CommentsBy")) & "  </span>")), "<span class='discussionTextClient'>" & Container.DataItem("CommentsDate") & " - " & ("Admin's CA Mail") & "  </span>")%></td>
												      <td width="20" align="center" valign="top">:</td>
												      <td valign="baseline" class="remarksAreaCollapse"> <%# Container.DataItem("Comments") %> </td>
													  <td width="10px"></td>
												    </tr>
											      </table>     									      
                            				</ItemTemplate>
										      </asp:DataList>
										    </td>
									      </tr>
									      <tr>
										    <td>&nbsp;</td>
										    <td>&nbsp;</td>
									      </tr>
									    </table>
									   
								   
								    </td>
    						    <td width="10">&nbsp;</td>
  					       </tr>
  						    <tr>
  						      <td height="10" style="padding:0px; margin:0px"></td>
  						      <td height="10" style="padding:0px; margin:0px"></td>
  						      <td height="10" style="padding:0px; margin:0px"></td>
						      </tr>
					      
					</table></div>
					
				</ItemTemplate>
			</asp:DataList>
        </div>  
          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#DFDFDF" visible="true">
                              	  <tr>
                                    <td class="smallText" height="8" style="padding:0px; margin:0px"></td>
                                	<td height="8" align="center" class="smallText" style="padding:0px; margin:0px;cursor:pointer;" id="tdCollapse" runat="server">
                                	    <a href="" border="0" onClick='<%# "javascript:CollapseIssueDetails(" & Container.DataItem("WOID") & ", this.id)" %>' runat="server" id="collapseDetails">
                                	        <img src="Images/Arrows/Grey-Arrow.gif" width="50" title='Expand' height="8" border="0" name='<%# "imgExp" & Container.DataItem("WOID") %>'  id='<%# "imgExp" & Container.DataItem("WOID") %>' onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow-Roll.gif')" onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Grey-Arrow.gif');" style="position:static;" />
                                	        <img src="Images/Arrows/Orange-Arrow-Col.gif" width="50" title='Collapse' height="8" border="0" name='<%# "imgCol" & Container.DataItem("WOID") %>'  id='<%# "imgCol" & Container.DataItem("WOID") %>' onmouseover="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col-Roll.gif')" onmouseout="javascript:changeImageOnMouseAction(this.id,'Images/Arrows/Orange-Arrow-Col.gif');" style="visibility:hidden; position:absolute;" />
                                	    </a>
                                	</td>
	                                <td class="smallText" height="8" style="padding:0px; margin:0px"></td>
                                  </tr>
                                 </table>    
    </div>     
</ItemTemplate>
</asp:datalist>


<div style="background-color:#dad8d9;width:100%;margin:2px;" >
<uc1:GridPager id="GridPagerBtm" runat="server"></uc1:GridPager></div>
</asp:Panel>
<asp:Panel ID="pnlNoRecords" runat="server" Visible="false" >
     <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
</asp:Panel>

<cc1:ModalPopupExtender ID="mdlUpdateRating" runat="server" TargetControlID="btnTempRateWO" PopupControlID="pnlUpdateWO" OkControlID="" CancelControlID="ancCancel1" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False"></cc1:ModalPopupExtender>
               <asp:Panel runat="server" ID="pnlUpdateWO"  Width="300px" CssClass="pnlConfirmWOListing" style="display:none;">
               <div id="div1">
               <b>Rate workorder</b> <br />  
                 <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnPositive" Checked="false" runat="server" Text="Positive" />
                 <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNeutral"  Checked="false" runat="server"  Text="Neutral"/>
                 <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnNegative" Checked="false" runat="server" Text="Negative" /><br /><br />
                 <b>Add comment here</b><br />
                    <asp:TextBox runat="server" ID="txtComment" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox>        
                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtComment" WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        
                 <div id="divButton" class="divButton" style="width: 85px; float:left;">
                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                        <a class="buttonText cursorHand" href='javascript:validate_RateWO("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_txtComment","Enter a new comment here")' id="a1"><strong>Submit</strong></a>
                        <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    </div>
                    <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                        <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                        <a class="buttonText cursorHand" runat="server" id="ancCancel1"><strong>Cancel</strong></a>
                        <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    </div> 
                    </div>
                    </asp:Panel>
                        <asp:Button runat="server" ID="hdnBtnRateWO"  OnClick="hdnBtnRateWO_Click" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
                        <asp:Button runat="server" ID="btnTempRateWO" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

 <cc1:ModalPopupExtender ID="mdlAction" runat="server" TargetControlID="btnActionTemp" PopupControlID="pnlActionConfirm" OkControlID="" CancelControlID="ancCancelAction" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
			      
<asp:Panel runat="server" ID="pnlActionConfirm"  Width="340px" CssClass="pnlConfirmWOListingForAction" style="display:none;">
               
    <div id="div2">
               <div class="divTopActionStyle"><b>OrderWork Action Form</b> </div>

               <div class="divTopActionStyle">
                           <b>Rate workorder</b> <br />  
                             <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnActionPositive" Checked="false" runat="server" Text="Positive" />
                             <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnActionNeutral"  Checked="false" runat="server"  Text="Neutral"/>
                             <asp:RadioButton GroupName="SelectedRatings" ID="rdBtnActionNegative" Checked="false" runat="server" Text="Negative" /><br />
                             Add comment for rating<br />
                             <asp:TextBox runat="server" ID="txtCommentAction" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox>        
                           <%-- <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtComment" WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        --%>
                </div> 
                    
                <div class="divTopActionStyle">
                            <b>Post Quick Note</b><br />
                            <asp:TextBox runat="server" ID="txtNoteAction" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox> 
                            <span style="color:#e51312;font-size:11px;">*Notes entered here are NOT visible to the Suppliers/Clients.</span><br />       
                            <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtNote" WatermarkText="Enter a new note here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        --%>
                </div>

                 <div class="divTopActionStyle" id="div3" runat="server">
                            <asp:CheckBox ID="chkShowClient" runat="server" style="font-weight:bold;" Text="Show To Client"  ></asp:CheckBox>
                            <asp:CheckBox ID="chkShowSupplier" runat="server" style="font-weight:bold;" Text="Show To Supplier"  ></asp:CheckBox>
                </div>
               
                <div class="divTopActionStyle" style="font-weight:bold;">
                            <asp:CheckBox ID="chkMyWatch" runat="server" Text="Add To MyWatch" ></asp:CheckBox>
                </div>
                 
                <div class="divTopActionStyle" style="font-weight:bold;">
                           <asp:CheckBox ID="chkMyRedFlag" runat="server" Text="Add To MyRedFlag" ></asp:CheckBox>
                </div>
                 
                 <div style="padding-left:20px;padding-top:10px;float:left;width:305px;">
               
                           <div  class="divButtonAction" style="width: 85px; float:left;cursor:pointer;">
                           <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner"  /></div>                    
                           <a class="buttonText cursorHand" onclick='javascript:validate_ActionComment("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_txtCommentAction","Please enter rating comment");' id="ancSubmitAction">Submit</a>
                           <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner"  /></div>
                           </div>

                           <div  class="divButtonAction" style="width: 85px; float:left; margin-left:20px;cursor:pointer;">
                           <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                           <a class="buttonText cursorHand" runat="server" id="ancCancelAction">Cancel</a>
                           <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                           </div> 

                </div>
                    
                    
    </div>
    
  </asp:Panel>
  
<asp:Button runat="server" ID="hdnBtnAction"  Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
<asp:Button runat="server" ID="btnActionTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

 </ContentTemplate>
 </asp:UpdatePanel>
 
 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
        <ProgressTemplate>
            <div class="gridText">
                <img  align=middle src="Images/indicator.gif" />
                <b>Fetching Data... Please Wait</b>
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="pnlIssueListing" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
<cc1:ModalPopupExtender ID="mdlQuickNotes" runat="server" TargetControlID="btnTemp" PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
			      
<asp:Panel runat="server" ID="pnlConfirm" Width="300px" CssClass="pnlConfirm" style="display:none;">
    <div id="divModalParent">
        <span style="font-family:Tahoma; font-size:14px;"><b>Add a QuickNote</b></span><br /><br />
        <asp:TextBox runat="server" ID="txtNote" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox>        
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtNote" WatermarkText="Enter a new note here" WatermarkCssClass="bodyTextGreyLeftAligned"></cc1:TextBoxWatermarkExtender>        
        <br /><br />
            <div id="divButton" class="divButton" style="width: 85px; float:left;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                    <a class="buttonText cursorHand" onclick='javascript:validate_requiredForIssueNote("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_txtNote","Please enter a note")' id="ancSubmit">Submit</a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div>
            <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>								
</asp:Panel>
<asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
<asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;"/>
<script type="text/javascript">
   function validate_RateWO(field, alerttxt) {

        var text = document.getElementById(field).value;
        if (text == null || text == "" || text == "Enter a new comment here") {
            alert(alerttxt);
        }
        else {
            if (text.length > 500) {
                alert('Please enter comments less than 500 characters');
            }
            else {
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_hdnBtnRateWO").click();
            }
        }
    }

//    function validate_ActionNotes(field, alerttxt) {

//        var text = document.getElementById(field).value;

//        if ((document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_txtCommentAction").value) == null || (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_txtCommentAction").value) == "" || (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_txtCommentAction").value) == "Enter a new comment here") {

//        }
//        else {
//            if (text != "Enter a new note here") {
//                while (text.indexOf(" ") != -1) {
//                    text = text.replace(" ", "");
//                }
//            }



//            document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_hdnBtnAction").click();

//        }
    //    }

    function validate_ActionComment(field, alerttxt) {
        var RatingSelected = 0;

        if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_rdBtnActionPositive").checked) {
            RatingSelected = 1;
        }
        else if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_rdBtnActionNeutral").checked) {
            RatingSelected = 1;
        }
        else if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_rdBtnActionNegative").checked) {
            RatingSelected = 1;
        }
        if (RatingSelected == 1) {
            var text = document.getElementById(field).value;
            if (text == null || text == "" || text == "Enter a new comment here") {
                //alert(alerttxt);
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_hdnBtnAction").click();
            }
            else {
                if (text.length > 500) {
                    alert('Please enter comments less than 500 characters');
                }
                else {
                    document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_hdnBtnAction").click();
                }

            }
        }
        else {
            document.getElementById("ctl00_ContentPlaceHolder1_UCAdminWOsIssueListing1_hdnBtnAction").click();
        }
        
    }

</script>


