﻿Public Class UCAccreditationForSearch
    Inherits System.Web.UI.UserControl
    Shared ws As New WSObjs

    Public Property BizDivId() As Integer
        Get
            If Not IsNothing(ViewState("AccredBizDivId")) Then
                Return ViewState("AccredBizDivId")
            Else
                Return ApplicationSettings.BizDivId
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("AccredBizDivId") = value
        End Set
    End Property
    Public Property CommonID() As Integer
        Get
            If Not IsNothing(ViewState("CommonID")) Then
                Return ViewState("CommonID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("CommonID") = value
        End Set
    End Property
    Public Property Type() As String
        Get
            If Not IsNothing(ViewState("Type")) Then
                Return ViewState("Type")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("Type") = value
        End Set
    End Property
    Public Property Mode() As String
        Get
            If Not IsNothing(ViewState("Mode")) Then
                Return ViewState("Mode")
            Else
                Return "AllAccreditations"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("Mode") = value
        End Set
    End Property
    Private _cachekey As String = ""
    Public Property AppendCachekey() As String
        Get
            Return _cachekey
        End Get
        Set(ByVal value As String)
            _cachekey = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            hdnType.Value = Type
            hdnCommonID.Value = CommonID
            If Not IsNothing(Request("vendorIDs")) Then
                If (Request("vendorIDs") <> "") Then
                    populateAccred()
                Else
                    populateAccred(True)
                End If
            Else
                populateAccred(True)
            End If
            'populateAccred(True)
            PopulateSelectedGrid()
        End If
    End Sub
    Public Sub populateAccred(Optional ByVal killCache As Boolean = False)
        Dim ds As New DataSet
        ds = GetSelectedAccred(killCache)
    End Sub
    Public Function GetSelectedAccred(Optional ByVal killCache As Boolean = False)
        hdnType.Value = Type
        hdnCommonID.Value = CommonID
        Dim ds As New DataSet
        ' this gets all Accred and selected Accred
        Dim CacheKey As String = "AccredList" & Session.SessionID & BizDivId & CommonID
        If _cachekey <> "" Then
            CacheKey = CacheKey & "-" & _cachekey
        End If

        If killCache Then
            Cache.Remove(CacheKey)
        End If

        If Cache(CacheKey) Is Nothing Then
            ds = ws.WSContact.GetAccreditations(CommonID, Mode)
            Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
        Else
            ds = CType(Page.Cache(CacheKey), DataSet)
        End If
        Return ds
    End Function

    Public Sub RemoveCallback(ByVal tag As String, ByVal obj As Object, ByVal reason As CacheItemRemovedReason)
        If (CacheItemRemovedReason.Underused = reason) Then
            Cache.Remove(tag)
            If Not IsNothing(obj) Then
                Cache.Insert(tag, obj, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(1, 0, 0), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))
            End If
        Else
            Cache.Remove(tag)
        End If
    End Sub
    Public Sub PopulateSelectedGrid(Optional ByVal killCache As Boolean = False)
        pnlSelectedAccreds.Visible = False
        pnlSelectedCompany.Visible = False
        pnlSelectedEngineer.Visible = False
        pnlSelectedOthers.Visible = False
        Dim ds As DataSet = GetSelectedAccred(killCache)
        Dim dtSelectedAccred As DataTable = ds.Tables(0)
        Dim dvSelectedAccred As New DataView
        If Not IsDBNull(dtSelectedAccred) Then
            If dtSelectedAccred.Rows.Count > 0 Then
                dvSelectedAccred = dtSelectedAccred.DefaultView

                If (Type = "AllAccreditations" Or Type = "ServiceAccreditations") Then
                    pnlSelectedAccreds.Visible = False

                    dvSelectedAccred.RowFilter = "Type IN ('Company')"
                    If (dvSelectedAccred.Count > 0) Then
                        pnlSelectedCompany.Visible = True

                        rptSelectedCompany.DataSource = dvSelectedAccred
                        rptSelectedCompany.DataBind()
                    Else
                        pnlSelectedCompany.Visible = False
                    End If

                    dvSelectedAccred.RowFilter = "Type IN ('Engineer')"
                    If (dvSelectedAccred.Count > 0) Then
                        pnlSelectedEngineer.Visible = True

                        rptSelectedEngineer.DataSource = dvSelectedAccred
                        rptSelectedEngineer.DataBind()
                    Else
                        pnlSelectedEngineer.Visible = False
                    End If

                    dvSelectedAccred.RowFilter = ""

                    dvSelectedAccred.RowFilter = "Type IN ('Others')"
                    If (dvSelectedAccred.Count > 0) Then
                        pnlSelectedOthers.Visible = True

                        rptSelectedOthers.DataSource = dvSelectedAccred
                        rptSelectedOthers.DataBind()
                    Else
                        pnlSelectedOthers.Visible = False
                    End If

                    dvSelectedAccred.RowFilter = ""

                Else
                    pnlSelectedCompany.Visible = False
                    pnlSelectedEngineer.Visible = False
                    pnlSelectedOthers.Visible = False

                    dvSelectedAccred.RowFilter = ""
                    pnlSelectedAccreds.Visible = True

                    gvSelectedAccreds.DataSource = dvSelectedAccred
                    gvSelectedAccreds.DataBind()
                    If dtSelectedAccred.Rows.Count > 10 Then
                        divAccreds.Attributes.Add("style", "height:208px")
                    Else
                        divAccreds.Attributes.Remove("style")
                    End If
                End If
            Else
                pnlSelectedAccreds.Visible = False
                pnlSelectedCompany.Visible = False
                pnlSelectedEngineer.Visible = False
                pnlSelectedOthers.Visible = False
            End If
        End If
    End Sub
    'Private Sub gvSelectedAccreds_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSelectedAccreds.PreRender
    '    If Not IsNothing(gvSelectedAccreds) Then
    '        If Not IsNothing(gvSelectedAccreds.HeaderRow) Then
    '            gvSelectedAccreds.HeaderRow.Visible = False
    '        End If
    '    End If
    'End Sub

    Public Sub hdnAccrSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnAccrSubmit.Click
        If (txtExpertise.Value <> "") Then
            divMoreInfoAutoSuggest.Style.Add("display", "none")
            If txtExpiryDate.Text <> "" Then
                'prepare regular expression
                Dim str As String = "(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                'match input value against regular expression 
                Dim myMatch As Match = System.Text.RegularExpressions.Regex.Match(txtExpiryDate.Text.Trim, str)
                If Not myMatch.Success Then
                    'if format is not correct
                    lblMoreInfo.Text = "Expiry date should be in dd/mm/yyyy"
                    divMoreInfoAutoSuggest.Style.Add("display", "block")
                    Exit Sub
                End If
            End If

            Dim ds As DataSet = GetSelectedAccred()
            Dim dtSelectedAccred As DataTable = ds.Tables(0)

            Dim dvSelectedAccred As New DataView
            If Not IsDBNull(dtSelectedAccred) Then
                If dtSelectedAccred.Rows.Count > 0 Then
                    dvSelectedAccred = dtSelectedAccred.DefaultView
                    dvSelectedAccred.RowFilter = "TagName IN ('" & txtExpertise.Value.Trim & "')"
                    If (dvSelectedAccred.Count > 0) Then
                        ResetData()
                        lblMessage.Text = "Selected item already exists in your selection."
                    Else
                        lblMessage.Text = ""
                        AddSelectedAccr(ds)
                    End If
                Else
                    lblMessage.Text = ""
                    AddSelectedAccr(ds)
                End If
            End If
        End If

    End Sub
    Public Sub AddSelectedAccr(ByVal ds As DataSet)
        Dim dtSelectedAccred As DataTable = ds.Tables(0)

        Dim dRow As DataRow = dtSelectedAccred.NewRow

        If (hdnTagID.Value = "") Then
            dRow("TagId") = 0
            dRow("Type") = "Others"
        Else
            dRow("TagId") = hdnTagID.Value
            If (hdnSelectedType.Value = "Company") Then
                dRow("Type") = "Company"
            ElseIf (hdnSelectedType.Value = "Engineer") Then
                dRow("Type") = "Engineer"
            Else
                dRow("Type") = "Others"
            End If
        End If

        dRow("TagName") = txtExpertise.Value.Trim
        dRow("TagInfo") = txtTagInfo.Text.Trim
        dRow("TagExpiry") = txtExpiryDate.Text.Trim
        dRow("OtherInfo") = txtOtherInfo.Text.Trim
        dRow("DateCreated") = Date.Today

        dtSelectedAccred.Rows.Add(dRow)

        Dim CacheKey As String = "AccredList" & Session.SessionID & BizDivId & CommonID
        Page.Cache.Remove(CacheKey)
        Page.Cache.Add(CacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf Me.RemoveCallback))

        PopulateSelectedGrid()

        ResetData()

    End Sub
    Public Sub ResetData()
        txtExpertise.Value = ""
        txtTagInfo.Text = ""
        txtExpiryDate.Text = ""
        txtOtherInfo.Text = ""
        hdnTagID.Value = ""
        hdnSelectedType.Value = ""
        lblMoreInfo.Text = ""
        lblMessage.Text = ""
    End Sub


    Public Sub lnkRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemove.Click

        Dim ds As DataSet = GetSelectedAccred()
        Dim dtSelectedAccred As DataTable = ds.Tables(0)
        Dim dvSelectedAccred As New DataView
        dvSelectedAccred = dtSelectedAccred.Copy.DefaultView

        Dim flagCheck As Boolean = False
        'add primary key as TagId 
        Dim keys(1) As DataColumn
        keys(0) = dtSelectedAccred.Columns("TagId")

        'Iteration through grid
        For Each row As GridViewRow In gvSelectedAccreds.Rows
            Dim chkBox As CheckBox = CType(row.FindControl("CheckAccred"), CheckBox)
            If chkBox.Checked Then
                'dtSelectedAccred.PrimaryKey = keys
                flagCheck = True
                If dtSelectedAccred.Rows.Count > 0 Then
                    Dim id As String
                    id = CType(row.FindControl("hdnTagId"), HtmlInputHidden).Value
                    Dim foundrow As DataRow() = dtSelectedAccred.Select("TagId = '" & id & "'")
                    If Not (foundrow Is Nothing) Then
                        dtSelectedAccred.Rows.Remove(foundrow(0))
                    End If
                End If

            End If
        Next

        Dim CacheKeySelected As String = "AccredList" & Session.SessionID & BizDivId & CommonID
        Page.Cache.Remove(CacheKeySelected)
        Page.Cache.Add(CacheKeySelected, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)

        'Show validation message
        If flagCheck Then
            lblMessage.Text = ""
        Else
            lblMessage.Text = ResourceMessageText.GetString("SeleteVendorDel")
        End If

        populateAccred()
        PopulateSelectedGrid()
    End Sub

    Private Sub rptSelectedCompany_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSelectedCompany.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "Remove") Then
                RemoveAccred(e.CommandArgument.ToString)
            End If
        End If
    End Sub
    Private Sub rptSelectedEngineer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSelectedEngineer.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "Remove") Then
                RemoveAccred(e.CommandArgument.ToString)
            End If
        End If
    End Sub
    Private Sub rptSelectedOther_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSelectedOthers.ItemCommand
        If e.Item.ItemType <> ListItemType.Header Then
            If (e.CommandName = "Remove") Then
                RemoveAccred(e.CommandArgument.ToString)
            End If
        End If
    End Sub

    Public Sub RemoveAccred(ByVal TagId As String)
        Dim ds As DataSet = GetSelectedAccred()
        Dim dtSelectedAccred As DataTable = ds.Tables(0)
        Dim dvSelectedAccred As New DataView
        dvSelectedAccred = dtSelectedAccred.Copy.DefaultView

        'add primary key as TagId 
        Dim keys(1) As DataColumn
        keys(0) = dtSelectedAccred.Columns("TagId")

        Dim foundrow As DataRow() = dtSelectedAccred.Select("TagId = '" & TagId & "'")
        If Not (foundrow Is Nothing) Then
            dtSelectedAccred.Rows.Remove(foundrow(0))
        End If

        Dim CacheKeySelected As String = "AccredList" & Session.SessionID & BizDivId & CommonID
        Page.Cache.Remove(CacheKeySelected)
        Page.Cache.Add(CacheKeySelected, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)

        populateAccred()
        PopulateSelectedGrid()
    End Sub
End Class