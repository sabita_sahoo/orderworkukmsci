﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCTableAOE.ascx.vb" Inherits="Admin.UCTableAOE" %>
<div class="clsITMain">
  <div class="clsITkillSet"><b>IT Skills Set</b></div>
  <input id="hdnCombIDs" type="hidden" name="hdnCombIDs" runat="server" value="0">
  <input id="hdnPageName" type="hidden" name="hdnPageName" runat="server" value="">
   

  <asp:Repeater ID="rptAOEMainIT" runat="server">
  <ItemTemplate>
  <div class='<%#IIF(Container.ItemIndex  Mod 2 = 0 ,"divStyle","divStyleAlternate") %>'>
     
     <div id='AOE<%#Eval("CombID")%>' onclick="javascript:CheckAOEMainCat(this.id,'<%= hdnCombIDs.ClientID %>')"  class='<%#IIF(Container.DataItem("SelectedAOE") <> 0,"clsSelectedSkillLeftDiv","clsNotSelectedSkillLeftDiv") %>'>                 
     <input id='hdnSubCatOfMainCat<%#Eval("CombID")%>' type="hidden" name="hdnMainCatCombId" value='<%#Eval("SubCatOfMainCat")%>'> 
        <asp:Label ID="lblMainCatNameIT" runat="server" Text=' <%#Eval("MainCatName")%> '></asp:Label>
     </div>
      <div class="divSkillSetRight">      
      <asp:Repeater ID="rptAOESubIT" runat="server">
        <ItemTemplate>      
           <input id='hdnMainCatCombId<%#Eval("CombID")%>' type="hidden" name="hdnMainCatCombId" value='<%#Eval("MainCatCombId")%>'>           
           <input id='hdnSelSubCatCombId<%#Eval("MainCatCombId")%>' type="hidden" name="hdnSelSubCatCombId" value='<%#Eval("CommonSelectedAOE")%>'>
           <div style=" text-align:center" id='AOE<%#Eval("CombID")%>' onclick="javascript:CheckAOESubCat(this.id,'<%= hdnCombIDs.ClientID %>')"  class='<%#IIF(Container.DataItem("SelectedAOE") <> 0,"clsSelectedSkillDiv","clsNotSelectedSkillDiv") %>'><%#Eval("SubCatName")%></div> 
        </ItemTemplate>    
       </asp:Repeater>                         
      </div>
  </div>
   </ItemTemplate>    
  </asp:Repeater>  
  
  </div>

<div class="clsRetailMain">
  <div class="clsRetailskillSet"><b>Retail Skills Set</b></div>
   <asp:Repeater ID="rptAOEMainRetail" runat="server">
  <ItemTemplate>
  <div class='<%#IIF(Container.ItemIndex  Mod 2 = 0 ,"divStyle","divStyleAlternate") %>'>
     
     <div id='AOE<%#Eval("CombID")%>' onclick="javascript:CheckAOEMainCat(this.id,'<%= hdnCombIDs.ClientID %>')"  class='<%#IIF(Container.DataItem("SelectedAOE") <> 0,"clsSelectedSkillLeftDiv","clsNotSelectedSkillLeftDiv") %>'>                 
     <input id='hdnSubCatOfMainCat<%#Eval("CombID")%>' type="hidden" name="hdnMainCatCombId" value='<%#Eval("SubCatOfMainCat")%>'> 
        <asp:Label ID="lblMainCatNameRetail" runat="server" Text=' <%#Eval("MainCatName")%> '></asp:Label>
     </div>
      <div class="divSkillSetRight">      
      <asp:Repeater ID="rptAOESubRetail" runat="server">
        <ItemTemplate>      
           <input id='hdnMainCatCombId<%#Eval("CombID")%>' type="hidden" name="hdnMainCatCombId" value='<%#Eval("MainCatCombId")%>'>           
           <input id='hdnSelSubCatCombId<%#Eval("MainCatCombId")%>' type="hidden" name="hdnSelSubCatCombId" value='<%#Eval("CommonSelectedAOE")%>'>
           <div style=" text-align:center" id='AOE<%#Eval("CombID")%>' onclick="javascript:CheckAOESubCat(this.id,'<%= hdnCombIDs.ClientID %>')"  class='<%#IIF(Container.DataItem("SelectedAOE") <> 0,"clsSelectedSkillDiv","clsNotSelectedSkillDiv") %>'><%#Eval("SubCatName")%></div> 
        </ItemTemplate>    
       </asp:Repeater>                         
      </div>
  </div>
   </ItemTemplate>    
  </asp:Repeater>  
  
  </div>
