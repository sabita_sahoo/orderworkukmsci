<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMSCompanyProfile.ascx.vb"
    Inherits="Admin.UCMSCompanyProfile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc3" TagName="UCDualListBox" Src="~/UserControls/UK/UCDualListBox.ascx" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>
<script language="javascript" src="JS/Scripts.js" type="text/javascript"></script>
<script type="text/javascript">

    function CheckFileExistence() {
        var flag = false;
        var str = '';
        VatCheckedYes = document.getElementById("ctl00_ContentPlaceHolder1_UCCompanyProfile1_RegisterYes").checked;
        if (VatCheckedYes == true) {
            if (!$('#ctl00_ContentPlaceHolder1_UCCompanyProfile1_UCFileUpload4_tdAttach').length) {

                str += "Please upload document in VAT section";
                flag = true;
            }
        }

        if (flag) {

            alert(str);
            return false;
        }
        else {
            return true;
        }
    }
</script>
<asp:Panel ID="PnlCompanyProfile" runat="server">
    <asp:UpdatePanel ID="UpdatePnlCompanyProfile" runat="server" UpdateMode="Conditional"
        RenderMode="Inline">
        <ContentTemplate>
            <asp:Panel ID="pnlSubmitForm" runat="server" Visible="true">
                <table cellspacing="0" cellpadding="0" width="100%" border="0" style="padding: 0px 0px 16px 0px;
                    margin: 0px;">
                    <tr>
                        <td width="115" class="txtWelcome paddingL10">
                            <div id="divButton" style="width: 115px;">
                                <div class="bottonTopGrey">
                                    <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                        height="4" class="btnCorner" style="display: none" /></div>
                                <a class="buttonText" runat="server" id="lnkBackToListing">
                                    <img src="Images/Arrows/Arrow-Back.gif" title="Back to Listing" width="4" height="8"
                                        hspace="0" vspace="0" border="0" class="paddingL10R8"><strong>Back to Listing</strong></a>
                                <div class="bottonBottomGrey">
                                    <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                        height="4" class="btnCorner" style="display: none" /></div>
                            </div>
                        </td>
                    </tr>
                </table>
                <div id="divValidationMain" class="divValidation" runat="server">
                    <div class="roundtopVal">
                        <blockquote>
                            <p>
                                <img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner"
                                    style="display: none" /></p>
                        </blockquote>
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="15" align="center">
                                &nbsp;
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                        </tr>
                        <tr valign="middle">
                            <td width="63" align="center" valign="top">
                                <img src="Images/Icons/Validation-Alert.gif" width="31" height="31">
                            </td>
                            <td class="validationText">
                                <div id="divValidationMsg">
                                </div>
                                <asp:Label ID="lblError" CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
                                <asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg"
                                    DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
                            </td>
                            <td width="20">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="15" align="center">
                                &nbsp;
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                            <td height="15">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <div class="roundbottomVal">
                        <blockquote>
                            <p>
                                <img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner"
                                    style="display: none" /></p>
                        </blockquote>
                    </div>
                </div>
                <div style="margin: 0px 5px 0px 5px;" id="divMainTab">
                    <div class="paddedBox" id="divProfile">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" class="HeadingRed">
                                    <strong>Company Profile</strong>&nbsp;&nbsp;
                                </td>
                                 <%--'OD-804 - .Net - Skills, Accreditation and Location coverage changes to make it Live--%>
                                <%--Company Profile Beta--%>
                                  <td align="left" class="padTop17">
                                       <asp:Panel ID="pnlContactProfileBeta" runat="server" Visible="false">
                                             <table runat="server" cellspacing="0" cellpadding="0" width="100%"
                                                 border="0" style="padding: 0px 0px 16px 0px; margin: 0px;">
                                                 <tr>
                                                     <td valign="top">
                                                         <table cellspacing="0" cellpadding="0" width="130" bgcolor="#F0F0F0" border="0">
                                                             <tr>
                                                                 <td width="5">
                                                                     <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                                 </td>
                                                                 <td width="100" class="txtBtnImage">
                                                                    <a href ="ContactProfileBeta.aspx" id="lnkCompanyProfileBeta" target="_blank">Company Profile Beta</a>
                                                                 </td>
                                                                 <td width="5">
                                                                     <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                                 </td>
                                                             </tr>
                                                         </table>
                                                     </td>
                                                      <td valign="middle">
                                                         <table cellspacing="0" cellpadding="0" width="50" bgcolor="#F0F0F0" border="0">
                                                             <tr>
                                                                 <asp:CheckBox ID="chkRecordVerifed" runat="server" Text="Record Verified" TextAlign="Right" Visible="false" /> 
                                                             </tr>
                                                         </table>
                                                     </td>
                                                   
                                                    
                                                 </tr>
                                             </table>
                                           </asp:Panel>
                                </td>






                                            <%----            --%>
                                <td align="left" class="padTop17">
                                    <table id="tblSaveTop" runat="server" cellspacing="0" cellpadding="0" width="100%"
                                        border="0" style="padding: 0px 0px 16px 0px; margin: 0px;">
                                        <tr>
                                            <td align="right" valign="top">
                                                <table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                                                    <tr>
                                                        <td width="5">
                                                            <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                        </td>
                                                        <td width="110" class="txtBtnImage">
                                                            <asp:LinkButton ID="btnSaveTop" OnClientClick="javascript:callSetFocusValidation('UCCompanyProfile1')"
                                                                CausesValidation="false" CssClass="txtListing" runat="server" TabIndex="67"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:LinkButton>
                                                        </td>
                                                        <td width="5">
                                                            <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="10">
                                            </td>
                                            <td width="125" align="right" valign="top" style="padding: 0px 0px 0px 0px;">
                                                <table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                                                    <tr>
                                                        <td width="5">
                                                            <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                        </td>
                                                        <td width="115" class="txtBtnImage">
                                                            <asp:LinkButton ID="btnResetTop" CausesValidation="false" runat="server" TabIndex="68"
                                                                class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton>
                                                        </td>
                                                        <td width="5">
                                                            <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table id="tblAccountManager" runat="server" cellpadding="0" cellspacing="0" width="100%"
                            border="0">
                            <tr>
                                <td width="6px">
                                    &nbsp;
                                </td>
                                <td height="50px" width="155">
                                    <asp:DropDownList ID="ddlOWAccountManager" runat="server" CssClass="formField150">
                                    </asp:DropDownList>
                                </td>
                                <td width="6px">
                                    &nbsp;
                                </td>
                                <td class="formTxt" valign="middle">
                                    Account Manager
                                </td>
                            </tr>
                        </table>
                        <!--  Company Info -->
                        <div class="divWorkOrder">
                            <div class="WorkOrderTopCurve">
                                <p>
                                    <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Company
                                        Information</strong></p>
                            </div>
                            <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                <tr>
                                    <td>
                                        <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr valign="top">
                                                <td class="formTxt" colspan="3" valign="top">
                                                    <div id="showSendMail" runat="server">
                                                        <p class="txtNavigation">
                                                            <strong>An email is sent to the
                                                                <asp:Label ID="lblUser1" runat="server"></asp:Label>
                                                                indicating him/her of the change in Account Status.</strong></p>
                                                        <p class="txtNavigation">
                                                            Can edit the text that is sent to the
                                                            <asp:Label ID="lblUser2" runat="server"></asp:Label>
                                                            via email.<b>Please do not remove text "<accountstatus>" - this will be replaced with
                                                                the status to which users account has been changed.</b><br>
                                                            <asp:TextBox TextMode="MultiLine" onblur="javascript:removeHTML(this.id);" ID="txtMessage"
                                                                TabIndex="11" runat="server" CssClass="formFieldGrey" Height="50" Style="width: 684px;
                                                                height: 50px;"></asp:TextBox></p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr>
                                                <td class="formTxt" width="162" align="left" valign="bottom" height="14">
                                                    Company Name<span class="bodytxtValidationMsg" id="CompanyNameIndicator" runat="server">
                                                        *</span>
                                                    <asp:RequiredFieldValidator ID="rqCompanyName" runat="server" ErrorMessage="Please enter Company Name"
                                                        ForeColor="#EDEDEB" ControlToValidate="txtCompanyName">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td class="formTxt" width="162" align="left" valign="bottom" height="14">
                                                    Business Area
                                                </td>
                                                <td align="left" class="formTxt" id="tdPartnerTitle" runat="server" visible="False">
                                                    Partner <span class="bodytxtValidationMsg" id="PartnerLevelIndicator" runat="server">
                                                        *</span>
                                                    <asp:RequiredFieldValidator ID="rqPartnerLevel" runat="server" ErrorMessage="Please select Partner Level"
                                                        ForeColor="#EDEDEB" ControlToValidate="ddlPartner">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td class="formTxt" align="left" valign="bottom" runat="server">
                                                    <asp:Label ID="lblTypeTxt" runat="server" Text="Type"></asp:Label>&nbsp;
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td align="left" height="24">
                                                    <asp:TextBox ID="txtCompanyName" TabIndex="12" runat="server" CssClass="formField150"></asp:TextBox>
                                                </td>
                                                <td align="left" height="24">
                                                    <asp:DropDownList ID="ddlBusinessArea" TabIndex="12" runat="server" CssClass="formField150">
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="170" align="left" id="tdPartnerValue" runat="server" visible="False">
                                                    <asp:DropDownList ID="ddlPartner" TabIndex="13" runat="server" CssClass="formField150"
                                                        Visible="false">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" height="24">
                                                    <asp:DropDownList ID="ddlType" AutoPostBack="true" TabIndex="14" runat="server" CssClass="formField90"
                                                        OnSelectedIndexChanged="ShowPopup">
                                                    </asp:DropDownList>
                                                    <%--'Poonam added on 19/8/2016 - Task - OA-300 : EA - Make add comment mandatory via pop-up when Deleting or Suspending a company--%>
                                                    <asp:LinkButton ID="lnkDummy" runat="server" Style="display: none" />
                                                    <asp:Button runat="server" ID="hdnBtnAction" CausesValidation="false" Height="0"
                                                        Width="0" BorderWidth="0" Style="visibility: hidden;" />
                                                    <cc1:ModalPopupExtender ID="mdlComment" runat="server" TargetControlID="lnkDummy"
                                                        PopupControlID="pnlComment" OkControlID="" CancelControlID="ancCancelAction"
                                                        BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
                                                    </cc1:ModalPopupExtender>
                                                    <asp:Panel runat="server" ID="pnlComment" Width="340px" CssClass="pnlConfirmWOListingForAction"
                                                        Style="display: none;">
                                                        <div id="div3">
                                                            <div class="divTopActionStyle">
                                                                <b>Comment</b>
                                                            </div>
                                                            <div class="divTopActionStyle">
                                                                Enter Comments&nbsp;<span class="bodytxtValidationMsg">*</span><br />
                                                                <asp:TextBox runat="server" ID="txtComment" Height="50" Width="280" CssClass="bodyTextGreyLeftAligned"
                                                                    TextMode="MultiLine"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtComment"
                                                                    WatermarkText="Enter a new comment here" WatermarkCssClass="bodyTextGreyLeftAligned">
                                                                </cc1:TextBoxWatermarkExtender>
                                                                <br />
                                                                Reason&nbsp;<span class="bodytxtValidationMsg">*</span>
                                                                <br />
                                                                <asp:DropDownList ID="ddlCommentReason" AutoPostBack="false" TabIndex="14" runat="server"
                                                                    CssClass="formField90" Width="227px">
                                                                </asp:DropDownList>
                                                                <br />
                                                            </div>
                                                            <div style="padding-left: 20px; padding-top: 10px; float: left; width: 305px;">
                                                                <div class="divButtonAction" style="width: 85px; float: left; cursor: pointer;">
                                                                    <div class="bottonTopGrey">
                                                                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                                                            height="4" class="btnCorner" />
                                                                    </div>
                                                                    <a class="buttonText cursorHand" onclick='javascript:validate_required_comment("ctl00_ContentPlaceHolder1_UCCompanyProfile1_txtComment","ctl00_ContentPlaceHolder1_UCCompanyProfile1_ddlCommentReason","Please Enter Comment","Please Select Reason")'
                                                                        id="ancSubmitAction">Submit</a>
                                                                    <div class="bottonBottomGrey">
                                                                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                                                            height="4" class="btnCorner" />
                                                                    </div>
                                                                </div>
                                                                <div class="divButtonAction" style="width: 85px; float: left; margin-left: 20px;
                                                                    cursor: pointer;">
                                                                    <div class="bottonTopGrey">
                                                                        <img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                                                            height="4" class="btnCorner" style="display: none" />
                                                                    </div>
                                                                    <a class="buttonText cursorHand" runat="server" id="ancCancelAction" onclick='javascript:refreshPage()'>
                                                                        Cancel</a>
                                                                    <div class="bottonBottomGrey">
                                                                        <img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4"
                                                                            height="4" class="btnCorner" style="display: none" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:CheckBox CssClass="formTxt" ID="chkSendMail" Text="Send Mail" runat="server"
                                                        AutoPostBack="true" Checked="true"></asp:CheckBox>&nbsp;
                                                    <asp:CheckBox CssClass="formTxt" ID="chkIsTestAccount" Text="Is Test Account" runat="server">
                                                    </asp:CheckBox>&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr valign="top">
                                                <td class="formTxt" align="left" width="162" height="14">
                                                    Company Registration No.
                                                </td>
                                                <td class="formTxt" align="left" width="324" height="14">
                                                    URL
                                                </td>
                                                <td class="formTxt" align="left" height="14">
                                                    Year Business Started
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td align="left" height="24">
                                                    <asp:TextBox ID="txtCompanyRegNo" TabIndex="15" runat="server" CssClass="formField150"></asp:TextBox>
                                                </td>
                                                <td align="left" height="24">
                                                    <asp:TextBox ID="txtCompanyURL" TabIndex="16" runat="server" CssClass="formField310">http://</asp:TextBox>
                                                </td>
                                                <td class="formTxt" align="left" height="14">
                                                    <asp:TextBox ID="txtBusinessStarted" TabIndex="17" runat="server" CssClass="formField width150"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr valign="top">
                                                <td class="formTxt" align="left" width="162" height="14">
                                                    No. of Employees
                                                </td>
                                                <td class="formTxt" align="left" width="164" height="14">
                                                    No. of Locations
                                                </td>
                                                <td width="160" height="14" align="left" class="formTxt">
                                                    <span runat="server" id="spIndustry">Industry</span>&nbsp;
                                                </td>
                                                <td align="left" class="formTxt">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td height="24" align="left">
                                                    <asp:DropDownList ID="ddlNoOfEmployees" TabIndex="18" runat="server" CssClass="formField150">
                                                    </asp:DropDownList>
                                                </td>
                                                <td height="24" align="left">
                                                    <asp:TextBox ID="txtNoOfLocations" TabIndex="19" runat="server" CssClass="formField150"></asp:TextBox>
                                                </td>
                                                <td height="24">
                                                    <asp:DropDownList ID="ddlIndustry" TabIndex="20" runat="server" CssClass="formField150">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <table height="40" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr valign="bottom">
                                                <td class="formTxt" align="left" width="162" height="16">
                                                    No. of Engineers
                                                    <asp:RangeValidator ID="RangeNoOfEngg" ControlToValidate="txtNoOfEngineers" MinimumValue="1"
                                                        MaximumValue="1000" Type="Integer" ErrorMessage="No. of Engineers must be from 1 to 1000"
                                                        runat="server" ForeColor="#EDEDEB">*</asp:RangeValidator>
                                                </td>
                                                <td width="166" height="14" align="left" class="formTxt">
                                                    What is your standard day rate?
                                                    <asp:RangeValidator ID="RangeStdDayRate" ControlToValidate="txtStdDayRate" MinimumValue="1"
                                                        MaximumValue="10000" Type="Integer" ErrorMessage="Standard day rate must be from 1 to 10000"
                                                        runat="server" ForeColor="#EDEDEB">*</asp:RangeValidator>
                                                </td>
                                                <td align="left" class="formTxt">
                                                    Do you ever have requirements you can't fulfil in-house?
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td width="161" height="24" align="left">
                                                    <asp:TextBox ID="txtNoOfEngineers" TabIndex="19" runat="server" CssClass="formField width150"></asp:TextBox>
                                                </td>
                                                <td width="166" height="24" align="left">
                                                    <asp:TextBox ID="txtStdDayRate" TabIndex="19" runat="server" CssClass="formField width150"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:RadioButton class="radformFieldPadT0" ID="FulfilInhouseYes" TabIndex="24" runat="server"
                                                        GroupName="rdFulfilInhouse" Text="Yes" TextAlign="right"></asp:RadioButton>
                                                    <asp:RadioButton class="radformFieldPadT0" ID="FulfilInhouseNo" TabIndex="25" runat="server"
                                                        GroupName="rdFulfilInhouse" Text="No" TextAlign="right"></asp:RadioButton>
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr valign="bottom" class="formTxt">
                                                <%--<td width="162" height="18" class="formTxt">Can you source spare parts? </td>--%>
                                                <td width="166" height="18" align="left" class="formTxt">
                                                    Are you VAT Registered?
                                                </td>
                                                <td class="formTxt" width="170">
                                                    VAT Registration number
                                                </td>
                                                <td align="left" class="formTxt">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <%--<td height="20" align="left" valign="top"><asp:RadioButton class="radformFieldPadT0" ID="SourcePartYes" TabIndex="24" runat="server" GroupName="rdSparePart"
																												Text="Yes" TextAlign="right"></asp:RadioButton>
															  <asp:RadioButton class="radformFieldPadT0" ID="SourcePartNo" TabIndex="25" runat="server" GroupName="rdSparePart"
																												Text="No" TextAlign="right"></asp:RadioButton></td>--%>
                                                <td valign="top" height="20">
                                                    <asp:RadioButton class="radformFieldPadT0" ID="RegisterYes" TabIndex="26" runat="server"
                                                        GroupName="rdVATRegister" Text="Yes" TextAlign="right"></asp:RadioButton>
                                                    <asp:RadioButton class="radformFieldPadT0" ID="RegisterNo" TabIndex="27" runat="server"
                                                        GroupName="rdVATRegister" Text="No" TextAlign="right"></asp:RadioButton>
                                                </td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtVATRegistrationNumber" TabIndex="28" runat="server" CssClass="formField width150"></asp:TextBox>
                                                </td>
                                                <td align="left">
                                                    <asp:CheckBox ID="chkbxAcceptMultipleWO" runat="server" Text="Accept Multiple Work Orders"
                                                        Checked="false" Visible="false" CssClass="chkpadR"></asp:CheckBox>
                                            </tr>
                                            <tr>
                                                <td width="320" height="25" align="left" valign="Top">
                                                    <uc2:UCFileUpload ID="UCFileUpload4" runat="server" Control="UCCompanyProfile1_UCFileUpload4"
                                                        Type="Vat" AttachmentForSource="Vat" ShowNewAttach="True" ShowUpload="True" MaxFiles="1">
                                                    </uc2:UCFileUpload>
                                                </td>
                                            </tr>
                                        </table>
                                        <table height="40" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr valign="bottom">
                                                <td class="formTxt" align="left" height="18" width="170">
                                                    How did they heard about us?
                                                </td>
                                                <td class="formTxt" align="left" height="18" id="tdHrd" align="left">
                                                    <asp:Label ID="lblHrdfrom" runat="server" Text="Heard From"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td>
                                                    <asp:DropDownList ID="ddlHrdAbt" Enabled="false" CssClass="formField width150" runat="server"
                                                        DataTextField="StandardValue" DataValueField="StandardValue" Height="18" Width="150"
                                                        onkeydown='javascript:SetHrdAboutUs(this.id);' onkeyup='javascript:SetHrdAboutUs(this.id);'
                                                        onchange='javascript:SetHrdAboutUs(this.id);'>
                                                    </asp:DropDownList>
                                                    <br />
                                                </td>
                                                <td valign="top">
                                                    <div runat="server" id="pnlHrdAbtUs" style="height: 12px; line-height: 14px; float: left;">
                                                        <asp:TextBox ID="txtHrdAbtUs" Enabled="false" runat="server" CssClass="formTxt" Width="150"
                                                            Style="margin-top: 0px;" MaxLength="30"></asp:TextBox></div>
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="tblProfile" runat="server" height="40" cellspacing="0" cellpadding="0"
                                            width="100%" border="0">
                                            <tr valign="bottom">
                                                <td class="formTxt" align="left" height="18">
                                                    Profile
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td align="left">
                                                    <asp:TextBox ID="txtProfile" TabIndex="29" runat="server" CssClass="formField width686height60"
                                                        TextMode="MultiLine" onblur="javascript:removeHTML(this.id);"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div class="WorkOrderBottomCurve">
                                <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                                    style="display: none" /></div>
                        </div>
                        <!--  Bank Details -->
                        <div class="divWorkOrder">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Bank
                                    Details</strong></div>
                            <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlbankDetails" runat="server">
                                            <table cellspacing="0" cellpadding="0" border="0">
                                                <tr valign="bottom" class="formTxt">
                                                    <td width="162" height="25" class="formTxt">
                                                        Sort Code
                                                        <%--Poonam added on 29/7/2015 - Task - OA-42 : Bank account Name should be mandatory in Bank details section for all the suppliers--%>
                                                        <span class="bodytxtValidationMsg" id="SortCodeIndicator" runat="server">*</span>
                                                        <asp:RequiredFieldValidator ID="rqSortCode" runat="server" ErrorMessage="Please enter Sort Code"
                                                            ForeColor="#EDEDEB" ControlToValidate="txtSortCode">*</asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ControlToValidate="txtSortCode" ErrorMessage="Please enter Sort Code in this format: 60-30-06"
                                                            ID="regSortCode" runat="server" ValidationExpression="^[0-9]{2}[-][0-9]{2}[-][0-9]{2}$"
                                                            ForeColor="#EDEDEB">*</asp:RegularExpressionValidator>
                                                    </td>
                                                    <td width="162" height="25" align="left" class="formTxt">
                                                        Account Number
                                                        <%--Poonam added on 29/7/2015 - Task - OA-42 : Bank account Name should be mandatory in Bank details section for all the suppliers--%>
                                                        <span class="bodytxtValidationMsg" id="AccountNumberIndicator" runat="server">*</span>
                                                        <asp:RequiredFieldValidator ID="rqAccountNumber" runat="server" ErrorMessage="Please enter Account Number"
                                                            ForeColor="#EDEDEB" ControlToValidate="txtAccountNo">*</asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ControlToValidate="txtAccountNo" ErrorMessage="Account Number must be 8 characters"
                                                            ForeColor="#EDEDEB" ID="rgAccNo" runat="server" ValidationExpression="[0-9a-zA-Z]{8,8}">*</asp:RegularExpressionValidator>
                                                    </td>
                                                    <td id="tdBankLabel" runat="server" class="formTxt" width="162">
                                                        Bank
                                                    </td>
                                                    <td class="formTxt">
                                                        Account Name
                                                        <%--Poonam added on 29/7/2015 - Task - OA-42 : Bank account Name should be mandatory in Bank details section for all the suppliers--%>
                                                        <span class="bodytxtValidationMsg" id="txtAccountNameIndicator" runat="server">*</span>
                                                        <asp:RequiredFieldValidator ID="rqtxtAccountName" runat="server" ErrorMessage="Please enter Account Name"
                                                            ForeColor="#EDEDEB" ControlToValidate="txtAccountName">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="20" align="left" valign="top">
                                                        <asp:TextBox ID="txtSortCode" TabIndex="30" runat="server" CssClass="formField width150"></asp:TextBox>
                                                    </td>
                                                    <td valign="top" height="20">
                                                        <asp:TextBox MaxLength="8" ID="txtAccountNo" TabIndex="31" runat="server" CssClass="formField width150"></asp:TextBox>
                                                    </td>
                                                    <td id="tdBankBox" runat="server" valign="top">
                                                        <asp:TextBox ID="txtKontoName" MaxLength="18" TabIndex="33" runat="server" CssClass="formField width150"></asp:TextBox>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:TextBox ID="txtAccountName" MaxLength="18" TabIndex="32" runat="server" CssClass="formField width150"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table cellspacing="0" cellpadding="0" border="0">
                                                <tr valign="bottom" class="formTxt">
                                                    <td  height="25" class="formTxt" colspan="4">
                                                        <asp:Label ID="Label1" runat="server" Text="Orderwork Payment Terms" ForeColor="black" Font-Bold="True"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr valign="bottom" class="formTxt">
                                                    <td width="162" height="20" class="formTxt">
                                                        Payment Terms
                                                    </td>
                                                    <td width="162" height="20" class="formTxt">
                                                        Discount&nbsp;%<br />
                                                    </td>
                                                    <td width="160" height="20" align="left" class="formTxt">
                                                        Credit Terms
                                                    </td>
                                                    <td class="formTxt">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="24" align="left" valign="top">
                                                        <asp:DropDownList ID="ddlSuppDiscountPaymentsTermsOW" Enabled="True" TabIndex="33" 
                                                            runat="server" CssClass="formField width150" AutoPostBack="true" >
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td height="24" align="left" valign="top">
                                                        <asp:TextBox ID="txtDiscountOW" runat="server" Enabled="false" MaxLength="5" CssClass="formField width150"
                                                            onblur='FormatttedAmount(this.id); ValidateDiscount("UCAccountSettings1_txtDiscountOW");'
                                                            Text="0.00" TabIndex="34"></asp:TextBox>
                                                    </td>
                                                    <td valign="top" height="24">
                                                        <asp:DropDownList ID="ddlBuyerPaymentsTerms" TabIndex="35" runat="server" CssClass="formField width150">
                                                        </asp:DropDownList>
                                                        
                                                    </td>
                                                    <td valign="top">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr valign="bottom" class="formTxt">
                                                    <td  height="25" class="formTxt" colspan="4">
                                                        <asp:Label ID="Label2" runat="server" Text="Empowered Payment Terms" ForeColor="black" Font-Bold="True"></asp:Label>
                                                    </td>
                                                </tr>
                                                 <tr valign="bottom" class="formTxt">
                                                    <td width="162" height="20" class="formTxt">
                                                        Payment Terms
                                                    </td>
                                                    <td width="162" height="20" class="formTxt">
                                                        Discount&nbsp;%<br />
                                                    </td>
                                                    <td  class="formTxt">
                                                        &nbsp;
                                                    </td>
                                                    <td class="formTxt">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="24" align="left" valign="top">
                                                        <asp:DropDownList ID="ddlSuppDiscountPaymentsTermsEM" Enabled="True" TabIndex="33" 
                                                            runat="server" CssClass="formField width150" AutoPostBack="true" >
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td height="24" align="left" valign="top">
                                                        <asp:TextBox ID="txtDiscountEM" runat="server" Enabled="false" MaxLength="5" CssClass="formField width150"
                                                            onblur='FormatttedAmount(this.id); ValidateDiscount("UCAccountSettings1_txtDiscountEM");'
                                                            Text="0.00" TabIndex="34"></asp:TextBox>
                                                    </td>
                                                    <td valign="top" >
                                                        &nbsp;
                                                        
                                                    </td>
                                                    <td valign="top">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                  <tr valign="bottom" class="formTxt">
                                                <td  height="25" class="formTxt" colspan="4">
                                                    <asp:Label ID="lblNote" runat="server" Text="Note: Withdrawal cut-off is on Tuesday midday to allow payment on Friday close of play" ForeColor="Red" ></asp:Label>
                                                </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <div class="WorkOrderBottomCurve">
                                <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                                    style="display: none" /></div>
                        </div>
                        <!-- Insurance Details -->
                        <div class="divWorkOrder">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Insurance
                                    Details</strong></div>
                            <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlinsurance" runat="server">
                                            <div id="divInsurance" class="divAttachment">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr valign="top">
                                                        <td class="padRight20Left20 formTxt" align="left">
                                                            <p>
                                                                As part of our reference checking process you are required to provide us with a
                                                                scanned copy of each insurance type you posses in order i.e. JPEG or PDF format.
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="padRight20Left20">
                                                            <table width="100%%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td class="smallText borderWhite1R">
                                                                        <span class="formTxt">
                                                                            <asp:CustomValidator ID="AttachmentValidatorInsurance" runat="server" ErrorMessage="Only PDF, JPEG and JPG files can be uploaded.">*</asp:CustomValidator>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="217" height="23" valign="top" class="formTxt">
                                                                        <asp:CheckBox ID="chkInsurance4" runat="server" TabIndex="35" Text="Employers Liability"
                                                                            class="chkpadR"></asp:CheckBox>
                                                                    </td>
                                                                    <td width="217" height="23" valign="top" class="formTxt">
                                                                        <asp:CheckBox ID="chkInsurance5" runat="server" TabIndex="36" Text="Public Liability"
                                                                            class="chkpadR"></asp:CheckBox>
                                                                    </td>
                                                                    <td height="23" valign="top" class="formTxt">
                                                                        <asp:CheckBox ID="chkInsurance6" runat="server" TabIndex="37" Text="Professional Indemnity"
                                                                            class="chkpadR"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- VALIDATION DONE -->
                                                            <!--  	style="visibility:hidden; overflow:hidden; position:absolute"	 -->
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left" width="220" height="24">
                                                                        <asp:TextBox ID="txtEmployeeLiabilityDate" runat="server" TabIndex="38" CssClass="formField width177"
                                                                            Text="Enter expiry date" onfocus="clearExpiryDate(this)" onblur="fillExpiryDate(this)"
                                                                            OnTextChanged="CheckChkBoxOnValueEntered"></asp:TextBox>
                                                                        <img alt="Click to Select" src="Images/calendar.gif" id="btnEmpLDate" style="cursor: pointer;
                                                                            vertical-align: middle;" />
                                                                        <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnEmpLDate"
                                                                            TargetControlID="txtEmployeeLiabilityDate" ID="calEmpLDate" runat="server">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ID="regEmployeeLiabilityDate" ControlToValidate="txtEmployeeLiabilityDate"
                                                                            ErrorMessage="Employers Liability Date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB"
                                                                            ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                                            runat="server">*</asp:RegularExpressionValidator>
                                                                    </td>
                                                                    <td align="left" width="220" height="24">
                                                                        <asp:TextBox ID="txtPublicLiabilityDate" runat="server" TabIndex="40" CssClass="formField width177"
                                                                            Text="Enter expiry date" onfocus="clearExpiryDate(this)" onblur="fillExpiryDate(this)"
                                                                            OnTextChanged="CheckChkBoxOnValueEntered"></asp:TextBox>
                                                                        <img alt="Click to Select" src="Images/calendar.gif" id="btnPubLDate" style="cursor: pointer;
                                                                            vertical-align: middle;" />
                                                                        <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnPubLDate"
                                                                            TargetControlID="txtPublicLiabilityDate" ID="calPubLDate" runat="server">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ID="regPublicLiabilityDate" ControlToValidate="txtPublicLiabilityDate"
                                                                            ErrorMessage="Public Liability Date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB"
                                                                            ValidationExpression="(^(((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3}))$)"
                                                                            runat="server">*</asp:RegularExpressionValidator>
                                                                    </td>
                                                                    <td align="left" height="24">
                                                                        <asp:TextBox ID="txtProfessionalIndemnityDate" runat="server" TabIndex="42" CssClass="formField width177"
                                                                            Text="Enter expiry date" onfocus="clearExpiryDate(this)" onblur="fillExpiryDate(this)"
                                                                            OnTextChanged="CheckChkBoxOnValueEntered"></asp:TextBox>
                                                                        <img alt="Click to Select" src="Images/calendar.gif" id="btnIndemDate" style="cursor: pointer;
                                                                            vertical-align: middle;" />
                                                                        <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox" PopupButtonID="btnIndemDate"
                                                                            TargetControlID="txtProfessionalIndemnityDate" ID="calIndemDate" runat="server">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ID="regProfessionalIndemnityDate" ControlToValidate="txtProfessionalIndemnityDate"
                                                                            ErrorMessage="Professional Liability Date should be in DD/MM/YYYY Format" ForeColor="#EDEDEB"
                                                                            ValidationExpression="(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)"
                                                                            runat="server">*</asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="220" align="left" valign="middle">
                                                                        <asp:TextBox ID="txtCoverAmountEmpLiabiity" runat="server" TabIndex="43" CssClass="formField width200"
                                                                            OnTextChanged="CheckChkBoxOnValueEntered"></asp:TextBox>
                                                                    </td>
                                                                    <cc1:TextBoxWatermarkExtender TargetControlID="txtCoverAmountEmpLiabiity" ID="TextBoxWatermarkExtender4"
                                                                        WatermarkText="Enter cover amount" runat="server" />
                                                                    <td width="220" align="left" valign="middle">
                                                                        <asp:TextBox ID="txtCoverAmountpubLiability" runat="server" TabIndex="44" CssClass="formField width200"
                                                                            OnTextChanged="CheckChkBoxOnValueEntered"></asp:TextBox>
                                                                    </td>
                                                                    <cc1:TextBoxWatermarkExtender TargetControlID="txtCoverAmountpubLiability" ID="TextBoxWatermarkExtender5"
                                                                        WatermarkText="Enter cover amount" runat="server" />
                                                                    <td align="left" valign="middle">
                                                                        <asp:TextBox ID="txtCoverAmountProfLiabity" runat="server" TabIndex="45" CssClass="formField width200"
                                                                            OnTextChanged="CheckChkBoxOnValueEntered"></asp:TextBox>
                                                                    </td>
                                                                    <cc1:TextBoxWatermarkExtender TargetControlID="txtCoverAmountProfLiabity" ID="TextBoxWatermarkExtender6"
                                                                        WatermarkText="Enter cover amount" runat="server" />
                                                                </tr>
                                                            </table>
                                                            <table width="100%" runat="server" id="tblInsurAttach" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td width="220" height="25" align="left" valign="Top">
                                                                        <uc2:UCFileUpload ID="UCFileUpload1" runat="server" Control="UCCompanyProfile1_UCFileUpload1"
                                                                            Type="Insurance" AttachmentForSource="EmployersLiability" ShowNewAttach="True"
                                                                            ShowUpload="True" MaxFiles="1"></uc2:UCFileUpload>
                                                                    </td>
                                                                    <td width="220" align="left" valign="Top">
                                                                        <uc2:UCFileUpload ID="UCFileUpload2" runat="server" Control="UCCompanyProfile1_UCFileUpload2"
                                                                            Type="Insurance" AttachmentForSource="PublicLiability" ShowNewAttach="True" ShowUpload="True"
                                                                            MaxFiles="1"></uc2:UCFileUpload>
                                                                    </td>
                                                                    <td align="left" valign="Top">
                                                                        <uc2:UCFileUpload ID="UCFileUpload3" runat="server" Control="UCCompanyProfile1_UCFileUpload3"
                                                                            Type="Insurance" AttachmentForSource="ProfessionalIndemnity" ShowNewAttach="True"
                                                                            ShowUpload="True" MaxFiles="1"></uc2:UCFileUpload>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="220" height="25" align="left" valign="bottom">
                                                            <a class='footerTxtSelected' visible="false" id="lnkEmpLiabInsur" runat="server"
                                                                target='_blank'></a>&nbsp;
                                                        </td>
                                                        <td width="220" align="left" valign="bottom">
                                                            <a class='footerTxtSelected' visible="false" id="lnkPubLiabInsur" runat="server"
                                                                target='_blank'></a>&nbsp;
                                                        </td>
                                                        <td align="left" valign="bottom">
                                                            <a class='footerTxtSelected' visible="false" id="lnkProfIndemInsur" runat="server"
                                                                target='_blank'></a>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <div class="WorkOrderBottomCurve">
                                <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                                    style="display: none" /></div>
                        </div>
                        <!-- Area of Expertise	-->
                        <div class="divWorkOrder">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Area
                                    of Expertise</strong></div>
                            <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%" border="0" id="tblAreaOfExpertise"
                                            runat="server">
                                            <tr valign="top">
                                                <td align="left">
                                                    <div id="divAOE" class="divAttachment" runat="server">
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td class="padRight20Left20 formTxt">
                                                                    <%@ register tagprefix="uc1" tagname="UCAOE" src="~/UserControls/Admin/UCTableAOE.ascx" %>
                                                                    <uc1:UCAOE ID="UCAOE1" runat="server"></uc1:UCAOE>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div class="WorkOrderBottomCurve">
                                <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                                    style="display: none" /></div>
                        </div>
                        <!-- Vendor Accreditations/Skills	-->
                        <div class="divWorkOrder">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Technical
                                    Expertise / Accreditations & keywords</strong></div>
                            <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlCertQual" runat="server">
                                            <table cellspacing="0" cellpadding="0" width="100%" border="0" id="tblCertQual" runat="server">
                                                <tr valign="top">
                                                    <td align="left">
                                                        <div class="divAttachment" runat="server">
                                                            <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="padRight20Left20 formTxt">
                                                                        <%@ register tagprefix="uc2" tagname="UCAdminAccreds" src="~/UserControls/Admin/UCAdminAccreditations.ascx" %>
                                                                        <uc2:UCAdminAccreds ID="UCAdminAccreds" runat="server"></uc2:UCAdminAccreds>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <%--<tr valign="top">
										  <td class="formTxt" align="left" height="14">Accreditations</td>
										</tr>
										<tr valign="top">
										  <td align="left" height="66"><asp:TextBox ID="txtAccreditations" onChange="validateMaxLen(this.id, '500', 'Accreditations can have max 500 characters.')"  TabIndex="46" runat="server" CssClass="formField width686height60" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"></asp:TextBox></td>
										</tr>--%>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <div class="WorkOrderBottomCurve">
                                <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                                    style="display: none" /></div>
                        </div>
                        <%--<!-- Company preferences	-->
		  <div class="divWorkOrder" >
				<div class="WorkOrderTopCurve"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/><strong>Company preferences</strong></div>	
					<table width="100%" cellspacing="0" cellpadding="9" border="0">
						<tr>
							<td>
								 <div id="divAccount" runat="server">
									  
									  <table cellspacing="0" cellpadding="0" width="100%" border="0" height="12">
										<tr>
										  <td valign="top" align="left"><asp:Label ID="lblError1" runat="server" CssClass="bodytxtValidationMsg"></asp:Label></td>
										</tr>
									  </table>
									  <asp:Panel ID="pnlSupplierPref" runat="server">
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
										  <tr valign="middle">
											<td width="263" height="14" valign="top" class="formTxt">Minimum Work Order Value willing to accept:</td>
											<td width="0" height="14" align="left" valign="bottom" class="formTxt">&pound;
												<asp:TextBox runat="server" TabIndex="47" style="text-align:right;width:50px" ID="txtMinWO" 
												                        CssClass="formField width150" Width="50" Text="0"></asp:TextBox>																												
                                            </td>
										  </tr>
										</table>
										<table cellspacing="0" cellpadding="0" width="474" border="0">
										  <tr valign="top">
											<td class="formTxt" align="left" width="202" height="14">Regions that you cover </td>
											<td class="formTxt" align="left" height="14">&nbsp;</td>
											<td class="formTxt" align="left" width="202" height="14">&nbsp;</td>
										  </tr>
										</table>
										<table cellspacing="0" cellpadding="0" width="474" border="0">
										  <tr valign="top">
											<td><uc3:UCDualListBox id="UCRegions" runat="server"></uc3:UCDualListBox> </td>
										  </tr>
										</table>
									  </asp:Panel>
									  <table cellspacing="0" cellpadding="0" width="100%" border="0">
										<tr>
										  <td height="14" colspan="5" valign="top" class="formTxt">&nbsp;</td>
										</tr>
										<tr>
										  <td height="14" colspan="5" valign="top" class="formTxt">List Companies with whom you do not wish to work with</td>
										</tr>
										<tr valign="bottom">
										  <td width="180" height="24"><asp:TextBox TabIndex="48" runat="server" ID="txtClient1" CssClass="formField width150"></asp:TextBox></td>
										  <td width="180" height="24"><asp:TextBox TabIndex="49" runat="server" ID="txtClient2" CssClass="formField width150"></asp:TextBox></td>
										  <td width="180" height="24"><asp:TextBox TabIndex="50" runat="server" ID="txtClient3" CssClass="formField width150"></asp:TextBox></td>
										  <td height="24"><asp:TextBox TabIndex="51" runat="server" ID="txtClient4" CssClass="formField width150"></asp:TextBox></td>
										</tr>
										<tr valign="bottom">
										  <td width="162" height="24"><asp:TextBox TabIndex="52" runat="server" ID="txtClient5" CssClass="formField width150"></asp:TextBox></td>
										  <td height="24"><asp:TextBox TabIndex="53" runat="server" ID="txtClient6" CssClass="formField width150"></asp:TextBox></td>
										  <td width="162" height="24"><asp:TextBox TabIndex="54" runat="server" ID="txtClient7" CssClass="formField width150"></asp:TextBox></td>
										  <td height="24"><asp:TextBox TabIndex="55" runat="server" ID="txtClient8" CssClass="formField width150"></asp:TextBox></td>
										</tr>
										
									  </table>
									  
									 </div>
							</td>
						</tr>
					</table>
					
					
		 		<div class="WorkOrderBottomCurve"><img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		  </div>--%>
                        <!-- Other Details	-->
                        <asp:Panel runat="server" ID="pnlOtherDetails">
                            <div class="divWorkOrder">
                                <div class="WorkOrderTopCurve">
                                    <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Other
                                        Details</strong></div>
                                <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                <tr valign="bottom" class="formTxt">
                                                    <td width="162" height="24" class="formTxt">
                                                        DBS Checked
                                                    </td>
                                                    <td width="160" height="24" class="formTxt">
                                                        UK Security Clearance
                                                    </td>
                                                    <td width="160" height="24" class="formTxt">
                                                        CSCS
                                                    </td>
                                                    <%--'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch--%>
                                                    <td width="160" height="24" class="formTxt">
                                                        Excel Partner
                                                    </td>
                                                    <td width="160" height="24" class="formTxt">
                                                        EPCP
                                                    </td>
                                                </tr>
                                                <tr align="left">
                                                    <td height="20" valign="top" tooltip="This read-only information is based on the user settings"
                                                        title="This read-only information is based on the user settings">
                                                        <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedEnhanced" runat="server"
                                                            GroupName="CRBChecked" Text="Enhanced" TextAlign="right"></asp:RadioButton>
                                                        <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedYes" TabIndex="56" runat="server"
                                                            GroupName="CRBChecked" Text="Basic" TextAlign="right"></asp:RadioButton>
                                                        <asp:RadioButton class="radformFieldPadT0" ID="CRBCheckedNo" TabIndex="57" runat="server"
                                                            GroupName="CRBChecked" Text="No" TextAlign="right"></asp:RadioButton>
                                                    </td>
                                                    <td valign="top" height="20" tooltip="This read-only information is based on the user settings"
                                                        title="This read-only information is based on the user settings">
                                                        <asp:RadioButton class="radformFieldPadT0" ID="UKSecurityClearanceYes" TabIndex="58"
                                                            runat="server" GroupName="UKSecurityClearance" Text="Yes" TextAlign="right">
                                                        </asp:RadioButton>
                                                        <asp:RadioButton class="radformFieldPadT0" ID="UKSecurityClearanceNo" TabIndex="59"
                                                            runat="server" GroupName="UKSecurityClearance" Text="No" TextAlign="right"></asp:RadioButton>
                                                    </td>
                                                    <td valign="top" tooltip="This read-only information is based on the user settings"
                                                        title="This read-only information is based on the user settings">
                                                        <asp:RadioButton class="radformFieldPadT0" ID="CSCSYes" TabIndex="59" runat="server"
                                                            GroupName="CSCS" Text="Yes" TextAlign="right"></asp:RadioButton>
                                                        <asp:RadioButton class="radformFieldPadT0" ID="CSCSNo" TabIndex="60" runat="server"
                                                            GroupName="CSCS" Text="No" TextAlign="right"></asp:RadioButton>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:RadioButton class="radformFieldPadT0" ID="ExcelPartnerYes" TabIndex="59" runat="server"
                                                            GroupName="ExcelPartner" Text="Yes" TextAlign="right"></asp:RadioButton>
                                                        <asp:RadioButton class="radformFieldPadT0" ID="ExcelPartnerNo" TabIndex="60" runat="server"
                                                            GroupName="ExcelPartner" Text="No" TextAlign="right"></asp:RadioButton>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:RadioButton class="radformFieldPadT0" ID="EPCPYes" TabIndex="59" runat="server"
                                                            GroupName="EPCP" Text="Yes" TextAlign="right"></asp:RadioButton>
                                                        <asp:RadioButton class="radformFieldPadT0" ID="EPCPNo" TabIndex="60" runat="server"
                                                            GroupName="EPCP" Text="No" TextAlign="right"></asp:RadioButton>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%--<table cellspacing="0" cellpadding="0" width="474" border="0">
								  <tr valign="top" class="formTxt">
									<td align="left" width="162" height="14" class="formTxt">&nbsp;
									</td>
									  <td align="left" width="180" height="14" class="formTxt">&nbsp;
									</td>
									  <td align="left" height="14" class="formTxt">&nbsp;
									</td>
									
								  </tr>
								  <tr valign="top" class="formTxt">
									
									<td align="left" width="162" height="14" class="formTxt">  ISO Registration No.
									   </td>
									<td align="left" width="180" height="14"  class="formTxt">  Investors in People Registration No.
										</td>
									<td class="formTxt">&nbsp;  </td>
								  </tr>
								  <tr>
									
									<td height="20" align="left" valign="top"><asp:TextBox ID="txtISORegNo" TabIndex="60" runat="server" CssClass="formField width150"></asp:TextBox></td>
									<td valign="top" align="left" height="20"><asp:TextBox MaxLength="8" ID="txtPeopleRegNo" TabIndex="61" runat="server" CssClass="formField width150"></asp:TextBox></td>
									<td valign="top">&nbsp;</td>
								  </tr>
								</table>--%>
                                            <table height="18" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                <tr valign="top">
                                                    <td class="formTxt" align="left" height="14">
                                                        Other Information
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td align="left" height="66">
                                                        <asp:TextBox onChange="validateMaxLen(this.id, '500', 'Other Information can have max 500 characters.')"
                                                            ID="txtOtherInformation" TabIndex="62" runat="server" CssClass="formField width686height60"
                                                            TextMode="MultiLine" onblur="javascript:removeHTML(this.id);"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <div class="WorkOrderBottomCurve">
                                    <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                                        style="display: none" /></div>
                            </div>
                        </asp:Panel>
                        <!--	Attachments	-->
                        <div class="divWorkOrder">
                            <div class="WorkOrderTopCurve">
                                <img src="Images/Curves/blue_LeftCurve.gif" alt="" align="absmiddle" /><strong>Attachments</strong></div>
                            <table width="100%" cellspacing="0" cellpadding="9" border="0">
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlAttachment" runat="server">
                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td valign="top" align="left" height="18" class="formTxt">
                                                        <div id="Div2" class="divAttachment" runat="server">
                                                            <uc2:UCFileUpload ID="UCFileUpload6" runat="server" Control="UCCompanyProfile1_UCFileUpload6"
                                                                ExistAttachmentSource="CompanyProfile" Type="Company" AttachmentForSource="CompanyProfile"
                                                                UploadCount="1" ShowExistAttach="False" ShowNewAttach="True" ShowUpload="True"
                                                                MaxFiles="12"></uc2:UCFileUpload>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <div class="WorkOrderBottomCurve">
                                <img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner"
                                    style="display: none" /></div>
                        </div>
                        <asp:Panel ID="pnlSupplierInfo" runat="server">
                        </asp:Panel>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td align="left" class="HeadingRed">
                                    &nbsp;
                                </td>
                                <td align="left" class="padTop17">
                                    <table id="tblSaveBottom" runat="server" cellspacing="0" cellpadding="0" width="100%"
                                        border="0" style="padding: 0px 0px 16px 0px; margin: 0px;">
                                        <tr>
                                            <td align="right" valign="top">
                                                <table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                                                    <tr>
                                                        <td width="5">
                                                            <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                        </td>
                                                        <td width="110" class="txtBtnImage">
                                                            <asp:LinkButton ID="btnSaveBottom" CausesValidation="false" OnClientClick="javascript:callSetFocusValidation('UCCompanyProfile1')"
                                                                CssClass="txtListing" runat="server" TabIndex="63"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:LinkButton>
                                                        </td>
                                                        <td width="5">
                                                            <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="10">
                                            </td>
                                            <td width="125" align="right" valign="top" style="padding: 0px 0px 0px 0px;">
                                                <table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                                                    <tr>
                                                        <td width="5">
                                                            <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5">
                                                        </td>
                                                        <td width="115" class="txtBtnImage">
                                                            <asp:LinkButton ID="btnResetBottom" CausesValidation="false" runat="server" TabIndex="64"
                                                                class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton>
                                                        </td>
                                                        <td width="5">
                                                            <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
            <!-- Confirm Action panel -->
            <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="padTop50">
                    <tr>
                        <td width="350" align="right">
                            <table cellspacing="0" cellpadding="0" bgcolor="#F0F0F0" border="0">
                                <tr>
                                    <td width="5">
                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" />
                                    </td>
                                    <td class="txtBtnImage">
                                        <asp:LinkButton ID="btnConfirm" runat="server" CausesValidation="false" class="txtListing"
                                            TabIndex="65"> <img src="Images/Icons/Icon-Confirm.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Confirm&nbsp;</asp:LinkButton>
                                    </td>
                                    <td width="5">
                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="10">
                            &nbsp;
                        </td>
                        <td align="left">
                            <table border="0" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
                                <tr>
                                    <td width="5">
                                        <img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" />
                                    </td>
                                    <td align="center" class="txtBtnImage">
                                        <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" class="txtListing"
                                            TabIndex="66"> <img src="Images/Icons/Icon-Cancel.gif" width="12" height="12" hspace="3" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
                                    </td>
                                    <td width="5">
                                        <img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSaveTop" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnResetTop" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSaveBottom" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnResetBottom" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ddlSuppDiscountPaymentsTermsOW" EventName="SelectedIndexChanged"  />
            <asp:AsyncPostBackTrigger ControlID="ddlSuppDiscountPaymentsTermsEM" EventName="SelectedIndexChanged"  />
            
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<%--'Poonam added on 19/8/2016 - Task - OA-300 : EA - Make add comment mandatory via pop-up when Deleting or Suspending a company--%>
<script type="text/javascript">
    function validate_required_comment(field, ddlfield, alerttxt, ddlalerttext) {
        var text = document.getElementById(field).value;
        var ddl = document.getElementById(ddlfield);
        var ddltext = ddl.options[ddl.selectedIndex].text;
        if (text == null || text == "" || text == "Enter a new comment here") {
            alert(alerttxt);
        }
        else if (ddltext == "Select Comment Reason") {
            alert('Select Comment Reason');
        }
        else {

            document.getElementById("ctl00_ContentPlaceHolder1_UCCompanyProfile1_hdnBtnAction").click();
        }
    }
    function refreshPage() {
        window.location.reload();
    }
</script>
<asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePnlCompanyProfile"
    runat="server">
    <ProgressTemplate>
        <div>
            <img align="middle" src="Images/indicator.gif" />
            Loading ...
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="PnlCompanyProfile"
    CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />
