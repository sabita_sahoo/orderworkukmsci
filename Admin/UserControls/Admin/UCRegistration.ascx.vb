Public Partial Class UCRegistration
    Inherits System.Web.UI.UserControl
    Private _bizDivId As Integer
    Private _country As String
    Public Property BizDivId() As Integer
        Get
            Return _bizDivId
        End Get
        Set(ByVal value As Integer)
            _bizDivId = value
        End Set
    End Property
    ''' <summary>
    ''' property defines the country for user control
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Country() As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            TermsAnchor.HRef = ApplicationSettings.OrderWorkUKURL & "SPTermsAndConditionOuter.aspx"
            Country = ApplicationSettings.Country
            BizDivId = ApplicationSettings.OWUKBizDivId
            CommonFunctions.PopulateSecurityQues(Page, ddlQuestion)
            'Populate the Business Area Options - Added by PratikT on 25th Feb, 2009
            CommonFunctions.PopulateBusinessArea(Page, ddlBusinessArea)
            'Poonam added on 29/7/2015 -Task - OA-7 : OW-ENH-ADMIN � Set Business Area = Retail as Default when creating Client account /RegisterAsBuyer.aspx � this is to be un-editable
            ddlBusinessArea.SelectedIndex = 2
            ddlBusinessArea.Enabled = True
        End If
    End Sub
    Public Sub ServerValidation(ByVal source As Object, ByVal args As ServerValidateEventArgs)
        If chkTermsAndConditions.Checked = False Then
            args.IsValid = False
            rqTermsAndConditions.Visible = True

        End If
    End Sub
    Public Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        'Initialize the Viewstates on Page Load only
        If Not IsPostBack Then
            ViewState("AttemptCountReg") = 0
            ViewState("PostcodeTrial1") = ""
        End If

        'If keyword is not blank, then allow user to lookup
        If txtCompanyPostalCode.Text.Trim <> "" Then

            'Lookup for address only if the postcode provided is different than previous one
            If ViewState("PostcodeTrial1") <> txtCompanyPostalCode.Text Then

                'Increment the Lookup attempt Count
                ViewState("AttemptCountReg") = ViewState("AttemptCountReg") + 1

                'Allow user to lookup only if attempts made are less than 2. On third attempt, show error message PostcodeLookupTrialsExpiry
                If ViewState("AttemptCountReg") < 5 Then
                    divAddList.Visible = True
                    CommonFunctions.callGetAddressList(txtCompanyPostalCode.Text.Trim, lblErr, lstProperties, txtCompanyPostalCode)
                    ViewState("PostcodeTrial1") = txtCompanyPostalCode.Text
                Else
                    btnFind.Enabled = False
                    divAddList.Visible = True
                    lstProperties.Visible = False
                    lblErr.Visible = True
                    lblErr.Text = ResourceMessageText.GetString("PostcodeLookupTrialsExpiry")
                End If
            End If
        Else
            divAddList.Visible = True
            lstProperties.Visible = False
            lblErr.Visible = True
            lblErr.Text = ResourceMessageText.GetString("PostcodeNotAvailable")
        End If
    End Sub
    Public Sub RegisterMe(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNextSubmit.Click
        Dim emailExistMigrate As Boolean = False
        Dim response As String = CommonFunctions.ValidateMailAndMigration(txtEmail.Text)
        If response = "" Then
            emailExistMigrate = False
            lblError.Text = ""
        Else
            emailExistMigrate = True
            lblError.Text = response
            divValidationMain.Visible = True
            Return
        End If
        Page.Validate()
        If (Page.IsValid = False) Then
            divValidationMain.Visible = True
        Else
            divValidationMain.Visible = False
        End If
        'Following if loops executes when form amd mail is validated and also takes care of double clicks
        If Page.IsValid And emailExistMigrate = False And btnNextSubmit.Enabled = True Then
            btnNextSubmit.Enabled = False
            Dim dsRegister As XSDContacts
            'Build Contacts XSD and perform contacts entries in DB
            dsRegister = SaveData()
            If Not IsNothing(dsRegister) Then

                pnlRegistrationComplete.Visible = True
                'Code to show the registration complete message
                ' Buyer
                ltrRegistrationCompleteMessage.Text = ResourceMessageText.GetString("RegistrationCompleteBuyer")
                pnlMain.Visible = False
                'Using dsRegister to eb pass as parameter to the email class
                Emails.SendMailRegister(dsRegister, True, ApplicationSettings.Country, ApplicationSettings.UserType.buyer, Trim(txtPassword.Text))
            Else
                lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
                divValidationMain.Visible = True
            End If
        End If
    End Sub
    ''' <summary>
    ''' copy the form data to the dataset and pass the xml to webmethod which saves the data.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SaveData() As DataSet

        Dim hitSource As String = ""
        If Not IsNothing(Request("src")) Then
            hitSource = Request("src")
        End If

        If hitSource = "" Then
            hitSource = "OrderWork"
        End If
        'To build dataset by the form field
        Dim dsRegister As New xsdcontacts

        Dim NewsletterStatus As Integer = 0

        '======================= DS entries for ContactType:Company =====================================
        Dim nrowContacts As XSDContacts.ContactsRow = dsRegister.Contacts.NewRow
        With nrowContacts
            .ContactID = 0
            .MainContactID = 0
            .IsCompany = True
            .CompanyName = Trim(txtCompanyName.Text)
            .Profile = Trim(txtProfile.Text)
            .Src = hitSource
            .Phone = Trim(txtCompanyPhone.Text)
            .Fax = Trim(txtCompanyFax.Text)
            .IsTestAccount = ChkIsTestAccount.Checked
            If ddlBusinessArea.SelectedValue <> "" Then
                .BusinessArea = ddlBusinessArea.SelectedValue
            End If
        End With
        dsRegister.Contacts.Rows.Add(nrowContacts)

        Dim nrowContAttributes As XSDContacts.ContactsAttributesRow = dsRegister.ContactsAttributes.NewRow
        With nrowContAttributes
            .ContactID = 0
            .URL = Trim(txtWebsiteURL.Text)
        End With
        dsRegister.ContactsAttributes.Rows.Add(nrowContAttributes)


        'For updating tblContactsAddress
        Dim nrowCAdd As XSDContacts.tblContactsAddressesRow = dsRegister.tblContactsAddresses.NewRow
        With nrowCAdd
            .ContactID = 0
            .AddressID = 1
            .Name = ResourceMessageText.GetString("MainOffice")
            .Type = "Business"
            .Address = Trim(txtCompanyAddress.Text)
            .City = Trim(txtCity.Text)
            .State = Trim(txtCounty.Text)
            .PostCode = CommonFunctions.FormatPostcode(txtCompanyPostalCode.Text)
            .CountryID = ApplicationSettings.DefaultCountry
            .Sequence = 0
            .IsDefault = True
            .IsDepot = False
            .Phone = Trim(txtCompanyPhone.Text)
            .Fax = Trim(txtCompanyFax.Text)
        End With
        dsRegister.tblContactsAddresses.Rows.Add(nrowCAdd)

        Dim nrowContSetUp As XSDContacts.ContactsSetupRow = dsRegister.ContactsSetup.NewRow
        With nrowContSetUp
            .ContactID = 0
            .BizDivID = ApplicationSettings.OWUKBizDivId
            .ClassID = ApplicationSettings.RoleClientID
            .Status = ApplicationSettings.StatusApprovedCompany

        End With
        Dim sitePrefix As String = "OW"
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryUK
                Select Case BizDivId
                    Case ApplicationSettings.OWUKBizDivId
                        sitePrefix = "OW"
                    Case ApplicationSettings.SFUKBizDivId
                        sitePrefix = "MS"
                End Select
            Case ApplicationSettings.CountryDE
                Select Case BizDivId
                    Case ApplicationSettings.OWDEBizDivId
                        sitePrefix = "DE"
                    Case ApplicationSettings.SFDEBizDivId
                        sitePrefix = "MS"
                End Select
        End Select

        'Dim strAOEIds As String = UCAOE1.GetSelectedCombIds("getCommaSeperatedCombIds", Nothing)
        'If strAOEIds <> "" Then
        '    Dim AOEIds As String() = strAOEIds.Split(",")
        '    For Each aoeId As String In AOEIds

        '        Dim nrowAOE As XSDContacts.ContactsExtAttributesRow = dsRegister.ContactsExtAttributes.NewRow
        '        With nrowAOE
        '            .ContactID = 0
        '            .AttributeLabel = sitePrefix & "AOE"
        '            .AttributeValue = aoeId
        '        End With
        '        dsRegister.ContactsExtAttributes.Rows.Add(nrowAOE)
        '    Next
        'End If
        dsRegister.ContactsSetup.Rows.Add(nrowContSetUp)


        '======================= END of DS entries for ContactType:Company =====================================

        '======================= DS entries for ContactType:Person =============================================
        'For updating tblContacts

        Dim nrowContacts1 As XSDContacts.ContactsRow = dsRegister.Contacts.NewRow
        With nrowContacts1
            .ContactID = 1
            .MainContactID = 0
            .IsCompany = False
            .Src = hitSource
            .Fname = Trim(txtFName.Text).Replace("""", "")
            .Lname = Trim(txtLName.Text).Replace("""", "")
            .FullName = Trim(txtFName.Text).Replace("""", "") + " " + Trim(txtLName.Text).Replace("""", "")
            .FulFilWOs = True
            .ReceiveWONotif = True
            .Email = Trim(txtEmail.Text)
            .CompanyName = Trim(txtCompanyName.Text)
            .Phone = Trim(txtCompanyPhone.Text)
            .Fax = Trim(txtCompanyFax.Text)
            .IsTestAccount = ChkIsTestAccount.Checked
            If ddlBusinessArea.SelectedValue <> "" Then
                .BusinessArea = ddlBusinessArea.SelectedValue
            End If
        End With
        dsRegister.Contacts.Rows.Add(nrowContacts1)


        'For  updating  tblContactsLogin
        If ChkSubscribe.Checked = True Then
            NewsletterStatus = 1
        End If
        Dim nrowCLogin As XSDContacts.tblContactsLoginRow = dsRegister.tblContactsLogin.NewRow
        With nrowCLogin
            .ContactID = 1
            .UserName = Trim(txtEmail.Text)
            .Password = Encryption.EncryptToMD5Hash(Trim(txtPassword.Text))
            .SecurityQues = ddlQuestion.SelectedValue.ToString
            .SecurityAns = Trim(txtAnswer.Text)
            .NewsletterStatus = NewsletterStatus
            .EnableLogin = True
            .EmailVerified = True
            If (CBool(ViewState("SP"))) Then
                .HeardAboutUs = ViewState("HrdAbtUs").ToString
            End If
        End With
        dsRegister.tblContactsLogin.Rows.Add(nrowCLogin)

        Dim nrowContSetUp1 As XSDContacts.ContactsSetupRow = dsRegister.ContactsSetup.NewRow
        With nrowContSetUp1
            .ContactID = 1
            .BizDivID = ApplicationSettings.OWUKBizDivId
            .ClassID = ApplicationSettings.RoleClientID
            .Status = ApplicationSettings.StatusActive
        End With
        dsRegister.ContactsSetup.Rows.Add(nrowContSetUp1)


        ' For Updating tblContactsSettings
        Dim nrowContSettings As XSDContacts.ContactsSettingsRow = dsRegister.ContactsSettings.NewRow
        With nrowContSettings
            .ContactID = 0
            'If (chkFreesat.Checked) Then
            '    .FreesatandDigitalAerials = True
            'Else
            '    .FreesatandDigitalAerials = False
            'End If
            'If (chkElex.Checked) Then
            '    .ConsumerElectronicandTV = True
            'Else
            '    .ConsumerElectronicandTV = False
            'End If
            'If (chkpc.Checked) Then
            '    .HomePCInstallations = True
            'Else
            '    .HomePCInstallations = False
            'End If
            'If (chkIT.Checked) Then
            '    .BusinessIT = True
            'Else
            '    .BusinessIT = False
            'End If
            .ServicePartnerFees = 10
            .PrefSPPricing = False
        End With
        dsRegister.ContactsSettings.Rows.Add(nrowContSettings)


        'For updating Roles Contacts Linkage
        Dim nrowCRole As XSDContacts.tblRolesContactLinkageRow = dsRegister.tblRolesContactLinkage.NewRow
        With nrowCRole
            .ContactID = 1

            .RoleGroupID = ApplicationSettings.RoleClientAdminID


            .BizDivID = ApplicationSettings.OWUKBizDivId
            .IsDefault = True
            .IsActive = False
            .AddressID = 1
        End With
        dsRegister.tblRolesContactLinkage.Rows.Add(nrowCRole)



        '======================= END of DS entries for ContactType:Person ======================================

        Dim xmlContent As String = dsRegister.GetXml
        ' Remove XML namespace attribute and its value
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)
        Dim success As Boolean
        success = ws.WSContact.CreateContact(xmlContent, Session("UserID"))
        If success Then
            Return dsRegister
        Else
            'lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
            'divValidationMain.Visible = True
        End If

    End Function
    Private Sub lstProperties_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstProperties.SelectedIndexChanged
        Dim strSelectedItem As String = lstProperties.SelectedValue
        CommonFunctions.callPopulateAddress(lblErr, strSelectedItem, lstProperties, txtCompanyPostalCode, txtCompanyName, txtCompanyAddress, txtCity, txtCounty)

        If lblErr.Text = "" Then
            divAddList.Visible = False
        Else
            divAddList.Visible = True
        End If
    End Sub
End Class