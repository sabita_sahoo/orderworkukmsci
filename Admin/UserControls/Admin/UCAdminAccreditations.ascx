﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCAdminAccreditations.ascx.vb" Inherits="Admin.UCAdminAccreditations" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:UpdatePanel ID="UpdatePanel2" runat="server"  >
 <ContentTemplate>
 <script language="javascript" src="JS/json2.js"  type="text/javascript"></script>
<script language="javascript" src="JS/jquery.js"  type="text/javascript"></script>
<script language="javascript" src="JS/JQuery.jsonSuggestCustom.js"  type="text/javascript"></script>
<script language="JavaScript" src="JS/DD_roundies.js" type="text/javascript"></script>
<script type="text/javascript">    DD_roundies.addRule('.roundifyRefineMainBody', '5px 5px 5px 5px');</script>
<style type="text/css">
.radioButtonList *
{
    float:left;
}
.radioButtonList label
{
    margin-right:10px;
}
</style>
 <input id="hdnCommonID" type="hidden" name="hdnCommonID" runat="server" value="0">
 <input id="hdnTagID" type="hidden" name="hdnTagID" runat="server" value="">
 <input id="hdnType" type="hidden" name="hdnType" runat="server" value="">
 <input id="hdnSelectedType" type="hidden" name="hdnSelectedType" runat="server" value="">
 <table>
<tr>
    <td><asp:Label ID=lblMessage  CssClass="bodytxtValidationMsg" runat=server></asp:Label></td>
</tr>
</table>
<asp:Panel ID="pnlAccreds" runat="server">
 <table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="274" valign="top" >
    <TABLE cellSpacing="0" cellPadding="0" width="230" border="0" id="tdPartnerMember" runat="server">		
        <TR >
            <TD valign="bottom" class="formTxt" id="tdMember" runat="server">Technical expertise / Accreditation's & Keywords<br>
            <input type="text" id="txtExpertise" class="formFieldGrey215" style="width:252px;padding-top:3px;font-family:Arial;font-size:12px;height:16px;" AutoComplete = "Off" onclick="javascript:getResultsAccred(this.id)"  onkeyup="javascript:getResultsAccred(this.id)" onkeydown='javascript:processKey' onblur='javascript:hideSearchResultAccred()' runat="server"/>
            <div id="divMoreInfoAutoSuggest"  runat="server" class="roundedCornersTopManDiv" style="display:none;">
        <div class="clsTopImage"></div>        
        <table>
            <tr>
                <td colspan="2" class="lblOverviewBlue" style="padding-top:5px;">
                 <strong>More Information of this Accreditation</strong><br />
                 <asp:Label ID="lblMoreInfo" runat="server" CssClass="bodytxtValidationMsg"></asp:Label>
                </td>
                <td align="right">   
                  <a id="ancclose" class="footerTxtSelected " style="cursor:pointer;" onclick="Javascript:HideDivMoreInfo();"><b>Close</b></a>                             
                </td>
            </tr>
            <tr style="height: 1px;">
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td width="104">
                    <asp:Label ID="Label3" CssClass="lblOverviewBlue" runat="server">Accreditation No.</asp:Label>
                </td>
                <td>
                    <asp:TextBox onblur="javascript:removeHTML(this.id);" MaxLength="100" TabIndex="1" ID="txtTagInfo" runat="server" style="width:176px;height:18px;border:1px solid #999999;" CssClass="Width163 borderTextBox"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr style="height: 1px;">
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td width="104">
                    <asp:Label CssClass="lblOverviewBlue" ID="Label4" runat="server">Date</asp:Label>
                </td>
                <td>
                    <asp:TextBox MaxLength="100" ID="txtExpiryDate" TabIndex="2" runat="server" style="width:176px;height:18px;border:1px solid #999999;" CssClass="Width163 borderTextBox"></asp:TextBox>
                    <img style="cursor:pointer;vertical-align:middle;margin-top:-8px;" id="btnExpiryDate" src="./Images/calendar.gif" alt="Click to Select" > 
                   <%-- <img alt="Click to Select" src="../OWLibrary/Images/calendar.gif" id="btnExpiryDate" style="cursor:pointer;vertical-align:middle;" />	--%>													            
                    <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnExpiryDate TargetControlID="txtExpiryDate" ID="calExpiryDate" runat="server">
					</cc1:CalendarExtender>                    
                </td>
                <td>
                </td>
            </tr>
            <tr style="height: 1px;">
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td width="104">
                    <asp:Label CssClass="lblOverviewBlue" ID="Label5" runat="server">Other Info</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtOtherInfo" TabIndex="3" onblur="javascript:removeHTML(this.id);" TextMode="MultiLine"   runat="server" style="width:176px;border:1px solid #999999;" CssClass="Width163 borderTextBox"></asp:TextBox>                   
                 </td>
                <td><a id="ancAddAccrMoreInfo" onclick="javascript:AddAccrMoreInfo()" style="cursor:pointer;margin-left:6px;" class="txtAddButton cursorHand"></a>
                </td>
            </tr>
            
        </table>
    </div>
            <br />
            <div id="searchResultAccredations" class="searchResultAccredations" style="font-size:10px;width:254px;margin:0px;background-color:#ffffff;font-family: arial;font-size:12px;padding:0px;">
        </TR>
    </TABLE>
	  </td>	 
 </tr>
 </table>
 </asp:Panel>
<asp:Panel ID="pnlSelectedCompanyAccreds" runat="server">
<table width="650"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="25" class="formTxt">List of Submitted Company Accreditations</td>
  </tr>
  <tr>
    <td>
      <div id="divAccreds" runat=server >
		 <asp:GridView ID="gvSelectedCompanyAccreds" CellPadding=2 CellSpacing=0 runat=server  AutoGenerateColumns="False" BorderWidth="1px"  BorderColor="#EDEDED" Width="1000" OnRowDataBound="gvSelectedCompanyAccreds_RowDataBound" AllowSorting="true" OnSorting="grdView_Sorting" >
			<Columns>
             <asp:TemplateField HeaderStyle-HorizontalAlign=Center  HeaderText="<input id='chkAll' onClick=CheckAll(this,'CheckAccred') type='checkbox' name='chkAll' />">							   
			   <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="gridRowText"/>			  
			    <ItemTemplate>
			      <asp:CheckBox ID="CheckAccred" Runat="server" ></asp:CheckBox>
			      <input type="hidden" Runat="server" id="hdnTagId" value=<%# databinder.eval(container.dataitem, "TagId") %>/>
			    </ItemTemplate>
			    <ItemStyle  />
			</asp:TemplateField> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="FullName" HeaderText="Name" HtmlEncode=false SortExpression="FullName" />
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="TagName" HeaderText="Accreditations" HtmlEncode=false SortExpression="TagName" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="TagInfo" HeaderText="Tag Info" HtmlEncode=false SortExpression="TagInfo" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="OtherInfo" HeaderText="Other Info" HtmlEncode=false SortExpression="OtherInfo" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="Type" HeaderText="Type" HtmlEncode=false SortExpression="Type" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="DateCreated" HeaderText="Created" DataFormatString="{0:dd/MM/yy}" HtmlEncode=false SortExpression="DateCreated" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="TagExpiry" HeaderText="Expiry" DataFormatString="{0:dd/MM/yy}" HtmlEncode=false SortExpression="TagExpiry" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="CreatedBy" HeaderText="Created By" HtmlEncode=false SortExpression="CreatedBy" /> 
			<asp:TemplateField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center HeaderText="Approval Status" SortExpression="ApprovalStatus" >
                <ItemTemplate>
                    <asp:HiddenField ID="hidTagContactLinkageId" runat="server" Value='<%# Eval("TagContactLinkageID") %>' />
                    <asp:RadioButtonList ID="rblApprovalStatus" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" DataTextField="StandardValue" DataValueField="StandardID" />
                </ItemTemplate>
            </asp:TemplateField>
			</Columns>
			<AlternatingRowStyle   BackColor="#E7E7E7" /> <RowStyle CssClass=gridRow /> 
		 </asp:GridView>
		<div style="float:right;clear:both;">
            <asp:Button ID="btnCompanyUpdate" runat="server" Text="Update" CausesValidation="false" />
         </div>
		</div></td>
  </tr>
  
   <tr>
    <td height="40" valign="middle" class="formTxt"> 
	<TABLE cellSpacing="0" cellPadding="0" width="100" bgColor="#a0a0a0" border="0">
          <TR>
            <TD height="18"><IMG id="IMG3" height="18" runat=server src="~/Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
            <TD><asp:linkbutton   class="txtButtonNoArrow" CausesValidation=false id="lnkCompanyRemove" runat="server">Delete Selected</asp:linkbutton></TD>
            <TD width="5"><IMG id="IMG4" height="18" runat=server src="~/Images/Curves/Grey-BtnRight.gif" width="5"></TD>
          </TR>
        </TABLE> </td>
  </tr>
</table>
</asp:Panel>
<asp:Panel ID="pnlSelectedEnggAccreds" runat="server">
<table width="1000"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="25" class="formTxt">List of Submitted Engineer Accreditations</td>
  </tr>
  <tr>
    <td>
      <div id="div1" runat=server >
		 <asp:GridView ID="gvSelectedEngineerAccreds" CellPadding="2" CellSpacing="0" runat="server"  AllowSorting="true" AutoGenerateColumns="False" BorderWidth="1px"  BorderColor="#EDEDED" OnRowDataBound="gvSelectedEngineerAccreds_RowDataBound" OnSorting="grdView_Sorting" Width="1000">
			<Columns>
             <asp:TemplateField HeaderStyle-HorizontalAlign=Center  HeaderText="<input id='chkAll' onClick=CheckAll(this,'CheckAccred') type='checkbox' name='chkAll' />">							   
			   <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="gridRowText"/>			  
			    <ItemTemplate>
			      <asp:CheckBox ID="CheckAccred" Runat="server" ></asp:CheckBox>
			      <input type="hidden" Runat="server" id="hdnTagId" value=<%# databinder.eval(container.dataitem, "TagId") %>/>
			    </ItemTemplate>
			    <ItemStyle  />
			</asp:TemplateField> 
            <asp:TemplateField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center HeaderText="Name" SortExpression="FullName" >
                <ItemTemplate >             
                   <%# "<a class=footerTxtSelected href=SpecialistsForm.aspx?ContactId=" & Container.DataItem("ContactID") & "&CompanyID=" & Request.QueryString("companyId") & "&bizDivId=" & Request.QueryString("bizDivId") & ">" & Container.DataItem("FullName") & "</a>"%>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="TagName" HeaderText="Accreditations" HtmlEncode="false" SortExpression="TagName" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="TagInfo" HeaderText="Tag Info" HtmlEncode=false SortExpression="TagInfo" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="OtherInfo" HeaderText="Other Info" HtmlEncode=false SortExpression="OtherInfo" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="Type" HeaderText="Type" HtmlEncode=false SortExpression="Type" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="DateCreated" HeaderText="Created" DataFormatString="{0:dd/MM/yy}" HtmlEncode=false SortExpression="DateCreated" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="TagExpiry" HeaderText="Expiry" DataFormatString="{0:dd/MM/yy}" HtmlEncode=false SortExpression="TagExpiry" /> 
            <asp:BoundField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center  DataField="CreatedBy" HeaderText="Created By" HtmlEncode=false SortExpression="CreatedBy" /> 
			<asp:TemplateField  ItemStyle-CssClass="gridTextNew"  ItemStyle-HorizontalAlign=left HeaderStyle-HorizontalAlign=Center HeaderText="Approval Status" SortExpression="ApprovalStatus" >
                <ItemTemplate>
                    <asp:HiddenField ID="hidTagContactLinkageId" runat="server" Value='<%# Eval("TagContactLinkageID") %>' />
                    <asp:RadioButtonList ID="rblApprovalStatus" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" DataTextField="StandardValue" DataValueField="StandardID" />
                </ItemTemplate>
            </asp:TemplateField>
			</Columns>
			<AlternatingRowStyle   BackColor="#E7E7E7" /> <RowStyle CssClass=gridRow />
		 </asp:GridView>
         <div style="float:right;clear:both;">
            <asp:Button ID="btnUpdate" runat="server" Text="Update" CausesValidation="false" />
         </div>
		
		</div></td>
  </tr>
  
   <tr>
    <td height="40" valign="middle" class="formTxt"> 
	<TABLE cellSpacing="0" cellPadding="0" width="100" bgColor="#a0a0a0" border="0">
          <TR>
            <TD height="18"><IMG id="IMG1" height="18" runat=server src="~/Images/Curves/Grey-BtnLeft.gif" width="5"></TD>
            <TD><asp:linkbutton   class="txtButtonNoArrow" CausesValidation=false id="lnkEngineerRemove" runat="server">Delete Selected</asp:linkbutton></TD>
            <TD width="5"><IMG id="IMG2" height="18" runat=server src="~/Images/Curves/Grey-BtnRight.gif" width="5"></TD>
          </TR>
        </TABLE> </td>
  </tr>
</table>
</asp:Panel>
<asp:Panel ID="pnlSelectedCompany" runat="server">
 <div style="border: 1px solid #999999;
    width: 644px;margin-top:10px;padding-top:10px;padding-bottom:10px;float:left;padding-right:10px;">
  <asp:Repeater ID="rptSelectedCompany" runat="server">
    <ItemTemplate>        
        <div class="divStyle1 roundifyRefineMainBody"><span class="clsFloatLeft"><asp:Label ID="lblAccreds" runat="server" Text=' <%#Eval("TagName")%> '></asp:Label></span>
        <span class="marginleft8 clsFloatLeft" >
        <asp:LinkButton ID="lnkbtnRemove" CommandName="Remove" CausesValidation="false" CommandArgument='<%#Container.DataItem("TagId") %>' runat="server" style="text-decoration:none;color:#000000;">
        x</asp:LinkButton></span></div>
    </ItemTemplate>    
  </asp:Repeater>  
  </div>
</asp:Panel>
<asp:Panel ID="pnlSelectedEngineer" runat="server">
 <div style="border: 1px solid #999999;
    width: 644px;margin-top:10px;padding-top:10px;padding-bottom:10px;float:left;padding-right:10px;">
  <asp:Repeater ID="rptSelectedEngineer" runat="server">
    <ItemTemplate>        
        <div class="divStyle2 roundifyRefineMainBody"><span class="clsFloatLeft"><asp:Label ID="lblAccreds" runat="server" Text=' <%#Eval("TagName")%> '></asp:Label></span>
        <span class="marginleft8 clsFloatLeft" >
        <asp:LinkButton ID="lnkbtnRemove" CommandName="Remove" CausesValidation="false" CommandArgument='<%#Container.DataItem("TagId") %>' runat="server" style="text-decoration:none;color:#000000;">
        x</asp:LinkButton></span></div>
    </ItemTemplate>    
  </asp:Repeater>  
  </div>
</asp:Panel>
 <asp:Panel ID="pnlSelectedOthers" runat="server">
 <div style="border: 1px solid #999999;
    width: 644px;margin-top:10px;padding-top:10px;padding-bottom:10px;float:left;padding-right:10px;">
  <asp:Repeater ID="rptSelectedOthers" runat="server">
    <ItemTemplate>        
        <div class="divStyle3 roundifyRefineMainBody"><span class="clsFloatLeft"><asp:Label ID="lblAccreds" runat="server" Text=' <%#Eval("TagName")%> '></asp:Label></span>
        <span class="marginleft8 clsFloatLeft" >
        <asp:LinkButton ID="lnkbtnRemove" CommandName="Remove" CausesValidation="false" CommandArgument='<%#Container.DataItem("TagId") %>' runat="server" style="text-decoration:none;color:#000000;">
        x</asp:LinkButton></span></div>
    </ItemTemplate>    
  </asp:Repeater>  
  </div>
</asp:Panel>
<asp:Button runat="server" ID="hdnAccrSubmit" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" CausesValidation="false" /> 			 
 </ContentTemplate>
 </asp:UpdatePanel>