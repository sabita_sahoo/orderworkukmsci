Partial Public Class UCAdminWOsIssueListing

    Inherits System.Web.UI.UserControl
    Protected WithEvents GridPagerTop As GridPager
    Protected WithEvents GridPagerBtm As GridPager
    Delegate Sub DelPopulateObject(ByVal CompID As Integer, ByVal myInt As Integer, ByVal InitialCall As Boolean, ByVal PagerIndex As Integer, ByVal killcache As Boolean, ByVal IssueBy As String, ByVal WorkorderID As String)
    Shared ws As New WSObjs
    Public WithEvents lnkbtnWatch As LinkButton
    Public WithEvents lnkbtnUnwatch As LinkButton

#Region "Property"


    Public Property Group() As String
        Get
            If Not IsNothing(ViewState("Group")) Then
                Return ViewState("Group")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("Group") = value

        End Set
    End Property
    Public Property BizDiv() As String
        Get
            If Not IsNothing(ViewState("BizDiv")) Then
                Return ViewState("BizDiv")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("BizDiv") = value
        End Set
    End Property

    Public Property startrowindex() As String
        Get
            If Not IsNothing(ViewState("startrowindex")) Then
                Return ViewState("startrowindex")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("startrowindex") = value
        End Set
    End Property


    Public Property CompID() As String
        Get
            If Not IsNothing(ViewState("CompID")) Then
                Return ViewState("CompID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("CompID") = value

        End Set
    End Property
    Public Property WorkorderID() As String
        Get
            If Not IsNothing(ViewState("WorkorderID")) Then
                Return ViewState("WorkorderID")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("WorkorderID") = value

        End Set
    End Property
    Public Property FromDate() As String
        Get
            If Not IsNothing(ViewState("FromDate")) Then
                Return ViewState("FromDate")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("FromDate") = value

        End Set
    End Property
    Public Property ToDate() As String
        Get
            If Not IsNothing(ViewState("ToDate")) Then
                Return ViewState("ToDate")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("ToDate") = value

        End Set
    End Property
    Public Property sortExpression() As String
        Get
            If Not IsNothing(ViewState("sortExpression")) Then
                Return ViewState("sortExpression")
            Else
                Return "DateModified"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("sortExpression") = value
        End Set
    End Property

    Public Property sortOrder() As String
        Get
            If Not IsNothing(ViewState("sortOrder")) Then
                Return ViewState("sortOrder")
            Else
                Return "DESC"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("sortOrder") = value
        End Set
    End Property
    Public Property sortOrderOld() As String
        Get
            If Not IsNothing(ViewState("sortOrderOld")) Then
                Return ViewState("sortOrderOld")
            Else
                Return "DESC"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("sortOrderOld") = value
        End Set
    End Property
    Public Property BillLoc() As Integer
        Get
            If Not IsNothing(Session("BillLoc")) Then
                Return Session("BillLoc")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            Session("BillLoc") = value
        End Set
    End Property
    Public Function FillBillingLocation() As DataView
        Dim dvBillLoc As DataView = Nothing
        If Not IsNothing(Session("dvBillLoc")) Then
            dvBillLoc = CType(Session("dvBillLoc"), DataView)
            If Not (dvBillLoc.Count > 0) Then
                dvBillLoc = Nothing
            End If
        End If
        Return dvBillLoc
    End Function


    Public Property SelectedWOIDs() As String
        Get
            If Not IsNothing(ViewState("SelectedWOIDs")) Then
                Return ViewState("SelectedWOIDs")
            Else
                Return "WorkOrderId"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("SelectedWOIDs") = value
        End Set
    End Property

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim delPopulate As New DelPopulateObject(AddressOf Populate)
        GridPagerTop.UpdateIndex = delPopulate
        GridPagerTop.PageSize = ApplicationSettings.GridViewPageSize
        GridPagerTop.PageObj = Nothing
        GridPagerTop.HasSinglePager = False
        GridPagerTop.useMaster = False
        GridPagerTop.ContentPlaceHolderId = ""
        GridPagerTop.UCObj = Me
        GridPagerTop.CompID = CompID
        GridPagerBtm.UpdateIndex = delPopulate
        GridPagerBtm.PageSize = 25
        GridPagerBtm.PageObj = Nothing
        GridPagerBtm.HasSinglePager = False
        GridPagerBtm.useMaster = False
        GridPagerBtm.ContentPlaceHolderId = ""
        GridPagerBtm.UCObj = Me
        GridPagerBtm.CompID = CompID

        If Not (IsNothing(Request.QueryString("PN"))) Then
            ViewState("PagerIndex") = CInt(Request.QueryString("PN").Trim)
        Else
            ViewState("PagerIndex") = 0
        End If
        If Not IsPostBack Then
            'Populate(CompID)
            sortExpression = "DateModified"
            ViewState("SortOld") = ""
            '/* DONOT CHANGE ANYTHING HERE */
            GridPagerTop.StartIndex = 0
            GridPagerBtm.StartIndex = 0

            If Not IsNothing(Session("IssueBy")) Then
                Populate(CompID, 0, True, ViewState("PagerIndex"), False, Session("IssueBy"), Session("IssueWorkorderID"))
            Else
                Populate(CompID, 0, True, ViewState("PagerIndex"))
            End If

            '/* DONOT CHANGE ANYTHING HERE */
        End If

    End Sub

    Public Sub Populate(ByVal CompID As Integer, ByVal StartIndex As Integer, ByVal InitialCall As Boolean, ByVal PagerIndex As Integer, Optional ByVal killcache As Boolean = False, Optional ByVal IssueBy As String = "ALL", Optional ByVal WorkorderID As String = "")

        If killcache = False Then
            Session.Remove("AdminIssueListing" & Session.SessionID)
        End If
        Dim BL As Integer = 0
        If CompID = 0 Then
            BL = 0
        Else
            BL = BillLoc
        End If
        If Session("AdminIssueListing" & Session.SessionID) Is Nothing Then
            Dim ds As DataSet
            Dim hideTestWOs As Boolean = CType(Me.Parent.FindControl("chkHideTestAcc"), CheckBox).Checked
            Dim MainCat As Boolean = CType(Me.Parent.FindControl("chkMainCat"), CheckBox).Checked
            ds = ws.WSWorkOrder.GetAdminWOListing(sortExpression & " " & sortOrder, StartIndex, 25, ApplicationSettings.WOGroupIssue, CompID, "", "", ApplicationSettings.OWUKBizDivId, hideTestWOs, IssueBy, False, BL, MainCat, False, WorkorderID, Session("UserID"), "", "")
            If ds.Tables(1).Rows(0).Item("TotalRecs") <> 0 Then
                pnlNoRecords.Visible = False
                pnlIssueListing.Visible = True
                HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
                HttpContext.Current.Items("tblWOUserNotes") = ds.Tables("tblWOUserNotes")
                Session.Add("AdminIssueListing" & Session.SessionID, ds)
                dlIssueWOListing.DataSource = ds.Tables(0).DefaultView
                dlIssueWOListing.DataBind()
                GridPagerTop.TotalCount = ds.Tables(1).Rows(0).Item("TotalRecs")
                GridPagerBtm.TotalCount = ds.Tables(1).Rows(0).Item("TotalRecs")

                If InitialCall = True Then
                    GridPagerBtm.InitializePager()
                    GridPagerTop.InitializePager()
                End If

                GridPagerTop.SetPager(PagerIndex)
                GridPagerBtm.SetPager(PagerIndex)

                ViewState("PagerIndex") = PagerIndex  'Intialze pager here
            Else
                pnlNoRecords.Visible = True
                pnlIssueListing.Visible = False
            End If
            Dim dvBillLoc As DataView
            dvBillLoc = ds.Tables("tblBillLoc").Copy.DefaultView
            Session("dvBillLoc") = dvBillLoc

            'Hide watch/unwatch icon

            'If ds.Tables("tblDetails").Rows(0).Item("IsWatch") = 0 Then
            '    lnkbtnWatch.Visible = True
            '    lnkbtnUnwatch.Visible = False
            'Else
            '    lnkbtnWatch.Visible = False
            '    lnkbtnUnwatch.Visible = True
            'End If

        End If

    End Sub

    Public Function GetLinks(ByVal parambizDivId As Object, ByVal companyId As Object, ByVal contactId As Object, ByVal SupplierCompanyName As String) As String
        Dim link As String = ""
        Dim contactType As ApplicationSettings.ContactType = ApplicationSettings.ContactType.approvedSupplier
        Dim classId As String
        Dim statusId As String
        classId = ApplicationSettings.RoleSupplierID
        statusId = ApplicationSettings.StatusApprovedCompany
        Dim bizDivId = parambizDivId
        link &= "<a class='footerTxtSelected ' href='companyprofile.aspx?sender=AdminWoListing&bizDivId=" & bizDivId & "&SearchWorkorderID=" & WorkorderID & "&contactType=" & contactType & "&companyid=" & companyId & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&mode=" & Request("mode") & "&ps=25&pn=0&sc=datecreated&so=Desc&CompID=" & CompID & "&FromDate=" & FromDate & "&ToDate=" & ToDate & "'>" & SupplierCompanyName & "</a>"

        Return link

    End Function

    Private Sub dlIssueWOListing_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlIssueWOListing.ItemCommand
        If e.Item.ItemType = ListItemType.Header Then
            ViewState("SortOld") = sortExpression
            If (sortExpression = e.CommandName.ToString) Then
                If (sortOrder = "ASC") Then
                    sortOrderOld = sortOrder
                    sortOrder = "DESC"
                Else
                    sortOrderOld = sortOrder
                    sortOrder = "ASC"
                End If
                sortExpression = sortExpression.ToString
            Else
                sortExpression = e.CommandName.ToString
                sortOrderOld = sortOrder
                sortOrder = "ASC"
            End If

            Dim aObj(6) As Object
            aObj(0) = CInt(CompID)
            aObj(1) = 0
            aObj(2) = False
            aObj(3) = 0
            aObj(4) = False
            aObj(5) = Session("IssueBy")
            aObj(6) = Session("IssueWorkorderID")
            Dim delPopulate As New DelPopulateObject(AddressOf Populate)
            delPopulate.DynamicInvoke(aObj)
        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            If (e.CommandName = "AddNote") Then
                Dim WorkOrderId As String
                WorkOrderId = e.CommandArgument.ToString
                ViewState.Add("SelectedWorkOrderId", WorkOrderId)
                CType(FindControl("mdlQuickNotes"), AjaxControlToolkit.ModalPopupExtender).Show()
            End If
        End If

        If (e.CommandName = "RateWO") Then
            ViewState.Add("SelectedWorkOrderId", e.CommandArgument.ToString)
            mdlUpdateRating.Show()
            rdBtnNegative.Checked = False
            rdBtnNeutral.Checked = False
            rdBtnPositive.Checked = False
            txtComment.Text = ""

            Dim ds As DataSet
            ds = CommonFunctions.GetWORating(CInt(e.CommandArgument.ToString))
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
                        Dim Result As Integer = ds.Tables(0).Rows(0)(0)
                        Select Case Result
                            Case -1
                                rdBtnNegative.Checked = True
                            Case 0
                                rdBtnNeutral.Checked = True
                            Case 1
                                rdBtnPositive.Checked = True
                        End Select
                    End If
                    txtComment.Text = ds.Tables("tblRating").Rows(0).Item("Comments")
                End If
            End If
        End If


        If (e.CommandName = "Action") Then
            Dim Param() As String
            Param = e.CommandArgument.ToString.Split(",")
            'WorkOrderId = e.CommandArgument.ToString
            WorkorderID = CType(Param(0), Integer)
            ViewState.Add("SelectedWorkOrderId", WorkorderID)
            CType(FindControl("mdlAction"), AjaxControlToolkit.ModalPopupExtender).Show()
            rdBtnActionPositive.Checked = False
            rdBtnActionNeutral.Checked = False
            rdBtnActionNegative.Checked = False
            txtCommentAction.Text = ""
            chkMyWatch.Checked = False
            chkMyRedFlag.Checked = False

            Dim Rating As Integer = CType(Param(1), Integer)
            Select Case Rating
                Case -1
                    rdBtnActionNegative.Checked = True
                Case 0
                    rdBtnActionNeutral.Checked = True
                Case 1
                    rdBtnActionPositive.Checked = True
            End Select
            txtCommentAction.Text = CType(Param(2), String)
            chkMyWatch.Checked = Param(3)
            chkMyRedFlag.Checked = Param(4)

            'Dim ds As DataSet
            'ds = CommonFunctions.GetWORating(WorkorderID, CInt(Session("UserID")), "WOAction")
            'If ds.Tables.Count > 0 Then
            '    If ds.Tables(0).Rows.Count > 0 Then
            '        If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
            '            Dim Result As Integer = ds.Tables(0).Rows(0)(0)
            '            Select Case Result
            '                Case -1
            '                    rdBtnActionNegative.Checked = True
            '                Case 0
            '                    rdBtnActionNeutral.Checked = True
            '                Case 1
            '                    rdBtnActionPositive.Checked = True
            '            End Select
            '        End If
            '        txtCommentAction.Text = ds.Tables("tblRating").Rows(0).Item("Comments")
            '    End If
            '    If ds.Tables(1).Rows.Count > 0 Then
            '        chkMyWatch.Checked = ds.Tables(1).Rows(0).Item("IsWatched")
            '        chkMyRedFlag.Checked = ds.Tables(1).Rows(0).Item("IsFlagged")
            '    End If
            'End If

        End If

        If (e.CommandName = "Watch") Then
            Dim WorkOrderId As String
            WorkOrderId = e.CommandArgument.ToString
            ws.WSWorkOrder.AddDeleteWatchedWO(WorkOrderId, Session("UserID"), "Add")
            ViewState("IsWatch") = 1
            lnkbtnWatch = e.Item.FindControl("lnkbtnWatch")
            lnkbtnUnwatch = e.Item.FindControl("lnkbtnUnwatch")
            lnkbtnWatch.Visible = False
            lnkbtnUnwatch.Visible = True
            PopulateGrid()
        End If

        If (e.CommandName = "Unwatch") Then
            Dim WorkOrderId As String
            WorkOrderId = e.CommandArgument.ToString
            ws.WSWorkOrder.AddDeleteWatchedWO(WorkOrderId, Session("UserID"), "Remove")
            ViewState("IsWatch") = 0
            lnkbtnWatch = e.Item.FindControl("lnkbtnWatch")
            lnkbtnUnwatch = e.Item.FindControl("lnkbtnUnwatch")
            lnkbtnWatch.Visible = True
            lnkbtnUnwatch.Visible = False
            PopulateGrid()
        End If





    End Sub

    Public Sub dlIssueWOListing_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlIssueWOListing.ItemDataBound
        'Getting WOID for each SI
        If e.Item.ItemType = ListItemType.Header Then
            If Not IsNothing(ViewState("SortOld")) Then
                If (ViewState("SortOld").ToString <> sortExpression) Then
                    Select Case sortExpression
                        Case "RefWOID"
                            CType(e.Item.FindControl("RefWOIDSortIcon"), Image).Visible = True
                        Case "DateCreated"
                            CType(e.Item.FindControl("DateCreatedSortIcon"), Image).Visible = True
                        Case "DateModified"
                            CType(e.Item.FindControl("DateModifiedSortIcon"), Image).Visible = True
                            If (ViewState("SortOld").ToString() = "") Then
                                CType(e.Item.FindControl("DateModifiedSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                            End If
                        Case "Location"
                            CType(e.Item.FindControl("LocationSortIcon"), Image).Visible = True
                        Case "BuyerCompany"
                            CType(e.Item.FindControl("BuyerSortIcon"), Image).Visible = True
                        Case "SupplierContact"
                            CType(e.Item.FindControl("SupplierContactSortIcon"), Image).Visible = True
                        Case "WOTitle"
                            CType(e.Item.FindControl("WOTitleSortIcon"), Image).Visible = True
                        Case "DateStart"
                            CType(e.Item.FindControl("DateStartSortIcon"), Image).Visible = True
                        Case "WholesalePrice"
                            CType(e.Item.FindControl("WholesalePriceSortIcon"), Image).Visible = True
                        Case "PlatformPrice"
                            CType(e.Item.FindControl("PlatformPriceSortIcon"), Image).Visible = True
                        Case "UpSellPrice"
                            CType(e.Item.FindControl("UpSellPriceSortIcon"), Image).Visible = True
                    End Select
                End If
                If Not IsNothing(ViewState("SortOld")) Then
                    If (ViewState("SortOld").ToString = sortExpression) Then
                        Select Case sortExpression
                            Case "RefWOID"
                                CType(e.Item.FindControl("RefWOIDSortIcon"), Image).Visible = True
                                If (sortOrderOld = "ASC") Then
                                    CType(e.Item.FindControl("RefWOIDSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                                Else
                                    CType(e.Item.FindControl("RefWOIDSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Up.gif"
                                End If

                            Case "DateCreated"
                                CType(e.Item.FindControl("DateCreatedSortIcon"), Image).Visible = True
                                If (sortOrderOld = "ASC") Then
                                    CType(e.Item.FindControl("DateCreatedSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                                Else
                                    CType(e.Item.FindControl("DateCreatedSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Up.gif"
                                End If
                            Case "DateModified"
                                CType(e.Item.FindControl("DateModifiedSortIcon"), Image).Visible = True
                                If (sortOrderOld = "ASC") Then
                                    CType(e.Item.FindControl("DateModifiedSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                                Else
                                    CType(e.Item.FindControl("DateModifiedSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Up.gif"
                                End If
                            Case "Location"
                                CType(e.Item.FindControl("LocationSortIcon"), Image).Visible = True
                                If (sortOrderOld = "ASC") Then
                                    CType(e.Item.FindControl("LocationSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                                Else
                                    CType(e.Item.FindControl("LocationSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Up.gif"
                                End If
                            Case "BuyerCompany"
                                CType(e.Item.FindControl("BuyerSortIcon"), Image).Visible = True
                                If (sortOrderOld = "ASC") Then
                                    CType(e.Item.FindControl("BuyerSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                                Else
                                    CType(e.Item.FindControl("BuyerSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Up.gif"
                                End If
                            Case "SupplierContact"
                                CType(e.Item.FindControl("SupplierContactSortIcon"), Image).Visible = True
                                If (sortOrderOld = "ASC") Then
                                    CType(e.Item.FindControl("SupplierContactSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                                Else
                                    CType(e.Item.FindControl("SupplierContactSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Up.gif"
                                End If
                            Case "WOTitle"
                                CType(e.Item.FindControl("WOTitleSortIcon"), Image).Visible = True
                                If (sortOrderOld = "ASC") Then
                                    CType(e.Item.FindControl("WOTitleSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                                Else
                                    CType(e.Item.FindControl("WOTitleSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Up.gif"
                                End If
                            Case "DateStart"
                                CType(e.Item.FindControl("DateStartSortIcon"), Image).Visible = True
                                If (sortOrderOld = "ASC") Then
                                    CType(e.Item.FindControl("DateStartSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                                Else
                                    CType(e.Item.FindControl("DateStartSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Up.gif"
                                End If
                            Case "WholesalePrice"
                                CType(e.Item.FindControl("WholesalePriceSortIcon"), Image).Visible = True
                                If (sortOrderOld = "ASC") Then
                                    CType(e.Item.FindControl("WholesalePriceSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                                Else
                                    CType(e.Item.FindControl("WholesalePriceSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Up.gif"
                                End If
                            Case "PlatformPrice"
                                CType(e.Item.FindControl("PlatformPriceSortIcon"), Image).Visible = True
                                If (sortOrderOld = "ASC") Then
                                    CType(e.Item.FindControl("PlatformPriceSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                                Else
                                    CType(e.Item.FindControl("PlatformPriceSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Up.gif"
                                End If
                            Case "UpSellPrice"
                                CType(e.Item.FindControl("UpSellPriceSortIcon"), Image).Visible = True
                                If (sortOrderOld = "ASC") Then
                                    CType(e.Item.FindControl("UpSellPriceSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Down.gif"
                                Else
                                    CType(e.Item.FindControl("UpSellPriceSortIcon"), Image).ImageUrl = ApplicationSettings.ImageFolder & "RedArrow-Up.gif"
                                End If
                        End Select
                    End If

                End If
            End If


        End If
        If e.Item.ItemType = ListItemType.Item Then
            Dim dlHistory As DataList = CType(e.Item.FindControl("DLSupplierResponse"), DataList)

            Dim lblWOID As HtmlInputHidden = CType(e.Item.FindControl("hdnWOIDs"), HtmlInputHidden)
            Dim ds As New DataSet
            ds = CType(Session("AdminIssueListing" & Session.SessionID), DataSet)
            Dim dv As New DataView
            If ds.Tables.Count > 2 Then
                If ds.Tables(2).Rows.Count > 0 Then
                    dlHistory.Visible = True
                    dv = ds.Tables(2).DefaultView
                    dv.RowFilter = "WOID = " & lblWOID.Value

                    dlHistory.DataSource = dv.ToTable.DefaultView
                    'ddlHistory_ItemDataBound(dlHistory, e)
                    dlHistory.DataBind()
                End If
            End If
        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim dlHistory As DataList = CType(e.Item.FindControl("DLSupplierResponse"), DataList)
            Dim lblWOID As HtmlInputHidden = CType(e.Item.FindControl("hdnWOIDs"), HtmlInputHidden)
            Dim ds As New DataSet
            ds = CType(Session("AdminIssueListing" & Session.SessionID), DataSet)
            Dim dv As New DataView
            If ds.Tables.Count > 2 Then
                If ds.Tables(2).Rows.Count > 0 Then
                    dv = ds.Tables(2).DefaultView
                    dv.RowFilter = "WOID = " & lblWOID.Value

                    dlHistory.DataSource = dv.ToTable.DefaultView
                    'ddlHistory_ItemDataBound(dlHistory, e)
                    dlHistory.DataBind()
                End If
            End If

        End If


    End Sub
    Public Sub ddlHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        If (DataBinder.Eval(e.Item, "BusinessArea") = ApplicationSettings.BusinessAreaRetail) Then

        End If
    End Sub

    Public Function GetActionColLinks(ByVal ContactID As Integer, ByVal CompanyID As Integer, ByVal WOID As Integer, ByVal ContactClassID As Integer)
        Dim SuppContID = ContactID
        Dim SuppCompID = CompanyID
        Dim link As String = ""
        Dim appendLink As String = ""
        Dim strGroup As String = ViewState("Group")
        Dim ds As New DataSet
        If Session("AdminIssueListing" & Session.SessionID) Is Nothing Then
            ds = CType(Session("AdminIssueListing" & Session.SessionID), DataSet)
        End If
        appendLink &= "WOID=" & WOID
        appendLink &= "&CompID=" & CompID
        appendLink &= "&SupContactId=" & SuppContID
        appendLink &= "&SupCompId=" & SuppCompID
        appendLink &= "&Group=" & ViewState("Group")
        appendLink &= "&BizDivID=" & ApplicationSettings.OWUKBizDivId
        appendLink &= "&FromDate=" & FromDate
        appendLink &= "&ToDate=" & ToDate
        If Not IsNothing(Session("IssueWorkorderID")) Then
            appendLink &= "&WorkorderID=" & Session("IssueWorkorderID")
        End If
        If Not IsNothing(Session("IssueWorkorderID")) Then
            appendLink &= "&SearchWorkorderID=" & Session("IssueWorkorderID")
        End If
        appendLink &= "&PS=" & ApplicationSettings.GridViewPageSize
        appendLink &= "&PN=" & ViewState("PagerIndex")
        appendLink &= "&SC=" & sortExpression
        appendLink &= "&SO=" & sortOrder
        appendLink &= "&sender=" & "UCWOsListing"

        'Change Issue Terms Icon and Accept Issue Icon
        If strGroup = ApplicationSettings.WOGroupIssue Then
            'THE FOLLOWING ICONS WILL BE SHOWN TO THE ADMIN ALL THE TIME IN THIS STATUS
            link &= "<a id='lnkDiscussIssue' runat='server' href='WODiscuss.aspx?Action=DiscussIssue&" & appendLink & "'><img src='Images/Icons/Discuss.gif' title='" & ResourceMessageText.GetString("Discuss") & "' width='16' height='12' hspace='2' vspace='0' border='0'></a>"
            link &= "<a id='lnkAcceptIssue' runat='server' href='WOAcceptIssue.aspx?Action=AcceptIssue&" & appendLink & "&sender=UCWODetails " & " '><img src='Images/Icons/Accept-Issue.gif' title='" & ResourceMessageText.GetString("AcceptIssue") & "' width='14' height='13' hspace='2' vspace='0' border='0'></a>"

            'Check here if the issue is raised by this user, then only the icon needs to be shown.
            If ContactClassID = ApplicationSettings.RoleOWID Then 'Admin has raised the issue
                link &= "<a id='lnkEditIssue' runat='server' href='WOChangeIssue.aspx?Action=EditIssue&" & appendLink & "&sender=UCWODetails" & " '><img src='Images/Icons/EditIssue.gif' title='" & ResourceMessageText.GetString("ChangeTerms") & "' width='17' height='13' hspace='2' vspace='0' border='0'></a>"
            Else
                link &= "<a id='lnkRejectIssue' runat='server' href='WORejectIssue.aspx?Action=RejectIssue&" & appendLink & "&sender=UCWODetails" & " '><img src='Images/Icons/Reject.gif' title='Reject Issue' hspace='2' vspace='0' border='0'></a>"
            End If
        End If
        Return link
    End Function

    Public Function getDiscussions(ByVal WOTrackingID As Integer, ByVal CompanyID As Integer) As DataView
        Dim ds As New DataSet
        If Not IsNothing(Session("AdminIssueListing" & Session.SessionID)) Then
            ds = CType(Session("AdminIssueListing" & Session.SessionID), DataSet)
        End If
        Dim dv As DataView
        If ds.Tables.Count > 3 Then
            If ds.Tables(3).Rows.Count > 0 Then
                dv = ds.Tables(3).DefaultView
                dv.RowFilter = "WOTrackingID = " & WOTrackingID
            End If
        End If

        Return dv
    End Function

    'Changed By Pankaj Malav Previously the filter is with WoTracking ID now changed to Company ID
    Public Function ShowHideComments(ByVal WOTrackingID As Integer, ByVal CompanyID As Integer) As Boolean
        Dim ds As New DataSet
        If Not IsNothing(Session("AdminIssueListing" & Session.SessionID)) Then
            ds = CType(Session("AdminIssueListing" & Session.SessionID), DataSet)
        End If
        Dim dv As DataView
        If ds.Tables.Count > 3 Then
            If ds.Tables(3).Rows.Count > 0 Then
                dv = ds.Tables(3).DefaultView
                If ViewState("Group") = ApplicationSettings.WOGroupCA Then
                    dv.RowFilter = "SupCompany = " & CompanyID
                Else
                    dv.RowFilter = "WOTrackingID = " & WOTrackingID
                End If
            End If
        End If
        If Not IsNothing(dv) Then
            If dv.Count = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If

    End Function

    ''' <summary>
    ''' Method to get the Selected WOIDs
    ''' </summary>
    ''' <remarks>PratikT-19-aug, 2008</remarks>
    Public Sub GetSelectedWOIDs()
        Dim selectedIds As String = ""
        For Each row As DataListItem In dlIssueWOListing.Items
            Dim chkBox As CheckBox = CType(row.FindControl("Check"), CheckBox)
            If Not (chkBox Is Nothing) Then
                If chkBox.Checked Then
                    If selectedIds = "" Then
                        selectedIds = CType(row.FindControl("hdnWOIDs"), HtmlInputHidden).Value
                    Else
                        selectedIds = selectedIds & "," & CType(row.FindControl("hdnWOIDs"), HtmlInputHidden).Value
                    End If
                End If
            End If
        Next
        'Assign Selected to UC Property.
        SelectedWOIDs = selectedIds
    End Sub

    Public Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click
        Dim message As String = txtNote.Text.Trim.Replace(Chr(13), "<BR>")
        Dim Success As DataSet = CommonFunctions.SaveNotes(ViewState("SelectedWorkOrderId"), Session("UserId"), message, False)
        txtNote.Text = ""

        PopulateGrid()

    End Sub

    Public Sub hdnBtnRateWO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnBtnRateWO.Click

        Dim Rating As Integer
        If rdBtnNegative.Checked = True Then
            Rating = -1
        ElseIf rdBtnPositive.Checked = True Then
            Rating = 1
        Else
            Rating = 0
        End If
        Dim Success As DataSet = CommonFunctions.SaveRatingComment(ViewState("SelectedWorkOrderId"), Session("CompanyID"), Session("UserID"), txtComment.Text.Trim, Rating)
        txtComment.Text = ""

        PopulateGrid()

    End Sub
    Public Function ShowHideUserNotes(ByVal WOID As Integer) As String
        If Not IsNothing(HttpContext.Current.Items("tblWOUserNotes")) Then
            Dim dt As DataTable = HttpContext.Current.Items("tblWOUserNotes")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "WOID = " & WOID & " and Comments <> ''"
            If dv.Count = 0 Then
                Return "False"
            Else
                Return "True"
            End If
        Else
            Return "False"
        End If

    End Function
    Public Function GetUserNotesDetail(ByVal WOID As Integer) As DataView
        Dim dv As DataView
        If Not IsNothing(HttpContext.Current.Items("tblWOUserNotes")) Then
            Dim dt As DataTable = HttpContext.Current.Items("tblWOUserNotes")
            dv = dt.DefaultView
            dv.RowFilter = "WOID = " & WOID
            Return dv
        Else
            Return dv
        End If
    End Function


    Protected Sub dlIssueWOListing_RowDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dlIssueWOListing.ItemDataBound

        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim PlatformPrice As Decimal
            Dim WholesalePrice As Decimal
            Dim UpSellPrice As Decimal

            PlatformPrice = DataBinder.Eval(e.Item.DataItem, "PlatformPrice")
            WholesalePrice = DataBinder.Eval(e.Item.DataItem, "WholesalePrice")
            UpSellPrice = DataBinder.Eval(e.Item.DataItem, "UpSellPrice")
            If (PlatformPrice > (WholesalePrice + UpSellPrice)) Then
                CType(e.Item.FindControl("SpanPP"), HtmlGenericControl).Style.Add("color", "Red")
            End If

            If (DataBinder.Eval(e.Item.DataItem, "TrackSP") = "1") Then
                Dim col As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#FFFFBC")
                e.Item.BackColor = col
                'e.Item.Attributes.Add("onMouseOver", "style.background='#FFEBC6';")
                'e.Item.Attributes.Add("onMouseOut", "style.background='#FFFFBC';")
            Else
                Dim OriginalCol As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#EDEDED")
                e.Item.BackColor = OriginalCol
            End If
            If (DataBinder.Eval(e.Item.DataItem, "IsWatch") = "1") Then
                Dim col As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#F78E18")
                e.Item.BackColor = col
                'e.Item.Attributes.Add("onMouseOver", "style.background='#FFEBC6';")
                'e.Item.Attributes.Add("onMouseOut", "style.background='#FFFFBC';")
            Else
                Dim OriginalCol As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#EDEDED")
                e.Item.BackColor = OriginalCol
            End If

        End If
    End Sub
    Public Function GetActionIcon(ByVal NtCnt As Integer, ByVal Rating As Integer) As String
        Dim ActionIcon As String
        ActionIcon = "~/Images/Icons/Action.gif"
        If (NtCnt > 0 And Rating = 2) Then
            ActionIcon = "~/Images/Icons/Action-note.gif"
        ElseIf (NtCnt > 0 And Rating = 0) Then
            ActionIcon = "~/Images/Icons/Action-note-neutral.gif"
        ElseIf (NtCnt > 0 And Rating = 1) Then
            ActionIcon = "~/Images/Icons/Action-note-positive.gif"
        ElseIf (NtCnt > 0 And Rating = -1) Then
            ActionIcon = "~/Images/Icons/Action-note-negative.gif"
        ElseIf (NtCnt = 0 And Rating = 0) Then
            ActionIcon = "~/Images/Icons/Action-neutral.gif"
        ElseIf (NtCnt = 0 And Rating = 1) Then
            ActionIcon = "~/Images/Icons/Action-positive.gif"
        ElseIf (NtCnt = 0 And Rating = -1) Then
            ActionIcon = "~/Images/Icons/Action-negative.gif"
        End If
        Return ActionIcon
    End Function
    Private Sub hdnBtnAction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnBtnAction.Click
        Dim Note As String = txtNoteAction.Text.Trim.Replace(Chr(13), "<BR>")
        Dim Comments As String = txtCommentAction.Text.Trim.Replace(Chr(13), "<BR>")
        Dim IsFlagged As Boolean = chkMyRedFlag.Checked
        Dim IsWatched As Boolean = chkMyWatch.Checked

        Dim Rating As Integer
        If rdBtnActionNegative.Checked = True Then
            Rating = -1
        ElseIf rdBtnActionPositive.Checked = True Then
            Rating = 1
        ElseIf rdBtnActionNeutral.Checked = True Then
            Rating = 0
        Else
            Rating = 2
        End If
        Dim Success As DataSet = CommonFunctions.SaveActionCommentNotes(ViewState("SelectedWorkOrderId"), Session("UserID"), Note, Comments, Session("CompanyID"), Rating, IsFlagged, IsWatched, chkShowClient.Checked, chkShowSupplier.Checked)
        txtNoteAction.Text = ""
        txtCommentAction.Text = ""

        PopulateGrid()

    End Sub

    Private Sub PopulateGrid()
        Dim aObj(6) As Object
        aObj(0) = CInt(CompID)
        aObj(1) = 0
        aObj(2) = False
        aObj(3) = 0
        aObj(4) = False
        aObj(5) = Session("IssueBy")
        aObj(6) = Session("IssueWorkorderID")
        Dim delPopulate As New DelPopulateObject(AddressOf Populate)
        delPopulate.DynamicInvoke(aObj)
    End Sub
    'Public Sub lnkbtnWatch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnWatch.Click
    '    ws.WSWorkOrder.AddDeleteWatchedWO(ViewState("WOID"), Session("UserID"), "Add")
    '    ViewState("IsWatch") = 1
    '    lnkbtnWatch.Visible = False
    '    lnkbtnUnwatch.Visible = True
    'End Sub
    'Public Sub lnkbtnUnwatch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnUnwatch.Click
    '    ws.WSWorkOrder.AddDeleteWatchedWO(ViewState("WOID"), Session("UserID"), "Remove")
    '    ViewState("IsWatch") = 0
    '    lnkbtnWatch.Visible = True
    '    lnkbtnUnwatch.Visible = False
    'End Sub
End Class