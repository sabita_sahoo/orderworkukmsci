<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCAutoMatchSettings.ascx.vb" Inherits="Admin.UCAutoMatchSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>


<asp:Panel ID="PnlCompanyProfile" runat="server"  >
 <asp:Panel ID="pnlSubmitForm" runat="server" visible="true">
 
    <table cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
            <tr>
              <td width="165" class="txtWelcome paddingL10"><div id="divButton" style="width: 165px;  ">
                  <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                  <a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" title="Back to Company Profile" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8" /><strong>Back to Company Profile</strong></a>
                  <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
              </div></td>                  
            </tr>
    </table>

    <div style="margin:0px 5px 0px 5px;" id="divMainTab">
      <div class="paddedBox"  id="divProfile" >
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td align="left" class="HeadingRed"><strong>AutoMatch Settings</strong></td>
            <td align="left" class="padTop17"><table id="tblSaveTop" runat="server" cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                <tr>
                  <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveTop" OnClientClick="javascript:callSetFocusValidation('UCCompanyProfile1')" CausesValidation="false"  CssClass="txtListing" runat="server" TabIndex="67"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                  <td width="10"></td>
                  <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetTop" CausesValidation="false" runat="server" TabIndex="68"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
        </table>
		
        <div class="divWorkOrder" >
	        <div class="WorkOrderTopCurve"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/><strong>Settings</strong></div>	
		        <table width="100%" cellspacing="0" cellpadding="9" border="0">
			        <tr>
				        <td>							 
				         <br /><asp:CheckBox CssClass="txtNavigation" ID="chkAutoMatch" runat="server" Text="&nbsp;<b>Enable AutoMatch</b>" AutoPostBack="false" OnClick="javascript:toggleView()"></asp:CheckBox><br /><br />
					         <asp:Panel ID="pnlSettings"  runat="server">
					            <input runat="server" type="checkbox" id="chkRefCheck" disabled="disabled" />&nbsp;<span class="txtNavigation">Reference Checked</span><br />
					            <input runat="server" type="checkbox" id="chkSecurity" disabled="disabled" />&nbsp;<span class="txtNavigation">Security Cleared</span><br />
					            <input runat="server" type="checkbox" id="chkCRB" disabled="disabled" />&nbsp;<span class="txtNavigation">DBS</span><br />
					            <input runat="server" type="checkbox" id="chkbxEngCRBCheck" disabled="disabled" />&nbsp;<span class="txtNavigation">Engineer DBS Checked</span><br />
					            <input runat="server" type="checkbox" id="chkbxEngCSCSCheck" disabled="disabled" />&nbsp;<span class="txtNavigation">Engineer CSCS Checked</span><br />
					            <input runat="server" type="checkbox" id="chkbxEngUKSecurityCheck" disabled="disabled" />&nbsp;<span class="txtNavigation">Engineer UK Security Checked</span><br />
					            <input runat="server" type="checkbox" id="chkbxEngRightToWork" disabled="disabled" />&nbsp;<span class="txtNavigation">Engineer Right to Work in UK</span><br />
						        <br />
						        <br /><span class="txtNavigation">Nearest To: </span><asp:DropDownList CssClass="txtNavigation" ID="ddlNearestTo" runat="server" Enabled="false"></asp:DropDownList>
					          </asp:Panel>
					          <asp:Panel ID="pnlSuccess"  runat="server" Visible="false">
						        <table width="500" border="0" cellspacing="0" cellpadding="10" height="100">
						          <tr>
							        <td align="center"><asp:Label runat="server" ID="lblMsg"></asp:Label></td>
						          </tr>
						        </table>
					          </asp:Panel>
				        </td>
			        </tr>
		        </table>
	        <div class="WorkOrderBottomCurve"><img src="Images/Curves/BtmLeft_Curve.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
          </div>
        	
        
        
         
          <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
              <td align="left" class="HeadingRed">&nbsp;</td>
              <td align="left" class="padTop17"><table id="tblSaveBottom" runat="server" cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                  <tr>
                    <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveBottom" CausesValidation="false" OnClientClick="callSetFocusValidation('UCCompanyProfile1')"  CssClass="txtListing" runat="server" TabIndex="63"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                        </tr>
                    </table></td>
                    <td width="10"></td>
                    <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetBottom"  CausesValidation="false" runat="server" TabIndex="64"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                        </tr>
                    </table></td>
                  </tr>
              </table></td>
            </tr>
          </table>
       
      </div>
    </div>
  </asp:Panel>
</asp:Panel> 