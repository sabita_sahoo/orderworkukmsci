<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCAdminProduct.ascx.vb" Inherits="Admin.UCAdminProduct" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Outer.ascx" %>


<!--

dont go

-->
<script language="javascript" src="JS/Scripts.js"  type="text/javascript"></script>
<script>
function DisableOrder()
{
    if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_chkbxIsDeleted").checked == true)
    {
        document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_hdnListOrder").value = document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtListingOrder").value
        document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtListingOrder").value = "0"
        document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtListingOrder").disabled = true;
    }
    else
    {
        if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_hdnListOrder").value != "0")
        {
            document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtListingOrder").value = document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_hdnListOrder").value
        }
        else
        {
            document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtListingOrder").value = ""
        }
        document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtListingOrder").disabled = false;
    }
}
function checkNonZero()
{
    if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtListingOrder").value == "0")
    {
     alert("Enter an integer value greater than 0")
     document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtListingOrder").value = ""
    }  
}
</script>
<asp:UpdatePanel ID="UpdatePanelCreateWorkOrder" runat="server">
<ContentTemplate>
<asp:Literal ID="LitAlert" runat="server"></asp:Literal>
<asp:Panel ID="pnlWoForm" runat="server" Visible="true">

<input type="button" id="btnFocus" name="btnFocus" class="hdnClass" runat="server"  />
<INPUT id="hdnListOrder" type="hidden" name="hdnListOrder" runat="server" value="0"> 
	<input type="hidden" runat="server" id="hdnvatvalue" ></input>
    <input type="hidden" runat="server" id="hdnWPvatvalue" value="0.00" ></input>
    <input type="hidden" runat="server" id="hdnCPvatvalue" value="0.00" ></input>
<div id="divValidationMain" class="divValidation" runat=server visible="false" >
	<div class="roundtopVal">
	  <blockquote>
	    <p><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	    </blockquote>
	</div>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr valign="middle">
				<td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				<td class="validationText"><div  id="divValidationMsg"></div>
				<asp:Label ID="lblError"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>
				<asp:ValidationSummary id="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList"></asp:ValidationSummary>
				</td>
				<td width="20">&nbsp;</td>
			</tr>
		</table>
	<div class="roundbottomVal">
	  <blockquote>
	    <p><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	    </blockquote>
	</div>
</div>

<TABLE width="100%" border="0" align="center" cellPadding="0" cellSpacing="0" style="PADDING-RIGHT:0px; PADDING-LEFT:0px; PADDING-BOTTOM:16px; MARGIN:0px; PADDING-TOP:0px">
	<TR>
	<TD  align="right" vAlign="top"><asp:Label ID="lblMessage" runat="server" CssClass="bodytxtRedSmall " style="font-weight:bold;"></asp:Label></TD>
	<TD width="100" align="right" vAlign="top" id="tdCopyServiceTop" runat="server">
    <TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0" width="130">
		<TR>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			<TD width="120" class="txtBtnImage">
			  <asp:LinkButton ID="btnCopyService" runat="server" class="txtListing" OnClick="CopyService" CausesValidation="false" > <img src="Images/Icons/Copy.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Copy Service&nbsp;</asp:LinkButton>
		  </TD>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		</TR>
	</TABLE>
    </TD>
    <TD width="10"></TD>
	<TD width="130" align="right" vAlign="top">
	<TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0" width="130">
		<TR>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			<TD width="120" class="txtBtnImage">
			  <asp:LinkButton ID="btnConfirmTop" runat="server" class="txtListing" CausesValidation="false" OnClientClick="callSetFocusValidation('UCTemplateWO1')" > <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:LinkButton>
		  </TD>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		</TR>
	</TABLE>
	</TD>
	<TD width="10"></TD>
	<TD vAlign="top" align="right" width="130">
		<TABLE cellSpacing="0" cellPadding="0"  bgColor="#f0f0f0" border="0">
			<TR>
				<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
				<TD class="txtBtnImage" width="130">
					<asp:Linkbutton id="btnResetTop"  runat="server" CausesValidation="false" CssClass="txtListing"><img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton>
				</TD>
				<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
			</TR>
		</TABLE>
	</td>
	<td width="10">&nbsp;</td>
	<td vAlign="top" align="right" width="60">
	    <TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0">
		<TR>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			<TD width="80" class="txtBtnImage">
			  <asp:LinkButton ID="btnCancelTop" runat="server" class="txtListing" CausesValidation="false"> <img src="Images/Icons/Icon-Cancel.gif" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
		  </TD>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		</TR>
	</TABLE>
	</td>
	</TR>
  </TABLE>



<div class="tabDivOuter" style="width:1200px;" id="divMainTab">
<div class="roundtopLightGreyCurve"  ><img src="Images/Curves/LightGrey-LTC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
<DIV id="divProfile" class="paddedBox" style="height:100%" >
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="35" class="HeadingRed">
	   <strong><asp:Label ID="lblHeading" runat="server" Text="Add / Edit Service" ></asp:Label></strong>
       <div style="float:right;text-align:right;">
       <div> <a id="lnkAddEditTimeBuilderQA" runat="server" class="footerTxtSelected">TimeBuilder Questions</a></div>
       <div style="margin-left:20px;"> <a id="lnkAddTBQRedirection" runat="server" class="footerTxtSelected">TimeBuilder Redirection</a></div>
       </div>
    </td>
  </tr>
  <tr>
    <td >
	   	<!--Product Details -->
        <div id="divWODetails" class="divWorkOrder" runat=server>
        <div style="clear:both;"></div>
            <div class="WorkOrderTopCurve" style="height:22px;"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/>&nbsp;<b>Service Details</b></div>
                <table width="100%"  border="0" cellspacing="0" cellpadding="10">
                  <tr>
                    <td>
                            <table width="100%"  border="0" cellpadding="0" cellspacing="0">
                                <tr >
                                  <td width="415" height="15" class="formLabelGrey" valign="top">&nbsp;Service Name<SPAN class="bodytxtValidationMsg" id="CompanyNameIndicator" runat="server"> *</SPAN>
															  <asp:RequiredFieldValidator id="rqCompanyName" runat="server" ErrorMessage="Please enter Service Name" ForeColor="#EDEDEB"
																	ControlToValidate="txtTitle">*</asp:RequiredFieldValidator></td>
								  <td valign="top" class="formLabelGrey" ></td>
                                </tr>
                                <tr >
                                  <td valign="top">
	                                <asp:TextBox id="txtTitle" runat="server" Width="375" CssClass="formFieldGrey " TabIndex="1"></asp:TextBox>
	                              </td>
	                              <td valign="top" class="formLabelGrey" >
	                                <asp:CheckBox ID="chkBoxFreesatService" valign="top" runat="server" Text="Freesat Service" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkbxIsDeleted" valign="top" runat="server" onclick="Javascript:DisableOrder();" Text="Mark Service As Deleted" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkReviewBids" valign="top" runat="server" Text="Allow Review Bids" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkIsHidden" valign="top" runat="server" Text="Mark Service As Hidden" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkIsWaitingToAutomatch" valign="top" runat="server" Text="Waiting to go through automatch" />
	                              </td>
                                </tr>
                        </table>
                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr >
                              <td width="200" height="15" class="formLabelGrey" valign="top">&nbsp;Work Category<SPAN class="bodytxtValidationMsg">*</SPAN> 
	                          <asp:RequiredFieldValidator ID="rqWOType" runat="server" ErrorMessage="Please enter Work Request type" ForeColor="#EDEDEB"
														                        ControlToValidate="ddlWOType">*</asp:RequiredFieldValidator></td>
                                <td width="10">&nbsp;</td>
                              <td width="200" height="15" class="formLabelGrey" valign="top">&nbsp;Work Sub Category</td>
                              <td width="10">&nbsp;</td>
                             
                                <td height="15" width="100" class="formLabelGrey" valign="top">&nbsp;Listing Order<SPAN class="bodytxtValidationMsg">*</SPAN>
                              <asp:RequiredFieldValidator ID="rgFieldValidatorListOrder" runat="server" ErrorMessage="Enter a value to denote the order in which the service is to be listed" ForeColor="#EDEDEB"
														                        ControlToValidate="txtListingOrder">*</asp:RequiredFieldValidator>
				              <asp:regularexpressionvalidator id="regfieldvallistingorder" runat="server" errormessage="please enter numeric listing order" 
				              validationexpression="^\d+$" controltovalidate="txtlistingorder" forecolor="#ededeb">*</asp:regularexpressionvalidator>
                              </td>
                              <td width="10">&nbsp;</td>
                               <td  height="15" class="formLabelGrey" valign="top">&nbsp;Start Date Deferral<SPAN class="bodytxtValidationMsg">*</SPAN>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter Start Date Deferral" ForeColor="#EDEDEB"
														                        ControlToValidate="txtStartDateDef">*</asp:RequiredFieldValidator>
				              <asp:RegularExpressionValidator ID="rgFieldValidator" runat="server" ErrorMessage="Please enter valid Start Date Deferral" 
				                        ValidationExpression="^[1]{1}[0-9]{0,1}$|^[0-9]{1}$" ControlToValidate="txtStartDateDef" ForeColor="#EDEDEB">*</asp:RegularExpressionValidator>
                              </td>
                               
                              
                            </tr>
                            <tr valign="top">
                            
	 		                        <td  height="28" valign="top"><asp:DropDownList id="ddlWOType" runat="server" CssClass="formFieldGrey" Width="200" TabIndex="4" autopostback="true"></asp:DropDownList></td>
      		                       <td width="10">&nbsp;</td>
      		                        <td height="28" valign="top"><asp:DropDownList id="ddlWOSubType"  runat="server" CssClass="formFieldGrey" Width="200" TabIndex="5" ></asp:DropDownList></td>
      		                        <td width="10">&nbsp;</td>      		                        
                                   <td height="28" valign="top" >
                                   <%--<input type="text" id="txtListingOrder" onblur="Javascript:checkNonZero();"  size="15" runat="server" class="formFieldGrey" />--%>
                                    <asp:TextBox ID="txtListingOrder" onblur="Javascript:checkNonZero();" Width="100" runat="server" CssClass="formFieldGrey" ></asp:TextBox>
                                    </td>        
		                            <td width="10">&nbsp;</td>                                            
                                    <td height="28" valign="top">
                                     <asp:TextBox ID="txtStartDateDef" runat="server" MaxLength="2" Text="1" CssClass="formFieldGrey" Width="100"></asp:TextBox>
                                     <asp:DropDownList id="ddStartDefTime"  runat="server" CssClass="formFieldGrey" TabIndex="5" >
                                     <asp:ListItem Text="Hour(s)" Value="Hours"></asp:ListItem> 
                                     <asp:ListItem Text="Day(s)" Value="Days"></asp:ListItem> 
                                     </asp:DropDownList>
                                    </td>
                                    
                            </tr>                            
                        </table>
						
						<table width="100%"  border="0" cellpadding="0" cellspacing="0">
                                <tr >
                                  <td width="420" height="15" class="formLabelGrey" valign="top">&nbsp;Title<SPAN class="bodytxtValidationMsg" id="PartnerLevelIndicator" runat="server"> *</SPAN>
																			<asp:RequiredFieldValidator id="rqPartnerLevel" runat="server" ErrorMessage="Please Enter Service Title" ForeColor="#EDEDEB"
																				ControlToValidate="txtInvoiceTitle">*</asp:RequiredFieldValidator></td>
								  <td class="formLabelGrey" valign="top" width="178" ><asp:Label ID="lbltxtBillingLocation" runat="server" Text="Billing Location"></asp:Label></td>
                                  <td class="formLabelGrey" valign="top"><asp:Label ID="tblsatdef" runat="server" Text="Saturday Deferral Date"></asp:Label></td>
                                </tr>
                                <tr >
                                  <td height="24" valign="top">
	                                <asp:TextBox id="txtInvoiceTitle" runat="server" CssClass="formFieldGrey" MaxLength="600" Width="400" TabIndex="1"></asp:TextBox>
	                              </td>
	                              <td valign="top"><asp:DropDownList id="drpdwnProductLocation"  runat="server" CssClass="formFieldGrey" Width="160" TabIndex="5" ></asp:DropDownList></td>
                                   <td valign="top"><asp:TextBox id="txtsatdef"  runat="server" Width="110" MaxLength="1" Text="0" CssClass="formFieldGrey" TabIndex="6" ></asp:TextBox>
                                   
                                   <asp:RegularExpressionValidator ID="regsatdef" runat="server" ErrorMessage="Please enter valid Saturday Deferral Date" 
				                        ValidationExpression="^[0-9]{1}$|^[1-4]{1}[0-9]{1}$|^50$" ControlToValidate="txtsatdef" ForeColor="#EDEDEB">*</asp:RegularExpressionValidator>
          
           <asp:RequiredFieldValidator ID="reqsatdef" runat="server" ErrorMessage="Please enter valid Saturday Deferral Date" ForeColor="#EDEDEB"
														                        ControlToValidate="txtsatdef">*</asp:RequiredFieldValidator>
                                   </td>
                                </tr>
                        </table>

                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr>
                              <td height="20" class="formLabelGrey">Scope of Work<SPAN class="bodytxtValidationMsg">*</SPAN> <asp:RequiredFieldValidator id="rqWOLongDesc" runat="server" ErrorMessage="Please enter Scope of Work"
																				                            ForeColor="#EDEDEB" ControlToValidate="txtWOLongDesc">*</asp:RequiredFieldValidator></td>
                              </tr>
                              
                              <tr>
                                <td><asp:TextBox id="txtWOLongDesc" runat="server" CssClass="formFieldGrey" Height="142" Width="706" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"
																				                            Rows="4" TabIndex="7"></asp:TextBox></td>
                              </tr>
                              <tr>
                              <td height="20" class="formLabelGrey">Client Scope</td>
                              </tr>
                              <tr>
                                <td><asp:TextBox id="txtClientScope" runat="server" CssClass="formFieldGrey" Height="142" Width="706" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"
																				                            Rows="4" TabIndex="8"></asp:TextBox></td>
                              </tr>
                              <tr>
                                <td height="22" valign="bottom" class="formLabelGrey">Special instructions to the Engineers</td>
                              </tr>
                              <tr>
                                <td><asp:TextBox id="txtSpecialInstructions" runat="server" CssClass="formFieldGrey" Height="60" Width="706" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);"
																				                            Rows="4" TabIndex="9"></asp:TextBox></td>
                              </tr>
                        </table>
                    </td>
                  </tr> 
                </table>               
	        <div class="WorkOrderBottomCurve"><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
	    </div>
    <!--Product Pricing -->
        <div id="divWOPrice" class="divWorkOrder" runat="server">
	        <div class="WorkOrderTopCurve" style="height:22px;"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/>&nbsp;<strong>Pricing</strong></div>
	         <table width="725"  border="0" cellspacing="0" cellpadding="10">
                  <tr>
                    <td>
                   		
                           <asp:Panel runat="server" ID="pnlWholesalePrice" >
                            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                  <td width="153" class="formLabelGrey">&nbsp;Wholesale Price (Include. VAT)<SPAN class="bodytxtValidationMsg">*</SPAN> <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter WholeSale Price (Include. VAT)"
																				                            ForeColor="#EDEDEB" ControlToValidate="txtSpendLimitWP">*</asp:RequiredFieldValidator>
																				                            <asp:RegularExpressionValidator runat="Server" ID="RegEx1" ErrorMessage="Please enter the Wholesale Price amount (Include. VAT) as either an integer or as an amount to 2 decimal places"
																				                            ForeColor="#EDEDEB" ControlToValidate="txtSpendLimitWP" ValidationExpression="^-?\d*(\.\d+)?$">*</asp:RegularExpressionValidator>


                                                                                                                                                   </td>
                                  <td width="153" class="formLabelGrey">
                                  &nbsp;&nbsp;&nbsp;Wholesale Price (excl. VAT):<SPAN class="bodytxtValidationMsg">*</SPAN>&nbsp;
                                                                                                            <asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter WholeSale Price (excl. VAT)" ForeColor="#EDEDEB" ControlToValidate="lblWOincludvat">*</asp:RequiredFieldValidator>
<asp:RegularExpressionValidator runat="Server" ID="RegularExpressionValidator1" ErrorMessage="Please enter the Wholesale Price amount (excl. VAT) as either an integer or as an amount to 2 decimal places" ForeColor="#EDEDEB" ControlToValidate="lblWOincludvat" ValidationExpression="^-?\d*(\.\d+)?$">*</asp:RegularExpressionValidator>
 

                                                                                                            

										</td>
                                </tr>

                                <tr valign="top">
                                  <td width="153" ><span class="formLabelGrey">&pound;</span>
                                      <asp:TextBox id="txtSpendLimitWP" Text="0.00" runat="server"  onblur="javascript:excludeVat(this.id);" CssClass="formFieldGrey width140" style="text-align:right" TabIndex="14"></asp:TextBox></td>
                                  <td width="153" class="formLabelGrey">

                                  <span class="formLabelGrey">&pound;</span><asp:TextBox runat="server" CssClass="formFieldGrey width140" ID="lblWOincludvat" Text="0.000" onblur="javascript:includeVat(this.id);" ></asp:TextBox>
                                     </td>
                                </tr>
                                <tr>
                                    <td width="153">&nbsp;</td>
                                    <td width="148">&nbsp;</td>
                                </tr>
                                <tr>
                                <td width="153" class="formLabelGrey">&nbsp;Customer Price (Include. VAT) <SPAN class="bodytxtValidationMsg">*</SPAN> <asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter Customer Price(Include. VAT)"
																				                            ForeColor="#EDEDEB" ControlToValidate="txtCustPrice">*</asp:RequiredFieldValidator>
																				                            <asp:RegularExpressionValidator runat="Server" ID="RegEx2" ErrorMessage="Please enter the Customer Price amount(Include. VAT) as either an integer or as an amount to 2 decimal places"
																				                            ForeColor="#EDEDEB" ControlToValidate="txtCustPrice" ValidationExpression="^-?\d*(\.\d+)?$">*</asp:RegularExpressionValidator></td>
                                    <td width="148" class="formLabelGrey">&nbsp;&nbsp;&nbsp;Customer Price (excl. VAT):<SPAN class="bodytxtValidationMsg">*</SPAN>
                                                                                                            <asp:RequiredFieldValidator id="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter Customer Price(excl. VAT)"
ForeColor="#EDEDEB" ControlToValidate="lblCPincludvat">*</asp:RequiredFieldValidator>
<asp:RegularExpressionValidator runat="Server" ID="RegularExpressionValidator2" ErrorMessage="Please enter the Customer Price amount(excl. VAT) as either an integer or as an amount to 2 decimal places"
ForeColor="#EDEDEB" ControlToValidate="lblCPincludvat" ValidationExpression="^-?\d*(\.\d+)?$">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr>
                                <td width="153">&nbsp;<span class="formLabelGrey">&pound;</span> <asp:TextBox id="txtCustPrice" Text="0.00" onblur="javascript:excludeVat(this.id);" runat="server" CssClass="formFieldGrey width140" style="text-align:right" TabIndex="14"></asp:TextBox></td>
                                    <td width="148">&nbsp;<span class="formLabelGrey">&pound;</span><asp:TextBox ID="lblCPincludvat" CssClass="formFieldGrey width140"  onblur="javascript:includeVat(this.id);" runat="server" Text="0.000" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td width="165">&nbsp;</td>
                                    <td width="148">&nbsp;</td>
                                </tr>

                               </table>
                           </asp:Panel>
                           <asp:Panel runat="server" ID="pnlPlatformPrice" >
                            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                  <td width="165" class="formLabelGrey">&nbsp;Portal Price (excl. VAT)<SPAN class="bodytxtValidationMsg">*</SPAN> <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter Portal Price"
																				                            ForeColor="#EDEDEB" ControlToValidate="txtSpendLimitPP">*</asp:RequiredFieldValidator>
																				                            <asp:RegularExpressionValidator runat="Server" ID="RegEx3" ErrorMessage="Please enter the Portal Price amount as either an integer or as an amount to 2 decimal places"
																				                            ForeColor="#EDEDEB" ControlToValidate="txtSpendLimitPP" ValidationExpression="^-?\d*(\.\d+)?$">*</asp:RegularExpressionValidator>
                                  </td>
                                  <td width="165" class="formLabelGrey">&nbsp;PP Per Rate(excl. VAT)<SPAN class="bodytxtValidationMsg">*</SPAN>
                                   <asp:RegularExpressionValidator runat="Server" ID="RegExPPPerRate" ErrorMessage="Please enter the PP Per Rate amount as either an integer or as an amount to 2 decimal places"
																				                            ForeColor="#EDEDEB" ControlToValidate="txtPPPerRate" ValidationExpression="^-?\d*(\.\d+)?$">*</asp:RegularExpressionValidator>
                                  </td>
                                  <td width="148" class="formLabelGrey"  id="td3" runat="server">&nbsp;</td>
                                </tr>

                                <tr valign="top">
                                  <td width="165" ><span class="formLabelGrey">&pound;</span>
                                      <asp:TextBox id="txtSpendLimitPP" Text="0" runat="server" CssClass="formFieldGrey width140" style="text-align:right" TabIndex="17"></asp:TextBox></td>
                                 <td width="165" ><span class="formLabelGrey">&pound;</span>
                                      <asp:TextBox id="txtPPPerRate" Text="0" runat="server" CssClass="formFieldGrey width140" style="text-align:right" TabIndex="17"></asp:TextBox>
                                       &nbsp;<asp:DropDownList id="ddPPPerRate" runat="server" CssClass="formFieldGrey" Width="70">
								                                  <asp:ListItem Text="Per Job" Value="Per Job"></asp:ListItem>
								                                  <asp:ListItem Text="Per Hour" Value="Per Hour"></asp:ListItem>
                                                                  <asp:ListItem Text="Per Day" Value="Per Day"></asp:ListItem>
                                                                  <asp:ListItem Text="Per Week" Value="Per Week"></asp:ListItem>                                                      
								                                </asp:DropDownList>    
                                  </td>
                                  <TD width="148" height="24" align="left" class="formLabelGrey" id="td4" runat="server"></TD>
                                </tr>
                                <tr>
                                    <td width="165">&nbsp;</td>
                                    <td width="148">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                               </table>
                           </asp:Panel>
                            <div style="clear:both">
							     
                     </td>
                    </tr>
                   </table>      
	        <div class="WorkOrderBottomCurve"><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
	    </div>
	    <div id="divFinance" class="divWorkOrder" runat="server" style="display:none">
	        <div class="WorkOrderTopCurve" style="height:22px;"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/>&nbsp;<strong>Finance Settings</strong></div>
	        <table width="725" cellspacing="0" cellpadding="9" border="0">
			        <tr>
				        <td class="gridTextNew" style="width:150px;">Supplier fee (%)</td>
				        <td>
				        <asp:TextBox ID="txtSPFee"  runat="server" MaxLength="5" CssClass="formFieldRight width60" onblur='FormatttedAmount(this.id); ValidateFees();' Text="0" ></asp:TextBox><br />
				        </td><td><input runat="server" type="checkbox" id="chkPrefSP"/>&nbsp;<span class="txtNavigation">Apply charge to Preferred Supplier</span></span></td></tr>
				        <tr></tr>
			</table>
	            
	        <div class="WorkOrderBottomCurve"><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
	        </div>
	<!--WO LOC -->	
	   <div style="width:100%;background-color:#F4F5EF;">
	   <div class="WorkOrderTopCurve" style="height:22px;"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/>&nbsp;<strong>Attachments</strong></div>
	    <div id="Div1" style="width:725px;">
             <uc2:UCFileUpload id="UCFileUpload6" runat="server" Control="UCAdminProduct1_UCFileUpload6" 
												Type="WOProduct"
												AttachmentForSource="WOProduct"  UploadCount="1" 
												ShowExistAttach="False" ShowNewAttach="True"
												ShowUpload="True" 
												MaxFiles="12"></uc2:UCFileUpload>                                             
                                                
        </div>                        
                                <div class="WorkOrderBottomCurve"><img src="Images/Curves/Attach-BL.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
     </div>
     
     <!--Client Question -->
     <div class="divWorkOrder" style="margin-top:17px;">
	        <div class="WorkOrderTopCurve" style="height:22px;"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/><strong>Questions to the Supplier</strong></div>
	         <table width="100%" cellspacing="9" cellpadding="0" border="0">
	                <tr>
		            <td style="width:680px;" class="gridTextNew">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Enter questions below.</strong> </td>
		            </tr>
			        <tr>
				        <td style="width:680px;">				        
				       <span class="gridTextNew">Yes/No type questions(Maximum 3)</span><br />
				       <table><tr><td style="width:380px;" >
				         <input runat="server"  type="checkbox" id="Qchk1" style="vertical-align:middle;" onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAdminProduct1_reqfldQ1","","ctl00_ContentPlaceHolder1_UCAdminProduct1_spanQ1","","ctl00_ContentPlaceHolder1_UCAdminProduct1_QChkMan1")' tabindex="45"/>&nbsp;<asp:TextBox ID="txtQ1"  MaxLength="50" TabIndex="46" runat="server" CssClass="formField width150" Width="340"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanQ1" runat="server">&nbsp;*</span>
				         </td>
				         <td>
				         <input runat="server"  type="checkbox" id="QChkMan1" style="vertical-align:middle;" disabled="disabled"/><span class="txtNavigation">Set as mandatory</span>
				         <asp:RequiredFieldValidator  ID="reqfldQ1" runat="server" ControlToValidate="txtQ1"  Display="None" Enabled="false" ErrorMessage="Please enter question 1">*</asp:RequiredFieldValidator> 
				         </td>
				         </tr>
				         <tr>
				         <td>
					     <input runat="server"  type="checkbox" id="Qchk2" style="vertical-align:middle;" onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAdminProduct1_reqfldQ2","","ctl00_ContentPlaceHolder1_UCAdminProduct1_spanQ2","","ctl00_ContentPlaceHolder1_UCAdminProduct1_QChkMan2")' tabindex="47"/>&nbsp;<asp:TextBox ID="txtQ2"  MaxLength="50" TabIndex="48" runat="server" CssClass="formField width150" Width="340"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanQ2" runat="server">&nbsp;*</span> 
					     </td>
					     <td>
					     <input runat="server"  type="checkbox" id="QChkMan2" style="vertical-align:middle;" disabled="disabled"/><span class="txtNavigation">Set as mandatory</span>
					     <asp:RequiredFieldValidator  ID="reqfldQ2" runat="server" ControlToValidate="txtQ2" Display="None" Enabled="false" ErrorMessage="Please enter question 2"></asp:RequiredFieldValidator>
					     </td>
					     </tr>
					     <tr>
					     <td>
					     <input runat="server"  type="checkbox" id="Qchk3" style="vertical-align:middle;" onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAdminProduct1_reqfldQ3","","ctl00_ContentPlaceHolder1_UCAdminProduct1_spanQ3","","ctl00_ContentPlaceHolder1_UCAdminProduct1_QChkMan3")' tabindex="49"/>&nbsp;<asp:TextBox ID="txtQ3"  MaxLength="50" TabIndex="50" runat="server" CssClass="formField width150" Width="340"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanQ3" runat="server">&nbsp;*</span>
					     </td>
					     <td>
					     <input runat="server"  type="checkbox" id="QChkMan3" style="vertical-align:middle;" disabled="disabled"/><span class="txtNavigation">Set as mandatory</span>
					     <asp:RequiredFieldValidator  ID="reqfldQ3" runat="server" ControlToValidate="txtQ3" Display="None" Enabled="false" ErrorMessage="Please enter question 3"></asp:RequiredFieldValidator>
					     </td>
					     </tr>
					     </table>
					     <span class="gridTextNew">Time based questions(Maximum 2)</span> <br />
					     <table><tr><td style="width:380px;" >
					     <input runat="server"  type="checkbox" id="Qchk4" style="vertical-align:middle;" onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAdminProduct1_reqfldQ4","","ctl00_ContentPlaceHolder1_UCAdminProduct1_spanQ4","","ctl00_ContentPlaceHolder1_UCAdminProduct1_QChkMan4")' tabindex="51"/>&nbsp;<asp:TextBox ID="txtQ4"  MaxLength="50" TabIndex="52" runat="server" CssClass="formField width150" Width="340"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanQ4" runat="server">&nbsp;*</span>
					     </td><td>
					     <input runat="server"  type="checkbox" id="QChkMan4" style="vertical-align:middle;" disabled="disabled"/><span class="txtNavigation">Set as mandatory</span>
					     <asp:RequiredFieldValidator  ID="reqfldQ4" runat="server" ControlToValidate="txtQ4" Display="None" Enabled="false" ErrorMessage="Please enter question 4"></asp:RequiredFieldValidator> </td>
					     </tr><tr><td>
					     <input runat="server"  type="checkbox" id="Qchk5" style="vertical-align:middle;" onclick='javascript:EnableDisableFields(this.id,"ctl00_ContentPlaceHolder1_UCAdminProduct1_reqfldQ5","","ctl00_ContentPlaceHolder1_UCAdminProduct1_spanQ5","","ctl00_ContentPlaceHolder1_UCAdminProduct1_QChkMan5")' tabindex="53"/>&nbsp;<asp:TextBox ID="txtQ5"  MaxLength="50" TabIndex="54" runat="server" CssClass="formField width150" Width="340"></asp:TextBox><span style="width:10px;font-size:12px;color:Red;display:none;" id="spanQ5" runat="server">&nbsp;*</span></td>
					     <td><input runat="server"  type="checkbox" id="QChkMan5" style="vertical-align:middle;" disabled="disabled"/><span class="txtNavigation">Set as mandatory</span>
					     <asp:RequiredFieldValidator  ID="reqfldQ5" runat="server" ControlToValidate="txtQ5" Display="None" Enabled="false" ErrorMessage="Please enter question 5"></asp:RequiredFieldValidator> 
					     <asp:CustomValidator runat="server" ID="custvalQ"  Display="None" ErrorMessage="Questions can not be duplicated" ></asp:CustomValidator></td>           
					     </tr>
				        </table>
				        </td>
				      </tr>
				      <tr><td>
				 <div style="font-family:Geneva, Arial, Helvetica, sans-serif; font-size:11px;" ><asp:CheckBox ID="chkForceSignOff" runat="server" Text="Force Sign off sheet" /></div>
				      </td></tr>
		        </table>	
		 </div>

     <div class="divWorkOrder" style="margin-top:17px;">
        <div class="WorkOrderTopCurve" style="height:22px;"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/><strong>Accreditations</strong></div>
          <table width="100%" cellspacing="9" cellpadding="0" border="0">	               
                <tr>
				    <td style="width:680px;">
                        <%@ Register TagPrefix="uc1" TagName="UCAccreds" Src="~/UserControls/Admin/UCAccreditationForSearch.ascx" %>
                         <uc1:UCAccreds id="UCAccreditations" runat="server"></uc1:UCAccreds>                    
                    </td>
                </tr>
          </table>
    </div>
    <div class="divWorkOrder" style="margin-top:17px;">
	        <div class="WorkOrderTopCurve" style="height:22px;"><img src="Images/Curves/blue_LeftCurve.gif" alt=""  align="absmiddle"/><strong>Questions to the Client/Customer</strong></div>
	         <table width="100%" cellspacing="9" cellpadding="0" border="0">	               
			        <tr>
				        <td style="width:680px;">				        				       
				           <table width="100%">
                           <tr>
                            <td>
                            <asp:Panel ID="pnlRemoveallClientQ" runat="server" Visible="false">                            
                                    <TABLE cellSpacing="0" cellPadding="0" width="140" bgColor="#f0f0f0" border="0" style="float:left;">
		                                <TR>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                                <TD class="txtBtnImage" width="120">
				                                <asp:Linkbutton id="lnkBtnRemoveallClientQ"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Remove All Questions</asp:LinkButton>
			                                </TD>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                                </TR>
	                                 </TABLE>
                            </asp:Panel>
                                   <TABLE cellSpacing="0" cellPadding="0" width="92" bgColor="#f0f0f0" border="0" style="float:right;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkBtnAddClientQ"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Add Question</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
                            </td>
                           </tr>
                           </table>
                           <asp:Panel ID="pnlClientQ" runat="server">
                           <input type="hidden" id="hdnQuestionCount" runat="server" />
                           <asp:Repeater ID="rptClientQuestions" runat="server" >
                                <ItemTemplate>
                                     <table style="padding:5px;margin-top:10px;border:dashed 1px black;">
                            <tr>
                              <td>Q<%#Container.dataitem("QuestionNo")%>
                               <input type="hidden" id="hdnQuestionNo" runat="server" value=<%#Container.dataitem("QuestionNo")%> />
                               <input type="hidden" id="hdnDBQuestionNo" runat="server" value=<%#Container.dataitem("DBQuestionNo")%> />
                               
                              &nbsp;<asp:TextBox ID="txtClientQ" runat="server" CssClass="formField width150" Width="340" Text=<%#Container.dataitem("ClientQ")%>></asp:TextBox>
                              &nbsp;&nbsp;
                              <asp:DropDownList id="ddQAnsCount" runat="server" CssClass="formFieldGrey" Width="35" AutoPostBack="true" OnSelectedIndexChanged="ddQAnsCount_SelectedIndexChanged" ToolTip=<%#Container.dataitem("DBQuestionNo")%>>                                
                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                              </asp:DropDownList>
                              
                            
	                            <TABLE cellSpacing="0" cellPadding="0" width="60" border="0" style="float:right;margin-right:140px;margin-left:20px;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120" style="background-color: #f0f0f0">
				                            <asp:Linkbutton id="lnkBtnRemoveClientQ"  runat="server" CausesValidation="false" CssClass="txtListing" CommandName="RemoveQuestion" CommandArgument='<%#Container.DataItem("QuestionNo") %>'>&nbsp;Remove&nbsp;</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                         <TD>
                                            <div style="width:50px; height:26px;float:left;margin-right:10px;margin-left:-20px;">
                                                <div style="float:left; width:24px; height:26px; margin-right:7px;"><asp:LinkButton ID="lnkBtnMoveUp"  CommandName="MoveUp"  CommandArgument='<%#Container.DataItem("Sequence") %>' visible= '<%#IIF(Container.DataItem("QuestionNo") = 1 , false,true)%>' runat="server" style="margin-left:35px;"><img id="imgMoveUp" runat="server" src="~/Images/Arrows/arrow-up.jpg" title="Move Up" border="0" class="cursorHand" /></asp:LinkButton></div>
                                                <div style="float:left; width:10px; height:26px;"><asp:LinkButton ID="lnkBtnMoveDown"  CommandName="MoveDown" CommandArgument='<%#Container.DataItem("Sequence") %>' visible= '<%#IIF(Container.DataItem("QuestionNo") < hdnQuestionCount.value, true,IIF(Container.DataItem("QuestionNo") = hdnQuestionCount.value , False, True))%>' runat="server" style="margin-left:20px;"><img id="imgMoveDown" runat="server" src="~/Images/Arrows/arrow-down.jpg" title="Move Down" border="0" class="cursorHand" /></asp:LinkButton></div>
                                            </div>                                        
                                        </TD>
		                            </TR>
	                            </TABLE>
                              </td>
                              
					         </tr>
                             <tr>
                                <td>
                                     <asp:Repeater ID="rptClientQuestionAnswers" runat="server" >
                                        <%-- <ItemTemplate>
                                         <table style="padding:5px;margin-top:10px;border:dashed 1px black;">
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Ans</td>
                                                <td>Message</td>
                                                <td>&nbsp;</td>                                                
                                            </tr>
                                            <tr>
                                                <td><b><%# Container.DataItem("AnsNo")%> <input type="hidden" id="hdnQuestionAnsNo" runat="server" value=<%#Container.dataitem("AnsNo")%> /></td>
                                                <td><asp:TextBox ID="txtAnswer" runat="server" CssClass="formField width150" Text=<%#Container.dataitem("Answer")%>></asp:TextBox></td>
                                                <td><asp:TextBox ID="txtAnswerMessage" runat="server" CssClass="formField width150"  Width="250" Text=<%#Container.dataitem("Message")%>></asp:TextBox></td>
                                                <td><asp:CheckBox ID="chkboxStopBooking" valign="top" runat="server" Text="Stops the booking" Checked=<%#Container.dataitem("StopBooking")%> /></td>                                              
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="3">Service</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="3">
                                                    <input type="hidden" id="hdnSelectedProduct" runat="server" value=<%#Container.dataitem("Product")%> />
                                                    <asp:DropDownList id="drpdwnProduct" runat="server" CssClass="formFieldGrey" Width="300"></asp:DropDownList>                              
                                                </td>
                                            </tr>                                           
                                         </table>   
                                         </ItemTemplate>--%>
                                          <ItemTemplate>
                                        Ans<%# Container.DataItem("AnsNo")%>
                                        <input type="hidden" id="hdnQuestionAnsNo" runat="server" value=<%#Container.dataitem("AnsNo")%> />
                                        <input type="hidden" id="hdnSelectedProduct" runat="server" value=<%#Container.dataitem("Product")%> />
                                        &nbsp;<asp:TextBox ID="txtAnswer" runat="server" CssClass="formField width150" Text=<%#Container.dataitem("Answer")%>></asp:TextBox>
                                        &nbsp;&nbsp;Message<%#Container.dataitem("AnsNo")%>&nbsp;<asp:TextBox ID="txtAnswerMessage" runat="server" CssClass="formField width150"  Width="250" Text=<%#Container.dataitem("Message")%>></asp:TextBox>
                                        &nbsp;&nbsp;Service<%#Container.dataitem("AnsNo")%>&nbsp;<asp:DropDownList id="drpdwnProduct" runat="server" CssClass="formFieldGrey" Width="300"></asp:DropDownList>               
                                        &nbsp;&nbsp;<asp:CheckBox ID="chkboxStopBooking" valign="top" runat="server" Text="Stops the booking" Checked=<%#Container.dataitem("StopBooking")%> />
                                        <br />
                                         </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                             </tr>					     
				            </table>
                             </ItemTemplate>
                            </asp:Repeater>
                            </asp:Panel>
				        </td>
				      </tr>				      
		        </table>	
		 </div>
	<!--Product Additional Info -->
	    
    </td>
  </tr>
   <tr>
    <td height="45">&nbsp;</td>
  </tr>
</table>
 </DIV>
</div>

<TABLE width="100%" border="0" align="center" cellPadding="0" cellSpacing="0" style="PADDING-RIGHT:0px; PADDING-LEFT:0px; PADDING-BOTTOM:0px; MARGIN:0px; PADDING-TOP:16px">
<TR>
<TD align="right" vAlign="top">&nbsp;</TD>
<TD width="100" align="right" vAlign="top" id="tdCopyServiceBtm" runat="server">
    <TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0" width="130">
		<TR>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			<TD width="120" class="txtBtnImage">
			  <asp:LinkButton ID="btnCopyServiceBtm" runat="server" class="txtListing" OnClick="CopyService"  CausesValidation="false" > <img src="Images/Icons/Copy.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Copy Service&nbsp;</asp:LinkButton>
		  </TD>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		</TR>
	</TABLE>
    </TD>
    <TD width="10"></TD>
<TD width="130" align="right" vAlign="top">
<TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0">
		<TR>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			<TD width="120" class="txtBtnImage">
			  <asp:LinkButton ID="btnConfirmBtm" runat="server" class="txtListing" CausesValidation="false" OnClientClick="callSetFocusValidation('UCTemplateWO1')"> <img src="Images/Icons/Submit.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Save Changes&nbsp;</asp:LinkButton>
			</TD>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		</TR>
	</TABLE>
</TD>
<TD width="10">&nbsp;</TD>
<TD vAlign="top" align="right" width="130">
	<TABLE cellSpacing="0" cellPadding="0"  bgColor="#f0f0f0" border="0">
		<TR>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			<TD class="txtBtnImage" width="130">
				<asp:Linkbutton id="btnResetBtm"  runat="server" CausesValidation="false" CssClass="txtListing"><img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Undo Changes&nbsp;</asp:LinkButton>
			</TD>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		</TR>
	</TABLE>
</td>
<TD width="10">&nbsp;</TD>
<td vAlign="top" align="right" width="60">
	    <TABLE cellSpacing="0" cellPadding="0" bgColor="#f0f0f0" border="0">
		<TR>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			<TD width="80" class="txtBtnImage">
			  <asp:LinkButton ID="btnCancelBtm" runat="server" class="txtListing" CausesValidation="false"> <img src="Images/Icons/Icon-Cancel.gif" hspace="5" vspace="0" align="absmiddle" border="0">&nbsp;Exit&nbsp;</asp:LinkButton>
		  </TD>
			<TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		</TR>
	</TABLE>
	</td>
</TR>
</TABLE>
</asp:Panel>

<cc1:ModalPopupExtender ID="mdlCopyService" runat="server" TargetControlID="btnTemp" PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
    <asp:Panel runat="server" ID="pnlConfirm" Width="250px" CssClass="pnlConfirm" style="display:none;">
        <div id="divModalParent">
            <span style="font-family:Tahoma; font-size:14px;"><b>Number of Services</b></span><br /><br />           
                <asp:TextBox runat="server" ID="txtCopyServiceNumber"  Width="200" CssClass="bodyTextGreyLeftAligned" ></asp:TextBox>        
            <br /><br />
                <div id="divButton" class="divButton" style="width: 85px; float:left;cursor:pointer;">
                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                        <a class="buttonText cursorHand" href='javascript:validate_required()' id="ancSubmit"><strong>Submit</strong></a>
                    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                </div>
                <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;cursor:pointer;">
                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                        <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                </div> 
        </div>								
    </asp:Panel>
    <asp:Button runat="server" ID="hdnButton" OnClick="hdnButton_Click" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
    <asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

</ContentTemplate>
  <Triggers>
  	<asp:AsyncPostBackTrigger EventName="SelectedIndexChanged" ControlID="ddlWOType" />
	<asp:AsyncPostBackTrigger EventName="Click" ControlID="btnConfirmBtm" />
	<asp:AsyncPostBackTrigger EventName="Click" ControlID="btnConfirmTop" />
	<asp:AsyncPostBackTrigger EventName="Click" ControlID="btnResetTop" />
	<asp:AsyncPostBackTrigger EventName="Click" ControlID="btnResetBtm" />
  </Triggers>
</asp:UpdatePanel>

 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanelCreateWorkOrder" runat="server">
        <ProgressTemplate>
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Please Wait...
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>
<cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender1" ControlToOverlayID="pnlWoForm" CssClass="updateProgress" TargetControlID="UpdateProgress1" runat="server" />

<asp:Panel ID="pnlWoSummary" runat="server" Visible="false">

<table width="100%" cellpadding="5" cellspacing="0" >
  <tr>
    <td class="BorderW HdrText">Date Created </td>
    <td class="BorderW HdrText">Location Name </td>
    <td class="BorderW HdrText">Contact</td>
    <td class="BorderW HdrText">Service Title </td>
    <td class="BorderW HdrText">Date Start</td>
    <td class="BorderW HdrText">Proposed Wholesale Price</td>
    <td class="BorderW HdrText BorderWhiteR">Proposed Platform Price</td>
    </tr>
  <tr>
    <td class="BorderW RowText"><asp:Label ID="lblDateCreated" runat="server" ></asp:Label></td>
    <td class="BorderW RowText"><asp:Label ID="lblLocName" runat="server" ></asp:Label></td>
    <td class="BorderW RowText"><asp:Label ID="lblContact" runat="server" ></asp:Label></td>
    <td class="BorderW RowText"><asp:Label ID="lblWOTitle" runat="server" ></asp:Label></td>
    <td class="BorderW RowText"><asp:Label ID="lblDateStart" runat="server" ></asp:Label></td>
    <td class="BorderW RowText"><asp:Label ID="lblWholeSalePrice" runat="server" ></asp:Label></td>
    <td class="BorderW RowText BorderWhiteR"><asp:Label ID="lblPlatformPrice" runat="server" ></asp:Label></td>
    </tr>
</table>

<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="txtWelcome">
<tr>
<td width="30" align="right" valign="top">&nbsp;</td>
<td align="left" valign="top" class="bodyTextGreyLeftAligned paddingT10B24MarginT8"><strong><asp:Label ID="lblCategory" runat="server"></asp:Label>:</strong></td>
<td width="20">&nbsp;</td>
</tr>					   
</table>

<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="txtWelcome">
<tr>
<td width="30" align="right" valign="top">&nbsp;</td>
<td id="tdDGDetailedDesc" align="left" valign="top"><strong>Scope of Work: </strong><asp:Label ID="lblLongDesc" runat="server"></asp:Label><br>
	<br />
	<asp:Panel ID="pnlSpecialInstructions" runat="server">
		<strong>Special instructions to the Engineers:</strong> <asp:Label ID="lblSpecialInstructions" runat="server"></asp:Label>
</asp:Panel>
				
</td>
<td width="30">&nbsp;</td>
</tr>
</table>
 	

</asp:Panel>
<script type="text/javascript">

    function excludeVat(input) {
        var vatDec = 1 + parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_hdnvatvalue").value);
        var realValue = (document.getElementById(input).value) / vatDec;
        if (input == 'ctl00_ContentPlaceHolder1_UCAdminProduct1_txtSpendLimitWP') {
            if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtSpendLimitWP").value.match(/^-?\d*(\.\d+)?$/)) {
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_lblWOincludvat").value = realValue.toFixed(3);
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_hdnWPvatvalue").value = realValue.toFixed(3);

            }
            else {
                alert("Please enter the Wholesale Price amount (Include. VAT) as either an integer or as an amount to 2 decimal places");
            }
        }
        else {
            if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtCustPrice").value.match(/^-?\d*(\.\d+)?$/)) {
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_lblCPincludvat").value = realValue.toFixed(3);
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_hdnCPvatvalue").value = realValue.toFixed(3);
            }
            else {
                alert("Please enter the Customer Price amount (Include. VAT) as either an integer or as an amount to 2 decimal places");
                
            }
        }
        
        
    }
    function includeVat(input) {

        
        var vatDec = parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_hdnvatvalue").value);
        var realValue = parseFloat(document.getElementById(input).value) * vatDec;
        if (input == 'ctl00_ContentPlaceHolder1_UCAdminProduct1_lblWOincludvat') {
            if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_lblWOincludvat").value.match(/^-?\d*(\.\d+)?$/)) {
                var finaleValue = parseFloat(document.getElementById(input).value) + realValue;
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtSpendLimitWP").value = finaleValue.toFixed(2);
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_hdnWPvatvalue").value = document.getElementById(input).value;
        }
            else {
                alert("Please enter the Wholesale Price amount (excl. VAT) as either an integer or as an amount to 3 decimal places");
               
                
            }
            }
        else {
            if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_lblCPincludvat").value.match(/^-?\d*(\.\d+)?$/)) {
                var finaleValue = parseFloat(document.getElementById(input).value) + realValue
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtCustPrice").value = finaleValue.toFixed(2);
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_hdnCPvatvalue").value = document.getElementById(input).value;
       }
        else {
            alert("Please enter the Customer Price amount (excl. VAT) as either an integer or as an amount to 3 decimal places");
        }
    }


}


function validate_required() {
    if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtCopyServiceNumber").value == "") {

        alert("Please enter Number of Services required");
    }
    else {
        if (IsNumeric(document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtCopyServiceNumber").value) == false) {
            alert("Please enter only positive numeric values");
        }
        else 
        {
        //alert(document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtCopyServiceNumber").value);
            if (document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_txtCopyServiceNumber").value > 9) {
                alert("Please enter numeric value less than 9");
            }
            else {
                document.getElementById("ctl00_ContentPlaceHolder1_UCAdminProduct1_hdnButton").click();
            }            
        }
        
    }
}
function IsNumeric(sText) {

    var IsNumber
    var isInteger_re = /^[0-9]+[0-9]*$/;
    if (sText.match(isInteger_re)) {
        IsNumber = true;
    }
    else {
        IsNumber = false;
    }
    return IsNumber;
} 

</script>




