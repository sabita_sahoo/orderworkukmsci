﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCTableOfSkills.ascx.vb" Inherits="Admin.UCTableOfSkills" %>
<script type="text/javascript">
    function CheckAOESubCat(AOEDivId, hdnCombIDs) {
        var objId = AOEDivId.replace("AOE", "");
        var MainCatCombId = document.getElementById("hdnMainCatCombId" + objId).value;
        var SelSubCatCombId = document.getElementById("hdnSelSubCatCombId" + MainCatCombId).value;
        SelSubCatCombId = SelSubCatCombId.replace("," + MainCatCombId, "");
        var strIDs = document.getElementById(hdnCombIDs).value;
        if (document.getElementById(AOEDivId).className == "clsNotSelectedSkillDiv") {
            if (strIDs != "") {
                strIDs = strIDs.replace("," + MainCatCombId, "");
                strIDs = strIDs + ",";
            }
            strIDs = strIDs + objId;
            strIDs = strIDs + "," + MainCatCombId;

            if (SelSubCatCombId != "")
            { SelSubCatCombId = SelSubCatCombId + "," }
            SelSubCatCombId = SelSubCatCombId + objId

            document.getElementById(AOEDivId).className = "clsSelectedSkillDiv";
            document.getElementById("AOE" + MainCatCombId).className = "clsSelectedSkillLeftDiv";
        }
        else {
            var str = "," + objId;
            strIDs = strIDs.replace(str, "")

            var strSelSubCatCombId = "," + objId;
            SelSubCatCombId = SelSubCatCombId.replace(strSelSubCatCombId, "");
            //alert(SelSubCatCombId);
            if (SelSubCatCombId == "0") {
                document.getElementById("AOE" + MainCatCombId).className = "clsNotSelectedSkillLeftDiv";
                str = "," + MainCatCombId;
                strIDs = strIDs.replace(str, "")
            }
            document.getElementById(AOEDivId).className = "clsNotSelectedSkillDiv";
        }
        document.getElementById(hdnCombIDs).value = strIDs;
        document.getElementById("hdnSelSubCatCombId" + MainCatCombId).value = SelSubCatCombId;
    };
</script>

<div class="clsITMain">
  <div class="clsITkillSet"><b>IT Skills Set</b></div>
  <input id="hdnCombIDs" type="hidden" name="hdnCombIDs" runat="server" value="0">
  <input id="hdnPageName" type="hidden" name="hdnPageName" runat="server" value="">
   

  <asp:Repeater ID="rptAOEMainIT" runat="server">
  <ItemTemplate>
  <div class='<%#IIF(Container.ItemIndex  Mod 2 = 0 ,"divStyle","divStyleAlternate") %>'>
     
     <div id='AOE<%#Eval("CombID")%>' onclick="CheckAOEMainCat(this.id,'<%= hdnCombIDs.ClientID %>')"  class='<%#IIF(Container.DataItem("SelectedAOE") <> 0,"clsSelectedSkillLeftDiv","clsNotSelectedSkillLeftDiv") %>'>                 
     <input id='hdnSubCatOfMainCat<%#Eval("CombID")%>' type="hidden" name="hdnMainCatCombId" value='<%#Eval("SubCatOfMainCat")%>'> 
        <asp:Label ID="lblMainCatNameIT" runat="server" Text=' <%#Eval("MainCatName")%> '></asp:Label>
     </div>
      <div class="divSkillSetRight">      
      <asp:Repeater ID="rptAOESubIT" runat="server">
        <ItemTemplate>      
           <input id='hdnMainCatCombId<%#Eval("CombID")%>' type="hidden" name="hdnMainCatCombId" value='<%#Eval("MainCatCombId")%>'>           
           <input id='hdnSelSubCatCombId<%#Eval("MainCatCombId")%>' type="hidden" name="hdnSelSubCatCombId" value='<%#Eval("CommonSelectedAOE")%>'>
           <div style=" text-align:center" id='AOE<%#Eval("CombID")%>' onclick="CheckAOESubCat(this.id,'<%= hdnCombIDs.ClientID %>')"  class='<%#IIF(Container.DataItem("SelectedAOE") <> 0,"clsSelectedSkillDiv","clsNotSelectedSkillDiv") %>'><%#Eval("SubCatName")%></div> 
        </ItemTemplate>    
       </asp:Repeater>                         
      </div>
  </div>
   </ItemTemplate>    
  </asp:Repeater>  
  
  </div>

<div class="clsRetailMain">
    <div class="clsRetailskillSet"><b>Retail Skills Set</b></div>
   <asp:Repeater ID="rptAOEMainRetail" runat="server">
  <ItemTemplate>
  <div class='<%#IIF(Container.ItemIndex  Mod 2 = 0 ,"divStyle","divStyleAlternate") %>'>
    
     <div id='AOE<%#Eval("CombID")%>' class='<%#IIF(Container.DataItem("SelectedAOE") <> 0,"clsSelectedSkillLeftDiv","clsNotSelectedSkillLeftDiv") %>'> 
        <input id='hdnSubCatOfMainCat<%#Eval("CombID")%>' type="hidden" name="hdnMainCatCombId" value='<%#Eval("SubCatOfMainCat")%>'>                        
      <asp:Label ID="lblMainCatNameRetail" runat="server" Text=' <%#Eval("MainCatName")%> '></asp:Label>
     </div>
      <div class="divSkillSetRight" id="test1">      
      <asp:Repeater ID="rptAOESubRetail" runat="server">
        <ItemTemplate>      
           <input id='hdnMainCatCombId<%#Eval("CombID")%>' type="hidden" name="hdnMainCatCombId" value='<%#Eval("MainCatCombId")%>'>           
           <input id='hdnSelSubCatCombId<%#Eval("MainCatCombId")%>' type="hidden" name="hdnSelSubCatCombId" value='<%#Eval("CommonSelectedAOE")%>'>
           <div style="text-align:center;" id='AOE<%#Eval("CombID")%>' onclick="javascript:CheckAOESubCat(this.id,'<%= hdnCombIDs.ClientID %>')"  class='<%#IIF(Container.DataItem("SelectedAOE") <> 0,"clsSelectedSkillDiv","clsNotSelectedSkillDiv") %>'><%#Eval("SubCatName")%></div> 
        </ItemTemplate>    
       </asp:Repeater>                         
      </div>
  </div>
   </ItemTemplate>    
  </asp:Repeater>  
  
  </div>
  