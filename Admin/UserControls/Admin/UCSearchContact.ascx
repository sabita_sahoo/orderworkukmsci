<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSearchContact.ascx.vb" Inherits="Admin.UCSearchContact" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="WebLibrary" Namespace="WebLibrary" TagPrefix="lib" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div id="divSearchContact" runat="Server" visible="true">
    <asp:DropDownList id="ddlContact" runat="server" CssClass="formFieldGrey150" DataTextField = "Contact" DataValueField = "CompanyId" EnableViewState="True" ></asp:DropDownList>
  
    <cc1:ListSearchExtender ID="ListSearchExtender1" runat="server" TargetControlID="ddlContact" PromptCssClass="ListSearchExtenderPrompt" PromptText="Type to Search" PromptPosition="Top">
    </cc1:ListSearchExtender>
</div>
<div id="divAutoSuggest" runat="Server" visible="false">        
    <input type="text" id="txtContact" class="formFieldGrey215" style="width:320px" onkeyup='javascript:getResults(this.id)' onkeydown='javascript:processKey' onblur='javascript:hideSearchResult()' runat="server"/><br /><div id="searchResult" class="searchResult" style="font-size:10px;">
    </div>
</div>
</ContentTemplate>
 </asp:UpdatePanel>
 <asp:UpdateProgress  ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
        <ProgressTemplate>
            <div class="gridText">
                <img  align=middle src="Images/indicator.gif" />
                <b>Fetching Data... Please Wait</b>
            </div>      
        </ProgressTemplate>
</asp:UpdateProgress>