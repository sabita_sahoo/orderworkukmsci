<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RefundBuyer.aspx.vb" Inherits="Admin.RefundBuyer" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Create Credit Note"%>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
			<script language="javascript" type="text/javascript">
function OpenNewWindowInvoice(InvoiceNo,BizDivId)
{
var w = 800;
var h = 600;
var left = (screen.width/2)-(w/2);
var top = (screen.height/2)-(h/2);
 window.open ('ViewCreditNote.aspx?invoiceNo='+ InvoiceNo + '&bizDivId='+ BizDivId + '', 'CreditNote', 'toolbar=0,location=0, directories=0, status=0, menubar=0, scrollbars=0, resizable=1, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
</script>
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
     <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0" style="margin-bottom:10px;">
			    <tr>
			    <td width="10px"></td>
			    <td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Create Credit Note"  runat="server"></asp:Label></td>
			    </tr>
			    </table>
		   <div id="divValidationMain" class="divValidation" runat="server" visible="false" style="margin:15px">
				<div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  <table width="96%"  border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					</tr>
					<tr valign="middle">
					  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
					  <td width="565" class="validationText"><div  id="divValidationMsg"></div>
					   <span class="validationText">
					  	<asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
					  </span>
					   
					  </td>
					  <td width="20">&nbsp;</td>
					</tr>
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15"><input type="button" id="btnFocus" name="btnFocus" class="hdnClass"  /></td>
					  <td height="15">&nbsp;</td>
					</tr>

					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15"><asp:Label ID="lblErrMsg" CssClass="HeadingRed" Text=""  runat="server"></asp:Label></td>
					  <td height="15">&nbsp;</td>
					</tr>

				  </table>  
				  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				</div>
				<asp:RequiredFieldValidator id="rqWOValue_WP" runat="server" ErrorMessage="Please enter Amount" ForeColor="#EDEDEB"
								                                                        ControlToValidate="txtbxAmt">*</asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="regexp" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtbxAmt"
												                                                                            ErrorMessage="Please enter valid Amount" ValidationExpression="^(\d)?(\d|,)*\.?\d*$">*</asp:RegularExpressionValidator>
				<asp:RequiredFieldValidator id="rqDesc" runat="server" ErrorMessage="Please enter Description" ForeColor="#EDEDEB"
								                                                        ControlToValidate="txtDescription">*</asp:RequiredFieldValidator>

              <asp:Label ID="lblWarningMessage" CssClass="HeadingRed" Text=""  runat="server" Visible="false" style="font-size:12px; font-weight:normal; margin-top:15px; margin-bottom:15px"></asp:Label>
              <table height="54" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server" id="tblSINumber">
                <tr valign="top">
                  <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                  <td class="formTxt" valign="top" align="left" width="166" height="14"><span class="formLabelGrey">&nbsp; Sales Invoice Number&nbsp;</span><span class="bodytxtValidationMsg">*</span> <asp:RequiredFieldValidator id="rqSINumber" runat="server" ErrorMessage="Please enter Sales Invoice Number" ForeColor="#EDEDEB"
									ControlToValidate="txtSINumber">*</asp:RequiredFieldValidator></td>
                  <td width="60" align="left" class="formTxt" >&nbsp;</td>				  
                </tr>
                <tr valign="top">
                  <td align="left">&nbsp;</td>
                  <td align="left" height="20"><asp:TextBox id="txtSINumber" TabIndex="1" runat="server" CssClass="formFieldGrey150"></asp:TextBox>
                  </td>                  
				  <td width="4" align="left">&nbsp;</td>
                </tr>
                <tr>
                <td height="20" colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left">&nbsp;</td>
                    <td align="left">
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                            <asp:LinkButton id="lnkbtnSubmit" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Submit&nbsp;</asp:LinkButton>
                        </div>
                    </td>
                    <td align="left">&nbsp;</td>
                </tr>
              </table>
              <asp:Panel id="pnlProceed" runat="server" visible="false">
               <table height="54" cellspacing="0" cellpadding="0" width="550" border="0" style="float:left;">
               <tr>
                <td class="formTxt" valign="top" align="left" width="28" height="40" >&nbsp;</td>
                    <td align="left">
                    <b>Client Name: &nbsp;</b>
                </td>
                <td width="348"><b><asp:Label ID="lblCompName" runat="server" ></asp:Label></b></td>
                </tr> 
               <tr>
                <td class="formTxt" valign="top" align="left" width="28" >&nbsp;</td>
                <td align="left" class="formTxt" width="170" >
                    <b>Sales Invoice: </b>
                </td>
                <td align="left" class="formTxt" width="348">
                    <asp:Label ID="lblSINo" runat="server" Text="Label"></asp:Label>
                </td>
                </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28" height="30" >&nbsp;</td>
                <td align="left" class="formTxt" valign="top" >
                    <b>Sale Invoice Net Amount: </b>
                </td>
                <td width="348" align="left" valign="top" class="formTxt" >
                    <span class="formLabelGrey" style="vertical-align:middle;">&pound;</span><asp:Label ID="lblSIAmt" runat="server" Text="Label"></asp:Label>
                </td>
                </tr> 
               <tr>
               <td colspan="3" height="15">&nbsp;
                    <table cellspacing="0" cellpadding="0" width="100%" border="0" >            
               
                <tr>
                  <td class="formTxt" valign="top" align="left" width="28" >&nbsp;</td>
                  <td class="formTxt" align="left" width="170"  ><b>Full / Partial Refund</b></td>
                  <td class="formTxt" align="left" width="230" ><b>Amount (exclusive of VAT)</b>&nbsp;<span class="bodytxtValidationMsg">*</span></td>
                  <td>&nbsp;</td>
                </tr>
                <tr valign="top">
                  <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                  <td class="formTxt" align="left" ><asp:RadioButton ID="rdbtnFull" OnCheckedChanged="rdbtnFull_CheckedChanged" AutoPostBack="true" runat="server" CausesValidation="false" Checked="true" GroupName="paymode" Text="Full" />
                  <asp:RadioButton ID="rdbtnPartial" runat="server" GroupName="paymode" Text="Partial" OnCheckedChanged="rdbtnFull_CheckedChanged" AutoPostBack="true" CausesValidation="false" /></td>
                  <td width="230" align="left" valign="top" class="formTxt" ><span class="formLabelGrey" style="vertical-align:middle;">&pound;</span>
                    <asp:TextBox  CssClass="formFieldGrey150" ID="txtbxAmt" runat="server" Text="0" MaxLength="16"></asp:TextBox></td>
                  <td>&nbsp;</td>
                </tr>  
                <tr>
                  <td height="80" class="formTxt" valign="top" align="left" width="28" >&nbsp;</td>
                  <td class="formTxt" align="left" width="170" colspan="2"  ><b>Description: </b>&nbsp;<span class="bodytxtValidationMsg">*</span><br /><asp:TextBox  CssClass="formFieldGrey150" ID="txtDescription" runat="server" MaxLength="100" Width="330"></asp:TextBox></td>                  
                  <td>&nbsp;</td>
                </tr>                   
                <tr>
                    <td height="40" colspan="4">&nbsp;
                    </td>
                </tr>    
                <tr>                   
                <td width="28" align="left">&nbsp;</td>
                     <td width="170" align="left">
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                            <asp:LinkButton id="lnkbtnCancel" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Cancel&nbsp;</asp:LinkButton>
                        </div>
                      </td>				      
				      <td width="160" align="right">
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                            <asp:LinkButton id="lnkbtnNext" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Next&nbsp;&nbsp;&nbsp;</asp:LinkButton>
                        </div>
                      </td>
                      <td >&nbsp;</td>
                </tr>
				<tr valign="top">
				  <td width="28" align="left">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                  <td width="230" height="20" align="left">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
              </table>
                </td>
               </tr>             
               </table>
               <table cellspacing="0" cellpadding="0" width="400" border="0" style="float:left;"> 
               <tr><td style="padding-bottom:5px;padding-top:10px;" valign="bottom"><b><asp:Label runat="server" ID="lblPreHdg" Text="Previous Credit Notes" CssClass="HeadingRed"></asp:Label></b></td></tr>
               <tr><td>
                <asp:Repeater runat="server" ID="rptExisCN"><ItemTemplate>
                <table cellspacing="0" cellpadding="0" width="400" border="0" style="float:left;">                           
                <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" width="170">
                    <b>Credit Note Invoice Date: </b>
                </td>
                <td align="left" class="formTxt" width="100" style="text-align:left;">
                    <%#Container.dataitem("InvoiceDate")%>
                </td>
                
               </tr>
                <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" width="170">
                    <b>Credit Note Invoice No: </b>
                </td>
                <td align="left" class="formTxt" width="100" style="text-align:left;">
                    <%#Container.dataitem("InvoiceNo")%>
                </td>
                
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>Credit Note Net Amount: </b>
                </td>
                <td align="left" class="formTxt" style="text-align:left;">
                    <span class="formLabelGrey" style="vertical-align:middle">&pound;</span><%#Container.dataitem("Amount")%>
                </td>
                
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>Credit Note VAT Amount: </b>
                </td>
                <td align="left" class="formTxt" style="text-align:left;">
                    <span class="formLabelGrey" style="vertical-align:middle">&pound;</span><%#Container.dataitem("VatAmount")%>
                </td>
                
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>Credit Note Total Amount: </b>
                </td>
                <td align="left" class="formTxt" style="text-align:left;">
                    <span class="formLabelGrey" style="vertical-align:middle">&pound;</span><%#Container.DataItem("TotalAmt")%>
                </td>
                
               </tr>  
               <tr>
                <td class="formTxt" valign="top" align="left" width="28" >&nbsp;</td>
                <td align="left" class="formTxt" valign="top">
                    <b>Credit Note Description: </b>
                </td>
                <td align="left" class="formTxt" style="text-align:left;">
                    <%#Container.DataItem("Description")%>
                </td>
                
               </tr>                                
                <tr>
                    <td colspan="3" height="15">&nbsp;
                    </td>
                </tr>
                </table>
                </ItemTemplate></asp:Repeater>
                </td></tr>
                </table>
                
              </asp:Panel>
              <table cellspacing="0" cellpadding="0" width="100%" border="0" runat="server" id="tblConfirmation" visible="false" style="margin-top:15px;">
              <tr><td>
              <table cellspacing="0" cellpadding="0" width="100%" border="0">
              <tr>
              <td>&nbsp;</td>
                <td class="formTxt" style="font-size:13px" colspan="3" height="30" valign="top"><b>Sales Invoice</b></td>
              </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" width="200" >
                    <b>Sales Invoice Number: </b>
                </td>
                <td align="left" class="formTxt" width="100" style="text-align:left;">
                    <asp:Label ID="lblConfSINo" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>Net Amount: </b>
                </td>
                <td align="left" class="formTxt" style="text-align:left;">
                    <span class="formLabelGrey" style="vertical-align:middle">&pound;</span><asp:Label ID="lblConfNetAmt" runat="server" Text=""></asp:Label>
                </td>
                <td align="left" width="700">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>VAT Amount: </b>
                </td>
                <td align="left" class="formTxt" style="text-align:left;">
                    <span class="formLabelGrey" style="vertical-align:middle">&pound;</span><asp:Label ID="lblConfVatAmt" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>Total Amount: </b>
                </td>
                <td align="left" class="formTxt" style="text-align:left;">
                    <span class="formLabelGrey" style="vertical-align:middle">&pound;</span><asp:Label ID="lblConfTotAmt" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" colspan="4" height="15">&nbsp;</td>                
               </tr>  
               <tr>
               <td>&nbsp;</td>
                <td class="formTxt" style="font-size:13px" colspan="3" height="30" valign="top"><b>Credit Note</b></td>
              </tr>
             </table>             
            <table cellspacing="0" cellpadding="0" width="100%" border="0">            
               
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" width="200">
                    <b>New Credit Note Net Amount: </b>
                </td>
                <td align="left" class="formTxt" width="720" style="text-align:left;">
                    <asp:Label ID="lblConfCNNetAmt" runat="server" Text=""></asp:Label>
                </td>
                <td align="left" width="120">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>New Credit Note VAT Amount: </b>
                </td>
                <td align="left" class="formTxt" style="text-align:left;">
                    <asp:Label ID="lblConfCNVat" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" >
                    <b>New Credit Note Total Amount: </b>
                </td>
                <td align="left" class="formTxt" style="text-align:left;">
                    <asp:Label ID="lblConfCNTotAmt" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>  
               <tr>
                <td class="formTxt" valign="top" align="left" width="28">&nbsp;</td>
                <td align="left" class="formTxt" valign="top" >
                    <b>New Credit Note Description: </b>
                </td>
                <td align="left" class="formTxt" style="text-align:left;">
                    <asp:Label ID="lblDesc" runat="server" Text=""></asp:Label>
                </td>
                <td align="left">&nbsp;</td>
               </tr>                             
                <tr>
                    <td colspan="4" height="45">&nbsp;
                    </td>
                </tr>                      
                <tr>                   
                <td align="left">&nbsp;</td>
                     <td align="left" width="205">
                     <div style="width:60px; float:left;">
                      <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                        <asp:LinkButton id="lnkbtnBack" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Back&nbsp;</asp:LinkButton>
                      </div>
                      </div>
                      <div style="width:60px; float:right; margin-left:30px;">
                      <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                        <asp:LinkButton id="lnkbtnConfCan" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Cancel&nbsp;</asp:LinkButton>
                      </div>
                      </div></td>				      
				      <td align="right" width="145">
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                            <asp:LinkButton id="lnkbtnConf" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Confirm&nbsp;</asp:LinkButton>
                        </div>
                      </td>
                      <td align="left">&nbsp;</td>
                </tr>
				<tr valign="top">				 
                  <td align="left" colspan="4">&nbsp;</td>
                </tr>
              </table>
              </td></tr>
              </table>
              <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="3%"  align="left">&nbsp;  
                    		
		        </td>                
                <td width="97%"  align="left"><asp:Label ID="lblMsgInner" runat="server" class="bodytxtValidationMsg" ></asp:Label>	</td>
              </tr>
              </table> 

           	<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="HeadingRed" style="padding:0px 0px 0px 10px;"><asp:Label ID="lblErrorMsg" Visible="false" runat=server></asp:Label></td>
			</tr>
			</table>
			           
          </ContentTemplate>
      
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />

		
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      </asp:Content>
