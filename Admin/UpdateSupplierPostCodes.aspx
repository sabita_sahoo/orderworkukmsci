<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UpdateSupplierPostCodes.aspx.vb" Inherits="Admin.UpdateSupplierPostCodes" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script language="JavaScript" type="text/JavaScript" src="JS/Scripts.js"></script>
<script language="javascript" src="JS/Scripts.js"  type="text/javascript"></script>
<!-- InstanceParam name="OpMenuRedTopCurves" type="boolean" value="true" --><!-- InstanceParam name="OpMenuRedBtmCurve" type="boolean" value="true" --><!-- InstanceParam name="OpMenuOrangeTopCurve" type="boolean" value="false" --><!-- InstanceParam name="OpMenuBtmOrangeCurve" type="boolean" value="false" --><!-- InstanceParam name="onLoad" type="text" value="" --><!-- InstanceParam name="OpQLRegion" type="boolean" value="false" --><!-- InstanceParam name="OpContentTopCurve" type="boolean" value="true" --><!-- InstanceParam name="OpUCMenu" type="boolean" value="true" --><!-- InstanceParam name="OpContentTopMargin" type="boolean" value="false" --><!-- InstanceParam name="OpAddSpecialist" type="boolean" value="false" --><!-- InstanceParam name="OpAddLocations" type="boolean" value="false" --><!-- InstanceParam name="OpLogoMessage" type="boolean" value="true" --><!-- InstanceParam name="SpacNavAdminHome" type="boolean" value="false" --><!-- InstanceParam name="OpUCSubMenu" type="boolean" value="true" --><!-- InstanceParam name="OpRgLogin" type="boolean" value="false" --><!-- InstanceParam name="OpRgSupplierLogin" type="boolean" value="false" --><!-- InstanceParam name="OpRgClientLogin" type="boolean" value="false" -->
<link href="Styles/OrderWork.css" rel="stylesheet" type="text/css" />
<link href="Styles/OrderWork.css" rel="stylesheet" type="text/css" />
<link href="Styles/Menu.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<style>
#divMenu {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	line-height: 12px;
	font-weight: normal;
	text-decoration: none;
	width: 100%;
	background-position: right top;
	background-repeat: no-repeat;
	border-top-width: 1px;
	border-top-style: solid; 
	border-top-color: #CECECE;
	background-color: #993366;
}

.divContentTopRed{
	width: 100%;
	background-color: #993366;
	background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
	height: 5px;
	
}
-->
</style>
<!-- InstanceEndEditable --><!-- InstanceParam name="opAccountSumm" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddUser" type="boolean" value="false" --><!-- InstanceParam name="OpRgCLAddLOcation" type="boolean" value="false" --><!-- InstanceParam name="OPWOSummary" type="boolean" value="false" --><!-- InstanceParam name="RgOpBtnNewWO" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddFunds" type="boolean" value="false" --><!-- InstanceParam name="opMenuRegion" type="boolean" value="true" --><!-- InstanceParam name="opContentTopRedCurve" type="boolean" value="false" --><!-- InstanceParam name="opUsersProfile" type="boolean" value="false" --><!-- InstanceParam name="opCompanyProfile" type="boolean" value="true" --><!-- InstanceParam name="opUsersListing" type="boolean" value="false" --><!-- InstanceParam name="opLocations" type="boolean" value="false" --><!-- InstanceParam name="opReferences" type="boolean" value="false" --><!-- InstanceParam name="opPreferences" type="boolean" value="false" --><!-- InstanceParam name="opUsers" type="boolean" value="false" --><!-- InstanceParam name="width" type="text" value="985" --><!-- InstanceParam name="opComments" type="boolean" value="false" --><!-- InstanceParam name="OpRgScriptManager" type="boolean" value="true" --><!-- InstanceParam name="OpFavSuppliers" type="boolean" value="true" --><!-- InstanceParam name="OpFavSuppliersLink" type="boolean" value="false" -->
</head>
    <title>Update Suppliers Post Codes</title>
</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
    <table width="985" height="100%"  border="0" align="center" cellpadding="0" cellspacing="0"> 
  <tr>
    <td valign="top" bgcolor="#f4f5f0">
	
	<div id="divLogo">
      <div class="roundtopSwitch"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
      <table width="100%" height="54" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><!-- InstanceBeginEditable name="EdLogo" --><img src="Images/OrderWorks-Logo-Beta.gif" alt="OrderWork - Trading IT Skills Across the UK" width="345" height="18" border="0" class="LogoImg" /><!-- InstanceEndEditable --></td>
          <td align="right" valign="bottom" class="LogoTopMsg">
		  <%@ Register TagPrefix="uc2" TagName="UCSwitchAdmin" Src="~/UserControls/UK/UCMSSwitchAdminUK.ascx" %>
          <uc2:UCSwitchAdmin id="UCSwitchAdmin1" runat="server"></uc2:UCSwitchAdmin>
          <%@ Register  TagPrefix="uc1" TagName="UCTopMSgLabel" Src="~/UserControls/UK/UCTopMSgLabelUK.ascx" %>
          <uc1:UCTopMSgLabel id="UCTopMSgLabel1" runat="server"></uc1:UCTopMSgLabel></a></td>
          <td width="19" align="right" valign="bottom" class="footerTxt">&nbsp;</td>
        </tr>
      </table>
      <div class="roundbottomSwitch"><img src="Images/Curves/Main-LBC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
    </div>
    
    <div id="divMenu"> 
      <div class="roundtopRed"><img src="Images/Curves/Red-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
       
      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="26" ><strong> 
         
        </tr>
      </table>
      
      <div class="roundbottomRed"><img src="Images/Curves/Red-LBC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
    </div>	
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div style="margin-left:20px;">
                        <div style="font-weight:bold">
                            Total Supplier Post Code: <asp:Label ID="lblTotalCount" runat="server" Text="Label"></asp:Label><br />
                            Total Valid Post Code: <asp:Label ID="lblValidPostCode" runat="server" Text="Label"></asp:Label>
                        </div>
                        <asp:Panel ID="pnlPostCodeData" runat="server" Visible="true">
                        <asp:GridView ID="gvSupplierPostCode"  runat="server" AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
                           CssClass="gridrow"  Width="800px" >          
                           <EmptyDataTemplate>
                           <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						            <tr>
							            <td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							            </td>
						            </tr>				
					            </table>
                              
                           </EmptyDataTemplate>
                           
                            <PagerTemplate>
                            
                               
                            </PagerTemplate> 
                            <Columns>  
                            <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px"  HeaderStyle-CssClass="gridHdr"  DataField="PostCode" HeaderText="PostCode" />
                            <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px"  HeaderStyle-CssClass="gridHdr"  DataField="GE" HeaderText="GridEast" />
                            <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px"  HeaderStyle-CssClass="gridHdr"  DataField="GN" HeaderText="GridNorth" />
                                                      
                           <asp:TemplateField Visible=true HeaderText="IsPostCode Valid">
                           <HeaderStyle Wrap=true CssClass="gridHdr" Width="50px" />
                           <ItemStyle Wrap=true  HorizontalAlign=Left Width="15px"  CssClass="gridRow"/>
                           <ItemTemplate>             
                               <%#SetValidateStatus(Container.DataItem("PostCode"))%>
                           </ItemTemplate>
                           </asp:TemplateField>                 
                            </Columns>
                           
                             <AlternatingRowStyle    CssClass=gridRow />
                             <RowStyle CssClass=gridRow />            
                            <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                            <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                            <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
                    
                          </asp:GridView>   
                          <br />
                          <br />
                          <asp:Button ID="btnUpdate" runat="server" Text="Click to Update Post Codes" Height="30" Width="220" />
                          </asp:Panel>
                          <asp:Panel ID="pnlConfirmation" runat="server" Visible="false">
                            <asp:Label ID="lblProcessing" runat="server" Text="lbl" ></asp:Label><img  align=middle src="Images/indicator.gif" />
                          </asp:Panel>
                          </div>
                        
                    </ContentTemplate>
                </asp:UpdatePanel>
  <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div></td>
  </tr>
  <tr>
    <td height="51" valign="top" bgcolor="#f4f5f0"><table width="945" height="46"  border="0" align="center" cellpadding="0" cellspacing="0" class="BdrTop">
      <tr valign="top">
        <td width="135" align="left" bgcolor="#f4f5f0" class="footerTxt"><img src="Images/Phone-Icon.gif" alt="Phone - 0203 053 0343" width="15" height="12" align="absmiddle" class="marginR5" /> 0203 053 0343 </td>
        <td align="left" bgcolor="#f4f5f0" class="footerTxt"><a href="mailto:info@orderwork.co.uk" class="footerTxtSelected"><img src="Images/Email-Icon.gif" alt="Contact Us" width="16" height="15" border="0" align="absmiddle" class="marginR5" />&nbsp; info@orderwork.co.uk</a></td>
        <td align="right" bgcolor="#f4f5f0" class="footerTxt"><!-- InstanceBeginEditable name="EdPrivacyPolicy" --><a href="https://www.orderwork.co.uk/privacy-policy/" target="_blank" class="footerTxtSelected">Policies</a><!-- InstanceEndEditable --></td>
      </tr>
    </table>	
    <div class="roundbottomWhite"><img src="Images/Curves/Main-LBC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div></td>
  </tr>
  <tr>
    <td height="35" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="footerTxt"><span >Copyright &copy; <script language="javascript" type="text/javascript">
									var currDate = new Date();
									document.write(currDate.getFullYear());
									</script> OrderWork Ltd.</span></td>
        <td align="right" class="footerTxt"> <!-- Design &amp; Developed by<a href="http://www.iniquus.com" target="_blank" class="footerTxt"> Iniquus</a> --></td>
      </tr>
    </table></td>
  </tr>
</table>
    </form>
</body>
</html>
