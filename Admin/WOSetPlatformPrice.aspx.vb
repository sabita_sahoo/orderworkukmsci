

Partial Public Class WOSetPlatformPrice
    Inherits System.Web.UI.Page

    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack() Then
            populateWOSummary()
        End If
    End Sub

    ''' <summary>
    ''' Function to populate the workorder summary
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateWOSummary()
        Dim ds As DataSet


        Dim woid As Integer = 0
        If Not IsNothing(Request("WOID")) Then
            If Request("WOID").Trim <> "" Then
                woid = Request("WOID").Trim
            End If
        End If

        ds = ws.WSWorkOrder.woGetSummary(woid, ApplicationSettings.ViewerAdmin, 0)



        If Not IsPostBack Then
            'Platform Price
            If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice")) Then
                txtSpendLimitPP.Text = ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice")
                ViewState("PlatformPrice") = ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice")
            End If
            If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("PPPerRate")) Then
                txtPPPerRate.Text = ds.Tables("tblWOSummary").Rows(0).Item("PPPerRate")
                ViewState("PPPerRate") = ds.Tables("tblWOSummary").Rows(0).Item("PPPerRate")
            End If
            If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("PerRate")) Then
                ddPPPerRate.SelectedValue = ds.Tables("tblWOSummary").Rows(0).Item("PerRate")
                ViewState("PerRate") = ds.Tables("tblWOSummary").Rows(0).Item("PerRate")
            End If
        End If

        'Store the version number in session
        CommonFunctions.StoreVerNum(ds.Tables("tblWOSummary"))

        'Workorder number
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("WorkOrderId")) Then
            'lblWONo.Text = ds.Tables("tblWOSummary").Rows(0).Item("WorkOrderId")
            ViewState("WorkOrderId") = ds.Tables("tblWOSummary").Rows(0).Item("WorkOrderId")
        End If
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("RefWOID")) Then
            lblWONo.Text = ds.Tables("tblWOSummary").Rows(0).Item("RefWOID")
        End If
        'Buyer Contact
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("BuyerContact")) Then
            lblBuyerContact.Text = ds.Tables("tblWOSummary").Rows(0).Item("BuyerContact")
        End If
        'Buyer Contact
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("BuyerCompany")) Then
            lblBuyerCompany.Text = ds.Tables("tblWOSummary").Rows(0).Item("BuyerCompany")
        End If
        'Location
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("Location")) Then
            lblLocation.Text = ds.Tables("tblWOSummary").Rows(0).Item("Location")
        End If
        'title
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("WOTitle")) Then
            lblTitle.Text = ds.Tables("tblWOSummary").Rows(0).Item("WOTitle")
        End If
        'Submitted date
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("DateCreated")) Then
            lblSubmitted.Text = Strings.FormatDateTime(ds.Tables("tblWOSummary").Rows(0).Item("DateCreated"), DateFormat.ShortDate)
        End If
        'Start date
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("DateStart")) Then
            lblWOStart.Text = Strings.FormatDateTime(ds.Tables("tblWOSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
        End If
        'End Date
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("DateEnd")) Then
            lblWOEnd.Text = Strings.FormatDateTime(ds.Tables("tblWOSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate)
        End If
        'Status
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("Status")) Then
            lblStatus.Text = ds.Tables("tblWOSummary").Rows(0).Item("Status")
            ViewState("Group") = ds.Tables("tblWOSummary").Rows(0).Item("Status")
        End If
        'Platform Price
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice")) Then
            If (ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice") = 0) Then
                lblPlatformPrice.Text = "Quote"
            Else
                lblPlatformPrice.Text = FormatNumber(ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice"), 2, TriState.True, TriState.False, TriState.False)
            End If
        Else
            lblPlatformPrice.Text = "-NA-"
        End If
        'Wholesale Price
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("WholesalePrice")) Then
            If (ds.Tables("tblWOSummary").Rows(0).Item("WholesalePrice") = 0) Then
                lblWholesalePrice.Text = "Quote"
            Else
                lblWholesalePrice.Text = FormatNumber(ds.Tables("tblWOSummary").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.False, TriState.False)
            End If
        Else
            lblWholesalePrice.Text = "-NA-"
        End If



        ViewState("LastActionRef") = ds.Tables("tblWOSummary").Rows(0).Item("LastActionRef")
        ViewState("WOID") = ds.Tables("tblWOSummary").Rows(0).Item("WOID")
    End Sub

#Region "Action"

    ''' <summary>
    ''' Function to update the DB
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkConfirm.Click
        Page.Validate()
        If Page.IsValid() Then
            divValidationMain.Visible = False

            Dim SpendLimitPP As Decimal = 0
            If (txtSpendLimitPP.Text.Trim <> "") Then
                SpendLimitPP = CDec(txtSpendLimitPP.Text.Trim)
            End If
            Dim PPPerRate As Decimal = 0
            If (txtPPPerRate.Text.Trim <> "") Then
                PPPerRate = CDec(txtPPPerRate.Text.Trim)
            End If
            Dim ds_Status As DataSet
            ds_Status = ws.WSWorkOrder.MS_WOSetPlatformPrice(ViewState("WOID"), SpendLimitPP, Session("CompanyId"), Session("UserId"), ApplicationSettings.RoleOWID, ViewState("LastActionRef"), txtWOComment.Text.Trim, Session("BizDivId"), chkReviewBid.Checked, PPPerRate, ddPPPerRate.SelectedValue, CommonFunctions.FetchVerNum())

            If ds_Status.Tables.Count <> 0 Then
                If ds_Status.Tables("tblStatus").Rows.Count > 0 Then
                    If ds_Status.Tables("tblStatus").Rows(0).Item("DBStatus") = -1 Then
                        'double entry                    
                    ElseIf ds_Status.Tables("tblStatus").Rows(0).Item("DBStatus") = -10 Then
                        divValidationMain.Visible = True
                        lblError.Text = ResourceMessageText.GetString("WOVersionControlMsg")
                        lblError.Text = lblError.Text.Replace("<Link>", "AdminWODetails.aspx?" & "WOID=" & ViewState("WOID") & "&WorkOrderID=" & ViewState("WOID") & "&FromDate=&ToDate=" & "&ContactID=" & Request("ContactId") & "&CompanyID=" & Request("CompanyID") & "&SupContactID=" & Request("SupContactID") & "&SupCompID=&PS=25&PN=0&SC=DateCreated&SO=1&BizDivID=1&sender=UCWOsListing&CompID=0")
                    Else
                        pnlSetPrice.Visible = False
                        pnlSuccess.Visible = True
                        lblSuccess.Text = "Portal Price set successfully for the WorkOrderId: " & ViewState("WorkOrderId")
                    End If
                Else
                    'no rows returned
                    divValidationMain.Visible = True
                    lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
                End If
            Else
                'no ds sent
                divValidationMain.Visible = True
                lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
            End If
        Else
            divValidationMain.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' Go back to listing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click, lnkBack.Click
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            If Request("sender").ToLower = "ucwoslisting" Then
                link = "AdminWOListing.aspx?"
            ElseIf Request("sender") = "SearchWO" Then
                link = "SearchWOs.aspx?"
            Else
                link = "AdminWODetails.aspx?"
            End If

            link &= "WOID=" & Request("WOID")
            link &= "&WorkOrderID=" & Request("WorkOrderID")
            link &= "&ContactId=" & Request("ContactId")
            link &= "&CompanyId=" & Request("CompanyId")
            link &= "&SupContactId=" & Request("SupContactId")
            link &= "&SupCompId=" & Request("SupCompId")
            If Not IsNothing(ViewState("Group")) Then
                link &= "&Group=" & ViewState("Group")
                link &= "&mode=" & ViewState("Group")
            Else
                link &= "&Group=" & Request("Group")
                link &= "&mode=" & Request("Group")
            End If
            link &= "&BizDivID=" & Request("BizDivID")
            link &= "&FromDate=" & Request("FromDate")
            link &= "&ToDate=" & Request("ToDate")
            link &= "&PS=" & Request("PS")
            link &= "&PN=" & Request("PN")
            link &= "&SC=" & Request("SC")
            link &= "&SO=" & Request("SO")
            link &= "&CompID=" & Request("CompID")
            If Not IsNothing(Request("SearchWorkorderID")) Then
                link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
            End If
            link &= "&sender=" & Request("sender")

            Response.Redirect(link)
        Else
            Response.Redirect("AdminWOListing.aspx?mode=Submitted")
        End If
    End Sub

#End Region

#Region "Validation"

    Private Sub rdo0ValuePP_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdo0ValuePP.CheckedChanged
        'Require Quotation
        If rdo0ValuePP.Checked Then
            txtSpendLimitPP.Enabled = False
            txtPPPerRate.Enabled = False
            txtPPPerRate.Text = "0"
            txtSpendLimitPP.Text = "0"
            ddPPPerRate.Enabled = False
            ddPPPerRate.SelectedValue = "Per Job"
            chkReviewBid.Visible = False
        Else
            txtSpendLimitPP.Enabled = True
            txtPPPerRate.Enabled = True
            ddPPPerRate.Enabled = True
            chkReviewBid.Visible = True
        End If
    End Sub

    Private Sub rdoNon0ValuePP_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdoNon0ValuePP.CheckedChanged
        'Proposed Price
        If rdoNon0ValuePP.Checked Then
            txtSpendLimitPP.Enabled = True
            txtSpendLimitPP.Text = ViewState("PlatformPrice")
            txtPPPerRate.Enabled = True
            txtPPPerRate.Text = ViewState("PPPerRate")
            ddPPPerRate.Enabled = True
            ddPPPerRate.SelectedValue = ViewState("PerRate")
            chkReviewBid.Visible = True
            CustPlatformPriceValidator.Enabled = True
        Else
            txtSpendLimitPP.Enabled = False
            txtPPPerRate.Enabled = False
            txtSpendLimitPP.Text = "0"
            txtPPPerRate.Text = "0"
            ddPPPerRate.Enabled = False
            ddPPPerRate.SelectedValue = "Per Job"
            chkReviewBid.Visible = False
            CustPlatformPriceValidator.Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Function to check is platform price is zero
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Public Sub ServerValidatePP(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustPlatformPriceValidator.ServerValidate, CustPPPerRateValidator.ServerValidate
        Try
            'Changed By Pankaj Malav on 08 Jan 2009
            'Description: Previously it is converting the value in int therefore value 0.25 is rounded of to 0 and thus showing validation
            'Chnaged into decimal.
            Dim num As Decimal = Decimal.Parse(CDec(args.Value))
            If rdoNon0ValuePP.Checked = True Then
                If (chkReviewBid.Checked = True) Then
                    If num >= 0 Then
                        args.IsValid = True
                    End If
                Else
                    If num <> 0 Then
                        args.IsValid = True
                    Else
                        args.IsValid = False
                    End If
                End If
                Exit Sub
            End If
        Catch exc As Exception
            args.IsValid = False
        End Try
        args.IsValid = True
    End Sub

#End Region



End Class