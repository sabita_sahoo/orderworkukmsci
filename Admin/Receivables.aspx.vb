
Partial Public Class Receivables
    Inherits System.Web.UI.Page

    Protected WithEvents UCDateRange1 As UCDateRange
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected trHeadings As System.Web.UI.HtmlControls.HtmlTableRow
    Protected trHeadings1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents btnAllocate As System.Web.UI.WebControls.LinkButton
    Protected WithEvents btnCancel As System.Web.UI.WebControls.LinkButton
    Protected tblAllocate As System.Web.UI.HtmlControls.HtmlTable
    Protected tblPaymentReceivedBtn As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents gvInvoices As Global.System.Web.UI.WebControls.GridView
    Protected WithEvents gvCashReceipts As Global.System.Web.UI.WebControls.GridView
    Protected WithEvents gvCreditNotes As Global.System.Web.UI.WebControls.GridView
    Protected WithEvents lblMsgCheckBox As System.Web.UI.WebControls.Label
    Protected WithEvents Check As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Chk1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Chk2 As System.Web.UI.WebControls.CheckBox
    Shared ws As New WSObjs
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'trHeadings.Visible = False
        'trHeadings1.Visible = False
        Security.SecurePage(Page)
        If Not Page.IsPostBack Then

            If Not Request("sender") Is Nothing Then
                ProcessBackToListing()
            Else
                UCSearchContact1.BizDivID = Session("BizDivId")
                UCSearchContact1.ClassId = 0
                UCSearchContact1.Filter = "GenerateCR"
                UCSearchContact1.populateContact()
                Dim currDate As DateTime = System.DateTime.Today
                txtPaymentDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
                ViewState!SortExpression = "InvoiceDate"
                gvInvoices.Sort(ViewState!SortExpression, SortDirection.Ascending)
                ViewState("ContactId") = UCSearchContact1.ddlContact.SelectedValue
                'btnExportSalesInvoice.HRef = "ExportToExcel.aspx?page=Receivables&bizDivId=" & Session("BizDivId") & "&adviceType=Sales&ContactId=" & ViewState("ContactId") & "&status=UnPaid" & "&sortExpression=" & gvInvoices.SortExpression
                'btnExportUnPaidCN.HRef = "ExportToExcel.aspx?page=Receivables&bizDivId=" & Session("BizDivId") & "&adviceType=UCN&ContactId=" & ViewState("ContactId") & "&status=UCN" & "&sortExpression=InvoiceDate"
                'btnExportUnallocatedCR.HRef = "ExportToExcel.aspx?page=Receivables&bizDivId=" & Session("BizDivId") & "&adviceType=UnAllocated&ContactId=" & ViewState("ContactId") & "&status=UnAllocated" & "&sortExpression=InvoiceDate"
            End If

            divEarlyDiscValidationGrid.Visible = False
        End If
        If UCSearchContact1.ddlContact.SelectedValue = "" Then
            gvInvoices.Visible = False
            gvCashReceipts.Visible = False
            trHeadings.Visible = False
            trHeadings1.Visible = False
            gvCreditNotes.Visible = False
            div2.Visible = False
            div3.Visible = False
        Else
            gvInvoices.Visible = True
            gvCashReceipts.Visible = True
            gvCreditNotes.Visible = True
            trHeadings.Visible = True
            trHeadings1.Visible = True
            div2.Visible = True
            div3.Visible = True
        End If

    End Sub

    Public Sub PopulateGrid()
        gvInvoices.DataBind()
        gvCashReceipts.DataBind()
        gvCreditNotes.DataBind()
        'btnExportSalesInvoice.HRef = "ExportToExcel.aspx?page=Receivables&bizDivId=" & Session("BizDivId") & "&adviceType=Sales&ContactId=" & ViewState("ContactId") & "&status=UnPaid" & "&sortExpression=" & gvInvoices.SortExpression
        'btnExportUnPaidCN.HRef = "ExportToExcel.aspx?page=Receivables&bizDivId=" & Session("BizDivId") & "&adviceType=UCN&ContactId=" & ViewState("ContactId") & "&status=UCN" & "&sortExpression=InvoiceDate"
        'btnExportUnallocatedCR.HRef = "ExportToExcel.aspx?page=Receivables&bizDivId=" & Session("BizDivId") & "&adviceType=UnAllocated&ContactId=" & ViewState("ContactId") & "&status=UnAllocated" & "&sortExpression=InvoiceDate"
    End Sub

    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then
            trHeadings.Visible = True
            trHeadings1.Visible = True
            If Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID Or Session("RoleGroupID") = ApplicationSettings.RoleOWFinanceID Or Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                tblAllocate.Visible = True
            Else
                tblAllocate.Visible = False
            End If
            divListings.Visible = True
            lblMsg.Text = ""
            ViewState("ContactId") = UCSearchContact1.ddlContact.SelectedValue
            PopulateGrid()
            divValidationMain.Visible = False

            divEarlyDiscValidationGrid.Visible = False
        End If
    End Sub
    Private Sub lnkExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExport.Click
        Dim selectedIds As String = ""
        selectedIds = CommonFunctions.getSelectedIdsOfGrid(gvInvoices, "Check", "hdnInvoiceNo")
        selectedIds = selectedIds & "," & CommonFunctions.getSelectedIdsOfGrid(gvCreditNotes, "Chk2", "hdnInvoiceNo2")
        selectedIds = selectedIds & "," & CommonFunctions.getSelectedIdsOfGrid(gvCashReceipts, "Chk1", "hdnInvoiceNo1")

        'Dim strurl As String
        hdnEtoE.Value = ApplicationSettings.WebPath & "ExportToExcel.aspx?page=Receivables&bizDivId=" & Session("BizDivId") & "&adviceType=UCN&ContactId=" & ViewState("ContactId") & "&status=ETE" & "&sortExpression=InvoiceDate" & "&Invoices=" & selectedIds

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "js", "handleReceivablesEtoE();", True)
        PopulateGrid()

    End Sub

    ''' <summary>
    ''' Method handles the page selected by the user
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvInvoices.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvInvoices.DataBind()
    End Sub

    Public Sub ddlPageSelector_SelectedIndexChanged1(ByVal sender As Object, ByVal e As EventArgs)
        gvCashReceipts.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvCashReceipts.DataBind()
    End Sub

    Public Sub ddlPageSelector_SelectedIndexChanged2(ByVal sender As Object, ByVal e As EventArgs)
        gvCreditNotes.PageIndex = CType(sender, DropDownList).SelectedIndex
        gvCreditNotes.DataBind()
    End Sub

    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating, ObjectDataSource2.ObjectCreating, ObjectDataSource3.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing, ObjectDataSource2.ObjectDisposing, ObjectDataSource3.ObjectDisposing
        e.Cancel = True
    End Sub

    Private Sub gvInvoices_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvInvoices.DataBound
        If Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID Or Session("RoleGroupID") = ApplicationSettings.RoleOWFinanceID Or Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
            CType(gvInvoices.TopPagerRow.FindControl("tblPaymentReceivedBtn"), System.Web.UI.HtmlControls.HtmlGenericControl).Visible = True
        Else
            CType(gvInvoices.TopPagerRow.FindControl("tblPaymentReceivedBtn"), System.Web.UI.HtmlControls.HtmlGenericControl).Visible = False
        End If
    End Sub

    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvInvoices.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvInvoices, e.Row, Me)
        End If
    End Sub

    Protected Sub GridView2_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvCashReceipts.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvCashReceipts, e.Row, Me)
        End If
    End Sub

    Protected Sub GridView3_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvCreditNotes.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvCreditNotes, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Object)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)


        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        Dim strInnerHtml As String = ResourceMessageText.GetString("ShowingRecords")
        strInnerHtml = strInnerHtml.Replace("<from>", from)
        strInnerHtml = strInnerHtml.Replace("<calTo>", calTo)
        tdTotalCount.InnerHtml = strInnerHtml & ViewState("rowCount")

        'tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex

        ShowHideGrid()


    End Sub

    Public Sub ShowHideGrid()
        If UCSearchContact1.ddlContact.SelectedValue = "" Then
            gvInvoices.Visible = False
            gvCashReceipts.Visible = False
            gvCreditNotes.Visible = False
            trHeadings.Visible = False
            trHeadings1.Visible = False
            div2.Visible = False
            div3.Visible = False
        Else
            gvInvoices.Visible = True
            gvCashReceipts.Visible = True
            gvCreditNotes.Visible = True
            trHeadings.Visible = True
            trHeadings1.Visible = True
            div2.Visible = True
            div3.Visible = True
        End If
    End Sub


    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                       ByVal maximumRows As System.Nullable(Of Integer)) As DataSet

        Dim ds As DataSet
        Dim bizDivId As Integer = Session("BizDivId")
        Dim contactid As String = ""
        Dim AdviceType As String = "Sales"
        Dim status As String = "Unpaid"
        If CStr(ViewState("ContactId")) = "" Then
            contactid = "0"
        Else
            contactid = ViewState("ContactId")
        End If
        Dim maxRows As Integer = 0
        If chkShowAll.Checked Then
            maxRows = 0
        Else
            maxRows = maximumRows
        End If

        ds = ws.WSFinance.MS_GetSalesReceiptAdviceListing(Session("BizDivId"), AdviceType, contactid, status, "", "", "admin", sortExpression, startRowIndex, maxRows, "", "")
        ViewState("rowCount") = ds.Tables("tblCount").Rows(0)(0)

        'Even if all show all is checked then paging should be shown so that the button "Payment Received" is visible.
        'If Not ViewState("rowCount") Is Nothing Then
        '    If chkShowAll.Checked Then
        '        gvInvoices.AllowPaging = False
        '    Else
        '        gvInvoices.AllowPaging = True
        '    End If
        'End If
        Return ds
    End Function

    Public Function SelectFromDB1(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                       ByVal maximumRows As System.Nullable(Of Integer)) As DataSet

        Dim ds As DataSet
        Dim bizDivId As Integer = Session("BizDivId")
        Dim contactid As String = ""
        Dim AdviceType As String = "UA Receipt"
        Dim status As String = "UnAllocated"
        If CStr(ViewState("ContactId")) = "" Then
            contactid = "0"
        Else
            contactid = ViewState("ContactId")
        End If
        Dim maxRows As Integer = 0
        If chkShowAll.Checked Then
            maxRows = 0
        Else
            maxRows = maximumRows
        End If
        ViewState!SortExpression = "InvoiceDate"


        ds = ws.WSFinance.MS_GetSalesReceiptAdviceListing(Session("BizDivId"), AdviceType, contactid, status, "", "", "admin", ViewState!SortExpression, startRowIndex, maxRows, "", "")
        ViewState("rowCount") = ds.Tables("tblCount").Rows(0)(0)

        If Not ViewState("rowCount") Is Nothing Then
            If chkShowAll.Checked Then
                gvCashReceipts.AllowPaging = False
            Else
                gvCashReceipts.AllowPaging = True
            End If
        End If
        Return ds
    End Function

    Public Function SelectFromDB2(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), _
                                       ByVal maximumRows As System.Nullable(Of Integer)) As DataSet

        Dim ds As DataSet
        Dim bizDivId As Integer = Session("BizDivId")
        Dim contactid As String = ""
        Dim AdviceType As String = "Credit Note"
        Dim status As String = "UCN"
        If CStr(ViewState("ContactId")) = "" Then
            contactid = "0"
        Else
            contactid = ViewState("ContactId")
        End If
        Dim maxRows As Integer = 0
        If chkShowAll.Checked Then
            maxRows = 0
        Else
            maxRows = maximumRows
        End If
        ViewState!SortExpression = "InvoiceDate"


        ds = ws.WSFinance.MS_GetSalesReceiptAdviceListing(Session("BizDivId"), AdviceType, contactid, status, "", "", "admin", ViewState!SortExpression, startRowIndex, maxRows, "", "")
        ViewState("rowCount") = ds.Tables("tblCount").Rows(0)(0)

        If Not ViewState("rowCount") Is Nothing Then
            If chkShowAll.Checked Then
                gvCreditNotes.AllowPaging = False
            Else
                gvCreditNotes.AllowPaging = True
            End If
        End If
        Return ds
    End Function

    ''' <summary>
    ''' To get the rowcount of listing
    ''' </summary>
    ''' <returns>Row count</returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Public Function SelectCount1() As Integer
        Return ViewState("rowCount")
    End Function

    Public Function SelectCount2() As Integer
        Return ViewState("rowCount")
    End Function

    Public Function GetLinks(ByVal invoiceNo As String, ByVal companyId As String, ByVal OnHold As Boolean) As String
        Dim link As String = ""
        Dim linkParams As String = ""

        linkParams &= "bizDivId=" & Session("BizDivId")
        linkParams &= "&companyId=" & companyId
        linkParams &= "&invoiceNo=" & invoiceNo
        linkParams &= "&PS=" & gvInvoices.PageSize
        linkParams &= "&PN=" & gvInvoices.PageIndex
        linkParams &= "&SC=" & gvInvoices.SortExpression
        linkParams &= "&SO=" & gvInvoices.SortDirection
        linkParams &= "&sender=Receivables"

        link &= "<a target='_blank' href=' ViewSalesInvoice.aspx?" & linkParams & "'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Invoices' title='View Invoices' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        link &= "<a  runat='server' href='SalesInvoiceWOListing.aspx?" & linkParams & "'><img runat='server' src='Images/Icons/View-All-WO-SI.gif' alt='view Work Orders' title='view Work Orders' width='14' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        If (OnHold = False) Then
            link &= "<a runat='server' href='InvoiceApproval.aspx?" & linkParams & "'><img src='Images/Icons/Edit.gif' alt='Edit Invoice' title='Edit Invoice' width='12' height='11' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        End If
        Return link
    End Function

    ''' <summary>
    ''' Method to make the column as sortable with the icon of DESC or ASC sort
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Private Sub gvInvoices_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvInvoices.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True
            End If
        End If

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvInvoices.RowDataBound
        MakeGridViewHeaderClickable(gvInvoices, e.Row)
    End Sub
    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvCashReceipts.RowDataBound
        MakeGridViewHeaderClickable(gvCashReceipts, e.Row)
    End Sub

    Protected Sub GridView3_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvCreditNotes.RowDataBound
        MakeGridViewHeaderClickable(gvCreditNotes, e.Row)
    End Sub

    Public Sub PaymentReceived(ByVal sender As Object, ByVal e As EventArgs)
        If txtPaymentDate.Text = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please enter date for Payment."
            gvInvoices.DataBind()
            Return
        Else
            Try
                Dim tmpFromDate As DateTime = DateTime.Parse(txtPaymentDate.Text.Trim)
            Catch ex As Exception
                divValidationMain.Visible = True
                lblMsg.Text = "Please enter valid date for Payment."
                gvInvoices.DataBind()
                Return
            End Try
            divValidationMain.Visible = False
            lblMsg.Text = ""
        End If
        lblMsg.Text = ""
        divValidationMain.Visible = False
        Dim invoices As String = CommonFunctions.getSelectedIdsOfGrid(gvInvoices, "Check", "hdnInvoiceNo")
        If invoices = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please Select atleast one unpaid sales invoice."
            gvInvoices.DataBind()
            Return
        End If
        If UCSearchContact1.ddlContact.SelectedValue = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please Select a Contact."
            gvInvoices.DataBind()
            Return
        Else
            ViewState("ContactId") = UCSearchContact1.ddlContact.SelectedValue
            lblMsg.Text = ""
            divValidationMain.Visible = False
        End If
        'TODO for NP :)
        Dim ds As New DataSet
        ds = ws.WSFinance.MSAccounts_GenerateCashReceipt(Session("BizDivId"), invoices, ViewState("ContactId"), txtPaymentDate.Text, 0, Session("UserId"))
        If ds.Tables.Count > 0 Then
            If ds.Tables("Success").Rows(0).Item("Success") = 0 Then
                divValidationMain.Visible = True
                lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
                gvInvoices.DataBind()
            Else
                'Commented because sales invoices are delinked from the purchase invoices
                'If ds.Tables("SupEmailData").Rows.Count > 0 Then
                '    Emails.SendMailPaymentAvailableInInvoiceSet(ds.Tables("SupEmailData").DefaultView)
                '    gvInvoices.DataBind()
                'End If
                If ds.Tables("Success").Rows(0).Item("DefSIs") <> "" Then
                    divValidationMain.Visible = True
                    divListings.Visible = False
                    divEarlyDiscValidationGrid.Visible = True
                    lblMsg.Text = "Following Invoices cannot be marked as paid as these invoices includes early discounts and the time period of early discounts are over"
                    gvEarlyDiscInvoices.DataSource = ds.Tables(1)
                    ViewState("tblEarlyDiscounts") = ds.Tables(1)
                    gvEarlyDiscInvoices.DataBind()
                End If

            End If
            gvInvoices.DataBind()
        End If
        gvInvoices.DataBind()

    End Sub

    Public Sub SendEmailNotification(ByVal sender As Object, ByVal e As EventArgs)
        lblMsg.Text = ""
        divValidationMain.Visible = False
        Dim invoices As String = CommonFunctions.getSelectedIdsOfGrid(gvInvoices, "Check", "hdnInvoiceNo")
        If invoices = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please Select atleast one unpaid sales invoice."
            gvInvoices.DataBind()
            Return
        End If
        If UCSearchContact1.ddlContact.SelectedValue = "" Then
            divValidationMain.Visible = True
            lblMsg.Text = "Please Select a Contact."
            gvInvoices.DataBind()
            Return
        Else
            ViewState("ContactId") = UCSearchContact1.ddlContact.SelectedValue
            lblMsg.Text = ""
            divValidationMain.Visible = False
        End If

        Dim ds As New DataSet
        ds = ws.WSFinance.MSAccounts_GetSIEmailNotification(invoices)
        For i As Integer = 0 To (ds.Tables.Count - 1)
            If ds.Tables(i).Rows.Count > 0 Then
                Dim dvEmail As New DataView
                dvEmail = ds.Tables(i).DefaultView
                'Call send email function
                Emails.SendMailSIGeneration(dvEmail, ApplicationSettings.OWUKBizDivId)
            End If
        Next i
        gvInvoices.DataBind()

    End Sub

    Public Sub ProcessBackToListing()
        UCSearchContact1.BizDivID = Session("BizDivId")
        UCSearchContact1.ClassId = 0
        UCSearchContact1.Filter = "GenerateCR"
        txtPaymentDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)
        UCSearchContact1.populateContact()
        Dim lItem As ListItem = UCSearchContact1.ddlContact.Items.FindByValue(Request("companyId"))
        If Not lItem Is Nothing Then
            UCSearchContact1.ddlContact.SelectedValue = Request("companyId")
            ViewState("ContactId") = Request("companyId")
        End If
        If Request("sender") = "Receivables" Then
            gvInvoices.PageIndex = Request("PN")
            gvInvoices.Sort(Request("SC"), Request("SO"))
        Else
            gvInvoices.PageIndex = 0
            gvInvoices.Sort("InvoiceDate", 0)
        End If
        PopulateGrid()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Response.Redirect("SupplierPaymentsOutstanding.aspx")
        UCSearchContact1.ddlContact.SelectedIndex = 0

        gvInvoices.Visible = False
        gvCashReceipts.Visible = False
        gvCreditNotes.Visible = False
        trHeadings.Visible = False
        trHeadings1.Visible = False
        div2.Visible = False
        div3.Visible = False
        lblMsgCheckBox.Text = ""
    End Sub

    Private Sub btnAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllocate.Click

        Dim cntSalesInvoice As Integer = 0
        Dim cntCashReceipt As Integer = 0
        Dim cntCreditNote As Integer = 0
        Dim SIAmount As Decimal = 0.0
        Dim totalCashReceiptAmt As Decimal = 0.0
        Dim totalCreditNoteAmt As Decimal = 0.0
        Dim SalesInvoiceNumber As String = ""
        Dim SIBusinessArea As Integer = -1
        Dim CRBusinessArea As Integer = -1
        Dim CNBusinessArea As Integer = -1
        'Check how many sales invoices are selected
        For Each row As GridViewRow In gvInvoices.Rows
            If CType(row.FindControl("Check"), CheckBox).Checked = True Then
                cntSalesInvoice = cntSalesInvoice + 1
            End If
        Next

        'Make sure that atleast one cash receipt is selected
        For Each row As GridViewRow In gvCashReceipts.Rows
            If CType(row.FindControl("Chk1"), CheckBox).Checked = True Then
                cntCashReceipt = cntCashReceipt + 1
                Exit For
            End If
        Next

        'Make sure that atleast one credit note is selected
        For Each row As GridViewRow In gvCreditNotes.Rows
            If CType(row.FindControl("Chk2"), CheckBox).Checked = True Then
                cntCreditNote = cntCreditNote + 1
                Exit For
            End If
        Next
        If cntCashReceipt = 0 And cntCreditNote = 0 Then
            lblMsgCheckBox.Visible = True
            lblMsgCheckBox.Text = "Please select atleast one cash receipt or credit note."
            divValidationMain.Visible = True
            lblMsg.Text = "Please select atleast one cash receipt or credit note."
            gvInvoices.DataBind()
            gvCashReceipts.DataBind()
            gvCreditNotes.DataBind()
            Return
        End If
        If cntSalesInvoice > 1 Then
            lblMsgCheckBox.Visible = True
            lblMsgCheckBox.Text = "Please select only one Sales Invoice"
            divValidationMain.Visible = True
            lblMsg.Text = "Please select only one Sales Invoice"
            gvInvoices.DataBind()
            gvCashReceipts.DataBind()
            gvCreditNotes.DataBind()
            Return
        ElseIf cntSalesInvoice = 0 Then
            lblMsgCheckBox.Visible = True
            lblMsgCheckBox.Text = "Please select one Sales Invoice"
            divValidationMain.Visible = True
            lblMsg.Text = "Please select one Sales Invoice"
            gvInvoices.DataBind()
            gvCashReceipts.DataBind()
            gvCreditNotes.DataBind()
            Return
        Else
            lblMsgCheckBox.Visible = False
            'Find the sales invoice number and the amount
            For Each row As GridViewRow In gvInvoices.Rows
                If CType(row.FindControl("Check"), CheckBox).Checked = True Then
                    SalesInvoiceNumber = CType(row.FindControl("hdnInvoiceNo"), HtmlInputHidden).Value
                    SIAmount = CType(row.FindControl("hdnSIAmount"), HtmlInputHidden).Value
                    SIBusinessArea = CInt(CType(row.FindControl("hdnSIBusinessArea"), HtmlInputHidden).Value)
                End If
            Next

            Dim dt As New DataTable("tblAllocateFunds")
            Dim col1 As New DataColumn("SalesInvoiceNumber")
            Dim col2 As New DataColumn("Type")
            Dim col3 As New DataColumn("AllocateInvoice")
            Dim col4 As New DataColumn("BizDivID")
            dt.Columns.Add(col1)
            dt.Columns.Add(col2)
            dt.Columns.Add(col3)
            dt.Columns.Add(col4)
            Dim drow As DataRow
            Dim count As Integer = 0
            For Each row As GridViewRow In gvCashReceipts.Rows
                If CType(row.FindControl("Chk1"), CheckBox).Checked = True Then
                    drow = dt.NewRow
                    drow("SalesInvoiceNumber") = SalesInvoiceNumber
                    drow("Type") = "CashReceipt"
                    drow("AllocateInvoice") = CType(row.FindControl("hdnInvoiceNo1"), HtmlInputHidden).Value
                    drow("BizDivID") = Session("BizDivId")
                    dt.Rows.Add(drow)
                    totalCashReceiptAmt = totalCashReceiptAmt + CType(row.FindControl("hdnCRAmount"), HtmlInputHidden).Value
                    CRBusinessArea = CInt(CType(row.FindControl("hdnCRBusinessArea"), HtmlInputHidden).Value)
                    count = count + 1
                End If
            Next
            If count <> 0 Then
                CRBusinessArea = CRBusinessArea / count
                count = 0
            End If

            For Each row As GridViewRow In gvCreditNotes.Rows
                If CType(row.FindControl("Chk2"), CheckBox).Checked = True Then
                    drow = dt.NewRow
                    drow("SalesInvoiceNumber") = SalesInvoiceNumber
                    drow("Type") = "CreditNote"
                    drow("AllocateInvoice") = CType(row.FindControl("hdnInvoiceNo2"), HtmlInputHidden).Value
                    drow("BizDivID") = Session("BizDivId")
                    dt.Rows.Add(drow)
                    totalCreditNoteAmt = totalCreditNoteAmt + CType(row.FindControl("hdnCNAmount"), HtmlInputHidden).Value
                    CNBusinessArea = CInt(CType(row.FindControl("hdnCNBusinessArea"), HtmlInputHidden).Value)
                    count = count + 1
                End If
            Next
            If count <> 0 Then
                CNBusinessArea = CNBusinessArea / count
                count = 0
            End If

            If SIAmount <> (totalCashReceiptAmt + totalCreditNoteAmt) Then
                lblMsgCheckBox.Visible = True
                lblMsgCheckBox.Text = "Sorry!!! Sales Invoice Amount does not match with the total of Cash Receipt and Credit Note Amount."
                divValidationMain.Visible = True
                lblMsg.Text = "Sorry!!! Sales Invoice Amount does not match with the total of Cash Receipt and Credit Note Amount."
                gvInvoices.DataBind()
                gvCashReceipts.DataBind()
                gvCreditNotes.DataBind()
            Else
                Dim ds As New DataSet
                ds.Tables.Add(dt)
                ds.DataSetName = "dsAllocateFunds"
                Dim xmlContent As String = ds.GetXml
                ds = ws.WSFinance.MSAccounts_AllocateFunds(xmlContent, Session("BizDivId"), SalesInvoiceNumber.ToString.Trim, ViewState("ContactId"), txtPaymentDate.Text, Session("UserId"))

                'If ds.Tables("SupEmailData").Rows.Count > 0 Then
                '    Emails.SendMailPaymentAvailableInInvoiceSet(ds.Tables("SupEmailData").DefaultView)
                'End If

                gvInvoices.DataBind()
                gvCashReceipts.DataBind()
                gvCreditNotes.DataBind()
                lblMsgCheckBox.Visible = True
                lblMsgCheckBox.Text = "Selected Sales Invoice is marked as Paid."
                divValidationMain.Visible = False
                lblMsg.Text = ""
            End If
        End If
    End Sub

    Public Sub CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim HidePaymentBtn As Boolean = False
        Dim i As Integer = 0
        For i = 0 To gvCreditNotes.Rows.Count - 1

            If Not CType(gvCreditNotes.Rows.Item(i).FindControl("Chk2"), System.Web.UI.WebControls.CheckBox) Is Nothing Then
                If CType(gvCreditNotes.Rows.Item(i).FindControl("Chk2"), System.Web.UI.WebControls.CheckBox).Checked = True Then
                    HidePaymentBtn = True
                    Exit For
                End If
            End If

        Next

        If HidePaymentBtn = False Then
            For i = 0 To gvCashReceipts.Rows.Count - 1

                If Not CType(gvCashReceipts.Rows.Item(i).FindControl("Chk1"), System.Web.UI.WebControls.CheckBox) Is Nothing Then
                    If CType(gvCashReceipts.Rows.Item(i).FindControl("Chk1"), System.Web.UI.WebControls.CheckBox).Checked = True Then
                        HidePaymentBtn = True
                        Exit For
                    End If
                End If
            Next
        End If

        If HidePaymentBtn = True Then
            CType(gvInvoices.TopPagerRow.FindControl("tblPaymentReceivedBtn"), System.Web.UI.HtmlControls.HtmlTable).Visible = False
        Else
            If Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID Or Session("RoleGroupID") = ApplicationSettings.RoleOWFinanceID Or Session("RoleGroupID") = ApplicationSettings.RoleFinanceManagerID Then
                CType(gvInvoices.TopPagerRow.FindControl("tblPaymentReceivedBtn"), System.Web.UI.HtmlControls.HtmlTable).Visible = True
            End If
        End If

    End Sub

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub

    Public Sub PayInclDisc(ByVal sender As Object, ByVal e As EventArgs)
        divValidationMain.Visible = False
        Dim ds As New DataSet
        ds = ws.WSFinance.MSAccounts_GenerateCashReceipt(Session("BizDivId"), hdnOnPageInvoiceNo.Text, ViewState("ContactId"), txtPaymentDate.Text, 2, Session("UserId"))
        If ds.Tables.Count > 0 Then
            If ds.Tables("Success").Rows(0).Item("Success") = 0 Then
                divValidationMain.Visible = True
                lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
            Else
                PopulateEarlyDiscountGrid(hdnOnPageInvoiceNo.Text)
            End If
        End If
    End Sub

    Public Sub PayExclDisc(ByVal sender As Object, ByVal e As EventArgs)
        divListings.Visible = False
        divValidationMain.Visible = True
        lblMsg.Text = "Please ensure you post a <a href=' CreateAdjustment.aspx?ContactID=" & ViewState("ContactId") & "&InvoiceNo=" & hdnOnPageInvoiceNo.Text & "'>manual adjustment</a> to offset the missed discount amount.<br>Click <a href='javascript:CallFuncToPayExclDisc();'>here</a> to mark the invoice paid excluding the discount"
    End Sub
    Public Sub PayInvoiceExclDiscount(ByVal sender As Object, ByVal e As EventArgs)
        Dim ds As New DataSet
        ds = ws.WSFinance.MSAccounts_GenerateCashReceipt(Session("BizDivId"), hdnOnPageInvoiceNo.Text, ViewState("ContactId"), txtPaymentDate.Text, 1, Session("UserId"))
        If ds.Tables.Count > 0 Then
            If ds.Tables("Success").Rows(0).Item("Success") = 0 Then
                divValidationMain.Visible = True
                lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
                gvInvoices.DataBind()
            Else
                'PopulateEarlyDiscountGrid(hdnOnPageInvoiceNo.Text)
                divListings.Visible = False
                divValidationMain.Visible = True
                divEarlyDiscValidationGrid.Visible = False
                lblMsg.Text = "Please ensure you post a <a href=' CreateAdjustment.aspx?ContactID=" & ViewState("ContactId") & "&InvoiceNo=" & hdnOnPageInvoiceNo.Text & "'>manual adjustment</a> to offset the missed discount amount."
            End If
        End If
    End Sub

    Private Sub PopulateEarlyDiscountGrid(ByVal InvoiceNo As String)
        divValidationMain.Visible = False
        Dim dt As DataTable
        dt = CType(ViewState("tblEarlyDiscounts"), DataTable)

        Dim Row As Integer, Col As Integer
        Col = 0
        Dim DelRow As DataRow
        For Row = 0 To dt.Rows.Count - 1
            Dim hasvalue As Boolean = False
            Dim dR As DataRow = dt.Rows(Row)
            If dt.Rows(Row)(Col) = InvoiceNo Then
                hasvalue = True
            End If
            If hasvalue = True Then
                DelRow = dR
            End If
        Next
        dt.Rows.Remove(DelRow)

        gvEarlyDiscInvoices.DataSource = dt
        gvEarlyDiscInvoices.DataBind()
    End Sub

End Class
