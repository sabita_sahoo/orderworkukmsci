

Partial Public Class FavouriteSupplierList
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs

#Region "Declaration"

    Protected WithEvents lblBuyerCompany As System.Web.UI.WebControls.Label
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label

    Protected WithEvents gvFavSuppliers As System.Web.UI.WebControls.GridView
    Protected WithEvents ObjectDataSource1 As ObjectDataSource

    Protected WithEvents hdnPageNo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hdnPageSize As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lnkBackToListing As System.Web.UI.HtmlControls.HtmlAnchor


    Protected WithEvents tdRemove As HtmlTableCell



    Shared _CompanyID As Integer
    Public Shared Property CompanyID() As Integer
        Get
            Return _CompanyID
        End Get
        Set(ByVal value As Integer)
            _CompanyID = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            Security.SecurePage(Page)
            If Not IsNothing(Request("mode")) Then
                If Request("mode") = "blacklist" Then
                    'Response.Redirect("ServicePartnerBlacklist.aspx?mode=" & Request("mode"))
                    lblBuyerCompany.Text = "Blacklist Suppliers For " & Session("BuyerCompanyName")
                    ViewState("mode") = "blacklist"
                    hdnMode.Value = "blacklist"
                    Page.Title = "OrderWork : Blacklist Suppliers"
                    gvFavSuppliers.Columns.Item(3).Visible = False
                    gvFavSuppliers.Columns.Item(4).Visible = False
                    gvFavSuppliers.Columns.Item(5).Visible = False
                ElseIf Request("mode") = "ClientBlacklist" Then
                    lblType.Text = "Client"
                    lblSearchType.Text = "Client"
                    tdAddSP.Visible = False
                    tdRemove.Visible = False
                    lblBuyerCompany.Text = "Client Blacklist For " & Session("BuyerCompanyName")
                    gvFavSuppliers.Columns.Item(1).HeaderText = "Client"
                    gvFavSuppliers.Columns.Item(3).Visible = False
                    gvFavSuppliers.Columns.Item(4).Visible = False
                    gvFavSuppliers.Columns.Item(5).Visible = False
                    ViewState("mode") = "ClientBlacklist"
                    hdnMode.Value = "ClientBlacklist"
                    Page.Title = "OrderWork : Client Blacklist"
                Else
                    lblBuyerCompany.Text = "Favourite Suppliers For " & Session("BuyerCompanyName")
                    trFixedCapacity.Visible = True
                    ViewState("mode") = "favourite"
                    hdnMode.Value = "favourite"
                    Page.Title = "OrderWork : Favourite Suppliers"
                End If
            Else
                lblBuyerCompany.Text = "Favourite Suppliers For " & Session("BuyerCompanyName")
                ViewState("mode") = "favourite"
                hdnMode.Value = "favourite"
            End If

            If Not IsNothing(Request("sender")) Then
                If Request("sender") = "CompanyProfile" Then
                    processBackToListing()
                Else
                    InitializeForm()
                End If
            Else
                InitializeForm()
            End If


        End If
    End Sub

    Private Sub processBackToListing()

        ViewState("bizDivId") = Session("ContactBizDivID")

        If Trim(Request("PS")) <> "" Then
            If IsNumeric(Trim(Request("PS"))) = True Then
                If CType(Trim(Request("PS")), Integer) <> 0 Then
                    hdnPageSize.Value = CType(Trim(Request("PS")), Integer)
                End If
            End If
        End If

        hdnPageNo.Value = 0
        If Trim(Request("PN")) <> "" Then
            If IsNumeric(Trim(Request("PN"))) = True Then
                If CType(Trim(Request("PN")), Integer) <> 0 Then
                    hdnPageNo.Value = CType(Trim(Request("PN")), Integer)
                End If
            End If
        End If

        ViewState!SortExpression = ""
        If Trim(Request("SC")) <> "" Then
            ViewState!SortExpression = Trim(Request("SC"))
        Else
            ViewState!SortExpression = "CompanyName"
        End If

        Dim sd As SortDirection
        sd = SortDirection.Ascending
        If Trim(Request("SO")) <> "" Then
            If Trim(Request("SO")).ToLower = "true" Then
                sd = SortDirection.Ascending
            End If
        End If

        If Not IsNothing(Request("companyid")) Then
            If Request("companyid") <> "" Then
                Session("ContactCompanyID") = Request("companyid")
            End If
        End If

        If Not IsNothing(Request("companyid")) Then
            If Request("companyid") <> "" Then
                Session("ContactCompanyID") = Request("companyid")
            End If
        End If

        If Not IsNothing(Request("bizdivid")) Then
            If Request("bizdivid") <> "" Then
                Session("ContactBizDivID") = Request("bizdivid")
            End If
        End If


        If Not IsNothing(Request("rolegroupid")) Then
            If Request("rolegroupid") <> "" Then
                Session("ContactRoleGroupID") = Request("rolegroupid")
            End If
        End If

        If Not IsNothing(Request("userid")) Then
            If Request("userid") <> "" Then
                Session("ContactUserID") = Request("userid")
            End If
        End If

        If Not IsNothing(Request("classid")) Then
            If Request("classid") <> "" Then
                Session("ContactClassID") = Request("classid")
            End If
        End If

        If Not IsNothing(Request("contacttype")) Then
            If Request("contacttype") <> "" Then
                Session("ContactType") = Request("contacttype")
            End If
        End If

        If Not IsNothing(Request("statusId")) Then
            If Request("statusId") <> "" Then
                Session("ContactStatusId") = Request("statusId")
            End If
        End If

        If Not IsNothing(Request("mode")) Then
            If Request("mode") <> "" Then
                Session("ListingMode") = Request("mode")
            End If
        End If

        If Not IsNothing(Request("buyerCompName")) Then
            If Request("buyerCompName") <> "" Then
                Session("BuyerCompanyName") = Request("buyerCompName")
            End If
        End If

        If (Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleClientID Then
            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            lnkBackToListing.HRef = "CompanyProfileBuyer.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        ElseIf Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
            lnkBackToListing.HRef = "CompanyProfile.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId")
        End If

        gvFavSuppliers.PageSize = hdnPageSize.Value
        gvFavSuppliers.Sort(ViewState!SortExpression, sd)
        PopulateGrid()
        gvFavSuppliers.PageIndex = hdnPageNo.Value

        Session("BuyersFavListingBacklink") = "&bizDivId=" & Request("bizDivId") & "&contactType=" & Session(Request("companyid") & "_ContactType") & "&companyid=" & Request("companyid") & "&userid=" & Session(Request("companyid") & "_ContactUserID") & "&classid=" & Session(Request("companyid") & "_ContactClassID") & "&statusId=" & Session(Request("companyid") & "_ContactStatusId") & "&rolegroupid=" & Session(Request("companyid") & "_ContactRoleGroupID") & "&mode=" & Session("ListingMode") & "&buyerCompName=" & Session("BuyerCompanyName")

    End Sub

    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub

    Private Sub InitializeForm()
        Dim bizDivId As Integer
        ViewState("Confirmed") = False 'Used while remove function for a Fav. Suppplier

        If Not IsNothing(Request("bizDivId")) Then
            If Request("bizDivId") <> "" Then
                bizDivId = Request("bizDivId")
            Else
                bizDivId = getDefaultBizDiv()
            End If
        Else
            bizDivId = getDefaultBizDiv()
        End If
        ViewState("bizDivId") = bizDivId

        If Trim(Request("PS")) <> "" Then
            If IsNumeric(Trim(Request("PS"))) = True Then
                If CType(Trim(Request("PS")), Integer) <> 0 Then
                    hdnPageSize.Value = CType(Trim(Request("PS")), Integer)
                End If
            End If
        End If
        hdnPageNo.Value = 0
        If Trim(Request("PN")) <> "" Then
            If IsNumeric(Trim(Request("PN"))) = True Then
                If CType(Trim(Request("PN")), Integer) <> 0 Then
                    hdnPageNo.Value = CType(Trim(Request("PN")), Integer)
                End If
            End If
        End If

        ViewState!SortExpression = ""
        If Trim(Request("SC")) <> "" Then
            ViewState!SortExpression = Trim(Request("SC"))
        Else
            ViewState!SortExpression = "CompanyName"
        End If
        Dim sd As SortDirection
        sd = SortDirection.Ascending
        If Trim(Request("SO")) <> "" Then
            If Trim(Request("SO")).ToLower = "true" Then
                sd = SortDirection.Ascending
            End If
        End If

        If Not IsNothing(Request("buyerCompName")) Then
            If Request("buyerCompName") <> "" Then
                Session("BuyerCompanyName") = Request("buyerCompName")
            End If
        End If

        gvFavSuppliers.PageSize = hdnPageSize.Value
        gvFavSuppliers.Sort(ViewState!SortExpression, sd)
        PopulateGrid()
        gvFavSuppliers.PageIndex = hdnPageNo.Value

        If (Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleClientID Then
            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            lnkBackToListing.HRef = "CompanyProfileBuyer.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        ElseIf (Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleSupplierID Then
            lnkBackToListing.HRef = "CompanyProfile.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId")
        End If

        Session("BuyersFavListingBacklink") = "&bizDivId=" & Request("bizDivId") & "&contactType=" & Session(Request("companyid") & "_ContactType") & "&companyid=" & Request("companyid") & "&userid=" & Session(Request("companyid") & "_ContactUserID") & "&classid=" & Session(Request("companyid") & "_ContactClassID") & "&statusId=" & Session(Request("companyid") & "_ContactStatusId") & "&rolegroupid=" & Session(Request("companyid") & "_ContactRoleGroupID") & "&mode=" & Session("ListingMode") & "&buyerCompName=" & Session("BuyerCompanyName")

    End Sub


    ''' <summary>
    ''' Function to populate favourite suppliers listing
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvFavSuppliers.DataBind()
            ShowHideButton()
        End If
    End Sub

    ''' <summary>
    ''' Get the Bizidvid.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getDefaultBizDiv() As Integer
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryUK
                Return ApplicationSettings.OWUKBizDivId
            Case ApplicationSettings.CountryDE
                Return ApplicationSettings.OWDEBizDivId
        End Select
    End Function

    Private Function processPage()
        If Not IsNothing(Request("companyid")) Then
            If Request("companyid") <> "" Then
                Session("ContactCompanyID") = Request("companyid")
            End If
        End If

        If Not IsNothing(Request("bizdivid")) Then
            If Request("bizdivid") <> "" Then
                Session("ContactBizDivID") = Request("bizdivid")
            End If
        End If


        If Not IsNothing(Request("rolegroupid")) Then
            If Request("rolegroupid") <> "" Then
                Session("ContactRoleGroupID") = Request("rolegroupid")
            End If
        End If

        If Not IsNothing(Request("userid")) Then
            If Request("userid") <> "" Then
                Session("ContactUserID") = Request("userid")
            End If
        End If

        If Not IsNothing(Request("classid")) Then
            If Request("classid") <> "" Then
                Session("ContactClassID") = Request("classid")
            End If
        End If

        ' Code to Show/Hide control for OW Admin
        Dim pagename As String
        pagename = CType(CType(Page.Request.Url, Object), System.Uri).AbsolutePath()
        'pagename = pagename.ToLower.Substring(pagename.LastIndexOf("/") + 1, (pagename.Length - 1) - pagename.LastIndexOf("/"))
        pagename = pagename.Substring(pagename.LastIndexOf("/") + 1, (pagename.Length - 1) - pagename.LastIndexOf("/"))
        Dim dvpriv As DataView = UCMSCompanyProfile.GetContact(Me, Session("ContactBizDivID"), True, True, Request("companyid")).Tables(15).Copy.DefaultView
        dvpriv.RowFilter = "PageName Like('" & pagename & "') "
        Security.GetPageControls(Page, dvpriv)

        'Following session variable added to fix the issue: Admin Company profile back to listing does not work
        If Not IsNothing(Request("sender")) Then
            Session("CompanyProfileQS") = "?sender=" & Request("sender") & "&bizDivId=" & Request("bizDivId") & "&contactType=" & Request("contactType") & "&companyid=" & Request("companyid") & "&userid=" & Request("userid") & "&classid=" & Request("classid") & "&statusId=" & Request("statusId") & "&rolegroupid=" & Request("rolegroupid") & "&mode=" & Request("mode") & "&ps=" & Request("ps") & "&pn=" & Request("pn") & "&sc=" & Request("sc") & "&so=" & Request("so") & "&fromDate=" & Request("fromDate") & "&toDate=" & Request("toDate")
        End If
    End Function


#Region "Grid Functions"

    ''' <summary>
    ''' Function to show hide remove buttons
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowHideButton()
        If Not IsNothing(ViewState("rowCount")) Then
            If (ViewState("rowCount") = 0) Then
                tdRemove.Visible = False
            End If
        End If
    End Sub


    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim rowCount As Integer
        Dim bizDivId As Integer = ViewState("bizDivId")
        Dim buyerCompId As String = Request("companyid")
        CompanyID = Request("companyid")
        Dim mode As String = ViewState("mode")
        ViewState("IsAM") = False
        'Dim billingLocId As Integer = 0
        'If LocType.Value = "ddl" Then
        '    billingLocId = ddlBillLoc.SelectedValue
        'ElseIf LocType.Value = "txt" Then
        '    If (txtBillingLoc.Value <> "") Then
        '        If (hdnBillingLocId.Value <> "") Then
        '            billingLocId = hdnBillingLocId.Value
        '        End If
        '    End If
        'End If
        'Dim SearchContactId As Integer = 0
        'If (txtContact.Value <> "") Then
        '    If (hdnContactID.Value <> "") Then
        '        SearchContactId = hdnContactID.Value
        '    End If
        'End If

        Dim billingLoc As String = ""
        If LocType.Value = "ddl" Then
            If (ddlBillLoc.SelectedItem.Text.ToString <> "All Locations") Then
                billingLoc = ddlBillLoc.SelectedItem.Text.ToString
            End If
        ElseIf LocType.Value = "txt" Then
            If (txtBillingLoc.Value <> "") Then
                If (txtBillingLoc.Value <> "All Locations") Then
                    billingLoc = txtBillingLoc.Value
                End If
            End If
        End If
        Dim SearchContact As String = ""
        If (txtContact.Value <> "") Then
            SearchContact = txtContact.Value
        End If

        Dim FixedCapacity As Integer = 0
        If (txtFixedCapacity.Text.Trim <> "") Then
            FixedCapacity = txtFixedCapacity.Text.Trim
        End If

        Dim AllowJobAcceptance As Integer = 0
        If (chkAllowJobAcceptance.Checked) Then
            AllowJobAcceptance = 1
        End If

        Dim ds As DataSet = ws.WSContact.MS_GetFavouriteSuppliers(bizDivId, buyerCompId, sortExpression, startRowIndex, maximumRows, rowCount, mode, SearchContact, billingLoc, txtComments.Text.Trim, FixedCapacity, drptxtFixedCapacityTimeSlot.SelectedValue, AllowJobAcceptance)
        If ds.Tables("IsAM").Rows.Count <> 0 Then
            ViewState("IsAM") = ds.Tables("IsAM").Rows(0)(0)
        End If
        ViewState("rowCount") = rowCount
        If Request("mode") <> "ClientBlacklist" Then
            If ds.Tables("tblLoction").Rows.Count > 10 Then
                ddlBillLoc.Visible = False
                txtBillingLoc.Visible = True
                LocType.Value = "txt"
            ElseIf ds.Tables("tblLoction").Rows.Count < 11 Then
                ddlBillLoc.Visible = True
                ddlBillLoc.DataSource = ds.Tables("tblLoction")
                ddlBillLoc.DataBind()
                Dim li As ListItem
                li = New ListItem
                li.Text = "All Locations"
                li.Value = 0
                ddlBillLoc.Items.Insert(0, li)
                LocType.Value = "ddl"
                txtBillingLoc.Visible = False
                'ddlBillLoc.SelectedValue = billingLocId
            End If
        Else
            ddlBillLoc.Visible = False
            txtBillingLoc.Visible = True
            If (SearchContact = "") Then
                txtBillingLoc.Disabled = True
            Else
                txtBillingLoc.Disabled = False
            End If
            LocType.Value = "txt"
        End If

        Return ds
    End Function

    ''' <summary>
    ''' ObjectDataSource Controls select Count Method which returns the row count for the listing 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Private Sub gvFavSuppliers_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvFavSuppliers.DataBound
        Try
            gvFavSuppliers.TopPagerRow.Visible = True
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvFavSuppliers.RowDataBound

        MakeGridViewHeaderClickable(gvFavSuppliers, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvFavSuppliers.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvFavSuppliers, e.Row, Me)
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
        If Not IsNothing(ViewState("rowCount")) Then
            If ViewState("rowCount") > 25 Then
                ddlPageSelector1.Enabled = True
            Else
                ddlPageSelector1.Enabled = False
            End If
        End If

        'setConfirmButtonText(gvPagerRow)
    End Sub

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvFavSuppliers.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub
    ''' <summary>
    ''' Returns the links column which has different images and links associated with it
    ''' </summary>
    ''' <param name="parambizDivId"></param>
    ''' <param name="companyId"></param>
    ''' <param name="contactId"></param>
    ''' <param name="classId"></param>
    ''' <param name="paramstatusId"></param>
    ''' <param name="roleGroupId"></param>
    ''' <param name="Status"></param>
    ''' <param name="paramactionDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLinks(ByVal parambizDivId As Integer, ByVal companyId As Integer, ByVal contactId As Integer, ByVal classId As Integer, ByVal paramstatusId As Object, ByVal roleGroupId As Integer, ByVal Status As String, ByVal Email As String, ByVal CompanyName As String, ByVal Name As String, ByVal paramActionDate As Object) As String
        Dim link As String = ""
        Dim actionDate As String = ""
        Dim bizDivId = ViewState("bizDivId")
        Dim pg As String
        Dim statusId As Integer = -1
        If Not IsDBNull(paramstatusId) Then
            If IsNumeric(paramstatusId) Then
                statusId = paramstatusId
            End If
        End If
        If Not IsDBNull(paramActionDate) Then
            actionDate = paramActionDate
        End If

        link &= "<a href='companyprofile.aspx?sender=FavouriteListing&bizDivId=" & bizDivId & "&companyid=" & companyId & "&userid=" & contactId & "&classid=" & classId & "&statusId=" & statusId & "&rolegroupid=" & roleGroupId & "&mode=edit&ps=" & gvFavSuppliers.PageSize & "&pn=" & gvFavSuppliers.PageIndex & "&sc=" & gvFavSuppliers.SortExpression & "&so=" & gvFavSuppliers.SortDirection & "'><img src='images/icons/viewdetails.gif' alt='view details' width='16' height='16' hspace='2' vspace='0' border='0'></a>"

        Return link
    End Function

#End Region

#Region "Action"
    Private Sub btnAddsupplier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddsupplier.Click
        Dim dsSuccess As DataSet
        Dim billingLocId As Integer = 0
        If LocType.Value = "ddl" Then
            billingLocId = ddlBillLoc.SelectedValue
        ElseIf LocType.Value = "txt" Then
            billingLocId = hdnBillingLocId.Value
        End If

        Dim FixedCapacity As Integer = 0
        If (txtFixedCapacity.Text.Trim <> "") Then
            FixedCapacity = txtFixedCapacity.Text.Trim
        End If

        Dim AllowJobAcceptance As Integer = 0
        If (chkAllowJobAcceptance.Checked) Then
            AllowJobAcceptance = 1
        End If

        dsSuccess = ws.WSContact.MS_AddSupplierToFavList(ViewState("bizDivId"), Request("companyid"), hdnContactID.Value, ViewState("mode"), billingLocId, txtComments.Text.Trim, CInt(Session("UserID")), FixedCapacity, drptxtFixedCapacityTimeSlot.SelectedValue, AllowJobAcceptance)
        If dsSuccess.Tables(0).Rows(0).Item("DBStatus") = -1 Then
            lblMessage.Text = "<p>Sorry, this combination of the Supplier company and billing location already exists in your " & ViewState("mode") & " list.<p/>"
        ElseIf dsSuccess.Tables(0).Rows(0).Item("DBStatus") = 1 Then
            lblMessage.Text = "<p>Supplier added to " & ViewState("mode") & "s list successfully<p/>"
            'txtContact.Value = ""
            'txtBillingLoc.Value = ""
            'txtComments.Text = ""
            'txtFixedCapacity.Text = ""
            'drptxtFixedCapacityTimeSlot.SelectedValue = ""
            'chkAllowJobAcceptance.Checked = False
            gvFavSuppliers.DataBind()
        End If
    End Sub
    ''' <summary>
    ''' Function to remove supplier form buyer's favourite list.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub RemoveSupplier(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim selectedSupplierIDs As String = ""

        selectedSupplierIDs = CommonFunctions.getSelectedIdsOfGridFavSP(gvFavSuppliers, "Check", "hdnMainContactId", "hdnBillLocId")

        Dim arrTemp As Array
        arrTemp = selectedSupplierIDs.Split(",")

        'If Last Fav. Supplier, then warn about disabling of AutoMatch.
        'BUG - Need to Check AM Status before giving the warning
        If ViewState("rowCount") = arrTemp.Length And ViewState("Confirmed") = False And ViewState("IsAM") = True Then
            WarningDisableAutoMatch()
            Exit Sub
        End If

        If selectedSupplierIDs = "" Then
            lblMessage.Text = "<p>Please select atleast one Supplier.</p>"
        Else
            lblMessage.Text = ""

            Dim dsSuccess As DataSet
            dsSuccess = ws.WSContact.MS_RemoveSupplierFromFavList(ViewState("bizDivId"), Request("companyid"), selectedSupplierIDs, Session("UserID"), ViewState("mode"))

            If dsSuccess.Tables("tblStatus").Rows.Count <> 0 Then
                If dsSuccess.Tables("tblStatus").Rows.Item(0).Item("DBStatus") = 1 Then
                    If ViewState("mode") <> "" Then
                        lblMessage.Text = "<p>Supplier(s) have been successfully removed from " & ViewState("mode") & " list.</p>"
                    Else
                        lblMessage.Text = "<p>Supplier(s) have been successfully removed from Favourite list.</p>"
                    End If

                Else
                    lblMessage.Text = "<p>" & ResourceMessageText.GetString("DBUpdateFail") & "</p>"
                End If
            Else
                lblMessage.Text = "<p>" & ResourceMessageText.GetString("DBUpdateFail") & "</p>"
            End If
            PopulateGrid()
        End If
    End Sub

    Public Sub WarningDisableAutoMatch()
        lblConfirm.Text = "<span style='color:#FF0000'><b>Warning: The Supplier list will be set to zero by this action and you have AutoMatch enabled. <br>By clicking 'Continue', the AutoMatch feature will be disabled.</b></span>"
        PnlConfirm.Visible = True
        pnlContent.Visible = False
    End Sub

    Private Sub lnkConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkConfirm.Click
        ViewState("Confirmed") = True
        PnlConfirm.Visible = False
        PnlContent.Visible = True
        RemoveSupplier(Page, e)
    End Sub

    Private Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click
        PnlConfirm.Visible = False
        PnlContent.Visible = True
    End Sub
#End Region

    <Services.WebMethod(), Script.Services.ScriptMethod()> _
   Public Shared Function GetSPForFavouriteAutoSuggest(ByVal prefixText As String, ByVal mode As String) As DataTable
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest(mode, prefixText, CompanyID)
        Return dt
    End Function
    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetBillingLocationAutoSuggest(ByVal prefixText As String, ByVal mode As String, ByVal contactId As String) As DataTable
        Dim dt As DataTable
        If (mode = "ClientBlacklist") Then
            dt = ws.WSWorkOrder.GetAutoSuggest("BillingLocation", prefixText, contactId)
        Else
            dt = ws.WSWorkOrder.GetAutoSuggest("BillingLocation", prefixText, CompanyID)
        End If

        Return dt
    End Function

   
    Private Sub gvFavSuppliers_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvFavSuppliers.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
                'top pager buttons
                Dim tdRemoveTop As HtmlTableCell = CType(pagerRowTop.FindControl("tdsetType2"), HtmlTableCell)

                If Request("mode") = "ClientBlacklist" Then
                    tdRemoveTop.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub btnSearch_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.ServerClick
        lblMessage.Text = ""
        'txtContact.Value = ""
        'txtBillingLoc.Value = ""
        gvFavSuppliers.DataBind()
    End Sub

    Private Sub btnReset_ServerClick(sender As Object, e As System.EventArgs) Handles btnReset.ServerClick
        lblMessage.Text = ""
        txtContact.Value = ""
        txtBillingLoc.Value = ""
        txtComments.Text = ""
        txtFixedCapacity.Text = ""
        drptxtFixedCapacityTimeSlot.SelectedValue = ""
        chkAllowJobAcceptance.Checked = False
        gvFavSuppliers.DataBind()
    End Sub
End Class