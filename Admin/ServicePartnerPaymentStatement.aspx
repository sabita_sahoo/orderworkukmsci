<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : " CodeBehind="ServicePartnerPaymentStatement.aspx.vb" Inherits="Admin.ServicePartnerPaymentStatement" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>    
 <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


	      
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td>&nbsp;</td>
							<td class="HeadingRed" ><strong>Supplier Statement of Payments Due</strong></td>
						</tr>

					  </table>
			      <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
			      <ContentTemplate>

					  <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top:15px; " >
					  	<tr >
							<td width="10"></td>
							<td width="250" ></td>
							<td width="20"></td>
							<td align="left"  style="padding-top:30px; " >
							    <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                    <a href="~/ExportToExcel.aspx?page=PIUnpaidReport" target="_blank" runat="server" ID="lnkExportReportExcel" class="txtButtonRed">Export to Excel</a>
								</div>
							</td>
							<td width="60" align="left" ></td>
						</tr>
					</table>
				  </ContentTemplate>					
					 </asp:UpdatePanel>
					  <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
						<ProgressTemplate >
				   <div class="gridText">
						<img  align=middle src="Images/indicator.gif" />
						<b>Fetching Data... Please Wait</b>
					</div>     
				</ProgressTemplate>
						</asp:UpdateProgress>
						<cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />					
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
					  <tr>
						<td style="padding-top:20px; ">
							 <rsweb:ReportViewer ID="ReportViewerOW" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="640px" ProcessingMode="Remote" Width="100%" bgColor="#FFFFFF">
									<ServerReport ReportServerUrl="" />
							 </rsweb:ReportViewer>
						</td>
					  </tr>						  
					  </table>
			<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      
      </asp:Content>