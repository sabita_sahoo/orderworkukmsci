'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ReferenceCheckForm

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''pnlSubmitForm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSubmitForm As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblTimeStamp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTimeStamp As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lnkBackToListingTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBackToListingTop As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''btnSaveTopNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveTopNew As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''tblApprovedSuppTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblApprovedSuppTop As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''btnApprovedSuppTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnApprovedSuppTop As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''divValidationMain control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divValidationMain As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblErrorMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblErrorMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''validationSummarySubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents validationSummarySubmit As Global.System.Web.UI.WebControls.ValidationSummary

    '''<summary>
    '''lblCompanyName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCompanyName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rgVatNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgVatNo As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtVATRegNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtVATRegNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCompRegNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCompRegNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rqNoOfEngg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rqNoOfEngg As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''RangeNoOfEngg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RangeNoOfEngg As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''RegExNoOfEngineers control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegExNoOfEngineers As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtNoOfEngineers control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNoOfEngineers As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblEmpLiability control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmpLiability As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''anchorEmployee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents anchorEmployee As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''CustValWOBeginDate1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CustValWOBeginDate1 As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''checkFutureDate1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents checkFutureDate1 As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''txtEmployeeLiabilityDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmployeeLiabilityDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''calEmpLDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calEmpLDate As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''rqCoverAmount1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rqCoverAmount1 As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''txtCoverAmountEmpLiability control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCoverAmountEmpLiability As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblPublicLiability control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPublicLiability As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''anchorPublic control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents anchorPublic As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''CustValWOBeginDate2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CustValWOBeginDate2 As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''checkFutureDate2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents checkFutureDate2 As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''txtPublicLiabilityDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPublicLiabilityDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''calPubLDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calPubLDate As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''rqCoverAmount2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rqCoverAmount2 As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''txtCoverAmountPubLiability control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCoverAmountPubLiability As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblProfIndemnity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProfIndemnity As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''anchorProfIndemnity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents anchorProfIndemnity As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''CustValWOBeginDate3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CustValWOBeginDate3 As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''checkFutureDate3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents checkFutureDate3 As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''txtProfessionalIndemnityDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProfessionalIndemnityDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''calIndemDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calIndemDate As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''rqCoverAmount3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rqCoverAmount3 As Global.System.Web.UI.WebControls.CustomValidator

    '''<summary>
    '''txtCoverAmountProfIndemnity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCoverAmountProfIndemnity As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblCompName1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCompName1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblContact1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblContact1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEmail1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmail1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPhone1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPhone1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblWorkDate1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWorkDate1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDesc1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDesc1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCompName2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCompName2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblContact2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblContact2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEmail2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmail2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPhone2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPhone2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblWorkDate2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWorkDate2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDesc2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDesc2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCompName3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCompName3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblContact3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblContact3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEmail3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmail3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPhone3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPhone3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblWorkDate3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWorkDate3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDesc3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDesc3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rdoRatingPositive1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingPositive1 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rdoRatingNeutral1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingNeutral1 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rdoRatingNegative1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingNegative1 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''txtMessage1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMessage1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rdoRatingPositive2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingPositive2 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rdoRatingNeutral2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingNeutral2 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rdoRatingNegative2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingNegative2 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''txtMessage2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMessage2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rdoRatingPositive3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingPositive3 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rdoRatingNeutral3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingNeutral3 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rdoRatingNegative3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingNegative3 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''txtMessage3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMessage3 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtGeneralComments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtGeneralComments As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''tblSendMail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblSendMail As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''chkSendMail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkSendMail As Global.System.Web.UI.HtmlControls.HtmlInputCheckBox

    '''<summary>
    '''divMailMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divMailMessage As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''txtMailMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMailMessage As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lnkBackToListing control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBackToListing As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''btnSaveTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveTop As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''tblApprovedSupp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblApprovedSupp As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''btnApprovedSupp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnApprovedSupp As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''pnlConfirm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlConfirm As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnConfirm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnConfirm As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.LinkButton
End Class
