'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class WOCancellation

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''lblHeading control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHeading As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''divValidationMain control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divValidationMain As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''validationSummarySubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents validationSummarySubmit As Global.System.Web.UI.WebControls.ValidationSummary

    '''<summary>
    '''tblWorkOrderId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblWorkOrderId As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''rqWorkOrderId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rqWorkOrderId As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtWorkOrderId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtWorkOrderId As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnView As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lblMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCancel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblWONo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWONo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblWOSubmitted control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWOSubmitted As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblWOLoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWOLoc As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblWOContact control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWOContact As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblWOTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWOTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblWOStart control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWOStart As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblWPPrice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWPPrice As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPPPrice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPPPrice As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblWOStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWOStatus As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtWPPrice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtWPPrice As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RqValWholesalePrice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RqValWholesalePrice As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''RgValPositiveNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RgValPositiveNumber As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtWPCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtWPCancel As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''trSuppTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trSuppTitle As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''trSuppDetails control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trSuppDetails As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''txtPPPrice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPPPrice As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RqValplatformPrice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RqValplatformPrice As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''RgValPositiveNumber1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RgValPositiveNumber1 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtPPCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPPCancel As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''TrRating control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TrRating As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''rdoRatingPositive control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingPositive As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rdoRatingNeutral control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingNeutral As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rdoRatingNegative control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoRatingNegative As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''RegExComments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegExComments As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''drpdwncancelreason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpdwncancelreason As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''reqdrpdwncancelreason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents reqdrpdwncancelreason As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtCompleteComments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCompleteComments As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''TdBtnCopy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TdBtnCopy As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''AnchorCopy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents AnchorCopy As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''btnCopy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCopy As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''TdBtnRematch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TdBtnRematch As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''AnchorRematch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents AnchorRematch As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''btnRematch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRematch As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''AnchorSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents AnchorSubmit As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''btnSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSubmit As Global.System.Web.UI.WebControls.Button
End Class
