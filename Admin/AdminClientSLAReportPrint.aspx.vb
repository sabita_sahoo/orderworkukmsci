Public Partial Class AdminClientSLAReportPrint
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       PopulateData()
    End Sub

    Public Sub PopulateData()
        Dim ds As DataSet = ws.WSWorkOrder.GetClientSLAReport(7677, Request("StartDate").ToString, Request("EndDate").ToString, "")
        If ds.Tables("tblClientSLAReport").DefaultView.Count > 0 Then
            If (ds.Tables("tblClientSLAReport").Rows(0).Item("SubmittedWO") <> "0") Then
                divMainReport.Visible = True
                lblSubmittedWO.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("SubmittedWO")
                lblClosedWO.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("ClosedWO")
                lblOnTime.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("OnTime")
                lblSLAPercentageMet.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("SLAPercentageMet")
                lblFirstTimeSuccess.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("FirstTimeSuccess")
                lblCancellationNo.Text = ds.Tables("tblClientSLAReport").Rows(0).Item("CancellationNumber")

                'Product
                If ds.Tables("tblProductSLAReport").DefaultView.Count > 0 Then
                    divProduct.Visible = True
                    DLProducts.DataSource() = ds.Tables("tblProductSLAReport")
                    DLProducts.DataBind()
                Else
                    divProduct.Visible = False
                End If

                'Billing Location
                If ds.Tables("tblBillingLocSLAReport").DefaultView.Count > 0 Then
                    divBillingLoc.Visible = True
                    DLBillingLoc.DataSource() = ds.Tables("tblBillingLocSLAReport")
                    DLBillingLoc.DataBind()
                Else
                    divBillingLoc.Visible = False
                End If

            Else
                divMainReport.Visible = False
            End If

        Else
            divMainReport.Visible = False
        End If
    End Sub
End Class