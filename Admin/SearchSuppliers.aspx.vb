

Partial Public Class SearchSuppliers
    Inherits System.Web.UI.Page

#Region "Declarations"

    Public Shared ws As New WSObjs

    Protected WithEvents ObjectDataSource1 As ObjectDataSource

    'Grid
    Protected WithEvents gvContacts As GridView

    'Label
    Protected WithEvents lblBuyerCompany As Label
    Protected WithEvents lblSearchCriteria As Label
    Protected WithEvents lblMessage As Label

    'TextBox
    Protected WithEvents txtKeyword As TextBox
    Protected WithEvents txtPostCode As TextBox
    Protected WithEvents txtFName As TextBox
    Protected WithEvents txtLName As TextBox
    Protected WithEvents txtCompanyName As TextBox
    Protected WithEvents txtEmail As TextBox

    'CheckBox
    Protected WithEvents chkRefChecked As CheckBox

    'Panel
    Protected WithEvents pnlSearchResult As Panel

    'Hidden Variables
    Protected WithEvents hdnRegionValue As HtmlInputHidden
    Protected WithEvents hdnRegionName As HtmlInputHidden
    Protected WithEvents hdnProductIds As HtmlInputHidden
    Protected WithEvents lnkBackToListing As HtmlAnchor

    'UC
    Protected WithEvents UCRegions As UCDualListBox
    Protected WithEvents UCAOE1 As UCAOE

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)



            If Not IsNothing(Request("mode")) Then
                If Request("mode") = "blacklist" Then
                    ViewState("mode") = Request("mode")
                    'CType(gvContacts.FindControl("lblMark"), Label).Text = "Mark as blacklist"
                Else
                    ViewState("mode") = "favourite"
                    'CType(gvContacts.FindControl("lblMark"), Label).Text = "Mark as Favourite"
                End If
            End If

            If Not IsNothing(Request("sender")) Then
                If Request("sender") = "CompanyProfile" Then
                    processBackToListing()
                Else
                    InitializeForm()
                End If
            Else
                InitializeForm()
            End If

            lblBuyerCompany.Text = "Search Suppliers For " & Session("BuyerCompanyName")

            'poonam modified on 6/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            If Session("ContactClassID") = ApplicationSettings.RoleClientID Then
                lnkBackToListing.HRef = "CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            ElseIf Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
                lnkBackToListing.HRef = "CompanyProfile.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
            End If
        End If
    End Sub

    ''' <summary>
    ''' Search Function which handles forms submission event
    ''' and preapres the search result
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub Search_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' display search criteria
        lblSearchCriteria.Text = PrepareSearchCriteria()
        lblMessage.Text = ""

        If chkRefChecked.Checked Then
            ViewState("statusId") = ApplicationSettings.StatusApprovedCompany
        Else
            ViewState("statusId") = ""
        End If

        pnlSearchResult.Visible = True
        PopulateGrid()
        gvContacts.PageIndex = 0
    End Sub

    Private Function PrepareSearchCriteria() As String
        Dim searchCriteria As String = ""
        searchCriteria &= "<strong>Search Accounts Criteria:</strong>"
        hdnRegionName.Value = getSelectedRegions("Text")
        hdnRegionValue.Value = getSelectedRegions("Value")
        'region name
        If hdnRegionName.Value.EndsWith(",") Then
            hdnRegionName.Value = hdnRegionName.Value.Substring(0, hdnRegionName.Value.Length - 1)
        End If
        If hdnRegionValue.Value.EndsWith(",") Then
            hdnRegionValue.Value = hdnRegionValue.Value.Substring(0, hdnRegionValue.Value.Length - 1)
        End If
        If hdnRegionName.Value <> "" Then
            searchCriteria &= " Region - <span class='txtOrange'>" & hdnRegionName.Value & "</span>"
        End If

        'keyword
        If txtKeyword.Text <> "" Then
            searchCriteria &= ", Keyword - <span class='txtOrange'>" & txtKeyword.Text & "</span>"
        End If

        'postcode
        If txtPostCode.Text <> "" Then
            searchCriteria &= ", PostCode - <span class='txtOrange'>" & txtPostCode.Text & "</span>"
        End If

        'first name
        If txtFName.Text <> "" Then
            searchCriteria &= ", First Name - <span class='txtOrange'>" & txtFName.Text & "</span>"
        End If

        'last name
        If txtLName.Text <> "" Then
            searchCriteria &= ", Last Name - <span class='txtOrange'>" & txtLName.Text & "</span>"
        End If

        'company name
        If txtCompanyName.Text <> "" Then
            searchCriteria &= ", Company Name - <span class='txtOrange'>" & txtCompanyName.Text & "</span>"
        End If

        'email
        If txtEmail.Text <> "" Then
            searchCriteria &= ", Email - <span class='txtOrange'>" & txtEmail.Text & "</span>"
        End If
        'ref checked
        If chkRefChecked.Checked = True Then
            searchCriteria &= " Reference Checked - <span class='txtOrange'>Yes</span>"
        Else
            searchCriteria &= " Reference Checked - <span class='txtOrange'>No</span>"
        End If
        'reset search criteria
        hdnRegionName.Value = ""
        Return searchCriteria
    End Function

#Region "Grid Functions"

    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' Function to populate the suppliers list
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvContacts.DataBind()
        End If
    End Sub

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim rowCount As Integer
        Dim bizDivId As Integer = ViewState("BizDivId")
        Dim statusId As String = ViewState("statusId")
        Dim skillSet As String = UCAOE1.GetSelectedCombIds("getCommaSeperatedCombIds", Nothing)
        Dim region As String = getSelectedRegions("Value")
        Dim postCode As String = txtPostCode.Text
        Dim keyword As String = txtKeyword.Text
        Dim fName As String = txtFName.Text
        Dim lName As String = txtLName.Text
        Dim companyName As String = txtCompanyName.Text
        Dim eMail As String = txtEmail.Text

        Dim ds As DataSet = ws.WSContact.MS_GetSuppliers(Request("companyid"), bizDivId, skillSet, region, postCode, keyword, statusId, fName, lName, companyName, eMail, sortExpression, startRowIndex, maximumRows, rowCount, ViewState("mode"))
        ViewState("rowCount") = rowCount
        Return ds
    End Function

    ''' <summary>
    ''' ObjectDataSource Controls select Count Method which returns the row count for the listing 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvContacts.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

    Private Sub gvContacts_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvContacts.PreRender
        Me.gvContacts.Controls(0).Controls(Me.gvContacts.Controls(0).Controls.Count - 1).Visible = True
        If ViewState("rowCount") < 1 Then
            Return
        End If
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
                'top pager buttons
                Dim tdMarkTop As HtmlTableCell = CType(pagerRowTop.FindControl("tdMark"), HtmlTableCell)
                Dim tdExportTop As HtmlTableCell = CType(pagerRowTop.FindControl("tdExport"), HtmlTableCell)
                If Not IsNothing(HttpContext.Current.Items("rowCount")) Then
                    If (HttpContext.Current.Items("rowCount") = 0) Then
                        tdMarkTop.Visible = False
                        tdExportTop.Visible = False
                    End If
                End If
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

                'bottom pager buttons
                Dim tdMarkBottom As HtmlTableCell = CType(pagerRowBottom.FindControl("tdMark"), HtmlTableCell)
                Dim tdExportBottom As HtmlTableCell = CType(pagerRowBottom.FindControl("tdExport"), HtmlTableCell)

                tdMarkBottom.Visible = False
                tdExportBottom.Visible = False
            End If
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowDataBound
        MakeGridViewHeaderClickable(gvContacts, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i


        End If
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvContacts, e.Row, Me)
        End If



    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)
        Dim btnExport As HtmlAnchor = CType(gvPagerRow.FindControl("btnExport"), HtmlAnchor)

        btnExport.HRef = prepareExportToExcelLink()

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then

            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex


        If ViewState("mode") <> "" Then
            If ViewState("mode") = "blacklist" Then
                CType(gvPagerRow.FindControl("ConfirmBtnMark"), AjaxControlToolkit.ConfirmButtonExtender).ConfirmText = "Do you want to add the Supplier to buyer's  blacklist?"
                CType(gvPagerRow.FindControl("btnMark"), LinkButton).Text = "<b>Mark as Blacklist</b>"
            Else
                CType(gvPagerRow.FindControl("ConfirmBtnMark"), AjaxControlToolkit.ConfirmButtonExtender).ConfirmText = "Do you want to add the Supplier to buyer's favourite list?"
                CType(gvPagerRow.FindControl("btnMark"), LinkButton).Text = "<b>Mark as Favourite</b>"
            End If
        Else
            CType(gvPagerRow.FindControl("ConfirmBtnMark"), AjaxControlToolkit.ConfirmButtonExtender).ConfirmText = "Do you want to add the Supplier to buyer's favourite list?"
            CType(gvPagerRow.FindControl("btnMark"), LinkButton).Text = "<b>Mark as Favourite</b>"
        End If

    End Sub

    ''' <summary>
    ''' Returns the links column which has different images and links associated with it
    ''' </summary>
    ''' <param name="paramBizDivId"></param>
    ''' <param name="companyId"></param>
    ''' <param name="contactId"></param>
    ''' <param name="classId"></param>
    ''' <param name="paramstatusId"></param>
    ''' <param name="roleGroupId"></param>
    ''' <param name="Status"></param>
    ''' <param name="paramactionDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLinks(ByVal bizDivId As Integer, ByVal companyId As Integer, ByVal contactId As Integer, ByVal classId As Integer, ByVal statusId As Object, ByVal roleGroupId As Integer, ByVal CompanyName As String, ByVal contactname As String, ByVal refChkDate As Object, ByVal Email As String) As String

        Dim link As String = ""
        Dim companyPageName As String = ""
        Dim invoicePageName As String = ""
        Dim linkParams As String = ""

        Dim linkStatusId As String = ""
        If chkRefChecked.Checked Then
            linkStatusId = ApplicationSettings.StatusApprovedCompany
        Else
            linkStatusId = ""
        End If

        '   this code decides which page is used to show company profile depending upon classid
        'poonam modified on 6/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
        If classId = ApplicationSettings.RoleSupplierID Then
            companyPageName = "CompanyProfile.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        ElseIf classId = ApplicationSettings.RoleClientID Then
            companyPageName = "CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        End If
        


        linkParams &= "bizDivId=" & bizDivId
        linkParams &= "&companyId=" & companyId
        linkParams &= "&contactId=" & contactId
        linkParams &= "&classId=" & classId
        linkParams &= "&statusId=" & linkStatusId
        linkParams &= "&roleGroupId=" & roleGroupId
        linkParams &= "&product=" & hdnProductIds.Value
        linkParams &= "&region=" & hdnRegionValue.Value
        linkParams &= "&txtPostCode=" & txtPostCode.Text
        linkParams &= "&txtKeyword=" & txtKeyword.Text
        linkParams &= "&refcheck=" & chkRefChecked.Checked
        linkParams &= "&txtFName=" & txtFName.Text
        linkParams &= "&txtLName=" & txtLName.Text
        linkParams &= "&txtCompanyName=" & txtCompanyName.Text
        linkParams &= "&txtEmail=" & txtEmail.Text

        linkParams &= "&Email=" & Email
        linkParams &= "&Name=" & contactname

        ' TODO
        'linkParams &= "&mode=" & ""
        linkParams &= "&sender=" & "AdminSearchSuppliers"
        linkParams &= "&PS=" & gvContacts.PageSize
        linkParams &= "&PN=" & gvContacts.PageIndex
        linkParams &= "&SC=" & gvContacts.SortExpression
        linkParams &= "&SO=" & gvContacts.SortDirection
        'Keep contact name at last
        linkParams &= "&companyname=" & Trim(CompanyName)
        linkParams &= "&contactname=" & Trim(contactname)
        link &= "<a class=footerTxtSelected href='" & companyPageName & "?" & linkParams & "'><img src='Images/Icons/ViewDetails.gif' alt='View Details' width='16' height='16' hspace='2' vspace='0' border='0'></a>"

        Return link
    End Function

    Public Function getSelectedRegions(ByVal columnName As String) As String
        Dim strRegions = ""

        Dim dt As DataTable = UCRegions.saveListBox()
        For Each dr As DataRow In dt.Rows
            strRegions &= dr(columnName) & ","
        Next
        Return strRegions
    End Function

    Public Function SetRefCheckedStatus(ByVal refChkDate As Object) As String
        If Not IsDBNull(refChkDate) Then
            If CStr(refChkDate) <> "" Then
                Return CDate(refChkDate).ToShortDateString
            End If
        End If
        Return "No"
    End Function

#End Region

#Region "Actions"

    ''' <summary>
    ''' Export to Excel link initiated
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function prepareExportToExcelLink() As String
        Dim linkParams As String = ""
        linkParams &= "bizDivId=" & ViewState("BizDivId")
        linkParams &= "&buyerCompanyId=" & Request("companyid")

        Dim statusId As String = ""
        If chkRefChecked.Checked Then
            statusId = ApplicationSettings.StatusApprovedCompany
        Else
            statusId = ""
        End If
        linkParams &= "&statusId=" & statusId

        linkParams &= "&skillSet=" & UCAOE1.GetSelectedCombIds("getCommaSeperatedCombIds", Nothing)
        linkParams &= "&region=" & getSelectedRegions("Value")
        linkParams &= "&postCode=" & txtPostCode.Text
        linkParams &= "&keyword=" & txtKeyword.Text
        linkParams &= "&fName=" & txtFName.Text
        linkParams &= "&lName=" & txtLName.Text
        linkParams &= "&companyName=" & txtCompanyName.Text
        linkParams &= "&eMail=" & txtEmail.Text
        linkParams &= "&sortExpression=" & gvContacts.SortExpression
        linkParams &= "&page=" & "AdminSearchSuppliers"
        linkParams &= "&mode=" & ViewState("mode")
        Return "ExportToExcel.aspx?" & linkParams
    End Function

    ''' <summary>
    ''' Function to add supplier to buyer's favourite list.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub MarkAsFavourite(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim selectedSupplierIDs As String = ""

        selectedSupplierIDs = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnMainContactId")

        If selectedSupplierIDs = "" Then
            lblMessage.Text = "<p>Please select atleast one supplier.</p>"
        Else
            lblMessage.Text = ""

            Dim dsSuccess As DataSet = Nothing
            dsSuccess = ws.WSContact.MS_AddSupplierToFavList(ViewState("bizDivId"), Request("companyid"), selectedSupplierIDs, ViewState("mode"), 0, "", CInt(Session("UserID")), 0, "", 0)

            If dsSuccess.Tables("tblStatus").Rows.Count <> 0 Then
                pnlSearchResult.Visible = False
                If dsSuccess.Tables("tblStatus").Rows.Item(0).Item("DBStatus") = 1 Then
                    If ViewState("mode") <> "" Then
                        If ViewState("mode") = "blacklist" Then
                            lblMessage.Text = "<p>Supplier(s) have been successfully added to Buyer's Blacklist.</p>"
                        Else
                            lblMessage.Text = "<p>Supplier(s) have been successfully added to Buyer's Favourite list.</p>"
                        End If
                    Else
                        lblMessage.Text = "<p>Supplier(s) have been successfully added to Buyer's Favourite list.</p>"
                    End If
                Else
                    lblMessage.Text = ResourceMessageText.GetString("DBUpdateFail")
                End If
            Else
                lblMessage.Text = ResourceMessageText.GetString("DBUpdateFail")
            End If
            PopulateGrid()
        End If
    End Sub

#End Region

    Public Sub processBackToListing()

        If Not IsNothing(Request("companyid")) Then
            If Request("companyid") <> "" Then
                Session("ContactCompanyID") = Request("companyid")
            End If
        End If

        If Not IsNothing(Request("companyid")) Then
            If Request("companyid") <> "" Then
                Session("ContactCompanyID") = Request("companyid")
            End If
        End If

        If Not IsNothing(Request("bizdivid")) Then
            If Request("bizdivid") <> "" Then
                Session("ContactBizDivID") = Request("bizdivid")
            End If
        End If


        If Not IsNothing(Request("rolegroupid")) Then
            If Request("rolegroupid") <> "" Then
                Session("ContactRoleGroupID") = Request("rolegroupid")
            End If
        End If

        If Not IsNothing(Request("userid")) Then
            If Request("userid") <> "" Then
                Session("ContactUserID") = Request("userid")
            End If
        End If

        If Not IsNothing(Request("classid")) Then
            If Request("classid") <> "" Then
                Session("ContactClassID") = Request("classid")
            End If
        End If

        If Not IsNothing(Request("contacttype")) Then
            If Request("contacttype") <> "" Then
                Session("ContactType") = Request("contacttype")
            End If
        End If

        If Not IsNothing(Request("statusId")) Then
            If Request("statusId") <> "" Then
                Session("ContactStatusId") = Request("statusId")
            End If
        End If

        If Not IsNothing(Request("mode")) Then
            If Request("mode") <> "" Then
                Session("ListingMode") = Request("mode")
            End If
        End If

        ViewState("BizDivId") = Request("bizDivId")
        UCAOE1.BizDivId = Request("bizDivId")
        HttpContext.Current.Items("companyId") = Request("companyId")
        HttpContext.Current.Items("contactId") = Request("contactId")
        HttpContext.Current.Items("classId") = Request("classId")
        HttpContext.Current.Items("statusId") = Request("statusId")
        HttpContext.Current.Items("roleGroupId") = Request("roleGroupId")
        hdnProductIds.Value = Request("product")
        hdnRegionValue.Value = Request("region")
        txtPostCode.Text = Request("txtPostCode")
        txtKeyword.Text = Request("txtKeyword")
        chkRefChecked.Checked = Request("refcheck")
        txtFName.Text = Request("txtFName")
        txtLName.Text = Request("txtLName")
        txtCompanyName.Text = Request("txtCompanyName")
        txtEmail.Text = Request("txtEmail")
        gvContacts.PageSize = Request("PS")


        If chkRefChecked.Checked Then
            ViewState("statusId") = ApplicationSettings.StatusApprovedCompany
        Else
            ViewState("statusId") = ""
        End If

        'Sort Expe
        ViewState!SortExpression = ""
        If Trim(Request("SC")) <> "" Then
            ViewState!SortExpression = Trim(Request("SC"))
        Else
            ViewState!SortExpression = "DateCreated"
        End If

        'Sort direction
        Dim sd As SortDirection
        sd = SortDirection.Descending
        If Trim(Request("SO")) <> "" Then
            If Trim(Request("SO")).ToLower = "0" Then
                sd = SortDirection.Ascending
            End If
        End If

        InitializeForm(False)
        lblSearchCriteria.Text = PrepareSearchCriteria()
        pnlSearchResult.Visible = True
        gvContacts.Sort(ViewState!SortExpression, sd)
        PopulateGrid()
        gvContacts.PageIndex = Request("PN")
    End Sub

    ''' <summary>
    ''' Initialize all the fields on the form
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub InitializeForm(Optional ByVal resetFields As Boolean = True)

        gvContacts.PageSize = ApplicationSettings.GridViewPageSize

        'Initialize the Bizdivid with OrderWrok ID
        If IsNothing(ViewState("BizDivId")) Then
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryUK
                    ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
                    UCAOE1.BizDivId = ApplicationSettings.OWUKBizDivId
                Case ApplicationSettings.CountryDE
                    ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
                    UCAOE1.BizDivId = ApplicationSettings.OWDEBizDivId
            End Select
        End If

        ' Populate regions
        UCRegions.populateListBox(Nothing, CommonFunctions.GetRegions(Page), "RegionName", "SettingValue", "RegionID")

        If resetFields Then
            txtKeyword.Text = ""
            txtPostCode.Text = ""
            txtFName.Text = ""
            txtLName.Text = ""
            txtCompanyName.Text = ""
            txtEmail.Text = ""
            lblSearchCriteria.Text = ""
            pnlSearchResult.Visible = False
            ViewState!SortExpression = "CompanyName"
            gvContacts.Sort(ViewState!SortExpression, SortDirection.Ascending)
        Else
            ' Repopulate region list to show selected regions
            UCRegions.showSelected(hdnRegionValue.Value)
        End If

        If Not IsNothing(Request("buyerCompName")) Then
            If Request("buyerCompName") <> "" Then
                Session("BuyerCompanyName") = Request("buyerCompName")
            End If
        End If


    End Sub

End Class