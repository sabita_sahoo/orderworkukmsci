﻿Imports System.Web.Mail
Imports System.IO
Imports System.Net

Public Class ResendEmail
    Inherits System.Web.UI.Page

    Shared ws As New WSObjs
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        UserAction.userAutoLoginAccept(Page)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
    End Sub

    Public Sub btnResendEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If txtEmailtoSend.Text.Trim() = "" Then
            lblMsg.Text = "Please enter an email address."
            Exit Sub
        End If
        If (txtEmailtoSend.Text.Trim() <> ApplicationSettings.OWCommonUserAccountEmail) Then
            Dim ds As DataSet = ws.WSSecurity.ResendEmailInfo(txtEmailtoSend.Text.Trim())
            Dim Name As String
            Dim RegistrationStatus As Integer
            Dim SecurityQues As Integer
            Dim SecurityAns As String
            If ds.Tables(0).Rows.Count <> 0 Then
                Name = Convert.ToString(ds.Tables(0).Rows(0)("FullName"))
                RegistrationStatus = ds.Tables(0).Rows(0)("RegistrationStatus")
                SecurityQues = ds.Tables(0).Rows(0)("SecurityQues")
                SecurityAns = Convert.ToString(ds.Tables(0).Rows(0)("SecurityAns"))


                Dim result As String
                'Dim URL = ApplicationSettings.OrderWorkMyURLWithOutSSL & "OWRearchAPI/api/account/callbackurl?emailId=" + txtEmailtoSend.Text.Trim()
                Dim URL = ApplicationSettings.APIPath + "/api/account/callbackurl?emailId=" + txtEmailtoSend.Text.Trim()
                'Create a Request
                Dim request As System.Net.HttpWebRequest = TryCast(System.Net.WebRequest.Create(URL), System.Net.HttpWebRequest)
                ' Get response  
                ServicePointManager.SecurityProtocol = DirectCast(3072, Net.SecurityProtocolType)


                Using response As System.Net.HttpWebResponse = TryCast(request.GetResponse(), System.Net.HttpWebResponse)
                    ' Get the response stream  
                    Dim reader As New StreamReader(response.GetResponseStream())
                    ' Read the whole contents and return as a string  
                    result = reader.ReadToEnd()
                End Using

                Dim message As String = ""

                'If (RegistrationStatus >= 1 And RegistrationStatus < 3 And SecurityQues = 0 And SecurityAns = "") Then
                '    message = "This email address has Not completed Step 1 Or Step 2. An email has been sent successfully To this account!"
                '    Emails.ReSendEmail(txtEmailtoSend.Text.Trim(), Name, result.Replace("""", ""))
                'ElseIf (RegistrationStatus >= 3 And SecurityQues = 0 And SecurityAns = "") Then
                '    message = "This email address has already completed Step 1 Or Step 2 Registration Process but has Not updated Security Question And Security Answer. An email has been sent successfully To this account!"
                '    Emails.ReSendEmail(txtEmailtoSend.Text.Trim(), Name, result.Replace("""", ""))
                'ElseIf (RegistrationStatus >= 3 And SecurityQues <> 0 And SecurityAns <> "") Then
                '    message = "This email address has already completed Step 1 Or Step 2 Registration Process along With Security Question And Answer"
                'End If
                If (RegistrationStatus = 1) Then
                    message = "This email address has already completed Step 1"
                    Emails.ReSendEmail(txtEmailtoSend.Text.Trim(), Name, result.Replace("""", ""))
                Else
                    message = "This email address has already completed Step 2"
                End If


                Dim sb As New System.Text.StringBuilder()
                sb.Append("<script type = 'text/javascript'>")
                sb.Append("window.onload=function(){")
                sb.Append("alert('")
                sb.Append(message)
                sb.Append("')};")
                sb.Append("</script>")
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
                lblMsg.Text = ""

                'If (RegistrationStatus > 1 And RegistrationStatus < 3) Then
                '    Emails.ReSendEmail(txtEmailtoSend.Text.Trim(), Name, result.Replace("""", ""))
                '    Dim message As String = "You have not completed Step 1 or Step 2. An email sent successfully to your account. Please follow the instructions for the completion of your Registration!!!!!"
                '    Dim sb As New System.Text.StringBuilder()
                '    sb.Append("<script type = 'text/javascript'>")
                '    sb.Append("window.onload=function(){")
                '    sb.Append("alert('")
                '    sb.Append(message)
                '    sb.Append("')};")
                '    sb.Append("</script>")
                '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
                '    lblMsg.Text = ""
                'ElseIf (RegistrationStatus >= 3) Then
                '    Emails.ReSendEmail(txtEmailtoSend.Text.Trim(), Name, result.Replace("""", ""))
                '    Dim message As String = "You have completed all your Registration Process. An email sent successfully to your account!!!!!"
                '    Dim sb As New System.Text.StringBuilder()
                '    sb.Append("<script type = 'text/javascript'>")
                '    sb.Append("window.onload=function(){")
                '    sb.Append("alert('")
                '    sb.Append(message)
                '    sb.Append("')};")
                '    sb.Append("</script>")
                '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
                '    lblMsg.Text = ""
                'End If

            Else
                lblMsg.Text = "This email address does not exist in our records. Please try again"
            End If

        End If

        txtEmailtoSend.Text = ""

    End Sub

End Class