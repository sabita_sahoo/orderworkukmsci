
Partial Public Class SplitWO
    Inherits System.Web.UI.Page

#Region "Declaration"
    Public Shared ws As New WSObjs
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack() Then
            populateValues()
        End If
    End Sub

    ''' <summary>
    ''' Function to populate the workorder summary
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateValues()
        If Not IsPostBack Then
            Dim ds As DataSet
            Dim woid As Integer = 0
            Dim BizDivId As Integer = 0
            BizDivId = ApplicationSettings.OWUKBizDivId
            ViewState("BizDivID") = BizDivId
            If Not IsNothing(Request("WOID")) Then
                If Request("WOID").Trim <> "" Then
                    woid = Request("WOID").Trim
                End If
            End If
            ds = ws.WSWorkOrder.woGetDataToSplitWO(woid, BizDivId)
            hdnPricingMethod.Value = ds.Tables(0).Rows(0).Item("PricingMethod")
            ViewState("PricingMethod") = ds.Tables(0).Rows(0).Item("PricingMethod")
            If ds.Tables(0).Rows(0).Item("PricingMethod") = "Fixed" Then
                HideFields()
                'Calculate Wholesale Stage Price and Platform Stage Price (the price is divided by 2 coz this is the default number of stages)
                txtWholesaleStagePrice.Text = ds.Tables("PricingDetails").Rows(0).Item("WholesalePrice") / 2
                txtPlatformStagePrice.Text = ds.Tables("PricingDetails").Rows(0).Item("PlatformPrice") / 2
            Else
                ShowFields()
            End If

            'Wholesale Price
            If Not IsDBNull(ds.Tables("PricingDetails").Rows(0).Item("WholesalePrice")) Then
                txtWholesalePrice.Text = ds.Tables("PricingDetails").Rows(0).Item("WholesalePrice")
            End If
            'WO Wholesale Day Rate Price
            If Not IsDBNull(ds.Tables("PricingDetails").Rows(0).Item("WholesaleDayJobRate")) Then
                txtWholesaleDayRatePrice.Text = ds.Tables("PricingDetails").Rows(0).Item("WholesaleDayJobRate")
            End If
            'Platform Price
            If Not IsDBNull(ds.Tables("PricingDetails").Rows(0).Item("PlatformPrice")) Then
                txtPlatformPrice.Text = ds.Tables("PricingDetails").Rows(0).Item("PlatformPrice")
            End If
            'WO Platform Day Rate Price
            If Not IsDBNull(ds.Tables("PricingDetails").Rows(0).Item("PlatformDayJobRate")) Then
                txtPlatformDayRatePrice.Text = ds.Tables("PricingDetails").Rows(0).Item("PlatformDayJobRate")
            End If
            'Buyer Company ID
            If Not IsDBNull(ds.Tables("PricingDetails").Rows(0).Item("CompanyId")) Then
                ViewState("BuyerCompanyId") = ds.Tables("PricingDetails").Rows(0).Item("CompanyId")
            End If
            'Buyer Contact ID
            If Not IsDBNull(ds.Tables("PricingDetails").Rows(0).Item("ContactId")) Then
                ViewState("BuyerContactId") = ds.Tables("PricingDetails").Rows(0).Item("ContactId")
            End If
        End If
    End Sub
    Public Sub HideFields()
        txtEstimatedTimeInDays.Visible = False
        tdEstimatedStageTimeReq.Visible = False
        tdEstimatedStageTimeReqTxtBox.Visible = False
        tdWOWholesaleDayRatePrice.Visible = False
        tdWOWholesaleDayRatePriceTxtBox.Visible = False
        tdWOPlatformDayRatePrice.Visible = False
        tdWOPlatformDayRatePriceTxtBox.Visible = False
        txtWholesaleStagePrice.Enabled = False
        txtPlatformStagePrice.Enabled = False
    End Sub
    Public Sub ShowFields()
        txtEstimatedTimeInDays.Visible = True
        tdEstimatedStageTimeReq.Visible = True
        tdEstimatedStageTimeReqTxtBox.Visible = True
        tdWOWholesaleDayRatePrice.Visible = True
        tdWOWholesaleDayRatePriceTxtBox.Visible = True
        tdWOPlatformDayRatePrice.Visible = True
        tdWOPlatformDayRatePriceTxtBox.Visible = True
    End Sub

    ''' <summary>
    ''' Go back to listing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click, lnkBack.Click, lnkCancel2.Click
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            If Request("sender") = "UCWOsListing" Then
                link = "AdminWOListing.aspx?"
            Else
                link = "AdminWODetails.aspx?"
            End If
            link &= "WOID=" & Request("WOID")
            link &= "&WorkOrderID=" & Request("WorkOrderID")
            link &= "&ContactId=" & Request("ContactId")
            link &= "&CompanyId=" & Request("CompanyId")
            link &= "&SupContactId=" & Request("SupContactId")
            link &= "&SupCompId=" & Request("SupCompId")
            link &= "&Group=" & Request("Group")
            link &= "&mode=" & Request("Group")
            link &= "&BizDivID=" & Request("BizDivID")
            link &= "&FromDate=" & Request("FromDate")
            link &= "&ToDate=" & Request("ToDate")
            link &= "&PS=" & Request("PS")
            link &= "&PN=" & Request("PN")
            link &= "&SC=" & Request("SC")
            link &= "&SO=" & Request("SO")
            link &= "&CompID=" & Request("CompID")
            link &= "&sender=" & Request("sender")
            Response.Redirect(link)
        Else
            Response.Redirect("AdminWOListing.aspx?mode=Submitted")
        End If
    End Sub

    Private Sub lnkSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSubmit.Click
        Page.Validate()
        If Page.IsValid = True Then
            divValidationMain.Visible = False
            pnlSetPrice.Visible = False
            pnlWarning.Visible = True
        Else
            divValidationMain.Visible = True
            divValidationMain.Focus()
            validationSummarySubmit.Focus()
        End If
    End Sub

    Private Sub lnkNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        pnlSetPrice.Visible = False
        pnlWarning.Visible = False
        pnlSuccess.Visible = True
        'Code to insert the work orders in the workorder and tracking tables.

        ViewState("NoOfStages") = txtNoOfStages.Text
        ViewState("DeferalDate") = txtDateDeferal.Text
        If ViewState("PricingMethod") = "Fixed" Then
            ViewState("EstimatedTimeInDays") = 1
            ViewState("WholesaleJobDayRate") = 0.0
            ViewState("PlatformPriceJobDayRate") = 0.0
        Else
            ViewState("EstimatedTimeInDays") = CInt(txtEstimatedTimeInDays.Text)
            ViewState("WholesaleJobDayRate") = CDec(txtWholesaleDayRatePrice.Text)
            ViewState("PlatformPriceJobDayRate") = CDec(txtPlatformDayRatePrice.Text)
        End If
        If txtWholesaleStagePrice.Text <> "" Then
            ViewState("WholesalePrice") = CDec(txtWholesaleStagePrice.Text)
        Else
            ViewState("WholesalePrice") = CDec(0.0)
        End If
        If txtPlatformStagePrice.Text <> "" Then
            ViewState("PlatformPrice") = CDec(txtPlatformStagePrice.Text)
        Else
            ViewState("PlatformPrice") = CDec(0.0)
        End If

        ViewState("WorkOrderID") = Request("WorkOrderID")
        ws.WSWorkOrder.CreatStagedWorkOrders(Session("UserId"), Session("CompanyId"), ViewState("BizDivID"), ViewState("NoOfStages"), ViewState("EstimatedTimeInDays"), ViewState("WholesalePrice"), ViewState("WholesaleJobDayRate"), ViewState("PlatformPrice"), ViewState("PlatformPriceJobDayRate"), ViewState("WorkOrderID"), ViewState("BuyerCompanyId"), ViewState("BuyerContactId"), ViewState("DeferalDate"))
    End Sub
End Class