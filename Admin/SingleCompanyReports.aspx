 <%@ Page Language="vb" AutoEventWireup="false" Codebehind="SingleCompanyReports.aspx.vb" Inherits="Admin.SingleCompanyReports" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Single Company Report"%>
    
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
   





	        
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
						<td>&nbsp;</td>
							<td class="HeadingRed" ><strong>Single Company Report</strong></td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						    <td class="formTxt" valign="top" height="18"><asp:Label ID="ErrLabel" runat="server" Visible="false" CssClass="HeadingRed"></asp:Label></td>
						</tr>
						<tr>
						   <td align="left" colspan="2" >
									 <table class="gridBorderB" width="100%"  border="0" cellpadding="10" cellspacing="0" >
									  <tr>
										 <td id="tdBackToListing" runat="server">
											<div id="divButton" class="divButton" style="width: 115px;">
												<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
													<a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" alt="Back to Listing" width="4" height="8" hspace="0" vspace="0" border="0" class="marginR8"><strong>Back to Listing</strong></a>
												<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
											</div>
										</td>
									 </tr>
									</table>
							</td>
						</tr>
					  </table>

					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
					  	<tr valign="top">
							<td width="20"></td>
							<td width="220" valign="top" >
					    		<table width="100%" cellpadding="0" cellspacing="0" border="0">								   
								   <tr valign="top">
										 <td class="formTxt" valign="top" height="20">Select Company</td>
									   </tr>

									   <tr >
										 <td >										        
												<%@ register src="~/UserControls/Admin/UCSearchContact.ascx" tagname="UCSearchContact" tagprefix="uc1" %>
												<uc1:UCSearchContact id="UCSearchContact1" runat="server"></uc1:UCSearchContact> 
										 </td>
									   </tr>
								</table>
							</td>
							<td width="5"></td>
							 <td align="left" width="370" valign="top">
								<TABLE cellSpacing="0" cellPadding="0" width="350" border="0" >
									<TR valign="top">
										<td width="350" style="padding-top:5px; ">
											<%@ register src="~/UserControls/UK/UCDateRangeUK.ascx" tagname="UCDateRange" tagprefix="uc1" %>
											<uc1:UCDateRange id="UCDateRange1" runat="server"></uc1:UCDateRange> 
									 </td>
									</TR>
								</TABLE>
							 </td>							 
							 <td style="padding-top:23px;"><asp:CheckBox ID="chkToDate" runat="server" CausesValidation="false" text="To Date" TextAlign="Right" class="formTxt" valign="top" height="24"/></td>
							<td width="20"></td>							
							<td align="left" style="padding-top:23px;">
								<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                    <asp:LinkButton ID="lnkBtnGenerateReport" causesValidation=True runat="server" CssClass="txtButtonRed" Text="Generate Report"></asp:LinkButton>
                                </div>
							 </td>
							<td align="left" id="TdPrint" runat="server" visible="false"  style="padding-top:23px;">
                                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                                    <a runat="server" target="_blank" id="btnPrint" class="txtButtonRed" style="cursor:pointer;" > &nbsp;Print&nbsp;</a>
                                </div>
                            </td>	
                             <%--'Poonam added on 3/6/2015 : Task - 4471404 : OWEM-ENH-ADMIN - Add export to excel function to /SingleCompanyReports.aspx--%>
                               <td align="left" style="padding-top:23px;">
                                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                                    <asp:LinkButton ID="lnkBtnExportToExcel" Visible="false" runat="server" CssClass="txtButtonRed" Text="Export To Excel"></asp:LinkButton>
                                </div>
                            </td>							 
						</tr>						
					</table>
					<%--<table>
					  <tr valign="top">
						<td align=center >
							 <rsweb:ReportViewer ID="ReportViewerOW" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="600px" ProcessingMode="Remote" Width="100%" bgColor="#FFFFFF">
									<ServerReport ReportServerUrl="" />
							 </rsweb:ReportViewer>
						</td>
					  </tr>						  
					  </table>--%>
					  
					  <asp:panel id="pnlShowRecords" runat="server" Visible="false">
					  <div style="margin:15px">
					  <p style="font-size:16px; text-align:center;"><strong>Single Company Report for <asp:Label ID="lblCompanyName" runat="server"></asp:Label></strong>
					   <br /><br />
					  <span style="font-size:14px;"><strong>Date Range : </strong><asp:Label ID="lblFromDate" runat="server"></asp:Label> To <asp:Label ID="lblToDate" runat="server"></asp:Label></span>
					  </p>
					  </div>
			    <div style="margin:15px">			    
			    <p style="text-decoration:underline; font-size:14px;"><strong>Summary</strong></p>
			    <div style="margin:15px">
			     <table >
			            <tr>
			                <td colspan="6"><strong style="text-decoration:underline">WorkOrders</strong></td>
			            </tr>
                        <tr >
                            <td width="100" style="font-weight:bold">Type</td>
                            <td width="100" style="font-weight:bold">No. of Orders</td>
                            <td width="80" style="font-weight:bold;text-align:right">WP</td>
                            <td width="80" style="font-weight:bold;text-align:right">PP</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin (%)</td>
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater1" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="100"><%#Container.DataItem("label")%></td>
                            <td width="100"><%#Container.DataItem("TotalWorkOrders")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalWholesalePrice")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalPlatformPrice")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("Margin")%></td>
                            <td width="80" style="text-align:right"><%#Container.DataItem("PercentMargin")%></td>
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:Repeater>
                
                <p></p>
			    <div>
			     <table>
			            <tr>
			                <td colspan="3"><strong style="text-decoration:underline">Invoices</strong></td>
			            </tr>
                        <tr>
                            <td width="100" style="font-weight:bold">Type</td>
                            <td width="120" style="font-weight:bold">No. of Invoices</td>
                            <td width="80" style="font-weight:bold;text-align:right">Net Value</td>                            
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater2" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="100"><%#Container.DataItem("InvoiceType")%></td>
                            <td width="120"><%#Container.DataItem("NoOfInvoices")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalAmount")%></td>                           
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:Repeater>
                </div>
                </div>
                <p></p>
			    <div id="divServices" runat="server" style="margin:15px">
			     <table>
			            <tr>
			                <td colspan="6"><strong style="text-decoration:underline">Services</strong></td>
			            </tr>
                        <tr>
                            <td width="220" style="font-weight:bold">Service Name</td>
                            <td width="60" style="font-weight:bold">Quantity</td>
                            <td width="80" style="font-weight:bold;text-align:right">CP</td>
                            <td width="80" style="font-weight:bold;text-align:right">WP</td>
                            <td width="80" style="font-weight:bold;text-align:right">PP</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin (%)</td>
                            <td width="80" style="font-weight:bold;text-align:right">OWMargin</td>
                            <td width="80" style="font-weight:bold;text-align:right">OWMargin (%)</td>
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater3" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="220"><%#Container.DataItem("ProductName")%></td>
                            <td width="60"><%#Container.DataItem("QuantitySold")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("CustomerPrice")%></td>                           
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("WholesalePrice")%></td>                           
                            <td width="80" style="text-align:right">&pound; <%# Container.DataItem("PlatformPrice")%></td>   
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("CustomerMargin")%></td>                           
                            <td width="80" style="text-align:right"><%#Container.DataItem("PercentCustomerMargin")%></td>  
                              <td width="80" style="text-align:right">&pound; <%# Container.DataItem("OWMargin")%></td>                           
                            <td width="80" style="text-align:right"><%#Container.DataItem("PercentOWMargin")%></td>                          
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:Repeater>
                </div>
                
                <p></p><p></p>
                <div style="margin:15px">
                <asp:Repeater ID="Repeater7" runat="server">
                <ItemTemplate>
                <input type="hidden" id="hdnBillingLocID" value='<%#Container.DataItem("BillingLocID")%>' runat="server" />                
                 <p style="text-decoration:underline; font-size:14px;"><strong><%#Container.DataItem("BillingLocName")%></strong></p>
			    <div style="margin:15px">
			     <table>
			            <tr>
			                <td colspan="6"><strong style="text-decoration:underline">WorkOrders</strong></td>
			            </tr>
                        <tr>
                            <td width="100" style="font-weight:bold">Type</td>
                            <td width="100" style="font-weight:bold">No. of Orders</td>
                            <td width="80" style="font-weight:bold;text-align:right">WP</td>
                            <td width="80" style="font-weight:bold;text-align:right">PP</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin (%)</td>
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater4" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="100"><%#Container.DataItem("label")%></td>
                            <td width="100"><%#Container.DataItem("TotalWorkOrders")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalWholesalePrice")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalPlatformPrice")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("Margin")%></td>
                            <td width="80" style="text-align:right"><%#Container.DataItem("PercentMargin")%></td>
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:Repeater>
                </div style="margin:15px">
                <p></p>
			    <div style="margin:15px">
			     <table>
			            <tr>
			                <td colspan="3"><strong style="text-decoration:underline">Invoices</strong></td>
			            </tr>
                        <tr>
                            <td width="100" style="font-weight:bold">Type</td>
                            <td width="120" style="font-weight:bold">No. of Invoices</td>
                            <td width="80" style="font-weight:bold;text-align:right">Net Value</td>                            
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater5" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="100"><%#Container.DataItem("InvoiceType")%></td>
                            <td width="120"><%#Container.DataItem("NoOfInvoices")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalAmount")%></td>                           
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:Repeater>
                </div>
                <p></p>
			    <div id="divServices2" runat="server" style="margin:15px">
			     <table>
			            <tr>
			                <td colspan="6"><strong style="text-decoration:underline">Services</strong></td>
			            </tr>
                        <tr>
                            <td width="220" style="font-weight:bold">Service Name</td>
                            <td width="60" style="font-weight:bold">Quantity</td>
                            <td width="80" style="font-weight:bold;text-align:right">CP</td>
                            <td width="80" style="font-weight:bold;text-align:right">WP</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin (%)</td>
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater6" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="220"><%#Container.DataItem("ProductName")%></td>
                            <td width="60"><%#Container.DataItem("QuantitySold")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("CustomerPrice")%></td>                           
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("WholesalePrice")%></td>                           
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("CustomerMargin")%></td>                           
                            <td width="80" style="text-align:right"><%#Container.DataItem("PercentCustomerMargin")%></td>                           
                        </tr>
                    </table>
                    
                </ItemTemplate>
                </asp:Repeater>
                </div>
                </ItemTemplate></asp:Repeater>
                </div>
                </div>
                </asp:panel>
                <asp:Panel ID="pnlNoRecords" runat="server" Visible="false">
                <table width="100%">
                <tr><td style="text-align:center; height:250px; padding-top:150px;" align="center">
                No records Available
                </td>
                </tr>
                </table>
                
                </asp:Panel>
                <asp:Panel ID="pnlInitialize" runat="server" Visible="true">
                
                </asp:Panel>
			<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>

</asp:Content>
