﻿<%@ Page Title="Blocked Dates" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" CodeBehind="BlockedDates.aspx.vb" Inherits="Admin.BlockedDates" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>

<link href='fullcalendar/fullcalendar.css' rel='stylesheet' />
<script type="text/javascript" src='Fullcalendar/jquery-1.9.1.min.js'></script>
<script type="text/javascript" src='Fullcalendar/jquery-ui-1.10.2.custom.min.js'></script>
<script type="text/javascript" src='Fullcalendar/fullcalendar.min.js'></script>
<script language="javascript" src="JS/main.js"  type="text/javascript"></script>

<script><asp:Literal id="litBlockDate" runat="server"></asp:Literal></script> 
<script type="text/javascript" language="javascript">
    function setCalender() {
        var Dates = {
            newDates: []
        };

        for (var i = 1; i <= BlockDateArr.length - 1; i++) {

            Dates.newDates.push({
                "id": 99,
                "title": BlockDateArr[i][0],
                "start": new Date(BlockDateArr[i][3], (BlockDateArr[i][2] - 1), BlockDateArr[i][1]),
                "description": BlockDateArr[i][4],
                "NewTitle": BlockDateArr[i][6]
            });
        }

        $(document).ready(function () {

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var calendar = $('#ctl00_ContentPlaceHolder1_calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title'

                },

                selectable: true,
                selectHelper: true,
                select: function (start, end, allDay) {
                    var seltime = start.getHours() + '.' + start.getMinutes();
                    var title = '';
                    var appdatetimestart = start;
                    appdatetimestart.setHours(0);
                    appdatetimestart.setMinutes(0);
                    appdatetimestart.setSeconds(0);
                    appdatetimestart.setMilliseconds(0);


                    var ctoday = new Date();
                    ctoday.setHours(0);
                    ctoday.setMinutes(0);
                    ctoday.setSeconds(0);
                    ctoday.setMilliseconds(0);
                    title = showappointmentpopup(start);
                    if (title) {
                        calendar.fullCalendar('renderEvent',
						{
						    title: title,
						    start: start,
						    end: end,
						    allDay: allDay
						},
						true // make the event "stick"
					);
                    }
                    calendar.fullCalendar('unselect');
                },
                editable: true,
                disableDragging: true,
                events: Dates.newDates,
                eventColor: '#AFAFAF',
                eventClick: function (calEvent) {
                    showUnBlockDate(calEvent.start);
                    AddRemoveDate(BlockDateArr[calEvent.NewTitle][7]);
                }

            });
        });
    }

    function AddRemoveDate(TimeSlot) {
        document.getElementById("ctl00_ContentPlaceHolder1_chkMorningSlot").checked = false;
        document.getElementById("ctl00_ContentPlaceHolder1_chkEveningSlot").checked = false;

        if (TimeSlot == 'Both') {
            document.getElementById("ctl00_ContentPlaceHolder1_chkMorningSlot").checked = true;
            document.getElementById("ctl00_ContentPlaceHolder1_chkEveningSlot").checked = true;
        }
        if (TimeSlot == '8am - 1pm') {
            document.getElementById("ctl00_ContentPlaceHolder1_chkMorningSlot").checked = true;
        }
        if (TimeSlot == '1pm - 6pm') {
            document.getElementById("ctl00_ContentPlaceHolder1_chkEveningSlot").checked = true;
        }
        document.getElementById("ctl00_ContentPlaceHolder1_hdnTimeSlot").value = TimeSlot;
        $find("ctl00_ContentPlaceHolder1_mdlDateInfo").show();
    }
    function showappointmentpopup(GetDate) {
        var date = new Date(GetDate);
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var hr = date.getHours();
        var min = date.getMinutes();
        document.getElementById("ctl00_ContentPlaceHolder1_appointdate").value = y + '/' + (m + 1) + '/' + d;
        document.getElementById("ctl00_ContentPlaceHolder1_ChkDate").value = "Yes";
        AddRemoveDate('');
    }
    function showUnBlockDate(GetDate) {

        var date = new Date(GetDate);
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var hr = date.getHours();
        var min = date.getMinutes();
        document.getElementById("ctl00_ContentPlaceHolder1_appointdate").value = y + '/' + (m + 1) + '/' + d;

    }
    function ShowInfo(Text) {
        //alert(Text);
        document.getElementById("divServiceText").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_lblServiceText").innerHTML = "";
        if (Text != "") {
            var obj = document.getElementById("divServiceText");
            //alert(posx);
            //alert(posy);
            obj.style.left = posx + 10 + 'px';
            obj.style.top = posy + 5 + 'px';
            obj.style.display = 'block'
            document.getElementById("ctl00_ContentPlaceHolder1_lblServiceText").innerHTML = BlockDateArr[Text][5];
        }
    }
    function validate_required() {
        document.getElementById("ctl00_ContentPlaceHolder1_btnSaveBlockDate").click();
    }
    </script>
      
<style type="text/css">
#ctl00_ContentPlaceHolder1_calendar 
{
width: 965px;
margin: 10px 50px 20px;
text-align: center;
font-size: 14px;
font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
float:left;
}		
.div1000{float:left;width:1000px;margin-left:50px;margin-top:10px;}
.div230{float:left;width:230px;}
.div360{float:left;width:360px;}
		
.divServices{float:left;width:650px;}
#ctl00_ContentPlaceHolder1_divDummy{width:1030px;height:300px;}
</style>
<input type="hidden" id="hdnContactID" runat="Server" value="" />
<input type="hidden" id="hdnCompName" runat="Server" value="" />
<input type="hidden" id="hdnClickButton" runat="Server" value="" />
<input type="hidden" id="ChkDate" runat="Server" value="" />
<input type="hidden" id="appointdate" runat="Server" value="" />
<input type="hidden" id="hdnbillinglocation" runat="Server" value="" />
<input type="hidden" id="hdnServiceValue" runat="Server" value="" />

<div class="div1000 HeadingRed" id="lblMessage">Block Dates</div>
<div id="divServiceText" style="padding:5px; z-index:999;position:absolute;background-color:#FFEBDE; font-family: Tahoma, Arial, Verdana; font-size: 11px; color: #000000; text-decoration: none; border: 1px solid #CECBCE;display:none;">
    <asp:Label ID="lblServiceText" runat="server" Text="ddddd"></asp:Label>
</div>
<div class="div1000">

<div class="div360"><asp:Label ID="Label2" runat="server" Text="Select Contact " style="float:left;margin-right:10px;"></asp:Label>
    <%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
    <uc1:UCSearchContact id="UCSearchContact1" runat="server" CompanyAutoSuggest="True"></uc1:UCSearchContact>
</div>
<div class="div230"><asp:Label ID="lblBillingLocation" runat="server" Text="Billing Location " Visible="false"></asp:Label>
                    <asp:Label ID="lblLocationMandatory" runat="server" Visible="false" Text="*" CssClass="bodytxtValidationMsg" ></asp:Label>
<asp:DropDownList ID="drpdwnBillingLocation" runat="server" Visible="False" CssClass="formFieldGrey" Width="210" DataTextField="Name" DataValueField="AddressID" style="float:left;margin-right:10px;" Onchange="javascript:PopulateCalender('test');">
                    </asp:DropDownList>
</div>
</div>

<div class="div1000" style="margin-top:1px;">
<div class="divServices">  
<table cellpadding="1" cellspacing="1" id="tblServices" runat="server" Visible="false">
    <tr>
                    
    <td>
    <input type="hidden" runat="server" id="hdnServiceSelected" name="hdnServiceSelected" >
        <strong>Add Service</strong>
        <asp:Label ID="lblServiceMandatory" runat="server" Visible="false" Text="*" CssClass="bodytxtValidationMsg" ></asp:Label>
        <br />
        <asp:ListBox ID="listService"  title="Service" Style="margin-top: 5px"
            SelectionMode="Multiple" runat="server" Width="375" Height="190px" 
            ToolTip="Service" DataValueField="ProductID" DataTextField="ProductName" DataMember="GroupLevel"
            CssClass="formField"></asp:ListBox>
        <br>
        <span style="color: #999999; font-size: 11px; line-height: 11px;">Note: Hold down CTRL
            to select more than one<br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Use Command key for
            Mac </span>
            <br />
            
    </td>
    <td >
        <input type="button" title="Add" width="20" height="20" onclick="addToList('ctl00_ContentPlaceHolder1_listService','ctl00_ContentPlaceHolder1_listServiceSel','ctl00_ContentPlaceHolder1_hdnServiceSelected','Add');PopulateCalender('test');"
            value='>>' style="width: 35px; height: 20px; padding-bottom: 0px; margin-bottom: 0px;
            vertical-align: middle; text-align: center;" title="Add an item from Left"><br />
        <br />
        <br />
        <input type="button" title="Remove" width="20" height="20" onclick="addToList('ctl00_ContentPlaceHolder1_listServiceSel','ctl00_ContentPlaceHolder1_listService','ctl00_ContentPlaceHolder1_hdnServiceSelected','Remove');PopulateCalender('test');"
            value='<<' style="width: 35px; height: 20px; padding-bottom: 0px; margin-bottom: 0px"
            title="Remove an item from Right">
    </td>
    <td>
        <strong>Selected Service</strong>
        <br />
        <asp:ListBox ID="listServiceSel" title="Selected Service" Style="margin-top: 5px"
            runat="server" Width="375px" Height="190px" DataTextField="ProductName" 
            DataValueField="ProductID" SelectionMode="Multiple" CssClass="formField"></asp:ListBox>
        <br>
        <span style="color: #999999; font-size: 11px; line-height: 11px;">Note: Hold down CTRL
            to select more than one<br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Use Command key for
            Mac </span>
    </td>                    
</tr>
            
</table> 
</div>


</div>
<div class="div1000" style="margin-top:20px;margin-bottom:20px;">
<div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:150px;margin-top:15px;" id="tblViewDates" runat="server">
    <a ID="lnkViewBlockDates" runat="server" class="txtButtonRed" onclick="javascript:ValidateAutoSuggestBlockDates()" style="cursor:pointer" Visible="False">View Blocked Dates</a>                        
</div>
<asp:Button ID="btnViewBlockDate" Width="0" Height="0" Borderwidth="0"  runat="server" Text="Button"  CausesValidation="false"  />	
</div>              
 
 
<asp:Button ID="btnSelect" Width="0" Height="0" Borderwidth="0"  runat="server" Text="Button"  CausesValidation="false"  />	
<asp:Button ID="btnSaveBlockDate"  Width="0" Height="0" Borderwidth="0"  runat="server" Text=""  CausesValidation="false"  />		 
<div id="divDummy" runat="server" visible="true"></div>

<div id="calendar" runat="server" Visible="False">


</div>

<div style="float:left;position:fixed;top:-200px;margin-top:180px;">
<cc1:ModalPopupExtender ID="mdlDateInfo" runat="server" TargetControlID="btnTemp" PopupControlID="pnlDateInfoConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackgroundConfirm" Drag="False" DropShadow="False"></cc1:ModalPopupExtender>
<asp:Panel runat="server" ID="pnlDateInfoConfirm" Width="300px" CssClass="pnlConfirm" style="display:none;">
    <div id="divModalParent">
       <div style="font-family:Tahoma; font-size:14px;margin-bottom:10px;"><b>Time Slot Info</b></div>
       <input type="hidden" id="hdnTimeSlot" runat="Server" value="" />
        <div id="divMsgPopup" runat="server" style="margin:10px;" visible="false"><asp:Label ID="lblMsgPopup" runat="server" CssClass="bodytxtValidationMsg"></asp:Label></div>
         <asp:CheckBox ID="chkMorningSlot" runat="server" Text="8am - 1pm" />
        <br /><br /> 
          <asp:CheckBox ID="chkEveningSlot" runat="server" Text="1pm - 6pm" />
        <br /><br />

            <div id="divButton" class="divButton" style="width: 85px; float:left;cursor:pointer;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                    <a class="buttonText cursorHand" href='javascript:validate_required()' id="ancSubmit"><strong>Submit</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div>
            <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;cursor:pointer;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>								
</asp:Panel>
<asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
</div>

</asp:Content>
