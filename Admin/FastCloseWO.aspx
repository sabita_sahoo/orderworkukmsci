<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Fast Close WO" CodeBehind="FastCloseWO.aspx.vb" Inherits="Admin.FastCloseWO" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

    
				<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
	     <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            <asp:UpdatePanel ID="UpdatePanelMultipleWO" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMultipleWO" runat="server">
                    
              <div style="margin-left:15px; margin-top:5px; font-size:12px;">
                <p class="paddingB4 HeadingRed"><strong>Welcome to OrderWork: Fast close WO</strong> </p>
                <asp:Panel ID="pnlGridListings" runat="server">
                    <div id="divValidWOIDs" runat="server" style="margin-top:10px;">
                        <strong><asp:Label ID="lblValidWO" runat="server" Text="Label"></asp:Label></strong><br />
                        <asp:DataList ID="dlValidWO" runat="server" CssClass="formLabelGrey" Font-Size="10px">
                                  <ItemTemplate>  
                                    <div style="width:100%;">
                                        <div style=" float:left;"><%#Container.DataItem("RefWOID")%> </div> 
                                        <div style=" float:left; margin-left:10px;">WorkOrder Title: <%#Container.DataItem("WOTitle")%> </div> 
                                    </div>
                                   </ItemTemplate>     
                       </asp:DataList>
                       <asp:Label ID="lblNovalidRecords" runat="server" Text="" Visible="false"></asp:Label>
                    </div>
                    
                    <div id="divInvalidWOIDs" runat="server" style="margin-top:10px;">
                        <strong><asp:Label ID="lblInValidWO" runat="server" Text="Label"></asp:Label></strong><br />
                        <asp:DataList ID="dlInvalidWO" runat="server" CssClass="formLabelGrey" Font-Size="10px">
                                  <ItemTemplate>  
                                    <div style="width:100%;">                                         
                                         <div style=" float:left;">WorkOrderID: <%#Container.DataItem("RefWOID")%> </div> 
                                         <div style=" float:left; margin-left:10px;">WorkOrder Title: <%#Container.DataItem("WOTitle")%> </div> 
                                    </div>
                                   </ItemTemplate>  
                        </asp:DataList>
                        
                    </div>
                </asp:Panel>
                
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div style=" margin-top:10px;">
                    <asp:Button ID="btnMassAcceptanceCanel" Visible="false" runat="server" Text="Cancel" />
                    <asp:Button ID="btnConfirm" runat="server" Text="Confirm" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                    </div>
                </asp:Panel>
                <div style=" margin-top:10px;">
                  <asp:Button ID="btnNext" runat="server" Text="Next" />
                  <asp:Button ID="btnBack" runat="server" Text="Back" />
                </div>
            </div>
                </asp:Panel>
            </ContentTemplate>
             <Triggers>
  	            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnConfirm" />
	            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnCancel" />
	            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnNext" />
	            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnBack" />
              </Triggers>
            </asp:UpdatePanel>
            
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
<asp:UpdateProgress  ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanelMultipleWO" runat="server">
                <ProgressTemplate>
                    <div class="gridText">
                        <img  align=middle src="Images/indicator.gif" />
                        <b>Please wait saving data...</b>
                    </div>      
                </ProgressTemplate>
            </asp:UpdateProgress>
        <cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender2" ControlToOverlayID="pnlMultipleWO" CssClass="updateProgress" TargetControlID="UpdateProgress2" runat="server" />


</asp:Content>