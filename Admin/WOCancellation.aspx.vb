Imports System.IO
Imports System.Net

Partial Public Class WOCancellation
    Inherits System.Web.UI.Page

    Dim ws As New WSObjs
    Private Delegate Sub AfterCancelWO(ByVal Mode As String, ByVal VerNum As Byte())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then
            If Not IsNothing(Request("WorkOrderID")) Then
                If (Request("WorkOrderID") <> "") Then
                    txtWorkOrderId.Text = Request("WorkOrderId")
                    populateWODetails(Request("WorkOrderID").Trim)
                    ViewState("Group") = Request("Group")
                End If
            End If
        End If
    End Sub

    ''' <summary>
    '''  Function to populate the WO cancellation form
    ''' </summary>
    ''' <param name="WorkOrderID"></param>
    ''' <remarks></remarks>
    Private Sub populateWODetails(ByVal WorkOrderID As String)
        Dim ds As DataSet

        ds = ws.WSWorkOrder.MS_WOCancellationGetDetails(WorkOrderID)
        'Check if workorder in Draft / Enquiry Draft stage
        If ds.Tables("tblWODetails").Rows.Count > 0 Then
            'Store the version number
            CommonFunctions.StoreVerNum(ds.Tables("tblWODetails"))

            If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("ClientContactId")) Then
                ViewState("ClientContactId") = ds.Tables("tblWODetails").Rows(0).Item("ClientContactId")
            End If
            If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("ClientCompanyId")) Then
                ViewState("ClientCompanyId") = ds.Tables("tblWODetails").Rows(0).Item("ClientCompanyId")
            End If

            txtCompleteComments.Visible = False

            Dim liStore As New ListItem
            liStore = New ListItem
            liStore.Text = "Please Select"
            liStore.Value = ""
            drpdwncancelreason.DataSource = ds.Tables("tblStandards")
            drpdwncancelreason.DataBind()
            drpdwncancelreason.Items.Insert(0, liStore)



            If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WOStatus")) Then
                ViewState("WOStatus") = ds.Tables("tblWODetails").Rows(0).Item("WOStatus")
            End If
            ViewState("DateClosed") = ds.Tables("tblWODetails").Rows(0).Item("DateClosed")
            'Check if workorder already cancelled.
            If (ViewState("WOStatus") <> ApplicationSettings.WOStatusID.Cancelled) Then
                'ckeck if workorder is closed.
                If (ViewState("WOStatus") = ApplicationSettings.WOStatusID.Closed) Then
                    'WO Title
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WOTitle")) Then
                        If checkIfCancelled(ds.Tables("tblWODetails").Rows(0).Item("WOTitle")) Then
                            ' WO Status = Closed through cancellation
                            lblMsg.Visible = True
                            lblMsg.Text = "Sorry, this WorkOrder is closed through the cancellation process."
                            pnlCancel.Visible = False
                        Else
                            ' WO Status = Closed
                            lblMsg.Visible = True
                            lblMsg.Text = "Sorry, this WorkOrder is closed."
                            pnlCancel.Visible = False
                        End If
                    End If
                Else
                    pnlCancel.Visible = True

                    'WO No
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("RefWOID")) Then
                        'lblWONo.Text = ds.Tables("tblWODetails").Rows(0).Item("WorkOrderId")
                        lblWONo.Text = ds.Tables("tblWODetails").Rows(0).Item("RefWOID")
                    End If
                    'Submitted
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("DateCreated")) Then
                        lblWOSubmitted.Text = Strings.FormatDateTime(ds.Tables("tblWODetails").Rows(0).Item("DateCreated"), DateFormat.ShortDate)
                    End If
                    'Location
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("Location")) Then
                        lblWOLoc.Text = ds.Tables("tblWODetails").Rows(0).Item("Location")
                    End If
                    'Contact
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("Contact")) Then
                        lblWOContact.Text = ds.Tables("tblWODetails").Rows(0).Item("Contact")
                    End If
                    'WO Title
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WOTitle")) Then
                        lblWOTitle.Text = ds.Tables("tblWODetails").Rows(0).Item("WOTitle")
                    End If
                    'WO Start
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("DateStart")) Then
                        lblWOStart.Text = Strings.FormatDateTime(ds.Tables("tblWODetails").Rows(0).Item("DateStart"), DateFormat.ShortDate)
                    End If
                    'Wholesale Price
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice")) Then
                        lblWPPrice.Text = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.False, TriState.False)
                    End If
                    'Platform Price
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PlatformPrice")) Then
                        lblPPPrice.Text = FormatNumber(ds.Tables("tblWODetails").Rows(0).Item("PlatformPrice"), 2, TriState.True, TriState.False, TriState.False)
                    End If
                    'Status
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("Status")) Then
                        lblWOStatus.Text = ds.Tables("tblWODetails").Rows(0).Item("Status")
                    End If

                    'Show/Hide Supplier Title & Details depending upon the stage
                    If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted Or ViewState("WOStatus") = ApplicationSettings.WOStatusID.CA Or ViewState("WOStatus") = ApplicationSettings.WOStatusID.Sent Then
                        trSuppTitle.Visible = False
                        trSuppDetails.Visible = False
                        ViewState("CancelForSupp") = False
                        ViewState("SupplierCompanyId") = False
                    Else
                        trSuppTitle.Visible = True
                        trSuppDetails.Visible = True
                        ViewState("CancelForSupp") = True
                        ViewState("SupplierCompanyId") = True
                    End If

                    'Show/Hide Rematch and copy wo action buttons

                    If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted Or ViewState("WOStatus") = ApplicationSettings.WOStatusID.Draft Then
                        If ds.Tables("tblWODetails").Rows(0).Item("WOStatusSent") = ApplicationSettings.WOStatusID.Sent Then
                            TdBtnCopy.Visible = True
                            TdBtnRematch.Visible = True
                        Else
                            TdBtnCopy.Visible = False
                            TdBtnRematch.Visible = False
                        End If
                    Else
                        TdBtnCopy.Visible = True
                        TdBtnRematch.Visible = True
                    End If



                    'Populate the price fields
                    'Wholesale Price
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice")) Then
                        txtWPPrice.Text = ds.Tables("tblWODetails").Rows(0).Item("WholesalePrice")
                        txtWPCancel.Text = FormatNumber(0, 2, TriState.True, TriState.False, TriState.False)
                    End If
                    'Platform Price
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PlatformPrice")) Then
                        txtPPPrice.Text = ds.Tables("tblWODetails").Rows(0).Item("PlatformPrice")
                        txtPPCancel.Text = FormatNumber(0, 2, TriState.True, TriState.False, TriState.False)
                    End If

                    'WOID
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WOID")) Then
                        ViewState("WOID") = ds.Tables("tblWODetails").Rows(0).Item("WOID")
                    End If
                    'DateEnd
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("DateEnd")) Then
                        ViewState("DateEnd") = ds.Tables("tblWODetails").Rows(0).Item("DateEnd")
                    End If
                    'DateStart
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("DateStart")) Then
                        ViewState("DateStart") = ds.Tables("tblWODetails").Rows(0).Item("DateStart")
                    End If
                    'WOTitle
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WOTitle")) Then
                        ViewState("WOTitle") = ds.Tables("tblWODetails").Rows(0).Item("WOTitle")
                    End If
                    'SpecialistSuppliesParts
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("SpecialistSuppliesParts")) Then
                        ViewState("SpecialistSuppliesParts") = ds.Tables("tblWODetails").Rows(0).Item("SpecialistSuppliesParts")
                    End If
                    'Location
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("Location")) Then
                        ViewState("Location") = ds.Tables("tblWODetails").Rows(0).Item("Location")
                    End If
                    'Work Order Id
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WorkOrderId")) Then
                        ViewState("WorkOrderId") = ds.Tables("tblWODetails").Rows(0).Item("WorkOrderId")
                    End If
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("RefWOID")) Then
                        ViewState("RefWOID") = ds.Tables("tblWODetails").Rows(0).Item("RefWOID")
                    End If
                    'WO Category
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WorkOrderId")) Then
                        ViewState("WOCategory") = ds.Tables("tblWODetails").Rows(0).Item("WOCategory")
                    End If
                    'PO Number
                    If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("PONumber")) Then
                        ViewState("PONumber") = ds.Tables("tblWODetails").Rows(0).Item("PONumber")
                    End If

                    'Status of the Workorder
                    ViewState("WorkOrderStatus") = ViewState("WOStatus")

                    If (ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted) Then
                        If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WOStatusSent")) Then
                            'Submitted workorder & Sent
                            If ((ds.Tables("tblWODetails").Rows(0).Item("WOStatusSent")) = ApplicationSettings.WOStatusID.Sent) Then
                                ViewState("WorkOrderStatus") = ApplicationSettings.WOStatusID.Sent
                                'Submitted workorder & Supplier Accepted
                            ElseIf ((ds.Tables("tblWODetails").Rows(0).Item("WOStatusSent")) = ApplicationSettings.WOStatusID.SupplierAccepted) Then
                                ViewState("WorkOrderStatus") = ApplicationSettings.WOStatusID.SupplierAccepted
                            End If
                        End If
                    ElseIf (ViewState("WOStatus") = ApplicationSettings.WOStatusID.CA) Then
                        If Not IsDBNull(ds.Tables("tblWODetails").Rows(0).Item("WOStatusSent")) Then
                            'CA & Supplier Accepted
                            If ((ds.Tables("tblWODetails").Rows(0).Item("WOStatusSent")) = ApplicationSettings.WOStatusID.SupplierAccepted) Then
                                ViewState("WorkOrderStatus") = ApplicationSettings.WOStatusID.SupplierAccepted
                            End If
                        End If
                    End If
                End If

                'Rating of the cancelled workorder
                If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Accepted Or ViewState("WOStatus") = ApplicationSettings.WOStatusID.Issue Or ViewState("WOStatus") = ApplicationSettings.WOStatusID.Completed Then
                    TrRating.Visible = True
                Else
                    TrRating.Visible = False
                End If

            Else
                ' WO Status = Cancelled
                lblMsg.Visible = True
                lblMsg.Text = "Sorry, this WorkOrder is already cancelled."
                pnlCancel.Visible = False
            End If
        Else
            ' WO Status = draft / Enquiry Draft / Closed
            lblMsg.Visible = True
            lblMsg.Text = "Sorry, this WorkOrder cannot be cancelled."
            pnlCancel.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' Function to check if the workorder is cancelled
    ''' </summary>
    ''' <param name="strTitle"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function checkIfCancelled(ByVal strTitle As String) As Boolean
        If Strings.InStr(strTitle, "Cancellation Charges") Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Function to create the back to listing link
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function BackToListing() As String

        Dim appendLink As String = ""

        If Request("sender") = "SearchWO" Then
            appendLink = "SearchWOs.aspx?"
            appendLink &= "&SrcWorkorderID=" & Request("txtWorkorderID")
            appendLink &= "&SrcCompanyName=" & Request("CompanyName")
            appendLink &= "&SrcKeyword=" & Request("KeyWord")
            appendLink &= "&SrcPONumber=" & Request("PONumber")
            appendLink &= "&SrcReceiptNumber=" & Request("ReceiptNumber")
            appendLink &= "&SrcPostCode=" & Request("PostCode")
            appendLink &= "&SrcDateStart=" & Request("DateStart")
            appendLink &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
            appendLink &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")
            appendLink &= "&PS=" & Request("PS")
            appendLink &= "&PN=" & Request("PN")
            appendLink &= "&SC=" & Request("SC")
            appendLink &= "&SO=" & Request("SO")
        Else
            'appendLink &= "WOID=" & Request("WOID")
            appendLink &= "WOID=" & ViewState("WOID")
            appendLink &= "&WorkOrderID=" & Request("WorkOrderID")
            appendLink &= "&ContactID=" & Request("ContactID")
            appendLink &= "&CompanyID=" & Request("CompanyID")
            appendLink &= "&CompID=" & Request("CompanyID")
            appendLink &= "&Viewer=" & Request("Viewer")
            'appendLink &= "&Group=" & Request("Group")
            'appendLink &= "&mode=" & Request("Group")

            appendLink &= "&Group=" & ViewState("Group")
            appendLink &= "&mode=" & ViewState("Group")

            appendLink &= "&sender=" & Request("sender")
            appendLink &= "&BizDivID=" & Session("BizDivId")
            appendLink &= "&FromDate=" & Request("FromDate")
            appendLink &= "&ToDate=" & Request("ToDate")
            appendLink &= "&PS=" & Request("PS")
            appendLink &= "&PN=" & Request("PN")
            appendLink &= "&SC=" & Request("SC")
            If Not IsNothing(Request("SearchWorkorderID")) Then
                appendLink &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
            End If
            appendLink &= "&SO=" & Request("SO")
            If Not IsNothing(Request("IsNextDay")) Then
                If Request("IsNextDay") = "Yes" Then
                    appendLink &= "&IsNextDay=" & "Yes"
                Else
                    appendLink &= "&IsNextDay=" & "No"
                End If
            End If
        End If

        Return appendLink
    End Function

    ''' <summary>
    ''' Function to populate the cancellation panel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Page.Validate()
        If Page.IsValid Then
            pnlCancel.Visible = True
            divValidationMain.Visible = False
            populateWODetails(txtWorkOrderId.Text.Trim)
        Else
            pnlCancel.Visible = False
            divValidationMain.Visible = True
        End If

        If Request("Sender") = "SearchWO" Then
            Response.Redirect("SearchWOs.aspx?" & BackToListing())
        Else
            'OA-476 - OA - Click on Back button after WP change should take user back to Wo Details
            'Response.Redirect("AdminWOListing.aspx?" & BackToListing())
            Response.Redirect("AdminWODetails.aspx?" & BackToListing())
        End If

    End Sub

    ''' <summary>
    ''' Function to go back to listing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If IsNothing(Request("WorkOrderID")) Then
            pnlCancel.Visible = False
        ElseIf Request("Sender") = "SearchWO" Then
            Response.Redirect("SearchWOs.aspx?" & BackToListing())
        Else
            'OA-476 - OA - Click on Back button after WP change should take user back to Wo Details
            'Response.Redirect("AdminWOListing.aspx?" & BackToListing())
            Response.Redirect("AdminWODetails.aspx?" & BackToListing())
        End If
    End Sub

    ''' <summary>
    ''' Function to cancel the workorder
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'Cancel Workorder
        CancelWO("Cancel")
    End Sub



    ''' <summary>
    ''' Function to cancel and copy the workorder
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        'Cancel Workorder
        CancelWO("Copy")
    End Sub

    ''' <summary>
    ''' Function to cancel, copy and rematch the workorder
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnRematch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRematch.Click
        'Cancel Workorder
        CancelWO("Rematch")
    End Sub

    Private Sub CancelWO(ByVal Mode As String)
        If Not (Request("CompanyID") Is Nothing) Then
            Session("AdminSelectedCompID") = Request("CompanyID")
        End If

        Dim ds_WOSuccess As DataSet
        Dim dvEmail As New DataView
        Dim dvEmailSupp As New DataView
        Dim sendMailToSupplier As Boolean = False
        Dim objEmail As New Emails
        Dim bizDivID As Integer = 0
        Dim WOAction As Integer
        bizDivID = Session("BizDivId")
        txtCompleteComments.Text = drpdwncancelreason.SelectedValue.ToString
        Page.Validate()

        If Page.IsValid() Then



            divValidationMain.Visible = False

            Dim strRating As Integer = 0
            If rdoRatingNeutral.Checked = True Then
                strRating = 0
            ElseIf rdoRatingNegative.Checked = True Then
                strRating = -1
            ElseIf rdoRatingPositive.Checked = True Then
                strRating = 1
            Else
                strRating = -2
            End If

            'Show/Hide Supplier Title & Details depending upon the stage
            If ViewState("WOStatus") = ApplicationSettings.WOStatusID.Submitted Or ViewState("WOStatus") = ApplicationSettings.WOStatusID.CA Or ViewState("WOStatus") = ApplicationSettings.WOStatusID.Sent Then
                If (CInt(txtWPCancel.Text.Trim) = 0) Then
                    ds_WOSuccess = ws.WSWorkOrder.MS_WOCancellationSave(ViewState("WOID"), "wocancel", Session("UserId"), Session("CompanyId"), ApplicationSettings.RoleOWID, ViewState("CancelForSupp"), 0, 0, txtCompleteComments.Text.Trim, bizDivID, ViewState("WorkOrderStatus"), CommonFunctions.FetchVerNum(), strRating)
                    WOAction = ApplicationSettings.WOStatusID.Cancelled
                ElseIf (CInt(txtWPCancel.Text.Trim) <> 0) Then
                    'If the Buyer Cancellation Charges is not zero
                    ds_WOSuccess = ws.WSWorkOrder.MS_WOCancellationSave(ViewState("WOID"), "woclose", Session("UserId"), Session("CompanyId"), ApplicationSettings.RoleOWID, ViewState("CancelForSupp"), CDec(txtWPCancel.Text.Trim), 0, txtCompleteComments.Text.Trim, bizDivID, ViewState("WorkOrderStatus"), CommonFunctions.FetchVerNum(), strRating)
                    WOAction = ApplicationSettings.WOStatusID.Closed
                End If
            Else
                'If the Buyer Cancellation charge and Supplier cancellation charge are zero then cancel the owrkorder
                If (CInt(txtWPCancel.Text.Trim) = 0) And (CInt(txtPPCancel.Text.Trim) = 0) Then
                    ds_WOSuccess = ws.WSWorkOrder.MS_WOCancellationSave(ViewState("WOID"), "wocancel", Session("UserId"), Session("CompanyId"), ApplicationSettings.RoleOWID, ViewState("CancelForSupp"), 0, 0, txtCompleteComments.Text.Trim, bizDivID, ViewState("WorkOrderStatus"), CommonFunctions.FetchVerNum(), strRating)
                    WOAction = ApplicationSettings.WOStatusID.Cancelled
                ElseIf (CInt(txtWPCancel.Text.Trim) <> 0) And (CInt(txtPPCancel.Text.Trim) = 0) Then
                    'If the Buyer Cancellation Charges is not zero and Supplier Cancellation Charges is zero
                    ds_WOSuccess = ws.WSWorkOrder.MS_WOCancellationSave(ViewState("WOID"), "woclose", Session("UserId"), Session("CompanyId"), ApplicationSettings.RoleOWID, ViewState("CancelForSupp"), CDec(txtWPCancel.Text.Trim), 0, txtCompleteComments.Text.Trim, bizDivID, ViewState("WorkOrderStatus"), CommonFunctions.FetchVerNum(), strRating)
                    WOAction = ApplicationSettings.WOStatusID.Closed
                ElseIf (CInt(txtWPCancel.Text.Trim) = 0) And (CInt(txtPPCancel.Text.Trim) <> 0) Then
                    'If the Buyer Cancellation Charges is zero and Supplier Cancellation Charges is not zero
                    ds_WOSuccess = ws.WSWorkOrder.MS_WOCancellationSave(ViewState("WOID"), "woclose", Session("UserId"), Session("CompanyId"), ApplicationSettings.RoleOWID, ViewState("CancelForSupp"), 0, CDec(txtPPCancel.Text.Trim), txtCompleteComments.Text.Trim, bizDivID, ViewState("WorkOrderStatus"), CommonFunctions.FetchVerNum(), strRating)
                    WOAction = ApplicationSettings.WOStatusID.Closed
                ElseIf (CInt(txtWPCancel.Text.Trim) <> 0) And (CInt(txtPPCancel.Text.Trim) <> 0) Then
                    'If the Buyer Cancellation Charges is not zero and Supplier Cancellation Charges is not zero
                    ds_WOSuccess = ws.WSWorkOrder.MS_WOCancellationSave(ViewState("WOID"), "woclose", Session("UserId"), Session("CompanyId"), ApplicationSettings.RoleOWID, ViewState("CancelForSupp"), CDec(txtWPCancel.Text.Trim), CDec(txtPPCancel.Text.Trim), txtCompleteComments.Text.Trim, bizDivID, ViewState("WorkOrderStatus"), CommonFunctions.FetchVerNum(), strRating)
                    WOAction = ApplicationSettings.WOStatusID.Closed
                End If

            End If
            Dim WOStatusText As String = ""

            If ds_WOSuccess.Tables.Count <> 0 Then
                If ds_WOSuccess.Tables("tblEmailRecipients").Rows.Count > 0 Then
                    If ds_WOSuccess.Tables("tblEmailRecipients").Rows(0).Item("ContactID") = -1 Then
                        'double entry
                    ElseIf ds_WOSuccess.Tables("tblEmailRecipients").Rows(0).Item("ContactID") = -10 Then
                        'divValidationMain.Visible = True
                        'Select Case ds_WOSuccess.Tables("tblEmailRecipients").Rows(0).Item("WorkOrderStatus")
                        '    Case ApplicationSettings.WOStatusID.Accepted
                        '        lblMsg.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                        '        WOStatusText = "Accepted"
                        '    Case ApplicationSettings.WOStatusID.Cancelled
                        '        lblMsg.Text = ResourceMessageText.GetString("WOVNoWOCancelled")
                        '        WOStatusText = "Cancelled"
                        '    Case ApplicationSettings.WOStatusID.Issue
                        '        lblMsg.Text = ResourceMessageText.GetString("WOVNoIssueRaised")
                        '        WOStatusText = "Issue"
                        '    Case ApplicationSettings.WOStatusID.Completed
                        '        lblMsg.Text = ResourceMessageText.GetString("WOVNoCompleted")
                        '        WOStatusText = "Completed"
                        '    Case ApplicationSettings.WOStatusID.Closed
                        '        lblMsg.Text = ResourceMessageText.GetString("WOVNoClosed")
                        '        WOStatusText = "Closed"
                        '    Case Else
                        '        lblMsg.Text = ResourceMessageText.GetString("WOVNoDetailsModified")
                        'End Select
                        WOStatusText = "Cancelled"
                        lblMsg.Text = ResourceMessageText.GetString("WOVersionControlMsg").Replace("<Link>", "AdminWODetails.aspx?WOID=" & Request("WOID") & "&WorkOrderID=" & Request("WorkOrderID") & "&SupContactId=" & Request("SupContactId") & "&SupCompId=" & Request("SupCompId") & "&Group=" & WOStatusText & "&CompID=" & Request("CompID") & "&FromDate=&ToDate=&PS=" & Request("PS") & "&PN=0&SC=DateCreated&SO=1&Viewer=" & ViewState("Viewer") & "&sender=UCWOsListing&BizDivID=" & Request("BizDivID"))
                        pnlCancel.Visible = False
                        tblWorkOrderId.Visible = False
                    Else
                        dvEmail = ds_WOSuccess.Tables("tblEmailRecipients").Copy.DefaultView
                        If IsDBNull(ViewState("DateClosed")) Then
                            objEmail.WODate = ""
                        Else
                            objEmail.WODate = FormatDateTime(ViewState("DateClosed"), DateFormat.ShortDate)
                        End If

                        objEmail.WOCategory = ViewState("WOCategory")
                        objEmail.WOEndDate = ViewState("DateEnd")
                        objEmail.WOLoc = ViewState("Location")
                        objEmail.WOPrice = ViewState("Value")
                        objEmail.WorkOrderID = ViewState("RefWOID")
                        objEmail.WOStartDate = ViewState("DateStart")
                        objEmail.WOTitle = ViewState("WOTitle")
                        objEmail.WPPrice = CDec(txtWPPrice.Text.Trim)
                        If txtPPPrice.Visible = True Then
                            objEmail.PPPrice = CDec(txtPPPrice.Text.Trim)
                            'Else
                            '    objEmail.PPPrice = 0
                        End If
                        Dim ppcancel As Decimal = 0
                        If txtPPCancel.Visible = True Then
                            ppcancel = CDec(txtPPCancel.Text.Trim)
                            objEmail.ppcancel = CDec(txtPPCancel.Text.Trim)
                        End If
                        Dim wpcancel As Decimal = 0
                        If txtWPCancel.Visible = True Then
                            wpcancel = CDec(txtWPCancel.Text.Trim)
                            objEmail.wpcancel = CDec(txtWPCancel.Text.Trim)
                        End If


                        'Set supplierID = 1 if mail to supplier is also to be sent.
                        'Added Comments in both the functions by Pankaj Malav on 23 Feb 2009 added Comments as parameter
                        If (ViewState("WorkOrderStatus") = ApplicationSettings.WOStatusID.Draft) Or (ViewState("WorkOrderStatus") = ApplicationSettings.WOStatusID.Submitted) Then
                            Emails.SendWOCancelMail(objEmail, dvEmail, False, txtCompleteComments.Text.Trim)
                        Else
                            Emails.SendWOCancelMail(objEmail, dvEmail, True, txtCompleteComments.Text.Trim, ds_WOSuccess)
                        End If

                        pnlCancel.Visible = False
                        lblMsg.Text = "The Work Order " & txtWorkOrderId.Text & " has been Cancelled"
                    End If
                Else
                    'no rows returned
                    divValidationMain.Visible = True
                    lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
                End If
            Else
                'no ds sent
                divValidationMain.Visible = True
                lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
            End If

            If (Mode = "Copy") Then
                Dim VerNum As Byte()
                VerNum = CommonFunctions.FetchVerNum()
                Dim CopyWO As [Delegate] = New AfterCancelWO(AddressOf AfterCancelActions)
                ThreadUtil.FireAndForget(CopyWO, New Object() {Mode, VerNum})

                If CInt(Request("CompanyID")) = 16363 Then
                    CommonFunctions.SendStatusUpdateToSONY(ViewState("RefWOID"), ViewState("PONumber"), "CLAIM", Nothing)
                End If

                ViewState("Group") = "Submitted"
                If Request("Sender") = "SearchWO" Then
                    Response.Redirect("SearchWOs.aspx?" & BackToListing())
                Else
                    Response.Redirect("AdminWOListing.aspx?" & BackToListing())
                End If
            ElseIf (Mode = "Rematch") Then
                Dim VerNum As Byte()
                VerNum = CommonFunctions.FetchVerNum()

                Dim CopyWO As [Delegate] = New AfterCancelWO(AddressOf AfterCancelActions)
                ThreadUtil.FireAndForget(CopyWO, New Object() {Mode, VerNum})

                ViewState("Group") = "Sent"
                If Request("Sender") = "SearchWO" Then
                    Response.Redirect("SearchWOs.aspx?" & BackToListing())
                Else
                    Response.Redirect("AdminWOListing.aspx?" & BackToListing())
                End If
            End If

        Else
            divValidationMain.Visible = True
        End If

    End Sub

    Private Sub AfterCancelActions(ByVal Mode As String, ByVal VerNum As Byte())
        Dim ds_Success As New DataSet
        Dim dsEmailContacts As New DataSet
        Dim dvEmail As New DataView
        Dim objCopyEmail As New Emails
        Dim objRematchEmail As New Emails

        Dim TrackCompanyID As Integer = Session("CompanyId")
        Dim TrackContactID As Integer = Session("UserId")
        Dim TrackContactClassID As Integer = Session("RoleGroupID")
        Dim WOStatusText As String = ""
        Dim bizDivID As Integer = 0
        Try
            bizDivID = Session("BizDivId")
            ds_Success = ws.WSWorkOrder.WO_DiscardDelCopy(ViewState("WOID"), bizDivID, CInt(ViewState("ClientCompanyId")), CInt(ViewState("ClientContactId")), "copy", TrackCompanyID, TrackContactID, TrackContactClassID, VerNum, Session("UserID"), Nothing)
            dvEmail = ds_Success.Tables("tblSendMail").Copy.DefaultView
            'Mail to the client & admin for wo copy
            objCopyEmail.WorkOrderID = ds_Success.Tables("tblWOID").Rows(0).Item("WorkOrderId")
            objCopyEmail.WOLoc = ds_Success.Tables("tblWOID").Rows(0).Item("City") & "," & ds_Success.Tables("tblWOID").Rows(0).Item("Postcode")
            objCopyEmail.WOTitle = ds_Success.Tables("tblWOID").Rows(0).Item("Title")
            objCopyEmail.WOPrice = ds_Success.Tables("tblWOID").Rows(0).Item("PlatformPrice")
            objCopyEmail.WOStartDate = ds_Success.Tables("tblWOID").Rows(0).Item("DateStart")
            objCopyEmail.WOEndDate = ds_Success.Tables("tblWOID").Rows(0).Item("DateEnd")
            objCopyEmail.WPPrice = ds_Success.Tables("tblWOID").Rows(0).Item("WholesalePrice")
            Emails.SendWorkOrderMail(objCopyEmail, ApplicationSettings.UserType.buyer, ApplicationSettings.WOAction.SubmitWO, "", dvEmail)
            ViewState("OriginalWOID") = ViewState("WOID")
            ViewState("WOID") = ds_Success.Tables("tblWOID").Rows(0).Item("WOID")
            dsEmailContacts = ws.WSWorkOrder.RematchWO(ViewState("WOID"), ViewState("OriginalWOID"), Session("CompanyID"), Session("UserID"), Mode)
            If (Mode = "Rematch") Then
                If (dsEmailContacts.Tables("tblStatus").Rows.Count <> 0) Then

                    If dsEmailContacts.Tables("tblStatus").Rows(0).Item("Status") = -1 Then
                        'DBUpdateFail
                        divValidationMain.Visible = True
                        lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
                    ElseIf dsEmailContacts.Tables("tblStatus").Rows(0).Item("Status") = 0 Then
                        'No suppliers to rematch
                        divValidationMain.Visible = True
                        lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
                    ElseIf dsEmailContacts.Tables("tblStatus").Rows(0).Item("Status") = 1 Then
                        'Send WO Notification Emails to Supplier Accounts (Administrator + All those that have Receive WO Notification = True)
                        If dsEmailContacts.Tables("tblSuppliers").Rows.Count <> 0 Then
                            If dsEmailContacts.Tables("tblWOSummary").Rows.Count <> 0 Then
                                'Add data to the Email object
                                objRematchEmail.WOCategory = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("WOCategory")
                                objRematchEmail.WOEndDate = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("DateEnd")
                                objRematchEmail.WOLoc = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("Location")
                                objRematchEmail.PPPrice = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("PlatformPrice")
                                objRematchEmail.WorkOrderID = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("RefWOID")
                                objRematchEmail.WOStartDate = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("DateStart")
                                objRematchEmail.WOTitle = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("WOTitle")
                                objRematchEmail.EstimatedTimeInDays = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("EstimatedTimeInDays")
                                objRematchEmail.WorkOrderDayRate = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("PlatformDayJobRate")
                                objRematchEmail.StagedWO = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("StagedWO")
                                objRematchEmail.WODate = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("SubmittedDate")
                                objRematchEmail.WODesc = dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("WOLongDesc")
                                If ApplicationSettings.SendIphoneNotification Then
                                    'Dim dSendNotification As [Delegate] = New AfterOrderMatch(AddressOf IPhoneNotification)
                                    'ThreadUtil.FireAndForget(dSendNotification, New Object() {dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("RematchCompanyID"), Convert.ToInt32(ViewState("WOID"))})
                                    IPhoneNotification(dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("RematchCompanyID"), Convert.ToInt32(ViewState("WOID")))
                                End If
                                Emails.SendOrderMatchMail(objRematchEmail, dsEmailContacts, bizDivID, dsEmailContacts.Tables("tblWOSummary").Rows(0).Item("PricingMethod"))
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            SendMail.SendMail("no-reply@orderwork.co.uk", ApplicationSettings.Error_Email_Contact1, "Error - " + ex.ToString, " Error in after cancel rematch")
        End Try
    End Sub

    Public Sub IPhoneNotification(ByVal SelectedIDs As String, ByVal WOID As Integer)
        Try
            Dim dsNotification As DataSet = ws.WSWorkOrder.IPhoneNotification(SelectedIDs, WOID)
            If dsNotification.Tables.Count > 0 Then
                If dsNotification.Tables("TokenDetails").Rows.Count > 0 Then
                    For i As Integer = 0 To dsNotification.Tables("TokenDetails").Rows.Count - 1
                        ' ws.WSiPhoneNotification.SendAlert(dsNotification.Tables("TokenDetails").Rows(i)("Token"), "New JOB -" & dsNotification.Tables("TokenDetails").Rows(i)("RefWOID") & " arrived", "default", 1)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

End Class