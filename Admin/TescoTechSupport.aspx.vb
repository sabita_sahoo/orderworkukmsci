

Partial Public Class TescoTechSupport
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then

            ddMonth.SelectedValue = Month(Date.Now)
            ddYear.SelectedValue = Year(Date.Now)
            tblTotal.Visible = False
            tblReportDateTime.Visible = False

        End If
        ' btnExport.HRef = "ExportToExcel.aspx?page=TescoTechSupport&Mode=Export&Month=" & ddMonth.SelectedValue & "&Year=" & ddYear.SelectedValue

    End Sub

    Private Sub lnkBtnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnGenerateReport.Click
        Dim ds As DataSet
        ds = ws.WSWorkOrder.MS_TescoReport(ddMonth.SelectedValue, ddYear.SelectedValue, "Report")
        ViewState.Add("TescoReport" & Session.SessionID, ds)
        If (ds.Tables("Locations").Rows.Count > 0) And (ds.Tables("TotalJobs").Rows.Count > 0) Then
            pnlNoRecords.Visible = False
            pnlListing.Visible = True
            dlTescoReport.DataSource = ds.Tables("Locations").DefaultView
            dlTescoReport.DataBind()
        Else
            pnlListing.Visible = False
            pnlNoRecords.Visible = True
        End If
    End Sub

    Public Sub dlTescoReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlTescoReport.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim dlJobsBreakUp As DataList = CType(e.Item.FindControl("dlJobsBreakUp"), DataList)

            Dim hdnGoodsLoc As HtmlInputHidden = CType(e.Item.FindControl("hdnGoodsLocation"), HtmlInputHidden)
            Dim ds As New DataSet
            ds = CType(ViewState("TescoReport" & Session.SessionID), DataSet)
            Dim dv As New DataView
            If ds.Tables.Count > 2 Then
                If ds.Tables("JobDetails").Rows.Count > 0 Then
                    dlJobsBreakUp.Visible = True
                    dv = ds.Tables("JobDetails").DefaultView
                    dv.RowFilter = "GoodsLocation = " & hdnGoodsLoc.Value

                    dlJobsBreakUp.DataSource = dv.ToTable.DefaultView

                    dlJobsBreakUp.DataBind()
                End If
                tblTotal.Visible = True
                If (ds.Tables("TotalJobs").Rows.Count > 0) Then
                    lblTotalJobs.Text = ds.Tables("TotalJobs").Rows(0).Item("TotalWOs")
                Else
                    lblTotalJobs.Text = 0
                End If
                tblReportDateTime.Visible = True
                Dim CurrHours As String
                Dim CurrMin As String

                CurrHours = Now.TimeOfDay.Hours
                CurrMin = Now.TimeOfDay.Minutes
                If CInt(CurrHours) < 10 Then
                    CurrHours = "0" & CurrHours
                Else
                    CurrHours = CurrHours
                End If

                If CInt(CurrMin) < 10 Then
                    CurrMin = "0" & CurrMin
                Else
                    CurrMin = CurrMin
                End If

                lblReportDateTime.Text = Date.Today.ToLongDateString.ToString & " - " & CurrHours & ":" & CurrMin
                TotalEngAvail.Text = ds.Tables("JobDetails").Compute("sum(EngrAvail)", "").ToString
                If (ds.Tables("TotalJobs").Rows.Count > 0) Then
                    TotalSuppliersUsed.Text = ds.Tables("TotalJobs").Rows(0).Item("cntDistinctSupplier").ToString
                End If
                If ds.Tables("JobDetails").Rows.Count > 0 Then
                    TotalJobsReceived.Text = ds.Tables("JobDetails").Compute("sum(JobsReceived)", "").ToString
                    TotalJobsInvoiced.Text = ds.Tables("JobDetails").Compute("sum(JobsInvoiced)", "").ToString
                    TotalJobsSLA.Text = ds.Tables("JobDetails").Compute("sum(JobsSLA)", "").ToString
                    TotalJobsActive.Text = ds.Tables("JobDetails").Compute("sum(JobsActive)", "").ToString
                End If


                If ds.Tables("JobDetails").Compute("sum(AverTime)", "") <> 0 Then
                    TotalAvgJobTime.Text = Strings.FormatNumber((ds.Tables("JobDetails").Compute("sum(AverTime)", "") / ds.Tables("JobDetails").Compute("sum(JobsInvoiced)", "")), 2, TriState.False, TriState.False, TriState.False)
                Else
                    TotalAvgJobTime.Text = "0"
                End If
                If ds.Tables("JobDetails").Compute("sum(JobsSLA)", "") <> 0 Then
                    TotalEngrOnTime.Text = Strings.FormatNumber(((ds.Tables("JobDetails").Compute("sum(JobsSLA)", "") / ds.Tables("JobDetails").Compute("sum(JobsInvoiced)", "")) * 100), 2, TriState.False, TriState.False, TriState.False) & " %"
                Else
                    TotalEngrOnTime.Text = "0.00 %"
                End If
            End If

        End If
    End Sub

    Private Sub lnkPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPrint.Click
        Dim strurl As String
        strurl = "PrintTescoReport.aspx?Month=" & ddMonth.SelectedValue & "&Year=" & ddYear.SelectedValue & "&Mode=Report"
        ResponseHelper.Redirect(strurl, "_blank", "")
    End Sub


End Class