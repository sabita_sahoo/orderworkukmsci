
Partial Public Class CompanyScorecard
    Inherits System.Web.UI.Page
    Protected WithEvents UCMenuMainAdmin As UCMenuMainAdmin
    Protected WithEvents UCMenuSubAdmin As UCMenuSubAdmin
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents lnkBackToListing As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents tdBackToListing As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ddMonth As System.Web.UI.HtmlControls.HtmlSelect
    Protected WithEvents ddYear As System.Web.UI.HtmlControls.HtmlSelect
    Protected WithEvents lnkExportReportExcel As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lnkExportReportPdf As System.Web.UI.WebControls.LinkButton



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
            Case ApplicationSettings.CountryUK
                ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
        End Select

        If Not IsPostBack Then
            ddMonth.SelectedIndex = ddMonth.Items.IndexOf(ddMonth.Items.FindByValue(CType(Now.Month, String)))
            ddYear.SelectedIndex = ddYear.Items.IndexOf(ddYear.Items.FindByValue(CType(Now.Year, String)))
        End If

    End Sub

    Private Sub lnkBtnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnGenerateReport.Click
        generateReport()
    End Sub

    Private Sub lnkExportReportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportReportExcel.Click

        Dim warnings As Microsoft.Reporting.WebForms.Warning()
        Dim streamids As String()
        Dim mimeType As String
        Dim encoding As String
        Dim fileType As String = "EXCEL"
        Dim extension As String = "xls"
        Dim fileName As String = "OWReport"
        Dim languageCode As String = ""
        Dim objParameter(6) As Microsoft.Reporting.WebForms.ReportParameter

        Dim dayinm As Integer
        dayinm = Date.DaysInMonth(CType(ddYear.Items(ddYear.SelectedIndex).Value, Integer), CType(ddMonth.Items(ddMonth.SelectedIndex).Value, Integer))

        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/CompanyScorecard"

        objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("SelMonth", ddMonth.Items(ddMonth.SelectedIndex).Value.ToString)
        objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("SelYear", ddYear.Items(ddYear.SelectedIndex).Value.ToString)
        objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("EndDate", dayinm.ToString)
        objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)
        objParameter(4) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "False")
        objParameter(5) = New Microsoft.Reporting.WebForms.ReportParameter("ReportType", "")

        'Language parameters
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                languageCode = "de-DE"
            Case ApplicationSettings.CountryUK
                languageCode = "en-GB"
        End Select
        objParameter(6) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)

        ReportViewerOW.ServerReport.SetParameters(objParameter)

        Dim bytes As Byte() = ReportViewerOW.ServerReport.Render(fileType, Nothing, mimeType, encoding, extension, streamids, warnings)
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=NewSalesRevenueReports." + extension)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub

    Private Sub lnkExportReportPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportReportPdf.Click

        Dim warnings As Microsoft.Reporting.WebForms.Warning()
        Dim streamids As String()
        Dim mimeType As String
        Dim encoding As String
        Dim fileType As String = "PDF"
        Dim extension As String = "pdf"
        Dim fileName As String = "OWReport"
        Dim languageCode As String = ""

        Dim objParameter(6) As Microsoft.Reporting.WebForms.ReportParameter

        Dim dayinm As Integer
        dayinm = Date.DaysInMonth(CType(ddYear.Items(ddYear.SelectedIndex).Value, Integer), CType(ddMonth.Items(ddMonth.SelectedIndex).Value, Integer))

        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/CompanyScorecard"
        objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("SelMonth", ddMonth.Items(ddMonth.SelectedIndex).Value.ToString)
        objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("SelYear", ddYear.Items(ddYear.SelectedIndex).Value.ToString)
        objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("EndDate", dayinm.ToString)
        objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)
        objParameter(4) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "False")
        objParameter(5) = New Microsoft.Reporting.WebForms.ReportParameter("ReportType", "")


        'Language parameters
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                languageCode = "de-DE"
            Case ApplicationSettings.CountryUK
                languageCode = "en-GB"
        End Select
        objParameter(6) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)
        ReportViewerOW.ServerReport.SetParameters(objParameter)

        Dim bytes As Byte() = ReportViewerOW.ServerReport.Render(fileType, Nothing, mimeType, encoding, extension, streamids, warnings)
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=NewSalesRevenueReports" + Date.Now + "." + extension)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub

    Private Sub generateReport()
        Dim languageCode As String = ""

        Dim dayinm As Integer
        dayinm = Date.DaysInMonth(CType(ddYear.Items(ddYear.SelectedIndex).Value, Integer), CType(ddMonth.Items(ddMonth.SelectedIndex).Value, Integer))

        'pnlExportReport.Visible = True
        ReportViewerOW.ShowParameterPrompts = False
        ReportViewerOW.ShowToolBar = True
        Dim reportSErverPath As Uri = New Uri(ApplicationSettings.ReportServerPath)
        ReportViewerOW.ServerReport.ReportServerUrl = reportSErverPath
        ReportViewerOW.ServerReport.ReportServerCredentials = New CustomReportCredentials.CustomReportCredentials(ApplicationSettings.ReportServerUserName, ApplicationSettings.ReportServerPassword, ApplicationSettings.ReportServerDomain)
        Dim objParameter(6) As Microsoft.Reporting.WebForms.ReportParameter

        ReportViewerOW.ServerReport.ReportPath = "/" & ApplicationSettings.ReportFolder & "/CompanyScorecard"

        objParameter(0) = New Microsoft.Reporting.WebForms.ReportParameter("SelMonth", ddMonth.Items(ddMonth.SelectedIndex).Value.ToString)
        objParameter(1) = New Microsoft.Reporting.WebForms.ReportParameter("SelYear", ddYear.Items(ddYear.SelectedIndex).Value.ToString)
        objParameter(2) = New Microsoft.Reporting.WebForms.ReportParameter("EndDate", dayinm.ToString)
        objParameter(3) = New Microsoft.Reporting.WebForms.ReportParameter("BizDivId", ViewState("BizDivId").ToString)
        objParameter(4) = New Microsoft.Reporting.WebForms.ReportParameter("HideHeader", "False")
        objParameter(5) = New Microsoft.Reporting.WebForms.ReportParameter("ReportType", "")


        'Language parameters
        Select Case ApplicationSettings.Country
            Case ApplicationSettings.CountryDE
                languageCode = "de-DE"
            Case ApplicationSettings.CountryUK
                languageCode = "en-GB"
        End Select
        objParameter(6) = New Microsoft.Reporting.WebForms.ReportParameter("Language", languageCode)
        ReportViewerOW.ServerReport.SetParameters(objParameter)
        ReportViewerOW.ServerReport.Refresh()
    End Sub
End Class
