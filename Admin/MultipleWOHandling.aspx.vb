
Imports System.Data
Imports System.Threading
Partial Public Class MultipleWOHandling
    Inherits System.Web.UI.Page
    Private Delegate Sub AfterWOProcess(ByVal ds As DataSet, ByVal woid As String)
    Shared ws As New WSObjs
    Public objEmail As New Emails

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack = True Then
            If Request.QueryString("mode") <> Nothing Then
                ViewState.Add("mode", Request.QueryString("mode"))
                ViewState.Add("WOID", Request.QueryString("WOID"))
            End If
            SetPageSettings()
            If ViewState("mode") = ApplicationSettings.WOGroupSubmitted Then
                PopulateContact()
            End If
            GetWOListing()
        End If
    End Sub
    ''' <summary>
    ''' Populate Valid suppliers in case of Submitted listing
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub PopulateContact()
        Dim ds As New DataSet
        ds = getContacts()
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                drpdwnContacts.DataSource = ds
                drpdwnContacts.DataTextField = "AttributeValue"
                drpdwnContacts.DataValueField = "ContactID"
                drpdwnContacts.DataBind()
            Else
                drpdwnContacts.Visible = False
                Label1.Text = "There are no Approved supplier who have selected Accept Multiple WO"
                btnNext.Visible = False
            End If
        Else
            drpdwnContacts.Visible = False
            Label1.Text = "There are no Approved supplier who have selected Accept Multiple WO"
            btnNext.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' Set the page settings i.e. accord to mode what is to be shown and whta is to be hidden
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub SetPageSettings()
        Select Case ViewState("mode")
            Case ApplicationSettings.WOGroupSubmitted
                pnlGridListings.Visible = True
                pnlRating.Visible = False
                pnlComments.Visible = False
                pnlServicePartnerList.Visible = True
                pnlConfirm.Visible = False
                divInvalidWOIDs.Visible = True
                lblInValidWO.Text = ResourceMessageText.GetString("InvalidWOList")
                divValidWOIDs.Visible = True
                lblValidWO.Text = ResourceMessageText.GetString("ValidWOList")
            Case ApplicationSettings.WOGroupIssue
                pnlGridListings.Visible = True
                pnlRating.Visible = False
                pnlServicePartnerList.Visible = False
                lblInValidWO.Text = ResourceMessageText.GetString("InvalidWOList")
                divValidWOIDs.Visible = True
                lblValidWO.Text = ResourceMessageText.GetString("MultipleAcceptIssueList")
                lblComments.Text = ResourceMessageText.GetString("CommentAcceptIssueWO")
                pnlComments.Visible = False
                pnlConfirm.Visible = True
                btnNext.Visible = False
                btnBack.Visible = False
                divInvalidWOIDs.Visible = False
                btnMassAcceptanceCanel.Visible = True
                btnCancel.Visible = False
                btnBack.Text = "Back to listing"
            Case ApplicationSettings.WOGroupAccepted
                pnlGridListings.Visible = True
                pnlRating.Visible = True
                pnlComments.Visible = True
                pnlServicePartnerList.Visible = False
                pnlConfirm.Visible = False
                divInvalidWOIDs.Visible = False
                divValidWOIDs.Visible = True
                lblValidWO.Text = ResourceMessageText.GetString("ValidWOList")
                lblComments.Text = ResourceMessageText.GetString("CommentCompleteWO")
            Case "Activeelapsed"
                pnlGridListings.Visible = True
                pnlRating.Visible = True
                pnlComments.Visible = True
                pnlServicePartnerList.Visible = False
                pnlConfirm.Visible = False
                divInvalidWOIDs.Visible = False
                divValidWOIDs.Visible = True
                lblValidWO.Text = ResourceMessageText.GetString("ValidWOList")
                lblComments.Text = ResourceMessageText.GetString("CommentCompleteWO")
            Case ApplicationSettings.WOGroupCompleted
                pnlGridListings.Visible = True
                pnlRating.Visible = True
                pnlComments.Visible = True
                pnlServicePartnerList.Visible = False
                pnlConfirm.Visible = False
                divInvalidWOIDs.Visible = False
                divValidWOIDs.Visible = True
                lblValidWO.Text = ResourceMessageText.GetString("ValidWOList")
                lblComments.Text = ResourceMessageText.GetString("CommentCloseWO")
            Case Else
                pnlGridListings.Visible = False
                pnlRating.Visible = False
                pnlServicePartnerList.Visible = False
                pnlConfirm.Visible = False

        End Select
    End Sub
    ''' <summary>
    ''' Get the listing of suppliers for ordermatching
    ''' </summary>
    ''' <param name="killCache"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getContacts(Optional ByVal killCache As Boolean = True) As DataSet
        Dim BizDivID As Integer
        Dim cacheKey As String = ""
        cacheKey = "MultipleAcceptServicePartners"
        BizDivID = Session("BizDivId")
        If killCache = True Then
            Page.Cache.Remove(cacheKey)
        End If
        Dim ds As DataSet
        If Page.Cache(cacheKey) Is Nothing Then
            ds = ws.WSContact.GetContactAcceptMultipleWO(BizDivID)
            Page.Cache.Add(cacheKey, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(Page.Cache(cacheKey), DataSet)
        End If
        Return ds
    End Function
    ''' <summary>
    ''' Show confirmation panel and hide next and back button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        pnlRating.Visible = False
        pnlComments.Visible = False
        pnlServicePartnerList.Visible = False
        pnlGridListings.Visible = True
        pnlConfirm.Visible = True
        btnNext.Visible = False
        btnBack.Visible = False
        divInvalidWOIDs.Visible = False
        Select Case ViewState("mode")
            Case ApplicationSettings.WOGroupCompleted
                btnBackConfirm.Visible = True
            Case Else
                btnBackConfirm.Visible = False
        End Select
    End Sub
    ''' <summary>
    ''' Should redirect to the previous listing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        'Dim lnk As String
        'If ViewState("mode") = ApplicationSettings.WOGroupIssue Then
        '    lnk = "AdminWOIssueListing.aspx?mode=" & ViewState("mode") & "&from=multiplewo"
        'Else
        '    lnk = "AdminWOListing.aspx?mode=" & ViewState("mode")
        'End If
        Dim link As String = ""
        If ViewState("mode") = ApplicationSettings.WOGroupIssue Then
            link = "AdminWOIssueListing.aspx?mode=" & ViewState("mode") & "&from=multiplewo"
        Else
            link = "AdminWOListing.aspx?"
            link &= "&Group=" & ViewState("mode")
            link &= "&mode=" & ViewState("mode")
            link &= "&BizDivID=" & Request("BizDivID")
            link &= "&FromDate=" & Request("FromDate")
            link &= "&ToDate=" & Request("ToDate")
            If Not IsNothing(Request("SearchWorkorderID")) Then
                link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
            End If

            link &= "&sender=" & "multiplewo"
        End If
        Response.Redirect(link)
    End Sub
    ''' <summary>
    ''' On click of Cancel it should be again goes into the same position as in before clicking on next
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        SetPageSettings()
        btnNext.Visible = True
        btnBack.Visible = True
    End Sub
    ''' <summary>
    ''' Updating the Status on clicking of confirm
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Dim Rating As String = "0"
        Dim WOID As String
        Dim Success As Integer = 0
        Dim ds As New DataSet
        Dim RatingClassID As Integer
        WOID = ViewState("ValidWOID")
        If rdoRatingNeutral.Checked = True Then
            Rating = 0
        ElseIf rdoRatingNegative.Checked = True Then
            Rating = -1
        ElseIf rdoRatingPositive.Checked = True Then
            Rating = 1
        End If
        If ViewState("mode") = ApplicationSettings.WOGroupCompleted Then
            RatingClassID = ApplicationSettings.RoleSupplierID
        ElseIf ViewState("mode") = ApplicationSettings.WOGroupAccepted Or ViewState("mode") = "Activeelapsed" Then
            RatingClassID = ApplicationSettings.RoleClientID
        End If
        If ViewState("mode") = ApplicationSettings.WOGroupSubmitted Then
            ds = ws.WSWorkOrder.UpdateMultipleWOStatus(WOID, ViewState("mode").ToString, drpdwnContacts.SelectedItem.Value, Session("CompanyID"), Session("UserID"), Session("BizDivId"), Rating, txtComments.Text.Trim, RatingClassID)
        ElseIf ViewState("mode") = ApplicationSettings.WOGroupIssue Then
            ds = ws.WSWorkOrder.AcceptMultipleIssueWO(WOID, Session("CompanyID"), Session("UserID"), txtComments.Text.Trim, 2)
            divInvalidWOIDs.Visible = False
        Else
            ds = ws.WSWorkOrder.UpdateMultipleWOStatus(WOID, ViewState("mode").ToString, 0, Session("CompanyID"), Session("UserID"), Session("BizDivId"), Rating, txtComments.Text.Trim, RatingClassID)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)("Success") = 1 Then
                If ds.Tables(1).Rows.Count > 0 Then

                    'OM-114 : Get PONumber from spAdmin_UpdateMultipleWOs.Check if company is ITEC HSO and its PO number is tbc.if yes then send an email sw admin on dev
                    For Each row As DataRow In ds.Tables(1).Rows
                        Dim PONumber As String = row("PONumber")
                        If (row("ClientCompanyId") = OrderWorkLibrary.ApplicationSettings.ITECHSOCompany And (PONumber = "tbc" Or PONumber = "TBC")) Then
                            OrderWorkLibrary.Emails.SendITECHSOJobAcceptEmail(row("WorkOrderID"), row("DateStart"), row("AptTime"))
                        End If
                    Next row
                    

                    ds.Tables(1).TableName = "WODetails"
                    Success = Success + 1
                End If
                If ds.Tables.Count > 3 Then
                    If ds.Tables(3).Rows.Count > 0 Then
                        ds.Tables(3).TableName = "LostMail"
                        Success = Success + 1
                    End If
                End If
                If ds.Tables(2).Rows.Count > 0 Then
                    ds.Tables(2).TableName = "SendMail"
                    Success = Success + 1
                End If
                If Success > 1 Then
                    If ViewState("mode") <> ApplicationSettings.WOGroupAccepted Then
                        If ViewState("mode") <> "Activeelapsed" Then
                            If ViewState("mode") = ApplicationSettings.WOGroupCompleted Then
                               CreateAndSendUpsellInvoice(WOID, ds)
                            End If
                            Dim dSave As [Delegate] = New AfterWOProcess(AddressOf sendMailer)
                            ThreadUtil.FireAndForget(dSave, New Object() {ds, WOID})
                        End If
                    End If
                End If
            End If
        End If
        Dim lnk As String
        If ViewState("mode") = ApplicationSettings.WOGroupIssue Then
            pnlConfirm.Visible = False
            divInvalidWOIDs.Visible = True
            If ds.Tables.Count > 3 Then
                lblInValidWO.Text = "List of WorkOrders cannot be Updated."
                If ds.Tables(3).Rows.Count > 0 Then
                    dlInvalidWO.DataSource = ds.Tables(3)
                    dlInvalidWO.DataBind()
                    lblNoInvalidRecords.Visible = False
                Else
                    divInvalidWOIDs.Visible = False
                End If
            End If
            If ds.Tables.Count > 4 Then
                lblValidWO.Text = ResourceMessageText.GetString("MultipleAcceptIssueSuccess")
                dlValidWO.Visible = False
            End If
            btnNext.Visible = False
            btnBack.Visible = True
        Else
            If ViewState("mode") = ApplicationSettings.WOGroupSubmitted Then
                lnk = "AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupAccepted
            ElseIf ViewState("mode") = ApplicationSettings.WOGroupAccepted Then
                lnk = "AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupCompleted
            ElseIf ViewState("mode") = "Activeelapsed" Then
                lnk = "AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupCompleted
            ElseIf ViewState("mode") = ApplicationSettings.WOGroupCompleted Then
                lnk = "AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupClosed
            Else
                lnk = "AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupSubmitted
            End If
            Response.Redirect(lnk)
        End If

    End Sub



    ''' <summary>
    ''' Getting valid and Invalid Work Orders in case of submitted and 
    ''' in case of others only displaying Valid Work Orders
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetWOListing()
        Dim ds As DataSet
        Dim dvWOID As DataView
        ds = ws.WSWorkOrder.GetValidInvaildWOList(ViewState("WOID").ToString, ViewState("mode").ToString)
        dvWOID = ds.Tables(0).DefaultView
        Dim csvWOID As New StringBuilder
        For Each dRow As DataRowView In dvWOID
            If csvWOID.ToString = "" Then
                csvWOID.Append(dRow("WOID"))
            Else
                csvWOID.Append("," & dRow("WOID"))
            End If
        Next
        ViewState("ValidWOID") = csvWOID.ToString
        Select Case ViewState("mode")
            Case ApplicationSettings.WOGroupSubmitted
                If ds.Tables(0).Rows.Count <> 0 Then
                    dlValidWO.DataSource = ds.Tables(0)
                    dlValidWO.DataBind()
                Else
                    lblNovalidRecords.Visible = True
                    lblNovalidRecords.Text = ResourceMessageText.GetString("NoWoListing")
                    btnNext.Visible = False
                End If
                If ds.Tables(1).Rows.Count <> 0 Then
                    dlInvalidWO.DataSource = ds.Tables(1)
                    dlInvalidWO.DataBind()
                Else
                    lblNoInvalidRecords.Visible = True
                    lblNoInvalidRecords.Text = ResourceMessageText.GetString("NoWoListing")
                End If
            Case ApplicationSettings.WOGroupAccepted
                If ds.Tables(0).Rows.Count <> 0 Then
                    dlValidWO.DataSource = ds.Tables(0)
                    dlValidWO.DataBind()
                Else
                    lblNoInvalidRecords.Visible = True
                    lblNoInvalidRecords.Text = ResourceMessageText.GetString("NoWoListing")
                    btnNext.Visible = False
                End If
                If ds.Tables(1).Rows.Count <> 0 Then
                    divInvalidWOIDs.Visible = True
                    lblInValidWO.Text = "List of WorkOrders cannot be Updated."
                    dlInvalidWO.DataSource = ds.Tables(1)
                    dlInvalidWO.DataBind()
                End If
            Case "Activeelapsed"
                If ds.Tables(0).Rows.Count <> 0 Then
                    dlValidWO.DataSource = ds.Tables(0)
                    dlValidWO.DataBind()
                Else
                    lblNoInvalidRecords.Visible = True
                    lblNoInvalidRecords.Text = ResourceMessageText.GetString("NoWoListing")
                    btnNext.Visible = False
                End If
                If ds.Tables(1).Rows.Count <> 0 Then
                    divInvalidWOIDs.Visible = True
                    lblInValidWO.Text = "List of WorkOrders cannot be Updated."
                    dlInvalidWO.DataSource = ds.Tables(1)
                    dlInvalidWO.DataBind()
                End If
            Case ApplicationSettings.WOGroupCompleted
                If ds.Tables(0).Rows.Count <> 0 Then
                    dlValidWO.DataSource = ds.Tables(0)
                    dlValidWO.DataBind()
                Else
                    lblNoInvalidRecords.Visible = True
                    lblNoInvalidRecords.Text = ResourceMessageText.GetString("NoWoListing")
                    btnNext.Visible = False
                End If
            Case ApplicationSettings.WOGroupIssue
                If ds.Tables(0).Rows.Count <> 0 Then
                    dlValidWO.DataSource = ds.Tables(0)
                    dlValidWO.DataBind()
                Else
                    lblNovalidRecords.Visible = True
                    lblNovalidRecords.Text = ResourceMessageText.GetString("NoWoListing")
                    btnNext.Visible = False
                End If
                If ds.Tables(1).Rows.Count <> 0 Then
                    dlInvalidWO.DataSource = ds.Tables(1)
                    dlInvalidWO.DataBind()
                Else

                    lblNoInvalidRecords.Visible = True
                    lblNoInvalidRecords.Text = ResourceMessageText.GetString("NoWoListing")
                End If
            Case Else

        End Select
    End Sub



    ''' <summary>
    ''' Sends mail to the respective contactid
    ''' </summary>
    ''' <param name="ds_WOSuccess"></param>
    ''' <param name="ContactId"></param>
    ''' <remarks></remarks>
    Public Sub sendMailer(ByVal ds_WOSuccess As DataSet, ByVal woid As String)
        Try
            'send mailer to respective party
            Dim MailerViewer As String
            Dim status As String
            Dim BizDivID As Integer
            Dim ContactId As Integer
            Dim Viewer As String
            Dim dvEmail As New DataView
            Dim dvWODetails As New DataView
            '' Required to filter dvEmail by WOID id also
            'If ViewState("mode") = ApplicationSettings.WOGroupCompleted Then
            '    Dim ds As New DataSet
            '    ds = ws.WSWorkOrder.AddMultipleUpSellInvoice(woid, Session("BizDivId"))
            'End If
            Dim addRowFilter As String = ""
            ContactId = Session("UserID")
            Dim i As Integer
            BizDivID = Session("BizDivId")
            Viewer = ApplicationSettings.ViewerAdmin
            If ds_WOSuccess.Tables("SendMail").Rows.Count > 0 Then
                dvEmail = ds_WOSuccess.Tables("SendMail").DefaultView
            End If
            If ds_WOSuccess.Tables("WODetails").Rows.Count > 0 Then
                dvWODetails = ds_WOSuccess.Tables("WODetails").DefaultView
            End If
            Select Case ViewState("mode")
                Case ApplicationSettings.WOGroupAccepted
                    status = ApplicationSettings.WOAction.WOComplete
                Case "Activeelapsed"
                    status = ApplicationSettings.WOAction.WOComplete
                Case ApplicationSettings.WOGroupCompleted
                    status = ApplicationSettings.WOAction.WOClose
                Case ApplicationSettings.WOGroupSubmitted
                    status = ApplicationSettings.WOAction.WOSPAccept


            End Select

            If dvWODetails.Count > 0 Then
                For i = 0 To dvWODetails.Count - 1
                    With dvWODetails(i)
                        objEmail.WorkOrderID = .Item("WorkOrderID")
                        objEmail.WOLoc = .Item("Location")
                        objEmail.WOTitle = .Item("WOTitle")
                        objEmail.WOCategory = .Item("WOMainCategory") & .Item("WOSubCategory")
                        objEmail.WPPrice = .Item("WholesalePrice")
                        objEmail.PPPrice = .Item("PlatformPrice")
                        objEmail.WOStartDate = Strings.FormatDateTime(.Item("DateStart"), DateFormat.ShortDate)
                        objEmail.WOEndDate = Strings.FormatDateTime(.Item("DateEnd"), DateFormat.ShortDate)
                        objEmail.ThermostatsQuestions = .Item("ThermostatsQuestions")
                        'status = .Item("Status")
                        If ApplicationSettings.Country <> ApplicationSettings.CountryDE Then
                            If Not IsDBNull(.Item("WholesaleDayJobRate")) Then
                                objEmail.WorkOrderWholesaleDayRate = .Item("WholesaleDayJobRate")
                                ViewState("WorkOrderWholesaleDayRate") = .Item("WholesaleDayJobRate")
                            End If
                            If Not IsDBNull(.Item("EstimatedTimeInDays")) Then
                                objEmail.EstimatedTimeInDays = .Item("EstimatedTimeInDays")
                            End If

                            If Not IsDBNull(.Item("PlatformDayJobRate")) Then
                                objEmail.WorkOrderDayRate = .Item("PlatformDayJobRate")
                            End If
                            If IsDBNull(.Item("StagedWO")) Then
                                ViewState("StagedWO") = "No"
                            Else
                                ViewState("StagedWO") = .Item("StagedWO")
                            End If

                            If Not IsDBNull(.Item("PricingMethod")) Then
                                ViewState("PricingMethod") = .Item("PricingMethod")
                            End If
                        End If
                        If Not IsNothing(ViewState("WorkOrderWholesaleDayRate")) Then
                            objEmail.WorkOrderWholesaleDayRate = ViewState("WorkOrderWholesaleDayRate") 'For Buyer
                        End If
                        objEmail.StagedWO = ViewState("StagedWO")


                        addRowFilter = " And WOID in (" & .Item("WOID") & ")"

                        Select Case ViewState("mode")
                            Case ApplicationSettings.WOGroupAccepted
                                Emails.SendWorkOrderMail(objEmail, MailerViewer, status, txtComments.Text.Trim, dvEmail, 0, addRowFilter)
                            Case "Activeelapsed"
                                Emails.SendWorkOrderMail(objEmail, MailerViewer, status, txtComments.Text.Trim, dvEmail, 0, addRowFilter)
                            Case ApplicationSettings.WOGroupCompleted
                                Emails.SendWorkOrderMail(objEmail, MailerViewer, status, txtComments.Text.Trim, dvEmail, 0, addRowFilter)
                            Case ApplicationSettings.WOGroupSubmitted
                                Emails.SendWorkOrderMail(objEmail, MailerViewer, status, txtComments.Text.Trim, dvEmail, 6, addRowFilter)
                                ' Send mails to contacts who lost the work orders
                                If ds_WOSuccess.Tables.Count > 3 Then
                                    Dim dvLost As New DataView
                                    If ds_WOSuccess.Tables(3).Rows.Count > 0 Then
                                        ' Get a reference of the LostMail table
                                        dvLost = ds_WOSuccess.Tables("LostMail").Copy.DefaultView
                                        ' Declare dataview for getting reference of the lost mail table for CC
                                        Dim dv_LostToCC As DataView
                                        dv_LostToCC = ds_WOSuccess.Tables("LostMail").Copy.DefaultView
                                        ' Filter the dataview for lost to fetch the primary supplier for the WOID as to field and then to iterate to send mail to them
                                        ' and also to thier CC contacts
                                        dvLost.RowFilter = "WOID in (" & .Item("WOID") & ") and Recipient = 'To'"
                                        For Each drow_LostMailer As DataRowView In dvLost
                                            ' Filter respective company CC contacts
                                            dv_LostToCC.RowFilter = "Recipient = 'CC' AND CompanyID = " & drow_LostMailer.Item("CompanyID")
                                            ' Sent both main supplier mail and also its company's CC contacts
                                            Emails.SendLostWOMail(.Item("WorkOrderID"), drow_LostMailer, dv_LostToCC)
                                        Next
                                    End If
                                End If
                            Case ApplicationSettings.WOGroupIssue
                                Emails.SendWorkOrderMail(objEmail, Viewer, ApplicationSettings.WOAction.WOAcceptIssue, txtComments.Text.Trim, dvEmail, 0, addRowFilter)
                        End Select
                    End With

                    'Dim dv_CCBuyer As New DataView
                    'Dim dv_CCSupplier As New DataView
                Next i
            End If
            'SendMail.SendMail(ApplicationSettings.EmailInfo, "sumikale@gmail.com", "Success", "Success in Multiple WO Handling Back ground process: at " & Now())
        Catch ex As Exception
            Dim exceptionstr As String
            exceptionstr = ApplicationSettings.WebPath & "<p> BackGround Process in Multiple WO Handling sending mail. Previous URL : " & Context.Request.UrlReferrer.AbsoluteUri & "Present Raw URL : " & Context.Request.RawUrl & "</p>"
            SendMail.SendMail(ApplicationSettings.EmailInfo, ApplicationSettings.Error_Email_Contact1, exceptionstr & ex.ToString, "Error in Multiple WO Handling Back ground process: at " & Now())
        End Try
    End Sub


    Private Sub btnMassAcceptanceCanel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMassAcceptanceCanel.Click
        Dim lnk As String
        If ViewState("mode") = ApplicationSettings.WOGroupIssue Then
            lnk = "AdminWOIssueListing.aspx?mode=" & ViewState("mode")
        Else
            lnk = "AdminWOListing.aspx?mode=" & ViewState("mode")
        End If
        Response.Redirect(lnk)
    End Sub
    Public Sub CreateAndSendUpsellInvoice(ByVal WOID As String, ByVal ds As DataSet)
        Dim dsUpsell As New DataSet
        dsUpsell = ws.WSWorkOrder.AddMultipleUpSellInvoice(WOID, Session("BizDivId"))

        Dim dvUpsell As New DataView
        Dim dvWODetails As New DataView
        Dim i As Integer
        If ds.Tables("WODetails").Rows.Count > 0 Then
            dvWODetails = ds.Tables("WODetails").DefaultView
        End If
        If dsUpsell.Tables(0).Rows.Count > 0 Then
            dvUpsell = dsUpsell.Tables(0).DefaultView
        End If
        If dvWODetails.Count > 0 Then
            For i = 0 To dvWODetails.Count - 1
                With dvWODetails(i)
                    If (ViewState("mode") = ApplicationSettings.WOGroupCompleted) Then

                        dvUpsell.RowFilter = "WOID = " & dvWODetails.Item(i)("WOID")
                        If dvUpsell.Count > 0 Then
                            If (CInt(dvWODetails.Item(i)("upsellprice")) > 0 And dvUpsell.Item(0)("UpSellSalesInvoice").ToString <> "" And dvWODetails.Item(i)("CustomerEmail").ToString <> "") Then
                                'First Send Upsell invoices
                                'Create PDF and send email
                                Dim FileName As String = CommonFunctions.CreatePDFUsingAspxURL(dvUpsell.Item(0)("UpSellSalesInvoice").ToString, "UpsellInvoices", "ViewUpSellSalesInvoice.aspx?BizDivId=" & ApplicationSettings.OWUKBizDivId & "&InvoiceNo=" & dvUpsell.Item(0)("UpSellSalesInvoice").ToString, HttpContext.Current.Server.MapPath("~/Attachments/"))
                                Dim Attachment As System.Net.Mail.Attachment
                                Attachment = New System.Net.Mail.Attachment(FileName)
                                Emails.SendUpsellInvoices("Orderwork - Receipt details for your installation service and/or materials purchased - " & dvWODetails.Item(i)("WorkOrderID").ToString, dvWODetails.Item(i)("CustomerEmail").ToString, Attachment, dvWODetails.Item(i)("ClientCompanyId"))
                            End If
                        End If
                        dvUpsell.RowFilter = ""
                    End If
                End With
            Next
        End If
    End Sub

    Private Sub btnBackConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackConfirm.Click
        Dim Rating As String = "0"
        Dim WOID As String
        Dim Success As Integer = 0
        Dim ds As New DataSet
        Dim RatingClassID As Integer
        WOID = ViewState("ValidWOID")
        If rdoRatingNeutral.Checked = True Then
            Rating = 0
        ElseIf rdoRatingNegative.Checked = True Then
            Rating = -1
        ElseIf rdoRatingPositive.Checked = True Then
            Rating = 1
        End If
        If ViewState("mode") = ApplicationSettings.WOGroupCompleted Then
            RatingClassID = ApplicationSettings.RoleSupplierID
        ElseIf ViewState("mode") = ApplicationSettings.WOGroupAccepted Or ViewState("mode") = "Activeelapsed" Then
            RatingClassID = ApplicationSettings.RoleClientID
        End If
        If ViewState("mode") = ApplicationSettings.WOGroupSubmitted Then
            ds = ws.WSWorkOrder.UpdateMultipleWOStatus(WOID, ViewState("mode").ToString, drpdwnContacts.SelectedItem.Value, Session("CompanyID"), Session("UserID"), Session("BizDivId"), Rating, txtComments.Text.Trim, RatingClassID)
        ElseIf ViewState("mode") = ApplicationSettings.WOGroupIssue Then
            ds = ws.WSWorkOrder.AcceptMultipleIssueWO(WOID, Session("CompanyID"), Session("UserID"), txtComments.Text.Trim, 2)
            divInvalidWOIDs.Visible = False
        Else
            ds = ws.WSWorkOrder.UpdateMultipleWOStatus(WOID, ViewState("mode").ToString, 0, Session("CompanyID"), Session("UserID"), Session("BizDivId"), Rating, txtComments.Text.Trim, RatingClassID)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)("Success") = 1 Then
                If ds.Tables(1).Rows.Count > 0 Then
                    ds.Tables(1).TableName = "WODetails"
                    Success = Success + 1
                End If
                If ds.Tables.Count > 3 Then
                    If ds.Tables(3).Rows.Count > 0 Then
                        ds.Tables(3).TableName = "LostMail"
                        Success = Success + 1
                    End If
                End If
                If ds.Tables(2).Rows.Count > 0 Then
                    ds.Tables(2).TableName = "SendMail"
                    Success = Success + 1
                End If
                If Success > 1 Then
                    If ViewState("mode") <> ApplicationSettings.WOGroupAccepted Then
                        If ViewState("mode") <> "Activeelapsed" Then

                            If ViewState("mode") = ApplicationSettings.WOGroupCompleted Then
                                CreateAndSendUpsellInvoice(WOID, ds)
                            End If

                            Dim dSave As [Delegate] = New AfterWOProcess(AddressOf sendMailer)
                            ThreadUtil.FireAndForget(dSave, New Object() {ds, WOID})
                        End If
                    End If
                End If
            End If
        End If
        Dim lnk As String
        If ViewState("mode") = ApplicationSettings.WOGroupIssue Then
            pnlConfirm.Visible = False
            divInvalidWOIDs.Visible = True
            If ds.Tables.Count > 3 Then
                lblInValidWO.Text = "List of WorkOrders cannot be Updated."
                If ds.Tables(3).Rows.Count > 0 Then
                    dlInvalidWO.DataSource = ds.Tables(3)
                    dlInvalidWO.DataBind()
                    lblNoInvalidRecords.Visible = False
                Else
                    divInvalidWOIDs.Visible = False
                End If
            End If
            If ds.Tables.Count > 4 Then
                lblValidWO.Text = ResourceMessageText.GetString("MultipleAcceptIssueSuccess")
                dlValidWO.Visible = False
            End If
            btnNext.Visible = False
            btnBack.Visible = True
        Else
            If ViewState("mode") = ApplicationSettings.WOGroupSubmitted Then
                lnk = "AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupAccepted
            ElseIf ViewState("mode") = ApplicationSettings.WOGroupAccepted Then
                lnk = "AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupCompleted
            ElseIf ViewState("mode") = "Activeelapsed" Then
                lnk = "AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupCompleted
            ElseIf ViewState("mode") = ApplicationSettings.WOGroupCompleted Then
                lnk = "AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupCompleted
            Else
                lnk = "AdminWOListing.aspx?mode=" & ApplicationSettings.WOGroupSubmitted
            End If
            Response.Redirect(lnk)
        End If
    End Sub
End Class