
Partial Public Class SearchFinance
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected WithEvents ImgBtnPay As Global.System.Web.UI.WebControls.ImageButton
    Protected WithEvents ImgBtnPIAv As Global.System.Web.UI.WebControls.ImageButton
    Protected WithEvents ImgBtnCNPay As Global.System.Web.UI.WebControls.ImageButton

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            gvFinance.PageSize = ApplicationSettings.GridViewPageSize
            If Not IsNothing(Request("sender")) Then
                If Request("sender") = "CashReceiptsCreditNotes" Or Request("sender") = "RelatedWorkOrders" Or Request("sender") = "WorkOrderDetails" Or Request("sender") = "RefundBuyer" Then
                    processBackToListing()
                Else
                    InitializeForm()
                End If
            Else
                InitializeForm()
            End If
            ''Added code for default showing of fields
            'If Request("sender") = "CashReceiptsCreditNotes" Or Request("sender") = "RelatedWorkOrders" Or Request("sender") = "WorkOrderDetails" Or Request("sender") = "RefundBuyer" Then
            '    If (Request("SrcInvoiceNo") <> "") Or (Request("SrcWorkOrderId") <> "") Or (Request("SrcKeyword") <> "") Or (Request("SrcPONumber") <> "") Or (Request("SrcCompanyName") <> "") Then
            '        divForAdvanceSearch.Visible = True
            '        lnkAdvanceSearch.Visible = False
            '    Else
            '        divForAdvanceSearch.Visible = False
            '        lnkAdvanceSearch.Visible = True
            '    End If
            'Else
            '    divForAdvanceSearch.Visible = False
            '    lnkAdvanceSearch.Visible = True
            'End If
            divEarlyDiscValidationGrid.Visible = False
            divValidationMain.Visible = False
        End If

    End Sub

    Public Sub InitializeForm(Optional ByVal resetFields As Boolean = True)

        'Initialize the Bizdivid with OrderWrok ID
        If IsNothing(ViewState("BizDivId")) Then
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryUK
                    ViewState("BizDivId") = ApplicationSettings.OWUKBizDivId
                Case ApplicationSettings.CountryDE
                    ViewState("BizDivId") = ApplicationSettings.OWDEBizDivId
            End Select
        End If

        If resetFields Then
            txtWorkorderID.Text = ""
            txtCompanyName.Text = ""
            txtKeyword.Text = ""
            txtPONumber.Text = ""
            txtInvoiceNo.Text = ""
            txtInvoiceDate.Text = ""
            ViewState!SortExpression = "InvoiceNumber"
            gvFinance.Sort(ViewState!SortExpression, SortDirection.Ascending)
        End If
    End Sub
    ''' <summary>
    ''' Show Search Criteria
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function PrepareSearchCriteria() As String
        lblSearchCriteria.Text = "<strong>Search Finance Criteria:</strong>"
        'keyword
        If txtKeyword.Text <> "" Then
            lblSearchCriteria.Text &= " Keyword - <span class='txtOrange'>" & txtKeyword.Text & "</span>"
        End If

        'Invoice No
        If txtInvoiceNo.Text <> "" Then
            lblSearchCriteria.Text &= " Invoice Number - <span class='txtOrange'>" & txtInvoiceNo.Text & "</span>"
        End If


        'WorkOrderID
        If txtWorkorderID.Text <> "" Then
            lblSearchCriteria.Text &= " Work Order ID - <span class='txtOrange'>" & txtWorkorderID.Text & "</span>"
        End If

        'PO Number
        If txtPONumber.Text <> "" Then
            lblSearchCriteria.Text &= " PO Number - <span class='txtOrange'>" & txtPONumber.Text & "</span>"
        End If

        'company name
        If txtCompanyName.Text <> "" Then
            lblSearchCriteria.Text &= " Company Name - <span class='txtOrange'>" & txtCompanyName.Text & "</span>"
        End If

        If txtInvoiceDate.Text <> "" Then
            lblSearchCriteria.Text &= " Invoice Date - <span class='txtOrange'>" & txtInvoiceDate.Text & "</span>"
        End If
        Return lblSearchCriteria.Text

    End Function
    ''' <summary>
    ''' Populate Grid
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvFinance.DataBind()
        End If
    End Sub
    ''' <summary>
    ''' Select From DB getting data set
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns>DataSet containing Search Results</returns>
    ''' <remarks></remarks>
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim rowCount As Integer
        Dim BizDivId As Integer = HttpContext.Current.Items("BizDivId")
        Dim Keyword As String = HttpContext.Current.Items("Keyword")
        Dim PONumber As String = HttpContext.Current.Items("PONumber")
        Dim InvoiceNo As String = HttpContext.Current.Items("InvoiceNo")
        Dim WorkOrderId As String = HttpContext.Current.Items("WorkOrderId")
        Dim CompanyName As String = HttpContext.Current.Items("CompanyName")
        Dim InvoiceDate As String = HttpContext.Current.Items("InvoiceDate")
        Dim ds As DataSet
        ds = ws.WSFinance.AdminSearchFinance(InvoiceNo, PONumber, WorkOrderId, Keyword, CompanyName, InvoiceDate, sortExpression, startRowIndex, maximumRows, rowCount)
        HttpContext.Current.Items("rowCount") = rowCount
        ViewState("totalRecords") = rowCount
        Return ds
    End Function
    ''' <summary>
    ''' Select Count Method
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function
    ''' <summary>
    ''' For Paging Mechanism
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        gvFinance.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub
    ''' <summary>
    ''' Pre Render Grid View
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub gvFinance_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvFinance.PreRender
        Me.gvFinance.Controls(0).Controls(Me.gvFinance.Controls(0).Controls.Count - 1).Visible = True
        If ViewState("rowCount") < 1 Then
            Return
        End If
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

            End If
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvFinance.RowDataBound
        MakeGridViewHeaderClickable(gvFinance, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvFinance.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvFinance, e.Row, Me)
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)
        'Dim btnExport As HtmlAnchor = CType(gvPagerRow.FindControl("btnExport"), HtmlAnchor)

        'btnExport.HRef = prepareExportToExcelLink()

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    Public Sub processBackToListing()
        If Not IsNothing(Request("SrcWorkorderID")) Then
            txtWorkorderID.Text = Request("SrcWorkorderID")
            ViewState("SrcWorkOrderId") = Request("SrcWorkorderID")
        End If
        If Not IsNothing(Request("SrcCompanyName")) Then
            txtCompanyName.Text = Request("SrcCompanyName").Replace(" and ", "&")
            ViewState("SrcCompanyName") = Request("SrcCompanyName").Replace(" and ", "&")
        End If
        If Not IsNothing(Request("SrcInvoiceDate")) Then
            txtInvoiceDate.Text = Request("SrcInvoiceDate").Replace(" and ", "&")
            ViewState("SrcInvoiceDate") = Request("SrcInvoiceDate").Replace(" and ", "&")
        End If
        If Not IsNothing(Request("SrcKeyword")) Then
            txtKeyword.Text = Request("SrcKeyword").Replace("ssss", """")
            ViewState("SrcKeyword") = Request("SrcKeyword").Replace("ssss", """")
        End If
        If Not IsNothing(Request("SrcPONumber")) Then
            txtPONumber.Text = Request("SrcPONumber")
            ViewState("SrcPONumber") = Request("SrcPONumber")
        End If
        If Not IsNothing(Request("SrcInvoiceNo")) Then
            txtInvoiceNo.Text = Request("SrcInvoiceNo")
            ViewState("SrcInvoiceNo") = Request("SrcInvoiceNo")
        End If
        If Not IsNothing(Request("bizDivId")) Then
            ViewState("BizDivId") = Request("bizDivId")
            ViewState("BizDivId") = Request("BizDivId")
        End If
        If Not IsNothing(Request("PS")) Then
            If Request("PS") <> "" Then
                gvFinance.PageSize = Request("PS")
            End If
        End If
        'Sort Expe
        If Not IsNothing(Request("SC")) Then
            ViewState!SortExpression = ""
            If Trim(Request("SC")) <> "" Then
                ViewState!SortExpression = Trim(Request("SC"))
            Else
                ViewState!SortExpression = "InvoiceNumber"
            End If
        End If

        'Sort direction
        Dim sd As SortDirection
        If Not IsNothing(Request("SO")) Then
            sd = SortDirection.Descending
            If Trim(Request("SO")) <> "" Then
                If Trim(Request("SO")).ToLower = "0" Then
                    sd = SortDirection.Ascending
                End If
            End If
        End If
        InitializeForm(False)
        lblSearchCriteria.Text = PrepareSearchCriteria()
        pnlSearchResult.Visible = True
        gvFinance.Sort(ViewState!SortExpression, sd)
        PopulateGrid()
        If Not IsNothing(Request("PN")) Then
            If Request("PN") <> "" Then
                gvFinance.PageIndex = Request("PN")
            Else
                gvFinance.PageIndex = 0
            End If
        Else
            gvFinance.PageIndex = 0
        End If
    End Sub

    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    Private Sub ObjectDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles ObjectDataSource1.Selecting
        InitializeGridParameters()
    End Sub

    Private Sub InitializeGridParameters()
        HttpContext.Current.Items("Keyword") = ViewState("SrcKeyword")
        HttpContext.Current.Items("PONumber") = ViewState("SrcPONumber")
        HttpContext.Current.Items("InvoiceNo") = ViewState("SrcInvoiceNo")
        HttpContext.Current.Items("WorkOrderId") = ViewState("SrcWorkOrderId")
        HttpContext.Current.Items("CompanyName") = ViewState("SrcCompanyName")
        HttpContext.Current.Items("InvoiceDate") = ViewState("SrcInvoiceDate")
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtWorkorderID.Text.Trim <> "" Or txtKeyword.Text.Trim <> "" Or txtPONumber.Text.Trim <> "" Or txtInvoiceNo.Text.Trim <> "" Or txtCompanyName.Text.Trim <> "" Or txtInvoiceDate.Text.Trim <> "" Then
            lblSearchCriteria.Text = PrepareSearchCriteria()
            pnlSearchResult.Visible = True
            ViewState("SrcWorkOrderId") = txtWorkorderID.Text.ToString.Trim
            ViewState("SrcCompanyName") = txtCompanyName.Text.ToString.Trim
            ViewState("SrcKeyword") = txtKeyword.Text.ToString.Trim
            ViewState("SrcPONumber") = txtPONumber.Text.ToString.Trim
            ViewState("SrcInvoiceNo") = txtInvoiceNo.Text.ToString.Trim
            ViewState("SrcInvoiceDate") = txtInvoiceDate.Text.ToString.Trim
            gvFinance.PageIndex = 0
            divEarlyDiscValidationGrid.Visible = False
            divValidationMain.Visible = False
            PopulateGrid()
        Else
            lblSearchCriteria.Text = "<strong> <span class='txtOrange'>Please Enter Search Criteria</span></strong>"
        End If

    End Sub

    Public Function GetLinks(ByVal InvoiceNo As String, ByVal CompanyID As Integer, ByVal WorkOrderId As String, ByVal InvoiceType As String, ByVal AllocatedTo As String, ByVal WOID As String, ByVal InvoiceStatus As String, ByVal param As String) As String
        Dim lnk As String
        lnk = ""
        If InvoiceType <> "" Then
            Select Case InvoiceType
                Case "Purchase"
                    'View Invoice: will open viewPurchaseInvoices.aspx in new window and Invoice No as a query string
                    lnk = lnk & "<a href=ViewPurchaseInvoice.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&WOID=" & WOID & "&bizdivid=1 runat='server' target='_blank' id='ViewInvoice'><img src='Images/Icons/View-Purchase-Invoice.gif' alt='View Purchase Invoice' hspace='2' vspace='0' border='0'></a>"
                    'View Payment: (if Invoice Status = Paid) viewCashpayment.aspx and Invoice No as a query string.
                    If InvoiceStatus = "Paid" Or InvoiceStatus = "Processed" Then
                        lnk = lnk & "<a href=ViewCashPayment.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&PaymentNo=" & AllocatedTo & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/View-Cash-Payment.gif' alt='View Payment' hspace='2' vspace='0' border='0'></a>"
                    End If
                    'View Work Order: redirect to AdminWODetails with woid and CompanyID in query string.
                    lnk = lnk & "<a href=AdminWODetails.aspx?woid=" & WOID & "&companyId=" & CompanyID & "&sender=SearchFinance&" & param & "&PS=" & gvFinance.PageSize & "&PN=" & gvFinance.PageIndex & "&SC=" & gvFinance.SortExpression & "&SO=" & gvFinance.SortDirection & "&Group=Closed" & "&bizdivid=1 runat='server' id='View WO'><img src='Images/Icons/ViewPIWODetails.gif' alt='View Work Orders' hspace='2' vspace='0' width='15' height='14' border='0'></a>"
                Case "Sales"
                    'View Invoice: will open viewSalesInvoices.aspx in new window and Invoice No as a query string
                    lnk = lnk & "<a href=ViewSalesInvoice.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Sales Invoice' hspace='2' vspace='0' border='0'></a>"
                    'View Payment: (if Invoice Status = Paid) ViewCashReceiptsCreditNotes.aspx and Invoice No as a query string.
                    If InvoiceStatus = "Paid" Then
                        lnk = lnk & "<a href=ViewCashReceiptsCreditNotes.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&PaymentNo=" & InvoiceNo & "&sender=SearchFinance&" & param & "&PS=" & gvFinance.PageSize & "&PN=" & gvFinance.PageIndex & "&SC=" & gvFinance.SortExpression & "&SO=" & gvFinance.SortDirection & "&bizdivid=1 runat='server' id='ViewInvoice'><img src='Images/Icons/View-Cash-Receipt.gif' alt='View Cash Receipts / Credit Notes' hspace='2' vspace='0' width='22' height='12' border='0'></a>"
                    End If
                    'View Related Work Orders: redirect to SalesInvoiceWOListings with InvoiceNo and CompanyID in query string.
                    lnk = lnk & "<a href=SalesInvoiceWOListing.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&PaymentNo=" & InvoiceNo & "&sender=SearchFinance&" & param & "&PS=" & gvFinance.PageSize & "&PN=" & gvFinance.PageIndex & "&SC=" & gvFinance.SortExpression & "&SO=" & gvFinance.SortDirection & "&bizdivid=1 runat='server' id='ViewRelatedWO'><img src='Images/Icons/View-All-WO-SI.gif' alt='View related Work Orders' hspace='2' vspace='0' width='15' height='14' border='0'></a>"
                    'Raise Credit Note: Redirect to RefundBuyer.aspx sending Sales Invoice Number in query string
                    'if user is not a representative then only he can check this option else hide it
                    If Session("RoleGroupID") <> ApplicationSettings.RoleOWRepID Then
                        lnk = lnk & "<a href=RefundBuyer.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&sender=SearchFinance&" & param & "&PS=" & gvFinance.PageSize & "&PN=" & gvFinance.PageIndex & "&SC=" & gvFinance.SortExpression & "&SO=" & gvFinance.SortDirection & " runat='server' id='RaiseCreditNote'><img src='Images/Icons/RaiseCreditNote.gif' alt='Raise Credit Notes' hspace='2' vspace='0' width='16' height='14' border='0'></a>"
                    End If
                Case "Credit Note"
                    'View Invoice: will open ViewCreditNote.aspx and InvoiceNo as query string
                    lnk = lnk & "<a href=ViewCreditNote.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/ViewCreditNote.gif' alt='View Credit Notes' hspace='2' vspace='0' border='0'></a>"
                    If InvoiceStatus = "Paid" Then
                        'View Invoice Allocation: If InvoiceStatus = Paid then redirect to ViewCashPayment.aspx and InvoiceNo in Query String
                        lnk = lnk & "<a href=ViewCashReceipt.aspx?invoiceNo=" & AllocatedTo & "&ReceiptType=Receipt&bizdivid=1 runat='server' id='ViewCNAlloc' target='_blank'><img src='Images/Icons/ViewInvoiceAllocation.gif' alt='View Invoice Allocation' hspace='2' vspace='0' height='14' width='12' border='0'></a>"
                    End If
                Case "Debit Note"
                    'View Invoice: will open ViewDebitNote.aspx and InvoiceNo as query string
                    lnk = lnk & "<a href=ViewDebitNote.aspx?invoiceNo=" & AllocatedTo & "&companyId=" & CompanyID & "&PaymentNo=" & InvoiceNo & "&bizDivId=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/ViewDebitNote.gif' alt='View Debit Notes' hspace='2' vspace='0' border='0'></a>"
                    ' ViewInvoiceAllocation: Redirect to ViewPurchaseInvoice.aspx and Allocatedto in query string as InvoiceNo
                    lnk = lnk & "<a href=ViewPurchaseInvoice.aspx?invoiceNo=" & AllocatedTo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewDNAlloc' target='_blank'><img src='Images/Icons/View-Purchase-Invoice.gif' alt='View Purchase Invoice' hspace='2' vspace='0' border='0'></a>"
                Case "Payment"
                    'View Invoice: will open ViewCashPayment.aspx and InvoiceNo as query string
                    lnk = lnk & "<a href=ViewCashPayment.aspx?invoiceNo=" & WOID & "&companyId=" & CompanyID & "&PaymentNo=" & InvoiceNo & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/View-Cash-Payment.gif' alt='View Payment' hspace='2' vspace='0' border='0'></a>"
                Case "Receipt"
                    'View Receipt
                    lnk = lnk & "<a href=ViewCashReceipt.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Cash Receipt' hspace='2' vspace='0' border='0'></a>"
                Case "UpSellSales"
                    'View Upsell Sales Invoice: will open ViewUpSellSales.aspx and InvoiceNo as query string
                    lnk = lnk & "<a href=ViewUpSellSalesInvoice.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewInvoice' target='_blank'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Upsell Sales Invoice' hspace='2' vspace='0' border='0'></a>"
                    'View Invoice Allocation: If InvoiceStatus = Paid then redirect to ViewCashPayment.aspx and InvoiceNo in Query String
                    lnk = lnk & "<a href=ViewUpSellCashReceipt.aspx?invoiceNo=" & InvoiceNo & "&companyId=" & CompanyID & "&bizdivid=1 runat='server' id='ViewCNAlloc' target='_blank'><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Upsell Cash Receipt' hspace='2' vspace='0' height='14' width='12' border='0'></a>"
                Case Else
                    'Unallocated Receipts cannot be viewed in details
                    lnk = ""
            End Select
        End If
        Return lnk
    End Function

    Private Sub gvFinance_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvFinance.RowCommand
        If (e.CommandName = "PaySI") Then
            Dim arrList As ArrayList = New ArrayList
            Dim str As String = e.CommandArgument.ToString
            arrList.AddRange(str.Split(","))
            Session("arrList(1)") = arrList(1)
            Session("arrList(0)") = arrList(0)
            mdlSIDatePaid.Show()
        ElseIf (e.CommandName = "PayCN") Then
            Dim arrList As ArrayList = New ArrayList
            Dim str As String = e.CommandArgument.ToString
            arrList.AddRange(str.Split(","))
            Session("arrList(1)") = arrList(1)
            Session("arrList(0)") = arrList(0)
            mdlCNDatePaid.Show()
        End If
    End Sub

    ''' <summary>
    ''' Called when Invoice type is Purchase and status is unpaid
    ''' Copied code from PIUnpaidAvailable
    ''' Create by Pankaj on 20 Nov
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ImgBtnPIAv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnPIAv.Click
        Dim InvoiceNo As String = CType(sender, ImageButton).CommandArgument.ToString
        Dim ds As New DataSet
        ds = ws.WSFinance.MS_ChangePIStatusToAvailable(Session("BizDivId"), InvoiceNo, "")
        If ds.Tables.Count > 0 Then
            If ds.Tables("Success").Rows(0).Item("Success") = 0 Then
                lblSearchCriteria.Text = ResourceMessageText.GetString("DBUpdateFail")
            Else
                If ds.Tables("SupEmailData").Rows.Count > 0 Then
                    Emails.SendMailPaymentAvailableInInvoiceSet(ds.Tables("SupEmailData").DefaultView)
                End If
            End If
        End If
        PopulateGrid()
    End Sub

    ''' <summary>
    ''' Called when Invoice type is Credit Note and status is unpaid
    ''' Copied code from CreditNotes
    ''' Create by Pankaj on 20 Nov''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub hdnButtonCN_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButtonCN.Click
        'Dim arrList As ArrayList = New ArrayList
        'Dim str As String = CType(sender, ImageButton).CommandArgument.ToString
        'arrList.AddRange(str.Split(","))
        ''Invoice No
        'Dim InvoiceNo As String
        ''Company Id
        'Dim CompanyId As Integer
        'CompanyId = arrList(1)
        'InvoiceNo = arrList(0)
        Dim ds As DataSet = New DataSet
        ds = ws.WSFinance.MS_PayCreditNote(Session("BizDivId"), Session("arrList(0)"), Session("arrList(1)"), Strings.FormatDateTime(txtCNDate.Text), Session("UserId"))
        'Dim ds As New DataSet
        'ds = ws.WSFinance.MSAccounts_GenerateCashReceipt(Session("BizDivId"), Session("arrList(0)"), Session("arrList(1)"), Strings.FormatDateTime(txtDate.Text), 0)
        Session.Remove("arrList(0)")
        Session.Remove("arrList(1)")
        PopulateGrid()
    End Sub

    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetWorkOrderID(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("InvoicedWorkOrderID", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function

    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetCompanyName(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("SearchCompanyName", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function

    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetPONumber(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("PONumber", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function

    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetInvoiceNo(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("InvoiceNo", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function

    Private Sub hdnButton_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click
        Dim ds As New DataSet
        ds = ws.WSFinance.MSAccounts_GenerateCashReceipt(Session("BizDivId"), Session("arrList(0)"), Session("arrList(1)"), Strings.FormatDateTime(txtDate.Text), 0, Session("UserId"))
        If ds.Tables.Count > 0 Then
            If ds.Tables("Success").Rows(0).Item("Success") = 0 Then
                lblSearchCriteria.Text = ResourceMessageText.GetString("DBUpdateFail")
            Else
                'Commented because sales invoices are delinked from the purchase invoices
                'If ds.Tables("SupEmailData").Rows.Count > 0 Then
                '    Emails.SendMailPaymentAvailableInInvoiceSet(ds.Tables("SupEmailData").DefaultView)
                'End If
                If ds.Tables("Success").Rows(0).Item("DefSIs") <> "" Then
                    divValidationMain.Visible = True
                    pnlSearchResult.Visible = False
                    divEarlyDiscValidationGrid.Visible = True
                    lblValMsg.Text = "Following Invoices cannot be marked as paid as these invoices includes early discounts and the time period of early discounts are over"
                    gvEarlyDiscInvoices.DataSource = ds.Tables(1)
                    ViewState("tblEarlyDiscounts") = ds.Tables(1)
                    gvEarlyDiscInvoices.DataBind()
                End If

            End If
        End If
        Session.Remove("arrList(0)")
        Session.Remove("arrList(1)")
        PopulateGrid()
    End Sub

    Public Sub PayInclDisc(ByVal sender As Object, ByVal e As EventArgs)
        'divValidationMain.Visible = False
        Dim ds As New DataSet
        ds = ws.WSFinance.MSAccounts_GenerateCashReceipt(Session("BizDivId"), hdnOnPageInvoiceNo.Text, Session("arrList(1)"), Strings.FormatDateTime(txtDate.Text), 2, Session("UserId"))
        If ds.Tables.Count > 0 Then
            If ds.Tables("Success").Rows(0).Item("Success") = 0 Then
                'divValidationMain.Visible = True
                lblSearchCriteria.Text = ResourceMessageText.GetString("DBUpdateFail")
            Else
                PopulateEarlyDiscountGrid(hdnOnPageInvoiceNo.Text)
            End If
        End If
    End Sub

    Public Sub PayExclDisc(ByVal sender As Object, ByVal e As EventArgs)
        pnlSearchResult.Visible = False
        'divValidationMain.Visible = True

    End Sub
    Public Sub PayInvoiceExclDiscount(ByVal sender As Object, ByVal e As EventArgs)
        Dim ds As New DataSet
        ds = ws.WSFinance.MSAccounts_GenerateCashReceipt(Session("BizDivId"), hdnOnPageInvoiceNo.Text, Session("arrList(1)"), Strings.FormatDateTime(txtDate.Text), 1, Session("UserId"))
        If ds.Tables.Count > 0 Then
            If ds.Tables("Success").Rows(0).Item("Success") = 0 Then
                'divValidationMain.Visible = True
                lblSearchCriteria.Text = ResourceMessageText.GetString("DBUpdateFail")
            Else
                divValidationMain.Visible = True
                lblValMsg.Text = "Please ensure you post a <a href=' CreateAdjustment.aspx?ContactID=" & Session("arrList(1)") & "&InvoiceNo=" & hdnOnPageInvoiceNo.Text & "'>manual adjustment</a> to offset the missed discount amount."
                'PopulateEarlyDiscountGrid(hdnOnPageInvoiceNo.Text)
                pnlSearchResult.Visible = False
                divEarlyDiscValidationGrid.Visible = False
            End If
        End If
    End Sub
    Private Sub PopulateEarlyDiscountGrid(ByVal InvoiceNo As String)
        divValidationMain.Visible = False
        lblSearchCriteria.Text = ""
        Dim dt As DataTable
        dt = CType(ViewState("tblEarlyDiscounts"), DataTable)

        Dim Row As Integer, Col As Integer
        Col = 0
        Dim DelRow As DataRow
        For Row = 0 To dt.Rows.Count - 1
            Dim hasvalue As Boolean = False
            Dim dR As DataRow = dt.Rows(Row)
            If dt.Rows(Row)(Col) = InvoiceNo Then
                hasvalue = True
            End If
            If hasvalue = True Then
                DelRow = dR
            End If
        Next
        dt.Rows.Remove(DelRow)

        gvEarlyDiscInvoices.DataSource = dt
        gvEarlyDiscInvoices.DataBind()
    End Sub
End Class