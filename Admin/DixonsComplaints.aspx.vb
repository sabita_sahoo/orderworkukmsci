﻿
Public Class DixonsComplaints
    Inherits System.Web.UI.Page

    Protected WithEvents ObjectDataSource1 As ObjectDataSource

    ''' <summary>
    ''' Shared wsobject to access web service webmethods
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            CommonFunctions.PopulateDixonsComplaintType(Page, ddlType, False)
            ddlType.Items.Insert(0, New ListItem("All", 0))
            CommonFunctions.PopulateDixonsComplaintType(Page, ddComplaintType, True)
            ddComplaintType.Items.Insert(0, New ListItem("Select Type", 0))

            ViewState!SortExpression = ""
            If Trim(Request("SC")) <> "" Then
                ViewState!SortExpression = Trim(Request("SC"))
            Else
                ViewState!SortExpression = "DateCreated"
            End If
            Dim sd As SortDirection
            sd = SortDirection.Descending
            If Trim(Request("SO")) <> "" Then
                If Trim(Request("SO")).ToLower = "true" Then
                    sd = SortDirection.Descending
                End If
            End If

            gvComplaints.PageSize = hdnPageSize.Value
            gvComplaints.Sort(ViewState!SortExpression, sd)
            PopulateGrid()
            gvComplaints.PageIndex = 0
        End If

        lblMsg.Text = ""
    End Sub

    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvComplaints.DataBind()
        End If

    End Sub
    Public Function prepareExportToExcelLink() As String
        Dim linkParams As String = ""
        linkParams &= "IsProcessed=" & ddlStatus.SelectedValue
        linkParams &= "&sortExpression=DateCreated DESC"
        linkParams &= "&startRowIndex=0"
        linkParams &= "&ComplaintRefNo=" & txtComplaintRefNo.Text.Trim
        linkParams &= "&ComplaintType=" & ddlType.SelectedValue
        linkParams &= "&DateCreatedFrom=" & txtDateCreatedFrom.Text.Trim
        linkParams &= "&DateCreatedTo=" & txtDateCreatedTo.Text.Trim
        linkParams &= "&DateProcessedFrom=" & txtDateProcessedFrom.Text.Trim
        linkParams &= "&DateProcessedTo=" & txtDateProcessedTo.Text.Trim
        linkParams &= "&page=" & "DixonsComplaints"
        Return "ExportToExcel.aspx?" & linkParams
    End Function

    ''' <summary>
    ''' sets the text for the confirm button. 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub setConfirmButtonText(ByVal gvPagerRow As GridViewRow)
        CType(gvPagerRow.FindControl("ConfirmBtnProcessed"), AjaxControlToolkit.ConfirmButtonExtender).ConfirmText = "Do you want to mark these complaints as processed?"
    End Sub

#Region "Grid Functions"

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        Dim rowCount As Integer
        ds = ws.WSContact.GetDixonsComplaintsListing(CInt(ddlStatus.SelectedValue), sortExpression, startRowIndex, maximumRows, rowCount, txtComplaintRefNo.Text.Trim, CInt(ddlType.SelectedValue), txtDateCreatedFrom.Text.Trim, txtDateCreatedTo.Text.Trim, txtDateProcessedFrom.Text.Trim, txtDateProcessedTo.Text.Trim, "")
        ViewState("rowCount") = rowCount
        If (ds.Tables(1).Rows.Count > 0) Then
            If (ds.Tables(1).Rows(0).Item("TotalRecs") > 0) Then
                tdExport.Visible = True
                btnExport.HRef = prepareExportToExcelLink()
            Else
                tdExport.Visible = False
            End If
        Else
            tdExport.Visible = False
        End If
        Return ds
    End Function

    ''' <summary>
    ''' ObjectDataSource Controls select Count Method which returns the row count for the listing 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvComplaints.RowDataBound
        MakeGridViewHeaderClickable(gvComplaints, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvComplaints.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvComplaints, e.Row, Me)
        End If
    End Sub

    Private Sub gvComplaints_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvComplaints.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
                'top pager buttons
                Dim tdProcessedTop As HtmlTableCell = CType(pagerRowTop.FindControl("tdProcessed"), HtmlTableCell)
                tdProcessedTop.Visible = True
                Dim chkSelectAll As CheckBox = CType(pagerRowTop.FindControl("chkSelectAll"), CheckBox)
                chkSelectAll.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

                'bottom pager buttons
                Dim tdProcessedBottom As HtmlTableCell = CType(pagerRowBottom.FindControl("tdProcessed"), HtmlTableCell)
                Dim chkSelectAll As CheckBox = CType(pagerRowBottom.FindControl("chkSelectAll"), CheckBox)

                tdProcessedBottom.Visible = False
                chkSelectAll.Visible = False
            End If
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)



        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
        setConfirmButtonText(gvPagerRow)
    End Sub

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        gvComplaints.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

#End Region

#Region "Actions"

    ''' <summary>
    ''' populates the list of Complaints
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnView.Click
        Page.Validate()
        If Page.IsValid Then
            lblMsg.Text = ""
            PopulateGrid()
            gvComplaints.PageIndex = 0
            
        End If
    End Sub

#End Region

#Region "Status Update"

    ''' <summary>
    ''' Function to set the company status to Processed - applicable only to buyers
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ProcessedComplaint(ByVal sender As Object, ByVal e As System.EventArgs)

        lblMsg.Text = ""
        Dim ComplaintIds As String = CommonFunctions.getSelectedIdsOfGrid(gvComplaints, "Check", "hdnComplaintId") 'getSelectedComplaintIds()
        If ComplaintIds = "" Then
            lblMsg.Text = "Please select Complaints to mark as Processed"
            Return
        End If
        ddComplaintType.SelectedValue = 0
        mdlComplaintType.Show()
    End Sub
    Public Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click
        Dim ds As DataSet
        Dim ComplaintIds As String = CommonFunctions.getSelectedIdsOfGrid(gvComplaints, "Check", "hdnComplaintId") 'getSelectedComplaintIds()
        Dim ComplaintType As Integer = ddComplaintType.SelectedValue
        'ds = ws.WSContact.UpdateDixonsComplaints(ComplaintIds, 1, CInt(ddComplaintType.SelectedValue))
        ds = ws.WSContact.UpdateDixonsComplaints(ComplaintIds, 1, ComplaintType, Session("UserId"))
        If ds.Tables.Count <> 0 Then
            If ds.Tables("tblStatus").Rows(0).Item("Success") <> 0 Then
                ' For populating the Textbox with the original contents of the mail
                lblMsg.Text = "Complaint Marked Processed Successfully."
                PopulateGrid()
            End If
        End If
    End Sub

    Public Function getSelectedComplaintIds() As String
        Dim ComplaintIds As String = ""
        For Each row As GridViewRow In gvComplaints.Rows
            Dim chkBox As HtmlInputCheckBox = CType(row.FindControl("Check"), HtmlInputCheckBox)
            If chkBox.Checked Then
                If ComplaintIds = "" Then
                    ComplaintIds = CType(row.FindControl("hdnComplaintId"), HtmlInputHidden).Value
                Else
                    ComplaintIds += "," & CType(row.FindControl("hdnComplaintId"), HtmlInputHidden).Value
                End If
            End If
        Next
        Return ComplaintIds
    End Function

#End Region

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub

    Public Sub rngDateCreated_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rngDateCreated.ServerValidate
        If Page.IsValid Then
            If txtDateCreatedFrom.Text.Trim <> "" And txtDateCreatedTo.Text.Trim <> "" Then
                Dim validDate As Boolean = True
                Try
                    Dim tmpFromDate As DateTime = DateTime.Parse(txtDateCreatedFrom.Text.Trim)
                    Dim tmpToDate As DateTime = DateTime.Parse(txtDateCreatedTo.Text.Trim)
                Catch ex As Exception
                    validDate = False
                End Try
                If validDate = True Then
                    Dim DateStart As New Date
                    Dim DateEnd As New Date
                    DateStart = txtDateCreatedFrom.Text.Trim
                    DateEnd = txtDateCreatedTo.Text.Trim
                    If DateStart <= DateEnd Then
                        args.IsValid = True
                    Else
                        args.IsValid = False
                    End If
                Else
                    args.IsValid = False
                End If
            End If
        End If
    End Sub
    Public Sub rngDateProcessed_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rngDateProcessed.ServerValidate
        If Page.IsValid Then
            If txtDateProcessedFrom.Text.Trim <> "" And txtDateProcessedTo.Text.Trim <> "" Then
                Dim validDate As Boolean = True
                Try
                    Dim tmpFromDate As DateTime = DateTime.Parse(txtDateProcessedFrom.Text.Trim)
                    Dim tmpToDate As DateTime = DateTime.Parse(txtDateProcessedTo.Text.Trim)
                Catch ex As Exception
                    validDate = False
                End Try
                If validDate = True Then
                    Dim DateStart As New Date
                    Dim DateEnd As New Date
                    DateStart = txtDateProcessedFrom.Text.Trim
                    DateEnd = txtDateProcessedTo.Text.Trim
                    If DateStart <= DateEnd Then
                        args.IsValid = True
                    Else
                        args.IsValid = False
                    End If
                Else
                    args.IsValid = False
                End If
            End If
        End If
    End Sub
End Class