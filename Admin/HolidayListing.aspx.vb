
Partial Public Class HolidayListing
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvHolidays.PageSize = ApplicationSettings.GridViewPageSize
            Session("chkbxShowDeleted") = False
            InitializeForm()
            If Session("RoleGroupID") = ApplicationSettings.RoleOWRepID Then
                btnAddHol.Visible = False
            End If
        End If
    End Sub

    Public Sub InitializeForm(Optional ByVal resetFields As Boolean = True)
        If resetFields Then
            ViewState!SortExpression = "HolidayDate"
            gvHolidays.Sort(ViewState!SortExpression, SortDirection.Ascending)
        End If
    End Sub

    ''' <summary>
    ''' Select From DB getting data set
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns>DataSet containing Search Results</returns>
    ''' <remarks></remarks>
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        If Not IsNothing(Session("chkbxShowDeleted")) Then
            ds = ws.WSContact.GetHolidays(sortExpression, startRowIndex, maximumRows, Session("chkbxShowDeleted"), "", 0)
        Else
            ds = ws.WSContact.GetHolidays(sortExpression, startRowIndex, maximumRows, False, "", 0)
        End If

        HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0)("TotalRecords")
        ViewState("totalRecords") = ds.Tables(1).Rows(0)("TotalRecords")
        Return ds
    End Function

    ''' <summary>
    ''' Select Count Method
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    ''' <summary>
    ''' For Paging Mechanism
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvHolidays.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

    ''' <summary>
    ''' Populate Grid
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvHolidays.DataBind()
        End If
    End Sub

    ''' <summary>
    ''' Pre Render Grid View
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub gvHolidays_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvHolidays.PreRender
        Me.gvHolidays.Controls(0).Controls(Me.gvHolidays.Controls(0).Controls.Count - 1).Visible = True
        If ViewState("rowCount") < 1 Then
            Return
        End If
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

            End If
        End If
    End Sub

    ''' <summary>
    ''' Grid View Row data bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub gvHolidays_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvHolidays.RowDataBound
        MakeGridViewHeaderClickable(gvHolidays, e.Row)
    End Sub

    ''' <summary>
    ''' Make grid view header clickable
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    ''' <summary>
    ''' Grid view row created
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvHolidays.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvHolidays, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' Set Pager Button States
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)
        'Dim btnExport As HtmlAnchor = CType(gvPagerRow.FindControl("btnExport"), HtmlAnchor)

        'btnExport.HRef = prepareExportToExcelLink()

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    ''' <summary>
    ''' Add, Edit Update of Holiday Listing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click
        Dim HolidayDate As String = txtDate.Text
        Dim HolidayTxt As String = txtHolidayTxt.Text.Trim
        Dim IsDeleted As Boolean = chkbxIsDeleted.Checked
        Try
            ws.WSContact.AddUpdateHolidays(hdnAddEditDateID.Value, HolidayDate, HolidayTxt, IsDeleted, 0)
        Catch ex As Exception
            lblErr.Text = ResourceMessageText.GetString("DBUpdateFail")
        End Try
        pnlListing.Style.Add("display", "block")
        pnlConfirm.Style.Add("display", "none")
        InitializeForm()

    End Sub
    Private Sub hdnButtonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButtonCancel.Click
        pnlListing.Style.Add("display", "block")
        pnlConfirm.Style.Add("display", "none")
        InitializeForm()
    End Sub

    ''' <summary>
    ''' Holidays Row command for Edit Holiday functionality
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub gvHolidays_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvHolidays.RowCommand

        If (e.CommandName = "Edit1") Then
            Dim arrList As ArrayList = New ArrayList
            Dim str As String = e.CommandArgument.ToString
            arrList.AddRange(str.Split(","))
            chkbxIsDeleted.Checked = arrList(3)
            txtHolidayTxt.Text = arrList(2)
            txtDate.Text = arrList(1)
            hdnAddEditDateID.Value = arrList(0)
            pnlListing.Style.Add("display", "none")
            pnlConfirm.Style.Add("display", "block")
            InitializeForm()
        End If
        
        'End If
        
    End Sub

    ''' <summary>
    ''' DB trip for showing all holidays
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub chkbxShowDeleted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkbxShowDeleted.CheckedChanged
        If Session("chkbxShowDeleted") = False Then
            Session("chkbxShowDeleted") = True
        Else
            Session("chkbxShowDeleted") = False
        End If
        InitializeForm()
    End Sub
End Class