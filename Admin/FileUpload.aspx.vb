
Partial Public Class FileUpload
    Inherits System.Web.UI.Page
    Protected WithEvents UCFileUpload1 As UCFileUpload_Outer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            UCFileUpload1.ClearUCFileUploadCache()
            UCFileUpload1.PopulateAttachedFiles()
        End If

        UCFileUpload1.BizDivID = CInt(Request.QueryString("BizDivID"))
        UCFileUpload1.AttachmentForID = CInt(Request.QueryString("WOID"))
        ViewState("woid") = CInt(Request.QueryString("WOID"))
        ViewState("WOStatus") = CInt(Request.QueryString("WOStatus"))
        UCFileUpload1.CompanyID = CInt(Request.QueryString("CompanyID"))
        UCFileUpload1.AttachmentForSource = "WOComplete"

    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dsWorkOrderForAttach As New WorkOrderNew
        dsWorkOrderForAttach = UCFileUpload1.ReturnFilledAttachments(dsWorkOrderForAttach)
        Dim xmlContent As String = dsWorkOrderForAttach.GetXml
        xmlContent = xmlContent.Remove(xmlContent.IndexOf("xmlns"), xmlContent.IndexOf(".xsd") - xmlContent.IndexOf("xmlns") + 5)
        CommonFunctions.SaveAttachments(xmlContent, ViewState("woid"), "WOComplete", "", 0, Session("UserID"))
        UCFileUpload1.ClearUCFileUploadCache()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "js", "CloseWindowForAttchments(" & ViewState("WOStatus") & ");", True)
    End Sub
End Class