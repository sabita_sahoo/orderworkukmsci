
Partial Public Class CurrentSalesReport
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Security.SecurePage(Page)

        


        Dim dt As DataTable
        Dim ds As New DataSet
        ds = GetCurrentRpt(True)

        ' this returns the submitted wo nos - here only checked for wos with status in 2,5 i.e submitted - Neetu
        If Not IsNothing(ds.Tables("Submitted")) Then
            If ds.Tables("Submitted").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("Submitted").Rows(0).Item("SubmittedWONo")) Then
                    lblSubmittedWOS.Text = 0
                Else
                    lblSubmittedWOS.Text = ds.Tables("Submitted").Rows(0).Item("SubmittedWONo")
                End If
            End If
        End If

        ' this returns the submitted wo value - here only checked for wos with status in 2,5 i.e submitted - Neetu
        If Not IsNothing(ds.Tables("SubmittedVal")) Then
            If ds.Tables("SubmittedVal").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("SubmittedVal").Rows(0).Item("SubmittedWOVal")) Then
                    lblSubmittedWOSVal.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblSubmittedWOSVal.Text = FormatCurrency(ds.Tables("SubmittedVal").Rows(0).Item("SubmittedWOVal"), 2, TriState.True, TriState.True, TriState.False)
                End If

            End If
        End If


        ' this returns the CA wo nos
        If Not IsNothing(ds.Tables("CA")) Then
            If ds.Tables("CA").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("CA").Rows(0).Item("CAWONo")) Then
                    lblCAWOS.Text = 0
                Else
                    lblCAWOS.Text = ds.Tables("CA").Rows(0).Item("CAWONo")
                End If
            End If
        End If

        ' this returns the CA wo value 
        If Not IsNothing(ds.Tables("CAVal")) Then
            If ds.Tables("CAVal").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("CAVal").Rows(0).Item("CAWOVal")) Then
                    lblCAWOSVal.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblCAWOSVal.Text = FormatCurrency(ds.Tables("CAVal").Rows(0).Item("CAWOVal"), 2, TriState.True, TriState.True, TriState.False)
                End If

            End If
        End If

        ' this returns the active wo nos - here only checked for wos with status in 6,7,9,10,11 and having tracking status = 6 - Neetu
        If Not IsNothing(ds.Tables("Active")) Then
            If ds.Tables("Active").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("Active").Rows(0).Item("ActiveWONo")) Then
                    lblActiveWOS.Text = 0
                Else
                    lblActiveWOS.Text = ds.Tables("Active").Rows(0).Item("ActiveWONo")
                End If
            End If
        End If

        'Dim ActiveWOVal, CancelledWOVal, TotalActiveWOVal As Decimal
        ' this returns the active wo value 
        If Not IsNothing(ds.Tables("ActiveVal")) Then
            If ds.Tables("ActiveVal").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("ActiveVal").Rows(0).Item("ActiveWOVal")) Then
                    lblActiveWOVal.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblActiveWOVal.Text = FormatCurrency(ds.Tables("ActiveVal").Rows(0).Item("ActiveWOVal"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If

        ' this returns the listing fee - here only checked for wos with status in 6,7,9,10,11 - Neetu
        If Not IsNothing(ds.Tables("Listing")) Then
            If ds.Tables("Listing").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("Listing").Rows(0).Item("ListingFee")) Then
                    lblListingFee.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblListingFee.Text = FormatCurrency(ds.Tables("Listing").Rows(0).Item("ListingFee"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If

        dt = ds.Tables("VATonListingFee")
        ' this returns the VAT on listing fee
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("ListingFeeVAT")) Then
                    lblVATonListingFee.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblVATonListingFee.Text = FormatCurrency(dt.Rows(0).Item("ListingFeeVAT"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If

        ' this returns the commission charge- here only checked for wos with status in 6,7,9,10,11 - Neetu
        If Not IsNothing(ds.Tables("Comm")) Then
            If ds.Tables("Comm").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("Comm").Rows(0).Item("Commission")) Then
                    lblCommission.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblCommission.Text = FormatCurrency(ds.Tables("Comm").Rows(0).Item("Commission"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If

        dt = ds.Tables("VATonComm")
        ' this returns VAT on the commission charge
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("CommissionChargeVAT")) Then
                    lblVATonCommission.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblVATonCommission.Text = FormatCurrency(dt.Rows(0).Item("CommissionChargeVAT"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If

        ' this returns the closed wo nos- here only checked for wos with status in 10 - Neetu
        If Not IsNothing(ds.Tables("Closed")) Then
            If ds.Tables("Closed").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("Closed").Rows(0).Item("ClosedWONo")) Then
                    lblClosedWOS.Text = 0
                Else
                    lblClosedWOS.Text = ds.Tables("Closed").Rows(0).Item("ClosedWONo")
                End If
            End If
        End If

        ' this returns the closed wos val - here only checked for wos with status in 10 - Neetu
        If Not IsNothing(ds.Tables("ClosedVal")) Then
            If ds.Tables("ClosedVal").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("ClosedVal").Rows(0).Item("ClosedWOVal")) Then
                    lblClosedWOVal.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblClosedWOVal.Text = FormatCurrency(ds.Tables("ClosedVal").Rows(0).Item("ClosedWOVal"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If

        ' this returns the cancelled wo no - here only checked for wos with status in 11 and wo tracking having status = 6 and also gets the last status before it was cancelled - Neetu
        If Not IsNothing(ds.Tables("Cancelled")) Then
            If ds.Tables("Cancelled").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("Cancelled").Rows(0).Item("CancelledWONo")) Then
                    lblCancelledWOS.Text = 0
                Else
                    lblCancelledWOS.Text = ds.Tables("Cancelled").Rows(0).Item("CancelledWONo")
                End If
            End If
        End If

        ' this returns the cancelled wo val - here only checked for wos with status in 11 and wo tracking having status = 6 and also gets the last status before it was cancelled - Neetu
        If Not IsNothing(ds.Tables("CancelledVal")) Then
            If ds.Tables("CancelledVal").Rows.Count > 0 Then
                If IsDBNull(ds.Tables("CancelledVal").Rows(0).Item("CancelledWOVal")) Then
                    lblCancelledWOVal.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblCancelledWOVal.Text = FormatCurrency(ds.Tables("CancelledVal").Rows(0).Item("CancelledWOVal"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If

        ''outstanding wos

        'submitted

        dt = ds.Tables("OSubmit")
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("OutstandingSubmitted")) Then
                    lblOSubmitted.Text = 0
                Else
                    lblOSubmitted.Text = dt.Rows(0).Item("OutstandingSubmitted")
                End If
            End If
        End If

        'CA
        dt = ds.Tables("OCA")
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("OutstandingCA")) Then
                    lblOCA.Text = 0
                Else
                    lblOCA.Text = dt.Rows(0).Item("OutstandingCA")
                End If
            End If
        End If

        'sent
        'dt = ds.Tables("OSent")
        'If Not IsNothing(dt) Then
        '    If dt.Rows.Count > 0 Then
        '        If IsDBNull(dt.Rows(0).Item("OutstandingSent")) Then
        '            lblOSent.Text = 0
        '        Else
        '            lblOSent.Text = dt.Rows(0).Item("OutstandingSent")
        '        End If
        '    End If
        'End If

        'active
        dt = ds.Tables("OActive")
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("OustandingActive")) Then
                    lblOActive.Text = 0
                Else
                    lblOActive.Text = dt.Rows(0).Item("OustandingActive")
                End If
            End If
        End If

        'issue
        dt = ds.Tables("OIssue")
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("OustandingIssue")) Then
                    lblOIssue.Text = 0
                Else
                    lblOIssue.Text = dt.Rows(0).Item("OustandingIssue")
                End If

            End If
        End If

        'completed
        dt = ds.Tables("OCompleted")
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("OustandingCompleted")) Then
                    lblOCompleted.Text = 0
                Else
                    lblOCompleted.Text = dt.Rows(0).Item("OustandingCompleted")
                End If
            End If
        End If

        ''funds
        ''funds to be received
        dt = ds.Tables("FundsToRecvd")
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("FTR")) Then
                    lblFTR.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblFTR.Text = FormatCurrency(dt.Rows(0).Item("FTR"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If

        ''funds received
        dt = ds.Tables("FundsRecvd")
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("FR")) Then
                    lblFR.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblFR.Text = FormatCurrency(dt.Rows(0).Item("FR"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If

        ''funds to be received outstanding
        dt = ds.Tables("OutFundsToRecv")
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("OutstandingFTR")) Then
                    lblOFTR.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblOFTR.Text = FormatCurrency(dt.Rows(0).Item("OutstandingFTR"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If

        ''completed payments
        dt = ds.Tables("SuppPaym")
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("CompletedPaymentsAmt")) Then
                    lblCompletedPayments.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblCompletedPayments.Text = FormatCurrency(dt.Rows(0).Item("CompletedPaymentsAmt"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If

        ''client account balance
        dt = ds.Tables("ClientBal")
        If Not IsNothing(dt) Then
            If dt.Rows.Count > 0 Then
                If IsDBNull(dt.Rows(0).Item("ClientAccBal")) Then
                    lblAccountBal.Text = FormatCurrency(0, 2, TriState.True, TriState.True, TriState.False)
                Else
                    lblAccountBal.Text = FormatCurrency(dt.Rows(0).Item("ClientAccBal"), 2, TriState.True, TriState.True, TriState.False)
                End If
            End If
        End If


        btnExport.HRef = "ExportToExcel.aspx?Page=CurrentSalesReport&bizDivId=" & Session("BizDivId")

    End Sub

    Public Function GetCurrentRpt(Optional ByVal KillCache As Boolean = False) As DataSet
        If KillCache = True Then
            Cache.Remove("Current_Report" & Session.SessionID)
        End If
        Dim ds As DataSet
        If Cache("Current_Report" & Session.SessionID) Is Nothing Then
            ds = ws.WSFinance.CurrentSalesReport(Session("BizDivId"))
            Cache.Add("Current_Report" & Session.SessionID, ds, Nothing, Date.MaxValue, New TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.NotRemovable, Nothing)
        Else
            ds = CType(Cache("Current_Report" & Session.SessionID), DataSet)
        End If
        Return ds
    End Function

End Class