
Partial Public Class Rating
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Protected WithEvents UCDateRange1 As UCDateRange
    Protected WithEvents UCSearchContact1 As UCSearchContact
    Protected WithEvents btnExport As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents rptComment As System.Web.UI.WebControls.GridView

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            Select Case ApplicationSettings.Country
                Case ApplicationSettings.CountryUK
                    ViewState("BizDivID") = ApplicationSettings.OWUKBizDivId
                Case ApplicationSettings.CountryDE
                    ViewState("BizDivID") = ApplicationSettings.OWDEBizDivId
            End Select
            Dim currDate As DateTime = System.DateTime.Today
            UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(Convert.ToDateTime("01/" & currDate.Month & "/" & currDate.Year), DateFormat.ShortDate)
            UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)


            UCSearchContact1.BizDivID = ViewState("BizDivID")
            UCSearchContact1.HideTestAccounts = 1
            UCSearchContact1.Filter = ""
            'ViewState("CompanyId") = UCSearchContact1.ddlContact.SelectedValue
            'ViewState("Rating") = ddFilterRating.SelectedValue
            'ViewState("FromDate") = UCDateRange1.txtFromDate.Text
            'ViewState("ToDate") = UCDateRange1.txtToDate.Text

            If Not IsNothing(Request("Rating")) Then
                If Request("Rating").ToString <> "" Then
                    ViewState("Rating") = Request("Rating")
                    ddFilterRating.SelectedValue = Request("Rating")
                Else
                    ViewState("Rating") = ddFilterRating.SelectedValue
                End If
            Else
                ViewState("Rating") = ddFilterRating.SelectedValue
            End If
            If Not IsNothing(Request("SearchWorkorderID")) Then
                If Request("SearchWorkorderID").ToString <> "" Then
                    ViewState("WorkorderID") = Request("SearchWorkorderID")
                    txtWorkorderID.Text = Request("SearchWorkorderID")
                Else
                    ViewState("WorkorderID") = ""
                End If
            Else
                ViewState("WorkorderID") = ""
            End If

            If Not IsNothing(Request("SelectedContact")) Then
                If Request("SelectedContact").ToString <> "" Then
                    UCSearchContact1.SelectedContact = 0
                    UCSearchContact1.populateContact()
                    If Request("SelectedContact") = "ALL" Then
                        UCSearchContact1.ddlContact.SelectedIndex = 1
                    Else
                        UCSearchContact1.SelectedContact = Request("SelectedContact")
                        UCSearchContact1.ddlContact.SelectedValue = Request("SelectedContact")
                    End If

                Else
                    ViewState("CompanyId") = UCSearchContact1.ddlContact.SelectedValue
                End If
            Else
                ViewState("CompanyId") = UCSearchContact1.ddlContact.SelectedValue
            End If

            If Not IsNothing(Request("FromDate")) Then
                If Request("FromDate").ToString <> "" Then
                    ViewState("FromDate") = Request("FromDate")
                    UCDateRange1.txtFromDate.Text = Request("FromDate")
                Else
                    ViewState("FromDate") = UCDateRange1.txtFromDate.Text
                End If
            Else
                ViewState("FromDate") = UCDateRange1.txtFromDate.Text
            End If

            If Not IsNothing(Request("ToDate")) Then
                If Request("ToDate").ToString <> "" Then
                    ViewState("ToDate") = Request("ToDate")
                    UCDateRange1.txtToDate.Text = Request("ToDate")
                Else
                    ViewState("ToDate") = UCDateRange1.txtToDate.Text
                End If
            Else
                ViewState("ToDate") = UCDateRange1.txtToDate.Text
            End If

            gvRating.Sort("DateClosed", SortDirection.Ascending)
            Populate()
        End If
    End Sub

    Public Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then
            Populate()
            gvRating.PageIndex = 0
        End If
    End Sub

    Public Sub Populate()
        ViewState("BizDivID") = Session("BizDivId")
        ViewState("FromDate") = UCDateRange1.txtFromDate.Text
        ViewState("ToDate") = UCDateRange1.txtToDate.Text

        ViewState("Rating") = ddFilterRating.SelectedValue
        ViewState("WorkorderID") = txtWorkorderID.Text
        If UCSearchContact1.ddlContact.SelectedValue = "" Then
            ViewState("CompanyId") = -1
            'ElseIf UCSearchContact1.ddlContact.SelectedValue = "ALL" Then
            '    ViewState("CompanyId") = 0
        Else
            ViewState("CompanyId") = UCSearchContact1.ddlContact.SelectedValue
        End If
        PopulateGrid()
    End Sub

    Public Function prepareExportToExcelLink() As String
        Dim linkParams As String = ""
        linkParams &= "BizDivId=" & ViewState("BizDivID")
        linkParams &= "&FromDate=" & ViewState("FromDate")
        linkParams &= "&ToDate=" & ViewState("ToDate")
        linkParams &= "&sortExpression=" & gvRating.SortExpression
        linkParams &= "&page=" & "AdminWORatings"
        linkParams &= "&WorkorderID=" & ViewState("WorkorderID")
        linkParams &= "&CompanyId=" & ViewState("CompanyId")
        Return "ExportToExcel.aspx?" & linkParams
    End Function

    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvRating.DataBind()
        End If
    End Sub

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim FromDate As String = HttpContext.Current.Items("FromDate")
        Dim ToDate As String = HttpContext.Current.Items("ToDate")
        Dim BizDivID As Integer = HttpContext.Current.Items("BizDivID")
        Dim CompanyId As String = HttpContext.Current.Items("CompanyId")
        Dim WorkorderID As String = HttpContext.Current.Items("WorkorderID")
        Dim Rating As String = HttpContext.Current.Items("Rating")


        Dim ds As DataSet = ws.WSWorkOrder.GetWORatings(FromDate, ToDate, BizDivID, sortExpression, startRowIndex, maximumRows, CompanyId, Rating, WorkorderID)

        If ds.Relations("LinksByHeader") Is Nothing Then
            ds.Relations.Add("LinksByHeader", ds.Tables(0).Columns("WOID"), ds.Tables(2).Columns("WOID"), False)
        End If

        HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
        Return ds
    End Function

    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvRating.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

    Private Sub gvContacts_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvRating.PreRender
        Me.gvRating.Controls(0).Controls(Me.gvRating.Controls(0).Controls.Count - 1).Visible = True
        If ViewState("rowCount") < 1 Then
            Return
        End If
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

            End If
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvRating.RowDataBound
        rptComment_RowDataBound(sender, e)
        MakeGridViewHeaderClickable(gvRating, e.Row)

    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvRating.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvRating, e.Row, Me)
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)
        Dim btnExport As HtmlAnchor = CType(gvPagerRow.FindControl("btnExport"), HtmlAnchor)

        btnExport.HRef = prepareExportToExcelLink()

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    Private Sub ObjectDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles ObjectDataSource1.Selecting
        InitializeGridParameters()
    End Sub

    Private Sub InitializeGridParameters()
        HttpContext.Current.Items("FromDate") = ViewState("FromDate")
        HttpContext.Current.Items("ToDate") = ViewState("ToDate")
        HttpContext.Current.Items("BizDivID") = ViewState("BizDivID")
        HttpContext.Current.Items("CompanyId") = ViewState("CompanyId")
        HttpContext.Current.Items("Rating") = ViewState("Rating")
        HttpContext.Current.Items("WorkorderID") = ViewState("WorkorderID")
    End Sub

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub

    Public Function ShowRatingIcon(ByVal Rating) As String
        Dim src As String
        Select Case (Rating)
            Case "0"
                src = "~/Images/Icons/NeutralRating.gif"
            Case "1"
                src = "~/Images/Icons/PositiveRating.gif"
            Case "-1"
                src = "~/Images/Icons/NegativeRating.gif"
            Case "NA"
                src = ""
        End Select
        Return src
    End Function

    Private Sub gvRating_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRating.RowCommand
        If (e.CommandName = "WOID") Then
            Dim WorkOrderId As String
            WorkOrderId = e.CommandArgument.ToString
            ViewState.Add("SelectedWorkOrderId", WorkOrderId)
            mdlUpdateRating.Show()

            rdBtnNegative.Checked = False
            rdBtnNeutral.Checked = False
            rdBtnPositive.Checked = False
            txtComment.Text = ""


            Dim ds As DataSet
            ds = CommonFunctions.GetWORating(WorkOrderId)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If Not IsDBNull(ds.Tables(0).Rows(0)(0)) Then
                        Dim Result As Integer = ds.Tables(0).Rows(0)(0)
                        Select Case Result
                            Case -1
                                rdBtnNegative.Checked = True
                            Case 0
                                rdBtnNeutral.Checked = True
                            Case 1
                                rdBtnPositive.Checked = True
                        End Select
                    End If
                End If
            End If
        End If
    End Sub

    Public Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click

        Dim Rating As Integer
        If rdBtnNegative.Checked = True Then
            Rating = -1
        ElseIf rdBtnPositive.Checked = True Then
            Rating = 1
        Else
            Rating = 0
        End If
        Dim Success As DataSet = CommonFunctions.SaveRatingComment(ViewState("SelectedWorkOrderId"), Session("CompanyID"), Session("UserID"), txtComment.Text.Trim, Rating)
        txtComment.Text = ""

        PopulateGrid()
    End Sub
    Private Sub rptComment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles rptComment.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rptComment1 As New DataList
            Dim dv As DataView
            rptComment1 = DirectCast(e.Row.FindControl("rptDiscussion"), DataList)
            dv = (DirectCast(e.Row.DataItem, DataRowView)).CreateChildView("LinksByHeader")
            dv.Sort = "DateCreated ASC"
            rptComment1.DataSource = dv
            rptComment1.DataBind()

        End If
    End Sub
    <Services.WebMethod(), Script.Services.ScriptMethod()> _
    Public Shared Function GetWorkOrderID(ByVal prefixText As String, ByVal contextKey As String) As String
        Dim dt As DataTable
        dt = ws.WSWorkOrder.GetAutoSuggest("WorkOrderID", prefixText, 0)
        Return WebLibrary.Utils.Tbl2Str(dt, prefixText)
    End Function
End Class