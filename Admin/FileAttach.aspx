<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FileAttach.vb" Inherits="Admin.FileAttach" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    
<link href="Styles/OrderWork.css" rel="stylesheet" type="text/css">  
<style>

    <asp:literal id="ltStyle" runat="server" ></asp:literal>

</style>
</head>
<body > 
    <form id="form1" runat="server" style="margin:0px 0px 0px 0px" >
   
    <asp:panel id="pnlNormalAttach" runat="server">
            <%@ Register TagPrefix="uc1" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUpload_Inner.ascx" %>
            <uc1:UCFileUpload id="fileUpload1" runat="server"></uc1:UCFileUpload>      
             </asp:panel>
         
         
            <asp:panel id="pnlInsuranceAttach" runat="server" visible="False">
            <%@ Register TagPrefix="uc2" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUploadInner_Insurance.ascx" %>
             <uc2:UCFileUpload id="fileUpload2" runat="server"></uc2:UCFileUpload> 
            </asp:panel>
       
            <asp:panel id="pnlStatementOfWork" runat="server" visible="False">
            <%@ Register TagPrefix="uc3" TagName="UCFileUpload" Src="~/UserControls/UK/UCFileUploadInner_StatementOfWork.ascx" %>
             <uc3:UCFileUpload id="fileUpload3" runat="server"></uc3:UCFileUpload> 
            </asp:panel>
     
   
     
           
          
      </form>
</body>
</html>
