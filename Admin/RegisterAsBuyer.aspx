<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Register as a buyer" CodeBehind="RegisterAsBuyer.aspx.vb" Inherits="Admin.RegisterAsBuyer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register TagPrefix="uc1" TagName="UCSearchContact" Src="~/UserControls/Admin/UCSearchContact.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <style type="text/css">
 td {
	
	line-height: 8px; !important

}
.bodytxtValidationMsg 
{
line-height:13px; !important
}
.clsmarginfloat
{
margin-right: 122px;
float: right;
}
 
 
 </style>
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
             <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
					  
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td width="10px"></td>
        <td class="HeadingRed">Create Client Account</td>
    </tr>
    <tr height=""10px>
        <td>&nbsp;</td>
        <td>
         <%@ Register TagPrefix="uc1" TagName="Registration"   Src="~/UserControls/Admin/UCRegistration.ascx" %>
          <uc1:Registration id="UCRegister" runat="server"></uc1:Registration>       
        </td>
    </tr>
</table>					
<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      
      
      </asp:Content>