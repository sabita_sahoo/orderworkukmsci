<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Accept Multiple WO" CodeBehind="MultipleWOHandling.aspx.vb" Inherits="Admin.MultipleWOHandling" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

   <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
			
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
    
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            <asp:UpdatePanel ID="UpdatePanelMultipleWO" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMultipleWO" runat="server">
                    
              <div style="margin-left:15px; margin-top:5px; font-size:12px;">
                <p class="paddingB4 HeadingRed"><strong>Welcome to OrderWork: Multiple Work Order Handling</strong> </p>
              
              	<div id="divValidationMain" class="divValidation" runat="server" visible="false" style="margin:15px">
				<div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  <table width="96%"  border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					</tr>
					<tr valign="middle">
					  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
					  <td width="565" class="validationText"><div  id="divValidationMsg"></div>
					   <span class="validationText">
					  	<asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
					  </span>					   
					  </td>
					  <td width="20">&nbsp;</td>
					</tr>
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15"><input type="button" id="btnFocus" name="btnFocus" class="hdnClass"  /></td>
					  <td height="15">&nbsp;</td>
					</tr>
				  </table>  
				  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				</div>
              
                <asp:Panel ID="pnlGridListings" runat="server">
                    <div id="divValidWOIDs" runat="server" style="margin-top:10px;">
                        <strong><asp:Label ID="lblValidWO" runat="server" Text="Label"></asp:Label></strong><br />
                        <asp:DataList ID="dlValidWO" runat="server" CssClass="formLabelGrey" Font-Size="10px">
                                  <ItemTemplate>  
                                    <div style="width:100%;">
                                        <div style=" float:left;"><%#Container.DataItem("RefWOID")%> </div> 
                                        <div style=" float:left; margin-left:10px;">WorkOrder Title: <%#Container.DataItem("WOTitle")%> </div> 
                                    </div>
                                   </ItemTemplate>     
                       </asp:DataList>
                       <asp:Label ID="lblNovalidRecords" runat="server" Text="" Visible="false"></asp:Label>
                    </div>
                    
                    <div id="divInvalidWOIDs" runat="server" style="margin-top:10px;">
                        <strong><asp:Label ID="lblInValidWO" runat="server" Text="Label"></asp:Label></strong><br />
                        <asp:DataList ID="dlInvalidWO" runat="server" CssClass="formLabelGrey" Font-Size="10px">
                                  <ItemTemplate>  
                                    <div style="width:100%;">                                         
                                         <div style=" float:left;">WorkOrderID: <%#Container.DataItem("RefWOID")%> </div> 
                                         <div style=" float:left; margin-left:10px;">WorkOrder Title: <%#Container.DataItem("WOTitle")%> </div> 
                                    </div>
                                   </ItemTemplate>  
                        </asp:DataList>
                        <asp:Label ID="lblNoInvalidRecords" runat="server" Text="" Visible="false"></asp:Label>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlRating" runat="server">
                    <div style=" margin-top:10px;">
                        <table visible="true" width="100%" border="0" cellspacing="0" cellpadding="0">
	                     <tr>
	                       <td height="20" valign="bottom" class="formLabelGrey" >Select a Rating<asp:CustomValidator id="rqRating" runat="server" ForeColor="#EDEDEB"
												                    ErrorMessage="Please select a rating">*</asp:CustomValidator></td>
	                     </tr>
	                     <tr>
	                       <td><asp:RadioButton id="rdoRatingPositive" style="vertical-align:baseline; " runat="server" CssClass="formLabelGrey" Text="Positive" TextAlign="right" GroupName="rdoRating"></asp:RadioButton>
			                    &nbsp;&nbsp;
			                    <asp:RadioButton id="rdoRatingNeutral" runat="server" CssClass="formLabelGrey" Text="Neutral" TextAlign="right" GroupName="rdoRating"  Checked="true"></asp:RadioButton>
			                    &nbsp;&nbsp;
			                    <asp:RadioButton id="rdoRatingNegative" runat="server" CssClass="formLabelGrey" Text="Negative"	TextAlign="right" GroupName="rdoRating"></asp:RadioButton>
	                       </td>
	                     </tr>	                     
	                    </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlComments" runat="server">
                    <div style=" margin-top:10px;">
                        <table visible="true" width="100%" border="0" cellspacing="0" cellpadding="0">
	                     <tr>
	                       <td height="20" valign="bottom" class="formLabelGrey" ><asp:Label id="lblComments" runat="server" Visible="true" CssClass="formLabelGrey"></asp:Label>
	                        <asp:RegularExpressionValidator id="RegExComments" runat="server" ValidationExpression="(.|[\r\n]){1,500}"  ErrorMessage="Please enter comments less than 500 characters" ForeColor="#EDEDEB"
					        ControlToValidate="txtComments">*</asp:RegularExpressionValidator>
	                       </td>
	                     </tr>
	                     <tr>
	                       <td><asp:TextBox id="txtComments" runat="server" CssClass="formFieldGrey width470height58" style="width:300px;" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" Rows="4"></asp:TextBox></td>
	                     </tr>
	                    </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlServicePartnerList" runat="server">
                    <div style=" margin-top:10px;">
                        <div style="height:18px;">
                            <strong><asp:Label ID="Label1" runat="server" Text="Select Service partner to ordermatch."></asp:Label></strong>
                        </div>
                        <asp:DropDownList ID="drpdwnContacts" runat="server" CssClass="formField">
                        </asp:DropDownList>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div style=" margin-top:10px;">
                    <asp:Button ID="btnMassAcceptanceCanel" Visible="false" runat="server" Text="Cancel" />
                    <asp:Button ID="btnConfirm" runat="server" CausesValidation="false"  Text="Confirm" />
                    <asp:Button ID="btnBackConfirm" runat="server" CausesValidation="false"  Text="Confirm and Back to Listing" />
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="false"  Text="Cancel" />
                    </div>
                </asp:Panel>
                <div style=" margin-top:10px;">
                  <asp:Button ID="btnNext" runat="server" CausesValidation="false"  Text="Next" />
                  <asp:Button ID="btnBack" runat="server" CausesValidation="false"  Text="Back" />
                </div>
            </div>
                </asp:Panel>
            </ContentTemplate>
             <Triggers>
  	            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnConfirm" />
	            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnCancel" />
	            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnNext" />
	            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnBack" />
              </Triggers>
            </asp:UpdatePanel>
            
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
<asp:UpdateProgress  ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanelMultipleWO" runat="server">
                <ProgressTemplate>
                    <div class="gridText">
                        <img  align=middle src="Images/indicator.gif" />
                        <b>Please wait saving data...</b>
                    </div>      
                </ProgressTemplate>
            </asp:UpdateProgress>
        <cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender2" ControlToOverlayID="pnlMultipleWO" CssClass="updateProgress" TargetControlID="UpdateProgress2" runat="server" />

</asp:Content>