Public Partial Class References
    Inherits System.Web.UI.Page

    Protected WithEvents UCReferenceUK1 As UCReference
    Protected WithEvents UCAccountSumm1 As UCAccountSummary

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        UCReferenceUK1.CompanyID = Request("CompanyID")
        If Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
            UCAccountSumm1.ShowRating = True
            lnkViewBlackListClient.Visible = True
            lnkViewWorkingDays.Visible = True
        ElseIf Session("ContactClassID") = ApplicationSettings.RoleClientID Then
            UCAccountSumm1.ShowRating = False
        End If
        UCAccountSumm1.BizDivId = Request("bizDivId")
        If Not IsNothing(Session(Request("companyid") & "_ContactClassID")) Then
            If Session(Request("companyid") & "_ContactClassID") = ApplicationSettings.RoleClientID Then
                pnlReferenceForm.Visible = False
                lblError.Text = ResourceMessageText.GetString("BuyerReferenceError")
            End If
        End If

        If (Session(Request("companyid") & "_ContactClassID")) = ApplicationSettings.RoleClientID Then
            'poonam modified on 2/10/2015 - Task - OA-60 : OA - Client Account profile gets Supplier layout sections after going to Add User
            lnkCompProfile.HRef = "CompanyProfileBuyer.aspx?" & "CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            lnkAutoMatch.HRef = "AutoMatchSettings.aspx?CompanyID=" & Request("CompanyID") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            lnkAccSettings.HRef = "AccountSettings.aspx?CompanyID=" & Request("CompanyID") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            lnkViewAllInvoices.HRef = "ViewAllInvoices.aspx?CompanyID=" & Request("companyid") & Request("CompanyID") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
            lnkViewAccountHistory.HRef = "AccountHistory.aspx?CompanyID=" & Request("companyid") & Request("CompanyID") & "&sender=buyer" & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        ElseIf Session("ContactClassID") = ApplicationSettings.RoleSupplierID Then
            lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId")
            lnkAutoMatch.HRef = "AutoMatchSettings.aspx?CompanyID=" & Request("CompanyID") & "&sender=company" & "&bizDivId=" & Request("bizDivId")
            lnkAccSettings.HRef = "AccountSettings.aspx?CompanyID=" & Request("CompanyID") & "&sender=company" & "&bizDivId=" & Request("bizDivId")
            lnkViewAllInvoices.HRef = "ViewAllInvoices.aspx?CompanyID=" & Request("companyid") & "&sender=company" & "&bizDivId=" & Request("bizDivId")
            lnkViewAccountHistory.HRef = "AccountHistory.aspx?CompanyID=" & Request("companyid") & "&sender=company" & "&bizDivId=" & Request("bizDivId")
        End If
        lnkViewFavSuppliers.HRef = "FavouriteSupplierList.aspx?mode=favourite&CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkAddProduct.HRef = "AdminProductList.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkAddProductNew.HRef = "ServiceListing.aspx?CompanyID=" & Request("CompanyID")
        'lnkViewBlackListsupplier.HRef = "FavouriteSupplierList.aspx?mode=blacklist&CompanyID=" & Session("ContactCompanyID")
        'lnkReferences.HRef = "References.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        lnkuser.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkloc.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        ' lnkCompProfile.HRef = "CompanyProfile.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId")
        lnkAddUsers.HRef = "Specialists.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkAddLocationsSupp.HRef = "Locations.aspx?CompanyID=" & Request("CompanyID") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkViewBlackListClient.HRef = "FavouriteSupplierList.aspx?mode=ClientBlacklist&CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")
        lnkViewWorkingDays.HRef = "WorkingDays.aspx?CompanyID=" & Request("companyid") & "&bizDivId=" & Request("bizDivId") & "&classId=" & Request("classId")

        'Prepare AutoMatch Link URL

    End Sub

End Class