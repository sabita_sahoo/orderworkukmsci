Public Partial Class Specialists
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Security.SecurePage(Page)
        HttpContext.Current.Items("MainContactID") = Request("companyid")
        HttpContext.Current.Items("BizDivID") = Request("bizDivId")
        UCSpecialistsListing.FindControl("tblChangeAdmin").Visible = True
    End Sub

End Class