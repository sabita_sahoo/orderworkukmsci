﻿
Partial Public Class SagePayExport
    Inherits System.Web.UI.Page
    Protected WithEvents UCDateRange1 As UCDateRange


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            UCDateRange1.txtFromDate.Text = Strings.FormatDateTime(System.DateTime.Today.AddMonths(-1), DateFormat.ShortDate)
            UCDateRange1.txtToDate.Text = Strings.FormatDateTime(System.DateTime.Today, DateFormat.ShortDate)

        End If
    End Sub
   

 
    Private Sub btnExport_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.ServerClick
        Dim Mode As String
        Dim FromDate As String
        Dim Todate As String
        Mode = ddlsagepaymode.SelectedValue
        FromDate = UCDateRange1.txtFromDate.Text
        Todate = UCDateRange1.txtToDate.Text
        Response.Redirect("ExportToExcel.aspx?page=SagePayExport&Mode=" & Mode & "&FromDate=" & FromDate & "&Todate=" & Todate)
    End Sub
End Class