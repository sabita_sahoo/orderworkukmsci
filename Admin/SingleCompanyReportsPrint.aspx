<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SingleCompanyReportsPrint.aspx.vb" Inherits="Admin.SingleCompanyReportsPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>OrderWork : Single Company Report</title>
<style type="text/css">
<!--
.invoiceLabel {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 32px;
	color: #B7B7B7;
	font-weight: bold;
	line-height:38px;

}
.InvoiceText {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.bdr1 {
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 4px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: solid;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.bdr2 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #C6C6C6;
	border-right-color: #C6C6C6;
	border-bottom-color: #C6C6C6;
	border-left-color: #C6C6C6;
}
.bdr3 {
	border-top-width: 1px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.style3 {font-size: 11px}
.style5 {font-weight: bold}
.style6 {font-weight: bold}
td
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	line-height: 16px;
	color: #000000;
}
body {
	background-color: #CECECE;
	height:100%;
}.VatMessage {
	text-align: center;
}
-->
</style>
</head>
<body style="height:100%;" onload="javascript:this.print()">
		<asp:Panel ID="pnlErrMsg" CssClass="pnlErrorMsg" Visible="False" runat="server">
			<table height="500" width="700" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FEFFD3">
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" align="center">
						  <tr>
							<td>&nbsp;</td>
							<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#CC3300; font-weight:bold;"><asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
						  </tr>
						</table>
					</td>
				</tr>
			</table>	
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlPrintContent" Visible="true">
			 <table height="95%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
            <tr>
              <td width="45" height="140">&nbsp;</td>
              <td width="650" height="140"><table width="100%" cellpadding="0" cellspacing="0" style=" border-width:medium; border-bottom-width:5px;border-bottom-color:#000000;">
                  <tr>
                    <td width="311">&nbsp;</td>
                    <td width="131" align="left" valign="bottom" >&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="311" valign="middle"><img src="Images/Invoice_Logo_New.gif" runat="server" id="imgLogo" alt="OrderWork - Services Reinvented" border="0" class="LogoImg" /></td>
                    <td align="right" valign="bottom" >&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="311" class="bdr1" >&nbsp;</td>
                    <td align="left" valign="bottom" class="bdr1" >&nbsp;</td>
                  </tr>
              </table></td>
              <td width="45" height="140">&nbsp;</td>
            </tr>
            <tr>
            
            </tr>
             <tr>
                
                <td width="45">&nbsp;</td>
                <td width="650">
                    <div style="margin:15px">
					<p style="font-size:16px; text-align:center;"><strong>Single Company Report for <asp:Label ID="lblCompanyName" runat="server"></asp:Label></strong>
					 <br /><br />
					  <span style="font-size:14px;"><strong>Date Range : </strong><asp:Label ID="lblFromDate" runat="server"></asp:Label> To <asp:Label ID="lblToDate" runat="server"></asp:Label></span>
					  </p>
					</div>
					<asp:panel id="pnlShowRecords" runat="server" Visible="false">
					  
			    <div style="margin:15px">			    
			    <p style="text-decoration:underline; font-size:14px;"><strong>Summary</strong></p>
			    <div style="margin:15px">
			     <table >
			            <tr>
			                <td colspan="6"><strong style="text-decoration:underline">WorkOrders</strong></td>
			            </tr>
                        <tr >
                            <td width="100" style="font-weight:bold">Type</td>
                            <td width="100" style="font-weight:bold">No. of Orders</td>
                            <td width="80" style="font-weight:bold;text-align:right">WP</td>
                            <td width="80" style="font-weight:bold;text-align:right">PP</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin (%)</td>
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater1" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="100"><%#Container.DataItem("label")%></td>
                            <td width="100"><%#Container.DataItem("TotalWorkOrders")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalWholesalePrice")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalPlatformPrice")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("Margin")%></td>
                            <td width="80" style="text-align:right"><%#Container.DataItem("PercentMargin")%></td>
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:Repeater>
                
                <p></p>
			    <div>
			     <table>
			            <tr>
			                <td colspan="3"><strong style="text-decoration:underline">Invoices</strong></td>
			            </tr>
                        <tr>
                            <td width="100" style="font-weight:bold">Type</td>
                            <td width="120" style="font-weight:bold">No. of Invoices</td>
                            <td width="80" style="font-weight:bold;text-align:right">Net Value</td>                            
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater2" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="100"><%#Container.DataItem("InvoiceType")%></td>
                            <td width="120"><%#Container.DataItem("NoOfInvoices")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalAmount")%></td>                           
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:Repeater>
                </div>
                </div>
                <p></p>
			    <div id="divServices" runat="server" style="margin:15px">
			     <table>
			            <tr>
			                <td colspan="6"><strong style="text-decoration:underline">Services</strong></td>
			            </tr>
                        <tr>
                            <td width="220" style="font-weight:bold">Service Name</td>
                            <td width="60" style="font-weight:bold">Quantity</td>
                            <td width="80" style="font-weight:bold;text-align:right">CP</td>
                            <td width="80" style="font-weight:bold;text-align:right">WP</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin (%)</td>
                             <td width="80" style="font-weight:bold;text-align:right">OWMargin</td>
                            <td width="80" style="font-weight:bold;text-align:right">OWMargin (%)</td>
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater3" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="220"><%#Container.DataItem("ProductName")%></td>
                            <td width="60"><%#Container.DataItem("QuantitySold")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("CustomerPrice")%></td>                           
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("WholesalePrice")%></td>                           
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("CustomerMargin")%></td>                           
                            <td width="80" style="text-align:right"><%#Container.DataItem("PercentCustomerMargin")%></td> 
                            <td width="80" style="text-align:right">&pound; <%# Container.DataItem("OWMargin")%></td>                           
                            <td width="80" style="text-align:right"><%#Container.DataItem("PercentOWMargin")%></td>                                                    
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:Repeater>
                </div>
                
                <p></p><p></p>
                <div style="margin:15px">
                <asp:Repeater ID="Repeater7" runat="server">
                <ItemTemplate>
                <input type="hidden" id="hdnBillingLocID" value='<%#Container.DataItem("BillingLocID")%>' runat="server" />                
                 <p style="text-decoration:underline; font-size:14px;"><strong><%#Container.DataItem("BillingLocName")%></strong></p>
			    <div style="margin:15px">
			     <table>
			            <tr>
			                <td colspan="6"><strong style="text-decoration:underline">WorkOrders</strong></td>
			            </tr>
                        <tr>
                            <td width="100" style="font-weight:bold">Type</td>
                            <td width="100" style="font-weight:bold">No. of Orders</td>
                            <td width="80" style="font-weight:bold;text-align:right">WP</td>
                            <td width="80" style="font-weight:bold;text-align:right">PP</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin (%)</td>
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater4" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="100"><%#Container.DataItem("label")%></td>
                            <td width="100"><%#Container.DataItem("TotalWorkOrders")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalWholesalePrice")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalPlatformPrice")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("Margin")%></td>
                            <td width="80" style="text-align:right"><%#Container.DataItem("PercentMargin")%></td>
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:Repeater>
                </div style="margin:15px">
                <p></p>
			    <div style="margin:15px">
			     <table>
			            <tr>
			                <td colspan="3"><strong style="text-decoration:underline">Invoices</strong></td>
			            </tr>
                        <tr>
                            <td width="100" style="font-weight:bold">Type</td>
                            <td width="120" style="font-weight:bold">No. of Invoices</td>
                            <td width="80" style="font-weight:bold;text-align:right">Net Value</td>                            
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater5" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="100"><%#Container.DataItem("InvoiceType")%></td>
                            <td width="120"><%#Container.DataItem("NoOfInvoices")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("TotalAmount")%></td>                           
                        </tr>
                    </table>
                </ItemTemplate>
                </asp:Repeater>
                </div>
                <p></p>
			    <div id="divServices2" runat="server" style="margin:15px">
			     <table>
			            <tr>
			                <td colspan="6"><strong style="text-decoration:underline">Services</strong></td>
			            </tr>
                        <tr>
                            <td width="220" style="font-weight:bold">Service Name</td>
                            <td width="60" style="font-weight:bold">Quantity</td>
                            <td width="80" style="font-weight:bold;text-align:right">CP</td>
                            <td width="80" style="font-weight:bold;text-align:right">WP</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin</td>
                            <td width="80" style="font-weight:bold;text-align:right">Margin (%)</td>
                        </tr>
                    </table>
                <asp:Repeater ID="Repeater6" runat="server">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td width="220"><%#Container.DataItem("ProductName")%></td>
                            <td width="60"><%#Container.DataItem("QuantitySold")%></td>
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("CustomerPrice")%></td>                           
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("WholesalePrice")%></td>                           
                            <td width="80" style="text-align:right">&pound; <%#Container.DataItem("CustomerMargin")%></td>                           
                            <td width="80" style="text-align:right"><%#Container.DataItem("PercentCustomerMargin")%></td>                           
                        </tr>
                    </table>
                    
                </ItemTemplate>
                </asp:Repeater>
                </div>
                </ItemTemplate></asp:Repeater>
                </div>
                </div>
                </asp:panel>
                    <p>&nbsp;</p></td>
                <td width="45" height="180" align="center">&nbsp;</td>
              </tr>
			
               <tr>
              <td width="45" height="60" align="center">&nbsp;</td>
              <td width="650" height="60" align="center" class="bdr3 style3"><strong>Registered Office: </strong>Montpelier Chambers, 61-63 High Street South, Dunstable, Bedfordshire LU6 3SF<br /></td>
              <td width="45" height="60" align="center">&nbsp;</td>
            </tr>
          </table>			
		</asp:Panel>
</body>
</html>

