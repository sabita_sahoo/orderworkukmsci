
Partial Public Class RefundBuyer
    Inherits System.Web.UI.Page
    Protected WithEvents gvWorkOrders, gvDebitNote As GridView
    Protected WithEvents ObjectDataSource1, ObjectDataSource2 As ObjectDataSource
    Protected WithEvents lnkBackToListing As HtmlAnchor
    Protected WithEvents btnExport As HtmlAnchor
    Protected WithEvents pnlCreditNote, pnlDebitNote As System.Web.UI.WebControls.Panel
    Protected WithEvents lblSI, lblPI As System.Web.UI.WebControls.Label
    Protected WithEvents lblMsg, lblMsgPI, lblErrMsg As System.Web.UI.WebControls.Label
    Protected WithEvents txtPartialAmount As System.Web.UI.WebControls.TextBox
    Protected WithEvents Check, CheckDebit As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSubmitData, btnSubmitDataDebit, btnCancelData, btnCancelDataDebit As System.Web.UI.WebControls.LinkButton
    Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim invoiceno As String = ""
            Dim pagesender As String = ""
            If Not IsNothing(Request("invoiceNo")) Then
                invoiceno = Request("invoiceNo")
            End If

            If Not IsNothing(Request("sender")) Then
                pagesender = Request("sender")
            End If

            If invoiceno <> "" And pagesender <> "" Then
                If pagesender = "GenCNDN" Then
                    txtSINumber.Text = invoiceno
                    'Added Condition by Pankaj Malav on 20 Nov 2008 so as to raise credit note directly
                ElseIf pagesender = "SearchFinance" Then
                    txtSINumber.Text = invoiceno
                End If
            End If
            rqDesc.Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Handles click of submit button on the first screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnSubmit.Click
        'Validating Sales invoice number has been entered
        Page.Validate()
        If Page.IsValid Then
            'Validating Proper Sales invoice
            If validateSI() Then
                If Not IsNothing(ViewState("ExistingCreditNotes")) Then
                    Dim dv As New DataView
                    dv = DirectCast(ViewState("ExistingCreditNotes"), DataTable).DefaultView

                    rptExisCN.DataSource = dv
                    rptExisCN.DataBind()
                    lblPreHdg.Text = "Previous Credit Notes"
                Else
                    rptExisCN.Visible = False
                    lblPreHdg.Text = ""

                End If
                'Hiding Input for Sales invoice
                divValidationMain.Visible = False
                tblSINumber.Visible = False
                'Showing Input for Amount
                pnlProceed.Visible = True
                'Enabling description field validator
                rqDesc.Enabled = True
                'Populating SI
                lblSINo.Text = ViewState("SI")
                'Populating Amount
                lblSIAmt.Text = ViewState("Amount")
                If Not (rdbtnPartial.Checked) Then
                    txtbxAmt.Enabled = False
                    txtbxAmt.Text = ViewState("Amount")
                End If

                lblCompName.Text = ViewState("CompanyName")

            Else
                divValidationMain.Visible = True
            End If
        Else
            divValidationMain.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' Validating sales invoice
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function validateSI() As Boolean
        Dim ds As New DataSet
        'Checking text entered in sales invoice is valid invoice number if yes then get the amount as well
        ds = ws.WSFinance.ValidateInvoices(txtSINumber.Text.Trim.ToString, "Sales")
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("SI") = ds.Tables(0).Rows(0)("InvoiceNo")
                ViewState("Amount") = ds.Tables(0).Rows(0)("Amount")
                ViewState("VatAmt") = ds.Tables(0).Rows(0)("VatAmount")
                ViewState("TotalAmt") = ds.Tables(0).Rows(0)("TotalAmt")
                ViewState("VatRate") = ds.Tables(0).Rows(0)("VatRate")
                ViewState("CompanyName") = ds.Tables(0).Rows(0)("CompanyName")
                If ds.Tables.Count > 1 Then
                    If ds.Tables(1).Rows.Count > 0 Then
                        ViewState("ExistingCreditNotes") = ds.Tables(1)
                    End If
                End If
                If Not IsNothing(ds.Tables(0).Rows(0)("Discount")) Then
                    If ds.Tables(0).Rows(0)("Discount") <> 0.0 Then
                        rdbtnFull.Enabled = False
                        rdbtnFull.Checked = False
                        rdbtnPartial.Checked = True
                        txtbxAmt.Enabled = True
                        txtbxAmt.Text = 0
                    End If
                Else
                    rdbtnFull.Enabled = True
                End If
                Return True
            Else
                'Show validation message
                lblErrMsg.Text = "Please enter valid Sales Invoice number"
                Return False
            End If
        Else
            'Show validation message
            lblErrMsg.Text = "Please enter valid Sales Invoice number"
            Return False
        End If
    End Function

    ''' <summary>
    ''' Handles change of radio button to cater for full or partial amount
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub rdbtnFull_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbtnFull.CheckedChanged
        If rdbtnFull.Checked Then
            'If full then show full amount
            txtbxAmt.Text = ViewState("Amount")
            'Disable text box
            txtbxAmt.Enabled = False
        Else
            'If partial then show 0 amount
            txtbxAmt.Text = 0.0
            'Enable text box to let the admin enter the values
            txtbxAmt.Enabled = True
        End If
    End Sub

    ''' <summary>
    ''' Handles cancel button click on second screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnCancel.Click
        tblSINumber.Visible = True
        pnlProceed.Visible = False
        lblWarningMessage.Visible = False
        'Disabling description field validator
        rqDesc.Enabled = False
    End Sub

    ''' <summary>
    ''' Handles Next button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnNext.Click
        Page.Validate()
        If IsValid() Then
            divValidationMain.Visible = False
            tblConfirmation.Visible = True
            pnlProceed.Visible = False
            Dim total As Double = 0.0
            If Not IsNothing(ViewState("ExistingCreditNotes")) Then
                Dim dv As New DataView
                dv = DirectCast(ViewState("ExistingCreditNotes"), DataTable).DefaultView
                total = dv.Table.Compute("Sum(TotalAmt)", "")
            End If

            lblConfSINo.Text = ViewState("SI")
            lblConfNetAmt.Text = ViewState("Amount")
            lblConfVatAmt.Text = ViewState("VatAmt")
            lblConfTotAmt.Text = ViewState("TotalAmt")
            lblDesc.Text = txtDescription.Text.Trim
            Dim amt As Double
            Dim vat As Double
            Try
                amt = CDec(txtbxAmt.Text.Trim)
                If amt > 0 Then
                    Dim vatrate As Double
                    If (Convert.ToDouble(ViewState("VatRate")) <= Convert.ToDouble(ApplicationSettings.VATPercentageOld) + 0.5) And (Convert.ToDouble(ViewState("VatRate")) >= Convert.ToDouble(ApplicationSettings.VATPercentageOld) - 0.5) Then
                        vatrate = ApplicationSettings.VATPercentageOld
                    Else
                        vatrate = ApplicationSettings.VATPercentage
                    End If
                    vat = FormatCurrency(amt * ((vatrate) / 100))
                Else
                    vat = 0.0
                End If
            Catch ex As Exception
                amt = 0.0
                vat = 0.0
            End Try
            total = total + amt + vat
            If total > Convert.ToDouble(ViewState("TotalAmt")) Then
                lblWarningMessage.Visible = True
                lblWarningMessage.Text = "You are crediting back more to the Client than the value of the original sales invoice.  Are you sure you wish to proceed?"
            Else
                lblWarningMessage.Visible = False
            End If
            ViewState("CNAmount") = amt
            lblConfCNNetAmt.Text = FormatCurrency(amt)
            lblConfCNVat.Text = FormatCurrency(vat)
            lblConfCNTotAmt.Text = FormatCurrency(amt + vat)
        Else
            divValidationMain.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' Handles Confirm button click on the confirmation screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnConf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnConf.Click
        If Not IsNothing(ViewState("ExistingCreditNotes")) Then
            ViewState("ExistingCreditNotes") = ""
        End If
        Try
            Dim ds As DataSet = New DataSet
            ds = ws.WSFinance.MS_RefundBuyer(ViewState("SI"), ViewState("CNAmount"), txtDescription.Text.Trim)
            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0)("InvoiceNo") <> "0") Then

                    'ScriptManager.RegisterStartupScript(Me, Me.GetType, "js", "javascript:alert('" & ds.Tables(0).Rows(0)("InvoiceNo") & "');", True)


                    'Response.Redirect("CreditNotes.aspx")


                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "openpopup", _
            "Sys.Application.add_load(function() {OpenNewWindowInvoice('" & ds.Tables(0).Rows(0)("InvoiceNo") & "','" & ApplicationSettings.OWUKBizDivId & "'); location.href='CreditNotes.aspx?Mode=CN';});", True)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try

        ' Response.Redirect("CreditNotes.aspx")
    End Sub

    ''' <summary>
    ''' Handles click of cancel button click on confirmation screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnConfCan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnConfCan.Click
        Response.Redirect("CreditNotes.aspx")
    End Sub

    ''' <summary>
    ''' Handles Back button click on confirmation screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnBack.Click
        tblConfirmation.Visible = False
        pnlProceed.Visible = True
        lblWarningMessage.Visible = False
        'Enabling description field validator
        rqDesc.Enabled = True
    End Sub
End Class
