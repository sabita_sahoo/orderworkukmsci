'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Rating

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblHeading control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHeading As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtWorkorderID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtWorkorderID As Global.WebLibrary.Input

    '''<summary>
    '''TextBoxWatermarkExtender2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxWatermarkExtender2 As Global.AjaxControlToolkit.TextBoxWatermarkExtender

    '''<summary>
    '''ddFilterRating control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddFilterRating As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lnkView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkView As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''gvRating control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvRating As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''ObjectDataSource1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ObjectDataSource1 As Global.System.Web.UI.WebControls.ObjectDataSource

    '''<summary>
    '''mdlUpdateRating control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mdlUpdateRating As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''pnlUpdateWO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlUpdateWO As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''rdBtnPositive control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdBtnPositive As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rdBtnNeutral control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdBtnNeutral As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rdBtnNegative control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdBtnNegative As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''txtComment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtComment As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''TextBoxWatermarkExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxWatermarkExtender1 As Global.AjaxControlToolkit.TextBoxWatermarkExtender

    '''<summary>
    '''ancCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ancCancel As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''hdnButton control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnButton As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnTemp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnTemp As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''UpdateProgress1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdateProgress1 As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''UpdateProgressOverlayExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdateProgressOverlayExtender1 As Global.Flan.Controls.UpdateProgressOverlayExtender
End Class
