<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SearchFinance.aspx.vb" Inherits="Admin.SearchFinance" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Search Finance"%>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="WebLibrary" Namespace="WebLibrary" TagPrefix="lib" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
	<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>	
	<%@ Import Namespace="System.Web.Script.Services" %>
    <%@ Import Namespace="System.Web.Services" %>
               <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<script language="JavaScript" type="text/JavaScript">
function PayEarlyDiscountInvoices(objid)
{
    
     var index = objid.lastIndexOf('_');  
    var identify = objid.substr(0,index); 
    
    if ((document.getElementById(identify+"_rdoPayInclDiscTotal").checked == false) && (document.getElementById(identify+"_rdoPayExclDiscTotal").checked == false))
    {
        alert("Please select any one of the options to proceed")
    }
    else
    {
        if (document.getElementById(identify+"_rdoPayInclDiscTotal").checked == true)
        {
            
            document.getElementById("hdnOnPageInvoiceNo").value = document.getElementById(identify+"_hdnInvoiceNo").value;
            document.getElementById("btnPayInclDisc").click() 
        }
        if(document.getElementById(identify+"_rdoPayExclDiscTotal").checked == true)
        {
            document.getElementById("hdnOnPageInvoiceNo").value = document.getElementById(identify+"_hdnInvoiceNo").value;
            document.getElementById("btnPayExclDisc").click()
        }
    }

}
function CallFuncToPayExclDisc()
{
     document.getElementById("btnPayInvoiceExclDisc").click()
    
}
</script>
 <div id="divContent">
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<input id="hdnPageSize" type="hidden" name="hdnPageSize" runat="server"> <input id="hdnPageNo" type="hidden" name="hdnPageNo" runat="server"><input id="sortExpr" type="hidden" name="sortExpr" runat="server"> <input id="sortOrder" type="hidden" name="sortOrder" runat="server">
			<input id="totalRecs" type="hidden" name="totalRecs" runat="server"> <input id="pageRecs" type="hidden" name="pageRecs" runat="server"><input id="defaultSortImgId" type="hidden" name="defaultSortImgId" runat="server"> <input id="criteria" type="hidden" name="criteria" runat="server">
			<input type="hidden" id="hdnWorkorderID" name="hdnWorkorderID" runat="server"><input type="hidden" id="hdnPONumber" name="hdnPONumber" runat="server">
			<input type="hidden" id="hdnKeyWord" name="hdnKeyWord" runat="server"><input type="hidden" id="hdnInvoiceNo" name="hdnInvoiceNo" runat="server"><input type="hidden" id="hdnCompanyName" name="hdnCompanyName" runat="server">
			 <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
             <ContentTemplate>
			<asp:Panel ID="pnlSearchParams" runat="server" defaultbutton="btnSearch" >
			    <div id="divParentSearch" style="margin-left:20px;">
			        <div id="divrow1" style="width:100%;margin-top:20px;">
			            <div class="formTxt" style="width:150px;">
			                Keyword
			                <asp:TextBox ID="txtKeyword" CssClass="formField150" runat="server" />
			            </div>        
			        </div>
			        <div id="divForAdvanceSearch" runat="server" visible="true">
			            <div id="divrow2" style="width:100%;margin-top:20px;">
			                <div class="formTxt" style="width:150px; float:left; margin-right:20px;">
			                    Invoice Number
			                    <lib:Input ID="txtInvoiceNo" runat="server" DataType="List"
				                    Method="GetInvoiceNo" CssClass="formField150"/>		                    			                    
			                </div>    
			                <div class="formTxt" style="width:150px; float:left; text-align:left; margin-right:20px;">
			                    Work Order ID
                                
  <asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" ForeColor="#EDEDEB" ControlToValidate="txtWorkorderID"
					            ErrorMessage="Please enter a numeric value for Work Order ID" ValidationExpression="^\d{11}-\d{1,2}|\d{11}|d$">*</asp:RegularExpressionValidator>

                                 <lib:Input ID="txtWorkorderID" runat="server" DataType="List"
				                    Method="GetWorkOrderID" CssClass="formField150"/>			                    
			                </div>    
			                <div class="formTxt" style="width:150px; float:left; margin-right:20px;">
			                    Company Name
			                    <lib:Input ID="txtCompanyName" runat="server" DataType="List"
				                    Method="GetCompanyName" CssClass="formField150"/>		                    
			                </div>    
			                <div class="formTxt" style="width:150px; float:left;margin-right:20px;">
			                    PO Number
			                    <lib:Input ID="txtPONumber" runat="server" DataType="List"
				                    Method="GetPONumber" CssClass="formField150"/>
			                </div>
			                <div class="formTxt" style="width:150px; float:left;">
			                    Invoice Date
			                    <asp:TextBox ID="txtInvoiceDate" runat="server" CssClass="formField105"></asp:TextBox>
			                   <img alt="Click to Select" src="Images/calendar.gif" id="btnFromDate" style="cursor:pointer; vertical-align:top;" />		    	
                               <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID="btnFromDate" TargetControlID="txtInvoiceDate" ID="calFromDate" runat="server"></cc1:CalendarExtender>
			                   <asp:RegularExpressionValidator Display="Dynamic" CssClass="txtOrange" ID="regStartDate" runat="server" ValidationGroup="ValidationTop" ForeColor="#EDEDEB" ControlToValidate="txtInvoiceDate" ErrorMessage="Invoice Date should be in DD/MM/YYYY Format" ValidationExpression="^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$">*</asp:RegularExpressionValidator>
			                </div> 
			            </div>
			        </div>
			        <div id="divrow4" style="width:100%;margin-top:20px;">
			          
			          <div style="clear:both"></div>
			            <div style="float:left;">
			                <asp:Button CssClass="formtxt" ID="btnSearch" runat="server" Text="Search Finance" />
                            
			            </div>	
			          
			          <div style="clear:both"></div>		            
			            <div id="divlnkAdvanceSearch" style="visibility:visible; float:left; margin-left:20px;"><a id="lnkAdvanceSearch" href="javascript:ShowAll()" runat="server" visible="false" >Advance Search</a></div>
			        </div>
			        <div id="div1" style="width:100%;margin-top:20px;">
			            <asp:Label runat="server" ID="lblSearchCriteria" CssClass="txtListing"></asp:Label>
			        </div>
			        <div id="div2" style="width:100%;margin-top:20px;">
			            <asp:ValidationSummary id="validationSummarySearch" runat="server" DisplayMode="List" CssClass="bodytxtValidationMsg"></asp:ValidationSummary>
			        </div>
			    </div>
			</asp:Panel>
			<div id="divValidationMain" visible=false class="divValidation" runat=server >
								  <div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<tr>
										  <td height="15" align="center">&nbsp;</td>
										  <td height="15">&nbsp;</td>
										  <td height="15">&nbsp;</td>
									</tr>
									<tr valign="middle">
					  						<td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
					  						<td class="validationText"><div  id="divValidationMsg"></div>
						  							<asp:Label ID="lblValMsg" runat="server"  CssClass="bodytxtValidationMsg"></asp:Label>		  	
					  						</td>
					  						<td width="20">&nbsp;</td>
									</tr>
									<tr>
											  <td height="15" align="center">&nbsp;</td>
											  <td height="15">&nbsp;</td>
											  <td height="15">&nbsp;</td>
									</tr>
				  			</table>
								  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
								</div>
			<div id="divEarlyDiscValidationGrid" runat="server">
			 <div id="div4" class="divredBorder">
						  <div class="roundtabRed"><img src="../Images/Curves/TabRed-LC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
						</div>
			 <asp:GridView ID="gvEarlyDiscInvoices" runat=server AllowPaging="False"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
						   Width="100%"  AllowSorting="false" PagerSettings-Visible="false" >          
						   
									   <Columns>  
												 
															
										<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="140px"   HeaderStyle-CssClass="gridHdr" DataField="InvoiceNo" HeaderText="Sales Invoice Number" />                
										<asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="150px" HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="Date Issued" />                                    
									
										<asp:TemplateField HeaderText="Net Amount" >               
									   <HeaderStyle CssClass="gridHdr" Width="150px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
									   <ItemTemplate>  
									   <%#FormatCurrency(Container.DataItem("InvoiceNetAmnt"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   	<asp:TemplateField HeaderText="Discount" >               
									   <HeaderStyle CssClass="gridHdr" Width="150px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
									   <ItemTemplate>  
									   <%#FormatCurrency(Container.DataItem("Discount"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   <asp:TemplateField HeaderText="Total Amt Incl. Discount" >               
									   <HeaderStyle CssClass="gridHdr" Width="200px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
									   <ItemTemplate>  
									   <asp:RadioButton runat="server" ID="rdoPayInclDiscTotal" GroupName="PayInvoice" />
									   <%#FormatCurrency(Container.DataItem("InclDiscTotal"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   <asp:TemplateField HeaderText="Total Amt Excl. Discount" >               
									   <HeaderStyle CssClass="gridHdr" Width="200px"  Wrap=true />
									   <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
									   <ItemTemplate>  
									   <asp:RadioButton runat="server" ID="rdoPayExclDiscTotal" GroupName="PayInvoice" />
									   <%#FormatCurrency(Container.DataItem("ExclDiscTotal"), 2, TriState.True, TriState.True, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   <asp:TemplateField >               
									   <HeaderStyle CssClass="gridHdr" Width="100px"   Wrap=true /> 
									  <ItemStyle Wrap=true HorizontalAlign=Left   CssClass="gridRow"/>
									   <ItemTemplate>   
                                            <input type=hidden runat=server id='hdnInvoiceNo'  value='<%#Container.DataItem("InvoiceNo") %>'/>
                                            <a id="lnkPayEarlyDisc" class="footerTxtSelected" runat="server" onclick='javascript:PayEarlyDiscountInvoices(this.id)' href="#" >Pay</a>
									   </ItemTemplate>
									   </asp:TemplateField>
									   
									   </Columns>
										 <AlternatingRowStyle    CssClass=gridRow />
										 <RowStyle CssClass=gridRow />            

										<HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
										<FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
								
						    </asp:GridView>
			                    <asp:Button Height="0" Width="0" BorderWidth="0px" ID="btnPayInclDisc" runat="server" OnClick="PayInclDisc" />
                                <asp:Button Height="0" Width="0" BorderWidth="0px" ID="btnPayExclDisc" runat="server" OnClick="PayInvoiceExclDiscount" />
                                <asp:textbox style="height:0px; width:0px; border:none 0px;" runat="server" id='hdnOnPageInvoiceNo'></asp:textbox>
</div>
                                <asp:Button Height="0" Width="0" BorderWidth="0px" ID="btnPayInvoiceExclDisc" runat="server" OnClick="PayInvoiceExclDiscount" />

			  <asp:Panel ID="pnlSearchResult" runat=server Visible=false>
         <table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td>	  <asp:GridView ID="gvFinance"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
               PagerSettings-Mode=NextPreviousFirstLast CssClass="gridrow"  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               
               <PagerTemplate>
                   
                  
                 <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>
							<td runat="server" visible="false" id="tdsetType1" width="90"><TABLE border="0" cellPadding="0" cellSpacing="0" width="90">
							  <TR>
								<TD class="txtListing" align="left"> 
							<div id="divButton" style="width: 115px;  ">
									  <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
									  <a  runat="server" target="_blank" id="btnExport" tabIndex="6" class="buttonText"> <strong>Export to Excel</strong></a>
									  <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
								    </div>
							
								</TD>
								<TD width="5"></TD>
							  </TR>
							</TABLE>						
							</td>
							
							
							<td width="10">&nbsp;</td>
						
						  
						  <td id="tdsetType2" align="left" width="121">
								
							</td>
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
        </PagerTemplate> 
               <Columns>  
                    <asp:BoundField  ItemStyle-CssClass="gridRow" ItemStyle-Width="18%"  HeaderStyle-CssClass="gridHdr"  DataField="InvoiceNumber" SortExpression="InvoiceNumber" HeaderText="Invoice Number" />            
                    <asp:TemplateField ItemStyle-Width="10%" SortExpression="InvoiceDate" HeaderText="Invoice Date" HeaderStyle-CssClass="gridHdr"  >               
                    <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow" />
                        <ItemTemplate>             
                            <%#Strings.FormatDateTime(Container.DataItem("InvoiceDate"), DateFormat.ShortDate)%>
                        </ItemTemplate>
                    </asp:TemplateField>                     
                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="10%"  HeaderStyle-CssClass="gridHdr"  DataField="InvoiceType" SortExpression="InvoiceType" HeaderText="Invoice Type" />            
                    <asp:BoundField ItemStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr"  DataField="CompanyName" SortExpression="CompanyName" HeaderText="Company Name" />            
                  
                    <asp:TemplateField SortExpression="InvoiceValue" HeaderText="Invoice Value" >               
									   <HeaderStyle CssClass="gridHdr"/>
                                    <ItemStyle CssClass="gridRow"/>

									   <ItemTemplate>  
										<%#Strings.FormatNumber(Container.DataItem("InvoiceValue"), 2, TriState.False, TriState.False, TriState.False)%>                       
									   </ItemTemplate>
									   </asp:TemplateField>

                    <asp:BoundField ItemStyle-CssClass="gridRow" HeaderStyle-CssClass="gridHdr"  DataField="InvoiceStatus" SortExpression="InvoiceStatus" HeaderText="Invoice Status" />            
                    <asp:TemplateField  SortExpression="Discount" HeaderText="Fee(s) Applied" ItemStyle-Width="8%">               
                    <HeaderStyle  CssClass="gridHdr" />
                    <ItemStyle CssClass="gridRow"/>
                        <ItemTemplate>             
                            <%#(Container.DataItem("Discount"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--'Poonam modified on 31-03-2017 - Task - OA-427 - OA - Unsupported spaces in Admin users --%>  
                    <asp:TemplateField ItemStyle-Width="10%">               
                    <HeaderStyle  CssClass="gridHdr" />
                    <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow"/>
                        <ItemTemplate>           
                         <%#GetLinks(Container.DataItem("InvoiceNumber"), Container.DataItem("CompanyID"), Container.DataItem("WorkOrderId"), Container.DataItem("InvoiceType"), Container.DataItem("AllocatedTo"), Container.DataItem("WOID"), Container.DataItem("InvoiceStatus"), "txtInvoiceNo=" & txtInvoiceNo.Text & "&txtKeyword=" & (txtKeyword.Text) & "&txtWorkorderID=" & txtWorkorderID.Text & "&txtCompanyName=" & (txtCompanyName.Text.Replace("&", " and ")) & "&txtPONumber=" & (txtPONumber.Text) & "&txtInvoiceDate=" & txtInvoiceDate.Text)%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="5%">               
                    <HeaderStyle  CssClass="gridHdr" />
                    <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow"/>
                        <ItemTemplate>             
                            <asp:ImageButton ID="ImgBtnPay" Visible='<%#iif(Session("RoleGroupID")<>admin.Applicationsettings.RoleOWRepID , iif(Container.dataitem("InvoiceType")="Sales", iif(Container.dataitem("InvoiceStatus")="Unpaid",true,false), false),false) %>' AlternateText="Mark SI as Paid" CommandArgument='<%#Container.dataitem("InvoiceNumber") & "," & Container.DataItem("CompanyID")%>' CommandName="PaySI" runat="server" ImageUrl="Images/Icons/Icon-Confirm.gif" ></asp:ImageButton>
                            <asp:ImageButton ID="ImgBtnPIAv" Visible='<%#iif(Session("RoleGroupID")<>admin.Applicationsettings.RoleOWRepID , iif(Container.dataitem("InvoiceType")="Purchase", iif(Container.dataitem("InvoiceStatus")="Unpaid" or Container.dataitem("InvoiceStatus")="On Hold",true,false), false),false) %>' AlternateText="Mark PI as Available" CommandArgument='<%#Container.dataitem("InvoiceNumber") %>' CausesValidation="false" OnClick="ImgBtnPIAv_Click" runat="server" Imageurl="Images/Icons/MakeInvoiceAvailable.gif" Height=13 Width=14 ></asp:ImageButton>
                            <asp:ImageButton ID="ImgBtnCNPay" Visible='<%#iif(Session("RoleGroupID")<>admin.Applicationsettings.RoleOWRepID , iif(Container.dataitem("InvoiceType")="Credit Note", iif(Container.dataitem("InvoiceStatus")="UnPaid",true,false), false),false) %>' AlternateText="Mark Credit Note as Paid" CommandArgument='<%#Container.dataitem("InvoiceNumber") & "," & Container.DataItem("CompanyID")%>' CommandName="PayCN" runat="server" Imageurl="Images/Icons/Icon-Confirm.gif" ></asp:ImageButton>
                        </ItemTemplate>
                    </asp:TemplateField>                               
                </Columns>
               
                 <AlternatingRowStyle  CssClass=gridRow />
                 <RowStyle CssClass=gridRow />            
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
        
              </asp:GridView>
				 </td>
            </tr>
            </table>
				
				 <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.SearchFinance" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
          </asp:Panel>
          </ContentTemplate>
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                Loading ...
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
                
 <cc1:ModalPopupExtender ID="mdlSIDatePaid" runat="server" TargetControlID="btnTemp" PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
				      
<asp:Panel runat="server" ID="pnlConfirm" Width="250px" CssClass="pnlConfirm" style="display:none;">
    <div id="divModalParent">
        <span style="font-family:Tahoma; font-size:14px;"><b>Enter Payment Date</b></span><br /><br />
        <asp:TextBox runat="server" ID="txtDate" Height="20" Width="200" CssClass="bodyTextGreyLeftAligned" ></asp:TextBox>                                
        <img alt="Click to Select" src="Images/calendar.gif" id="btnBeginDate" style="cursor:pointer; vertical-align:top;" />		                               
         <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=btnBeginDate TargetControlID="txtDate" ID="calBeginDate" runat="server" ></cc1:CalendarExtender>
            <div id="divButton" class="divButton" style="width: 85px; float:left;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                    <a class="buttonText cursorHand" onclick='javascript:validate_required("ctl00_ContentPlaceHolder1_txtDate","Please enter a date")' id="ancSubmit">Submit</a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div>
            <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="ancCancel"><strong>Cancel</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>	
    <div id="divProgressDate" style="display:none">
                <img  align="middle" src="Images/indicator.gif" />
                Processing ...
            </div>  									
</asp:Panel>
<asp:Button runat="server" ID="hdnButton" Height="0" Width="0" BorderWidth="0" />
<asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" />

<cc1:ModalPopupExtender ID="mdlCNDatePaid" runat="server" TargetControlID="btnCNPaid" PopupControlID="pnlCNPaid" OkControlID="" CancelControlID="a2" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
				      
<asp:Panel runat="server" ID="pnlCNPaid" Width="250px" CssClass="pnlConfirm" style="display:none;">
    <div id="divModalPopUp">
        <span style="font-family:Tahoma; font-size:14px;"><b>Enter Payment Date</b></span><br /><br />
        <asp:TextBox runat="server" ID="txtCNDate" Height="20" Width="200" CssClass="bodyTextGreyLeftAligned" ></asp:TextBox>                                
        <img alt="Click to Select" src="Images/calendar.gif" id="Img1" style="cursor:pointer; vertical-align:top;" />		                               
         <cc1:CalendarExtender Format="dd/MM/yyyy" CssClass="calendarBox"  PopupButtonID=Img1 TargetControlID="txtCNDate" ID="CalendarExtender1" runat="server" ></cc1:CalendarExtender>
            <div id="divButton" class="divButton" style="width: 85px; float:left;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                    <a class="buttonText cursorHand" onclick='javascript:validateCN_required("ctl00_ContentPlaceHolder1_txtCNDate","Please enter a date")' id="a1">Submit</a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div>
            <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;">
                <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                    <a class="buttonText cursorHand" runat="server" id="a2"><strong>Cancel</strong></a>
                <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
            </div> 
    </div>	
    <div id="divShowProgress" style="display:none">
                <img  align="middle" src="Images/indicator.gif" />
                Processing ...
            </div>  							
</asp:Panel>
<asp:Button runat="server" ID="hdnButtonCN" Height="0" Width="0" BorderWidth="0" />
<asp:Button runat="server" ID="btnCNPaid" Height="0" Width="0" BorderWidth="0" />
                
            <!-- InstanceEndEditable -->            
            </td>
			   
		 
         </tr>
         </table>
      </div>
      </asp:Content>
