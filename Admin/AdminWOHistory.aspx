<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :" Codebehind="AdminWOHistory.aspx.vb" Inherits="Admin.AdminWOHistory" %>



    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
		<table class="gridBorderB" width="100%"  border="0" cellpadding="10" cellspacing="0" bgcolor="#DAD8D9">
                      <tr>
					     <td>
							<div id="divButton" class="divButton" style="width: 115px;">
								<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
									<a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" alt="Back to Listing" width="4" height="8" hspace="0" vspace="0" border="0" class="marginR8"><strong>Back to Listing</strong></a>
								<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
							</div>
						</td>
                          <td width="176px" >
							<div id="divButton" class="divButtonCreate" style="width: 176px; height:21px">
								<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
									<a class="buttonText" href="WOForm.aspx" ><img src="Images/Icons/Icon-NewOrder.gif" width="13" height="13" hspace="0" vspace="0" border="0" align="absmiddle" class="marginR8"><strong>Create Work Request</strong></a>
								<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
							</div>
						</td>
					 </tr>
         </table>
         <%@ Register TagPrefix="uc1" TagName="UCAdminWOHistory" Src="~/UserControls/Admin/UCAdminWOHistoryUK.ascx" %>
         <uc1:UCAdminWOHistory id="UCAdminWOHistory1" runat="server"></uc1:UCAdminWOHistory>
        
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
      
      </asp:Content>