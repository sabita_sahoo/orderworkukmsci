'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AdminClientSLAReport

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''hdnCompID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCompID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnStartDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnStartDate As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnToDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnToDate As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''divDateRange control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divDateRange As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lnkView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkView As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''TdPrint control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TdPrint As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''btnPrint control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPrint As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''TdExpToExc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TdExpToExc As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''btnExport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExport As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''divMainReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divMainReport As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblSubmittedWO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSubmittedWO As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClosedWO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClosedWO As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOnTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOnTime As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSLAPercentageMet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSLAPercentageMet As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFirstTimeSuccess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFirstTimeSuccess As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCancellationNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCancellationNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''divProduct control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divProduct As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''DLProducts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DLProducts As Global.System.Web.UI.WebControls.DataList

    '''<summary>
    '''divBillingLoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divBillingLoc As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''DLBillingLoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DLBillingLoc As Global.System.Web.UI.WebControls.DataList

    '''<summary>
    '''tblNoRecords control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblNoRecords As Global.System.Web.UI.HtmlControls.HtmlTable
End Class
