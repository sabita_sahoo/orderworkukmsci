<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UpSellSalesInvoicePaid.aspx.vb" Inherits="Admin.UpSellSalesInvoicePaid" MasterPageFile="~/MasterPage/OWAdmin.Master" Title="OrderWork : Paid UpSell Invoice"%>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
			<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10px"></td>
			<td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Up Sell Invoices" runat="server"></asp:Label></td>
			</tr>
			</table>			
            	<asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline>
             <ContentTemplate>
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
		<td width="10px"></td>
		<td width="300"><table width="100%" cellpadding="0" cellspacing="0" border="0">
			 <tr>
			   <td width="300">
			     <%@ Register TagPrefix="uc1" TagName="UCDateRange" Src="~/UserControls/UK/UCDateRangeUK.ascx" %>
                 <uc1:UCDateRange id="UCDateRange1" runat="server"></uc1:UCDateRange> 
			   </td>
			  </tr>
			</table>
		</td>
		<td align="left">
            <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                <asp:LinkButton ID="lnkView" causesValidation=False OnClick="lnkView_Click" runat="server" CssClass="txtButtonRed" Text="View"></asp:LinkButton>
            </div>
		</td>
			 <td align="left">
                <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                    <a runat="server" target="_blank" id="btnExport" class="txtButtonRed"> &nbsp;Export to Excel&nbsp;</a>
                </div>
		    </td>
		</tr>
		</table>	
		
	
           <table><tr>
             <td class="paddingL30 padB21" valign="top" padB21 paddingTop1>
		<asp:Label ID="lblMsg" runat="server"  CssClass="bodytxtValidationMsg"></asp:Label>		  	
		</td></tr>
             </table>
	   <hr style="background-color:#993366;height:5px;margin:0px;"/>
           	<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td>
              <asp:GridView ID="gvInvoices"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
               PagerSettings-Mode=NextPreviousFirstLast  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
               <EmptyDataTemplate>
               <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
						<tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
							</td>
						</tr>				
					</table>
                  
               </EmptyDataTemplate>
               
               <PagerTemplate>
                   
            <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
						<tr>																				
							<td width="10">&nbsp;</td>												  
						  					
							<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right" runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
													</div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
        </PagerTemplate> 
               <Columns>  
                           
                <asp:BoundField ItemStyle-CssClass="gridRow"  ItemStyle-Width="180px" HeaderStyle-CssClass="gridHdr"  DataFormatString="{0:dd/MM/yy}"  HtmlEncode=false  DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="Date Issued" />                    
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="InvoiceNo" SortExpression="InvoiceNo" HeaderText="Invoice No" />                
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="CashReceiptNo" SortExpression="CashReceiptNo" HeaderText="Cash Receipt No." />                
                
                <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" DataField="ClientCompanyName" SortExpression="ClientCompanyName" HeaderText="Client Name" />                
                <asp:TemplateField SortExpression="InvoiceTotal" HeaderText="Total Amount" >               
               <HeaderStyle CssClass="gridHdr" Width="200px"  Wrap=true />
               <ItemStyle Wrap=true HorizontalAlign=Left  Width="200px"  CssClass="gridRow"/>
               <ItemTemplate>   
                <%#FormatCurrency(Container.DataItem("InvoiceTotal"), 2, TriState.True, TriState.True, TriState.False)%>                       
               </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="200px"   HeaderStyle-CssClass="gridHdr" SortExpression="EndCustomerName" DataField="EndCustomerName"  HeaderText="Customer Name" /> 

               <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">                
                <ItemStyle CssClass="gridRowIcons" Wrap=true  Width="14px"/>
                <ItemTemplate> 
				<a runat=server id="lnkAdminWODetails" href=<%#"~/AdminWODetails.aspx?WOID=" & Container.DataItem("WOID") & "&BizDivID=" & Container.DataItem("BizDivId") & "&CompanyId=0" & "&" & "PS=" & gvInvoices.PageSize & "&" & "PN=" & gvInvoices.PageIndex & "&" & "SC=" & gvInvoices.SortExpression & "&" & "SO=" & gvInvoices.SortDirection & "&" & "sender=UpSellSalesInvoicePaid" & "&" & "FromDate=" & UCDateRange1.txtFromDate.Text & "&" & "ToDate=" & UCDateRange1.txtToDate.Text & "&Group=Invoiced" %>><img src='Images/Icons/ViewDetails.gif' alt='view Work Order details' width='16' height='16' hspace='2' vspace='0' border='0' align='absmiddle'></a>
				 
				
				</ItemTemplate>  
               </asp:TemplateField>

               <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">                
                <ItemStyle CssClass="gridRowIcons" Wrap=true  Width="14px"/>
                <ItemTemplate> 
				<a runat=server target="_blank" id="lnkViewSalesInvoice" href=<%#"~/ViewUpSellSalesInvoice.aspx?invoiceNo=" & Container.DataItem("invoiceNo") & "&CompanyId=0" & "&bizDivId=" & Container.DataItem("bizDivId") %>><img src='Images/Icons/View-Sales-Invoice.gif' alt='View Invoices' width='13' height='15' hspace='2' vspace='0' border='0' align='absmiddle'></a>
				</ItemTemplate>  
               </asp:TemplateField>

               <asp:TemplateField visible=true HeaderStyle-CssClass="gridHdr">                
                <ItemStyle CssClass="gridRowIcons" Wrap=true  Width="14px"/>
                <ItemTemplate> 
				<a runat=server target="_blank" id="lnkViewCashReceipt" href=<%#"~/ViewUpSellCashReceipt.aspx?invoiceNo=" & Container.DataItem("invoiceNo") & "&CompanyId=0" & "&bizDivId=" & Container.DataItem("bizDivId") %>><img src='Images/Icons/View-Cash-Receipt.gif' alt='View Cash Receipt' width='15' height='14' hspace='2' vspace='0' border='0' align='absmiddle'></a>
				</ItemTemplate>  
               </asp:TemplateField>
                                
				</Columns>
               
                 <AlternatingRowStyle    CssClass=gridRow />
                 <RowStyle CssClass=gridRow />            
                <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
                <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
        
              </asp:GridView>
				 </td>
            </tr>
            </table>
				
				 <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.UpSellSalesInvoicePaid" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
          </asp:Panel>
          </ContentTemplate>
          <Triggers>
              <asp:AsyncPostBackTrigger   ControlID="lnkView"  EventName="Click" /> 
              </Triggers>
             </asp:UpdatePanel>
              <asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server">
                <ProgressTemplate >
            <div>
                <img  align=middle src="Images/indicator.gif" />
                  <b>Fetching Data... Please Wait</b>
            </div>      
        </ProgressTemplate>
                </asp:UpdateProgress>
                <cc2:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" />
            <!-- InstanceEndEditable --></td>
         </tr>
         </table>
      </div>
      </asp:Content>

