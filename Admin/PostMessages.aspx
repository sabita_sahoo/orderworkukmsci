<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :Post Messages"  CodeBehind="PostMessages.aspx.vb" Inherits="Admin.PostMessages" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<script type="text/javascript">
    function ConfirmPostMessage(id) {
        var isEmailSend = document.getElementById('ctl00_ContentPlaceHolder1_chkSendMail').checked;
        if (isEmailSend == true) {
            var conf = confirm("Once you click OK here, email will be sent to Suppliers");
            if (conf == true) {
                document.getElementById('ctl00_ContentPlaceHolder1_btnConfirmPostMessage').click();
            }
        }
        else {
            document.getElementById('ctl00_ContentPlaceHolder1_btnConfirmPostMessage').click();
        }
    }
function ValidateFunction()
{
        
}
</script>




			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            <asp:UpdatePanel ID="UpdatePanelMultipleWO" runat="server">
                <ContentTemplate>
                
                <div id="divValidationMain" class="divValidation" runat="server" visible="false" style="margin:15px">
				<div class="roundtopVal"><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				  <table width="96%"  border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15">&nbsp;</td>
					  <td height="15">&nbsp;</td>
					</tr>
					<tr valign="middle">
					  <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
					  <td width="565" class="validationText"><div  id="divValidationMsg"></div>
					   <span class="validationText">
					  	<asp:ValidationSummary ID="validationSummarySubmit" runat="server" CssClass="bodytxtValidationMsg" DisplayMode="BulletList" EnableClientScript="false"></asp:ValidationSummary>
					  </span><br />
					   <asp:Label CssClass="validationText" Visible="false" style="color:#ff0000;" runat="server" ID="lblErr"></asp:Label>
					  </td>
					  <td width="20">&nbsp;</td>
					</tr>
					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15"><input type="button" id="btnFocus" name="btnFocus" class="hdnClass"  /></td>
					  <td height="15">&nbsp;</td>
					</tr>

					<tr>
					  <td height="15" align="center">&nbsp;</td>
					  <td width="565" height="15"><asp:Label ID="lblErrMsg" CssClass="HeadingRed" Text=""  runat="server"></asp:Label></td>
					  <td height="15">&nbsp;</td>
					</tr>

				  </table>  
				  <div class="roundbottomVal"><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
				</div>
                    <asp:Panel ID="pnlMultipleWO" runat="server">
                    
              <div style="margin-left:15px; margin-top:5px; font-size:12px;">
                <p class="paddingB4 HeadingRed"><strong>Post Messages</strong> </p>
                <p><asp:CheckBox CssClass="formTxt"  ID="chkMsgClients" runat="server"  Text="Clients" Enabled="false" />  <asp:CheckBox  CssClass="formTxt" ID="chkMsgServicePartners" runat="server" Text="Suppliers" Checked="true" AutoPostBack="true"  /></p>
                <asp:panel runat="server" ID="pnlSkillsArea" style="padding-left:55px;">
                   <%--<asp:CheckBox CssClass="formTxt" Checked="true"  ID="chkBusinessIT" runat="server"    Text="Business IT" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <asp:CheckBox CssClass="formTxt" Checked="true" ID="chkConsumerElectronicandTV" runat="server"   Text="Consumer Electronic and TV" /> <br />
                   <asp:CheckBox CssClass="formTxt" Checked="true" ID="chkFreesatandDigitalAerials" runat="server"  Text="Freesat and Digital Aerial" />
                   <asp:CheckBox CssClass="formTxt" Checked="true" ID="chkHomePCInstallations" runat="server"    Text="Home PC Installations" /> <br />--%>
                  
                  <div style="width:940px;">
                   <asp:Repeater ID="rptMessagesAOE" runat="server">
                    <ItemTemplate>                        
                        <asp:CheckBox ID="Check" runat="server" Checked=<%#IIF(Container.DataItem("SelectedAOE") <> 0,"true","false") %> ></asp:CheckBox>
                        <input type=hidden runat=server id="hdnWOProductID" value='<%#Container.DataItem("WOProductID")%>'/> <%#Eval("MainCatName")%>
                    </ItemTemplate>    
                   </asp:Repeater> 
                   </div>

                </asp:panel>
                <asp:Panel ID="pnlComments" runat="server">
                    <div style=" margin-top:10px;">
                        <table visible="true" width="100%" border="0" cellspacing="0" cellpadding="0">
                         <tr>
			               <td id="tdDateRange" runat="server" width="285px;" style="padding-right:5px;" align="left">
			               <table cellpadding="0" cellspacing="0" border="0">
			                 <tr>
			                   <td>			          
			                     <%@ Register TagPrefix="uc1" TagName="UCDateRange" Src="~/UserControls/UK/UCDateRangeUK.ascx" %>
                                 <uc1:UCDateRange id="UCDateRange1" runat="server"></uc1:UCDateRange> 
			                   </td>
			                  </tr>
			                  </table>
		                     </td>    	
			             </tr>
                        <tr>
	                       <td height="20" valign="bottom" class="formLabelGrey" >Subject &nbsp;<span class="bodytxtValidationMsg">*</span>
	                       <asp:RequiredFieldValidator id="rfvSubject" runat="server" ErrorMessage="Please enter the subject" ForeColor="#EDEDEB"
                            ControlToValidate="txtSubject">*</asp:RequiredFieldValidator>
	                       </td>
	                     </tr>
	                     <tr>
	                       <td><asp:TextBox id="txtSubject" runat="server" CssClass="formFieldGrey" style="width:300px;" Height="18px" MaxLength="100"></asp:TextBox></td>
	                     </tr>
	                     <tr>
	                       <td height="20" valign="bottom" class="formLabelGrey" >Message &nbsp;<span class="bodytxtValidationMsg">*</span>
	                       <asp:RequiredFieldValidator id="rfvMessage" runat="server" ErrorMessage="Please enter text for message body" ForeColor="#EDEDEB"
                            ControlToValidate="txtMessage">*</asp:RequiredFieldValidator>
	                       </td>
	                     </tr>
	                     <tr>
	                       <td><asp:TextBox id="txtMessage" runat="server" CssClass="formFieldGrey" style="width:400px;" TextMode="MultiLine"   onblur="javascript:removeHTML(this.id);" Rows="4" MaxLength="2000"></asp:TextBox></td>
	                     </tr>
	                    </table>
                    </div>
                </asp:Panel>
                
                <div style=" margin-top:10px;">                
                 <asp:CheckBox CssClass="formTxt" ID="chkIsImp" runat="server"    Text="Mark As important" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:CheckBox CssClass="formTxt" ID="chkIsDisplayLogin" runat="server"    Text="Always Display on login" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:CheckBox CssClass="formTxt" ID="chkSendMail" runat="server" Text="Send an email to Suppliers" /> 
                </div>
                <div style=" margin-top:10px;">
                
                    <div style="width:60px; float:left;">
                      <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                        <asp:LinkButton id="lnkbtnBack" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false">&nbsp;Back&nbsp;</asp:LinkButton>
                      </div>
                      </div>
                      <div style="width:100px; float:left;margin-left:10px;">
                        <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:100px;">
                            <a class="txtButtonRed" id="ancPostMessage" runat="server" onclick="Javascript:ConfirmPostMessage(this.id)" causesvalidation="false">&nbsp;Post Message&nbsp;</a>
                             <asp:Button ID="btnConfirmPostMessage" runat="server" CausesValidation="false" Text="" Style="background-color: White; border: 0px none; padding: 0px; width: 0px;
                            height: 0px;" />
                            <%--<asp:LinkButton id="lnkbtnPostMessage" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false">&nbsp;Post Message&nbsp;</asp:LinkButton>--%>
                        </div>
                      </div>
                     <div style="width:60px; float:left; margin-left:10px;">
                      <div style="cursor:pointer;-moz-border-radius: 5px; border-radius:5px;background-color:#993366 !important;color:#FFFFFF;text-align:center;width:60px;">
                        <asp:LinkButton id="lnkbtnReset" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false">&nbsp;Reset&nbsp;</asp:LinkButton>
                      </div>
                      </div>
                      <div style="float:left; margin-left:10px;">
                        <asp:CheckBox  ID="chkMarkDeleted" runat="server" CssClass="formTxt" Text="Mark as deleted"/>
                      </div>
                    
                </div>
                
                
            </div>
                </asp:Panel>
            </ContentTemplate>
             
            </asp:UpdatePanel>
            
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
<asp:UpdateProgress  ID="UpdateProgress2" AssociatedUpdatePanelID="UpdatePanelMultipleWO" runat="server">
                <ProgressTemplate>
                    <div class="gridText">
                        <img  align=middle src="Images/indicator.gif" />
                        <b>Please wait saving data...</b>
                    </div>      
                </ProgressTemplate>
            </asp:UpdateProgress>
        <cc2:UpdateProgressOverlayExtender ID="UpdateProgressOverlayExtender2" ControlToOverlayID="pnlMultipleWO" CssClass="updateProgress" TargetControlID="UpdateProgress2" runat="server" />
</asp:Content>