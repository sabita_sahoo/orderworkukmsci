<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GenerateCNDN.aspx.vb" Inherits="Admin.GenerateCNDN" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
    <%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">  
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/AdminTemplate.dwt" codeOutsideHTMLIsLocked="false" -->
<head runat=server>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>OrderWork : GenerateCNDN</title>
<!-- InstanceEndEditable --><!-- InstanceParam name="opRgNewsletter" type="boolean" value="true" --><!-- InstanceParam name="opRgClient" type="boolean" value="false" --><!-- InstanceParam name="opRgSupplier" type="boolean" value="false" --><!-- InstanceParam name="opLogo" type="boolean" value="true" --><!-- InstanceParam name="opHomeFlash" type="boolean" value="false" -->
<script language="JavaScript" type="text/JavaScript" src="JS/Scripts.js"></script>
<script language="javascript" src="JS/Scripts.js"  type="text/javascript"></script>
<!-- InstanceParam name="OpMenuRedTopCurves" type="boolean" value="true" --><!-- InstanceParam name="OpMenuRedBtmCurve" type="boolean" value="true" --><!-- InstanceParam name="OpMenuOrangeTopCurve" type="boolean" value="false" --><!-- InstanceParam name="OpMenuBtmOrangeCurve" type="boolean" value="false" --><!-- InstanceParam name="onLoad" type="text" value="" --><!-- InstanceParam name="OpQLRegion" type="boolean" value="false" --><!-- InstanceParam name="OpContentTopCurve" type="boolean" value="true" --><!-- InstanceParam name="OpUCMenu" type="boolean" value="true" --><!-- InstanceParam name="OpContentTopMargin" type="boolean" value="false" --><!-- InstanceParam name="OpAddSpecialist" type="boolean" value="false" --><!-- InstanceParam name="OpAddLocations" type="boolean" value="false" --><!-- InstanceParam name="OpLogoMessage" type="boolean" value="true" --><!-- InstanceParam name="SpacNavAdminHome" type="boolean" value="false" --><!-- InstanceParam name="OpUCSubMenu" type="boolean" value="true" --><!-- InstanceParam name="OpRgLogin" type="boolean" value="false" --><!-- InstanceParam name="OpRgSupplierLogin" type="boolean" value="false" --><!-- InstanceParam name="OpRgClientLogin" type="boolean" value="false" -->
<link href="Styles/OrderWork.css" rel="stylesheet" type="text/css">
<link href="Styles/OrderWork.css" rel="stylesheet" type="text/css">
<link href="Styles/Menu.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<style>
#divMenu {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
	line-height: 12px;
	font-weight: normal;
	text-decoration: none;
	width: 100%;
	background-position: right top;
	background-repeat: no-repeat;
	border-top-width: 1px;
	border-top-style: solid; 
	border-top-color: #CECECE;
	background-color: #993366;
}

.divContentTopRed{
	width: 100%;
	background-color: #993366;
	background: url(Images/Curves/Red-RTC.gif) no-repeat right top;
	height: 5px;
	
}
</style>
<!-- InstanceEndEditable --><!-- InstanceParam name="opAccountSumm" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddUser" type="boolean" value="false" --><!-- InstanceParam name="OpRgCLAddLOcation" type="boolean" value="false" --><!-- InstanceParam name="OPWOSummary" type="boolean" value="false" --><!-- InstanceParam name="RgOpBtnNewWO" type="boolean" value="false" --><!-- InstanceParam name="OpRgAddFunds" type="boolean" value="false" --><!-- InstanceParam name="opMenuRegion" type="boolean" value="true" --><!-- InstanceParam name="opContentTopRedCurve" type="boolean" value="false" --><!-- InstanceParam name="opUsersProfile" type="boolean" value="false" --><!-- InstanceParam name="opCompanyProfile" type="boolean" value="true" --><!-- InstanceParam name="opUsersListing" type="boolean" value="false" --><!-- InstanceParam name="opLocations" type="boolean" value="false" --><!-- InstanceParam name="opReferences" type="boolean" value="false" --><!-- InstanceParam name="opPreferences" type="boolean" value="false" --><!-- InstanceParam name="opUsers" type="boolean" value="false" --><!-- InstanceParam name="width" type="text" value="985" --><!-- InstanceParam name="opComments" type="boolean" value="false" --><!-- InstanceParam name="OpRgScriptManager" type="boolean" value="true" --><!-- InstanceParam name="OpFavSuppliers" type="boolean" value="false" --><!-- InstanceParam name="OpFavSuppliersLink" type="boolean" value="false" -->
</head>



<body onLoad=";">
<form id="Form1" method="post" runat="server">

 
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>

<table width="985" height="100%"  border="0" align="center" cellpadding="0" cellspacing="0"> 
  <tr>
    <td valign="top" bgcolor="#f4f5f0">
	
	<div id="divLogo">
      <div class="roundtopSwitch"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
      <table width="100%" height="54" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><!-- InstanceBeginEditable name="EdLogo" --><img src="Images/OrderWorks-Logo-Beta.gif" alt="OrderWork - Trading IT Skills Across the UK" width="345" height="18" border="0" class="LogoImg" /><!-- InstanceEndEditable --></td>
          <td align="right" valign="bottom" class="LogoTopMsg">
		  <%@ Register TagPrefix="uc2" TagName="UCSwitchAdmin" Src="~/UserControls/UK/UCMSSwitchAdminUK.ascx" %>
          <uc2:UCSwitchAdmin id="UCSwitchAdmin1" runat="server"></uc2:UCSwitchAdmin>
          <%@ Register  TagPrefix="uc1" TagName="UCTopMSgLabel" Src="~/UserControls/UK/UCTopMSgLabelUK.ascx" %>
          <uc1:UCTopMSgLabel id="UCTopMSgLabel1" runat="server"></uc1:UCTopMSgLabel></a></td>
          <td width="19" align="right" valign="bottom" class="footerTxt">&nbsp;</td>
        </tr>
      </table>
      <div class="roundbottomSwitch"><img src="Images/Curves/Main-LBC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
    </div>
    
    <div id="divMenu"> 
      <div class="roundtopRed"><img src="Images/Curves/Red-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
       
      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="26" ><strong> 
           <%@ Register TagPrefix="uc1" TagName="UCMenuMainAdmin" Src="~/UserControls/Admin/UCMenuMainAdmin.ascx" %>
            <uc1:UCMenuMainAdmin id="UCMenuMainAdmin1" runat="server"></uc1:UCMenuMainAdmin></strong> </td>
        </tr>
      </table>
      
      <div class="roundbottomRed"><img src="Images/Curves/Red-LBC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
    </div>
     <div id="divDDMenu"> 
	        <%@ Register TagPrefix="uc1" TagName="UCMenuSubAdmin" Src="~/UserControls/Admin/UCMenuSubAdmin.ascx" %> 
            <uc1:UCMenuSubAdmin id="UCMenuSubAdmin" runat="server"></uc1:UCMenuSubAdmin>
		<script language="JavaScript" type="text/JavaScript" src="JS/Menu.js"></script> 
		  </div>
	        
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" --><table width="100%" cellpadding="0" cellspacing="0">
			    <tr>
			    <td width="10px"></td>
			    <td><asp:Label ID="lblHeading" CssClass="HeadingRed" Text="Credit/Debit Confirmation"  runat="server"></asp:Label></td>
			    </tr>
			    </table>
		   
				
              <p class="txtHeading">&nbsp;                </p>
			  <asp:Panel runat="server" ID="pnlCreditNote" Visible="true">
			   
					<table width="100%" cellpadding="0" cellspacing="0">
					 <tr>
						<td width="10px"></td>			
						<td class="HeadingRed"><asp:Label ID="lblSI" runat=server></asp:Label></td>
					 </tr>
			        </table>			
	   			<div id="div1" class="divredBorder">
              		<div class="roundtabRed"><img src="../Images/Curves/TabRed-LC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
            	</div>
           	<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
            	<tr>
            		<td>
					  <asp:GridView ID="gvWorkOrders"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
					   PagerSettings-Mode=NextPreviousFirstLast  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true>          
					   <EmptyDataTemplate>
						   <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
									<tr>
										<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
										</td>
									</tr>				
								</table>
                 
	               		</EmptyDataTemplate>
               
               			<PagerTemplate>
                   
            			<table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
							<tr>																				
								<td width="10">&nbsp;</td>												  
						  					
								<td>
								<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
								  <tr>
									<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
										<TR>
											<td align="right" valign="middle">
											  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right" runat="server" class="formLabelGrey"></div>
											</td>
											<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
												<td align="right" width="36">
												<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
												  </div>	
													<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
													</div>																							   
												</td>
												<td width="50" align="center" style="margin-right:3px; ">													
													<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField" ID="ddlPageSelector" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
												</td>
												<td width="30" valign="bottom">
													<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
													</div>
													<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
													 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
													</div>
												</td>
												</tr>
											</table>
											</td>
										</TR>
									</TABLE></td>
								  </tr>
								</table>
						  </td>
						</tr>
					</table> 
                   
       				 </PagerTemplate> 
					   <Columns>  

						<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="65px"   HeaderStyle-CssClass="gridHdr" DataField="hdnRefWOID" HeaderText="WorkOrderID" />    
						<asp:BoundField ItemStyle-CssClass="gridRow"  HeaderStyle-CssClass="gridHdr" DataField="hdnWOTitle" HeaderText="WO Title" />                                            
						<asp:BoundField ItemStyle-CssClass="gridRow"  HeaderStyle-CssClass="gridHdr" DataField="Amount" HeaderText="Credit Net Amount" />
						<asp:BoundField ItemStyle-CssClass="gridRow"  HeaderStyle-CssClass="gridHdr" DataField="Vat" HeaderText="Credit VAT amount" />
						<asp:BoundField ItemStyle-CssClass="gridRow"  HeaderStyle-CssClass="gridHdr" DataField="Total" HeaderText="Credit Total amount" /> 

						</Columns>
					   
						 <AlternatingRowStyle    CssClass=gridRow />
						 <RowStyle CssClass=gridRow />            
						<PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
						<HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
						<FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
				
					  </asp:GridView>
				 </td>
            </tr>
		</table>
			
			
				
				 <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.GenerateCNDN" SortParameterName="sortExpression">                
            </asp:ObjectDataSource>
			 </asp:Panel>


              <p class="txtHeading">&nbsp;                </p>
			  <asp:Panel runat="server" ID="pnlDebitNote" Visible="true">

		            <table width="100%" cellpadding="0" cellspacing="0" border="0">
					  <tr>
						<td width="10px"></td>
					  </tr>
					</table>

					<table width="100%" cellpadding="0" cellspacing="0">
					 <tr>
						<td width="10px"></td>			
						<td class="HeadingRed"><asp:Label ID="lblPI" runat=server></asp:Label></td>
					 </tr>
			        </table>			
					<div id="div1" class="divredBorder">
						<div class="roundtabRed"><img src="../Images/Curves/TabRed-LC.gif" alt="" width="5" height="5" class="corner" style="DISPLAY: none"></div>
					</div>
					
					<table width="100%"  align=center border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>
										  <asp:GridView ID="gvDebitNote"  runat="server" AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
										   PagerSettings-Mode=NextPreviousFirstLast  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource2"   AllowSorting=true>          
										   <EmptyDataTemplate>
											   <table  width="100%" border="0" cellpadding="10" cellspacing="0">					
														<tr>
															<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category.
															</td>
														</tr>				
													</table>
									 
											</EmptyDataTemplate>
								   
											<PagerTemplate>
									   
											<table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
												<tr>																				
													<td width="10">&nbsp;</td>												  
																
													<td>
													<table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
													  <tr>
														<td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
															<TR>
																<td align="right" valign="middle">
																  <div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right" runat="server" class="formLabelGrey"></div>
																</td>
																<td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server">
																<table width="100%" border="0" cellpadding="0" cellspacing="0">
																	<tr>
																	<td align="right" width="36">
																	<div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																		<asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />
																	  </div>	
																		<div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																		<asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />
																		</div>																							   
																	</td>
																	<td width="50" align="center" style="margin-right:3px; ">													
																		<asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged1" CssClass="formField" ID="ddlPageSelector" runat="server" AutoPostBack="true">
																		</asp:DropDownList>
																	</td>
																	<td width="30" valign="bottom">
																		<div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
																		 <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />
																		</div>
																		<div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
																		 <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />
																		</div>
																	</td>
																	</tr>
																</table>
																</td>
															</TR>
														</TABLE></td>
													  </tr>
													</table>
											  </td>
											</tr>
										</table> 
									   
										 </PagerTemplate> 
										   <Columns>  
											    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="65px"   HeaderStyle-CssClass="gridHdr" DataField="SupplierName" HeaderText="Supplier" />    
											    <asp:BoundField ItemStyle-CssClass="gridRow"  HeaderStyle-CssClass="gridHdr" DataField="WorkOrderId" HeaderText="WorkOrderID" />
											    <asp:BoundField ItemStyle-CssClass="gridRow"  HeaderStyle-CssClass="gridHdr" DataField="WOTitle" HeaderText="WorkOrder Title" />
											    <asp:BoundField ItemStyle-CssClass="gridRow"  HeaderStyle-CssClass="gridHdr" DataField="WOVal" HeaderText="Debit Net Amount" />
											    <asp:BoundField ItemStyle-CssClass="gridRow"  HeaderStyle-CssClass="gridHdr" DataField="Discount" HeaderText="Debit disc Amount" />
											    <asp:BoundField ItemStyle-CssClass="gridRow"  HeaderStyle-CssClass="gridHdr" DataField="Vat" HeaderText="Debit Total VAT amount" />
                                                <asp:BoundField ItemStyle-CssClass="gridRow"  HeaderStyle-CssClass="gridHdr" DataField="Total" HeaderText="Debit Total amount" />											                                            
											</Columns>
										   
											 <AlternatingRowStyle    CssClass=gridRow />
											 <RowStyle CssClass=gridRow />            
											<PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />
											<HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />
											<FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />
									
										  </asp:GridView>
									 </td>
								</tr>
							</table>					
							 <asp:ObjectDataSource  ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB1"  EnablePaging="True" SelectCountMethod="SelectCount1" TypeName="Admin.GenerateCNDN" SortParameterName="sortExpression">                
						</asp:ObjectDataSource>					
			 </asp:Panel>			 

            <p class="txtHeading">&nbsp;                </p> 
           	<table width="100%"  align="center" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" class="HeadingRed" style="padding:0px 0px 0px 10px;"><asp:Label ID="lblMsg" Visible="true" Text="Warning:  This process is not reversible once the Submit is clicked" runat="server" style="text-align:center;"></asp:Label></td>
			</tr>

			<tr>
				<td align="right">
						<table cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
							 <tr height="20px">
								<td></td>
								<td></td>
								<td></td>
							 </tr>              
							<tr valign="top">
							  <td align="left">&nbsp;</td>
							  <td width="70" align="left" id="tdSubmitData" runat=server visible="false">
								  <table cellspacing="0" cellpadding="0" width="60" bgcolor="#993366" border="0">
									  <tr>
										<td height="18"><img height="18" src="Images/Curves/Red-BtnLeft.gif" width="5" /></td>
										<td><asp:LinkButton id="btnSubmitData" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Submit&nbsp;</asp:LinkButton></td>
										<td width="5"><img height="18" src="Images/Curves/Red-BtnRight.gif" width="5" /></td>
									  </tr>
								  </table>
							  </td>
							  <td align="left" width="10">&nbsp;</td>
							  <td width="70" align="left" id="tdCancelData" runat=server visible="false">
								<table cellspacing="0" cellpadding="0" width="60" bgcolor="#993366" border="0">
									<tr>
									  <td height="18"><img height="18" src="Images/Curves/Red-BtnLeft.gif" width="5" /></td>
									  <td><asp:LinkButton id="btnCancelData" tabIndex="2" runat="server" CssClass="txtButtonRed" CausesValidation="false"> &nbsp;Back&nbsp;</asp:LinkButton></td>
									  <td width="5"><img height="18" src="Images/Curves/Red-BtnRight.gif" width="5" /></td>  
									</tr>
								</table>
							</tr>
						  </table>
				</td>
			</tr>
			</table>
			           
         
		
            <!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div></td>
  </tr>
  <tr>
    <td height="51" valign="top" bgcolor="#f4f5f0"><table width="945" height="46"  border="0" align="center" cellpadding="0" cellspacing="0" class="BdrTop">
      <tr valign="top">
        <td width="135" align="left" bgcolor="#f4f5f0" class="footerTxt"><img src="Images/Phone-Icon.gif" alt="Phone - 0203 053 0343" width="15" height="12" align="absmiddle" class="marginR5" /> 0203 053 0343 </td>
        <td align="left" bgcolor="#f4f5f0" class="footerTxt"><a href="mailto:info@orderwork.co.uk" class="footerTxtSelected"><img src="Images/Email-Icon.gif" alt="Contact Us" width="16" height="15" border="0" align="absmiddle" class="marginR5" />&nbsp; info@orderwork.co.uk</a></td>
        <td align="right" bgcolor="#f4f5f0" class="footerTxt"><!-- InstanceBeginEditable name="EdPrivacyPolicy" --><a href="https://www.orderwork.co.uk/privacy-policy/" target="_blank" class="footerTxtSelected">Policies</a><!-- InstanceEndEditable --></td>
      </tr>
    </table>	
    <div class="roundbottomWhite"><img src="Images/Curves/Main-LBC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div></td>
  </tr>
  <tr>
    <td height="35" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="footerTxt"><span >Copyright &copy; <script language="javascript" type="text/javascript">
									var currDate = new Date();
									document.write(currDate.getFullYear());
									</script> OrderWork Ltd.</span></td>
        <td align="right" class="footerTxt"> <!-- Design &amp; Developed by<a href="http://www.iniquus.com" target="_blank" class="footerTxt"> Iniquus</a> --></td>
      </tr>
    </table></td>
  </tr>
</table>
</form>
</body>
<!-- InstanceEnd --></html>
