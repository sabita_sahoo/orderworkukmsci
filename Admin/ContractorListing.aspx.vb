﻿
Partial Public Class ContractorListing
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        UserAction.userAutoLoginAccept(Page)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)
            gvContractor.Sort("DateCreated", SortDirection.Descending)
            gvContractor.PageSize = ApplicationSettings.GridViewPageSize
            HttpContext.Current.Session("HideDeleted") = chkHideDeleted.Checked
            PopulateStandards()
        End If
    End Sub
    ''' <summary>
    ''' Populating Standards
    ''' Category, Sub Category
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateStandards()
        Dim dsXML As New DataSet()
        dsXML.ReadXml(MapPath("~/XML/WOFormStandards.xml"))
        'get the dataview of table "Country", which is default table name
        Dim dv_WOType As DataView = dsXML.Tables("MainCat").DefaultView
        'Dim dv_FreesatMake As DataView = dsXML.Tables("FreesatMake").DefaultView

        ''Populating Freesat Make model
        'drpdwnFreesatMake.DataSource = dv_FreesatMake
        'drpdwnFreesatMake.DataBind()

        'now define datatext field and datavalue field of dropdownlist
        ddlWOType.DataTextField = "Name"
        ddlWOType.DataValueField = "CombId"
        ddlWOType.DataSource = dv_WOType
        ddlWOType.DataBind()

        Dim li2 As New ListItem
        li2 = New ListItem
        li2.Text = ResourceMessageText.GetString("WOCategoryList")
        li2.Value = ""
        ddlWOType.Items.Insert(0, li2)
    End Sub
    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvContractor.DataBind()

        End If
    End Sub

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim HideDeletedAccounts As Integer
        If Not HttpContext.Current.Session("HideDeleted") Is Nothing Then
            If HttpContext.Current.Session("HideDeleted") = "True" Then
                HideDeletedAccounts = 1
            Else
                HideDeletedAccounts = 0
            End If
        End If
        Dim ds As DataSet = ws.WSContact.GetContractor(sortExpression, startRowIndex, maximumRows, HttpContext.Current.Session("AOE"), HideDeletedAccounts)
        HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
        Return ds
    End Function

    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvContractor.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub
    Private Sub ObjectDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles ObjectDataSource1.Selecting
        InitializeGridParameters()
    End Sub

    Private Sub InitializeGridParameters()
        HttpContext.Current.Session("AOE") = hdnSubCategoryVal.Value
        HttpContext.Current.Items("HideDeleted") = chkHideDeleted.Checked
    End Sub

    Private Sub gvContractor_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvContractor.PreRender
        Me.gvContractor.Controls(0).Controls(Me.gvContractor.Controls(0).Controls.Count - 1).Visible = True
        If ViewState("rowCount") < 1 Then
            Return
        End If
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)
            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

            End If
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContractor.RowDataBound
        MakeGridViewHeaderClickable(gvContractor, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContractor.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvContractor, e.Row, Me)
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)
        Dim btnExport As HtmlAnchor = CType(gvPagerRow.FindControl("btnExport"), HtmlAnchor)

        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    Public Function chkForNull(ByVal DateCreated As Object) As Object
        Dim DateCreatedDate As Object = DateCreated
        If Not IsDBNull(DateCreatedDate) Then
            Return Strings.FormatDateTime(DateCreatedDate, DateFormat.ShortDate)
        Else
            Return "Not Yet Logged in "
        End If
    End Function

    
    ''' <summary>
    ''' To return dispplay path information of the attached file
    ''' </summary>
    ''' <param name="filePath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getlink(ByVal filePath As String)
        If Not filePath Is Nothing Then
            If filePath <> "" Then
                Dim index As Integer = filePath.IndexOf("Attachment")

                Dim str As String = filePath.Substring(index)
                'str = ApplicationSettings.AttachmentDisplayPath(ApplicationSettings.OWUKBizDivId) + str
                str = ApplicationSettings.AzureStoragePath.ToString + "OWMYUK/" + str + CommonFunctions.AzureStorageAccessKey().ToString
                Return str.Replace("\", "/")
            End If
        End If
        Return ""
    End Function

    Public Sub lnkSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSearch.Click, chkHideDeleted.CheckedChanged
        HttpContext.Current.Session("AOE") = hdnSubCategoryVal.Value
        HttpContext.Current.Session("HideDeleted") = chkHideDeleted.Checked
        gvContractor.PageIndex = 0
        PopulateGrid()
    End Sub
    
    Private Sub gvContractor_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvContractor.RowCommand
        Dim ds As DataSet
        If (e.CommandName = "Del") Then
            ds = ws.WSContact.ShowDeleteContractor(CInt(e.CommandArgument), 1)
        ElseIf (e.CommandName = "Show") Then
            ds = ws.WSContact.ShowDeleteContractor(CInt(e.CommandArgument), 0)
        End If
        HttpContext.Current.Session("AOE") = hdnSubCategoryVal.Value
        HttpContext.Current.Session("HideDeleted") = chkHideDeleted.Checked
        gvContractor.PageIndex = 0
        PopulateGrid()
    End Sub
End Class