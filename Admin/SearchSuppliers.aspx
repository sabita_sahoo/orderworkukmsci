<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Search Suppliers" CodeBehind="SearchSuppliers.aspx.vb" Inherits="Admin.SearchSuppliers" %>
<%@ Register Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="Microsoft.Web.UI.WebControls" TagPrefix="iewc" %>
<%@ Register Assembly="Flan.Controls" Namespace="Flan.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

			<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" -->
            <INPUT name="hdnRegionName" type="hidden" id="hdnRegionName" runat="server">
            <INPUT name="hdnRegionValue" type="hidden" id="hdnRegionValue" runat="server">
            <INPUT name="hdnProductIds" type="hidden" id="hdnProductIds" runat="server">
			
			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 15px 0px; margin:0px; ">
					<TR>
					  <td width="170" class="txtWelcome"><div id="divButton" style="width: 170px;  ">
						  <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
						  <a class="buttonText" runat="server" id="lnkBackToListing"><img src="Images/Arrows/Arrow-Back.gif" title="Back to Listing" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8"><strong>Back to Company Profile</strong></a>
						  <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
					  </div></td>                  
					</TR>
		   </TABLE>
		   
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode=Inline> 
			<ContentTemplate>
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="20">&nbsp;</td>
                <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td><asp:Label ID="lblBuyerCompany" runat="server" CssClass="HeadingRed"></asp:Label></td>
                    </tr>
					 <tr>
					  <td>&nbsp;</td>
					</tr>
                    <tr>
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr valign="top">
                            <td ><TABLE cellSpacing="0" cellPadding="0" width="474" border="0">
                                <TR vAlign="top">
                                  <TD class="formTxt padRight20Left20" align="left" width="202" height="14"><strong>Select Regions</strong></TD>
                                  <TD class="formTxt" align="left" height="30">&nbsp;</TD>
                                  <TD class="formTxt" align="left" width="202" height="14">&nbsp;</TD>
                                </TR>
                                <tr>
                                  <td width="273" class="padRight20Left20"><%@ Register TagPrefix="uc3" TagName="UCDualListBox" Src="~/UserControls/UK/UCDualListBox.ascx" %>
                                      <uc3:UCDualListBox id="UCRegions" runat="server"></uc3:UCDualListBox> </td>
                                </tr>
                            </TABLE></td>
                            <td rowspan="3" align="left" valign=top><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr valign="bottom">
                                  <td width="293" height="15"><span class="formTxt">Keyword</span></td>
                                </tr>
                                <tr>
                                  <td width="293"><asp:TextBox ID="txtKeyword" CssClass="formField200" runat="server" /></td>
                                </tr>
                                <tr>
                                  <td width="182" height="15"><span class="formTxt">PostCode</span></td>
                                </tr>
                                <tr>
                                  <td><asp:TextBox ID="txtPostCode" onkeyup="makeUppercase(this.id)" MaxLength="8" CssClass="formField200" runat="server" /></td>
                                </tr>
                                <tr>
                                  <td><span class="formTxt">First Name</span></td>
                                </tr>
                                <tr>
                                  <td><asp:TextBox ID="txtFName"  CssClass="formField200" runat="server" /></td>
                                </tr>
                                <tr>
                                  <td><span class="formTxt">Last Name</span></td>
                                </tr>
                                <tr>
                                  <td><asp:TextBox ID="txtLName"  CssClass="formField200" runat="server" /></td>
                                </tr>
                                <tr>
                                  <td><span class="formTxt">Company Name</span></td>
                                </tr>
                                <tr>
                                  <td><asp:TextBox ID="txtCompanyName"  CssClass="formField200" runat="server" /></td>
                                </tr>
                                <tr>
                                  <td><span class="formTxt">Email</span></td>
                                </tr>
                                <tr>
                                  <td><asp:TextBox ID="txtEmail"  CssClass="formField200" runat="server" /></td>
                                </tr>
                                <tr valign="bottom">
                                  <td width="293" height="25"><asp:CheckBox id="chkRefChecked" tabIndex="7" runat="server" CssClass="formTxt" TextAlign="right"
																								Text="Reference Checked"></asp:CheckBox></td>
                                </tr>
                            </table></td>
                          </tr>
						  <tr>
						  <td>&nbsp;</td>
						  </tr>
                          <tr valign="top">
                            <td><table>
                                <tr>
                                  <td class="padRight20Left20" ><span class="formTxt"><strong>Area of Expertise </strong></span></td>
                                </tr>
                                <tr>
                                  <td valign="top" class="padRight20Left20" ><%@ Register TagPrefix="uc1" TagName="UCAOE" Src="~/UserControls/UK/UCAOEUK.ascx" %>
                                      <uc1:UCAOE id="UCAOE1" runat="server"></uc1:UCAOE> </td>
                                </tr>
                            </table></td>
                          </tr>
                          <tr valign="top">
                            <td align="right" colspan="2" width="30%" style="padding-right:110px "><asp:Button CssClass="formtxt"  ID="btnSearch" OnClick="Search_Click" runat="server" Text="Search for Accounts" /></td>
                          </tr>
                          <tr valign="top">
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                          </tr>
                          <tr valign="top">
                            <td colspan="2" class="txtListing "><asp:Label runat="server" ID="lblSearchCriteria" CssClass="txtListing"></asp:Label></td>
                          </tr>
                      </table></td>
                    </tr>
                  </table>
                    <span align="center"><b>
                    <asp:label runat="server" id="lblMsgSent" ></asp:label>
                  </b></span></td>
                <td width="20">&nbsp;</td>
              </tr>
            </table>
            <asp:Panel ID="pnlSearchResult" runat=server Visible=false>
              <table width="98%"  align=center border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
				  
				  <asp:GridView ID="gvContacts"  runat=server AllowPaging="True"  AutoGenerateColumns="False" ForeColor="#333333" BorderWidth="1px" PageSize=25 BorderColor="White"  
                    PagerSettings-Mode=NextPreviousFirstLast CssClass="gridrow"  PagerSettings-Position=TopAndBottom Width="100%" DataSourceID="ObjectDataSource1"   AllowSorting=true> 
					<EmptyDataTemplate>
						<table  width="100%" border="0" cellpadding="10" cellspacing="0">
						  <tr>
							<td align="center" style="text-align:center" valign="middle" height="300" class="txtWelcome">Sorry! There are no records in this category. </td>
						  </tr>
						</table>
					</EmptyDataTemplate> 
					<PagerTemplate>
                    <table width="100%" height="35" border="0" cellpadding="5" cellspacing="0" bgcolor="#DAD8D9" id="tblMainTop" style="visibility:visible " runat="server">
                      <tr>
                      <td runat="server"  id="tdMark" width="150" align="left" class="txtWelcome paddingL10"><div id="divButton" style="width: 130px;  ">
							<div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>								
							<asp:LinkButton OnClick="MarkAsFavourite" class="buttonText" style="cursor:hand;" id="btnMark"  runat=server><strong>Mark as Favourite</strong></asp:LinkButton>
						   <cc2:ConfirmButtonExtender ID="ConfirmBtnMark" TargetControlID="btnMark" ConfirmText="Do you want to add the Supplier to Client's favourite list?" runat="server">
							</cc2:ConfirmButtonExtender> 
							<div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
						</div></td>
						<td width="10">&nbsp;</td>
						<td runat="server"  id="tdExport" width="150" align="right" class="txtWelcome paddingL10"><div id="divButton" style="width: 115px;  ">
							  <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
							  <a  runat="server" target="_blank" id="btnExport" tabIndex="6" class="buttonText"> <strong>Export to Excel</strong></a>
							  <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
							</div></td>
					  <td width="10">&nbsp;</td>
										  
                        <td id="tdsetType2" align="left" width="121"></td>
                        <td><table width="100%" id="tblTop" runat="server"  border="0" cellpadding="0" cellspacing="0" bgcolor="#DAD8D9">
                            <tr>
                              <td align="right" ><TABLE width="100%" border="0" cellPadding="0" cellSpacing="0" >
                                  <TR>
                                    <td align="right" valign="middle"><div id="tdTotalCount" style="width:200px; padding-right:20px; text-align:right " runat="server" class="formLabelGrey"></div></td>
                                    <td align="right" width="136" valign="middle" id="tdPagerUp"  runat="server"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                          <td align="right" width="36"><div id="divDoubleBackUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
                                              <asp:ImageButton ID="btnFirst" CommandName="Page" ImageUrl="~/Images/Icons/first.gif" CommandArgument="First" runat="server" />            
                                </div>
                                              <div id="divBackUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
                                                <asp:ImageButton ID="btnPrevious" CommandName="Page" ImageUrl="~/Images/Icons/back.gif" CommandArgument="Prev" runat="server" />              
                              </div></td>
                                          <td width="50" align="center" style="margin-right:3px; "><asp:DropDownList style="margin-left:4px;" OnSelectedIndexChanged="ddlPageSelector_SelectedIndexChanged" CssClass="formField40 marginLR5" ID="ddlPageSelector" runat="server" AutoPostBack="true"> </asp:DropDownList>
                                          </td>
                                          <td width="30" valign="bottom"><div id="divNextUp" runat="server" style="float:left; vertical-align:bottom; padding-top:2px; ">
                                              <asp:ImageButton ID="btnNext" CommandName="Page" ImageUrl="~/Images/Icons/next.gif" CommandArgument="Next" runat="server" />            
                                </div>
                                              <div id="divDoubleNextUp" runat="server" style="float:right; vertical-align:bottom; padding-top:2px; ">
                                                <asp:ImageButton ID="btnLast" CommandName="Page" ImageUrl="~/Images/Icons/last.gif" CommandArgument="Last" runat="server" />              
                              </div></td>
                                        </tr>
                                    </table></td>
                                  </TR>
                              </TABLE></td>
                            </tr>
                        </table></td>
                      </tr>
                    </table>
                    </PagerTemplate>
                    <Columns>
					
					<asp:TemplateField HeaderText=" <input id='chkSelectAll' onClick=CheckAll(this,'Check') type='checkbox' name='chkSelectAll' />" HeaderStyle-CssClass="gridHdr" >
				   <ItemStyle Wrap=true HorizontalAlign=Left Width="15px"  CssClass="gridRow"/>
					   <ItemTemplate>             
						   <asp:CheckBox ID="Check" runat="server" ></asp:CheckBox>
							<input type=hidden runat=server id="hdnMainContactId"  value='<%#Container.DataItem("CompanyId")%>'/>
					   </ItemTemplate>
				   </asp:TemplateField> 
					

                    <asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px"  HeaderStyle-CssClass="gridHdr"  DataField="CompanyName" SortExpression="CompanyName" HeaderText="Company" /> 
					<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="100px"  HeaderStyle-CssClass="gridHdr"  DataField="Name" SortExpression="Name" HeaderText="Administrator Name" /> 
					<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px"  HeaderStyle-CssClass="gridHdr"  DataField="Email" SortExpression="Email" HeaderText="Administrator Email" /> 
					<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px"  HeaderStyle-CssClass="gridHdr"  DataField="Location" SortExpression="Location" HeaderText="Location" /> 
					<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="75px"  HeaderStyle-CssClass="gridHdr"  DataField="Phone" SortExpression="Phone" HeaderText="Phone" /> 
					<asp:TemplateField SortExpression="Rating"   HeaderText="Rating">
                    <HeaderStyle CssClass="gridHdr" Width="50px"  Wrap=true  />            
                    <ItemStyle Wrap=true HorizontalAlign=Left  Width="15px"  CssClass="gridRow"/>            
						<ItemTemplate>
							<%#Container.DataItem("RatingScore").ToString().Replace(".00","") & "%"%> 
						</ItemTemplate>
                    </asp:TemplateField> 
					<asp:BoundField ItemStyle-CssClass="gridRow" ItemStyle-Width="50px"  HeaderStyle-CssClass="gridHdr"  DataField="Status" SortExpression="Status" HeaderText="Status" /> 
					
					<asp:TemplateField SortExpression="RefCheckedDate" Visible=false   HeaderText="Reference Checked">
                    <HeaderStyle CssClass="gridHdr" Width="50px"  Wrap=true />            
                    <ItemStyle Wrap=true HorizontalAlign=Left  Width="15px"  CssClass="gridRow"/>            
						<ItemTemplate>
							<%#SetRefCheckedStatus(Container.DataItem("RefCheckedDate"))%> 
						</ItemTemplate>
                    </asp:TemplateField> 
					
					<asp:TemplateField ItemStyle-Width="20px">
                    <HeaderStyle  CssClass="gridHdr" />            
                    <ItemStyle Wrap=true HorizontalAlign=Left CssClass="gridRow"/>            
						<ItemTemplate> 
							<%#GetLinks(Container.DataItem("BizDivID"), Container.DataItem("CompanyID"), Container.DataItem("ContactID"), Container.DataItem("ClassID"), Container.DataItem("StatusID"), Container.DataItem("RoleGroupID"),Container.DataItem("CompanyName"),Container.DataItem("Name"),Container.DataItem("RefCheckedDate"),Container.DataItem("Email"))%> 
						</ItemTemplate>
                    </asp:TemplateField>
                    
					</Columns>
                    <AlternatingRowStyle    CssClass=gridRow /> 
					<RowStyle CssClass=gridRow />
                    <PagerStyle BorderWidth="0px"  BackColor="#D7D7D7" ForeColor="White" HorizontalAlign="Center" />            
                    <HeaderStyle  CssClass="gridHdr"  HorizontalAlign=Left VerticalAlign=Top  Height="40px" />            
                    <FooterStyle  BackColor="#D7D7D7"  Font-Bold="True" ForeColor="White" />            
				   </asp:GridView> </td>
                </tr>
              </table>
              <asp:ObjectDataSource  ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"  SelectMethod="SelectFromDB"  EnablePaging="True" SelectCountMethod="SelectCount" TypeName="Admin.SearchSuppliers" SortParameterName="sortExpression"> </asp:ObjectDataSource> 
		 </asp:Panel>
		 
		 <table width="100%"  border="0" cellspacing="0" cellpadding="0">
		 		<tr>
                <td height="20">&nbsp;</td>
                <td height="20">&nbsp;</td>
                <td height="20">&nbsp;</td>
              </tr>
              <tr>
                <td width="20">&nbsp;</td>
                <td valign="top">
                    <span align="center"><b>
                    <asp:label runat="server" id="lblMessage" ></asp:label>
                  </b></span></td>
                <td width="20">&nbsp;</td>
              </tr>
            </table>
		 
         </ContentTemplate> 
		</asp:UpdatePanel> 
		<asp:UpdateProgress    AssociatedUpdatePanelID="UpdatePanel1" ID="UpdateProgress1" runat="server"> 
		<ProgressTemplate >
            <div class="gridText"> 
				<img  align=middle src="Images/indicator.gif" /> <b>Fetching Data... Please Wait</b> 
			</div>
        </ProgressTemplate> 
	 </asp:UpdateProgress> 
	 
	<cc1:UpdateProgressOverlayExtender ControlToOverlayID="UpdatePanel1" TargetControlID="UpdateProgress1" CssClass="updateProgress" ID="UpdateProgressOverlayExtender1" runat="server" /> 
	
	<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>
      </div>
  
      </asp:Content>