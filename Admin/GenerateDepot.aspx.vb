
Partial Public Class GenerateDepot
    Inherits System.Web.UI.Page
    Shared ws As New WSObjs
    Public objEmail As New Emails

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack = True Then
            If Request.QueryString("mode") <> Nothing Then
                ViewState.Add("mode", Request.QueryString("mode"))
                ViewState.Add("WOID", Request.QueryString("WOID"))
                If Not IsNothing("IsNextDay") Then
                    ViewState.Add("IsNextDay", Request.QueryString("IsNextDay"))
                Else
                    ViewState.Add("IsNextDay", "No")
                End If

                'lnkPrint.HRef = "PrintDepotSheets.aspx?WOID=" & ViewState("WOID") & "&ContactId=" & Session("ContactId")
            End If
            GenerateDepotSheet()
        End If
    End Sub

    Public Sub GenerateDepotSheet()
        Dim ds As New DataSet
        ds = ws.WSWorkOrder.WO_GenerateDepot(ViewState("WOID"))
        If ds.Tables("tblDepot").Rows.Count > 0 Then
            pnlMultipleWO.Visible = True
            pnlNoRecords.Visible = False
            dlDepotSheet.DataSource = ds.Tables("tblDepot")
            dlDepotSheet.DataBind()
        Else
            pnlMultipleWO.Visible = False
            pnlNoRecords.Visible = True
        End If

    End Sub

    'Public Sub lnkEmail_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmail.ServerClick
    '    
    'End Sub

    Public Sub lnkbtnCancel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnCancel.ServerClick, btnNoRecords.ServerClick
        Response.Redirect("AdminWOListing.aspx?mode=" & ViewState("mode") & "&IsNextDay=" & ViewState("IsNextDay"))
    End Sub

    Private Sub lnkEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmail.Click, lnkPrint.Click
        ViewState("DepotClick") = CType(sender, LinkButton).CommandName.ToString
        CType(Master.FindControl("ContentPlaceHolder1").FindControl("mdlCollectionDate"), AjaxControlToolkit.ModalPopupExtender).Show()
        txtDate.Text = Strings.FormatDateTime(Date.Today.AddDays(1), DateFormat.ShortDate)
    End Sub

    Private Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click
        If ViewState("DepotClick") = "PrintDepotSheets" Then
            'or you can use if you don't want to size it
            Dim strurl As String
            strurl = "PrintDepotSheets.aspx?WOID=" & ViewState("WOID") & "&ContactId=" & Session("ContactId") & "&CDate=" & txtDate.Text.Trim
            ResponseHelper.Redirect(strurl, "_blank", "")
        Else
            Dim ds As New DataSet
            ds = ws.WSWorkOrder.WO_PrintDepotSheets(ViewState("WOID"), CInt(Session("UserId")), "mail")
            If ds.Tables.Count > 2 Then

                If ds.Tables(2).Rows.Count > 0 Then
                    Dim dvEmail As New DataView
                    dvEmail = ds.Tables(2).DefaultView
                    Emails.DepotMail(ds, dvEmail, txtDate.Text.Trim)
                End If
                Response.Redirect("AdminWOListing.aspx?mode=" & ViewState("mode") & "&IsNextDay=" & ViewState("IsNextDay"))
            End If
        End If
    End Sub
End Class