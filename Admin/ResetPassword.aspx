<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork :" CodeBehind="ResetPassword.aspx.vb" Inherits="Admin.ResetPassword" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>


	        
			
	        <div id="divContent">
         
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="marginT11B35">
         
         <tr>
            <td valign="top"><!-- InstanceBeginEditable name="EdtContent" --> 
              <table width="100%"  border="0" cellspacing="0" cellpadding="15">
                <tr>
                  <td><strong>Reset Password </strong></td>
                </tr>
                <tr>
                  <td><P>Enter Email :<BR>
                        <asp:TextBox id="txtEmailtoReset" runat="server"></asp:TextBox>
                       <asp:RegularExpressionValidator ID="rgEmail" runat="server" ControlToValidate="txtEmailtoReset" ErrorMessage="Please enter valid Email Address" ValidationExpression="^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,}$"></asp:RegularExpressionValidator>
                  </P>
                    <P>
                      <asp:Button id="btnResetPassword" runat="server" Text="Reset Password" onclick=btnResetPassword_Click></asp:Button>
                      <BR>
                    </P>
                    <P>
                      <asp:Label id="lblMsg"  runat="server"></asp:Label>
                    </P></td>
                </tr>
              </table>
              <P>&nbsp;</P>
			<!-- InstanceEndEditable --></td>
			   
		 
         </tr>
         </table>


         <cc1:ModalPopupExtender ID="mdlPONumber" runat="server" TargetControlID="btnTemp" PopupControlID="pnlConfirm" OkControlID="" CancelControlID="ancCancel" BackgroundCssClass="modalBackground" Drag="False" DropShadow="False">
  </cc1:ModalPopupExtender>
    <asp:Panel runat="server" ID="pnlConfirm" Width="250px" CssClass="pnlConfirm" style="display:none;">
        <div id="divModalParent">
            <span style="font-family:Tahoma; font-size:14px;"><b>Reset Password</b></span><br /><br />           
                <table>
                    <tr>
                        <td valign="top" align="left">
                            <asp:Label runat="server" Width="120px" ID="lblPasswordReset" Font-Bold="true" Text="Set New Password : "></asp:Label>
                        </td>
                        <td valign="top" align="left">
                            <asp:TextBox runat="server" Width="120px" ID="txtPasswordReset" CssClass="bodyTextGreyLeftAligned" ></asp:TextBox><br /><br />
                       </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblHelpText" runat="server" ForeColor="Red" Font-Bold="true" text="Please leave blank if you want to generate a random password (Only certain accounts allow you to set the password manually)"></asp:Label>
                            
                        </td>
                    </tr>
                </table>                
            <br />
                <div id="divButton" class="divButton" style="width: 100px; float:left;cursor:pointer;">
                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>                    
                        <a class="buttonText cursorHand" href='javascript:validate_required()' id="ancSubmit" runat="server"><strong>Save & Email</strong></a>
                    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                </div>
                
                <div id="divButton" class="divButton" style="width: 85px; float:left; margin-left:20px;cursor:pointer;">
                    <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                        <a class="buttonText cursorHand" runat="server" id="ancCancel" runat="server"><strong>Cancel</strong></a>
                    <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                </div> 
        </div>								
    </asp:Panel>
    <asp:Button runat="server" ID="hdnButton" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />
    <asp:Button runat="server" ID="btnTemp" Height="0" Width="0" BorderWidth="0" style="visibility:hidden;" />

      </div>
      
      
      </asp:Content>