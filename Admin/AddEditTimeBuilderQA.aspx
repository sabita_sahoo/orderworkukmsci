﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddEditTimeBuilderQA.aspx.vb" Inherits="Admin.AddEditTimeBuilderQA" MasterPageFile="~/MasterPage/OWAdmin.Master" title="OrderWork : Add/Edit TimeBuilder Question/Answer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanelTBQ" runat="server" >
<ContentTemplate>
    <div id="divContent">
         <script>
             function ChangeQuestionTab(id, tab) {
                 var pagename;                 
                 pagename = id.replace('divQ' + tab + 'Tab', '');
               
                 HideAllQuestionTabs(pagename);
                 if (tab == "Tab1") {
                     document.getElementById(pagename + 'divOuterTab1').style.display = 'block';
                     document.getElementById(pagename + 'divQTab1Tab').className = "floatLeft clstabImage1Selected";                   
                 }
                 else if (tab == "Tab2") {
                     document.getElementById(pagename + 'divOuterTab2').style.display = 'block';
                     document.getElementById(pagename + 'divQTab2Tab').className = "floatLeft clstabImage2Selected";
                 }
                 else if (tab == "Tab3") {
                     document.getElementById(pagename + 'divOuterTab3').style.display = 'block';
                     document.getElementById(pagename + 'divQTab3Tab').className = "floatLeft clstabImage3Selected";
                 }
                 else if (tab == "Tab4") {
                     document.getElementById(pagename + 'divOuterTab4').style.display = 'block';
                     document.getElementById(pagename + 'divQTab4Tab').className = "floatLeft clstabImage4Selected";
                 }
                 else if (tab == "Tab5") {
                     document.getElementById(pagename + 'divOuterTab5').style.display = 'block';
                     document.getElementById(pagename + 'divQTab5Tab').className = "floatLeft clstabImage5Selected";
                 }
                 else if (tab == "Tab6") {
                     document.getElementById(pagename + 'divOuterTab6').style.display = 'block';
                     document.getElementById(pagename + 'divQTab6Tab').className = "floatLeft clstabImage6Selected";
                 }                 
             }
             function HideAllQuestionTabs(pagename) {

                 document.getElementById(pagename + 'divOuterTab1').style.display = 'none';
                 document.getElementById(pagename + 'divOuterTab2').style.display = 'none';
                 document.getElementById(pagename + 'divOuterTab3').style.display = 'none';
                 document.getElementById(pagename + 'divOuterTab4').style.display = 'none';
                 document.getElementById(pagename + 'divOuterTab5').style.display = 'none';
                 document.getElementById(pagename + 'divOuterTab6').style.display = 'none';

                 document.getElementById(pagename + 'divQTab1Tab').className = "floatLeft clstabImage1";
                 document.getElementById(pagename + 'divQTab2Tab').className = "floatLeft clstabImage2";
                 document.getElementById(pagename + 'divQTab3Tab').className = "floatLeft clstabImage3";
                 document.getElementById(pagename + 'divQTab4Tab').className = "floatLeft clstabImage4";
                 document.getElementById(pagename + 'divQTab5Tab').className = "floatLeft clstabImage5";
                 document.getElementById(pagename + 'divQTab6Tab').className = "floatLeft clstabImage6";             

             }
            
             function GetClientId(strid) {
                 var count = document.forms[0].length;
                 var i = 0;
                 var eleName;
                 for (i = 0; i < count; i++) {
                     eleName = document.forms[0].elements[i].id;
                     pos = eleName.indexOf(strid);
                     if (pos >= 0) break;
                 }
                 return eleName;
             }
             function CheckIsNumeric(id) {
                 if (document.getElementById(id).value != "") {
                     if (IsNumeric(document.getElementById(id).value) == false) {
                         document.getElementById(id).value = "";
                         alert("Please enter positive integer value.");
                     }
                 }
             }
             function IsNumeric(sText) {

                 var IsNumber
                 var isInteger_re = /^[0-9]+[0-9]*$/;
                 if (sText.match(isInteger_re)) {
                     IsNumber = true;
                 }
                 else {
                     IsNumber = false;

                 }

                 return IsNumber;
             }
</script>
		
        <div class="roundtopWhite"><img src="Images/Curves/Main-LTC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></div>
        
		 <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
         
         <tr>
            <td valign="top">
             <table cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px; ">
                <tr>
                  <td width="165" class="txtWelcome paddingL10"><div id="divButton" style="width: 120px;  ">
                      <div class="bottonTopGrey"><img src="Images/Curves/Bdr-Btn-TL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                      <a class="buttonText" runat="server" id="lnkBackToService"><img src="Images/Arrows/Arrow-Back.gif" title="Back to Company Profile" width="4" height="8" hspace="0" vspace="0" border="0" class="paddingL10R8" /><strong>Back to Service</strong></a>
                      <div class="bottonBottomGrey"><img src="Images/Curves/Bdr-Btn-BL.gif" alt="" hspace="0" vspace="0" border="0" width="4" height="4" class="btnCorner" style="display: none" /></div>
                  </div></td>                  
                </tr>
             </table>
            </td>
         </tr>
         </table>
         <div style="margin:0px 5px 0px 5px;" id="divMainTab">
            <div class="paddedBox"  id="divProfile" >
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="left" class="HeadingRed"><strong>TimeBuilder Questions</strong></td>
                    <td align="left" class="padTop17">
                     <asp:Panel ID="pnlRemoveallTimeBuilderQ" runat="server" Visible="false">                            
                                    <TABLE cellSpacing="0" cellPadding="0" width="140" bgColor="#f0f0f0" border="0" style="float:left;">
		                                <TR>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                                <TD class="txtBtnImage" width="120">
				                                <asp:Linkbutton id="lnkBtnRemoveallTimeBuilderQ"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Remove All Questions&nbsp;</asp:LinkButton>
			                                </TD>
			                                <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                                </TR>
	                                 </TABLE>
                    </asp:Panel>       
                    <table id="tblSaveTop" runat="server" cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                <tr>
                  <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveTop" ValidationGroup="VGAcc" CssClass="txtListing" runat="server" TabIndex="67"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                  <td width="10"></td>
                  <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                      <tr>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                        <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetTop" CausesValidation="false" runat="server" TabIndex="68"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                        <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                      </tr>
                  </table></td>
                </tr>
            </table>            
                    </td>
                  </tr>
             </table>

              <div class="divWorkOrder" style="margin-top:17px;float:left;">	      
                    <div id="divValidationTBQuestions" class="divValidation" runat=server visible="false" >
	                    <div class="roundtopVal">
	                      <blockquote>
	                        <p><img src="Images/Curves/Validation-TLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	                        </blockquote>
	                    </div>
		                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
			                    <tr valign="middle">
				                    <td width="63" align="center" valign="top"><img src="Images/Icons/Validation-Alert.gif" width="31" height="31"></td>
				                    <td class="validationText"><div  id="divValidationMsg"></div>
				                        <asp:Label ID="lblValidationTBQuestions"  CssClass="bodytxtValidationMsg" Text="" runat="server"></asp:Label>				                    
				                    </td>
				                    <td width="20">&nbsp;</td>
			                    </tr>
		                    </table>
	                    <div class="roundbottomVal">
	                      <blockquote>
	                        <p><img src="Images/Curves/Validation-BLC.gif" alt="" width="5" height="5" class="corner" style="display: none" /></p>
	                        </blockquote>
	                    </div>
                </div>                             
                 <div class="floatLeft clstabImage1Selected" id="divQTab1Tab" runat="server" onclick="Javascript:ChangeQuestionTab(this.id,'Tab1');"></div>
                 <div class="floatLeft clstabImage2" id="divQTab2Tab" runat="server" onclick="Javascript:ChangeQuestionTab(this.id,'Tab2');"></div>
                 <div class="floatLeft clstabImage3" id="divQTab3Tab" runat="server" onclick="Javascript:ChangeQuestionTab(this.id,'Tab3');"></div>
                 <div class="floatLeft clstabImage4" id="divQTab4Tab" runat="server" onclick="Javascript:ChangeQuestionTab(this.id,'Tab4');"></div>
                 <div class="floatLeft clstabImage5" id="divQTab5Tab" runat="server" onclick="Javascript:ChangeQuestionTab(this.id,'Tab5');"></div>
                 <div class="floatLeft clstabImage6" id="divQTab6Tab" runat="server" onclick="Javascript:ChangeQuestionTab(this.id,'Tab6');"></div>
	             <table width="100%" cellspacing="0" cellpadding="0" border="0">	
                     <tr>
				        <td>				       				       
				          <div id = "divOuterpnlTimeBuilderQuestions" class="divOuterpnlTimeBuilderQuestions">
                           <div id="divOuterTab1" runat="server">
                                <table width="100%">
                           <tr>
                           <td>
                                &nbsp;<b>Tab Name</b>&nbsp;&nbsp;<asp:TextBox ID="txtTab1Name" runat="server" CssClass="formField" Width="200"></asp:TextBox>
                           </td>
                            <td>                           
                             <TABLE cellSpacing="0" cellPadding="0" width="92" bgColor="#f0f0f0" border="0" style="float:right;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkAddTimeBuilderTab1Q"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Add Question&nbsp;</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
                            </td>
                           </tr>
                           </table>
                                <asp:Repeater ID="rptTimeBuilderTab1Q" runat="server" >
                                <ItemTemplate>
                                     <table style="padding:5px;margin-top:10px;border:solid 1px black;">
                                     <tr>
                                      <td>Q<%# Container.DataItem("RowNum")%>                               
                                       <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />            
                                       <input type="hidden" id="hdnSequence" runat="server" value=<%#Container.dataitem("Sequence")%> />             
                                       <input type="hidden" id="hdnBaseQuestionSequence" runat="server" value=<%#Container.dataitem("BaseQuestionSequence")%> />                                       
                                       <input type="hidden" id="hdnBaseQuestionSequenceTab" runat="server" value=<%#Container.dataitem("BaseQuestionSequenceTab")%> />                                       
                                       <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />         
                                      &nbsp;<asp:TextBox ID="txtTimeBuilderQ" runat="server" CssClass="formField" Width="750" Text=<%#Container.dataitem("Question")%>></asp:TextBox>
                                      <TABLE cellSpacing="0" cellPadding="0" width="60" bgColor="#f0f0f0" border="0" style="float:right;margin-left:20px;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkBtnRemoveTimeBuilderQ"  runat="server" CausesValidation="false" CssClass="txtListing" CommandName="RemoveQuestion" CommandArgument='<%#Container.DataItem("QuestionId") %>'>&nbsp;Remove&nbsp;</asp:LinkButton>
                                            
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                        <TD>
                                            <div style="width:50px; height:26px;float:left;margin-right:10px;margin-left:-20px;">
                                                <div style="float:left; width:24px; height:26px; margin-right:7px;"><asp:LinkButton ID="lnkBtnMoveUp" Visible='<%#IIF(Container.DataItem("Sequence") = 1 , False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>' CommandName="MoveUp" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:35px;"><img id="imgMoveUp" runat="server" src="~/Images/Arrows/arrow-up.jpg" title="Move Up" border="0" class="cursorHand" /></asp:LinkButton></div>
                                                <div style="float:left; width:10px; height:26px;"><asp:LinkButton ID="lnkBtnMoveDown" Visible='<%#IIF(Container.DataItem("Sequence") = Container.DataItem("MaxSequence"), False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>'  CommandName="MoveDown" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:20px;"><img id="imgMoveDown" runat="server" src="~/Images/Arrows/arrow-down.jpg" title="Move Down" border="0" class="cursorHand" /></asp:LinkButton></div>
                                            </div>                                        
                                        </TD>
		                            </TR>
	                            </TABLE>
                                      </td>
					              </tr>
                                  <tr>
                                    <td>                                       
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415">Message</td>
                                                <td width="165">Summary</td>
                                                <td width="40">Time</td>
                                                <td width="180">Question Dependent Sequence</td>
                                                <td width="185">Question Dependent Tab</td>
                                                <td width="95">Is Mandatory</td>
                                                <td width="50">Min Qty</td>
                                                <td width="50">Max Qty</td>
                                                <td>Qty Ans</td>
                                            </tr>
                                            
                                        </table>      
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415"><asp:TextBox ID="txtQMessage" runat="server" TextMode="MultiLine" Rows="4" CssClass="formField" Width="400" Text=<%#Container.dataitem("QuestionMessage").Replace("<BR>",chr(10))%>></asp:TextBox></td>
                                                <td width="165"><asp:TextBox ID="txtQSummary" runat="server" CssClass="formField" Width="150" Text=<%#Container.dataitem("QuestionSummary")%>></asp:TextBox></td>
                                                <td width="40"><asp:TextBox ID="txtQTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="25" Text=<%#Container.dataitem("QuestionTime")%>></asp:TextBox></td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependent" runat="server" CssClass="formFieldGrey" Width="173"></asp:DropDownList>    
                                                </td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependentTab" runat="server" CssClass="formFieldGrey" Width="173">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab1" Value="Tab1"></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab2" Value="Tab2"></asp:ListItem>
                                                        <asp:ListItem Text="Tab3" Value="Tab3"></asp:ListItem>
                                                        <asp:ListItem Text="Tab4" Value="Tab4"></asp:ListItem>
                                                        <asp:ListItem Text="Tab5" Value="Tab5"></asp:ListItem>
                                                        <asp:ListItem Text="Tab6" Value="Tab6"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="100"><asp:CheckBox ID="chkIsMandatory" valign="top" runat="server" Text="Is Mandatory" Checked=<%#Container.dataitem("IsMandatory")%> /> </td>
                                                <td width="50"><asp:TextBox ID="txtMinQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MinQuantity")%>></asp:TextBox></td>
                                                <td width="50"><asp:TextBox ID="txtMaxQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MaxQuantity")%>></asp:TextBox></td>
                                                <td>
                                                    <asp:DropDownList id="ddQAnsCount" runat="server" CssClass="formFieldGrey" Width="35" AutoPostBack="true" OnSelectedIndexChanged="ddQAnsCount_SelectedIndexChanged" TabIndex=<%#Container.dataitem("QuestionId")%> ToolTip=<%#Container.dataitem("QuestionType")%>>               
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>                    
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>                         
                                    </td>
                                  </tr>
                                  <tr>
                                        <td>
                                             <table style="padding:5px;margin-top:10px;border:dashed 1px black;">
                                               <tr>
                                                <td>
                                                 <asp:Repeater ID="rptTimeBuilderQuestionAnswers" runat="server" >                                        
                                                      <ItemTemplate>
                                                        Ans<%# Container.DataItem("RowNum")%>                                            
                                                        <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />              
                                                        <input type="hidden" id="hdnAnswerId" runat="server" value=<%#Container.dataitem("AnswerId")%> />                                                                                              
                                                        <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />                                
                                                        &nbsp;<asp:TextBox ID="txtAnswer" runat="server" CssClass="formField width150" Text=<%#Container.dataitem("Answer")%>></asp:TextBox>
                                                        &nbsp;&nbsp;Message<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerMessage" runat="server" CssClass="formField width150" TextMode="MultiLine" Rows="4"  Width="400" Text=<%#Container.dataitem("AnswerMessage").Replace("<BR>",chr(10))%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;Time<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField width150"  Width="25" Text=<%#Container.dataitem("AnswerTime")%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;<asp:CheckBox ID="chkStopBooking" valign="top" runat="server" Text="Stops the booking" Checked=<%#Container.dataitem("StopBooking")%> />
                                                        <br />
                                                     </ItemTemplate>
                                                </asp:Repeater>
                                              </td>
                                            </tr>
                                            </table>
                                        </td>
                                     </tr>					     
				                    </table>
                             </ItemTemplate>
                            </asp:Repeater>
                            </div>
                           <div style="Display:none;" id="divOuterTab2" runat="server" >
                                <table width="100%">
                           <tr>
                            <td>
                                &nbsp;<b>Tab Name</b>&nbsp;&nbsp;<asp:TextBox ID="txtTab2Name" runat="server" CssClass="formField" Width="200"></asp:TextBox>
                           </td>
                            <td>                           
                             <TABLE cellSpacing="0" cellPadding="0" width="92" bgColor="#f0f0f0" border="0" style="float:right;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkAddTimeBuilderTab2Q"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Add Question&nbsp;</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
                            </td>
                           </tr>
                           </table>
                                <asp:Repeater ID="rptTimeBuilderTab2Q" runat="server" >
                                <ItemTemplate>
                                   <table style="padding:5px;margin-top:10px;border:solid 1px black;">
                                     <tr>
                                      <td>Q<%# Container.DataItem("RowNum")%>                               
                                       <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />            
                                       <input type="hidden" id="hdnSequence" runat="server" value=<%#Container.dataitem("Sequence")%> />             
                                       <input type="hidden" id="hdnBaseQuestionSequence" runat="server" value=<%#Container.dataitem("BaseQuestionSequence")%> />                                       
                                       <input type="hidden" id="hdnBaseQuestionSequenceTab" runat="server" value=<%#Container.dataitem("BaseQuestionSequenceTab")%> />                                       
                                       <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />         
                                      &nbsp;<asp:TextBox ID="txtTimeBuilderQ" runat="server" CssClass="formField" Width="750" Text=<%#Container.dataitem("Question")%>></asp:TextBox>
                                      <TABLE cellSpacing="0" cellPadding="0" width="60" bgColor="#f0f0f0" border="0" style="float:right;margin-left:20px;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkBtnRemoveTimeBuilderQ"  runat="server" CausesValidation="false" CssClass="txtListing" CommandName="RemoveQuestion" CommandArgument='<%#Container.DataItem("QuestionId") %>'>&nbsp;Remove&nbsp;</asp:LinkButton>
                                            
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                        <TD>
                                            <div style="width:50px; height:26px;float:left;margin-right:10px;margin-left:-20px;">
                                                <div style="float:left; width:24px; height:26px; margin-right:7px;"><asp:LinkButton ID="lnkBtnMoveUp" Visible='<%#IIF(Container.DataItem("Sequence") = 1 , False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>' CommandName="MoveUp" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:35px;"><img id="imgMoveUp" runat="server" src="~/Images/Arrows/arrow-up.jpg" title="Move Up" border="0" class="cursorHand" /></asp:LinkButton></div>
                                                <div style="float:left; width:10px; height:26px;"><asp:LinkButton ID="lnkBtnMoveDown" Visible='<%#IIF(Container.DataItem("Sequence") = Container.DataItem("MaxSequence"), False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>'  CommandName="MoveDown" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:20px;"><img id="imgMoveDown" runat="server" src="~/Images/Arrows/arrow-down.jpg" title="Move Down" border="0" class="cursorHand" /></asp:LinkButton></div>
                                            </div>                                        
                                        </TD>
		                            </TR>
	                            </TABLE>
                                      </td>
					              </tr>
                                  <tr>
                                    <td>                                       
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415">Message</td>
                                                <td width="165">Summary</td>
                                                <td width="40">Time</td>
                                                <td width="180">Question Dependent Sequence</td>
                                                <td width="185">Question Dependent Tab</td>
                                                <td width="95">Is Mandatory</td>
                                                <td width="50">Min Qty</td>
                                                <td width="50">Max Qty</td>
                                                <td>Qty Ans</td>
                                            </tr>
                                            
                                        </table>      
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415"><asp:TextBox ID="txtQMessage" runat="server" TextMode="MultiLine" Rows="4" CssClass="formField" Width="400" Text=<%#Container.dataitem("QuestionMessage").Replace("<BR>",chr(10))%>></asp:TextBox></td>
                                                <td width="165"><asp:TextBox ID="txtQSummary" runat="server" CssClass="formField" Width="150" Text=<%#Container.dataitem("QuestionSummary")%>></asp:TextBox></td>
                                                <td width="40"><asp:TextBox ID="txtQTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="25" Text=<%#Container.dataitem("QuestionTime")%>></asp:TextBox></td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependent" runat="server" CssClass="formFieldGrey" Width="173"></asp:DropDownList>    
                                                </td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependentTab" runat="server" CssClass="formFieldGrey" Width="173">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab1" Value="Tab1"></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab2" Value="Tab2"></asp:ListItem>
                                                        <asp:ListItem Text="Tab3" Value="Tab3"></asp:ListItem>
                                                        <asp:ListItem Text="Tab4" Value="Tab4"></asp:ListItem>
                                                        <asp:ListItem Text="Tab5" Value="Tab5"></asp:ListItem>
                                                        <asp:ListItem Text="Tab6" Value="Tab6"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="100"><asp:CheckBox ID="chkIsMandatory" valign="top" runat="server" Text="Is Mandatory" Checked=<%#Container.dataitem("IsMandatory")%> /> </td>
                                                <td width="50"><asp:TextBox ID="txtMinQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MinQuantity")%>></asp:TextBox></td>
                                                <td width="50"><asp:TextBox ID="txtMaxQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MaxQuantity")%>></asp:TextBox></td>
                                                <td>
                                                    <asp:DropDownList id="ddQAnsCount" runat="server" CssClass="formFieldGrey" Width="35" AutoPostBack="true" OnSelectedIndexChanged="ddQAnsCount_SelectedIndexChanged" TabIndex=<%#Container.dataitem("QuestionId")%> ToolTip=<%#Container.dataitem("QuestionType")%>>               
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>                    
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>                         
                                    </td>
                                  </tr>
                                  <tr>
                                        <td>
                                             <table style="padding:5px;margin-top:10px;border:dashed 1px black;">
                                               <tr>
                                                <td>
                                                 <asp:Repeater ID="rptTimeBuilderQuestionAnswers" runat="server" >                                        
                                                      <ItemTemplate>
                                                        Ans<%# Container.DataItem("RowNum")%>                                            
                                                        <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />              
                                                        <input type="hidden" id="hdnAnswerId" runat="server" value=<%#Container.dataitem("AnswerId")%> />                                                                                              
                                                        <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />                                
                                                        &nbsp;<asp:TextBox ID="txtAnswer" runat="server" CssClass="formField width150" Text=<%#Container.dataitem("Answer")%>></asp:TextBox>
                                                        &nbsp;&nbsp;Message<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerMessage" runat="server" CssClass="formField width150" TextMode="MultiLine" Rows="4"  Width="400" Text=<%#Container.dataitem("AnswerMessage").Replace("<BR>",chr(10))%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;Time<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField width150"  Width="25" Text=<%#Container.dataitem("AnswerTime")%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;<asp:CheckBox ID="chkStopBooking" valign="top" runat="server" Text="Stops the booking" Checked=<%#Container.dataitem("StopBooking")%> />
                                                        <br />
                                                     </ItemTemplate>
                                                </asp:Repeater>
                                              </td>
                                            </tr>
                                            </table>
                                        </td>
                                     </tr>					     
				                    </table>
                             </ItemTemplate>
                            </asp:Repeater>
                            </div>
                           <div style="Display:none;" id="divOuterTab3" runat="server" >
                                <table width="100%">
                           <tr>
                            <td>
                                &nbsp;<b>Tab Name</b>&nbsp;&nbsp;<asp:TextBox ID="txtTab3Name" runat="server" CssClass="formField" Width="200"></asp:TextBox>
                           </td>
                            <td>                           
                             <TABLE cellSpacing="0" cellPadding="0" width="92" bgColor="#f0f0f0" border="0" style="float:right;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkAddTimeBuilderTab3Q"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Add Question&nbsp;</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
                            </td>
                           </tr>
                           </table>
                                <asp:Repeater ID="rptTimeBuilderTab3Q" runat="server" >
                                <ItemTemplate>
                                    <table style="padding:5px;margin-top:10px;border:solid 1px black;">
                                     <tr>
                                      <td>Q<%# Container.DataItem("RowNum")%>                               
                                       <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />            
                                       <input type="hidden" id="hdnSequence" runat="server" value=<%#Container.dataitem("Sequence")%> />             
                                       <input type="hidden" id="hdnBaseQuestionSequence" runat="server" value=<%#Container.dataitem("BaseQuestionSequence")%> />                                       
                                       <input type="hidden" id="hdnBaseQuestionSequenceTab" runat="server" value=<%#Container.dataitem("BaseQuestionSequenceTab")%> />                                       
                                       <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />         
                                      &nbsp;<asp:TextBox ID="txtTimeBuilderQ" runat="server" CssClass="formField" Width="750" Text=<%#Container.dataitem("Question")%>></asp:TextBox>
                                      <TABLE cellSpacing="0" cellPadding="0" width="60" bgColor="#f0f0f0" border="0" style="float:right;margin-left:20px;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkBtnRemoveTimeBuilderQ"  runat="server" CausesValidation="false" CssClass="txtListing" CommandName="RemoveQuestion" CommandArgument='<%#Container.DataItem("QuestionId") %>'>&nbsp;Remove&nbsp;</asp:LinkButton>
                                            
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                        <TD>
                                            <div style="width:50px; height:26px;float:left;margin-right:10px;margin-left:-20px;">
                                                <div style="float:left; width:24px; height:26px; margin-right:7px;"><asp:LinkButton ID="lnkBtnMoveUp" Visible='<%#IIF(Container.DataItem("Sequence") = 1 , False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>' CommandName="MoveUp" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:35px;"><img id="imgMoveUp" runat="server" src="~/Images/Arrows/arrow-up.jpg" title="Move Up" border="0" class="cursorHand" /></asp:LinkButton></div>
                                                <div style="float:left; width:10px; height:26px;"><asp:LinkButton ID="lnkBtnMoveDown" Visible='<%#IIF(Container.DataItem("Sequence") = Container.DataItem("MaxSequence"), False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>'  CommandName="MoveDown" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:20px;"><img id="imgMoveDown" runat="server" src="~/Images/Arrows/arrow-down.jpg" title="Move Down" border="0" class="cursorHand" /></asp:LinkButton></div>
                                            </div>                                        
                                        </TD>
		                            </TR>
	                            </TABLE>
                                      </td>
					              </tr>
                                  <tr>
                                    <td>                                       
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415">Message</td>
                                                <td width="165">Summary</td>
                                                <td width="40">Time</td>
                                                <td width="180">Question Dependent Sequence</td>
                                                <td width="185">Question Dependent Tab</td>
                                                <td width="95">Is Mandatory</td>
                                                <td width="50">Min Qty</td>
                                                <td width="50">Max Qty</td>
                                                <td>Qty Ans</td>
                                            </tr>
                                            
                                        </table>      
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415"><asp:TextBox ID="txtQMessage" runat="server" TextMode="MultiLine" Rows="4" CssClass="formField" Width="400" Text=<%#Container.dataitem("QuestionMessage").Replace("<BR>",chr(10))%>></asp:TextBox></td>
                                                <td width="165"><asp:TextBox ID="txtQSummary" runat="server" CssClass="formField" Width="150" Text=<%#Container.dataitem("QuestionSummary")%>></asp:TextBox></td>
                                                <td width="40"><asp:TextBox ID="txtQTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="25" Text=<%#Container.dataitem("QuestionTime")%>></asp:TextBox></td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependent" runat="server" CssClass="formFieldGrey" Width="173"></asp:DropDownList>    
                                                </td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependentTab" runat="server" CssClass="formFieldGrey" Width="173">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab1" Value="Tab1"></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab2" Value="Tab2"></asp:ListItem>
                                                        <asp:ListItem Text="Tab3" Value="Tab3"></asp:ListItem>
                                                        <asp:ListItem Text="Tab4" Value="Tab4"></asp:ListItem>
                                                        <asp:ListItem Text="Tab5" Value="Tab5"></asp:ListItem>
                                                        <asp:ListItem Text="Tab6" Value="Tab6"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="100"><asp:CheckBox ID="chkIsMandatory" valign="top" runat="server" Text="Is Mandatory" Checked=<%#Container.dataitem("IsMandatory")%> /> </td>
                                                <td width="50"><asp:TextBox ID="txtMinQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MinQuantity")%>></asp:TextBox></td>
                                                <td width="50"><asp:TextBox ID="txtMaxQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MaxQuantity")%>></asp:TextBox></td>
                                                <td>
                                                    <asp:DropDownList id="ddQAnsCount" runat="server" CssClass="formFieldGrey" Width="35" AutoPostBack="true" OnSelectedIndexChanged="ddQAnsCount_SelectedIndexChanged" TabIndex=<%#Container.dataitem("QuestionId")%> ToolTip=<%#Container.dataitem("QuestionType")%>>               
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>                    
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>                         
                                    </td>
                                  </tr>
                                  <tr>
                                        <td>
                                             <table style="padding:5px;margin-top:10px;border:dashed 1px black;">
                                               <tr>
                                                <td>
                                                 <asp:Repeater ID="rptTimeBuilderQuestionAnswers" runat="server" >                                        
                                                      <ItemTemplate>
                                                        Ans<%# Container.DataItem("RowNum")%>                                            
                                                        <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />              
                                                        <input type="hidden" id="hdnAnswerId" runat="server" value=<%#Container.dataitem("AnswerId")%> />                                                                                              
                                                        <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />                                
                                                        &nbsp;<asp:TextBox ID="txtAnswer" runat="server" CssClass="formField width150" Text=<%#Container.dataitem("Answer")%>></asp:TextBox>
                                                        &nbsp;&nbsp;Message<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerMessage" runat="server" CssClass="formField width150" TextMode="MultiLine" Rows="4"  Width="400" Text=<%#Container.dataitem("AnswerMessage").Replace("<BR>",chr(10))%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;Time<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField width150"  Width="25" Text=<%#Container.dataitem("AnswerTime")%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;<asp:CheckBox ID="chkStopBooking" valign="top" runat="server" Text="Stops the booking" Checked=<%#Container.dataitem("StopBooking")%> />
                                                        <br />
                                                     </ItemTemplate>
                                                </asp:Repeater>
                                              </td>
                                            </tr>
                                            </table>
                                        </td>
                                     </tr>					     
				                    </table>
                             </ItemTemplate>
                            </asp:Repeater>
                            </div>
                           <div style="Display:none;" id="divOuterTab4" runat="server" >
                                <table width="100%">
                           <tr>
                            <td>
                                &nbsp;<b>Tab Name</b>&nbsp;&nbsp;<asp:TextBox ID="txtTab4Name" runat="server" CssClass="formField" Width="200"></asp:TextBox>
                           </td>
                            <td>                           
                             <TABLE cellSpacing="0" cellPadding="0" width="92" bgColor="#f0f0f0" border="0" style="float:right;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkAddTimeBuilderTab4Q"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Add Question&nbsp;</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
                            </td>
                           </tr>
                           </table>
                                <asp:Repeater ID="rptTimeBuilderTab4Q" runat="server" >
                                <ItemTemplate>
                                     <table style="padding:5px;margin-top:10px;border:solid 1px black;">
                                     <tr>
                                      <td>Q<%# Container.DataItem("RowNum")%>                               
                                       <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />            
                                       <input type="hidden" id="hdnSequence" runat="server" value=<%#Container.dataitem("Sequence")%> />             
                                       <input type="hidden" id="hdnBaseQuestionSequence" runat="server" value=<%#Container.dataitem("BaseQuestionSequence")%> />                                       
                                       <input type="hidden" id="hdnBaseQuestionSequenceTab" runat="server" value=<%#Container.dataitem("BaseQuestionSequenceTab")%> />                                       
                                       <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />         
                                      &nbsp;<asp:TextBox ID="txtTimeBuilderQ" runat="server" CssClass="formField" Width="750" Text=<%#Container.dataitem("Question")%>></asp:TextBox>
                                      <TABLE cellSpacing="0" cellPadding="0" width="60" bgColor="#f0f0f0" border="0" style="float:right;margin-left:20px;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkBtnRemoveTimeBuilderQ"  runat="server" CausesValidation="false" CssClass="txtListing" CommandName="RemoveQuestion" CommandArgument='<%#Container.DataItem("QuestionId") %>'>&nbsp;Remove&nbsp;</asp:LinkButton>
                                            
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                        <TD>
                                            <div style="width:50px; height:26px;float:left;margin-right:10px;margin-left:-20px;">
                                                <div style="float:left; width:24px; height:26px; margin-right:7px;"><asp:LinkButton ID="lnkBtnMoveUp" Visible='<%#IIF(Container.DataItem("Sequence") = 1 , False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>' CommandName="MoveUp" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:35px;"><img id="imgMoveUp" runat="server" src="~/Images/Arrows/arrow-up.jpg" title="Move Up" border="0" class="cursorHand" /></asp:LinkButton></div>
                                                <div style="float:left; width:10px; height:26px;"><asp:LinkButton ID="lnkBtnMoveDown" Visible='<%#IIF(Container.DataItem("Sequence") = Container.DataItem("MaxSequence"), False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>'  CommandName="MoveDown" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:20px;"><img id="imgMoveDown" runat="server" src="~/Images/Arrows/arrow-down.jpg" title="Move Down" border="0" class="cursorHand" /></asp:LinkButton></div>
                                            </div>                                        
                                        </TD>
		                            </TR>
	                            </TABLE>
                                      </td>
					              </tr>
                                  <tr>
                                    <td>                                       
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415">Message</td>
                                                <td width="165">Summary</td>
                                                <td width="40">Time</td>
                                                <td width="180">Question Dependent Sequence</td>
                                                <td width="185">Question Dependent Tab</td>
                                                <td width="95">Is Mandatory</td>
                                                <td width="50">Min Qty</td>
                                                <td width="50">Max Qty</td>
                                                <td>Qty Ans</td>
                                            </tr>
                                            
                                        </table>      
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415"><asp:TextBox ID="txtQMessage" runat="server" TextMode="MultiLine" Rows="4" CssClass="formField" Width="400" Text=<%#Container.dataitem("QuestionMessage").Replace("<BR>",chr(10))%>></asp:TextBox></td>
                                                <td width="165"><asp:TextBox ID="txtQSummary" runat="server" CssClass="formField" Width="150" Text=<%#Container.dataitem("QuestionSummary")%>></asp:TextBox></td>
                                                <td width="40"><asp:TextBox ID="txtQTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="25" Text=<%#Container.dataitem("QuestionTime")%>></asp:TextBox></td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependent" runat="server" CssClass="formFieldGrey" Width="173"></asp:DropDownList>    
                                                </td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependentTab" runat="server" CssClass="formFieldGrey" Width="173">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab1" Value="Tab1"></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab2" Value="Tab2"></asp:ListItem>
                                                        <asp:ListItem Text="Tab3" Value="Tab3"></asp:ListItem>
                                                        <asp:ListItem Text="Tab4" Value="Tab4"></asp:ListItem>
                                                        <asp:ListItem Text="Tab5" Value="Tab5"></asp:ListItem>
                                                        <asp:ListItem Text="Tab6" Value="Tab6"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="100"><asp:CheckBox ID="chkIsMandatory" valign="top" runat="server" Text="Is Mandatory" Checked=<%#Container.dataitem("IsMandatory")%> /> </td>
                                                <td width="50"><asp:TextBox ID="txtMinQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MinQuantity")%>></asp:TextBox></td>
                                                <td width="50"><asp:TextBox ID="txtMaxQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MaxQuantity")%>></asp:TextBox></td>
                                                <td>
                                                    <asp:DropDownList id="ddQAnsCount" runat="server" CssClass="formFieldGrey" Width="35" AutoPostBack="true" OnSelectedIndexChanged="ddQAnsCount_SelectedIndexChanged" TabIndex=<%#Container.dataitem("QuestionId")%> ToolTip=<%#Container.dataitem("QuestionType")%>>               
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>                    
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>                         
                                    </td>
                                  </tr>
                                  <tr>
                                        <td>
                                             <table style="padding:5px;margin-top:10px;border:dashed 1px black;">
                                               <tr>
                                                <td>
                                                 <asp:Repeater ID="rptTimeBuilderQuestionAnswers" runat="server" >                                        
                                                      <ItemTemplate>
                                                        Ans<%# Container.DataItem("RowNum")%>                                            
                                                        <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />              
                                                        <input type="hidden" id="hdnAnswerId" runat="server" value=<%#Container.dataitem("AnswerId")%> />                                                                                              
                                                        <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />                                
                                                        &nbsp;<asp:TextBox ID="txtAnswer" runat="server" CssClass="formField width150" Text=<%#Container.dataitem("Answer")%>></asp:TextBox>
                                                        &nbsp;&nbsp;Message<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerMessage" runat="server" CssClass="formField width150" TextMode="MultiLine" Rows="4"  Width="400" Text=<%#Container.dataitem("AnswerMessage").Replace("<BR>",chr(10))%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;Time<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField width150"  Width="25" Text=<%#Container.dataitem("AnswerTime")%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;<asp:CheckBox ID="chkStopBooking" valign="top" runat="server" Text="Stops the booking" Checked=<%#Container.dataitem("StopBooking")%> />
                                                        <br />
                                                     </ItemTemplate>
                                                </asp:Repeater>
                                              </td>
                                            </tr>
                                            </table>
                                        </td>
                                     </tr>					     
				                    </table>
                             </ItemTemplate>
                            </asp:Repeater>
                            </div>
                           <div style="Display:none;" id="divOuterTab5" runat="server" >
                                <table width="100%">
                           <tr>
                            <td>
                                &nbsp;<b>Tab Name</b>&nbsp;&nbsp;<asp:TextBox ID="txtTab5Name" runat="server" CssClass="formField" Width="200"></asp:TextBox>
                           </td>
                            <td>                           
                             <TABLE cellSpacing="0" cellPadding="0" width="92" bgColor="#f0f0f0" border="0" style="float:right;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkAddTimeBuilderTab5Q"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Add Question&nbsp;</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
                            </td>
                           </tr>
                           </table>
                                <asp:Repeater ID="rptTimeBuilderTab5Q" runat="server" >
                                <ItemTemplate>
                                      <table style="padding:5px;margin-top:10px;border:solid 1px black;">
                                     <tr>
                                      <td>Q<%# Container.DataItem("RowNum")%>                               
                                       <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />            
                                       <input type="hidden" id="hdnSequence" runat="server" value=<%#Container.dataitem("Sequence")%> />             
                                       <input type="hidden" id="hdnBaseQuestionSequence" runat="server" value=<%#Container.dataitem("BaseQuestionSequence")%> />                                       
                                       <input type="hidden" id="hdnBaseQuestionSequenceTab" runat="server" value=<%#Container.dataitem("BaseQuestionSequenceTab")%> />                                       
                                       <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />         
                                      &nbsp;<asp:TextBox ID="txtTimeBuilderQ" runat="server" CssClass="formField" Width="750" Text=<%#Container.dataitem("Question")%>></asp:TextBox>
                                      <TABLE cellSpacing="0" cellPadding="0" width="60" bgColor="#f0f0f0" border="0" style="float:right;margin-left:20px;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkBtnRemoveTimeBuilderQ"  runat="server" CausesValidation="false" CssClass="txtListing" CommandName="RemoveQuestion" CommandArgument='<%#Container.DataItem("QuestionId") %>'>&nbsp;Remove&nbsp;</asp:LinkButton>
                                            
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                        <TD>
                                            <div style="width:50px; height:26px;float:left;margin-right:10px;margin-left:-20px;">
                                                <div style="float:left; width:24px; height:26px; margin-right:7px;"><asp:LinkButton ID="lnkBtnMoveUp" Visible='<%#IIF(Container.DataItem("Sequence") = 1 , False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>' CommandName="MoveUp" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:35px;"><img id="imgMoveUp" runat="server" src="~/Images/Arrows/arrow-up.jpg" title="Move Up" border="0" class="cursorHand" /></asp:LinkButton></div>
                                                <div style="float:left; width:10px; height:26px;"><asp:LinkButton ID="lnkBtnMoveDown" Visible='<%#IIF(Container.DataItem("Sequence") = Container.DataItem("MaxSequence"), False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>'  CommandName="MoveDown" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:20px;"><img id="imgMoveDown" runat="server" src="~/Images/Arrows/arrow-down.jpg" title="Move Down" border="0" class="cursorHand" /></asp:LinkButton></div>
                                            </div>                                        
                                        </TD>
		                            </TR>
	                            </TABLE>
                                      </td>
					              </tr>
                                  <tr>
                                    <td>                                       
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415">Message</td>
                                                <td width="165">Summary</td>
                                                <td width="40">Time</td>
                                                <td width="180">Question Dependent Sequence</td>
                                                <td width="185">Question Dependent Tab</td>
                                                <td width="95">Is Mandatory</td>
                                                <td width="50">Min Qty</td>
                                                <td width="50">Max Qty</td>
                                                <td>Qty Ans</td>
                                            </tr>
                                            
                                        </table>      
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415"><asp:TextBox ID="txtQMessage" runat="server" TextMode="MultiLine" Rows="4" CssClass="formField" Width="400" Text=<%#Container.dataitem("QuestionMessage").Replace("<BR>",chr(10))%>></asp:TextBox></td>
                                                <td width="165"><asp:TextBox ID="txtQSummary" runat="server" CssClass="formField" Width="150" Text=<%#Container.dataitem("QuestionSummary")%>></asp:TextBox></td>
                                                <td width="40"><asp:TextBox ID="txtQTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="25" Text=<%#Container.dataitem("QuestionTime")%>></asp:TextBox></td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependent" runat="server" CssClass="formFieldGrey" Width="173"></asp:DropDownList>    
                                                </td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependentTab" runat="server" CssClass="formFieldGrey" Width="173">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab1" Value="Tab1"></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab2" Value="Tab2"></asp:ListItem>
                                                        <asp:ListItem Text="Tab3" Value="Tab3"></asp:ListItem>
                                                        <asp:ListItem Text="Tab4" Value="Tab4"></asp:ListItem>
                                                        <asp:ListItem Text="Tab5" Value="Tab5"></asp:ListItem>
                                                        <asp:ListItem Text="Tab6" Value="Tab6"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="100"><asp:CheckBox ID="chkIsMandatory" valign="top" runat="server" Text="Is Mandatory" Checked=<%#Container.dataitem("IsMandatory")%> /> </td>
                                                <td width="50"><asp:TextBox ID="txtMinQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MinQuantity")%>></asp:TextBox></td>
                                                <td width="50"><asp:TextBox ID="txtMaxQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MaxQuantity")%>></asp:TextBox></td>
                                                <td>
                                                    <asp:DropDownList id="ddQAnsCount" runat="server" CssClass="formFieldGrey" Width="35" AutoPostBack="true" OnSelectedIndexChanged="ddQAnsCount_SelectedIndexChanged" TabIndex=<%#Container.dataitem("QuestionId")%> ToolTip=<%#Container.dataitem("QuestionType")%>>               
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>                    
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>                         
                                    </td>
                                  </tr>
                                  <tr>
                                        <td>
                                             <table style="padding:5px;margin-top:10px;border:dashed 1px black;">
                                               <tr>
                                                <td>
                                                 <asp:Repeater ID="rptTimeBuilderQuestionAnswers" runat="server" >                                        
                                                      <ItemTemplate>
                                                        Ans<%# Container.DataItem("RowNum")%>                                            
                                                        <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />              
                                                        <input type="hidden" id="hdnAnswerId" runat="server" value=<%#Container.dataitem("AnswerId")%> />                                                                                              
                                                        <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />                                
                                                        &nbsp;<asp:TextBox ID="txtAnswer" runat="server" CssClass="formField width150" Text=<%#Container.dataitem("Answer")%>></asp:TextBox>
                                                        &nbsp;&nbsp;Message<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerMessage" runat="server" CssClass="formField width150" TextMode="MultiLine" Rows="4"  Width="400" Text=<%#Container.dataitem("AnswerMessage").Replace("<BR>",chr(10))%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;Time<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField width150"  Width="25" Text=<%#Container.dataitem("AnswerTime")%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;<asp:CheckBox ID="chkStopBooking" valign="top" runat="server" Text="Stops the booking" Checked=<%#Container.dataitem("StopBooking")%> />
                                                        <br />
                                                     </ItemTemplate>
                                                </asp:Repeater>
                                              </td>
                                            </tr>
                                            </table>
                                        </td>
                                     </tr>					     
				                    </table>
                             </ItemTemplate>
                            </asp:Repeater>
                            </div>
                           <div style="Display:none;" id="divOuterTab6" runat="server" >
                                <table width="100%">
                           <tr>
                            <td>
                                &nbsp;<b>Tab Name</b>&nbsp;&nbsp;<asp:TextBox ID="txtTab6Name" runat="server" CssClass="formField" Width="200"></asp:TextBox>
                           </td>
                            <td>                           
                             <TABLE cellSpacing="0" cellPadding="0" width="92" bgColor="#f0f0f0" border="0" style="float:right;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkAddTimeBuilderTab6Q"  runat="server" CausesValidation="false" CssClass="txtListing">&nbsp;Add Question&nbsp;</asp:LinkButton>
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
		                            </TR>
	                            </TABLE>
                            </td>
                           </tr>
                           </table>
                                <asp:Repeater ID="rptTimeBuilderTab6Q" runat="server" >
                                <ItemTemplate>
                                  <table style="padding:5px;margin-top:10px;border:solid 1px black;">
                                     <tr>
                                      <td>Q<%# Container.DataItem("RowNum")%>                               
                                       <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />            
                                       <input type="hidden" id="hdnSequence" runat="server" value=<%#Container.dataitem("Sequence")%> />             
                                       <input type="hidden" id="hdnBaseQuestionSequence" runat="server" value=<%#Container.dataitem("BaseQuestionSequence")%> />                                       
                                       <input type="hidden" id="hdnBaseQuestionSequenceTab" runat="server" value=<%#Container.dataitem("BaseQuestionSequenceTab")%> />                                       
                                       <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />         
                                      &nbsp;<asp:TextBox ID="txtTimeBuilderQ" runat="server" CssClass="formField" Width="750" Text=<%#Container.dataitem("Question")%>></asp:TextBox>
                                      <TABLE cellSpacing="0" cellPadding="0" width="60" bgColor="#f0f0f0" border="0" style="float:right;margin-left:20px;">
		                            <TR>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-LC.gif" width="5"></TD>
			                            <TD class="txtBtnImage" width="120">
				                            <asp:Linkbutton id="lnkBtnRemoveTimeBuilderQ"  runat="server" CausesValidation="false" CssClass="txtListing" CommandName="RemoveQuestion" CommandArgument='<%#Container.DataItem("QuestionId") %>'>&nbsp;Remove&nbsp;</asp:LinkButton>
                                            
			                            </TD>
			                            <TD width="5"><IMG height="24" src="Images/Curves/IconGrey-RC.gif" width="5"></TD>
                                        <TD>
                                            <div style="width:50px; height:26px;float:left;margin-right:10px;margin-left:-20px;">
                                                <div style="float:left; width:24px; height:26px; margin-right:7px;"><asp:LinkButton ID="lnkBtnMoveUp" Visible='<%#IIF(Container.DataItem("Sequence") = 1 , False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>' CommandName="MoveUp" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:35px;"><img id="imgMoveUp" runat="server" src="~/Images/Arrows/arrow-up.jpg" title="Move Up" border="0" class="cursorHand" /></asp:LinkButton></div>
                                                <div style="float:left; width:10px; height:26px;"><asp:LinkButton ID="lnkBtnMoveDown" Visible='<%#IIF(Container.DataItem("Sequence") = Container.DataItem("MaxSequence"), False, IIF(Container.DataItem("Sequence") = 0 , False, True))%>'  CommandName="MoveDown" CommandArgument='<%#Container.dataitem("Sequence") %>' runat="server" style="margin-left:20px;"><img id="imgMoveDown" runat="server" src="~/Images/Arrows/arrow-down.jpg" title="Move Down" border="0" class="cursorHand" /></asp:LinkButton></div>
                                            </div>                                        
                                        </TD>
		                            </TR>
	                            </TABLE>
                                      </td>
					              </tr>
                                  <tr>
                                    <td>                                       
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415">Message</td>
                                                <td width="165">Summary</td>
                                                <td width="40">Time</td>
                                                <td width="180">Question Dependent Sequence</td>
                                                <td width="185">Question Dependent Tab</td>
                                                <td width="95">Is Mandatory</td>
                                                <td width="50">Min Qty</td>
                                                <td width="50">Max Qty</td>
                                                <td>Qty Ans</td>
                                            </tr>
                                            
                                        </table>      
                                        <table>
                                            <tr style="vertical-align:top;">
                                                <td width="415"><asp:TextBox ID="txtQMessage" runat="server" TextMode="MultiLine" Rows="4" CssClass="formField" Width="400" Text=<%#Container.dataitem("QuestionMessage").Replace("<BR>",chr(10))%>></asp:TextBox></td>
                                                <td width="165"><asp:TextBox ID="txtQSummary" runat="server" CssClass="formField" Width="150" Text=<%#Container.dataitem("QuestionSummary")%>></asp:TextBox></td>
                                                <td width="40"><asp:TextBox ID="txtQTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="25" Text=<%#Container.dataitem("QuestionTime")%>></asp:TextBox></td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependent" runat="server" CssClass="formFieldGrey" Width="173"></asp:DropDownList>    
                                                </td>
                                                <td width="181">
                                                    <asp:DropDownList id="drpdwnQDependentTab" runat="server" CssClass="formFieldGrey" Width="173">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab1" Value="Tab1"></asp:ListItem>                    
                                                        <asp:ListItem Text="Tab2" Value="Tab2"></asp:ListItem>
                                                        <asp:ListItem Text="Tab3" Value="Tab3"></asp:ListItem>
                                                        <asp:ListItem Text="Tab4" Value="Tab4"></asp:ListItem>
                                                        <asp:ListItem Text="Tab5" Value="Tab5"></asp:ListItem>
                                                        <asp:ListItem Text="Tab6" Value="Tab6"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="100"><asp:CheckBox ID="chkIsMandatory" valign="top" runat="server" Text="Is Mandatory" Checked=<%#Container.dataitem("IsMandatory")%> /> </td>
                                                <td width="50"><asp:TextBox ID="txtMinQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MinQuantity")%>></asp:TextBox></td>
                                                <td width="50"><asp:TextBox ID="txtMaxQuantity" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField" Width="30" Text=<%#Container.dataitem("MaxQuantity")%>></asp:TextBox></td>
                                                <td>
                                                    <asp:DropDownList id="ddQAnsCount" runat="server" CssClass="formFieldGrey" Width="35" AutoPostBack="true" OnSelectedIndexChanged="ddQAnsCount_SelectedIndexChanged" TabIndex=<%#Container.dataitem("QuestionId")%> ToolTip=<%#Container.dataitem("QuestionType")%>>               
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>                    
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>                         
                                    </td>
                                  </tr>
                                  <tr>
                                        <td>
                                             <table style="padding:5px;margin-top:10px;border:dashed 1px black;">
                                               <tr>
                                                <td>
                                                 <asp:Repeater ID="rptTimeBuilderQuestionAnswers" runat="server" >                                        
                                                      <ItemTemplate>
                                                        Ans<%# Container.DataItem("RowNum")%>                                            
                                                        <input type="hidden" id="hdnQuestionId" runat="server" value=<%#Container.dataitem("QuestionId")%> />              
                                                        <input type="hidden" id="hdnAnswerId" runat="server" value=<%#Container.dataitem("AnswerId")%> />                                                                                              
                                                        <input type="hidden" id="hdnQuestionType" runat="server" value=<%#Container.dataitem("QuestionType")%> />                                
                                                        &nbsp;<asp:TextBox ID="txtAnswer" runat="server" CssClass="formField width150" Text=<%#Container.dataitem("Answer")%>></asp:TextBox>
                                                        &nbsp;&nbsp;Message<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerMessage" runat="server" CssClass="formField width150" TextMode="MultiLine" Rows="4"  Width="400" Text=<%#Container.dataitem("AnswerMessage").Replace("<BR>",chr(10))%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;Time<%# Container.DataItem("RowNum")%>&nbsp;<asp:TextBox ID="txtAnswerTime" onblur="Javascript:CheckIsNumeric(this.id)" runat="server" CssClass="formField width150"  Width="25" Text=<%#Container.dataitem("AnswerTime")%>></asp:TextBox>                                                                                             
                                                        &nbsp;&nbsp;<asp:CheckBox ID="chkStopBooking" valign="top" runat="server" Text="Stops the booking" Checked=<%#Container.dataitem("StopBooking")%> />
                                                        <br />
                                                     </ItemTemplate>
                                                </asp:Repeater>
                                              </td>
                                            </tr>
                                            </table>
                                        </td>
                                     </tr>					     
				                    </table>
                             </ItemTemplate>
                            </asp:Repeater>
                            </div>   
                          </div>
				        </td>
				      </tr>				      

                 </table>		
		     </div>       
             
             <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
              <td align="left" class="HeadingRed">&nbsp;</td>
              <td align="left" class="padTop17"><table id="tblSaveBottom" runat="server" cellspacing="0" cellpadding="0" width="100%" border="0" style="padding:0px 0px 16px 0px; margin:0px;">
                  <tr>
                    <td align="right" valign="top"><table cellspacing="0" cellpadding="0" width="120" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td width="110" class="txtBtnImage"><asp:LinkButton ID="btnSaveBottom" ValidationGroup="VGAcc"  CssClass="txtListing" runat="server" TabIndex="63"> <img src="Images/Icons/Icon-Save.gif" width="14" height="13" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Save Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                        </tr>
                    </table></td>
                    <td width="10"></td>
                    <td width="125" align="right" valign="top" style="padding:0px 0px 0px 0px;"><table cellspacing="0" cellpadding="0" width="125" bgcolor="#F0F0F0" border="0">
                        <tr>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-LC.gif" width="5" /></td>
                          <td width="115" class="txtBtnImage"><asp:LinkButton ID="btnResetBottom"  CausesValidation="false" runat="server" TabIndex="64"  class="txtListing"> <img src="Images/Icons/Icon-Reset.gif" width="14" height="16" hspace="5" vspace="0" align="absmiddle" border="0" />&nbsp;Undo Changes&nbsp;</asp:LinkButton></td>
                          <td width="5"><img height="24" src="Images/Curves/IconGrey-RC.gif" width="5" /></td>
                        </tr>
                    </table></td>
                  </tr>
              </table></td>
            </tr>
          </table>       
            </div>          
               
         </div>
      </div>
</ContentTemplate>								
</asp:UpdatePanel>
</asp:Content>