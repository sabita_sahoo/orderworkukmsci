﻿

Public Class StandardListing
    Inherits System.Web.UI.Page

    Protected WithEvents ObjectDataSource1 As ObjectDataSource

    ''' <summary>
    ''' Shared wsobject to access web service webmethods
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Security.SecurePage(Page)

            ViewState!SortExpression = ""
            If Trim(Request("SC")) <> "" Then
                ViewState!SortExpression = Trim(Request("SC"))
            Else
                ViewState!SortExpression = "StandardLabel"
            End If
            Dim sd As SortDirection
            sd = SortDirection.Descending
            If Trim(Request("SO")) <> "" Then
                If Trim(Request("SO")).ToLower = "true" Then
                    sd = SortDirection.Descending
                End If
            End If

            gvStandards.PageSize = hdnPageSize.Value
            gvStandards.Sort(ViewState!SortExpression, sd)
            PopulateGrid()
            gvStandards.PageIndex = 0
        End If

        lblMsg.Text = ""
    End Sub

    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvStandards.DataBind()
        End If
    End Sub

  

#Region "Grid Functions"

    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim ds As DataSet
        Dim IsActive As Integer
        If (chkIsActive.Checked = True) Then
            IsActive = 1
        Else
            IsActive = 0
        End If
        ds = ws.WSContact.GetStandardListing(sortExpression, startRowIndex, maximumRows, ddlStdType.SelectedValue, IsActive)
        ViewState("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
       
        Return ds
    End Function

    ''' <summary>
    ''' ObjectDataSource Controls select Count Method which returns the row count for the listing 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return ViewState("rowCount")
    End Function

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvStandards.RowDataBound
        MakeGridViewHeaderClickable(gvStandards, e.Row)
    End Sub

    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub
    Private Sub ObjectDataSource1_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles ObjectDataSource1.ObjectCreating
        e.ObjectInstance = Me
    End Sub

    Private Sub ObjectDataSource1_ObjectDisposing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceDisposingEventArgs) Handles ObjectDataSource1.ObjectDisposing
        e.Cancel = True
    End Sub
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvStandards.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvStandards, e.Row, Me)
        End If
    End Sub

    Private Sub gvStandards_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStandards.PreRender
        Dim grid As GridView = CType(sender, GridView)
        If Not (grid Is Nothing) Then
            Dim pagerRowTop As GridViewRow = CType(grid.TopPagerRow, GridViewRow)
            Dim pagerRowBottom As GridViewRow = CType(grid.BottomPagerRow, GridViewRow)

            If Not (pagerRowTop Is Nothing) Then
                pagerRowTop.Visible = True
               
              
            End If
            If Not (pagerRowBottom Is Nothing) Then
                pagerRowBottom.Visible = True

            
            End If
        End If
    End Sub

    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)



        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))

        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1

        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        gvStandards.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

#End Region

#Region "Actions"

    ''' <summary>
    ''' populates the list of Complaints
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnView.Click
        Page.Validate()
        If Page.IsValid Then
            lblMsg.Text = ""
            PopulateGrid()
            gvStandards.PageIndex = 0

        End If
    End Sub

#End Region

#Region "Status Update"

    ''' <summary>
    ''' Function to set the company status to Processed - applicable only to buyers
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
 
    Public Sub hdnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnButton.Click
        Dim mode As String
        If ViewState("SelectedStandardID") = 0 Then
            mode = "add"
        Else
            mode = "edit"
        End If
        Dim IsActive As Integer
        If (chkIsActivePopup.Checked = True) Then
            IsActive = 1
        Else
            IsActive = 0
        End If
        Dim ds As DataSet = ws.WSContact.AddEditStandard(txtStandardValue.Text.Trim, mode, ViewState("SelectedStandardID"), ddlMdlStdType.SelectedValue, IsActive)
        PopulateGrid()
        Dim dsView As DataView = CommonFunctions.GetGeneralStandards(Page, True).Tables(0).Copy.DefaultView
        'StandardID = StandardID
    End Sub

    Private Sub gvStandards_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStandards.RowCommand
        If (e.CommandName = "EditStd") Then
            ddlMdlStdType.SelectedValue = ""
            txtStandardValue.Text = ""
            Dim arrList As ArrayList = New ArrayList
            Dim str As String = e.CommandArgument.ToString
            arrList.AddRange(str.Split("#"))
            If (arrList(3) = True) Then
                chkIsActivePopup.Checked = True
            Else
                chkIsActivePopup.Checked = False
            End If
            ddlMdlStdType.SelectedValue = arrList(2)
            txtStandardValue.Text = arrList(1)
            Dim StandardID As String
            StandardID = arrList(0)
            ViewState.Add("SelectedStandardID", StandardID)
            mdlStandard.Show()

        End If
    End Sub
    Public Sub AddNewStandard(ByVal sender As Object, ByVal e As System.EventArgs)

        ViewState.Add("SelectedStandardID", 0)
        ddlMdlStdType.SelectedValue = ""
        txtStandardValue.Text = ""
        mdlStandard.Show()
    End Sub

    Public Function getSelectedComplaintIds() As String
        Dim ComplaintIds As String = ""
        For Each row As GridViewRow In gvStandards.Rows
            Dim chkBox As HtmlInputCheckBox = CType(row.FindControl("Check"), HtmlInputCheckBox)
            If chkBox.Checked Then
                If ComplaintIds = "" Then
                    ComplaintIds = CType(row.FindControl("hdnStandardID"), HtmlInputHidden).Value
                Else
                    ComplaintIds += "," & CType(row.FindControl("hdnStandardID"), HtmlInputHidden).Value
                End If
            End If
        Next
        Return ComplaintIds
    End Function

#End Region

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
            ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        End If
    End Sub
    
End Class