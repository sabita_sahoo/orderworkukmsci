Imports System.Globalization
Imports System.Text.RegularExpressions
Partial Public Class CreateAdminUser
    Inherits System.Web.UI.Page
    Public Shared ws As New WSObjs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack Then

            pnlSuccess.Visible = False
            pnlCreateUsr.Visible = True
            trJobTitle.Visible = False
            tblchangepass.Visible = True
            'Poonam modified on 11/05/2015 - 4576510 - OW-LIVE/DEV - Personal Profile Page throws error
            If Request("ContactID") <> "" Then
                trrolegp.Visible = True
                tblCancelBtn.Visible = True
            Else
                'Poonam modified on 2/10/2015 - OA-75 - OA - Add Role pick list to CreateAdminUser.aspx
                trrolegp.Visible = True
                tblCancelBtn.Visible = False
            End If
            'Populate Role Groups
            PopulateRolesGroups()
            If Not IsNothing(Request("mode")) Then
                tblCreateUserBtn.Visible = False
                tblEditUserBtn.Visible = True
                lblMsg.Text = "Edit Existing Admin User"
                lblMsgCreatedEdited.Text = "User had been Edited. Thank You!!!"
                txtJobTitle.Text = ""
                txtPassword.ReadOnly = True
                rqPassword.Enabled = False
                PopulateDataToEdit()
                PopulateMainCategory("Edit")
                If Not IsNothing(Request("MenuID")) Then
                    If Request("MenuID") <> "" Then
                        PopulatehomepageID(Request("MenuID"))
                    Else
                        PopulatehomepageID(0)
                    End If
                Else
                    PopulatehomepageID(ViewState("MenuID"))
                End If

            Else
                PopulatehomepageID(0)
                tblchangepass.Visible = False
                tblCreateUserBtn.Visible = True
                tblEditUserBtn.Visible = False
                lblMsg.Text = "Create New Admin User"
                lblMsgCreatedEdited.Text = "New User had been Created. Thank You!!!"
                txtJobTitle.Text = ""
                txtPassword.ReadOnly = False
                rqPassword.Enabled = True
                PopulateMainCategory("Add")
            End If
        End If
    End Sub
    Public Function PopulatehomepageID(ByVal homepageid As Integer)

        Dim dsXML As New DataSet()
        dsXML.ReadXml(MapPath("~/XML/WOFormStandards.xml"))
        Dim dv_menu As DataView = dsXML.Tables("Menu").DefaultView
        If ViewState("RolegroupID") = "" Then
            dv_menu.RowFilter = "RoleGroupID=" & (9)
        Else
            dv_menu.RowFilter = "RoleGroupID=" & (ViewState("RolegroupID"))
        End If

        ddlhomepage.DataSource = dv_menu
        ddlhomepage.DataTextField = "MenuLabel"
        ddlhomepage.DataValueField = "MenuID"
        ddlhomepage.DataBind()
        If homepageid = 0 Then
            ddlhomepage.SelectedValue = 243
        Else
            ddlhomepage.SelectedValue = homepageid
        End If
        'ddlhomepage.SelectedValue = (Request("MenuID"))
        'Rows(Request("RoleGroupID"))
    End Function
    Public Function PopulateRolesGroups()
        Dim li As ListItem
        li = New ListItem
        li.Text = "Administrator"
        li.Value = ApplicationSettings.RoleOWAdminID
        ddlRoleGroup.Items.Add(li)
        li = Nothing
        li = New ListItem
        li.Text = "Finance"
        li.Value = ApplicationSettings.RoleOWFinanceID
        ddlRoleGroup.Items.Add(li)
        li = Nothing
        li = New ListItem
        li.Text = "Manager"
        li.Value = ApplicationSettings.RoleOWManagerID
        ddlRoleGroup.Items.Add(li)
        li = Nothing
        li = New ListItem
        li.Text = "Marketing"
        li.Value = ApplicationSettings.RoleOWMarketingID
        ddlRoleGroup.Items.Add(li)
        li = Nothing
        li = New ListItem
        li.Text = "Representative"
        li.Value = ApplicationSettings.RoleOWRepID
        ddlRoleGroup.Items.Add(li)
        li = Nothing
        li = New ListItem
        li.Text = "Finance Manager"
        li.Value = ApplicationSettings.RoleFinanceManagerID
        ddlRoleGroup.Items.Add(li)
        Return Nothing
    End Function

    Public Sub PopulateMainCategory(ByVal Mode As String)
        hdnMainCatSelected.Value = ""
        Dim dsXML As New DataSet()
        dsXML.ReadXml(MapPath("~/XML/WOFormStandards.xml"))
        Dim dvMainCat As DataView = dsXML.Tables("MainCat").DefaultView
        If (Mode = "Add") Then
            If dvMainCat.Count > 0 Then
                For Each dvRow As DataRowView In dvMainCat
                    If (hdnMainCatSelected.Value = "" Or hdnMainCatSelected.Value = "0" Or hdnMainCatSelected.Value Is Nothing) Then
                        hdnMainCatSelected.Value = dvRow("MainCatID")
                    Else
                        hdnMainCatSelected.Value = hdnMainCatSelected.Value & "," & dvRow("MainCatID")
                    End If
                Next
            End If
            listMainCatSel.DataSource = dvMainCat
            listMainCatSel.DataBind()
        ElseIf (Mode = "Edit") Then
            If Not IsNothing(Request("MainCat")) Then
                dvMainCat.RowFilter = "MainCatID NOT IN (" & Request("MainCat") & ")"
                listMainCat.DataSource = dvMainCat
                listMainCat.DataBind()
                hdnMainCatSelected.Value = Request("MainCat")
                Dim dvMainCatSel As DataView = dsXML.Tables("MainCat").DefaultView
                dvMainCatSel.RowFilter = "MainCatID IN (" & Request("MainCat") & ")"
                listMainCatSel.DataSource = dvMainCatSel
                listMainCatSel.DataBind()
            ElseIf Not IsNothing(ViewState("MainCat")) Then
                dvMainCat.RowFilter = "MainCatID NOT IN (" & ViewState("MainCat") & ")"
                listMainCat.DataSource = dvMainCat
                listMainCat.DataBind()
                hdnMainCatSelected.Value = ViewState("MainCat")
                Dim dvMainCatSel As DataView = dsXML.Tables("MainCat").DefaultView
                dvMainCatSel.RowFilter = "MainCatID IN (" & ViewState("MainCat") & ")"
                listMainCatSel.DataSource = dvMainCatSel
                listMainCatSel.DataBind()
            Else
                listMainCat.DataSource = dvMainCat
                listMainCat.DataBind()
            End If
        End If

    End Sub

    Public Function PopulateDataToEdit()
        trPassword.Visible = False
        'Poonam modified on 11/05/2015 - 4576510 - OW-LIVE/DEV - Personal Profile Page throws error
        If Not IsNothing(Request("ContactID")) Then
            ViewState("UserName") = Request("UserName")
            'Poonam modified on 22-03-2017 - Task - OA-427 - OA - Unsupported spaces in Admin users
            ViewState("FirstName") = Request("FirstName").Replace("dquotes", "'").ToString
            ViewState("LastName") = Request("LastName").Replace("dquotes", "'").ToString
            ViewState("RoleGroupName") = Request("RoleGroupName")
            ViewState("ContactID") = Request("ContactID")
            chkEnableLogin.Checked = Request("EnableLogin")
            ViewState("RolegroupID") = Request("RolegroupID").ToString
        Else
            Dim ds As New DataSet
            ds = ws.WSContact.EditLoggedInUser(Session("UserID"))
            If ds.Tables.Count > 0 Then
                ViewState("UserName") = ds.Tables(0).Rows(0)("UserName")
                ViewState("FirstName") = ds.Tables(0).Rows(0)("FirstName")
                ViewState("LastName") = ds.Tables(0).Rows(0)("LastName")
                ViewState("RoleGroupName") = ds.Tables(0).Rows(0)("RoleGroupName")
                ViewState("ContactID") = ds.Tables(0).Rows(0)("ContactID")
                chkEnableLogin.Checked = ds.Tables(0).Rows(0)("EnableLogin")
                ViewState("MainCat") = ds.Tables(0).Rows(0)("MainCatID")
                ViewState("MenuID") = ds.Tables(0).Rows(0)("MenuID")
                ViewState("RolegroupID") = ds.Tables(0).Rows(0)("RolegroupID").ToString
            End If

        End If

        txtUserName.Text = ViewState("UserName")
        txtFName.Text = ViewState("FirstName")
        txtLName.Text = ViewState("LastName")



        If ViewState("RoleGroupName") = "Administrator" Then
            ddlRoleGroup.SelectedValue = ApplicationSettings.RoleOWAdminID
        ElseIf ViewState("RoleGroupName") = "Representative" Then
            ddlRoleGroup.SelectedValue = ApplicationSettings.RoleOWRepID
        ElseIf ViewState("RoleGroupName") = "Finance" Then
            ddlRoleGroup.SelectedValue = ApplicationSettings.RoleOWFinanceID
        ElseIf ViewState("RoleGroupName") = "Manager" Then
            ddlRoleGroup.SelectedValue = ApplicationSettings.RoleOWManagerID
        ElseIf ViewState("RoleGroupName") = "Marketing" Then
            ddlRoleGroup.SelectedValue = ApplicationSettings.RoleOWMarketingID
        ElseIf ViewState("RoleGroupName") = "Finance Manager" Then
            ddlRoleGroup.SelectedValue = ApplicationSettings.RoleFinanceManagerID
        End If


        If (Session("RoleGroupID") = ApplicationSettings.RoleOWAdminID) Then
            tblEditUserBtn.Visible = True
            ddlRoleGroup.Enabled = True
        Else
            tblEditUserBtn.Visible = False
            ddlRoleGroup.Enabled = False
            If (ViewState("ContactID") = Session("UserID")) Then
                tblEditUserBtn.Visible = True
            End If
        End If

        Return Nothing
    End Function

    Private Sub lnkCreateUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCreateUser.Click
        Page.Validate()
        If Page.IsValid Then
            Dim EmailExists As Boolean = ws.WSContact.DoesLoginExist(txtUserName.Text.Trim)
            If EmailExists = True Then
                pnlSuccess.Visible = True
                pnlCreateUsr.Visible = False
                lblMsg.Visible = False
                lblMsgCreatedEdited.Text = "Sorry! The User cannot be created since the specified Email already exists in our records."
                Exit Sub
            End If
            divValidationMain.Visible = False

            Dim Success As Boolean
            Success = ws.WSContact.AddAdminUser(txtUserName.Text.Trim, Encryption.EncryptToMD5Hash(txtPassword.Text.Trim), 8, "jerry", txtFName.Text.Trim, txtLName.Text.Trim, txtJobTitle.Text.Trim, ddlRoleGroup.SelectedValue, chkEnableLogin.Checked, Session("UserID"), hdnMainCatSelected.Value)
            If Success = True Then
                pnlSuccess.Visible = True
                pnlCreateUsr.Visible = False
                lblMsg.Visible = False
            End If
        Else
            divValidationMain.Visible = True
        End If
    End Sub

    Private Sub lnkEditUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEditUser.Click
        Page.Validate()
        If Page.IsValid Then
            If ViewState("UserName") <> txtUserName.Text.Trim Then 'This condition is put coz we need not do the db trip if the email is not changed at all.
                Dim EmailExists As Boolean = ws.WSContact.DoesLoginExist(txtUserName.Text.Trim)
                If EmailExists = True Then
                    pnlSuccess.Visible = True
                    pnlCreateUsr.Visible = False
                    lblMsg.Visible = False
                    lblMsgCreatedEdited.Text = "Sorry! The User cannot be edited with the specified Email since this Email already exists in our records."
                    Exit Sub
                End If
            End If

            divValidationMain.Visible = False
            Dim NewPassword As String
            NewPassword = txtNewPassword.Text.Trim
            If (NewPassword <> "") Then
                NewPassword = Encryption.EncryptToMD5Hash(NewPassword)
            End If

            Dim Success As Boolean
            Success = ws.WSContact.EditAdminUser(txtUserName.Text.Trim, txtFName.Text.Trim, txtLName.Text.Trim, NewPassword, ddlRoleGroup.SelectedValue, ViewState("ContactID"), chkEnableLogin.Checked, Session("UserID"), hdnMainCatSelected.Value, ddlhomepage.SelectedValue)
            If Success = True Then
                pnlSuccess.Visible = True
                pnlCreateUsr.Visible = False
                lblMsg.Visible = False
            End If
        Else
            divValidationMain.Visible = True
        End If
    End Sub
    Private Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click
        Response.Redirect("AdminUsers.aspx")
    End Sub
    'poonam added on 11/1/2016 - Task - OA-156 : OA - Implement Password complexity policy for Admin Portal users
    Sub custPasswordValidate_ServerValidate(source As Object, args As ServerValidateEventArgs) Handles custPasswordValidate.ServerValidate
        Dim substringOW As String = "order"
        Dim substringEM As String = "emp"
        Static reg As New Regex("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?!.*[\W]).*$")
        If ((txtNewPassword.Text.Trim.ToLower().IndexOf(substringOW) >= 0) Or (txtNewPassword.Text.Trim.ToLower().IndexOf(substringEM) >= 0)) Then
            args.IsValid = False
        ElseIf (reg.IsMatch(txtNewPassword.Text.Trim())) Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If

    End Sub
    'poonam added on 11/1/2016 - Task - OA-156 : OA - Implement Password complexity policy for Admin Portal users
    Sub cvPassword_ServerValidate(source As Object, args As ServerValidateEventArgs) Handles cvPassword.ServerValidate
        Dim substringOW As String = "order"
        Dim substringEM As String = "emp"
        Static reg As New Regex("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?!.*[\W]).*$")
        If ((txtPassword.Text.Trim.ToLower().IndexOf(substringOW) >= 0) Or (txtPassword.Text.Trim.ToLower().IndexOf(substringEM) >= 0)) Then
            args.IsValid = False
        ElseIf (reg.IsMatch(txtPassword.Text.Trim())) Then
            args.IsValid = True
        Else
            args.IsValid = False
        End If

    End Sub
End Class