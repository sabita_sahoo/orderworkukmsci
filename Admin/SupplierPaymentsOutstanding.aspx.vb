Imports System
Imports System.Data
Imports System.Threading


Partial Public Class SupplierPaymentsOutstanding
    Inherits System.Web.UI.Page
    ''' <summary>
    ''' Shared wsobject to access web service webmethods
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared ws As New WSObjs
    Private Delegate Sub AfterAdvicesUpdate(ByVal SelectedIDs As String)
    Private Delegate Sub AfterStatusUpdate(ByVal dsSendMail As DataSet, ByVal SelectedIDs As String)
    Protected WithEvents UCDateRange1 As UCDateRange

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)

        If Not Page.IsPostBack Then
            Dim status As String

            If Not IsNothing(Request("status")) Then
                If Request("status") <> "" Then
                    status = Request("status")
                Else
                    status = "Requested"
                End If
            Else
                status = "Requested"
            End If
            hdnStatus.Value = status
            Dim currDate As DateTime = System.DateTime.Today
            UCDateRange1.Visible = False
            UCDateRange1.txtFromDate.Text = ""
            UCDateRange1.txtToDate.Text = ""
            gvContacts.Sort("Date", SortDirection.Ascending)
            If gvContacts.Rows.Count = 0 Then
                pnlExcel.Visible = False
            Else
                pnlExcel.Visible = True
                If (hdnStatus.Value = "Requested") Then
                    tblExport.Style.Add("visibility", "visible")
                    divConfirm.Style.Add("visibility", "hidden")
                End If
                'CreateExportToExcelLink()
            End If
            Dim TabName As String

            If (status = "WithHeld") Then
                TabName = "Tab3"
            ElseIf (status = "In Process") Then
                TabName = "Tab2"
            Else
                TabName = "Tab1"
            End If
            ChangeTab(TabName)
        End If
    End Sub

    ''' <summary>
    ''' Initialise paramter before calling the selectDB
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InitializeGridParameters()
        HttpContext.Current.Items("fromDate") = UCDateRange1.txtFromDate.Text
        HttpContext.Current.Items("toDate") = UCDateRange1.txtToDate.Text
        HttpContext.Current.Items("bizDivId") = Session("BizDivId")
        HttpContext.Current.Items("status") = hdnStatus.Value
        If chkShowAll.Checked = True Then
            gvContacts.PageSize = 100
        Else
            gvContacts.PageSize = 25
        End If
    End Sub

    ''' <summary>
    ''' Selected Event handler for objectdatasource to set rowcount
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ObjectDataSource1_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Selected
        ViewState("rowCount") = HttpContext.Current.Items("rowCount")
    End Sub

    ''' <summary>
    ''' intializes the paramaters for ODC
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ObjectDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles ObjectDataSource1.Selecting
        InitializeGridParameters()
    End Sub

    ''' <summary>
    ''' To return dataset for listing
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectFromDB(ByVal sortExpression As String, ByVal startRowIndex As System.Nullable(Of Integer), ByVal maximumRows As System.Nullable(Of Integer)) As DataSet
        Dim executiontime As New Stopwatch
        executiontime.Start()
        Dim rowCount As Integer = 0
        Dim bizDivId As Integer = HttpContext.Current.Items("bizDivId")
        Dim fromDate As String = HttpContext.Current.Items("fromDate")
        Dim toDate As String = HttpContext.Current.Items("toDate")
        Dim status As String = HttpContext.Current.Items("status")
        Dim subAccount As Integer = 0
        Dim maxRows As Integer = ApplicationSettings.GridViewPageSize
        If Not HttpContext.Current.Session("CheckAll") Is Nothing Then
            If HttpContext.Current.Session("CheckAll") = "True" Then
                maxRows = 100
            Else
                maxRows = maximumRows
            End If
        End If
        Dim ds As DataSet = ws.WSFinance.MSAccounts_GetSupplierPaymentDetails(subAccount, status, fromDate, toDate, bizDivId, False, sortExpression, startRowIndex, maxRows, "")
        HttpContext.Current.Items("rowCount") = ds.Tables(1).Rows(0).Item("TotalRecs")
        executiontime.Stop()
        Dim OMTime As String
        OMTime = executiontime.ElapsedMilliseconds.ToString

        CommonFunctions.createLog("Total Time for fetching Data from database for Supplier Payments to bind with the datagrid is " & OMTime & " ms.")

        Return ds
    End Function

    ''' <summary>
    ''' ObjectDataSource Controls select Count Method which returns the row count for the listing 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectCount() As Integer
        Return HttpContext.Current.Items("rowCount")
    End Function

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowDataBound
        MakeGridViewHeaderClickable(gvContacts, e.Row)
    End Sub

    ''' <summary>
    ''' Function to handle header click, to sort 
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gridViewRow"></param>
    ''' <remarks></remarks>
    Public Sub MakeGridViewHeaderClickable(ByVal gridView As GridView, ByVal gridViewRow As GridViewRow)
        If gridViewRow.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To gridView.Columns.Count - 1
                Dim sortExpression As String = gridView.Columns(i).SortExpression
                Dim tableCell As TableCell = gridViewRow.Cells(i)
                If Not String.IsNullOrEmpty(sortExpression) Then
                    Dim sortDirectionImageControl As System.Web.UI.WebControls.Image
                    sortDirectionImageControl = New System.Web.UI.WebControls.Image
                    Dim imageUrl As String = "~/Images/sort_neutral.gif"
                    If sortExpression = gridView.SortExpression Then
                        If gridView.SortDirection = SortDirection.Ascending Then
                            imageUrl = "~/Images/RedArrow-Up.gif"
                        Else
                            imageUrl = "~/Images/RedArrow-Down.gif"
                        End If
                        sortDirectionImageControl.ImageUrl = imageUrl
                        sortDirectionImageControl.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px")
                        tableCell.Wrap = False
                        tableCell.Controls.Add(sortDirectionImageControl)
                    End If
                End If
            Next i
        End If
    End Sub

    ''' <summary>
    ''' GridView event handler to call SetPagerButtonStates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvContacts.RowCreated
        If e.Row.RowType = DataControlRowType.Pager Then
            SetPagerButtonStates(gvContacts, e.Row, Me)
        End If
    End Sub

    ''' <summary>
    ''' To control the pager button states
    ''' </summary>
    ''' <param name="gridView"></param>
    ''' <param name="gvPagerRow"></param>
    ''' <param name="page"></param>
    ''' <remarks></remarks>
    Public Sub SetPagerButtonStates(ByVal gridView As GridView, ByVal gvPagerRow As GridViewRow, ByVal page As Page)
        Dim pageIndex As Integer = gridView.PageIndex
        Dim pageCount As Integer = gridView.PageCount
        Dim pageSize As Integer = gridView.PageSize
        Dim calTo As Integer
        Dim from As Integer
        Dim btnFirst As ImageButton = CType(gvPagerRow.FindControl("btnFirst"), ImageButton)
        Dim btnPrevious As ImageButton = CType(gvPagerRow.FindControl("btnPrevious"), ImageButton)
        Dim btnNext As ImageButton = CType(gvPagerRow.FindControl("btnNext"), ImageButton)
        Dim btnLast As ImageButton = CType(gvPagerRow.FindControl("btnLast"), ImageButton)
        Dim tdTotalCount As HtmlGenericControl = CType(gvPagerRow.FindControl("tdTotalCount"), HtmlGenericControl)
        btnFirst.Visible = (pageIndex <> 0)
        btnPrevious.Visible = (pageIndex <> 0)
        btnNext.Visible = (pageIndex < (pageCount - 1))
        btnLast.Visible = (pageIndex < (pageCount - 1))
        If (pageIndex < (pageCount - 1)) = False Then
            calTo = (pageIndex + 1) * (pageSize) - pageSize + (ViewState("rowCount") Mod pageSize)
        Else
            calTo = pageIndex * pageSize + pageSize
        End If
        If calTo = 0 And ViewState("rowCount") > 0 Then
            calTo = ViewState("rowCount")
        End If
        from = pageIndex * pageSize + 1
        tdTotalCount.InnerHtml = "Showing " & from & " to " & calTo & " of " & ViewState("rowCount")
        Dim ddlPageSelector1 As DropDownList = CType(gvPagerRow.FindControl("ddlPageSelector"), DropDownList)
        ddlPageSelector1.Items.Clear()
        For i As Integer = 1 To gridView.PageCount
            ddlPageSelector1.Items.Add(i.ToString())
        Next i
        ddlPageSelector1.SelectedIndex = pageIndex
    End Sub

    ''' <summary>
    ''' Event handler to handle page index change
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub ddlPageSelector_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        gvContacts.PageIndex = CType(sender, DropDownList).SelectedIndex
        PopulateGrid()
    End Sub

    ''' <summary>
    ''' Populate grid function to bind data and set href for export to excel button
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateGrid()
        Page.Validate()
        If Page.IsValid Then
            gvContacts.DataBind()
            If gvContacts.Rows.Count = 0 Then
                pnlExcel.Visible = False
            Else
                pnlExcel.Visible = True
                If (hdnStatus.Value = "Requested") Then
                    tblExport.Style.Add("visibility", "visible")
                    divConfirm.Style.Add("visibility", "hidden")
                End If
                'CreateExportToExcelLink()
            End If
        End If
    End Sub

    'Public Sub CreateExportToExcelLink()
    '    hdnEtoE.Value = "ExportToExcel.aspx?page=SupplierPaymentsOutstanding&bizDivId=" & Session("BizDivId") & "&subAccount=" & 0 & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & _
    '                            UCDateRange1.txtToDate.Text & "&status=" & hdnStatus.Value & "&sortExpression=" & gvContacts.SortExpression _
    '                            & "&startRowIndex=" & gvContacts.PageIndex & "&maximumRows=" & gvContacts.PageSize
    'End Sub

    Private Sub lnkExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExport.Click
        Dim selectedIds As String = ""
        selectedIds = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnID")
        'Dim strurl As String
        hdnEtoE.Value = ApplicationSettings.WebPath & "ExportToExcel.aspx?page=SupplierPaymentsOutstanding&bizDivId=" & Session("BizDivId") & "&subAccount=" & 0 & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & _
                            UCDateRange1.txtToDate.Text & "&status=" & hdnStatus.Value & "&sortExpression=" & gvContacts.SortExpression _
                                    & "&startRowIndex=" & gvContacts.PageIndex & "&maximumRows=" & gvContacts.PageSize & "&Invoices=" & selectedIds

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "js", "handleSPOutstandingEtoE();", True)
    End Sub
    Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        Dim selectedIds As String = ""
        selectedIds = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnID")
        'Dim strurl As String
        hdnEtoE.Value = ApplicationSettings.WebPath & "ExportToExcel.aspx?page=SupplierPaymentsOutstanding&bizDivId=" & Session("BizDivId") & "&subAccount=" & 0 & "&fromDate=" & UCDateRange1.txtFromDate.Text & "&toDate=" & _
                            UCDateRange1.txtToDate.Text & "&status=" & hdnStatus.Value & "&sortExpression=" & gvContacts.SortExpression _
                                    & "&startRowIndex=" & gvContacts.PageIndex & "&maximumRows=" & gvContacts.PageSize & "&Invoices=" & selectedIds

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "js", "handleReceivablesEtoE();", True)
    End Sub
    Private Sub btnBOSExport_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBOSExport.ServerClick
        Dim selectedIds As String = ""
        selectedIds = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnID")
        hdnBOS.Value = ApplicationSettings.WebPath & "BOSExport.aspx?Invoices=" & selectedIds
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "js", "handleSPOutstandingBOS();", True)
    End Sub
    ''' <summary>
    ''' Populates the list when new tab is selected for bizdiv
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub Tab1Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtn1.Click
        ChangeTab("Tab1")
    End Sub
    Public Sub Tab2Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtn2.Click
        ChangeTab("Tab2")
    End Sub

    Public Sub Tab3Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtn3.Click
        ChangeTab("Tab3")
    End Sub


    Public Sub ChangeTab(ByVal TabName As String)
        gvContacts.PageIndex = 0
        lblMsg.Text = ""
        tblHoldPayment.Visible = False
        lnkbtn1.CssClass = "NormalTab"
        lnkbtn2.CssClass = "NormalTab"
        lnkbtn3.CssClass = "NormalTab"
        tdHold.Visible = False
        tdMakeAvailable.Visible = False
        pnlExcel.Visible = False
        pnlComplete.Visible = False
        tblBOSExport.Visible = False
        tdShowAll.Visible = False
        Page.Validate()
        If Page.IsValid Then
            If (TabName = "Tab1") Then
                lnkbtn1.CssClass = "HighlightTab"
                hdnStatus.Value = "Requested"

                tblHoldPayment.Visible = True
                tdHold.Visible = True
                pnlExcel.Visible = True
                pnlComplete.Visible = True

                'Added By Pankaj Malav on Nov 4 2008 to restore the status of check Box for show all
                HttpContext.Current.Session("CheckAll") = "False"
            End If
            If (TabName = "Tab2") Then
                lnkbtn2.CssClass = "HighlightTab"
                hdnStatus.Value = "In Process"

                tblHoldPayment.Visible = True
                tdHold.Visible = True
                pnlExcel.Visible = True
                If Session("RoleGroupID") <> ApplicationSettings.RoleOWRepID Then
                    pnlComplete.Visible = True
                    tblBOSExport.Visible = True
                End If
                tdShowAll.Visible = True

                'Added By Pankaj Malav on Nov 4 2008 to restore the status of check Box for show all
                HttpContext.Current.Session("CheckAll") = chkShowAll.Checked

            End If
            If (TabName = "Tab3") Then
                lnkbtn3.CssClass = "HighlightTab"
                hdnStatus.Value = "WithHeld"

                tblHoldPayment.Visible = True
                tdMakeAvailable.Visible = True
                tdShowAll.Visible = True

                'Added By Pankaj Malav on Nov 4 2008 to restore the status of check Box for show all
                HttpContext.Current.Session("CheckAll") = chkShowAll.Checked
            End If
            gvContacts.Columns(0).Visible = True
            PopulateGrid()
        End If
        Security.SecurePage(Page)

    End Sub

    Private Function GetSelectedIds() As String
        Dim selectedIds As String = ""
        For Each row As GridViewRow In gvContacts.Rows
            Dim chkBox As HtmlInputCheckBox = CType(row.FindControl("Check"), HtmlInputCheckBox)
            If chkBox.Checked Then
                If selectedIds = "" Then
                    selectedIds = CType(row.FindControl("hdnID"), HtmlInputHidden).Value
                Else
                    selectedIds += "," & CType(row.FindControl("hdnID"), HtmlInputHidden).Value
                End If
            End If
        Next
        Return selectedIds
    End Function

    ''' <summary>
    ''' populates the list of contacts
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub btnComplete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnComplete.Click
        Dim executiontime As New Stopwatch
        executiontime.Start()
        Page.Validate()
        Dim selectedIds As String = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnID") ' GetSelectedIds()
        If selectedIds = "" Then
            lblMsg.Text = ResourceMessageText.GetString("SelectRecords") '"Please select  contact to change the status"
            Return
        End If
        Dim strdate As String
        If txtDateCompleted.Text.Trim <> "" Then
            Dim myMatch As Match = System.Text.RegularExpressions.Regex.Match(txtDateCompleted.Text.Trim, "(^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](1|2)[0-9]{3})$)")
            If Not myMatch.Success Then
                lblMsg.Text = ResourceMessageText.GetString("DateFormat")
                Return
            End If
            strdate = txtDateCompleted.Text.Trim
        Else
            strdate = Strings.FormatDateTime(Date.Now, DateFormat.ShortDate)
        End If

        If hdnStatus.Value = "Requested" Then
            Dim dsUpdateStatus As DataSet
            'Update Requested Invoices to In Process
            dsUpdateStatus = ws.WSFinance.MSAccounts_UpdateStatusToInProcess(Session("BizDivId"), UCDateRange1.txtFromDate.Text, UCDateRange1.txtToDate.Text, selectedIds)
        End If

        Dim ds As DataSet = ws.WSFinance.MSAccounts_CompleteSupplierPayment(Trim(selectedIds), strdate, Session("BizDivId"), "Update", Session("UserId"))

        Dim dCreateReceipts As [Delegate] = New AfterAdvicesUpdate(AddressOf CreateReceipts)
        ThreadUtil.FireAndForget(dCreateReceipts, New Object() {selectedIds})

        executiontime.Stop()
        Dim OMTime As String
        OMTime = executiontime.ElapsedMilliseconds.ToString

        CommonFunctions.createLog("Total Time for Validating, Updating and fetching Data from database for Supplier Payments with parameters " & selectedIds & "," & strdate & " is " & OMTime & " ms.")
        gvContacts.DataBind()
        'If Page.IsValid Then
        '    PopulateGrid()
        'End If
    End Sub

    Public Sub AfterUpdate(ByVal dsSendmail As DataSet, ByVal selectedIDs As String)
        Try
            If dsSendmail.Tables.Count <> 0 Then
                If dsSendmail.Tables(0).Rows.Count > 0 Then
                    If dsSendmail.Tables(0).Rows(0).Item("ContactID") = -1 Then
                        'double entry
                    Else
                        'send mails to all user whose withdraw funds are success
                        Dim dvPaymentDetails As DataView = dsSendmail.Tables("tblPaymentDetails").DefaultView
                        Dim dsView As DataView = dsSendmail.Tables(0).Copy.DefaultView
                        For Each dsViewRow As DataRowView In dsView
                            dvPaymentDetails.RowFilter = "ContactID=" & dsViewRow("CompanyID")
                            Emails.SendAddWithdrawFundsMail(dsViewRow, dvPaymentDetails, txtDateCompleted.Text.Trim, "VoucherStatusComplete", Session("BizDivId"), True)
                        Next
                    End If
                Else
                    'no rows returned
                    lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
                End If
                'Else
                'no ds sent
                'lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
            End If
            CommonFunctions.createLog("Sending Emails of Withdraw Funds on date: " & Date.Now().ToString & " for ids " & selectedIDs.ToString)
        Catch ex As Exception
            Dim exceptionstr As String
            exceptionstr = ApplicationSettings.WebPath & "<p> BackGround Process in Supplier Payments sending mail. data set = " & dsSendmail.GetXml & " Previous URL : " & Context.Request.UrlReferrer.AbsoluteUri & "Present Raw URL : " & Context.Request.RawUrl & "</p>"
            CommonFunctions.createLog("OWAdmin" & exceptionstr & " WOSave Error: " & ex.ToString)
            SendMail.SendMail(ApplicationSettings.EmailInfo, ApplicationSettings.Error_Email_Contact1, exceptionstr & ex.ToString, "Error in Supplier Payments Back ground process: at " & Now())
        End Try
    End Sub

    Public Sub CreateReceipts(ByVal SelectedIDs As String)
        Try
            Dim dsSendmail As DataSet = ws.WSFinance.MSAccounts_CompleteSupplierPayment(SelectedIDs, "", Session("BizDivId"), "Complete", Session("UserId"))

            Dim dSendMail As [Delegate] = New AfterStatusUpdate(AddressOf AfterUpdate)
            ThreadUtil.FireAndForget(dSendMail, New Object() {dsSendmail, SelectedIDs})

            CommonFunctions.createLog("Creating receipts on date: " & Date.Now().ToString & " for ids " & SelectedIDs.ToString)
        Catch ex As Exception
            Dim exceptionstr As String
            exceptionstr = ApplicationSettings.WebPath & "<p> BackGround Process in Supplier Payments creating receipts. Selected IDs = " & SelectedIDs & " Previous URL : " & Context.Request.UrlReferrer.AbsoluteUri & "Present Raw URL : " & Context.Request.RawUrl & "</p>"
            CommonFunctions.createLog("OWAdmin" & exceptionstr & " WOSave Error: " & ex.ToString)
            SendMail.SendMail(ApplicationSettings.EmailInfo, ApplicationSettings.Error_Email_Contact1, exceptionstr & ex.ToString, "Error in Supplier Payments Back ground process: at " & Now())

        End Try
    End Sub

    Private Sub btnUpdateStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateStatus.Click
        If hdnStatus.Value = "Requested" Then

            Dim selectedIds As String = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnID")

            Dim ds As DataSet
            'Update Requested Invoices to In Process
            ds = ws.WSFinance.MSAccounts_UpdateStatusToInProcess(Session("BizDivId"), UCDateRange1.txtFromDate.Text, UCDateRange1.txtToDate.Text, selectedIds)
            'If Invoices were updated successfully then export same to excel, show the In Process tab .
            If ds.Tables(0).Rows(0).Item(0) = 1 Then
                'Select the In Process Tab
                'TabStripInner1.SelectedIndex = 1
                'InnerTabStrip_SelectedIndexChange(Nothing, Nothing)
                'Update the Listing
                PopulateGrid()
                'Export to Excel
                'ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "callExportToExcel", "window.open(document.getElementById('hdnEtoE').value);", True)
            End If

        End If
    End Sub

    Public Function GetLinks(ByVal invoiceNo As String, ByVal companyId As String, ByVal WOID As String) As String
        Dim link As String = ""
        Dim linkParams As String = ""

        linkParams &= "invoiceNo=" & invoiceNo
        linkParams &= "&companyId=" & companyId

        linkParams &= "&WOID=" & WOID
        linkParams &= "&bizDivId=" & Session("BizDivId")
        linkParams &= "&sender=SuppPayOutstanding"
        linkParams &= "&status=" & hdnStatus.Value

        If linkParams.Contains("'") Then
            linkParams = linkParams.Replace("'", "O&#39;")
        End If

        link &= "<a  target='_blank' href='ViewPurchaseInvoice.aspx?" & linkParams & "'><img src='Images/Icons/View-Purchase-Invoice.gif' alt='View Purchase Invoice' width='18' height='14' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        If (hdnStatus.Value <> "WithHeld") Then
            link &= "<a target='_parent' href='InvoiceApproval.aspx?" & linkParams & "'><img src='Images/Icons/Edit.gif' alt='Edit Invoice' title='Edit Invoice' width='12' height='11' hspace='2' vspace='0' border='0' align='absmiddle'></a>"
        End If
        Return link
    End Function

    Private Sub chkShowAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowAll.CheckedChanged
        Dim executiontime As New Stopwatch
        executiontime.Start()
        gvContacts.PageIndex = 0
        HttpContext.Current.Session("CheckAll") = chkShowAll.Checked

        gvContacts.DataBind()
        executiontime.Stop()
        Dim OMTime As String
        OMTime = executiontime.ElapsedMilliseconds.ToString
        CommonFunctions.createLog("Total Time for fetching Data from database for Supplier Payments on Show All is " & OMTime & " ms.")

    End Sub

    Private Sub ScriptManager1_AsyncPostBackError(ByVal sender As Object, ByVal e As System.Web.UI.AsyncPostBackErrorEventArgs) Handles ScriptManager1.AsyncPostBackError
        'If Not IsNothing(e.Exception) And Not IsDBNull(e.Exception.InnerException) Then
        '    ScriptManager1.AsyncPostBackErrorMessage = e.Exception.InnerException.Message
        'End If
        ScriptManager1.AsyncPostBackErrorMessage = _
           "Unfortunately the operation was not completed. Please try again to continue.- OrderWork Team."

    End Sub

    Public Sub MakeAvailable(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then
            lblMsg.Text = ""
            'this is to check that at least one invoice is selected
            Dim selectedIds As String = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnID") ' GetSelectedIds()
            If selectedIds = "" Then
                lblMsg.Text = "Please select at least one invoice to mark as unHold"
                Return
            End If

            Dim ds As New DataSet
            ds = ws.WSFinance.MS_ChangePIStatusToAvailable(Session("BizDivId"), selectedIds, "")
            If ds.Tables.Count > 0 Then
                If ds.Tables("Success").Rows(0).Item("Success") = 0 Then
                    lblMsg.Text = ResourceMessageText.GetString("DBUpdateFail")
                Else
                    If ds.Tables("SupEmailData").Rows.Count > 0 Then
                        Emails.SendMailPaymentAvailableInInvoiceSet(ds.Tables("SupEmailData").DefaultView)
                    End If
                End If
            End If
            PopulateGrid()
        End If
    End Sub

    Public Sub WithHoldPayment(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Page.IsValid Then
            lblMsg.Text = ""
            'this is to check that at least one invoice is selected
            Dim selectedIds As String = CommonFunctions.getSelectedIdsOfGrid(gvContacts, "Check", "hdnID") ' GetSelectedIds()
            If selectedIds = "" Then
                lblMsg.Text = "Please select at least one invoice to mark payment on hold."
                Return
            End If

            Dim ds As New DataSet
            ds = ws.WSFinance.MS_ChangePIStatusToAvailable(Session("BizDivId"), selectedIds, "hold")
            If ds.Tables.Count > 0 Then
                If ds.Tables("Success").Rows(0).Item("Success") = 0 Then
                    lblMsg.Text = "Selected invoice payment is already marked as on hold."
                End If
            End If
            PopulateGrid()
        End If
    End Sub


    



   
End Class