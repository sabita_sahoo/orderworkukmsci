

Partial Public Class WOSetWholesalePrice
    Inherits System.Web.UI.Page


#Region "Declaration"

    Public Shared ws As New WSObjs

    Protected WithEvents txtPONumber As TextBox

    Protected WithEvents txtSpecialInstructions As TextBox
    Protected WithEvents txtInvoiceTitle As TextBox
    Protected WithEvents tdSpecialInstText As HtmlTableCell

    Protected WithEvents txtUpSellPrice As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtUpSellInvoiceTitle As System.Web.UI.WebControls.TextBox

   


#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Security.SecurePage(Page)
        If Not IsPostBack() Then
            populateWOSummary()
        End If
    End Sub

    ''' <summary>
    ''' Function to populate the workorder summary
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateWOSummary()
        Dim ds As DataSet

        Dim woid As Integer = 0
        If Not IsNothing(Request("WOID")) Then
            If Request("WOID").Trim <> "" Then
                woid = Request("WOID").Trim
            End If
        End If

      

        ds = ws.WSWorkOrder.woGetWholesalePriceSummary(woid, ApplicationSettings.ViewerAdmin, 0)

        Dim CompanyId As String = ""
        CompanyId = ds.Tables("tblWOSummary").Rows(0).Item("CompanyId")

        If CompanyId = ApplicationSettings.OutsourceCompany Or CompanyId = ApplicationSettings.ExponentialECompany Or CompanyId = ApplicationSettings.ITECHSOCompany Or CompanyId = ApplicationSettings.DemoCompany Then
            drpTime.Items.Clear()
            'drpTime.Items.Insert(0, New ListItem("Please Select", ""))
            drpTime.Items.Insert(0, New ListItem("Arrival 0 - 1", "Arrival 0 - 1"))
            drpTime.Items.Insert(1, New ListItem("Arrival 1 - 2", "Arrival 1 - 2"))
            drpTime.Items.Insert(2, New ListItem("Arrival 2 - 3", "Arrival 2 - 3"))
            drpTime.Items.Insert(3, New ListItem("Arrival 3 - 4", "Arrival 3 - 4"))
            drpTime.Items.Insert(4, New ListItem("Arrival 4 - 5", "Arrival 4 - 5"))
            drpTime.Items.Insert(5, New ListItem("Arrival 5 - 6", "Arrival 5 - 6"))
            drpTime.Items.Insert(6, New ListItem("Arrival 6 - 7", "Arrival 6 - 7"))
            drpTime.Items.Insert(7, New ListItem("Arrival 7 - 8", "Arrival 7 - 8"))
            drpTime.Items.Insert(8, New ListItem("Arrival 8 - 9", "Arrival 8 - 9"))
            drpTime.Items.Insert(9, New ListItem("Arrival 9 - 10", "Arrival 9 - 10"))
            drpTime.Items.Insert(10, New ListItem("Arrival 10 - 11", "Arrival 10 - 11"))
            drpTime.Items.Insert(11, New ListItem("Arrival 11 - 12", "Arrival 11 - 12"))
            drpTime.Items.Insert(12, New ListItem("Arrival 12 - 13", "Arrival 12 - 13"))
            drpTime.Items.Insert(13, New ListItem("Arrival 13 - 14", "Arrival 13 - 14"))
            drpTime.Items.Insert(14, New ListItem("Arrival 14 - 15", "Arrival 14 - 15"))
            drpTime.Items.Insert(15, New ListItem("Arrival 15 - 16", "Arrival 15 - 16"))
            drpTime.Items.Insert(16, New ListItem("Arrival 16 - 17", "Arrival 16 - 17"))
            drpTime.Items.Insert(17, New ListItem("Arrival 17 - 18", "Arrival 17 - 18"))
            drpTime.Items.Insert(18, New ListItem("Arrival 18 - 19", "Arrival 18 - 19"))
            drpTime.Items.Insert(19, New ListItem("Arrival 19 - 20", "Arrival 19 - 20"))
            drpTime.Items.Insert(20, New ListItem("Arrival 20 - 21", "Arrival 20 - 21"))
            drpTime.Items.Insert(21, New ListItem("Arrival 21 - 22", "Arrival 21 - 22"))
            drpTime.Items.Insert(22, New ListItem("Arrival 22 - 23", "Arrival 22 - 23"))
            drpTime.Items.Insert(23, New ListItem("Arrival 23 - 24", "Arrival 23 - 24"))
            'OM-153 Added by Sabita New Slot for Amazon
        ElseIf CompanyId = ApplicationSettings.AmazonCompany Then
            drpTime.Items.Clear()
            drpTime.Items.Insert(0, New ListItem("Arrival 8 - 11", "Arrival 8 - 11"))
            'drpTime.Items.Insert(1, New ListItem("Arrival 9 - 12", "Arrival 9 - 12"))
            'drpTime.Items.Insert(2, New ListItem("Arrival 10 - 13", "Arrival 10 - 13"))
            'drpTime.Items.Insert(3, New ListItem("Arrival 11 - 14", "Arrival 11 - 14"))
            drpTime.Items.Insert(1, New ListItem("Arrival 12 - 15", "Arrival 12 - 15"))
            'drpTime.Items.Insert(5, New ListItem("Arrival 13 - 16", "Arrival 13 - 16"))
            'drpTime.Items.Insert(6, New ListItem("Arrival 14 - 17", "Arrival 14 - 17"))
            'drpTime.Items.Insert(7, New ListItem("Arrival 15 - 18", "Arrival 15 - 18"))
            drpTime.Items.Insert(2, New ListItem("Arrival 16 - 19", "Arrival 16 - 19"))
        Else
            drpTime.Items.Clear()
            drpTime.Items.Insert(0, New ListItem("Please Select", "0"))
            drpTime.Items.Insert(1, New ListItem("8am - 1pm", "8am - 1pm"))
            drpTime.Items.Insert(2, New ListItem("1pm - 6pm", "1pm - 6pm"))
            drpTime.Items.Insert(3, New ListItem("All Day", "All Day"))

        End If

        'Store the version number in session
        CommonFunctions.StoreVerNum(ds.Tables("tblWOSummary"))

        If ds.Tables(3).Rows.Count > 0 Then
            If ds.Tables(3).Rows(0).Item("TimeSlot1Enabled") <> 0 Or ds.Tables(3).Rows(0).Item("TimeSlot2Enabled") <> 0 Or ds.Tables(3).Rows(0).Item("TimeSlot3Enabled") <> 0 Then
                drpTime.Items.Clear()
                Dim itemcount As Integer = 0

                If ds.Tables(3).Rows(0).Item("TimeSlot1Enabled") <> 0 Then
                    drpTime.Items.Insert(itemcount, New ListItem(ds.Tables(3).Rows(0).Item("TimeSlot1"), ds.Tables(3).Rows(0).Item("TimeSlot1")))
                End If
                If ds.Tables(3).Rows(0).Item("TimeSlot2Enabled") <> 0 Then
                    drpTime.Items.Insert(itemcount, New ListItem(ds.Tables(3).Rows(0).Item("TimeSlot2"), ds.Tables(3).Rows(0).Item("TimeSlot2")))
                    itemcount = itemcount + 1
                End If
                If ds.Tables(3).Rows(0).Item("TimeSlot3Enabled") <> 0 Then
                    drpTime.Items.Insert(itemcount, New ListItem(ds.Tables(3).Rows(0).Item("TimeSlot3"), ds.Tables(3).Rows(0).Item("TimeSlot3")))
                    itemcount = itemcount + 1
                End If
                If ds.Tables(3).Rows(0).Item("TimeSlot3Enabled") <> 0 Then
                    drpTime.Items.Insert(itemcount, New ListItem(ds.Tables(3).Rows(0).Item("TimeSlot3"), ds.Tables(3).Rows(0).Item("TimeSlot3")))
                    itemcount = itemcount + 1
                End If
            End If
        End If

        'Workorder number
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("WorkOrderId")) Then
            'lblWONo.Text = ds.Tables("tblWOSummary").Rows(0).Item("WorkOrderId")
            ViewState("WorkOrderId") = ds.Tables("tblWOSummary").Rows(0).Item("WorkOrderId")
        End If
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("RefWOID")) Then
            lblWONo.Text = ds.Tables("tblWOSummary").Rows(0).Item("RefWOID")
        End If
        'Buyer Contact
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("BuyerContact")) Then
            lblBuyerContact.Text = ds.Tables("tblWOSummary").Rows(0).Item("BuyerContact")
        End If
        'Buyer Contact
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("BuyerCompany")) Then
            lblBuyerCompany.Text = ds.Tables("tblWOSummary").Rows(0).Item("BuyerCompany")
        End If
        'Location
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("Location")) Then
            lblLocation.Text = ds.Tables("tblWOSummary").Rows(0).Item("Location")
        End If
        'title
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("WOTitle")) Then
            lblTitle.Text = ds.Tables("tblWOSummary").Rows(0).Item("WOTitle")
        End If
        'Quantity
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("Quantity")) Then
            txtQuantity.Text = ds.Tables("tblWOSummary").Rows(0).Item("Quantity")
        End If
        'Submitted date
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("DateCreated")) Then
            lblSubmitted.Text = Strings.FormatDateTime(ds.Tables("tblWOSummary").Rows(0).Item("DateCreated"), DateFormat.ShortDate)
        End If
        'Start date
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("DateStart")) Then
            lblWOStart.Text = Strings.FormatDateTime(ds.Tables("tblWOSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
        End If
        'End Date
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("DateEnd")) Then
            lblWOEnd.Text = Strings.FormatDateTime(ds.Tables("tblWOSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate)
        End If
        'Status
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("Status")) Then
            lblStatus.Text = ds.Tables("tblWOSummary").Rows(0).Item("Status")
            ViewState("Group") = ds.Tables("tblWOSummary").Rows(0).Item("Status")
        End If


        'WO Title
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("WOTitle")) Then
            txtWOTitle.Text = ds.Tables("tblWOSummary").Rows(0).Item("WOTitle")
        End If

        'Start Date
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("DateStart")) Then
            txtScheduleWInBegin.Text = Strings.FormatDateTime(ds.Tables("tblWOSummary").Rows(0).Item("DateStart"), DateFormat.ShortDate)
        End If

        'End Date
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("DateEnd")) Then
            txtScheduleWInEnd.Text = Strings.FormatDateTime(ds.Tables("tblWOSummary").Rows(0).Item("DateEnd"), DateFormat.ShortDate)
        End If


        'Special Instructions
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("SpecialInstructionsText")) Then

            If ds.Tables("tblWOSummary").Rows(0).Item("SpecialInstructionsText").ToString.Contains("<BR>") Then
                txtSpecialInstructions.Text = ds.Tables("tblWOSummary").Rows(0).Item("SpecialInstructionsText").ToString.Replace("<BR>", Chr(13))
            Else
                txtSpecialInstructions.Text = ds.Tables("tblWOSummary").Rows(0).Item("SpecialInstructionsText") + "<BR>".ToString.Replace("<BR>", Chr(13))
            End If
        End If

        'CustomerEmail
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("CustEmail")) Then
            txtCustEmail.Text = ds.Tables("tblWOSummary").Rows(0).Item("CustEmail")
        End If

        'PO Number
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("PONumber")) Then
            txtPONumber.Text = ds.Tables("tblWOSummary").Rows(0).Item("PONumber")
        End If

        'PO Number
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("RelatedWorkOrder")) Then
            txtRelatedWorkorder.Text = ds.Tables("tblWOSummary").Rows(0).Item("RelatedWorkOrder")
        End If

        ' Product purchases
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("ProductDetails")) Then

            txtProductsPurchased.Text = ds.Tables("tblWOSummary").Rows(0).Item("ProductDetails").ToString.Replace("<BR>", Chr(13))
        End If

        'JRS Number
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("JRSNumber")) Then
            txtJRSNumber.Text = ds.Tables("tblWOSummary").Rows(0).Item("JRSNumber")
        End If

        'Invoice Title
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("InvoiceTitle")) Then
            txtInvoiceTitle.Text = ds.Tables("tblWOSummary").Rows(0).Item("InvoiceTitle")
        End If


        'Platform Price
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice")) Then
            If (ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice") = 0) Then
                lblPlatformPrice.Text = "Quote"
            Else
                lblPlatformPrice.Text = FormatNumber(ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice"), 2, TriState.True, TriState.False, TriState.False)
            End If
        Else
            lblPlatformPrice.Text = "-NA-"
        End If
        'Wholesale Price
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("WholesalePrice")) Then
            If (ds.Tables("tblWOSummary").Rows(0).Item("WholesalePrice") = 0) Then
                lblWholesalePrice.Text = "Quote"
            Else
                lblWholesalePrice.Text = FormatNumber(ds.Tables("tblWOSummary").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.False, TriState.False)
            End If
            ViewState("OldWholesalePrice") = FormatNumber(ds.Tables("tblWOSummary").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.False, TriState.False)
            txtWholesalePrice.Text = FormatNumber(ds.Tables("tblWOSummary").Rows(0).Item("WholesalePrice"), 2, TriState.True, TriState.False, TriState.False)
        Else
            lblWholesalePrice.Text = "-NA-"
            ViewState("OldWholesalePrice") = 0
            txtWholesalePrice.Text = 0
        End If
        'Wholesale Day Job rate
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("WholesaleDayJobRate")) Then
            txtWholesaleDayRateSETWP.Text = FormatNumber(ds.Tables("tblWOSummary").Rows(0).Item("WholesaleDayJobRate"), 2, TriState.True, TriState.False, TriState.False)
        End If
        'Estimated time in days
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("EstimatedTimeInDays")) Then
            txtEstimatedTimeInDaysSETWP.Text = ds.Tables("tblWOSummary").Rows(0).Item("EstimatedTimeInDays")
        End If
        'IsSaturdayDisabled
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("IsSaturdayDisabled")) Then
            hdnIsSaturdayDisabled.Value = ds.Tables("tblWOSummary").Rows(0)("IsSaturdayDisabled")
        End If
        'IsSundayDisabled
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("IsSundayDisabled")) Then
            hdnIsSundayDisabled.Value = ds.Tables("tblWOSummary").Rows(0)("IsSundayDisabled")
        End If
        If Not IsPostBack Then
            'Platform Price
            If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice")) Then
                txtSpendLimitPP.Text = ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice")
                ViewState("PlatformPrice") = ds.Tables("tblWOSummary").Rows(0).Item("PlatformPrice")
            End If
        End If

        ViewState("LastActionRef") = ds.Tables("tblWOSummary").Rows(0).Item("LastActionRef")
        ViewState("WOID") = ds.Tables("tblWOSummary").Rows(0).Item("WOID")

        'Show/Hide fields depending upon the workorder status.


        Dim wostatus As Integer
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("WOStatus")) Then
            wostatus = ds.Tables("tblWOSummary").Rows(0).Item("WOStatus")
        End If

        ' 1. For workorders before Active stage should show the Special Instructions field & the others also
        ' 2. For workorders in and After Active stage should show the other field but the special instructions field should be hidden
        Select Case wostatus
            Case ApplicationSettings.WOStatusID.Accepted
                txtSpecialInstructions.Visible = False
                tdSpecialInstText.Visible = False
            Case ApplicationSettings.WOStatusID.Issue
                txtSpecialInstructions.Visible = False
                tdSpecialInstText.Visible = False
            Case ApplicationSettings.WOStatusID.Completed
                txtSpecialInstructions.Visible = False
                tdSpecialInstText.Visible = False
        End Select

        If ApplicationSettings.Country = ApplicationSettings.CountryUK Then



            'Staged WO Code
            If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("StagedWO")) Then
                If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("PricingMethod")) Then
                    If ds.Tables("tblWOSummary").Rows(0).Item("PricingMethod") = "DailyRate" Then
                        tdHdrEstimatedTimeInDays.Visible = True
                        tdHdrWholesaleDayRate.Visible = True
                        tdEstimatedTimeInDays.Visible = True
                        tdWholesaleDayRate.Visible = True
                        txtWholesalePrice.Enabled = False
                    Else
                        txtWholesalePrice.Enabled = True
                    End If
                Else
                    txtWholesalePrice.Enabled = True
                End If
            End If
        End If

        'Business Area
        ViewState("BusinessArea") = ds.Tables("tblWOSummary").Rows(0).Item("BusinessArea")
        If Not IsNothing(ViewState("BusinessArea")) Then
            If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaIT Then
                lblLocationOfGoods.Text = "Location Of Equipments"
                lblProductsPurchased.Text = "Equipments Details"
                rqWODateRange.Enabled = True
                rqWOEnd.Enabled = True
            ElseIf ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                'tdScheduleEndDate.Style.Add("display", "none")
                'tdScheduleWindowBegin.Style.Add("display", "none")
                'tdTxtScheduleWInBegin.Style.Add("display", "none")
                'tdTxtScheduleWInEnd.Style.Add("display", "none")
                'rqWODateRange.Enabled = False
                'rqWOEnd.Enabled = False
                tdTxtScheduleWInBegin.Style.Add("display", "inline")
                tdScheduleWindowBegin.Style.Add("display", "inline")
                tdScheduleEndDate.Style.Add("display", "none")
                tdTxtScheduleWInEnd.Style.Add("display", "none")
                tdAppointmentTime.Visible = True
                tdDdbxAppointmentTime.Visible = True
                RqFldAptTime.Enabled = True
                rqWODateRange.Enabled = True
                rqWOEnd.Enabled = True
            Else
                rqWODateRange.Enabled = True
                rqWOEnd.Enabled = True
            End If
        End If

        'Update WO module
        If Not IsNothing(Request("IsUpdate")) Then
            If Request("IsUpdate") = "yes" Then
                trSetPPtxt.Visible = True
                trSetPPtxtbox.Visible = True
                ViewState("IsUpdate") = "yes"
                If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                    tdTxtScheduleWInBegin.Style.Add("display", "inline")
                    tdScheduleWindowBegin.Style.Add("display", "inline")
                    tdScheduleEndDate.Style.Add("display", "none")
                    tdTxtScheduleWInEnd.Style.Add("display", "none")
                    tdAppointmentTime.Visible = True
                    tdDdbxAppointmentTime.Visible = True
                    RqFldAptTime.Enabled = True
                    rqWODateRange.Enabled = True
                    rqWOEnd.Enabled = True
                End If

            Else

                If Request("Group") = "Accepted" Or Request("Group") = "ActiveElapsed" Or Request("Group") = "Completed" Then
                    If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaIT Then
                        trSetPPtxt.Visible = True
                        trSetPPtxtbox.Visible = True
                    End If
                End If

                ViewState("IsUpdate") = "no"
                txtWholesalePrice.Enabled = True
            End If
        Else

            If Request("Group") = "Accepted" Or Request("Group") = "ActiveElapsed" Or Request("Group") = "Completed" Then
                If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaIT Then
                    trSetPPtxt.Visible = True
                    trSetPPtxtbox.Visible = True
                End If
            End If

            ViewState("IsUpdate") = "no"
            txtWholesalePrice.Enabled = True
        End If

        If (ds.Tables("tblDepotLocations").Rows.Count > 0) Then
            Dim dvDepotLocation As DataView = ds.Tables("tblDepotLocations").DefaultView
            drpdwnGoodsCollection.DataSource = dvDepotLocation.ToTable
            drpdwnGoodsCollection.DataBind()
            Dim li As New ListItem
            li = New ListItem
            li.Text = "Customer's Home"
            li.Value = 1
            drpdwnGoodsCollection.Items.Insert(0, li)
            Dim li1 As New ListItem
            li1 = New ListItem
            li1.Text = "None Specified"
            li1.Value = 0
            drpdwnGoodsCollection.Items.Insert(0, li1)
        Else
            Dim li As New ListItem
            li = New ListItem
            li.Text = "Customer's Home"
            li.Value = 1
            drpdwnGoodsCollection.Items.Insert(0, li)
            Dim li1 As New ListItem
            li1 = New ListItem
            li1.Text = "None Specified"
            li1.Value = 0
            drpdwnGoodsCollection.Items.Insert(0, li1)
        End If
        If Not IsDBNull(ds.Tables(0).Rows(0).Item("AptTime")) Then
            If ds.Tables(0).Rows(0).Item("AptTime") = ApplicationSettings.EveningInstallation Then
                'drpTime.Items.Remove(CStr(ApplicationSettings.AllDaySlot))
                Dim itemcount As Integer = 0
                itemcount = drpTime.Items.Count

                Dim liInstallationTime As New ListItem
                liInstallationTime.Text = ApplicationSettings.EveningInstallation
                liInstallationTime.Value = ApplicationSettings.EveningInstallation
                drpTime.Items.Insert(itemcount, liInstallationTime)
                'itemcount = itemcount + 1
                'Dim liAllDayTime As New ListItem
                'liAllDayTime.Text = ApplicationSettings.AllDaySlot
                'liAllDayTime.Value = ApplicationSettings.AllDaySlot
                'drpTime.Items.Insert(itemcount, liAllDayTime)

                If Not drpTime.Items.FindByText(ds.Tables(0).Rows(0).Item("AptTime")) Is Nothing Then
                    drpTime.SelectedValue = ds.Tables(0).Rows(0).Item("AptTime")
                Else

                    itemcount = drpTime.Items.Count
                    drpTime.Items.Insert(itemcount, New ListItem(ds.Tables(0).Rows(0).Item("AptTime"), ds.Tables(0).Rows(0).Item("AptTime")))
                    drpTime.SelectedValue = ds.Tables(0).Rows(0).Item("AptTime")
                End If

                drpTime.Enabled = False
            Else
                'drpTime.SelectedValue = ds.Tables(0).Rows(0).Item("AptTime")
                If Not drpTime.Items.FindByText(ds.Tables(0).Rows(0).Item("AptTime")) Is Nothing Then
                    drpTime.SelectedValue = ds.Tables(0).Rows(0).Item("AptTime")
                Else
                    Dim itemcount As Integer = 0
                    itemcount = drpTime.Items.Count
                    drpTime.Items.Insert(itemcount, New ListItem(ds.Tables(0).Rows(0).Item("AptTime"), ds.Tables(0).Rows(0).Item("AptTime")))
                    drpTime.SelectedValue = ds.Tables(0).Rows(0).Item("AptTime")
                End If
            End If

            'hdnInstTime.Value = drpTime.SelectedValue
        End If
        If Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("BusinessDivision")) Then
            If (ds.Tables("tblWOSummary").Rows(0).Item("BusinessDivision") <> "") Then
                If ds.Tables("tblWOSummary").Rows(0).Item("Location") = ds.Tables("tblWOSummary").Rows(0).Item("BusinessDivision") Then
                    drpdwnGoodsCollection.SelectedValue = 1
                Else
                    drpdwnGoodsCollection.SelectedValue = IIf(Not IsDBNull(ds.Tables("tblWOSummary").Rows(0).Item("GoodsLocation")), ds.Tables("tblWOSummary").Rows(0).Item("GoodsLocation"), 1)
                End If
            Else
                drpdwnGoodsCollection.SelectedValue = 0
            End If
        Else
            drpdwnGoodsCollection.SelectedValue = 0
        End If

        'upsell info
        If (wostatus = ApplicationSettings.WOStatusID.Accepted) Or (wostatus = ApplicationSettings.WOStatusID.Issue) Or (wostatus = ApplicationSettings.WOStatusID.Completed) Then
            trupsell.Visible = True
            If (ds.Tables("tblWOUpsell").Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables("tblWOUpsell").Rows(0).Item("UpSellPrice")) Then
                    If Not CInt(ds.Tables("tblWOUpsell").Rows(0).Item("UpSellPrice")) = 0 Then
                        txtUpSellPrice.Text = IIf(Not IsDBNull(ds.Tables("tblWOUpsell").Rows(0).Item("UpSellPrice")), ds.Tables("tblWOUpsell").Rows(0).Item("UpSellPrice"), "")
                        txtUpSellInvoiceTitle.Text = IIf(Not IsDBNull(ds.Tables("tblWOUpsell").Rows(0).Item("UpSellInvoiceTitle")), ds.Tables("tblWOUpsell").Rows(0).Item("UpSellInvoiceTitle"), "OrderWork Services")
                    End If
                    txtBCFName.Text = IIf(Not IsDBNull(ds.Tables("tblWOUpsell").Rows(0).Item("BCFirstName")), ds.Tables("tblWOUpsell").Rows(0).Item("BCFirstName"), "")
                    txtBCLName.Text = IIf(Not IsDBNull(ds.Tables("tblWOUpsell").Rows(0).Item("BCLastName")), ds.Tables("tblWOUpsell").Rows(0).Item("BCLastName"), "")
                    txtBCAddress.Text = IIf(Not IsDBNull(ds.Tables("tblWOUpsell").Rows(0).Item("BCAddress")), ds.Tables("tblWOUpsell").Rows(0).Item("BCAddress"), "")
                    txtBCCity.Text = IIf(Not IsDBNull(ds.Tables("tblWOUpsell").Rows(0).Item("BCCity")), ds.Tables("tblWOUpsell").Rows(0).Item("BCCity"), "")
                    txtBCCounty.Text = IIf(Not IsDBNull(ds.Tables("tblWOUpsell").Rows(0).Item("BCCounty")), ds.Tables("tblWOUpsell").Rows(0).Item("BCCounty"), "")
                    txtBCPostalCode.Text = IIf(Not IsDBNull(ds.Tables("tblWOUpsell").Rows(0).Item("BCPostCode")), ds.Tables("tblWOUpsell").Rows(0).Item("BCPostCode"), "")
                    txtBCPhone.Text = IIf(Not IsDBNull(ds.Tables("tblWOUpsell").Rows(0).Item("BCPhone")), ds.Tables("tblWOUpsell").Rows(0).Item("BCPhone"), "")
                    txtBCFax.Text = IIf(Not IsDBNull(ds.Tables("tblWOUpsell").Rows(0).Item("BCFax")), ds.Tables("tblWOUpsell").Rows(0).Item("BCFax"), "")
                End If
            End If
        Else
            trupsell.Visible = False
        End If



    End Sub


    ''' <summary>
    ''' Function called from the form to update the DB
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkConfirm.Click
        Page.Validate()
        If Page.IsValid() Then
            If Not IsNothing(Request("IsUpdate")) Then
                If Request("IsUpdate") = "yes" Then
                    pnlConfirmation.Visible = True
                    pnlSetPrice.Visible = False
                Else
                    updateData(True, "setWP")
                    pnlSuccess.Visible = True
                    pnlSetPrice.Visible = False
                End If
            Else
                updateData(True, "setWP")
                pnlSuccess.Visible = True
                pnlSetPrice.Visible = False
            End If
        Else
            divValidationMain.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' This is called from confirmation panel for Update WorkOrder
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnConfirm.Click
        Page.Validate()
        If Page.IsValid() Then
            updateData(True, "UpdateWO")
            pnlConfirmation.Visible = False
            pnlSuccess.Visible = True
        Else
            divValidationMain.Visible = True
        End If

    End Sub

    ''' <summary>
    ''' This is called from confirmation panel for Update WorkOrder
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkbtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnCancel.Click
        Page.Validate()
        If Page.IsValid() Then
            updateData(False, "UpdateWO")
            pnlConfirmation.Visible = False
            pnlSuccess.Visible = True
        Else
            divValidationMain.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' Function to update data in DB
    ''' </summary>
    ''' <param name="sendemail"></param>
    ''' <param name="Mode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateData(ByVal sendemail As Boolean, ByVal Mode As String)
        Dim ds_Status As DataSet
        divValidationMain.Visible = False
        Dim UpSellPrice As Decimal = 0.0
        Dim WholesalePrice As Decimal = 0.0
        Dim WholesaleDayRateSETWP As Decimal = 0.0

        If txtUpSellPrice.Text.Trim = "" Then
            UpSellPrice = 0.0
        Else
            UpSellPrice = txtUpSellPrice.Text.Trim
        End If


        If txtWholesalePrice.Text.Trim = "" Then
            WholesalePrice = CDec(ViewState("OldWholesalePrice"))
        Else
            WholesalePrice = CDec(txtWholesalePrice.Text.Trim)
        End If

        If txtWholesaleDayRateSETWP.Text.Trim = "" Then
            WholesaleDayRateSETWP = 0.0
        Else
            WholesaleDayRateSETWP = CDec(txtWholesaleDayRateSETWP.Text.Trim)
        End If
        If Request.Browser.Browser = "IE" Then
            txtProductsPurchased.Text = txtProductsPurchased.Text.Replace(Chr(13), "<BR>")

        Else
            txtProductsPurchased.Text = txtProductsPurchased.Text.Replace(Chr(10), "<BR>")
        End If

        Dim dsWorkOrder As New WorkOrderNew
        Dim nrowUpSell As WorkOrderNew.tblWOUpsellRow = dsWorkOrder.tblWOUpsell.NewRow
        With nrowUpSell
            .WOID = ViewState("WOID")
            If txtUpSellPrice.Text.Trim <> "" Then
                .UpSellPrice = txtUpSellPrice.Text
                If txtUpSellInvoiceTitle.Text.Trim <> "" Then
                    .UpSellInvoiceTitle = txtUpSellInvoiceTitle.Text
                End If
                If txtBCFName.Text.Trim <> "" Then
                    .BCFirstName = txtBCFName.Text
                End If
                If txtBCLName.Text.Trim <> "" Then
                    .BCLastName = txtBCLName.Text
                End If
                If txtBCAddress.Text.Trim <> "" Then
                    .BCAddress = txtBCAddress.Text
                End If
                If txtBCCity.Text.Trim <> "" Then
                    .BCCity = txtBCCity.Text
                End If
                If txtBCCounty.Text.Trim <> "" Then
                    .BCCounty = txtBCCounty.Text
                End If
                If txtBCPostalCode.Text.Trim <> "" Then
                    .BCPostCode = txtBCPostalCode.Text
                End If
                If txtBCPhone.Text.Trim <> "" Then
                    .BCPhone = txtBCPhone.Text
                End If
                If txtBCFax.Text.Trim <> "" Then
                    .BCFax = txtBCFax.Text
                End If
            End If

        End With

        dsWorkOrder.tblWOUpsell.Rows.Add(nrowUpSell)
        Dim xmlUpsellContent As String = dsWorkOrder.GetXml
        xmlUpsellContent = xmlUpsellContent.Remove(xmlUpsellContent.IndexOf("xmlns"), xmlUpsellContent.IndexOf(".xsd") - xmlUpsellContent.IndexOf("xmlns") + 5)
        ds_Status = ws.WSWorkOrder.MS_WOSetWholesalePrice(ViewState("WOID"), WholesalePrice, Session("CompanyId"), Session("UserId"), ApplicationSettings.RoleOWID, ViewState("LastActionRef"), txtWOComment.Text.Trim, Session("BizDivId"), txtSpecialInstructions.Text.ToString.Replace(Chr(13), "<BR>"), txtPONumber.Text.Trim, txtJRSNumber.Text.Trim, "", txtInvoiceTitle.Text.Trim, txtWOTitle.Text.Trim, txtScheduleWInBegin.Text.Trim, txtScheduleWInEnd.Text.Trim, txtUpSellPrice.Text, xmlUpsellContent, txtEstimatedTimeInDaysSETWP.Text.Trim, WholesaleDayRateSETWP, txtProductsPurchased.Text, drpdwnGoodsCollection.SelectedValue, CommonFunctions.FetchVerNum(), CDec(txtSpendLimitPP.Text.Trim), ViewState("IsUpdate"), drpTime.SelectedItem.Text, txtCustEmail.Text.Trim, txtQuantity.Text.Trim, txtRelatedWorkorder.Text.Trim)

        If ds_Status.Tables.Count <> 0 Then
            If ds_Status.Tables("tblStatus").Rows.Count > 0 Then
                If ds_Status.Tables("tblStatus").Rows(0).Item("DBStatus") = -1 Then
                    'double entry                   
                ElseIf ds_Status.Tables("tblStatus").Rows(0).Item("DBStatus") = -10 Then
                    divValidationMain.Visible = True
                    lblError.Text = ResourceMessageText.GetString("WOVersionControlMsg")
                    lblError.Text = lblError.Text.Replace("<Link>", "AdminWODetails.aspx?" & "WOID=" & ViewState("WOID") & "&WorkOrderID=" & ViewState("WOID") & "&FromDate=&ToDate=" & "&ContactID=" & Request("ContactId") & "&CompanyID=" & Request("CompanyID") & "&SupContactID=" & Request("SupContactID") & "&SupCompID=&PS=25&PN=0&SC=DateCreated&SO=1&BizDivID=1&sender=UCWOsListing&CompID=0")
                Else
                    pnlSetPrice.Visible = False
                    pnlSuccess.Visible = True
                    If Mode = "UpdateWO" And sendemail Then
                        Dim dtDistinct As New DataTable
                        Dim dvEmailInfo As New DataView
                        Dim dvSupplier As New DataView
                        Dim CompanyID As Integer
                        ' Store only distinct supplierIS in dtDistinct
                        ds_Status.Tables(2).TableName = "tblMail"
                        dtDistinct = CommonFunctions.SelectDistinct("tblMail", ds_Status.Tables("tblMail"), "CompanyID")
                        dvSupplier = ds_Status.Tables(1).Copy.DefaultView
                        dvEmailInfo = ds_Status.Tables(2).Copy.DefaultView
                        For Each row As DataRow In dtDistinct.Rows
                            CompanyID = row("CompanyID")
                            dvSupplier.RowFilter = "CompanyID = '" & CompanyID & "'"
                            dvEmailInfo.RowFilter = "CompanyID = '" & CompanyID & "'"
                            Emails.sendUpdateWorkorderMail(ViewState("WorkOrderId"), txtSpendLimitPP.Text, txtWOTitle.Text, lblLocation.Text, txtWOComment.Text.Trim, txtScheduleWInBegin.Text, txtScheduleWInEnd.Text, drpTime.SelectedItem.Text, drpdwnGoodsCollection.SelectedItem.Text, txtProductsPurchased.Text, Session("BizDivId"), dvEmailInfo.ToTable.DefaultView, dvSupplier.ToTable.DefaultView, ViewState("BusinessArea"))
                        Next
                        lblSuccess.Text = "Changes successfully saved for the WorkOrderId: " & ViewState("WorkOrderId")
                    ElseIf Mode = "setWP" Then
                        'If wholesale price is not updated but other details are modified do not send a mail
                        If (ViewState("OldWholesalePrice") = WholesalePrice) Then
                            lblSuccess.Text = "Changes successfully saved for the WorkOrderId: " & ViewState("WorkOrderId")
                        Else
                            'If wholesale price is modified then send a amil.
                            lblSuccess.Text = "Wholesale Price set successfully for the WorkOrderId: " & ViewState("WorkOrderId")
                            'Email: The email should contain the old and changed wholesale price plus comments. 
                            Emails.sendWholesalePriceMail(lblWONo.Text.Trim, CDec(ViewState("OldWholesalePrice")), WholesalePrice, txtWOComment.Text.Trim, ds_Status.Tables(0).Rows(0).Item("BuyerName"), ds_Status.Tables(0).Rows(0).Item("BuyerEmail"), Session("BizDivId"))
                        End If
                    Else
                        lblSuccess.Text = "Changes successfully saved for the WorkOrderId: " & ViewState("WorkOrderId")
                    End If
                End If
            Else
                'no rows returned
                divValidationMain.Visible = True
                lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
            End If
        Else
            'no ds sent
            divValidationMain.Visible = True
            lblError.Text = ResourceMessageText.GetString("DBUpdateFail")
        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' Go back to listing
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub lnkCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancel.Click, lnkBack.Click
        Dim link As String = ""
        If Not IsNothing(Request("sender")) Then
            If Request("sender") = "UCWOsListing" Then
                If Not IsNothing(ViewState("Group")) Then
                    If ViewState("Group") = ApplicationSettings.WOGroupIssue Then
                        link = "AdminWOIssueListing.aspx?"
                    Else
                        'OA-476 - OA - Click on Back button after WP change should take user back to Wo Details
                        'link = "AdminWOListing.aspx?"
                        link = "AdminWODetails.aspx?"
                    End If
                Else
                    If Request("Group") = ApplicationSettings.WOGroupIssue Then
                        link = "AdminWOIssueListing.aspx?"
                    Else
                        'OA-476 - OA - Click on Back button after WP change should take user back to Wo Details
                        'link = "AdminWOListing.aspx?"
                        link = "AdminWODetails.aspx?"
                    End If
                End If
            ElseIf Request("sender") = "SearchWO" Then
                link = "SearchWOs.aspx?"
                link &= "&SrcWorkorderID=" & Request("txtWorkorderID")
                link &= "&SrcCompanyName=" & Request("CompanyName")
                link &= "&SrcKeyword=" & Request("KeyWord")
                link &= "&SrcPONumber=" & Request("PONumber")
                link &= "&SrcReceiptNumber=" & Request("ReceiptNumber")
                link &= "&SrcPostCode=" & Request("PostCode")
                link &= "&SrcDateStart=" & Request("DateStart")
                link &= "&SrcDateSubmittedFrom=" & Request("DateSubmittedFrom")
                link &= "&SrcDateSubmittedTo=" & Request("DateSubmittedTo")
                link &= "&PS=" & Request("PS")
                link &= "&PN=" & Request("PN")
                link &= "&SC=" & Request("SC")
                link &= "&SO=" & Request("SO")
                Response.Redirect(link)
            Else
                link = "AdminWODetails.aspx?"
            End If

            link &= "WOID=" & Request("WOID")
            'OA-476 - 2.Change wp or WO and click cancel.it does not populates Cancellation details.following code commented and replace with viewstate
            link &= "&WorkOrderID=" & ViewState("WorkOrderId") 'Request("WorkOrderID")
            link &= "&ContactId=" & Request("ContactId")
            link &= "&CompanyId=" & Request("CompanyId")
            link &= "&SupContactId=" & Request("SupContactId")
            link &= "&SupCompId=" & Request("SupCompId")
            If Not IsNothing(Request("Group")) Then
                link &= "&Group=" & Request("Group")
                link &= "&mode=" & Request("Group")
            Else
                link &= "&Group=" & ViewState("Group")
                link &= "&mode=" & ViewState("Group")
            End If



            link &= "&BizDivID=" & Request("BizDivID")
            link &= "&FromDate=" & Request("FromDate")
            link &= "&ToDate=" & Request("ToDate")
            link &= "&PS=" & Request("PS")
            link &= "&PN=" & Request("PN")
            link &= "&SC=" & Request("SC")
            link &= "&SO=" & Request("SO")
            link &= "&CompID=" & Request("CompID")
            If Not IsNothing(Request("SearchWorkorderID")) Then
                link &= "&SearchWorkorderID=" & Request("SearchWorkorderID")
            End If
            link &= "&sender=" & Request("sender")
            If Not IsNothing(Request("IsNextDay")) Then
                If Request("IsNextDay") = "Yes" Then
                    link &= "&IsNextDay=" & "Yes"
                Else
                    link &= "&IsNextDay=" & "No"
                End If
            End If
            Response.Redirect(link)
        Else
            Response.Redirect("AdminWOListing.aspx?mode=Submitted")
        End If
    End Sub


#Region "Validation"
    Public Sub custValiDateRange_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles rqWODateRange.ServerValidate
        If Page.IsValid Then
            If Not IsNothing(ViewState("BusinessArea")) Then
                If ViewState("BusinessArea") <> ApplicationSettings.BusinessAreaRetail Then
                    If txtScheduleWInBegin.Text.Trim <> "" And txtScheduleWInEnd.Text.Trim <> "" Then
                        Dim DateStart As New Date
                        Dim DateEnd As New Date
                        DateStart = txtScheduleWInBegin.Text.Trim
                        DateEnd = txtScheduleWInEnd.Text.Trim
                        If DateStart <= DateEnd Then
                            args.IsValid = True
                        Else
                            args.IsValid = False
                        End If
                    End If
                Else
                    args.IsValid = True
                End If
            Else
                args.IsValid = True
            End If
            
        End If
    End Sub

    Public Sub CustomValidatorLocationOfGoods_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomValidatorLocationOfGoods.ServerValidate
        If Page.IsValid Then
            If drpdwnGoodsCollection.SelectedValue = 0 Then
                If ViewState("BusinessArea") = ApplicationSettings.BusinessAreaRetail Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            Else
                args.IsValid = True
            End If
        End If
    End Sub
    Public Sub CustomValCustomerEmail_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomValCustomerEmail.ServerValidate

        If (txtUpSellPrice.Text <> "" And txtUpSellPrice.Text <> "0" And txtUpSellPrice.Text <> "0.00") Then
            If txtCustEmail.Text = "" Then
                args.IsValid = False
            Else
                args.IsValid = True
            End If
        Else
            args.IsValid = True
        End If

    End Sub
    Public Sub CustomValInvoiceTitle_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomValInvoiceTitle.ServerValidate

            If (txtUpSellPrice.Text <> "" And txtUpSellPrice.Text <> "0" And txtUpSellPrice.Text <> "0.00") Then
                If txtUpSellInvoiceTitle.Text = "" Then
                    args.IsValid = False
                Else
                    args.IsValid = True
                End If
            Else
                args.IsValid = True
            End If

    End Sub

    ''' <summary>
    ''' Function to check is platform price is zero
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Public Sub ServerValidatePP(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustPlatformPriceValidator.ServerValidate
        Try
            'Changed By Pankaj Malav on 08 Jan 2009
            'Description: Previously it is converting the value in int therefore value 0.25 is rounded of to 0 and thus showing validation
            'Chnaged into decimal.
            Dim num As Decimal = Decimal.Parse(CDec(args.Value))
           
        Catch exc As Exception
        End Try
        args.IsValid = True
    End Sub

  
#End Region

   
  
End Class