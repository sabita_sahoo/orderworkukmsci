﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Net.Mail;
using System.IO;

namespace SendGridMail
{
   
    public static class SGMail
    {
        //static void Main(string[] args)
        //{

        //    SendGridMail.SGMail.Execute("sabita.sahoo@empowereduk.com", "sabysahoo9@gmail.com", "test", "test", null, "").Wait();
        //}
        public static async Task Execute(string mailFrom, String mailTo , String mailBody, String mailSubject, Personalization personalization, string apiKey, System.Net.Mail.Attachment attachment =null) 
        {
            // Retrieve the API key from the environment variables. See the project README for more info about setting this up.
         //var apiKey1 = "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"; ;
            try
            {
                var client = new SendGridClient(apiKey);

                // Send a Single Email using the Mail Helper
                var from = new EmailAddress(mailFrom, "Orderwork - No-reply");
                var subject = mailSubject;
                var to = new EmailAddress(mailTo);
                //var cc = new EmailAddress(cc);
                var plainTextContent = mailBody;
                var htmlContent = mailBody;
                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                if (attachment != null)
                {
                    msg.AddAttachment(GetSendGridAttachment(attachment));
                }
                if (personalization.Ccs != null)
                {
                    msg.AddCcs(personalization.Ccs);
                   
                }
                if (personalization.Bccs != null)
                {
                    msg.AddBccs(personalization.Bccs);

                }

                var response = await client.SendEmailAsync(msg);

                //Console.WriteLine(msg.Serialize());
                //Console.WriteLine(response.StatusCode);
                //Console.WriteLine(response.Headers);
                //Console.WriteLine("\n\nPress <Enter> to continue.");
                //Console.ReadLine();

            }
            catch(Exception ex)
            {
                throw ex;
            }
           
        }

        public static SendGrid.Helpers.Mail.Attachment GetSendGridAttachment(this System.Net.Mail.Attachment attachment)
        {
            using (var stream = new MemoryStream())
            {
                try
                {
                    attachment.ContentStream.CopyTo(stream);
                    return new SendGrid.Helpers.Mail.Attachment()
                    {
                        Disposition = "attachment",
                        Type = attachment.ContentType.MediaType,
                        Filename = attachment.Name,
                        ContentId = attachment.ContentId,
                        Content = Convert.ToBase64String(stream.ToArray())
                    };
                }
                finally
                {
                    stream.Close();
                }
            }
        }

    }
}
