''' <summary>
''' Class contains shared properties which returns application settings key values.
''' Calling project must contain the  required key in web.config
''' </summary>
''' <author>Chandrashekhar Muradnar</author>
''' <remarks></remarks>
Public Class ApplicationSettings
    
    ''' <summary>
    ''' Enumerated datatype for site types (site/admin)
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum siteTypes
        admin
        site
    End Enum

   
    ''' <summary>
    ''' Site name
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SiteName()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SiteName").ToString()
        End Get
    End Property

    
    ''' <summary>
    ''' property returns the country for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Country()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("Country").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns the UK string  for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CountryUK()
        Get
            Return "UK"
        End Get
    End Property

    ''' <summary>
    ''' property returns the German string  for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CountryDE()
        Get
            Return "DE"
        End Get
    End Property


    ''' <summary>
    ''' property returns the country for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property DefaultCountry()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("DefaultCountry").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns bizdivid for web site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property BizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("BizDivId").ToString()
        End Get
    End Property
   

    ''' <summary>
    ''' propery returns type of website, wether its admin website or client site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SiteType()
        Get
            Select Case System.Configuration.ConfigurationManager.AppSettings("SiteType").ToString()
                Case "site"
                    Return siteTypes.site
                Case "admin"
                    Return siteTypes.admin
                Case Else
                    Return siteTypes.site
            End Select

        End Get
    End Property

    ''' <summary>
    ''' property returns guest login
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property GuestLogin()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("GuestLogin").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns guest password
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property GuestPassword()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("GuestPassword").ToString()
        End Get
    End Property

    


    ''' <summary>
    ''' property returns "CountryID_UK" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CountryID_UK()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CountryID_UK").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns "CountryID_DE" value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property CountryID_DE()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("CountryID_DE").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns BizDivId for Order Work UK site
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OWUKBizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OWUKBizDivId").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns BizDivId for Skills Finder UK    
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SFUKBizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SFUKBizDivId").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns BizDivId for Order Work DE
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OWDEBizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OWDEBizDivId").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property SFDEBizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SFDEBizDivId").ToString()
        End Get
    End Property
    ''' <summary>
    ''' property returns BizDivId of peer application for SF UK
    ''' for example for OW UK peer1 application is SF UK 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property SFUKPeer1BizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SFUKPeer1BizDivId").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returns BizDivId of peer application for OW
    ''' for example for OW UK peer1 application is SF UK 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OWUKPeer1BizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OWUKPeer1BizDivId").ToString()
        End Get
    End Property


    ''' <summary>
    ''' property returns BizDivId of peer application for OW DE
    ''' for example for OW UK peer1 application is SF UK 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property OWDEPeer1BizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("OWDEPeer1BizDivId").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property SFDEPeer1BizDivId()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SFDEPeer1BizDivId").ToString()
        End Get
    End Property


    ''' <summary>
    ''' Porperty returns application root path
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WSRoot()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WSRoot").ToString()
        End Get
    End Property

    ''' <summary>
    ''' property returnsweb application path
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property WebPath()
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("WebPath").ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns the VAT Percentage
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property VATPercentage() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATPercentage").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property VATPercentageOld() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATPercentageOld").ToString()
        End Get
    End Property
    Public Shared ReadOnly Property VATPercentageOlder() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATPercentageOlder").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property VATPercentageOldest() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATPercentageOldest").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property LastVATChangeDate() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("LastVATChangeDate").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property SecondLastVATChangeDate() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("SecondLastVATChangeDate").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property VATChangeDate() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATChangeDate").ToString()
        End Get
    End Property

    Public Shared ReadOnly Property VATTolerance() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("VATTolerance").ToString()
        End Get
    End Property


    ''' <summary>
    ''' Returns VAT Rate depending on the requested date
    ''' </summary>
    ''' <param name="InvoiceDate"></param>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property VATPercentage(ByVal InvoiceDate As Date) As String
        Get
            Dim vatChangeDate As Date
            Dim oldvatChangeDate As Date
            Dim oldervatChangeDate As Date
            Try
                VATChangeDate = Strings.FormatDateTime(ApplicationSettings.VATChangeDate, DateFormat.ShortDate)
                oldvatChangeDate = Strings.FormatDateTime(ApplicationSettings.LastVATChangeDate, DateFormat.ShortDate)
                oldervatChangeDate = Strings.FormatDateTime(ApplicationSettings.SecondLastVATChangeDate, DateFormat.ShortDate)
                If (InvoiceDate < vatChangeDate And InvoiceDate >= oldvatChangeDate) Then
                    Return ApplicationSettings.VATPercentageOld
                ElseIf (InvoiceDate < oldvatChangeDate And InvoiceDate >= oldervatChangeDate) Then
                    Return ApplicationSettings.VATPercentageOlder
                ElseIf (InvoiceDate >= vatChangeDate) Then
                    Return ApplicationSettings.VATPercentage
                Else
                    Return ApplicationSettings.VATPercentageOldest
                End If
            Catch ex As Exception
                Return ApplicationSettings.VATPercentage
            End Try
        End Get
    End Property


    ''' <summary>
    ''' Returns the Calculated VAT Percentage For Sales Invoice
    ''' This returns the Calculated VAT Percentage which is being used to calculate the total amount
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property VATPercentage(ByVal VATAmnt As Decimal, ByVal WOAmnt As Decimal) As String
        Get
            Dim VATPercent As Decimal
            VATPercent = (VATAmnt * 100) / WOAmnt
            Dim tolerance As Decimal = ApplicationSettings.VATTolerance
            If (VATPercent > (ApplicationSettings.VATPercentageOld - tolerance) And VATPercent < (ApplicationSettings.VATPercentageOld + tolerance)) Then
                Return ApplicationSettings.VATPercentageOld
            ElseIf (VATPercent > (ApplicationSettings.VATPercentage - tolerance) And VATPercent < (ApplicationSettings.VATPercentage + tolerance)) Then
                Return ApplicationSettings.VATPercentage
            ElseIf (VATPercent > (ApplicationSettings.VATPercentageOlder - tolerance) And VATPercent < (ApplicationSettings.VATPercentageOlder + tolerance)) Then
                Return ApplicationSettings.VATPercentageOlder
            Else
                Return ApplicationSettings.VATPercentage
            End If
        End Get
    End Property


    ''' <summary>
    ''' Returns the 1st mail id to which error mail should get sent
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Error_Email_Contact1() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("Error_Email_Contact1").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Returns the 2nd mail id to which error mail should get sent
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Error_Email_Contact2() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("Error_Email_Contact2").ToString()
        End Get
    End Property
    ''' <summary>
    ''' Returns the 2nd mail id to which error mail should get sent
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ShowDetailedError() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ShowDetailedError").ToString()
        End Get
    End Property

    Public Enum ContactClassID
        Client = 1
        Supplier = 2
        OW = 3
        Bank = 132
        WSUser = 450
        UpSell = 451
    End Enum
End Class
