Imports System.Data.SqlClient
Imports Util
Imports System.Globalization
Imports System.Threading


Public Class CommonFunctions

    Public Shared Sub InitializeProperties(ByVal paramCountry As String, ByRef LT_CONNECTION_NAME As String)
        Select Case paramCountry
            Case ApplicationSettings.CountryUK
                LT_CONNECTION_NAME = "webdbUK"
            Case ApplicationSettings.CountryDE
                LT_CONNECTION_NAME = "webdbDE"
            Case "reports"
                LT_CONNECTION_NAME = "webReportsUK"
        End Select
    End Sub

    Public Shared Sub setCultureSettings(ByVal country As String, ByVal siteType As String)
        If siteType = ApplicationSettings.siteTypes.admin Or country = ApplicationSettings.CountryUK Then
            Dim cl As New System.Globalization.CultureInfo("en-GB")
            System.Threading.Thread.CurrentThread.CurrentCulture = cl
            System.Threading.Thread.CurrentThread.CurrentUICulture = cl
        ElseIf siteType <> ApplicationSettings.siteTypes.admin And country = ApplicationSettings.CountryDE Then
            Dim cl As New System.Globalization.CultureInfo("de-DE")
            System.Threading.Thread.CurrentThread.CurrentCulture = cl
            System.Threading.Thread.CurrentThread.CurrentUICulture = cl
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = "."
        End If
    End Sub

    Public Shared Function getDBConn(ByRef LT_CONNECTION_NAME As String) As SqlClient.SqlConnection
        Dim Con As SqlConnection
        Try
            Con = DBUtil.Connect(LT_CONNECTION_NAME)
        Catch Ex As Exception
            Trace.WriteLine("Exception occured while connecting to database")
            Throw Ex
            ExceptionUtil.ThrowServerFaultCodeSoapException("Error connecting to Database.", "Sorry! Our system is currently facing some issues. Please check back later!", "", Ex, EventLogEntryType.Error)
        End Try
        Return Con
    End Function

    Public Shared Function getReportsDBConn(ByRef LT_CONNECTION_NAME As String) As SqlClient.SqlConnection
        Dim Con As SqlConnection
        Try
            Con = DBUtil.Connect(LT_CONNECTION_NAME)
        Catch Ex As Exception
            Trace.WriteLine("Exception occured while connecting to database")
            Throw Ex
            ExceptionUtil.ThrowServerFaultCodeSoapException("Error connecting to Database.", "Sorry! Our system is currently facing some issues. Please check back later!", "", Ex, EventLogEntryType.Error)
        End Try
        Return Con
    End Function

    Public Shared Function convertDate(ByVal dateToConvert As Date) As String
        Dim converteddate As String
        converteddate = CType(dateToConvert.Year, String) & "/" & CType(dateToConvert.Day, String) & "/" & CType(dateToConvert.Month, String)
        Return converteddate
    End Function

End Class
