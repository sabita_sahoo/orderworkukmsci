Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Authentication
Imports System.Data.SqlClient
Imports System.Data


<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
Public Class WSWorkOrder
    Inherits System.Web.Services.WebService

    Public Authentication As AuthenticationHeader
    Private Shared LT_CONNECTION_NAME As String = "webdbUK"
    Private Shared LT_CONNECTION_BulkAction_NAME As String = "bulkdb"
    Private Shared _siteCountry As String
    Private Shared _bizDivId As Integer
    Private Shared _siteType As String

    Public Property country() As String
        Get
            Return _siteCountry
        End Get
        Set(ByVal value As String)
            _siteCountry = value
        End Set
    End Property

    Public Property bizDivId() As Integer
        Get
            Return _bizDivId
        End Get
        Set(ByVal value As Integer)
            _bizDivId = value
        End Set
    End Property

    Public Property siteType() As String
        Get
            Return _siteType
        End Get
        Set(ByVal value As String)
            _siteType = value
        End Set
    End Property

    ''' <summary>
    ''' This webmethod intializes web service depending on the country and bizdivid
    ''' to select db depending on country 
    ''' </summary>
    ''' <param name="paramCountry"></param>
    ''' <param name="paramBizDivId"></param>
    ''' <returns>Nothing</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Initialize properties"), _
    SoapHeader("Authentication")> _
  Public Function IntializeProperties(ByVal paramCountry As String, ByVal paramBizDivId As Integer, ByVal paramSiteType As String)
        bizDivId = paramBizDivId
        country = paramCountry
        siteType = paramSiteType
        CommonFunctions.InitializeProperties(paramCountry, LT_CONNECTION_NAME)
        Return Nothing
    End Function

    ''' <summary>
    ''' This method will return the Work Orders Listing for Admin.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="Group"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Orders Listing."), _
    SoapHeader("Authentication")> _
    Public Function GetAdminWOListing(ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                                ByVal maximumRows As Integer, ByVal Group As String, ByVal CompanyID As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer, ByVal hideTestWOs As Boolean, ByVal IssueBy As String, ByVal isNextDay As Boolean, ByVal billloc As Integer, ByVal MainCat As Boolean, ByVal Watched As Boolean, ByVal WorkorderID As String, ByVal LoggedInUserID As Integer, ByVal CompanyName As String, ByVal CustomerMobile As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim i As Integer
            If CompanyName <> "" Then
                If CompanyName.IndexOf("'") <> -1 Then
                    CompanyName = CompanyName.Replace("'", "''")
                End If
                Dim strCompanyName() As String = Split(CompanyName, ",")
                CompanyName = ""
                For i = 0 To strCompanyName.GetLength(0) - 1
                    If (strCompanyName.Length > 1) Then
                        If CompanyName <> "" Then
                            CompanyName &= ","
                        End If
                        CompanyName &= "'" & CStr(strCompanyName.GetValue(i)) & "'"
                    Else
                        CompanyName = strCompanyName(0).ToString
                    End If
                Next
            End If
            Dim ProcedureName As String = "spMS_GetAdminWOListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Group", Group)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@hideTestWOs", hideTestWOs)
            If (WorkorderID = "0") Then
                WorkorderID = ""
            End If
            cmd.Parameters.AddWithValue("@WorkorderID", WorkorderID)
            cmd.Parameters.AddWithValue("@IssueBy", IssueBy)
            cmd.Parameters.AddWithValue("@isNextDay", isNextDay)
            cmd.Parameters.AddWithValue("@billloc", billloc)
            cmd.Parameters.AddWithValue("@MainCat", MainCat)
            cmd.Parameters.AddWithValue("@Watched", Watched)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@BusinessArea", 101)
            cmd.Parameters.AddWithValue("@CompanyName", CompanyName)
            cmd.Parameters.AddWithValue("@CustomerMobile", CustomerMobile)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsLisitng")
            da.Fill(ds)
            If ds.Tables.Count > 2 Then
                ds.Tables(0).TableName = "tblWorkOrdersListing"
                ds.Tables(1).TableName = "tblCount"
            End If
            ds.Tables(ds.Tables.Count - 2).TableName = "tblWOUserNotes"
            ds.Tables(ds.Tables.Count - 1).TableName = "tblBillLoc"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This method will return the Work Orders Listing for Buyer.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="Group"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Orders Listing."), _
    SoapHeader("Authentication")> _
     Public Function GetBuyerWOListing(ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                                ByVal maximumRows As Integer, ByVal Group As String, ByVal CompanyID As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetBuyerWOListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Group", Group)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsLisitng")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrdersListing"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will return the Work Orders Listing for Buyer.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="Group"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Orders Listing."), _
    SoapHeader("Authentication")> _
     Public Function GetSupplierWOListing(ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                                ByVal maximumRows As Integer, ByVal Group As String, ByVal CompanyID As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetSupplierWOListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Group", Group)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsLisitng")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrdersListing"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Webmethod to return the Work Orders Details in the admin site.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order Details."), _
    SoapHeader("Authentication")> _
    Public Function GetAdminWODetails(ByVal WOID As Integer, ByVal CompanyID As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetAdminWODetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblLastAction"
            ds.Tables(4).TableName = "tblCADetails"
            ds.Tables(5).TableName = "tblComments"
            ds.Tables(6).TableName = "tblAttachments"
            ds.Tables(7).TableName = "tblWOLoc"
            ds.Tables(8).TableName = "tblQA"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Webmethod to return the Work Orders Details in the site.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order Details."), _
    SoapHeader("Authentication")> _
     Public Function GetBuyerWODetails(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetBuyerWODetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            'ds.Tables(3).TableName = "tblLastAction"
            'ds.Tables(4).TableName = "tblCADetails"
            ds.Tables(3).TableName = "tblComments"
            ds.Tables(4).TableName = "tblAttachments"
            ds.Tables(5).TableName = "tblWOLoc"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Webmethod to return the Work Orders Details in the site.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order Details."), _
    SoapHeader("Authentication")> _
     Public Function GetSupplierWODetails(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetSupplierWODetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblLastAction"
            ds.Tables(4).TableName = "tblCADetails"
            ds.Tables(5).TableName = "tblComments"
            ds.Tables(6).TableName = "tblAttachments"
            ds.Tables(7).TableName = "tblWOLoc"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method is used to delete/discard/copy wo as draft
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="ContactID"></param>
    ''' <param name="Action"></param>
    ''' <param name="TrackCompanyID"></param>
    ''' <param name="TrackContactID"></param>
    ''' <param name="TrackContactClassID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method is used to delete/discard/copy wo as draft"), _
  SoapHeader("Authentication")> _
    Public Function WO_DiscardDelCopy(ByVal WOID As Integer, ByVal BizDivId As Integer, ByVal CompanyID As Integer, ByVal ContactID As Integer, ByVal Action As String, ByVal TrackCompanyID As Integer, ByVal TrackContactID As Integer, ByVal TrackContactClassID As Integer, ByVal WOVersionNo As Byte(), ByVal LoggedInUserID As Integer, ByVal WOTrackingVNo As Byte()) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_DiscardDelCopy"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@TrackCompanyID", TrackCompanyID)
            cmd.Parameters.AddWithValue("@TrackContactID", TrackContactID)
            cmd.Parameters.AddWithValue("@TrackContactClassID", TrackContactClassID)
            cmd.Parameters.AddWithValue("@Action", Action)
            cmd.Parameters.AddWithValue("@WOVersionNo", WOVersionNo)
            cmd.Parameters.AddWithValue("@WOTrackingVersionNo", WOTrackingVNo)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)


            Dim ds As New DataSet("WODiscardDelCopy")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            If ds.Tables.Count <> 0 Then
                ds.Tables(0).TableName = "tblWOID"
                ds.Tables(1).TableName = "tblSendMail"
                ds.Tables(2).TableName = "tblDiscardMail"
            End If

            Return ds

            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Function to get the standard values to populate the RAQ form
    ''' 1. Security Q/A
    ''' 2. Workorder types
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the RAQ Standards."), _
    SoapHeader("Authentication")> _
    Public Function woRAQGetStandards(ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spWO_RAQWorkorder_GetStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("RAQ")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrderTypes"
            ds.Tables(1).TableName = "tblWorkOrderSubTypes"
            ds.Tables(2).TableName = "tblSecurityQuestion"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woRAQGetStandards. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will return the Work order details for Create Similar WO.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work order details for Create Similar WO."), _
   SoapHeader("Authentication")> _
   Public Function woGetDetails_SampleWO(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spWO_SampleWO_Populate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SampleWO")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrder"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woGetDetails_SampleWO. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO details for Sample WO. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Save the Contact info and workorder details created using the RAQ form
    ''' </summary>
    ''' <param name="xmlContact"></param>
    ''' <param name="xmlWorkOrder"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will add raq details "), _
SoapHeader("Authentication")> _
 Public Function MS_WOAddRAQDetails(ByVal xmlContact As String, ByVal xmlWorkOrder As String, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_RAQSave"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlContact", xmlContact)
            cmd.Parameters.AddWithValue("@xmlWorkOrder", xmlWorkOrder)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim ds As New DataSet("WONotification")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSendMail"
            ds.Tables(1).TableName = "tblWorkOrder"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woAddRAQDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the RAQ details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will return the Work Orders which match the Search Criteria Passed. "), _
       SoapHeader("Authentication")> _
    Public Function GetSearchWorkOrders(ByVal BizDivId As Integer, ByVal PONumber As String, ByVal ReceiptNumber As String, ByVal PostCode As String, ByVal CompanyName As String, ByVal Keyword As String, ByVal WorkOrderId As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer, ByRef DateStart As String, ByRef DateSubmittedFrom As String, ByRef DateSubmittedTo As String, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            If Keyword.IndexOf("'") <> -1 Then
                Keyword = Keyword.Replace("'", "''")
            End If
            'If CompanyName.IndexOf("'") <> -1 Then
            '    CompanyName = CompanyName.Replace("'", "''")
            'End If
            If PONumber.IndexOf("'") <> -1 Then
                PONumber = PONumber.Replace("'", "''")
            End If
            'Poonam added on 14/8/2015 - Task - OA-56 : CLONE - Can we select multiple Company name instead of one by one.
            Dim i As Integer
            If CompanyName <> "" Then
                If CompanyName.IndexOf("'") <> -1 Then
                    CompanyName = CompanyName.Replace("'", "''")
                End If
                Dim strCompanyName() As String = Split(CompanyName, ",")
                CompanyName = ""
                For i = 0 To strCompanyName.GetLength(0) - 1
                    If (strCompanyName.Length > 1) Then
                        If CompanyName <> "" Then
                            CompanyName &= ","
                        End If
                        CompanyName &= "'" & CStr(strCompanyName.GetValue(i)) & "'"
                    Else
                        CompanyName = strCompanyName(0).ToString
                    End If
                Next
            End If
            Dim ProcedureName As String = "spMS_WO_SearchWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@Keyword", Keyword)
            cmd.Parameters.AddWithValue("@PONumber", PONumber)
            cmd.Parameters.AddWithValue("@ReceiptNumber", ReceiptNumber)
            cmd.Parameters.AddWithValue("@PostCode", PostCode)
            cmd.Parameters.AddWithValue("@WorkOrderId", WorkOrderId)
            cmd.Parameters.AddWithValue("@CompanyName", CompanyName)
            cmd.Parameters.AddWithValue("@DateStart", DateStart)
            cmd.Parameters.AddWithValue("@DateSubmittedFrom", DateSubmittedFrom) 'Modified by Poonam on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
            cmd.Parameters.AddWithValue("@DateSubmittedTo", DateSubmittedTo) 'Modified by Poonam on 30/7/2015 - OA-44 : Get the Workorders which doesnt have PO numbers with specific date range.
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.CommandTimeout = 1800
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchWorkOrders"
            ds.Tables(1).TableName = "tblCount"
            rowCount = ds.Tables("tblCount").Rows(0)(0)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_GetSearchAccounts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Web method to return the locations dataset associated for the contact
    ''' </summary>
    ''' <param name="companyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Locations associated with a contact."), _
   SoapHeader("Authentication")> _
   Public Function woGetContactsLocations(ByVal companyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spWO_GetContactsLocations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", companyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrderLoc")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblLocations"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Web method to return the standard data need for create workorder form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Orders Standards."), _
    SoapHeader("Authentication")> _
    Public Function MS_WOCreateWOGetStandards(ByVal BizDivID As Integer, ByVal ContactID As Integer, ByVal WOID As Integer, ByVal CompanyId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_CreateWorkOrder_GetStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("CreateWorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCountries"
            ds.Tables(1).TableName = "tblWorkOrderTypes"
            ds.Tables(2).TableName = "tblWorkOrderSubTypes"
            ds.Tables(3).TableName = "tblDressCode"
            ds.Tables(4).TableName = "tblAttachmentsCompany"
            ds.Tables(5).TableName = "tblAttachmentsWO"
            ds.Tables(6).TableName = "tblAttachmentsWOStatement"
            ds.Tables(7).TableName = "tblNextAvOrder"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the standard data need for create workorder form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Orders Standards."), _
    SoapHeader("Authentication")> _
    Public Function MS_WOCreateWOGetDetails(ByVal ContactID As Integer, ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_CreateWOFormDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("CreateWorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAttachmentsCompany"
            ds.Tables(1).TableName = "tblAttachmentsWO"
            ds.Tables(2).TableName = "tblLocDetails"
            ds.Tables(3).TableName = "tblProductsList"
            ds.Tables(4).TableName = "tblContactsSettings"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will return the Work order details for Edit WO.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work order details for Edit WO."), _
    SoapHeader("Authentication")> _
    Public Function MS_WOEditWOGetDetails(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_EditWO_Populate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("EditWO")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblWODetails"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woEditWOGetDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO details for Edit workorderform. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Function to add / update the workorder details from the Create workorder from
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="doMode"></param>
    ''' <param name="EditedBy"></param>
    ''' <param name="AdminCompID"></param>
    ''' <param name="AdminConID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will add/update workorder "), _
 SoapHeader("Authentication")> _
    Public Function MS_WOAddUpdateWorkOrderDetails(ByVal xmlStr As String, ByVal doMode As String, ByVal EditedBy As String, ByVal AdminCompID As Integer, ByVal AdminConID As Integer, ByVal ActionBy As String, ByVal ProductID As String, ByVal VersionNo As Byte(), ByVal LoggedInUserID As Integer, ByVal IncludeWeekends As Boolean, ByVal IncludeHolidays As Boolean, ByVal ClientQAnsId As Integer, ByVal PPPerRate As Decimal, ByVal PerRate As String, ByVal ReceiptNumber As String, ByVal ClientScope As String, ByVal TagOtherLinkage As String, ByVal IsWaitingToAutomatch As Boolean, ByVal Quantity As Decimal, ByVal RelatedWorkOrder As String) As DataSet

        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMS_WO_CreateWorkOrder_Save"

            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 1000
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@EditedBy", EditedBy)
            cmd.Parameters.AddWithValue("@AdminCompID", AdminCompID)
            cmd.Parameters.AddWithValue("@AdminConID", AdminConID)
            cmd.Parameters.AddWithValue("@ActionBy", ActionBy)
            cmd.Parameters.AddWithValue("@ProductID", ProductID)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@IncludeWeekends", IncludeWeekends)
            cmd.Parameters.AddWithValue("@IncludeBankHolidays", IncludeHolidays)
            cmd.Parameters.AddWithValue("@ClientQAnsId", ClientQAnsId)
            cmd.Parameters.AddWithValue("@PPPerRate", PPPerRate)
            cmd.Parameters.AddWithValue("@PerRate", PerRate)
            cmd.Parameters.AddWithValue("@ReceiptNumber", ReceiptNumber)
            cmd.Parameters.AddWithValue("@ClientScope", ClientScope)
            cmd.Parameters.AddWithValue("@TagOtherLinkage", TagOtherLinkage)
            cmd.Parameters.AddWithValue("@IsWaitingToAutomatch", IsWaitingToAutomatch)
            cmd.Parameters.AddWithValue("@Quantity", Quantity)
            cmd.Parameters.AddWithValue("@RelatedWorkOrder", RelatedWorkOrder)


            Dim ds As New DataSet("WONotification")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "tblSendMail"
                If ds.Tables.Count > 1 Then
                    ds.Tables(1).TableName = "tblWorkOrder"
                End If


            End If
            ds.Tables(ds.Tables.Count - 1).TableName = "EmailStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_CreateWorkOrder_Save. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the workorder data. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

  
    <WebMethod(Description:="This method will add multiple workorders"), _
        SoapHeader("Authentication")> _
    Public Function CreateMultipleWO(ByVal WOID As Integer, ByVal NoOfWOs As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSWO_CreateMultipleWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@NoOfWOs", NoOfWOs)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "tblSendMail"
                If ds.Tables.Count > 1 Then
                    ds.Tables(1).TableName = "tblWorkOrder"
                End If
            End If
            ds.Tables(ds.Tables.Count - 1).TableName = "EmailStatus"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function



    ''' <summary>
    ''' This method will return the balance and WO Listing Details for particular company and contact of type buyer
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the balance and WO Listing Details for particular company and contact of type buyer."), _
        SoapHeader("Authentication")> _
    Public Function woBuyWorkSummary(ByVal BizDivID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spWO_BuyWorkSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_SupplyWorkSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the dataset of WO summary with amount balance for Buy work order summary
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns>returns dataset with 3 tables with third table as account balance</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the balance and WO Listing Details for particular company and contact of type buyer."), _
        SoapHeader("Authentication")> _
    Public Function MS_woBuyWorkSummary(ByVal BizDivID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_BuyWorkSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_SupplyWorkSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the Workorder summary for ordermatch
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Ordermatch Standards."), _
    SoapHeader("Authentication")> _
    Public Function MS_WOOrderMatchGetSummary(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_OrderMatch_GetWOSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWOSummary"
            If ds.Tables.Count > 2 Then
                ds.Tables(2).TableName = "tblGeoCode"
            Else
                ds.Tables(1).TableName = "tblGeoCode"
            End If

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woOrderMatchGetSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO Summary for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the standard data need for Ordematch form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Ordermatch Standards."), _
    SoapHeader("Authentication")> _
    Public Function woOrderMatchGetStandards(ByVal CountryID As Integer, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spWO_Ordermatch_GetStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CountryID", CountryID)
            'cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Ordermatch")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblRegionCodes"
            'ds.Tables(1).TableName = "tblWorkOrderTypes"

            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woOrderMatchGetStandards. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the supplier list for ordermatch
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the list of suppliers for Ordermatch."), _
    SoapHeader("Authentication")> _
    Public Function woOrderMatchGetSuppliers(ByVal CompanyName As String, ByVal Keyword As String, ByVal PostCode As String, ByVal NoOfEmployees As String, ByVal ShowSupplierWithCW As Boolean, ByVal Skills As String, ByVal WOID As Integer, ByVal StatusID As String, ByVal RegionCode As String, ByVal vendorIds As String, ByVal MinWOValue As Integer, ByVal sortExpression As String, ByVal CRBCheck As String, ByVal CSCS As String, ByVal UKSecurity As String, ByVal FavSupplier As Boolean, ByVal MaximumRows As Integer, ByVal PageIndex As Integer, ByVal NearestTo As String, ByVal Distance As Integer, ByVal ApprovedServicePartner As Boolean, ByVal HideTestAccount As Boolean, ByVal EngineerCRBCheck As Integer, ByVal EngineerCSCSCheck As Boolean, ByVal EngineerUKSecurityCheck As Boolean, ByVal EngineerRightToWorkInUK As Boolean, ByVal EngineerProofOfMeeting As Boolean, ByVal strSkillKeywords As String, ByVal ExcelPartner As String, ByVal EPCP As String, ByVal CategoryType As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim i As Integer
            If Skills <> "" Then
                Skills = Skills.Replace("782", "")
                Skills = Skills.Replace(",,", ",")
                If Skills.StartsWith(",") = True Then
                    Skills = Skills.Substring(1, Skills.Length - 1)
                End If
                If Skills.EndsWith(",") = True Then
                    Skills = Skills.Substring(0, Skills.Length - 1)
                End If
            End If

            If Skills <> "" Then
                Dim strSkills() As String = Split(Skills, ",")
                Skills = ""
                For i = 0 To strSkills.GetLength(0) - 1
                    If Skills <> "" Then
                        Skills &= ","
                    End If
                    Skills &= "'" & CStr(strSkills.GetValue(i)) & "'"
                Next
            End If

            If RegionCode <> "" Then
                Dim strRegionCodeCode() As String = Split(RegionCode, ",")
                RegionCode = ""
                For i = 0 To strRegionCodeCode.GetLength(0) - 1
                    If RegionCode <> "" Then
                        RegionCode &= ","
                    End If
                    RegionCode &= "'" & CStr(strRegionCodeCode.GetValue(i)) & "'"
                Next
            End If

            If vendorIds <> "" Then
                Dim strVendorIds() As String = Split(vendorIds, ",")
                vendorIds = ""
                For i = 0 To strVendorIds.GetLength(0) - 1
                    If vendorIds <> "" Then
                        vendorIds &= ","
                    End If
                    vendorIds &= "'" & CStr(strVendorIds.GetValue(i)) & "'"
                Next
            End If
            If Keyword <> "" Then
                If Keyword.IndexOf("'") <> -1 Then
                    Keyword = Keyword.Replace("'", "")
                End If
            End If
            If CompanyName <> "" Then
                If CompanyName.IndexOf("'") <> -1 Then
                    CompanyName = CompanyName.Replace("'", "")
                End If
            End If
            Dim ProcedureName As String
            ProcedureName = "spMS_WO_Ordermatch_GetSuppliers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@SkillSet", Skills)

            cmd.Parameters.AddWithValue("@CompanyName", CompanyName)
            cmd.Parameters.AddWithValue("@NoOfEmployees", NoOfEmployees)
            cmd.Parameters.AddWithValue("@ShowSupplierWithCW", ShowSupplierWithCW)
            cmd.Parameters.AddWithValue("@vendorIds", vendorIds)


            cmd.Parameters.AddWithValue("@keyword", Keyword)
            cmd.Parameters.AddWithValue("@PostCode", PostCode)
            cmd.Parameters.AddWithValue("@RegionCode", RegionCode)
            cmd.Parameters.AddWithValue("@MinWOValue", MinWOValue)
            cmd.Parameters.AddWithValue("@StatusId", StatusID)
            cmd.Parameters.AddWithValue("@CRBCheck", CRBCheck)
            cmd.Parameters.AddWithValue("@NearestTo", NearestTo)
            cmd.Parameters.AddWithValue("@Distance", Distance)
            cmd.Parameters.AddWithValue("@UKSecurity", UKSecurity)
            cmd.Parameters.AddWithValue("@CSCS", CSCS)
            cmd.Parameters.AddWithValue("@FavSupp", FavSupplier)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@maximumRows", MaximumRows)
            cmd.Parameters.AddWithValue("@startRowIndex", PageIndex)

            cmd.Parameters.AddWithValue("@ApprovedServicePartner", ApprovedServicePartner)
            cmd.Parameters.AddWithValue("@HideTestAccount", HideTestAccount)
            cmd.Parameters.AddWithValue("@EngineerCRBCheck", EngineerCRBCheck)
            cmd.Parameters.AddWithValue("@EngineerCSCSCheck", EngineerCSCSCheck)
            cmd.Parameters.AddWithValue("@EngineerUKSecurityCheck", EngineerUKSecurityCheck)
            cmd.Parameters.AddWithValue("@EngineerRightToWorkInUK", EngineerRightToWorkInUK)
            cmd.Parameters.AddWithValue("@EngineerProofOfMeeting", EngineerProofOfMeeting)
            cmd.Parameters.AddWithValue("@strSkillKeywords", strSkillKeywords)
            'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
            cmd.Parameters.AddWithValue("@ExcelPartner", ExcelPartner)
            cmd.Parameters.AddWithValue("@EPCP", EPCP)
            cmd.Parameters.AddWithValue("@CategoryType", CategoryType)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchSuppliers"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woOrderMatchGetSuppliers. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning list of suppliers for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the supplier post code list for post code anywhere
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the list of suppliers postcodes which needs data updation for Ordermatch."), _
    SoapHeader("Authentication")> _
    Public Function woOrderMatchGetSuppliersInvalidPostCodes(ByVal CompanyName As String, ByVal Keyword As String, ByVal PostCode As String, ByVal NoOfEmployees As String, ByVal ShowSupplierWithCW As Boolean, ByVal Skills As String, ByVal WOID As Integer, ByVal StatusID As String, ByVal RegionCode As String, ByVal vendorIds As String, ByVal MinWOValue As Integer, ByVal CRBCheck As String, ByVal CSCS As String, ByVal UKSecurity As String, ByVal FavSupplier As Boolean, ByVal NearestTo As String, ByVal Distance As Integer, ByVal ExcelPartner As String, ByVal EPCP As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim i As Integer
            If Skills <> "" Then
                Skills = Skills.Replace("782", "")
                Skills = Skills.Replace(",,", ",")
                If Skills.StartsWith(",") = True Then
                    Skills = Skills.Substring(1, Skills.Length - 1)
                End If
                If Skills.EndsWith(",") = True Then
                    Skills = Skills.Substring(0, Skills.Length - 1)
                End If
            End If

            If Skills <> "" Then
                Dim strSkills() As String = Split(Skills, ",")
                Skills = ""
                For i = 0 To strSkills.GetLength(0) - 1
                    If Skills <> "" Then
                        Skills &= ","
                    End If
                    Skills &= "'" & CStr(strSkills.GetValue(i)) & "'"
                Next
            End If

            If RegionCode <> "" Then
                Dim strRegionCodeCode() As String = Split(RegionCode, ",")
                RegionCode = ""
                For i = 0 To strRegionCodeCode.GetLength(0) - 1
                    If RegionCode <> "" Then
                        RegionCode &= ","
                    End If
                    RegionCode &= "'" & CStr(strRegionCodeCode.GetValue(i)) & "'"
                Next
            End If

            If vendorIds <> "" Then
                Dim strVendorIds() As String = Split(vendorIds, ",")
                vendorIds = ""
                For i = 0 To strVendorIds.GetLength(0) - 1
                    If vendorIds <> "" Then
                        vendorIds &= ","
                    End If
                    vendorIds &= "'" & CStr(strVendorIds.GetValue(i)) & "'"
                Next
            End If
            If Keyword <> "" Then
                If Keyword.IndexOf("'") <> -1 Then
                    Keyword = Keyword.Replace("'", "")
                End If
            End If
            If CompanyName <> "" Then
                If CompanyName.IndexOf("'") <> -1 Then
                    CompanyName = CompanyName.Replace("'", "")
                End If
            End If
            Dim ProcedureName As String
            ProcedureName = "spMS_WOOrdermatch_GetSuppliersPostCodes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@SkillSet", Skills)

            cmd.Parameters.AddWithValue("@CompanyName", CompanyName)
            cmd.Parameters.AddWithValue("@NoOfEmployees", NoOfEmployees)
            cmd.Parameters.AddWithValue("@ShowSupplierWithCW", ShowSupplierWithCW)
            cmd.Parameters.AddWithValue("@vendorIds", vendorIds)


            cmd.Parameters.AddWithValue("@keyword", Keyword)
            cmd.Parameters.AddWithValue("@PostCode", PostCode)
            cmd.Parameters.AddWithValue("@RegionCode", RegionCode)
            cmd.Parameters.AddWithValue("@MinWOValue", MinWOValue)
            cmd.Parameters.AddWithValue("@StatusId", StatusID)
            cmd.Parameters.AddWithValue("@CRBCheck", CRBCheck)
            cmd.Parameters.AddWithValue("@NearestTo", NearestTo)
            cmd.Parameters.AddWithValue("@Distance", Distance)
            cmd.Parameters.AddWithValue("@UKSecurity", UKSecurity)
            cmd.Parameters.AddWithValue("@CSCS", CSCS)
            cmd.Parameters.AddWithValue("@FavSupp", FavSupplier)
            'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
            cmd.Parameters.AddWithValue("@ExcelPartner", ExcelPartner)
            cmd.Parameters.AddWithValue("@EPCP", EPCP)

            cmd.CommandTimeout = 100
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblPostCodesSuppliers"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woOrderMatchGetSuppliers. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning list of suppliers for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Web method will insert the coordinates of the post code in the tblGeoCodes table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will insert the coordinates of the post code in the tblGeoCodes table."), _
    SoapHeader("Authentication")> _
    Public Function InsertPostCodeCoordinates(ByVal xmlContent As String)
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String
            ProcedureName = "sp_InsertPostCodeCoordinates"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlContent", xmlContent)
            Dim da As New SqlDataAdapter(cmd)
            cmd.ExecuteNonQuery()
            Return ""
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in InsertPostCodeCoordinates. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while inserting the values. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This method will make entries for OW Rep and each supplier to whom the WO is sent and return all those Supplier contacts to whom the notification email of WO needs to be sent.
    ''' </summary>
    ''' <param name="SupContactIDs"></param>
    ''' <param name="WOID"></param>
    ''' <param name="DateStart"></param>
    ''' <param name="DateEnd"></param>
    ''' <param name="EstimatedTime"></param>
    ''' <param name="Value"></param>
    ''' <param name="SpecialistSuppliesParts"></param>
    ''' <param name="ActualTime"></param>
    ''' <param name="OWCompanyID"></param>
    ''' <param name="OWRepID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will make entries for OW Rep and each supplier to whom the WO is sent and return all those Supplier contacts to whom the notification email of WO needs to be sent."), _
      SoapHeader("Authentication")> _
        Public Function SendWO(ByVal SupContactIDs As String, ByVal WOID As Integer, ByVal DateStart As String, ByVal DateEnd As String, ByVal EstimatedTime As String, ByVal Value As Decimal, ByVal SpecialistSuppliesParts As Boolean, ByVal ActualTime As String, ByVal OWCompanyID As Integer, ByVal OWRepID As Integer, ByVal IsAutoMatch As Boolean, ByVal BuyerCompID As String, ByVal NearestTo As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try

            Dim ProcedureName As String = "spWO_OrderMatch_SendWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@SupContactIDs", SupContactIDs)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            If DateStart <> "" Then
                cmd.Parameters.AddWithValue("@DateStart", CommonFunctions.convertDate(DateStart))
            Else
                cmd.Parameters.AddWithValue("@DateStart", DateStart)
            End If
            If DateEnd <> "" Then
                cmd.Parameters.AddWithValue("@DateEnd", CommonFunctions.convertDate(DateEnd))
            Else
                cmd.Parameters.AddWithValue("@DateEnd", DateEnd)
            End If

            cmd.Parameters.AddWithValue("@EstimatedTime", EstimatedTime)
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@SpecialistSuppliesParts", SpecialistSuppliesParts)
            cmd.Parameters.AddWithValue("@ActualTime", ActualTime)
            cmd.Parameters.AddWithValue("@OWCompanyID", OWCompanyID)
            cmd.Parameters.AddWithValue("@OWRepID", OWRepID)
            cmd.Parameters.AddWithValue("@IsAutoMatched", IsAutoMatch)
            cmd.Parameters.AddWithValue("@BuyerCompID", BuyerCompID)
            cmd.Parameters.AddWithValue("@NearestTo", NearestTo)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contacts")
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "tblSuppliers"
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_OrderMatch_SendWO. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Work Orders Histroy.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order History."), _
    SoapHeader("Authentication")> _
     Public Function GetBuyerWOHistory(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetBuyerWOHistory"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsHistory")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblActionHistory"
            ds.Tables(4).TableName = "tblComments"
            ds.Tables(5).TableName = "tblAttachments"
            ds.Tables(6).TableName = "tblWOLoc"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Webmethod to return the Work Orders Histroy.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order History."), _
    SoapHeader("Authentication")> _
     Public Function GetSupplierWOHistory(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetSupplierWOHistory"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsHistory")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblActionHistory"
            ds.Tables(4).TableName = "tblComments"
            ds.Tables(5).TableName = "tblAttachments"
            ds.Tables(6).TableName = "tblWOLoc"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will add Blocked Dates "), _
  SoapHeader("Authentication")> _
    Public Function AddUpdateBlockedDate(ByVal CompanyID As Integer, ByVal BillingId As Integer, ByVal BlockedDate As String, ByVal IsBlocked As Boolean, ByVal IsBlockedByAdmin As Boolean, ByVal ServiceListValue As String, ByVal TimeSlot As String, ByVal mode As String) As DataSet

        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spAddBlockedDate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BillingId", BillingId)
            cmd.Parameters.AddWithValue("@BlockedDate", BlockedDate)
            cmd.Parameters.AddWithValue("@IsBlocked", IsBlocked)
            cmd.Parameters.AddWithValue("@IsBlockedByAdmin", IsBlockedByAdmin)
            cmd.Parameters.AddWithValue("@ServiceListValue", ServiceListValue)
            cmd.Parameters.AddWithValue("@TimeSlot", TimeSlot)
            cmd.Parameters.AddWithValue("@mode", mode)
            Dim ds As New DataSet("AddBlockedDate")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spAddBlockedDate. )
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Webmethod to return the Work Orders Histroy.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order History."), _
    SoapHeader("Authentication")> _
     Public Function GetAdminWOHistory(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetAdminWOHistory"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsHistory")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblActionHistory"
            ds.Tables(4).TableName = "tblComments"
            ds.Tables(5).TableName = "tblAttachments"
            ds.Tables(6).TableName = "tblWOLoc"
            ds.Tables(7).TableName = "AdditionalHistory"
            ds.Tables(8).TableName = "tblQA"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the dataset of WO summary with amount balance for supply workorder summary
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns>returns dataset with 3 tables with third table as account balance</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the balance and WO Listing Details for particular company and contact of type supplier."), _
        SoapHeader("Authentication")> _
    Public Function MS_WOSupplyWorkSummary(ByVal BizDivID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_SupplyWorkSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spWO_SupplyWorkSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the complete Work Order information.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Action"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="bizDivID"></param>
    ''' <param name="chkValid"></param>
    ''' <param name="Viewer"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Orders Listing."), _
    SoapHeader("Authentication")> _
     Public Function MS_GetWO(ByVal WOID As Integer, ByVal Action As String, ByVal CompanyID As Integer, ByVal bizDivID As Integer, ByVal chkValid As Boolean, ByVal Viewer As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_GetWOProcessDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@SupCompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@Action", Action)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            cmd.Parameters.AddWithValue("@ChkValid", chkValid)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOInfo")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrderSummary"
            ''specialist list
            If Action = "woacceptissue" Or Action = "wospcaccept" Or Action = "wospaccept" Or Action = "wospchangeca" Or Action = "woraiseissue" Or Action = "wochangeissue" Then
                If ds.Tables.Count > 1 Then
                    ds.Tables(1).TableName = "tblSpecialists"
                End If
            End If

            'last action 
            If Action = "wospchangeca" Or Action = "wochangeissue" Or Action = "woacceptissue" Then
                ds.Tables(2).TableName = "tblLastAction"
                ds.Tables(3).TableName = "tblComments"
            ElseIf Action = "woacceptca" Or Action = "worejectissue" Or Action = "wodiscussca" Or Action = "wodiscuss" Then
                ds.Tables(1).TableName = "tblLastAction"
                ds.Tables(2).TableName = "tblComments"
            End If

            If Action = "woraiseissue" Then
                ds.Tables(4).TableName = "tblCommentsRaiseIssue"
            End If

            If Action = "woacceptissue" Then
                ds.Tables(6).TableName = "tblCommentsRaiseIssue"
            End If

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' WebMethod to perform the wo process
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="SupCompanyID"></param>
    ''' <param name="SupContactID"></param>
    ''' <param name="ContactId"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="Action"></param>
    ''' <param name="bizDivID"></param>
    ''' <param name="DateStart"></param>
    ''' <param name="DateEnd"></param>
    ''' <param name="Value"></param>
    ''' <param name="SpecialistSuppliesParts"></param>
    ''' <param name="Comments"></param>
    ''' <param name="SpecialistID"></param>
    ''' <param name="ContactClassId"></param>
    ''' <param name="WOTrackID"></param>
    ''' <param name="DataRef"></param>
    ''' <param name="LastActionRef"></param>
    ''' <param name="Rating"></param>
    ''' <param name="RatingComments"></param>
    ''' <param name="AttachmentsComplete"></param>
    ''' <param name="RatedContactClassId"></param>
    ''' <param name="NewProposedDayRate"></param>
    ''' <param name="NewEstimatedTimeInDays"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will perform the wo process"), _
        SoapHeader("Authentication")> _
    Public Function MS_PerformWOProcess(ByVal WOID As Integer, ByVal SupCompanyID As Integer, ByVal SupContactID As Integer, ByVal ContactId As Integer, ByVal CompanyId As Integer, ByVal Action As String, ByVal bizDivID As Integer, ByVal DateStart As String, ByVal DateEnd As String, ByVal Value As Decimal, ByVal Comments As String, ByVal SpecialistID As Integer, ByVal ContactClassId As Integer, ByVal WOTrackID As Integer, ByVal DataRef As Integer, ByVal LastActionRef As Integer, ByVal Rating As Integer, ByVal RatingComments As String, ByVal AttachmentsComplete As String, ByVal RatedContactClassId As Integer, ByVal NewProposedDayRate As Decimal, ByVal NewEstimatedTimeInDays As Integer, ByVal AptTime As String, ByVal WOVersionNo As Byte(), ByVal WOTrackingVNo As Byte(), ByVal xmlQA As String, ByVal SLA As Integer, ByVal UserId As Integer, ByVal ChangeService As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_SaveWOActions"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@WOTrackID", WOTrackID)
            cmd.Parameters.AddWithValue("@DataRef", DataRef)
            cmd.Parameters.AddWithValue("@LastActionRef", LastActionRef)
            cmd.Parameters.AddWithValue("@SupCompanyID", SupCompanyID)
            cmd.Parameters.AddWithValue("@SupContactID", SupContactID)
            cmd.Parameters.AddWithValue("@Action", Action)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            If DateStart <> "" Then
                cmd.Parameters.AddWithValue("@DateStart", CommonFunctions.convertDate(DateStart))
            End If
            If DateEnd <> "" Then
                cmd.Parameters.AddWithValue("@DateEnd", CommonFunctions.convertDate(DateEnd))
            End If
            If AptTime <> "" Then
                cmd.Parameters.AddWithValue("@AptTime", AptTime)
            Else
                cmd.Parameters.AddWithValue("@AptTime", "")
            End If
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@SpecialistID", SpecialistID)
            cmd.Parameters.AddWithValue("@ContactClassId", ContactClassId)
            cmd.Parameters.AddWithValue("@Rating", Rating)
            cmd.Parameters.AddWithValue("@RatingComments", RatingComments)
            If AttachmentsComplete <> "" Then
                cmd.Parameters.AddWithValue("@AttachmentXSD", AttachmentsComplete)
            End If
            cmd.Parameters.AddWithValue("@RatedContactClassId", RatedContactClassId)
            If CStr(NewProposedDayRate) = "" Then
                NewProposedDayRate = 0.0
            End If
            cmd.Parameters.AddWithValue("@NewProposedDayRate", NewProposedDayRate)
            If CStr(NewEstimatedTimeInDays) = "" Then
                NewEstimatedTimeInDays = 1
            End If
            cmd.Parameters.AddWithValue("@NewEstimatedTimeInDays", NewEstimatedTimeInDays)
            cmd.Parameters.AddWithValue("@WOVersionNo", WOVersionNo)
            cmd.Parameters.AddWithValue("@WOTrackingVersionNo", WOTrackingVNo)
            cmd.Parameters.AddWithValue("@xmlQA", xmlQA)
            cmd.Parameters.AddWithValue("@SLA", SLA)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserId)
            cmd.Parameters.AddWithValue("@ChangeService", ChangeService)
            Dim ds As New DataSet("WOProcess")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            If ds.Tables.Count <> 0 Then
                If ds.Tables(0).Rows(0).Item("ContactID") <> -1 And ds.Tables(0).Rows(0).Item("ContactID") <> -10 And ds.Tables(0).Rows(0).Item("ContactID") <> -11 Then
                    Select Case Action
                        Case "wospaccept", "woacceptca"
                            If ds.Tables.Count = 4 Then
                                ds.Tables(1).TableName = "Mailers"
                                ds.Tables(2).TableName = "LostDiscardSupp"
                                If ds.Tables.Count > 3 Then
                                    ds.Tables(3).TableName = "WOStatus"
                                End If
                            ElseIf ds.Tables.Count = 3 Then
                                ds.Tables(0).TableName = "Mailers"
                                ds.Tables(1).TableName = "LostDiscardSupp"
                                If ds.Tables.Count > 2 Then
                                    ds.Tables(2).TableName = "WOStatus"
                                End If
                            End If
                        Case "woacceptissue", "worejectissue"
                            If ds.Tables.Count = 3 Then
                                ds.Tables(1).TableName = "Mailers"
                                ds.Tables(2).TableName = "tblWOStatus"
                            ElseIf ds.Tables.Count = 2 Then
                                ds.Tables(0).TableName = "Mailers"
                                ds.Tables(1).TableName = "tblWOStatus"
                            End If
                        Case "woclose"
                            If ds.Tables.Count = 5 Then
                                ds.Tables(1).TableName = "Mailers"
                                ds.Tables(2).TableName = "tblPayments"
                                ds.Tables(3).TableName = "tblAttachments"
                                ds.Tables(4).TableName = "tblUpsellInfo"
                            ElseIf ds.Tables.Count = 4 Then
                                ds.Tables(0).TableName = "Mailers"
                                ds.Tables(1).TableName = "tblPayments"
                                ds.Tables(2).TableName = "tblAttachments"
                                ds.Tables(3).TableName = "tblUpsellInfo"
                            End If
                        Case Else
                            If ds.Tables.Count = 2 Then
                                ds.Tables(1).TableName = "Mailers"
                            ElseIf ds.Tables.Count = 1 Then
                                ds.Tables(0).TableName = "Mailers"
                            End If
                    End Select
                End If
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the Workorder summary 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Ordermatch Standards."), _
    SoapHeader("Authentication")> _
    Public Function woGetSummary(ByVal WOID As Integer, ByVal Viewer As String, ByVal SupCompId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_GetWOSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            cmd.Parameters.AddWithValue("@SupCompanyID", SupCompId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWOSummary"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woOrderMatchGetSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO Summary for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This web method will update the Platform Price and make a new tracking entry for the same
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Value"></param>
    ''' <param name="AdminCompId"></param>
    ''' <param name="AdminContId"></param>
    ''' <param name="ClassId"></param>
    ''' <param name="lastActionRef"></param>
    ''' <param name="Comments"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:=" This web method will update the Platform Price and make a new tracking entry for the same"), _
      SoapHeader("Authentication")> _
    Public Function MS_WOSetPlatformPrice(ByVal WOID As Integer, ByVal Value As Decimal, ByVal AdminCompId As Integer, ByVal AdminContId As Integer, ByVal ClassId As Integer, ByVal lastActionRef As Integer, ByVal Comments As String, ByVal BizDivId As Integer, ByVal ReviewBids As Boolean, ByVal PPPerRate As Decimal, ByVal PerRate As String, ByVal VersionNo As Byte()) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try

            Dim ProcedureName As String = "spMS_WO_SetPlatformPrice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@AdminCompId", AdminCompId)
            cmd.Parameters.AddWithValue("@AdminContId", AdminContId)
            cmd.Parameters.AddWithValue("@ContactClassID", ClassId)
            cmd.Parameters.AddWithValue("@LastActionRef", lastActionRef)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@ReviewBids", ReviewBids)
            cmd.Parameters.AddWithValue("@PPPerRate", PPPerRate)
            cmd.Parameters.AddWithValue("@PerRate", PerRate)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMSWO_SetPlatformPrice. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This web method will update the Wholesale Price and make a new tracking entry for the same
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Value"></param>
    ''' <param name="AdminCompId"></param>
    ''' <param name="AdminContId"></param>
    ''' <param name="ClassId"></param>
    ''' <param name="lastActionRef"></param>
    ''' <param name="Comments"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:=" This web method will update the Wholesale Price and make a new tracking entry for the same"), _
      SoapHeader("Authentication")> _
    Public Function MS_WOSetWholesalePrice(ByVal WOID As Integer, ByVal Value As Decimal, ByVal AdminCompId As Integer, ByVal AdminContId As Integer, ByVal ClassId As Integer, ByVal lastActionRef As Integer, ByVal Comments As String, ByVal BizDivId As Integer, ByVal specialInst As String, ByVal PONumber As String, ByVal JobNumber As String, ByVal CustNumber As String, ByVal InvoiceTitle As Object, ByVal WOTitle As String, ByVal WOStartDate As Object, ByVal WOEndDate As Object, ByVal Upsellprice As Decimal, ByVal xmlUpsellContent As String, ByVal EstimatedTimeInDays As String, ByVal WholesaleDayRate As Decimal, ByVal EquipmentDetails As String, ByVal GoodsLocation As Integer, ByVal VersionNo As Byte(), ByVal PlatformPrice As Decimal, ByVal IsUpdate As String, ByVal AptTime As String, ByVal CustEmail As String, ByVal Quantity As Decimal, ByVal RelatedWorkOrder As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try

            Dim ProcedureName As String = "spMS_WO_SetWholesalePrice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@AdminCompId", AdminCompId)
            cmd.Parameters.AddWithValue("@AdminContId", AdminContId)
            cmd.Parameters.AddWithValue("@ContactClassID", ClassId)
            cmd.Parameters.AddWithValue("@LastActionRef", lastActionRef)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@specialInst", specialInst)
            cmd.Parameters.AddWithValue("@PONumber", PONumber)
            cmd.Parameters.AddWithValue("@JobNumber", JobNumber)
            cmd.Parameters.AddWithValue("@CustNumber", CustNumber)
            cmd.Parameters.AddWithValue("@InvoiceTitle", InvoiceTitle)
            '*********************Staged WO***************************
            cmd.Parameters.AddWithValue("@WOTitle", WOTitle)
            cmd.Parameters.AddWithValue("@WOStartDate", WOStartDate)
            cmd.Parameters.AddWithValue("@WOEndDate", WOEndDate)
            cmd.Parameters.AddWithValue("@EstimatedTimeInDays", EstimatedTimeInDays)
            cmd.Parameters.AddWithValue("@WholesaleDayRate", WholesaleDayRate)
            '*********************Staged WO***************************

            'UpSell Price and Invoice Title
            cmd.Parameters.AddWithValue("@xmlUpsellContent", xmlUpsellContent)
            cmd.Parameters.AddWithValue("@WOUpSellInvoiceTitle", "")
            cmd.Parameters.AddWithValue("@EquipmentDetails", EquipmentDetails)
            cmd.Parameters.AddWithValue("@GoodsLocation", GoodsLocation)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)

            'For CA user can also set platform price 
            cmd.Parameters.AddWithValue("@PlatformPrice", PlatformPrice)
            cmd.Parameters.AddWithValue("@IsUpdate", IsUpdate)
            cmd.Parameters.AddWithValue("@AptTime", AptTime)
            cmd.Parameters.AddWithValue("@CustEmail", CustEmail)
            cmd.Parameters.AddWithValue("@Quantity", Quantity)
            cmd.Parameters.AddWithValue("@RelatedWorkOrder", RelatedWorkOrder)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            ds.Tables(1).TableName = "tblSupplierDetails"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMSWO_SetPlatformPrice. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Work Orders Details in the site.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Viewer"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order Details."), _
    SoapHeader("Authentication")> _
     Public Function MS_GetWODetails(ByVal WOID As Integer, ByVal Viewer As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetWODetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContacts"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblAttachments"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will update the Buyer Accepted status in DB
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Value"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="ContactId"></param>
    ''' <param name="ClassId"></param>
    ''' <param name="lastActionRef"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will update the Buyer Accepted status in DB "), _
      SoapHeader("Authentication")> _
        Public Function MS_WOSetBuyerAccepted(ByVal WOID As Integer, ByVal Value As Decimal, ByVal CompanyId As Integer, ByVal ContactId As Integer, ByVal ClassId As Integer, ByVal lastActionRef As Integer, ByVal PONumber As String, ByVal JRSNumber As String, ByVal CustNumber As String, ByVal BusinessDivision As String, ByVal ProductName As String, ByVal AddnProductName As String, ByVal xmlAttachments As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try

            Dim ProcedureName As String = "spMS_WO_SetBuyerAccepted"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@PONumber", PONumber)
            cmd.Parameters.AddWithValue("@JRSNumber", JRSNumber)
            cmd.Parameters.AddWithValue("@CustNumber", CustNumber)
            cmd.Parameters.AddWithValue("@BusinessDivision", BusinessDivision)
            cmd.Parameters.AddWithValue("@ProductName", ProductName)
            cmd.Parameters.AddWithValue("@AddnProductName", AddnProductName)
            cmd.Parameters.AddWithValue("@xmlAttachments", xmlAttachments)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@ContactClassID", ClassId)
            cmd.Parameters.AddWithValue("@LastActionRef", lastActionRef)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            ds.Tables(1).TableName = "tblWOStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMSWO_SetPlatformPrice. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Function to get the Workorder detals related to the Cancellation form
    ''' </summary>
    ''' <param name="WorkOrderID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return wo details and account voucher detais for a given workorder id"), _
SoapHeader("Authentication")> _
 Public Function MS_WOCancellationGetDetails(ByVal WorkOrderID As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_Cancellation_GetDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WorkOrderID", WorkOrderID)
            Dim ds As New DataSet("WOCancellation")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWODetails"
            ds.Tables(1).TableName = "tblStandards"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woCancellationGetDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the RAQ details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Function to save account settings and Workorder details after WO cancellation
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Action"></param>
    ''' <param name="AdminContactID"></param>
    ''' <param name="AdminCompID"></param>
    ''' <param name="ContactClassId"></param>
    ''' <param name="CancelForSupp"></param>
    ''' <param name="BuyerCancel"></param>
    ''' <param name="SupplierCancel"></param>
    ''' <param name="Comments"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This function will do the accounting entries and save the workorder after cancellation"), _
SoapHeader("Authentication")> _
 Public Function MS_WOCancellationSave(ByVal WOID As String, ByVal Action As String, ByVal AdminContactID As Integer, ByVal AdminCompID As Integer, ByVal ContactClassId As Integer, ByVal CancelForSupp As Boolean, ByVal BuyerCancel As Decimal, ByVal SupplierCancel As Decimal, ByVal Comments As String, ByVal BizDivID As Object, ByVal WOStatus As Integer, ByVal WOVersionNo As Byte(), ByVal Rating As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_CancelWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BuyerCanChr", BuyerCancel)
            cmd.Parameters.AddWithValue("@SuppCanChr", SupplierCancel)
            cmd.Parameters.AddWithValue("@CancelForSupp", CancelForSupp)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Action", Action)
            cmd.Parameters.AddWithValue("@AdminCompId ", AdminCompID)
            cmd.Parameters.AddWithValue("@AdminContId", AdminContactID)
            cmd.Parameters.AddWithValue("@ContactClassID", ContactClassId)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@WOStatus", WOStatus)
            cmd.Parameters.AddWithValue("@WOVersionNo", WOVersionNo)
            cmd.Parameters.AddWithValue("@Rating", Rating)
            Dim ds As New DataSet("WOSendMail")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblEmailRecipients"
            If (ds.Tables.Count > 1) Then
                ds.Tables(1).TableName = "tblEmailRecipientsSupp"
            End If
            If (ds.Tables.Count > 2) Then
                ds.Tables(2).TableName = "tblEmailRecipientsSuppCancellation"
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woCancellationGetDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the RAQ details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    '''  Webmethod to return the Work Orders Listing.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="Viewer"></param>
    ''' <param name="Group"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Orders Listing."), _
    SoapHeader("Authentication")> _
     Public Function GetWOListing(ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                                ByVal maximumRows As Integer, ByVal Viewer As String, ByVal Group As String, ByVal CompanyID As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spWO_GetWOListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            cmd.Parameters.AddWithValue("@Group", Group)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsLisitng")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrdersListing"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Work Orders Details.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Viewer"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order Details."), _
    SoapHeader("Authentication")> _
     Public Function GetWODetails(ByVal WOID As Integer, ByVal Viewer As String, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spWO_GetWODetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblLastAction"
            ds.Tables(4).TableName = "tblCADetails"
            ds.Tables(5).TableName = "tblComments"
            ds.Tables(6).TableName = "tblAttachments"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Work Orders Histroy.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="Viewer"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order History."), _
    SoapHeader("Authentication")> _
     Public Function GetWOHistory(ByVal WOID As Integer, ByVal Viewer As String, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spWO_GetWOHistory"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsHistory")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblContact"
            ds.Tables(2).TableName = "tblDetails"
            ds.Tables(3).TableName = "tblActionHistory"
            ds.Tables(4).TableName = "tblComments"
            ds.Tables(5).TableName = "tblAttachments"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Webmethod to return the Balance for the Account
    ''' </summary>
    ''' <param name="Account"></param>
    ''' <param name="SubAccount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the balance for a contact for a particular account type."), _
    SoapHeader("Authentication")> _
     Public Function CheckBalance(ByVal Account As String, ByVal SubAccount As Integer, ByVal BizDivId As Integer) As Decimal
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spAccounts_CheckBalance"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            Dim balance As Decimal

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Account", Account)
            cmd.Parameters.AddWithValue("@SubAccount", SubAccount)
            cmd.Parameters.Add("@Balance", SqlDbType.Float).Direction = ParameterDirection.Output
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.ExecuteNonQuery()
            balance = CDec(cmd.Parameters.Item("@balance").Value)
            Return balance
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="CompanyId"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Webmethod to return uninvoiced closed wo for buyer"), _
    SoapHeader("Authentication")> _
     Public Function MS_GetClosedWOUnInvoiced(ByVal BizDivId As Integer, ByVal CompanyId As String, ByVal FromDate As String, ByVal ToDate As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMSAccounts_GetClosedWOUnInvoced"
            Dim cmd As New SqlCommand(ProcedureName, conn)


            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ClosedWos")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblClosedWOs"
            ds.Tables(1).TableName = "tblCount"
            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Sample Work Order's Details
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="bizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Sample Work Order's Details."), _
    SoapHeader("Authentication")> _
     Public Function GetSampleWOsDetails(ByVal WOID As Integer, ByVal bizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spWO_GetSampleWorkOrdersDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@bizDivID", bizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SampleWOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrdersDetails"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to return the Sample Work Orders
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="recordCount"></param>
    ''' <param name="showOnSite"></param>
    ''' <param name="bizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Sample Work Orders."), _
    SoapHeader("Authentication")> _
     Public Function GetSampleWOs(ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                                ByVal maximumRows As Integer, ByRef recordCount As System.Nullable(Of Integer), ByVal showOnSite As String, ByVal bizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spWO_GetSampleWorkOrders"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@showOnSite", showOnSite)
            cmd.Parameters.AddWithValue("@bizDivID", bizDivID)
            cmd.Parameters.Add(New SqlParameter("@numresults", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, Nothing, DataRowVersion.Default, 0))
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SampleWOsListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrders"
            'ds.Tables(1).TableName = "tblCount"
            recordCount = cmd.Parameters("@numresults").Value
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method returns the WO Ratings.
    ''' </summary>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivID"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method returns the WO Ratings. "), _
   SoapHeader("Authentication")> _
    Public Function GetWORatings(ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivID As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByVal CompanyId As String, ByVal Rating As Integer, ByVal WorkorderID As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_AdminGetRatings"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@Rating", Rating)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@WorkorderID", WorkorderID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)


            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchWorkOrders"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_GetSearchAccounts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method returns the details of the sample Work Order.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Sample Work Order's Details."), _
        SoapHeader("Authentication")> _
         Public Function GetSampleWOsDetailsAdmin(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spWO_GetSampleWorkOrdersDetailsAdmin"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SampleWOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrdersDetails"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning the details of the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to add / update the sample WO
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="WOCategoryID"></param>
    ''' <param name="WOTitle"></param>
    ''' <param name="WOLongDesc"></param>
    ''' <param name="PONumber"></param>
    ''' <param name="JRSNumber"></param>
    ''' <param name="CustomerNumber"></param>
    ''' <param name="SpecialInstructions"></param>
    ''' <param name="DateStart"></param>
    ''' <param name="DateEnd"></param>
    ''' <param name="EstimatedTime"></param>
    ''' <param name="Value"></param>
    ''' <param name="SpecialistSuppliesParts"></param>
    ''' <param name="LocationID"></param>
    ''' <param name="SuppDet"></param>
    ''' <param name="ClientDet"></param>
    ''' <param name="ShowOnSite"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method adds or updates a record in WorkOrderSamples table. For new record pass WOID as 0 "), _
       SoapHeader("Authentication")> _
       Public Function AddUpdateSampleWO(ByVal WOID As Integer, ByVal WOCategoryID As Integer, ByVal WOTitle As String, ByVal WOLongDesc As String, ByVal PONumber As String, ByVal JRSNumber As String, ByVal CustomerNumber As String, ByVal SpecialInstructions As String, ByVal DateStart As String, ByVal DateEnd As String, ByVal EstimatedTime As String, ByVal Value As String, ByVal SpecialistSuppliesParts As Boolean, ByVal LocationID As Integer, ByVal SuppDet As String, ByVal ClientDet As String, ByVal ShowOnSite As Boolean, ByVal BizDivId As Integer) As Integer
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spWO_UpdateSampleWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOCategoryID ", WOCategoryID)
            cmd.Parameters.AddWithValue("@WOTitle", WOTitle)
            cmd.Parameters.AddWithValue("@WOLongDesc", WOLongDesc)
            cmd.Parameters.AddWithValue("@PONumber", PONumber)
            cmd.Parameters.AddWithValue("@JRSNumber", JRSNumber)
            cmd.Parameters.AddWithValue("@CustomerNumber", CustomerNumber)
            cmd.Parameters.AddWithValue("@SpecialInstructions", SpecialInstructions)
            cmd.Parameters.AddWithValue("@LocationId", LocationID)
            cmd.Parameters.AddWithValue("@SupplierDetails", SuppDet)
            cmd.Parameters.AddWithValue("@ClientDetails", ClientDet)
            cmd.Parameters.AddWithValue("@ShowOnSite", ShowOnSite)
            'cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            'Dim BizDivId As Integer = getBizDivId()
            If BizDivId <> 0 Then
                cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            End If
            Try
                If DateStart <> "" Then
                    cmd.Parameters.AddWithValue("@DateStart", CommonFunctions.convertDate(DateStart))
                End If
            Catch ex As Exception
            End Try
            Try
                If DateEnd <> "" Then
                    cmd.Parameters.AddWithValue("@DateEnd", CommonFunctions.convertDate(DateEnd))
                End If
            Catch ex As Exception

            End Try
            Try
                cmd.Parameters.AddWithValue("@Value", CDec(Value))
            Catch ex As Exception

            End Try
            cmd.Parameters.AddWithValue("@EstimatedTime", EstimatedTime)

            cmd.Parameters.AddWithValue("@SpecialistSuppliesParts", SpecialistSuppliesParts)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters("@WOID").Direction = ParameterDirection.InputOutput
            cmd.ExecuteNonQuery()
            WOID = CInt(cmd.Parameters.Item("@WOID").Value)
            Return WOID
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This method will delete the Sample Work Order
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will delete the sample work order."), _
            SoapHeader("Authentication")> _
             Public Function DeleteSampleWorkOrder(ByVal WOID As Integer)
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spWO_DeleteSampleWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("DeleteSampleWorkOrder")
            cmd.ExecuteNonQuery()
            Return Nothing
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Save the Contact info and workorder details created using the RAQ form
    ''' </summary>
    ''' <param name="xmlContact"></param>
    ''' <param name="xmlWorkOrder"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will add raq details "), _
SoapHeader("Authentication")> _
 Public Function woAddRAQDetails(ByVal xmlContact As String, ByVal xmlWorkOrder As String, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spWO_AddRAQ_Save"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlContact", xmlContact)
            cmd.Parameters.AddWithValue("@xmlWorkOrder", xmlWorkOrder)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim ds As New DataSet("WONotification")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSendMail"
            ds.Tables(1).TableName = "tblWorkOrder"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woAddRAQDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while saving the RAQ details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the standard data need for create workorder form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Orders Standards."), _
    SoapHeader("Authentication")> _
    Public Function woCreateWOGetStandards(ByVal BizDivID As Integer, ByVal ContactID As Integer, ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spWO_CreateWorkOrder_GetStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("CreateWorkOrder")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCountries"
            ds.Tables(1).TableName = "tblWorkOrderTypes"
            ds.Tables(2).TableName = "tblWorkOrderSubTypes"
            ds.Tables(3).TableName = "tblDressCode"
            ds.Tables(4).TableName = "tblAttachmentsCompany"
            ds.Tables(5).TableName = "tblAttachmentsWO"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will return Work Order details for the selected company."), _
    SoapHeader("Authentication")> _
    Public Function woMSGenerateReportCompanyWO(ByVal BizDivID As Integer, ByVal companyId As Integer, ByVal fromDate As String, ByVal toDate As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "sp_MSGenerateReportCompanyWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@CompanyId", companyId)
            cmd.Parameters.AddWithValue("@FromDate", fromDate)
            cmd.Parameters.AddWithValue("@ToDate", toDate)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrderDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrderDetails"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will Add or Update template ."), _
SoapHeader("Authentication")> _
Public Function AddUpdateTemplateWO(ByVal xmlStr As String, ByVal doMode As String, ByVal SpecialistSuppliesParts As String, ByVal EstimatedTime As String) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Dim WOID As Integer

        Try
            Dim ProcedureName As String = "spMS_WO_AddUpdateTemplate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@SpecialistSuppliesParts", SpecialistSuppliesParts)
            cmd.Parameters.AddWithValue("@EstimatedTime", EstimatedTime)
            cmd.Parameters.AddWithValue("@WOID", 0)
            cmd.Parameters("@WOID").Direction = ParameterDirection.InputOutput
            cmd.ExecuteNonQuery()
            WOID = CInt(cmd.Parameters.Item("@WOID").Value)
            Return WOID
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will return template details based on the TemplateID passed."), _
SoapHeader("Authentication")> _
Public Function GetTemplateForTemplateID(ByVal TemplateID As Integer, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_GetTemplateForTemplateID"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", TemplateID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrderDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWODetails"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function



    <WebMethod(Description:="This method will return template listing based on the BizDivID passed."), _
SoapHeader("Authentication")> _
Public Function GetTemplateListing(ByVal BizDivID As Integer, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_GetTemplateList"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("TemplateListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrderTemplate"
            ds.Tables(1).TableName = "tblNoOfRows"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Web method to return the data to show on upload parameters page
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This method will return the Work Order category id,Dress code,Attachments."), _
    SoapHeader("Authentication")> _
    Public Function MS_GetWOUploadParametersDetails(ByVal BizDivId As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_GetWOUploadParametersDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("UploadParams")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWOCombID"
            ds.Tables(1).TableName = "tblDressCode"
            ds.Tables(2).TableName = "tblAttachments"
            ds.Tables(3).TableName = "tblWOUploadAccounts"
            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Web method to save the data from upload parameters page to db
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This method will return the Work Order category id,Dress code,Attachments."), _
    SoapHeader("Authentication")> _
    Public Function MS_SaveWOUploadParametersDetails(ByVal BizDivId As Integer, ByVal CompanyID As Integer, ByVal DressCodeId As String, ByVal IsEnable As Boolean, ByVal WOCombId As String, ByVal XMLDoc As String, ByVal LoggedInUserID As Integer)
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_SaveWOUploadParametersDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@DressCodeId", DressCodeId)
            cmd.Parameters.AddWithValue("@WOCombId", WOCombId)
            cmd.Parameters.AddWithValue("@IsEnable", IsEnable)
            cmd.Parameters.AddWithValue("@xmlDoc", XMLDoc)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.ExecuteNonQuery()
            Return Nothing
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the authenticated conatctid and maincontactid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This method will return the Work Order category id,Dress code,Attachments."), _
    SoapHeader("Authentication")> _
    Public Function MS_AuthenticateWOUploadAccount(ByVal BizDivId As Integer, ByVal AccountId As String, ByVal Password As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_AuthenticateWOUploadAccount"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@AccountId", AccountId)
            cmd.Parameters.AddWithValue("@Password", Password)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ContactDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContactDetails"
            ds.Tables(1).TableName = "tblContactAddress"
            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''
    ''' <summary>
    ''' Web method to make an entry in [tblWSWOUploadLog] for transaction made by buyer application with wxposed web service
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="Web method to make an entry in [tblWSWOUploadLog] for transaction made by buyer application with wxposed web service"), _
    SoapHeader("Authentication")> _
    Public Function MS_InsertWOUploadLogEntry(ByVal AccountId As String, ByVal BizDivId As Integer, ByVal IPAddress As String, ByVal Method As String, ByVal Status As String, ByVal ResponseMessage As String)
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_InsertWOUploadLogEntry"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AccountId", AccountId)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@IPAddress", IPAddress)
            cmd.Parameters.AddWithValue("@Method", Method)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@ResponseMessage", ResponseMessage)
            cmd.ExecuteNonQuery()
            Return Nothing
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''
    ''' <summary>
    ''' Web method to make an entry in [tblWSWOUploadLog] for transaction made by buyer application with wxposed web service
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="Web method to get details from  [tblWSWOUploadLog] for transaction made by buyer application with exposed web service"), _
    SoapHeader("Authentication")> _
    Public Function MS_getWebServiceLogDetails(ByVal companyId As Integer, ByVal BizDivId As Integer, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_getWebServiceLogDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@companyId", companyId)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("LogDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblLogDetails"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''
    ''' <summary>
    ''' Web Method to get data to Split the work order
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="Web Method to get data to Split the work order"), _
    SoapHeader("Authentication")> _
    Public Function woGetDataToSplitWO(ByVal WOID As Object, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetDataToSplitWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("PricingDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "PricingDetails"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''
    ''' <summary>
    ''' Web Method to create Staged Work orders
    ''' </summary>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="Web Method to to create Staged Work orders"), _
    SoapHeader("Authentication")> _
    Public Sub CreatStagedWorkOrders(ByVal ContactID As Integer, ByVal CompanyID As Integer, ByVal BizDivID As Integer, ByVal NoOfStages As Integer, ByVal EstimatedTimeInDays As Integer, ByVal WholesalePrice As Decimal, ByVal WholesaleJobDayRate As Decimal, ByVal PlatformPrice As Decimal, ByVal PlatformJobDayRate As Object, ByVal WorkOrderID As String, ByVal BuyerCompanyId As Integer, ByVal BuyerContactId As Integer, ByVal DeferalDate As Integer)
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_SplitWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@NoOfStages", NoOfStages)
            cmd.Parameters.AddWithValue("@EstimatedTimeInDays", EstimatedTimeInDays)
            cmd.Parameters.AddWithValue("@WholesalePrice", WholesalePrice)
            cmd.Parameters.AddWithValue("@WholesaleJobDayRate", WholesaleJobDayRate)
            cmd.Parameters.AddWithValue("@PlatformPrice", PlatformPrice)
            cmd.Parameters.AddWithValue("@PlatformJobDayRate", PlatformJobDayRate)
            cmd.Parameters.AddWithValue("@WorkOrderID", WorkOrderID)
            cmd.Parameters.AddWithValue("@BuyerCompanyId", BuyerCompanyId)
            cmd.Parameters.AddWithValue("@BuyerContactId", BuyerContactId)
            cmd.Parameters.AddWithValue("@DeferalDate", DeferalDate)
            Dim da As New SqlDataAdapter(cmd)
            'Dim ds As New DataSet("Success")
            cmd.ExecuteNonQuery()
            'da.Fill(ds)
            'ds.Tables(0).TableName = "Success"
            'Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Sub
    ''

    ''' <summary>
    ''' this webmethod  returns the data for web services access for the given bizdivid and companyid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="Web method to get details from  [tblWSWOUploadAccounts] for web service accounts"), _
    SoapHeader("Authentication")> _
    Public Function MS_GetWebServiceAccessKey(ByVal companyId As Integer, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetWebServiceAccessKey"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@companyId", companyId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WebServiceAccessKey")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWebServiceAccessKey"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' this webmethod  creates the entry for credentials to access OW  web services 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="this webmethod  creates the entry for credentials to access OW  web services "), _
    SoapHeader("Authentication")> _
    Public Function MS_CreateWebSrviceAccessKeyEntry(ByVal companyId As Integer, ByVal BizDivId As Integer, ByVal password As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_CreateWebSrviceAccessKeyEntry"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyId", companyId)
            cmd.Parameters.AddWithValue("@Password", password)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WebServiceAccessKey")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWebServiceAccessKey"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' this webmethod  updates the IsEnable status for the web service acconut
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="this webmethod  updates the IsEnable status for the web service acconut"), _
    SoapHeader("Authentication")> _
    Public Function MS_UpdateWebServiceAccessKeyDetails(ByVal AccountId As String, ByVal isEnable As Boolean)
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_UpdateWebServiceAccessKeyDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AccountId", AccountId)
            cmd.Parameters.AddWithValue("@IsEnable", isEnable)
            cmd.ExecuteNonQuery()
            Return Nothing
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' this webmethod  returns the status of the wo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="this webmethod  returns the status of the wo"), _
    SoapHeader("Authentication")> _
    Public Function woGetWOStatus(ByVal WOID As String, ByVal WOType As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetWOStatus"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@WOTYPE", WOType)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOStaus")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWOStaus"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' this webmethod  returns the status of the wo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="this webmethod  returns the Valid Invalid List of wo"), _
    SoapHeader("Authentication")> _
    Public Function GetValidInvaildWOList(ByVal WOID As String, ByVal mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_GetAcceptedMultipleWOs"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Mode", mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ListWO")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' this webmethod  returns the status of the wo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="this webmethod  update Multiple wo status"), _
    SoapHeader("Authentication")> _
    Public Function UpdateMultipleWOStatus(ByVal WOID As String, ByVal mode As String, ByVal SupplierCompID As Integer, ByVal AdminCompID As Integer, ByVal AdminContactID As Integer, ByVal BizDivId As Integer, ByVal Rating As String, ByVal RatingComments As String, ByVal RatingClassID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_BulkAction_NAME)
        Try

            Dim ProcedureName As String = "spAdmin_UpdateMultipleWOs"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
	    cmd.CommandTimeout = 1000
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Mode", mode)
            cmd.Parameters.AddWithValue("@SupplierCompID", SupplierCompID)
            cmd.Parameters.AddWithValue("@AdminCompID", AdminCompID)
            cmd.Parameters.AddWithValue("@AdminContactID", AdminContactID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivId)
            cmd.Parameters.AddWithValue("@Rating", Rating)
            cmd.Parameters.AddWithValue("@RatingComments", RatingComments)
            cmd.Parameters.AddWithValue("@RatingClassID", RatingClassID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOPrice")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' this webmethod  adds multiple upsell sales invoice
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="this webmethod  adds multiple upsell sales invoice"), _
    SoapHeader("Authentication")> _
    Public Function AddMultipleUpSellInvoice(ByVal WOID As String, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_BulkAction_NAME)
        Try

            Dim ProcedureName As String = "spMS_AdminAddUpSellInvoices"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivId)
            'cmd.ExecuteNonQuery()
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' this webmethod  returns the Price of the WOID provided
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="this webmethod  update Multiple wo status"), _
    SoapHeader("Authentication")> _
    Public Function GetWOPrice(ByVal WOID As String, ByVal mode As Int32) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spAdmin_GetWOPrice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Mode", mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("MailerDetails")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Add or update Product details
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="doMode"></param>
    ''' <param name="SpecialistSuppliesParts"></param>
    ''' <param name="EstimatedTime"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will Add or Update Product."), _
   SoapHeader("Authentication")> _
    Public Function AddUpdateAdminProduct(ByVal xmlStr As String, ByVal doMode As String, ByVal Location As Integer, ByVal IsDeleted As Boolean, ByVal xmlQA As String, ByVal IsSignOffSheetReqd As Boolean, ByVal IsHidden As Boolean, ByVal PPPerRate As Decimal, ByVal PerRate As String, ByVal IsWaitingToAutomatch As Boolean) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Dim WOID As Integer
        Try
            Dim ProcedureName As String = "spMS_WO_AddUpdateProduct"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@Location", Location)
            cmd.Parameters.AddWithValue("@IsDeleted", IsDeleted)
            cmd.Parameters.AddWithValue("@IsHidden", IsHidden)
            cmd.Parameters.AddWithValue("@IsWaitingToAutomatch", IsWaitingToAutomatch)
            cmd.Parameters.AddWithValue("@PPPerRate", PPPerRate)
            cmd.Parameters.AddWithValue("@PerRate", PerRate)
            cmd.Parameters.AddWithValue("@WOID", 0)
            cmd.Parameters("@WOID").Direction = ParameterDirection.InputOutput
            cmd.Parameters.AddWithValue("@xmlQA", xmlQA)
            cmd.Parameters.AddWithValue("@IsSignOffSheetReqd", IsSignOffSheetReqd)
            cmd.ExecuteNonQuery()
            WOID = CInt(cmd.Parameters.Item("@WOID").Value)
            Return WOID
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will Add or Update Product."), _
   SoapHeader("Authentication")> _
   Public Function populateProductDetails(ByVal ProductID As Integer, ByVal CompanyID As Integer, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Dim WOID As Integer

        Try
            Dim ProcedureName As String = "spMS_WO_GetProductDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ProductID", ProductID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will Add or Update Product."), _
       SoapHeader("Authentication")> _
       Public Function populateProductDetailsNew(ByVal ProductID As Integer, ByVal CompanyID As Integer, ByVal BizDivID As Integer) As DataTable
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Dim WOID As Integer

        Try
            Dim ProcedureName As String = "spMS_WO_GetProductDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ProductID", ProductID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataTable
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will get the product listing."), _
   SoapHeader("Authentication")> _
    Public Function GetProductListing(ByVal CompanyID As Integer, ByVal BizDivID As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                                ByVal maximumRows As Integer, ByRef recordCount As System.Nullable(Of Integer), ByVal ShowDeleted As Boolean) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetProductListing "
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID.ToString)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID.ToString)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@ShowDeleted", ShowDeleted)
            cmd.Parameters.AddWithValue("@ShowHidden", 1)
            cmd.Parameters.Add(New SqlParameter("@numresults", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, Nothing, DataRowVersion.Default, 0))
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("LocationListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "Locations"
            HttpContext.Current.Items("rowcount") = ds.Tables(1).Rows(0).Item(0)
            recordCount = HttpContext.Current.Items("rowcount")
            ds.Tables(2).TableName = "tblAMStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Web method to return the Billing locations dataset associated for the contact
    ''' </summary>
    ''' <param name="companyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Locations associated with a contact."), _
   SoapHeader("Authentication")> _
   Public Function woGetContactsBillingLocations(ByVal companyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spWO_GetContactsBillingLocations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", companyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrderLoc")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblLocations"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This WebMethod gets the PO number for the given WOID for Admin - Edit PO number page
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks>Pratik Trivedi - 17 Oct, 2008</remarks>
    <WebMethod(Description:="This method will return the Locations associated with a contact."), _
        SoapHeader("Authentication")> _
    Public Function GetPONumber(ByVal WOID As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_GetPONumber"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            'ds.Tables(0).TableName = "PONumber"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This Webmethod updates the PO number for the given WOID.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="NewPONumber"></param>
    ''' <returns></returns>
    ''' <remarks>Pratik Trivedi - 17 Oct, 2008</remarks>
    <WebMethod(Description:="This method will return the Locations associated with a contact."), _
        SoapHeader("Authentication")> _
    Public Function UpdatePONumber(ByVal WOID As String, ByVal NewPONumber As String, ByVal UserId As Integer, ByVal type As String, ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_UpdatePONumber"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@PONum", NewPONumber)
            cmd.Parameters.AddWithValue("@LoggedInUserId", UserId)
            cmd.Parameters.AddWithValue("@Type", type)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            Dim da As New SqlDataAdapter(cmd)
            cmd.ExecuteNonQuery()
            Dim ds As New DataSet("AutoMatchSettings")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method will retrive the AutoMatch Settings for a user"), _
SoapHeader("Authentication")> _
Public Function GetAutoMatchDetails(ByVal BizDivId As Integer, ByVal CompanyID As Integer, ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetAutoMatchDetailsAndFavSuppliers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@BuyerCompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@NewWOId", WOID)
            cmd.ExecuteNonQuery()
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AutoMatchSettings")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAutomatch"
            If ds.Tables.Count > 1 Then
                ds.Tables(1).TableName = "tblSearchSuppliers"
                If ds.Tables.Count > 2 Then
                    ds.Tables(2).TableName = "tblCount"
                    If ds.Tables.Count > 3 Then
                        ds.Tables(3).TableName = "tblPostCode"
                    End If
                End If
            End If
            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This WebMethod gets the Notes for the given WOID for Admin - WO Notes
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks>Pratik Trivedi - 14 Nov, 2008</remarks>
    <WebMethod(Description:="This WebMethod gets the Notes for the given WOID for Admin - WO Notes"), _
        SoapHeader("Authentication")> _
    Public Function GetWONotes(ByVal WOID As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_GetWONotes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Notes"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This Webmethod updates the Notes for the given WOID.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="ContactID"></param>
    ''' <param name="Note"></param>
    ''' <returns></returns>
    ''' <remarks>Pratik Trivedi - 17 Oct, 2008</remarks>
    <WebMethod(Description:="This method will return the Locations associated with a contact."), _
        SoapHeader("Authentication")> _
    Public Function UpdateWONotes(ByVal WOID As String, ByVal ContactID As Integer, ByVal Note As String, ByVal ReturnData As Boolean, ByVal NoteCategory As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_SaveWONotes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@Note", Note)
            cmd.Parameters.AddWithValue("@ReturnData", ReturnData)
            cmd.Parameters.AddWithValue("@CompanyID", System.Configuration.ConfigurationManager.AppSettings("AdminCompanyID"))
            cmd.Parameters.AddWithValue("@ContactClassID", ApplicationSettings.ContactClassID.OW)
            'poonam added on 13/4/2016 - Task - OA-224 : OA - Add Quick note dropdown with note types - Phase 2
            cmd.Parameters.AddWithValue("@NoteCategory", NoteCategory)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            If ReturnData = True Then
                ds.Tables(1).TableName = "Notes"
            End If
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Called by CASendMail to populate Details on Send CA Mail
    ''' Created By Pankaj Malav on 16 Dec 2008
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order Details + Comments and Action details for CA."), _
    SoapHeader("Authentication")> _
     Public Function GetCADetails(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_GetCADetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblCADetails"
            ds.Tables(2).TableName = "tblComments"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Called by CASendMail to Save Details and get Info for Send CA Mail
    ''' Created By Pankaj Malav on 16 Dec 2008
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="Comments"></param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order Details + Comments and Action details for CA."), _
    SoapHeader("Authentication")> _
     Public Function SaveCACommentsSendMail(ByVal CompanyID As String, ByVal WOID As Integer, ByVal Comments As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_SaveCASendMailDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            'ds.Tables(0).TableName = "tblSummary"
            'ds.Tables(1).TableName = "tblCADetails"
            'ds.Tables(2).TableName = "tblComments"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <param name="Val"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order Auto Suggest."), _
   SoapHeader("Authentication")> _
    Public Function GetAutoSuggest(ByVal mode As String, ByVal Val As String, ByVal CompanyID As Integer) As DataTable
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_AutoSuggest"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Mode", mode)
            cmd.Parameters.AddWithValue("@Val", Val)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsSuggest")
            da.Fill(ds)
            'ds.Tables(0).TableName = "tblSummary"
            'ds.Tables(1).TableName = "tblCADetails"
            'ds.Tables(2).TableName = "tblComments"
            Return ds.Tables(0)
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Added By Pankaj Malav on 26 Feb 2009 for getting AdminWOHistory enbaling undiscarding the workorder 
    ''' </summary>
    ''' <param name="woid"></param>
    ''' <param name="Companyid"></param>
    ''' <param name="WOTackingId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will enable the admin to undiscard the work order"), _
       SoapHeader("Authentication")> _
        Public Function WO_AdminWODiscard(ByVal woid As String, ByVal Companyid As Integer, ByVal WOTackingId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_AdminUndiscard"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", woid)
            cmd.Parameters.AddWithValue("@CompanyID", Companyid)
            cmd.Parameters.AddWithValue("@WOTrackingId", WOTackingId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSuccess"
            If ds.Tables.Count > 1 Then
                ds.Tables(1).TableName = "tblActionHistory"
                ds.Tables(2).TableName = "tblComments"
                ds.Tables(3).TableName = "tblSupplierEmail"
                ds.Tables(4).TableName = "AdditionalHistory"
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Fetch records for generating depot sheets
    ''' </summary>
    ''' <param name="woid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Fetch records for generating depot sheets"), _
       SoapHeader("Authentication")> _
        Public Function WO_GenerateDepot(ByVal woid As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_GenerateDepotSheet"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", woid)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("GenDepot")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblDepot"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Fetch records for Printing depot sheets
    ''' </summary>
    ''' <param name="woid"></param>
    ''' <param name="ContactId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Fetch records for Printing depot sheets"), _
       SoapHeader("Authentication")> _
        Public Function WO_PrintDepotSheets(ByVal woid As String, ByVal ContactId As Integer, ByVal mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_PrintDepotSheet"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", woid)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@mode", mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("PrintDepot")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblDepot"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function



    <WebMethod(Description:="Fetch Post code co-ordinates"), _
    SoapHeader("Authentication")> _
     Public Function WO_GetPostCodeCoordinates(ByVal postCode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_GetPostCodeCo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@postCode", postCode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("postCode")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblpostCode"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="Check if postcode exists in the db"), _
               SoapHeader("Authentication")> _
        Public Function DoesPostCodeExists(ByVal PostCode As String) As Boolean
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim rowCount As Boolean
            Dim ProcedureName As String
            ProcedureName = "sp_InsertPostCodeCheck"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Postcode", PostCode)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCount"
            rowCount = CBool(ds.Tables("tblCount").Rows(0)(0))
            Return rowCount
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in Postcode Info Fetching. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while Fetching Postcode Info. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This method will return the Work order details for Edit WO.
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Check if postcode exists in the db"), _
               SoapHeader("Authentication")> _
    Public Function MS_WOFormEditWOGetDetails(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WOFORM_EditWOPopulate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("EditWO")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWODetails"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woEditWOGetDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO details for Edit workorderform. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This is created specifically for New WO form created on 12 May 2005
    ''' </summary>
    ''' <param name="ProductID"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Check if postcode exists in the db"), _
               SoapHeader("Authentication")> _
    Public Function populateWOFormProductDetails(ByVal ProductID As Integer, ByVal CompanyID As Integer, ByVal BizDivId As Integer, ByVal LoggedInUserID As Integer, ByVal Mode As String, ByVal ServiceID As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WOFORM_GetProductDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ProductID", ProductID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivId)
            cmd.Parameters.AddWithValue("@LoggedInUserId", LoggedInUserID)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@ServiceID", ServiceID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This is created specifically for Utility which will fetch the invalid supplier post codes
    ''' </summary>  
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Check if postcode exists in the db"), _
               SoapHeader("Authentication")> _
    Public Function PopulateInValidSupplierPostCodes() As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetInValidSupplierPostCodes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Web method to return the Workorder summary 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Data required for population of WO Set WholesalePrice page."), _
    SoapHeader("Authentication")> _
    Public Function woGetWholesalePriceSummary(ByVal WOID As Integer, ByVal Viewer As String, ByVal SupCompId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_GetWOSetWholesalePriceSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Viewer", Viewer)
            cmd.Parameters.AddWithValue("@SupCompanyID", SupCompId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "tblWOSummary"
                ds.Tables(1).TableName = "tblDepotLocations"
                ds.Tables(2).TableName = "tblWOUpsell"
            End If
            
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woOrderMatchGetSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO Summary for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the Workorder summary 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Data required for population of WO Set WholesalePrice page."), _
    SoapHeader("Authentication")> _
    Public Function CopyProduct(ByVal ProductIDs As String, ByVal CompId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_WO_CopyProduct"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ProductIDs", ProductIDs)
            cmd.Parameters.AddWithValue("@CompanyID", CompId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOSummary")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in woOrderMatchGetSummary. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning WO Summary for Ordermatch form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This webmethod will update the Price for a given Work Order ID
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <returns>result</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will update the Rating for a given Work Order ID"), _
     SoapHeader("Authentication")> _
     Public Function UpdateRating(ByVal WOID As String, ByVal Mode As String, ByVal Rating As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_UpdateRatings"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WorkOrderID", WOID)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@Rating", Rating)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "tblRatings"
            End If
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod will enable Mass acceptance of WorkOrder in Issue Status
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="AdminCompID"></param>
    ''' <param name="AdminContactID"></param>
    ''' <param name="Comments"></param>
    ''' <param name="contactclassid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will enable Mass acceptance of WorkOrder in Issue Status"), _
     SoapHeader("Authentication")> _
     Public Function AcceptMultipleIssueWO(ByVal WOID As String, ByVal AdminCompID As Integer, ByVal AdminContactID As Integer, ByVal Comments As String, ByVal contactclassid As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_BulkAction_NAME)
        Try
            Dim ProcedureName As String = "spMSAdmin_AcceptMultipleIssueWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@AdminCompID", AdminCompID)
            cmd.Parameters.AddWithValue("@AdminContactID", AdminContactID)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@ContactClassID", contactclassid)
            cmd.Parameters.AddWithValue("@BizDivID", 1)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Added By Snehal for fetching and removing attachments for sign off sheets
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will enable the admin to remove/ reattach attachments for sign off sheets"), _
       SoapHeader("Authentication")> _
        Public Function GetAttachments(ByVal workorderid As String, ByVal Companyid As String, ByVal mode As String, ByVal AttachmentsId As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_Attachments"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WorkOrderid", workorderid)
            cmd.Parameters.AddWithValue("@CompanyId", Companyid)
            cmd.Parameters.AddWithValue("@mode", mode)
            cmd.Parameters.AddWithValue("@AttachmentsId", AttachmentsId)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            If (mode = "GetAttachments") And (ds.Tables.Count > 0) Then
                ds.Tables(0).TableName = "tblAttachmentDetails"
                ds.Tables(1).TableName = "tblCount"
            End If

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Added By Snehal for fetching and removing attachments for sign off sheets
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will enable the admin to remove/ reattach attachments for sign off sheets"), _
       SoapHeader("Authentication")> _
    Public Function SaveAttachments(ByVal xmlContent As String, ByVal WOID As Integer, ByVal Source As String, ByVal LinkageSource As String, ByVal WOUploadFlag As Integer, ByVal LoggedInUserID As Integer)
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spAttachment"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlContent)
            cmd.Parameters.AddWithValue("@ID", WOID)
            cmd.Parameters.AddWithValue("@Source", Source)
            cmd.Parameters.AddWithValue("@LinkageSource", LinkageSource)
            cmd.Parameters.AddWithValue("@WOUploadFlag", WOUploadFlag)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            'Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <param name="Val"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Goods Location Auto Suggest."), _
   SoapHeader("Authentication")> _
    Public Function GetGoodsLocation(ByVal mode As String, ByVal Val As String, ByVal CompanyID As Integer) As DataTable
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_AutoSuggestGoodsLocation"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Mode", mode)
            cmd.Parameters.AddWithValue("@Val", Val)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsSuggest")
            da.Fill(ds)
            Return ds.Tables(0)
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This function will return the rating of workorder
    ''' </summary>
    ''' <param name="woid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Fetch rating of workorder"), _
       SoapHeader("Authentication")> _
    Public Function GetWorkorderRating(ByVal woid As String, ByVal LoggedInUserID As Integer, ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_GetRating"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", woid)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WORating")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblRating"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This function will return the rating of workorder
    ''' </summary>
    ''' <param name="woid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="saves new rating and comment"), _
       SoapHeader("Authentication")> _
    Public Function UpdateWorkorderRating(ByVal WOID As String, ByVal AdminCompID As Integer, ByVal AdminContactID As Integer, ByVal Comments As String, ByVal rating As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_UpdateRating"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@AdminCompID", AdminCompID)
            cmd.Parameters.AddWithValue("@AdminContactID", AdminContactID)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@BizDivID", 1)
            cmd.Parameters.AddWithValue("@Rating", rating)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("UpdateWORating")
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This function will return the details of depot location
    ''' </summary>
    ''' <param name="woid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="saves new rating and comment"), _
       SoapHeader("Authentication")> _
    Public Function GetDepotLocDetails(ByVal AddressID As Integer) As DataTable
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_GetDepotLocDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AddressID", AddressID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDepotLocDetails")
            da.Fill(ds)
            Return ds.Tables(0)
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This webmethod will return all the voucher entries for selected voucher type"), _
       SoapHeader("Authentication")> _
       Public Function MS_TescoReport(ByVal Month As Integer, ByVal Year As Integer, ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_TescoReport"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Month", Month)
            cmd.Parameters.AddWithValue("@Year", Year)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            Dim ds As New DataSet("TescoReport")
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "Locations"
            If ds.Tables.Count = 3 Then
                ds.Tables(1).TableName = "JobDetails"
                ds.Tables(2).TableName = "TotalJobs"
            End If
            
            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Check Service Sequence
    ''' </summary>
    ''' <param name="LocationId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Check Service Sequence."), _
     SoapHeader("Authentication")> _
     Public Function CheckServiceSequence(ByVal CompanyId As Integer, ByVal Sequence As Integer, ByVal WOID As Integer) As DataSet
        Dim IsDepot As Boolean = False
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_CheckServiceSequence"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@Sequence", Sequence)
            cmd.Parameters.AddWithValue("@WOID", WOID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Status"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    '''' <summary>
    '''' Web method will update the service sequence
    '''' </summary>
    '''' <returns></returns>
    '''' <remarks></remarks>
    '<WebMethod(Description:="This method will update the service sequence in tblworkordertemplates table."), _
    'SoapHeader("Authentication")> _
    'Public Function UpdateServiceSequence(ByVal xmlContent As String)
    '    CommonFunctions.setCultureSettings(country, siteType)
    '    Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
    '    Try
    '        Dim ProcedureName As String
    '        ProcedureName = "spMS_UpdateServiceSequence"
    '        Dim cmd As New SqlCommand(ProcedureName, conn)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.Parameters.AddWithValue("@xmlContent", xmlContent)
    '        Dim da As New SqlDataAdapter(cmd)
    '        cmd.ExecuteNonQuery()
    '        Return ""
    '    Catch ex As Exception
    '        Throw ex 'ExceptionUtil.CreateSoapException("Exception in InsertPostCodeCoordinates. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while inserting the values. Please try again.", ex)
    '    Finally
    '        Try : conn.Close() : Catch : End Try
    '    End Try
    'End Function

    ''' <summary>
    ''' Web method will update the service sequence
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will update the service sequence in tblworkordertemplates table."), _
    SoapHeader("Authentication")> _
    Public Function UpdateServiceSequence(ByVal Mode As String, ByVal Sequence As Integer, ByVal CompanyId As Integer)
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String
            ProcedureName = "spMS_UpdateServiceSequence"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@Sequence", Sequence)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            Dim da As New SqlDataAdapter(cmd)
            cmd.ExecuteNonQuery()
            Return ""
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in InsertPostCodeCoordinates. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while inserting the values. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This Webmethod updates Seq using pop up of swap or move to in product listing
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="ContactID"></param>
    ''' <param name="Note"></param>
    ''' <returns></returns>
    ''' <remarks>Sumit Kale</remarks>
    <WebMethod(Description:="This Webmethod updates Seq using pop up of swap or move to in product listing."), _
        SoapHeader("Authentication")> _
    Public Function MoveSwapSequence(ByVal OldSeq As Integer, ByVal NewSeq As Integer, ByVal CompanyId As Integer, ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMS_MoveSwapSequence"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@OldSeq", OldSeq)
            cmd.Parameters.AddWithValue("@NewSeq", NewSeq)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@Mode", Mode)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Status"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This Webmethod is used for the Fast close of the workorders
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="LoggedInUserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This Webmethod updates Seq using pop up of swap or move to in product listing."), _
        SoapHeader("Authentication")> _
    Public Function WOFastClose(ByVal WOID As String, ByVal LoggedInUserID As Integer, ByVal mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WOFastClose"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@mode", mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "SendMail"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This Webmethod is used get the bestbuy report
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="LoggedInUserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This Webmethod is used get the bestbuy report"), _
        SoapHeader("Authentication")> _
    Public Function GetClientSLAReport(ByVal CompanyId As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_ReportsBestBuy"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@CompanyID", CompanyId)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@Mode", Mode)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ClientSLAReport")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblClientSLAReport"
            If (Mode <> "Excel") Then
                ds.Tables(1).TableName = "tblProductSLAReport"
                ds.Tables(2).TableName = "tblBillingLocSLAReport"
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This Webmethod Gets the token and the suppliercompanyid to send the notification after ordermatch.
    ''' </summary>
    ''' <param name="compIds"></param>
    ''' <param name="WOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This Webmethod Gets the token and the suppliercompanyid to send the notification after ordermatch."), _
        SoapHeader("Authentication")> _
    Public Function IPhoneNotification(ByVal compIds As String, ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spIPhoneApp_GetTokenForNotification"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@SupplierCompanyID", compIds)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "TokenDetails"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Webmethod to Rematch WorkOrder
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will Rematch WorkOrder"), _
    SoapHeader("Authentication")> _
     Public Function RematchWO(ByVal WOID As Integer, ByVal OriginalWOID As Integer, ByVal OWCompanyID As Integer, ByVal OWRepID As Integer, ByVal Action As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_Rematch_WOSummary"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@OriginalWOID", OriginalWOID)
            cmd.Parameters.AddWithValue("@OWCompanyID", OWCompanyID)
            cmd.Parameters.AddWithValue("@OWRepID", OWRepID)
            cmd.Parameters.AddWithValue("@Action", Action)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsData")
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "tblStatus"
                If ds.Tables.Count > 1 Then
                    ds.Tables(1).TableName = "tblSuppliers"
                    ds.Tables(2).TableName = "tblWOSummary"
                End If
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Webmethod to Get the comma seperated WorkOrderIDs for BackGround process of Depot email generation
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will Rematch WorkOrder"), _
    SoapHeader("Authentication")> _
     Public Function GetWOID_BackGroundServiceDepotSheet() As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_GetDepotSheetWorkOrders"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsData")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Add or Delete Watched Workorder
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="doMode"></param>
    ''' <param name="SpecialistSuppliesParts"></param>
    ''' <param name="EstimatedTime"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will Add or Delete Watched Workorder."), _
   SoapHeader("Authentication")> _
   Public Function AddDeleteWatchedWO(ByVal WOID As String, ByVal LoggedInUserID As Integer, ByVal Mode As String)
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "SPAddRemoveWatchedWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will Rematch WorkOrder"), _
   SoapHeader("Authentication")> _
    Public Function ExecuteJohnLewisJob(ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_ExecuteJohnLewisJob"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Mode", Mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This function will return the rating of workorder
    ''' </summary>
    ''' <param name="woid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="saves new rating and comment"), _
       SoapHeader("Authentication")> _
    Public Function UpdateWorkorderAction(ByVal WOID As String, ByVal LoggedInUserID As Integer, ByVal Note As String, ByVal Comments As String, ByVal CompanyID As Integer, ByVal Rating As Integer, ByVal IsFlagged As Boolean, ByVal IsWatched As Boolean, ByVal ShowClient As Boolean, ByVal ShowSupplier As Boolean, ByVal NoteCategory As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "SpActionWatchFlag"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@Note", Note)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@ContactClassID", ApplicationSettings.ContactClassID.OW)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@Rating", Rating)
            cmd.Parameters.AddWithValue("@BizDivID", 1)
            cmd.Parameters.AddWithValue("@IsWatched", IsWatched)
            cmd.Parameters.AddWithValue("@IsFlagged", IsFlagged)
            cmd.Parameters.AddWithValue("@ShowClient", ShowClient)
            cmd.Parameters.AddWithValue("@ShowSupplier", ShowSupplier)
            'poonam modified on 8/3/2016 - Task - OA-215 : OA - Add Quick note dropdown with note types
            cmd.Parameters.AddWithValue("@NoteCategory", NoteCategory)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("UpdateWORating")
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="adds client question answers while booking job"), _
       SoapHeader("Authentication")> _
    Public Function WOSubmitQuestionAnswers(ByVal xmlQA As String, ByVal LoggedInUserID As Integer, ByVal ProductId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WOSubmitQuestionAnswers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlQA", xmlQA)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@ProductId", ProductId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will Add or Delete Watched Workorder."), _
SoapHeader("Authentication")> _
    Public Function DeleteServiceQuestions(ByVal ProductID As String, ByVal CompanyId As Integer, ByVal Field As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "SPDeleteServiceQuestions"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ProductID", ProductID)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@Field", Field)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' this webmethod  returns the status of the wo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="this webmethod  update Multiple wo status"), _
    SoapHeader("Authentication")> _
    Public Function SendAndAutoaccept(ByVal WOID As Integer, ByVal SupplierCompID As Integer, ByVal LoggedInUserID As Integer, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_BulkAction_NAME)
        Try

            Dim ProcedureName As String = "spAdmin_AutoAcceptWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@SupplierCompID", SupplierCompID)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivId)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(EnableSession:=True, Description:="this webmethod  Find WOFrom POstCodeAndRefWOID"), _
 SoapHeader("Authentication")> _
    Public Function SearchWorkOrder(WorkOrderID As String, Postcode As String) As String
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_AT800FindWOFromPostcodeAndRefWOID"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RefWOID", WorkOrderID)
            cmd.Parameters.AddWithValue("@PostCode", Postcode)
            cmd.ExecuteNonQuery()
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WO")
            da.Fill(ds)

            ds.Tables(0).TableName = "WO"

            Try
                Dim returnString As String
                If ds.Tables("WO").Rows.Count > 0 Then
                    returnString = ds.Tables("WO").Rows(0)("JobTitle").ToString + " " + ds.Tables("WO").Rows(0)("ContactFullName").ToString + " " + ds.Tables("WO").Rows(0)("ContactAddress").ToString
                Else
                    returnString = ""
                End If

                Return (returnString)
            Catch ex As Exception
                Return "NOT FOUND"
            End Try

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(EnableSession:=True, Description:="this webmethod  Inserts AignOffSheet for AT800"), _
    SoapHeader("Authentication")> _
    Public Function AT800InsertSignOffSheet(RefWOID As String, Installer_Return_Code As Integer, Filt_Type As Integer, Filt_Manuf As Integer, ProblemNot4G As Boolean, Installer_Notes As String)

        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String
            ProcedureName = "AT800InsertSignOffSheet"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RefWOID", RefWOID)
            cmd.Parameters.AddWithValue("@Installer_Return_Code", Installer_Return_Code)
            cmd.Parameters.AddWithValue("@Filt_Type", Filt_Type)
            cmd.Parameters.AddWithValue("@Filt_Manuf", Filt_Manuf)
            cmd.Parameters.AddWithValue("@ProblemNot4G", ProblemNot4G)
            cmd.Parameters.AddWithValue("@Installer_Notes", Installer_Notes)

            Dim da As New SqlDataAdapter(cmd)
            cmd.ExecuteNonQuery()
            Return ""
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in InsertPostCodeCoordinates. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while inserting the values. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    ''' <summary>
    ''' This webmethod returns the AT800 Rollout Report
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns the AT800 Rollout Report"), _
    SoapHeader("Authentication")> _
    Public Function GetAT800RolloutReport(Region As String, JobSubmitted As DateTime?, WORefNo As String, TCCRefNo As String, PostCode As String, VisitDate As DateTime?, RC As String, IsIssue As String,
                                          JobCancelled As DateTime?, CancellationReason As String, SignOffSheetReceived As DateTime?, JobClosed As DateTime?, OriginalJobType As String, FinalJobType As String,
                                          JobCompletionReason As String, Notes As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "AT800RolloutReport"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("Region", Region)
            cmd.Parameters.AddWithValue("JobSubmitted", JobSubmitted)
            cmd.Parameters.AddWithValue("WORefNo", WORefNo)
            cmd.Parameters.AddWithValue("TCCRefNo", TCCRefNo)
            cmd.Parameters.AddWithValue("PostCode", PostCode)
            cmd.Parameters.AddWithValue("VisitDate", VisitDate)
            cmd.Parameters.AddWithValue("RC", RC)
            cmd.Parameters.AddWithValue("IsIssue", IsIssue)
            cmd.Parameters.AddWithValue("JobCancelled", JobCancelled)
            cmd.Parameters.AddWithValue("CancellationReason", CancellationReason)
            cmd.Parameters.AddWithValue("SignOffSheetReceived", SignOffSheetReceived)
            cmd.Parameters.AddWithValue("JobClosed", JobClosed)
            cmd.Parameters.AddWithValue("OriginalJobType", OriginalJobType)
            cmd.Parameters.AddWithValue("FinalJobType", FinalJobType)
            cmd.Parameters.AddWithValue("JobCompletionReason", JobCompletionReason)
            cmd.Parameters.AddWithValue("Notes", Notes)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the AT800 Daily Report
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns the AT800 Daily Report"), _
    SoapHeader("Authentication")> _
    Public Function GetAT800DailyReport(CurrentDate As DateTime) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "AT800DailyReport"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("CurrentDate", CurrentDate)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the AT800 Daily Report Cancellations Statistics
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns the AT800 Daily Report Cancellations Statistics"), _
    SoapHeader("Authentication")> _
    Public Function GetAT800DailyReportCancellations(CurrentDate As DateTime) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "AT800DailyReportCancellations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("CurrentDate", CurrentDate)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the AT800 Daily Report Cancellations Statistics
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns the AT800 Daily Report Completions Statistics"), _
    SoapHeader("Authentication")> _
    Public Function GetAT800DailyReportCompletions(CurrentDate As DateTime, CompletionReasonId As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "AT800DailyReportCompletions"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("CurrentDate", CurrentDate)
            cmd.Parameters.AddWithValue("CompletionReasonId", CompletionReasonId)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the different Job Types
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns the different Job Types"), _
    SoapHeader("Authentication")> _
    Public Function GetAT800JobTypes() As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "AT800JobTypes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the different Job Completion Reasons
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns the different Job Completion Reasons"), _
    SoapHeader("Authentication")> _
    Public Function GetAT800JobCompletionReasons() As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "AT800JobCompletionReasons"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the different AT800 Job Types
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod gets the latest at800 rollout that users are authorised to see"), _
    SoapHeader("Authentication")> _
    Public Function GetMaxAT800Rollout() As DateTime
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim LatestDate As DateTime
            Dim ProcedureName As String = "GetMaxAT800Rollout"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            LatestDate = ds.Tables(0).Rows(0)(0)
            Return LatestDate
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the different AT800 Job Types
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod authorisers users to view a days at800 rollout data"), _
    SoapHeader("Authentication")> _
    Public Function AuthoriseAT800Rollout(CurrentDate As DateTime) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "AuthoriseAT800Rollout"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("CurrentDate", CurrentDate)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the different AT800 Job Types
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns all the cancellation reasons"), _
    SoapHeader("Authentication")> _
    Public Function GetCancellationReasons() As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "GetStandardsGeneral"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("Type", "Cancellation Reasons")
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the different AT800 Job Types
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns all the values for filters on AT800 Rollout Report"), _
    SoapHeader("Authentication")> _
    Public Function GetAT800RolloutValues() As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "GetAT800RolloutValues"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the different Post codes based on what has already been entered
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns the different Post Codes based on what has already been entered"), _
    SoapHeader("Authentication")> _
    Public Function GetAT800PostCodes(prefixText As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "AT800PostCodes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("PostCode", prefixText)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the different WO Ref No's based on what has already been entered
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns the different WO Ref No's based on what has already been entered"), _
    SoapHeader("Authentication")> _
    Public Function GetAT800WORefNo(prefixText As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "AT800WORefNo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("WORefNO", prefixText)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the different WO Ref No's based on what has already been entered
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns the different TCC Ref No's based on what has already been entered"), _
    SoapHeader("Authentication")> _
    Public Function GetAT800TCCRefNo(prefixText As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "AT800TCCRefNo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("TCCRefNO", prefixText)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod updates AT800 Rollout Data
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod updates AT800 Rollout Data"), _
    SoapHeader("Authentication")> _
    Public Function AT800RolloutUpdate(WOID As Integer, TCCRefNo As String, VisitDate As String, CancellationReason As String, OriginalJobType As String, FinalJobType As String, JobCompletionReason As String, Notes As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "AT800RolloutUpdate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("WOID", WOID)
            'cmd.Parameters.AddWithValue("WORefNo", WORefNo)
            cmd.Parameters.AddWithValue("TCCRefNo", TCCRefNo)
            If Not String.IsNullOrEmpty(VisitDate) Then
                cmd.Parameters.AddWithValue("VisitDate", DateTime.Parse(VisitDate))
            Else
                cmd.Parameters.AddWithValue("VisitDate", Nothing)
            End If
            'If Not String.IsNullOrEmpty(JobCancelled) Then
            '    cmd.Parameters.AddWithValue("JobCancelled", DateTime.Parse(JobCancelled))
            'Else
            '    cmd.Parameters.AddWithValue("JobCancelled", Nothing)
            'End If
            cmd.Parameters.AddWithValue("CancellationReason", CancellationReason)
            cmd.Parameters.AddWithValue("OriginalJobType", OriginalJobType)
            cmd.Parameters.AddWithValue("FinalJobType", FinalJobType)
            cmd.Parameters.AddWithValue("JobCompletionReason", JobCompletionReason)
            cmd.Parameters.AddWithValue("Notes", Notes)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns the AT800 Job Type Statistics
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod returns the AT800 Job Type Statistics"), _
    SoapHeader("Authentication")> _
    Public Function GetJobTypeStatistics(CurrentDate As DateTime) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "GetJobTypeStatistics"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("CurrentDate", CurrentDate)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This webmethod updates the Tag Contact Linkage table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod updates the Tag Contact Linkage table"), _
    SoapHeader("Authentication")> _
    Public Function UpdateTagContactLinkage(TagContactLinkageID As Integer, ApprovalStatus As Integer, DateModified As DateTime, UserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "UpdateTagContactLinkage"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("TagContactLinkageID", TagContactLinkageID)
            cmd.Parameters.AddWithValue("ApprovalStatus", ApprovalStatus)
            cmd.Parameters.AddWithValue("DateModified", DateModified)
            cmd.Parameters.AddWithValue("UserID", UserID)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This webmethod updates the Tag Linkage table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod updates the Tag Linkage table"), _
    SoapHeader("Authentication")> _
    Public Function UpdateTagLinkage(TagLinkageID As Integer, ApprovalStatus As Integer, DateModified As DateTime, UserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "UpdateTagLinkage"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("@TagLinkageID", TagLinkageID)
            cmd.Parameters.AddWithValue("@ApprovalStatus", ApprovalStatus)
            cmd.Parameters.AddWithValue("@DateModified", DateModified)
            cmd.Parameters.AddWithValue("@UserID", UserID)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This webmethod deletes an entry in the Tag Linkage table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod deletes an entry in the Tag Linkage table"), _
    SoapHeader("Authentication")> _
    Public Function DeleteTagLinkage(TagLinkageID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "DeleteTagLinkage"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("TagLinkageID", TagLinkageID)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method is used to Create New WO when the WO is in CA stage"), _
  SoapHeader("Authentication")> _
    Public Function WO_CreateWorkOrderForCAWO(ByVal WOID As Integer, ByVal BizDivId As Integer, ByVal CompanyID As Integer, ByVal ContactID As Integer, ByVal TrackCompanyID As Integer, ByVal TrackContactID As Integer, ByVal TrackContactClassID As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_CreateWorkOrderForCAWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@TrackCompanyID", TrackCompanyID)
            cmd.Parameters.AddWithValue("@TrackContactID", TrackContactID)
            cmd.Parameters.AddWithValue("@TrackContactClassID", TrackContactClassID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim ds As New DataSet("WODiscardDelCopy")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            If ds.Tables.Count <> 0 Then
                ds.Tables(0).TableName = "tblWOID"
                ds.Tables(1).TableName = "tblSendMail"
                ds.Tables(2).TableName = "tblDiscardMail"
            End If

            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod inserts record into SONYURL table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod Inserts the URLs in the SONY table, if the Web service call fails we can later run this table"), _
    SoapHeader("Authentication")> _
    Public Function InsertURLforSONY(ByVal RefWOID As String, ByVal URL As String, ByVal Status As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMS_InsertSONYURL"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("@RefWOID", RefWOID)
            cmd.Parameters.AddWithValue("@URL", URL)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will return Sony Data for comparisons"), _
    SoapHeader("Authentication")> _
    Public Function GetSonyData(ByVal CompanyName As String, ByVal ContractNrList As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "SonyGetSystemData"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyName", CompanyName)
            cmd.Parameters.AddWithValue("@ContractNrList", ContractNrList)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsHistory")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCompanyID"
            ds.Tables(1).TableName = "tblProductDetails"
            ds.Tables(2).TableName = "tblSupplierAddressList"
            ds.Tables(3).TableName = "tblSonyJobList"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Called by AcceptedSendMail to populate Details on Send Accepted Mail
    ''' Created By Hemisha Desai on 25 April 2014
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order Details + Comments and Action details for Accepted."), _
    SoapHeader("Authentication")> _
    Public Function GetAcceptedDetails(ByVal WOID As Integer, ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_GetAcceptedDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSummary"
            ds.Tables(1).TableName = "tblAcceptedDetails"
            ds.Tables(2).TableName = "tblComments"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Called by AcceptedSendMail to Save Details and get Info for Send Accpedted Mail
    ''' Created By Hemisha Desai on 24 April 2014
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="Comments"></param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the Work Order Details + Comments and Action details for Accepted."), _
    SoapHeader("Authentication")> _
    Public Function SaveAcceptedCommentsSendMail(ByVal CompanyID As String, ByVal WOID As Integer, ByVal Comments As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_SaveAcceptedSendMailDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            'ds.Tables(0).TableName = "tblSummary"
            'ds.Tables(1).TableName = "tblCADetails"
            'ds.Tables(2).TableName = "tblComments"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Called by WOBulkCancellation to cancel Workorders in bulk
    ''' Created By Hemisha Desai on 02 July 2014
    ''' </summary>
    ''' <param name="WOID"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="Comments"></param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will cancel Work Orders In bulk"), _
    SoapHeader("Authentication")> _
    Public Function WOBulkCancellation(ByVal RefWOIDs As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_CancelWOinBulk"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RefWOIDs", RefWOIDs)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WOsDetails")
            da.Fill(ds)
            'ds.Tables(0).TableName = "tblSummary"
            'ds.Tables(1).TableName = "tblCADetails"
            'ds.Tables(2).TableName = "tblComments"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will get all the services selected while booking WO for Dixons"), _
    SoapHeader("Authentication")> _
    Public Function GetDixonsServicesSelection(ByVal WOID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMs_WO_GetDixonsServicesSelection"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ServicesSelection")
            da.Fill(ds)
            ds.Tables(0).TableName = "ServicesSelection"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    ''' <summary>
    ''' This webmethod bulk update PONumbers 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True, Description:="This webmethod bulk update PONumbers"), _
    SoapHeader("Authentication")> _
    Public Function UpdateBulkPONumbers(ByVal xmlDoc As String, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMS_UpdateBulkPONumbers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.Parameters.AddWithValue("@xmlDoc", xmlDoc)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.CommandType = CommandType.StoredProcedure
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will add or edit TimeBuilder QA"), _
SoapHeader("Authentication")> _
    Public Function AddEditServiceQA(ByVal xmlContent As String, ByVal NoOfQuestions As Integer, ByVal CompanyID As Integer, ByVal ProductID As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_BulkAction_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_AddEditServiceQA"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlContent", xmlContent)
            cmd.Parameters.AddWithValue("@NoOfQuestions", NoOfQuestions)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ProductID", ProductID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="Get Listing of all Holidays."), _
   SoapHeader("Authentication")> _
    Public Function GetCustomerHistory(ByVal SortExpression As String, ByVal StartRowIndex As String, ByVal MaxRows As String, ByVal WOID As Integer) As DataSet
        Dim IsDepot As Boolean = False
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_GetCustomerHistory"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", MaxRows)
            cmd.Parameters.AddWithValue("@WOID", WOID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "CustomerHistoryListing"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will be used to copy services"), _
       SoapHeader("Authentication")> _
    Public Function CopyService(ByVal WOID As String, ByVal NoOfServices As Integer, ByVal UserId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMSWO_CreateMultipleServices"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@NoOfServices", NoOfServices)
            cmd.Parameters.AddWithValue("@LoggedInUserId", UserId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds 
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning standard data for create work order form. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will be used to copy services"), _
    SoapHeader("Authentication")> _
    Public Function InsertServiceVouchers(ByVal xmlContent As String, ByVal companyID As Integer) As DataSet
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "sp_InsertServiceVoucher"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", companyID)
            cmd.Parameters.AddWithValue("@xmlDoc", xmlContent)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will be used to copy services"), _
    SoapHeader("Authentication")> _
    Public Function IsDuplicateVoucher(ByVal CompanyID As Integer, ByVal VoucherCode As String) As DataSet
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "sp_IsDuplicateVoucherCode"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@VoucherCode", VoucherCode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will be used to get the account source and the payment terms"), _
  SoapHeader("Authentication")> _
    Public Function GetAccountSourceAndPaymentTerms(ByVal WOID As String, ByVal CompanyID As String) As DataSet
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spGetAccountSourceAndPaymentTerms"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOID", WOID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Source"
            ds.Tables(1).TableName = "PaymentTerm"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


End Class
