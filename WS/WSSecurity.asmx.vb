Imports System.Web
Imports System.Web.Services
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections
Imports System.Web.Services.Protocols
Imports System.Security.Principal
Imports System.Collections.Specialized
Imports System.Security.Cryptography
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Text
Imports Authentication
Imports Util
Imports System.Diagnostics

<System.Web.Services.WebService(Namespace:="http://tempuri.org/WS/WSSecurity")> _
Public Class WSSecurity
    Inherits System.Web.Services.WebService

    Public Authentication As AuthenticationHeader
    Private Shared LT_CONNECTION_NAME As String = "webdbUK"
    Private Shared _siteCountry As String
    Private Shared _bizDivId As Integer
    Private Shared _siteType As String



#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

    Public Shared Property country() As String
        Get
            Return _siteCountry
        End Get
        Set(ByVal value As String)
            _siteCountry = value
        End Set
    End Property
    Public Shared Property bizDivId() As Integer
        Get
            Return _bizDivId
        End Get
        Set(ByVal value As Integer)
            _bizDivId = value
        End Set
    End Property

    Public Property siteType() As String
        Get
            Return _siteType
        End Get
        Set(ByVal value As String)
            _siteType = value
        End Set
    End Property



    ''' <summary>
    ''' This webmethod intializes web service depending on the country and bizdivid
    ''' to select db depending on country 
    ''' </summary>
    ''' <param name="paramCountry"></param>
    ''' <param name="paramBizDivId"></param>
    ''' <returns>Nothing</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Initialize properties"), _
    SoapHeader("Authentication")> _
   Public Function IntializeProperties(ByVal paramCountry As String, ByVal paramBizDivId As Integer, ByVal paramSiteType As String)
        bizDivId = paramBizDivId
        country = paramCountry
        siteType = paramSiteType
        CommonFunctions.InitializeProperties(paramCountry, LT_CONNECTION_NAME)
        Return Nothing
    End Function

    ''' <summary>
    ''' Web Method to authenticate user on login and returns its details and menu
    ''' </summary>
    ''' <param name="UserName">Login Email ID</param>
    ''' <param name="Password">Password</param>
    ''' <param name="paramBizDivId">BizDivID of site</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will check a user login and return details & menu."), _
SoapHeader("Authentication")> _
Public Function AuthenticateUser(ByVal UserName As String, ByVal Password As String, ByVal paramBizDivId As Integer, ByVal BusinessID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)

        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            'Defining the stored procedure to use "spSecurity_AuthenticateUser"
            Dim ProcedureName As String = "spSecurity_AuthenticateUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            'setting the command type and also parameter of the Stored procedure
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", UserName)
            cmd.Parameters.AddWithValue("@Password", Password)
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            cmd.Parameters.AddWithValue("@BusinessID", BusinessID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            'Fill dataset as return by the stored procedure "spSecurity_AuthenticateUser" 
            da.Fill(ds)
            ' Name the tables of the dataset
            If ds.Tables(0).Rows.Count <> 0 Then
                ds.Tables(0).TableName = "LoginInfo"
                ds.Tables(1).TableName = "ParentMenu"
                ds.Tables(2).TableName = "ChildMenu"
            End If
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try

        End Try


    End Function
    ''' <summary>
    ''' Web Method to authenticate user on login and returns its details and menu
    ''' </summary>
    ''' <param name="UserName">Login Email ID</param>
    ''' <param name="Password">Password</param>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will check a user login and return details & menu."), _
SoapHeader("Authentication")> _
Public Function AuthenticateAdminUser(ByVal UserName As String, ByVal Password As String, ByVal BusinessID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            'Defining the stored procedure to use "spSecurity_AuthenticateUser"
            Dim ProcedureName As String = "spSecurity_AuthenticateUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            'setting the command type and also parameter of the Stored procedure
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", UserName)
            cmd.Parameters.AddWithValue("@Password", Password)
            cmd.Parameters.AddWithValue("@BizDivId", 0)
            cmd.Parameters.AddWithValue("@BusinessID", BusinessID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            'Fill dataset as return by the stored procedure "spSecurity_AuthenticateUser" 
            da.Fill(ds)
            ' Name the tables of the dataset
            If ds.Tables(0).Rows.Count <> 0 Then
                ds.Tables(0).TableName = "LoginInfo"
            End If
            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try

        End Try


    End Function
    ''' <summary>
    ''' Web Method to fetch security question and answer for an particular contact
    ''' </summary>
    ''' <param name="Email"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will fetch the security question and answer for an particular contact."), _
SoapHeader("Authentication")> _
     Public Function GetSecurityQuesAns(ByVal Email As String) As DataSet
        'Opening a connection string to connect to the DB and dispose as not required.
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetSecurityQuesAns"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", Email)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try


    End Function

    ''' <summary>
    ''' Web Method to update login date for a logged in user
    ''' </summary>
    ''' <param name="ContactID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will update login date for a logged in user."), _
SoapHeader("Authentication")> _
Public Function UpdateLoginDate(ByVal ContactID As Integer) As Boolean
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spSecurity_UpdateLoginDate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method will return the different levels of menu that user has access to.
    ''' </summary>
    ''' <param name="RoleCategory"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the different levels of menu that user has access to."), _
     SoapHeader("Authentication")> _
      Public Function GetRolesGroups(ByVal RoleCategory As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spSecurity_GetRolesGroups"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RoleCategory", RoleCategory)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to return status that is accessible to the Role Group that Contact belongs to
    ''' </summary>
    ''' <param name="RoleGroupID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the statuses that is accessible to the Role Group that Contact belongs to."), _
SoapHeader("Authentication")> _
 Public Function GetStatuses(ByVal RoleGroupID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spSecurity_GetStatuses"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RoleGroupID", RoleGroupID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns accounts activation status with contactid and maincontactid
    ''' </summary>
    ''' <param name="ContactId"></param>
    ''' <param name="MainContactId"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="oldStatus"></param>
    ''' <param name="newStatus"></param>
    ''' <returns>Dataset with table value as ContactID, MainContactId, bizDivId</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will update the user status to to new status"), _
 SoapHeader("Authentication")> _
 Public Function UpdateStatus(ByVal ContactId As Integer, ByVal MainContactId As Integer, ByVal BizDivId As Integer, ByVal oldStatus As Integer, ByVal newStatus As Integer) As Boolean
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spSecurity_UpdateUserStatus"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@MainContactId", MainContactId)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@oldStatus", oldStatus)
            cmd.Parameters.AddWithValue("@newStatus", newStatus)

            'Return 1 if command executed sucessfully
            cmd.Parameters.Add("@Success", SqlDbType.Bit).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value


        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' WebMethods to return MainContactID email, FirstName 
    ''' </summary>
    ''' <param name="CompanyId"></param>
    ''' <returns name="UserName"></returns>
    ''' <returns name="FName"></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the MainContactID email address with FirstName ."), _
    SoapHeader("Authentication")> _
    Public Function GetMainContactEmail(ByVal CompanyId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spSecurity_GetMainContactEmail"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            If ds.Tables(0).Rows.Count <> 0 Then
                ds.Tables(0).TableName = "LoginInfo"
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function



    ''' <summary>
    ''' Web Method to Change User Login Info. Password and Security Info
    ''' </summary>
    ''' <param name="ContactID"></param>
    ''' <param name="UserName"></param>
    ''' <param name="OldPassword"></param>
    ''' <param name="NewPassword"></param>
    ''' <param name="SecurityQuest"></param>
    ''' <param name="SecurityAns"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Change User Login Info. Password & Security Info."), _
        SoapHeader("Authentication")> _
    Public Function ChangeLoginInfo(ByVal ContactID As Integer, ByVal UserName As String, ByVal OldPassword As String, ByVal NewPassword As String, ByVal SecurityQuest As Integer, ByVal SecurityAns As String, ByVal LoggedInUserID As Integer) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim success As Integer
            Dim ProcedureName As String

            ProcedureName = "spSecurity_ChangeLoginInfo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@contactID", ContactID)
            cmd.Parameters.AddWithValue("@UserName", UserName)
            cmd.Parameters.AddWithValue("@OldPassword", OldPassword)
            cmd.Parameters.AddWithValue("@NewPassword", NewPassword)
            cmd.Parameters.AddWithValue("@SecurityQues", SecurityQuest)
            cmd.Parameters.AddWithValue("@SecurityAns", SecurityAns)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Decimal).Direction = ParameterDirection.Output
            'poonam modified on 14/6/2016 - Task - OA-276 : OA - Change SP to record Name of password changer/requester
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            cmd.ExecuteNonQuery()

            'Change made by PB: to get bizdivid as the return parameter. For mailers.
            'If CInt(cmd.Parameters.Item("@Success").Value) = 1 Then
            '    success = True
            'Else
            '    success = False
            'End If
            success = cmd.Parameters.Item("@Success").Value
            Return success
        Catch TransEx As Exception
            Throw TransEx 'ExceptionUtil.CreateSoapException("Error in Update for tables: " & tablesForUpdate & ControlChars.CrLf & TransEx.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Error updating contacts information.")
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="Resend Email Info."), _
        SoapHeader("Authentication")> _
    Public Function ResendEmailInfo(ByVal UserName As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String
            Dim cmd As SqlCommand
            ProcedureName = "spSecurity_ResendEmailInfo"
            cmd = New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", UserName)
            'poonam modified on 14/6/2016 - Task - OA-276 : OA - Change SP to record Name of password changer/requester
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            cmd.ExecuteNonQuery()

            Return ds
        Catch TransEx As Exception
            Throw TransEx 'ExceptionUtil.CreateSoapException("Error in Update for tables: " & tablesForUpdate & ControlChars.CrLf & TransEx.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Error updating contacts information.")
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="Forgot Password. Returns password if email, security ques and answer is correct. else password is returned as blank"), _
SoapHeader("Authentication")> _
Public Function ForgotPassword(ByVal UserName As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Dim tablesForUpdate As String = ""
        Try

            Dim ProcedureName As String
            Dim cmd As SqlCommand

            'Login Table
            ProcedureName = "spSecurity_ForgotPassword"
            cmd = New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", UserName)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            cmd.ExecuteNonQuery()

            '  theTrans.Commit()
            Return ds 'password
        Catch TransEx As Exception
            Trace.WriteLine("Exception Updating " & tablesForUpdate & " Tables. Trying Rollback.")
            Trace.WriteLine(TransEx.Message)
            Trace.WriteLine(TransEx.StackTrace)
            Try
                '  theTrans.Rollback()
                '  Trace.WriteLine("Rollback Successfull")
            Catch Rex As Exception
                Trace.WriteLine("RollBack Failed...")
                Trace.WriteLine(TransEx.Message)
                Trace.WriteLine(TransEx.StackTrace)
                Throw Rex 'ExceptionUtil.CreateSoapException("Error occurred during Rollback " & TransEx.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Error while updating Branch information")
            End Try
            Throw TransEx 'ExceptionUtil.CreateSoapException("Error in Update for tables: " & tablesForUpdate & ControlChars.CrLf & TransEx.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Error updating contacts information.")
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function



    <WebMethod(Description:="This method will update the user status for added user"), _
    SoapHeader("Authentication")> _
    Public Function UpdateAddedUserStatus(ByVal Email As String, ByVal question As Integer, ByVal answer As String, ByVal password As String, ByVal BizDivId As Integer)
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spSecurity_UpdateAddedUserStatus"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Email", Email)
            cmd.Parameters.AddWithValue("@Question", question)
            cmd.Parameters.AddWithValue("@Answer", answer)
            cmd.Parameters.AddWithValue("@Password", password)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value


        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


End Class
