Imports System.Web
Imports System.Web.Services
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections
Imports System.Web.Services.Protocols
Imports System.Security.Principal
Imports System.Collections.Specialized
Imports System.Security.Cryptography
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Text
Imports Authentication
Imports Util
Imports Admin
Imports System.Diagnostics

<System.Web.Services.WebService(Namespace:="http://tempuri.org/WS/WSContacts")> _
Public Class WSContacts
    Inherits System.Web.Services.WebService


    Public Authentication As AuthenticationHeader
    Private Shared LT_CONNECTION_NAME As String = "webdbUK"
    Private Shared LT_CONNECTION_BulkAction_NAME As String = "bulkdb"
    Private Shared _siteCountry As String
    Private Shared _bizDivId As Integer
    Private Shared _siteType As String


#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region


    Public Shared Property country() As String
        Get
            Return _siteCountry
        End Get
        Set(ByVal value As String)
            _siteCountry = value
        End Set
    End Property

    Public Shared Property bizDivId() As Integer
        Get
            Return _bizDivId
        End Get
        Set(ByVal value As Integer)
            _bizDivId = value
        End Set
    End Property

    Public Property siteType() As String
        Get
            Return _siteType
        End Get
        Set(ByVal value As String)
            _siteType = value
        End Set
    End Property



    ''' <summary>
    ''' This webmethod intializes web service depending on the country and bizdivid
    ''' to select db depending on country 
    ''' </summary>
    ''' <param name="paramCountry"></param>
    ''' <param name="paramBizDivId"></param>
    ''' <returns>Nothing</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Initialize properties"), _
    SoapHeader("Authentication")> _
    Public Function IntializeProperties(ByVal paramCountry As String, ByVal paramBizDivId As Integer, ByVal paramSiteType As String)
        bizDivId = paramBizDivId
        country = paramCountry
        siteType = paramSiteType
        CommonFunctions.InitializeProperties(paramCountry, LT_CONNECTION_NAME)
        Return Nothing
    End Function


    <WebMethod(Description:="This method will get Contact in the form Company Name - Contact Name. this is used to populate the dropdown."), _
      SoapHeader("Authentication")> _
    Public Function MS_GetContactsByName(ByVal bizDivId As String, ByVal ClassID As Integer, ByVal Filter As String, ByVal HideTestAccounts As Boolean, ByVal defaultview As Boolean, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMSAdmin_GetContactsByName"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ClassID", ClassID)
            cmd.Parameters.AddWithValue("@BizDivId", CInt(bizDivId))
            cmd.Parameters.AddWithValue("@Filter", Filter)
            cmd.Parameters.AddWithValue("@HideTestAccounts", HideTestAccounts)
            cmd.Parameters.AddWithValue("@DefaultView", defaultview)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ContactsNames")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContactsNames"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to return account summary of a contact
    ''' </summary>
    ''' <param name="CompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the account summary for a Contact"), _
SoapHeader("Authentication")> _
    Public Function MS_GetAccountSummaryForCompany(ByVal CompanyID As Integer, ByVal BizDivId As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSContacts_GetAccountSummaryForCompany"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            'Dim BizDivId As Integer = WSContacts.bizDivId
            'If BizDivId <> 0 Then
            '    cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            'End If
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AccountSummary")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact' account  information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to return attachment details as dataset for contact or workorder
    ''' </summary>
    ''' <param name="contactID"></param>
    ''' <param name="linkSource"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Webmethod to fetch attachment details for a contact, company or workorder"), _
SoapHeader("Authentication")> _
    Public Function GetAttachmentsDetails(ByVal contactID As Integer, ByVal linkSource As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetAttachmentsDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", contactID)
            cmd.Parameters.AddWithValue("@LinkSource", linkSource)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Attachments")
            da.Fill(ds)
            ds.Tables(0).TableName = "Attachment"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetAttachmentsDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    <WebMethod(Description:="This method will get Contact Listing Details for a particular contact type (Client' /  'ApprovedSupplier' / 'NonApprovedSupplier' / 'Supplier')with Paging/Sorting. for Export to excel function "), _
               SoapHeader("Authentication")> _
    Public Function GetContactExporttoExcel(ByVal bizDivId As String, ByVal MainClassID As String, ByVal FromDate As String, ByVal ToDate As String, ByVal statusId As String, ByVal DateCriteriaField As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spAdmin_ContactsExportToExcel"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@MainClassID", MainClassID)
            cmd.Parameters.AddWithValue("@statusId", statusId)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            End If
            If DateCriteriaField <> "" Then
                cmd.Parameters.AddWithValue("@DateCriteriaField", DateCriteriaField)
            End If

            cmd.Parameters.AddWithValue("@BizDivId", CInt(bizDivId))

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ExportToExcel")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContacts"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Web Method to Fetch Specialists listing
    ''' </summary>
    ''' <param name="ContactID">Contact ID </param>
    ''' <param name="sortExpression">Column Name to Sort</param>
    ''' <param name="startRowIndex">Start Row index</param>
    ''' <param name="maximumRows">Maximum row per page</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Webmethod to fetch Specialist listing for a contact"), _
   SoapHeader("Authentication")> _
    Public Function GetSpecialistsListing(ByVal ContactID As Integer, ByVal BizDivID As Integer, ByVal UserSiteType As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                                ByVal maximumRows As Integer, ByRef recordCount As System.Nullable(Of Integer), ByVal FilterString As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetSpecialists"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)

            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@siteType", UserSiteType)

            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@FilterString", FilterString)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.Add(New SqlParameter("@numresults", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, Nothing, DataRowVersion.Default, 0))
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SpecialistsListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "Specialists"
            recordCount = ds.Tables(1).Rows(0).Item("TotalRecs")
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function
    ''' <summary>
    ''' Web Method to Update Company Status 
    ''' </summary>
    ''' <param name="CurrContactID">Current Admin ContactID </param>
    ''' <param name="NewAdminID">ContactID of New Admin</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Webmethod to fetch Specialist listing for a contact"), _
   SoapHeader("Authentication")> _
    Public Function UpdateAdminStatus(ByVal CompanyID As Integer, ByVal CurrAdminContactID As Integer, ByVal NewAdminID As Integer, ByVal NewAdminStatus As Integer, ByVal OldAdminNewStatus As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            'Defining the stored procedure to use "spAdmin_Contacts_UpdateSpecialist"
            Dim ProcedureName As String = "spAdmin_Contacts_UpdateSpecialist"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            'setting the command type and also parameter of the Stored procedure
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CurrAdminContactID", CurrAdminContactID)
            cmd.Parameters.AddWithValue("@NewAdminID", NewAdminID)
            cmd.Parameters.AddWithValue("@NewAdminStatus", NewAdminStatus)
            cmd.Parameters.AddWithValue("@OldAdminNewStatus", OldAdminNewStatus)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            'Fill dataset as return by the stored procedure "spSecurity_AuthenticateUser" 
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function



    <WebMethod(Description:="This method will return the buyer/Suppliers which match the Search Criteria Passed. "), _
   SoapHeader("Authentication")> _
    Public Function GetSearchAccounts(ByVal paramBizDivId As Integer, ByVal SkillSet As String, ByVal Region As String, ByVal PostCode As String, ByVal Keyword As String, ByVal StatusId As String, ByVal fName As String, ByVal lName As String, ByVal companyName As String, ByVal email As String, ByVal vendorIds As String, ByVal CRBCheck As String, ByVal CSCS As String, ByVal UKSecurity As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim i As Integer
            If SkillSet <> "" Then
                'Remove Product - Other option

                SkillSet = SkillSet.Replace("782", "")
                SkillSet = SkillSet.Replace(",,", ",")
                If SkillSet.StartsWith(",") = True Then
                    SkillSet = SkillSet.Substring(1, SkillSet.Length - 1)
                End If
                If SkillSet.EndsWith(",") = True Then
                    SkillSet = SkillSet.Substring(0, SkillSet.Length - 1)
                End If

                If SkillSet <> "" Then
                    Dim strSkills() As String = Split(SkillSet, ",")
                    SkillSet = ""
                    For i = 0 To strSkills.GetLength(0) - 1
                        If SkillSet <> "" Then
                            SkillSet &= ","
                        End If
                        SkillSet &= "'" & CStr(strSkills.GetValue(i)) & "'"
                    Next
                End If
            End If

            If Region <> "" Then
                Dim strRegionCode() As String = Split(Region, ",")
                Region = ""
                For i = 0 To strRegionCode.GetLength(0) - 1
                    If Region <> "" Then
                        Region &= ","
                    End If
                    Region &= "'" & CStr(strRegionCode.GetValue(i)) & "'"
                Next
            End If


            If vendorIds <> "" Then
                Dim strVendorIds() As String = Split(vendorIds, ",")
                vendorIds = ""
                For i = 0 To strVendorIds.GetLength(0) - 1
                    If vendorIds <> "" Then
                        vendorIds &= ","
                    End If
                    vendorIds &= "'" & CStr(strVendorIds.GetValue(i)) & "'"
                Next
            End If

            If Keyword.IndexOf("'") <> -1 Then
                Keyword = Keyword.Replace("'", "")
            End If
            If companyName.IndexOf("'") <> -1 Then
                companyName = companyName.Replace("'", "''")
            End If
            Dim ProcedureName As String = "spContacts_GetSearchAccounts"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            cmd.Parameters.AddWithValue("@keyword", Keyword)
            cmd.Parameters.AddWithValue("@PostCode", PostCode)
            cmd.Parameters.AddWithValue("@SkillSet", SkillSet)
            cmd.Parameters.AddWithValue("@RegionCode", Region)
            cmd.Parameters.AddWithValue("@StatusId", StatusId)
            cmd.Parameters.AddWithValue("@firstName", fName)
            cmd.Parameters.AddWithValue("@lastName", lName)
            cmd.Parameters.AddWithValue("@companyName", companyName)
            cmd.Parameters.AddWithValue("@Email", email)
            cmd.Parameters.AddWithValue("@CRBCheck", CRBCheck)
            cmd.Parameters.AddWithValue("@CSCS", CSCS)
            cmd.Parameters.AddWithValue("@UKSecurity", UKSecurity)
            cmd.Parameters.AddWithValue("@vendorIds", vendorIds)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)

            cmd.CommandTimeout = 1800
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchAccounts"
            ds.Tables(1).TableName = "tblCount"
            rowCount = ds.Tables("tblCount").Rows(0)(0)
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spContacts_GetSearchAccounts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method will return the buyer/Suppliers which match the Search Criteria Passed and also post code co-ordinates. "), _
   SoapHeader("Authentication")> _
    Public Function GetMSSearchAccounts(ByVal paramBizDivId As Integer, ByVal SkillSet As String, ByVal Region As String, ByVal PostCode As String, ByVal Keyword As String, ByVal StatusId As String, ByVal fName As String, ByVal lName As String, ByVal companyName As String, ByVal email As String, ByVal vendorIds As String, ByVal CRBCheck As String, ByVal CSCS As String, ByVal UKSecurity As String, ByVal distance As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer, ByVal TrackSP As Boolean, ByVal EngineerCRBCheck As Integer, ByVal EngineerCSCSCheck As Boolean, ByVal EngineerUKSecurityCheck As Boolean, ByVal EngineerRightToWorkInUK As Boolean, ByVal EngineerProofOfMeeting As Boolean, ByVal ContactId As Integer, ByVal strExcelPartner As String, ByVal strEPCP As String, ByVal strCategoryType As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim i As Integer
            If SkillSet <> "" Then
                'Remove Product - Other option

                SkillSet = SkillSet.Replace("782", "")
                SkillSet = SkillSet.Replace(",,", ",")
                If SkillSet.StartsWith(",") = True Then
                    SkillSet = SkillSet.Substring(1, SkillSet.Length - 1)
                End If
                If SkillSet.EndsWith(",") = True Then
                    SkillSet = SkillSet.Substring(0, SkillSet.Length - 1)
                End If

                If SkillSet <> "" Then
                    Dim strSkills() As String = Split(SkillSet, ",")
                    SkillSet = ""
                    For i = 0 To strSkills.GetLength(0) - 1
                        If SkillSet <> "" Then
                            SkillSet &= ","
                        End If
                        SkillSet &= "'" & CStr(strSkills.GetValue(i)) & "'"
                    Next
                End If
            End If

            If Region <> "" Then
                Dim strRegionCode() As String = Split(Region, ",")
                Region = ""
                For i = 0 To strRegionCode.GetLength(0) - 1
                    If Region <> "" Then
                        Region &= ","
                    End If
                    Region &= "'" & CStr(strRegionCode.GetValue(i)) & "'"
                Next
            End If

            If vendorIds <> "" Then
                Dim strvendorIds() As String = Split(vendorIds, ",")
                vendorIds = ""
                For i = 0 To strvendorIds.GetLength(0) - 1
                    If vendorIds <> "" Then
                        vendorIds &= ","
                    End If
                    vendorIds &= "'" & CStr(strvendorIds.GetValue(i)) & "'"
                Next
            End If

            'If vendorIds <> "" Then
            '    Dim strVendorIds() As String = Split(vendorIds, ",")
            '    Dim indVendorIds() As String
            '    vendorIds = ""
            '    For i = 0 To strVendorIds.GetLength(0) - 1
            '        indVendorIds = Split(strVendorIds.GetValue(i), "#")
            '        If indVendorIds.GetValue(0) <> "" Then
            '            If vendorIds <> "" Then
            '                vendorIds = vendorIds & " OR "
            '            End If
            '            vendorIds &= "( VendorID = " & CStr(indVendorIds.GetValue(0))
            '            If indVendorIds.GetLength(0) > 1 Then
            '                If indVendorIds.GetValue(1) = "" Then
            '                    vendorIds = vendorIds & ")"
            '                Else
            '                    vendorIds = vendorIds & " AND  PartnerID = " & CStr(indVendorIds.GetValue(1)) & ")"
            '                End If
            '            Else
            '                vendorIds = vendorIds & ")"
            '            End If
            '        End If

            '    Next
            'End If

            If Keyword.IndexOf("'") <> -1 Then
                Keyword = Keyword.Replace("'", "''")
            End If

            'Added by Snehal to handle special single quote
            If companyName.IndexOf("'") <> -1 Then
                companyName = companyName.Replace("'", "''")
            End If
            If fName.IndexOf("'") <> -1 Then
                fName = fName.Replace("'", "''")
            End If
            If lName.IndexOf("'") <> -1 Then
                lName = lName.Replace("'", "''")
            End If

            If email.IndexOf("'") <> -1 Then
                email = email.Replace("'", "''")
            End If


            Dim ProcedureName As String = "spMSContacts_GetSearchAccounts"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            cmd.Parameters.AddWithValue("@keyword", Keyword)
            cmd.Parameters.AddWithValue("@PostCode", PostCode)
            cmd.Parameters.AddWithValue("@SkillSet", SkillSet)
            cmd.Parameters.AddWithValue("@RegionCode", Region)
            cmd.Parameters.AddWithValue("@StatusId", StatusId)
            cmd.Parameters.AddWithValue("@firstName", fName)
            cmd.Parameters.AddWithValue("@lastName", lName)
            cmd.Parameters.AddWithValue("@companyName", companyName)
            cmd.Parameters.AddWithValue("@Email", email)
            cmd.Parameters.AddWithValue("@CRBCheck", CRBCheck)
            cmd.Parameters.AddWithValue("@CSCS", CSCS)
            cmd.Parameters.AddWithValue("@UKSecurity", UKSecurity)
            cmd.Parameters.AddWithValue("@vendorIds", vendorIds)
            cmd.Parameters.AddWithValue("@Distance", distance)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@TrackSP", TrackSP)
            cmd.Parameters.AddWithValue("@EngineerCRBCheck", EngineerCRBCheck)
            cmd.Parameters.AddWithValue("@EngineerCSCSCheck", EngineerCSCSCheck)
            cmd.Parameters.AddWithValue("@EngineerUKSecurityCheck", EngineerUKSecurityCheck)
            cmd.Parameters.AddWithValue("@EngineerRightToWorkInUK", EngineerRightToWorkInUK)
            cmd.Parameters.AddWithValue("@EngineerProofOfMeeting", EngineerProofOfMeeting)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@CategoryType", strCategoryType)
            'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
            cmd.Parameters.AddWithValue("@ExcelPartner", strExcelPartner)
            cmd.Parameters.AddWithValue("@EPCP", strEPCP)

            cmd.CommandTimeout = 1800
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchSuppliers"
            ds.Tables(1).TableName = "tblCount"
            rowCount = ds.Tables("tblCount").Rows(0)(0)
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spContacts_GetSearchAccounts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    'spMS_Contacts_SearchAccounts
    ''' <summary>
    ''' Created By Pankaj Malav on 17 Nov 2008
    ''' Searches the accounts using Free Text Service
    ''' </summary>
    ''' <param name="Profile"></param>
    ''' <param name="CompanyAccreditations"></param>
    ''' <param name="OtherInfo"></param>
    ''' <param name="Keyword"></param>
    ''' <param name="CompanyName"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="rowCount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the buyer/Suppliers which match the Search Criteria Passed. "), _
   SoapHeader("Authentication")> _
    Public Function AdminSearchAccounts(ByVal Profile As String, ByVal CompanyAccreditations As String, ByVal OtherInfo As String, ByVal Keyword As String, ByVal CompanyName As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMS_Contacts_SearchAccounts"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@keyword", Keyword)
            cmd.Parameters.AddWithValue("@Profile", Profile)
            cmd.Parameters.AddWithValue("@Accreditations", CompanyAccreditations)
            cmd.Parameters.AddWithValue("@AdditionalInfo", OtherInfo)
            cmd.Parameters.AddWithValue("@CompanyName", CompanyName)
            cmd.Parameters.AddWithValue("@BizDivId", ApplicationSettings.OWUKBizDivId)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)

            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchAccounts"
            ds.Tables(1).TableName = "tblCount"
            rowCount = ds.Tables("tblCount").Rows(0)(0)
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spContacts_GetSearchAccounts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' gets the categories for a contact in bizdivid
    ''' </summary>
    ''' <param name="contactId"></param>
    ''' <param name="paramBizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Get combinationids for AOE for a contact in a bizdiv"), _
SoapHeader("Authentication")> _
    Public Function GetContactsCategories(ByVal combIds As String, ByVal paramBizDivId As Integer) As DataSet

        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetCategories"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@combIds", combIds)
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AOE")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCombIds"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    <WebMethod(Description:="Get combinationids for AOE for a contact in a bizdiv"), _
SoapHeader("Authentication")> _
    Public Function GetContactsAOE(ByVal combIds As String, ByVal paramBizDivId As Integer) As DataSet

        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetAOE"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@combIds", combIds)
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AOE")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    <WebMethod(Description:="Get combinationids for AOE for a contact in a bizdiv"), _
    SoapHeader("Authentication")> _
    Public Function GetCategoriesForBizDivId(ByVal paramBizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetCategoriesForBizDiv"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AOE")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCombIds"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    ''' <summary>
    ''' Web Method to create new contact
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will create new contact"), _
   SoapHeader("Authentication")> _
    Public Function CreateContact(ByVal xmlStr As String, ByVal LoggedInUserID As Integer) As Boolean
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_Create_Contact"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value

            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to reutn All Attributes for a contact
    ''' </summary>
    ''' <param name="ContactId"></param>
    ''' <param name="AttributeLabel"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will get all attributes for a contact"), _
          SoapHeader("Authentication")> _
    Public Function GetAttributesForContact(ByVal ContactId As Integer, ByVal AttributeLabel As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spContacts_GetAttributeForContact"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@AttributeLabel", AttributeLabel)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Attributes")
            da.Fill(ds)
            ds.Tables(0).TableName = "Attributes"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function



    ''' <summary>
    ''' To fetch the only Location name of the contact
    ''' </summary>
    ''' <param name="ContactID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Webmethod to fetch location Name only for particular for a contact"), _
    SoapHeader("Authentication")> _
    Public Function GetLocationName(ByVal ContactID) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetLocationName"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Locations")
            da.Fill(ds)
            ds.Tables(0).TableName = "LocationName"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function


    ''' <summary>
    ''' WebMethod to return the details of the contact
    ''' </summary>
    ''' <param name="contactID"></param>
    ''' <param name="IsMainContact"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns>Returns dataset for contact details</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return details of the contact."), _
  SoapHeader("Authentication")> _
    Public Function GetContact(ByVal contactID As Integer, ByVal IsMainContact As Integer, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetContactDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@contactID", contactID)
            cmd.Parameters.AddWithValue("@IsMainContact", IsMainContact)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ContactInfo")
            da.Fill(ds)

            ds.Tables(0).TableName = "Contacts"
            ds.Tables(1).TableName = "ContactSettings"
            ds.Tables(2).TableName = "ContactReferences"
            'ds.Tables(3).TableName = "ContactMethods"
            'ds.Tables(4).TableName = "ContactAttributes"
            ds.Tables(4).TableName = "ContactAttachments"
            'ds.Tables(5).TableName = "ContactAttributesMemo"
            'ds.Tables(6).TableName = "ContactLinkage"
            'ds.Tables(7).TableName = "ClassesLinkage"
            'ds.Tables(8).TableName = "ContactSettings"
            'ds.Tables(9).TableName = "ContactReferences"
            'ds.Tables(10).TableName = "ContactRolesLinkage"
            'ds.Tables(11).TableName = "ContactAttachments"
            'ds.Tables(12).TableName = "ContactBizDivLinkage"
            'ds.Tables(13).TableName = "ContactSkillsExams"
            'ds.Tables(14).TableName = "ContactInsurAttachment"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will add or edit the location"), _
   SoapHeader("Authentication")> _
    Public Function AddEditSpecialist(ByVal xmlStr As String, ByVal contactID As Integer, ByVal doMode As String, ByVal bizDivID As Integer, ByVal LoggedInUserID As Integer) As Boolean
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_SaveSpecialist"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@contactID", contactID)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()

            Return CInt(cmd.Parameters.Item("@Success").Value)
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveSpecialist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to check whether the email address already registered with the site or not
    ''' </summary>
    ''' <param name="Email"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Check whether the email address already registered with the site or not."), _
  SoapHeader("Authentication")> _
    Public Function DoesLoginExist(ByVal Email As String) As Boolean
        Dim mailStatus As Boolean = False
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_DoesEmailExist"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Email", Email)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("UserData")
            da.Fill(ds)
            ds.Tables(0).TableName = "UserData"
            If ds.Tables("UserData").Rows.Count > 0 Then
                If Not IsDBNull(ds.Tables("UserData").Rows(0).Item("ContactID")) Then
                    'User exists
                    mailStatus = True
                Else
                    'User not registered error.
                    mailStatus = False
                End If
            Else
                mailStatus = False
            End If
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

        DoesLoginExist = mailStatus
    End Function

    ''' <summary>
    ''' Get User details of the loggedin user to be able to edit the settings.
    ''' </summary>
    ''' <param name="ContactID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Get User details of the loggedin user to be able to edit the settings."), _
  SoapHeader("Authentication")> _
    Public Function EditLoggedInUser(ByVal ContactID As Integer) As DataSet
        Dim mailStatus As Boolean = False
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSContacts_getAdminUserDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("UserData")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will fetch 'ReceiveWONotif' and 'FulFilWOs' settings for a company "), _
    SoapHeader("Authentication")> _
    Public Function GetContactSettings(ByVal companyID As Integer, ByVal contactId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetSettings"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", companyID)
            cmd.Parameters.AddWithValue("@ContactID", contactId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Settings")
            da.Fill(ds)
            ds.Tables(0).TableName = "Setting"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' method used by admin to fetch contacts details as well its controls privleges
    ''' </summary>
    ''' <param name="contactID"></param>
    ''' <param name="IsMainContact"></param>
    ''' <param name="BizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return details of the contact."), _
SoapHeader("Authentication")> _
    Public Function AdminGetContactDetails(ByVal contactID As Integer, ByVal IsMainContact As Integer, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_GetContactDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@contactID", contactID)
            cmd.Parameters.AddWithValue("@IsMainContact", IsMainContact)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ContactInfo")
            da.Fill(ds)

            ds.Tables(0).TableName = "Contacts"
            ds.Tables(1).TableName = "ContactSettings"
            ds.Tables(2).TableName = "ContactReferences"
            ds.Tables(3).TableName = "ContactsSetUp"
            ds.Tables(4).TableName = "ContactSkillsExams"
            ds.Tables(5).TableName = "ContactAttachments"
            ds.Tables(6).TableName = "ControlsPrivileges"
            ds.Tables(7).TableName = "FinanceSettings"
            ds.Tables(8).TableName = "TimeSlots"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod returns accounts activation status with contactid and maincontactid
    ''' </summary>
    ''' <param name="userName"></param>
    ''' <param name="password"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns>Dataset with table value as ContactID, MainContactId, bizDivId</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method returns accounts activation status with contactid and maincontactid"), _
SoapHeader("Authentication")> _
    Public Function GetAccountActivationStatus(ByVal userName As String, ByVal password As String, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetActivationInfo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserName", userName)
            cmd.Parameters.AddWithValue("@password", password)
            cmd.Parameters.AddWithValue("@bizDivId", BizDivId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AccountStatus")
            da.Fill(ds)
            ds.Tables(0).TableName = "Account"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetAccountActivationStatus. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will return boolean value  the company profile"), _
    SoapHeader("Authentication")> _
    Public Function IsAddedUser(ByVal Email As String) As Boolean
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_IsAddedUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Email", Email)
            cmd.Parameters.AddWithValue("@IsAddedUser", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@IsAddedUser").Value
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method will get Contact Listing Details for a particular contact type (Client' /  'ApprovedSupplier' / 'NonApprovedSupplier' / 'Supplier')with Paging/Sorting. "), _
      SoapHeader("Authentication")> _
    Public Function GetContactListing(ByVal bizDivId As String, ByVal MainClassID As String, ByVal FromDate As String, ByVal ToDate As String, ByVal statusId As String, ByVal DateCriteriaField As String, ByVal Status As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spAdmin_GetContactsListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@MainClassID", MainClassID)
            cmd.Parameters.AddWithValue("@statusId", statusId)
            cmd.Parameters.AddWithValue("@Status", Status)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            End If
            cmd.Parameters.AddWithValue("@DateCriteriaField", DateCriteriaField)

            cmd.Parameters.AddWithValue("@BizDivId", CInt(bizDivId))

            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contacts")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContacts"
            ds.Tables(1).TableName = "tblCount"
            If Not IsDBNull(ds.Tables("tblCount").Rows(0)(0)) Then
                rowCount = ds.Tables("tblCount").Rows(0)(0)
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:=""), _
   SoapHeader("Authentication")> _
    Public Function GetUnapprovedEngineersListing(ByVal bizDivId As String, ByVal CompanyID As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spAdmin_GetUnapprovedEngineersListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivId", CInt(bizDivId))
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contacts")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContacts"
            ds.Tables(1).TableName = "tblCount"
            If Not IsDBNull(ds.Tables("tblCount").Rows(0)(0)) Then
                rowCount = ds.Tables("tblCount").Rows(0)(0)
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:=""), _
   SoapHeader("Authentication")> _
    Public Function GetDixonsComplaintsListing(ByVal IsProcessed As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer, ByRef ComplaintRefNo As String, ByRef ComplaintType As Integer, ByRef DateCreatedFrom As String, ByRef DateCreatedTo As String, ByRef DateProcessedFrom As String, ByRef DateProcessedTo As String, ByVal Type As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spAdmin_GetDixonsComplaints"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IsProcessed", IsProcessed)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@ComplaintRefNo", ComplaintRefNo)
            cmd.Parameters.AddWithValue("@ComplaintType", ComplaintType)
            cmd.Parameters.AddWithValue("@DateCreatedFrom", DateCreatedFrom)
            cmd.Parameters.AddWithValue("@DateCreatedTo", DateCreatedTo)
            cmd.Parameters.AddWithValue("@DateProcessedFrom", DateProcessedFrom)
            cmd.Parameters.AddWithValue("@DateProcessedTo", DateProcessedTo)
            cmd.Parameters.AddWithValue("@Type", Type)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("DixonsComplaints")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblDixonsComplaints"
            ds.Tables(1).TableName = "tblCount"
            If Not IsDBNull(ds.Tables("tblCount").Rows(0)(0)) Then
                rowCount = ds.Tables("tblCount").Rows(0)(0)
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:=""), _
   SoapHeader("Authentication")> _
    Public Function GetAccreditationTagsListing(ByVal IsActive As Integer, ByVal IsLocked As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer, ByRef TagName As String, ByRef TagFor As String, ByRef DateCreatedFrom As String, ByRef DateCreatedTo As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spAdmin_GetAccreditationTags"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IsActive", IsActive)
            cmd.Parameters.AddWithValue("@IsLocked", IsLocked)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@TagName", TagName)
            cmd.Parameters.AddWithValue("@TagFor", TagFor)
            cmd.Parameters.AddWithValue("@DateCreatedFrom", DateCreatedFrom)
            cmd.Parameters.AddWithValue("@DateCreatedTo", DateCreatedTo)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AccreditationTags")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAccreditationTags"
            ds.Tables(1).TableName = "tblCount"
            If Not IsDBNull(ds.Tables("tblCount").Rows(0)(0)) Then
                rowCount = ds.Tables("tblCount").Rows(0)(0)
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:=""), _
   SoapHeader("Authentication")> _
    Public Function GetAccreditationContactTagsListing(ByVal IsActive As Integer, ByVal IsLocked As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer, ByRef TagName As String, ByRef TagFor As String, ByRef DateCreatedFrom As String, ByRef DateCreatedTo As String, ByVal Name As String, ByVal ApprovalStatus As Nullable(Of Integer)) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spAdmin_GetAccreditationContactTags"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IsActive", IsActive)
            cmd.Parameters.AddWithValue("@IsLocked", IsLocked)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@TagName", TagName)
            cmd.Parameters.AddWithValue("@TagFor", TagFor)
            cmd.Parameters.AddWithValue("@Name", Name)
            cmd.Parameters.AddWithValue("@ApprovalStatus", ApprovalStatus)
            cmd.Parameters.AddWithValue("@DateCreatedFrom", DateCreatedFrom)
            cmd.Parameters.AddWithValue("@DateCreatedTo", DateCreatedTo)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AccreditationTags")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAccreditationTags"
            ds.Tables(1).TableName = "tblCount"
            If Not IsDBNull(ds.Tables("tblCount").Rows(0)(0)) Then
                rowCount = ds.Tables("tblCount").Rows(0)(0)
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method will Add Remove Accreditations."), _
      SoapHeader("Authentication")> _
    Public Function AddUpdateAccreditationTags(ByVal IsActive As Integer, ByVal IsLocked As Integer, ByVal TagId As Integer, ByVal TagFor As String, ByVal TagName As String, ByVal LoggedInUserID As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spAdmin_AddUpdateAccreditationTags"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@TagId", TagId)
            cmd.Parameters.AddWithValue("@TagFor", TagFor)
            cmd.Parameters.AddWithValue("@TagName", TagName)
            cmd.Parameters.AddWithValue("@IsActive", IsActive)
            cmd.Parameters.AddWithValue("@IsLocked", IsLocked)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will CheckUniqueness"), _
     SoapHeader("Authentication")> _
    Public Function CheckUniqueness(ByVal Value As String, ByVal Type As String, ByVal TagId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_CheckUniqueness"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Value", Value)
            cmd.Parameters.AddWithValue("@Type", Type)
            cmd.Parameters.AddWithValue("@Id", TagId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Value")
            da.Fill(ds)
            ds.Tables(0).TableName = "Value"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationName. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function
    ''' <summary>
    ''' This method updates entries into the Classes Linkage table.
    ''' </summary>
    ''' <param name="ClassID"></param>
    ''' <param name="ContactIDs"></param>
    ''' <param name="BizDivID"></param>
    ''' <param name="AdminComp"></param>
    ''' <param name="AdminCont"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method updates entries into the Classes Linkage table. "), _
 SoapHeader("Authentication")> _
    Public Function UpdateCompanyType(ByVal ClassID As Integer, ByVal Status As Integer, ByVal ContactIDs As String, ByVal BizDivID As Integer, ByVal AdminComp As Integer, ByVal AdminCont As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            'Dim WOTrackingID As Integer
            Dim ProcedureName As String = "spContacts_Update_CompanyType"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@CompanyID", ContactIDs)
                .Parameters.AddWithValue("@Status", Status)
                .Parameters.AddWithValue("@ActionByComp", AdminComp)
                .Parameters.AddWithValue("@ActionByCont", AdminCont)
                .Parameters.AddWithValue("@BizDivId", BizDivID)
                .Parameters.AddWithValue("@ClassID", ClassID)
            End With

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contacts")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContacts"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:=""), _
SoapHeader("Authentication")> _
    Public Function UpdateContactsOWApproveStatus(ByVal ContactIDs As String, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            'Dim WOTrackingID As Integer
            Dim ProcedureName As String = "spContacts_UpdateContactsOWApproveStatus"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@ContactIDs", ContactIDs)
                .Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            End With

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Status")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:=""), _
SoapHeader("Authentication")> _
    Public Function UpdateDixonsComplaints(ByVal ComplaintId As String, ByVal IsProcessed As Integer, ByVal ComplaintType As Integer, ByVal ContactID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            'Dim WOTrackingID As Integer
            Dim ProcedureName As String = "spAdmin_AddUpdateDixonsComplaints"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@ComplaintId", ComplaintId)
                .Parameters.AddWithValue("@IsProcessed", IsProcessed)
                .Parameters.AddWithValue("@ComplaintType", ComplaintType)
                .Parameters.AddWithValue("@ContactID", ContactID)
            End With

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Status")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This WebMethod used to Add/Edit user info and change Login info of user who is logged in to system.
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="contactID"></param>
    ''' <param name="doMode"></param>
    ''' <param name="bizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    <WebMethod(Description:="This method will add or edit the user Info"),
    SoapHeader("Authentication")>
    Public Function AddEditSpecialistAndChangeLoginInfo(ByVal xmlStr As String, ByVal contactID As Integer, ByVal doMode As String, ByVal bizDivID As Integer, ByVal UserName As String, ByVal OldPassword As String, ByVal NewPassword As String, ByVal SecurityQuest As Integer, ByVal SecurityAns As String, ByVal VersionNo As Byte(), ByVal LoggedInUserID As Integer, ByVal IPhonePin As Integer, ByVal OtherLocations As String, ByVal xmlAccreds As String, ByVal xmlEmergencyContact As String, ByVal VehicleRegNo As String, ByVal CVRequired As Boolean, ByVal DBSExpiryDate As String, ByVal IsRecordVerified As Boolean) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim success As Integer
            Dim ProcedureName As String = "spMS_Contacts_SaveSpecialistAndChangeLoginInfo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@contactID", contactID)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            cmd.Parameters.AddWithValue("@UserName", UserName)
            cmd.Parameters.AddWithValue("@OldPassword", OldPassword)
            cmd.Parameters.AddWithValue("@NewPassword", NewPassword)
            cmd.Parameters.AddWithValue("@SecurityQues", SecurityQuest)
            cmd.Parameters.AddWithValue("@SecurityAns", SecurityAns)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)
            cmd.Parameters.AddWithValue("@IPhonePin", IPhonePin)
            cmd.Parameters.AddWithValue("@OtherLocations", OtherLocations)
            cmd.Parameters.AddWithValue("@xmlAccreds", xmlAccreds)
            'Poonam modified on 30/12/2015 - Task - OA-112 : OA - Add Emergency Contact fields in User detail page
            cmd.Parameters.AddWithValue("@xmlEmergencyContact", xmlEmergencyContact)
            cmd.Parameters.AddWithValue("@VehicleRegNo", VehicleRegNo)
            cmd.Parameters.AddWithValue("@CVRequired", CVRequired)
            cmd.Parameters.AddWithValue("@IsRecordVerified", IsRecordVerified)

            If DBSExpiryDate <> "" And DBSExpiryDate <> "Enter expiry date" Then
                cmd.Parameters.AddWithValue("@DBSExpiryDate", Date.Parse(DBSExpiryDate))
            Else
                cmd.Parameters.AddWithValue("@DBSExpiryDate", DBSExpiryDate)
            End If



            'cmd.Parameters.AddWithValue("@OWApproved", OWApproved)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()

            '  Return CInt(cmd.Parameters.Item("@Success").Value)
            success = cmd.Parameters.Item("@Success").Value
            Return success
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveSpecialist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will update references"), _
     SoapHeader("Authentication")> _
    Public Function ConvertBuyerToSupplier(ByVal xmlStr As String, ByVal comapnyID As Integer, ByVal contactId As Integer, ByVal BizDivID As Integer) As Boolean
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_BuyerToSupplier"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@CompanyID", comapnyID)
            cmd.Parameters.AddWithValue("@ContactID", contactId)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            ' Return cmd.ExecuteNonQuery()

            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method fetch accreditatios for the company level"), _
   SoapHeader("Authentication")> _
    Public Function GetCompanyAccreditations(ByVal BizDivID As Integer, ByVal ContactID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "[spContacts_GetCompanyAccreditations]"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Vendors")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAllVendors"
            ds.Tables(1).TableName = "tblAllPartners"
            ds.Tables(2).TableName = "tblSelectedVendors"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' WebMethod to update company profile
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will update the company profile"),
        SoapHeader("Authentication")>
    Public Function UpdateCompanyProfile(ByVal xmlStr As String, ByVal contactId As Integer, ByVal bizDivID As Integer, ByVal VersionNo As Byte(), ByVal LoggedInUserID As Integer, ByVal ExcelPartner As Integer, ByVal EPCP As Integer, ByVal EmpPaymentTermDiscount As Decimal, ByVal EmpPaymentTermId As Integer, ByVal OwPaymentTermDiscount As Decimal, ByVal OwPaymentTermId As Integer, ByVal IsRecordVerified As Boolean) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim success As Integer
            Dim ProcedureName As String = "spContacts_SaveCompanyProfile"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            cmd.Parameters.AddWithValue("@ContactID", contactId)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            'Poonam added on 30/09/2016  -  Task  - OA-303 : OA - Add Excel Partner and EPCP Checkboxes for Suppliers and in Search/ordermatch
            cmd.Parameters.AddWithValue("@ExcelPartner", ExcelPartner)
            cmd.Parameters.AddWithValue("@EPCP", EPCP)
            'OA-622 discount changes
            cmd.Parameters.AddWithValue("@EmpPaymentTermId", EmpPaymentTermId)
            cmd.Parameters.AddWithValue("@EmpPaymentTermDiscount", EmpPaymentTermDiscount)
            cmd.Parameters.AddWithValue("@OwPaymentTermId", OwPaymentTermId)
            cmd.Parameters.AddWithValue("@OwPaymentTermDiscount", OwPaymentTermDiscount)
            cmd.Parameters.AddWithValue("@IsRecordVerified", IsRecordVerified)
            cmd.ExecuteNonQuery()

            'Return cmd.Parameters("@Success").Value
            success = cmd.Parameters.Item("@Success").Value
            Return success
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' WebMethod to update company's account setting
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will update the company's account settings"),
        SoapHeader("Authentication")>
    Public Function UpdateAccountSettingForCompany(ByVal xmlStr As String, ByVal contactId As Integer, ByVal bizDivID As Integer, ByVal VersionNo As Byte(), ByVal FinanceLabels As String, ByVal LoggedInUserID As Integer, ByVal CategoryType As String, ByVal source As String, ByVal WOCreationFlow As Integer, ByVal NewPortalAccess As Boolean, ByVal OldPortalAccess As Boolean) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim Success As Integer
            Dim ProcedureName As String = "spContacts_SaveAccountSettings"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            cmd.Parameters.AddWithValue("@ContactID", contactId)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)
            cmd.Parameters.AddWithValue("@FinanceFields", FinanceLabels)
            cmd.Parameters.AddWithValue("@CategoryType", CategoryType)
            cmd.Parameters.AddWithValue("@source", source)
            cmd.Parameters.AddWithValue("@WOCreationFlow", WOCreationFlow)
            cmd.Parameters.AddWithValue("@NewPortalAccess", NewPortalAccess)
            cmd.Parameters.AddWithValue("@OldPortalAccess", OldPortalAccess)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Success = cmd.Parameters("@Success").Value
            Return Success
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method fetch accreditatios for the company level"), _
       SoapHeader("Authentication")> _
    Public Function GetContactAccreditations(ByVal BizDivID As Integer, ByVal ContactID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "[spContacts_GetContactAccreditations]"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Accreds")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAllVendors"
            ds.Tables(1).TableName = "tblAllCerts"
            ds.Tables(2).TableName = "tblSelectedAccreds"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to Fetch location listing
    ''' </summary>
    ''' <param name="ContactID"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Webmethod to fetch location listing for a contact"), _
   SoapHeader("Authentication")> _
    Public Function GetLocationListing(ByVal ContactID As Integer, ByVal UserSiteType As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                                ByVal maximumRows As Integer, ByRef recordCount As System.Nullable(Of Integer), ByVal FilterString As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetLocations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@siteType", UserSiteType)
            cmd.Parameters.AddWithValue("@FilterString", FilterString)
            cmd.Parameters.Add(New SqlParameter("@numresults", SqlDbType.Int, 0, ParameterDirection.Output, False, 0, 0, Nothing, DataRowVersion.Default, 0))
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("LocationListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "Locations"
            recordCount = ds.Tables(1).Rows(0).Item("TotalRecs")
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    ''' <summary>
    ''' Webmethod to add the location or edit the existing location
    ''' </summary>
    ''' <param name="xmlStr">XML file for the Locations</param>
    ''' <param name="addressId">Address ID </param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will add or edit the location"), _
    SoapHeader("Authentication")> _
    Public Function AddEditLocation(ByVal xmlStr As String, ByVal addressId As Integer, ByVal VersionNo As Byte(), ByVal LoggedInUserID As Integer, ByVal DoNotIncludeInOrdermatch As Boolean) As Integer

        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim Success As Integer
            Dim ProcedureName As String = "spContacts_SaveLocations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@AddressID", addressId)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@DoNotIncludeInOrdermatch", DoNotIncludeInOrdermatch)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()

            Success = cmd.Parameters("@Success").Value
            Return Success
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will migrate contrct from source to dest bizdiv
    ''' </summary>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the skills and exams for a contact"), _
   SoapHeader("Authentication")> _
    Public Function Migrate(ByVal CompanyId As Double, ByVal sourceBizDiv As Integer, ByVal destBizDiv As Integer, ByVal xmlContent As String, ByVal LoggedInUserID As Integer) As Boolean
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spContacts_Migrate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@contactId", CompanyId)
            cmd.Parameters.AddWithValue("@sourceBizDiv", sourceBizDiv)
            cmd.Parameters.AddWithValue("@destBizDiv", destBizDiv)
            cmd.Parameters.AddWithValue("@xmlDoc", xmlContent)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value

            ' cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
        Return Nothing
    End Function

    ''' <summary>
    ''' Web Method to return migration status details
    ''' </summary>
    ''' <param name="eMail"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the account Migration  status  Details"), _
   SoapHeader("Authentication")> _
    Public Function GetAccountMigrationDetails(ByVal eMail As String, ByVal isRegisterPage As Boolean) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_CheckMigrationStatus"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Email", eMail)
            If isRegisterPage Then
                Select Case country
                    Case ApplicationSettings.CountryDE
                        Select Case ApplicationSettings.OWUKBizDivId
                            Case ApplicationSettings.OWDEBizDivId
                                cmd.Parameters.AddWithValue("@BizDivId", ApplicationSettings.OWDEPeer1BizDivId)
                        End Select
                    Case ApplicationSettings.CountryUK
                        Select Case ApplicationSettings.OWUKBizDivId
                            Case ApplicationSettings.OWUKBizDivId
                                cmd.Parameters.AddWithValue("@BizDivId", ApplicationSettings.OWUKPeer1BizDivId)
                            Case ApplicationSettings.SFUKBizDivId
                                cmd.Parameters.AddWithValue("@BizDivId", ApplicationSettings.SFUKPeer1BizDivId)
                        End Select
                End Select
            Else
                cmd.Parameters.AddWithValue("@BizDivId", bizDivId)
            End If



            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("MigrationStatus")
            da.Fill(ds)
            ds.Tables(0).TableName = "user"
            ds.Tables(1).TableName = "spcialistCount"
            ds.Tables(2).TableName = "contactAttributes"
            ds.Tables(3).TableName = "companyAttributes"
            ds.Tables(4).TableName = "contactRoleLinkage"
            ds.Tables(5).TableName = "contactBizDivLinkage"
            ds.Tables(6).TableName = "companyBizDivLinkage"
            ds.Tables(7).TableName = "contactClasses"
            ds.Tables(8).TableName = "ContactAccreditations"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Webmethod to update the references of the contact.
    ''' </summary>
    ''' <param name="xmlStr">XML file of references</param>
    ''' <param name="contactId">ContactID of contact</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will update references"), _
    SoapHeader("Authentication")> _
    Public Function UpdateReferences(ByVal xmlStr As String, ByVal contactId As Integer) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_SaveReferences"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@ContactID", contactId)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Decimal).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method will get all suppliers in a regions specified and having AOE specified"), _
    SoapHeader("Authentication")> _
    Public Function GetSupplierDetails(ByVal SupplierId As Integer, ByVal bizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMS_Contacts_GetSupplierDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@SupplierId", SupplierId)
            cmd.Parameters.AddWithValue("@BizDivId", bizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Suppliers")
            da.Fill(ds)
            ds.Tables(0).TableName = "SupplierDetails"
            ds.Tables(1).TableName = "Region"
            ds.Tables(2).TableName = "Locations"
            ds.Tables(3).TableName = "AOE"
            ds.Tables(4).TableName = "CertsQuals"
            ds.Tables(5).TableName = "Attributes"
            ds.Tables(6).TableName = "Ratings"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to fetch the dataset and rowcount for sample supplier listing
    ''' </summary>
    ''' <param name="aoe"></param>
    ''' <param name="region"></param>
    ''' <param name="bizDivID"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="recordCount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Webmethod to fetch sample supplier's listing for a contact"), _
  SoapHeader("Authentication")> _
    Public Function GetSampleSuppliersListing(ByVal aoe As String, ByVal region As String, ByVal bizDivID As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                               ByVal maximumRows As Integer, ByRef recordCount As System.Nullable(Of Integer)) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_Contacts_SampleSuppliers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AOE", aoe)
            cmd.Parameters.AddWithValue("@Region", region)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@numresults", SqlDbType.Int).Direction = ParameterDirection.Output
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SampleSuppliers")
            da.Fill(ds)
            ds.Tables(0).TableName = "Supplier"
            recordCount = ds.Tables(1).Rows(0).Item("TotalRecs")
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function

    ''' <summary>
    ''' This webmethod returns the supplier count 
    ''' </summary>
    ''' <param name="AOE"></param>
    ''' <param name="Region"></param>
    ''' <returns>returns supplier count</returns>
    ''' <remarks></remarks>

    <WebMethod(Description:="This method will get count of Suppliers and specialists"), _
       SoapHeader("Authentication")> _
    Public Function GetSupplierCount(ByVal AOE As String, ByVal Region As String, ByVal BizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spContacts_GetSuppliersCount"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AOE", AOE)
            cmd.Parameters.AddWithValue("@Region", Region)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Suppliers")
            da.Fill(ds)
            ds.Tables(0).TableName = "SupplierCount"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_GetSuppliersCount. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This method will save Rating for Supplier,Cover Amount,Expiry Date,General Comments, VAT Registartion No,Company Reg. No.,and Company Strength to DB
    ''' </summary>
    ''' <param name="RatedCompany"></param>
    ''' <param name="RatedContact"></param>
    ''' <param name="RatingCompany"></param>
    ''' <param name="RatingContact"></param>
    ''' <param name="GeneralComments"></param>
    ''' <param name="Rating1"></param>
    ''' <param name="Comment1"></param>
    ''' <param name="Rating2"></param>
    ''' <param name="Comment2"></param>
    ''' <param name="Rating3"></param>
    ''' <param name="Comment3"></param>
    ''' <param name="coverAmtEmpLiability"></param>
    ''' <param name="coverAmtPublicLiability"></param>
    ''' <param name="coverAmtProfIndemnity"></param>
    ''' <param name="dateEmpLiability"></param>
    ''' <param name="datePublicLiability"></param>
    ''' <param name="dateProfIndemnity"></param>
    ''' <param name="compRegno"></param>
    ''' <param name="VATRegno"></param>
    ''' <param name="CompStrength"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will Add a Rating for a Supplier/Client for a Work Order by a Client/Supplier"), _
  SoapHeader("Authentication")> _
    Public Function AddUpdateReferenceCheck(ByVal xmlStr As String, ByVal ContactID As Integer, ByVal RatedCompany As Integer, ByVal RatedContact As Integer, ByVal RatingCompany As Integer, ByVal RatingContact As Integer, ByVal ClassID As Integer, ByVal BizDivID As Integer, ByVal IsApprove As Boolean, ByVal LoggedInUserID As Integer, ByVal RandomPassword As String) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_Update_ContactRefCheck"
            Dim cmd As New SqlCommand
            cmd = New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@RatedCompany", RatedCompany)
            cmd.Parameters.AddWithValue("@RatedContact", RatedContact)
            cmd.Parameters.AddWithValue("@RatingCompany", RatingCompany)
            cmd.Parameters.AddWithValue("@RatingContact", RatingContact)

            cmd.Parameters.AddWithValue("@Status", ClassID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@IsApprove", IsApprove)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            'poonam modified on 14/8/2015 - Task - OM-13:OW - ENH - Add Create Supplier account Page in OW
            cmd.Parameters.AddWithValue("@RandomPassword", RandomPassword)
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value
            ' theTrans.Commit()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Update_ContactRefCheck. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This methos will return the details of the users who can log into the admin site.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the details of the users who can log into the admin site."), _
            SoapHeader("Authentication")> _
    Public Function GetAdminSiteUsers(ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByVal HideDeleted As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spSecurity_GetAdminSiteUsers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@HideDeleted", HideDeleted)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AdminSiteUsers")
            da.Fill(ds)
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This methos will return the details of the users who can log into the admin site.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the listing of contractors"), _
            SoapHeader("Authentication")> _
    Public Function GetContractor(ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByVal AOE As String, ByVal HideDeleted As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spContacts_GetContractor"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@HideDeleted", HideDeleted)
            cmd.Parameters.AddWithValue("@AOE", AOE)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contractor")
            da.Fill(ds)
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This methos will return the details of the users who can log into the admin site.
    ''' </summary>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the listing of contractors"), _
            SoapHeader("Authentication")> _
    Public Function ShowDeleteContractor(ByVal ContractorId As Integer, ByVal Action As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spContacts_ShowDeleteContractor"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContractorId", ContractorId)
            cmd.Parameters.AddWithValue("@Action", Action)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contractor")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This method adds the admin user.
    ''' </summary>
    ''' <param name="UserName"></param>
    ''' <param name="Password"></param>
    ''' <param name="SecurityQues"></param>
    ''' <param name="SecurityAns"></param>
    ''' <param name="FName"></param>
    ''' <param name="LName"></param>
    ''' <param name="JobTitle"></param>
    ''' <param name="RoleGroupID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method updates entries into the Classes Linkage table. "), _
SoapHeader("Authentication")> _
    Public Function AddAdminUser(ByVal UserName As String, ByVal Password As String, ByVal SecurityQues As Integer, ByVal SecurityAns As String, ByVal FName As String, ByVal LName As String, ByVal JobTitle As String, ByVal RoleGroupID As Integer, ByVal enableLogin As Boolean, ByVal LoggedInUserID As Integer, ByVal MainCat As String) As Boolean
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            'Dim WOTrackingID As Integer
            Dim ProcedureName As String = "spAdmin_CreateNewAdminUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@UserName", UserName)
                .Parameters.AddWithValue("@Password", Password)
                .Parameters.AddWithValue("@SecurityQues", SecurityQues)
                .Parameters.AddWithValue("@SecurityAns", SecurityAns)
                .Parameters.AddWithValue("@FName", FName)
                .Parameters.AddWithValue("@LName", LName)
                .Parameters.AddWithValue("@JobTitle", JobTitle)
                .Parameters.AddWithValue("@RoleGroupID", RoleGroupID)
                .Parameters.AddWithValue("@enableLogin", enableLogin)
                .Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
                .Parameters.AddWithValue("@MainCat", MainCat)
            End With

            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method edits the admin user
    ''' </summary>
    ''' <param name="UserName"></param>
    ''' <param name="FName"></param>
    ''' <param name="LName"></param>
    ''' <param name="RoleGroupID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method updates entries into the Classes Linkage table. "), _
SoapHeader("Authentication")> _
    Public Function EditAdminUser(ByVal UserName As String, ByVal FName As String, ByVal LName As String, ByVal Password As String, ByVal RoleGroupID As Integer, ByVal ContactID As Integer, ByVal enableLogin As Boolean, ByVal LoggedInUserID As Integer, ByVal MainCat As String, ByVal MenuID As Integer) As Boolean
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            'Dim WOTrackingID As Integer
            Dim ProcedureName As String = "spAdmin_EditAdminUser"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@UserName", UserName)
                .Parameters.AddWithValue("@FName", FName)
                .Parameters.AddWithValue("@LName", LName)
                .Parameters.AddWithValue("@Password", Password)
                .Parameters.AddWithValue("@RoleGroupID", RoleGroupID)
                .Parameters.AddWithValue("@ContactID", ContactID)
                .Parameters.AddWithValue("@enableLogin", enableLogin)
                .Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
                .Parameters.AddWithValue("@MainCat", MainCat)
                .Parameters.AddWithValue("@MenuID", MenuID)
            End With
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will return the reference check form fields for a particular contact.
    ''' </summary>
    ''' <param name="CompanyID"></param>
    ''' <param name="ContactId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the reference check form fields for a particular contact. "), _
SoapHeader("Authentication")> _
    Public Function GetRefCheckData(ByVal CompanyID As Integer, ByVal ContactId As Integer, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_ReferenceCheck"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            'Dim RegionId As String
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)


            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "ContactAttributes"
            ds.Tables(1).TableName = "ContactAttachInsur"
            ds.Tables(2).TableName = "ContactReferences"
            ds.Tables(3).TableName = "ContactRatingsRC"
            ds.Tables(4).TableName = "ContactApproval"
            ds.Tables(5).TableName = "ContactEmail"
            ds.Tables(6).TableName = "EmployeeLiabiliyAttachment"
            ds.Tables(7).TableName = "PublicLiabilityAttachment"
            ds.Tables(8).TableName = "ProfIndemnityAttachment"
            ds.Tables(9).TableName = "TrackDetailsSP"

            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spContacts_ReferenceCheck. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method returns dataset for contacts comments"), _
    SoapHeader("Authentication")> _
    Public Function GetCommentsForContact(ByVal contactID As Integer, ByVal bizDivID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetComments"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", contactID)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Comments")
            da.Fill(ds)
            ds.Tables(0).TableName = "Comment"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetAccountActivationStatus. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' WEb method to add comment to the contact
    ''' </summary>
    ''' <param name="contactId"></param>
    ''' <param name="commentBy"></param>
    ''' <param name="comment"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will add comment for a contact"), _
    SoapHeader("Authentication")> _
    Public Function AddComments(ByVal contactId As Integer, ByVal commentBy As Integer, ByVal comment As String, ByVal AttachmentXSD As String, ByVal Reason As Integer) As Boolean
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_AddComments"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", contactId)
            cmd.Parameters.AddWithValue("@CommentBy", commentBy)
            cmd.Parameters.AddWithValue("@Comments", comment)
            cmd.Parameters.AddWithValue("@AttachmentXSD", AttachmentXSD)
            'Poonam added on 9/8/2016 - Task - OA-300 : OA - Make add comment mandatory via pop-up when Deleting or Suspending a company
            cmd.Parameters.AddWithValue("@Reason", Reason)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to fetch limited list of the web content dataset with column as type and contentid
    ''' </summary>
    ''' <param name="type"></param>
    ''' <param name="bizDivID"></param>
    ''' <param name="showonSite"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will fetch data for limited list of press releases, articles comment for a contact"), _
SoapHeader("Authentication")> _
    Public Function GetWebContentLimitedList(ByVal type As String, ByVal bizDivID As Integer, ByVal showonSite As Boolean, ByVal NoOfRecs As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetWebContentLimitedList"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Type", type)
            cmd.Parameters.AddWithValue("@BizDivID", bizDivID)
            cmd.Parameters.AddWithValue("@ShowOnSite", showonSite)
            cmd.Parameters.AddWithValue("@NoofRec", NoOfRecs)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WebContents")
            da.Fill(ds)
            ds.Tables(0).TableName = type
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function





    ''' <summary>
    ''' Web method for fetching web content of all type
    ''' </summary>
    ''' <param name="showOnSite"></param>
    ''' <param name="type"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="recordCount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Webmethod to fetch sample supplier's listing for a contact"), _
SoapHeader("Authentication")> _
    Public Function GetAllWebContent(ByVal showOnSite As Boolean, ByVal type As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, _
                           ByVal maximumRows As Integer, ByRef recordCount As System.Nullable(Of Integer)) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_Contacts_GetAllWebContent"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Type", type)
            cmd.Parameters.AddWithValue("@ShowOnSite", showOnSite)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WebContents")
            da.Fill(ds)
            ds.Tables(0).TableName = "WebContent"
            recordCount = ds.Tables(1).Rows(0).Item("TotalRecs")
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetLocationListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try

    End Function


    ''' <summary>
    ''' Web method to return record for a particular contentID
    ''' </summary>
    ''' <param name="contentID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method returns dataset for particular contentID"), _
SoapHeader("Authentication")> _
    Public Function GetWebContent(ByVal contentID As Integer, ByVal contentType As String, ByVal sitefor As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_Contacts_GetWebContentForContentID"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContentID", contentID)
            cmd.Parameters.AddWithValue("@SiteType", sitefor)
            cmd.Parameters.AddWithValue("@ContentType", contentType)



            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contents")
            da.Fill(ds)
            ds.Tables(0).TableName = "Content"
            ds.Tables(1).TableName = "WebContentLinkage"
            ds.Tables(2).TableName = "ContentPageLinkage"
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetWebContent. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving account activation information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Web method to add and edit web content
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="contentID"></param>
    ''' <param name="doMode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will add or edit web content"), _
 SoapHeader("Authentication")> _
    Public Function AddEditWebContent(ByVal xmlStr As String, ByVal contentID As Integer, ByVal doMode As String, ByVal strChkBox As String) As Boolean

        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_Content_SaveWebContent"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@doMode", doMode)
            cmd.Parameters.AddWithValue("@contentID", contentID)
            cmd.Parameters.AddWithValue("@strChkBox", strChkBox)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value


        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return web contents to be shown on site, such as press releases, articles, careers and quotation
    ''' </summary>
    ''' <param name="type"></param>
    ''' <param name="showOnSite"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method fetch WebContent for site"), _
    SoapHeader("Authentication")> _
    Public Function GetWebContentForSite(ByVal type As String, ByVal showOnSite As Boolean, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetWebContentForSite"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Type", type)
            cmd.Parameters.AddWithValue("@ShowOnSite", showOnSite)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contents")
            da.Fill(ds)
            ds.Tables(0).TableName = "Content"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return web contents to be shown on site, such as press releases, articles, careers and quotation
    ''' </summary>
    ''' <param name="type"></param>
    ''' <param name="showOnSite"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method fetch WebContent for site"), _
    SoapHeader("Authentication")> _
    Public Function GetContentForSiteForm(ByVal type As String, ByVal showOnSite As Boolean, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spGetWebContentForSite"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Type", type)
            cmd.Parameters.AddWithValue("@ShowOnSite", showOnSite)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contents")
            da.Fill(ds)
            ds.Tables(0).TableName = "Content"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will get the favourite suppliers for a given buyer "), _
     SoapHeader("Authentication")> _
    Public Function MS_GetFavouriteSuppliers(ByVal bizDivId As String, ByVal BuyerCompId As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer, ByVal mode As String, ByVal SearchContact As String, ByVal billingLoc As String, ByVal Comment As String, ByVal FixedCapacity As Integer, ByVal FixedCapacityTimeSlot As String, ByVal AllowJobAcceptance As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMSContacts_GetFavouriteSuppliers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", bizDivId)
            cmd.Parameters.AddWithValue("@BuyerCompId", BuyerCompId)
            cmd.Parameters.AddWithValue("@mode", mode)
            cmd.Parameters.AddWithValue("@SearchContact", SearchContact)
            cmd.Parameters.AddWithValue("@billingLoc", billingLoc)
            cmd.Parameters.AddWithValue("@Comment", Comment)
            cmd.Parameters.AddWithValue("@FixedCapacity", FixedCapacity)
            cmd.Parameters.AddWithValue("@FixedCapacityTimeSlot", FixedCapacityTimeSlot)
            cmd.Parameters.AddWithValue("@AllowJobAcceptance", AllowJobAcceptance)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contacts")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContacts"
            ds.Tables(1).TableName = "tblCount"
            ds.Tables(2).TableName = "IsAM"
            If ds.Tables.Count > 3 Then
                ds.Tables(3).TableName = "tblLoction"
            End If

            If Not IsDBNull(ds.Tables("tblCount").Rows(0)(0)) Then
                rowCount = ds.Tables("tblCount").Rows(0)(0)
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMSContacts_GetFavouriteSuppliers. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method will remove the favourite suppliers for a given buyer "), _
     SoapHeader("Authentication")> _
    Public Function MS_RemoveSupplierFromFavList(ByVal bizDivId As String, ByVal BuyerCompId As String, ByVal supplierCompanyids As String, ByVal LoggedInUserID As Integer, ByVal mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMS_Contacts_RemoveSupplierFromFavList"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", bizDivId)
            cmd.Parameters.AddWithValue("@BuyerCompId", BuyerCompId)
            cmd.Parameters.AddWithValue("@SupplierCompIds", supplierCompanyids)

            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@mode", mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Status")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMS_Contacts_RemoveSupplierFormFavList. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will return the Suppliers which match the Search Criteria Passed. "), _
   SoapHeader("Authentication")> _
    Public Function MS_GetSuppliers(ByVal BuyerCompanyId As Integer, ByVal paramBizDivId As Integer, ByVal SkillSet As String, ByVal Region As String, ByVal PostCode As String, ByVal Keyword As String, ByVal StatusId As String, ByVal fName As String, ByVal lName As String, ByVal companyName As String, ByVal email As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer, ByVal mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim i As Integer
            If SkillSet <> "" Then
                'Remove Product - Other option

                SkillSet = SkillSet.Replace("782", "")
                SkillSet = SkillSet.Replace(",,", ",")
                If SkillSet.StartsWith(",") = True Then
                    SkillSet = SkillSet.Substring(1, SkillSet.Length - 1)
                End If
                If SkillSet.EndsWith(",") = True Then
                    SkillSet = SkillSet.Substring(0, SkillSet.Length - 1)
                End If

                If SkillSet <> "" Then
                    Dim strSkills() As String = Split(SkillSet, ",")
                    SkillSet = ""
                    For i = 0 To strSkills.GetLength(0) - 1
                        If SkillSet <> "" Then
                            SkillSet &= ","
                        End If
                        SkillSet &= "'" & CStr(strSkills.GetValue(i)) & "'"
                    Next
                End If
            End If

            If Region <> "" Then
                Dim strRegionCode() As String = Split(Region, ",")
                Region = ""
                For i = 0 To strRegionCode.GetLength(0) - 1
                    If Region <> "" Then
                        Region &= ","
                    End If
                    Region &= "'" & CStr(strRegionCode.GetValue(i)) & "'"
                Next
            End If
            If Keyword.IndexOf("'") <> -1 Then
                Keyword = Keyword.Replace("'", "")
            End If
            Dim ProcedureName As String = "spMS_Contacts_GetSearchSupplier"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            cmd.Parameters.AddWithValue("@keyword", Keyword)
            cmd.Parameters.AddWithValue("@PostCode", PostCode)
            cmd.Parameters.AddWithValue("@SkillSet", SkillSet)
            cmd.Parameters.AddWithValue("@RegionCode", Region)
            cmd.Parameters.AddWithValue("@StatusId", StatusId)
            cmd.Parameters.AddWithValue("@firstName", fName)
            cmd.Parameters.AddWithValue("@lastName", lName)
            cmd.Parameters.AddWithValue("@companyName", companyName)
            cmd.Parameters.AddWithValue("@Email", email)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@BuyerCompId", BuyerCompanyId)
            cmd.Parameters.AddWithValue("@mode", mode)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchAccounts"
            ds.Tables(1).TableName = "tblCount"
            rowCount = ds.Tables("tblCount").Rows(0)(0)
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spContacts_GetSearchAccounts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will add the favourite suppliers to a given buyer's list "), _
        SoapHeader("Authentication")> _
    Public Function MS_AddSupplierToFavList(ByVal bizDivId As String, ByVal BuyerCompId As String, ByVal supplierCompanyids As String, ByVal mode As String, ByVal billingLocId As Integer, ByVal Comment As String, ByVal LoggedInUserID As Integer, ByVal FixedCapacity As Integer, ByVal FixedCapacityTimeSlot As String, ByVal AllowJobAcceptance As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMS_Contacts_AddSupplierToFavList"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", bizDivId)
            cmd.Parameters.AddWithValue("@BuyerCompId", BuyerCompId)
            cmd.Parameters.AddWithValue("@SupplierCompIds", supplierCompanyids)
            cmd.Parameters.AddWithValue("@billingLocId", billingLocId)
            cmd.Parameters.AddWithValue("@mode", mode)
            cmd.Parameters.AddWithValue("@Comment", Comment)
            cmd.Parameters.AddWithValue("@FixedCapacity", FixedCapacity)
            cmd.Parameters.AddWithValue("@FixedCapacityTimeSlot", FixedCapacityTimeSlot)
            cmd.Parameters.AddWithValue("@AllowJobAcceptance", AllowJobAcceptance)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Status")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblStatus"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spMS_Contacts_RemoveSupplierFormFavList. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to Add Registrant Details to the DB called from UCWhitePaperDownloads
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will create new contact"), _
   SoapHeader("Authentication")> _
    Public Function SaveRegistrantDetails(ByVal FName As String, ByVal LName As String, ByVal Email As String, ByVal CName As String, ByVal Position As String, ByVal Phone As String, ByVal MarketingActivity As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_SaveRegistrantDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@FName", FName)
            cmd.Parameters.AddWithValue("@LName", LName)
            cmd.Parameters.AddWithValue("@Email", Email)
            cmd.Parameters.AddWithValue("@CName", CName)
            cmd.Parameters.AddWithValue("@Position", Position)
            cmd.Parameters.AddWithValue("@Phone", Phone)
            cmd.Parameters.AddWithValue("@MarketingActivity", MarketingActivity)
            'cmd.Parameters.AddWithValue("@RegID", SqlDbType.Int).Direction = ParameterDirection.Output
            'cmd.ExecuteNonQuery()
            'Return cmd.Parameters("@RegID").Value
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("RegistrantDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblRegistrantDetails"
            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to Update Registrant Details i.e Confirm the Email Address
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will create new contact"), _
   SoapHeader("Authentication")> _
    Public Function UpdateRegistrantDetails(ByVal RegID As Integer) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_UpdateRegistrantEmailStatus"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RegID", RegID)

            cmd.Parameters.AddWithValue("@Status", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Status").Value

            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web Method to Get Registrant Details to be displayed in Admin Site
    ''' </summary>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaxRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will create new contact"), _
   SoapHeader("Authentication")> _
    Public Function GetRegistrantListing(ByVal SortExpression As String, ByVal StartRowIndex As String, ByVal MaxRows As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_Contacts_GetRegistrantListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", MaxRows)
            cmd.ExecuteNonQuery()
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("RegistrantListing")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblRegistrantDetails"
            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method will create new contact"), _
SoapHeader("Authentication")> _
    Public Function GetInsuranceDetails(ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivId As Integer, ByVal SortExpression As String, ByVal StartRowIndex As String, ByVal MaxRows As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetInsuranceDetailsForSuppliers"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", MaxRows)
            cmd.ExecuteNonQuery()
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("InsuranceDetails")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblInsuranceDetails"
            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method will Populate Suppliers that have opt for Accept Multiple WO"), _
SoapHeader("Authentication")> _
    Public Function GetContactAcceptMultipleWO(ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_GetServicePartnersAcceptMultipleWO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.ExecuteNonQuery()
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("ServicePartners")
            da.Fill(ds)
            ds.Tables(0).TableName = "ContactMultipleAcceptedWO"
            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method will get the AutoMatch Settings for a user"), _
SoapHeader("Authentication")> _
    Public Function GetAutoMatchSettings(ByVal CompanyID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_GetAutoMatchSettings"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.ExecuteNonQuery()
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AutoMatchSettings")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSettings"
            Return ds
            ' Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method will Save the AutoMatch Settings for a user"), _
SoapHeader("Authentication")> _
    Public Function SaveAutomatchSettings(ByVal CompanyID As Integer, ByVal ReferenceCheck As Boolean, ByVal SecurityCheck As Boolean _
            , ByVal CRBCheck As Boolean, ByVal NearestDistance As String, ByVal AutoMatchStatus As Boolean, ByVal VersionNo As Byte(), ByVal LoggedInUserID As Integer _
            , ByVal EngineerCRBCheck As Boolean, ByVal EngineerCSCSCheck As Boolean, ByVal EngineerUKSecurityCheck As Boolean, ByVal EngineerRightToWorkInUK As Boolean) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_SaveAutoMatchSettings"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ReferenceCheck", ReferenceCheck)
            cmd.Parameters.AddWithValue("@SecurityCheck", SecurityCheck)
            cmd.Parameters.AddWithValue("@CRBCheck", CRBCheck)
            cmd.Parameters.AddWithValue("@NearestDistance", NearestDistance)
            cmd.Parameters.AddWithValue("@AutoMatchStatus", AutoMatchStatus)
            cmd.Parameters.AddWithValue("@VersionNo", VersionNo)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@EngineerCRBCheck", EngineerCRBCheck)
            cmd.Parameters.AddWithValue("@EngineerCSCSCheck", EngineerCSCSCheck)
            cmd.Parameters.AddWithValue("@EngineerUKSecurityCheck", EngineerUKSecurityCheck)
            cmd.Parameters.AddWithValue("@EngineerRightToWorkInUK", EngineerRightToWorkInUK)


            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            Return cmd.Parameters("@Success").Value

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Check depot location
    ''' </summary>
    ''' <param name="LocationId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Check whether the Location is a depot location."), _
     SoapHeader("Authentication")> _
    Public Function CheckDepotLocation(ByVal LocationId As Integer) As DataSet
        Dim IsDepot As Boolean = False
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_Contacts_CheckDepotLocation"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@LocationId", LocationId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "UserData"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Getting Holiday Listing
    ''' </summary>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaxRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>  
    <WebMethod(Description:="Get Listing of all Holidays."), _
     SoapHeader("Authentication")> _
    Public Function GetHolidays(ByVal SortExpression As String, ByVal StartRowIndex As String, ByVal MaxRows As String, ByVal showAll As Boolean, ByVal src As String, ByVal ContactId As Integer) As DataSet
        Dim IsDepot As Boolean = False
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetHolidayListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@showAll", showAll)
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", MaxRows)
            cmd.Parameters.AddWithValue("@src", src)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "HolidaysListing"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Add or Update Holidays
    ''' </summary>
    ''' <param name="DateID"></param>
    ''' <param name="HolidayDate"></param>
    ''' <param name="Holidaytxt"></param>
    ''' <param name="IsDeleted"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Get Listing of all Holidays."), _
     SoapHeader("Authentication")> _
    Public Function AddUpdateHolidays(ByVal DateID As Integer, ByVal HolidayDateFrom As String, ByVal Holidaytxt As String, ByVal IsDeleted As Boolean, ByVal ContactId As Integer) As DataSet
        Dim IsDepot As Boolean = False
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_AddUpdateHolidays"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@DateID", DateID)
            cmd.Parameters.AddWithValue("@HolidayTxt", Holidaytxt)
            cmd.Parameters.AddWithValue("@HolidayDateString", HolidayDateFrom)
            cmd.Parameters.AddWithValue("@IsDeleted", IsDeleted)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method is used to save multiple specialist
    ''' </summary>
    ''' <param name="xmlStr"></param>
    ''' <param name="contactID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will add or edit the user Info"), _
    SoapHeader("Authentication")> _
    Public Function AddMultipleSpecialist(ByVal xmlStr As String, ByVal contactID As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_BulkAction_NAME)
        Try
            Dim ProcedureName As String = "spMS_Contacts_SaveMultipleSpecialist"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlDoc", xmlStr)
            cmd.Parameters.AddWithValue("@contactID", contactID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveSpecialist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This method is used to migrate the account from Client to Supplier
    ''' </summary>
    ''' <param name="contactID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will add or edit the user Info"), _
    SoapHeader("Authentication")> _
    Public Function MigrateAccounts(ByVal BizDivID As Integer, ByVal CompanyId As Integer, ByVal CompanyName As String, ByVal VAT As String, ByVal chkFreesat As Boolean, ByVal chkElex As Boolean, ByVal chkIT As Boolean, ByVal chkpc As Boolean, ByVal LoggedInUserID As Integer) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spAccounts_Migration"
            Dim Success As Integer
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@CompanyName", CompanyName)
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@VAT", VAT)
            cmd.Parameters.AddWithValue("@chkFreesat", chkFreesat)
            cmd.Parameters.AddWithValue("@chkElex", chkElex)
            cmd.Parameters.AddWithValue("@chkIT", chkIT)
            cmd.Parameters.AddWithValue("@chkpc", chkpc)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            Success = cmd.Parameters("@Success").Value
            Return Success
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_SaveSpecialist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will add or edit the message"), _
    SoapHeader("Authentication")> _
    Public Function PostMessage(ByVal MsgID As Integer, ByVal Subject As String, ByVal Message As String, ByVal ForContactClassID As Integer, ByVal Status As Integer, ByVal Type As Integer, ByVal SkillSet As String, ByVal ISImp As Boolean, ByVal IsDisplayLogin As Boolean, ByVal FromDisplayDate As String, ByVal EndDisplayDate As String) As DataSet

        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_PostMesagesOrFeedback"

            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@MsgID", MsgID)
            cmd.Parameters.AddWithValue("@Subject", Subject)
            cmd.Parameters.AddWithValue("@Message", Message)
            cmd.Parameters.AddWithValue("@ContactClassID", ApplicationSettings.ContactClassID.OW)
            cmd.Parameters.AddWithValue("@CompanyID", System.Configuration.ConfigurationManager.AppSettings("AdminCompanyID"))
            cmd.Parameters.AddWithValue("@ContactID", 1174)
            cmd.Parameters.AddWithValue("@ForContactClassID", ForContactClassID)
            cmd.Parameters.AddWithValue("@ForCompanyID", 0) 'Passing 0 for now
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@Type", Type)
            cmd.Parameters.AddWithValue("@SkillSet", SkillSet)

            cmd.Parameters.AddWithValue("@ISImp", ISImp)
            cmd.Parameters.AddWithValue("@IsDisplayLogin", IsDisplayLogin)
            cmd.Parameters.AddWithValue("@FromDisplayDate", FromDisplayDate)
            cmd.Parameters.AddWithValue("@EndDisplayDate", EndDisplayDate)


            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSuccess"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will add or edit the message"), _
    SoapHeader("Authentication")> _
    Public Function GetMessageDetails(ByVal MsgID As Integer) As DataSet

        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetMessageDetails"

            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@MsgID", MsgID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="Get Listing of all Holidays."), _
     SoapHeader("Authentication")> _
    Public Function GetGeneralMessagesListing(ByVal SortExpression As String, ByVal StartRowIndex As String, ByVal MaxRows As String, ByVal Status As Integer, ByVal Type As Integer, ByVal Month As Integer, ByVal Year As Integer) As DataSet
        Dim IsDepot As Boolean = False
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetGeneralMessagesListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", MaxRows)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@ForContactClassID", 0)
            cmd.Parameters.AddWithValue("@Type", Type)
            cmd.Parameters.AddWithValue("@Month", Month)
            cmd.Parameters.AddWithValue("@Year", Year)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "MsgsListing"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_doesLoginExist. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while checking login credentials. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Added new method for fetching ratings
    ''' </summary>
    ''' <param name="ContactID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the account summary for a Contact"), _
SoapHeader("Authentication")> _
    Public Function MS_GetContactRatings(ByVal ContactID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_GetContactRatings"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Ratings")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact' account  information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will return the cancel reasom listing."), _
           SoapHeader("Authentication")> _
    Public Function GetStandardListing(ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByVal Type As String, ByVal IsActive As Boolean) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spSecurity_GetStandardListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@Type", Type)
            cmd.Parameters.AddWithValue("@IsActive", IsActive)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AdminCancel")
            da.Fill(ds)
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will Add Edit Cancelreason."), _
           SoapHeader("Authentication")> _
    Public Function AddEditStandard(ByVal txtcancelreason As String, ByVal mode As String, ByVal StandardId As Integer, ByVal Type As String, ByVal IsActive As Boolean) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spSecurity_AddEditStandard"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@txtcancelreason", txtcancelreason)
            cmd.Parameters.AddWithValue("@mode", mode)
            cmd.Parameters.AddWithValue("@StandardId", StandardId)
            cmd.Parameters.AddWithValue("@Type", Type)
            cmd.Parameters.AddWithValue("@IsActive", IsActive)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AdminCancel")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will return the SP working days."), _
        SoapHeader("Authentication")> _
    Public Function GetSPHoliday(ByVal CompanyId As Integer, ByVal WorkingDays As String, ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spMSAccounts_GetSPWorkingDays"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyId)
            cmd.Parameters.AddWithValue("@WorkingDays", WorkingDays)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("SPHolidays")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSPHolidaySummary"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will Accreditations."), _
        SoapHeader("Authentication")> _
    Public Function GetAccreditations(ByVal CommonID As Integer, ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spContacts_GetAccreditations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CommonID", CommonID)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Accreditations")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will Add Remove Accreditations."), _
       SoapHeader("Authentication")> _
    Public Function AddRemoveAccreditations(ByVal ContactID As Integer, ByVal TagId As Integer, ByVal TagType As String, ByVal TagName As String, ByVal TagInfo As String, ByVal OtherInfo As String, ByVal TagExpiry As String, ByVal Mode As String, ByVal TagIdToRemove As String, ByVal LoggedInUserID As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spContacts_AddRemoveAccreditations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@TagId", TagId)
            cmd.Parameters.AddWithValue("@TagType", TagType)
            cmd.Parameters.AddWithValue("@TagName", TagName)
            cmd.Parameters.AddWithValue("@TagInfo", TagInfo)
            cmd.Parameters.AddWithValue("@OtherInfo", OtherInfo)
            cmd.Parameters.AddWithValue("@TagExpiry", TagExpiry)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@TagIdToRemove", TagIdToRemove)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will add or edit TimeBuilder QA"), _
    SoapHeader("Authentication")> _
    Public Function AddEditTimeBuilderQA(ByVal xmlContent As String, ByVal NoOfQuestions As Integer, ByVal Mode As String, ByVal CompanyID As Integer, ByVal ProductID As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_BulkAction_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_AddEditTimeBuilderQA"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlContent", xmlContent)
            cmd.Parameters.AddWithValue("@NoOfQuestions", NoOfQuestions)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ProductID", ProductID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return the Usertypes for the selected user
    ''' </summary>
    <WebMethod(Description:=" Web method to return the Usertypes for the selected user"), _
SoapHeader("Authentication")> _
    Public Function GetUserType(ByVal CompanyId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spAdmin_GetUserTypeForSelectedCompany"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "UserTypes"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to update the default booking form for the selected user and usertype
    ''' </summary>
    <WebMethod(Description:=" Web method to return the Usertypes for the selected user"), _
SoapHeader("Authentication")> _
    Public Function UpdateDefaultBookingForm(ByVal CompanyId As Integer, ByVal RoleGroupId As Integer, ByVal DefaultBookingForm As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spAdmin_UpdateDefaultBookingFormForUserType"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@RoleGroupId", RoleGroupId)
            cmd.Parameters.AddWithValue("@DefaultBookingForm", DefaultBookingForm)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds

        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will return the Account History listing."), _
         SoapHeader("Authentication")> _
    Public Function GetAccountHistory(ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByVal CompanyId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "GetAccountHistoryDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AccountHistoryDetails")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will return the Account History listing."), _
         SoapHeader("Authentication")> _
    Public Function GetUserAccountHistory(ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByVal ContactId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "GetUserAccountHistoryDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("UserAccountHistoryDetails")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method Update Contact Image"), _
         SoapHeader("Authentication")> _
    Public Function UpdateContactImage(ByVal ContactId As Integer, ByVal ImageUrl As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spUpdateContactImage"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@imageUrl", ImageUrl)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("AccountHistoryDetails")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method Update Contact Profile compalte email sent info"), _
         SoapHeader("Authentication")> _
    Public Function UpdateUserProfileCompletionMailInfo(ByVal ContactId As Integer, ByVal IsProfileCompleted As Integer, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spContacts_UpdateIsProfileCompleted"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            cmd.Parameters.AddWithValue("@IsProfileCompleted", IsProfileCompleted)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Success")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method GetProfileCompleteData info"), _
         SoapHeader("Authentication")> _
    Public Function GetProfileCompleteData(ByVal ContactID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)

        Try
            Dim ProcedureName As String = "spContacts_GetProfileCompleteData"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactId", ContactID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("tblData")
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while deleting the sample work order. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    <WebMethod(Description:="This method will get Contact Listing Details for a particular contact type (Client' /  'ApprovedSupplier' / 'NonApprovedSupplier' / 'Supplier')with Paging/Sorting. "),
      SoapHeader("Authentication")>
    Public Function GetAutoUnapprovedSuppliersListing(ByVal bizDivId As String, ByVal FromDate As String, ByVal ToDate As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spAdmin_GetAutoUnapprovedSuppliersListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
            End If
            cmd.Parameters.AddWithValue("@BizDivId", CInt(bizDivId))
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("Contacts")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblContacts"
            ds.Tables(1).TableName = "tblCount"
            If Not IsDBNull(ds.Tables("tblCount").Rows(0)(0)) Then
                rowCount = ds.Tables("tblCount").Rows(0)(0)
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will get Engineer Rate Info"),
      SoapHeader("Authentication")>
    Public Function GetEngineerRateInfo(ByVal ContactId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "sp_GetEngineerRateInfo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@ContactId", ContactId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("EngineerRateInfo")
            da.Fill(ds)
            ds.Tables(0).TableName = "EngineerRateInfo"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will save Engineer Rate Info"),
      SoapHeader("Authentication")>
    Public Function SaveEngineerRateInfo(ByVal xmlEngineerRateInfo As String, ByVal ContactId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "sp_UpdateEngineerRateInfo"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlEngineerRateInfo", xmlEngineerRateInfo)
            cmd.Parameters.AddWithValue("@ContactId", ContactId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "Success"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

End Class