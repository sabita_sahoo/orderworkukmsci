Imports System.Web
Imports System.Web.Services
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections
Imports System.Web.Services.Protocols
Imports System.Security.Principal
Imports System.Collections.Specialized
Imports System.Security.Cryptography
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Text
Imports Authentication
Imports Util
Imports System.Diagnostics

<System.Web.Services.WebService(Namespace:="http://tempuri.org/WS/WSStandards")> _
Public Class WSStandards
    Inherits System.Web.Services.WebService


    Public Authentication As AuthenticationHeader
    Private Shared LT_CONNECTION_NAME As String = "webdbUK"
    Private Shared _siteCountry As String
    Private Shared _bizDivId As Integer
    Private Shared _siteType As String



#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region



    Public Shared Property country() As String
        Get
            Return _siteCountry
        End Get
        Set(ByVal value As String)
            _siteCountry = value
        End Set
    End Property
    Public Shared Property bizDivId() As Integer
        Get
            Return _bizDivId
        End Get
        Set(ByVal value As Integer)
            _bizDivId = value
        End Set
    End Property

    Public Property siteType() As String
        Get
            Return _siteType
        End Get
        Set(ByVal value As String)
            _siteType = value
        End Set
    End Property

    ''' <summary>
    ''' This webmethod intializes web service depending on the country and bizdivid
    ''' to select db depending on country 
    ''' </summary>
    ''' <param name="paramCountry"></param>
    ''' <param name="paramBizDivId"></param>
    ''' <returns>Nothing</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Initialize properties"), _
    SoapHeader("Authentication")> _
   Public Function IntializeProperties(ByVal paramCountry As String, ByVal paramBizDivId As Integer, ByVal paramSiteType As String)
        bizDivId = paramBizDivId
        country = paramCountry
        siteType = paramSiteType
        CommonFunctions.InitializeProperties(paramCountry, LT_CONNECTION_NAME)
        Return Nothing
    End Function


    ''' <summary>
    ''' Web Method to return all main category types defined in table "tblStandardsWOTypesLinkage"
    ''' </summary>
    ''' <param name="paramBizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return all main category types defined in tblStandardsWOTypesLinkage."), _
        SoapHeader("Authentication")> _
    Public Function GetMainCategoryTypes(ByVal paramBizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spStandards_GetMainCatTypes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "MainCategoryTypes"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method will return all sub  category types for maincatid defined in tblStandardsWOTypesLinkage.
    ''' </summary>
    ''' <param name="mainCatId"></param>
    ''' <param name="paramBizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return all sub  category types for maincatid defined in tblStandardsWOTypesLinkage."), _
        SoapHeader("Authentication")> _
    Public Function GetSubCategoryTypes(ByVal mainCatId As Integer, ByVal paramBizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spStandards_GetSubCatTypes"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@MainCatId", mainCatId)
            cmd.Parameters.AddWithValue("@BizDivId", paramBizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "SubCategoryTypes"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    '''  Web Method to return general standards required
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return all category types defined in tblStandardsWOTypesLinkage."), _
        SoapHeader("Authentication")> _
    Public Function GetGeneralStandards() As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spLookUp_GetCompanyStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "General"
            ds.Tables(1).TableName = "CertQual"
            ds.Tables(2).TableName = "Regions"
            ds.Tables(3).TableName = "Skills"
            ds.Tables(4).TableName = "Country"
            ds.Tables(5).TableName = "Industries"
            ds.Tables(6).TableName = "RolesGroups"
            ds.Tables(7).TableName = "AdminUsers"
            ds.Tables(8).TableName = "DiscountPaymentTerm"
            ds.Tables(9).TableName = "WoCreationFlow"


            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Web method to return all standards defined in tblStandardsGeneral for the passed label business, and Account type of orderwork.
    ''' </summary>
    ''' <param name="Label"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return all standards defined in tblStandardsGeneral for the passed label, business and account type of orderwork."), _
SoapHeader("Authentication")> _
Public Function GetStandardsForWebContent(ByVal Label As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)


        Try
            Dim ProcedureName As String = "spLookUp_GetStandardsForWebContent"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Label", Label)


            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "StandardsGeneral"
            ds.Tables(1).TableName = "Business"

            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Web method to return reports for Single company report based on Case 1155    
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="FromDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return all return report based on contact and date selected ."), _
SoapHeader("Authentication")> _
Public Function GetSingleCompanyReport(ByVal BizDivID As Integer, ByVal CompanyID As Integer, ByVal ToDate As String, ByVal FromDate As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "sp_MSGenerateReportCompany_New"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivID)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web method to return all standards defined in tblStandardsGeneral for the passed label business, and Account type of orderwork.
    ''' </summary>
    ''' <param name="Label"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return all standards defined in tblStandardsGeneral for the passed label, business and account type of orderwork."), _
SoapHeader("Authentication")> _
Public Function GetStandards(ByVal Label As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)


        Try
            Dim ProcedureName As String = "spLookUp_GetStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Label", Label)


            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "StandardsGeneral"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    'Poonam added on 5/6/2015 - 4492527 - OW -- NEW -- Professional Service WO reports 
    ''' <summary>
    ''' Web method to return all standards defined in tblStandardsWO for the passed label 
    ''' </summary>
    ''' <param name="Label"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return all standards defined in tblStandardsWO for the passed label."), _
SoapHeader("Authentication")> _
    Public Function GetWoStandards(ByVal Label As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)


        Try
            Dim ProcedureName As String = "spLookUp_GetWoStandards"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Label", Label)


            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "StandardsWoGeneral"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    
    <WebMethod(Description:="This method will return all return report based on contact,email and date selected ."), _
SoapHeader("Authentication")> _
    Public Function GetSingleUserReport(ByVal CompanyID As Integer, ByVal Email As String, ByVal FromDate As String, ByVal ToDate As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "GenerateSingleUserReport"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@Email", Email)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This method will return all return report based on contact,email and date selected ."), _
SoapHeader("Authentication")> _
    Public Function UpdatePostCodeLatLong(ByVal Postcode As String, ByVal Latitude As String, ByVal Longitude As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "SP_CheckPostcodeExist"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            Postcode = OrderWorkLibrary.CommonFunctions.FormatPostcode(Postcode)
            cmd.Parameters.AddWithValue("@Postcode", Postcode)
            cmd.Parameters.AddWithValue("@Latitude", Latitude)
            cmd.Parameters.AddWithValue("@Longitude", Longitude)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will return all return report based on contact,email and date selected ."), _
SoapHeader("Authentication")> _
    Public Function GetAddress(ByVal RecordType As String, ByVal RecordID As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spContacts_GetAddress"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@RecordType", RecordType)
            cmd.Parameters.AddWithValue("@RecordID", RecordID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Web method to return reports for Single company report based on Case 1155    
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="FromDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return all return report based on contact and date selected ."),
SoapHeader("Authentication")>
    Public Function GetRevenueReport(ByVal SelMonth As String, ByVal SelYear As String, ByVal EndDate As String, ByVal ReportType As String, ByVal bizDivId As Integer, ByVal dtStart As String, ByVal dtEnd As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_RevenueReport"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 1000
            cmd.Parameters.AddWithValue("@SelMonth", SelMonth)
            cmd.Parameters.AddWithValue("@SelYear", SelYear)
            cmd.Parameters.AddWithValue("@EndDate", EndDate)
            cmd.Parameters.AddWithValue("@ReportType", ReportType)
            cmd.Parameters.AddWithValue("@BizDivId", bizDivId)
            cmd.Parameters.AddWithValue("@dtStart", dtStart)
            cmd.Parameters.AddWithValue("@dtEnd", dtEnd)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will return all return report based on contact and date selected ."),
       SoapHeader("Authentication")>
    Public Function GetServiceWoReport(ByVal DtFrom As String, ByVal DtTo As String, ByVal DtStart As String, ByVal DtEnd As String, ByVal SelPO As String, ByVal SelStatus As String, ByVal SelCName As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_ProfessionalServiceWOReport"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 1000
            cmd.Parameters.AddWithValue("@StartDate", DtFrom)
            cmd.Parameters.AddWithValue("@EndDate", DtTo)
            cmd.Parameters.AddWithValue("@SubmittedDateFrom", DtStart)
            cmd.Parameters.AddWithValue("@SubmittedDateTo", DtEnd)
            cmd.Parameters.AddWithValue("@PONumber", SelPO)
            cmd.Parameters.AddWithValue("@Status", SelStatus)
            cmd.Parameters.AddWithValue("@CompanyName", SelCName)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in GetCurrency. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while returning Currency information. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

End Class