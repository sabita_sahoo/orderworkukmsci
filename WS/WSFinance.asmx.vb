Imports System.Web
Imports System.Web.Services
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections
Imports System.Web.Services.Protocols
Imports System.Security.Principal
Imports System.Collections.Specialized
Imports System.Security.Cryptography
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Text
Imports Authentication
Imports Util
Imports System.Diagnostics

<System.Web.Services.WebService(Namespace:="http://tempuri.org/WS/WSFinance")> _
Public Class WSFinance
    Inherits System.Web.Services.WebService


    Public Authentication As AuthenticationHeader
    Private Shared LT_CONNECTION_NAME As String = "webdbUK"
    Private Shared LT_CONNECTION_BulkAction_NAME As String = "bulkdb"
    Private Shared _siteCountry As String
    Private Shared _bizDivId As Integer
    Private Shared _siteType As String


#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region



    Public Property country() As String
        Get
            Return _siteCountry
        End Get
        Set(ByVal value As String)
            _siteCountry = value
        End Set
    End Property
    Public Property bizDivId() As Integer
        Get
            Return _bizDivId
        End Get
        Set(ByVal value As Integer)
            _bizDivId = value
        End Set
    End Property

    Public Property siteType() As String
        Get
            Return _siteType
        End Get
        Set(ByVal value As String)
            _siteType = value
        End Set
    End Property



    ''' <summary>
    ''' This webmethod intializes web service depending on the country and bizdivid
    ''' to select db depending on country 
    ''' </summary>
    ''' <param name="paramCountry"></param>
    ''' <param name="paramBizDivId"></param>
    ''' <returns>Nothing</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Initialize properties"), _
    SoapHeader("Authentication")> _
  Public Function IntializeProperties(ByVal paramCountry As String, ByVal paramBizDivId As Integer, ByVal paramSiteType As String)
        bizDivId = paramBizDivId
        country = paramCountry
        siteType = paramSiteType
        CommonFunctions.InitializeProperties(paramCountry, LT_CONNECTION_NAME)
        Return Nothing
    End Function

    <WebMethod(Description:="This webmethod will return all the voucher entries for selected voucher type"), _
    SoapHeader("Authentication")> _
    Public Function MS_GetAccountVoucherDetails(ByVal BizDivId As Integer, ByVal Account As String, ByVal SubAccount As Integer, ByVal StartDate As String, ByVal EndDate As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetAccountVoucherDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@Account", Account)
            cmd.Parameters.AddWithValue("@SubAccount", SubAccount)
            cmd.Parameters.AddWithValue("@FromDate", StartDate)
            cmd.Parameters.AddWithValue("@ToDate", EndDate)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim ds As New DataSet("VoucherDetails")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAccountVoucherDetails"
            ds.Tables(1).TableName = "tblCount"
            Return ds

        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This webmethod will return all the purchase invoices"), _
       SoapHeader("Authentication")> _
       Public Function MS_GetPurchasePaymentAdviceListing(ByVal BizDivId As Integer, ByVal AdviceType As String, ByVal CompanyId As String, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal RequestFrom As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetPurchasePaymentAdviceListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@AdviceType", AdviceType)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@RequestFrom", RequestFrom)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)

            Dim ds As New DataSet("PurchaseInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblPurchaseInvoices"
            ds.Tables(1).TableName = "tblCount"
            If Status = "UnpaidAvailable" Then
                ds.Tables(2).TableName = "tblAvailableCount"
            End If
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method returns the listing of purchase invoices - unpaid and available
    ''' </summary>
    <WebMethod(Description:="This method returns the listing of purchase invoices - unpaid and available "), _
SoapHeader("Authentication")> _
    Public Function MS_GetPurchaseInvoicesForStatusUA(ByVal SupplierAccount As String, ByVal BizDivId As Integer, ByVal Over30Day As Boolean, ByVal HideAvailable As Boolean, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByVal HideDemoAccount As Boolean, ByVal mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetPurchaseInvoicesForStatusUA"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@SupplierAccount", SupplierAccount)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@Over30Day", Over30Day.ToString)
            cmd.Parameters.AddWithValue("@HideAvailable", HideAvailable)
            cmd.Parameters.AddWithValue("@HideDemoAccount", HideDemoAccount)
            cmd.Parameters.AddWithValue("@mode", mode)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblPurchInvListing"
            ds.Tables(1).TableName = "tblCount"
            ds.Tables(2).TableName = "tblUnPaidCount"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetAdviceListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Listing information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method returns the details about the purchase invoice
    ''' </summary>
    <WebMethod(Description:="This method returns the listing of purchase invoices - unpaid and available "), _
SoapHeader("Authentication")> _
    Public Function MS_GetPurchaseAdviceDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetPurchaseAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)            
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblBillingAddress"
            ds.Tables(2).TableName = "tblCustomerDetails"
            ds.Tables(3).TableName = "tblOWAddress"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetAdviceListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Listing information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This method will change the status of the selected invoices from unpaid to available
    ''' </summary>
    ''' <param name="bizDivId"></param>
    ''' <param name="InvoiceNos"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will change the status of the selected invoices from unpaid to available"), _
        SoapHeader("Authentication")> _
    Public Function MS_ChangePIStatusToAvailable(ByVal bizDivId As Integer, ByVal InvoiceNos As String, ByVal mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_ChangePIStatusToAvailable"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", bizDivId)
            cmd.Parameters.AddWithValue("@InvoiceNos", InvoiceNos)
            cmd.Parameters.AddWithValue("@mode", mode)
            Dim ds As New DataSet("InvoiceAvailable")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(1).TableName = "SupEmailData"
                ds.Tables(0).TableName = "Success"
            End If
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method returns the details about the cash payment receipt
    ''' </summary>
    ''' <param name="bizDivId"></param>
    ''' <param name="AdviceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will return the details about the cash payment receipt"), _
        SoapHeader("Authentication")> _
    Public Function MS_GetPaymentAdviceDetails(ByVal bizDivId As Integer, ByVal AdviceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetPaymentAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", bizDivId)
            cmd.Parameters.AddWithValue("@AdviceNumber", AdviceNumber)
            Dim ds As New DataSet("PaymentDetails")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblPaymentDetails"
            ds.Tables(2).TableName = "tblBillingAddress"
            ds.Tables(3).TableName = "tblCustomerDetails"
            ds.Tables(4).TableName = "tblOWAddress"

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This method will change the status of the selected invoices from available to requested
    ''' </summary>
    ''' <param name="bizDivId"></param>
    ''' <param name="InvoiceNos"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will change the status of the selected invoices from available to requested"), _
    SoapHeader("Authentication")> _
Public Function MSAccounts_ChangePIStatusToRequested(ByVal bizDivId As Integer, ByVal InvoiceNos As String, ByVal CompanyID As Integer, ByVal UserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_ChangePIStatusToRequested"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", bizDivId)
            cmd.Parameters.AddWithValue("@InvoiceNos", InvoiceNos)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserID)
            Dim ds As New DataSet("InvoiceRequested")

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in spContacts_Custom_GetContacts. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This method returns the details of the supplier payments.
    ''' </summary>
    ''' <param name="SubAccount"></param>
    ''' <param name="Status"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="UpdateVoucherStatus"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method returns the details of the supplier payments. "), _
SoapHeader("Authentication")> _
    Public Function MSAccounts_GetSupplierPaymentDetails(ByVal SubAccount As Integer, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal BizDivId As Integer, ByVal UpdateVoucherStatus As Integer, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByVal Invoices As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetSupplierPaymentDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@SubAccount", SubAccount)
            cmd.Parameters.AddWithValue("@Status", Status)
            If FromDate <> "" Then
                cmd.Parameters.AddWithValue("@FromDate", CommonFunctions.convertDate(FromDate))
                'cmd.Parameters.AddWithValue("@FromDate", FromDate)
            Else
                cmd.Parameters.AddWithValue("@FromDate", FromDate)
            End If
            If ToDate <> "" Then
                cmd.Parameters.AddWithValue("@ToDate", CommonFunctions.convertDate(ToDate))
                'cmd.Parameters.AddWithValue("@ToDate", ToDate)
            Else
                cmd.Parameters.AddWithValue("@ToDate", ToDate)
            End If

            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@UpdateVoucherStatus", UpdateVoucherStatus)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@Invoices", Invoices)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblPaymentDetails"
            If (maximumRows <> 0) Then
                ds.Tables(1).TableName = "tblCount"
            End If
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GetSupplierPaymentDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving records for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This function will update voucher status to complete and return dataset with one table(Contactid,username,CompanyName,FName,Supplierbalance,CompanyID,Vouchernumber,Debit)
    ''' </summary>
    ''' <param name="voucherDetails"></param>
    ''' <param name="dateCompleted"></param>
    ''' <param name="bizDivID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This function will update voucher status to complete and return dataset with one table(Contactid,username,CompanyName,FName,Supplierbalance,CompanyID,Vouchernumber,Debit) "), _
  SoapHeader("Authentication")> _
   Public Function MSAccounts_CompleteSupplierPayment(ByVal advices As String, ByVal dateCompleted As String, ByVal bizDivID As Integer, ByVal mode As String, ByVal UserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_BulkAction_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_CompleteSupplierPayment"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@PIs", advices)
            If dateCompleted <> "" Then
                cmd.Parameters.AddWithValue("@DateCompleted", CommonFunctions.convertDate(dateCompleted))
            Else
                cmd.Parameters.AddWithValue("@DateCompleted", dateCompleted)
            End If
            cmd.Parameters.AddWithValue("@BizDivId", bizDivID)
            cmd.Parameters.AddWithValue("@Mode", mode)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserID)
            cmd.CommandTimeout = 1200
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "tblSuppliers"
                ds.Tables(1).TableName = "tblPaymentDetails"
            End If
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_CompleteSupplierPayment. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while upda. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This method changes to status from new to in process
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method changes to status from new to in process "), _
SoapHeader("Authentication")> _
    Public Function MSAccounts_UpdateStatusToInProcess(ByVal BizDivId As Integer, ByVal FromDate As String, ByVal Todate As String, ByVal InvoiceNos As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_UpdateVoucherStatusToInprocess"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", Todate)
            cmd.Parameters.AddWithValue("@InvoiceNos", InvoiceNos)
            Dim ds As New DataSet("Success")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSuccess"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_UpdateVoucherStatusToInprocess. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This webmethod will return all the sales invoices"), _
      SoapHeader("Authentication")> _
    Public Function MS_GetSalesReceiptAdviceListing(ByVal BizDivId As Integer, ByVal AdviceType As String, ByVal CompanyId As String, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal RequestFrom As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer, ByVal InvoiceNo As String, ByVal Invoices As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetSalesReceiptAdviceListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@AdviceType", AdviceType)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@RequestFrom", RequestFrom)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            cmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo)
            cmd.Parameters.AddWithValue("@Invoices", Invoices)

            Dim ds As New DataSet("SalesInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSalesInvoices"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This webmethod will return all the paid sales invoices"), _
     SoapHeader("Authentication")> _
     Public Function MS_GetPaidSalesListing(ByVal BizDivId As Integer, ByVal AdviceType As String, ByVal CompanyId As String, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal RequestFrom As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetPaidSalesListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@AdviceType", AdviceType)
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@RequestFrom", RequestFrom)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim ds As New DataSet("SalesInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSalesInvoices"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This webmethod will return all the paid sales invoices"), _
     SoapHeader("Authentication")> _
     Public Function MS_GetCRCNListing(ByVal BizDivId As Integer, ByVal SINumber As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetCRCNListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@SINumber", SINumber)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim ds As New DataSet("SalesInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCRCNs"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method returns the details about the purchase invoice
    ''' </summary>
    <WebMethod(Description:="This method returns the listing of purchase invoices - unpaid and available "), _
SoapHeader("Authentication")> _
    Public Function MS_GetSalesAdviceDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetSalesAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblWODetails"
            ds.Tables(2).TableName = "tblBillingAddress"
            ds.Tables(3).TableName = "tblOrderWorkDetails"
            ds.Tables(4).TableName = "tblOWAddress"
            ds.Tables(5).TableName = "tblBankDetails"
            ds.Tables(6).TableName = "tblListingFee"
            ds.Tables(7).TableName = "tblWorkOrderList"
            ds.Tables(8).TableName = "tblFinanceFields"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GetSalesAdviceDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Listing information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method generates the sales invoice
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method generates the sales invoice"), _
SoapHeader("Authentication")> _
    Public Function MSAccounts_GenerateSalesInvoice(ByVal BizDivId As Integer, ByVal WOIDs As String, ByVal BuyerId As Integer, ByVal InvoiceDate As String, ByVal ListingFee As Decimal, ByVal UserID As Integer, ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_BulkAction_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GenSalesInvoice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 1800
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@WOIDs", WOIDs)
            cmd.Parameters.AddWithValue("@BuyerId", BuyerId)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserID)
            cmd.Parameters.AddWithValue("@InvoiceDate", InvoiceDate)
            cmd.Parameters.AddWithValue("@ListingFee", ListingFee)
            cmd.CommandTimeout = 10000
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GenSalesInvoice. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This method generates the lst of wos for selected sales invoice
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method generates the lst of wos for selected sales invoice"), _
SoapHeader("Authentication")> _
    Public Function MS_GetWOsForSalesInvoice(ByVal BizDivId As Integer, ByVal AdviceNumber As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetWOsForSalesInvoice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@AdviceNumber", AdviceNumber)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("WorkOrders")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrders"
            ds.Tables(1).TableName = "tblCount"
        
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GetWOsForSalesInvoice. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This method generates the lst of wos for selected sales invoice
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method generates the lst of wos "), _
SoapHeader("Authentication")> _
    Public Function MS_GetWOsForDebitNotes(ByVal BizDivId As Integer, ByVal WOIDS As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetDebitNoteListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@WOIDS", WOIDS)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet("DebitWorkOrders")
            da.Fill(ds)
            ds.Tables(0).TableName = "tblWorkOrders"
            ds.Tables(1).TableName = "tblCount"

            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GetDebitNoteListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function



    ''' <summary>
    ''' This method generates the sales invoice
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method generates the cash receipt for sales invoices"), _
SoapHeader("Authentication")> _
    Public Function MSAccounts_GenerateCashReceipt(ByVal BizDivId As Integer, ByVal SIs As String, ByVal BuyerId As Integer, ByVal ReceiptDate As String, ByVal PayInclExclDisc As Integer, ByVal UserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GenCashReceipt"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@SIs", SIs)
            cmd.Parameters.AddWithValue("@BuyerId", BuyerId)
            cmd.Parameters.AddWithValue("@ReceiptDate", ReceiptDate)
            cmd.Parameters.AddWithValue("@PayInclExclDisc", PayInclExclDisc)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserID)
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "Success"
                'following line is commeted as we have delinked the purchase invoices from the sales invoices
                'ds.Tables(1).TableName = "SupEmailData"
            End If
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GenCashReceipt. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Returns username information for given comma seperated Sales Invoice
    ''' </summary>
    ''' <param name="SIs"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method generates the cash receipt for sales invoices"), _
SoapHeader("Authentication")> _
    Public Function MSAccounts_GetSIEmailNotification(ByVal SIs As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GenSISendMail"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@SalesInvoice", SIs)

            Dim ds As New DataSet
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in MSAccounts_GetSIEmailNotification. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This method allocates the selected credit notes and receipts to the sales invoice
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method allocates the selected credit notes and receipts to the sales invoice"), _
SoapHeader("Authentication")> _
    Public Function MSAccounts_AllocateFunds(ByVal XMLContent As String, ByVal BizDivId As Integer, ByVal SIs As String, ByVal BuyerId As Integer, ByVal ReceiptDate As String, ByVal UserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_AssignUnAllocatedFunds"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@XMLContent", XMLContent)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@SIs", SIs)
            cmd.Parameters.AddWithValue("@BuyerId", BuyerId)
            cmd.Parameters.AddWithValue("@LoggedInUserID", userid)
            cmd.Parameters.AddWithValue("@ReceiptDate", ReceiptDate)
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            'ds.Tables(0).TableName = "SupEmailData"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GenCashReceipt. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while updating the status. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method returns the listing of purchase invoices - unpaid and available "), _
SoapHeader("Authentication")> _
    Public Function MS_GetReceiptAdviceDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetReceiptAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblInvoices"
            ds.Tables(2).TableName = "tblBillingAddress"
            ds.Tables(3).TableName = "tblOrderWorkDetails"
            ds.Tables(4).TableName = "tblOWAddress"
            ds.Tables(5).TableName = "tblUnAllocatedReceipt"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetAdviceListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Listing information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method returns the details of the un allocated receipt "), _
SoapHeader("Authentication")> _
    Public Function MS_GetUnAllocatedReceiptAdviceDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetUnAllocatedReceiptAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblInvoices"
            ds.Tables(2).TableName = "tblBillingAddress"
            ds.Tables(3).TableName = "tblOrderWorkDetails"
            ds.Tables(4).TableName = "tblOWAddress"
            ds.Tables(5).TableName = "tblUnAllocatedReceipt"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetAdviceListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Listing information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This function will get the bank details and the supplier balance for the given company id
    ''' </summary>
    ''' <param name="CompanyID"></param>
    ''' <param name="BizDivId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This function will get the bank details and the supplier balance for the given company id"), _
   SoapHeader("Authentication")> _
    Public Function MS_GetBankDetails(ByVal CompanyID As Integer, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetBankDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyId", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblBankDetails"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetBankDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving bank details and the supplier balance for the given company id. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Returns Dataset with information for Completed Sales report.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:=" "), _
      SoapHeader("Authentication")> _
        Public Function CompletedSalesReport(ByVal SelMonth As String, ByVal SelYear As String, ByVal EndDate As String, ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spAdmin_CompletedSalesRpt"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@SelMonth", SelMonth)
            cmd.Parameters.AddWithValue("@SelYear", SelYear)

            cmd.Parameters.AddWithValue("@EndDate", EndDate)

            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            ds.Tables(0).TableName = "Closed"
            ds.Tables(1).TableName = "ClosedVal"
            ds.Tables(2).TableName = "Listing"
            ds.Tables(3).TableName = "VATonListing"
            ds.Tables(4).TableName = "Comm"
            ds.Tables(5).TableName = "VATonComm"
            ds.Tables(6).TableName = "Cancelled"
            ds.Tables(7).TableName = "CancelledVal"

            ''funds
            ds.Tables(8).TableName = "FundsToRecvd"
            ds.Tables(9).TableName = "FundsRecvd"
            ds.Tables(10).TableName = "OutFundsToRecv"
            ds.Tables(11).TableName = "SuppPaym"
            ds.Tables(12).TableName = "ClientBal"



            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Returns Dataset with information for Current Sales report.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:=" "), _
      SoapHeader("Authentication")> _
        Public Function CurrentSalesReport(ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spAdmin_CurrentSalesRpt"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            ds.Tables(0).TableName = "Submitted"
            ds.Tables(1).TableName = "SubmittedVal"
            ds.Tables(2).TableName = "CA"
            ds.Tables(3).TableName = "CAVal"
            ds.Tables(4).TableName = "Active"
            ds.Tables(5).TableName = "ActiveVal"
            ' ds.Tables(4).TableName = "MinusCancelledVal"
            ds.Tables(6).TableName = "Closed"
            ds.Tables(7).TableName = "ClosedVal"
            ds.Tables(8).TableName = "Cancelled"
            ds.Tables(9).TableName = "CancelledVal"
            ds.Tables(10).TableName = "Listing"
            ds.Tables(11).TableName = "VATonListingFee"
            ds.Tables(12).TableName = "Comm"
            ds.Tables(13).TableName = "VATonComm"

            ''outstanding wos
            ds.Tables(14).TableName = "OSubmit"
            'ds.Tables(15).TableName = "OSent"
            ds.Tables(15).TableName = "OCA"
            ds.Tables(16).TableName = "OActive"
            ds.Tables(17).TableName = "OIssue"
            ds.Tables(18).TableName = "OCompleted"

            ''funds
            ds.Tables(19).TableName = "FundsToRecvd"
            ds.Tables(20).TableName = "FundsRecvd"
            ds.Tables(21).TableName = "OutFundsToRecv"
            ds.Tables(22).TableName = "SuppPaym"
            ds.Tables(23).TableName = "ClientBal"

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This webmethod will return all the purchase invoices"), _
      SoapHeader("Authentication")> _
      Public Function pagingtest(ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "DeleteMePagingTest1"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)
            Dim ds As New DataSet("PurchaseInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblPurchaseInvoices"
            ds.Tables(1).TableName = "tblCount"            
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This webmethod will return all the UpSell sales invoices"), _
     SoapHeader("Authentication")> _
     Public Function MS_GetUpSellSalesReceiptAdviceListing(ByVal BizDivId As Integer, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetUpSellSalesReceiptAdviceListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)

            Dim ds As New DataSet("UpSellSalesInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblUpSellSalesInvoices"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' SP Cretes the part payment - Adds funds to the buyer account as unallocated
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="ContactID"></param>
    ''' <param name="Particulars"></param>
    ''' <param name="Amount"></param>
    ''' <param name="Comments"></param>
    ''' <param name="AdminCompanyID"></param>
    ''' <param name="AdminContactID"></param>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will add unallocated funds"), _
     SoapHeader("Authentication")> _
     Public Sub MS_CreatePartPayment(ByVal BizDivId As Integer, ByVal CompanyID As Integer, ByVal ContactID As Integer, ByVal Particulars As String, ByVal Amount As Decimal, ByVal Comments As String, ByVal AdminCompanyID As Integer, ByVal AdminContactID As Integer, ByVal DateCreated As String, ByVal UserID As Integer)
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_CreatePartPayment"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@ContactID", ContactID)
            cmd.Parameters.AddWithValue("@Particulars", Particulars)
            cmd.Parameters.AddWithValue("@Amount", Amount)
            cmd.Parameters.AddWithValue("@Comments", Comments)
            cmd.Parameters.AddWithValue("@AdminCompanyID", AdminCompanyID)
            cmd.Parameters.AddWithValue("@AdminContactID", AdminContactID)
            cmd.Parameters.AddWithValue("@DateCreated", DateCreated)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserID)
            cmd.ExecuteNonQuery()
            'Dim ds As New DataSet("UpSellSalesInvoices")
            'Dim da As New SqlDataAdapter(cmd)
            'da.Fill(ds)
            'ds.Tables(0).TableName = "tblUpSellSalesInvoices"
            'ds.Tables(1).TableName = "tblCount"
            'Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Sub

    ''' <summary>
    ''' This method returns the details about the upsell sales invoice
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method returns the listing of upsell sales invoice "), _
SoapHeader("Authentication")> _
    Public Function MS_GetUpSellSalesAdviceDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetUpsellSalesAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblWODetails"
            ds.Tables(2).TableName = "tblOrderWorkDetails"
            ds.Tables(3).TableName = "tblOWAddress"
            ds.Tables(4).TableName = "tblWorkOrderList"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GetSalesAdviceDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Listing information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This function is used to retrieve upsell receipt details for the passed Invoice Number 
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns>Retuens a dataset storing upsell receipt details for the passed Invoice Number </returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method returns the listing of purchase invoices - unpaid and available "), _
SoapHeader("Authentication")> _
   Public Function MS_GetUpSellReceiptAdviceDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetUpSellReceiptAdviceDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            ds.Tables(0).TableName = "tblInvoices"
            ds.Tables(1).TableName = "tblBillingAddress"
            ds.Tables(2).TableName = "tblOrderWorkDetails"
            ds.Tables(3).TableName = "tblOWAddress"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spAccounts_GetAdviceListing. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Listing information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Returns a dataset with credit note details
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns>Returns a dataset with credit note details </returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Returns a dataset with credit note details "), _
SoapHeader("Authentication")> _
   Public Function MS_GetCreditNoteDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetCreditNoteDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            ds.Tables(0).TableName = "tblBillingAddress"
            ds.Tables(1).TableName = "tblCompRegNo"
            ds.Tables(2).TableName = "tblOWAddress"
            ds.Tables(3).TableName = "tblCreditNoteDetails"
            ds.Tables(4).TableName = "tblTotalAmount"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GetCreditNoteDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Web method will make the entries for the refund of the buyer
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method will make the entries for the refund of the buyer."), _
    SoapHeader("Authentication")> _
    Public Function MS_RefundBuyer(ByVal SINo As String, ByVal Amount As Decimal, ByVal Description As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String
            ProcedureName = "spMSAccounts_RefundBuyer"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@SINo", SINo)
            cmd.Parameters.AddWithValue("@Amount", Amount)
            cmd.Parameters.AddWithValue("@Description", Description)
            Dim ds As New DataSet("InvoiceNo")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblInvoiceNo"
            Return ds
        Catch ex As Exception
            Throw ex 'ExceptionUtil.CreateSoapException("Exception in InsertPostCodeCoordinates. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while inserting the values. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod will return all the Credit Notes
    ''' </summary>
    ''' <param name="CompanyID"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will return all the Credit Notes"), _
     SoapHeader("Authentication")> _
     Public Function MS_GetCreditNotesAdviceListing(ByVal CompanyID As Integer, ByVal BizDivId As Integer, ByVal mode As String, ByVal FromDate As String, ByVal ToDate As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetCreditNotesAdviceListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@mode", mode)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)

            Dim ds As New DataSet("CreditNotes")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblCreditNotes"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' This webmethod will mark the Credit Note as paid
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="CNotes"></param>
    ''' <param name="BuyerId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will mark the Credit Note as paid"), _
     SoapHeader("Authentication")> _
     Public Function MS_PayCreditNote(ByVal BizDivId As Integer, ByVal CNotes As String, ByVal BuyerId As Integer, ByVal InvoiceDate As String, ByVal UserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GenCashPaymentForCreditNote"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@CNs", CNotes)
            cmd.Parameters.AddWithValue("@BuyerId", BuyerId)
            cmd.Parameters.AddWithValue("@InvoiceDate", InvoiceDate)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserID)
            Dim ds As New DataSet("CreditNotes")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSuccess"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This webmethod will update the Invoice Date for a given Invoice Number
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <param name="NewDate"></param>
    ''' <returns>result</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will update the Invoice Date for a given Invoice Number"), _
     SoapHeader("Authentication")> _
    Public Function UpdateAccountsInvoiceDate(ByVal BizDivId As Integer, ByVal InvoiceNumber As String, ByVal NewDate As String, ByVal UserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_Accounting_UpdateInvoiceDate"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivId)
            cmd.Parameters.AddWithValue("@InvoiceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@NewDate", NewDate)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserID)
            Dim ds As New DataSet("Success")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSuccess"
            Return ds
            'cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            ''Execute(Query)
            'cmd.ExecuteNonQuery() ' To execute SP
            ''Get the output parameter as status stating that the operation is successfull
            'Dim result As Integer
            'result = cmd.Parameters.Item("@Success").Value
            'Return result
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This webmethod will update the Invoice Date for a given Invoice Number
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <param name="NewDate"></param>
    ''' <returns>result</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will update the Invoice Date for a given Invoice Number"), _
     SoapHeader("Authentication")> _
    Public Function UpdateAccountsInvoicePO(ByVal BizDivId As Integer, ByVal InvoiceNumber As String, ByVal NewInvoicePO As String, ByVal UserID As Integer) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_Accounting_UpdateInvoicePO"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivId)
            cmd.Parameters.AddWithValue("@InvoiceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@NewInvoicePO", NewInvoicePO)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserID)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            'Execute(Query)
            cmd.ExecuteNonQuery() ' To execute SP
            'Get the output parameter as status stating that the operation is successfull
            Dim result As Integer
            result = cmd.Parameters.Item("@Success").Value
            Return result
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod will update the VAT Details for a given Invoice Number
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns>"result"</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will update the VAT Details for a given Invoice Number"), _
     SoapHeader("Authentication")> _
     Public Function UpdateVATDetails(ByVal BizDivID As Integer, ByVal InvoiceNumber As String, ByVal UserID As Integer) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_Accounting_UpdateVATDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@InvoiceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserID)
            cmd.Parameters.AddWithValue("@Success", SqlDbType.Int).Direction = ParameterDirection.Output
            'Execute(Query)
            cmd.ExecuteNonQuery() ' To execute SP
            'Get the output parameter as status stating that the operation is successfull
            Dim result As Integer
            result = cmd.Parameters.Item("@Success").Value
            Return result
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod will update the Price for a given Work Order ID
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="WOID"></param>
    ''' <param name="NewPrice"></param>
    ''' <param name="Type"></param>
    ''' <returns>result</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will update the Price for a given Work Order ID"), _
     SoapHeader("Authentication")> _
    Public Function UpdatePrice(ByVal BizDivID As Integer, ByVal WOID As String, ByVal NewPrice As Decimal, ByVal Type As String, ByVal UserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_Accounting_UpdatePrice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@WorkOrderID", WOID)
            cmd.Parameters.AddWithValue("@NewPrice", NewPrice)
            cmd.Parameters.AddWithValue("@Type", Type)
            cmd.Parameters.AddWithValue("@LoggedInUserId", UserID)
            Dim ds As New DataSet("Success")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSuccess"
            Return ds
            ''Execute(Query)
            'cmd.ExecuteNonQuery() ' To execute SP
            ''Get the output parameter as status stating that the operation is successfull
            'Dim ds As New DataSet
            'Dim da As New SqlDataAdapter(cmd)
            'Dim result As Integer
            'da.Fill(ds)
            'result = CInt(ds.Tables(0).Rows(0)(0))
            'Return result
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This webmethod will update the Price for a given Work Order ID
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="WOID"></param>
    ''' <param name="NewPrice"></param>
    ''' <param name="Type"></param>
    ''' <returns>result</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will update the Upsell invoice for a given Work Order ID"), _
     SoapHeader("Authentication")> _
    Public Function UpdateUpsellInvoice(ByVal WorkOrderID As String, ByVal value As Decimal, ByVal UserID As Integer) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_CreateUpsellInvoiceAfterClosure"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WorkOrderID", WorkOrderID)
            cmd.Parameters.AddWithValue("@value", value)
            cmd.Parameters.AddWithValue("@LoggedInUserId", UserID)

            'Execute(Query)
            cmd.ExecuteNonQuery() ' To execute SP
            'Get the output parameter as status stating that the operation is successfull
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter(cmd)
            Dim result As Integer
            da.Fill(ds)
            result = CInt(ds.Tables(0).Rows(0)(0))
            Return result
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod will update the Price for a given InvoiceNo
    ''' </summary>
    ''' <param name="BizDivID"></param>
    ''' <param name="InvoiceNo"></param>
    ''' <param name="NewPrice"></param>
    ''' <param name="Type"></param>
    ''' <param name="UserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will update the Price for a given InvoiceNo"), _
     SoapHeader("Authentication")> _
     Public Function UpdateInvoicePrice(ByVal BizDivID As Integer, ByVal InvoiceNo As String, ByVal NewPrice As Decimal, ByVal Type As String, ByVal UserID As Integer) As Integer
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_Accounting_UpdateInvoicePrice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivID", BizDivID)
            cmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo)
            cmd.Parameters.AddWithValue("@NewPrice", NewPrice)
            cmd.Parameters.AddWithValue("@Type", Type)
            cmd.Parameters.AddWithValue("@LoggedInUserId", UserID)

            'Execute(Query)
            cmd.ExecuteNonQuery() ' To execute SP
            'Get the output parameter as status stating that the operation is successfull
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter(cmd)
            Dim result As Integer
            da.Fill(ds)
            result = CInt(ds.Tables(0).Rows(0)(0))
            Return result
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Returns a dataset with credit note details
    ''' </summary>
    ''' <param name="BizDivId"></param>
    ''' <param name="InvoiceNumber"></param>
    ''' <returns>Returns a dataset with credit note details </returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Returns a dataset with Debit note details "), _
SoapHeader("Authentication")> _
   Public Function MS_GetDebitNoteDetails(ByVal BizDivId As Integer, ByVal InvoiceNumber As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetDebitNoteDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            ds.Tables(0).TableName = "tblBillingAddress"
            ds.Tables(1).TableName = "tblCompRegNo"
            ds.Tables(2).TableName = "tblOWAddress"
            ds.Tables(3).TableName = "tblDebitNoteDetails"
            ds.Tables(4).TableName = "tblTotalAmount"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GetCreditNoteDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Web-Method to get the Unpaid Purchase Invoice Report - Data as in Supplier Payment Statement Report.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Customized for Orderwork. Pratik Trivedi - 19 Nov, 2008</remarks>
    <WebMethod(Description:="Returns a dataset with Unpaid PI details "), _
        SoapHeader("Authentication")> _
    Public Function MS_GetUnpaidPIReport(ByVal BizDivId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_ReportsServicePartnerPIExport"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblUnpaidPIDetails"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Created By Pankaj Malav on 19 Nov 2008
    ''' Returns dataset of Search Results for Finance
    ''' </summary>
    ''' <param name="InvoiceNo"></param>
    ''' <param name="PONumber"></param>
    ''' <param name="WorkOrderId"></param>
    ''' <param name="Keyword"></param>
    ''' <param name="CompanyName"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <param name="rowCount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Returns a dataset with Search Finance Results PI details "), _
        SoapHeader("Authentication")> _
    Public Function AdminSearchFinance(ByVal InvoiceNo As String, ByVal PONumber As String, ByVal WorkOrderId As String, ByVal Keyword As String, ByVal CompanyName As String, ByVal InvoiceDate As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByRef rowCount As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            'Added by Snehal to handle special single quote
            If Keyword.IndexOf("'") <> -1 Then
                Keyword = Keyword.Replace("'", "''")
            End If
            If PONumber.IndexOf("'") <> -1 Then
                PONumber = PONumber.Replace("'", "''")
            End If
            If InvoiceNo.IndexOf("'") <> -1 Then
                InvoiceNo = InvoiceNo.Replace("'", "''")
            End If
            If CompanyName.IndexOf("'") <> -1 Then
                CompanyName = CompanyName.Replace("'", "''")
            End If


            Dim ProcedureName As String = "spMSAccounts_SearchFinance"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@keyword", Keyword)
            cmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo)
            cmd.Parameters.AddWithValue("@PONumber", PONumber)
            cmd.Parameters.AddWithValue("@WorkOrderId", WorkOrderId)
            cmd.Parameters.AddWithValue("@CompanyName", CompanyName)
            cmd.Parameters.AddWithValue("@InvoiceDate", InvoiceDate)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)

            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSearchFinance"
            ds.Tables(1).TableName = "tblCount"
            rowCount = ds.Tables("tblCount").Rows(0)(0)
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_SearchFinance. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Added By Pankaj Malav on 20 Nov 2008
    ''' Updates the Billing Location of comma seperated woids
    ''' </summary>
    ''' <param name="WOIDs"></param>
    ''' <param name="CompanyID"></param>
    ''' <param name="NewAddrId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Updates the Billing Location of comma seperated woids"), _
           SoapHeader("Authentication")> _
       Public Function UpdateBillingLocation(ByVal WOIDs As String, ByVal CompanyID As String, ByVal NewAddrId As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMSAccounts_UpdateBillingLocations"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOIDs", WOIDs)
            cmd.Parameters.AddWithValue("@BuyerId", CompanyID)
            cmd.Parameters.AddWithValue("@NewAddrId", NewAddrId)
            cmd.Parameters.AddWithValue("@BizDivId", 1)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_UpdateBillingLocations. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving contact information for the search criterion. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Returns a dataset of Bank Of Scotland Exports Payment Details
    ''' </summary>
    ''' <param name="SubAccount"></param>
    ''' <param name="Status"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="bizDivId"></param>
    ''' <param name="UpdateVoucherStatus"></param>
    ''' <param name="sortExpression"></param>
    ''' <param name="startRowIndex"></param>
    ''' <param name="maximumRows"></param>
    ''' <returns>Dataset with payment records and payments summary</returns>
    ''' <remarks>Added by Pratik Trivedi - 18 Dec, 2008</remarks>
    <WebMethod(Description:="Returns a dataset of Bank Of Scotland Exports Payment Details"), _
        SoapHeader("Authentication")> _
    Public Function GetBOSExports(ByVal SubAccount As String, ByVal Status As String, ByVal FromDate As String, ByVal ToDate As String, ByVal bizDivId As String, ByVal UpdateVoucherStatus As String, ByVal sortExpression As String, ByVal startRowIndex As Integer, ByVal maximumRows As Integer, ByVal Invoices As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMSAccounts_GetBOSExportDetails"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@SubAccount", SubAccount)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@BizDivId", bizDivId)
            cmd.Parameters.AddWithValue("@UpdateVoucherStatus", UpdateVoucherStatus)
            cmd.Parameters.AddWithValue("@sortExpression", sortExpression)
            cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex)
            cmd.Parameters.AddWithValue("@maximumRows", maximumRows)
            cmd.Parameters.AddWithValue("@Invoices", Invoices)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblBOSPayments"
            ds.Tables(1).TableName = "tblTrailer"

            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GetBOSExportDetails. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Supplier Payment Details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' Returns a datset which shows confirmation screen on Sales Invoice generation
    ''' </summary>
    ''' <param name="WOIDs"></param>
    ''' <param name="ListingFees"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Returns a dataset showing confirmation screen in Sales Invoice Generation"), _
    SoapHeader("Authentication")> _
    Public Function GetSIConfirmationScreen(ByVal WOIDs As String, ByVal Mode As String, ByVal ListingFees As Decimal) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMS_AccountsSalesInvoiceGenerationConfirmation"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@WOIDs", WOIDs)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@ListingFees", ListingFees)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblParent"
            ds.Tables(1).TableName = "tblChild"

            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMS_AccountsSalesInvoiceGenerationConfirmation. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Supplier Payment Details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' Validates the invoices
    ''' </summary>
    ''' <param name="InvoiceNo"></param>
    ''' <param name="mode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Returns a dataset to validate Invoices (SI or PI)"), _
    SoapHeader("Authentication")> _
Public Function ValidateInvoices(ByVal InvoiceNo As String, ByVal mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMSAccounts_ValidateInvoice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo)
            cmd.Parameters.AddWithValue("@Mode", mode)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "tblInvoiceDetails"
            End If

            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_ValidateInvoice. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Supplier Payment Details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function


    ''' <summary>
    ''' Generate Debit Notes
    ''' </summary>
    ''' <param name="InvoiceNo"></param>
    ''' <param name="Amount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Returns a dataset to validate Invoices (SI or PI)"), _
    SoapHeader("Authentication")> _
Public Function MS_GenerateDebitNotes(ByVal InvoiceNo As String, ByVal Amount As Double, ByVal UserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMSAccounts_GenDebitNote"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@PI", InvoiceNo)
            cmd.Parameters.AddWithValue("@Amt", Amount)
            cmd.Parameters.AddWithValue("@LoggedInUserID", UserID)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "tblSuccess"
            End If

            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_ValidateInvoice. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Supplier Payment Details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="Returns a dataset to get success/failure of insertion of manual adjustment entries"), _
    SoapHeader("Authentication")> _
Public Function MS_ManualAdjustment(ByVal BizDivId As Integer, ByVal Invoices As String, ByVal BuyerId As Integer, ByVal AdjmtDate As String, ByVal AdjType As String, ByVal AdjCategory As String, ByVal AdjAmount As Decimal, ByVal InclVAT As Boolean) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMSAccounts_GenManualAdjustmentEntries"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@Invoices", Invoices)
            cmd.Parameters.AddWithValue("@BuyerId", BuyerId)
            cmd.Parameters.AddWithValue("@AdjType", AdjType)
            cmd.Parameters.AddWithValue("@AdjCategory", AdjCategory)
            cmd.Parameters.AddWithValue("@AdjDate", AdjmtDate)
            cmd.Parameters.AddWithValue("@AdjAmount", AdjAmount)
            cmd.Parameters.AddWithValue("@IncludeVAT", InclVAT)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            If ds.Tables.Count > 0 Then
                ds.Tables(0).TableName = "tblSuccess"
            End If

            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_ValidateInvoice. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Supplier Payment Details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="Returns a dataset to print sales invoices"), _
SoapHeader("Authentication")> _
Public Function MS_GetSalesAdviceDetails_PrintList(ByVal BizDivId As Integer, ByVal InvoiceNumber As String, ByVal UserId As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMSAccounts_GetSalesAdviceDetails_PrintList"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@CustomerId", UserId)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            ds.Tables(1).TableName = "tblWODetails"
            ds.Tables(2).TableName = "tblOrderWorkDetails"
            ds.Tables(3).TableName = "tblOWAddress"
            ds.Tables(4).TableName = "tblBankDetails"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSAccounts_GetSalesAdviceDetails_PrintList. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Supplier Payment Details. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="Returns a dataset to containing Value of InvoiceNumber"), _
SoapHeader("Authentication")> _
Public Function MS_GetInvoiceValue(ByVal InvoiceNumber As String, ByVal Mode As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try

            Dim ProcedureName As String = "spMSFinance_GetValue"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AdviceNumber", InvoiceNumber)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAdviceDetails"
            Return ds
        Catch ex As Exception
            Throw ExceptionUtil.CreateSoapException("Exception in spMSFinance_GetValue. Ex: " & ex.ToString, SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, "Encountered an error while retrieving Value of InvoiceNumber. Please try again.", ex)
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    <WebMethod(Description:="This webmethod will return all the UpSell sales invoices"), _
    SoapHeader("Authentication")> _
    Public Function SagePayExport(ByVal Mode As String, ByVal FromDate As String, ByVal ToDate As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMS_WO_SagePayExport"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure


            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.CommandTimeout = 180
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ds.Tables(0).TableName = "tblSagePay"

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    ''' <summary>
    ''' This webmethod will return all the Credit Notes
    ''' </summary>
    ''' <param name="CompanyID"></param>
    ''' <param name="BizDivId"></param>
    ''' <param name="FromDate"></param>
    ''' <param name="ToDate"></param>
    ''' <param name="SortExpression"></param>
    ''' <param name="StartRowIndex"></param>
    ''' <param name="MaximumRows"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will return all the Invoices"), _
     SoapHeader("Authentication")> _
    Public Function MS_GetAllInvoices(ByVal CompanyID As Integer, ByVal BizDivId As Integer, ByVal FromDate As String, ByVal ToDate As String, ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetAllInvoices"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
            cmd.Parameters.AddWithValue("@BizDivId", BizDivId)
            cmd.Parameters.AddWithValue("@FromDate", FromDate)
            cmd.Parameters.AddWithValue("@ToDate", ToDate)
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)

            Dim ds As New DataSet("AllInvoices")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblAllInvoices"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function

    ''' <summary>
    ''' This webmethod will return all the Credit Notes
    ''' </summary>
    ''' <param name="InvoiceNo"></param>   
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod will return the Invoices to edit"), _
     SoapHeader("Authentication")> _
    Public Function MS_GetInvoiceToEdit(ByVal InvoiceNo As String) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetInvoiceToEdit"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo)

            Dim ds As New DataSet("InvoiceToEdit")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblInvoiceToEdit"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This method will add or edit TimeBuilder QA"), _
    SoapHeader("Authentication")> _
    Public Function EditInvoice(ByVal xmlContent As String, ByVal InvoiceNo As String, ByVal Mode As String, ByVal Note As String, ByVal LoggedInUserID As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_BulkAction_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_EditInvoice"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@xmlContent", xmlContent)
            cmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo)
            cmd.Parameters.AddWithValue("@Mode", Mode)
            cmd.Parameters.AddWithValue("@Note", Note)
            cmd.Parameters.AddWithValue("@LoggedInUserID", LoggedInUserID)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
    <WebMethod(Description:="This webmethod will return all the Invoices list to Edit"), _
    SoapHeader("Authentication")> _
    Public Function GetInvoiceToEditListing(ByVal SortExpression As String, ByVal StartRowIndex As Integer, ByVal MaximumRows As Integer) As DataSet
        CommonFunctions.setCultureSettings(country, siteType)
        Dim conn As SqlConnection = CommonFunctions.getDBConn(LT_CONNECTION_NAME)
        Try
            Dim ProcedureName As String = "spMSAccounts_GetInvoiceToEditListing"
            Dim cmd As New SqlCommand(ProcedureName, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@SortExpression", SortExpression)
            cmd.Parameters.AddWithValue("@StartRowIndex", StartRowIndex)
            cmd.Parameters.AddWithValue("@MaximumRows", MaximumRows)

            Dim ds As New DataSet("InvoiceToEdit")
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            ds.Tables(0).TableName = "tblInvoiceToEdit"
            ds.Tables(1).TableName = "tblCount"
            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            Try : conn.Close() : Catch : End Try
        End Try
    End Function
End Class